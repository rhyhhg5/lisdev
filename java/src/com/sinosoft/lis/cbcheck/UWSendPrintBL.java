package com.sinosoft.lis.cbcheck;

import java.util.*;

import utils.system;

import com.ibm.db2.jcc.a.bo;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.f1print.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
//import com.sinosoft.lis.tb.CommonBL;
//import com.sinosoft.lis.tb.TempFeeAppBL;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统个人单人工核保送打印队列部分</p>
 * <p>Description: 逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author WHN
 * @version 1.0
 */
public class UWSendPrintBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    private String mManageCom;

    /** 业务处理相关变量 */

    private String mContNo = "";

    private String mOldPolNo = "";

    private GlobalInput mGlobalInput = new GlobalInput();

    /**打印队列表**/
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

    private LOPRTManagerSet mLOPRTManagerSet = new LOPRTManagerSet();

    /**打印处理函数**/
    private PrintManagerBL tPrintManagerBL = new PrintManagerBL();

    private LMUWSet mLMUWSet = new LMUWSet();

    /**标记自核通过*/
    private String AutoUWFlag = "";
    private String UWFlag = "";

    /** 是否打印通知书标志。 */
    private String mNoPrtFlag = null;

    public UWSendPrintBL()
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        int flag = 0; //判断是不是所有数据都不成功
        int j = 0; //符合条件数据个数

        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();

        System.out.println("---1---");
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        System.out.println("---UWSendPrintBL getInputData---");

        mOldPolNo = mLOPRTManagerSchema.getOtherNo();
        mContNo = mLOPRTManagerSchema.getOtherNo();

        //校验
        if (!checkUW())
        {
            return false;
        }
        //校验
        if (!checkPrint())
        {
            return false;
        }
        System.out.println("---UWSendPrintBL checkData---");
        // 数据操作业务处理
        if (!dealData())
        {
            return false;
        }

        System.out.println("---UWSendPrintBL dealData---");
        //准备给后台的数据
        prepareOutputData();

        System.out.println("---UWSendPrintBL prepareOutputData---");

        for (int i = 1; i <= mLOPRTManagerSet.size(); i++)
        {
            LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
            tLOPRTManagerSchema = mLOPRTManagerSet.get(i);

            mInputData.clear();
            mInputData.add(mGlobalInput);
            mInputData.add(tLOPRTManagerSchema);
            System.out.println("tLOPRTManagerSchema : " + tLOPRTManagerSchema);
            //            System.out.println("mGlobalInput : "+mGlobalInput);
            if (!tPrintManagerBL.submitData(mInputData, "REQUEST"))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tPrintManagerBL.mErrors);
                CError tError = new CError();
                tError.moduleName = "UWSendPrintBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }

        //发送核保通过短信
        sendSMS(mContNo);

        System.out.println("---UWSendPrintBL commitData---");
        return true;
    }

	private boolean checkUW() {
		UWFlag = new ExeSQL().getOneValue("select 1 from lwmission lw where missionprop2='"
					+ mContNo+ "' and activityid='0000001104' and exists (select 1 from lwmission lwm where lwm.missionid=lw.missionid and activityid='0000001110')  union all    select 1 from lwmission lw where missionprop2='"
					+ mContNo+ "' and activityid='0000001200'");
		System.out.println("UWFlag================"+UWFlag);
		return true;
	}

	/**
     * 数据操作类业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean dealData()
    {

        if (dealOnePol() == false)
        {
            return false;
        }

        return true;
    }

    /**
     * 操作一张保单的业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean dealOnePol()
    {
        // 健康信息
        if (preparePrint() == false)
        {
            return false;
        }
        /**
         * 解决银行转帐问题.当发交费通知
         * 书时向财务收费中插入银行转帐信息
         */
        if (prepareBankData() == false)
        {
            return false;
        }

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mOperate = mGlobalInput.Operator;
        mManageCom = mGlobalInput.ManageCom;
        if (mGlobalInput == null)
        {
            buildError("getInputData", "传入的操作信息为空！");
            return false;
        }
        mLOPRTManagerSchema.setSchema((LOPRTManagerSchema) cInputData
                .getObjectByObjectName("LOPRTManagerSchema", 0));
        TransferData tTransferData = new TransferData();
        tTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        if (tTransferData != null)
        {
            if (!StrTool.cTrim(
                    (String) tTransferData.getValueByName("AutoUWFlag"))
                    .equals(""))
            {
                this.AutoUWFlag = (String) tTransferData
                        .getValueByName("AutoUWFlag");
            }

            mNoPrtFlag = (String) tTransferData.getValueByName("NoPrtFlag");
        }
        if (mLOPRTManagerSchema == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "UWSendPrintBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有传入数据!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 校验是否打印
     * @return
     */
    private boolean checkPrint()
    {
        //生成流水号
        String tsql = "select distinct 1 from LOPRTManager where otherno = '"
                + mContNo
                + "' and code = '"
                + mLOPRTManagerSchema.getCode().trim()
                + "' and stateflag = '0' and prtseq = (select max(prtseq) from LOPRTManager where otherno = '"
                + mContNo + "' and code = '"
                + mLOPRTManagerSchema.getCode().trim() + "')";
        System.out.println(tsql);

        ExeSQL tExeSQL = new ExeSQL();
        String tflag = tExeSQL.getOneValue(tsql);

        if (tflag.trim().equals("1"))
        {
            CError tError = new CError();
            tError.moduleName = "UWSendPrintBL";
            tError.functionName = "checkPrint";
            tError.errorMessage = "已在打印队列尚未打印!";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (mLOPRTManagerSchema.getCode().equals("15"))
        {
            tsql = "select distinct 1 from LOPRTManager where otherno = '"
                    + mContNo + "' and code = '"
                    + mLOPRTManagerSchema.getCode().trim() + "'";
            System.out.println(tsql);

            tExeSQL = new ExeSQL();
            tflag = tExeSQL.getOneValue(tsql);
            System.out.println("tflag" + tflag);
            if (tflag.trim().equals("1"))
            {
                System.out.println("tflag" + tflag);
                CError tError = new CError();
                tError.moduleName = "UWSendPrintBL";
                tError.functionName = "checkPrint";
                tError.errorMessage = "在签单处已发缴费催办通知书!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }

        return true;
    }

    /**
     * 校验是不是主险
     * @return
     */

    private boolean checkMain()
    {

        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setContNo(mContNo);
        LCPolSet tLCPolSet = new LCPolSet();
        tLCPolSet = tLCPolDB.query();
        if (tLCPolSet.size() == 0)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCPolDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "UWSendPrintBL";
            tError.functionName = "checkMain";
            tError.errorMessage = "没有保单信息!";
            this.mErrors.addOneError(tError);
            return false;
        }
        double tTemperFee = 0.0;
        double tRealPayMoney = 0.0;
        double tSumRealPayMoney = 0.0;
        String tStrSQL;
        ExeSQL tExeSQL = new ExeSQL();
        for (int n = 1; n <= tLCPolSet.size(); n++)
        {
            LCPolSchema tLCPolSchema = new LCPolSchema();
            tLCPolSchema = tLCPolSet.get(n);

            //发首期交费通知书的时候需要判断，该投保单是否确实到帐的金额不足

            //计算以交保费

            tStrSQL = "select sum(PayMoney) from ("
                    + "select sum(PayMoney) PayMoney from LJTempFee where "
                    + "TempFeeType in ('0','6','1') and otherno in (select polno from lcpol where PolNo='"
                    + tLCPolSchema.getPolNo()
                    + "') and EnterAccDate is not null"
                    + " union "
                    + "select sum(PayMoney) PayMoney from LJTempFee where "
                    + "TempFeeType in ('1','5') and otherno in (select prtno from lcpol where PolNo='"
                    + tLCPolSchema.getPolNo()
                    + "') and riskcode in (select riskcode from lcpol where PolNo='"
                    + tLCPolSchema.getPolNo()
                    + "') and EnterAccDate is not null" + ") as a ";

            SSRS ssrs = tExeSQL.execSQL(tStrSQL);
            String arr = "";
            if (ssrs.equals("null"))
            {
                arr = ssrs.GetText(1, 1);
            }
            else
            {
                arr = "0";
            }
            tRealPayMoney = Double.parseDouble(arr);
            tSumRealPayMoney += tRealPayMoney;

        }
        System.out.println("实际交的金额:" + tSumRealPayMoney);

        if (tSumRealPayMoney == 0)
        {
            tTemperFee = 0.0;
        }
        else
        {
            tTemperFee = tSumRealPayMoney;
        }

        System.out.println("实际交的金额:" + tTemperFee);
        //计算应交保费
        double tPrem = 0.0;
        String tShouldPayMoney = "";
        tStrSQL = "select sum(prem) from LCCont where ContNo='"
                + mLOPRTManagerSchema.getOtherNo() + "'";

        tShouldPayMoney = tExeSQL.getOneValue(tStrSQL);

        System.out.println("应该交的金额:" + tShouldPayMoney);

        if (tShouldPayMoney == null || tShouldPayMoney.trim().equals(""))
        {
            tPrem = 0.0;
        }
        else
        {
            tPrem = Double.parseDouble(tShouldPayMoney);
        }

        //实交与应交进行比较
        if (tTemperFee >= tPrem)
        {
            CError tError = new CError();
            tError.moduleName = "UWSendPrintBL";
            tError.functionName = "checkMain";
            tError.errorMessage = "该投保单到帐的金额已足,不必再发发首期交费通知书";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;

    }

    /**
     * 校验是不是主险
     * @return
     */
    private boolean checkMain15()
    {

        //计算已交保费
        ExeSQL tExeSQL = new ExeSQL();
        double tTemperFee = 0.0;
        String tRealPayMoney = "";
        String tStrSQL = "select sum(PayMoney) from ("
                + "select sum(PayMoney) PayMoney from LJTempFee where "
                + "TempFeeType in ('0','6','1') and otherno in (select contno from lccont where ContNo='"
                + mLOPRTManagerSchema.getOtherNo()
                + "') and EnterAccDate is not null"
                + " union "
                + "select sum(PayMoney) PayMoney from LJTempFee where "
                + "TempFeeType in ('1','5') and otherno in (select prtno from lCGrpcont where GrpContNo='"
                + mLOPRTManagerSchema.getOtherNo()
                + "') and EnterAccDate is not null" + ") as a";

        tRealPayMoney = tExeSQL.getOneValue(tStrSQL);

        System.out.println("实际交的金额:" + tRealPayMoney);
        if (tRealPayMoney == null || tRealPayMoney.trim().equals(""))
        {
            tTemperFee = 0.0;
        }
        else
        {
            tTemperFee = Double.parseDouble(tRealPayMoney);
        }

        System.out.println("实际交的金额:" + tTemperFee);
        //计算应交保费
        double tPrem = 0.0;
        String tShouldPayMoney = "";
        tStrSQL = "select prem from lccont where ContNo='"
                + mLOPRTManagerSchema.getOtherNo() + "'";

        tShouldPayMoney = tExeSQL.getOneValue(tStrSQL);

        System.out.println("应该交的金额:" + tShouldPayMoney);

        if (tShouldPayMoney == null || tShouldPayMoney.trim().equals(""))
        {
            tPrem = 0.0;
        }
        else
        {
            tPrem = Double.parseDouble(tShouldPayMoney);
        }

        //实交与应交进行比较
        if (tTemperFee >= tPrem)
        {
            CError tError = new CError();
            tError.moduleName = "UWSendPrintBL";
            tError.functionName = "checkMain";
            tError.errorMessage = "该投保单到帐的金额已足,不必再发缴费通知书";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 准备打印信息
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean preparePrint()
    {
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mLOPRTManagerSchema.getOtherNo());
        if (tLCContDB.getInfo() == false)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCContDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "UWSendPrintBL";
            tError.functionName = "preparePrint";
            tError.errorMessage = "没有保单信息!";
            this.mErrors.addOneError(tError);
            return false;
        }

        LCContSchema tLCContSchema = new LCContSchema();
        tLCContSchema = tLCContDB.getSchema();

        if ("1".equals(mNoPrtFlag))
        {
            System.out.println(tLCContSchema.getPrtNo() + "：不需要打印缴费通知书。");
            return true;
        }

        mLOPRTManagerSchema.setAgentCode(tLCContSchema.getAgentCode());
        mLOPRTManagerSchema.setPrtType(PrintManagerBL.PT_FRONT);
        mLOPRTManagerSchema.setManageCom(tLCContSchema.getManageCom());

        //通知书类型
        if (mLOPRTManagerSchema.getOtherNoType().equals("00"))
        {
            mLOPRTManagerSchema.setOtherNoType(PrintManagerBL.ONT_INDPOL);
        }
        if (mLOPRTManagerSchema.getOtherNoType().equals("01"))
        {
            mLOPRTManagerSchema.setOtherNoType(PrintManagerBL.ONT_GRPPOL);
        }
        if (mLOPRTManagerSchema.getOtherNoType().equals("02"))
        {
            mLOPRTManagerSchema.setOtherNoType(PrintManagerBL.ONT_CONT);
        }
        if (mLOPRTManagerSchema.getOtherNoType().equals("03"))
        {
            mLOPRTManagerSchema.setOtherNoType(PrintManagerBL.ONT_EDOR);
        }

        //单据类型
        if (mLOPRTManagerSchema.getCode().equals("06"))
        {
            //mLOPRTManagerSchema.setCode(PrintManagerBL.CODE_URGE);
            if (prepareURGE(mLOPRTManagerSchema) == false)
            {
                return false;
            }
        }
        if (mLOPRTManagerSchema.getCode().equals("07"))
        {
            // 判断保单是否需要打印通知书。
            if (!checkCanSendFirstPay(tLCContSchema))
                return true;
            // -------------------------------

            System.out.println("缴费方式为:" + tLCContSchema.getPayMode());
            // 对于缴费方式为银行转帐的，不发送缴费通知书。
//            if ("4".equals(tLCContSchema.getPayMode()))
//            {
//                return true;
//            }
            // -------------------------------

            //生成首期交费通知书号码，不再使用打印流水号，by Minim at 20050512
            String prtSeq = "";

            // 如果，先收费保单需要发缴费通知时，缴费凭证号，应为录单录入的缴费凭证号。
            //            if (CommonBL.getIsFristPayOfSingleCont(tLCContSchema.getContNo()))
            //            {
            //                prtSeq = tLCContSchema.getTempFeeNo();
            //            }
            //            else
            //            {
            prtSeq = PubFun1.CreateMaxNo("PAYNOTICENO", tLCContSchema
                    .getPrtNo());
            //            }

            if (prtSeq == null || "".equals(prtSeq))
            {
                CError tError = new CError();
                tError.moduleName = "UWSendPrintBL";
                tError.functionName = "preparePrint";
                tError.errorMessage = "获取缴费通知书号失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            // -------------------------------

            mLOPRTManagerSchema.setPrtSeq(prtSeq);
            System.out.println("按PICC规则的首期交费通知书号:"
                    + mLOPRTManagerSchema.getPrtSeq());

            // 先收费的保单不进行原有保费是否足额的校验。
            //            if (!CommonBL.getIsFristPayOfSingleCont(tLCContSchema.getContNo()))
            //            {
            //                if (!checkMain())
            //                {
            //                    return false;
            //                }
            //            }
            LDSysVarDB tLDSysVarDB = new LDSysVarDB();
            tLDSysVarDB.setSysVar("URGEInterval");

            if (tLDSysVarDB.getInfo() == false)
            {
                CError tError = new CError();
                tError.moduleName = "UWSendPrintBL";
                tError.functionName = "prepareURGE";
                tError.errorMessage = "没有描述催发间隔!";
                this.mErrors.addOneError(tError);
                return false;
            }
            FDate tFDate = new FDate();
            int tInterval = Integer.parseInt(tLDSysVarDB.getSysVarValue());
            Date tDate = PubFun.calDate(
                    tFDate.getDate(PubFun.getCurrentDate()), tInterval, "D",
                    null);

            mLOPRTManagerSchema.setCode(PrintManagerBL.CODE_FIRSTPAY);
            mLOPRTManagerSchema.setForMakeDate(tDate);

            mLOPRTManagerSet.add(mLOPRTManagerSchema);
        }

        if (mLOPRTManagerSchema.getCode().equals("15"))
        {
            if (!checkMain15())
            {
                return false;
            }

            mLOPRTManagerSchema.setCode(PrintManagerBL.CODE_URGE_PB);

            mLOPRTManagerSet.add(mLOPRTManagerSchema);
        }
        return true;
    }

    /**
     * 取催发数据
     */
    private boolean prepareURGE(LOPRTManagerSchema tLOPRTManagerSchema)
    {
        LDSysVarDB tLDSysVarDB = new LDSysVarDB();
        tLDSysVarDB.setSysVar("URGEInterval");

        if (tLDSysVarDB.getInfo() == false)
        {
            CError tError = new CError();
            tError.moduleName = "UWSendPrintBL";
            tError.functionName = "prepareURGE";
            tError.errorMessage = "没有描述催发间隔!";
            this.mErrors.addOneError(tError);
            return false;
        }

        FDate tFDate = new FDate();
        int tInterval = Integer.parseInt(tLDSysVarDB.getSysVarValue());
        System.out.println(tInterval);

        Date tDate = PubFun.calDate(tFDate.getDate(PubFun.getCurrentDate()),
                tInterval, "D", null);
        System.out.println(tDate);

        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        LOPRTManagerSet tLOPRTManagerSet = new LOPRTManagerSet();
        String tsql = "select * from loprtmanager where otherno = '"
                + tLOPRTManagerSchema.getOtherNo()
                + "' and code in ('03','05','07') and stateflag = '2' and donedate = "
                + tDate;
        //    String tsql = "select * from loprtmanager where otherno = '"+ tLOPRTManagerSchema.getOtherNo() +"'"
        //
        //                + " and code in ('03','05','07') ";
        System.out.println(tsql);
        tLOPRTManagerSet = tLOPRTManagerDB.executeQuery(tsql);

        if (tLOPRTManagerSet.size() > 0)
        {
            for (int i = 1; i <= tLOPRTManagerSet.size(); i++)
            {
                LOPRTManagerSchema t2LOPRTManagerSchema = new LOPRTManagerSchema();
                t2LOPRTManagerSchema = tLOPRTManagerSet.get(i);

                mLOPRTManagerSchema.setCode(t2LOPRTManagerSchema.getCode());
                mLOPRTManagerSet.add(mLOPRTManagerSchema);
            }
        }

        return true;
    }

    /**
     *准备需要保存的数据
     **/
    private void prepareOutputData()
    {
        //mInputData.clear();
        // mInputData.add( mGlobalInput);
        //mInputData.add( mLOPRTManagerSet );
    }

    /**
     * 用于客户选择交费方式为银行转帐
     * 自动在财务收费里面添加一条银行
     * 转帐信息
     * 2005-05-23 杨明 添加
     * @return boolean
     */
    private boolean prepareBankData()
    {

        LCContDB forBankLCContDB = new LCContDB();
        LCPolDB forBankLCPolDB = new LCPolDB();
        LCPolSet forBankLCPolSet = new LCPolBLSet();
        LCContSchema forBankLCContSchema = new LCContSchema();
        LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();
        LJTempFeeClassSet tLJTempFeeClassSet = new LJTempFeeClassSet();
        //存储各种银行信息,银行编码,户名,帐号
        String BankFlag = "";
        String BankCode = "";
        String BankAccNo = "";
        String AccName = "";
        /**
         * 合同信息
         */
        forBankLCContDB.setContNo(this.mContNo);
        if (!forBankLCContDB.getInfo())
        {
            CError tError = new CError();
            tError.moduleName = "UWSendPrintBL";
            tError.functionName = "prepareBankData";
            tError.errorMessage = "在准备银行转账信息时查询合同信息失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        forBankLCContSchema = forBankLCContDB.getSchema();
        BankFlag = forBankLCContSchema.getPayMode();
        BankCode = forBankLCContSchema.getBankCode();
        BankAccNo = forBankLCContSchema.getBankAccNo();
        AccName = forBankLCContSchema.getAccName();

        // 正常先收费保单不生成，银行转帐信息。
        //        if ("1".equals(forBankLCContSchema.getContType())
        //                && "0".equals(forBankLCContSchema.getCardFlag())
        //                && CommonBL.getIsFristPayOfSingleCont(forBankLCContSchema
        //                        .getContNo()))
        //        {
        //            return true;
        //        }

        //校验是否银行在途
    	String bankOnTheWay = new ExeSQL().getOneValue("select db2inst1.nvl(count(1), 0) from LJSPay where OtherNo = '" 
    			+ forBankLCContSchema.getPrtNo() + "' and OtherNoType in ('9','16') and BankOnTheWayFlag = '1'");
    	if(!bankOnTheWay.equals("0.0"))
    	{
    		CError tError = new CError();
            tError.moduleName = "UWSendPrintBL";
            tError.functionName = "prepareBankData";
            tError.errorMessage = "保单银行在途，请待回盘之后进行相同操作！";
            this.mErrors.addOneError(tError);
            return false;
    	}
        
    	MMap mmap = new MMap();
    	mmap.put("update ljspay set CanSendBank = '1' where otherno = '"
                 + forBankLCContSchema.getPrtNo() + "' and (BankOnTheWayFlag is null or BankOnTheWayFlag = '0')", "UPDATE");
    	VData mVData = new VData();
        mVData.add(mmap);
        PubSubmit mPubSubmit = new PubSubmit();
        if (!mPubSubmit.submitData(mVData, "UPDATE"))
        {
            this.mErrors.copyAllErrors(mPubSubmit.mErrors);
            return false;
        }
    	//删除非银行在途，且锁定的财务应收记录
    	MMap map = new MMap();
        SSRS getNoticeNo = new ExeSQL().execSQL("select distinct a.tempfeeno from ljtempfee "
                                + " a left join ljspay b "
                                + " on a.tempfeeno = b.getnoticeno "
                                + " and (b.BankOnTheWayFlag is null or b.BankOnTheWayFlag = '0') "
                                + " and b.OtherNoType in ('9','16') "
                                + " and b.CanSendBank = '1' "
                                + " where a.otherno='"
                                + forBankLCContSchema.getPrtNo()+"'"
                                + " and a.enteraccdate is null "
                                );
         for (int i = 1; i <= getNoticeNo.MaxRow; i++) {
             String getNoticeNoString = getNoticeNo.GetText(i, 1);
             map.put("delete from LJSPayB where GetNoticeNo = '"
                      + getNoticeNoString + "'", "DELETE");
             //备份到B表
             map.put("insert into LJSPayB (select GetNoticeNo, OtherNo, OtherNoType, AppntNo, SumDuePayMoney, PayDate, "
                      + "BankOnTheWayFlag, BankSuccFlag, SendBankCount, ApproveCode, ApproveDate, SerialNo, Operator, MakeDate, "
                      + "MakeTime, ModifyDate, ModifyTime, ManageCom, AgentCom, AgentType, BankCode, BankAccNo, RiskCode, "
                      + "AgentCode, AgentGroup, AccName, StartPayDate, PayTypeFlag, '7', '', '1',markettype,salechnl "
                      + "from LJSPay where GetNoticeNo = '"
                      + getNoticeNoString + "')","INSERT");
             //删除未到帐的财务暂收记录
             map.put("delete from LJSPay where GetNoticeNo = '"
                      + getNoticeNoString + "'", "DELETE");
             map.put("delete from ljtempfeeclass where tempfeeno = '"
                      + getNoticeNoString+"'"+" and EnterAccDate is null ", "DELETE");
             map.put("delete from ljtempfee where tempfeeno = '"
                      + getNoticeNoString + "'"+" and OtherNoType = '4' and EnterAccDate is null ", "DELETE");
         }//end modify
        // --------------------
    	
        if ("4".equals(BankFlag)||"8".equals(BankFlag))
        { //4代表银行转账
            if (StrTool.cTrim(BankAccNo).equals("")
                    || StrTool.cTrim(BankCode).equals("")
                    || BankAccNo.equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "UWSendPrintBL";
                tError.functionName = "prepareBankData";
                tError.errorMessage = "交费方式选择银行转账,但银行代码,银行账号,户名信息不完整!";
                this.mErrors.addOneError(tError);
                return false;
            }

            /**
             * 险种信息
             */
            forBankLCPolDB.setContNo(mContNo);
            forBankLCPolSet = forBankLCPolDB.query();

            if (forBankLCPolSet.size() < 1)
            {
                CError tError = new CError();
                tError.moduleName = "UWSendPrintBL";
                tError.functionName = "prepareBankData";
                tError.errorMessage = "在准备银行转账信息时查询险种信息有错!";
                this.mErrors.addOneError(tError);
                return false;
            }


            // 删掉非转帐数据
//            MMap tDelMap = null;
//            tDelMap = delOtherTempFee(forBankLCContSchema);
//            if (tDelMap == null)
//            {
//                return false;
//            }
//            map.add(tDelMap);
//            tDelMap = null;
//

            
           MMap tTempFeeMap = null;
           tTempFeeMap = dealTempFeeDataOfBank(forBankLCContSchema);
           if (tTempFeeMap == null)
           {
               return false;
           }
           map.add(tTempFeeMap);
//         tTempFeeMap = null;

            // 尚未更新正式机。
            // 先收费处理
            //            if (!dealPayFirst(forBankLCContSchema, tLJTempFeeSet,
            //                    tLJTempFeeClassSet))
            //            {
            //                return false;
            //            }
            // -------------------------------

        }
        VData tVData = new VData();
        tVData.addElement(tLJTempFeeSet);
        tVData.addElement(tLJTempFeeClassSet);
        tVData.addElement(mGlobalInput);
        tVData.add(map);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(tVData, "DELETE&INSERT"))
        {
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        
        return true;
    }

    /**
     * dealPayFirst
     * 处理先收费业务
     * @param cLJTempFeeSet LJTempFeeSet
     * @param cLJTempFeeClassSet LJTempFeeClassSet
     * @return boolean
     */
    private boolean dealPayFirst(LCContSchema tLCContSchlema,
            LJTempFeeSet cLJTempFeeSet, LJTempFeeClassSet cLJTempFeeClassSet)
    {
        //若保单不是先收费(先收费=银行转账+TempFeeNo不为空)，则不处理。若先收费校验规则变化，则需要修改if内容
        if (!"4".equals(tLCContSchlema.getPayMode())
                || tLCContSchlema.getTempFeeNo() == null
                || tLCContSchlema.getTempFeeNo().equals(""))
        {
            return true;
        }

        //校验是否已先收费回盘
        LJTempFeeClassDB tLJTempFeeClassDB = new LJTempFeeClassDB();
        tLJTempFeeClassDB.setTempFeeNo(tLCContSchlema.getTempFeeNo());
        tLJTempFeeClassDB.setPayMode("12"); //先收费
        LJTempFeeClassSet tLJTempFeeClassSet = tLJTempFeeClassDB.query();

        for (int i = 1; i <= cLJTempFeeSet.size(); i++)
        {
            cLJTempFeeSet.get(i).setTempFeeNo(tLCContSchlema.getPrtNo()); //先收费的TempFeeNo存印刷号
            cLJTempFeeSet.get(i).setTempFeeType("16"); //先收费的TempFeeType
            cLJTempFeeSet.get(i).setSaleChnl(tLCContSchlema.getSaleChnl());

            if (tLJTempFeeClassSet.size() > 0)
            {
                cLJTempFeeSet.get(i).setEnterAccDate(
                        tLJTempFeeClassSet.get(1).getEnterAccDate());
                cLJTempFeeSet.get(i).setPayDate(
                        tLJTempFeeClassSet.get(1).getPayDate());
                cLJTempFeeSet.get(i).setSerialNo(
                        tLJTempFeeClassSet.get(1).getSerialNo());
                cLJTempFeeSet.get(i).setConfMakeDate(
                        tLJTempFeeClassSet.get(1).getConfMakeDate());
            }
        }

        cLJTempFeeClassSet.clear(); //先收费不生成class

        return true;
    }

    /**
     * 校验保单是否需要发送缴费通知书。
     * 目前原则：足额，不发缴费通知书。
     * @return true-需发送；false-不需要发送
     */
    private boolean checkCanSendFirstPay(LCContSchema tLCContSchema)
    {
        double tDifMoney = 0;
        tDifMoney = calDifPrem(tLCContSchema, AutoUWFlag);

        return tDifMoney < 0;
    }

    /**
     * 获取保单险种的追加保费金额。
     * <br />目前只有万能类险种，有新契约追加保费概念。
     * @param cRiskCode 险种代码
     * @return 追加保费金额，如果险种无新契约追加保费概念，则返回0。
     */
    private double getPolSupplementaryPrem(String cRiskCode)
    {
        double tSupplementaryPrem = 0d;
        // 目前只有万能类险种，在新单发盘时累计追加保费。
        if (com.sinosoft.lis.bq.CommonBL.isULIRisk(cRiskCode))
        {
            String tStrSql = null;

            if (StrTool.cTrim(this.AutoUWFlag).equals("1"))
            {
                // 自核通过时，视为每个险种均可正常承保。
                tStrSql = "select sum(lcp.SupplementaryPrem) from LCPol lcp "
                        + " where lcp.ContNo = '" + mContNo + "' "
                        + " and lcp.RiskCode = '" + cRiskCode + "' ";
            }
            else
            {
                tStrSql = "select sum(lcp.SupplementaryPrem) from LCPol lcp "
                        + " where lcp.UWFlag in ('4', '9') "
                        + " and lcp.ContNo = '" + mContNo + "' "
                        + " and lcp.RiskCode = '" + cRiskCode + "' ";
            }
            String tResult = new ExeSQL().getOneValue(tStrSql);
            if (!"".equals(tResult))
            {
                tSupplementaryPrem = Double.parseDouble(tResult);
            }
        }
        return tSupplementaryPrem;
    }

    private boolean sendSMS(String tContNo)
    {
        try
        {
            LDSysVarDB tLDSysVarDB = new LDSysVarDB();
            tLDSysVarDB.setSysVar("Mobile");
            if (tLDSysVarDB.getInfo() == false)
            {
                CError tError = new CError();
                tError.moduleName = "UWSendPrintBL";
                tError.functionName = "sendSMS";
                tError.errorMessage = "没有短信接口是否开通的描述!";
                this.mErrors.addOneError(tError);
                return false;
            }
            // 1-表示开通 0-表示未开通
            if ("1".equals(tLDSysVarDB.getSysVarValue()))
            {
                if (tContNo == null || tContNo.equals("")
                        || tContNo.equals("null"))
                {
                    CError tError = new CError();
                    tError.moduleName = "UWSendPrintBL";
                    tError.functionName = "sendSMS";
                    tError.errorMessage = "查询首期交费通知书相应的保单号码失败!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                //保单表信息
                LCContDB tLCContDB = new LCContDB();
                tLCContDB.setContNo(tContNo);
                if (!tLCContDB.getInfo())
                {
                    CError tError = new CError();
                    tError.moduleName = "UWSendPrintBL";
                    tError.functionName = "sendSMS";
                    tError.errorMessage = "查询首期交费通知书相应的保单失败!";
                    this.mErrors.addOneError(tError);
                    return false;
                }

                // 判断是否需要发送短信，如不需要直接返回true。
                String tContCardFlag = tLCContDB.getCardFlag();
                tContCardFlag = tContCardFlag == null ? "0" : tContCardFlag;

                String tStrSql = "select CodeName from LDCode "
                        + " where CodeType = 'smssendflag' " + " and Code = '"
                        + tContCardFlag + "'";

                String tIsNeed = new ExeSQL().getOneValue(tStrSql);
                if (!"1".equals(tIsNeed))
                {
                    return true;
                }
                // ----------------------------

                //投保人表信息
                LCAppntDB tLCAppntDB = new LCAppntDB();
                tLCAppntDB.setContNo(tContNo);
                if (!tLCAppntDB.getInfo())
                {
                    CError tError = new CError();
                    tError.moduleName = "UWSendPrintBL";
                    tError.functionName = "sendSMS";
                    tError.errorMessage = "查询投保人信息失败!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                //地址表信息
                LCAddressDB tLCAddressDB = new LCAddressDB();
                tLCAddressDB.setCustomerNo(tLCContDB.getAppntNo());
                tLCAddressDB.setAddressNo(tLCAppntDB.getAddressNo());
                if (!tLCAddressDB.getInfo())
                {
                    CError tError = new CError();
                    tError.moduleName = "UWSendPrintBL";
                    tError.functionName = "sendSMS";
                    tError.errorMessage = "查询投保人地址信息失败!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                String tAppntPhone = tLCAddressDB.getMobile(); //投保人手机
                //代理人信息
                LAAgentDB tLAAgentDB = new LAAgentDB();
                tLAAgentDB.setAgentCode(tLCContDB.getAgentCode());
                if (!tLAAgentDB.getInfo())
                {
                    CError tError = new CError();
                    tError.moduleName = "UWSendPrintBL";
                    tError.functionName = "sendSMS";
                    tError.errorMessage = "查询代理人信息失败!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                String tAgentMobile = tLAAgentDB.getMobile();
                //区别性别
                String tSex = "";
                if (tLCContDB.getAppntSex().equals("0"))
                {
                    tSex = "先生";
                }
                else
                {
                    tSex = "女士";
                }

                ExeSQL tExeSQL = new ExeSQL();
                String Prem = tExeSQL
                        .getOneValue("select sum(prem) from lcpol where contno = '"
                                + tContNo
                                + "' and uwflag in ('4','9') with ur ");
                if (StrTool.cTrim(Prem).equals(""))
                {
                    CError tError = new CError();
                    tError.moduleName = "UWSendPrintBL";
                    tError.functionName = "sendSMS";
                    tError.errorMessage = "保单费用查询失败!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                double tPrem = Double.parseDouble(Prem);
                if (tPrem <= 0)
                {
                    CError tError = new CError();
                    tError.moduleName = "UWSendPrintBL";
                    tError.functionName = "sendSMS";
                    tError.errorMessage = "保单费用不能小于0元!";
                    this.mErrors.addOneError(tError);
                    return false;
                }

                //如险种为万能险，首期保费金额应该为“期缴保费+首期追加保费”。
                String premFlag = "首期保费";
                String ULISQL = new ExeSQL()
                        .getOneValue("select count(1) from LCPol a, LMRiskApp b where a.ContNo = '"
                                + tContNo
                                + "' and a.RiskCode = b.RiskCode and b.RiskType4 = '4'");
                if (Integer.parseInt(ULISQL) > 0)
                    premFlag = "期缴保费+首期追加保费";

                if (tAppntPhone == null || tAppntPhone.equals("")
                        || tAppntPhone.equals("null"))
                {
                    System.out.print("该投保人没有手机号码信息");
                }
                else
                {
                    String tAppntText = "尊敬的" + tLCContDB.getAppntName() + tSex
                            + "，您的投保申请" + tLCContDB.getPrtNo() + "号保单已经审核通过，"
                            + premFlag + Prem + "元，为确保您及时享有保障，请您尽快缴纳保险费。"
                            + "详情可致电我公司客户服务专线“95591或4006695518”。";
                    //投保人短信消息
                    PubFun.sendSMS(tAppntPhone, tAppntText, tLCContDB
                            .getManageCom());
                }
                if (tAgentMobile == null || tAgentMobile.equals("")
                        || tAgentMobile.equals("null"))
                {
                    System.out.print("该代理人没有手机号码信息");
                }
                else
                {
                    String tAgentText = "您的客户" + tLCContDB.getAppntName()
                            + tSex + "递交的投保申请" + tLCContDB.getPrtNo()
                            + "已审核通过，" + premFlag + Prem + "元，请协助客户及时交费生效保单。";
                    //业务员短信消息
                    PubFun.sendSMS(tAgentMobile, tAgentText, tLCContDB
                            .getManageCom());
                }
            }
            return true;
        }
        catch (Exception ex)
        {
            System.out.print("发送短信失败！");
            return false;
        }
    }

    /**
     * 获取暂收保费与应缴保费差值。
     * 原则：差值 = 暂收保费 - 应缴保费
     * @param tContInfo
     * @return
     */
    private double calDifPrem(LCContSchema tContInfo, String tAutoUWFlag)
    {
        ExeSQL tExeSQL = new ExeSQL();
        String tStrSql = null;
        String tResult = null;

        // 校验到帐保费是否足额。
        if (StrTool.cTrim(tAutoUWFlag).equals("1"))
        {
            // 自核通过的情况
            double tContSumPrem = 0;
            double tContTempFeeSumMoney = 0;
            double tContTheWarMoney = 0;

            // 获取整单保费，由于自核通过时，uwflag尚未更新数据库，因此视为该单下全部包含险种的总保费即为整单保费
            String tContNo = tContInfo.getContNo();
            tResult = null;
            tStrSql = null;
            tStrSql = "select nvl(sum(nvl(lcp.Prem, 0) + nvl(lcp.SupplementaryPrem, 0)), 0) from LCPol lcp "
                    + " where lcp.ContNo = '" + tContNo + "' ";
            tResult = tExeSQL.getOneValue(tStrSql);
            tContSumPrem = Double.parseDouble(tResult);
            tResult = null;
            tStrSql = null;
            // --------------------

            System.out.println("tContSumPrem:" + tContSumPrem);

            // 获取暂收总保费。
            String tPrtNo = tContInfo.getPrtNo();
            tResult = null;
            tStrSql = null;
            tStrSql = " select nvl(sum(ljtf.PayMoney), 0) from LJTempFee ljtf "
                    + " where ljtf.OtherNoType = '4' " // 个单暂收核销前标志
                    // + " and ljtf.RiskCode != '000000' " // 除去保全相关暂收数据
                    + " and ljtf.ConfFlag = '0' " // 可用销暂收数据
                    + " and ljtf.EnterAccDate is not null " // 财务到帐
                    + " and ljtf.OtherNo = '" + tPrtNo + "' ";
            tResult = tExeSQL.getOneValue(tStrSql);
            tContTempFeeSumMoney = Double.parseDouble(tResult);
            tResult = null;
            tStrSql = null;
            // --------------------
            
            tResult = null;
            tStrSql = null;
            tStrSql = " select nvl(sum(SumDuePayMoney),0) from ljspay ljs "
                    + " where ljs.BankOnTheWayFlag='1' and ljs.OtherNoType in ('16','1') " // 个单暂收核销前标志
                    + " and otherno = '"+tPrtNo+"'";
            tResult = tExeSQL.getOneValue(tStrSql);
            tContTheWarMoney = Double.parseDouble(tResult);
            tResult = null;
            tStrSql = null;
            
            System.out.println("tContTempFeeSumMoney:" + tContTempFeeSumMoney);
            System.out.println("Dif:" + (tContTempFeeSumMoney - tContSumPrem));

            return (tContTempFeeSumMoney - tContSumPrem + tContTheWarMoney);

        }
        else
        {
            String tPrtNo = tContInfo.getPrtNo();

            tResult = null;
            tStrSql = null;
            tStrSql = "select LF_PayMoneyNo('" + tPrtNo + "') from Dual";
            tResult = new ExeSQL().getOneValue(tStrSql);
            if ("".equals(tResult))
            {
                // 不会出现这种情况，为防止万一，如果产生，后台报错，但不阻断，不发通知书。
                String tStrErr = "以印刷号查询，未查到合同号。";
                System.out.println(tStrErr);
                buildError("getDifPrem", tStrErr);
                return 0;
            }
            double tOutPrem = 0;
            tOutPrem = Double.parseDouble(tResult);
            System.out.println("DifPrem:" + tOutPrem);
            tResult = null;
            tStrSql = null;
            return tOutPrem;
        }
    }

    /**
     * 删除未到帐的暂收轨迹。
     * @param cContInfo
     * @return
     */
    private MMap delOtherTempFee(LCContSchema cContInfo)
    {
        MMap tMMap = new MMap();

        String tPrtNo = cContInfo.getPrtNo();
        String tStrSql = " select * from LJTempFee ljtf "
                + " where 1 = 1 and ljtf.EnterAccDate is null "
                + " and ljtf.OtherNo = '" + tPrtNo + "' ";
        LJTempFeeSet tTempFeeSet = new LJTempFeeDB().executeQuery(tStrSql);
        tMMap.put(tTempFeeSet, SysConst.DELETE);

        tStrSql = null;
        tStrSql = " select * from LJTempFeeClass ljtfc "
                + " where ljtfc.TempFeeNo in ("
                + " select ljtf.TempFeeNo from LJTempFee ljtf "
                + " where 1 = 1 and ljtf.EnterAccDate is null "
                + " and ljtf.OtherNo = '" + tPrtNo + "') ";
        LJTempFeeClassSet tTempFeeClassSet = new LJTempFeeClassDB()
                .executeQuery(tStrSql);
        tMMap.put(tTempFeeClassSet, SysConst.DELETE);

        return tMMap;
    }

    /**
     * 产生银行发盘暂收数据。
     * @param cContInfo
     * @return
     */
    private MMap dealTempFeeDataOfBank(LCContSchema cContInfo)
    {
        MMap tMMap = new MMap();

        String prtSeq = PubFun1
                .CreateMaxNo("PAYNOTICENO", cContInfo.getPrtNo());
        String serNo = PubFun1
                .CreateMaxNo("SERIALNO", cContInfo.getManageCom());

        GregorianCalendar Calendar = new GregorianCalendar();
        Calendar.setTime((new FDate()).getDate(PubFun.getCurrentDate()));
        Calendar.add(Calendar.DATE, 0);
        
//      计算不足保费
        double tDifMoney = 0;
        
        String Sql1="select distinct(agreedpaydate) from lccontsub where prtno='"+cContInfo.getPrtNo()+"' and agreedpayflag='1'";
        ExeSQL agExeSQL = new ExeSQL();
        SSRS SqlResult1=agExeSQL.execSQL(Sql1);
   	 	String paydate="";
   	 	String sysdate=PubFun.getCurrentDate();
   	 	if(SqlResult1!=null && SqlResult1.getMaxRow()>0){
   		 paydate=SqlResult1.GetText(1, 1);
   		 if(sysdate.compareTo(paydate)>0){
   			paydate=sysdate;
   		 }
   	 	}
        
        if(cContInfo.getPayMode().equals("8")){
        	 ExeSQL tExeSQL = new ExeSQL();
        	 String sql="select distinct riskcode from lcpol where contno='"+cContInfo.getContNo()+"'";
             SSRS riskSSR=tExeSQL.execSQL(sql);
             if(riskSSR==null||riskSSR.getMaxRow()<1){
            	 CError tError = new CError();
                 tError.moduleName = "UWSendPrintBL";
                 tError.functionName = "getRiskcode";
                 tError.errorMessage = "未找到险种信息!";
                 this.mErrors.addOneError(tError);                
             }else{
            	 for(int k=1;k<=riskSSR.getMaxRow();k++){
                	 LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
                     tLJTempFeeSchema.setTempFeeNo(prtSeq);
                     tLJTempFeeSchema.setTempFeeType("1");
                     tLJTempFeeSchema.setRiskCode(riskSSR.GetText(k, 1));

                     tLJTempFeeSchema.setAgentGroup(cContInfo.getAgentGroup());
                     tLJTempFeeSchema.setAPPntName(cContInfo.getAppntName());
                     tLJTempFeeSchema.setAgentCode(cContInfo.getAgentCode());
                     tLJTempFeeSchema.setPayDate(Calendar.getTime());
                     if(!"".equals(paydate)){
                     	tLJTempFeeSchema.setPayDate(paydate);
                     }
                     String money="select sum(prem)+sum(SupplementaryPrem) from lcpol where contno='"+cContInfo.getContNo()+"' and riskcode='"+riskSSR.GetText(k, 1)+"' ";
                     double mon=Double.parseDouble(tExeSQL.getOneValue(money));
                     tLJTempFeeSchema.setPayMoney(mon);
                     // --------------------

                     tLJTempFeeSchema.setManageCom(cContInfo.getManageCom());
                     tLJTempFeeSchema.setOtherNo(cContInfo.getPrtNo());
                     tLJTempFeeSchema.setOtherNoType("4");
                     tLJTempFeeSchema.setPolicyCom(cContInfo.getManageCom());
                     tLJTempFeeSchema.setSerialNo(serNo);
                     tLJTempFeeSchema.setConfFlag("0");
                     tLJTempFeeSchema.setOperator(mGlobalInput.Operator);
                     tLJTempFeeSchema.setMakeDate(PubFun.getCurrentDate());
                     tLJTempFeeSchema.setModifyDate(PubFun.getCurrentDate());
                     tLJTempFeeSchema.setModifyTime(PubFun.getCurrentTime());
                     tLJTempFeeSchema.setMakeTime(PubFun.getCurrentTime());

                     tMMap.put(tLJTempFeeSchema, SysConst.INSERT);
                 	
                     tDifMoney+=tLJTempFeeSchema.getPayMoney();
                 }
                             	 
             }
                          
        }
        else{
        	
        	tDifMoney = calDifPrem(cContInfo, AutoUWFlag);
            if (tDifMoney >= 0)
            {
                System.out.println("暂收保费足额：【" + tDifMoney + "】");
                return new MMap();// 如果足额，不生成银行数据。
            }
        	
        	LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
            tLJTempFeeSchema.setTempFeeNo(prtSeq);
            tLJTempFeeSchema.setTempFeeType("1");
            tLJTempFeeSchema.setRiskCode("000000");

            tLJTempFeeSchema.setAgentGroup(cContInfo.getAgentGroup());
            tLJTempFeeSchema.setAPPntName(cContInfo.getAppntName());
            tLJTempFeeSchema.setAgentCode(cContInfo.getAgentCode());
            tLJTempFeeSchema.setPayDate(Calendar.getTime());
            if(!"".equals(paydate)){
            	tLJTempFeeSchema.setPayDate(paydate);
            }

          
            
            tDifMoney = -1 * tDifMoney;
            tLJTempFeeSchema.setPayMoney(tDifMoney);
            // --------------------

            tLJTempFeeSchema.setManageCom(cContInfo.getManageCom());
            tLJTempFeeSchema.setOtherNo(cContInfo.getPrtNo());
            tLJTempFeeSchema.setOtherNoType("4");
            tLJTempFeeSchema.setPolicyCom(cContInfo.getManageCom());
            tLJTempFeeSchema.setSerialNo(serNo);
            tLJTempFeeSchema.setConfFlag("0");
            tLJTempFeeSchema.setOperator(mGlobalInput.Operator);
            tLJTempFeeSchema.setMakeDate(PubFun.getCurrentDate());
            tLJTempFeeSchema.setModifyDate(PubFun.getCurrentDate());
            tLJTempFeeSchema.setModifyTime(PubFun.getCurrentTime());
            tLJTempFeeSchema.setMakeTime(PubFun.getCurrentTime());

            tMMap.put(tLJTempFeeSchema, SysConst.INSERT);
        }
       
        
        

        LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();
        tLJTempFeeClassSchema.setTempFeeNo(prtSeq);
        tLJTempFeeClassSchema.setPayMode(cContInfo.getPayMode());
        tLJTempFeeClassSchema.setPayDate(Calendar.getTime());
        if(!"".equals(paydate)){
        	tLJTempFeeClassSchema.setPayDate(paydate);
        }
        tLJTempFeeClassSchema.setPayMoney(tDifMoney);
        tLJTempFeeClassSchema.setManageCom(cContInfo.getManageCom());
        tLJTempFeeClassSchema.setPolicyCom(cContInfo.getManageCom());

        String BankCode = cContInfo.getBankCode();
        String BankAccNo = cContInfo.getBankAccNo();
        String AccName = cContInfo.getAccName();
        tLJTempFeeClassSchema.setBankCode(BankCode);
        //上海医保人核之后锁待收费数据
        ExeSQL mExeSQL = new ExeSQL();
   	    String sqlRisk="select riskcode  from lcpol where contno='"+cContInfo.getContNo()+"'";
        SSRS riskSSR1=mExeSQL.execSQL(sqlRisk);
        if(riskSSR1==null||riskSSR1.getMaxRow()<1){
        	 CError tError = new CError();
             tError.moduleName = "UWSendPrintBL";
             tError.functionName = "getRiskcode";
             tError.errorMessage = "未找到险种信息!";
             this.mErrors.addOneError(tError);                
         }else{
        	  if("123201".equals(riskSSR1.GetText(1, 1)) || "220601".equals(riskSSR1.GetText(1, 1)) || "123202".equals(riskSSR1.GetText(1, 1)) || "220602".equals(riskSSR1.GetText(1, 1))){
        		  tLJTempFeeClassSchema.setBankCode("");
              }
        	 
        }
        tLJTempFeeClassSchema.setBankAccNo(BankAccNo);
        tLJTempFeeClassSchema.setAccName(AccName);

        tLJTempFeeClassSchema.setSerialNo(serNo);
        tLJTempFeeClassSchema.setConfFlag("0");
        tLJTempFeeClassSchema.setOperator(mGlobalInput.Operator);
        tLJTempFeeClassSchema.setMakeDate(PubFun.getCurrentDate());
        tLJTempFeeClassSchema.setModifyDate(PubFun.getCurrentDate());
        tLJTempFeeClassSchema.setModifyTime(PubFun.getCurrentTime());
        tLJTempFeeClassSchema.setMakeTime(PubFun.getCurrentTime());

        tMMap.put(tLJTempFeeClassSchema, SysConst.INSERT);

        if("PD".equals(cContInfo.getPrtNo().substring(0, 2))&&!"1".equals(UWFlag)){   
        	LCPolSchema tLCPolSchema = new LCPolSchema();
    		LCPolSet mLCPolSet = new LCPolSet();
    		LCPolDB tLCPolDB = new LCPolDB();
    		tLCPolDB.setContNo(cContInfo.getContNo());
    		mLCPolSet = tLCPolDB.query();
            //查询出主险保单数据
    		for (int j = 1; j <= mLCPolSet.size(); j++) {
    			tLCPolSchema = mLCPolSet.get(j);
    			if (tLCPolSchema.getPolNo().equals(tLCPolSchema.getMainPolNo())) {
    				break;
    			}
    		}
    		FDate fDate = new FDate();
    		Date paytoDate = new Date(); // 交费日期
    		paytoDate = fDate.getDate(tLCPolSchema.getCValiDate());
    		String strPayDate = fDate.getString(PubFun.calDate(paytoDate, 2, "M",
    				null));
        	LJSPaySchema tLJSPaySchema = new LJSPaySchema();
    		// 如果有多条暂交费纪录，其暂交费号是相同的
    		tLJSPaySchema.setGetNoticeNo(tLJTempFeeClassSchema.getTempFeeNo());
    		tLJSPaySchema.setOtherNo(cContInfo.getPrtNo());
    		tLJSPaySchema.setOtherNoType("9");
    		tLJSPaySchema.setAppntNo(cContInfo.getAppntNo());
    		tLJSPaySchema.setPayDate(strPayDate);
    		tLJSPaySchema.setStartPayDate(cContInfo.getCValiDate());
    		if(!"".equals(paydate)){
    			tLJSPaySchema.setStartPayDate(paydate);
    		}
    		tLJSPaySchema.setBankSuccFlag("0");
    		tLJSPaySchema.setSendBankCount(0);// 送银行次数
    		tLJSPaySchema.setSumDuePayMoney(tDifMoney);
    		tLJSPaySchema.setApproveCode(cContInfo.getApproveCode());
    		tLJSPaySchema.setApproveDate(cContInfo.getApproveDate());
    		tLJSPaySchema.setRiskCode(tLCPolSchema.getRiskCode());
    		tLJSPaySchema.setBankAccNo(tLJTempFeeClassSchema.getBankAccNo());// 个人保单表没有，从暂交费子表中取
    		tLJSPaySchema.setBankCode(tLJTempFeeClassSchema.getBankCode());
    		tLJSPaySchema.setAccName(tLJTempFeeClassSchema.getAccName());
    		tLJSPaySchema.setManageCom(cContInfo.getManageCom());
    		tLJSPaySchema.setAgentCode(cContInfo.getAgentCode());
    		tLJSPaySchema.setAgentGroup(cContInfo.getAgentGroup());
    		tLJSPaySchema.setSerialNo(serNo); // 流水号
    		tLJSPaySchema.setCanSendBank("p");
    		tLJSPaySchema.setOperator(mGlobalInput.Operator);
    		tLJSPaySchema.setMakeDate(PubFun.getCurrentDate());
    		tLJSPaySchema.setMakeTime(PubFun.getCurrentTime());
    		tLJSPaySchema.setModifyDate(PubFun.getCurrentDate());
    		tLJSPaySchema.setModifyTime(PubFun.getCurrentTime());
    		System.out.println("不是人核近来的也不是核保回复进来的？");
    		tMMap.put(tLJSPaySchema, SysConst.INSERT);
        }       

        return tMMap;
        
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "UWSendPrintBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }

}
