package com.sinosoft.lis.cbcheck;

import com.sinosoft.lis.db.LDPersonDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.db.LCRReportDB;
import com.sinosoft.lis.vdb.LCRReportDBSet;
import com.sinosoft.lis.db.LCCUWMasterDB;
import com.sinosoft.lis.vschema.LCRReportResultSet;
import com.sinosoft.lis.vschema.LCRReportSet;


/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: SinoSoft</p>
 * @author HYQ
 * @version 1.0
 */

public class RReportResultBL
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    private VData mInputData;
    private GlobalInput tGI = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;
    private String mOperator;
    private String mManageCom;

    /** 业务操作类 */
    private LCRReportResultSet mLCRReportResultSet = new LCRReportResultSet();
    private LCRReportSchema mLCRReportSchema = new LCRReportSchema();

    private LCCUWMasterSchema mLCCUWMasterSchema = new LCCUWMasterSchema();

    /** 业务数据 */
    private String mName = "";
    private String mDelSql = "";

    public RReportResultBL()
    {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        System.out.println("Operate==" + cOperate);
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        System.out.println("After getinputdata");

        if (!checkData())
        {
            return false;
        }

        //进行业务处理
        if (!dealData())
        {
            return false;
        }
        System.out.println("After dealData！");
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        System.out.println("After prepareOutputData");

        System.out.println("Start RReportResultBL Submit...");

        PubSubmit tSubmit = new PubSubmit();

        if (!tSubmit.submitData(mResult, ""))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSubmit.mErrors);

            CError tError = new CError();
            tError.moduleName = "GrpUWAutoChkBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        System.out.println("RReportResultBL end");

        return true;
    }


    /**
     * prepareOutputData
     *
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        mResult.clear();
        MMap map = new MMap();

        //map.put(mDelSql, "DELETE");
        //map.put(mLCRReportResultSet, "INSERT");
        map.put(mLCRReportSchema,"UPDATE");
        map.put(mLCCUWMasterSchema,"UPDATE");
        mResult.add(map);

        return true;
    }


    /**
     * dealData
     * 业务逻辑处理
     * @return boolean
     */
    private boolean dealData()
    {

      if (prepareReport() == false)
      {
         return false;
      }
                        //先删除已经存过的数据
        //mDelSql = "delete from lcRReportresult where 1=1 "
        //          + " and ProposalContNo= '" +
        //          mLCRReportResultSet.get(1).getProposalContNo() + "'"
        //          + " and prtseq = '" + mLCRReportResultSet.get(1).getPrtSeq() +
        //          "'"
        //          ;
        //int tSerialNo = 1;
        //for (int i = 1; i <= mLCRReportResultSet.size(); i++)
        //{
        //   mLCRReportResultSet.get(i).setName(mName);
        //    mLCRReportResultSet.get(i).setSerialNo("" + tSerialNo);
        //    mLCRReportResultSet.get(i).setOperator(mOperator);
        //    mLCRReportResultSet.get(i).setMakeDate(PubFun.getCurrentDate());
        //    mLCRReportResultSet.get(i).setMakeTime(PubFun.getCurrentTime());
        //    mLCRReportResultSet.get(i).setModifyDate(PubFun.getCurrentDate());
        //    mLCRReportResultSet.get(i).setModifyTime(PubFun.getCurrentTime());
        //    tSerialNo++;
        //}
        return true;
    }


    /**
     * checkData
     * 数据校验
     * @return boolean
     */
    private boolean checkData()
    {
        LDPersonDB tLDPersonDB = new LDPersonDB();
        LDPersonSchema tLDPersonSchema = new LDPersonSchema();
        tLDPersonDB.setCustomerNo(mLCRReportSchema.getCustomerNo());
        if (!tLDPersonDB.getInfo())
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCGrpContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "RReportResultBL";
            tError.functionName = "checkData";
            tError.errorMessage = "客户信息查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        tLDPersonSchema = tLDPersonDB.getSchema();
        mName = tLDPersonSchema.getName();
        return true;
    }

    private boolean prepareReport()
            {
              //取回复内容
              String tReplyContent = mLCRReportSchema.getReplyContente();
              String tContent = mLCRReportSchema.getContente();
              System.out.println(tReplyContent);
              //取生调记录
              String tsql = "select * from LCRReport where ContNo = '"+mLCRReportSchema.getProposalContNo()+"' and prtseq =  '"+mLCRReportSchema.getPrtSeq()+"'";

              System.out.println("sql:"+tsql);
              LCRReportDB tLCRReportDB = new LCRReportDB();
              LCRReportSet tLCRReportSet = new LCRReportSet();

              tLCRReportSet = tLCRReportDB.executeQuery(tsql);

              if(tLCRReportSet.size() == 0)
              {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "UWRReportReplyBL";
                tError.functionName = "prepareReport";
                tError.errorMessage = "此单没有经过契调管理回复!";
                this.mErrors .addOneError(tError) ;
                return false;
              }
              else
              {
                mLCRReportSchema = tLCRReportSet.get(1);
              }
              mLCRReportSchema.setContente(tContent);
              mLCRReportSchema.setReplyContente(tReplyContent);
              mLCRReportSchema.setReplyFlag("1");
              mLCRReportSchema.setReplyOperator(mOperate);
              mLCRReportSchema.setReplyDate(PubFun.getCurrentDate());
              mLCRReportSchema.setReplyTime(PubFun.getCurrentTime());
              mLCRReportSchema.setModifyDate(PubFun.getCurrentDate());
              mLCRReportSchema.setModifyTime(PubFun.getCurrentTime());

              //核保主表信息
              LCCUWMasterDB tLCCUWMasterDB = new LCCUWMasterDB();
              tLCCUWMasterDB.setContNo(mLCRReportSchema.getContNo());

              if(tLCCUWMasterDB.getInfo() == false)
              {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "UWRReportBL";
                tError.functionName = "prepareReport";
                tError.errorMessage = "无核保主表信息!";
                this.mErrors .addOneError(tError) ;
                return false;
              }

              mLCCUWMasterSchema = tLCCUWMasterDB.getSchema();


              mLCCUWMasterSchema.setReportFlag("2");

              return true;
	}
    /**
     * getInputData
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        // 公用变量
        tGI = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        //mLCRReportResultSet = (LCRReportResultSet) cInputData.
        //                      getObjectByObjectName("LCRReportResultSet", 0);
       mLCRReportSchema.setSchema((LCRReportSchema)cInputData.getObjectByObjectName("LCRReportSchema",0));

        mOperator = tGI.Operator;
        if (mOperator == null || mOperator.length() <= 0)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCGrpContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "RReportResultBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据Operator失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //取得疾病信息
        //if (mLCRReportResultSet == null || mLCRReportResultSet.size() <= 0)
        //{
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCGrpContDB.mErrors );
         //   CError tError = new CError();
         //   tError.moduleName = "RReportResultBL";
         //   tError.functionName = "getInputData";
          //  tError.errorMessage = "前台传输数据LCRReportResultSet失败!";
         //   this.mErrors.addOneError(tError);
          //  return false;
       // }
        return true;
    }

    /**
     * 返回结果
     * @return VData
     */
    public VData getResult()
    {
        return mResult;
    }

}
