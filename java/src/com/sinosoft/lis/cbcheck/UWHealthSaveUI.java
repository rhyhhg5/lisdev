package com.sinosoft.lis.cbcheck;

import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;

/**
 * <p>Title: PICCH核心业务系统</p>
 *体检信息录入用户接口类
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class UWHealthSaveUI
{
    public CErrors mErrors = new CErrors();

    public UWHealthSaveUI()
    {
    }

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     *
     * @param cInputData VData：包括：
     * 1、GlobalInput对象，操作员信息
     * 2、
     * @param operate String： 数据操作字符串，
     * @return boolean：true提交成功, false提交失败
     */
    public boolean submitData(VData cInputData, String operate)
    {
        UWHealthSaveBL bl = new UWHealthSaveBL();
        if(!bl.submitData(cInputData, operate))
        {
            mErrors.copyAllErrors(bl.mErrors);
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
        UWHealthSaveUI ui = new UWHealthSaveUI();
    }
}
