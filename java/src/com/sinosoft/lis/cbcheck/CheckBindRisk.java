package com.sinosoft.lis.cbcheck;

import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * 绑定产品拒保类核保结论校验类(个险)
 * 
 * @author 张成轩
 */
public class CheckBindRisk {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	private CErrors mErrors = new CErrors();

	/**
	 * 校验绑定产品的险种核保结论(校验拒保类核保结论,个险)
	 * 
	 * @param tLCContSchema
	 *            保单信息Schema集合
	 * @return 校验标志
	 */
	public boolean checkUWFlag(LCContSchema tLCContSchema) {
		// 校验传入的字段是否为空
		if (tLCContSchema == null || tLCContSchema.getContNo() == null) {
			addError("传入的查询信息为空", "checkUWFlag");
			return false;
		}
		// 获取保单号
		String contNo = tLCContSchema.getContNo();
		// 查询是否有拒保类核保结论,提取mainpolno,对一组绑定产品仅有一个mainpolno
		String querySql = "select polno from lcpol where contno='" + contNo
				+ "' and uwflag in ('a','1','8')";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = tExeSQL.execSQL(querySql);
		System.out.println("--记录数--" + tSSRS.MaxRow);
		// 对每个mainpolno依此循环
		for (int row = 1; row <= tSSRS.MaxRow; row++) {
			LCPolSchema tLCPolSchema = new LCPolSchema();
			tLCPolSchema.setPolNo(tSSRS.GetText(row, 1));
			GetBindRisk tGetBindRisk = new GetBindRisk();
			// 获取绑定产品集合
			LCPolSet tLCPolSet = tGetBindRisk.getBindRisk(tLCPolSchema);
			if (tLCPolSet == null) {
				this.mErrors.copyAllErrors(tGetBindRisk.getErrors());
				return false;
			}
			// 对绑定产品每个险种依此进行校验
			for (int setRow = 1; setRow <= tLCPolSet.size(); setRow++) {
				String uwFlag = tLCPolSet.get(setRow).getUWFlag();
				// 校验核保结论
				if (!uwFlag.matches("a|1|8")) {
					addError("主附险绑定产品，若其中一个险种为拒保类结论，则其它险种核保结论也必须为拒保类,险种代码："
							+ tLCPolSet.get(setRow).getRiskCode(),
							"checkUWFlag");
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 添加错误信息
	 * 
	 * @param message
	 *            错误信息
	 * @param function
	 *            错误产生函数名
	 */
	private void addError(String message, String function) {
		// 添加错误信息
		CError tError = new CError();
		tError.moduleName = "CheckBindRisk";
		tError.functionName = function;
		tError.errorMessage = message;
		this.mErrors.addOneError(tError);
		System.out.println(message);
	}

	/**
	 * 返回错误
	 * 
	 * @return 错误信息集合
	 */
	public CErrors getErrors() {
		return this.mErrors;
	}

	/**
	 * 调用示例
	 */
	public static void main(String[] arr) {
		CheckBindRisk tCheckBindRisk = new CheckBindRisk();
		LCContSchema tLCContSchema = new LCContSchema();
		// 绑定
		//tLCContSchema.setContNo("13029410571");
		// 非绑定
		tLCContSchema.setContNo("13041336440");
		if (!tCheckBindRisk.checkUWFlag(tLCContSchema)) {
			System.out
					.println(tCheckBindRisk.getErrors().getError(0).errorMessage);
		}
	}
}
