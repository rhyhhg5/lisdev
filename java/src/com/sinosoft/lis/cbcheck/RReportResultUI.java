package com.sinosoft.lis.cbcheck;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCRReportResultSchema;
import com.sinosoft.lis.vschema.LCRReportResultSet;
import com.sinosoft.lis.schema.LCRReportSchema;
import com.sinosoft.lis.vschema.LCRReportSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: SinoSoft</p>
 * @author HYQ
 * @version 1.0
 */

public class RReportResultUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();


    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    private VData mInputData;

    /** 数据操作字符串 */
    private String mOperate;

    public RReportResultUI()
    {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        this.mOperate = cOperate;

        RReportResultBL tRReportResultBL = new RReportResultBL();

        System.out.println("--------RReportResult Start!---------");
        tRReportResultBL.submitData(cInputData, cOperate);
        System.out.println("--------RReportResult End!---------");

        //如果有需要处理的错误，则返回
        if (tRReportResultBL.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tRReportResultBL.mErrors);
        }
        System.out.println("error num=" + mErrors.getErrorCount());
        mInputData = null;
        mResult = tRReportResultBL.getResult();

        return true;
    }

    /**
     * 返回结果方法
     * @return VData
     */
    public VData getResult()
    {
        return mResult;
    }

    public static void main(String[] args)
    {
        VData tVData = new VData();
        GlobalInput mGlobalInput = new GlobalInput();

        /** 全局变量 */
        mGlobalInput.Operator = "001";
//        mGlobalInput.ComCode = "86";
        mGlobalInput.ManageCom = "86";

         LCRReportSchema tLCRReportSchema = new LCRReportSchema();


         tLCRReportSchema.setProposalContNo("130110000013806");
         tLCRReportSchema.setPrtSeq("810000000000675");
          tLCRReportSchema.setReplyContente("dsfdsf");


        LCRReportResultSet tLCRReportResultSet = new LCRReportResultSet();
        LCRReportResultSchema tLCRReportResultSchema = new
                LCRReportResultSchema();
        LCRReportResultSchema tLCRReportResultSchema1 = new
                LCRReportResultSchema();
        tLCRReportResultSchema.setContNo("130110000013806");
        tLCRReportResultSchema.setGrpContNo("32136");
        tLCRReportResultSchema.setProposalContNo("130110000013806");
        tLCRReportResultSchema.setPrtSeq("810000000000675");
        tLCRReportResultSchema.setCustomerNo("0000493520");
        tLCRReportResultSchema.setRReportResult("sdfsdf");
        tLCRReportResultSchema.setICDCode("asdfsdf");
        tLCRReportResultSet.add(tLCRReportResultSchema);


        tVData.add(mGlobalInput);

        tVData.add(tLCRReportSchema);
          tVData.add(tLCRReportResultSet);
        RReportResultUI tRReportResultUI = new RReportResultUI();
        try
        {
            if (tRReportResultUI.submitData(tVData, ""))
            {

            }
            else
            {
                System.out.println("error:" +
                                   tRReportResultUI.mErrors.getError(0).
                                   errorMessage);
            }
        }
        catch (Exception e)
        {
            System.out.println("error:" + e);
        }

    }

}
