package com.sinosoft.lis.cbcheck;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: 团体单问题修改确认UI</p>
 * <p>Description:
 * 对团体单问题修改后进行确认.置团体整单及团单下未核保通过的个单的状态为新单复核状态
 * </p>
 * <p>Copyright: Copyright (c) 2003-08-20</p>
 * <p>Company: </p>
 * @author sxy
 * @version 1.0
 */

public class GrpQuestModifyMakeSureUI
{
    /** 传入数据的容器 */
	private VData mInputData = new VData();
	/** 数据操作字符串 */
	private String mOperate;
	/** 错误处理类 */
	public CErrors mErrors = new CErrors();

    public GrpQuestModifyMakeSureUI() {
    }


    /**
	* 数据提交方法
	* @param: cInputData 传入的数据
	*		  cOperate 数据操作字符串
	* @return: boolean
	**/
	public boolean submitData( VData cInputData, String cOperate )
	{
		// 数据操作字符串拷贝到本类中
		this.mOperate = cOperate;

		GrpQuestModifyMakeSureBL tGrpQuestModifyMakeSureBL = new GrpQuestModifyMakeSureBL();

		System.out.println("----UI BEGIN---");
		if( tGrpQuestModifyMakeSureBL.submitData( cInputData, mOperate ) == false )
		{
			// @@错误处理
			this.mErrors.copyAllErrors( tGrpQuestModifyMakeSureBL.mErrors );
			return false;
		}
		System.out.println("----UI END---");
		return true;
	}
}