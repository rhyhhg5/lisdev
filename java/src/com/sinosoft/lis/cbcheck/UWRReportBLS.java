package com.sinosoft.lis.cbcheck;

import java.sql.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统个人单人工核保生调录入部分 </p>
 * <p>Description: 数据库功能类</p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft< /p>
 * @author WHN
 * @version 1.0
 */

public class UWRReportBLS
{
        //是否存在需要人工核保保单
	int merrno = 0;
	//传输数据类
	private VData mInputData ;
	//错误处理类，每个需要错误处理的类中都放置该类
	public  CErrors mErrors = new CErrors();

	//
	private LCUWErrorSet mLCUWErrorSet = new LCUWErrorSet();

	public UWRReportBLS() {}

	public static void main(String[] args)
	{
	}

	//传输数据的公共方法
	public boolean submitData(VData cInputData,String cOperate)
	{
	    //首先将数据在本类中做一个备份
	    mInputData=(VData)cInputData.clone() ;

	System.out.println("Start UWRReportBLS Submit...");
		if (!this.saveData())
			return false;
	System.out.println("End UWRReportBLS Submit...");

	    mInputData=null;
	    return true;
	}

	private boolean saveData()
	{

                LCRReportSchema mLCRReportSchema = (LCRReportSchema)mInputData.getObjectByObjectName("LCRReportSchema",0);
                LCUWMasterSchema mLCUWMasterSchema = (LCUWMasterSchema)mInputData.getObjectByObjectName("LCUWMasterSchema",0);
		Connection conn = DBConnPool.getConnection();

	    	if (conn==null)
	    	{
                      // @@错误处理
                      CError tError = new CError();
                      tError.moduleName = "UWRReportBLS";
                      tError.functionName = "saveData";
                      tError.errorMessage = "数据库连接失败!";
                      this.mErrors .addOneError(tError) ;
                      return false;
	    	}

		try
		{
                  conn.setAutoCommit(false);

                  // 保存部分
                  // 保单
                      LCRReportDB tLCRReportDB = new LCRReportDB(conn);
                      tLCRReportDB.setSchema(mLCRReportSchema);
                      if (tLCRReportDB.insert() == false)
                      {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tLCRReportDB.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "UWRReportBLS";
                        tError.functionName = "saveData";
                        tError.errorMessage = "LCRReport表插入失败!";
                        this.mErrors .addOneError(tError) ;
                        conn.rollback() ;
                        conn.close();
                        return false;
                      }

System.out.println("-----------EEE--------------");
                                if(mLCUWMasterSchema != null)
                                {
                                  LCUWMasterDB tLCUWMasterDB = new LCUWMasterDB();
                                  tLCUWMasterDB.setSchema(mLCUWMasterSchema);
                                  if(tLCUWMasterDB.update() == false)
                                  {
                                    // @@错误处理
                                    this.mErrors.copyAllErrors(tLCUWMasterDB.mErrors);
                                    CError tError = new CError();
                                    tError.moduleName = "UWRReportBLS";
                                    tError.functionName = "saveData";
                                    tError.errorMessage = "LCUWMaster表更新失败!";
                                    this.mErrors .addOneError(tError) ;
                                    conn.rollback() ;
                                    conn.close();
                                    return false;
                                  }
                                }

                   conn.commit() ;
                   conn.close();
		} // end of try
		catch (Exception ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "UWRReportBLS";
			tError.functionName = "submitData";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);
			try{conn.rollback() ;} catch(Exception e){}
			return false;
		}

	    return true;
	}

}