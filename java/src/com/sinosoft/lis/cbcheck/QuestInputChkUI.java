package com.sinosoft.lis.cbcheck;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;


/**
 * <p>Title: Web业务系统问题件录入部分</p>
 * <p>Description:接口功能类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author WHN
 * @modified by ZhangRong  2004.11
 * @version 1.0
 */
public class QuestInputChkUI
{
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 往界面传输数据的容器 */
	private VData mResult = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	public QuestInputChkUI()
	{
	}

	/**
	传输数据的公共方法
	*/
	public boolean submitData(VData cInputData, String cOperate)
	{
		//将操作数据拷贝到本类中
		this.mOperate = cOperate;

		QuestInputChkBL tQuestInputChkBL = new QuestInputChkBL();

		System.out.println("---QuestInputChkBL UI BEGIN---");

		if (tQuestInputChkBL.submitData(cInputData, mOperate) == false)
		{
			// @@错误处理
			this.mErrors.copyAllErrors(tQuestInputChkBL.mErrors);

			CError tError = new CError();
			tError.moduleName = "QuestInputChkUI";
			tError.functionName = "submitData";
			tError.errorMessage = "数据查询失败!";
			this.mErrors.addOneError(tError);
			mResult.clear();

			return false;
		}

		return true;
	}

	// @Main
	public static void main(String[] args)
	{
		GlobalInput tG = new GlobalInput();
		tG.Operator = "001";
		tG.ManageCom = "86";

		LCContSchema p = new LCContSchema();
		p.setContNo("13000001222");
		LCContSet tLCContSet = new LCContSet();
		tLCContSet.add(p);

		LCPENoticeSchema tLCPENoticeSchema = new LCPENoticeSchema();

		LCIssuePolSchema tLCIssuePolSchema = new LCIssuePolSchema();

		tLCIssuePolSchema.setBackObjType("3");
		tLCIssuePolSchema.setIssueCont("规则错误");
		tLCIssuePolSchema.setOperatePos("1");
		tLCIssuePolSchema.setContNo("13000001222");
		tLCIssuePolSchema.setIssueType("C");
                tLCIssuePolSchema.setQuestionObj("234");
		LCIssuePolSet tLCIssuePolSet = new LCIssuePolSet();
		tLCIssuePolSet.add(tLCIssuePolSchema);

		VData tVData = new VData();
		tVData.add(tLCContSet);

		//tVData.add( tLCPENoticeSet);
		//tVData.add( tLCPENoticeItemSet);
		tVData.add(tLCIssuePolSet);
		tVData.add(tG);

		QuestInputChkUI ui = new QuestInputChkUI();

		if (ui.submitData(tVData, "") == true)
		{
			System.out.println("---ok---");
		}
		else
		{
			System.out.println("---NO---");
		}
	}
}
