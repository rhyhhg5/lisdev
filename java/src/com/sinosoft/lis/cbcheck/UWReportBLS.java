package com.sinosoft.lis.cbcheck;

import oracle.jdbc.OracleResultSet;

import oracle.jdbc.driver.*;

import oracle.sql.*;

import java.io.*;
import java.sql.*;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;


/**
 * <p>Title: Web业务系统核保报告书查询录入部分</p>
 * <p>Description: 数据库功能类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author WHN
 * @version 1.0
 */
public class UWReportBLS
{
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	//传输数据类
	private VData mInputData;

	/** 数据操作字符串 */
	private String mOperate;
	private String mContent;

	public UWReportBLS()
	{
	}

	public static void main(String[] args)
	{
	}

	/**
	 *传输数据的公共方法
	 **/
	public boolean submitData(VData cInputData, String cOperate)
	{
		//将操作数据拷贝到本类中
		this.mOperate = cOperate;
		mInputData = (VData)cInputData.clone();
		System.out.println("Start UWReportBLS Submit...");

		if (this.mOperate.equals("INSERT"))
		{
			if (!saveReport())
			{
				System.out.println("Insert failed");
				System.out.println("End UWReportBLS Submit...");

				return false;
			}
		}

		System.out.println("Insert succ");

		return true;
	}

	/**
	 * 保存函数
	 */
	private boolean saveReport()
	{
		LCUWReportSchema tLCUWReportSchema = new LCUWReportSchema();    //定义局部变量，之后赋值
		tLCUWReportSchema = (LCUWReportSchema)mInputData.getObjectByObjectName("LCUWReportSchema", 0);
		mContent = (String)mInputData.getObjectByObjectName("String", 0);

		//System.out.println("Start Save...");
		Connection conn;    //建立数据库的连接
		conn = null;
		conn = DBConnPool.getConnection();

		if (conn == null)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "UWReportBLS";
			tError.functionName = "saveData";
			tError.errorMessage = "数据库连接失败!";
			this.mErrors.addOneError(tError);

			return false;
		}

		//conn值有效，则先删除纪录，再插入新纪录
		try
		{
			conn.setAutoCommit(false);

			if ((tLCUWReportSchema.getProposalContNo() != null) && !tLCUWReportSchema.getProposalContNo().equals(""))
			{
				System.out.println("delete...");

				LCUWReportDB tLCUWReportDB = new LCUWReportDB(conn);
				tLCUWReportDB.setSchema(tLCUWReportSchema);

				if (!tLCUWReportDB.delete())
				{
					// @@错误处理
					this.mErrors.copyAllErrors(tLCUWReportDB.mErrors);

					CError tError = new CError();
					tError.moduleName = "LCUWReportBLS";
					tError.functionName = "saveData";
					tError.errorMessage = "数据删除失败!";
					this.mErrors.addOneError(tError);
					conn.rollback();
					conn.close();

					return false;
				}

				conn.commit();
			}

			conn.close();

			System.out.println("Start 保存...");

			String a = tLCUWReportSchema.getGrpContNo();
			String b = tLCUWReportSchema.getContNo();
			String c = tLCUWReportSchema.getProposalContNo();
			String d = tLCUWReportSchema.getUWOperator();
			String e = tLCUWReportSchema.getPolType();
			String f = tLCUWReportSchema.getManageCom();
			String g = tLCUWReportSchema.getMakeDate();
			String h = tLCUWReportSchema.getMakeTime();
			String i = tLCUWReportSchema.getModifyDate();
			String j = tLCUWReportSchema.getModifyTime();

			//处理二进制
			byte[] tbytes;
			OutputStream tOutputStream;
			mContent = StrTool.GBKToUnicode(mContent);
			tbytes = mContent.getBytes();

			Statement stmt = conn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);
			String tSql = "insert into LCUWReport values ('" + a + "','" + b + "','" + c + "','" + d + "','" + e + "','" + f + "',empty_blob(),'" + g + "','" + h + "','" + i + "','" + j + "')";
			System.out.println(tSql);
			stmt.execute(tSql);

			//      conn.commit();;
			tSql = "select contente from lcuwreport where ContNo = '" + e + "' and uwoperator = '" + h + "' FOR UPDATE";
			System.out.println(tSql);

			ResultSet rs = null;
			rs = stmt.executeQuery(tSql);
			rs.next();

			BLOB blob = ((OracleResultSet)rs).getBLOB("Contente");

			OutputStream os = null;
			os = blob.getBinaryOutputStream();

			int inData = 0;

			while (inData <= (tbytes.length - 1))
			{
				os.write(tbytes[inData]);
				inData = inData + 1;
			}

			os.flush();
			os.close();

			conn.commit();    //数据库将数值全部传入
			conn.close();
		}
		catch (Exception ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "UWReportBLS";
			tError.functionName = "submitData";
			tError.errorMessage = ex.toString();
			this.mErrors.addOneError(tError);

			try
			{
				conn.rollback();    //数据库不传入任何数值
				conn.close();
			}
			catch (Exception e)
			{
			}

			return false;
		}

		return true;
	}
}
