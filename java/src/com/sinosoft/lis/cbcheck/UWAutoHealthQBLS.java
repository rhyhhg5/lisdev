package com.sinosoft.lis.cbcheck;

import java.sql.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统体检资料录入结论部分</p>
 * <p>Description: 数据库功能类</p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft< /p>
 * @author WHN
 * @version 1.0
 */

public class UWAutoHealthQBLS
{
        //是否存在需要人工核保保单
	int merrno = 0;
	//传输数据类
	private VData mInputData ;
	//错误处理类，每个需要错误处理的类中都放置该类
	public  CErrors mErrors = new CErrors();

	//
	//private LCUWErrorDB mLCUWErrorDB = new LCUWErrorDB();
	private LCUWErrorSet mLCUWErrorSet = new LCUWErrorSet();

	public UWAutoHealthQBLS() {}

	public static void main(String[] args)
	{
	}

	//传输数据的公共方法
	public boolean submitData(VData cInputData,String cOperate)
	{
	    //首先将数据在本类中做一个备份
	    mInputData=(VData)cInputData.clone() ;

	System.out.println("Start UWAutoHealthQBLS Submit...");
		if (!this.saveData())
			return false;
	System.out.println("End UWAutoHealthQBLS Submit...");

	    mInputData=null;
	    return true;
	}

	private boolean saveData()
	{

	  	LCPENoticeSet mLCPENoticeSet = (LCPENoticeSet)mInputData.getObjectByObjectName("LCPENoticeSet",0);

		Connection conn = DBConnPool.getConnection();

	    	if (conn==null)
	    	{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "UWAutoHealthQBLS";
			tError.functionName = "saveData";
			tError.errorMessage = "数据库连接失败!";
			this.mErrors .addOneError(tError) ;
			return false;
	    	}

		try
		{
			conn.setAutoCommit(false);

			// 删除部分
			int polCount = mLCPENoticeSet.size();
			if (polCount > 0)
			{
				LCPENoticeSchema tLCPENoticeSchema = new LCPENoticeSchema();
				tLCPENoticeSchema = ( LCPENoticeSchema )mLCPENoticeSet.get( 1 );
				String tProposalNo = tLCPENoticeSchema.getContNo();
                                String tInsuredNo = tLCPENoticeSchema.getCustomerNo();

				// 核保主表
				LCPENoticeDB tLCPENoticeDB = new LCPENoticeDB( conn );
				tLCPENoticeDB.setContNo( tProposalNo );
                                tLCPENoticeDB.setCustomerNo(tInsuredNo);
				if (tLCPENoticeDB.deleteSQL() == false)
				{
					// @@错误处理
				    	this.mErrors.copyAllErrors(tLCPENoticeDB.mErrors);
					CError tError = new CError();
					tError.moduleName = "UWAtuoHealthQBLS";
					tError.functionName = "saveData";
					tError.errorMessage = "LCPENotice表删除失败!";
					this.mErrors .addOneError(tError) ;
		        		conn.rollback() ;
                                        conn.close();
					return false;
				}

				// 保存部分
				// 体检资料主表

				tLCPENoticeDB.setSchema(tLCPENoticeSchema);

				if(tLCPENoticeDB.insert() == false)
				{
					// @@错误处理
				    	this.mErrors.copyAllErrors(tLCPENoticeDB.mErrors);
					CError tError = new CError();
					tError.moduleName = "UWAutoHealthQBLS";
					tError.functionName = "saveData";
					tError.errorMessage = "LCPENotice表保存失败!";
					this.mErrors .addOneError(tError) ;
		        		conn.rollback() ;
                                        conn.close();
					return false;
				}

System.out.println("-----------DDD--------------");
System.out.println("-----------EEE--------------");

				conn.commit() ;

			} // end of for
                        conn.close();
		} // end of try
		catch (Exception ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "UWAutoHealthQBLS";
			tError.functionName = "submitData";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);
			try{conn.rollback() ;} catch(Exception e){}
			return false;
		}

	    return true;
	}

}