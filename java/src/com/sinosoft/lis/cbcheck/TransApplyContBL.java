package com.sinosoft.lis.cbcheck;

import com.cbsws.obj.RspSaleInfo;
import com.sinosoft.httpclient.inf.GetPlatformCustomerNo;
import com.sinosoft.httpclient.inf.LCContTransApply;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCContSubDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class TransApplyContBL {

	/** 传入数据的容器 */
	private VData mInputData = new VData();

	private TransferData mTransferData = new TransferData();

	private GlobalInput mGlobalInput = new GlobalInput();

	private MMap map = new MMap();
	
	/** 往前面传输数据的容器 */
    private String mResult = "";

	private String mPrtNo = "";

	private ExeSQL mExeSQL = new ExeSQL();

	private LCContSubDB mLCContSubDB = null;

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	public boolean submitData(VData cInputData, String cOperate) {

		// 数据过滤器
		if (!getInputData(cInputData, cOperate)) {
			return false;
		}

		// 进行业务处理
		if (!dealData()) {
			return false;
		}
		
//		this.prepareOutputData();
//
//		PubSubmit tPubSubmit = new PubSubmit();
//
//		if (!tPubSubmit.submitData(mInputData, cOperate)) {
//			// @@错误处理
//			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
//
//			CError tError = new CError();
//			tError.moduleName = "ContBL";
//			tError.functionName = "submitData";
//			tError.errorMessage = "数据提交失败!";
//
//			this.mErrors.addOneError(tError);
//			return false;
//		}

		return true;
	}

	private boolean dealData() {

		LCContDB tLCContDB = new LCContDB();
		LCContSet tLCContSet=tLCContDB.executeQuery("select * from lccont where prtno='"+ mPrtNo + "' ");
		if (tLCContSet.size()==0){
			CError tError = new CError();
			tError.moduleName = "TaxCheckContBL";
			tError.functionName = "dealData";
			tError.errorMessage = "保单信息查询失败！";
			this.mErrors.addOneError(tError);
			return false;
		}
		
		LCContSchema tLCContSchema = tLCContSet.get(1);

		SSRS tSSRS = new SSRS();
		String tsql = "select 1 from lcpol where prtno='"+ mPrtNo + "' "
		            + " and exists (select 1 from lmriskapp where riskcode=lcpol.riskcode and TaxOptimal='Y') ";
		tSSRS = mExeSQL.execSQL(tsql);
		if (tSSRS.getMaxRow() == 0) {
			CError tError = new CError();
			tError.moduleName = "TaxCheckContBL";
			tError.functionName = "dealData";
			tError.errorMessage = "该单不为税优保单！";
			this.mErrors.addOneError(tError);
			return false;
		}

		mLCContSubDB = new LCContSubDB();
		mLCContSubDB.setPrtNo(mPrtNo);
		if (!mLCContSubDB.getInfo()) {
			CError tError = new CError();
			tError.moduleName = "TaxCheckContBL";
			tError.functionName = "dealData";
			tError.errorMessage = "保单税优信息查询失败！";
			this.mErrors.addOneError(tError);
			return false;

		}

		String tCustomerNo= tLCContSchema.getAppntNo();
		tsql = " select ld.customerno,ld.PlatformCustomerNo from ldperson ld where ld.customerno='"+ tCustomerNo + "' ";
		tSSRS = mExeSQL.execSQL(tsql);
		if (tSSRS.getMaxRow() > 0){
			String tplatno = tSSRS.GetText(1, 2);
			if("".equals(tplatno)||null==tplatno){
				if(!getSYCustomerNo(tCustomerNo)){
					return false;
				}
			}
		}else{
			CError tError = new CError();
			tError.moduleName = "TaxCheckContBL";
			tError.functionName = "dealData";
			tError.errorMessage = "投保人信息查询失败！";
			this.mErrors.addOneError(tError);
			return false;
		}
		
		if (!getTransApply()) {
			CError tError = new CError();
			tError.moduleName = "TaxCheckContBL";
			tError.functionName = "dealData";
			tError.errorMessage = "税优保单转移申请失败！";
			this.mErrors.addOneError(tError);
			return false;
		}
		
		return true;
	}

	// 获取客户平台号,并存储
	private boolean getSYCustomerNo(String cCustomerNo) {
		TransferData mTransferData = new TransferData();
		mTransferData.setNameAndValue("CustomerNo", cCustomerNo);
		VData tVData = new VData();
		tVData.add(mTransferData);
		GetPlatformCustomerNo tGetPlatformCustomerNo = new GetPlatformCustomerNo();
		if(!tGetPlatformCustomerNo.submitData(tVData,"HQSYSBM")){
			CError tError = new CError();
			tError.moduleName = "TaxCheckContBL";
			tError.functionName = "dealData";
			tError.errorMessage = "获取平台客户号失败！";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	// 转移申请
	private boolean getTransApply() {
		VData tVData = new VData();
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("PrtNo", mPrtNo);
		tVData.add(tTransferData);
		LCContTransApply tLCContTransApply = new LCContTransApply();
		if(!tLCContTransApply.submitData(tVData, "ZBXPT")){
			CError tError = new CError();
			tError.moduleName = "TaxCheckContBL";
			tError.functionName = "dealData";
			tError.errorMessage = tLCContTransApply.mErrors.getError(0).errorMessage;
			this.mErrors.addOneError(tError);
			return false;		
		}
		return true;
	}

	private void prepareOutputData() {
		mInputData.clear();
		mInputData.add(map);

	}

	private boolean getInputData(VData cInputData, String cOperate) {
		// 从VData容器中取出对象TransferData
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0));
		mTransferData = (TransferData) cInputData.getObjectByObjectName(
				"TransferData", 0);

		// 分别对TransferData中数据进行过滤
		mPrtNo = (String) mTransferData.getValueByName("PrtNo");
		if (mPrtNo == null || "".equals(mPrtNo)) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "TaxCheckContBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "印刷号获取失败！";
			this.mErrors.addOneError(tError);
			return false;
		}

		return true;
	}
	
	/**
     * 得到处理后的结果集
     * @return 结果集
     */

    public String getResult()
    {
        return mResult;
    }
}


