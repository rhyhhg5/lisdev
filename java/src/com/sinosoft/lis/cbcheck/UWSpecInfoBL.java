package com.sinosoft.lis.cbcheck;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.schema.LDCodeSchema;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.schema.LAAccountsSchema;
import com.sinosoft.lis.vschema.LAAccountsSet;
import com.sinosoft.lis.db.LAAccountsDB;
/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 销售管理系统</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author
 * @version 1.0
 */
public class UWSpecInfoBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LDCodeSchema mLDCodeSchema = new LDCodeSchema();
    private TransferData mTransferData = new TransferData();
    /** 最后递交Map */
    private MMap map = new MMap();

    private String mSpecNoOld = "";

    private String mDiseaseNameOld = "";


    public UWSpecInfoBL() {
    }
    /**
     * submitData
     *
     * @param nInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData nInputData, String cOperate) {
        System.out.println("into UWSpecInfoBL...");
        this.mInputData = nInputData;
        this.mOperate = cOperate;

        if (!getInputData()) {
            return false;
        }
        if (!checkData()) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
        if (!prepareOutputData()) {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
         if (!tPubSubmit.submitData(this.mResult, "UPDATE")) {
         this.mErrors.copyAllErrors(tPubSubmit.mErrors);
         return false;
     }
//        System.out.println("UWSpecInfoBL finished...");
        return true;
    }

    /**
     * getInputData
     *
     * @return boolean
     */
    private boolean getInputData() {
        System.out.println("into UWSpecInfoBL.getInputData()...");
        this.mGlobalInput.setSchema((GlobalInput) mInputData.
                             getObjectByObjectName("GlobalInput", 0));

        this.mTransferData = (TransferData) mInputData.getObjectByObjectName(
                             "TransferData", 0);

        mSpecNoOld = (String) mTransferData.getValueByName("SpecNoOld");

        mDiseaseNameOld = (String) mTransferData.getValueByName("DiseaseNameOld");

         this.mLDCodeSchema.setSchema((LDCodeSchema) mInputData.
                                 getObjectByObjectName(
                               "LDCodeSchema", 0));


        return true;
    }

    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData() {
        System.out.println("into UWSpecInfoBL.checkData()...");
        if (this.mGlobalInput == null) {
            String str = "登陆信息为null，可能是页面超时，请重新登陆!";
            buildError("checkData", str);
            return false;
        }
        return true;
    }

    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData() {
        System.out.println("into UWSpecInfoBL.dealData()...");
        System.out.println("dealData:" + mOperate);
     if (this.mOperate.equals("UPDATE||MAIN")) {
     if (!updateData()) {
         return false;
       }
     }
     if (this.mOperate.equals("DELETE||MAIN")) {
         if (!deleteData()) {
             return false;
       }
   }

//    if (!tPubSubmit.submitData(this.mResult, "DELETE")) {
//    this.mErrors.copyAllErrors(tPubSubmit.mErrors);
//    return false;
//    }

    System.out.println("BriefCardSignBL finished...");

        return true;
    }
    private boolean updateData() {
//        System.out.println("mLAAccountsSchema.getAgentCode():" +
//                           mLAAccountsSchema.getAgentCode());

        if (mLDCodeSchema.getCode() == null) {
            CError tError = new CError();
            tError.moduleName = "UWSpecInfoBL";
            tError.functionName = "insertData";
            tError.errorMessage = "没有录入有效的免责编码!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("mLDCodeSchema.getCode():" +
                           mLDCodeSchema.getCode());
        String sql = "select * from ldcode where code ='" +
                     mLDCodeSchema.getCode() + "'";
        System.out.println(sql);
        LDCodeDB tLDCodeDB = new LDCodeDB();
        LDCodeSet tLDCodeSet = new LDCodeSet();
        tLDCodeSet = tLDCodeDB.executeQuery(sql);
        System.out.println(tLDCodeDB.executeQuery(sql));
        if ( tLDCodeSet != null &&tLDCodeSet.size() > 0) {
//          System.out.println(tLDCodeSet.size());
//            CError tError = new CError();
//            tError.moduleName = "UWSpecInfoBL";
//            tError.functionName = "insertData";
//            tError.errorMessage = "已经存在免责信息" + mLDCodeSchema.getCodeName() +
//                                  "!";
//            this.mErrors.addOneError(tError);
//            return false;
            mLDCodeSchema.setCodeType("speccode");
            map.put("delete from LDCode where code = '" +
                 mLDCodeSchema.getCode() + "' and CodeType = 'speccode'", "DELETE");
            map.put(mLDCodeSchema, "INSERT");
            return true;
        }
             mLDCodeSchema.setCodeType("speccode");
             map.put("delete from LDCode where code = '" +
                 mLDCodeSchema.getCode() + "' and CodeType = 'speccode' ", "DELETE");
             map.put(mLDCodeSchema, "INSERT");
        return true;
    }
//删除免责信息
    private boolean deleteData() {

    String sql = "select * from ldcode where code ='" +
                     mLDCodeSchema.getCode() + "' and codetype ='speccode'";

        System.out.println(sql);

        LDCodeDB tLDCodeDB = new LDCodeDB();
        LDCodeSet tLDCodeSet = new LDCodeSet();
        tLDCodeSet = tLDCodeDB.executeQuery(sql);
        if (tLDCodeSet == null || tLDCodeSet.size() == 0) {
            CError tError = new CError();
            tError.moduleName = "UWSpecInfoBL";
            tError.functionName = "updataData";
            tError.errorMessage = "不存在" + mDiseaseNameOld +
                                  "免责信息，不能进行修改!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLDCodeSchema.setCodeType("speccode");
        map.put("delete from LDCode where code = '" +
                 mLDCodeSchema.getCode() + "' and codetype ='speccode'", "DELETE");
        return true;
    }

    /**
     * prepareOutputData
     *
     * @return boolean
     */
    private boolean prepareOutputData() {
        System.out.println("into UWSpecInfoBL.prepareOutputData()...");
        this.mResult.add(map);
        return true;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "UWSpecInfoBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

}
