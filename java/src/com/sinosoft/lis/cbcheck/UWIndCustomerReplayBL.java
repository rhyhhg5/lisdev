package com.sinosoft.lis.cbcheck;

import com.sinosoft.lis.db.LCUWMasterDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LCUWMasterSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.db.LCPolDB;

/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class UWIndCustomerReplayBL
{

    /** 报错对象 */
    public CErrors mErrors = new CErrors();

    /** 全局数据对象 */
    private VData mInputData;

    /** 操作符 */
    private String mOperate;

    /** LCPol */
    private LCPolSchema mLCPolSchema;

    /** LCUWMaster */
    private LCUWMasterSchema mLCUWMasterSchema;

    /** 参数 */
    private TransferData mTransferData;

    /** 客户回复 */
    private String mCustomerReply;

    /** 登陆信息 */
    private GlobalInput mGlobalInput;

    /** 递交信息 */
    private MMap map = new MMap();

    /** UWIdea */
    private String mUWIdea;

    private VData mResult = new VData();

    public UWIndCustomerReplayBL()
    {
    }

    /**
     * 传入数据接口
     * @param tInputData VData
     * @param tOperate String
     * @return boolean
     */
    public boolean submitData(VData tInputData, String tOperate)
    {

        this.mInputData = tInputData;
        this.mOperate = tOperate;

        System.out.println("进入BL");

        if (!getInputData())
        {
            return false;
        }

        if (!checkData())
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        if (!perpareOutPutData())
        {
            return false;
        }
        System.out.println("准备递交~~~");
        PubSubmit ps = new PubSubmit();
        if (!ps.submitData(this.mResult, null))
        {
            this.mErrors.copyAllErrors(ps.mErrors);
            return false;
        }
        return true;
    }

    /**
     * perpareOutPutData
     *
     * @return boolean
     */
    private boolean perpareOutPutData()
    {
        map.put(this.mLCPolSchema, "UPDATE");
        map.put(this.mLCUWMasterSchema, "UPDATE");
        this.mResult.add(map);
        return true;
    }

    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData()
    {
        /** 首先判断客户意见 */
        System.out.println("进入dealData");
        if (this.mCustomerReply.equals("01"))
        {
            /** 客户不同意 */
            System.out.println("客户不同意~");
            /** 如果客户不同意,不更改核保轨迹表,只更改险种和保单核保标记 */
            this.mLCPolSchema.setUWFlag("a");
        }
        else if (this.mCustomerReply.equals("00"))
        {
            /** 客户同意 */
            System.out.println("客户同意");
            this.mLCPolSchema.setUWFlag("4");
        }
        this.mLCPolSchema.setModifyDate(PubFun.getCurrentDate());
        this.mLCPolSchema.setModifyTime(PubFun.getCurrentTime());
        this.mLCPolSchema.setOperator(this.mGlobalInput.Operator);
        this.mLCPolSchema.setRemark(this.mUWIdea);
        /** 核保轨迹表不更改,只将客户意见填入 */
        LCUWMasterDB tLCUWMasterDB = new LCUWMasterDB();
        tLCUWMasterDB.setProposalNo(mLCPolSchema.getProposalNo());
        if (!tLCUWMasterDB.getInfo())
        {
            buildError("dealData", "没有查询到险种核保信息！");
            return false;
        }

        mLCUWMasterSchema = tLCUWMasterDB.getSchema();

        /** 核保结论是否是变更承保 */
        if (!mLCUWMasterSchema.getPassFlag().equals("4"))
        {
            buildError("dealData", "核保师下发的核保结论不是变更承保,因此客户不能做出意见回复！");
            return false;
        }

        mLCUWMasterSchema.setCustomerReply(mCustomerReply);
        mLCUWMasterSchema.setModifyDate(PubFun.getCurrentDate());
        mLCUWMasterSchema.setModifyTime(PubFun.getCurrentTime());
        mLCUWMasterSchema.setOperator(this.mGlobalInput.Operator);
        return true;
    }

    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData()
    {
        System.out.println("进入checkData");

        if (this.mGlobalInput == null)
        {
            buildError("checkData", "登陆信息为null,请重新登陆！");
            return false;
        }

        if (this.mLCPolSchema == null)
        {
            buildError("checkData", "没有传入险种信息！");
            return false;
        }

        if (this.mLCPolSchema.getPolNo() == null
                || this.mLCPolSchema.getPolNo().equals(""))
        {
            buildError("checkData", "没有传入险种号码！");
            return false;
        }

        LCPolDB tLCPolDB = mLCPolSchema.getDB();
        if (!tLCPolDB.getInfo())
        {
            buildError("checkData", "查询险种信息失败！");
            return false;
        }
        mLCPolSchema.setSchema(tLCPolDB);

        if (this.mTransferData == null)
        {
            buildError("checkData", "险种核保结论为null！");
            return false;
        }

        mCustomerReply = (String) mTransferData.getValueByName("CustomerReply");

        if (mCustomerReply == null || mCustomerReply.equals(""))
        {
            buildError("checkData", "没有传入客户意见！");
            return false;
        }

        mUWIdea = (String) mTransferData.getValueByName("UWIdea");

        //        if (mUWIdea == null || mUWIdea.equals("")) {
        //            buildError("checkData", "没有传入客户意见！");
        //            return false;
        //        }

        return true;
    }

    /**
     * getInputData
     *
     * @return boolean
     */
    private boolean getInputData()
    {
        System.out.println("进入getInputData");
        if (this.mInputData == null)
        {
            buildError("getInputData", "传入数据对象为null！");
            return false;
        }

        if (this.mOperate == null)
        {
            buildError("getInputData", "操作符为null！");
            return false;
        }

        mLCPolSchema = (LCPolSchema) mInputData.getObjectByObjectName(
                "LCPolSchema", 0);

        mTransferData = (TransferData) mInputData.getObjectByObjectName(
                "TransferData", 0);

        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);

        return true;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "UWIndCustomerReplayBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }

    public static void main(String[] args)
    {

        VData tVData = new VData();

        GlobalInput tG = new GlobalInput();
        tG.Operator = "001";
        tG.ManageCom = "86";

        LCPolSchema tLCPolSchema = new LCPolSchema();
        tLCPolSchema.setPolNo("11000038564");
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("CustomerReply", "01");
        tVData.add(tLCPolSchema);
        tVData.add(tTransferData);
        tVData.add(tG);
        UWIndCustomerReplayBL tUWIndCustomerReplayBL = new UWIndCustomerReplayBL();
        if (!tUWIndCustomerReplayBL.submitData(tVData, ""))
        {
            System.out.println(tUWIndCustomerReplayBL.mErrors.getContent());
        }
    }
}
