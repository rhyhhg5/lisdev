package com.sinosoft.lis.cbcheck;

import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;

/**
 * 绑定产品险种信息获取类
 * 
 * @author 张成轩
 */
public class GetBindRisk {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	private CErrors mErrors = new CErrors();

	/**
	 * 获取绑定产品险种信息(个险)
	 * 
	 * @param tLCPolSchema 险种信息Schema集合
	 * @return 保单下绑定险种LCPolSet集合
	 */
	public LCPolSet getBindRisk(LCPolSchema tLCPolSchema) {

		LCPolDB tLCPolDB = tLCPolSchema.getDB();
		if (!tLCPolDB.getInfo()) {
			addError("获取险种信息失败!", "getBindRisk");
			return null;
		}

		if (tLCPolDB.getPolNo().equals(tLCPolDB.getMainPolNo())) {
			// 传入的lcpol是主险的情况
			String polNo = tLCPolDB.getPolNo();
			String riskcode = tLCPolDB.getRiskCode();
			LCPolDB appLCPolDB = new LCPolDB();
			String querySql = "select * from LCPol where MainPolNo='" + polNo
					+ "' and exists (select 1 from ldcode1 where codetype='checkappendrisk' and code1='" + riskcode + "' and code=lcpol.riskcode) ";
			System.out.println(querySql);

			// 通过主险polno查询全部附加险信息
			LCPolSet tLCPolSet = appLCPolDB.executeQuery(querySql);
			tLCPolSet.add(tLCPolDB.getSchema());

			if (tLCPolSet == null || tLCPolSet.size() == 0) {
				addError("获取险种信息失败!", "getBindRisk");
				return null;
			}
			System.out.println("----LCPolSet----" + tLCPolSet.size());

			return tLCPolSet;

		} else {
			// 传入的lcpol是附加险的情况
			String mainPolNo = tLCPolDB.getMainPolNo();
			String riskcode = tLCPolDB.getRiskCode();

			LCPolDB appLCPolDB = new LCPolDB();
			// 查找该附加险绑定的主险编码
			String querySql = "select * from LCPol where PolNo='" + mainPolNo
					+ "' and exists (select 1 from ldcode1 where codetype='checkappendrisk' and code='" + riskcode + "' and code1=lcpol.riskcode) ";
			System.out.println(querySql);
			LCPolSet tLCPolSet = appLCPolDB.executeQuery(querySql);
			if (tLCPolSet.size() > 1) {
				if (tLCPolSet == null || tLCPolSet.size() == 0) {
					addError("获取主险信息失败!", "getBindRisk");
					return null;
				}
			} else if (tLCPolSet.size() == 0) {
				// 没有查到绑定的主险，直接返回附加险
				tLCPolSet.add(tLCPolDB.getSchema());
				System.out.println("----LCPolSet----" + tLCPolSet.size());
				
				return tLCPolSet;
			} else {
				// 传入的lcpol是主险的情况
				String bindPolNo = tLCPolSet.get(1).getPolNo();
				String bingRiskcode = tLCPolSet.get(1).getRiskCode();
				LCPolDB bingLCPolDB = new LCPolDB();
				String bingQuerySql = "select * from LCPol where MainPolNo='" + bindPolNo
						+ "' and exists (select 1 from ldcode1 where codetype='checkappendrisk' and code1='" + bingRiskcode
						+ "' and code=lcpol.riskcode) ";
				System.out.println(bingQuerySql);

				// 通过主险polno查询全部附加险信息
				LCPolSet bingLCPolSet = bingLCPolDB.executeQuery(bingQuerySql);
				bingLCPolSet.add(tLCPolSet.get(1));

				if (tLCPolSet == null || tLCPolSet.size() == 0) {
					addError("获取险种信息失败!", "getBindRisk");
					return null;
				}
				System.out.println("----LCPolSet----" + bingLCPolSet.size());

				return bingLCPolSet;
			}
		}
		
		addError("获取绑定险种异常!", "getBindRisk");
		return null;
	}

	/**
	 * 添加错误信息
	 * 
	 * @param message 错误信息
	 * @param function 错误产生函数名
	 */
	private void addError(String message, String function) {
		// 添加错误信息
		CError tError = new CError();
		tError.moduleName = "CheckBindRisk";
		tError.functionName = function;
		tError.errorMessage = message;
		this.mErrors.addOneError(tError);
		System.out.println(message);
	}

	/**
	 * 返回错误
	 * 
	 * @return 错误信息集合
	 */
	public CErrors getErrors() {
		return this.mErrors;
	}

	/**
	 * 调用示例
	 */
	public static void main(String[] arr) {
		GetBindRisk tGetBindRisk = new GetBindRisk();
		LCPolSchema tLCPolSchema = new LCPolSchema();
		tLCPolSchema.setMainPolNo("167201206151");
		LCPolSet tLCPolSet = tGetBindRisk.getBindRisk(tLCPolSchema);
		if (tLCPolSet == null) {
			System.out.println(tGetBindRisk.getErrors().getError(0).errorMessage);
		}
	}
}
