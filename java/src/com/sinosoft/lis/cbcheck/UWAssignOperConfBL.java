package com.sinosoft.lis.cbcheck;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.LDCode1Schema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class UWAssignOperConfBL {
	public CErrors mErrors = new CErrors();

	private String mOperate = "";
	
	private TransferData mTransferData = new TransferData();

	MMap map = new MMap();

	private GlobalInput mGlobalInput = new GlobalInput();

	public UWAssignOperConfBL() {
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "UWAssignOperConfBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	public boolean submitData(VData cInputData, String cOperate) {
		
		System.out.println("Begin getInputData");
		if (!getInputData(cInputData,cOperate)) {
			return false;
		}
		System.out.println("End getInputData");

        if(!dealData()){
        	return false;
        }
		
        if(map.size()>0){
        	VData vData = new VData();
    		vData.add(map);

    		PubSubmit pubsubmit = new PubSubmit();
    		if (!pubsubmit.submitData(vData, "")) {
    			mErrors.copyAllErrors(pubsubmit.mErrors);
    			return false;
    		}
        }		
		System.out.println("---End pubsubmit---");
		System.out.println("操作成功！");
		return true;
	}

	private boolean dealData() {
		String mComCode = (String)mTransferData.getValueByName("comCode");
    	String mUwUser = (String)mTransferData.getValueByName("uwOper");
    	if(mComCode==null||"".equals(mComCode)){
    		buildError("dealData", "管理机构不能为空。");
    		return false;
    	}else{
    		if(mComCode.length()!=4){
    			buildError("dealData", "管理机构只能为4位机构。");
    			return false;
    		}    		
    	}
    	if(mUwUser==null||"".equals(mUwUser)){
    		buildError("dealData", "核保师不能为空。");
    		return false;
    	}
    	
    	if(mOperate.equals("save")){//添加
    		LDCode1Schema tLDCode1Schema = new LDCode1Schema();
    		tLDCode1Schema.setCodeType("uwuserconf");
    		tLDCode1Schema.setCode(mUwUser);
    		tLDCode1Schema.setCode1(mComCode);
    		tLDCode1Schema.setCodeName(new ExeSQL().getOneValue("select name from ldcom where comcode='"+mComCode+"'"));
    		String mStrSQL = "select 1 from ldcode1 where codetype='uwuserconf' and code ='"+mUwUser+"' and code1 = '"+mComCode+"'";
        	ExeSQL mExeSQL = new ExeSQL();
        	String result = mExeSQL.getOneValue(mStrSQL);
        	if("1".equals(result)){
        		buildError("save", "该核保师:"+mUwUser+"已配置过"+mComCode+"机构！");
        		return false;
        	}else{
        		map.put(tLDCode1Schema, SysConst.INSERT);
        	}
    	}else if(mOperate.equals("change")){//修改
    		String strSQL = "update ldcode1 set code1 = '" +mComCode+ "' where codetype='uwuserconf' and code = '" +mUwUser+"'";
    		map.put(strSQL, SysConst.UPDATE);
    	}else if(mOperate.equals("delete")){//删除
    		String strSQL = "delete from ldcode1 where code1= '"+mComCode+"' and code = '"+mUwUser+"' and codetype='uwuserconf' "; 
    		map.put(strSQL, SysConst.DELETE);
    	}
    	
		return true;
	}

	private boolean getInputData(VData cInputData,String cOperate) {
		mOperate = cOperate;
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput",
                0);
        
        if (mGlobalInput == null)
        {
            buildError("checkData", "处理超时，请重新登录。");
            return false;
        }

        if (mTransferData == null)
        {
            buildError("checkData", "所需参数不完整。");
            return false;
        }

        if (mOperate == null)
        {
            buildError("checkData", "操作类型错误。");
            return false;
        }

		return true;
	}

}
