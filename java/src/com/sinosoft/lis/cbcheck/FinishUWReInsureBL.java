package com.sinosoft.lis.cbcheck;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.LCReInsurTaskSet;
import com.sinosoft.lis.db.LCReInsurTaskDB;
import com.sinosoft.lis.schema.LCReInsurTaskSchema;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class FinishUWReInsureBL
{
    public CErrors mErrors = new CErrors();
    private MMap map = new MMap();

    public FinishUWReInsureBL()
    {
    }

    /**
     *
     * @param data VData
     * @param operator String
     * @return boolean
     */
    public boolean submitData(VData data, String operator)
    {
        if(!dealData(data))
        {
            return false;
        }

        VData d = new VData();
        d.add(map);

        PubSubmit p = new PubSubmit();
        if(!p.submitData(d, ""))
        {
            System.out.println(p.mErrors.getErrContent());

            CError tError = new CError();
            tError.moduleName = "FinishUWReInsureBL";
            tError.functionName = "submitData";
            tError.errorMessage = "提交失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }

    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData(VData data)
    {
        TransferData tTransferData = (TransferData)data
                                     .getObjectByObjectName("TransferData", 0);
        String mRiskType;
        mRiskType = (String)tTransferData.getValueByName("RiskType");
        System.out.println("标记"+mRiskType);
    if(mRiskType.equals("1")) {
        String mGrpPolNo = (String) tTransferData.getValueByName("GrpPolNo");
         StringBuffer sql = new StringBuffer(255);
         sql.append("select * from LCReInsurTask where polno='");
         sql.append(mGrpPolNo);
         sql.append("' order by uwno desc");
         LCReInsurTaskSet tLCReInsurTaskSet = new LCReInsurTaskSet();
         tLCReInsurTaskSet = new LCReInsurTaskDB().executeQuery(sql.toString());
         if(tLCReInsurTaskSet.size() == 0)
         {
             CError tError = new CError();
             tError.moduleName = "SendUWReInsureBL";
             tError.functionName = "finishData";
             tError.errorMessage = "该险种没有再保审核信息";
             mErrors.addOneError(tError);
             System.out.println(tError.errorMessage);
             return false;
         }

         LCReInsurTaskSchema tLCReInsurTaskSchema = tLCReInsurTaskSet.get(1);  //取最后一次审核信息
         tLCReInsurTaskSchema.setState("02");  //办结
         map.put(tLCReInsurTaskSchema, SysConst.UPDATE);

        return true;
    }

else{
    String mPolNo = (String) tTransferData.getValueByName("PolNo");

    StringBuffer sql = new StringBuffer(255);
    sql.append("select * from LCReInsurTask where polno='");
    sql.append(mPolNo);
    sql.append("' order by uwno desc");
    LCReInsurTaskSet tLCReInsurTaskSet = new LCReInsurTaskSet();
    tLCReInsurTaskSet = new LCReInsurTaskDB().executeQuery(sql.toString());
    if (tLCReInsurTaskSet.size() == 0) {
        CError tError = new CError();
        tError.moduleName = "SendUWReInsureBL";
        tError.functionName = "finishData";
        tError.errorMessage = "该险种没有再保审核信息";
        mErrors.addOneError(tError);
        System.out.println(tError.errorMessage);
        return false;
    }

    LCReInsurTaskSchema tLCReInsurTaskSchema = tLCReInsurTaskSet.get(1); //取最后一次审核信息
    tLCReInsurTaskSchema.setState("02"); //办结
    map.put(tLCReInsurTaskSchema, SysConst.UPDATE);

    return true;
      }
    }

    public static void main(String[] args)
    {
        FinishUWReInsureBL finishuwreinsurebl = new FinishUWReInsureBL();
    }
}
