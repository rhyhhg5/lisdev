package com.sinosoft.lis.cbcheck;

import com.sinosoft.utility.*;
import com.sinosoft.lis.cbcheck.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description:
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author
 * @version 1.0
 */

public class GrpApplyRecallPolUI {

    private VData mInputData;
    private GlobalInput tG = new GlobalInput();
    private VData mResult = new VData();
    public CErrors mErrors = new CErrors();
    public GrpApplyRecallPolUI() {
    }

    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate) {
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();

        GrpApplyRecallPolBL tGrpApplyRecallPolBL = new GrpApplyRecallPolBL();
        System.out.println("Start tApplyRecallPolBL Submit...");
        tGrpApplyRecallPolBL.submitData(mInputData, cOperate);

        System.out.println("End tApplyRecallPolBL Submit...");

        //如果有需要处理的错误，则返回
        if (tGrpApplyRecallPolBL.mErrors.needDealError()) {
            mErrors.copyAllErrors(tGrpApplyRecallPolBL.mErrors);
            return false;
        }
        mResult.clear();
        mResult = tGrpApplyRecallPolBL.getResult();
        mInputData = null;
        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public static void main(String[] args) {
        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "86";
        tG.Operator = "001";
        TransferData mTransferData = new TransferData();
        mTransferData.setNameAndValue("CardFlag", "1");
        LCGrpApplyRecallPolSchema tLCGrpApplyRecallPolSchema = new
                LCGrpApplyRecallPolSchema();
        tLCGrpApplyRecallPolSchema.setRemark("test");
        tLCGrpApplyRecallPolSchema.setPrtNo("12000000374");

        // 准备传输数据 VData
        VData tVData = new VData();
        tVData.add(tLCGrpApplyRecallPolSchema);
        tVData.add(tG);
        tVData.add(mTransferData);
        // 数据传输
        GrpApplyRecallPolUI tGrpApplyRecallPolUI = new GrpApplyRecallPolUI();
        if (tGrpApplyRecallPolUI.submitData(tVData, "") == false) {
            System.out.println(tGrpApplyRecallPolUI.mErrors.getError(0).
                               errorMessage);
        }
    }

}
