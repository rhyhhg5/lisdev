package com.sinosoft.lis.cbcheck;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;


/**
 * <p>Title: Web业务系统承保个人单自动核保部分</p>
 * <p>Description:接口功能类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author WHN
 * @modified by ZhangRong  2004.11
 * @version 1.0
 */
public class UWAutoChkUI
{
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 往界面传输数据的容器 */
	private VData mResult = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	public UWAutoChkUI()
	{
	}

	/**
	传输数据的公共方法
	*/
	public boolean submitData(VData cInputData, String cOperate)
	{
		//将操作数据拷贝到本类中
		this.mOperate = cOperate;

		UWAutoChkBL tUWAutoChkBL = new UWAutoChkBL();

		System.out.println("---UWAutoChkUI BEGIN---");

		if (tUWAutoChkBL.submitData(cInputData, mOperate) == false)
		{
			// @@错误处理
			this.mErrors.copyAllErrors(tUWAutoChkBL.mErrors);
			CError tError = new CError();
			tError.moduleName = "UWAutoChkUI";
			tError.functionName = "submitData";
			this.mErrors.addOneError(tError);
			mResult.clear();

			return false;
		}
		else
		{
			this.mErrors.copyAllErrors(tUWAutoChkBL.mErrors);
		}

		return true;
	}

	// @Main
	public static void main(String[] args)
	{
		GlobalInput tG = new GlobalInput();
		tG.Operator = "001";

		LCContSchema tLCContSchema = new LCContSchema();

		tLCContSchema.setContNo("13000492666");

		VData tVData = new VData();
		tVData.add(tLCContSchema);
		tVData.add(tG);

		UWAutoChkUI ui = new UWAutoChkUI();

		if (ui.submitData(tVData, "") == true)
		{
			System.out.println("---ok---");
		}
		else
		{
			System.out.println("---NO---");
		}

		CErrors tError = ui.mErrors;
		int n = tError.getErrorCount();

		String Content = "TEST!";

		if (n > 0)
		{
			Content = Content.trim() + "有未通过自动核保保单原因是:";

			for (int i = 0; i < n; i++)
			{
				//tError = tErrors.getError(i);
				Content = Content.trim() + i + ". " + tError.getError(i).errorMessage.trim() + ".";
			}
		}
	}
}
