package com.sinosoft.lis.cbcheck;

import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class HeathIssueManagerBL {
	public CErrors mErrors = new CErrors();

	private String mCodeType = "";
  
	private String mMissionId = "";
	
	private String mPrtSeq ="";
	
	private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

	MMap map = new MMap();

	private GlobalInput mGlobalInput = new GlobalInput();

	public HeathIssueManagerBL() {
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "QyModifyUWOperatorBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	public boolean submitData(VData cInputData, String cOperate) {
		if (!cOperate.equals("DELETE")) {
			buildError("submitData", "不支持的操作字符串");
			return false;
		}

		System.out.println("Begin getInputData");
		if (!getInputData(cInputData)) {
			return false;
		}
		System.out.println("End getInputData");

		if(!dealdata()){
			
			return false;
		}
		
		
        if(map.size()>0){
        	VData vData = new VData();
    		vData.add(map);

    		PubSubmit pubsubmit = new PubSubmit();
    		if (!pubsubmit.submitData(vData, "")) {
    			mErrors.copyAllErrors(pubsubmit.mErrors);
    			return false;
    		}
        }else{
        	buildError("Delete", "没有要删除的单证信息！");
			return false;
        }		
		System.out.println("---End pubsubmit---");
		System.out.println("操作成功！");
		return true;
	}

	private boolean dealdata() {
		String loprtSQL="";
		String lcpSQL="";
		String lcpitemSQL="";
		String lzsSQL = "";
		String lwmSQL ="";
		String lwmUSQL1="";
		String lwmUSQL2="";
		String lwmUSQL3="";
		String esd = "";
		String esr = "";
		String esp = "";
		
		//体检件
		if(mCodeType.equals("03")){
			loprtSQL="delete from loprtmanager where prtseq='"+mPrtSeq+"'";
			lcpSQL="delete from LCPENotice where prtseq='"+mPrtSeq+"'";
			lcpitemSQL="delete from LCPENoticeItem where prtseq='"+mPrtSeq+"'";
			lzsSQL ="delete from LZSysCertify where CertifyNo='"+mPrtSeq+"'";
			lwmSQL ="delete from lwmission where missionid='"+mMissionId+"' and missionprop14='"+mPrtSeq+"' and activityid<>'0000001101'";
			esd = "delete from es_doc_main where doccode='"+mPrtSeq+"' and subtype='TB15'";
			esr = "delete from es_doc_relation where docid= (select docid from es_doc_main where doccode='"+mPrtSeq+"' and subtype='TB15') ";
			esp = "delete from es_doc_pages where docid= (select docid from es_doc_main where doccode='"+mPrtSeq+"' and subtype='TB015') ";
			lwmUSQL1="update lwmission set missionprop14 = null where missionid='"+mMissionId+"' and missionprop14='"+mPrtSeq+"' and activityid='0000001101'";
			SSRS tSSRS = new ExeSQL().execSQL("select 1 from lwmission where missionid='"+mMissionId+"' and missionprop14 is not null and activityid in ('0000001022','0000001104')");
			if(tSSRS.getMaxRow()>0){
				lwmUSQL2 = "update lwmission set activitystatus ='3' where  missionid='"+mMissionId+"' and activityid='0000001100' ";	
			}else{
				lwmUSQL2 = "update lwmission set activitystatus ='1' where  missionid='"+mMissionId+"' and activityid='0000001100' ";	
			}
			
			map.put(lwmUSQL2, "UPDATE");
			map.put(lwmUSQL1, "UPDATE");
			map.put(loprtSQL, "DELETE");
			map.put(lcpSQL, "DELETE");
			map.put(lcpitemSQL, "DELETE");
			map.put(lzsSQL, "DELETE");
			map.put(lwmSQL, "DELETE");
			map.put(esd, "DELETE");
			map.put(esr, "DELETE");
			map.put(esp, "DELETE");
			
			
		}else if(mCodeType.equals("04")){//契调件
			loprtSQL="delete from loprtmanager where prtseq='"+mPrtSeq+"'";
			lcpSQL="delete from LCRReport where prtseq='"+mPrtSeq+"'";
			lcpitemSQL="delete from LCRReportitem where prtseq='"+mPrtSeq+"'";
			lzsSQL ="delete from LZSysCertify where CertifyNo='"+mPrtSeq+"'";
			lwmSQL ="delete from lwmission where missionid='"+mMissionId+"' and missionprop14='"+mPrtSeq+"' and activityid<>'0000001104'";
			esd = "delete from es_doc_main where doccode='"+mPrtSeq+"' and subtype='TB16'";
			esr = "delete from es_doc_relation where docid= (select docid from es_doc_main where doccode='"+mPrtSeq+"' and subtype='TB16') ";
			esp = "delete from es_doc_pages where docid= (select docid from es_doc_main where doccode='"+mPrtSeq+"' and subtype='TB16') ";
			lwmUSQL1="update lwmission set missionprop14 = null where missionid='"+mMissionId+"' and missionprop14='"+mPrtSeq+"' and activityid='0000001104'";
			SSRS tSSRS = new ExeSQL().execSQL("select 1 from lwmission where missionid='"+mMissionId+"' and missionprop14 is not null and activityid in ('0000001022','0000001101')");
			if(tSSRS.getMaxRow()>0){
				lwmUSQL2 = "update lwmission set activitystatus ='3' where  missionid='"+mMissionId+"' and activityid='0000001100' ";	
			}else{
				lwmUSQL2 = "update lwmission set activitystatus ='1' where  missionid='"+mMissionId+"' and activityid='0000001100' ";	
			}
			map.put(lwmUSQL2, "UPDATE");
			map.put(lwmUSQL1, "UPDATE");
			map.put(loprtSQL, "DELETE");
			map.put(lcpSQL, "DELETE");
			map.put(lcpitemSQL, "DELETE");
			map.put(lzsSQL, "DELETE");
			map.put(lwmSQL, "DELETE");
			map.put(esd, "DELETE");
			map.put(esr, "DELETE");
			map.put(esp, "DELETE");
									
		}else if(mCodeType.equals("85")){//问题件
			loprtSQL="delete from loprtmanager where prtseq='"+mPrtSeq+"'";
			lcpSQL="delete from LCIssuePol where prtseq='"+mPrtSeq+"'";
			lzsSQL ="delete from LZSysCertify where CertifyNo='"+mPrtSeq+"'";
			lwmSQL ="delete from lwmission where missionid='"+mMissionId+"' and missionprop14='"+mPrtSeq+"' and activityid<>'0000001022'";
			esd = "delete from es_doc_main where doccode='"+mPrtSeq+"' and subtype='TB22'";
			esr = "delete from es_doc_relation where docid= (select docid from es_doc_main where doccode='"+mPrtSeq+"' and subtype='TB22') ";
			esp = "delete from es_doc_pages where docid= (select docid from es_doc_main where doccode='"+mPrtSeq+"' and subtype='TB22') ";
			lwmUSQL1 = "update lwmission set activitystatus ='1' where  missionid='"+mMissionId+"' and activityid='0000001001' and 1=(select count(1) from lwmission where missionid='"+mMissionId+"' and missionprop14='"+mPrtSeq+"' and activityid='0000001022')";
			lwmUSQL2 = "update lwmission set missionprop14=null where missionid='"+mMissionId+"' and missionprop14='"+mPrtSeq+"' and activityid='0000001022'";
			SSRS tSSRS = new ExeSQL().execSQL("select 1 from lwmission where missionid='"+mMissionId+"' and missionprop14 is not null and activityid in ('0000001104','0000001101')");
			if(tSSRS.getMaxRow()>0){
				lwmUSQL3 = "update lwmission set activitystatus ='3' where  missionid='"+mMissionId+"' and activityid='0000001100' ";	
			}else{
				lwmUSQL3 = "update lwmission set activitystatus ='1' where  missionid='"+mMissionId+"' and activityid='0000001100' ";	
			}
			
			map.put(lwmUSQL1, "UPDATE");
			map.put(lwmUSQL2, "UPDATE");
			map.put(lwmUSQL3, "UPDATE");
			map.put(loprtSQL, "DELETE");
			map.put(lcpSQL, "DELETE");
			map.put(lzsSQL, "DELETE");
			map.put(lwmSQL, "DELETE");
			map.put(esd, "DELETE");
			map.put(esr, "DELETE");
			map.put(esp, "DELETE");			
		}else{
			buildError("getInputData", "获取单证类型失败！");
			return false;
		}
		return true;
	}

	private boolean getInputData(VData cInputData) {
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0));
		mLOPRTManagerSchema = (LOPRTManagerSchema) cInputData.getObjectByObjectName("LOPRTManagerSchema",
				0);

		if (mLOPRTManagerSchema == null || "".equals(mLOPRTManagerSchema)) {
			buildError("getInputData", "获取单证信息失败！");
			return false;
		}
		LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
		tLOPRTManagerDB.setPrtSeq(mLOPRTManagerSchema.getPrtSeq());
		if(!tLOPRTManagerDB.getInfo()){
			buildError("getInputData", "获取单证信息失败！");
			return false;
		}
		
		mLOPRTManagerSchema=tLOPRTManagerDB.getSchema();
		
		mCodeType = mLOPRTManagerSchema.getCode();
		mMissionId = mLOPRTManagerSchema.getStandbyFlag3();
		mPrtSeq = mLOPRTManagerSchema.getPrtSeq();
		return true;
	}


}
