package com.sinosoft.lis.cbcheck;

import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LWMissionSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.db.LWMissionDB;
import com.sinosoft.utility.StrTool;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.MMap;

/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class UWIndStateBL {
    /** 报错处理 */
    public CErrors mErrors = new CErrors();
    /** 全局操作符 */
    private String mOperate;
    /** 传入要素 */
    private VData mInputData;
    /** 最终操作完成的数据封装 */
    private VData mResult = new VData();
    /** 工作流表LWMission */
    private LWMissionSchema mLWMissionSchema;
    /** 全局登陆信息 */
    private GlobalInput mGlobalInput;
    /** 状态信息 */
    private String mActivityStatus;
    /** 全局递交Map */
    private MMap map = new MMap();
    public UWIndStateBL() {
    }

    public static void main(String[] args) {
        UWIndStateBL uwindstatebl = new UWIndStateBL();
    }

    /**
     * submitData
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        this.mInputData = cInputData;
        this.mOperate = cOperate;

        if (!getInputData()) {
            return false;
        }

        if (!checkData()) {
            return false;
        }

        if (!dealData()) {
            return false;
        }

        if (!prepareOutputData()) {
            return false;
        }

        PubSubmit ps = new PubSubmit();
        if (!ps.submitData(mResult, null)) {
            this.mErrors.copyAllErrors(ps.mErrors);
            return false;
        }

        return true;
    }

    /**
     * prepareOutputData
     *
     * @return boolean
     */
    private boolean prepareOutputData() {
        map.put(this.mLWMissionSchema, "UPDATE");
        this.mResult.add(this.mLWMissionSchema);
        this.mResult.add(this.map);
        return true;
    }

    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData() {
        this.mLWMissionSchema.setActivityStatus(this.mActivityStatus);
        this.mLWMissionSchema.setModifyDate(PubFun.getCurrentDate());
        this.mLWMissionSchema.setModifyTime(PubFun.getCurrentTime());
        return true;
    }

    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData() {
        System.out.println("UWIndStateBL。。。。。。。checkData。。。。。");
        if (this.mLWMissionSchema == null) {
            buildError("checkData", "没有传入工作流信息，请重新确认操作是否正确！");
            return false;
        }
        if (this.mLWMissionSchema.getMissionID() == null) {
            buildError("checkData", "传入工作流ID为null！");
            return false;
        }
        if (this.mLWMissionSchema.getSubMissionID() == null) {
            buildError("checkData", "没有传入工作流子节点！");
            return false;
        }

        mActivityStatus = mLWMissionSchema.getActivityStatus();

        if (mActivityStatus == null || mActivityStatus.equals("")) {
            buildError("checkData", "工作流状态信息为null！");
            return false;
        }
        LWMissionDB tLWMissionDB = mLWMissionSchema.getDB();
        if (!tLWMissionDB.getInfo()) {
            buildError("checkData", "查询工作信息失败，原因是当前保单已不属于人工核保阶段！");
            return false;
        }
        mLWMissionSchema.setSchema(tLWMissionDB);

        if (mActivityStatus.equals(StrTool.cTrim(mLWMissionSchema.
                                                 getActivityStatus()))) {
            buildError("checkData", "当前核保任务的状态与所点击的状态相同，不需要发生改变！");
            return false;
        }
        return true;
    }

    /**
     * getInputData
     *
     * @return boolean
     */
    private boolean getInputData() {
        if (this.mInputData == null) {
            buildError("getInputData", "传入封装要素为null！");
            return false;
        }
        if (this.mOperate == null) {
            buildError("getInputData", "传入操作符为null！");
            return false;
        }
        mLWMissionSchema = (LWMissionSchema) mInputData.getObjectByObjectName(
                "LWMissionSchema", 0);
        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);

        return true;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "UWIndStateBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }

}
