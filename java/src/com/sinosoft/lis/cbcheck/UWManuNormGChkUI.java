package com.sinosoft.lis.cbcheck;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;


/**
 * <p>Title: Web业务系统承保个人人工核保部分</p>
 * <p>Description:接口功能类
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author ZhangRong
 * @version 1.0
 */
public class UWManuNormGChkUI
{
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 往界面传输数据的容器 */
	private VData mResult = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	public UWManuNormGChkUI()
	{
	}

	// @Main
	public static void main(String[] args)
	{
		GlobalInput tG = new GlobalInput();
		tG.Operator = "001";
		tG.ComCode = "86000000";
		tG.ManageCom = "86000000";

		LCPolSchema p = new LCPolSchema();
		LCSpecSchema tLCSpecSchema = new LCSpecSchema();
		LCPremSchema tLCPremSchema = new LCPremSchema();
		LCUWMasterSchema tLCUWMasterSchema = new LCUWMasterSchema();

		p.setProposalNo("10110000014500");
		p.setPolNo("10110000014500");
		p.setRemark("测试");
		tLCUWMasterSchema.setUWIdea("测试");
		tLCUWMasterSchema.setPostponeDay("1");
		p.setUWFlag("9");

		LCPolSet tLCPolSet = new LCPolSet();
		LCUWMasterSet tLCUWMasterSet = new LCUWMasterSet();

		tLCPolSet.add(p);
		tLCUWMasterSet.add(tLCUWMasterSchema);

		VData tVData = new VData();
		tVData.add(tLCPolSet);
		tVData.add(tLCUWMasterSet);
		tVData.add(tG);

		UWManuNormGChkUI ui = new UWManuNormGChkUI();

		if (ui.submitData(tVData, "") == true)
		{
			System.out.println("---ok---");
		}
		else
		{
			System.out.println("---NO---");
		}
	}

	/**
	传输数据的公共方法
	*/
	public boolean submitData(VData cInputData, String cOperate)
	{
		//将操作数据拷贝到本类中
		this.mOperate = cOperate;

		UWManuNormGChkBL tUWManuNormGChkBL = new UWManuNormGChkBL();

		System.out.println("---UWManuNormGChkBL UI BEGIN---");

		if (tUWManuNormGChkBL.submitData(cInputData, mOperate) == false)
		{
			// @@错误处理
			this.mErrors.copyAllErrors(tUWManuNormGChkBL.mErrors);

			CError tError = new CError();
			tError.moduleName = "UWAutoChkUI";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";

			//      this.mErrors .addOneError(tError) ;
			mResult.clear();

			return false;
		}

		System.out.println("---UWManuNormGChkBL UI End OK---");

		return true;
	}
}
