package com.sinosoft.lis.cbcheck;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
/**
 * <p>Title: Web业务系统分红处理功能部分</p>
 * <p>Description:接口功能类
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author WHN
 * @version 1.0
 */

public class GetBonusUI
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  public GetBonusUI() {}

// @Main
  public static void main(String[] args)
  {
  GlobalInput tG = new GlobalInput();
  tG.Operator = "001";
  tG.ManageCom = "86110000";

  LOBonusPolSchema tLOBonusPolSchema = new LOBonusPolSchema();
  LOBonusPolSet tLOBonusPolSet = new LOBonusPolSet();
  tLOBonusPolSchema.setPolNo("86110020030210001051");
  tLOBonusPolSchema.setFiscalYear("2003");

//  LOBonusPolSchema tLOBonusPolSchema1 = new LOBonusPolSchema();
//  tLOBonusPolSchema1.setPolNo("86110020030210005509");
//  tLOBonusPolSchema1.setFiscalYear("2003");

  tLOBonusPolSet.add(tLOBonusPolSchema);
//  tLOBonusPolSet.add(tLOBonusPolSchema1);

  VData tVData = new VData();
  tVData.add( tLOBonusPolSet);
  tVData.add( tG );
  GetBonusUI ui = new GetBonusUI();
  if( ui.submitData( tVData, "" ) == true )
      System.out.println("---ok---");
  else
      System.out.println("---NO---");
  }

  /**
  传输数据的公共方法
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;

    GetBonusBL tGetBonusBL = new GetBonusBL();

    System.out.println("---GetBonusBL UI BEGIN---");
    if (tGetBonusBL.submitData(cInputData,mOperate) == false)
        {
                  // @@错误处理
      this.mErrors.copyAllErrors(tGetBonusBL.mErrors);
      CError tError = new CError();
      tError.moduleName = "GetBonusUI";
      tError.functionName = "submitData";
      tError.errorMessage = "数据处理失败!";
      this.mErrors .addOneError(tError) ;
      mResult.clear();
      return false;
        }
    return true;
  }
}