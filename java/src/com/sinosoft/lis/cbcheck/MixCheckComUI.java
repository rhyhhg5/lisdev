package com.sinosoft.lis.cbcheck;


import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;


public class MixCheckComUI {

	public CErrors mErrors = new CErrors();
	
	public boolean submitData(VData cInputData, String cOperate) {
		
		MixCheckComBL tMixCheckComBL = new MixCheckComBL();//BL处理类
		
		if (tMixCheckComBL.submitData(cInputData, cOperate) == false)
		{
			// @@错误处理
			this.mErrors.copyAllErrors(tMixCheckComBL.mErrors);
			return false;
		}
		return true;
	}
	
}
