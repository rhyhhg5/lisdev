package com.sinosoft.lis.cbcheck;

import java.sql.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统到帐确认部分 </p>
 * <p>Description:数据库功能类 </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft< /p>
 * @author WHN
 * @version 1.0
 */

public class FinFeeSureBLS
{
        //是否存在需要人工核保保单
	int merrno = 0;
	//传输数据类
	private VData mInputData ;
	//错误处理类，每个需要错误处理的类中都放置该类
	public  CErrors mErrors = new CErrors();

	//
	private LCUWErrorSet mLCUWErrorSet = new LCUWErrorSet();

	public FinFeeSureBLS() {}

	public static void main(String[] args)
	{
	}

	//传输数据的公共方法
	public boolean submitData(VData cInputData,String cOperate)
	{
	    //首先将数据在本类中做一个备份
	    mInputData=(VData)cInputData.clone() ;

	System.out.println("Start FinFeeSureBLS Submit...");
		if (!this.saveData())
			return false;
	System.out.println("End FinFeeSureBLS Submit...");

	    mInputData=null;
	    return true;
	}

	private boolean saveData()
	{

	  	LJTempFeeClassSet mLJTempFeeClassSet = (LJTempFeeClassSet)mInputData.getObjectByObjectName("LJTempFeeClassSet",0);
                LJTempFeeSet mLJTempFeeSet = (LJTempFeeSet)mInputData.getObjectByObjectName("LJTempFeeSet",0);

                try
                {
                  for(int feeno = 1;feeno <= mLJTempFeeClassSet.size();feeno++)
                  {
                    LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();
                    tLJTempFeeClassSchema = mLJTempFeeClassSet.get(feeno);
                    String tTempFeeNo = tLJTempFeeClassSchema.getTempFeeNo();

                    Connection conn = DBConnPool.getConnection();
                    //conn = .getDefaultConnection();
                    if (conn==null)
                    {
                      // @@错误处理
                      CError tError = new CError();
                      tError.moduleName = "FinFeeSureBLS";
                      tError.functionName = "saveData";
                      tError.errorMessage = tTempFeeNo+"收据，数据库连接失败!";
                      this.mErrors .addOneError(tError) ;
                      //return false;
                      continue;
                    }

                    conn.setAutoCommit(false);

                    // 修改部分
                    LJTempFeeClassDB tLJTempFeeClassDB = new LJTempFeeClassDB(conn);
                    tLJTempFeeClassDB.setSchema(tLJTempFeeClassSchema);
                    if (tLJTempFeeClassDB.update() == false)
                    {
                      // @@错误处理
                      this.mErrors.copyAllErrors(tLJTempFeeClassDB.mErrors);
                      CError tError = new CError();
                      tError.moduleName = "FinFeeSureBLS";
                      tError.functionName = "saveData";
                      tError.errorMessage = tTempFeeNo+"暂交费关联表更新失败!";
                      this.mErrors .addOneError(tError) ;
                      conn.rollback() ;
                      conn.close();
                      //return false;
                      continue;
                    }

                    //集体
                    int fnum = mLJTempFeeSet.size();
                    if( fnum > 0)
                    {
                      for(int i = 1;i<= fnum;i++)
                      {
                        LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
                        tLJTempFeeSchema = mLJTempFeeSet.get(i);
                        if (tTempFeeNo.equals(tLJTempFeeSchema.getTempFeeNo()))
                        {
                          LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB(conn);
                          tLJTempFeeDB.setSchema(tLJTempFeeSchema);
                          if (tLJTempFeeDB.update() == false)
                          {
                            // @@错误处理
                            this.mErrors.copyAllErrors(tLJTempFeeClassDB.mErrors);
                            CError tError = new CError();
                            tError.moduleName = "FinFeeSureBLS";
                            tError.functionName = "saveData";
                            tError.errorMessage = tTempFeeNo+"暂交费表更新失败!";
                            this.mErrors .addOneError(tError) ;
                            conn.rollback() ;
                            conn.close();
                            //return false;
                            continue;
                          }
                        }
                      }
                    }

                    System.out.println("-----------EEE--------------");

                    conn.commit() ;
                    conn.close();

                  } // end of for
                } // end of try
                catch (Exception ex)
                {
                  // @@错误处理
                  CError tError = new CError();
                  tError.moduleName = "FinFeeSureBLS";
                  tError.functionName = "submitData";
                  tError.errorMessage = ex.toString();
                  this.mErrors .addOneError(tError);
                  return false;
                }
                return true;
        }
}