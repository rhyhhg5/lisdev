package com.sinosoft.lis.cbcheck;

import java.sql.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.f1print.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统分红处理功能部分 </p>
 * <p>Description: 数据库操作类</p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft< /p>
 * @author WHN
 * @version 1.0
 */

public class GetBonusBLS
{
        //是否存在需要人工核保保单
        int merrno = 0;
        //传输数据类
        private VData mInputData ;
        //错误处理类，每个需要错误处理的类中都放置该类
        public  CErrors mErrors = new CErrors();

        //
        private LCUWErrorSet mLCUWErrorSet = new LCUWErrorSet();

        public GetBonusBLS() {}

        public static void main(String[] args)
        {
        }

        //传输数据的公共方法
        public boolean submitData(VData cInputData,String cOperate)
        {
            //首先将数据在本类中做一个备份
            mInputData=(VData)cInputData.clone() ;

        System.out.println("Start CutBonusBLS Submit...");
                if (!this.saveData())
                        return false;
        System.out.println("End CutBonusBLS Submit...");

            mInputData=null;
            return true;
        }

        private boolean saveData()
        {

                LOBonusPolSet mLOBonusPolSet = (LOBonusPolSet)mInputData.getObjectByObjectName("LOBonusPolSet",0);
                LJAPaySet mLJAPaySet = (LJAPaySet)mInputData.getObjectByObjectName("LJAPaySet",0);
                LJAPayPersonSet mLJAPayPersonSet = (LJAPayPersonSet)mInputData.getObjectByObjectName("LJAPayPersonSet",0);
                LJAGetSet mLJAGetSet = (LJAGetSet)mInputData.getObjectByObjectName("LJAGetSet",0);
                LJABonusGetSet mLJABonusGetSet = (LJABonusGetSet)mInputData.getObjectByObjectName("LJABonusGetSet",0);
                LCInsureAccSet mLCInsureAccSet = (LCInsureAccSet)mInputData.getObjectByObjectName("LCInsureAccSet",0);
                LCInsureAccTraceSet mLCInsureAccTraceSet = (LCInsureAccTraceSet)mInputData.getObjectByObjectName("LCInsureAccTraceSet",0);
                LJTempFeeClassSet mLJTempFeeClassSet = (LJTempFeeClassSet)mInputData.getObjectByObjectName("LJTempFeeClassSet",0);
                LJTempFeeSet mLJTempFeeSet = (LJTempFeeSet)mInputData.getObjectByObjectName("LJTempFeeSet",0);
                LJFIGetSet mLJFIGetSet = (LJFIGetSet)mInputData.getObjectByObjectName("LJFIGetSet",0);
                LCPolSet mLCPolSet = (LCPolSet)mInputData.getObjectByObjectName("LCPolSet",0);
                LCDutySet mLCDutySet=(LCDutySet)mInputData.getObjectByObjectName("LCDutySet",0);
                LCPremSet mLCPremSet=(LCPremSet)mInputData.getObjectByObjectName("LCPremSet",0);
                LCGetSet mLCGetSet=(LCGetSet)mInputData.getObjectByObjectName("LCGetSet",0);
                LOPRTManagerSet mLOPRTManagerSet=(LOPRTManagerSet)mInputData.getObjectByObjectName("LOPRTManagerSet",0);

                try
                {
                  for(int feeno = 1;feeno <= mLOBonusPolSet.size();feeno++)
                  {
                      String tflag = "";
                      LOBonusPolSchema tLOBonusPolSchema = new LOBonusPolSchema();
                      LCPolDB tLCPolDB = new LCPolDB();
                      tLOBonusPolSchema = mLOBonusPolSet.get(feeno);

                      if(tLOBonusPolSchema.getBonusMoney()==0)
                          continue;

                      String tPolNo = tLOBonusPolSchema.getPolNo();
                      tLCPolDB.setPolNo(tPolNo);
                      if(!tLCPolDB.getInfo())
                      {
                          // @@错误处理
                          CError tError = new CError();
                          tError.moduleName = "CutBonusBLS";
                          tError.functionName = "saveData";
                          tError.errorMessage = tPolNo+"保单数据读取失败!";
                          this.mErrors .addOneError(tError) ;
                          //return false;
                          continue;
                      }

                      Connection conn = DBConnPool.getConnection();
                      if (conn==null)
                      {
                          // @@错误处理
                          CError tError = new CError();
                          tError.moduleName = "CutBonusBLS";
                          tError.functionName = "saveData";
                          tError.errorMessage = tPolNo+"收据，数据库连接失败!";
                          this.mErrors .addOneError(tError) ;
                          continue;
                      }

                      conn.setAutoCommit(false);

                      tflag = tLCPolDB.getBonusGetMode();
                      if(tflag.equals("2")) //现金
                      {
                          LOBonusPolDB tLOBonusPolDB = new LOBonusPolDB(conn);
                          tLOBonusPolDB.setSchema(tLOBonusPolSchema);
                          if(tLOBonusPolDB.update() == false)
                          {
                              // @@错误处理
                              this.mErrors.copyAllErrors(tLOBonusPolDB.mErrors);
                              CError tError = new CError();
                              tError.moduleName = "CutBonusBLS";
                              tError.functionName = "saveData";
                              tError.errorMessage = tPolNo+"分红表更新失败!";
                              this.mErrors .addOneError(tError) ;
                              conn.rollback() ;
                              conn.close();
                              //return false;
                              continue;
                          }

                      //给付总表
                      if(mLJAGetSet.size()>0)
                      {
                        for(int i = 1; i <= mLJAGetSet.size();i++)
                        {
                          LJAGetSchema tLJAGetSchema = new LJAGetSchema();
                          tLJAGetSchema = mLJAGetSet.get(i);
                          if(tLJAGetSchema.getOtherNo().equals(tPolNo))
                          {
                            LJAGetDB tLJAGetDB = new LJAGetDB(conn);
                            tLJAGetDB.setSchema(tLJAGetSchema);
                            if(tLJAGetDB.insert() == false)
                            {
                              // @@错误处理
                              this.mErrors.copyAllErrors(tLJAGetDB.mErrors);
                              CError tError = new CError();
                              tError.moduleName = "CutBonusBLS";
                              tError.functionName = "saveData";
                              tError.errorMessage = tPolNo+"实付总表生成失败!";
                              this.mErrors .addOneError(tError) ;
                              conn.rollback() ;
                              conn.close();
                              //return false;
                              continue;
                            }


                            //给付总表
                            if(mLJABonusGetSet.size()>0)
                            {
                              for(int j = 1; j <= mLJABonusGetSet.size();j++)
                              {
                                LJABonusGetSchema tLJABonusGetSchema = new LJABonusGetSchema();
                                tLJABonusGetSchema = mLJABonusGetSet.get(j);
                                if(tLJABonusGetSchema.getActuGetNo().equals(tLJAGetSchema.getActuGetNo()))
                                {
                                  LJABonusGetDB tLJABonusGetDB = new LJABonusGetDB(conn);
                                  tLJABonusGetDB.setSchema(tLJABonusGetSchema);
                                  if(tLJABonusGetDB.insert() == false)
                                  {
                                    // @@错误处理
                                    this.mErrors.copyAllErrors(tLJABonusGetDB.mErrors);
                                    CError tError = new CError();
                                    tError.moduleName = "CutBonusBLS";
                                    tError.functionName = "saveData";
                                    tError.errorMessage = tPolNo+"红利给付表生成失败!";
                                    this.mErrors .addOneError(tError) ;
                                    conn.rollback() ;
                                    conn.close();
                                    //return false;
                                    continue;
                                  }
                                }
                              }
                            }

                          }
                        }
                      }

                      conn.commit() ;
                      conn.close();
                    }

                    if(tflag.equals("3")) //保费
                    {
                      LOBonusPolDB tLOBonusPolDB = new LOBonusPolDB(conn);
                      tLOBonusPolDB.setSchema(tLOBonusPolSchema);
                      if(tLOBonusPolDB.update() == false)
                      {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tLOBonusPolDB.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "CutBonusBLS";
                        tError.functionName = "saveData";
                        tError.errorMessage = tPolNo+"分红表更新失败!";
                        this.mErrors .addOneError(tError) ;
                        conn.rollback() ;
                        conn.close();
                        continue;
                      }

                      //给付总表
                      if(mLJAGetSet.size()>0)
                      {
                        for(int i = 1; i <= mLJAGetSet.size();i++)
                        {
                          LJAGetSchema tLJAGetSchema = new LJAGetSchema();
                          tLJAGetSchema = mLJAGetSet.get(i);
                          if(tLJAGetSchema.getOtherNo().equals(tPolNo))
                          {
                            LJAGetDB tLJAGetDB = new LJAGetDB(conn);
                            tLJAGetDB.setSchema(tLJAGetSchema);
                            if(tLJAGetDB.insert() == false)
                            {
                              // @@错误处理
                              this.mErrors.copyAllErrors(tLJAGetDB.mErrors);
                              CError tError = new CError();
                              tError.moduleName = "CutBonusBLS";
                              tError.functionName = "saveData";
                              tError.errorMessage = tPolNo+"实付总表生成失败!";
                              this.mErrors .addOneError(tError) ;
                              conn.rollback() ;
                              conn.close();
                              //return false;
                              continue;
                            }


                            //给付总表
                            if(mLJABonusGetSet.size()>0)
                            {
                              for(int j = 1; j <= mLJABonusGetSet.size();j++)
                              {
                                LJABonusGetSchema tLJABonusGetSchema = new LJABonusGetSchema();
                                tLJABonusGetSchema = mLJABonusGetSet.get(j);
                                if(tLJABonusGetSchema.getActuGetNo().equals(tLJAGetSchema.getActuGetNo()))
                                {
                                  LJABonusGetDB tLJABonusGetDB = new LJABonusGetDB(conn);
                                  tLJABonusGetDB.setSchema(tLJABonusGetSchema);
                                  if(tLJABonusGetDB.insert() == false)
                                  {
                                    // @@错误处理
                                    this.mErrors.copyAllErrors(tLJABonusGetDB.mErrors);
                                    CError tError = new CError();
                                    tError.moduleName = "CutBonusBLS";
                                    tError.functionName = "saveData";
                                    tError.errorMessage = tPolNo+"红利给付表生成失败!";
                                    this.mErrors .addOneError(tError) ;
                                    conn.rollback() ;
                                    conn.close();
                                    //return false;
                                    continue;
                                  }
                                }
                              }
                            }

                            //财务付费总表
                            if(mLJFIGetSet.size()>0)
                            {
                              for(int j = 1; j <= mLJFIGetSet.size();j++)
                              {
                                LJFIGetSchema tLJFIGetSchema = new LJFIGetSchema();
                                tLJFIGetSchema = mLJFIGetSet.get(j);
                                if(tLJFIGetSchema.getActuGetNo().equals(tLJAGetSchema.getActuGetNo()))
                                {
                                  LJFIGetDB tLJFIGetDB = new LJFIGetDB(conn);
                                  tLJFIGetDB.setSchema(tLJFIGetSchema);
                                  if(tLJFIGetDB.insert() == false)
                                  {
                                    // @@错误处理
                                    this.mErrors.copyAllErrors(tLJFIGetDB.mErrors);
                                    CError tError = new CError();
                                    tError.moduleName = "CutBonusBLS";
                                    tError.functionName = "saveData";
                                    tError.errorMessage = tPolNo+"财务给付表生成失败!";
                                    this.mErrors .addOneError(tError) ;
                                    conn.rollback() ;
                                    conn.close();
                                    //return false;
                                    continue;
                                  }
                                }
                              }
                            }

                          }
                        }
                      }
                      //个人实收表
                      if(mLJAPayPersonSet.size()>0)
                      {
                        for(int i = 1; i <= mLJAPaySet.size();i++)
                        {
                          LJAPayPersonSchema tLJAPayPersonSchema = new LJAPayPersonSchema();
                          tLJAPayPersonSchema = mLJAPayPersonSet.get(i);
                          if(tLJAPayPersonSchema.getPolNo().equals(tPolNo))
                          {
                            LJAPayPersonDB tLJAPayPersonDB = new LJAPayPersonDB(conn);
                            tLJAPayPersonDB.setSchema(tLJAPayPersonSchema);
                            if(tLJAPayPersonDB.insert() == false)
                            {
                              // @@错误处理
                              this.mErrors.copyAllErrors(tLJAPayPersonDB.mErrors);
                              CError tError = new CError();
                              tError.moduleName = "CutBonusBLS";
                              tError.functionName = "saveData";
                              tError.errorMessage = tPolNo+"个人实收表生成失败!";
                              this.mErrors .addOneError(tError) ;
                              conn.rollback() ;
                              conn.close();
                              //return false;
                              continue;
                            }

                            //实收总表
                            if(mLJAPaySet.size()>0)
                            {
                              for(int j = 1; j <= mLJAPaySet.size();j++)
                              {
                                LJAPaySchema tLJAPaySchema = new LJAPaySchema();
                                tLJAPaySchema = mLJAPaySet.get(j);
                                if(tLJAPaySchema.getPayNo().equals(tLJAPayPersonSchema.getPayNo()))
                                {
                                  LJAPayDB tLJAPayDB = new LJAPayDB(conn);
                                  tLJAPayDB.setSchema(tLJAPaySchema);
                                  if(tLJAPayDB.insert() == false)
                                  {
                                    // @@错误处理
                                    this.mErrors.copyAllErrors(tLJAPayDB.mErrors);
                                    CError tError = new CError();
                                    tError.moduleName = "CutBonusBLS";
                                    tError.functionName = "saveData";
                                    tError.errorMessage = tPolNo+"实收总表生成失败!";
                                    this.mErrors .addOneError(tError) ;
                                    conn.rollback() ;
                                    conn.close();
                                    //return false;
                                    continue;
                                  }
                                }
                              }
                            }

                            //保单表
                            LCPolDB tempLCPolDB = new LCPolDB(conn);
                            for(int n=1;n<=mLCPolSet.size();n++)
                            {
                                if(mLCPolSet.get(n).getPolNo().equals(tPolNo))
                                {
                                    tempLCPolDB.setSchema(mLCPolSet.get(n));
                                    if(tempLCPolDB.update() == false)
                                    {
                                        this.mErrors.copyAllErrors(tempLCPolDB.mErrors);
                                        CError tError = new CError();
                                        tError.moduleName = "CutBonusBLS";
                                        tError.functionName = "saveData";
                                        tError.errorMessage = tPolNo+"保单表更新失败!";
                                        this.mErrors .addOneError(tError) ;
                                        conn.rollback() ;
                                        conn.close();
                                        continue;
                                  }
                               }
                            }


                            //暂交费关联表
                            if(mLJTempFeeClassSet.size()>0)
                            {
                              for(int j = 1; j <= mLJTempFeeClassSet.size();j++)
                              {
                                LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();
                                tLJTempFeeClassSchema = mLJTempFeeClassSet.get(j);
                                if(tLJTempFeeClassSchema.getTempFeeNo().equals(tLJAPayPersonSchema.getPayNo()))
                                {
                                  LJTempFeeClassDB tLJTempFeeClassDB = new LJTempFeeClassDB(conn);
                                  tLJTempFeeClassDB.setSchema(tLJTempFeeClassSchema);
                                  if(tLJTempFeeClassDB.insert() == false)
                                  {
                                    // @@错误处理
                                    this.mErrors.copyAllErrors(tLJTempFeeClassDB.mErrors);
                                    CError tError = new CError();
                                    tError.moduleName = "CutBonusBLS";
                                    tError.functionName = "saveData";
                                    tError.errorMessage = tPolNo+"暂交费关联表生成失败!";
                                    this.mErrors .addOneError(tError) ;
                                    conn.rollback() ;
                                    conn.close();
                                    //return false;
                                    continue;
                                  }
                                }
                              }
                            }

                            //暂交费表
                            if(mLJTempFeeSet.size()>0)
                            {
                              for(int j = 1; j <= mLJTempFeeSet.size();j++)
                              {
                                LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
                                tLJTempFeeSchema = mLJTempFeeSet.get(j);
                                if(tLJTempFeeSchema.getTempFeeNo().equals(tLJAPayPersonSchema.getPayNo()))
                                {
                                  LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB(conn);
                                  tLJTempFeeDB.setSchema(tLJTempFeeSchema);
                                  if(tLJTempFeeDB.insert() == false)
                                  {
                                    // @@错误处理
                                    this.mErrors.copyAllErrors(tLJTempFeeDB.mErrors);
                                    CError tError = new CError();
                                    tError.moduleName = "CutBonusBLS";
                                    tError.functionName = "saveData";
                                    tError.errorMessage = tPolNo+"暂交费表生成失败!";
                                    this.mErrors .addOneError(tError) ;
                                    conn.rollback() ;
                                    conn.close();
                                    //return false;
                                    continue;
                                  }
                                }
                              }
                            }

                          }
                        }
                      }


                      conn.commit() ;
                      conn.close();

                    }

                    if(tflag.equals("1")) //帐户
                    {
                      LOBonusPolDB tLOBonusPolDB = new LOBonusPolDB(conn);
                      tLOBonusPolDB.setSchema(tLOBonusPolSchema);
                      if(tLOBonusPolDB.update() == false)
                      {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tLOBonusPolDB.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "CutBonusBLS";
                        tError.functionName = "saveData";
                        tError.errorMessage = tPolNo+"分红表更新失败!";
                        this.mErrors .addOneError(tError) ;
                        conn.rollback() ;
                        conn.close();
                        //return false;
                        continue;
                      }

                      //帐户表
                      if(mLCInsureAccSet.size()>0)
                      {
                        for(int i = 1; i <= mLCInsureAccSet.size();i++)
                        {
                          LCInsureAccSchema tLCInsureAccSchema = new LCInsureAccSchema();
                          tLCInsureAccSchema = mLCInsureAccSet.get(i);
                          if(tLCInsureAccSchema.getPolNo().equals(tPolNo))
                          {
                            LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB(conn);
                            //tLCInsureAccDB.setPolNo(tPolNo);
                            //String tsql = "delete from LCInsureAcc where PolNo= '"+tPolNo+"'";
                            tLCInsureAccDB.setSchema(tLCInsureAccSchema);
                            if(tLCInsureAccDB.delete() == false)
                            {
                              // @@错误处理
                              this.mErrors.copyAllErrors(tLCInsureAccDB.mErrors);
                              CError tError = new CError();
                              tError.moduleName = "CutBonusBLS";
                              tError.functionName = "saveData";
                              tError.errorMessage = tPolNo+"帐户表删除失败!";
                              this.mErrors .addOneError(tError) ;
                              conn.rollback() ;
                              conn.close();
                              //return false;
                              continue;
                            }


                            if(tLCInsureAccDB.insert() == false)
                            {
                              // @@错误处理
                              this.mErrors.copyAllErrors(tLCInsureAccDB.mErrors);
                              CError tError = new CError();
                              tError.moduleName = "CutBonusBLS";
                              tError.functionName = "saveData";
                              tError.errorMessage = tPolNo+"帐户表生成失败!";
                              this.mErrors .addOneError(tError) ;
                              conn.rollback() ;
                              conn.close();
                              //return false;
                              continue;
                            }
                          }
                        }
                      }

                      //帐户轨迹表
                      if(mLCInsureAccTraceSet.size()>0)
                      {
                        for(int i = 1; i <= mLCInsureAccTraceSet.size();i++)
                        {
                          LCInsureAccTraceSchema tLCInsureAccTraceSchema = new LCInsureAccTraceSchema();
                          tLCInsureAccTraceSchema = mLCInsureAccTraceSet.get(i);
                          if(tLCInsureAccTraceSchema.getPolNo().equals(tPolNo))
                          {
                            LCInsureAccTraceDB tLCInsureAccTraceDB = new LCInsureAccTraceDB(conn);
                            tLCInsureAccTraceDB.setSchema(tLCInsureAccTraceSchema);
                            if(tLCInsureAccTraceDB.insert() == false)
                            {
                              // @@错误处理
                              this.mErrors.copyAllErrors(tLCInsureAccTraceDB.mErrors);
                              CError tError = new CError();
                              tError.moduleName = "CutBonusBLS";
                              tError.functionName = "saveData";
                              tError.errorMessage = tPolNo+"核保轨迹表生成失败!";
                              this.mErrors .addOneError(tError) ;
                              conn.rollback() ;
                              conn.close();
                              //return false;
                              continue;
                            }
                          }
                        }
                      }

                      conn.commit() ;
                      conn.close();
                    }

                    if(tflag.equals("5")) //增额交清
                    {
                        LOBonusPolDB tLOBonusPolDB = new LOBonusPolDB(conn);
                        tLOBonusPolDB.setSchema(tLOBonusPolSchema);
                        if(tLOBonusPolDB.update() == false)
                        {
                          // @@错误处理
                          this.mErrors.copyAllErrors(tLOBonusPolDB.mErrors);
                          CError tError = new CError();
                          tError.moduleName = "CutBonusBLS";
                          tError.functionName = "saveData";
                          tError.errorMessage = tPolNo+"分红表更新失败!";
                          this.mErrors .addOneError(tError) ;
                          conn.rollback() ;
                          conn.close();
                          //return false;
                          continue;
                      }

                      LCDutyDBSet tLCDutyDBSet=new LCDutyDBSet(conn);
                      tLCDutyDBSet.set(mLCDutySet);
                      if(tLCDutyDBSet.insert() == false)
                      {
                          // @@错误处理
                          this.mErrors.copyAllErrors(tLCDutyDBSet.mErrors);
                          CError tError = new CError();
                          tError.moduleName = "CutBonusBLS";
                          tError.functionName = "saveData";
                          tError.errorMessage = tPolNo+"责任表插入失败!";
                          this.mErrors .addOneError(tError) ;
                          conn.rollback() ;
                          conn.close();
                          continue;
                      }

                      LCGetDBSet tLCGetDBSet=new LCGetDBSet(conn);
                      tLCGetDBSet.set(mLCGetSet);
                      if(tLCGetDBSet.insert() == false)
                      {
                          // @@错误处理
                          this.mErrors.copyAllErrors(tLCGetDBSet.mErrors);
                          CError tError = new CError();
                          tError.moduleName = "CutBonusBLS";
                          tError.functionName = "saveData";
                          tError.errorMessage = tPolNo+"领取表插入失败!";
                          this.mErrors .addOneError(tError) ;
                          conn.rollback() ;
                          conn.close();
                          continue;
                      }

                      LCPremDBSet tLCPremDBSet=new LCPremDBSet(conn);
                      tLCPremDBSet.set(mLCPremSet);
                      if(tLCPremDBSet.insert() == false)
                      {
                          // @@错误处理
                          this.mErrors.copyAllErrors(tLCPremDBSet.mErrors);
                          CError tError = new CError();
                          tError.moduleName = "CutBonusBLS";
                          tError.functionName = "saveData";
                          tError.errorMessage = tPolNo+"保费项表插入失败!";
                          this.mErrors .addOneError(tError) ;
                          conn.rollback() ;
                          conn.close();
                          continue;
                      }

                    //保单表
                    LCPolDB tempLCPolDB = new LCPolDB(conn);
                    for(int n=1;n<=mLCPolSet.size();n++)
                    {
                        if(mLCPolSet.get(n).getPolNo().equals(tPolNo))
                        {
                            tempLCPolDB.setSchema(mLCPolSet.get(n));
                            if(tempLCPolDB.update() == false)
                            {
                                this.mErrors.copyAllErrors(tempLCPolDB.mErrors);
                                CError tError = new CError();
                                tError.moduleName = "CutBonusBLS";
                                tError.functionName = "saveData";
                                tError.errorMessage = tPolNo+"保单表更新失败!";
                                this.mErrors .addOneError(tError) ;
                                conn.rollback() ;
                                conn.close();
                                continue;
                           }
                        }
                     }

                     //给付总表
                     if(mLJAGetSet.size()>0)
                     {
                         for(int i = 1; i <= mLJAGetSet.size();i++)
                         {
                             LJAGetSchema tLJAGetSchema = new LJAGetSchema();
                             tLJAGetSchema = mLJAGetSet.get(i);
                             if(tLJAGetSchema.getOtherNo().equals(tPolNo))
                             {
                                 LJAGetDB tLJAGetDB = new LJAGetDB(conn);
                                 tLJAGetDB.setSchema(tLJAGetSchema);
                                 if(tLJAGetDB.insert() == false)
                                 {
                                     // @@错误处理
                                     this.mErrors.copyAllErrors(tLJAGetDB.mErrors);
                                     CError tError = new CError();
                                     tError.moduleName = "CutBonusBLS";
                                     tError.functionName = "saveData";
                                     tError.errorMessage = tPolNo+"实付总表生成失败!";
                                     this.mErrors .addOneError(tError) ;
                                     conn.rollback() ;
                                     conn.close();
                                     continue;
                                 }


                                 //给付子表
                                 if(mLJABonusGetSet.size()>0)
                                 {
                                     for(int j = 1; j <= mLJABonusGetSet.size();j++)
                                     {
                                         LJABonusGetSchema tLJABonusGetSchema = new LJABonusGetSchema();
                                         tLJABonusGetSchema = mLJABonusGetSet.get(j);
                                         if(tLJABonusGetSchema.getActuGetNo().equals(tLJAGetSchema.getActuGetNo()))
                                         {
                                             LJABonusGetDB tLJABonusGetDB = new LJABonusGetDB(conn);
                                             tLJABonusGetDB.setSchema(tLJABonusGetSchema);
                                             if(tLJABonusGetDB.insert() == false)
                                             {
                                                 // @@错误处理
                                                 this.mErrors.copyAllErrors(tLJABonusGetDB.mErrors);
                                                 CError tError = new CError();
                                                 tError.moduleName = "CutBonusBLS";
                                                 tError.functionName = "saveData";
                                                 tError.errorMessage = tPolNo+"红利给付表生成失败!";
                                                 this.mErrors .addOneError(tError) ;
                                                 conn.rollback() ;
                                                 conn.close();
                                                 continue;
                                             }
                                         }
                                     }
                                 }

                                 //财务付费总表
                                 if(mLJFIGetSet.size()>0)
                                 {
                                     for(int j = 1; j <= mLJFIGetSet.size();j++)
                                     {
                                         LJFIGetSchema tLJFIGetSchema = new LJFIGetSchema();
                                         tLJFIGetSchema = mLJFIGetSet.get(j);
                                         if(tLJFIGetSchema.getActuGetNo().equals(tLJAGetSchema.getActuGetNo()))
                                         {
                                             LJFIGetDB tLJFIGetDB = new LJFIGetDB(conn);
                                             tLJFIGetDB.setSchema(tLJFIGetSchema);
                                             if(tLJFIGetDB.insert() == false)
                                             {
                                                 // @@错误处理
                                                 this.mErrors.copyAllErrors(tLJFIGetDB.mErrors);
                                                 CError tError = new CError();
                                                 tError.moduleName = "CutBonusBLS";
                                                 tError.functionName = "saveData";
                                                 tError.errorMessage = tPolNo+"财务给付表生成失败!";
                                                 this.mErrors .addOneError(tError) ;
                                                 conn.rollback() ;
                                                 conn.close();
                                                 continue;
                                             }
                                         }
                                     }
                                 }

                             }
                         }
                     }
                     //个人实收表
                     if(mLJAPayPersonSet.size()>0)
                     {
                         for(int i = 1; i <= mLJAPaySet.size();i++)
                         {
                             LJAPayPersonSchema tLJAPayPersonSchema = new LJAPayPersonSchema();
                             tLJAPayPersonSchema = mLJAPayPersonSet.get(i);
                             if(tLJAPayPersonSchema.getPolNo().equals(tPolNo))
                             {
                                 LJAPayPersonDB tLJAPayPersonDB = new LJAPayPersonDB(conn);
                                 tLJAPayPersonDB.setSchema(tLJAPayPersonSchema);
                                 if(tLJAPayPersonDB.insert() == false)
                                 {
                                     // @@错误处理
                                     this.mErrors.copyAllErrors(tLJAPayPersonDB.mErrors);
                                     CError tError = new CError();
                                     tError.moduleName = "CutBonusBLS";
                                     tError.functionName = "saveData";
                                     tError.errorMessage = tPolNo+"个人实收表生成失败!";
                                     this.mErrors .addOneError(tError) ;
                                     conn.rollback() ;
                                     conn.close();
                                     continue;
                                 }

                                 //实收总表
                                 if(mLJAPaySet.size()>0)
                                 {
                                     for(int j = 1; j <= mLJAPaySet.size();j++)
                                     {
                                         LJAPaySchema tLJAPaySchema = new LJAPaySchema();
                                         tLJAPaySchema = mLJAPaySet.get(j);
                                         if(tLJAPaySchema.getPayNo().equals(tLJAPayPersonSchema.getPayNo()))
                                         {
                                             LJAPayDB tLJAPayDB = new LJAPayDB(conn);
                                             tLJAPayDB.setSchema(tLJAPaySchema);
                                             if(tLJAPayDB.insert() == false)
                                             {
                                                 // @@错误处理
                                                 this.mErrors.copyAllErrors(tLJAPayDB.mErrors);
                                                 CError tError = new CError();
                                                 tError.moduleName = "CutBonusBLS";
                                                 tError.functionName = "saveData";
                                                 tError.errorMessage = tPolNo+"实收总表生成失败!";
                                                 this.mErrors .addOneError(tError) ;
                                                 conn.rollback() ;
                                                 conn.close();
                                                 continue;
                                             }
                                         }
                                     }
                                 }


                                 //暂交费关联表
                                 if(mLJTempFeeClassSet.size()>0)
                                 {
                                     for(int j = 1; j <= mLJTempFeeClassSet.size();j++)
                                     {
                                         LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();
                                         tLJTempFeeClassSchema = mLJTempFeeClassSet.get(j);
                                         if(tLJTempFeeClassSchema.getTempFeeNo().equals(tLJAPayPersonSchema.getPayNo()))
                                         {
                                             LJTempFeeClassDB tLJTempFeeClassDB = new LJTempFeeClassDB(conn);
                                             tLJTempFeeClassDB.setSchema(tLJTempFeeClassSchema);
                                             if(tLJTempFeeClassDB.insert() == false)
                                             {
                                                 // @@错误处理
                                                 this.mErrors.copyAllErrors(tLJTempFeeClassDB.mErrors);
                                                 CError tError = new CError();
                                                 tError.moduleName = "CutBonusBLS";
                                                 tError.functionName = "saveData";
                                                 tError.errorMessage = tPolNo+"暂交费关联表生成失败!";
                                                 this.mErrors .addOneError(tError) ;
                                                 conn.rollback() ;
                                                 conn.close();
                                                 continue;
                                             }
                                         }
                                     }
                                 }

                                 //暂交费表
                                 if(mLJTempFeeSet.size()>0)
                                 {
                                     for(int j = 1; j <= mLJTempFeeSet.size();j++)
                                     {
                                         LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
                                         tLJTempFeeSchema = mLJTempFeeSet.get(j);
                                         if(tLJTempFeeSchema.getTempFeeNo().equals(tLJAPayPersonSchema.getPayNo()))
                                         {
                                             LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB(conn);
                                             tLJTempFeeDB.setSchema(tLJTempFeeSchema);
                                             if(tLJTempFeeDB.insert() == false)
                                             {
                                                 // @@错误处理
                                                 this.mErrors.copyAllErrors(tLJTempFeeDB.mErrors);
                                                 CError tError = new CError();
                                                 tError.moduleName = "CutBonusBLS";
                                                 tError.functionName = "saveData";
                                                 tError.errorMessage = tPolNo+"暂交费表生成失败!";
                                                 this.mErrors .addOneError(tError) ;
                                                 conn.rollback() ;
                                                 conn.close();
                                                 //return false;
                                                 continue;
                                             }
                                         }
                                     }
                                 }

                             }
                         }
                     }

                     conn.commit() ;
                     conn.close();
                    }

                  } // end of for
                } // end of try
                catch (Exception ex)
                {
                  // @@错误处理
                  CError tError = new CError();
                  tError.moduleName = "CutBonusBLS";
                  tError.functionName = "submitData";
                  tError.errorMessage = ex.toString();
                  this.mErrors .addOneError(tError);
                  return false;
                }
                return true;
        }

        /**
         * 分配红利后插入打印数据
         * @param tLCPolSchema
         * @return
         */
        public LOPRTManagerSchema getPrintData(LCPolSchema tLCPolSchema)
        {
            LOPRTManagerSchema tLOPRTManagerSchema=new LOPRTManagerSchema();
            try
            {
                String tLimit=PubFun.getNoLimit(tLCPolSchema.getManageCom());
                String prtSeqNo=PubFun1.CreateMaxNo("PRTSEQNO",tLimit);
                tLOPRTManagerSchema.setPrtSeq(prtSeqNo);
                tLOPRTManagerSchema.setOtherNo(tLCPolSchema.getPolNo());
                tLOPRTManagerSchema.setOtherNoType("00");
                tLOPRTManagerSchema.setCode(PrintManagerBL.CODE_BONUSPAY);
                tLOPRTManagerSchema.setManageCom(tLCPolSchema.getManageCom());
                tLOPRTManagerSchema.setAgentCode(tLCPolSchema.getAgentCode());
                tLOPRTManagerSchema.setReqCom(tLCPolSchema.getManageCom());
                tLOPRTManagerSchema.setReqOperator(tLCPolSchema.getOperator());
                tLOPRTManagerSchema.setPrtType("0");
                tLOPRTManagerSchema.setStateFlag("0");
                tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
                tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
            }
            catch(Exception ex)
            {
                return null;
            }

            return tLOPRTManagerSchema;
        }

}