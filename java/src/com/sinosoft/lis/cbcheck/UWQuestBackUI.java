/**
 * created 2008-8-4
 * by LY
 */
package com.sinosoft.lis.cbcheck;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class UWQuestBackUI
{
    /** �������� */
    public CErrors mErrors = new CErrors();

    public UWQuestBackUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            UWQuestBackBL tUWQuestBackBL = new UWQuestBackBL();
            if (!tUWQuestBackBL.submitData(cInputData, cOperate))
            {
                this.mErrors.copyAllErrors(tUWQuestBackBL.mErrors);
                return false;
            }
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            this.mErrors.addOneError(e.getMessage());
            return false;
        }
    }
}
