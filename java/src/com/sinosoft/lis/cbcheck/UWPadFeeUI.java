package com.sinosoft.lis.cbcheck;

import java.lang.*;
import java.util.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bank.RealTimeSendBankUI;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.cbcheck.*;
import com.sinosoft.lis.f1print.*;
import com.sinosoft.lis.finfee.*;

/**
 * <p>
 * Title: Web业务系统承保个人人工核保部分
 * </p>
 * <p>
 * Description: 逻辑处理类
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author WHN
 * @version 1.0
 */
public class UWPadFeeUI {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 往后面传输数据的容器 */
	private VData mInputData;

	private String getnoticeno = "";
	
	private String bankcode="";

	private TransferData mTransferData = new TransferData();

	private GlobalInput mGlobalInput = new GlobalInput();// 公共信息

	private String mMissionID = "";

	private String mPrtNo = "";

	private String mContNo = "";

	public UWPadFeeUI() {
	}

	/**
	 * 传输数据的公共方法
	 * 
	 * @param: cInputData 输入的数据 cOperate 数据操作
	 * @return:
	 */
	public boolean submitData(VData cInputData, String cOperate) {

		// 将操作数据拷贝到本类中
		mInputData = (VData) cInputData.clone();

		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		if (!dealData()) {
			return false;
		}

		return true;
	}

	private boolean dealData() {

		// 查询是否核保完毕，签单工作流生成
		LWMissionDB tLWMissionDB = new LWMissionDB();
		String signSQL = "select 1 from lwmission where missionid='"
				+ mMissionID
				+ "' and activityid='0000001150' and missionprop1='" + mContNo
				+ "' and missionprop2='" + mPrtNo + "' ";
		LWMissionSet tLWMissionSet = new LWMissionSet();
		tLWMissionSet = tLWMissionDB.executeQuery(signSQL);
		if (tLWMissionSet.size() != 1) {
			return false;
		}
		
		LCContDB tLCCont = new LCContDB();
		tLCCont.setContNo(mContNo);
		if(!tLCCont.getInfo()){
			return false;
		}
		if(!"PD".equals(tLCCont.getPrtNo().substring(0, 2))){
			return false;
		}
		
		SSRS tSSRS=new SSRS();
		String tempinfo = "select 1 from ljtempfee where otherno='"
				+ mPrtNo + "' and enteraccdate is null";
		tSSRS = new ExeSQL().execSQL(tempinfo);
		if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
			System.out.println("保单无收费信息");
			return false;
		}

		String ljsinfo = "select getnoticeno,bankcode from ljspay where otherno='"
				+ mPrtNo + "' and othernotype='9' and cansendbank='p' ";
		tSSRS = new ExeSQL().execSQL(ljsinfo);
		if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
			System.out.println("保单应收数据错误");
			return false;
		} else {
			getnoticeno = tSSRS.GetText(1, 1);
			bankcode = tSSRS.GetText(1, 2);
		}

		RealTimeSendBankUI tRealTimeSendBankUI = new RealTimeSendBankUI();
		TransferData transferData1 = new TransferData();
		transferData1.setNameAndValue("getNoticeNo", getnoticeno);
		transferData1.setNameAndValue("bankCode", "7705");
		transferData1.setNameAndValue("uniteBankCode",bankcode );

		VData tVData = new VData();
		tVData.add(transferData1);
		tVData.add(mGlobalInput);

		if (!tRealTimeSendBankUI.submitData(tVData, "GETMONEY")) {
			return false;
		}

		return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) {
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0));
		mTransferData = (TransferData) cInputData.getObjectByObjectName(
				"TransferData", 0);
		mMissionID = (String) mTransferData.getValueByName("MissionID");
		mPrtNo = (String) mTransferData.getValueByName("PrtNo");
		mContNo = (String) mTransferData.getValueByName("ContNo");
		if (mPrtNo == "" || mPrtNo == null) {
			return false;
		}
		if (mMissionID == "" || mMissionID == null) {
			return false;
		}
		if (mContNo == "" || mContNo == null) {
			return false;
		}
		return true;

	}

}
