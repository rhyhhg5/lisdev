package com.sinosoft.lis.cbcheck;

import oracle.jdbc.OracleResultSet;

import oracle.sql.*;

import java.io.*;
import java.lang.*;
import java.sql.*;
import java.util.*;

import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.cbcheck.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;


/**
 * <p>Title: Web业务系统核保报告书查询录入部分</p>
 * <p>Description: 逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author WHN
 * @modified ZhangRong
 * @version 1.0
 */
public class UWReportBL
{
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 往后面传输数据的容器 */
	private VData mInputData;

	/** 往界面传输数据的容器 */
	private VData mResult = new VData();

	/** 数据操作字符串 */
	private String mOperate;
	private String mManageCom;
	private String mContent;
	private LCUWReportSchema mLCUWReportSchema = new LCUWReportSchema();
	private GlobalInput mGlobalInput = new GlobalInput();

	public UWReportBL()
	{
	}

	/**
	 * 传输数据的公共方法
	 * @param: cInputData 输入的数据
	 *         cOperate 数据操作
	 * @return:
	 */
	public boolean submitData(VData cInputData, String cOperate)
	{
		//将操作数据拷贝到本类中
		mInputData = (VData)cInputData.clone();
		mOperate = cOperate;

		//得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData))
		{
			return false;
		}

		System.out.println("---UWReportBL getInputData---");

		if (!dealData())
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "UWReportBL";
			tError.functionName = "dealData";
			tError.errorMessage = "数据处理失败!";
			this.mErrors.addOneError(tError);

			return false;
		}

		if (this.mOperate.equals("QUERY"))
		{
			if (!this.submitquery())
			{
				return false;
			}
		}
		else if (this.mOperate.equals("INSERT"))    //先查询再插入
		{
			//准备给后台的数据
			if (!prepareOutputData())
			{
				return false;
			}

			System.out.println("---UWReportBL prepareOutputData---");

			//数据提交
			System.out.println("Start UWReporBL Submit...");

			UWReportBLS tUWReportBLS = new UWReportBLS();
			System.out.println("End UWReportBL Submit...");

			//如果有需要处理的错误，则返回
			if (!tUWReportBLS.submitData(mInputData, cOperate))
			{
				// @@错误处理
				this.mErrors.copyAllErrors(tUWReportBLS.mErrors);

				CError tError = new CError();
				tError.moduleName = "UWReportBL";
				tError.functionName = "submitData";
				tError.errorMessage = "数据提交失败!";
				this.mErrors.addOneError(tError);

				return false;
			}
		}

		System.out.println("---UWReportBL commitData---");

		return true;
	}

	/**
	 * 从输入数据中得到所有对象
	 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData)
	{
		mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput", 0));
		mManageCom = mGlobalInput.ManageCom;

		//下面这句话要加上，因为传过来的数据中还有schema的信息
		mLCUWReportSchema.setSchema((LCUWReportSchema)cInputData.getObjectByObjectName("LCUWReportSchema", 0));
		mContent = (String)cInputData.getObjectByObjectName("String", 0);

		LCContDB tLCContDB = new LCContDB();
		tLCContDB.setContNo(mLCUWReportSchema.getContNo());

		LCContSet tLCContSet = tLCContDB.query();

		if (tLCContSet.size() == 0)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "UWReportBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "无法查到合同号为:" + mLCUWReportSchema.getContNo() + " 的合同记录！";
			this.mErrors.addOneError(tError);

			return false;
		}

		mLCUWReportSchema.setGrpContNo(tLCContSet.get(1).getGrpContNo());
		mLCUWReportSchema.setProposalContNo(tLCContSet.get(1).getProposalContNo());

		return true;
	}

	/**
	 * 准备往后层输出所需要的数据
	 * 输出：如果准备数据时发生错误则返回false,否则返回true
	 */
	private boolean submitquery()
	{
		String str;
		Connection conn;    //建立数据库的连接
		conn = null;
		conn = DBConnPool.getConnection();

		String tSQL = "";
		java.sql.Blob tBlob = null;
		COracleBlob tCOracleBlob = new COracleBlob();

		if (conn == null)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "UWReportBL";
			tError.functionName = "submitquery";
			tError.errorMessage = "数据库连接失败!";
			this.mErrors.addOneError(tError);

			return false;
		}

		try
		{
			conn.setAutoCommit(false);

			//处理二进制
			byte[] tbytes;

			tSQL = " and ContNo = '" + mLCUWReportSchema.getContNo() + "' and uwoperator = '" + mLCUWReportSchema.getUWOperator() + "'";
			tBlob = tCOracleBlob.SelectBlob("lcuwreport", "contente", tSQL, conn);

			if (tBlob == null)
			{
				throw new Exception("找不到打印数据");
			}

			InputStream ins = null;
			ins = tBlob.getBinaryStream();

			ByteArrayOutputStream os = new ByteArrayOutputStream();

			int nData = 0;

			while ((nData = ins.read()) != -1)
			{
				os.write(nData);
			}

			os.flush();
			os.close();

			str = new String(os.toByteArray());
			str = StrTool.unicodeToGBK(str);

			ins.close();
			conn.close();
		}
		catch (Exception ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "UWReportBL";
			tError.functionName = "submitquery";
			tError.errorMessage = ex.toString();
			this.mErrors.addOneError(tError);

			try
			{
				conn.close();
			}
			catch (Exception e)
			{
			}

			return false;
		}

		this.mResult.clear();
		mResult.add(str);

		return true;
	}

	/*
	* 取所在总单类型
	*/
	private boolean GetPolType()
	{
		/* comment by zhr 2004.11.20
			try{
			  String tContNo;
			  tContNo=mLCUWReportSchema.getContNo();
			  String tflag = "";
			  tflag = tContNo.substring(11,13);
			  if(tflag.equals("11")||tflag.equals("21"))
				mLCUWReportSchema.setPolType("1");
			  else if(tflag.equals("12")||tflag.equals("22"))
				mLCUWReportSchema.setPolType("2");
			  else if(tflag.equals("13")||tflag.equals("23"))
				mLCUWReportSchema.setPolType("3");
			}
			catch(Exception ex)
			{
			  // @@错误处理
			  CError tError =new CError();
			  tError.moduleName="UWReportBL";
			  tError.functionName="GetPolType";
			  tError.errorMessage=ex.toString();
			  this.mErrors .addOneError(tError) ;
			  return false;
			}
		*/
		mLCUWReportSchema.setPolType("1");

		return true;
	}

	/**
	 * 准备体检资料信息
	 * 输出：如果发生错误则返回false,否则返回true
	 */
	private boolean dealData()
	{
		LCUWReportSchema tLCUWReportSchema = new LCUWReportSchema();    //++

		if (!GetPolType())
		{
			return false;
		}

		tLCUWReportSchema.setSchema(mLCUWReportSchema);    //++
		tLCUWReportSchema.setManageCom(mManageCom);
		tLCUWReportSchema.setMakeDate(PubFun.getCurrentDate());
		tLCUWReportSchema.setMakeTime(PubFun.getCurrentTime());
		tLCUWReportSchema.setModifyDate(PubFun.getCurrentDate());
		tLCUWReportSchema.setModifyTime(PubFun.getCurrentTime());

		mLCUWReportSchema.setSchema(tLCUWReportSchema);

		return true;
	}

	/**
	 *准备需要保存的数据
	 **/
	private boolean prepareOutputData()
	{
		try
		{
			this.mInputData.clear();
			this.mInputData.add(mLCUWReportSchema);
			this.mInputData.add(mContent);
			mResult.clear();
		}
		catch (Exception ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "UWReportBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);

			return false;
		}

		return true;
	}

	/**
	 *这个函数是往界面传返回值用的
	 **/
	public VData getResult()
	{
		return mResult;
	}
}
