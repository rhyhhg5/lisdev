package com.sinosoft.lis.cbcheck;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.util.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 承保暂交费业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author tjj
 * @version 1.0
 */
public class UWModifyTypeBCBL
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData ;
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;
  private String mPolNo;

  /**  */
  private LPEdorMainSchema mLPEdorMainSchema = new LPEdorMainSchema();
  private LPGrpEdorMainSchema mLPGrpEdorMainSchema = new LPGrpEdorMainSchema();

  private  LCBnfSet  mLCBnfSet= new LCBnfSet();
  /** 全局数据 */
  private GlobalInput mGlobalInput = new GlobalInput();

  private LPEdorMainSet outLPEdorMainSet = new LPEdorMainSet();

  public UWModifyTypeBCBL() {}

  /**
   * 传输数据的公共方法
   * @param: cInputData 输入的数据
   *         cOperate 数据操作
   * @return:
   */
  public void setOperate(String cOperate)
  {
	this.mOperate=cOperate;
  }

  public  String getOperate()
  {
	return this.mOperate;
  }

  public boolean submitData(VData cInputData,String cOperate)
  {
	System.out.println("--------------in SubMit-------");
	//将操作数据拷贝到本类中
	mInputData = (VData)cInputData.clone() ;

	//得到外部传入的数据,将数据备份到本类中
	getInputData(cInputData)  ;


     //数据校验操作（checkdata)
	if (!checkData())
      return false;
    //数据准备操作（preparedata())
    if(!prepareData() )
       return false;

 cOperate=this.getOperate();
 System.out.println("---oper"+cOperate);

 UWModifyTypeBCBLS tUWModifyTypeBCBLS = new UWModifyTypeBCBLS();
 if(tUWModifyTypeBCBLS.submitData(mInputData, "") == false)	{
   //@@错误处理
   this.mErrors.copyAllErrors(tUWModifyTypeBCBLS.mErrors);
   return false;

 }
 return true;
  }

  public VData getResult()
  {
	return mResult;
  }

  /**
   * 数据操作类业务处理
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private void getInputData(VData cInputData)
  {
	mLCBnfSet=(LCBnfSet)cInputData.getObjectByObjectName("LCBnfSet",0);
	mPolNo = (String) cInputData.getObjectByObjectName("String",0);
	mGlobalInput = (GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0);
  }
  /**
   *只是撤销本次申请的某个保全项目
   */

  /**
   * 校验传入的数据的合法性
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean checkData()
  {

    boolean flag = true;
	if (mLCBnfSet != null && mLCBnfSet.size()>0)
	{
	  LCBnfSet tLCBnfSet = new LCBnfSet();

	  for (int i=1;i<=mLCBnfSet.size();i++)
	  {
		LDPersonSchema tLDPersonSchema = new LDPersonSchema();
		if (mLCBnfSet.get(i).getName()==null||mLCBnfSet.get(i).getName().trim().equals(""))
		{
		  // @@错误处理
		  CError tError = new CError();
		  tError.moduleName = "PBnfBL";
		  tError.functionName = "checkdata";
		  tError.errorMessage = "客户姓名不能为空!";
		  this.mErrors.addOneError(tError);
		  return false;
		}
		if (mLCBnfSet.get(i).getCustomerNo()!=null&&!mLCBnfSet.get(i).getCustomerNo().trim().equals(""))
		{
		  LDPersonDB tLDPersonDB = new LDPersonDB();
		  tLDPersonDB.setCustomerNo(mLCBnfSet.get(i).getCustomerNo());
		  if (!tLDPersonDB.getInfo())
		  {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "PBnfBL";
			tError.functionName = "checkdata";
			tError.errorMessage = "客户号有误,请重新输入!";
			this.mErrors.addOneError(tError);
			return false;
		  } else {
			tLDPersonSchema = tLDPersonDB.getSchema();
			Reflections tRef = new Reflections();
//            if (!tRef.compareFields(mLPBnfSet.get(i).getSchema(),tLDPersonSchema))
//            {
//              // @@错误处理
//              CError tError = new CError();
//              tError.moduleName = "PBnfBL";
//              tError.functionName = "checkdata";
//              tError.errorMessage = "不能变更客户信息!";
//              this.mErrors.addOneError(tError);
//              return false;
//            }
		  }
		}
		if (i==1)
		{
		  tLCBnfSet.add(mLCBnfSet.get(i).getSchema());
		}
		else
		{
		  boolean tBnfFlag = false;
		  for (int j=1;j<=tLCBnfSet.size();j++)
		  {
			if (mLCBnfSet.get(i).getBnfType().equals(tLCBnfSet.get(j).getBnfType())
				&&mLCBnfSet.get(i).getBnfGrade().equals(tLCBnfSet.get(j).getBnfGrade()))
			{
			  tLCBnfSet.get(j).setBnfLot(mLCBnfSet.get(i).getBnfLot()+tLCBnfSet.get(j).getBnfLot());
			  System.out.println("-----1:"+tLCBnfSet.get(j).getBnfLot());
			  tBnfFlag = true;
			}
		  }
		  if (!tBnfFlag)
		  {
			tLCBnfSet.add(mLCBnfSet.get(i).getSchema());
		  }
		}
	  }
	  for (int i=1;i<=tLCBnfSet.size();i++)
	  {
		System.out.println("-----2:"+tLCBnfSet.get(i).getBnfLot());
		if (tLCBnfSet.get(i).getBnfLot()!=1)
		{
		  // @@错误处理
		  CError tError = new CError();
		  tError.moduleName = "PBnfBL";
		  tError.functionName = "checkdata";
		  tError.errorMessage = "受益级别"+tLCBnfSet.get(i).getBnfGrade()+"之和不等于一!";
		  this.mErrors.addOneError(tError);
		  return false;
		}
	  }
	}
    return flag;
  }

  /**
   * 准备需要保存的数据
   */
  private boolean prepareData()
  {
	System.out.println("start prepare Data....");
	String cOperate;
	LCBnfSet aLCBnfSet = new LCBnfSet();
    LCBnfSet bLCBnfSet = new LCBnfSet();

	//准备个人批改受益人的信息
	for (int i=1;i<=mLCBnfSet.size();i++)
	{
	  LCBnfSchema tLCBnfSchema = new LCBnfSchema();
	  tLCBnfSchema.setSchema(mLCBnfSet.get(i));
	  tLCBnfSchema.setBnfNo(i);
	  tLCBnfSchema.setModifyDate(PubFun.getCurrentDate());
	  tLCBnfSchema.setModifyTime(PubFun.getCurrentTime());
	  aLCBnfSet.add(tLCBnfSchema);
	  System.out.println("Bnflots : " + tLCBnfSchema.getBnfLot());
	}

	//查询原受益人记录
	LCBnfDB tLCBnfDB = new LCBnfDB();
	String tStr = "select * from lcbnf where polno = '" + mPolNo + "'";
    bLCBnfSet = tLCBnfDB.executeQuery(tStr) ;
	if(bLCBnfSet == null )
	{
	  // @@错误处理
		 CError tError = new CError();
		 tError.moduleName = "PBnfBL";
		 tError.functionName = "checkdata";
		 tError.errorMessage = "受益人记录查询失败";
		 this.mErrors.addOneError(tError);
		  return false;
	}

	mInputData.clear();
	mInputData.add(aLCBnfSet) ;
	if(bLCBnfSet.size()>0)
	mInputData.add(bLCBnfSet) ;
		return true;

  }


}