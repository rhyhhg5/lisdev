package com.sinosoft.lis.cbcheck;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
/**
 * <p>Title: Web业务系统承保个人人工核保部分</p>
 * <p>Description:接口功能类
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author WHN
 * @version 1.0
 */
public class UWManuNormChkUI
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  public UWManuNormChkUI() {}

// @Main
  public static void main(String[] args)
  {
    GlobalInput tG = new GlobalInput();
     tG.Operator = "001";
     tG.ComCode = "86000000";
     tG.ManageCom = "86000000";
     LCPolSchema p = new LCPolSchema();
     LCSpecSchema tLCSpecSchema = new LCSpecSchema();
     LCPremSchema tLCPremSchema = new LCPremSchema();
     LCUWMasterSchema tLCUWMasterSchema = new LCUWMasterSchema();

     p.setProposalNo( "86110020030110005578" );
     p.setPolNo("86110020030110005578");
     p.setRemark("测试a");
//  tLCSpecSchema.setSpecContent("测试特别约定");
//  tLCPremSchema.setDutyCode("201001");
//  tLCPremSchema.setPayStartDate("2002-10-18");
//  tLCPremSchema.setPayEndDate("2002-10-18");
//  tLCPremSchema.setPrem(900);
     tLCUWMasterSchema.setUWIdea("测试b");
     tLCUWMasterSchema.setPostponeDay("1");
     p.setUWFlag("1");

     LCPolSet tLCPolSet = new LCPolSet();
//  LCSpecSet tLCSpecSet = new LCSpecSet();
//  LCPremSet tLCPremSet = new LCPremSet();
     LCUWMasterSet tLCUWMasterSet = new LCUWMasterSet();

     tLCPolSet.add(p);
     //tLCSpecSet.add(tLCSpecSchema);
//  tLCPremSet.add(tLCPremSchema);

//  tLCPremSchema = new LCPremSchema();
//  tLCPremSchema.setDutyCode("205001");
//  tLCPremSchema.setPayStartDate("2002-12-18");
//  tLCPremSchema.setPayEndDate("2005-12-18");
//  tLCPremSchema.setPrem(111);

//  tLCPremSet.add(tLCPremSchema);
     tLCUWMasterSet.add(tLCUWMasterSchema);

     VData tVData = new VData();
     tVData.add( tLCPolSet );
//  tVData.add(tLCSpecSet);
//  tVData.add(tLCPremSet);
     tVData.add(tLCUWMasterSet);
     tVData.add( tG );

     UWManuNormChkUI ui = new UWManuNormChkUI();
     if( ui.submitData( tVData, "" ) == true )
         System.out.println("---ok---");
     else
      System.out.println("---NO---");
  }

  /**
  传输数据的公共方法
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;

    UWManuNormChkBL tUWManuNormChkBL = new UWManuNormChkBL();

    System.out.println("---UWManuNormChkBL UI BEGIN---");
    if (tUWManuNormChkBL.submitData(cInputData,mOperate) == false)
	{
  		// @@错误处理
      this.mErrors.copyAllErrors(tUWManuNormChkBL.mErrors);
      CError tError = new CError();
      tError.moduleName = "UWAutoChkUI";
      tError.functionName = "submitData";
      //tError.errorMessage = "数据查询失败!";
      this.mErrors .addOneError(tError) ;
      mResult.clear();
      return false;
	}

    return true;
  }

}