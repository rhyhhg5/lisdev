package com.sinosoft.lis.cbcheck;

import java.util.*;


import com.sinosoft.lis.db.*;
import com.sinosoft.lis.f1print.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;


/**
 * <p>Title: Web业务系统承保集体人工核保部分</p>
 * <p>Description: 逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author WHN
 * @modified by zhangrong
 * @version 1.0
 */
public class AskGrpUWManuNormChkBL
{
        /** 错误处理类，每个需要错误处理的类中都放置该类 */
        public CErrors mErrors = new CErrors();

        /** 往后面传输数据的容器 */
        private VData mInputData;

        /** 往界面传输数据的容器 */
        MMap mMap = new MMap();
        private VData mResult = new VData();

        /** 数据操作字符串 */
        private String mOperator;    //操作员
        private String mManageCom;
        private String mpassflag;    //通过标记
        private int merrcount;    //错误条数
        private String mCalCode;    //计算编码
        private String mUser;
        private FDate fDate = new FDate();
        private double mValue;
        private double mprem;
        private double mamnt;

        /** 业务处理相关变量 */
        private String mGrpContNo = "";
        private String mPolNo = "";
        private String mUWFlag = "";    //核保标志
        private Date mvalidate = null;
        private String mUWIdea = "";    //核保结论
        private int mpostday;    //延长天数
        private String mUWPopedom = "";    //操作员核保级别
        private String mAppGrade = "";    //上报级别
        private String mMainFlag = "";    //主险标记

        /**团体合同表*/
        private LCGrpContSet mLCGrpContSet = new LCGrpContSet();
        private LCGrpContSet mAllLCGrpContSet = new LCGrpContSet();
        private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();

        /**团体保单表*/
        private LCGrpPolSet mLCGrpPolSet = new LCGrpPolSet();
        private LCGrpPolSet mAllLCGrpPolSet = new LCGrpPolSet();
        private LCGrpPolSchema mLCGrpPolSchema = new LCGrpPolSchema();

        /**合同表*/
        private LCContSet mLCContSet = new LCContSet();
        private LCContSet mAllLCContSet = new LCContSet();
        private LCContSchema mLCContSchema = new LCContSchema();

        /**保单表*/
        private LCPolSet mLCPolSet = new LCPolSet();
        private LCPolSet mAllLCPolSet = new LCPolSet();
        private LCPolSchema mLCPolSchema = new LCPolSchema();

        /** 保费项表 */
        private LCPremSet mLCPremSet = new LCPremSet();
        private LCPremSet mAllLCPremSet = new LCPremSet();
        private LCPremSchema mLCPremSchema = new LCPremSchema();
        private LCPremSet mmLCPremSet = new LCPremSet();

        /** 领取项表 */
        private LCGetSet mLCGetSet = new LCGetSet();
        private LCGetSet mAllLCGetSet = new LCGetSet();
        private LCGetSchema mLCGetSchema = new LCGetSchema();

        /** 责任表 */
        private LCDutySet mLCDutySet = new LCDutySet();
        private LCDutySet mAllLCDutySet = new LCDutySet();
        private LCDutySchema mLCDutySchema = new LCDutySchema();

        /** 特别约定表 */
        private LCSpecSet mLCSpecSet = new LCSpecSet();
        private LCSpecSet mAllLCSpecSet = new LCSpecSet();
        private LCSpecSchema mLCSpecSchema = new LCSpecSchema();

        /** 特别约定注释表 */
        private LCSpecNoteSet mLCSpecNoteSet = new LCSpecNoteSet();
        private LCSpecNoteSet mAllLCSpecNoteSet = new LCSpecNoteSet();

        /** 核保主表 */
        private LCUWMasterSet mLCUWMasterSet = new LCUWMasterSet();
        private LCUWMasterSet mAllLCUWMasterSet = new LCUWMasterSet();
        private LCCUWMasterSet mLCCUWMasterSet = new LCCUWMasterSet();
        private LCCUWMasterSet mAllLCCUWMasterSet = new LCCUWMasterSet();
        private LCGUWMasterSet mLCGUWMasterSet = new LCGUWMasterSet();
        private LCGUWMasterSet mAllLCGUWMasterSet = new LCGUWMasterSet();
        private LCGCUWMasterSet mLCGCUWMasterSet = new LCGCUWMasterSet();
        private LCGCUWMasterSet mAllLCGCUWMasterSet = new LCGCUWMasterSet();

        /** 核保子表 */
        private LCUWSubSet mLCUWSubSet = new LCUWSubSet();
        private LCUWSubSet mAllLCUWSubSet = new LCUWSubSet();
        private LCCUWSubSet mLCCUWSubSet = new LCCUWSubSet();
        private LCCUWSubSet mAllLCCUWSubSet = new LCCUWSubSet();
        private LCGUWSubSet mLCGUWSubSet = new LCGUWSubSet();
        private LCGUWSubSet mAllLCGUWSubSet = new LCGUWSubSet();
        private LCGCUWSubSet mLCGCUWSubSet = new LCGCUWSubSet();
        private LCGCUWSubSet mAllLCGCUWSubSet = new LCGCUWSubSet();

        /** 核保错误信息表 */
        private LCCUWErrorSet mLCCUWErrorSet = new LCCUWErrorSet();
        private LCCUWErrorSet mAllLCErrSet = new LCCUWErrorSet();

        /** 团体核保错误信息表 */
        private LCGCUWErrorSet mLCGCUWErrorSet = new LCGCUWErrorSet();
        private LCGCUWErrorSet mAllLCGErrSet = new LCGCUWErrorSet();
        private LMUWSet mLMUWSet = new LMUWSet();
        private GlobalInput mGlobalInput = new GlobalInput();

        /**业务跟踪主表*/
        private LCAskTrackMainSchema mLCAskTrackMainSchema = new LCAskTrackMainSchema();

         /**打印管理表*/
         LOPRTManagerSet mLOPRTManagerSet = new LOPRTManagerSet();
        public AskGrpUWManuNormChkBL()
        {
        }

        /**
         * 传输数据的公共方法
         * @param: cInputData 输入的数据
         *         cOperate 数据操作
         * @return:
         */
        public boolean submitData(VData cInputData, String cOperate)
        {
                int flag = 0;    //判断是不是所有数据都不成功
                int j = 0;    //符合条件数据个数
                TransferData tTransferData = new TransferData();
                mResult.clear();

                //将操作数据拷贝到本类中
                mInputData = (VData)cInputData.clone();

                //得到外部传入的数据,将数据备份到本类中
                if (!getInputData(cInputData))
                {
                        tTransferData.setNameAndValue("FinishFlag", "0");
                        mResult.add(tTransferData);
                        return false;
                }

                System.out.println("---GrpUWManuNormChkBL getInputData OK---");

               /*
                if (checkFinished(mLCGrpContSchema))
                {
                        tTransferData.setNameAndValue("FinishFlag", "1");
                        mResult.add(tTransferData);
                        return true;
                }
               */
                // 校验数据
                /*
                if (!checkApprove(mLCGrpContSchema))
                {
                        tTransferData.setNameAndValue("FinishFlag", "0");
                        mResult.add(tTransferData);
                        return false;
                }
               */
                /*
                //判断是不是整单已经确认过
                if (!checkUWGrpPol(mLCGrpContSchema))
                {
                        tTransferData.setNameAndValue("FinishFlag", "0");
                        mResult.add(tTransferData);
                        return false;
                }

                //判断核保级别
                if (!checkUWGrade(mLCGrpContSchema))
                {
                        tTransferData.setNameAndValue("FinishFlag", "0");
                        mResult.add(tTransferData);
                        return false;
                }
                  */
                //判断个单是不是全部通过(当核保结论为正常通过或通融承保时,要确保该团体单下的所有个单均已通过核保)
                /*
                if (!mUWFlag.equals("1") && !mUWFlag.equals("6") && !mUWFlag.equals("a") && !mUWFlag.equals("7") && !mUWFlag.equals("8"))
                {
                        if (!checkUWPol(mLCGrpContSchema))
                        {
                                tTransferData.setNameAndValue("FinishFlag", "0");
                                mResult.add(tTransferData);
                                return false;
                        }
                }
                */

                if (!dealData())
                {
                        tTransferData.setNameAndValue("FinishFlag", "0");
                        mResult.add(tTransferData);
                        return false;
                }


                //commented by zhr 2004.11
                System.out.println("---GrpUWManuNormChkBL dealData---");

                //准备给后台的数据
                prepareOutputData();
                System.out.println("---GrpUWManuNormChkBL prepareOutputData---");

                //数据提交


                PubSubmit tSubmit = new PubSubmit();

                if (!tSubmit.submitData(mResult, ""))
                {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tSubmit.mErrors);

                        CError tError = new CError();
                        tError.moduleName = "GrpUWAutoChkBL";
                        tError.functionName = "submitData";
                        tError.errorMessage = "数据提交失败!";
                        this.mErrors.addOneError(tError);
                        tTransferData.setNameAndValue("FinishFlag", "0");
                        mResult.add(tTransferData);

                        return false;
                }

                System.out.println("---GrpUWManuNormChkBL commitData---");
                tTransferData.setNameAndValue("FinishFlag", "1");
                mResult.add(tTransferData);

                return true;
        }

        /**
         * 数据操作类业务处理
         * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
         */
        private boolean dealData()
        {
                if (dealOnePol() == false)
                {
                        return false;
                }
                if(preparetrack() == false)
               {
                 return false;
                }
               if(prepareprint() == false)
               {
                 return false;
               }


                return true;
        }

        /**
         * 操作一张保单的业务处理
         * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
         */
        private boolean dealOnePol()
        {
                mLCGrpContSchema.setUWFlag(mUWFlag);
                mLCGrpContSchema.setUWOperator(mOperator);
                mLCGrpContSchema.setUWDate(PubFun.getCurrentDate());
                mLCGrpContSchema.setUWTime(PubFun.getCurrentTime());
                mLCGrpContSchema.setOperator(mOperator);
                mLCGrpContSchema.setModifyDate(PubFun.getCurrentDate());
                mLCGrpContSchema.setModifyTime(PubFun.getCurrentTime());
                mLCGrpContSet.clear();
                mLCGrpContSet.add(mLCGrpContSchema);

                LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
                tLCGrpPolDB.setGrpContNo(mGrpContNo);

                LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.query();

                if ((tLCGrpPolSet == null) || (tLCGrpPolSet.size() <= 0))
                {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tLCGrpPolDB.mErrors);

                        CError tError = new CError();
                        tError.moduleName = "GrpUWManuNormChkBL";
                        tError.functionName = "dealOnePol";
                        tError.errorMessage = "集体险种保单查询失败!";
                        this.mErrors.addOneError(tError);

                        return false;
                }

                int n = tLCGrpPolSet.size();
                int i = 0;
                LCGrpPolSchema tLCGrpPolSchema;

                for (i = 1; i <= n; i++)
                {
                        tLCGrpPolSchema = tLCGrpPolSet.get(i);
                        tLCGrpPolSchema.setUWFlag(mUWFlag);
                        tLCGrpPolSchema.setUWOperator(mOperator);
                        tLCGrpPolSchema.setUWDate(PubFun.getCurrentDate());
                        tLCGrpPolSchema.setUWTime(PubFun.getCurrentTime());
                        tLCGrpPolSchema.setOperator(mOperator);
                        tLCGrpPolSchema.setModifyDate(PubFun.getCurrentDate());
                        tLCGrpPolSchema.setModifyTime(PubFun.getCurrentTime());
                        mLCGrpPolSet.add(tLCGrpPolSchema);
                }

                LCGCUWMasterSchema tLCGCUWMasterSchema = new LCGCUWMasterSchema();
                LCGCUWMasterDB tLCGCUWMasterDB = new LCGCUWMasterDB();
                tLCGCUWMasterDB.setGrpContNo(mGrpContNo);

                LCGCUWMasterSet tLCGCUWMasterSet = new LCGCUWMasterSet();
                tLCGCUWMasterSet = tLCGCUWMasterDB.query();

                if (tLCGCUWMasterDB.mErrors.needDealError())
                {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tLCGCUWMasterDB.mErrors);

                        CError tError = new CError();
                        tError.moduleName = "GrpUWManuNormChkBL";
                        tError.functionName = "prepareUW";
                        tError.errorMessage = "集体核保总表取数失败!";
                        this.mErrors.addOneError(tError);

                        return false;
                }

                n = tLCGCUWMasterSet.size();

                System.out.println("mastercount=" + n);

                if (n == 1)
                {
                        tLCGCUWMasterSchema = tLCGCUWMasterSet.get(1);
                        tLCGCUWMasterSchema.setPassFlag(mUWFlag);    //通过标志
                        tLCGCUWMasterSchema.setAutoUWFlag("2");    // 1 自动核保 2 人工核保
                        tLCGCUWMasterSchema.setUWGrade(mUWPopedom);
                        tLCGCUWMasterSchema.setAppGrade(mAppGrade);
                        tLCGCUWMasterSchema.setState(mUWFlag);
                        tLCGCUWMasterSchema.setUWIdea(mUWIdea);
                        tLCGCUWMasterSchema.setOperator(mOperator);    //操作员
                        tLCGCUWMasterSchema.setModifyDate(PubFun.getCurrentDate());
                        tLCGCUWMasterSchema.setModifyTime(PubFun.getCurrentTime());
                }
                else
                {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tLCGCUWMasterDB.mErrors);

                        CError tError = new CError();
                        tError.moduleName = "GrpUWManuNormChkBL";
                        tError.functionName = "prepareUW";
                        tError.errorMessage = "集体核保总表取数据不唯一!";
                        this.mErrors.addOneError(tError);

                        return false;
                }

                mLCGCUWMasterSet.clear();
                mLCGCUWMasterSet.add(tLCGCUWMasterSchema);

                // 核保轨迹表
                LCGCUWSubSchema tLCGCUWSubSchema = new LCGCUWSubSchema();
                LCGCUWSubDB tLCGCUWSubDB = new LCGCUWSubDB();
                tLCGCUWSubDB.setGrpContNo(mGrpContNo);

                LCGCUWSubSet tLCGCUWSubSet = new LCGCUWSubSet();
                tLCGCUWSubSet = tLCGCUWSubDB.query();

                if (tLCGCUWSubDB.mErrors.needDealError())
                {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tLCGCUWSubDB.mErrors);

                        CError tError = new CError();
                        tError.moduleName = "GrpUWManuNormChkBl";
                        tError.functionName = "prepareUW";
                        tError.errorMessage = "集体核保轨迹表查询失败!";
                        this.mErrors.addOneError(tError);

                        return false;
                }

                int m = tLCGCUWSubSet.size();

                if (m >= 0)
                {
                        m++;    //核保次数
                        tLCGCUWSubSchema = new LCGCUWSubSchema();    //tLCGCUWSubSet.get(1);

                        tLCGCUWSubSchema.setUWNo(m);    //第几次核保
                        tLCGCUWSubSchema.setGrpContNo(tLCGCUWMasterSchema.getGrpContNo());
                        tLCGCUWSubSchema.setProposalGrpContNo(tLCGCUWMasterSchema.getProposalGrpContNo());
                        tLCGCUWSubSchema.setAgentCode(tLCGCUWMasterSchema.getAgentCode());
                        tLCGCUWSubSchema.setAgentGroup(tLCGCUWMasterSchema.getAgentGroup());
                        tLCGCUWSubSchema.setUWGrade(tLCGCUWMasterSchema.getUWGrade());    //核保级别
                        tLCGCUWSubSchema.setAppGrade(tLCGCUWMasterSchema.getAppGrade());    //申请级别
                        tLCGCUWSubSchema.setAutoUWFlag(tLCGCUWMasterSchema.getAutoUWFlag());
                        tLCGCUWSubSchema.setState(tLCGCUWMasterSchema.getState());
                        tLCGCUWSubSchema.setPassFlag(tLCGCUWMasterSchema.getState());
                        tLCGCUWSubSchema.setPostponeDay(tLCGCUWMasterSchema.getPostponeDay());
                        tLCGCUWSubSchema.setPostponeDate(tLCGCUWMasterSchema.getPostponeDate());
                        tLCGCUWSubSchema.setUpReportContent(tLCGCUWMasterSchema.getUpReportContent());
                        tLCGCUWSubSchema.setHealthFlag(tLCGCUWMasterSchema.getHealthFlag());
                        tLCGCUWSubSchema.setSpecFlag(tLCGCUWMasterSchema.getSpecFlag());
                        tLCGCUWSubSchema.setSpecReason(tLCGCUWMasterSchema.getSpecReason());
                        tLCGCUWSubSchema.setQuesFlag(tLCGCUWMasterSchema.getQuesFlag());
                        tLCGCUWSubSchema.setReportFlag(tLCGCUWMasterSchema.getReportFlag());
                        tLCGCUWSubSchema.setChangePolFlag(tLCGCUWMasterSchema.getChangePolFlag());
                        tLCGCUWSubSchema.setChangePolReason(tLCGCUWMasterSchema.getChangePolReason());
                        tLCGCUWSubSchema.setAddPremReason(tLCGCUWMasterSchema.getAddPremReason());
                        tLCGCUWSubSchema.setPrintFlag(tLCGCUWMasterSchema.getPrintFlag());
                        tLCGCUWSubSchema.setPrintFlag2(tLCGCUWMasterSchema.getPrintFlag2());
                        tLCGCUWSubSchema.setUWIdea(tLCGCUWMasterSchema.getUWIdea());
                        tLCGCUWSubSchema.setOperator(tLCGCUWMasterSchema.getOperator());    //操作员
                        tLCGCUWSubSchema.setManageCom(tLCGCUWMasterSchema.getManageCom());
                        tLCGCUWSubSchema.setMakeDate(PubFun.getCurrentDate());
                        tLCGCUWSubSchema.setMakeTime(PubFun.getCurrentTime());
                        tLCGCUWSubSchema.setModifyDate(PubFun.getCurrentDate());
                        tLCGCUWSubSchema.setModifyTime(PubFun.getCurrentTime());
                }
                else
                {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tLCGCUWSubDB.mErrors);

                        CError tError = new CError();
                        tError.moduleName = "GrpUWManuNormChkBL";
                        tError.functionName = "prepareUW";
                        tError.errorMessage = "集体核保轨迹表查询失败!";
                        this.mErrors.addOneError(tError);

                        return false;
                }

                mLCGCUWSubSet.clear();
                mLCGCUWSubSet.add(tLCGCUWSubSchema);

                LCGUWMasterSchema tLCGUWMasterSchema = new LCGUWMasterSchema();
                LCGUWMasterDB tLCGUWMasterDB = new LCGUWMasterDB();
                tLCGUWMasterDB.setGrpContNo(mGrpContNo);

                LCGUWMasterSet tLCGUWMasterSet = new LCGUWMasterSet();
                tLCGUWMasterSet = tLCGUWMasterDB.query();

                if (tLCGUWMasterDB.mErrors.needDealError())
                {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tLCGUWMasterDB.mErrors);

                        CError tError = new CError();
                        tError.moduleName = "GrpUWManuNormChkBL";
                        tError.functionName = "prepareUW";
                        tError.errorMessage = "集体核保总表取数失败!";
                        this.mErrors.addOneError(tError);

                        return false;
                }

                n = tLCGUWMasterSet.size();
                System.out.println("mastercount=" + n);

                if (n <= 0)
                {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tLCGUWMasterDB.mErrors);

                        CError tError = new CError();
                        tError.moduleName = "GrpUWManuNormChkBL";
                        tError.functionName = "prepareUW";
                        tError.errorMessage = "集体核保总表取数据不唯一!";
                        this.mErrors.addOneError(tError);

                        return false;
                }

                mLCGUWMasterSet.clear();
                mLCGUWSubSet.clear();

                for (i = 1; i <= n; i++)
                {
                        tLCGUWMasterSchema = tLCGUWMasterSet.get(i);
                        tLCGUWMasterSchema.setPassFlag(mUWFlag);    //通过标志
                        tLCGUWMasterSchema.setAutoUWFlag("2");    // 1 自动核保 2 人工核保
                        tLCGUWMasterSchema.setUWGrade(mUWPopedom);
                        tLCGUWMasterSchema.setAppGrade(mAppGrade);
                        tLCGUWMasterSchema.setState(mUWFlag);
                        tLCGUWMasterSchema.setUWIdea(mUWIdea);
                        tLCGUWMasterSchema.setOperator(mOperator);    //操作员
                        tLCGUWMasterSchema.setModifyDate(PubFun.getCurrentDate());
                        tLCGUWMasterSchema.setModifyTime(PubFun.getCurrentTime());
                        mLCGUWMasterSet.add(tLCGUWMasterSchema);

                        // 核保轨迹表
                        LCGUWSubSchema tLCGUWSubSchema = new LCGUWSubSchema();
                        LCGUWSubDB tLCGUWSubDB = new LCGUWSubDB();
                        tLCGUWSubDB.setGrpPolNo(tLCGUWMasterSchema.getGrpPolNo());

                        LCGUWSubSet tLCGUWSubSet = new LCGUWSubSet();
                        tLCGUWSubSet = tLCGUWSubDB.query();

                        if (tLCGUWSubDB.mErrors.needDealError())
                        {
                                // @@错误处理
                                this.mErrors.copyAllErrors(tLCGUWSubDB.mErrors);

                                CError tError = new CError();
                                tError.moduleName = "GrpUWManuNormChkBl";
                                tError.functionName = "prepareUW";
                                tError.errorMessage = "集体核保轨迹表查询失败!";
                                this.mErrors.addOneError(tError);

                                return false;
                        }

                        m = tLCGUWSubSet.size();

                        if (m >= 0)
                        {
                                m++;    //核保次数
                                tLCGUWSubSchema = new LCGUWSubSchema();    //tLCGUWSubSet.get(1);

                                tLCGUWSubSchema.setUWNo(m);    //第几次核保
                                tLCGUWSubSchema.setGrpContNo(tLCGUWMasterSchema.getGrpContNo());
                                tLCGUWSubSchema.setGrpPolNo(tLCGUWMasterSchema.getGrpPolNo());
                                tLCGUWSubSchema.setProposalGrpContNo(tLCGUWMasterSchema.getProposalGrpContNo());
                                tLCGUWSubSchema.setGrpProposalNo(tLCGUWMasterSchema.getGrpProposalNo());
                                tLCGUWSubSchema.setAgentCode(tLCGUWMasterSchema.getAgentCode());
                                tLCGUWSubSchema.setAgentGroup(tLCGUWMasterSchema.getAgentGroup());
                                tLCGUWSubSchema.setUWGrade(tLCGUWMasterSchema.getUWGrade());    //核保级别
                                tLCGUWSubSchema.setAppGrade(tLCGUWMasterSchema.getAppGrade());    //申请级别
                                tLCGUWSubSchema.setAutoUWFlag(tLCGUWMasterSchema.getAutoUWFlag());
                                tLCGUWSubSchema.setState(tLCGUWMasterSchema.getState());
                                tLCGUWSubSchema.setPassFlag(tLCGUWMasterSchema.getState());
                                tLCGUWSubSchema.setPostponeDay(tLCGUWMasterSchema.getPostponeDay());
                                tLCGUWSubSchema.setPostponeDate(tLCGUWMasterSchema.getPostponeDate());
                                tLCGUWSubSchema.setUpReportContent(tLCGUWMasterSchema.getUpReportContent());
                                tLCGUWSubSchema.setHealthFlag(tLCGUWMasterSchema.getHealthFlag());
                                tLCGUWSubSchema.setSpecFlag(tLCGUWMasterSchema.getSpecFlag());
                                tLCGUWSubSchema.setSpecReason(tLCGUWMasterSchema.getSpecReason());
                                tLCGUWSubSchema.setQuesFlag(tLCGUWMasterSchema.getQuesFlag());
                                tLCGUWSubSchema.setReportFlag(tLCGUWMasterSchema.getReportFlag());
                                tLCGUWSubSchema.setChangePolFlag(tLCGUWMasterSchema.getChangePolFlag());
                                tLCGUWSubSchema.setChangePolReason(tLCGUWMasterSchema.getChangePolReason());
                                tLCGUWSubSchema.setAddPremReason(tLCGUWMasterSchema.getAddPremReason());
                                tLCGUWSubSchema.setPrintFlag(tLCGUWMasterSchema.getPrintFlag());
                                tLCGUWSubSchema.setPrintFlag2(tLCGUWMasterSchema.getPrintFlag2());
                                tLCGUWSubSchema.setUWIdea(tLCGUWMasterSchema.getUWIdea());
                                tLCGUWSubSchema.setOperator(tLCGUWMasterSchema.getOperator());    //操作员
                                tLCGUWSubSchema.setManageCom(tLCGUWMasterSchema.getManageCom());
                                tLCGUWSubSchema.setMakeDate(PubFun.getCurrentDate());
                                tLCGUWSubSchema.setMakeTime(PubFun.getCurrentTime());
                                tLCGUWSubSchema.setModifyDate(PubFun.getCurrentDate());
                                tLCGUWSubSchema.setModifyTime(PubFun.getCurrentTime());
                        }
                        else
                        {
                                // @@错误处理
                                this.mErrors.copyAllErrors(tLCGUWSubDB.mErrors);

                                CError tError = new CError();
                                tError.moduleName = "GrpUWManuNormChkBL";
                                tError.functionName = "prepareUW";
                                tError.errorMessage = "集体核保轨迹表查询失败!";
                                this.mErrors.addOneError(tError);

                                return false;
                        }

                        mLCGUWSubSet.add(tLCGUWSubSchema);
                }

                //上级核保
                if (mUWFlag.equals("6"))
                {
                        uplevel();
                }

                if (mUWFlag.equals("1") || mUWFlag.equals("6") || mUWFlag.equals("a"))
                {
                        // 保单
                        if (preparePol() == false)
                        {
                                return false;
                        }

                        // 核保信息
                        if (prepareUW() == false)
                        {
                                return false;
                        }
                }


                LCPolSet tLCPolSet = new LCPolSet();
                tLCPolSet.set(mLCPolSet);
                mAllLCPolSet.add(tLCPolSet);

                LCContSet tLCContSet = new LCContSet();
                tLCContSet.set(mLCContSet);
                mAllLCContSet.add(tLCContSet);

                LCGrpPolSet tLCGrpPolSet1 = new LCGrpPolSet();
                tLCGrpPolSet1.set(mLCGrpPolSet);
                mAllLCGrpPolSet.add(tLCGrpPolSet1);

                LCGrpContSet tLCGrpContSet = new LCGrpContSet();
                tLCGrpContSet.set(mLCGrpContSet);
                mAllLCGrpContSet.add(tLCGrpContSet);

                LCUWMasterSet tLCUWMasterSet = new LCUWMasterSet();
                tLCUWMasterSet.set(mLCUWMasterSet);
                mAllLCUWMasterSet.add(tLCUWMasterSet);

                LCCUWMasterSet tLCCUWMasterSet = new LCCUWMasterSet();
                tLCCUWMasterSet.set(mLCCUWMasterSet);
                mAllLCCUWMasterSet.add(tLCCUWMasterSet);

                LCGUWMasterSet tLCGUWMasterSet1 = new LCGUWMasterSet();
                tLCGUWMasterSet1.set(mLCGUWMasterSet);
                mAllLCGUWMasterSet.add(tLCGUWMasterSet1);

                LCGCUWMasterSet tLCGCUWMasterSet1 = new LCGCUWMasterSet();
                tLCGCUWMasterSet1.set(mLCGCUWMasterSet);
                mAllLCGCUWMasterSet.add(tLCGCUWMasterSet1);

                LCUWSubSet tLCUWSubSet = new LCUWSubSet();
                tLCUWSubSet.set(mLCUWSubSet);
                mAllLCUWSubSet.add(tLCUWSubSet);

                LCCUWSubSet tLCCUWSubSet = new LCCUWSubSet();
                tLCCUWSubSet.set(mLCCUWSubSet);
                mAllLCCUWSubSet.add(tLCCUWSubSet);

                LCGUWSubSet tLCGUWSubSet1 = new LCGUWSubSet();
                tLCGUWSubSet1.set(mLCGUWSubSet);
                mAllLCGUWSubSet.add(tLCGUWSubSet1);

                LCGCUWSubSet tLCGCUWSubSet1 = new LCGCUWSubSet();
                tLCGCUWSubSet1.set(mLCGCUWSubSet);
                mAllLCGCUWSubSet.add(tLCGCUWSubSet1);

                return true;
        }

        /**
         * 从输入数据中得到所有对象
         * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
         */
        private boolean getInputData(VData cInputData)
        {
                String getflag = "true";    //判断承保条件是否接受
                GlobalInput tGlobalInput = new GlobalInput();
                mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput", 0));
                mOperator = mGlobalInput.Operator;
                mManageCom = mGlobalInput.ManageCom;

                //取投保单
                mLCGrpContSet.set((LCGrpContSet)cInputData.getObjectByObjectName("LCGrpContSet", 0));

                int n = mLCGrpContSet.size();

                if (n == 1)
                {
                        LCGrpContSchema tLCGrpContSchema = mLCGrpContSet.get(1);
                        LCGrpContDB tLCGrpContDB = new LCGrpContDB();

                        mGrpContNo = tLCGrpContSchema.getGrpContNo();
                        mUWIdea = tLCGrpContSchema.getRemark();
                        mUWFlag = tLCGrpContSchema.getUWFlag();
                        System.out.println("muwflag=" + mUWFlag);

                        //校验是不是以下核保结论
                        if (mUWFlag == null)
                        {
                                // @@错误处理
                                CError tError = new CError();
                                tError.moduleName = "GrpUWManuNormChkBL";
                                tError.functionName = "getInputData";
                                tError.errorMessage = "没有选择核保结论";
                                this.mErrors.addOneError(tError);

                                return false;
                        }

                        if (mUWFlag.equals(""))
                        {
                                // @@错误处理
                                CError tError = new CError();
                                tError.moduleName = "GrpUWManuNormChkBL";
                                tError.functionName = "getInputData";
                                tError.errorMessage = "没有选择核保结论";
                                this.mErrors.addOneError(tError);

                                return false;
                        }

                        tLCGrpContDB.setGrpContNo(mGrpContNo);

                        if (tLCGrpContDB.getInfo() == false)
                        {
                                // @@错误处理
                                this.mErrors.copyAllErrors(tLCGrpContDB.mErrors);

                                CError tError = new CError();
                                tError.moduleName = "GrpUWManuNormChkBL";
                                tError.functionName = "getInputData";
                                tError.errorMessage = mGrpContNo + "集体合同单查询失败!";
                                this.mErrors.addOneError(tError);

                                return false;
                        }
                        else
                        {
                                tLCGrpContSchema.setSchema(tLCGrpContDB);
                                mLCGrpContSchema.setSchema(tLCGrpContDB);
                        }

                        mLCGCUWMasterSet.set((LCGCUWMasterSet)cInputData.getObjectByObjectName("LCGCUWMasterSet", 0));
                        n = mLCGCUWMasterSet.size();

                        if (n == 1)
                        {
                                LCGCUWMasterSchema tLCGCUWMasterSchema = mLCGCUWMasterSet.get(1);
                                LCGCUWMasterDB tLCGCUWMasterDB = new LCGCUWMasterDB();
                                tLCGCUWMasterDB.setGrpContNo(tLCGrpContSchema.getGrpContNo());
                                tLCGCUWMasterDB.setProposalGrpContNo(tLCGrpContSchema.getGrpContNo());
                                System.out.println("--BL--Master--" + tLCGrpContSchema.getGrpContNo());

                                if (tLCGCUWMasterDB.getInfo() == false)
                                {
                                        // @@错误处理
                                        this.mErrors.copyAllErrors(tLCGCUWMasterDB.mErrors);

                                        CError tError = new CError();
                                        tError.moduleName = "GrpUWManuNormChkBL";
                                        tError.functionName = "getInputData";
                                        tError.errorMessage = mGrpContNo + "集体核保总表查询失败!";
                                        this.mErrors.addOneError(tError);

                                        return false;
                                }
                        }
                        else
                        {
                                return false;
                        }
                }
                else
                {
                        return false;
                }

                return true;
        }
/*
        private boolean checkFinished(LCGrpContSchema tLCGrpContSchema)
        {
                if (!"5".equals(tLCGrpContSchema.getUWFlag()))
                {
                        return true;
                }
                return false;
        }
*/
        /**
         * 校验投保单是否复核
         * 输出：如果发生错误则返回false,否则返回true
         */
        private boolean checkApprove(LCGrpContSchema tLCGrpContSchema)
        {
                if (!tLCGrpContSchema.getApproveFlag().equals("9"))
                {
                        // @@错误处理
                        CError tError = new CError();
                        tError.moduleName = "GrpUWManuNormChkBL";
                        tError.functionName = "checkApprove";
                        tError.errorMessage = "投保单尚未进行复核操作，不能核保!（投保单号：" + tLCGrpContSchema.getGrpContNo().trim() + "）";
                        this.mErrors.addOneError(tError);

                        return false;
                }

                return true;
        }

        /**
         * 校验核保员级别
         * 输出：如果发生错误则返回false,否则返回true
         */
        private boolean checkUWGrade(LCGrpContSchema tLCGrpContSchema)
        {
                LDUserDB tLDUserDB = new LDUserDB();
                tLDUserDB.setUserCode(mOperator);

                if (!tLDUserDB.getInfo())
                {
                        CError tError = new CError();
                        tError.moduleName = "GrpUWManuNormChkBL";
                        tError.functionName = "checkUWGrade";
                        tError.errorMessage = "无此操作员信息，不能核保!（操作员：" + mOperator + "）";
                        this.mErrors.addOneError(tError);

                        return false;
                }

                String tUWPopedom = tLDUserDB.getUWPopedom();
                mUWPopedom = tLDUserDB.getUWPopedom();
                mAppGrade = mUWPopedom;

                if (tUWPopedom.equals(""))
                {
                        CError tError = new CError();
                        tError.moduleName = "GrpUWManuNormChkBL";
                        tError.functionName = "checkUWGrade";
                        tError.errorMessage = "操作员无核保权限，不能核保!（操作员：" + mOperator + "）";
                        this.mErrors.addOneError(tError);

                        return false;
                }

                LCGCUWMasterDB tLCGCUWMasterDB = new LCGCUWMasterDB();
                tLCGCUWMasterDB.setProposalGrpContNo(mGrpContNo);

                if (!tLCGCUWMasterDB.getInfo())
                {
                        CError tError = new CError();
                        tError.moduleName = "GrpUWManuNormChkBL";
                        tError.functionName = "checkUWGrade";
                        tError.errorMessage = "没有核保信息，不能核保!（操作员：" + mOperator + "）";
                        this.mErrors.addOneError(tError);

                        return false;
                }
                else
                {
                        String tappgrade = tLCGCUWMasterDB.getAppGrade();

                        if (tUWPopedom.compareTo(tappgrade) < 0)
                        {
                                CError tError = new CError();
                                tError.moduleName = "GrpUWManuNormChkBL";
                                tError.functionName = "checkUWGrade";
                                tError.errorMessage = "已经提交上级核保，不能核保!（操作员：" + mOperator + "）";
                                this.mErrors.addOneError(tError);

                                return false;
                        }
                }

                return true;
        }

        /**
         * 校验团体保单下个单是不是全部通过核保
         * 输出：如果发生错误则返回false,否则返回true
         */
        /*
        private boolean checkUWPol(LCGrpContSchema tLCGrpContSchema)
        {
                LCCUWMasterDB tLCCUWMasterDB = new LCCUWMasterDB();
                tLCCUWMasterDB.setGrpContNo(tLCGrpContSchema.getGrpContNo());

                LCCUWMasterSet tLCCUWMasterSet = new LCCUWMasterSet();
                tLCCUWMasterSet = tLCCUWMasterDB.query();

                int n = tLCCUWMasterSet.size();

                if (n > 0)
                {
                        for (int i = 1; i <= n; i++)
                        {
                                LCCUWMasterSchema tLCCUWMasterSchema = new LCCUWMasterSchema();
                                tLCCUWMasterSchema = tLCCUWMasterSet.get(i);

                                String tUWFlag = tLCCUWMasterSchema.getPassFlag();

                                if (tUWFlag.equals("0") || tUWFlag.equals("2") || tUWFlag.equals("5") || tUWFlag.equals("6") || tUWFlag.equals("8"))
                                {
                                        CError tError = new CError();
                                        tError.moduleName = "GrpUWManuNormChkBL";
                                        tError.functionName = "checkUWPol";
                                        tError.errorMessage = "集体单下有未通过核保的个人投保单!";
                                        this.mErrors.addOneError(tError);

                                        return false;
                                }
                        }
                }

                return true;
        }
      */
        /**
         * 校验团体保单下个单是不是全部通过核保
         * 输出：如果发生错误则返回false,否则返回true
         */
        private boolean checkUWGrpPol(LCGrpContSchema tLCGrpContSchema)
        {
                LCGCUWMasterDB tLCGCUWMasterDB = new LCGCUWMasterDB();
                tLCGCUWMasterDB.setGrpContNo(mGrpContNo);

                LCGCUWMasterSet tLCGCUWMasterSet = new LCGCUWMasterSet();
                tLCGCUWMasterSet = tLCGCUWMasterDB.query();

                int n = tLCGCUWMasterSet.size();

                if (n > 0)
                {
                        for (int i = 1; i <= n; i++)
                        {
                                LCGCUWMasterSchema tLCGCUWMasterSchema = new LCGCUWMasterSchema();
                                tLCGCUWMasterSchema = tLCGCUWMasterSet.get(i);

                                String tUWFlag = tLCGCUWMasterSchema.getPassFlag();

                                if (tUWFlag.equals("4") || tUWFlag.equals("9"))
                                {
                                        CError tError = new CError();
                                        tError.moduleName = "GrpUWManuNormChkBL";
                                        tError.functionName = "checkUWGrpPol";
                                        tError.errorMessage = "已经整单确认!";
                                        this.mErrors.addOneError(tError);

                                        return false;
                                }
                        }
                }

                return true;
        }





        /**
         * 准备团体保单信息
         * 输出：如果发生错误则返回false,否则返回true
         */
        private boolean preparePol()
        {

                if (mUWFlag.equals("1") || mUWFlag.equals("6") || mUWFlag.equals("a") || mUWFlag.equals("7") || mUWFlag.equals("8"))
                {
                        preparePPol();
                }

                return true;
        }

        /**
         * 准备核保信息
         * 输出：如果发生错误则返回false,否则返回true
         */
        private boolean prepareUW()
        {
                LCCUWMasterSchema tLCCUWMasterSchema = new LCCUWMasterSchema();
                LCCUWMasterDB tLCCUWMasterDB = new LCCUWMasterDB();
                tLCCUWMasterDB.setGrpContNo(mGrpContNo);

                LCCUWMasterSet tLCCUWMasterSet = new LCCUWMasterSet();
                tLCCUWMasterSet = tLCCUWMasterDB.query();

                if (tLCCUWMasterDB.mErrors.needDealError())
                {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tLCCUWMasterDB.mErrors);

                        CError tError = new CError();
                        tError.moduleName = "GrpUWManuNormChkBL";
                        tError.functionName = "prepareUW";
                        tError.errorMessage = "核保总表取数失败!";
                        this.mErrors.addOneError(tError);

                        return false;
                }

                int n = tLCCUWMasterSet.size();
                int i = 0;
                System.out.println("mastercount=" + n);

                if (n <= 0)
                {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tLCCUWMasterDB.mErrors);

                        CError tError = new CError();
                        tError.moduleName = "GrpUWManuNormChkBL";
                        tError.functionName = "prepareUW";
                        tError.errorMessage = "合同单核保总表取数据失败!";
                        this.mErrors.addOneError(tError);

                        return false;
                }

                for (i = 1; i <= n; i++)
                {
                        tLCCUWMasterSchema = tLCCUWMasterSet.get(i);
                        tLCCUWMasterSchema.setPassFlag(mUWFlag);    //通过标志
                        tLCCUWMasterSchema.setAutoUWFlag("2");    // 1 自动核保 2 人工核保
                        tLCCUWMasterSchema.setUWGrade(mUWPopedom);
                        tLCCUWMasterSchema.setAppGrade(mAppGrade);
                        tLCCUWMasterSchema.setState(mUWFlag);
                        tLCCUWMasterSchema.setUWIdea(mUWIdea);
                        tLCCUWMasterSchema.setOperator(mOperator);    //操作员
                        tLCCUWMasterSchema.setModifyDate(PubFun.getCurrentDate());
                        tLCCUWMasterSchema.setModifyTime(PubFun.getCurrentTime());

                        mLCCUWMasterSet.clear();
                        mLCCUWMasterSet.add(tLCCUWMasterSchema);

                        // 核保轨迹表
                        LCCUWSubSchema tLCCUWSubSchema = new LCCUWSubSchema();
                        LCCUWSubDB tLCCUWSubDB = new LCCUWSubDB();
                        tLCCUWSubDB.setContNo(tLCCUWMasterSchema.getContNo());

                        LCCUWSubSet tLCCUWSubSet = new LCCUWSubSet();
                        tLCCUWSubSet = tLCCUWSubDB.query();

                        if (tLCCUWSubDB.mErrors.needDealError())
                        {
                                // @@错误处理
                                this.mErrors.copyAllErrors(tLCCUWSubDB.mErrors);

                                CError tError = new CError();
                                tError.moduleName = "GrpUWManuNormChkBl";
                                tError.functionName = "prepareUW";
                                tError.errorMessage = "核保轨迹表查询失败!";
                                this.mErrors.addOneError(tError);

                                return false;
                        }

                        int m = tLCCUWSubSet.size();

                        if (m >= 0)
                        {
                                m++;    //核保次数
                                tLCCUWSubSchema = new LCCUWSubSchema();    //tLCCUWSubSet.get(1);

                                tLCCUWSubSchema.setUWNo(m);    //第几次核保
                                tLCCUWSubSchema.setContNo(tLCCUWMasterSchema.getContNo());
                                tLCCUWSubSchema.setGrpContNo(tLCCUWMasterSchema.getGrpContNo());
                                tLCCUWSubSchema.setProposalContNo(tLCCUWMasterSchema.getProposalContNo());
                                tLCCUWSubSchema.setInsuredNo(tLCCUWMasterSchema.getInsuredNo());
                                tLCCUWSubSchema.setInsuredName(tLCCUWMasterSchema.getInsuredName());
                                tLCCUWSubSchema.setAppntNo(tLCCUWMasterSchema.getAppntNo());
                                tLCCUWSubSchema.setAppntName(tLCCUWMasterSchema.getAppntName());
                                tLCCUWSubSchema.setAgentCode(tLCCUWMasterSchema.getAgentCode());
                                tLCCUWSubSchema.setAgentGroup(tLCCUWMasterSchema.getAgentGroup());
                                tLCCUWSubSchema.setUWGrade(tLCCUWMasterSchema.getUWGrade());    //核保级别
                                tLCCUWSubSchema.setAppGrade(tLCCUWMasterSchema.getAppGrade());    //申请级别
                                tLCCUWSubSchema.setAutoUWFlag(tLCCUWMasterSchema.getAutoUWFlag());
                                tLCCUWSubSchema.setState(tLCCUWMasterSchema.getState());
                                tLCCUWSubSchema.setPassFlag(tLCCUWMasterSchema.getState());
                                tLCCUWSubSchema.setPostponeDay(tLCCUWMasterSchema.getPostponeDay());
                                tLCCUWSubSchema.setPostponeDate(tLCCUWMasterSchema.getPostponeDate());
                                tLCCUWSubSchema.setUpReportContent(tLCCUWMasterSchema.getUpReportContent());
                                tLCCUWSubSchema.setHealthFlag(tLCCUWMasterSchema.getHealthFlag());
                                tLCCUWSubSchema.setSpecFlag(tLCCUWMasterSchema.getSpecFlag());
                                tLCCUWSubSchema.setSpecReason(tLCCUWMasterSchema.getSpecReason());
                                tLCCUWSubSchema.setQuesFlag(tLCCUWMasterSchema.getQuesFlag());
                                tLCCUWSubSchema.setReportFlag(tLCCUWMasterSchema.getReportFlag());
                                tLCCUWSubSchema.setChangePolFlag(tLCCUWMasterSchema.getChangePolFlag());
                                tLCCUWSubSchema.setChangePolReason(tLCCUWMasterSchema.getChangePolReason());
                                tLCCUWSubSchema.setAddPremReason(tLCCUWMasterSchema.getAddPremReason());
                                tLCCUWSubSchema.setPrintFlag(tLCCUWMasterSchema.getPrintFlag());
                                tLCCUWSubSchema.setPrintFlag2(tLCCUWMasterSchema.getPrintFlag2());
                                tLCCUWSubSchema.setUWIdea(tLCCUWMasterSchema.getUWIdea());
                                tLCCUWSubSchema.setOperator(tLCCUWMasterSchema.getOperator());    //操作员
                                tLCCUWSubSchema.setManageCom(tLCCUWMasterSchema.getManageCom());
                                tLCCUWSubSchema.setMakeDate(PubFun.getCurrentDate());
                                tLCCUWSubSchema.setMakeTime(PubFun.getCurrentTime());
                                tLCCUWSubSchema.setModifyDate(PubFun.getCurrentDate());
                                tLCCUWSubSchema.setModifyTime(PubFun.getCurrentTime());
                        }
                        else
                        {
                                // @@错误处理
                                this.mErrors.copyAllErrors(tLCCUWSubDB.mErrors);

                                CError tError = new CError();
                                tError.moduleName = "GrpUWManuNormChkBL";
                                tError.functionName = "prepareUW";
                                tError.errorMessage = "核保轨迹表查询失败!";
                                this.mErrors.addOneError(tError);

                                return false;
                        }

                        mLCCUWSubSet.clear();
                        mLCCUWSubSet.add(tLCCUWSubSchema);
                }

                LCUWMasterSchema tLCUWMasterSchema = new LCUWMasterSchema();
                LCUWMasterDB tLCUWMasterDB = new LCUWMasterDB();
                tLCUWMasterDB.setGrpContNo(mGrpContNo);

                LCUWMasterSet tLCUWMasterSet = new LCUWMasterSet();
                tLCUWMasterSet = tLCUWMasterDB.query();

                if (tLCUWMasterDB.mErrors.needDealError())
                {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tLCUWMasterDB.mErrors);

                        CError tError = new CError();
                        tError.moduleName = "GrpUWManuNormChkBL";
                        tError.functionName = "prepareUW";
                        tError.errorMessage = "核保总表取数失败!";
                        this.mErrors.addOneError(tError);

                        return false;
                }

                n = tLCUWMasterSet.size();
                i = 0;
                System.out.println("mastercount=" + n);

                if (n <= 0)
                {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tLCUWMasterDB.mErrors);

                        CError tError = new CError();
                        tError.moduleName = "GrpUWManuNormChkBL";
                        tError.functionName = "prepareUW";
                        tError.errorMessage = "核保总表取数据不唯一!";
                        this.mErrors.addOneError(tError);

                        return false;
                }

                mLCUWMasterSet.clear();
                mLCUWSubSet.clear();

                for (i = 1; i <= n; i++)
                {
                        tLCUWMasterSchema = tLCUWMasterSet.get(i);
                        tLCUWMasterSchema.setPassFlag(mUWFlag);    //通过标志
                        tLCUWMasterSchema.setAutoUWFlag("2");    // 1 自动核保 2 人工核保
                        tLCUWMasterSchema.setUWGrade(mUWPopedom);
                        tLCUWMasterSchema.setAppGrade(mAppGrade);
                        tLCUWMasterSchema.setState(mUWFlag);
                        tLCUWMasterSchema.setUWIdea(mUWIdea);
                        tLCUWMasterSchema.setOperator(mOperator);    //操作员
                        tLCUWMasterSchema.setModifyDate(PubFun.getCurrentDate());
                        tLCUWMasterSchema.setModifyTime(PubFun.getCurrentTime());
                        mLCUWMasterSet.add(tLCUWMasterSchema);

                        // 核保轨迹表
                        LCUWSubSchema tLCUWSubSchema = new LCUWSubSchema();
                        LCUWSubDB tLCUWSubDB = new LCUWSubDB();
                        tLCUWSubDB.setPolNo(tLCUWMasterSchema.getPolNo());

                        LCUWSubSet tLCUWSubSet = new LCUWSubSet();
                        tLCUWSubSet = tLCUWSubDB.query();

                        if (tLCUWSubDB.mErrors.needDealError())
                        {
                                // @@错误处理
                                this.mErrors.copyAllErrors(tLCUWSubDB.mErrors);

                                CError tError = new CError();
                                tError.moduleName = "GrpUWManuNormChkBl";
                                tError.functionName = "prepareUW";
                                tError.errorMessage = "核保轨迹表查询失败!";
                                this.mErrors.addOneError(tError);

                                return false;
                        }

                        int m = tLCUWSubSet.size();

                        if (m >= 0)
                        {
                                m++;    //核保次数
                                tLCUWSubSchema = new LCUWSubSchema();    //tLCUWSubSet.get(1);

                                tLCUWSubSchema.setUWNo(m);    //第几次核保
                                tLCUWSubSchema.setContNo(tLCUWMasterSchema.getContNo());
                                tLCUWSubSchema.setPolNo(tLCUWMasterSchema.getPolNo());
                                tLCUWSubSchema.setGrpContNo(tLCUWMasterSchema.getGrpContNo());
                                tLCUWSubSchema.setProposalContNo(tLCUWMasterSchema.getProposalContNo());
                                tLCUWSubSchema.setProposalNo(tLCUWMasterSchema.getProposalNo());
                                tLCUWSubSchema.setInsuredNo(tLCUWMasterSchema.getInsuredNo());
                                tLCUWSubSchema.setInsuredName(tLCUWMasterSchema.getInsuredName());
                                tLCUWSubSchema.setAppntNo(tLCUWMasterSchema.getAppntNo());
                                tLCUWSubSchema.setAppntName(tLCUWMasterSchema.getAppntName());
                                tLCUWSubSchema.setAgentCode(tLCUWMasterSchema.getAgentCode());
                                tLCUWSubSchema.setAgentGroup(tLCUWMasterSchema.getAgentGroup());
                                tLCUWSubSchema.setUWGrade(tLCUWMasterSchema.getUWGrade());    //核保级别
                                tLCUWSubSchema.setAppGrade(tLCUWMasterSchema.getAppGrade());    //申请级别
                                tLCUWSubSchema.setAutoUWFlag(tLCUWMasterSchema.getAutoUWFlag());
                                tLCUWSubSchema.setState(tLCUWMasterSchema.getState());
                                tLCUWSubSchema.setPassFlag(tLCUWMasterSchema.getState());
                                tLCUWSubSchema.setPostponeDay(tLCUWMasterSchema.getPostponeDay());
                                tLCUWSubSchema.setPostponeDate(tLCUWMasterSchema.getPostponeDate());
                                tLCUWSubSchema.setUpReportContent(tLCUWMasterSchema.getUpReportContent());
                                tLCUWSubSchema.setHealthFlag(tLCUWMasterSchema.getHealthFlag());
                                tLCUWSubSchema.setSpecFlag(tLCUWMasterSchema.getSpecFlag());
                                tLCUWSubSchema.setSpecReason(tLCUWMasterSchema.getSpecReason());
                                tLCUWSubSchema.setQuesFlag(tLCUWMasterSchema.getQuesFlag());
                                tLCUWSubSchema.setReportFlag(tLCUWMasterSchema.getReportFlag());
                                tLCUWSubSchema.setChangePolFlag(tLCUWMasterSchema.getChangePolFlag());
                                tLCUWSubSchema.setChangePolReason(tLCUWMasterSchema.getChangePolReason());
                                tLCUWSubSchema.setAddPremReason(tLCUWMasterSchema.getAddPremReason());
                                tLCUWSubSchema.setPrintFlag(tLCUWMasterSchema.getPrintFlag());
                                tLCUWSubSchema.setPrintFlag2(tLCUWMasterSchema.getPrintFlag2());
                                tLCUWSubSchema.setUWIdea(tLCUWMasterSchema.getUWIdea());
                                tLCUWSubSchema.setOperator(tLCUWMasterSchema.getOperator());    //操作员
                                tLCUWSubSchema.setManageCom(tLCUWMasterSchema.getManageCom());
                                tLCUWSubSchema.setMakeDate(PubFun.getCurrentDate());
                                tLCUWSubSchema.setMakeTime(PubFun.getCurrentTime());
                                tLCUWSubSchema.setModifyDate(PubFun.getCurrentDate());
                                tLCUWSubSchema.setModifyTime(PubFun.getCurrentTime());
                        }
                        else
                        {
                                // @@错误处理
                                this.mErrors.copyAllErrors(tLCUWSubDB.mErrors);

                                CError tError = new CError();
                                tError.moduleName = "GrpUWManuNormChkBL";
                                tError.functionName = "prepareUW";
                                tError.errorMessage = "核保轨迹表查询失败!";
                                this.mErrors.addOneError(tError);

                                return false;
                        }

                        mLCUWSubSet.add(tLCUWSubSchema);
                }

                return true;
        }

        /**
         * 个人保单
         * @return
         */
        private boolean preparePPol()
        {
                LCContDB tLCContDB = new LCContDB();
                LCContSet tLCContSet = new LCContSet();

                tLCContDB.setGrpContNo(mGrpContNo);
                tLCContSet = tLCContDB.query();

                int n = 0;
                int i = 0;

                if (tLCContSet != null)
                {
                        n = tLCContSet.size();

                        LCContSchema tLCContSchema = null;

                        for (i = 1; i <= n; i++)
                        {
                                tLCContSchema = tLCContSet.get(i);
                                tLCContSchema.setUWFlag(mUWFlag);
                                tLCContSchema.setUWOperator(mOperator);
                                tLCContSchema.setUWDate(PubFun.getCurrentDate());
                                tLCContSchema.setUWTime(PubFun.getCurrentTime());
                                tLCContSchema.setOperator(mOperator);
                                tLCContSchema.setModifyDate(PubFun.getCurrentDate());
                                tLCContSchema.setModifyTime(PubFun.getCurrentTime());
                                mLCContSet.add(tLCContSchema);
                        }
                }

                LCPolDB tLCPolDB = new LCPolDB();
                LCPolSet tLCPolSet = new LCPolSet();
                tLCPolDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
                tLCPolSet = tLCPolDB.query();

                if (tLCPolSet.size() > 0)
                {
                        n = tLCPolSet.size();

                        LCPolSchema tLCPolSchema = null;

                        for (i = 1; i <= n; i++)
                        {
                                tLCPolSchema = tLCPolSet.get(i);
                                tLCPolSchema.setUWFlag(mUWFlag);
                                tLCPolSchema.setUWCode(mOperator);
                                tLCPolSchema.setUWDate(PubFun.getCurrentDate());
                                tLCPolSchema.setUWTime(PubFun.getCurrentTime());
                                tLCPolSchema.setOperator(mOperator);
                                tLCPolSchema.setModifyDate(PubFun.getCurrentDate());
                                tLCPolSchema.setModifyTime(PubFun.getCurrentTime());
                                mLCPolSet.add(tLCPolSchema);
                        }
                }

                return true;
        }



        /**
         * 打印信息表
         * @return
         */
        private boolean prepareprint()
        {
                LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
                String strNoLimit = PubFun.getNoLimit( mGlobalInput.ComCode );
                String tPrtSeq = PubFun1.CreateMaxNo("PRTSEQNO", strNoLimit);
                tLOPRTManagerSchema.setPrtSeq(tPrtSeq);
                tLOPRTManagerSchema.setOtherNo(mLCGrpContSchema.getGrpContNo());
                tLOPRTManagerSchema.setManageCom(mLCGrpContSchema.getManageCom());
                tLOPRTManagerSchema.setPrtType(PrintManagerBL.PT_FRONT);
                switch(mUWFlag.charAt(0))
                {
                  case '1':
                    tLOPRTManagerSchema.setCode(PrintManagerBL.ASK_GRP_DECLINE);
                    break;
                  case '6':
                      tLOPRTManagerSchema.setCode(PrintManagerBL.ASK_GRP_DEFER);
                      break;
                  case 'a':
                    tLOPRTManagerSchema.setCode(PrintManagerBL.Ask_GRP_WITHDRAW);
                    break;
                  case '4':
                  case '9':
                    tLOPRTManagerSchema.setCode(PrintManagerBL.ASk_GRP_SUCESS);
                    break;

                }

                tLOPRTManagerSchema.setAgentCode(mLCGrpContSchema.getAgentCode());
                tLOPRTManagerSchema.setOtherNoType(PrintManagerBL.ONT_GRPPOL);
                tLOPRTManagerSchema.setReqOperator(mLCGrpContSchema.getOperator());
                tLOPRTManagerSchema.setReqCom(mLCGrpContSchema.getManageCom());
                tLOPRTManagerSchema.setExeCom(mLCGrpContSchema.getManageCom());
                tLOPRTManagerSchema.setStateFlag("0");



               mLOPRTManagerSet.add(tLOPRTManagerSchema);



                return true;
        }
       private boolean preparetrack()
       {
         mLCAskTrackMainSchema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
         mLCAskTrackMainSchema.setProposalContNo(mLCGrpContSchema.getProposalGrpContNo());
         mLCAskTrackMainSchema.setState("0");
         mLCAskTrackMainSchema.setAgentCode(mLCGrpContSchema.getAgentCode());
         mLCAskTrackMainSchema.setAgentGroup(mLCGrpContSchema.getAgentGroup());
         mLCAskTrackMainSchema.setAppntNo(mLCGrpContSchema.getAppntNo());
         mLCAskTrackMainSchema.setOperator(mLCGrpContSchema.getOperator());
         mLCAskTrackMainSchema.setManageCom(mLCGrpContSchema.getManageCom());
         mLCAskTrackMainSchema.setTrackStartDate(PubFun.getCurrentDate());
         mLCAskTrackMainSchema.setTrackStartTime(PubFun.getCurrentTime());
         mLCAskTrackMainSchema.setMakeDate(PubFun.getCurrentDate());
         mLCAskTrackMainSchema.setMakeTime(PubFun.getCurrentTime());
         mLCAskTrackMainSchema.setModifyDate(PubFun.getCurrentDate());
         mLCAskTrackMainSchema.setModifyTime(PubFun.getCurrentTime());



         return true;

       }
        /**
         * 待上级核保
         * 输出：如果发生错误则返回false,否则返回true
         */
        private void uplevel()
        {
                LCGCUWErrorDB tLCGCUWErrorDB = new LCGCUWErrorDB();
                LCGCUWMasterDB tLCGCUWMasterDB = new LCGCUWMasterDB();
                LCGCUWMasterSchema tLCGCUWMasterSchema = new LCGCUWMasterSchema();

                tLCGCUWErrorDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
                tLCGCUWMasterDB.setProposalGrpContNo(mLCGrpContSchema.getGrpContNo());

                if (tLCGCUWMasterDB.getInfo() == true)
                {
                        tLCGCUWMasterSchema = tLCGCUWMasterDB.getSchema();
                }

                String tcurrgrade = "";

                if (tLCGCUWMasterSchema.getAppGrade() == null)
                {
                        tcurrgrade = "A";
                }
                else
                {
                        tcurrgrade = tLCGCUWMasterSchema.getAppGrade();
                }

                String tpolno = mLCGrpContSchema.getGrpContNo();
                String tsql = "select * from LCGCUWerror where GrpContNo = '" + tpolno.trim() + "' and uwno = (select max(uwno) from LCGCUWerror where GrpContNo = '" + tpolno.trim() + "')";
                LCGCUWErrorSet tLCGCUWErrorSet = tLCGCUWErrorDB.executeQuery(tsql);

                int errno = tLCGCUWErrorSet.size();

                if (errno > 0)
                {
                        for (int i = 1; i <= errno; i++)
                        {
                                LCGCUWErrorSchema tLCGCUWErrorSchema = new LCGCUWErrorSchema();
                                tLCGCUWErrorSchema = tLCGCUWErrorSet.get(i);

                                String terrgrade = tLCGCUWErrorSchema.getUWGrade();

                                if (terrgrade.compareTo(tcurrgrade) > 0)
                                {
                                        tcurrgrade = terrgrade;
                                }
                        }
                }

                mAppGrade = tcurrgrade;

                //与当前核保员级别校验
                if (((mUWPopedom.compareTo(mAppGrade) >= 0) && (mUWPopedom.compareTo("L") < 0)))
                {
                        char[] temp;
                        char tempgrade;
                        temp = mUWPopedom.toCharArray();
                        tempgrade = (char)((int)temp[0] + 1);
                        System.out.println("上报级别:" + tempgrade);
                        mAppGrade = String.valueOf(tempgrade);
                }
        }

        /**
         * 准备需要保存的数据
         */
        private void prepareOutputData()
        {
                mMap.put(mAllLCPolSet, "UPDATE");
                mMap.put(mAllLCContSet, "UPDATE");
                mMap.put(mAllLCGrpContSet, "UPDATE");
                mMap.put(mAllLCGrpPolSet, "UPDATE");
                mMap.put(mAllLCUWMasterSet, "DELETE&INSERT");
                mMap.put(mAllLCUWSubSet, "INSERT");
                mMap.put(mAllLCCUWMasterSet, "DELETE&INSERT");
                mMap.put(mAllLCCUWSubSet, "INSERT");
                mMap.put(mAllLCGUWMasterSet, "DELETE&INSERT");
                mMap.put(mAllLCGUWSubSet, "INSERT");
                mMap.put(mAllLCGCUWMasterSet, "DELETE&INSERT");
                mMap.put(mAllLCGCUWSubSet, "INSERT");
                if(mLCAskTrackMainSchema!=null)
                {
                mMap.put(mLCAskTrackMainSchema, "INSERT");
                }
                mMap.put(mLOPRTManagerSet, "INSERT");
                mResult.add(mMap);
        }

        public VData getResult()
        {
                return mResult;
        }
}
