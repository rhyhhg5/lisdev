package com.sinosoft.lis.cbcheck;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Description:被保险人资料变更功能类
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @version 1.0
 */
public class UWModifyTypeBDUI
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  public UWModifyTypeBDUI() {}

  /**
  传输数据的公共方法
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
	//将操作数据拷贝到本类中
	this.mOperate = cOperate;
	UWModifyTypeBDBL tUWModifyTypeBDBL = new UWModifyTypeBDBL();
	System.out.println("---UI BEGIN---"+mOperate);
	if (tUWModifyTypeBDBL.submitData(cInputData,mOperate) == false)
	{
	  // @@错误处理
	  this.mErrors.copyAllErrors(tUWModifyTypeBDBL.mErrors);
	  return false;
	}
	else
	  mResult = tUWModifyTypeBDBL.getResult();
	return true;
  }

  public VData getResult()
  {
	return mResult;
  }

  public static void main(String[] args)
  {
	VData tInputData=new VData();
	GlobalInput tGlobalInput = new GlobalInput();
	UWModifyTypeBDUI aUWModifyTypeBDUI = new UWModifyTypeBDUI();
	LCBnfSet tLPBnfSet = new LCBnfSet();
	LCBnfSchema tLPBnfSchema = new LCBnfSchema();
	tGlobalInput.ManageCom="86";
	tGlobalInput.Operator="001";


	tInputData.addElement("86110020030110002064");
	tInputData.addElement("2");
	aUWModifyTypeBDUI.submitData(tInputData,"INSERT||MAIN");

	System.out.println("-------test...");
  }
}