package com.sinosoft.lis.cbcheck;

import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.f1print.PrintManagerBL;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.lis.vschema.LOPRTManagerSet;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.LCGrpRReportItemSchema;
import com.sinosoft.lis.db.LCGrpRReportItemDB;
import com.sinosoft.lis.vschema.LCGrpRReportItemSet;

public class GrpUWSendRReportBL {
    public GrpUWSendRReportBL() {}

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private MMap map = new MMap();

    /** 往界面传输数据的容器 */
    private VData mInputData = new VData();

    /** 数据操作字符串 */
    private String mManageCom;

    /** 业务处理相关变量 */

    private String mContNo = "";
    private GlobalInput mGlobalInput = new GlobalInput();


    /**打印队列表**/
    private LCGrpContSet tLCGrpContSet = new LCGrpContSet();
    private LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
    private LOPRTManagerSet mLOPRTManagerSet = new LOPRTManagerSet();
    private LCGrpRReportItemSchema tLCGrpRReportItemSchema = new  LCGrpRReportItemSchema();

    /**打印处理函数**/
    private PrintManagerBL tPrintManagerBL = new PrintManagerBL();


    /**
     * submitData
     *
     * @param cInputData VData
     * @param mOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String mOperate) {
        //将操作数据拷贝到本类中
        System.out.println("---1---");
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;
        System.out.println("---UWSendPrintBL getInputData---");

        //校验
        if (!checkPrint())
            return false;
        System.out.println("---UWSendPrintBL checkData---");
        // 数据操作业务处理
        if (!dealData())
            return false;

        System.out.println("---UWSendPrintBL dealData---");
        //准备给后台的数据
        if (!prepareOutputData())
            return false;

        PubSubmit tPubSubmit = new PubSubmit();

        if (!tPubSubmit.submitData(this.mInputData, "INSERT")) {
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }

        System.out.println("---UWSendPrintBL prepareOutputData---");

        return false;
    }

    /**
     * prepareOutputData
     *
     * @return boolean
     */
    private boolean prepareOutputData() {
        this.mInputData.add(map);
        return true;
    }

    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData() {
        if (dealOnePol() == false)
            return false;
        return true;
    }

    /**
     * checkPrint
     *
     * @return boolean
     */
    private boolean checkPrint() {
        /**
         * 填充数据
         */
        if (this.tLCGrpContSet == null) {
            System.out.println(
                    "GrpUWSendRReportBL.checkPrint()  \n--Line:118  --Author:YangMing");
            CError tError = new CError();
            tError.moduleName = "GrpUWSendRReportBL";
            tError.functionName = "checkPrint";
            tError.errorMessage = "传入的合同信息为空!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (this.tLCGrpContSet.size() == 0) {
            System.out.println(
                    "GrpUWSendRReportBL.checkPrint()  \n--Line:128  --Author:YangMing");
            CError tError = new CError();
            tError.moduleName = "GrpUWSendRReportBL";
            tError.functionName = "checkPrint";
            tError.errorMessage = "传入的合同信息为空!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (this.tLCGrpContSet.size() > 1) {
            System.out.println(
                    "GrpUWSendRReportBL.checkPrint()  \n--Line:138  --Author:YangMing");
            CError tError = new CError();
            tError.moduleName = "GrpUWSendRReportBL";
            tError.functionName = "checkPrint";
            tError.errorMessage = "传入的合同信息缺乏唯一性!";
            this.mErrors.addOneError(tError);
            return false;
        }
        this.tLCGrpContSchema = tLCGrpContSet.get(1).getSchema();
        if (tLCGrpContSchema == null) {
            System.out.println(
                    "GrpUWSendRReportBL.checkPrint()  \n--Line:149  --Author:YangMing");
            CError tError = new CError();
            tError.moduleName = "GrpUWSendRReportBL";
            tError.functionName = "checkPrint";
            tError.errorMessage = "传入的合同信息为空!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (tLCGrpContSchema.getGrpContNo().equals("")) {
            System.out.println(
                    "GrpUWSendRReportBL.checkPrint()  \n--Line:159  --Author:YangMing");
            CError tError = new CError();
            tError.moduleName = "GrpUWSendRReportBL";
            tError.functionName = "checkPrint";
            tError.errorMessage = "传后台的合同号码为空!";
            this.mErrors.addOneError(tError);
            return false;
        }
        LCGrpContDB tLCGrpContDB = tLCGrpContSchema.getDB();
        if (!tLCGrpContDB.getInfo()) {
            System.out.println(
                    "GrpUWSendRReportBL.checkPrint()  \n--Line:170  --Author:YangMing");
            CError tError = new CError();
            tError.moduleName = "GrpUWSendRReportBL";
            tError.functionName = "checkPrint";
            tError.errorMessage = "查询合同信息失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        tLCGrpContSchema = tLCGrpContDB.getSchema();
        /**
         * 准备打印对列表相关信息
         */
        this.mLOPRTManagerSchema.setCode("74");
        this.mLOPRTManagerSchema.setOtherNo(this.tLCGrpContSchema.getGrpContNo());
        this.mLOPRTManagerSchema.setOtherNoType("01");

        //生成流水号
//        String tsql = "select distinct 1 from LOPRTManager where otherno = '" +
//                      mContNo + "' and code = '" +
//                      mLOPRTManagerSchema.getCode().trim() +
//                      "' and stateflag = '0' and prtseq = (select max(prtseq) from LOPRTManager where otherno = '" +
//                      mContNo + "' and code = '" +
//                      mLOPRTManagerSchema.getCode().trim() + "')";
//        System.out.println(tsql);
//
//        ExeSQL tExeSQL = new ExeSQL();
//        String tflag = tExeSQL.getOneValue(tsql);
//
//        if (tflag.trim().equals("1")) {
//            CError tError = new CError();
//            tError.moduleName = "UWSendPrintBL";
//            tError.functionName = "checkPrint";
//            tError.errorMessage = "已在打印队列尚未打印!";
//            this.mErrors.addOneError(tError);
//            return false;
//        }

//        if (mLOPRTManagerSchema.getCode().equals("15")) {
//            tsql = "select distinct 1 from LOPRTManager where otherno = '" +
//                   mContNo +
//                   "' and code = '" + mLOPRTManagerSchema.getCode().trim() +
//                   "'";
//            System.out.println(tsql);
//
//            tExeSQL = new ExeSQL();
//            tflag = tExeSQL.getOneValue(tsql);
//            System.out.println("tflag" + tflag);
//            if (tflag.trim().equals("1")) {
//                System.out.println("tflag" + tflag);
//                CError tError = new CError();
//                tError.moduleName = "UWSendRReportBL";
//                tError.functionName = "checkPrint";
//                tError.errorMessage = "在签单处已发缴费催办通知书!";
//                this.mErrors.addOneError(tError);
//                return false;
//            }
//        }

        return true;
    }

    /**
     * getInputData
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        this.tLCGrpContSet.set((LCGrpContSet) cInputData.getObjectByObjectName(
                "LCGrpContSet", 0));
          this.tLCGrpRReportItemSchema.setSchema((LCGrpRReportItemSchema) cInputData.getObjectByObjectName(
                "LCGrpRReportItemSchema", 0));
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mManageCom = mGlobalInput.ManageCom;


        return true;
    }

    /**
     * 操作一张保单的业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     *
     * @return boolean
     */
    private boolean dealOnePol() {
        // 健康信息
        if (preparePrint() == false)
            return false;
        return true;
    }

    /**
     * 准备打印信息 输出：如果发生错误则返回false,否则返回true
     *
     * @return boolean
     */
    private boolean preparePrint() {

        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(mLOPRTManagerSchema.getOtherNo());
        if (tLCGrpContDB.getInfo() == false) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCGrpContDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "GrpUWSendPrintBL";
            tError.functionName = "preparePrint";
            tError.errorMessage = "没有保单信息!";
            this.mErrors.addOneError(tError);
            return false;
        }

        LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
        tLCGrpContSchema = tLCGrpContDB.getSchema();

        mLOPRTManagerSchema.setAgentCode(tLCGrpContSchema.getAgentCode());
        mLOPRTManagerSchema.setPrtType(PrintManagerBL.PT_FRONT);
        mLOPRTManagerSchema.setManageCom(tLCGrpContSchema.getManageCom());
        mLOPRTManagerSchema.setReqCom(this.mManageCom);
        mLOPRTManagerSchema.setReqOperator(this.mGlobalInput.Operator);
        //mLOPRTManagerSchema.setPrtType(PrintManagerBL.PT_FRONT);
        mLOPRTManagerSchema.setStateFlag("0");
        mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());


        //String Sql="select PrtSeq from LCGrpRReportItem where GrpContNo='tLCGrpRReportItemSchema'";

        LCGrpRReportItemDB tLCGrpRReportItemDB = new  LCGrpRReportItemDB();
        tLCGrpRReportItemDB.setGrpContNo(this.tLCGrpRReportItemSchema.getGrpContNo() );
        if (tLCGrpRReportItemDB.query()  == null) {
           // @@错误处理
           this.mErrors.copyAllErrors(tLCGrpRReportItemDB.mErrors);
           CError tError = new CError();
           tError.moduleName = "GrpUWSendPrintBL";
           tError.functionName = "preparePrint";
           tError.errorMessage = "没有保单信息!";
           this.mErrors.addOneError(tError);
           return false;
       }
       LCGrpRReportItemSet tLCGrpRReportItemSet =new LCGrpRReportItemSet();
       tLCGrpRReportItemSet.set(tLCGrpRReportItemDB.query());
        LCGrpRReportItemSchema tLCGrpRReportItemSchema = new  LCGrpRReportItemSchema();
        tLCGrpRReportItemSchema = tLCGrpRReportItemSet.get(1) ;

        mLOPRTManagerSchema.setPrtSeq(tLCGrpRReportItemSchema.getPrtSeq());
        System.out.println("PrtSeq : " + tLCGrpRReportItemSchema.getPrtSeq());
        map.put(mLOPRTManagerSchema, "INSERT");
        map.put("update Lwmission set activitystatus='2',"
                + "ModifyDate='" +PubFun.getCurrentDate() +
                "',ModifyTime='" +PubFun.getCurrentTime() +
                "' where Missionprop1='" +
                tLCGrpContSet.get(1).getGrpContNo() + "' and activityid='0000002004'  "
        , "UPDATE");

        return true;
    }
}
