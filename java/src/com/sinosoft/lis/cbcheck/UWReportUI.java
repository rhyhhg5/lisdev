package com.sinosoft.lis.cbcheck;

import oracle.jdbc.driver.*;

import oracle.sql.BLOB;

import java.io.*;
import java.sql.*;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;


/**
 * <p>Title: Web业务系统核保报告书查询录入部分</p>
 * <p>Description:接口功能类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author WHN
 * @modified ZhangRong
 * @version 1.0
 */
public class UWReportUI
{
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 往界面传输数据的容器 */
	private VData mResult = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	public UWReportUI()
	{
	}

	/**
	传输数据的公共方法
	*/
	public boolean submitData(VData cInputData, String cOperate)
	{
		//将操作数据拷贝到本类中
		this.mOperate = cOperate;

		UWReportBL tUWReportBL = new UWReportBL();

		System.out.println("---UWReportUI BEGIN---");

		if (tUWReportBL.submitData(cInputData, mOperate) == false)
		{
			// @@错误处理
			this.mErrors.copyAllErrors(tUWReportBL.mErrors);

			CError tError = new CError();
			tError.moduleName = "UWReportUI";
			tError.functionName = "submitData";
			tError.errorMessage = "数据查询失败!";
			this.mErrors.addOneError(tError);
			mResult.clear();

			return false;
		}

		mResult.clear();
		mResult = tUWReportBL.getResult();

		return true;
	}

	/*这个函数是往界面传返回值用的
	  */
	public VData getResult()
	{
		return mResult;
	}

	// @Main
	public static void main(String[] args) throws Exception
	{
		String content = "test content";

		LCUWReportSchema tLCUWReportSchema = new LCUWReportSchema();
		tLCUWReportSchema.setContNo("00000000000000000006");
		tLCUWReportSchema.setUWOperator("001");

		GlobalInput tGlobalInput = new GlobalInput();
		tGlobalInput.ManageCom = "86110000";
		tGlobalInput.Operator = "001";

		VData tVData = new VData();
		tVData.add(tLCUWReportSchema);
		tVData.add(tGlobalInput);
		tVData.add(content);

		UWReportUI tUWReportUI = new UWReportUI();
		tUWReportUI.submitData(tVData, "INSERT");
	}
}
