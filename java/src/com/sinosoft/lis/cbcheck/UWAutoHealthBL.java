package com.sinosoft.lis.cbcheck;

import java.lang.*;
import java.util.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.cbcheck.*;
import com.sinosoft.lis.f1print.*;

/**
 * <p>Title: Web业务系统体检资料录入部分</p>
 * <p>Description: 逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author WHN
 * @version 1.0
 */
public class UWAutoHealthBL
{
        /** 错误处理类，每个需要错误处理的类中都放置该类 */
        public  CErrors mErrors = new CErrors();
        /** 往后面传输数据的容器 */
        private VData mInputData ;
        /** 往界面传输数据的容器 */
        private VData mResult = new VData();
        /** 数据操作字符串 */
        private String mOperate;
        private String mManageCom;
        private String mpassflag; //通过标记
        private int merrcount; //错误条数

        private FDate fDate = new FDate();

        private String mInsuredNo = "";

        /** 业务处理相关变量 */
        private LCContSet mLCContSet = new LCContSet();

        private LCContSchema mLCContSchema = new LCContSchema();
        private String mPolNo = "";
        private String mOldPolNo = "";


        /** 核保主表 */
        private LCCUWMasterSet mLCUWMasterSet = new LCCUWMasterSet();
        private LCCUWMasterSet mAllLCCUWMasterSet = new LCCUWMasterSet();
        private LCCUWMasterSchema mLCCUWMasterSchema = new LCCUWMasterSchema();




        /** 体检资料主表 */
        private LCPENoticeSet mLCPENoticeSet = new LCPENoticeSet();
        private LCPENoticeSet mAllLCPENoticeSet = new LCPENoticeSet();

        private LCPENoticeSchema mmLCPENoticeSchema = new LCPENoticeSchema();
        /** 体检资料项目表 */
        private LCPENoticeItemSet mLCPENoticeItemSet = new LCPENoticeItemSet();
        private LCPENoticeItemSet mmLCPENoticeItemSet = new LCPENoticeItemSet();
        private LCPENoticeItemSet mAllLCPENoticeItemSet = new LCPENoticeItemSet();
        /** 打印管理表 */
        private LOPRTManagerSet mLOPRTManagerSet = new LOPRTManagerSet();
        private LOPRTManagerSet mAllLOPRTManagerSet = new LOPRTManagerSet();

        private GlobalInput mGlobalInput = new GlobalInput();

        public UWAutoHealthBL() {}
        String mPrtSeq;

        /**
        * 传输数据的公共方法
        * @param: cInputData 输入的数据
        *         cOperate 数据操作
        * @return:
        */
        public boolean submitData(VData cInputData,String cOperate)
        {
                int flag = 0; //判断是不是所有数据都不成功
                int j = 0; //符合条件数据个数

                //将操作数据拷贝到本类中
                mInputData = (VData)cInputData.clone();


                System.out.println("---1---");
                   //得到外部传入的数据,将数据备份到本类中
                    if (!getInputData(cInputData))
                        return false;
                System.out.println("---UWAutoHealthBL getInputData---");

                LCContSchema tLCContSchema = new LCContSchema();
                //tLCContSchema = ( LCContSchema )mLCContSet.get( 1 );
                mmLCPENoticeSchema = mLCPENoticeSet.get(1);
                String tProposalNo = mmLCPENoticeSchema.getProposalContNo();
                mOldPolNo = mmLCPENoticeSchema.getProposalContNo();
                mPolNo = mmLCPENoticeSchema.getProposalContNo();
                mInsuredNo = mmLCPENoticeSchema.getCustomerNo()
                             ;

                // 校验数据是否打印
                if (!checkPrint())
                  return false;

                System.out.println("---UWAutoHealthBL checkData---");
                // 数据操作业务处理
                if (!dealData(tLCContSchema))
                  //continue;
                  return false;
                else
                {
                  flag = 1;
                  j++;

                }


                if (flag == 0)
                {
                  CError tError = new CError();
                  tError.moduleName = "UWAutoHealthBL";
                  tError.functionName = "submitData";
                  tError.errorMessage = "没有自动通过保单!";
                  this.mErrors .addOneError(tError) ;
                  return false;
                }


                System.out.println("---UWAutoHealthBL dealData---");
                    //准备给后台的数据
                    prepareOutputData();

                System.out.println("---UWAutoHealthBL prepareOutputData---");
                //数据提交
                PubSubmit tSubmit = new PubSubmit();
                if (!tSubmit.submitData(mResult, ""))
                {
                  // @@错误处理
                  this.mErrors.copyAllErrors(tSubmit.mErrors);
                  CError tError = new CError();
                  tError.moduleName = "UWManuAddChkBL";
                  tError.functionName = "submitData";
                  tError.errorMessage = "数据提交失败!";
                  this.mErrors .addOneError(tError) ;
                  return false;
                }
                System.out.println("UWManuAddChkBL Submit OK!");
                return true;
        }

        /**
        * 数据操作类业务处理
        * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
        */
        private boolean dealData(LCContSchema tLCContSchema)
        {

                if (dealOnePol() == false)
                        return false;

                return true;
        }

        /**
        * 操作一张保单的业务处理
        * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
        */
        private boolean dealOnePol()
        {

                // 健康信息
                          if (prepareHealth() == false)
                        return false;

            //打印队列
            if (print() == false)
                  return false;


                LCPENoticeSet tLCPENoticeSet = new LCPENoticeSet();
                tLCPENoticeSet.set( mLCPENoticeSet );
                mAllLCPENoticeSet.add( tLCPENoticeSet );

                LCPENoticeItemSet tLCPENoticeItemSet = new LCPENoticeItemSet();
                tLCPENoticeItemSet.set( mLCPENoticeItemSet );
                mAllLCPENoticeItemSet.add( tLCPENoticeItemSet );

                return true;
        }

        /**
        * 从输入数据中得到所有对象
        *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
        */
        private boolean getInputData(VData cInputData)
        {
            GlobalInput tGlobalInput = new GlobalInput();

                tGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));

                mOperate = tGlobalInput.Operator;
                mManageCom = tGlobalInput.ManageCom;
               System.out.println(mManageCom);

                mLCPENoticeSet.set((LCPENoticeSet)cInputData.getObjectByObjectName("LCPENoticeSet",0));
                mmLCPENoticeItemSet.set((LCPENoticeItemSet)cInputData.getObjectByObjectName("LCPENoticeItemSet",0));
                mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));

                int flag = 0; //怕判断是不是所有保单都失败
                int j = 0; //符合条件保单个数


                if (1 > 0)
                {
                    LCContDB tLCContDB = new LCContDB();

                        //取被保人客户号
                        LCPENoticeSchema tLCPENoticeSchema = new LCPENoticeSchema();
                        tLCPENoticeSchema = mLCPENoticeSet.get(1);
                        mInsuredNo = tLCPENoticeSchema.getCustomerNo();

                        tLCContDB.setContNo( tLCPENoticeSchema.getContNo());
                        System.out.println("--BL--Pol--"+tLCPENoticeSchema.getContNo());
                        String temp = tLCPENoticeSchema.getContNo();
                        System.out.println("temp"+temp);
                        mLCContSet = tLCContDB.query();
                        if (mLCContSet.size()== 0)
                        {
                                // @@错误处理
                                this.mErrors.copyAllErrors( tLCContDB.mErrors );
                                CError tError = new CError();
                                tError.moduleName = "UWAutoHealthBL";
                                tError.functionName = "getInputData";
                                tError.errorMessage = "投保单查询失败!";
                                this.mErrors .addOneError(tError) ;
                                return false;
                        }
                        else
                        {
                                mLCContSchema = ( LCContSchema )mLCContSet.get( 1 );
                                flag = 1;
                                j++;

                        }

                }

                if (flag == 0)
                {
                        return false;
                }
                else
                {
                        return true;
                }
        }

        /**
         * 校验是否打印
         * @return
         */
        private boolean checkPrint()
        {
          LCPENoticeDB tLCPENoticeDB = new LCPENoticeDB();
          tLCPENoticeDB.setContNo(mPolNo);
          tLCPENoticeDB.setCustomerNo(mInsuredNo);
          LCPENoticeSet tLCPENoticeSet = tLCPENoticeDB.query();
          if( tLCPENoticeSet.size()==0)
          {
          }
          else
          {
            if(tLCPENoticeSet.get(1).getPrintFlag().equals("Y"))
            {
              CError tError = new CError();
              tError.moduleName = "UWAutoHealthBL";
              tError.functionName = "checkPrint";
              tError.errorMessage = "体检通知已经录入尚未打印，不能录入新体检资料!";
              this.mErrors .addOneError(tError) ;
              return false;
            }
          }

          return true;
        }

                /**
                 * 打印信息表
                 * @return
                 */
                private boolean print()
                {

                  // 处于未打印状态的通知书在打印队列中只能有一个
                  // 条件：同一个单据类型，同一个其它号码，同一个其它号码类型
                  LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();

                  tLOPRTManagerDB.setCode( PrintManagerBL.CODE_PE);//体检
                  tLOPRTManagerDB.setOtherNo( mPolNo );
                  tLOPRTManagerDB.setOtherNoType( PrintManagerBL.ONT_INDPOL );//保单号
                  tLOPRTManagerDB.setStandbyFlag1(mInsuredNo);
                  tLOPRTManagerDB.setStateFlag("0");

                  LOPRTManagerSet tLOPRTManagerSet = tLOPRTManagerDB.query();
                  if( tLOPRTManagerSet == null )
                  {
                        // @@错误处理
                        CError tError = new CError();
                        tError.moduleName = "UWAutoHealthBL";
                        tError.functionName = "preparePrint";
                        tError.errorMessage = "查询打印管理表信息出错!";
                        this.mErrors.addOneError(tError) ;
                        return false;
                  }

                  if( tLOPRTManagerSet.size() != 0 )
                  {
                        // @@错误处理
                        CError tError = new CError();
                        tError.moduleName = "PEdorUWAutoHealthAfterInitService";
                        tError.functionName = "preparePrint";
                        tError.errorMessage = "在打印队列中已有一个处于未打印状态的体检通知书!";
                        this.mErrors.addOneError(tError) ;
                        return false;
                  }

                  //准备打印管理表数据
                  LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
                  System.out.println(mGlobalInput.ComCode);


                  System.out.println("mPrtSeq="+mPrtSeq);
                  mLOPRTManagerSchema.setPrtSeq(mPrtSeq);
                  mLOPRTManagerSchema.setOtherNo(mPolNo);

                  mLOPRTManagerSchema.setStandbyFlag3(mLCContSchema.getGrpContNo());//modify by zhangxing
                  System.out.println("StandbyFlag3="+mLOPRTManagerSchema.getStandbyFlag3());
                  mLOPRTManagerSchema.setOtherNoType(PrintManagerBL.ONT_INDPOL );//保单号
                  mLOPRTManagerSchema.setCode(PrintManagerBL.CODE_PE);//体检
                  mLOPRTManagerSchema.setManageCom(mLCContSchema.getManageCom());
                  mLOPRTManagerSchema.setAgentCode(mLCContSchema.getAgentCode());
                  mLOPRTManagerSchema.setReqCom( mGlobalInput.ComCode );
                  mLOPRTManagerSchema.setReqOperator( mGlobalInput.Operator );

                  mLOPRTManagerSchema.setPrtType(PrintManagerBL.PT_FRONT);//前台打印
                  mLOPRTManagerSchema.setStateFlag("0");
                  mLOPRTManagerSchema.setPatchFlag("0") ;
                  mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
                  mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());

                  mLOPRTManagerSchema.setStandbyFlag1(mInsuredNo);//被保险人编码

                  mLOPRTManagerSet.add(mLOPRTManagerSchema);
                  return true;
        }

        /**
        * 准备体检资料信息
        * 输出：如果发生错误则返回false,否则返回true
        */
        private boolean prepareHealth()
        {
                int tuwno = 0;
                //取险种名称


                //取代理人姓名
                LAAgentDB tLAAgentDB = new LAAgentDB();
                tLAAgentDB.setAgentCode(mLCContSchema.getAgentCode());
                if(!tLAAgentDB.getInfo())
                {
                  // @@错误处理
                  CError tError = new CError();
                  tError.moduleName = "UWAutoHealthBL";
                  tError.functionName = "prepareHealth";
                  tError.errorMessage = "取业务员姓名失败!";
                  this.mErrors .addOneError(tError) ;
                  return false;
                }

                LCPENoticeSchema tLCPENoticeSchema = new LCPENoticeSchema();
                tLCPENoticeSchema = mLCPENoticeSet.get(1);
                mInsuredNo = tLCPENoticeSchema.getCustomerNo();



                String strNoLimit = PubFun.getNoLimit( mGlobalInput.ComCode );
                String tPrtSeq = PubFun1.CreateMaxNo("PRTSEQNO", strNoLimit);
                  mPrtSeq = tPrtSeq;
                tLCPENoticeSchema.setPrtSeq(mPrtSeq);
                System.out.println("mPrtSeq=" + mPrtSeq);
                //tLCPENoticeSchema.setRiskName(tLMRiskDB.getRiskName());
                /*Lis5.3 upgrade get
                tLCPENoticeSchema.setInsuredName(tLCInsuredDB.getName());
                */
                tLCPENoticeSchema.setGrpContNo(mLCContSchema.getGrpContNo());
                //tLCPENoticeSchema.setName(mInsuredNo);
                tLCPENoticeSchema.setName(mLCContSchema.getInsuredName());
                tLCPENoticeSchema.setPrintFlag("Y");
                tLCPENoticeSchema.setAppName(mLCContSchema.getAppntName());
                tLCPENoticeSchema.setAgentCode(mLCContSchema.getAgentCode());
                tLCPENoticeSchema.setAgentName(tLAAgentDB.getName());
                tLCPENoticeSchema.setManageCom(mLCContSchema.getManageCom());

               tLCPENoticeSchema.setPEBeforeCond(mmLCPENoticeSchema.getPEBeforeCond());
                tLCPENoticeSchema.setOperator(mOperate); //操作员
                tLCPENoticeSchema.setMakeDate(PubFun.getCurrentDate());
                tLCPENoticeSchema.setMakeTime(PubFun.getCurrentTime());
                tLCPENoticeSchema.setModifyDate(PubFun.getCurrentDate());
                tLCPENoticeSchema.setModifyTime(PubFun.getCurrentTime());
                tLCPENoticeSchema.setRemark(mmLCPENoticeSchema.getRemark());

                mLCPENoticeSet.clear();
                mLCPENoticeSet.add(tLCPENoticeSchema);



                //取体检资料项目信息
                mLCPENoticeItemSet.clear();
                merrcount = mmLCPENoticeItemSet.size();
                if ( merrcount >0 )
                {
                    for (int i = 1;i<=merrcount;i++)
                    {
                        //取出错信息
                        LCPENoticeItemSchema tLCPENoticeItemSchema = new LCPENoticeItemSchema();
                        tLCPENoticeItemSchema = mmLCPENoticeItemSet.get(i);
                        //生成流水号
                        String tserialno = ""+ i;

                        tLCPENoticeItemSchema.setProposalContNo( mPolNo );
                        tLCPENoticeItemSchema.setContNo(mPolNo);
                        tLCPENoticeItemSchema.setPrtSeq(mPrtSeq);
                        //由于先没有该字段，先把它注释掉，用到的时候再看（张星）
                       //tLCPENoticeItemSchemasetPEItemCode(tLCPENoticeItemSchema.getPEItemCode()); //核保规则编码
                        tLCPENoticeItemSchema.setPEItemName(tLCPENoticeItemSchema.getPEItemName()); //核保出错信息
                      // tLCPENoticeItemSchema.setCustomerNo(mInsuredNo);
                        tLCPENoticeItemSchema.setModifyDate(PubFun.getCurrentDate()); //当前值
                        tLCPENoticeItemSchema.setModifyTime(PubFun.getCurrentTime());

                        LCPENoticeItemSchema ttLCPENoticeItemSchema = new LCPENoticeItemSchema();
                        ttLCPENoticeItemSchema.setSchema(tLCPENoticeItemSchema);

                        mLCPENoticeItemSet.add(ttLCPENoticeItemSchema);
                    }
                }

                //核保主表信息
                LCCUWMasterDB tLCCUWMasterDB = new LCCUWMasterDB();
                tLCCUWMasterDB.setContNo(mPolNo);

                if(tLCCUWMasterDB.getInfo() == false)
                {
                  // @@错误处理
                  CError tError = new CError();
                  tError.moduleName = "UWAutoHealthBL";
                  tError.functionName = "prepareHealth";
                  tError.errorMessage = "无核保主表信息!";
                  this.mErrors .addOneError(tError) ;
                  return false;
                }

                mLCCUWMasterSchema = tLCCUWMasterDB.getSchema();

                //校验是否已经发放核保通知书
                if (mLCCUWMasterSchema.getPrintFlag().equals("Y"))
                {
                  CError tError = new CError();
                  tError.moduleName = "UWAutoHealthBL";
                  tError.functionName = "prepareHealth";
                  tError.errorMessage = "已经发核保通知不可录入!";
                  this.mErrors .addOneError(tError) ;
                  return false;
                }
                mLCCUWMasterSchema.setHealthFlag("1");

                return true;
        }

        /**
        * 准备需要保存的数据
        */
        private boolean prepareOutputData()
        {

          MMap map = new MMap();

          map.put(mAllLCPENoticeSet,"INSERT");
          map.put(mAllLCPENoticeItemSet,"INSERT");
          map.put(mLCCUWMasterSchema,"UPDATE");
          map.put(mLOPRTManagerSet,"INSERT");

          mResult.add(map);

          return true;

        }

}
