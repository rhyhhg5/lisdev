package com.sinosoft.lis.cbcheck;

import java.lang.*;
import java.util.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.cbcheck.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 承保核保业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author WHN
 * @version 1.0
 */
public class ContUWAutoChkBL {
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors = new CErrors();

  /** 往后面传输数据的容器 */
  private VData mInputData;

  /** 往界面传输数据的容器 */
  private VData mResult = new VData();

  /** 数据操作字符串 */
  private String mOperate;
  private String mpassflag; //通过标记
  private int merrcount; //错误条数
  private String mCalCode; //计算编码
  private String mUser;
  private FDate fDate = new FDate();
  private double mValue;
  private int merrflag; //判断个人单是不是全部通过标志
  private String muwgrade = ""; //申请级别
  private String mguwgrade = "";
  private String mcuwgrade = "";

  /** 业务处理相关变量 */
  private LCPolSet mLCPolSet = new LCPolSet();
  private LCPolSet mmLCPolSet = new LCPolSet();
  private LCPolSet m2LCPolSet = new LCPolSet();
  private LCPolSet mAllLCPolSet = new LCPolSet();
  private LCPolSchema mLCPolSchema = new LCPolSchema();
  private String mPolNo = "";
  private String mOldPolNo = "";

  /**团体保单表**/
  private LCGrpPolSet mLCGrpPolSet = new LCGrpPolSet();
  private LCGrpPolSet mmLCGrpPolSet = new LCGrpPolSet();
  private LCGrpPolSet mAllLCGrpPolSet = new LCGrpPolSet();
  private LCGrpPolSet m2LCGrpPolSet = new LCGrpPolSet();
  private LCGrpPolSchema mLCGrpPolSchema = new LCGrpPolSchema();
  private String mGrpPolNo = "";
  private String mOldGrpPolNo = "";

  /**合同保单表*/
  private LCContSet mLCContSet = new LCContSet();
  private LCContSet m2LCContSet = new LCContSet();
  private LCContSet mmLCContSet = new LCContSet();
  private LCContSet mAllLCContSet = new LCContSet();
  private LCContSchema mLCContSchema = new LCContSchema();
  private String mContPolNo = "";
  private String mOldContPolNo = "";

  /** 保费项表 */
  private LCPremSet mLCPremSet = new LCPremSet();
  private LCPremSet mAllLCPremSet = new LCPremSet();

  /** 领取项表 */
  private LCGetSet mLCGetSet = new LCGetSet();
  private LCGetSet mAllLCGetSet = new LCGetSet();

  /** 责任表 */
  private LCDutySet mLCDutySet = new LCDutySet();
  private LCDutySet mAllLCDutySet = new LCDutySet();

  /** 特别约定表 */
  private LCSpecSet mLCSpecSet = new LCSpecSet();
  private LCSpecSet mAllLCSpecSet = new LCSpecSet();

  /** 特别约定注释表 */
  private LCSpecNoteSet mLCSpecNoteSet = new LCSpecNoteSet();
  private LCSpecNoteSet mAllLCSpecNoteSet = new LCSpecNoteSet();

  /** 合同核保主表 */
  private LCCUWMasterSet mLCCUWMasterSet = new LCCUWMasterSet();
  private LCCUWMasterSet mAllLCCUWMasterSet = new LCCUWMasterSet();
  private LCCUWMasterSchema mLCCUWMasterSchema = new LCCUWMasterSchema();

  /** 合同核保子表 */
  private LCCUWSubSet mLCCUWSubSet = new LCCUWSubSet();
  private LCCUWSubSet mAllLCCUWSubSet = new LCCUWSubSet();
  private LCCUWSubSchema mLCCUWSubSchema = new LCCUWSubSchema();

  /** 合同核保错误信息表 */
  private LCCUWErrorSet mLCCUWErrorSet = new LCCUWErrorSet();
  private LCCUWErrorSet mAllLCCErrSet = new LCCUWErrorSet();

  /** 团体核保主表 */
  private LCGUWMasterSet mLCGUWMasterSet = new LCGUWMasterSet();
  private LCGUWMasterSet mAllLCGUWMasterSet = new LCGUWMasterSet();
  private LCGUWMasterSchema mLCGUWMasterSchema = new LCGUWMasterSchema();

  /** 团体核保子表 */
  private LCGUWSubSet mLCGUWSubSet = new LCGUWSubSet();
  private LCGUWSubSet mAllLCGUWSubSet = new LCGUWSubSet();
  private LCGUWSubSchema mLCGUWSubSchema = new LCGUWSubSchema();

  /** 团体核保错误信息表 */
  private LCGUWErrorSet mLCGUWErrorSet = new LCGUWErrorSet();
  private LCGUWErrorSet mAllLCGErrSet = new LCGUWErrorSet();

  /** 核保主表 */
  private LCUWMasterSet mLCUWMasterSet = new LCUWMasterSet();
  private LCUWMasterSet mAllLCUWMasterSet = new LCUWMasterSet();
  private LCUWMasterSchema mLCUWMasterSchema = new LCUWMasterSchema();

  /** 核保子表 */
  private LCUWSubSet mLCUWSubSet = new LCUWSubSet();
  private LCUWSubSet mAllLCUWSubSet = new LCUWSubSet();
  private LCUWSubSchema mLCUWSubSchema = new LCUWSubSchema();

  /** 核保错误信息表 */
  private LCUWErrorSet mLCUWErrorSet = new LCUWErrorSet();
  private LCUWErrorSet mAllLCErrSet = new LCUWErrorSet();

  /** 告知表 */
  private LCCustomerImpartSet mLCCustomerImpartSet = new LCCustomerImpartSet();
  private LCCustomerImpartSet mAllLCCustomerImpartSet = new LCCustomerImpartSet();

  /** 投保人表 */
  private LCAppntIndSet mLCAppntIndSet = new LCAppntIndSet();
  private LCAppntIndSet mAllLCAppntIndSet = new LCAppntIndSet();

  /** 受益人表 */
  private LCBnfSet mLCBnfSet = new LCBnfSet();
  private LCBnfSet mAllLCBnfSet = new LCBnfSet();

  /** 被保险人表 */
  private LCInsuredSet mLCInsuredSet = new LCInsuredSet();
  private LCInsuredSet mAllLCInsuredSet = new LCInsuredSet();

  /**计算公式表**/
  private LMUWSchema mLMUWSchema = new LMUWSchema();

  //private LMUWDBSet mLMUWDBSet = new LMUWDBSet();
  private LMUWSet mLMUWSet = new LMUWSet();
  private LMUWSet mGLMUWSet = new LMUWSet();
  private LMUWSet mCLMUWSet = new LMUWSet();

  private CalBase mCalBase = new CalBase();

  public ContUWAutoChkBL() {}

  /**
   * 传输数据的公共方法
   * @param: cInputData 输入的数据
   *         cOperate 数据操作
   * @return:
   */
  public boolean submitData(VData cInputData, String cOperate) {
    int flag = 0; //判断是不是所有数据都不成功
    int j = 0; //符合条件数据个数

    //将操作数据拷贝到本类中
    mInputData = (VData) cInputData.clone();
    //GlobalInput tGlobalInput = new GlobalInput();
    //this.mOperate = tGlobalInput.;

    System.out.println("---ContUWAutoChkBL getInputData begin---");
    //得到外部传入的数据,将数据备份到本类中
    if (!CgetInputData(cInputData))
      return false;
    System.out.println("---ContUWAutoChkBL getInputData end---");

    int n = mLCContSet.size();
    if (n > 0) {
      for (int i = 1; i <= n; i++) {
        //初始化
        merrflag = 0;
        mpassflag = "0";

        LCContSchema tLCContSchema = new LCContSchema();
        tLCContSchema = (LCContSchema) mLCContSet.get(i);
        mLCContSchema = (LCContSchema) mLCContSet.get(i);
        /*Lis5.3 upgrade set
                            String tProposalNo = tLCContSchema.getProposalNo();
                            mOldContPolNo = tLCContSchema.getProposalNo();
                            mContPolNo = tLCContSchema.getProposalNo();
         */
        String tProposalNo = tLCContSchema.getProposalContNo();
        mOldContPolNo = tLCContSchema.getProposalContNo();
        mContPolNo = tLCContSchema.getProposalContNo();

        // 团单校验数据
        System.out.println("---ContUWAutoChkBL checkData begin---");
        if (!CcheckApprove(tLCContSchema)) {
          continue;
        }
        else {
          m2LCContSet.add(tLCContSchema);
        }

        //校验核保级别
        if (!checkUWGrade(tLCContSchema)) {
          continue;
        }
        else {
          m2LCContSet.add(tLCContSchema);
        }

        System.out.println("---ContUWAutoChkBL checkData end---");

        //合同单下团单核保
        if (!GsubmitData(tLCContSchema))
          continue;
        else {
          m2LCContSet.add(tLCContSchema);
        }

        //合同单下个人单核保
        if (!PsubmitData(tLCContSchema))
          continue;
        else {
          m2LCGrpPolSet.add(tLCContSchema);
        }

        // 团单数据操作业务处理
        if (!CdealData(tLCContSchema))
          continue;
        else {
          m2LCContSet.add(tLCContSchema);
        }

        flag = 1;
        System.out.println("---ContUWAutoChkBL GdealData end---");
      }
    }
    else {
      CError tError = new CError();
      tError.moduleName = "ContUWAutoChkBL";
      tError.functionName = "submitData";
      tError.errorMessage = "没有传入合同保单!";
      this.mErrors.addOneError(tError);
      return false;
    }

    System.out.println("---ContUWAutoChkBL dealData---");
    //准备给后台的数据
    prepareOutputData();

    if (flag == 0) {
      CError tError = new CError();
      tError.moduleName = "ContUWAutoChkBL";
      tError.functionName = "submitData";
      tError.errorMessage = "没有自动通过保单!";
      this.mErrors.addOneError(tError);
      return false;
    }

    System.out.println("---ContUWAutoChkBL prepareOutputData---");
    //数据提交
    ContUWAutoChkBLS tContUWAutoChkBLS = new ContUWAutoChkBLS();
    System.out.println("Start ContUWAutoChkBL Submit...");
    if (!tContUWAutoChkBLS.submitData(mInputData, mOperate)) {
      // @@错误处理
      this.mErrors.copyAllErrors(tContUWAutoChkBLS.mErrors);
      CError tError = new CError();
      tError.moduleName = "ContUWAutoChkBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    System.out.println("---ContUWAutoChkBL commitData---");
    return true;
  }

  /**
   * 传输数据的公共方法
   * @param: cInputData 输入的数据
   *         cOperate 数据操作
   * @return:
   */
  public boolean GsubmitData(LCContSchema tLCContSchema) {
    int flag = 0; //判断是不是所有数据都不成功
    int j = 0; //符合条件数据个数

    System.out.println("---GrpUWAutoChkBL getInputData begin---");
    //得到外部传入的数据,将数据备份到本类中
    if (!GgetInputData(tLCContSchema))
      return false;
    System.out.println("---GrpUWAutoChkBL getInputData end---");

    int n = mLCGrpPolSet.size();
    if (n > 0) {
      for (int i = 1; i <= n; i++) {
        //初始化
        merrflag = 0;
        mpassflag = "0";

        LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
        tLCGrpPolSchema = (LCGrpPolSchema) mLCGrpPolSet.get(i);
        mLCGrpPolSchema = (LCGrpPolSchema) mLCGrpPolSet.get(i);
        String tGrpProposalNo = tLCGrpPolSchema.getGrpProposalNo();
        mOldGrpPolNo = tLCGrpPolSchema.getGrpProposalNo();
        mGrpPolNo = tLCGrpPolSchema.getGrpProposalNo();

        // 团单校验数据
        System.out.println("---GrpUWAutoChkBL checkData begin---");
        if (!GcheckApprove(tLCGrpPolSchema)) {
          return false;
        }
        else {
          m2LCGrpPolSet.add(tLCGrpPolSchema);
        }

        System.out.println("---GrpUWAutoChkBL checkData end---");

        // 团单数据操作业务处理
        if (!GdealData(tLCGrpPolSchema))
          return false;
        else {
          m2LCGrpPolSet.add(tLCGrpPolSchema);
        }

        flag = 1;
        System.out.println("---GrpUWAutoChkBL GdealData end---");
      }
      System.out.println("---GrpUWAutoChkBL dealData---");
    }
    return true;
  }

  /**
   * 传输数据的公共方法
   * @param: cInputData 输入的数据
   *         cOperate 数据操作
   * @return:
   */
  private boolean PsubmitData(LCContSchema tLCContSchema) {
    int flag = 0; //判断是不是所有数据都不成功
    int j = 0; //符合条件数据个数

    //将操作数据拷贝到本类中
    //mInputData = (VData)cInputData.clone();
    //GlobalInput tGlobalInput = new GlobalInput();
    //this.mOperate = tGlobalInput.;

    System.out.println("---1---");
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(tLCContSchema))
      return false;
    System.out.println("---UWAutoChkBL getInputData---");

    int n = mLCPolSet.size();
    if (n > 0) {
      for (int i = 1; i <= n; i++) {
        LCPolSchema tLCPolSchema = new LCPolSchema();
        tLCPolSchema = (LCPolSchema) mLCPolSet.get(i);
        mLCPolSchema = (LCPolSchema) mLCPolSet.get(i);
        String tProposalNo = tLCPolSchema.getProposalNo();
        mOldPolNo = tLCPolSchema.getProposalNo();
        mPolNo = tLCPolSchema.getProposalNo();

        // 校验数据
        if (!checkApprove(tLCPolSchema)) {
          return false;
        }
        else {
          m2LCPolSet.add(tLCPolSchema);
        }

        System.out.println("---UWAutoChkBL checkData---");
        // 数据操作业务处理

        if (!dealData(tLCPolSchema))
          return false;
        else {
          m2LCPolSet.add(tLCPolSchema);
        }

        flag = 1;
      }
    }
    else {
      CError tError = new CError();
      tError.moduleName = "ContUWAutoChkBL";
      tError.functionName = "submitData";
      tError.errorMessage = "没有个人保单!";
      this.mErrors.addOneError(tError);
      return false;
    }

    System.out.println("---UWAutoChkBL dealData---");

    return true;
  }

  /**
   * 数据操作类业务处理
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean CdealData(LCContSchema tLCContSchema) {
    /*Lis5.3 upgrade set
         mOldGrpPolNo = tLCContSchema.getProposalNo();
     */
    mOldGrpPolNo = tLCContSchema.getProposalContNo();
    //String tkinds = tLCContSchema.getRiskCode();

    //准备算法
    if (CCheckKinds(tLCContSchema) == false)
      return false;

    //准备数据
    CCheckPolInit(tLCContSchema);

    //分单核保
    //mpassflag = "0";
    int n = mCLMUWSet.size();
    if (n == 0) {
      mpassflag = "9";
    }
    else {
      int j = 0;
      for (int i = 1; i <= n; i++) {
        //取计算编码
        LMUWSchema tLMUWSchema = new LMUWSchema();
        tLMUWSchema = mCLMUWSet.get(i);
        mCalCode = tLMUWSchema.getCalCode();
        if (CheckPol() == 0) {
          mCLMUWSet.remove(tLMUWSchema);
        }
        else {
          mpassflag = "5"; //待人工核保

          //取核保级别
          if (j == 0) {
            mcuwgrade = tLMUWSchema.getUWGrade();
            j++;
          }
          else {
            String tuwgrade = tLMUWSchema.getUWGrade();
            if (mcuwgrade.compareTo(tuwgrade) > 0) {
              mcuwgrade = tuwgrade;
            }
          }

        }
      }
      if (mpassflag.equals("0") == true) {
        mpassflag = "9";
      }
    }

    if (merrflag == 0 && mpassflag.equals("9")) {
      mpassflag = "9";
    }
    else {
      mpassflag = "5";
    }

    //if (preparePubInfo( tLCPolSchema ) == false)
    //	return false;

    if (CdealOnePol() == false)
      return false;

    return true;
  }

  /**
   * 数据操作类业务处理
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean GdealData(LCGrpPolSchema tLCGrpPolSchema) {
    mOldGrpPolNo = tLCGrpPolSchema.getGrpPolNo();
    String tkinds = tLCGrpPolSchema.getRiskCode();

    //准备算法
    if (GCheckKinds(tLCGrpPolSchema) == false)
      return false;

    //准备数据
    GCheckPolInit(tLCGrpPolSchema);

    //分单核保
    //mpassflag = "0";
    int n = mGLMUWSet.size();
    if (n == 0) {
      mpassflag = "9";
    }
    else {
      int j = 0;
      for (int i = 1; i <= n; i++) {
        //取计算编码
        LMUWSchema tLMUWSchema = new LMUWSchema();
        tLMUWSchema = mGLMUWSet.get(i);
        mCalCode = tLMUWSchema.getCalCode();
        if (CheckPol() == 0) {
          mGLMUWSet.remove(tLMUWSchema);
        }
        else {
          mpassflag = "5"; //待人工核保

          //取核保级别
          if (j == 0) {
            mguwgrade = tLMUWSchema.getUWGrade();
            j++;
          }
          else {
            String tuwgrade = tLMUWSchema.getUWGrade();
            if (mguwgrade.compareTo(tuwgrade) > 0) {
              mguwgrade = tuwgrade;
            }
          }
        }
      }
      if (mpassflag.equals("0") == true) {
        mpassflag = "9";
      }
    }

    if (merrflag == 0 && mpassflag.equals("9")) {
      mpassflag = "9";
    }
    else {
      mpassflag = "5";
    }

    //if (preparePubInfo( tLCPolSchema ) == false)
    //	return false;

    if (GdealOnePol() == false)
      return false;

    return true;
  }

  /**
   * 数据操作类业务处理
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean dealData(LCPolSchema tLCPolSchema) {
    mOldPolNo = tLCPolSchema.getPolNo();
    String tkinds = tLCPolSchema.getRiskCode();

    //准备算法
    if (CheckKinds(tLCPolSchema) == false)
      return false;

    //准备数据
    CheckPolInit(tLCPolSchema);

    //个人单核保
    //mpassflag = "0";
    int n = mLMUWSet.size();
    if (n == 0) {
      merrflag = 0;
      mpassflag = "9";
    }
    else {
      int j = 0;
      for (int i = 1; i <= n; i++) {
        //取计算编码
        LMUWSchema tLMUWSchema = new LMUWSchema();
        tLMUWSchema = mLMUWSet.get(i);
        mCalCode = tLMUWSchema.getCalCode();
        if (CheckPol() == 0) {
          mLMUWSet.remove(tLMUWSchema);
        }
        else {
          mpassflag = "5"; //待人工核保
          merrflag = 1;

          //取核保级别
          if (j == 0) {
            muwgrade = tLMUWSchema.getUWGrade();
            j++;
          }
          else {
            String tuwgrade = tLMUWSchema.getUWGrade();
            if (muwgrade.compareTo(tuwgrade) > 0) {
              muwgrade = tuwgrade;
            }
          }
        }
      }
      if (mpassflag.equals("0") == true) {
        mpassflag = "9";
      }
    }

    //if (preparePubInfo( tLCPolSchema ) == false)
    //	return false;

    if (dealOnePol() == false)
      return false;

    return true;
  }

  /**
   * 操作一张团体保单的业务处理
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean CdealOnePol() {
    // 保单
    if (CpreparePol() == false)
      return false;
    // 核保信息
    if (CprepareUW() == false)
      return false;

    LCContSchema tLCContSchema = new LCContSchema();
    tLCContSchema.setSchema(mLCContSchema);
    mAllLCContSet.add(tLCContSchema);

    LCCUWMasterSet tLCCUWMasterSet = new LCCUWMasterSet();
    tLCCUWMasterSet.set(mLCCUWMasterSet);
    mAllLCCUWMasterSet.add(tLCCUWMasterSet);

    LCCUWSubSet tLCCUWSubSet = new LCCUWSubSet();
    tLCCUWSubSet.set(mLCCUWSubSet);
    mAllLCCUWSubSet.add(tLCCUWSubSet);

    LCCUWErrorSet tLCCUWErrorSet = new LCCUWErrorSet();
    tLCCUWErrorSet.set(mLCCUWErrorSet);
    mAllLCCErrSet.add(tLCCUWErrorSet);

    return true;
  }

  /**
   * 操作一张团体保单的业务处理
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean GdealOnePol() {
    // 保单
    if (GpreparePol() == false)
      return false;
    // 核保信息
    if (GprepareUW() == false)
      return false;

    LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
    tLCGrpPolSchema.setSchema(mLCGrpPolSchema);
    mAllLCGrpPolSet.add(tLCGrpPolSchema);

    LCGUWMasterSet tLCGUWMasterSet = new LCGUWMasterSet();
    tLCGUWMasterSet.set(mLCGUWMasterSet);
    mAllLCGUWMasterSet.add(tLCGUWMasterSet);

    LCGUWSubSet tLCGUWSubSet = new LCGUWSubSet();
    tLCGUWSubSet.set(mLCGUWSubSet);
    mAllLCGUWSubSet.add(tLCGUWSubSet);

    LCGUWErrorSet tLCGUWErrorSet = new LCGUWErrorSet();
    tLCGUWErrorSet.set(mLCGUWErrorSet);
    mAllLCGErrSet.add(tLCGUWErrorSet);

    return true;
  }

  /**
   * 操作一张保单的业务处理
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean dealOnePol() {
    // 保单
    if (preparePol() == false)
      return false;
    // 核保信息
    if (prepareUW() == false)
      return false;

    LCPolSchema tLCPolSchema = new LCPolSchema();
    tLCPolSchema.setSchema(mLCPolSchema);
    mAllLCPolSet.add(tLCPolSchema);

    LCUWMasterSet tLCUWMasterSet = new LCUWMasterSet();
    tLCUWMasterSet.set(mLCUWMasterSet);
    mAllLCUWMasterSet.add(tLCUWMasterSet);

    LCUWSubSet tLCUWSubSet = new LCUWSubSet();
    tLCUWSubSet.set(mLCUWSubSet);
    mAllLCUWSubSet.add(tLCUWSubSet);

    LCUWErrorSet tLCUWErrorSet = new LCUWErrorSet();
    tLCUWErrorSet.set(mLCUWErrorSet);
    mAllLCErrSet.add(tLCUWErrorSet);

    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果团体单没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean CgetInputData(VData cInputData) {
    GlobalInput tGlobalInput = new GlobalInput();
    tGlobalInput.setSchema( (GlobalInput) cInputData.getObjectByObjectName(
        "GlobalInput", 0));
    mOperate = tGlobalInput.Operator;

    mmLCContSet.set( (LCContSet) cInputData.getObjectByObjectName("LCContSet",
        0));
    int n = mmLCContSet.size();
    int flag = 0; //怕判断是不是所有保单都失败
    int j = 0; //符合条件保单个数

    for (int i = 1; i <= n; i++) {
      LCContDB tLCContDB = new LCContDB();
      LCContSchema tLCContSchema = mmLCContSet.get(i);
      tLCContDB.setContNo(tLCContSchema.getContNo());
      System.out.println("--BL--GrpPol--" + tLCContSchema.getContNo());
      String temp = tLCContSchema.getContNo();
      System.out.println("temp" + temp);
      if (tLCContDB.getInfo() == false) {
        // @@错误处理
        this.mErrors.copyAllErrors(tLCContDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "ContUWAutoChkBL";
        tError.functionName = "getInputData";
        tError.errorMessage = "投保单查询失败!";
        this.mErrors.addOneError(tError);
        return false;
      }
      else {
        flag = 1;
        j++;
        tLCContSchema.setSchema(tLCContDB);
        mLCGrpPolSet.add(tLCContSchema);
      }

    }

    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果团体单没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean GgetInputData(LCContSchema tLCContSchema) {
    int n = mmLCGrpPolSet.size();
    int flag = 0; //怕判断是不是所有保单都失败
    int j = 0; //符合条件保单个数

    LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
    /*Lis5.3 upgrade set
     System.out.println("--BL--GrpPol--"+tLCContSchema.getProposalNo());
     */
    System.out.println("--BL--GrpPol--" + tLCContSchema.getProposalContNo());
    /*Lis5.3 upgrade set
         tLCGrpPolDB.setContNo(mContPolNo);
     */
    mLCGrpPolSet = tLCGrpPolDB.query();

    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(LCContSchema tLCContSchema) {

    LCPolDB tLCPolDB = new LCPolDB();
    tLCPolDB.setContNo(mContPolNo);
    mLCPolSet = tLCPolDB.query();

    return true;

  }

  /**
   * 校验投保单是否复核
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean checkApprove(LCPolSchema tLCPolSchema) {
    if (tLCPolSchema.getApproveCode() == null || tLCPolSchema.getApproveDate() == null) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "UWAutoChkBL";
      tError.functionName = "checkApprove";
      tError.errorMessage = "投保单尚未进行复核操作，不能核保!（投保单号：" +
          tLCPolSchema.getPolNo().trim() + "）";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 校验团体投保单是否复核
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean CcheckApprove(LCContSchema tLCContSchema) {
    if (tLCContSchema.getApproveCode().equals("") ||
        tLCContSchema.getApproveDate() == null) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "ContUWAutoChkBL";
      tError.functionName = "GcheckApprove";
      /*Lis5.3 upgrade set
      tError.errorMessage = "投保单尚未进行复核操作，不能核保!（投保单号：" +
          tLCContSchema.getProposalNo().trim() + "）";
      */
      tError.errorMessage = "投保单尚未进行复核操作，不能核保!（投保单号：" +
          tLCContSchema.getProposalContNo().trim() + "）";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 校验团体投保单是否复核
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean GcheckApprove(LCGrpPolSchema tLCGrpPolSchema) {
    if (tLCGrpPolSchema.getApproveCode().equals("") ||
        tLCGrpPolSchema.getApproveDate() == null) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "GrpUWAutoChkBL";
      tError.functionName = "GcheckApprove";
      tError.errorMessage = "投保单尚未进行复核操作，不能核保!（投保单号：" +
          tLCGrpPolSchema.getGrpPolNo().trim() + "）";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 校验核保员级别
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean checkUWGrade(LCContSchema tLCContSchema) {
    LDUserDB tLDUserDB = new LDUserDB();
    tLDUserDB.setUserCode(mOperate);

    if (!tLDUserDB.getInfo()) {
      CError tError = new CError();
      tError.moduleName = "ContUWAutoChkBL";
      tError.functionName = "checkApprove";
      tError.errorMessage = "无此操作员信息，不能核保!（操作员：" + mOperate + "）";
      this.mErrors.addOneError(tError);
      return false;
    }

    String tUWPopedom = tLDUserDB.getUWPopedom();
    if (tUWPopedom.equals("")) {
      CError tError = new CError();
      tError.moduleName = "ContUWAutoChkBL";
      tError.functionName = "checkApprove";
      tError.errorMessage = "操作员无核保权限，不能核保!（操作员：" + mOperate + "）";
      this.mErrors.addOneError(tError);
      return false;
    }

    return true;
  }

  /**
   * 校验投保单是否核保
   * 输出：如果发生错误则返回false,否则返回true
   */
  /*whn
    private boolean checkUW(LCPolSchema tLCPolSchema)
    {
     if (tLCPolSchema.getUWCode() != null || tLCPolSchema.getUWDate() != null)
     {
    // @@错误处理
    CError tError = new CError();
    tError.moduleName = "UWAutoChkBL";
    tError.functionName = "checkApprove";
    tError.errorMessage = "投保单已经自动核保，请订正后重新再核!（投保单号：" + tLCPolSchema.getPolNo().trim() + "）";
    this.mErrors .addOneError(tError) ;
    return false;
   }
   return true;
    }
    whn*/

  /**
   * 核保险种信息校验,准备核保算法
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean CCheckKinds(LCContSchema tLCContSchema) {
    mCLMUWSet.clear();
    LMUWSchema tLMUWSchema = new LMUWSchema();
    //查询算法编码
    //tLMUWSchema.setRiskCode(tLCGrpPolSchema.getRiskCode().trim());
    tLMUWSchema.setRelaPolType("C");
    tLMUWSchema.setUWType("1");

    LMUWDB tLMUWDB = new LMUWDB();
    tLMUWDB.setSchema(tLMUWSchema);

    LMUWDBSet tLMUWDBSet = new LMUWDBSet();
    mCLMUWSet = tLMUWDB.query();
    if (tLMUWDB.mErrors.needDealError() == true) {
      // @@错误处理
      this.mErrors.copyAllErrors(tLMUWDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "ContUWAutoChkBL";
      tError.functionName = "Ccheckpol";
      tError.errorMessage = "险种信息查询失败!";
      this.mErrors.addOneError(tError);
      mGLMUWSet.clear();
      return false;
    }
    return true;
  }

  /**
   * 核保险种信息校验,准备核保算法
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean GCheckKinds(LCGrpPolSchema tLCGrpPolSchema) {
    mGLMUWSet.clear();
    LMUWSchema tLMUWSchema = new LMUWSchema();
    //查询算法编码
    tLMUWSchema.setRiskCode(tLCGrpPolSchema.getRiskCode().trim());
    tLMUWSchema.setRelaPolType("I");
    tLMUWSchema.setUWType("1");

    LMUWDB tLMUWDB = new LMUWDB();
    tLMUWDB.setSchema(tLMUWSchema);

    LMUWDBSet tLMUWDBSet = new LMUWDBSet();
    mGLMUWSet = tLMUWDB.query();
    if (tLMUWDB.mErrors.needDealError() == true) {
      // @@错误处理
      this.mErrors.copyAllErrors(tLMUWDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "UWAutoChkBL";
      tError.functionName = "checkpol";
      tError.errorMessage = "险种信息查询失败!";
      this.mErrors.addOneError(tError);
      mGLMUWSet.clear();
      return false;
    }
    return true;
  }

  /**
   * 核保险种信息校验,准备核保算法
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean CheckKinds(LCPolSchema tLCPolSchema) {
    mLMUWSet.clear();
    LMUWSchema tLMUWSchema = new LMUWSchema();
    //查询算法编码
    tLMUWSchema.setRiskCode(tLCPolSchema.getRiskCode().trim());
    tLMUWSchema.setRelaPolType("G");

    LMUWDB tLMUWDB = new LMUWDB();
    tLMUWDB.setSchema(tLMUWSchema);

    LMUWDBSet tLMUWDBSet = new LMUWDBSet();
    mLMUWSet = tLMUWDB.query();
    if (tLMUWDB.mErrors.needDealError() == true) {
      // @@错误处理
      this.mErrors.copyAllErrors(tLMUWDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "UWAutoChkBL";
      tError.functionName = "checkpol";
      tError.errorMessage = "险种信息查询失败!";
      this.mErrors.addOneError(tError);
      mLMUWSet.clear();
      return false;
    }
    return true;
  }

  /**
   * 团体单核保数据准备
   * 输出：如果发生错误则返回false,否则返回true
   */
  private void CCheckPolInit(LCContSchema tLCContSchema) {
    mCalBase = new CalBase();
    mCalBase.setPrem(tLCContSchema.getPrem());
    mCalBase.setGet(tLCContSchema.getAmnt());
    mCalBase.setMult(tLCContSchema.getMult());
    //mCalBase.setYears( tLCPolSchema.getYears() );
    //mCalBase.setAppAge( tLCGrpPolSchema.getInsuredAppAge() );
    //mCalBase.setSex( tLCGrpPolSchema.getInsuredSex() );
    //mCalBase.setJob( tLCGrpPolSchema.getOccupationType() );
    //mCalBase.setCount( tLCGrpPolSchema.getInsuredPeoples() );
    mCalBase.setPolNo(tLCContSchema.getContNo());
  }

  /**
   * 团体单核保数据准备
   * 输出：如果发生错误则返回false,否则返回true
   */
  private void GCheckPolInit(LCGrpPolSchema tLCGrpPolSchema) {
    mCalBase = new CalBase();
    mCalBase.setPrem(tLCGrpPolSchema.getPrem());
    mCalBase.setGet(tLCGrpPolSchema.getAmnt());
    mCalBase.setMult(tLCGrpPolSchema.getMult());
    //mCalBase.setYears( tLCPolSchema.getYears() );
    //mCalBase.setAppAge( tLCGrpPolSchema.getInsuredAppAge() );
    //mCalBase.setSex( tLCGrpPolSchema.getInsuredSex() );
    //mCalBase.setJob( tLCGrpPolSchema.getOccupationType() );
    //mCalBase.setCount( tLCGrpPolSchema.getInsuredPeoples() );
    mCalBase.setPolNo(tLCGrpPolSchema.getGrpPolNo());
  }

  /**
   * 个人单核保数据准备
   * 输出：如果发生错误则返回false,否则返回true
   */
  private void CheckPolInit(LCPolSchema tLCPolSchema) {
    mCalBase = new CalBase();
    mCalBase.setPrem(tLCPolSchema.getPrem());
    mCalBase.setGet(tLCPolSchema.getAmnt());
    mCalBase.setMult(tLCPolSchema.getMult());
    //mCalBase.setYears( tLCPolSchema.getYears() );
    mCalBase.setAppAge(tLCPolSchema.getInsuredAppAge());
    mCalBase.setSex(tLCPolSchema.getInsuredSex());
    mCalBase.setJob(tLCPolSchema.getOccupationType());
    mCalBase.setCount(tLCPolSchema.getInsuredPeoples());
    mCalBase.setPolNo(tLCPolSchema.getPolNo());
  }

  /**
   * 团体单核保
   * 输出：如果发生错误则返回false,否则返回true
   */
  private double CCheckPol() {
    // 计算
    Calculator mCalculator = new Calculator();
    mCalculator.setCalCode(mCalCode);
    //增加基本要素
    mCalculator.addBasicFactor("Get", mCalBase.getGet());
    mCalculator.addBasicFactor("Mult", mCalBase.getMult());
    mCalculator.addBasicFactor("Prem", mCalBase.getPrem());
    //mCalculator.addBasicFactor("PayIntv", mCalBase.getPayIntv() );
    //mCalculator.addBasicFactor("GetIntv", mCalBase.getGetIntv() );
    mCalculator.addBasicFactor("AppAge", mCalBase.getAppAge());
    mCalculator.addBasicFactor("Sex", mCalBase.getSex());
    mCalculator.addBasicFactor("Job", mCalBase.getJob());
    mCalculator.addBasicFactor("PayEndYear", mCalBase.getPayEndYear());
    mCalculator.addBasicFactor("GetStartDate", "");
    //mCalculator.addBasicFactor("GetYear", mCalBase.getGetYear() );
    mCalculator.addBasicFactor("Years", mCalBase.getYears());
    mCalculator.addBasicFactor("GrpPolNo", mCalBase.getPolNo());
    mCalculator.addBasicFactor("GetFlag", "");
    mCalculator.addBasicFactor("ValiDate", "");
    mCalculator.addBasicFactor("Count", mCalBase.getCount());
    mCalculator.addBasicFactor("FirstPayDate", "");
    //mCalculator.addBasicFactor("AddRate", mCalBase.getAddRate() );
    //mCalculator.addBasicFactor("GDuty", mCalBase.getGDuty() );
    mCalculator.addBasicFactor("PolNo", mCalBase.getPolNo());

    String tStr = "";
    tStr = mCalculator.calculate();
    if (tStr.trim().equals(""))
      mValue = 0;
    else
      mValue = Double.parseDouble(tStr);

    System.out.println(mValue);
    return mValue;
  }

  /**
   * 团体单核保
   * 输出：如果发生错误则返回false,否则返回true
   */
  private double GCheckPol() {
    // 计算
    Calculator mCalculator = new Calculator();
    mCalculator.setCalCode(mCalCode);
    //增加基本要素
    mCalculator.addBasicFactor("Get", mCalBase.getGet());
    mCalculator.addBasicFactor("Mult", mCalBase.getMult());
    mCalculator.addBasicFactor("Prem", mCalBase.getPrem());
    //mCalculator.addBasicFactor("PayIntv", mCalBase.getPayIntv() );
    //mCalculator.addBasicFactor("GetIntv", mCalBase.getGetIntv() );
    mCalculator.addBasicFactor("AppAge", mCalBase.getAppAge());
    mCalculator.addBasicFactor("Sex", mCalBase.getSex());
    mCalculator.addBasicFactor("Job", mCalBase.getJob());
    mCalculator.addBasicFactor("PayEndYear", mCalBase.getPayEndYear());
    mCalculator.addBasicFactor("GetStartDate", "");
    //mCalculator.addBasicFactor("GetYear", mCalBase.getGetYear() );
    mCalculator.addBasicFactor("Years", mCalBase.getYears());
    mCalculator.addBasicFactor("GrpPolNo", mCalBase.getPolNo());
    mCalculator.addBasicFactor("GetFlag", "");
    mCalculator.addBasicFactor("ValiDate", "");
    mCalculator.addBasicFactor("Count", mCalBase.getCount());
    mCalculator.addBasicFactor("FirstPayDate", "");
    //mCalculator.addBasicFactor("AddRate", mCalBase.getAddRate() );
    //mCalculator.addBasicFactor("GDuty", mCalBase.getGDuty() );
    mCalculator.addBasicFactor("PolNo", mCalBase.getPolNo());

    String tStr = "";
    tStr = mCalculator.calculate();
    if (tStr.trim().equals(""))
      mValue = 0;
    else
      mValue = Double.parseDouble(tStr);

    System.out.println(mValue);
    return mValue;
  }

  /**
   * 个人单核保
   * 输出：如果发生错误则返回false,否则返回true
   */
  private double CheckPol() {
    // 计算
    Calculator mCalculator = new Calculator();
    mCalculator.setCalCode(mCalCode);
    //增加基本要素
    mCalculator.addBasicFactor("Get", mCalBase.getGet());
    mCalculator.addBasicFactor("Mult", mCalBase.getMult());
    mCalculator.addBasicFactor("Prem", mCalBase.getPrem());
    //mCalculator.addBasicFactor("PayIntv", mCalBase.getPayIntv() );
    //mCalculator.addBasicFactor("GetIntv", mCalBase.getGetIntv() );
    mCalculator.addBasicFactor("AppAge", mCalBase.getAppAge());
    mCalculator.addBasicFactor("Sex", mCalBase.getSex());
    mCalculator.addBasicFactor("Job", mCalBase.getJob());
    mCalculator.addBasicFactor("PayEndYear", mCalBase.getPayEndYear());
    mCalculator.addBasicFactor("GetStartDate", "");
    //mCalculator.addBasicFactor("GetYear", mCalBase.getGetYear() );
    mCalculator.addBasicFactor("Years", mCalBase.getYears());
    mCalculator.addBasicFactor("Grp", "");
    mCalculator.addBasicFactor("GetFlag", "");
    mCalculator.addBasicFactor("ValiDate", "");
    mCalculator.addBasicFactor("Count", mCalBase.getCount());
    mCalculator.addBasicFactor("FirstPayDate", "");
    //mCalculator.addBasicFactor("AddRate", mCalBase.getAddRate() );
    //mCalculator.addBasicFactor("GDuty", mCalBase.getGDuty() );
    mCalculator.addBasicFactor("PolNo", mCalBase.getPolNo());
    //mCalculator.addBasicFactor("GrpPolNo",mCalBase.getgrp);

    String tStr = "";
    tStr = mCalculator.calculate();
    if (tStr.trim().equals(""))
      mValue = 0;
    else
      mValue = Double.parseDouble(tStr);

    System.out.println(mValue);
    return mValue;
  }

  /**
   * 准备保单信息
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean CpreparePol() {
    System.out.println("C核保标志" + mpassflag);
    mLCContSchema.setUWFlag(mpassflag); //待人工核保
    mLCContSchema.setUWOperator(mOperate);
    mLCContSchema.setModifyDate(PubFun.getCurrentDate());
    mLCContSchema.setModifyTime(PubFun.getCurrentTime());

    return true;
  }

  /**
   * 准备保单信息
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean GpreparePol() {
    System.out.println("G核保标志" + mpassflag);
    mLCGrpPolSchema.setUWFlag(mpassflag); //待人工核保
    mLCGrpPolSchema.setUWOperator(mOperate);
    mLCGrpPolSchema.setModifyDate(PubFun.getCurrentDate());
    mLCGrpPolSchema.setModifyTime(PubFun.getCurrentTime());

    return true;
  }

  /**
   * 准备保单信息
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean preparePol() {
    System.out.println("P核保标志" + mpassflag);
    mLCPolSchema.setUWFlag(mpassflag); //待人工核保
    mLCPolSchema.setUWCode(mOperate);
    mLCPolSchema.setUWDate(PubFun.getCurrentDate());

    return true;
  }

  /**
   * 准备团体单核保信息
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean CprepareUW() {
    int tuwno = 0;
    LCCUWMasterSchema tLCCUWMasterSchema = new LCCUWMasterSchema();
    LCCUWMasterDB tLCCUWMasterDB = new LCCUWMasterDB();
    tLCCUWMasterDB.setContNo(mOldContPolNo);
    LCCUWMasterSet tLCCUWMasterSet = new LCCUWMasterSet();
    tLCCUWMasterSet = tLCCUWMasterDB.query();
    if (tLCCUWMasterDB.mErrors.needDealError()) {
      // @@错误处理
      this.mErrors.copyAllErrors(tLCCUWMasterDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "ContUWAtuoChkBL";
      tError.functionName = "CprepareUW";
      tError.errorMessage = "LCCUWMaster表取数失败!";
      this.mErrors.addOneError(tError);
      return false;
    }

    int n = tLCCUWMasterSet.size();
    if (n == 0) {
      //tLCCUWMasterSchema.setGrpPolNo( mGrpPolNo );
//      tLCCUWMasterSchema.setProposalNo(mGrpPolNo);
      tLCCUWMasterSchema.setUWNo(1);
      tLCCUWMasterSchema.setContNo("00000000000000000000");
//      tLCCUWMasterSchema.setRiskVersion(mLCGrpPolSchema.getRiskVersion());
//      tLCCUWMasterSchema.setRiskCode(mLCGrpPolSchema.getRiskCode());
      /*Lis5.3 upgrade set
      tLCCUWMasterSchema.setGrpNo(mLCGrpPolSchema.getGrpNo());
      */
      //tLCCUWMasterSchema.setName(mLCGrpPolSchema.getName());
      //tLCCUWMasterSchema.setAppntNo(mLCPolSchema.getAppntNo());
      //tLCCUWMasterSchema.setAppntName(mLCPolSchema.getAppntName());
      tLCCUWMasterSchema.setAgentCode(mLCGrpPolSchema.getAgentGroup());
      //tLPCUWMasterSchema.setAgentGroup(mLCPolSchema.getAgentGroup());
      tLCCUWMasterSchema.setPassFlag(mpassflag); //通过标志
      tLCCUWMasterSchema.setUWGrade(mcuwgrade); //核保级别
      tLCCUWMasterSchema.setAppGrade(mcuwgrade); //申报级别
      tLCCUWMasterSchema.setPostponeDay("");
      tLCCUWMasterSchema.setPostponeDate("");
      tLCCUWMasterSchema.setAutoUWFlag("1"); // 1 自动核保 2 人工核保
      tLCCUWMasterSchema.setState(mpassflag);
      tLCCUWMasterSchema.setManageCom(mLCPolSchema.getManageCom());
      tLCCUWMasterSchema.setUWIdea("");
      tLCCUWMasterSchema.setUpReportContent("");
      tLCCUWMasterSchema.setOperator(mOperate); //操作员
      tLCCUWMasterSchema.setMakeDate(PubFun.getCurrentDate());
      tLCCUWMasterSchema.setMakeTime(PubFun.getCurrentTime());
      tLCCUWMasterSchema.setModifyDate(PubFun.getCurrentDate());
      tLCCUWMasterSchema.setModifyTime(PubFun.getCurrentTime());
    }
    else if (n == 1) {

      tLCCUWMasterSchema = tLCCUWMasterSet.get(1);

      tuwno = tLCCUWMasterSchema.getUWNo();
      tuwno = tuwno + 1;

      tLCCUWMasterSchema.setUWNo(tuwno);
      tLCCUWMasterSchema.setPassFlag(mpassflag); //通过标志
      tLCCUWMasterSchema.setAutoUWFlag("1"); // 1 自动核保 2 人工核保
      tLCCUWMasterSchema.setUWGrade(mcuwgrade); //核保级别
      tLCCUWMasterSchema.setAppGrade(mcuwgrade); //申报级别
      tLCCUWMasterSchema.setOperator(mOperate); //操作员
      tLCCUWMasterSchema.setModifyDate(PubFun.getCurrentDate());
      tLCCUWMasterSchema.setModifyTime(PubFun.getCurrentTime());
    }
    else {
      // @@错误处理
      this.mErrors.copyAllErrors(tLCCUWMasterDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "ContUWAtuoChkBL";
      tError.functionName = "CprepareUW";
      tError.errorMessage = "LCCUWMaster表取数据不唯一!";
      this.mErrors.addOneError(tError);
      return false;
    }

    mLCCUWMasterSet.clear();
    mLCCUWMasterSet.add(tLCCUWMasterSchema);

    // 核保轨迹表
    LCCUWSubSchema tLCCUWSubSchema = new LCCUWSubSchema();
    LCCUWSubDB tLCCUWSubDB = new LCCUWSubDB();
    tLCCUWSubDB.setContNo(mContPolNo);
    LCCUWSubSet tLCCUWSubSet = new LCCUWSubSet();
    tLCCUWSubSet = tLCCUWSubDB.query();
    if (tLCCUWSubDB.mErrors.needDealError()) {
      // @@错误处理
      this.mErrors.copyAllErrors(tLCCUWSubDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "ContUWAutoChkBL";
      tError.functionName = "CprepareUW";
      tError.errorMessage = "LCCUWSub表取数失败!";
      this.mErrors.addOneError(tError);
      return false;
    }

    int m = tLCCUWSubSet.size();
    if (m > 0) {
      m++; //核保次数
      tLCCUWSubSchema = tLCCUWSubSet.get(1);

      tLCCUWSubSchema.setUWNo(m); //第几次核保
//      tLCCUWSubSchema.setUWFlag(mpassflag); //核保意见
      tLCCUWSubSchema.setUWGrade(mcuwgrade); //核保级别
      tLCCUWSubSchema.setAppGrade(mcuwgrade); //申请级别
      tLCCUWSubSchema.setAutoUWFlag("1");
      tLCCUWSubSchema.setOperator(mOperate); //操作员
      tLCCUWSubSchema.setModifyDate(PubFun.getCurrentDate());
      tLCCUWSubSchema.setModifyTime(PubFun.getCurrentTime());
    }
    else {
      //tLCGUWSubSchema.setGrpProposalNo( mGrpPolNo );
      tLCCUWSubSchema.setUWNo(1);
      tLCCUWSubSchema.setContNo(mContPolNo);
      tLCCUWSubSchema.setManageCom(mLCGrpPolSchema.getManageCom());
//      tLCCUWSubSchema.setUWFlag(mpassflag); //核保意见
      tLCCUWSubSchema.setUWGrade(mcuwgrade); //核保级别
      tLCCUWSubSchema.setAppGrade(mcuwgrade); //申请级别
      tLCCUWSubSchema.setPostponeDay("");
      tLCCUWSubSchema.setPostponeDate("");
      tLCCUWSubSchema.setAutoUWFlag("1");
      tLCCUWSubSchema.setUWIdea("");
      tLCCUWSubSchema.setUpReportContent("");
      tLCCUWSubSchema.setState(mpassflag); //状态
      tLCCUWSubSchema.setOperator(mOperate); //操作员
      tLCCUWSubSchema.setMakeDate(PubFun.getCurrentDate());
      tLCCUWSubSchema.setMakeTime(PubFun.getCurrentTime());
      tLCCUWSubSchema.setModifyDate(PubFun.getCurrentDate());
      tLCCUWSubSchema.setModifyTime(PubFun.getCurrentTime());
    }

    mLCCUWSubSet.clear();
    mLCCUWSubSet.add(tLCCUWSubSchema);

    // 核保错误信息表
    LCCUWErrorSchema tLCCUWErrorSchema = new LCCUWErrorSchema();
    LCCUWErrorDB tLCCUWErrorDB = new LCCUWErrorDB();
    tLCCUWErrorDB.setContNo(mContPolNo);
    LCCUWErrorSet tLCCUWErrorSet = new LCCUWErrorSet();
    tLCCUWErrorSet = tLCCUWErrorDB.query();
    if (tLCCUWErrorDB.mErrors.needDealError()) {
      // @@错误处理
      this.mErrors.copyAllErrors(tLCCUWErrorDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "ContUWAutoChkBL";
      tError.functionName = "CprepareUW";
      tError.errorMessage = "LCCError表取数失败!";
      this.mErrors.addOneError(tError);
      return false;
    }

    /**
                     int k = tLCUWErrorSet.size();

                     if ( k > 0)
                     {
            k++; //核保次数
            tLCUWErrorSchema = mLCUWErrorSet.get(k);

            tLCUWErrorSchema.setUWNo(k); //核保次数
                     }
                     else
                     {
     **/
    tLCCUWErrorSchema.setSerialNo("0");
    if (m > 0) {
      tLCCUWErrorSchema.setUWNo(m);
    }
    else {
      tLCCUWErrorSchema.setUWNo(1);
    }
    //tLCCUWErrorSchema.setGrpPolNo(mPolNo);
    //tLCCUWErrorSchema.setContNo("00000000000000000000");
    tLCCUWErrorSchema.setContNo(mContPolNo);
    /*Lis5.3 upgrade set
    tLCCUWErrorSchema.setGrpNo(mLCGrpPolSchema.getGrpNo());
    */
    //tLCCUWErrorSchema.setName(mLCGrpPolSchema.getName());
    //tLCCUWErrorSchema.setAppntNo(mLCPolSchema.getAppntNo());
    //tLCCUWErrorSchema.setAppntName(mLCPolSchema.getAppntName());
    tLCCUWErrorSchema.setManageCom(mLCPolSchema.getManageCom());
    tLCCUWErrorSchema.setUWRuleCode(""); //核保规则编码
    tLCCUWErrorSchema.setUWError(""); //核保出错信息
    tLCCUWErrorSchema.setCurrValue(""); //当前值
    tLCCUWErrorSchema.setModifyDate(PubFun.getCurrentDate());
    tLCCUWErrorSchema.setModifyTime(PubFun.getCurrentTime());
    tLCCUWErrorSchema.setUWPassFlag(mpassflag);

    //}

    //取核保错误信息
    mLCUWErrorSet.clear();
    merrcount = mCLMUWSet.size();
    if (merrcount > 0) {
      for (int i = 1; i <= merrcount; i++) {
        //取出错信息
        LMUWSchema tLMUWSchema = new LMUWSchema();
        tLMUWSchema = mCLMUWSet.get(i);
        //生成流水号
        String tserialno = "" + i;

        tLCCUWErrorSchema.setSerialNo(tserialno);
        tLCCUWErrorSchema.setUWRuleCode(tLMUWSchema.getUWCode()); //核保规则编码
        tLCCUWErrorSchema.setUWError(tLMUWSchema.getRemark().trim()); //核保出错信息
        tLCCUWErrorSchema.setUWGrade(tLMUWSchema.getUWGrade());
        tLCCUWErrorSchema.setCurrValue(""); //当前值

        LCCUWErrorSchema ttLCCUWErrorSchema = new LCCUWErrorSchema();
        ttLCCUWErrorSchema.setSchema(tLCCUWErrorSchema);
        mLCCUWErrorSet.add(ttLCCUWErrorSchema);
      }
    }

    return true;
  }

  /**
   * 准备团体单核保信息
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean GprepareUW() {
    int tuwno = 0;
    LCGUWMasterSchema tLCGUWMasterSchema = new LCGUWMasterSchema();
    LCGUWMasterDB tLCGUWMasterDB = new LCGUWMasterDB();
    tLCGUWMasterDB.setGrpPolNo(mOldGrpPolNo);
    LCGUWMasterSet tLCGUWMasterSet = new LCGUWMasterSet();
    tLCGUWMasterSet = tLCGUWMasterDB.query();
    if (tLCGUWMasterDB.mErrors.needDealError()) {
      // @@错误处理
      this.mErrors.copyAllErrors(tLCGUWMasterDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "UWAtuoChkBL";
      tError.functionName = "prepareUW";
      tError.errorMessage = "LCUWMaster表取数失败!";
      this.mErrors.addOneError(tError);
      return false;
    }

    int n = tLCGUWMasterSet.size();
    if (n == 0) {
      tLCGUWMasterSchema.setGrpPolNo(mGrpPolNo);
      tLCGUWMasterSchema.setGrpProposalNo(mGrpPolNo);
      tLCGUWMasterSchema.setUWNo(1);
//      tLCGUWMasterSchema.setContNo("00000000000000000000");
//      tLCGUWMasterSchema.setRiskVersion(mLCGrpPolSchema.getRiskVersion());
//      tLCGUWMasterSchema.setRiskCode(mLCGrpPolSchema.getRiskCode());
      /*Lis5.3 upgrade set
      tLCGUWMasterSchema.setGrpNo(mLCGrpPolSchema.getGrpNo());
      */
      //tLCGUWMasterSchema.setName(mLCGrpPolSchema.getName());
      //tLCGUWMasterSchema.setAppntNo(mLCPolSchema.getAppntNo());
      //tLCGUWMasterSchema.setAppntName(mLCPolSchema.getAppntName());
      tLCGUWMasterSchema.setAgentCode(mLCGrpPolSchema.getAgentGroup());
      //tLPGUWMasterSchema.setAgentGroup(mLCPolSchema.getAgentGroup());
      tLCGUWMasterSchema.setPassFlag(mpassflag); //通过标志
      tLCGUWMasterSchema.setUWGrade(mguwgrade); //核保级别
      tLCGUWMasterSchema.setAppGrade(mguwgrade); //申报级别
      tLCGUWMasterSchema.setPostponeDay("");
      tLCGUWMasterSchema.setPostponeDate("");
      tLCGUWMasterSchema.setAutoUWFlag("1"); // 1 自动核保 2 人工核保
      tLCGUWMasterSchema.setState("");
      tLCGUWMasterSchema.setManageCom(mLCPolSchema.getManageCom());
      tLCGUWMasterSchema.setUWIdea("");
      tLCGUWMasterSchema.setUpReportContent("");
      tLCGUWMasterSchema.setOperator(mOperate); //操作员
      tLCGUWMasterSchema.setMakeDate(PubFun.getCurrentDate());
      tLCGUWMasterSchema.setMakeTime(PubFun.getCurrentTime());
      tLCGUWMasterSchema.setModifyDate(PubFun.getCurrentDate());
      tLCGUWMasterSchema.setModifyTime(PubFun.getCurrentTime());
    }
    else if (n == 1) {

      tLCGUWMasterSchema = tLCGUWMasterSet.get(1);

      tuwno = tLCGUWMasterSchema.getUWNo();
      tuwno = tuwno + 1;

      tLCGUWMasterSchema.setUWNo(tuwno);
      tLCGUWMasterSchema.setPassFlag(mpassflag); //通过标志
      tLCGUWMasterSchema.setAutoUWFlag("1"); // 1 自动核保 2 人工核保
      tLCGUWMasterSchema.setUWGrade(mguwgrade); //核保级别
      tLCGUWMasterSchema.setAppGrade(mguwgrade); //申报级别
      tLCGUWMasterSchema.setOperator(mOperate); //操作员
      tLCGUWMasterSchema.setModifyDate(PubFun.getCurrentDate());
      tLCGUWMasterSchema.setModifyTime(PubFun.getCurrentTime());
    }
    else {
      // @@错误处理
      this.mErrors.copyAllErrors(tLCGUWMasterDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "ContUWAtuoChkBL";
      tError.functionName = "GprepareUW";
      tError.errorMessage = "LCGUWMaster表取数据不唯一!";
      this.mErrors.addOneError(tError);
      return false;
    }

    mLCGUWMasterSet.clear();
    mLCGUWMasterSet.add(tLCGUWMasterSchema);

    // 核保轨迹表
    LCGUWSubSchema tLCGUWSubSchema = new LCGUWSubSchema();
    LCGUWSubDB tLCGUWSubDB = new LCGUWSubDB();
    tLCGUWSubDB.setGrpPolNo(mGrpPolNo);
    LCGUWSubSet tLCGUWSubSet = new LCGUWSubSet();
    tLCGUWSubSet = tLCGUWSubDB.query();
    if (tLCGUWSubDB.mErrors.needDealError()) {
      // @@错误处理
      this.mErrors.copyAllErrors(tLCGUWSubDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "ContUWAutoChkBL";
      tError.functionName = "GprepareUW";
      tError.errorMessage = "LCGUWSub表取数失败!";
      this.mErrors.addOneError(tError);
      return false;
    }

    int m = tLCGUWSubSet.size();
    if (m > 0) {
      m++; //核保次数
      tLCGUWSubSchema = tLCGUWSubSet.get(1);

      tLCGUWSubSchema.setUWNo(m); //第几次核保
//      tLCGUWSubSchema.setUWFlag(mpassflag); //核保意见
      tLCGUWSubSchema.setUWGrade(mguwgrade); //核保级别
      tLCGUWSubSchema.setAppGrade(mguwgrade); //申请级别
      tLCGUWSubSchema.setAutoUWFlag("1");
      tLCGUWSubSchema.setOperator(mOperate); //操作员
      tLCGUWSubSchema.setModifyDate(PubFun.getCurrentDate());
      tLCGUWSubSchema.setModifyTime(PubFun.getCurrentTime());
    }
    else {
      //tLCGUWSubSchema.setGrpProposalNo( mGrpPolNo );
      tLCGUWSubSchema.setUWNo(1);
      tLCGUWSubSchema.setGrpPolNo(mGrpPolNo);
      tLCGUWSubSchema.setManageCom(mLCGrpPolSchema.getManageCom());
//      tLCGUWSubSchema.setUWFlag(mpassflag); //核保意见
      tLCGUWSubSchema.setUWGrade(mguwgrade); //核保级别
      tLCGUWSubSchema.setAppGrade(mguwgrade); //申请级别
      tLCGUWSubSchema.setPostponeDay("");
      tLCGUWSubSchema.setPostponeDate("");
      tLCGUWSubSchema.setAutoUWFlag("1");
      tLCGUWSubSchema.setUWIdea("");
      tLCGUWSubSchema.setUpReportContent("");
      tLCGUWSubSchema.setState(mpassflag); //状态
      tLCGUWSubSchema.setOperator(mOperate); //操作员
      tLCGUWSubSchema.setMakeDate(PubFun.getCurrentDate());
      tLCGUWSubSchema.setMakeTime(PubFun.getCurrentTime());
      tLCGUWSubSchema.setModifyDate(PubFun.getCurrentDate());
      tLCGUWSubSchema.setModifyTime(PubFun.getCurrentTime());
    }

    mLCGUWSubSet.clear();
    mLCGUWSubSet.add(tLCGUWSubSchema);

    // 核保错误信息表
    LCGUWErrorSchema tLCGUWErrorSchema = new LCGUWErrorSchema();
    LCGUWErrorDB tLCGUWErrorDB = new LCGUWErrorDB();
    tLCGUWErrorDB.setGrpPolNo(mGrpPolNo);
    LCGUWErrorSet tLCGUWErrorSet = new LCGUWErrorSet();
    tLCGUWErrorSet = tLCGUWErrorDB.query();
    if (tLCGUWErrorDB.mErrors.needDealError()) {
      // @@错误处理
      this.mErrors.copyAllErrors(tLCGUWErrorDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "ContUWAtuoChkBL";
      tError.functionName = "GprepareUW";
      tError.errorMessage = "LCCError表取数失败!";
      this.mErrors.addOneError(tError);
      return false;
    }

    /**
                     int k = tLCUWErrorSet.size();

                     if ( k > 0)
                     {
            k++; //核保次数
            tLCUWErrorSchema = mLCUWErrorSet.get(k);

            tLCUWErrorSchema.setUWNo(k); //核保次数
                     }
                     else
                     {
     **/
    tLCGUWErrorSchema.setSerialNo("0");
    if (m > 0) {
      tLCGUWErrorSchema.setUWNo(m);
    }
    else {
      tLCGUWErrorSchema.setUWNo(1);
    }
    tLCGUWErrorSchema.setGrpPolNo(mPolNo);
    //tLCGUWErrorSchema.setContNo("00000000000000000000");
    tLCGUWErrorSchema.setGrpPolNo(mPolNo);
    /*Lis5.3 upgrade set
    tLCGUWErrorSchema.setGrpNo(mLCGrpPolSchema.getGrpNo());
    */
    //tLCGUWErrorSchema.setName(mLCGrpPolSchema.getName());
    //tLCGUWErrorSchema.setAppntNo(mLCPolSchema.getAppntNo());
    //tLCGUWErrorSchema.setAppntName(mLCPolSchema.getAppntName());
    tLCGUWErrorSchema.setManageCom(mLCPolSchema.getManageCom());
    tLCGUWErrorSchema.setUWRuleCode(""); //核保规则编码
    tLCGUWErrorSchema.setUWError(""); //核保出错信息
    tLCGUWErrorSchema.setCurrValue(""); //当前值
    tLCGUWErrorSchema.setModifyDate(PubFun.getCurrentDate());
    tLCGUWErrorSchema.setModifyTime(PubFun.getCurrentTime());
    tLCGUWErrorSchema.setUWPassFlag(mpassflag);

    //}

    //取核保错误信息
    mLCUWErrorSet.clear();
    merrcount = mGLMUWSet.size();
    if (merrcount > 0) {
      for (int i = 1; i <= merrcount; i++) {
        //取出错信息
        LMUWSchema tLMUWSchema = new LMUWSchema();
        tLMUWSchema = mGLMUWSet.get(i);
        //生成流水号
        String tserialno = "" + i;

        tLCGUWErrorSchema.setSerialNo(tserialno);
        tLCGUWErrorSchema.setUWRuleCode(tLMUWSchema.getUWCode()); //核保规则编码
        tLCGUWErrorSchema.setUWError(tLMUWSchema.getRemark().trim()); //核保出错信息
        tLCGUWErrorSchema.setUWGrade(tLMUWSchema.getUWGrade());
        tLCGUWErrorSchema.setCurrValue(""); //当前值

        LCGUWErrorSchema ttLCGUWErrorSchema = new LCGUWErrorSchema();
        ttLCGUWErrorSchema.setSchema(tLCGUWErrorSchema);
        mLCGUWErrorSet.add(ttLCGUWErrorSchema);
      }
    }

    return true;
  }

  /**
   * 准备个人单核保信息
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean prepareUW() {
    int tuwno = 0;
    LCUWMasterSchema tLCUWMasterSchema = new LCUWMasterSchema();
    LCUWMasterDB tLCUWMasterDB = new LCUWMasterDB();
    tLCUWMasterDB.setPolNo(mOldPolNo);
    LCUWMasterSet tLCUWMasterSet = new LCUWMasterSet();
    tLCUWMasterSet = tLCUWMasterDB.query();
    if (tLCUWMasterDB.mErrors.needDealError()) {
      // @@错误处理
      this.mErrors.copyAllErrors(tLCUWMasterDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "UWAtuoChkBL";
      tError.functionName = "prepareUW";
      tError.errorMessage = "LCUWMaster表取数失败!";
      this.mErrors.addOneError(tError);
      return false;
    }

    int n = tLCUWMasterSet.size();
    if (n == 0) {
      tLCUWMasterSchema.setPolNo(mPolNo);
      tLCUWMasterSchema.setProposalNo(mPolNo);
      tLCUWMasterSchema.setUWNo(1);
      tLCUWMasterSchema.setContNo("");
//      tLCUWMasterSchema.setGrpPolNo("00000000000000000000");
//      tLCUWMasterSchema.setRiskVersion(mLCPolSchema.getRiskVersion());
//      tLCUWMasterSchema.setRiskCode(mLCPolSchema.getRiskCode());
      tLCUWMasterSchema.setInsuredNo(mLCPolSchema.getInsuredNo());
      tLCUWMasterSchema.setInsuredName(mLCPolSchema.getInsuredName());
      tLCUWMasterSchema.setAppntNo(mLCPolSchema.getAppntNo());
      tLCUWMasterSchema.setAppntName(mLCPolSchema.getAppntName());
      tLCUWMasterSchema.setAgentCode(mLCPolSchema.getAgentGroup());
      tLCUWMasterSchema.setAgentGroup(mLCPolSchema.getAgentGroup());
      tLCUWMasterSchema.setPassFlag(mpassflag); //通过标志
      tLCUWMasterSchema.setUWGrade(muwgrade); //核保级别
      tLCUWMasterSchema.setAppGrade(muwgrade); //申报级别
      tLCUWMasterSchema.setPostponeDay("");
      tLCUWMasterSchema.setPostponeDate("");
      tLCUWMasterSchema.setAutoUWFlag("1"); // 1 自动核保 2 人工核保
      tLCUWMasterSchema.setState(mpassflag);
      tLCUWMasterSchema.setManageCom(mLCPolSchema.getManageCom());
      tLCUWMasterSchema.setUWIdea("");
      tLCUWMasterSchema.setUpReportContent("");
      tLCUWMasterSchema.setOperator(mOperate); //操作员
      tLCUWMasterSchema.setMakeDate(PubFun.getCurrentDate());
      tLCUWMasterSchema.setMakeTime(PubFun.getCurrentTime());
      tLCUWMasterSchema.setModifyDate(PubFun.getCurrentDate());
      tLCUWMasterSchema.setModifyTime(PubFun.getCurrentTime());
    }
    else if (n == 1) {

      tLCUWMasterSchema = tLCUWMasterSet.get(1);

      tuwno = tLCUWMasterSchema.getUWNo();
      tuwno = tuwno + 1;

      tLCUWMasterSchema.setUWNo(tuwno);
      tLCUWMasterSchema.setPassFlag(mpassflag); //通过标志
      tLCUWMasterSchema.setAutoUWFlag("1"); // 1 自动核保 2 人工核保
      tLCUWMasterSchema.setUWGrade(muwgrade); //核保级别
      tLCUWMasterSchema.setAppGrade(muwgrade); //申报级别
      tLCUWMasterSchema.setOperator(mOperate); //操作员
      tLCUWMasterSchema.setModifyDate(PubFun.getCurrentDate());
      tLCUWMasterSchema.setModifyTime(PubFun.getCurrentTime());
    }
    else {
      // @@错误处理
      this.mErrors.copyAllErrors(tLCUWMasterDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "UWAtuoChkBL";
      tError.functionName = "prepareUW";
      tError.errorMessage = "LCUWMaster表取数据不唯一!";
      this.mErrors.addOneError(tError);
      return false;
    }

    mLCUWMasterSet.clear();
    mLCUWMasterSet.add(tLCUWMasterSchema);

    // 核保轨迹表
    LCUWSubSchema tLCUWSubSchema = new LCUWSubSchema();
    LCUWSubDB tLCUWSubDB = new LCUWSubDB();
    tLCUWSubDB.setPolNo(mOldPolNo);
    LCUWSubSet tLCUWSubSet = new LCUWSubSet();
    tLCUWSubSet = tLCUWSubDB.query();
    if (tLCUWSubDB.mErrors.needDealError()) {
      // @@错误处理
      this.mErrors.copyAllErrors(tLCUWSubDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "ContProposalSignBL";
      tError.functionName = "prepareUW";
      tError.errorMessage = "LCUWSub表取数失败!";
      this.mErrors.addOneError(tError);
      return false;
    }

    int m = tLCUWSubSet.size();
    if (m > 0) {
      m++; //核保次数
      tLCUWSubSchema = tLCUWSubSet.get(1);

      tLCUWSubSchema.setUWNo(m); //第几次核保
//      tLCUWSubSchema.setUWFlag(mpassflag); //核保意见
      tLCUWSubSchema.setUWGrade(muwgrade); //核保级别
      tLCUWSubSchema.setAppGrade(muwgrade); //申请级别
      tLCUWSubSchema.setAutoUWFlag("1");
      tLCUWSubSchema.setOperator(mOperate); //操作员
      tLCUWSubSchema.setModifyDate(PubFun.getCurrentDate());
      tLCUWSubSchema.setModifyTime(PubFun.getCurrentTime());
    }
    else {
      tLCUWSubSchema.setProposalNo(mPolNo);
      tLCUWSubSchema.setUWNo(1);
      tLCUWSubSchema.setPolNo(mPolNo);
      tLCUWSubSchema.setManageCom(mLCPolSchema.getManageCom());
//      tLCUWSubSchema.setUWFlag(mpassflag); //核保意见
      tLCUWSubSchema.setUWGrade(muwgrade); //核保级别
      tLCUWSubSchema.setAppGrade(muwgrade); //申请级别
      tLCUWSubSchema.setPostponeDay("");
      tLCUWSubSchema.setPostponeDate("");
      tLCUWSubSchema.setAutoUWFlag("1");
      tLCUWSubSchema.setUWIdea("");
      tLCUWSubSchema.setUpReportContent("");
      tLCUWSubSchema.setState(mpassflag); //状态
      tLCUWSubSchema.setOperator(mOperate); //操作员
      tLCUWSubSchema.setMakeDate(PubFun.getCurrentDate());
      tLCUWSubSchema.setMakeTime(PubFun.getCurrentTime());
      tLCUWSubSchema.setModifyDate(PubFun.getCurrentDate());
      tLCUWSubSchema.setModifyTime(PubFun.getCurrentTime());
    }

    mLCUWSubSet.clear();
    mLCUWSubSet.add(tLCUWSubSchema);

    // 核保错误信息表
    LCUWErrorSchema tLCUWErrorSchema = new LCUWErrorSchema();
    LCUWErrorDB tLCUWErrorDB = new LCUWErrorDB();
    tLCUWErrorDB.setPolNo(mOldPolNo);
    LCUWErrorSet tLCUWErrorSet = new LCUWErrorSet();
    tLCUWErrorSet = tLCUWErrorDB.query();
    if (tLCUWErrorDB.mErrors.needDealError()) {
      // @@错误处理
      this.mErrors.copyAllErrors(tLCUWErrorDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "ContProposalSignBL";
      tError.functionName = "prepareUW";
      tError.errorMessage = "LCErr表取数失败!";
      this.mErrors.addOneError(tError);
      return false;
    }

    /**
       int k = tLCUWErrorSet.size();

       if ( k > 0)
       {
                          k++; //核保次数
     tLCUWErrorSchema = mLCUWErrorSet.get(k);

                          tLCUWErrorSchema.setUWNo(k); //核保次数
       }
       else
       {
     **/
    tLCUWErrorSchema.setSerialNo("0");
    if (m > 0) {
      tLCUWErrorSchema.setUWNo(m);
    }
    else {
      tLCUWErrorSchema.setUWNo(1);
    }
    tLCUWErrorSchema.setPolNo(mPolNo);
    tLCUWErrorSchema.setContNo("");
//    tLCUWErrorSchema.setGrpPolNo("");
    tLCUWErrorSchema.setInsuredNo(mLCPolSchema.getInsuredNo());
    tLCUWErrorSchema.setInsuredName(mLCPolSchema.getInsuredName());
    tLCUWErrorSchema.setAppntNo(mLCPolSchema.getAppntNo());
    tLCUWErrorSchema.setAppntName(mLCPolSchema.getAppntName());
    tLCUWErrorSchema.setManageCom(mLCPolSchema.getManageCom());
    tLCUWErrorSchema.setUWRuleCode(""); //核保规则编码
    tLCUWErrorSchema.setUWError(""); //核保出错信息
    tLCUWErrorSchema.setCurrValue(""); //当前值
    tLCUWErrorSchema.setModifyDate(PubFun.getCurrentDate());
    tLCUWErrorSchema.setModifyTime(PubFun.getCurrentTime());
    tLCUWErrorSchema.setUWPassFlag(mpassflag);

    //}

    //取核保错误信息
    mLCUWErrorSet.clear();
    merrcount = mLMUWSet.size();
    if (merrcount > 0) {
      for (int i = 1; i <= merrcount; i++) {
        //取出错信息
        LMUWSchema tLMUWSchema = new LMUWSchema();
        tLMUWSchema = mLMUWSet.get(i);
        //生成流水号
        String tserialno = "" + i;

        tLCUWErrorSchema.setSerialNo(tserialno);
        tLCUWErrorSchema.setUWRuleCode(tLMUWSchema.getUWCode()); //核保规则编码
        tLCUWErrorSchema.setUWError(tLMUWSchema.getRemark().trim()); //核保出错信息
        tLCUWErrorSchema.setUWGrade(tLMUWSchema.getUWGrade());
        tLCUWErrorSchema.setCurrValue(""); //当前值

        LCUWErrorSchema ttLCUWErrorSchema = new LCUWErrorSchema();
        ttLCUWErrorSchema.setSchema(tLCUWErrorSchema);
        mLCUWErrorSet.add(ttLCUWErrorSchema);
      }
    }

    return true;
  }

  /**
   * 准备需要保存的数据
   */
  private void prepareOutputData() {
    mInputData.clear();
    mInputData.add(mAllLCContSet);
    mInputData.add(mAllLCGrpPolSet);
    mInputData.add(mAllLCPolSet);
    mInputData.add(mAllLCCUWMasterSet);
    mInputData.add(mAllLCCUWSubSet);
    mInputData.add(mAllLCCErrSet);
    mInputData.add(mAllLCGUWMasterSet);
    mInputData.add(mAllLCGUWSubSet);
    mInputData.add(mAllLCGErrSet);
    mInputData.add(mAllLCUWMasterSet);
    mInputData.add(mAllLCUWSubSet);
    mInputData.add(mAllLCErrSet);
  }

}
