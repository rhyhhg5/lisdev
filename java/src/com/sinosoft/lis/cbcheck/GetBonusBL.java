package com.sinosoft.lis.cbcheck;

import java.lang.*;
import java.util.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.cbcheck.*;
import java.text.*;
import java.sql.*;

/**
 * <p>Title: Web业务系统分红处理功能部分</p>
 * <p>Description: 逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author WHN
 * @version 1.0
 */
public class GetBonusBL
{
        /** 错误处理类，每个需要错误处理的类中都放置该类 */
        public  CErrors mErrors = new CErrors();
        /** 往后面传输数据的容器 */
        private VData mInputData ;
        /** 往界面传输数据的容器 */
        private VData mResult = new VData();
        /** 数据操作字符串 */
        private String mOperate;
        private String mIsueManageCom;
        private String mManageCom;
        private String mpassflag; //通过标记
        private int merrcount; //错误条数
        private String mCalCode; //计算编码
        private String mUser;
        private FDate fDate = new FDate();
        private double mValue;
        private String mInsuredNo = "";
        private String mBackObj = "";
        private String mflag = ""; //分，个单标记

        /** 业务处理相关变量 */
        private LCPolSet mLCPolSet = new LCPolSet();
        private LCPolSet mmLCPolSet = new LCPolSet();
        private LCPolSet m2LCPolSet = new LCPolSet();
        private LCPolSet mAllLCPolSet = new LCPolSet();
        private LCPolSchema mLCPolSchema = new LCPolSchema();
        private String mPolNo = "";
        private String mOldPolNo = "";
        /** 集体单表 */
        private LCGrpPolSet mLCGrpPolSet = new LCGrpPolSet();
        private LCGrpPolSchema mLCGrpPolSchema = new LCGrpPolSchema();
        /** 保费项表 */
        private LCPremSet mLCPremSet = new LCPremSet();
        /** 领取项表 */
        private LCGetSet mLCGetSet = new LCGetSet();
        /** 责任表 */
        private LCDutySet mLCDutySet = new LCDutySet();

        /** 暂交费表 */
        private LJTempFeeSet mLJTempFeeSet = new LJTempFeeSet();

        /** 暂交费关联表 */
        private LJTempFeeClassSchema mLJTempFeeClassSchema = new LJTempFeeClassSchema();
        private LJTempFeeClassSet mLJTempFeeClassSet = new LJTempFeeClassSet();
        private LJTempFeeClassSet mmLJTempFeeClassSet = new LJTempFeeClassSet();
        /** 分红计算表 */
        private LOBonusPolSchema mLOBonusPolSchema = new LOBonusPolSchema();
        private LOBonusPolSet mLOBonusPolSet = new LOBonusPolSet();

        private LOBonusPolSet m2LOBonusPolSet = new LOBonusPolSet();

        /** 实收总表 */
        private LJAPaySet mLJAPaySet = new LJAPaySet();

        /** 个人实收表 */
        private LJAPayPersonSet mLJAPayPersonSet = new LJAPayPersonSet();

        /** 给付总表*/
        private LJAGetSet mLJAGetSet = new LJAGetSet();

        /** 红利给付表 */
        private LJABonusGetSet mLJABonusGetSet = new LJABonusGetSet();

        /** 帐户表 */
        private LCInsureAccSet mLCInsureAccSet = new LCInsureAccSet();

        /** 帐户轨迹表 */
        private LCInsureAccTraceSet mLCInsureAccTraceSet = new LCInsureAccTraceSet();

        /** 财务给付总表 */
        private LJFIGetSet mLJFIGetSet = new LJFIGetSet();


        /*转换精确位数的对象   */
        private String FORMATMODOL="0.00";//保费保额计算出来后的精确位数
        private DecimalFormat mDecimalFormat=new DecimalFormat(FORMATMODOL);//数字转换对象

        /**计算公式表**/
        private LMUWSchema mLMUWSchema = new LMUWSchema();
        //private LMUWDBSet mLMUWDBSet = new LMUWDBSet();
        private LMUWSet mLMUWSet = new LMUWSet();

        private CalBase mCalBase = new CalBase();

        public GetBonusBL() {}

        /**
        * 传输数据的公共方法
        * @param: cInputData 输入的数据
        *         cOperate 数据操作
        * @return:
        */
        public boolean submitData(VData cInputData,String cOperate)
        {
            mInputData = (VData)cInputData.clone();

            if (!getInputData(cInputData))
                return false;

            String tLimit = PubFun.getNoLimit(mManageCom);
            String tSerialNo = PubFun1.CreateMaxNo( "SERIALNO", tLimit );//产生流水号码

            boolean errflag = false; //判断是不是所有数据都不成功
            for (int i = 1;i <= mLOBonusPolSet.size(); i++)
            {
                mErrors.clearErrors();
                LOBonusPolSchema tLOBonusPolSchema = ( LOBonusPolSchema )mLOBonusPolSet.get( i );
                // 数据操作业务处理
                if (!dealData(tLOBonusPolSchema))
                {
                    insertErrLog(tSerialNo,tLOBonusPolSchema.getPolNo(),mErrors.getLastError()," ");
                    errflag=true;
                    continue;
                }
            }

            if (errflag == true)
            {
                CError.buildErr(this,"分红处理部分数据失败!请察看红利错误纪录表");
                return false;
            }
            System.out.println("---CutBonusBL dealData---");
            return true;
        }

        /**
        * 数据操作类业务处理
        * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
        */
        private boolean dealData(LOBonusPolSchema tLOBonusPolSchema)
        {
                init();

                if (prepareFee(tLOBonusPolSchema) == false)
                        return false;

                //准备给后台的数据
                prepareOutputData();
                //数据提交
                GetBonusBLS tGetBonusBLS = new GetBonusBLS();
                if (!tGetBonusBLS.submitData(mInputData,mOperate))
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tGetBonusBLS.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "CutBonusBL";
                    tError.functionName = "submitData";
                    tError.errorMessage = "数据提交失败!";
                    this.mErrors .addOneError(tError) ;
                    return false;
                }
                System.out.println("---CutBonusBL commitData---");

                return true;
        }

        /**
        * 从输入数据中得到所有对象
        *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
        */
        private boolean getInputData(VData cInputData)
        {
            GlobalInput tGlobalInput = ((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
            mLOBonusPolSet=((LOBonusPolSet)cInputData.getObjectByObjectName("LOBonusPolSet",0));

            if (mLOBonusPolSet.size() == 0||tGlobalInput==null)
            {
                CError.buildErr(this, "没有传入数据!");
                return false;
            }
            mOperate = tGlobalInput.Operator;
            mManageCom = tGlobalInput.ManageCom;
            return true;
        }

        /**
        * 准备分红信息
        * 输出：如果发生错误则返回false,否则返回true
        */
        private boolean prepareFee(LOBonusPolSchema tLOBonusPolSchema)
        {
               LCPolDB tLCPolDB = new LCPolDB();
               tLCPolDB.setPolNo(tLOBonusPolSchema.getPolNo());
                if(tLCPolDB.getInfo() == false)
                {
                  CError.buildErr(this,"没有"+tLOBonusPolSchema.getPolNo()+"保单!");
                  return false;
                }
                LCPolSchema tLCPolSchema = tLCPolDB.getSchema();

                LOBonusPolDB tLOBonusPolDB=new LOBonusPolDB();
                tLOBonusPolDB.setPolNo(tLOBonusPolSchema.getPolNo());
                tLOBonusPolDB.setFiscalYear(tLOBonusPolSchema.getFiscalYear());
                if(tLOBonusPolDB.getInfo()==false)
                {
                    CError.buildErr(this,"没有"+tLOBonusPolSchema.getPolNo()+"保单红利纪录!");
                    return false;
                }

                mLOBonusPolSchema=tLOBonusPolDB.getSchema();
                if(mLOBonusPolSchema.getBonusFlag().equals("1"))
                {
                    CError.buildErr(this,tLOBonusPolSchema.getPolNo()+"红利已经分配!");
                    return false;
                }
                if (mLOBonusPolSchema.getBonusMoney()==0)
                {
                    updateLCPolBonusForZero(tLCPolSchema,mLOBonusPolSchema.getFiscalYear());
                    return true;
                }
                int tDay=PubFun.calInterval(tLCPolSchema.getPaytoDate(),mLOBonusPolSchema.getSGetDate(),"D");
                if(tDay>0)
                {
                    CError.buildErr(this,tLOBonusPolSchema.getPolNo()+"保单的交至日期小于红利应分配日期!");
                    return false;
                }
                int tDay2=PubFun.calInterval(mLOBonusPolSchema.getSGetDate(),PubFun.getCurrentDate(),"D");
                if(tDay2<0)
                {
                    CError.buildErr(this,tLOBonusPolSchema.getPolNo()+"未到红利应分配日期，不能分配该保单红利");
                    return false;
                }

                String tflag = tLCPolDB.getBonusGetMode();
                if(tflag==null)
                {
                    CError.buildErr(this,tLOBonusPolSchema.getPolNo()+"保单的红利领取方式为空!");
                    return false;
                }
                if (tflag.equals("2")) //现金
                {
                  if(!CutCash(tLCPolSchema))
                    return false;
                }

                if(tflag.equals("3")) //保费 (集体单不容许个人抵交保费)
                {

                    if(!CutPrem(tLCPolSchema))
                        return false;
                }

                if(tflag.equals("1")) //累计生息-帐户
                {
                  if (!CutAcc(tLCPolSchema))
                    return false;
                }
                if(tflag.equals("5")) //增额交清
                {
                  if (!CutByAddPrem(tLCPolSchema))
                    return false;
                }

                return true;
        }

        /**
         *现金
         **/
        private boolean CutCash(LCPolSchema tLCPolSchema)
        {
          //给付总表
          LJAGetSchema tLJAGetSchema = new LJAGetSchema();
          LCBnfSchema tLCBnfSchema = new LCBnfSchema();
          LCBnfSet tLCBnfSet = new LCBnfSet();
          LCBnfDB tLCBnfDB = new LCBnfDB();
          String tLimit=PubFun.getNoLimit(tLCPolSchema.getManageCom());
          String tGetNo = PubFun1.CreateMaxNo("GETNO",tLimit);
          String tNoticeNo = PubFun1.CreateMaxNo("GETNOTICENO",tLimit);

          String tDrawerID="";
          LCAppntIndDB tLCAppntIndDB = new LCAppntIndDB();
          tLCAppntIndDB.setPolNo(tLCPolSchema.getPolNo());
          tLCAppntIndDB.setCustomerNo(tLCPolSchema.getAppntNo());
          if(tLCAppntIndDB.getInfo()==false)
          {
              tDrawerID="";
          }
          else
          {
             tDrawerID = tLCAppntIndDB.getIDNo();
        }
          ////取红利受益人
          tLCBnfDB.setPolNo(tLCPolSchema.getPolNo());
          tLCBnfDB.setBnfType("2");
          tLCBnfSet = tLCBnfDB.query();
          if (tLCBnfSet.size() > 0)
          {
            tLCBnfSchema = tLCBnfSet.get(1);
            tLJAGetSchema.setDrawer(tLCBnfSchema.getName());
            tLJAGetSchema.setDrawerID(tLCBnfSchema.getIDNo());
          }
          else
          {
            LCInsuredDB tLCInsuredDB = new LCInsuredDB();
            /*Lis5.3 upgrade set
            tLCInsuredDB.setPolNo(tLCPolSchema.getPolNo());
            tLCInsuredDB.setCustomerNo(tLCPolSchema.getInsuredNo());
            */
            if(!tLCInsuredDB.getInfo())
            {
              // @@错误处理
              this.mErrors.copyAllErrors( tLCInsuredDB.mErrors );
              CError tError = new CError();
              tError.moduleName = "CutBonusBL";
              tError.functionName = "CutCash";
              tError.errorMessage = tLCPolSchema.getPolNo()+"保单没有被保人信息!";
              this.mErrors .addOneError(tError) ;
              return false;
            }
            tLJAGetSchema.setDrawer(tLCPolSchema.getInsuredName());
            /*Lis5.3 upgrade get
            tLJAGetSchema.setDrawerID(tLCInsuredDB.getIDNo());
            */
          }

          tLJAGetSchema.setActuGetNo(tGetNo);
          tLJAGetSchema.setOtherNo(tLCPolSchema.getPolNo());
          tLJAGetSchema.setOtherNoType("7");
          tLJAGetSchema.setPayMode("1");
          tLJAGetSchema.setAppntNo(tLCPolSchema.getAppntNo());
          tLJAGetSchema.setSumGetMoney(mLOBonusPolSchema.getBonusMoney());
          tLJAGetSchema.setSaleChnl(tLCPolSchema.getSaleChnl());
          tLJAGetSchema.setShouldDate(mLOBonusPolSchema.getSGetDate());
          tLJAGetSchema.setEnterAccDate(" ");
          tLJAGetSchema.setConfDate(" ");
          tLJAGetSchema.setApproveCode(tLCPolSchema.getApproveCode());
          tLJAGetSchema.setApproveDate(tLCPolSchema.getApproveDate());
          tLJAGetSchema.setGetNoticeNo(tNoticeNo);
          /*Lis5.3 upgrade get
          tLJAGetSchema.setBankCode(tLCPolSchema.getBankCode());
          tLJAGetSchema.setBankAccNo(tLCPolSchema.getBankAccNo());
          */
          tLJAGetSchema.setDrawer(tLCPolSchema.getAppntName());
          tLJAGetSchema.setDrawerID(tDrawerID);
          tLJAGetSchema.setSerialNo("0");
          tLJAGetSchema.setOperator(mOperate);
          tLJAGetSchema.setMakeDate(PubFun.getCurrentDate());
          tLJAGetSchema.setMakeTime(PubFun.getCurrentTime());
          tLJAGetSchema.setModifyDate(PubFun.getCurrentDate());
          tLJAGetSchema.setModifyTime(PubFun.getCurrentTime());
          tLJAGetSchema.setBankOnTheWayFlag("");
          tLJAGetSchema.setBankSuccFlag("");
          tLJAGetSchema.setSendBankCount("");
          tLJAGetSchema.setManageCom(tLCPolSchema.getManageCom());
          tLJAGetSchema.setAgentCom(tLCPolSchema.getAgentCom());
          tLJAGetSchema.setAgentType(tLCPolSchema.getAgentType());
          tLJAGetSchema.setAgentCode(tLCPolSchema.getAgentCode());
          tLJAGetSchema.setAgentGroup(tLCPolSchema.getAgentGroup());

          mLJAGetSet.add(tLJAGetSchema);

          //红利给付表
          LJABonusGetSchema tLJABonusGetSchema = new LJABonusGetSchema();
          tLJABonusGetSchema.setActuGetNo(tGetNo);
          tLJABonusGetSchema.setOtherNo(tLCPolSchema.getPolNo());
          tLJABonusGetSchema.setOtherNoType("7");
          tLJABonusGetSchema.setBonusYear(String.valueOf(mLOBonusPolSchema.getFiscalYear()));
          tLJABonusGetSchema.setPayMode("1");
          tLJABonusGetSchema.setGetMoney(mLOBonusPolSchema.getBonusMoney());
          tLJABonusGetSchema.setGetDate(mLOBonusPolSchema.getSGetDate());
          tLJABonusGetSchema.setEnterAccDate("");
          tLJABonusGetSchema.setConfDate("");
          tLJABonusGetSchema.setManageCom(tLCPolSchema.getManageCom());
          tLJABonusGetSchema.setAgentCom(tLCPolSchema.getAgentCom());
          tLJABonusGetSchema.setAgentType(tLCPolSchema.getAgentType());
          tLJABonusGetSchema.setAPPntName(tLCPolSchema.getAppntName());
          tLJABonusGetSchema.setAgentGroup(tLCPolSchema.getAgentGroup());
          tLJABonusGetSchema.setAgentCode(tLCPolSchema.getAgentCode());
          tLJABonusGetSchema.setFeeOperationType("HLTF");
          tLJABonusGetSchema.setFeeFinaType("HLTF");
          tLJABonusGetSchema.setSerialNo("0");
          tLJABonusGetSchema.setOperator(mOperate);
          tLJABonusGetSchema.setMakeTime(PubFun.getCurrentTime());
          tLJABonusGetSchema.setMakeDate(PubFun.getCurrentDate());
          tLJABonusGetSchema.setState("0");
          tLJABonusGetSchema.setGetNoticeNo(tNoticeNo);
          tLJABonusGetSchema.setModifyDate(PubFun.getCurrentDate());
          tLJABonusGetSchema.setModifyTime(PubFun.getCurrentTime());

          mLJABonusGetSet.add(tLJABonusGetSchema);

          //分红表
          mLOBonusPolSchema.setBonusFlag("1");
          mLOBonusPolSchema.setAGetDate(PubFun.getCurrentDate());
          mLOBonusPolSchema.setModifyDate(PubFun.getCurrentDate());
          mLOBonusPolSchema.setModifyTime(PubFun.getCurrentTime());

          m2LOBonusPolSet.add(mLOBonusPolSchema);

          return true;
        }

        /**
         *保费
         **/
        private boolean CutPrem(LCPolSchema tLCPolSchema)
        {
          //给付总表
          LJAGetSchema tLJAGetSchema = new LJAGetSchema();
          LCBnfSchema tLCBnfSchema = new LCBnfSchema();
          LCBnfSet tLCBnfSet = new LCBnfSet();
          LCBnfDB tLCBnfDB = new LCBnfDB();
          String tLimit=PubFun.getNoLimit(tLCPolSchema.getManageCom());
          String tGetNo = PubFun1.CreateMaxNo("GETNO",tLimit);
          String tNoticeNo = PubFun1.CreateMaxNo("GETNOTICENO",tLimit);
          String tPayNo = PubFun1.CreateMaxNo("PAYNO",tLimit);
          String serNo=PubFun1.CreateMaxNo("SERIALNO",tLimit);
          String tDrawerID="";
          String tappntname = "";
          LCAppntIndDB tLCAppntIndDB = new LCAppntIndDB();
          tLCAppntIndDB.setPolNo(tLCPolSchema.getPolNo());
          tLCAppntIndDB.setCustomerNo(tLCPolSchema.getAppntNo());
          if(tLCAppntIndDB.getInfo()==false)
          {
          }
          else
          {
             tDrawerID = tLCAppntIndDB.getIDNo();
             tappntname = tLCAppntIndDB.getName();
          }
          ////取红利受益人
          tLCBnfDB.setPolNo(tLCPolSchema.getPolNo());
          tLCBnfDB.setBnfType("2");
          tLCBnfSet = tLCBnfDB.query();
          if (tLCBnfSet.size() > 0)
          {
            tLCBnfSchema = tLCBnfSet.get(1);
            tLJAGetSchema.setDrawer(tLCBnfSchema.getName());
            tLJAGetSchema.setDrawerID(tLCBnfSchema.getIDNo());
          }
          else
          {
            LCInsuredDB tLCInsuredDB = new LCInsuredDB();
            /*Lis5.3 upgrade set
            tLCInsuredDB.setPolNo(tLCPolSchema.getPolNo());
            tLCInsuredDB.setCustomerNo(tLCPolSchema.getInsuredNo());
            */
            if(!tLCInsuredDB.getInfo())
            {
              // @@错误处理
              this.mErrors.copyAllErrors( tLCInsuredDB.mErrors );
              CError tError = new CError();
              tError.moduleName = "CutBonusBL";
              tError.functionName = "CutPrem";
              tError.errorMessage = tLCPolSchema.getPolNo()+"保单没有被保人信息!";
              this.mErrors .addOneError(tError) ;
              return false;
            }
            tLJAGetSchema.setDrawer(tLCPolSchema.getInsuredName());
            /*Lis5.3 upgrade set
            tLJAGetSchema.setDrawerID(tLCInsuredDB.getIDNo());
            */
          }

          tLJAGetSchema.setActuGetNo(tGetNo);
          tLJAGetSchema.setOtherNo(tLCPolSchema.getPolNo());
          tLJAGetSchema.setOtherNoType("7");
          tLJAGetSchema.setPayMode("5");
          tLJAGetSchema.setAppntNo(tLCPolSchema.getAppntNo());
          tLJAGetSchema.setSumGetMoney(mLOBonusPolSchema.getBonusMoney());
          tLJAGetSchema.setSaleChnl(tLCPolSchema.getSaleChnl());
          tLJAGetSchema.setShouldDate(mLOBonusPolSchema.getSGetDate());
          tLJAGetSchema.setEnterAccDate(PubFun.getCurrentDate());
          tLJAGetSchema.setConfDate(PubFun.getCurrentDate());
          tLJAGetSchema.setApproveCode(tLCPolSchema.getApproveCode());
          tLJAGetSchema.setApproveDate(tLCPolSchema.getApproveDate());
          tLJAGetSchema.setGetNoticeNo(tNoticeNo);
          /*Lis5.3 upgrade get
          tLJAGetSchema.setBankCode(tLCPolSchema.getBankCode());
          tLJAGetSchema.setBankAccNo(tLCPolSchema.getBankAccNo());
          */
          tLJAGetSchema.setDrawer(tLCPolSchema.getAppntName());
          tLJAGetSchema.setDrawerID(tDrawerID);
          tLJAGetSchema.setSerialNo(serNo);
          tLJAGetSchema.setOperator(mOperate);
          tLJAGetSchema.setMakeDate(PubFun.getCurrentDate());
          tLJAGetSchema.setMakeTime(PubFun.getCurrentTime());
          tLJAGetSchema.setModifyDate(PubFun.getCurrentDate());
          tLJAGetSchema.setModifyTime(PubFun.getCurrentTime());
          tLJAGetSchema.setBankOnTheWayFlag("");
          tLJAGetSchema.setBankSuccFlag("");
          tLJAGetSchema.setSendBankCount("");
          tLJAGetSchema.setManageCom(tLCPolSchema.getManageCom());
          tLJAGetSchema.setAgentCom(tLCPolSchema.getAgentCom());
          tLJAGetSchema.setAgentType(tLCPolSchema.getAgentType());
          tLJAGetSchema.setAgentCode(tLCPolSchema.getAgentCode());
          tLJAGetSchema.setAgentGroup(tLCPolSchema.getAgentGroup());

          mLJAGetSet.add(tLJAGetSchema);

          //红利给付表
          LJABonusGetSchema tLJABonusGetSchema = new LJABonusGetSchema();

          tLJABonusGetSchema.setActuGetNo(tGetNo);
          tLJABonusGetSchema.setOtherNo(tLCPolSchema.getPolNo());
          tLJABonusGetSchema.setOtherNoType("7");
          tLJABonusGetSchema.setBonusYear(String.valueOf(mLOBonusPolSchema.getFiscalYear()));
          tLJABonusGetSchema.setPayMode("5");
          tLJABonusGetSchema.setGetMoney(mLOBonusPolSchema.getBonusMoney());
          tLJABonusGetSchema.setGetDate(PubFun.getCurrentDate());
          tLJABonusGetSchema.setEnterAccDate(PubFun.getCurrentDate());
          tLJABonusGetSchema.setConfDate(PubFun.getCurrentDate());
          tLJABonusGetSchema.setManageCom(tLCPolSchema.getManageCom());
          tLJABonusGetSchema.setAgentCom(tLCPolSchema.getAgentCom());
          tLJABonusGetSchema.setAgentType(tLCPolSchema.getAgentType());
          tLJABonusGetSchema.setAPPntName(tLCPolSchema.getAppntName());
          tLJABonusGetSchema.setAgentGroup(tLCPolSchema.getAgentGroup());
          tLJABonusGetSchema.setAgentCode(tLCPolSchema.getAgentCode());
          tLJABonusGetSchema.setFeeOperationType("HLTF");
          tLJABonusGetSchema.setFeeFinaType("HLTF");
          tLJABonusGetSchema.setSerialNo("0");
          tLJABonusGetSchema.setOperator(mOperate);
          tLJABonusGetSchema.setMakeTime(PubFun.getCurrentTime());
          tLJABonusGetSchema.setMakeDate(PubFun.getCurrentDate());
          tLJABonusGetSchema.setState("0");
          tLJABonusGetSchema.setGetNoticeNo(tNoticeNo);
          tLJABonusGetSchema.setModifyDate(PubFun.getCurrentDate());
          tLJABonusGetSchema.setModifyTime(PubFun.getCurrentTime());

          mLJABonusGetSet.add(tLJABonusGetSchema);

          //财务实付总表
          LJFIGetSchema tLJFIGetSchema = new LJFIGetSchema();
          tLJFIGetSchema.setActuGetNo(tLJAGetSchema.getActuGetNo());
          tLJFIGetSchema.setPayMode(tLJAGetSchema.getPayMode());
          tLJFIGetSchema.setOtherNo(tLJAGetSchema.getOtherNo());
          tLJFIGetSchema.setOtherNoType(tLJAGetSchema.getOtherNoType());
          tLJFIGetSchema.setGetMoney(tLJAGetSchema.getSumGetMoney());
          tLJFIGetSchema.setShouldDate(tLJAGetSchema.getShouldDate());
          tLJFIGetSchema.setEnterAccDate(tLJAGetSchema.getEnterAccDate());
          tLJFIGetSchema.setConfDate(tLJAGetSchema.getConfDate());
          tLJFIGetSchema.setSaleChnl(tLJAGetSchema.getSaleChnl());
          tLJFIGetSchema.setManageCom(tLJAGetSchema.getManageCom());
          tLJFIGetSchema.setAPPntName(tappntname);
          tLJFIGetSchema.setAgentCom(tLJAGetSchema.getAgentCom());
          tLJFIGetSchema.setAgentType(tLJAGetSchema.getAgentType());
          tLJFIGetSchema.setAgentGroup(tLJAGetSchema.getAgentGroup());
          tLJFIGetSchema.setAgentCode(tLJAGetSchema.getAgentCode());
          tLJFIGetSchema.setSerialNo(tLJAGetSchema.getSerialNo());
          tLJFIGetSchema.setDrawer(tLJAGetSchema.getDrawer());
          tLJFIGetSchema.setDrawerID(tLJAGetSchema.getDrawerID());
          tLJFIGetSchema.setOperator(tLJAGetSchema.getOperator());
          tLJFIGetSchema.setMakeTime(tLJAGetSchema.getMakeTime());
          tLJFIGetSchema.setMakeDate(tLJAGetSchema.getMakeDate());
          tLJFIGetSchema.setState("0");
          tLJFIGetSchema.setModifyDate(tLJAGetSchema.getModifyDate());
          tLJFIGetSchema.setModifyTime(tLJAGetSchema.getModifyTime());

          mLJFIGetSet.add(tLJFIGetSchema);

          //暂交费分类表
          LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();
          tLJTempFeeClassSchema.setTempFeeNo(tPayNo);
          tLJTempFeeClassSchema.setPayMode("5");
          tLJTempFeeClassSchema.setChequeNo("");
          tLJTempFeeClassSchema.setPayMoney(mLOBonusPolSchema.getBonusMoney());
//          tLJTempFeeClassSchema.setAPPntName(tappntname);
          tLJTempFeeClassSchema.setPayDate(PubFun.getCurrentDate());
          tLJTempFeeClassSchema.setConfDate(PubFun.getCurrentDate());
          tLJTempFeeClassSchema.setApproveDate(PubFun.getCurrentDate());
          tLJTempFeeClassSchema.setEnterAccDate(PubFun.getCurrentDate());
          tLJTempFeeClassSchema.setConfFlag("1");
          tLJTempFeeClassSchema.setSerialNo(serNo);
          tLJTempFeeClassSchema.setOperator(mOperate);
          tLJTempFeeClassSchema.setMakeDate(PubFun.getCurrentDate());
          tLJTempFeeClassSchema.setMakeTime(PubFun.getCurrentTime());
          tLJTempFeeClassSchema.setModifyDate(PubFun.getCurrentDate());
          tLJTempFeeClassSchema.setModifyTime(PubFun.getCurrentTime());
          tLJTempFeeClassSchema.setManageCom(tLCPolSchema.getManageCom());
          tLJTempFeeClassSchema.setConfMakeDate(PubFun.getCurrentDate());
          mLJTempFeeClassSet.add(tLJTempFeeClassSchema);

          //暂交费表
          LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();

          tLJTempFeeSchema.setTempFeeNo(tPayNo);
          tLJTempFeeSchema.setTempFeeType("7");
          tLJTempFeeSchema.setRiskCode(tLCPolSchema.getRiskCode());
          tLJTempFeeSchema.setPayIntv(tLCPolSchema.getPayIntv());
          tLJTempFeeSchema.setOtherNo(tLCPolSchema.getPolNo());
          tLJTempFeeSchema.setOtherNoType("0");
          tLJTempFeeSchema.setPayMoney(mLOBonusPolSchema.getBonusMoney());
          tLJTempFeeSchema.setPayDate(PubFun.getCurrentDate());
          tLJTempFeeSchema.setEnterAccDate(PubFun.getCurrentDate());
          tLJTempFeeSchema.setConfDate(PubFun.getCurrentDate());
          tLJTempFeeSchema.setSaleChnl(tLCPolSchema.getSaleChnl());
          tLJTempFeeSchema.setManageCom(tLCPolSchema.getManageCom());
          tLJTempFeeSchema.setAgentCom(tLCPolSchema.getAgentCom());
          tLJTempFeeSchema.setAgentType(tLCPolSchema.getAgentType());
          tLJTempFeeSchema.setAPPntName(tLCPolSchema.getAppntName());
          tLJTempFeeSchema.setAgentGroup(tLCPolSchema.getAgentGroup());
          tLJTempFeeSchema.setAgentCode(tLCPolSchema.getAgentCode());
          tLJTempFeeSchema.setConfFlag("1");
          tLJTempFeeSchema.setSerialNo(serNo);
          tLJTempFeeSchema.setOperator(mOperate);
          tLJTempFeeSchema.setMakeTime(PubFun.getCurrentTime());
          tLJTempFeeSchema.setMakeDate(PubFun.getCurrentDate());
          tLJTempFeeSchema.setModifyDate(PubFun.getCurrentDate());
          tLJTempFeeSchema.setModifyTime(PubFun.getCurrentTime());
          tLJTempFeeSchema.setConfMakeDate(PubFun.getCurrentDate());
          mLJTempFeeSet.add(tLJTempFeeSchema);

          //实收总表
          LJAPaySchema tLJAPaySchema = new LJAPaySchema();
          //String tLimit=PubFun.getNoLimit(tLCPolSchema.getManageCom());

          tLJAPaySchema.setPayNo(tPayNo);         //交费收据号码
          tLJAPaySchema.setIncomeNo(tLCPolSchema.getPolNo());          //应收/实收编号
          tLJAPaySchema.setIncomeType("2");    //应收/实收编号类型
          tLJAPaySchema.setAppntNo(tLCPolSchema.getAppntNo());           //  投保人客户号码
          tLJAPaySchema.setSumActuPayMoney(mLOBonusPolSchema.getBonusMoney()); // 总实交金额
          tLJAPaySchema.setEnterAccDate(PubFun.getCurrentDate());   // 到帐日期
          tLJAPaySchema.setPayDate(PubFun.getCurrentDate());           //交费日期
          tLJAPaySchema.setConfDate(PubFun.getCurrentDate());      //确认日期
          tLJAPaySchema.setApproveCode(mLCPolSchema.getApproveCode());   //复核人编码
          tLJAPaySchema.setApproveDate(PubFun.getCurrentDate());   //  复核日期
          tLJAPaySchema.setSerialNo(serNo);                           //流水号
          tLJAPaySchema.setOperator(mOperate);         // 操作员
          tLJAPaySchema.setMakeDate(PubFun.getCurrentDate());                     //入机时间
          tLJAPaySchema.setMakeTime(PubFun.getCurrentTime());                     //入机时间
          tLJAPaySchema.setModifyDate(PubFun.getCurrentDate());                   //最后一次修改日期
          tLJAPaySchema.setModifyTime(PubFun.getCurrentTime());                   //最后一次修改时间
          tLJAPaySchema.setManageCom(tLCPolSchema.getManageCom());
          tLJAPaySchema.setAgentCom(tLCPolSchema.getAgentCom());
          tLJAPaySchema.setAgentType(tLCPolSchema.getAgentType());
          /*Lis5.3 upgrade get
          tLJAPaySchema.setBankCode(tLCPolSchema.getBankCode());      //银行编码
          tLJAPaySchema.setBankAccNo(tLCPolSchema.getBankAccNo());   //银行帐号
          */
          tLJAPaySchema.setRiskCode(tLCPolSchema.getRiskCode());   // 险种编码
          tLJAPaySchema.setAgentCode(tLCPolSchema.getAgentCode());
          tLJAPaySchema.setAgentGroup(tLCPolSchema.getAgentGroup());


          mLJAPaySet.add(tLJAPaySchema);

          //个人实收表
          LJAPayPersonSchema tLJAPayPersonSchema = new LJAPayPersonSchema();

          tLJAPayPersonSchema.setPolNo(tLCPolSchema.getPolNo());           //保单号码
          tLJAPayPersonSchema.setPayCount("2");     //第几次交费-记为续期交费
          tLJAPayPersonSchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());     //集体保单号码
          tLJAPayPersonSchema.setContNo(tLCPolSchema.getContNo());         //总单/合同号码
          tLJAPayPersonSchema.setAppntNo(tLCPolSchema.getAppntNo());       //投保人客户号码
          tLJAPayPersonSchema.setPayNo(tPayNo);          //交费收据号码

          if (tLCPolSchema.getGrpPolNo().equals("00000000000000000000")&&tLCPolSchema.getContNo().equals("00000000000000000000"))
          {
            tLJAPayPersonSchema.setPayAimClass("1");//交费目的分类
          }
          if (!tLCPolSchema.getGrpPolNo().equals("00000000000000000000")&&tLCPolSchema.getContNo().equals("00000000000000000000"))
          {
            tLJAPayPersonSchema.setPayAimClass("2");//交费目的分类
          }

          tLJAPayPersonSchema.setDutyCode("0000000000");      //责任编码
          tLJAPayPersonSchema.setPayPlanCode("00000000");//交费计划编码
          tLJAPayPersonSchema.setSumDuePayMoney(0);//总应交金额
          tLJAPayPersonSchema.setSumActuPayMoney(mLOBonusPolSchema.getBonusMoney());//总实交金额
          tLJAPayPersonSchema.setPayIntv(tLCPolSchema.getPayIntv());        //交费间隔
          tLJAPayPersonSchema.setPayDate(PubFun.getCurrentDate());        //交费日期
          tLJAPayPersonSchema.setPayType("YET");        //交费类型
          tLJAPayPersonSchema.setEnterAccDate(PubFun.getCurrentDate()); //到帐日期
          tLJAPayPersonSchema.setConfDate(PubFun.getCurrentDate());         //确认日期
          tLJAPayPersonSchema.setLastPayToDate(tLCPolSchema.getPaytoDate());  //原交至日期
          tLJAPayPersonSchema.setCurPayToDate(tLCPolSchema.getPaytoDate());    //现交至日期
          tLJAPayPersonSchema.setInInsuAccState("0");//转入保险帐户状态
          tLJAPayPersonSchema.setApproveCode(tLCPolSchema.getApproveCode());      //复核人编码
          tLJAPayPersonSchema.setApproveDate(tLCPolSchema.getApproveDate());      //复核日期
          tLJAPayPersonSchema.setSerialNo(serNo);                              //流水号
          tLJAPayPersonSchema.setOperator(mOperate);      //操作员
          tLJAPayPersonSchema.setMakeDate(PubFun.getCurrentDate());                        //入机日期
          tLJAPayPersonSchema.setMakeTime(PubFun.getCurrentTime());                        //入机时间
          tLJAPayPersonSchema.setGetNoticeNo(tNoticeNo);//通知书号码
          tLJAPayPersonSchema.setModifyDate(PubFun.getCurrentDate());                      //最后一次修改日期
          tLJAPayPersonSchema.setModifyTime(PubFun.getCurrentTime());                      //最后一次修改时间

          tLJAPayPersonSchema.setManageCom(tLCPolSchema.getManageCom());    //管理机构
          tLJAPayPersonSchema.setAgentCom(tLCPolSchema.getAgentCom());//代理机构
          tLJAPayPersonSchema.setAgentType(tLCPolSchema.getAgentType());      //代理机构内部分类
          tLJAPayPersonSchema.setRiskCode(tLCPolSchema.getRiskCode());      //险种编码
          tLJAPayPersonSchema.setAgentCode(tLCPolSchema.getAgentCode());
          tLJAPayPersonSchema.setAgentGroup(tLCPolSchema.getAgentGroup());
          mLJAPayPersonSet.add(tLJAPayPersonSchema);

          //分红表
          mLOBonusPolSchema.setBonusFlag("1");
          mLOBonusPolSchema.setAGetDate(PubFun.getCurrentDate());
          mLOBonusPolSchema.setModifyDate(PubFun.getCurrentDate());
          mLOBonusPolSchema.setModifyTime(PubFun.getCurrentTime());

          m2LOBonusPolSet.add(mLOBonusPolSchema);

          //个人保单表余额添加
          tLCPolSchema.setLeavingMoney(tLCPolSchema.getLeavingMoney()+mLOBonusPolSchema.getBonusMoney());
          tLCPolSchema.setModifyTime(PubFun.getCurrentTime());
          tLCPolSchema.setModifyDate(PubFun.getCurrentDate());
          mLCPolSet.add(tLCPolSchema);
          return true;
        }

        /**
         *帐户
         **/
        private boolean CutAcc(LCPolSchema tLCPolSchema)
        {
          VData tVData = new VData();
          DealAccount tDealAccount = new DealAccount();
          LMRiskInsuAccSchema tLMRiskInsuAccSchema = new LMRiskInsuAccSchema();
          LMRiskInsuAccDB tLMRiskInsuAccDB = new LMRiskInsuAccDB();
          LMRiskInsuAccSet tLMRiskInsuAccSet = new LMRiskInsuAccSet();

          //取红利帐户
          String tsql = "";
          tsql = "select * from LMRiskInsuAcc where InsuAccNo in (select InsuAccNo from LMRiskToAcc where riskcode = '"+tLCPolSchema.getRiskCode().trim()+"') and acctype = '004'";
          tLMRiskInsuAccSet = tLMRiskInsuAccDB.executeQuery(tsql);
          if (tLMRiskInsuAccSet.size() > 0)
          {
            tLMRiskInsuAccSchema = tLMRiskInsuAccSet.get(1);
          }
          else
          {
              CError.buildErr(this,"帐户描述关联表缺少描述！");
              return false;
          }

          LCInsureAccSet tLCInsureAccSet = new LCInsureAccSet();
          LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
          tLCInsureAccDB.setPolNo(tLCPolSchema.getPolNo());
          tLCInsureAccDB.setAccType("004");
          tLCInsureAccSet = tLCInsureAccDB.query();
          boolean AccCreatFlag=true;//帐户新生成标记

          //生成帐户数据
          if( tLCInsureAccSet.size() > 0)
          {
            tVData = tDealAccount.addPrem(tLCPolSchema.getPolNo(),tLMRiskInsuAccSchema.getInsuAccNo(),tLCPolSchema.getPolNo(),"1","HL",tLCPolSchema.getManageCom(),mLOBonusPolSchema.getBonusMoney());
            AccCreatFlag=false;
          }
          else
          {
            tVData = tDealAccount.getLCInsureAcc(tLCPolSchema.getPolNo(),tLMRiskInsuAccSchema.getAccCreatePos(),tLCPolSchema.getPolNo(),"1",tLCPolSchema.getManageCom(),"004","HL",mLOBonusPolSchema.getBonusMoney());
             AccCreatFlag=true;
          }

          if (tVData == null)
          {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "CutBonusBL";
            tError.functionName = "CutAcc";
            tError.errorMessage = tLCPolSchema.getPolNo()+"帐户数据生成失败!";
            this.mErrors .addOneError(tError) ;
            return false;
          }

          //帐户表
          tLCInsureAccSet = new LCInsureAccSet();
          tLCInsureAccSet = (LCInsureAccSet)tVData.getObjectByObjectName("LCInsureAccSet",0);
          if (tLCInsureAccSet.size()> 0)
          {
            for (int i = 1;i <= tLCInsureAccSet.size();i++)
            {
              LCInsureAccSchema tLCInsureAccSchema = new LCInsureAccSchema();
              tLCInsureAccSchema = tLCInsureAccSet.get(i);
              if( AccCreatFlag==true)
              {
                  tLCInsureAccSchema.setBalaDate(mLOBonusPolSchema.getSGetDate());
              }
              mLCInsureAccSet.add(tLCInsureAccSchema);
            }
          }
          //帐户轨迹表
          LCInsureAccTraceSet tLCInsureAccTraceSet = new LCInsureAccTraceSet();
          tLCInsureAccTraceSet = (LCInsureAccTraceSet)tVData.getObjectByObjectName("LCInsureAccTraceSet",0);
          if (tLCInsureAccTraceSet.size()> 0)
          {
            for (int i = 1;i <= tLCInsureAccTraceSet.size();i++)
            {
              LCInsureAccTraceSchema tLCInsureAccTraceSchema = new LCInsureAccTraceSchema();
              tLCInsureAccTraceSchema = tLCInsureAccTraceSet.get(i);
              tLCInsureAccTraceSchema.setPayDate(mLOBonusPolSchema.getSGetDate());//交费日期计为应分配日期
              mLCInsureAccTraceSet.add(tLCInsureAccTraceSchema);
            }
          }

          //分红表
          mLOBonusPolSchema.setBonusFlag("1");
          mLOBonusPolSchema.setAGetDate(PubFun.getCurrentDate());
          mLOBonusPolSchema.setModifyDate(PubFun.getCurrentDate());
          mLOBonusPolSchema.setModifyTime(PubFun.getCurrentTime());

          m2LOBonusPolSet.add(mLOBonusPolSchema);

          return true;

        }

        /**
         * 增额交清
         * @param tLCPolSchema
         * @return
         */
        private boolean CutByAddPrem(LCPolSchema tLCPolSchema)
        {
            double tBonusMoney=mLOBonusPolSchema.getBonusMoney();
            String CurrentDate=PubFun.getCurrentDate();
            String CurrentTime=PubFun.getCurrentTime();

            //1-查询该保单的责任信息
            LCDutySchema tLCDutySchema=new LCDutySchema();
            LCPremSchema tLCPremSchema=new LCPremSchema();
            LCGetSchema tLCGetSchema = new LCGetSchema();
            LCGetSet tSaveLCGetSet = new LCGetSet();
            LCDutyDB tLCDutyDB = new LCDutyDB();
            tLCDutyDB.setPolNo(tLCPolSchema.getPolNo());
            LCDutySet tLCDutySet = tLCDutyDB.query();
            if(tLCDutySet.size()==0)
            {
                this.mErrors.copyAllErrors (tLCDutyDB.mErrors);
                return false;
            }
            int serNo=0;
            for(int i=1;i<=tLCDutySet.size();i++)
            {
                if(tLCDutySet.get(i).getDutyCode().length()!=6)
                {
                   if(tLCDutySet.get(i).getDutyCode().length()==10)
                   {
                       serNo=serNo+1;
                   }
                   continue;
                }
                else
                {
                    //得到一条正常责任项
                    tLCDutySchema=tLCDutySet.get(i);
                }
            }
            LCPremDB tLCPremDB=new LCPremDB();
            tLCPremDB.setPolNo(tLCPolSchema.getPolNo());
            tLCPremDB.setDutyCode(tLCDutySchema.getDutyCode());
            LCPremSet tLCPremSet=tLCPremDB.query();
            if(tLCPremSet.size()==0)
            {
                this.mErrors.copyAllErrors (tLCPremDB.mErrors);
                return false;
            }
            for(int n=1;n<=tLCPremSet.size();n++)
            {
                if(tLCPremSet.get(n).getPayPlanCode().length()!=6)
                {
                  continue;
                }
                //得到一条正常保费项纪录
                tLCPremSchema=tLCPremSet.get(n);
                break;
            }
            LCGetDB tLCGetDB=new LCGetDB();
            tLCGetDB.setPolNo(tLCPolSchema.getPolNo());
            tLCGetDB.setDutyCode(tLCDutySchema.getDutyCode());
            LCGetSet tLCGetSet=tLCGetDB.query();
            if(tLCGetSet.size()==0)
            {
                this.mErrors.copyAllErrors (tLCGetDB.mErrors);
                return false;
            }
            for(int n=1;n<=tLCGetSet.size();n++)
            {
                if(tLCGetSet.get(n).getGetDutyCode().length()!=6)
                {
                  continue;
                }
                //得到一条正常领取项纪录
                tLCGetSchema=new LCGetSchema();
                tLCGetSchema=tLCGetSet.get(n);
                tSaveLCGetSet.add(tLCGetSchema);
            }

            //CalBonus tCalBonus=new CalBonus();
            //计算保单年度值--需要校验:即不满一年的需要加 1 --润年问题
            int PolYear=PubFun.calInterval(tLCPolSchema.getCValiDate(),mLOBonusPolSchema.getSGetDate(),"Y" );
            //int ActuAge=PubFun.calInterval(tLCPolSchema.getInsuredBirthday(),mLOBonusPolSchema.getSGetDate(),"Y");
            int ActuAge=tLCPolSchema.getInsuredAppAge();
            CalBase mCalBase = new CalBase();
            //mCalBase.setAmnt(tLCPolSchema.getAmnt());
            mCalBase.setPrem(tBonusMoney);
            mCalBase.setAppAge( ActuAge );
            mCalBase.setSex(tLCPolSchema.getInsuredSex());
            mCalBase.setPayEndYear(tLCPolSchema.getPayEndYear());
            mCalBase.setPayEndYearFlag(tLCPolSchema.getPayEndYearFlag());
            mCalBase.setPayIntv(0);//都是趸交

            Calculator mCalculator = new Calculator();
            LMRiskBonusDB tLMRiskBonusDB=new LMRiskBonusDB();
            tLMRiskBonusDB.setRiskCode(tLCPolSchema.getRiskCode());
            if(tLMRiskBonusDB.getInfo()==false)
            {
                CError tError =new CError();
                tError.moduleName="AssignBonus";
                tError.functionName="getByAddPrem";
                tError.errorMessage="没有找到增额交清对应的险种描述项";
                this.mErrors .addOneError(tError);
                return false;
            }
            if(tLMRiskBonusDB.getAddAmntCoefCode()==null)
            {
                CError tError =new CError();
                tError.moduleName="AssignBonus";
                tError.functionName="getByAddPrem";
                tError.errorMessage="没有找到增额交清对应的险种算法";
                this.mErrors .addOneError(tError);
                return false;
            }
            mCalculator.setCalCode( tLMRiskBonusDB.getAddAmntCoefCode());

            //增加基本要素
            mCalculator.addBasicFactor("Prem", mCalBase.getPrem() );
            mCalculator.addBasicFactor("AppAge", mCalBase.getAppAge() );
            mCalculator.addBasicFactor("Sex", mCalBase.getSex() );
            mCalculator.addBasicFactor("PayEndYear", mCalBase.getPayEndYear() );
            mCalculator.addBasicFactor("PayEndYearFlag", mCalBase.getPayEndYearFlag() );
            mCalculator.addBasicFactor("PolYear",String.valueOf(PolYear));//添加保单年度值
            mCalculator.addBasicFactor("PayIntv",mCalBase.getPayIntv());
            //计算得到新的保额
            String tStr = "";
            double mValue;
            tStr = mCalculator.calculate() ;
            if (tStr.trim().equals(""))
                mValue = 0;
            else
                mValue = Double.parseDouble( tStr );

            if(mValue==0) return false;

            //格式化，取两位精度-校验
            String strCalPrem=mDecimalFormat.format(mValue);//转换计算后的保费(规定的精度)
            mValue=Double.parseDouble(strCalPrem);
            double douBonusMoney=tBonusMoney;
            FDate tFDate=new FDate();

            //被保人新的年龄
            String newInsuredAge=Integer.toString (PubFun.calInterval(tLCPolSchema.getInsuredBirthday(),mLOBonusPolSchema.getSGetDate(),"Y"));
            //当前年龄和原年龄之间的间隔
            String intvAge=Integer.toString (Integer.parseInt(newInsuredAge)-tLCPolSchema.getInsuredAppAge());
            //新的保险年期
            String insuYear=Integer.toString (tLCPolSchema.getInsuYear()-PolYear);
            //新的交费年期
            String newPayYears=Integer.toString (tLCPolSchema.getPayYears()-PolYear);
            //新的交至日期=当前日期+新的保险年期
            String payToDate=tFDate.getString(PubFun.calDate(tFDate.getDate(mLOBonusPolSchema.getSGetDate()),Integer.parseInt(insuYear),"Y",null));
            //新的终交日期
            String payEndDate=payToDate;
            //新的终交年龄年期
            int newPayEndYear=tLCPolSchema.getPayEndYear();
            //终交年龄年期标志
            String payEndYearFlag=tLCPolSchema.getPayEndYearFlag();
            if(payEndYearFlag.equals("Y"))
            {
                newPayEndYear=tLCPolSchema.getPayEndYear()-Integer.parseInt(intvAge);
            }
            //新的保险年龄年期
            int newInsuYear=tLCPolSchema.getInsuYear();
            //终交年龄年期标志
            String insuYearFlag=tLCPolSchema.getInsuYearFlag();
            if(insuYearFlag.equals("Y"))
            {
                newInsuYear=tLCPolSchema.getInsuYear()-Integer.parseInt(intvAge);
            }

            //准备责任项，保费项，领取项数据--原来的责任编码为6位），第7位为1，剩余的3位为流水号
            serNo=serNo+1;
            String newSerNo="";
            String strSerNo=String.valueOf(serNo);
            if(strSerNo.length()==1)
                newSerNo="00"+strSerNo;
            if(strSerNo.length()==2)
                newSerNo="0"+strSerNo;
            if(strSerNo.length()==3)
                newSerNo=strSerNo;
            tLCDutySchema.setDutyCode(tLCDutySchema.getDutyCode()+"1"+newSerNo);
            tLCDutySchema.setMult(1);
            tLCDutySchema.setPrem(tBonusMoney);
            tLCDutySchema.setStandPrem(tBonusMoney);
            tLCDutySchema.setSumPrem(tBonusMoney);
            tLCDutySchema.setAmnt(mValue);
            tLCDutySchema.setRiskAmnt(mValue);
            tLCDutySchema.setFirstPayDate(CurrentDate);
            tLCDutySchema.setPaytoDate(payToDate);
            tLCDutySchema.setPayEndDate(payToDate);
            tLCDutySchema.setPayIntv(0);
            tLCDutySchema.setPayEndYear(newPayEndYear);
            tLCDutySchema.setPayEndYearFlag(payEndYearFlag);
            tLCDutySchema.setInsuYear(newInsuYear);
            tLCDutySchema.setInsuYearFlag(insuYearFlag);
            tLCDutySchema.setPayYears(newPayYears);
            tLCDutySchema.setYears(insuYear);
            tLCDutySchema.setModifyDate(CurrentDate);
            tLCDutySchema.setModifyTime(CurrentTime);
            mLCDutySet.add(tLCDutySchema);

            //准备保费项
            tLCPremSchema.setPolNo(tLCPolSchema.getPolNo());
            tLCPremSchema.setDutyCode(tLCDutySchema.getDutyCode());
            tLCPremSchema.setPayTimes(1);
            tLCPremSchema.setPayIntv(0);
            /*Lis5.3 upgrade get
            tLCPremSchema.setMult(1);
            */
            tLCPremSchema.setStandPrem(tBonusMoney);
            tLCPremSchema.setPrem(tBonusMoney);
            tLCPremSchema.setSumPrem(tBonusMoney);
            tLCPremSchema.setPayStartDate(CurrentDate);
            tLCPremSchema.setPayEndDate(payEndDate);
            tLCPremSchema.setPaytoDate(payToDate);
            tLCPremSchema.setUrgePayFlag("N");
            tLCPremSchema.setState("");
            tLCPremSchema.setModifyDate(CurrentDate);
            tLCPremSchema.setModifyTime(CurrentTime);
            mLCPremSet.add(tLCPremSchema);

            //准备领取项
            for(int i=1;i<=tSaveLCGetSet.size();i++)
            {
                tSaveLCGetSet.get(i).setPolNo(tLCPolSchema.getPolNo());
                tSaveLCGetSet.get(i).setDutyCode(tLCDutySchema.getDutyCode());
                tSaveLCGetSet.get(i).setStandMoney(mValue);
                //tSaveLCGetSet.get(i).setActuGet(mValue);
                tSaveLCGetSet.get(i).setBalaDate("");
                tSaveLCGetSet.get(i).setModifyDate(CurrentDate);
                tSaveLCGetSet.get(i).setModifyTime(CurrentTime);
            }
            mLCGetSet.add(tSaveLCGetSet);

            //分红表
            mLOBonusPolSchema.setBonusFlag("1");
            mLOBonusPolSchema.setAGetDate(CurrentDate);
            mLOBonusPolSchema.setModifyDate(CurrentDate);
            mLOBonusPolSchema.setModifyTime(PubFun.getCurrentTime());

            m2LOBonusPolSet.add(mLOBonusPolSchema);

            //更新个人保单表:保费增加,保额增加
            tLCPolSchema.setPrem(tLCPolSchema.getPrem()+douBonusMoney);
            tLCPolSchema.setSumPrem(tLCPolSchema.getSumPrem()+douBonusMoney);
            tLCPolSchema.setAmnt(tLCPolSchema.getAmnt()+mValue);
            tLCPolSchema.setRiskAmnt(tLCPolSchema.getRiskAmnt()+mValue);
            tLCPolSchema.setModifyDate(CurrentDate);
            tLCPolSchema.setModifyTime(CurrentTime);
            mLCPolSet.add(tLCPolSchema);

            //给付总表
            LJAGetSchema tLJAGetSchema = new LJAGetSchema();
            LCBnfSchema tLCBnfSchema = new LCBnfSchema();
            LCBnfSet tLCBnfSet = new LCBnfSet();
            LCBnfDB tLCBnfDB = new LCBnfDB();
            String tLimit=PubFun.getNoLimit(tLCPolSchema.getManageCom());
            String tGetNo = PubFun1.CreateMaxNo("GETNO",tLimit);
            String tNoticeNo = PubFun1.CreateMaxNo("GETNOTICENO",tLimit);
            String tPayNo = PubFun1.CreateMaxNo("PAYNO",tLimit);
            String sNo=PubFun1.CreateMaxNo("SERIALNO",tLimit);
            String tDrawerID="";
            String tappntname = "";
            LCAppntIndDB tLCAppntIndDB = new LCAppntIndDB();
            tLCAppntIndDB.setPolNo(tLCPolSchema.getPolNo());
            tLCAppntIndDB.setCustomerNo(tLCPolSchema.getAppntNo());
            if(tLCAppntIndDB.getInfo()==false)
            {
            }
            else
            {
               tDrawerID = tLCAppntIndDB.getIDNo();
               tappntname = tLCAppntIndDB.getName();
            }
            ////取红利受益人
            tLCBnfDB.setPolNo(tLCPolSchema.getPolNo());
            tLCBnfDB.setBnfType("2");
            tLCBnfSet = tLCBnfDB.query();
            if (tLCBnfSet.size() > 0)
            {
              tLCBnfSchema = tLCBnfSet.get(1);
              tLJAGetSchema.setDrawer(tLCBnfSchema.getName());
              tLJAGetSchema.setDrawerID(tLCBnfSchema.getIDNo());
            }
            else
            {
              LCInsuredDB tLCInsuredDB = new LCInsuredDB();
              /*Lis5.3 upgrade set
              tLCInsuredDB.setPolNo(tLCPolSchema.getPolNo());
              tLCInsuredDB.setCustomerNo(tLCPolSchema.getInsuredNo());
              */
              if(!tLCInsuredDB.getInfo())
              {
                // @@错误处理
                this.mErrors.copyAllErrors( tLCInsuredDB.mErrors );
                CError tError = new CError();
                tError.moduleName = "CutBonusBL";
                tError.functionName = "CutPrem";
                tError.errorMessage = tLCPolSchema.getPolNo()+"保单没有被保人信息!";
                this.mErrors .addOneError(tError) ;
                return false;
              }
              tLJAGetSchema.setDrawer(tLCPolSchema.getInsuredName());
              /*Lis5.3 upgrade get
              tLJAGetSchema.setDrawerID(tLCInsuredDB.getIDNo());
              */
            }

            tLJAGetSchema.setActuGetNo(tGetNo);
            tLJAGetSchema.setOtherNo(tLCPolSchema.getPolNo());
            tLJAGetSchema.setOtherNoType("7");
            tLJAGetSchema.setPayMode("5");
            tLJAGetSchema.setAppntNo(tLCPolSchema.getAppntNo());
            tLJAGetSchema.setSumGetMoney(mLOBonusPolSchema.getBonusMoney());
            tLJAGetSchema.setSaleChnl(tLCPolSchema.getSaleChnl());
            tLJAGetSchema.setShouldDate(mLOBonusPolSchema.getSGetDate());
            tLJAGetSchema.setEnterAccDate(CurrentDate);
            tLJAGetSchema.setConfDate(CurrentDate);
            tLJAGetSchema.setApproveCode(tLCPolSchema.getApproveCode());
            tLJAGetSchema.setApproveDate(tLCPolSchema.getApproveDate());
            tLJAGetSchema.setGetNoticeNo(tNoticeNo);
            /*Lis5.3 upgrade get
            tLJAGetSchema.setBankCode(tLCPolSchema.getBankCode());
            tLJAGetSchema.setBankAccNo(tLCPolSchema.getBankAccNo());
            */
            tLJAGetSchema.setDrawer(tLCPolSchema.getAppntName());
            tLJAGetSchema.setDrawerID(tDrawerID);
            tLJAGetSchema.setSerialNo(sNo);
            tLJAGetSchema.setOperator(mOperate);
            tLJAGetSchema.setMakeDate(CurrentDate);
            tLJAGetSchema.setMakeTime(CurrentTime);
            tLJAGetSchema.setModifyDate(CurrentDate);
            tLJAGetSchema.setModifyTime(CurrentTime);
            tLJAGetSchema.setBankOnTheWayFlag("");
            tLJAGetSchema.setBankSuccFlag("");
            tLJAGetSchema.setSendBankCount("");
            tLJAGetSchema.setManageCom(tLCPolSchema.getManageCom());
            tLJAGetSchema.setAgentCom(tLCPolSchema.getAgentCom());
            tLJAGetSchema.setAgentType(tLCPolSchema.getAgentType());
            tLJAGetSchema.setAgentCode(tLCPolSchema.getAgentCode());
            tLJAGetSchema.setAgentGroup(tLCPolSchema.getAgentGroup());

            mLJAGetSet.add(tLJAGetSchema);

            //红利给付表
            LJABonusGetSchema tLJABonusGetSchema = new LJABonusGetSchema();

            tLJABonusGetSchema.setActuGetNo(tGetNo);
            tLJABonusGetSchema.setOtherNo(tLCPolSchema.getPolNo());
            tLJABonusGetSchema.setOtherNoType("7");
            tLJABonusGetSchema.setBonusYear(String.valueOf(mLOBonusPolSchema.getFiscalYear()));
            tLJABonusGetSchema.setPayMode("5");
            tLJABonusGetSchema.setGetMoney(mLOBonusPolSchema.getBonusMoney());
            tLJABonusGetSchema.setGetDate(CurrentDate);
            tLJABonusGetSchema.setEnterAccDate(CurrentDate);
            tLJABonusGetSchema.setConfDate(CurrentDate);
            tLJABonusGetSchema.setManageCom(tLCPolSchema.getManageCom());
            tLJABonusGetSchema.setAgentCom(tLCPolSchema.getAgentCom());
            tLJABonusGetSchema.setAgentType(tLCPolSchema.getAgentType());
            tLJABonusGetSchema.setAPPntName(tLCPolSchema.getAppntName());
            tLJABonusGetSchema.setAgentGroup(tLCPolSchema.getAgentGroup());
            tLJABonusGetSchema.setAgentCode(tLCPolSchema.getAgentCode());
            tLJABonusGetSchema.setFeeOperationType("HLTF");
            tLJABonusGetSchema.setFeeFinaType("HLTF");
            tLJABonusGetSchema.setSerialNo(sNo);
            tLJABonusGetSchema.setOperator(mOperate);
            tLJABonusGetSchema.setMakeTime(CurrentTime);
            tLJABonusGetSchema.setMakeDate(CurrentDate);
            tLJABonusGetSchema.setState("0");
            tLJABonusGetSchema.setGetNoticeNo(tNoticeNo);
            tLJABonusGetSchema.setModifyDate(CurrentDate);
            tLJABonusGetSchema.setModifyTime(CurrentTime);

            mLJABonusGetSet.add(tLJABonusGetSchema);

            //财务实付总表
            LJFIGetSchema tLJFIGetSchema = new LJFIGetSchema();
            tLJFIGetSchema.setActuGetNo(tLJAGetSchema.getActuGetNo());
            tLJFIGetSchema.setPayMode(tLJAGetSchema.getPayMode());
            tLJFIGetSchema.setOtherNo(tLJAGetSchema.getOtherNo());
            tLJFIGetSchema.setOtherNoType(tLJAGetSchema.getOtherNoType());
            tLJFIGetSchema.setGetMoney(tLJAGetSchema.getSumGetMoney());
            tLJFIGetSchema.setShouldDate(tLJAGetSchema.getShouldDate());
            tLJFIGetSchema.setEnterAccDate(tLJAGetSchema.getEnterAccDate());
            tLJFIGetSchema.setConfDate(tLJAGetSchema.getConfDate());
            tLJFIGetSchema.setSaleChnl(tLJAGetSchema.getSaleChnl());
            tLJFIGetSchema.setManageCom(tLJAGetSchema.getManageCom());
            tLJFIGetSchema.setAPPntName(tappntname);
            tLJFIGetSchema.setAgentCom(tLJAGetSchema.getAgentCom());
            tLJFIGetSchema.setAgentType(tLJAGetSchema.getAgentType());
            tLJFIGetSchema.setAgentGroup(tLJAGetSchema.getAgentGroup());
            tLJFIGetSchema.setAgentCode(tLJAGetSchema.getAgentCode());
            tLJFIGetSchema.setSerialNo(tLJAGetSchema.getSerialNo());
            tLJFIGetSchema.setDrawer(tLJAGetSchema.getDrawer());
            tLJFIGetSchema.setDrawerID(tLJAGetSchema.getDrawerID());
            tLJFIGetSchema.setOperator(tLJAGetSchema.getOperator());
            tLJFIGetSchema.setMakeTime(tLJAGetSchema.getMakeTime());
            tLJFIGetSchema.setMakeDate(tLJAGetSchema.getMakeDate());
            tLJFIGetSchema.setState("0");
            tLJFIGetSchema.setModifyDate(tLJAGetSchema.getModifyDate());
            tLJFIGetSchema.setModifyTime(tLJAGetSchema.getModifyTime());
            tLJFIGetSchema.setConfMakeDate(CurrentDate);
            mLJFIGetSet.add(tLJFIGetSchema);

            //暂交费分类表
            LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();
            tLJTempFeeClassSchema.setTempFeeNo(tPayNo);
            tLJTempFeeClassSchema.setPayMode("5");
            tLJTempFeeClassSchema.setChequeNo("");
            tLJTempFeeClassSchema.setPayMoney(mLOBonusPolSchema.getBonusMoney());
//            tLJTempFeeClassSchema.setAPPntName(tappntname);
            tLJTempFeeClassSchema.setPayDate(CurrentDate);
            tLJTempFeeClassSchema.setConfDate(CurrentDate);
            tLJTempFeeClassSchema.setApproveDate(CurrentDate);
            tLJTempFeeClassSchema.setEnterAccDate(CurrentDate);
            tLJTempFeeClassSchema.setConfFlag("1");
            tLJTempFeeClassSchema.setSerialNo(sNo);
            tLJTempFeeClassSchema.setOperator(mOperate);
            tLJTempFeeClassSchema.setMakeDate(CurrentDate);
            tLJTempFeeClassSchema.setMakeTime(CurrentTime);
            tLJTempFeeClassSchema.setModifyDate(CurrentDate);
            tLJTempFeeClassSchema.setModifyTime(CurrentTime);
            tLJTempFeeClassSchema.setManageCom(tLCPolSchema.getManageCom());
            tLJTempFeeClassSchema.setConfMakeDate(CurrentDate);
            mLJTempFeeClassSet.add(tLJTempFeeClassSchema);

            //暂交费表
            LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
            tLJTempFeeSchema.setTempFeeNo(tPayNo);
            tLJTempFeeSchema.setTempFeeType("7");
            tLJTempFeeSchema.setRiskCode(tLCPolSchema.getRiskCode());
            tLJTempFeeSchema.setPayIntv(tLCPolSchema.getPayIntv());
            tLJTempFeeSchema.setOtherNo(tLCPolSchema.getPolNo());
            tLJTempFeeSchema.setOtherNoType("0");
            tLJTempFeeSchema.setPayMoney(mLOBonusPolSchema.getBonusMoney());
            tLJTempFeeSchema.setPayDate(CurrentDate);
            tLJTempFeeSchema.setEnterAccDate(CurrentDate);
            tLJTempFeeSchema.setConfDate(CurrentDate);
            tLJTempFeeSchema.setSaleChnl(tLCPolSchema.getSaleChnl());
            tLJTempFeeSchema.setManageCom(tLCPolSchema.getManageCom());
            tLJTempFeeSchema.setAgentCom(tLCPolSchema.getAgentCom());
            tLJTempFeeSchema.setAgentType(tLCPolSchema.getAgentType());
            tLJTempFeeSchema.setAPPntName(tLCPolSchema.getAppntName());
            tLJTempFeeSchema.setAgentGroup(tLCPolSchema.getAgentGroup());
            tLJTempFeeSchema.setAgentCode(tLCPolSchema.getAgentCode());
            tLJTempFeeSchema.setConfFlag("1");
            tLJTempFeeSchema.setSerialNo(sNo);
            tLJTempFeeSchema.setOperator(mOperate);
            tLJTempFeeSchema.setMakeDate(CurrentDate);
            tLJTempFeeSchema.setMakeTime(CurrentTime);
            tLJTempFeeSchema.setModifyDate(CurrentDate);
            tLJTempFeeSchema.setModifyTime(CurrentTime);
            tLJTempFeeSchema.setConfMakeDate(CurrentDate);
            mLJTempFeeSet.add(tLJTempFeeSchema);

            //实收总表
            LJAPaySchema tLJAPaySchema = new LJAPaySchema();
            //String tLimit=PubFun.getNoLimit(tLCPolSchema.getManageCom());

            tLJAPaySchema.setPayNo(tPayNo);         //交费收据号码
            tLJAPaySchema.setIncomeNo(tLCPolSchema.getPolNo());          //应收/实收编号
            tLJAPaySchema.setIncomeType("2");    //应收/实收编号类型
            tLJAPaySchema.setAppntNo(tLCPolSchema.getAppntNo());           //  投保人客户号码
            tLJAPaySchema.setSumActuPayMoney(mLOBonusPolSchema.getBonusMoney()); // 总实交金额
            tLJAPaySchema.setEnterAccDate(CurrentDate);   // 到帐日期
            tLJAPaySchema.setPayDate(CurrentDate);           //交费日期
            tLJAPaySchema.setConfDate(CurrentDate);      //确认日期
            tLJAPaySchema.setApproveCode(mLCPolSchema.getApproveCode());   //复核人编码
            tLJAPaySchema.setApproveDate(CurrentDate);   //  复核日期
            tLJAPaySchema.setSerialNo(sNo);                           //流水号
            tLJAPaySchema.setOperator(mOperate);         // 操作员
            tLJAPaySchema.setMakeDate(CurrentDate);                     //入机时间
            tLJAPaySchema.setMakeTime(CurrentTime);                     //入机时间
            tLJAPaySchema.setModifyDate(CurrentDate);                   //最后一次修改日期
            tLJAPaySchema.setModifyTime(CurrentTime);                   //最后一次修改时间
            tLJAPaySchema.setManageCom(tLCPolSchema.getManageCom());
            tLJAPaySchema.setAgentCom(tLCPolSchema.getAgentCom());
            tLJAPaySchema.setAgentType(tLCPolSchema.getAgentType());
            /*Lis5.3 upgrade get
            tLJAPaySchema.setBankCode(tLCPolSchema.getBankCode());      //银行编码
            tLJAPaySchema.setBankAccNo(tLCPolSchema.getBankAccNo());   //银行帐号
            */
            tLJAPaySchema.setRiskCode(tLCPolSchema.getRiskCode());   // 险种编码
            tLJAPaySchema.setAgentCode(tLCPolSchema.getAgentCode());
            tLJAPaySchema.setAgentGroup(tLCPolSchema.getAgentGroup());

            mLJAPaySet.add(tLJAPaySchema);

            //个人实收表
            LJAPayPersonSchema tLJAPayPersonSchema = new LJAPayPersonSchema();

            tLJAPayPersonSchema.setPolNo(tLCPolSchema.getPolNo());           //保单号码
            tLJAPayPersonSchema.setPayCount("2");     //第几次交费--记为续期交费
            tLJAPayPersonSchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());     //集体保单号码
            tLJAPayPersonSchema.setContNo(tLCPolSchema.getContNo());         //总单/合同号码
            tLJAPayPersonSchema.setAppntNo(tLCPolSchema.getAppntNo());       //投保人客户号码
            tLJAPayPersonSchema.setPayNo(tPayNo);          //交费收据号码

            if (tLCPolSchema.getGrpPolNo().equals("00000000000000000000")&&tLCPolSchema.getContNo().equals("00000000000000000000"))
            {
              tLJAPayPersonSchema.setPayAimClass("1");//交费目的分类
            }
            if (!tLCPolSchema.getGrpPolNo().equals("00000000000000000000")&&tLCPolSchema.getContNo().equals("00000000000000000000"))
            {
              tLJAPayPersonSchema.setPayAimClass("2");//交费目的分类
            }

            tLJAPayPersonSchema.setDutyCode(tLCDutySchema.getDutyCode());      //责任编码
            tLJAPayPersonSchema.setPayPlanCode("00000000");//交费计划编码
            tLJAPayPersonSchema.setSumDuePayMoney(0);//总应交金额
            tLJAPayPersonSchema.setSumActuPayMoney(mLOBonusPolSchema.getBonusMoney());//总实交金额
            tLJAPayPersonSchema.setPayIntv(0);        //交费间隔
            tLJAPayPersonSchema.setPayDate(CurrentDate);        //交费日期
            tLJAPayPersonSchema.setPayType("ZC");        //交费类型
            tLJAPayPersonSchema.setEnterAccDate(CurrentDate); //到帐日期
            tLJAPayPersonSchema.setConfDate(CurrentDate);         //确认日期
            tLJAPayPersonSchema.setLastPayToDate(tLCDutySchema.getFirstPayDate());  //原交至日期
            tLJAPayPersonSchema.setCurPayToDate(tLCDutySchema.getPaytoDate());    //现交至日期
            tLJAPayPersonSchema.setInInsuAccState("0");//转入保险帐户状态
            tLJAPayPersonSchema.setApproveCode(tLCPolSchema.getApproveCode());      //复核人编码
            tLJAPayPersonSchema.setApproveDate(tLCPolSchema.getApproveDate());      //复核日期
            tLJAPayPersonSchema.setSerialNo(sNo);                              //流水号
            tLJAPayPersonSchema.setOperator(mOperate);      //操作员
            tLJAPayPersonSchema.setMakeDate(CurrentDate);                        //入机日期
            tLJAPayPersonSchema.setMakeTime(CurrentTime);                        //入机时间
            tLJAPayPersonSchema.setGetNoticeNo(tNoticeNo);//通知书号码
            tLJAPayPersonSchema.setModifyDate(CurrentDate);                      //最后一次修改日期
            tLJAPayPersonSchema.setModifyTime(CurrentTime);                      //最后一次修改时间
            tLJAPayPersonSchema.setManageCom(tLCPolSchema.getManageCom());    //管理机构
            tLJAPayPersonSchema.setAgentCom(tLCPolSchema.getAgentCom());//代理机构
            tLJAPayPersonSchema.setAgentType(tLCPolSchema.getAgentType());      //代理机构内部分类
            tLJAPayPersonSchema.setRiskCode(tLCPolSchema.getRiskCode());      //险种编码
            tLJAPayPersonSchema.setAgentCode(tLCPolSchema.getAgentCode());
            tLJAPayPersonSchema.setAgentGroup(tLCPolSchema.getAgentGroup());
            mLJAPayPersonSet.add(tLJAPayPersonSchema);

            return true;
        }

        /**
        *准备需要保存的数据
        **/
        private void prepareOutputData()
        {
            LOBonusPolSet tLOBonusPolSet = new LOBonusPolSet();
            tLOBonusPolSet.set( m2LOBonusPolSet );

            LJAPaySet tLJAPaySet = new LJAPaySet();
            tLJAPaySet.set( mLJAPaySet );

            LJAPayPersonSet tLJAPayPersonSet = new LJAPayPersonSet();
            tLJAPayPersonSet.set( mLJAPayPersonSet );

            LJAGetSet tLJAGetSet = new LJAGetSet();
            tLJAGetSet.set( mLJAGetSet );

            LJABonusGetSet tLJABonusGetSet = new LJABonusGetSet();
            tLJABonusGetSet.set( mLJABonusGetSet );

            LCInsureAccSet tLCInsureAccSet = new LCInsureAccSet();
            tLCInsureAccSet.set( mLCInsureAccSet );

            LCInsureAccTraceSet tLCInsureAccTraceSet = new LCInsureAccTraceSet();
            tLCInsureAccTraceSet.set( mLCInsureAccTraceSet );

            LJFIGetSet tLJFIGetSet = new LJFIGetSet();
            tLJFIGetSet.set(mLJFIGetSet);

            LJTempFeeClassSet tLJTempFeeClassSet = new LJTempFeeClassSet();
            tLJTempFeeClassSet.set(mLJTempFeeClassSet);

            LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();
            tLJTempFeeSet.set(mLJTempFeeSet);

            LCGetSet tLCGetSet = new LCGetSet();
            tLCGetSet.set( mLCGetSet );

            LCPremSet tLCPremSet = new LCPremSet();
            tLCPremSet.set( mLCPremSet );

            LCDutySet tLCDutySet = new LCDutySet();
            tLCDutySet.set( mLCDutySet );

            mInputData.clear();
            mInputData.add( tLOBonusPolSet );
            mInputData.add( tLJAPaySet );
            mInputData.add( tLJAPayPersonSet);
            mInputData.add( tLJAGetSet);
            mInputData.add( tLJABonusGetSet);
            mInputData.add( tLCInsureAccSet);
            mInputData.add( tLCInsureAccTraceSet);
            mInputData.add( tLJFIGetSet);
            mInputData.add( tLJTempFeeClassSet);
            mInputData.add( tLJTempFeeSet);
            mInputData.add(tLCGetSet);
            mInputData.add(tLCPremSet);
            mInputData.add(tLCDutySet);

            mInputData.add(mLCPolSet);
        }

        private boolean updateLCPolBonusForZero(LCPolSchema tLCPolSchema,int tBonusYear)
        {

            Connection conn = DBConnPool.getConnection();
            if (conn==null)
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "AssignBonus";
                tError.functionName = "getByRePay";
                tError.errorMessage = "数据库连接失败!";
                this.mErrors .addOneError(tError) ;
                return false;
            }
            try
            {
                conn.setAutoCommit(false);
                String strSQL="update LOBonusPol set AGetDate='"+PubFun.getCurrentDate()+"' , BonusFlag='1'";
                strSQL=strSQL+" , ModifyDate='"+PubFun.getCurrentDate()+"' , ModifyTime='"+PubFun.getCurrentTime()+"'";
                strSQL=strSQL+" where PolNo='"+tLCPolSchema.getPolNo()+"' and FiscalYear="+tBonusYear;
                /**保单红利表更新**/
                ExeSQL tExeSQL = new ExeSQL(conn);
                if(!tExeSQL.execUpdateSQL(strSQL))
                {
                    this.mErrors.copyAllErrors(tExeSQL.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "AssignBonus";
                    tError.functionName = "getByRePay";
                    tError.errorMessage = "保单红利表数据更新失败!";
                    this.mErrors .addOneError(tError) ;
                    conn.rollback();
                    conn.close();
                    return false;
                }
                conn.commit();
                }catch(Exception ex)
                {
                    CError tError =new CError();
                    tError.moduleName="AssignBonus";
                    tError.functionName="getByRePay";
                    tError.errorMessage=ex.toString();
                    this.mErrors .addOneError(tError);
                    try
                    {
                        conn.rollback() ;
                    }
                    catch(Exception e){}
                    return false;
                }

                return true;
        }

        /**
         * 纪录错误信息
         * @param tSerialNo
         * @param tPolNo
         * @param errDescribe
         * @param tGetMode
         * @return
         */
        public boolean insertErrLog(String tSerialNo,String tPolNo,String errDescribe,String tGetMode)
        {
            LOBonusAssignErrLogDB tLOBonusAssignErrLogDB= new LOBonusAssignErrLogDB();
            tLOBonusAssignErrLogDB.setSerialNo(tSerialNo);
            tLOBonusAssignErrLogDB.setPolNo(tPolNo);
            tLOBonusAssignErrLogDB.setGetMode(tGetMode);
            tLOBonusAssignErrLogDB.seterrMsg(errDescribe);
            tLOBonusAssignErrLogDB.setmakedate(PubFun.getCurrentDate());
            tLOBonusAssignErrLogDB.setmaketime(PubFun.getCurrentTime());
            if(tLOBonusAssignErrLogDB.insert()==false)
            {
                CError tError =new CError();
                tError.moduleName="AssignBonus";
                tError.functionName="insertErrLog";
                tError.errorMessage="纪录错误日志时发生错误："+tLOBonusAssignErrLogDB.mErrors.getFirstError()+"；解决该问题后，请再次分配当日红利";
                this.mErrors .addOneError(tError);
                return false;
            }
            return true;
        }

        /**
         *
         * @return
         */
        private boolean init()
        {
            m2LOBonusPolSet.clear();
            mLJAPaySet.clear();
            mLJAPayPersonSet.clear();
            mLJAGetSet.clear();
            mLJABonusGetSet.clear();
            mLCInsureAccSet.clear();
            mLCInsureAccTraceSet.clear();
            mLJFIGetSet.clear();
            mLJTempFeeClassSet.clear();
            mLJTempFeeSet.clear();
            mLCGetSet.clear();
            mLCPremSet.clear();
            mLCDutySet.clear();
            return true;
        }

}
