package com.sinosoft.lis.cbcheck;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 修改或新增核保特约</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 1.0
 */

import com.sinosoft.utility.*;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.JdbcUrl;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import java.sql.*;
import com.sinosoft.lis.pubfun.*;

public class UWModifySpecBLS {
  /** 传入数据的容器 */
  private VData mInputData = new VData();
  /** 传出数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors=new CErrors();

  /** 传入的业务数据 */
  private LCSpecSet inLCSpecSet = new LCSpecSet();
  private LCSpecSet delLCSpecSet = new LCSpecSet();

  public UWModifySpecBLS() {
  }

  /**
   * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
   * @param cInputData 传入的数据,VData对象
   * @param cOperate 数据操作字符串，主要包括"INSERT"
   * @return 布尔值（true--提交成功, false--提交失败）
   */
  public boolean submitData(VData cInputData, String cOperate) {
    //将操作数据拷贝到本类中
    this.mInputData = (VData)cInputData.clone();
    this.mOperate =cOperate;

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData()) return false;
    System.out.println("---End getInputData---");

    //保存信息
    if (!saveData()) return false;
    System.out.println("---End saveData---");

    return true;
  }

  /**
   * 将外部传入的数据分解到本类的属性中
   * @param: 无
   * @return: boolean
   */
  private boolean getInputData()	{
    try {
      inLCSpecSet = (LCSpecSet)mInputData.getObjectByObjectName("LCSpecSet", 0);
      delLCSpecSet = (LCSpecSet)mInputData.getObjectByObjectName("LCSpecSet", 1);
    }
    catch (Exception e) {
      // @@错误处理
      e.printStackTrace();
      CError tError = new CError();
      tError.moduleName = "UWModifySpecBLS";
      tError.functionName = "getInputData";
      tError.errorMessage = "接收数据失败!!";
      this.mErrors.addOneError(tError) ;
      return false;
    }

    return true;
  }

  /**
   * 数据库操作
   * @return: boolean
   */
  private boolean saveData() {
    System.out.println("---Start Save---");

    //建立数据库连接
    Connection conn=DBConnPool.getConnection();
    if (conn == null) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "UWModifySpecBLS";
      tError.functionName = "saveData";
      tError.errorMessage = "数据库连接失败!";
      this.mErrors.addOneError(tError) ;
      return false;
    }

    try {
      //开始事务，锁表
      conn.setAutoCommit(false);

      if (mOperate.equals("INSERT")) {
        //删除特约信息表
        //System.out.println("delLCSpecSet:" + delLCSpecSet.size() + " : " + delLCSpecSet.encode());
        LCSpecDBSet delLCSpecDBSet = new LCSpecDBSet(conn);
        delLCSpecDBSet.set(delLCSpecSet);
        if (!delLCSpecDBSet.delete()) {
          try { conn.rollback(); } catch (Exception e) {}
          conn.close();
          System.out.println("LCSpec Insert Failed");
          return false;
        }

        //更新特约信息表
        //System.out.println("inLCSpecSet:" + inLCSpecSet.size() + " : " + inLCSpecSet.encode());
        LCSpecDBSet tLCSpecDBSet = new LCSpecDBSet(conn);
        tLCSpecDBSet.set(inLCSpecSet);
        if (!tLCSpecDBSet.insert()) {
          try { conn.rollback(); } catch (Exception e) {}
          conn.close();
          System.out.println("LCSpec Insert Failed");
          return false;
        }
      }

      conn.commit();
      conn.close();
      System.out.println("---End Committed---");
    }
    catch (Exception e) {
      // @@错误处理
      e.printStackTrace();
      CError tError = new CError();
      tError.moduleName = "UWModifySpecBLS";
      tError.functionName = "submitData";
      tError.errorMessage = e.toString();
      this.mErrors.addOneError(tError);
      try{ conn.rollback() ;} catch(Exception ex){}
      return false;
    }

    return true;
  }

  /**
   * 数据输出方法，供外界获取数据处理结果
   * @return 包含有数据查询结果字符串的VData对象
   */
  public VData getResult() {
    return mResult;
  }

  /**
   * 主函数，测试用
   */
  public static void main(String[] args) {
    UWModifySpecBLS UWModifySpecBLS1 = new UWModifySpecBLS();
  }
}
