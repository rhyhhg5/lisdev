package com.sinosoft.lis.cbcheck;

import java.sql.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统被保人资料变动功能部分 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft< /p>
 * @author Tjj
 * @version 1.0
 */

public class UWModifyTypeBCBLS
{
  //传输数据类
  private VData mInputData ;
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors = new CErrors();
  //操作符
  public String mOperate;

  public UWModifyTypeBCBLS() {}

  public static void main(String[] args)
  {
  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
       LCBnfSet tLCBnfSet = new LCBnfSet();
	   LCBnfSet bLCBnfSet = new LCBnfSet();
	   String Flag;

	   tLCBnfSet = (LCBnfSet)cInputData.getObjectByObjectName("LCBnfSet",0);
	   bLCBnfSet = (LCBnfSet)cInputData.getObjectByObjectName("LCBnfSet",1);


	   Connection conn = null;
	   conn = DBConnPool.getConnection();

	   if (conn==null)
	   {
		 // @@错误处理
		 CError tError = new CError();
		 tError.moduleName = "PEdorAppConfirmWTBLS";
		 tError.functionName = "saveData";
		 tError.errorMessage = "数据库连接失败!";
		 this.mErrors .addOneError(tError) ;
		 return false;
	   }

	   try
	   {
		 conn.setAutoCommit(false);

		 if (bLCBnfSet != null && bLCBnfSet.size() > 0)
		 {
		   for(int i = 1; i<= bLCBnfSet.size();i++)
		   {
			 LCBnfDB tLCBnfDB = new LCBnfDB(conn);
			 tLCBnfDB.setPolNo(bLCBnfSet.get(i).getPolNo());
			 tLCBnfDB.setBnfNo(bLCBnfSet.get(i).getBnfNo());
			 tLCBnfDB.setBnfType(bLCBnfSet.get(i).getBnfType());
			 if (!tLCBnfDB.deleteSQL())
			 {
			   // @@错误处理
			   CError tError = new CError();
			   tError.moduleName = "UWModifyTypeBCBLS";
			   tError.functionName = "saveData";
			   tError.errorMessage = "受益人变动信息删除失败!";
			   this.mErrors .addOneError(tError) ;
			   conn.rollback();
			   conn.close();
			   return false;
			 }
		   }
		 }


		 if (tLCBnfSet!=null&&tLCBnfSet.size()>0)
		 {
		   for(int i = 1; i<= tLCBnfSet.size();i++)
		   {
		   LCBnfDB tLCBnfDB = new LCBnfDB(conn);
		   tLCBnfDB.setSchema(tLCBnfSet.get(i)) ;
		   if (!tLCBnfDB.insert())
		   {
			 // @@错误处理
			 CError tError = new CError();
			 tError.moduleName = "PEdorBCDetailBLS";
			 tError.functionName = "saveData";
			 tError.errorMessage = "受益人变动信息保存失败!";
			 this.mErrors .addOneError(tError) ;
			 conn.rollback();
			 conn.close();
			 return false;
		   }
		 }
		 }

		 conn.commit();
		 conn.close();
	   }
	   catch (Exception ex)
	   {
		 // @@错误处理
		 CError tError = new CError();
		 tError.moduleName = "PEdorBCDetailBLS";
		 tError.functionName = "submitData";
		 tError.errorMessage = ex.toString();
		 this.mErrors .addOneError(tError);
		 try
		 {
		   conn.rollback() ;
		   conn.close();
		   } catch(Exception e){}
		 return false;
	   }
	   return true;
  }

}