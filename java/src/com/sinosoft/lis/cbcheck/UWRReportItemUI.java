package com.sinosoft.lis.cbcheck;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCRReportResultSchema;
import com.sinosoft.lis.vschema.LCRReportResultSet;
import com.sinosoft.lis.schema.LCRReportSchema;
import com.sinosoft.lis.vschema.LCRReportSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;


/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: SinoSoft</p>
 * @author HYQ
 * @version 1.0
 */

public class UWRReportItemUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    private VData mInputData;
    /** 数据操作字符串 */
    private String mOperate;
    public UWRReportItemUI()
    {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        this.mOperate = cOperate;

        UWRReportItemBL tUWRReportItemBL = new UWRReportItemBL();
        tUWRReportItemBL.submitData(cInputData, cOperate);

        //如果有需要处理的错误，则返回
        if (tUWRReportItemBL.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tUWRReportItemBL.mErrors);
        }
        mInputData = null;
        mResult = tUWRReportItemBL.getResult();

        return true;
    }

    /**
     * 返回结果方法
     * @return VData
     */
    public VData getResult()
    {
        return mResult;
    }

}
