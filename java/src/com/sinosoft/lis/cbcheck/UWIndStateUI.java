package com.sinosoft.lis.cbcheck;

import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;

/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class UWIndStateUI {
    /** �������� */
    public CErrors mErrors = new CErrors();

    public UWIndStateUI() {
    }

    public static void main(String[] args) {
        UWIndStateUI uwindstateui = new UWIndStateUI();
    }

    public boolean submitData(VData cInputData, String cOperate) {
        UWIndStateBL tUWIndStateBL = new UWIndStateBL();
        if (!tUWIndStateBL.submitData(cInputData, cOperate)) {
            this.mErrors.copyAllErrors(tUWIndStateBL.mErrors);
            return false;
        }
        return true;
    }

}
