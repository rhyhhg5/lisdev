package com.sinosoft.lis.cbcheck;

import com.sinosoft.utility.*;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.JdbcUrl;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import java.sql.*;
import com.sinosoft.lis.pubfun.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class QuestionAdditionalRiskChkBLS  {
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors=new CErrors();
  private VData mInputData ;
  private LCIssuePolSet mLCIssuePolSet ;

  public QuestionAdditionalRiskChkBLS() {
  }
  public static void main(String[] args) {
    QuestionAdditionalRiskChkBLS proposalBLS1 = new QuestionAdditionalRiskChkBLS();
  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    boolean tReturn =false;
    //首先将数据在本类中做一个备份
    mInputData=(VData)cInputData.clone() ;

    tReturn=save();
    if (tReturn)
      System.out.println("Save sucessful");
    else
        System.out.println("Save failed") ;

    mInputData=null;
    return tReturn;
  }

  private boolean save()
  {
    System.out.println("Start Save...");
    Connection conn=DBConnPool.getConnection();
    if (conn==null)
    {
    		// @@错误处理
    		CError tError = new CError();
		tError.moduleName = "QuestionAdditionalRiskChkBLS";
                tError.functionName = "save";
	        tError.errorMessage = "数据库连接失败!";
		this.mErrors .addOneError(tError) ;
 		return false;
    }
    try{
      conn.setAutoCommit(false);
      System.out.println("Start 信息...");
      //问题件表数据保存
      LCIssuePolDB tLCIssuePolDB=new LCIssuePolDB(conn);
      tLCIssuePolDB.setSchema((LCIssuePolSchema)mInputData.getObjectByObjectName("LCIssuePolSchema",0));
      if(!tLCIssuePolDB.insert())
      {
    		// @@错误处理
         this.mErrors.copyAllErrors(tLCIssuePolDB.mErrors);
    	 CError tError = new CError();
	 tError.moduleName = "QuestionAdditionalRiskChkBLS";
	 tError.functionName = "saveData";
	 tError.errorMessage = "问题件表数据保存失败!";
	 this.mErrors .addOneError(tError) ;
         conn.rollback() ;
         conn.close();
 	 return false;
      }
      //投保单表数据更新
      LCPolDB tLCPolDB=new LCPolDB(conn);
      tLCPolDB.setSchema((LCPolSchema)mInputData.getObjectByObjectName("LCPolSchema",0));
      if(!tLCPolDB.update())
      {
                    // @@错误处理
         this.mErrors.copyAllErrors(tLCPolDB.mErrors);
         CError tError = new CError();
         tError.moduleName = "QuestionAdditionalRiskChkBLS";
         tError.functionName = "saveData";
         tError.errorMessage = "投保单表数据更新失败!";
         this.mErrors .addOneError(tError) ;
         conn.rollback() ;
         conn.close();
         return false;
      }
          conn.commit() ;
          conn.close();
    }
    catch (Exception ex)
    {
      System.out.println("Exception:"+ex.toString());
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="QuestionAdditionalRiskChkBLS";
      tError.functionName="submitData";
      tError.errorMessage=ex.toString() ;
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
  }

}