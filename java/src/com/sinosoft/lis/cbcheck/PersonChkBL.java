package com.sinosoft.lis.cbcheck;

import java.lang.*;
import java.util.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.cbcheck.*;


/**
 * <p>Title: Web业务系统相同客户查询部分</p>
 * <p>Description: 逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author WHN
 * @version 1.0
 */
public class PersonChkBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();


    /** 往后面传输数据的容器 */
    private VData mInputData;


    /** 往界面传输数据的容器 */
    private VData mResult = new VData();


    /** 数据操作字符串 */
    private String mOperate;
    private String mIsueManageCom;
    private String mManageCom;
    private String mpassflag; //通过标记
    private int merrcount; //错误条数
    private String mCalCode; //计算编码
    private String mUser;
    private FDate fDate = new FDate();
    private double mValue;
    private String mInsuredNo = "";
    private String mBackObj = "";
    private String mflag = ""; //A 投保人  I b被保人
    private String mTempFeeNo = ""; //交费收据号


    /** 业务处理相关变量 */
    private LCPolSet mLCPolSet = new LCPolSet();
    private LCPolSet mmLCPolSet = new LCPolSet();
    private LCPolSet m2LCPolSet = new LCPolSet();
    private LCPolSet mAllLCPolSet = new LCPolSet();
    private LCContSchema mLCContSchema = new LCContSchema();
    private String mContNo = "";
    private String mOldPolNo = "";


    /** 集体单表 */
    private LCGrpPolSet mAllLCGrpPolSet = new LCGrpPolSet();
    private LCGrpPolSet mLCGrpPolSet = new LCGrpPolSet();
    private LCGrpPolSchema mLCGrpPolSchema = new LCGrpPolSchema();


    /** 保费项表 */
    private LCPremSet mLCPremSet = new LCPremSet();
    private LCPremSet mAllLCPremSet = new LCPremSet();


    /** 领取项表 */
    private LCGetSet mLCGetSet = new LCGetSet();
    private LCGetSet mAllLCGetSet = new LCGetSet();


    /** 责任表 */
    private LCDutySet mLCDutySet = new LCDutySet();
    private LCDutySet mAllLCDutySet = new LCDutySet();


    /** 特别约定表 */
    private LCSpecSet mLCSpecSet = new LCSpecSet();
    private LCSpecSet mAllLCSpecSet = new LCSpecSet();


    /** 特别约定注释表 */
    private LCSpecNoteSet mLCSpecNoteSet = new LCSpecNoteSet();
    private LCSpecNoteSet mAllLCSpecNoteSet = new LCSpecNoteSet();


    /** 核保主表 */
    private LCUWMasterSet mLCUWMasterSet = new LCUWMasterSet();
    private LCUWMasterSet mAllLCUWMasterSet = new LCUWMasterSet();
    private LCUWMasterSchema mLCUWMasterSchema = new LCUWMasterSchema();


    /** 核保子表 */
    private LCUWSubSet mLCUWSubSet = new LCUWSubSet();
    private LCUWSubSet mAllLCUWSubSet = new LCUWSubSet();
    private LCUWSubSchema mLCUWSubSchema = new LCUWSubSchema();


    /** 核保错误信息表 */
    private LCUWErrorSet mLCUWErrorSet = new LCUWErrorSet();
    private LCUWErrorSet mAllLCErrSet = new LCUWErrorSet();


    /** 告知表 */
    private LCCustomerImpartSet mLCCustomerImpartSet = new LCCustomerImpartSet();
    private LCCustomerImpartSet mAllLCCustomerImpartSet = new
            LCCustomerImpartSet();


    /** 投保人表 */
    private LCAppntSet mLCAppntSet = new LCAppntSet();
    private LCAppntSet mAllLCAppntSet = new LCAppntSet();


    /** 受益人表 */
    private LCBnfSet mLCBnfSet = new LCBnfSet();
    private LCBnfSet mAllLCBnfSet = new LCBnfSet();


    /** 被保险人表 */
    private LCInsuredSet mLCInsuredSet = new LCInsuredSet();
    private LCInsuredSet mAllLCInsuredSet = new LCInsuredSet();


    /** 体检资料主表 */
    private LCPENoticeSet mLCPENoticeSet = new LCPENoticeSet();
    private LCPENoticeSet mAllLCPENoticeSet = new LCPENoticeSet();
    private LCPENoticeSchema mLCPENoticeSchema = new LCPENoticeSchema();


    /** 体检资料项目表 */
    private LCPENoticeItemSet mLCPENoticeItemSet = new LCPENoticeItemSet();
    private LCPENoticeItemSet mmLCPENoticeItemSet = new LCPENoticeItemSet();
    private LCPENoticeItemSet mAllLCPENoticeItemSet = new LCPENoticeItemSet();


    /** 问题件表 */
    private LCIssuePolSet mLCIssuePolSet = new LCIssuePolSet();
    private LCIssuePolSet mmLCIssuePolSet = new LCIssuePolSet();
    private LCIssuePolSet mAllLCIssuePolSet = new LCIssuePolSet();


    /** 暂交费表 */
    private LJTempFeeSet mLJTempFeeSet = new LJTempFeeSet();
    private LJTempFeeSet mAllLJTempFeeSet = new LJTempFeeSet();


    /** 暂交费关联表 */
    private LJTempFeeClassSchema mLJTempFeeClassSchema = new
            LJTempFeeClassSchema();
    private LJTempFeeClassSet mLJTempFeeClassSet = new LJTempFeeClassSet();
    private LJTempFeeClassSet mmLJTempFeeClassSet = new LJTempFeeClassSet();
    private LJTempFeeClassSet mAllLJTempFeeClassSet = new LJTempFeeClassSet();


    /** 分红计算表 */
    private LOBonusMainSchema mLOBonusMainSchema = new LOBonusMainSchema();
    private LOBonusMainSet mLOBonusMainSet = new LOBonusMainSet();
    private LOBonusMainSet mmLOBonusMainSet = new LOBonusMainSet();
    private LOBonusMainSet m2LOBonusMainSet = new LOBonusMainSet();
    private LOBonusMainSet mAllLOBonusMainSet = new LOBonusMainSet();


    /** 实收总表 */
    private LJAPaySet mLJAPaySet = new LJAPaySet();
    private LJAPaySet mAllLJAPaySet = new LJAPaySet();


    /** 个人实收表 */
    private LJAPayPersonSchema mLJAPayPersonSchema = new LJAPayPersonSchema();
    private LJAPayPersonSet mLJAPayPersonSet = new LJAPayPersonSet();
    private LJAPayPersonSet m2LJAPayPersonSet = new LJAPayPersonSet();
    private LJAPayPersonSet mAllLJAPayPersonSet = new LJAPayPersonSet();


    /** 给付总表*/
    private LJAGetSet mLJAGetSet = new LJAGetSet();
    private LJAGetSet mAllLJAGetSet = new LJAGetSet();


    /** 红利给付表 */
    private LJABonusGetSet mLJABonusGetSet = new LJABonusGetSet();
    private LJABonusGetSet mAllLJABonusGetSet = new LJABonusGetSet();


    /** 帐户表 */
    private LCInsureAccSet mLCInsureAccSet = new LCInsureAccSet();
    private LCInsureAccSet mAllLCInsureAccSet = new LCInsureAccSet();


    /** 帐户轨迹表 */
    private LCInsureAccTraceSet mLCInsureAccTraceSet = new LCInsureAccTraceSet();
    private LCInsureAccTraceSet mAllLCInsureAccTraceSet = new
            LCInsureAccTraceSet();


    /** 暂交费退费应付表 */
    private LJAGetTempFeeSet mLJAGetTempFeeSet = new LJAGetTempFeeSet();
    private LJAGetTempFeeSet mAllLJAGetTempFeeSet = new LJAGetTempFeeSet();


    /** 客户信息表 */
    private LDPersonSet mLDPersonSet = new LDPersonSet();
    private LDPersonSet mAllLDPersonSet = new LDPersonSet();


    /**计算公式表**/
    private LMUWSchema mLMUWSchema = new LMUWSchema();


    //private LMUWDBSet mLMUWDBSet = new LMUWDBSet();
    private LMUWSet mLMUWSet = new LMUWSet();

    private CalBase mCalBase = new CalBase();

    public PersonChkBL()
    {}


    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        int flag = 0; //判断是不是所有数据都不成功
        int j = 0; //符合条件数据个数

        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        //GlobalInput tGlobalInput = new GlobalInput();
        //this.mOperate = tGlobalInput.;

        System.out.println("---2---");
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;
        System.out.println("---PersonChkBL getInputData---");

        // 数据操作业务处理
        if (!dealData())
            return false;
        else
        {
            flag = 1;
        }
        System.out.println("---PersonChkBL checkData---");

        if (flag == 0)
        {
            CError tError = new CError();
            tError.moduleName = "PersonChkBL";
            tError.functionName = "submitData";
            tError.errorMessage = "校验失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        System.out.println("---PersonChkBL dealData---");
        //准备给后台的数据
        prepareOutputData();

        System.out.println("---PersonChkBL prepareOutputData---");
        //数据提交
        return true;
    }


    /**
     * 数据操作类业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean dealData()
    {

        if (dealOnePol() == false)
            return false;

        return true;
    }


    /**
     * 操作一张保单的业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean dealOnePol()
    {
        // 健康信息
        if (mflag.equals("A"))
        {
            if (prepareAppnt() == false)
                return false;
        }
        if (mflag.equals("I"))
        {
            if (prepareInsured() == false)
                return false;
        }

        return true;
    }


    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mOperate = tGlobalInput.Operator;
        mManageCom = tGlobalInput.ManageCom;

        mLCContSchema.setSchema((LCContSchema) cInputData.getObjectByObjectName(
                "LCContSchema", 0));

        int flag = 0; //怕判断是不是所有保单都失败
        int j = 0; //符合条件保单个数

        if (mLCContSchema != null)
        {
            mContNo = mLCContSchema.getContNo();
            mInsuredNo= mLCContSchema.getInsuredNo();
            mflag = mLCContSchema.getAppFlag();
            System.out.println("Contno:" + mContNo);
            System.out.println("InsuredNo:" + mInsuredNo);
            System.out.println("flag:" + mflag);
        }
        else
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "PersonChkBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有传入数据!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    /**
     * 准备客户信息
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean prepareAppnt()
    {
        /**
         *取保单信息
         */
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mContNo);
        if (tLCContDB.getInfo() == false)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PersonChkBL";
            tError.functionName = "prepareAppnt";
            tError.errorMessage = "没有" + mContNo + "保单!";
            this.mErrors.addOneError(tError);
            return false;
        }

        LCContSchema tLCContSchema = new LCContSchema();
        tLCContSchema = tLCContDB.getSchema();

        String tAppntNo = tLCContSchema.getAppntNo();

        LCAppntSchema tLCAppntSchema = new LCAppntSchema();
        LCAppntDB tLCAppntDB = new LCAppntDB();
        tLCAppntDB.setContNo(tLCContSchema.getContNo());
        tLCAppntDB.setAppntNo(tAppntNo);

        if (tLCAppntDB.getInfo() == false)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PersonChkBL";
            tError.functionName = "prepareAppnt";
            tError.errorMessage = "没有" + tAppntNo + "客户!";
            this.mErrors.addOneError(tError);
            return false;
        }

        tLCAppntSchema = tLCAppntDB.getSchema();
        mLCAppntSet.add(tLCAppntSchema);

        //LCAppntDB t2LCAppntDB = new LCAppntDB();
        //LCAppntSet tLCAppntSet = new LCAppntSet();
        String tsql = "select distinct customerno from LDPerson where name = '" +
                      tLCAppntDB.getAppntName() + "' and Birthday = '" +
                      tLCAppntDB.getAppntBirthday() + "' and Sex = '" +
                      tLCAppntDB.getAppntSex() +
                      "' and CustomerNo <> '" + tAppntNo + "'";
        //t2LCAppntDB.setName(tLCAppntDB.getName());
        //t2LCAppntDB.setBirthday(tLCAppntDB.getBirthday());
        //t2LCAppntDB.setSex(tLCAppntDB.getSex());

        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(tsql);
        System.out.println(tsql);
        if (tSSRS.MaxRow > 0)
        {
            for (int i = 1; i <= tSSRS.MaxRow; i++)
            {
                LDPersonDB tLDPersonDB = new LDPersonDB();
                tLDPersonDB.setCustomerNo(tSSRS.GetText(1, i));

                if (tLDPersonDB.getInfo() == false)
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "PersonChkBL";
                    tError.functionName = "prepareAppnt";
                    tError.errorMessage = "没有" + tSSRS.GetText(1, i) + "客户信息!";
                    this.mErrors.addOneError(tError);
                    //return false;
                }
                else
                {
                    LDPersonSchema tLDPersonSchema = new LDPersonSchema();
                    tLDPersonSchema = tLDPersonDB.getSchema();
                    mLDPersonSet.add(tLDPersonSchema);
                }
            }
        }
        else
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PersonChkBL";
            tError.functionName = "prepareAppnt";
            tError.errorMessage = "没有相同客户信息!";
            this.mErrors.addOneError(tError);
            //return false;
        }

        return true;
    }


    /**
     * 准备分红信息
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean prepareInsured()
    {
        /**
         *取保单信息
         */
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mContNo);
        if (tLCContDB.getInfo() == false)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PersonChkBL";
            tError.functionName = "prepareInsured";
            tError.errorMessage = "没有" + mContNo + "保单!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //LCContSchema tLCContSchema = new LCContSchema();
        //tLCContSchema = tLCContDB.getSchema();

        //String tInsuredNo = tLCContSchema.getInsuredNo();

        LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        tLCInsuredDB.setContNo(mContNo);
        tLCInsuredDB.setInsuredNo(mInsuredNo);
        if (tLCInsuredDB.getInfo() == false)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PersonChkBL";
            tError.functionName = "prepareInsured";
            tError.errorMessage = "没有" + mInsuredNo + "客户!";
            this.mErrors.addOneError(tError);
            return false;
        }

        tLCInsuredSchema = tLCInsuredDB.getSchema();
        mLCInsuredSet.add(tLCInsuredSchema);

        String tsql = "";
        tsql = "select distinct customerno from LDPerson where name = '" +
                      tLCInsuredDB.getName() + "' and Birthday = '" +
                      tLCInsuredDB.getBirthday() + "' and Sex = '" +
                      tLCInsuredDB.getSex() + "' and CustomerNo <> '" +
                      mInsuredNo + "'";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(tsql);

        if (tSSRS.MaxRow > 0)
        {
            for (int i = 1; i <= tSSRS.MaxRow; i++)
            {
                LDPersonDB tLDPersonDB = new LDPersonDB();
                tLDPersonDB.setCustomerNo(tSSRS.GetText(1, i));

                if (tLDPersonDB.getInfo() == false)
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "PersonChkBL";
                    tError.functionName = "prepareInsured";
                    tError.errorMessage = "没有" + tSSRS.GetText(1, i) + "客户信息!";
                    this.mErrors.addOneError(tError);
                    //return false;
                }
                else
                {
                    LDPersonSchema tLDPersonSchema = new LDPersonSchema();
                    tLDPersonSchema = tLDPersonDB.getSchema();
                    mLDPersonSet.add(tLDPersonSchema);
                }
            }
        }
        else
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PersonChkBL";
            tError.functionName = "prepareInsured";
            tError.errorMessage = "没有相同客户信息!";
            this.mErrors.addOneError(tError);
            //return false;
        }

        return true;
    }


    /**
     *准备需要保存的数据
     **/
    private void prepareOutputData()
    {
        int j = 1;
        LDPersonSet tLDPersonSet = new LDPersonSet();
        tLDPersonSet.set(mLDPersonSet);
        mAllLDPersonSet.add(tLDPersonSet);

        mInputData.clear();
        mInputData.add(mAllLDPersonSet);

        if (mflag.equals("A"))
        {
            LCAppntSet tLCAppntSet = new LCAppntSet();
            tLCAppntSet.set(mLCAppntSet);
            mAllLCAppntSet.add(tLCAppntSet);

            mInputData.add(mAllLCAppntSet);
        }

        if (mflag.equals("I"))
        {
            LCInsuredSet tLCInsuredSet = new LCInsuredSet();
            tLCInsuredSet.set(mLCInsuredSet);
            mAllLCInsuredSet.add(tLCInsuredSet);

            mInputData.add(mAllLCInsuredSet);
        }

        System.out.println("size1:" + mInputData.size());
    }

    public VData getResult()
    {
        return mInputData;
    }
}
