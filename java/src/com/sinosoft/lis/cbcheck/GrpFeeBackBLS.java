package com.sinosoft.lis.cbcheck;

import java.sql.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统集体充帐部分 </p>
 * <p>Description: 数据库功能类</p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft< /p>
 * @author WHN
 * @version 1.0
 */

public class GrpFeeBackBLS
{
        //是否存在需要人工核保保单
	int merrno = 0;
	//传输数据类
	private VData mInputData ;
	//错误处理类，每个需要错误处理的类中都放置该类
	public  CErrors mErrors = new CErrors();

	//
	private LCUWErrorSet mLCUWErrorSet = new LCUWErrorSet();

	public GrpFeeBackBLS() {}

	public static void main(String[] args)
	{
	}

	//传输数据的公共方法
	public boolean submitData(VData cInputData,String cOperate)
	{
	    //首先将数据在本类中做一个备份
	    mInputData=(VData)cInputData.clone() ;

	System.out.println("Start GrpFeeBackBLS Submit...");
		if (!this.saveData())
			return false;
	System.out.println("End GrpFeeBackBLS Submit...");

	    mInputData=null;
	    return true;
	}

	private boolean saveData()
	{

          LCPolSet mLCPolSet = (LCPolSet)mInputData.getObjectByObjectName("LCPolSet",0);
          LCGrpPolSet mLCGrpPolSet = (LCGrpPolSet)mInputData.getObjectByObjectName("LCGrpPolSet",0);
          LJAPayPersonSet mLJAPayPersonSet = (LJAPayPersonSet)mInputData.getObjectByObjectName("LJAPayPersonSet",0);
          LJAPayGrpSet mLJAPayGrpSet = (LJAPayGrpSet)mInputData.getObjectByObjectName("LJAPayGrpSet",0);
          LJAGetTempFeeSet mLJAGetTempFeeSet = (LJAGetTempFeeSet)mInputData.getObjectByObjectName("LJAGetTempFeeSet",0);
          LJAGetSet mLJAGetSet = (LJAGetSet)mInputData.getObjectByObjectName("LJAGetSet",0);
          LCDutySet mLCDutySet = (LCDutySet)mInputData.getObjectByObjectName("LCDutySet",0);
          LCPremSet mLCPremSet = (LCPremSet)mInputData.getObjectByObjectName("LCPremSet",0);
          LCInsureAccSet mLCInsureAccSet = (LCInsureAccSet)mInputData.getObjectByObjectName("LCInsureAccSet",0);
          LCInsureAccTraceSet mLCInsureAccTraceSet = (LCInsureAccTraceSet)mInputData.getObjectByObjectName("LCInsureAccTraceSet",0);

          Connection conn = DBConnPool.getConnection();
          try
          {

                    if (conn==null)
                    {
                      // @@错误处理
                      CError tError = new CError();
                      tError.moduleName = "GrpFeeBackBLS";
                      tError.functionName = "saveData";
                      tError.errorMessage = "数据库连接失败!";
                      this.mErrors .addOneError(tError) ;
                      return false;
                    }

                    conn.setAutoCommit(false);

                    // 修改部分

                    //团体保单表
                    if(mLCGrpPolSet.size()>0)
                    {
                      for (int i = 1;i <= mLCGrpPolSet.size();i++)
                      {
                        LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
                        tLCGrpPolSchema = mLCGrpPolSet.get(i);

                        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB(conn);
                        tLCGrpPolDB.setSchema(tLCGrpPolSchema);
                        if(tLCGrpPolDB.update() == false)
                        {
                          // @@错误处理
                          this.mErrors.copyAllErrors(tLCGrpPolDB.mErrors);
                          CError tError = new CError();
                          tError.moduleName = "GrpFeeBackBLS";
                          tError.functionName = "saveData";
                          tError.errorMessage = "团体保单表更新失败!";
                          this.mErrors .addOneError(tError) ;
                          conn.rollback() ;
                          conn.close();
                          return false;
                        }
                      }
                    }

                    System.out.println("-----------团体保单表更新结束--------------");

                    //保单表
                    if(mLCPolSet.size()>0)
                    {
                      for (int i = 1;i <= mLCPolSet.size();i++)
                      {
                        LCPolSchema tLCPolSchema = new LCPolSchema();
                        tLCPolSchema = mLCPolSet.get(i);

                        LCPolDB tLCPolDB = new LCPolDB(conn);
                        tLCPolDB.setSchema(tLCPolSchema);
                        if(tLCPolDB.update() == false)
                        {
                          // @@错误处理
                          this.mErrors.copyAllErrors(tLCPolDB.mErrors);
                          CError tError = new CError();
                          tError.moduleName = "GrpFeeBackBLS";
                          tError.functionName = "saveData";
                          tError.errorMessage = "保单表更新失败!";
                          this.mErrors .addOneError(tError) ;
                          conn.rollback() ;
                          conn.close();
                          return false;
                        }
                      }
                    }

                    System.out.println("-----------保单表更新结束--------------");

                    //集体实收表
                    if(mLJAPayGrpSet.size()>0)
                    {
                      LJAPayGrpSchema tLJAPayGrpSchema = new LJAPayGrpSchema();
                      tLJAPayGrpSchema = mLJAPayGrpSet.get(1);
                      String tPayNo = tLJAPayGrpSchema.getPayNo();

                      LJAPayGrpDB tLJAPayGrpDB = new LJAPayGrpDB(conn);
                      tLJAPayGrpDB.setPayNo(tPayNo);
                      if(tLJAPayGrpDB.deleteSQL() == false)
                      {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tLJAPayGrpDB.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "GrpFeeBackBLS";
                        tError.functionName = "saveData";
                        tError.errorMessage = "集体实收表删除失败!";
                        this.mErrors .addOneError(tError) ;
                        conn.rollback() ;
                        conn.close();
                        return false;
                      }

                      for (int i = 1;i <= mLJAPayGrpSet.size();i++)
                      {
                        tLJAPayGrpSchema = new LJAPayGrpSchema();
                        tLJAPayGrpSchema = mLJAPayGrpSet.get(i);
                        tLJAPayGrpDB.setSchema(tLJAPayGrpSchema);
                        if(tLJAPayGrpDB.insert() == false)
                        {
                          // @@错误处理
                          this.mErrors.copyAllErrors(tLJAPayGrpDB.mErrors);
                          CError tError = new CError();
                          tError.moduleName = "GrpFeeBackBLS";
                          tError.functionName = "saveData";
                          tError.errorMessage = "集体实收表更新失败!";
                          this.mErrors .addOneError(tError) ;
                          conn.rollback() ;
                          conn.close();
                          return false;
                        }
                      }
                    }

                    System.out.println("-----------集体实收表更新结束--------------");

                    //个人实收表
                    if(mLJAPayPersonSet.size()>0)
                    {
                      LJAPayPersonSchema tLJAPayPersonSchema = new LJAPayPersonSchema();
                      tLJAPayPersonSchema = mLJAPayPersonSet.get(1);
                      String tPayNo = tLJAPayPersonSchema.getPayNo();

                      LJAPayPersonDB tLJAPayPersonDB = new LJAPayPersonDB(conn);
                      tLJAPayPersonDB.setPayNo(tPayNo);
                      if(tLJAPayPersonDB.deleteSQL() == false)
                      {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tLJAPayPersonDB.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "GrpFeeBackBLS";
                        tError.functionName = "saveData";
                        tError.errorMessage = "个人实收表删除失败!";
                        this.mErrors .addOneError(tError) ;
                        conn.rollback() ;
                        conn.close();
                        return false;
                      }

                      for (int i = 1;i <= mLJAPayPersonSet.size();i++)
                      {
                        tLJAPayPersonSchema = new LJAPayPersonSchema();
                        tLJAPayPersonSchema = mLJAPayPersonSet.get(i);
                        tLJAPayPersonDB.setSchema(tLJAPayPersonSchema);
                        if(tLJAPayPersonDB.insert() == false)
                        {
                          // @@错误处理
                          this.mErrors.copyAllErrors(tLJAPayPersonDB.mErrors);
                          CError tError = new CError();
                          tError.moduleName = "GrpFeeBackBLS";
                          tError.functionName = "saveData";
                          tError.errorMessage = "个人实收表更新失败!";
                          this.mErrors .addOneError(tError) ;
                          conn.rollback() ;
                          conn.close();
                          return false;
                        }
                      }
                    }

                    System.out.println("-----------个人实收表更新结束--------------");

                    //暂交费退费应付表
                    if(mLJAGetTempFeeSet.size()>0)
                    {
                      for (int i = 1;i <= mLJAGetTempFeeSet.size();i++)
                      {
                        LJAGetTempFeeSchema tLJAGetTempFeeSchema = new LJAGetTempFeeSchema();
                        tLJAGetTempFeeSchema = mLJAGetTempFeeSet.get(i);

                        LJAGetTempFeeDB tLJAGetTempFeeDB = new LJAGetTempFeeDB(conn);
                        tLJAGetTempFeeDB.setSchema(tLJAGetTempFeeSchema);
                        if(tLJAGetTempFeeDB.delete() == false)
                        {
                          // @@错误处理
                          this.mErrors.copyAllErrors(tLJAGetTempFeeDB.mErrors);
                          CError tError = new CError();
                          tError.moduleName = "GrpFeeBackBLS";
                          tError.functionName = "saveData";
                          tError.errorMessage = "暂交费退费应付删除失败!";
                          this.mErrors .addOneError(tError) ;
                          conn.rollback() ;
                          conn.close();
                          return false;
                        }

                        if(tLJAGetTempFeeDB.insert() == false)
                        {
                          // @@错误处理
                          this.mErrors.copyAllErrors(tLJAGetTempFeeDB.mErrors);
                          CError tError = new CError();
                          tError.moduleName = "GrpFeeBackBLS";
                          tError.functionName = "saveData";
                          tError.errorMessage = "暂交费退费应付更新失败!";
                          this.mErrors .addOneError(tError) ;
                          conn.rollback() ;
                          conn.close();
                          return false;
                        }
                      }
                    }

                    System.out.println("-----------暂交费退费应付表更新结束--------------");

                    //实付总表
                    if(mLJAGetSet.size()>0)
                    {
                      for (int i = 1;i <= mLJAGetSet.size();i++)
                      {
                        LJAGetSchema tLJAGetSchema = new LJAGetSchema();
                        tLJAGetSchema = mLJAGetSet.get(i);

                        LJAGetDB tLJAGetDB = new LJAGetDB(conn);
                        tLJAGetDB.setSchema(tLJAGetSchema);
                        if(tLJAGetDB.insert() == false)
                        {
                          // @@错误处理
                          this.mErrors.copyAllErrors(tLJAGetDB.mErrors);
                          CError tError = new CError();
                          tError.moduleName = "GrpFeeBackBLS";
                          tError.functionName = "saveData";
                          tError.errorMessage = "实付总表更新失败!";
                          this.mErrors .addOneError(tError) ;
                          conn.rollback() ;
                          conn.close();
                          return false;
                        }
                      }
                    }

                    System.out.println("-----------实付总表更新结束--------------");

                    //保险责任表
                    if(mLCDutySet.size()>0)
                    {
                      for (int i = 1;i <= mLCDutySet.size();i++)
                      {
                        LCDutySchema tLCDutySchema = new LCDutySchema();
                        tLCDutySchema = mLCDutySet.get(i);

                        LCDutyDB tLCDutyDB = new LCDutyDB(conn);
                        tLCDutyDB.setSchema(tLCDutySchema);
                        if(tLCDutyDB.update() == false)
                        {
                          // @@错误处理
                          this.mErrors.copyAllErrors(tLCDutyDB.mErrors);
                          CError tError = new CError();
                          tError.moduleName = "GrpFeeBackBLS";
                          tError.functionName = "saveData";
                          tError.errorMessage = "保险责任表更新失败!";
                          this.mErrors .addOneError(tError) ;
                          conn.rollback() ;
                          conn.close();
                          return false;
                        }
                      }
                    }

                    System.out.println("-----------保险责任表更新结束--------------");

                    //保费项表
                    if(mLCPremSet.size()>0)
                    {
                      for (int i = 1;i <= mLCPremSet.size();i++)
                      {
                        LCPremSchema tLCPremSchema = new LCPremSchema();
                        tLCPremSchema = mLCPremSet.get(i);

                        LCPremDB tLCPremDB = new LCPremDB(conn);
                        tLCPremDB.setSchema(tLCPremSchema);
                        if(tLCPremDB.update() == false)
                        {
                          // @@错误处理
                          this.mErrors.copyAllErrors(tLCPremDB.mErrors);
                          CError tError = new CError();
                          tError.moduleName = "GrpFeeBackBLS";
                          tError.functionName = "saveData";
                          tError.errorMessage = "保费项表更新失败!";
                          this.mErrors .addOneError(tError) ;
                          conn.rollback() ;
                          conn.close();
                          return false;
                        }
                      }
                    }

                    System.out.println("-----------保费项表更新结束--------------");

                    //帐户表
                    if(mLCInsureAccSet.size()>0)
                    {
                      for(int i = 1; i <= mLCInsureAccSet.size();i++)
                      {
                        LCInsureAccSchema tLCInsureAccSchema = new LCInsureAccSchema();
                        tLCInsureAccSchema = mLCInsureAccSet.get(i);
                          LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB(conn);
                          tLCInsureAccDB.setSchema(tLCInsureAccSchema);
                          if(tLCInsureAccDB.delete() == false)
                          {
                            // @@错误处理
                            this.mErrors.copyAllErrors(tLCInsureAccDB.mErrors);
                            CError tError = new CError();
                            tError.moduleName = "GrpFeeBackBLS";
                            tError.functionName = "saveData";
                            tError.errorMessage = "帐户表删除失败!";
                            this.mErrors .addOneError(tError) ;
                            conn.rollback() ;
                            conn.close();
                            //return false;
                            continue;
                          }

                          if(tLCInsureAccDB.insert() == false)
                          {
                            // @@错误处理
                            this.mErrors.copyAllErrors(tLCInsureAccDB.mErrors);
                            CError tError = new CError();
                            tError.moduleName = "GrpFeeBackBLS";
                            tError.functionName = "saveData";
                            tError.errorMessage = "帐户表生成失败!";
                            this.mErrors .addOneError(tError) ;
                            conn.rollback() ;
                            conn.close();
                            //return false;
                            continue;
                          }

                      }
                    }

                    System.out.println("-----------帐户表更新结束--------------");

                    //帐户轨迹表
                    if(mLCInsureAccTraceSet.size()>0)
                    {
                      for(int i = 1; i <= mLCInsureAccTraceSet.size();i++)
                      {
                        LCInsureAccTraceSchema tLCInsureAccTraceSchema = new LCInsureAccTraceSchema();
                        tLCInsureAccTraceSchema = mLCInsureAccTraceSet.get(i);
                          LCInsureAccTraceDB tLCInsureAccTraceDB = new LCInsureAccTraceDB(conn);
                          tLCInsureAccTraceDB.setSchema(tLCInsureAccTraceSchema);
                          if(tLCInsureAccTraceDB.insert() == false)
                          {
                            // @@错误处理
                            this.mErrors.copyAllErrors(tLCInsureAccTraceDB.mErrors);
                            CError tError = new CError();
                            tError.moduleName = "GrpFeeBackBLS";
                            tError.functionName = "saveData";
                            tError.errorMessage = "核保轨迹表生成失败!";
                            this.mErrors .addOneError(tError) ;
                            conn.rollback() ;
                            conn.close();
                            //return false;
                            continue;
                          }

                      }
                    }
                    System.out.println("-----------帐户轨迹表更新结束--------------");

                    conn.commit() ;
                    conn.close();

                } // end of try
                catch (Exception ex)
                {
                  // @@错误处理
                  CError tError = new CError();
                  tError.moduleName = "GrpFeeBackBLS";
                  tError.functionName = "submitData";
                  tError.errorMessage = ex.toString();
                  this.mErrors .addOneError(tError);
                  try {
                    conn.rollback() ;
                    conn.close();
                  }
                  catch (Exception e) {
                  }
                  return false;
                }
                return true;
        }
}