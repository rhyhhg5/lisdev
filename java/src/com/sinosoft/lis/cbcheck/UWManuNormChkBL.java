package com.sinosoft.lis.cbcheck;

import java.lang.*;
import java.util.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.cbcheck.*;
import com.sinosoft.lis.f1print.*;
import com.sinosoft.lis.finfee.*;
/**
 * <p>Title: Web业务系统承保个人人工核保部分</p>
 * <p>Description: 逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author WHN
 * @version 1.0
 */
public class UWManuNormChkBL
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData ;
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate; //操作员
  private String mManageCom;
  private String mpassflag; //通过标记
  private String mBackFlag;
  private int merrcount; //错误条数
  private String mCalCode; //计算编码
  private String mUser;
  private FDate fDate = new FDate();
  private double mValue;
  private double mprem;
  private double mamnt;

  /** 业务处理相关变量 */
  private LCPolSet mLCPolSet = new LCPolSet();
  private LCPolSet mmLCPolSet = new LCPolSet();
  private LCPolSet m2LCPolSet = new LCPolSet();
  private LCPolSet mAllLCPolSet = new LCPolSet();
  private LCPolSchema mLCPolSchema = new LCPolSchema();
  private String mPolNo = "";
  private String mOldPolNo = "";
  private String mUWFlag = "";  //核保标志
  private Date mvalidate = null;
  private String mUWIdea = ""; //核保结论
  private String mUPUWCode = "";
//  private int mpostday; //延长天数
  private String mpostday; //延长天数
  private String mUWPopedom = ""; //操作员核保级别
  private String mAppGrade = ""; //上报级别
  private String mReason = ""; //加费原因
  private String mSpecReason = ""; //特约原因
  private String mQuesFlag = ""; //是否有问题件标记
  private String mQuesOrgFlag = ""; //机构问题件标记
  private String mQuesOpeFlag = ""; //操作员问题件标记
  private String mPolType = ""; //保单类型
  private String mAgentPrintFlag = ""; //是否有返回业务员要打印的问题件标记
  private String mAgentQuesFlag = ""; //是否有返回业务员问题件标记

  //modify by Minim
  private String mBackUWGrade = "";
  private String mBackAppGrade = "";
  private String mOperator = "";
  private Reflections mReflections = new Reflections();

  private String mPrtNo = "";
  private LJSPaySet outLJSPaySet = new LJSPaySet();
  private LJTempFeeSet outLJTempFeeSet = new LJTempFeeSet();
  private LBTempFeeSet outLBTempFeeSet = new LBTempFeeSet();
  private LJTempFeeClassSet outLJTempFeeClassSet = new LJTempFeeClassSet();
  private LBTempFeeClassSet outLBTempFeeClassSet = new LBTempFeeClassSet();
  //End modify by Minim

  /** 保费项表 */
  private LCPremSet mLCPremSet = new LCPremSet();
  private LCPremSet mAllLCPremSet = new LCPremSet();
  private LCPremSchema mLCPremSchema = new LCPremSchema();
  private LCPremSet mmLCPremSet = new LCPremSet();
  /** 领取项表 */
  private LCGetSet mLCGetSet = new LCGetSet();
  private LCGetSet mAllLCGetSet = new LCGetSet();
  private LCGetSchema mLCGetSchema = new LCGetSchema();
  /** 责任表 */
  private LCDutySet mLCDutySet = new LCDutySet();
  private LCDutySet mAllLCDutySet = new LCDutySet();
  private LCDutySchema mLCDutySchema = new LCDutySchema();
  /** 特别约定表 */
  private LCSpecSet mLCSpecSet = new LCSpecSet();
  private LCSpecSet mAllLCSpecSet = new LCSpecSet();
  private LCSpecSchema mLCSpecSchema = new LCSpecSchema();
  private LBSpecSet mLBSpecSet = new LBSpecSet();
  private LBSpecSet mAllLBSpecSet = new LBSpecSet();
  /** 特别约定注释表 */
  private LCSpecNoteSet mLCSpecNoteSet = new LCSpecNoteSet();
  private LCSpecNoteSet mAllLCSpecNoteSet = new LCSpecNoteSet();
  /** 核保主表 */
  private LCUWMasterSet mLCUWMasterSet = new LCUWMasterSet();
  private LCUWMasterSet mAllLCUWMasterSet = new LCUWMasterSet();
  /** 核保子表 */
  private LCUWSubSet mLCUWSubSet = new LCUWSubSet();
  private LCUWSubSet mAllLCUWSubSet = new LCUWSubSet();
  /** 核保错误信息表 */
  private LCUWErrorSet mLCUWErrorSet = new LCUWErrorSet();
  private LCUWErrorSet mAllLCErrSet = new LCUWErrorSet();
  /** 告知表 */
  private LCCustomerImpartSet mLCCustomerImpartSet = new LCCustomerImpartSet();
  private LCCustomerImpartSet mAllLCCustomerImpartSet = new LCCustomerImpartSet();
  /** 投保人表 */
  private LCAppntIndSet mLCAppntIndSet = new LCAppntIndSet();
  private LCAppntIndSet mAllLCAppntIndSet = new LCAppntIndSet();
  /** 受益人表 */
  private LCBnfSet mLCBnfSet = new LCBnfSet();
  private LCBnfSet mAllLCBnfSet = new LCBnfSet();
  /** 被保险人表 */
  private LCInsuredSet mLCInsuredSet = new LCInsuredSet();
  private LCInsuredSet mAllLCInsuredSet = new LCInsuredSet();
  /** 打印管理表 */
  private LOPRTManagerSet mLOPRTManagerSet = new LOPRTManagerSet();
  private LOPRTManagerSet mAllLOPRTManagerSet = new LOPRTManagerSet();
  /**轨迹锁表**/
  private LDSysTraceSchema mLDSysTraceSchema = new LDSysTraceSchema();
  private LDSysTraceSet mLDSysTraceSet = new LDSysTraceSet();
  private LDSysTraceSet mAllLDSysTraceSet = new LDSysTraceSet();
  /**计算公式表**/
  private LMUWSchema mLMUWSchema = new LMUWSchema();
  //private LMUWDBSet mLMUWDBSet = new LMUWDBSet();
  private LMUWSet mLMUWSet = new LMUWSet();

  private CalBase mCalBase = new CalBase();

  private GlobalInput mGlobalInput = new GlobalInput();

  private String mGetNoticeNo = "";

  public UWManuNormChkBL() {}

  public static void main(String[] args)
  {
    GlobalInput tG = new GlobalInput();
    tG.Operator = "aaa";
    LCPolSchema p = new LCPolSchema();
    p.setProposalNo( "86110020030110005578" );
    p.setApproveFlag( "1" );
//    p.setUWFlag("1");
    VData tVData = new VData();
    tVData.add( p );
    tVData.add( tG );
    ProposalApproveUI ui = new ProposalApproveUI();
    if( ui.submitData( tVData, "" ) == true )
      System.out.println("---ok---");
    else
      System.out.println("---NO---");
  }

  /**
   * 传输数据的公共方法
   * @param: cInputData 输入的数据
   *         cOperate 数据操作
   * @return:
   */
  public boolean submitData(VData cInputData,String cOperate)
  {
    int flag = 0; //判断是不是所有数据都不成功
    int j = 0; //符合条件数据个数

    //将操作数据拷贝到本类中
    mInputData = (VData)cInputData.clone();
	//mOperator=cOperate;

	//得到外部传入的数据,将数据备份到本类中
	System.out.println("---UWManuNormChkBL getInputData Begin ---");
    if (!getInputData(cInputData))
      return false;
    System.out.println("---UWManuNormChkBL getInputData End ---");

    // 校验该投保单是否已复核过,是则可进行核保,否则不能进行
    if (!checkApprove())
      return false;

    //校验核保级别
	System.out.println("---UWManuNormChkBL checkUWGrade Begin---");
    if (!checkUWGrade())
      return false;
    System.out.println("---UWManuNormChkBL checkUWGrade End---");

    //生成给付通知书号
    String tLimit = PubFun.getNoLimit(mManageCom);
    mGetNoticeNo = PubFun1.CreateMaxNo("GETNOTICENO",tLimit);//产生即付通知书号
	System.out.println("---tLimit---"+tLimit);

    //拒保、延期或撤单时，校验银行在途数据
    if (mUWFlag.equals("a") || mUWFlag.equals("1") || mUWFlag.equals("2")) {
      //查询应收总表数据
      String strSql = "select * from ljspay where trim(otherno) in "
                    + " (select trim(polno) from lcpol where prtno='" + mPrtNo + "' "
                    + " union "
                    + " select trim(proposalno) from lcpol where prtno='" + mPrtNo + "' "
                    + " union "
                    + " select trim(prtno) from lcpol where prtno='" + mPrtNo + "' )";

      outLJSPaySet = (new LJSPayDB()).executeQuery(strSql);

      for (int i=0; i<outLJSPaySet.size(); i++) {
        if (outLJSPaySet.get(i+1).getBankOnTheWayFlag().equals("1")) {
          System.out.println("有银行在途数据，不允许拒保、延期或撤单!");
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "UWManuNormChkBL";
          tError.functionName = "submitData";
          tError.errorMessage = "有银行在途数据，不允许拒保、延期或撤单!";
          this.mErrors .addOneError(tError) ;
          return false;
        }
      }

      //查询暂交费表数据
      strSql = "select * from ljtempfee where EnterAccDate is null and trim(otherno) in "
             + " (select trim(polno) from lcpol where prtno='" + mPrtNo + "' "
             + " union "
             + " select trim(proposalno) from lcpol where prtno='" + mPrtNo + "' "
             + " union "
             + " select trim(prtno) from lcpol where prtno='" + mPrtNo + "' )";

      outLJTempFeeSet = (new LJTempFeeDB()).executeQuery(strSql);

      if (outLJTempFeeSet.size() > 0) {
        for (int i=0; i<outLJTempFeeSet.size(); i++) {
          LBTempFeeSchema tLBTempFeeSchema = new LBTempFeeSchema();
          mReflections.transFields(tLBTempFeeSchema, outLJTempFeeSet.get(i+1));
          tLBTempFeeSchema.setBackUpSerialNo(PubFun1.CreateMaxNo("LBTempFee", 20));
          outLBTempFeeSet.add(tLBTempFeeSchema);

          LJTempFeeClassDB tLJTempFeeClassDB = new LJTempFeeClassDB();
          tLJTempFeeClassDB.setTempFeeNo(outLJTempFeeSet.get(i+1).getTempFeeNo());
          outLJTempFeeClassSet.add(tLJTempFeeClassDB.query());
        }

        for (int i=0; i<outLJTempFeeClassSet.size(); i++) {
          LBTempFeeClassSchema tLBTempFeeClassSchema = new LBTempFeeClassSchema();
          mReflections.transFields(tLBTempFeeClassSchema, outLJTempFeeClassSet.get(i+1));
          tLBTempFeeClassSchema.setBackUpSerialNo(PubFun1.CreateMaxNo("LBTFClass", 20));
          outLBTempFeeClassSet.add(tLBTempFeeClassSchema);
        }
      }
//      System.out.println("outLJTempFeeSet:" + outLJTempFeeSet.encode());
//      System.out.println("outLBTempFeeSet:" + outLBTempFeeSet.encode());
    }


    if(mUWFlag.equals("1")||mUWFlag.equals("2")||mUWFlag.equals("4")||mUWFlag.equals("9"))
    {
      //次标准体校验核保员级别
      if(!checkStandGrade())
        return false;

      if(!checkBackPol())
        return false;
    }

    //拒保或延期要校验核保员级别
    if(mUWFlag.equals("1")||mUWFlag.equals("2"))
    {
      if(!checkUserGrade())
        return false;
    }

    //校验主附险
	System.out.println("校验主附险"+mUWFlag);
    if(!mUWFlag.equals("1")&&!mUWFlag.equals("2")&&!mUWFlag.equals("3")&&!mUWFlag.equals("6")&&!mUWFlag.equals("8")&&!mUWFlag.equals("a")&&!mUWFlag.equals("b")&&!mUWFlag.equals("z"))
    {
      if (!checkMain())
        return false;
    }

    // 数据操作业务处理
	System.out.println("数据操作业务处理"+mUWFlag);
    if (!dealData())
      return false;

    System.out.println("---UWManuNormChkBL dealData---");



    //准备给后台的数据
    prepareOutputData();
    System.out.println("---UWManuNormChkBL prepareOutputData---");

    //数据提交
    UWManuNormChkBLS tUWManuNormChkBLS = new UWManuNormChkBLS();
   // System.out.println(" UWManuNormChkBLS Submit   Start");
    if (!tUWManuNormChkBLS.submitData(mInputData,mOperate))
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tUWManuNormChkBLS.mErrors);
      CError tError = new CError();
      tError.moduleName = "UWManuNormChkBL";
      tError.functionName = "submitData";
      //tError.errorMessage = "数据提交失败!";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    //System.out.println("---UWManuNormChkBL commitData---");


//撤单退费处理
    System.out.println("Start Return Tempfee");
    if(mUWFlag.equals("a") || mUWFlag.equals("1") || mUWFlag.equals("2"))
      if(!returnFee())
        return false;
    return true;
  }

  /**
   * 数据操作撤单业务处理
   * 输出：如果出错，则返回false,否则返回true
   */

  private boolean returnFee()
  {
    System.out.println("In ReturnFee");

    String payMode=""; //交费方式
    String BankCode="";//银行编码
    String BankAccNo="";//银行账号
    String AccName="";  //户名

    //准备TransferData数据
    String strSql="";
    LCPolDB tLCPolDB = new LCPolDB();

    tLCPolDB.setPolNo(mAllLCPolSet.get(1).getPolNo());
    if( tLCPolDB.getInfo() == false )
    {
      // @@错误处理
      this.mErrors.copyAllErrors( tLCPolDB.mErrors );

      return false;
    }

    //测试该投保单是否有暂交费待退
    strSql = "select * from ljtempfee where trim(otherno) in ( "
           + "select proposalno from lcpol where mainpolno ='" + tLCPolDB.getPolNo() + "'"
           + " union select '"+tLCPolDB.getPrtNo() +"' from dual )"
           + " and EnterAccDate is not null "
	       + " and confdate is  null";

	LJTempFeeDB sLJTempFeeDB=new LJTempFeeDB();
	LJTempFeeSet sLJTempFeeSet=new LJTempFeeSet();
	sLJTempFeeSet=sLJTempFeeDB.executeQuery(strSql);
	System.out.println("暂交费数量:  "+sLJTempFeeSet.size());
  if(sLJTempFeeSet.size()==0)
   {
	System.out.println("Out ReturnFee");
	return true;
   }

    //如果通知书号不为空，找出退费方式（优先级依次为支票，银行，现金）
    GetPayType tGetPayType=new GetPayType();
    if(tGetPayType.getPayTypeForLCPol(tLCPolDB.getPrtNo())==false)
    {
        // @@错误处理
        this.mErrors.copyAllErrors( tGetPayType.mErrors );
		System.out.println("查询出错信息  :"+tGetPayType.mErrors);
        return false;
    }
    else
    {
	  System.out.println("BankCode:  "+tGetPayType.getPayMode());
	  System.out.println("AccNo"+tGetPayType.getBankCode());
	  System.out.println("AccName"+tGetPayType.getBankAccNo());

        payMode=tGetPayType.getPayMode(); //交费方式
        BankCode=tGetPayType.getBankCode();//银行编码
        BankAccNo=tGetPayType.getBankAccNo();//银行账号
        AccName=tGetPayType.getAccName();  //户名
    }

    TransferData sTansferData=new TransferData();
    sTansferData.setNameAndValue("PayMode",payMode);
    if(payMode.equals("1"))
    {
        sTansferData.setNameAndValue("BankFlag","0" );
    }
    else
    {
        sTansferData.setNameAndValue("BankCode",BankCode);
        sTansferData.setNameAndValue("AccNo",BankAccNo);
        sTansferData.setNameAndValue("AccName",AccName );
        sTansferData.setNameAndValue("BankFlag","1" );
    }
//    String tLimit = PubFun.getNoLimit(sLJTempFeeClassSet.get(1).getManageCom());
    sTansferData.setNameAndValue("GetNoticeNo", mGetNoticeNo);

   // System.out.println("BankCode:  "+BankCode);
	//System.out.println("AccNo"+BankAccNo);
	//System.out.println("AccName"+AccName);

    // 准备暂交费表数据
   // strSql="select * from ljtempfee where otherno in ((select proposalno from lcpol where mainpolno ='"
      //    +tLCPolDB.getPolNo()+"'),'"+tLCPolDB.getPrtNo() +"')";

   // System.out.println("暂交费查询语句  :"+strSql);

   // LJTempFeeDB sLJTempFeeDB=new LJTempFeeDB();
   // LJTempFeeSet sLJTempFeeSet=new LJTempFeeSet();
   // sLJTempFeeSet=sLJTempFeeDB.executeQuery(strSql);

   // System.out.println("暂交费数量:  "+sLJTempFeeSet.size());

    LJTempFeeSet tLJTempFeeSet=new LJTempFeeSet();
    LJAGetTempFeeSet tLJAGetTempFeeSet=new LJAGetTempFeeSet();

    strSql="select uwidea from LCUWMaster where polno='"+tLCPolDB.getPolNo()+"'";
    LCUWMasterDB sLCUWMasterDB=new LCUWMasterDB();
    LCUWMasterSet sLCUWMasterSet=new LCUWMasterSet();
    sLCUWMasterSet=sLCUWMasterDB.executeQuery(strSql);


    System.out.println("核保意见:  "+mAllLCUWMasterSet.get(1).getUWIdea());

    for (int index=1; index<=sLJTempFeeSet.size(); index++) {
      System.out.println("HaveDate In Second1");

      LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
      tLJTempFeeSchema.setTempFeeNo(sLJTempFeeSet.get(index).getTempFeeNo() );
      tLJTempFeeSchema.setTempFeeType(sLJTempFeeSet.get(index).getTempFeeType() );
      tLJTempFeeSchema.setRiskCode(sLJTempFeeSet.get(index).getRiskCode() );
      tLJTempFeeSet.add(tLJTempFeeSchema);

      LJAGetTempFeeSchema tLJAGetTempFeeSchema = new LJAGetTempFeeSchema();
      //tLJAGetTempFeeSchema.setGetReasonCode(mAllLCUWMasterSet.get(1).getUWIdea());
      tLJAGetTempFeeSchema.setGetReasonCode("99");

      tLJAGetTempFeeSet.add(tLJAGetTempFeeSchema);

      System.out.println("HaveDate In Second2");
      try {
        System.out.println("TempFeeNo:  "+sLJTempFeeSet.get(index).getTempFeeNo());
        System.out.println("TempFeeType:  "+sLJTempFeeSet.get(index).getTempFeeType());
        System.out.println("RiskCode:  "+sLJTempFeeSet.get(index).getRiskCode());
      }
      catch (Exception e) {
        System.out.println("无银行数据!");
      }

    }


    // 准备传输数据 VData
    VData tVData = new VData();
    tVData.add(tLJTempFeeSet);
    tVData.add(tLJAGetTempFeeSet);
    tVData.add(sTansferData);
    tVData.add(mGlobalInput);

    // 数据传输
    System.out.println("--------开始传输数据---------");
    TempFeeWithdrawBL tTempFeeWithdrawBL = new TempFeeWithdrawBL();
    if(tTempFeeWithdrawBL.submitData(tVData, "INSERT")==true)
      System.out.println("---ok---");
    else
      System.out.println("---NO---");

    if (tTempFeeWithdrawBL.mErrors.needDealError()) {
      // @@错误处理
      this.mErrors.copyAllErrors(tTempFeeWithdrawBL.mErrors);
      CError tError = new CError();
      tError.moduleName = "UWManuNormChkBL";
      tError.functionName = "submitData";
      tError.errorMessage = "核保成功,但退费失败!";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    System.out.println("Out ReturnFee");
    return true ;
  }


  /**
   * 数据操作类业务处理
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */

  private boolean dealData()
  {
    System.out.println("IN dealData()"+mUWFlag);
    if(!mUWFlag.equals("1")&&!mUWFlag.equals("2")&&!mUWFlag.equals("8")&&!mUWFlag.equals("a")&&!mUWFlag.equals("b")&&!mUWFlag.equals("z"))
    {
	  System.out.println("begin dealOnePol()");
      if (dealOnePol() == false)
        return false;
    }
    else
    {
	  System.out.println("begin dealAllPol()");
      if (dealAllPol() == false)
        return false;
    }

    return true;
  }

  /**
   * 操作一张保单的业务处理
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean dealOnePol()
  {
    //取上报级别
    if (mUWFlag.equals("6"))
    {
	  System.out.println("IN dealOnePol()");
      if(!uplevel())
	  {
	    return false;
	  }

      LDSysTraceSet tLDSysTraceSet = new LDSysTraceSet();
      tLDSysTraceSet.set( mLDSysTraceSet );
      mAllLDSysTraceSet.add( tLDSysTraceSet );

    }

    // 保单
    if (preparePol()== false)
      return false;

    // 核保信息
    if (prepareUW() == false)
      return false;

    if(mUWFlag.equals("b"))
    {
      if(print() == false)
        return false;
    }    //多余代码

    if (mUWFlag.equals("3"))
    {
      CondAccept();

	  LBSpecSet tLBSpecSet = new LBSpecSet();
	  tLBSpecSet.set(mLBSpecSet);
	  mAllLBSpecSet.add(mLBSpecSet) ;

      LCPremSet tLCPremSet = new LCPremSet();
      tLCPremSet.set( mmLCPremSet );
      mAllLCPremSet.add( tLCPremSet );

      LCSpecSet tLCSpecSet = new LCSpecSet();
      tLCSpecSet.set( mLCSpecSet );
      mAllLCSpecSet.add( tLCSpecSet );

      LCDutySet tLCDutySet = new LCDutySet();
      tLCDutySet.set(mLCDutySet);
      mAllLCDutySet.add(tLCDutySet);
    }

    LCPolSet tLCPolSet = new LCPolSet();
    tLCPolSet.set( mLCPolSet );
    mAllLCPolSet.add( tLCPolSet );

    LCUWMasterSet tLCUWMasterSet = new LCUWMasterSet();
    tLCUWMasterSet.set( mLCUWMasterSet );
    mAllLCUWMasterSet.add( tLCUWMasterSet );

    LCUWSubSet tLCUWSubSet = new LCUWSubSet();
    tLCUWSubSet.set( mLCUWSubSet );
    mAllLCUWSubSet.add( tLCUWSubSet );

    return true;
  }

  /**
   * 操作一张保单及相关附加险的业务处理
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean dealAllPol()
  {

    //　校验增加附加险申请
    if (checkAddPol() == false)
      return false;

    // 保单
    if (prepareAllPol() == false)
      return false;

    // 核保信息
    if (prepareAllUW() == false)
      return false;

    //不是团体下个人单
    if(!mPolType.equals("2"))
    {
      if(mUWFlag.equals("1")||mUWFlag.equals("2")||mUWFlag.equals("8")||mUWFlag.equals("a"))
      {
        if(print() == false)
          return false;

        if(mUWFlag.equals("8")&&mAgentPrintFlag.equals("1"))
        {
          if(printAgent() == false)
            return false;
        }
      }
    }

    if (mUWFlag.equals("2"))
    {
      TimeAccept();
    }

    LCPolSet tLCPolSet = new LCPolSet();
    tLCPolSet.set( mLCPolSet );
    mAllLCPolSet.add( tLCPolSet );

    LCUWMasterSet tLCUWMasterSet = new LCUWMasterSet();
    tLCUWMasterSet.set( mLCUWMasterSet );
    mAllLCUWMasterSet.add( tLCUWMasterSet );

    LCUWSubSet tLCUWSubSet = new LCUWSubSet();
    tLCUWSubSet.set( mLCUWSubSet );
    mAllLCUWSubSet.add( tLCUWSubSet );

    return true;
  }


  /**
   * 从输入数据中得到所有对象
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {
    String getflag = "true";  //判断承保条件是否接收

    GlobalInput tGlobalInput = new GlobalInput();
    mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
    mOperate = mGlobalInput.Operator;
    mManageCom = mGlobalInput.ManageCom;

    //取投保单
    mLCPolSet.set((LCPolSet)cInputData.getObjectByObjectName("LCPolSet",0));
    int n = mLCPolSet.size();
    if(n == 1)
    {
      LCPolSchema tLCPolSchema = mLCPolSet.get(1);
      LCPolDB tLCPolDB = new LCPolDB();

      mUWFlag = tLCPolSchema.getUWFlag();
      mOldPolNo = tLCPolSchema.getProposalNo();
      mPolNo = tLCPolSchema.getProposalNo();
      mUWIdea = tLCPolSchema.getRemark();
	  if(mUWFlag.equals("6") )
	  {
	     mUPUWCode = tLCPolSchema.getUWCode();
		 tLCPolSchema.setUWCode("") ;
	  }
      System.out.println("muwflagm+UPUWCode="+mUWFlag+mUPUWCode);

      //校验是不是以下核保结论
      if(mUWFlag == null)
      {
        // @@错误处理
        CError tError = new CError();
        tError.moduleName = "UWManuNormChkBL";
        tError.functionName = "getInputData";
        tError.errorMessage = "没有选择核保结论";
        this.mErrors .addOneError(tError) ;
        return false;
      }

      if(mUWFlag.equals(""))
      {
        // @@错误处理
        CError tError = new CError();
        tError.moduleName = "UWManuNormChkBL";
        tError.functionName = "getInputData";
        tError.errorMessage = "没有选择核保结论";
        this.mErrors .addOneError(tError) ;
        return false;
      }

      tLCPolDB.setPolNo( mPolNo);
      System.out.println("--保单表中的保单号:  "+mPolNo);
      if (tLCPolDB.getInfo() == false)
      {
        // @@错误处理
        this.mErrors.copyAllErrors( tLCPolDB.mErrors );
        CError tError = new CError();
        tError.moduleName = "UWManuNormChkBL";
        tError.functionName = "getInputData";
        tError.errorMessage = mPolNo+"投保单查询失败!";
        this.mErrors .addOneError(tError) ;
        return false;
      }
      else
      {
        mLCPolSchema.setSchema( tLCPolDB );
      }

      //判断是团单下个单还是个人单
      if(!mLCPolSchema.getGrpPolNo().equals("00000000000000000000"))
      {
        mPolType = "2";
      }

      if (mUWFlag.equals("3"))
      {
        mLCPremSet.set((LCPremSet)cInputData.getObjectByObjectName("LCPremSet",0));
        n = mLCPremSet.size();
        if (n > 0)
        {
        }

        mLCSpecSet.set((LCSpecSet)cInputData.getObjectByObjectName("LCSpecSet",0));
        n = mLCSpecSet.size();
         if (n == 1)
         {
         }
      }

      mLCUWMasterSet.set((LCUWMasterSet)cInputData.getObjectByObjectName("LCUWMasterSet",0));
      n = mLCUWMasterSet.size();
      if (n == 1)
      {
        LCUWMasterSchema tLCUWMasterSchema = mLCUWMasterSet.get(1);

        mReason = tLCUWMasterSchema.getAddPremReason();
        mSpecReason = tLCUWMasterSchema.getSpecReason();
        //取延长时间
        if (mUWFlag.equals("2"))
        {
          mpostday = tLCUWMasterSchema.getPostponeDay();
          System.out.println("mpostday="+mpostday);
        }
        LCUWMasterDB tLCUWMasterDB = new LCUWMasterDB();
        tLCUWMasterDB.setPolNo( mPolNo);
        tLCUWMasterDB.setProposalNo(mPolNo);
        System.out.println("--核保主表中的保单号: "+mPolNo);
        if (tLCUWMasterDB.getInfo() == false)
        {
          // @@错误处理
          this.mErrors.copyAllErrors( tLCUWMasterDB.mErrors );
          CError tError = new CError();
          tError.moduleName = "UWManuNormChkBL";
          tError.functionName = "getInputData";
          tError.errorMessage = mPolNo+"投保单查询失败!";
          this.mErrors .addOneError(tError);
          return false;
        }

      }
      else
      {
        return false;
      }

      //取附加险
      tLCPolDB = new LCPolDB();
      tLCPolDB.setMainPolNo(mPolNo);
      m2LCPolSet = tLCPolDB.query();

      //add by Minim
      if (m2LCPolSet.size() > 0) mPrtNo = m2LCPolSet.get(1).getPrtNo();

    }
    else
    {
      return false;
    }

    return true;
  }

  /**
   * 校验投保单是否复核
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean checkApprove()
  {
    if (mLCPolSchema.getApproveCode() == null || mLCPolSchema.getApproveDate() == null)
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "UWManuNormChkBL";
      tError.functionName = "checkApprove";
      tError.errorMessage = "投保单尚未进行复核操作，不能核保!（投保单号：" + mPolNo.trim() + "）";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
  }

  /**
   * 校验核保员级别
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean checkUWGrade()
  {
    LDUserDB tLDUserDB = new LDUserDB();
    tLDUserDB.setUserCode(mOperate);
    System.out.println("mOperate"+mOperate) ;
    if(!tLDUserDB.getInfo())
    {
      CError tError = new CError();
      tError.moduleName = "UWManuNormChkBL";
      tError.functionName = "checkUWGrade";
      tError.errorMessage = "无此操作员信息，不能核保!（操作员：" + mOperate + "）";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    String tUWPopedom = tLDUserDB.getUWPopedom();
    mUWPopedom = tUWPopedom;
    mAppGrade = mUWPopedom;
    System.out.println("tUWPopedom"+tUWPopedom) ;
    if (tUWPopedom==null || tUWPopedom.equals(""))
    {
      CError tError = new CError();
      tError.moduleName = "UWManuNormChkBL";
      tError.functionName = "checkUWGrade";
      tError.errorMessage = "操作员无核保权限，不能核保!（操作员：" + mOperate + "）";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    LCUWMasterDB tLCUWMasterDB = new LCUWMasterDB();
    tLCUWMasterDB.setPolNo(mLCPolSchema.getPolNo());
    tLCUWMasterDB.setProposalNo(mLCPolSchema.getPolNo());
    System.out.println("tUWPopedom"+tUWPopedom) ;
    if(!tLCUWMasterDB.getInfo())
    {
      CError tError = new CError();
      tError.moduleName = "UWManuNormChkBL";
      tError.functionName = "checkUWGrade";
      tError.errorMessage = "没有核保信息，不能核保!（操作员：" + mOperate + "）";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    else
    {
      String tappgrade = tLCUWMasterDB.getAppGrade();
	  if( tappgrade==null||tappgrade.equals(""))
	  {
			CError tError = new CError();
			tError.moduleName = "UWManuNormChkBL";
			tError.functionName = "checkUWGrade";
			tError.errorMessage = "该投保单无核保级别，不能核保!";
			this.mErrors .addOneError(tError) ;
			return false;
      }
	  System.out.println("tappgrade+tUWPopedom"+tappgrade+tUWPopedom) ;
      if( tUWPopedom.compareTo(tappgrade) < 0)
      {
        CError tError = new CError();
        tError.moduleName = "UWManuNormChkBL";
        tError.functionName = "checkUWGrade";
        tError.errorMessage = "已经提交上级核保，不能核保!（操作员：" + mOperate + "）";
        this.mErrors .addOneError(tError) ;
        return false;
      }
    }

    LCUWErrorDB tLCUWErrorDB = new LCUWErrorDB();
    tLCUWErrorDB.setPolNo(mLCPolSchema.getPolNo());
    String tpolno = mLCPolSchema.getPolNo();
    String tsql = "select * from lcuwerror where polno = '"+tpolno.trim()+"' and uwno = (select max(uwno) from lcuwerror where polno = '"+tpolno.trim()+"')";
    LCUWErrorSet tLCUWErrorSet = tLCUWErrorDB.executeQuery(tsql);

    int errno = tLCUWErrorSet.size();
    if (errno > 0)
    {
      for( int i = 1; i <= errno; i++)
      {
        LCUWErrorSchema tLCUWErrorSchema = new LCUWErrorSchema();
        tLCUWErrorSchema = tLCUWErrorSet.get(i);
        String terrgrade = tLCUWErrorSchema.getUWGrade();
        System.out.println("terrgrade"+terrgrade) ;
        if(terrgrade!=null&&tUWPopedom.compareTo(terrgrade) < 0 && !mUWFlag.equals("6")&& !mUWFlag.equals("8")&& !mUWFlag.equals("3"))
        {
          CError tError = new CError();
          tError.moduleName = "UWManuNormChkBL";
          tError.functionName = "checkUWGrade";
          tError.errorMessage = "核保级别不够，请录入核保意见，申请待上级核保!（操作员：" + mOperate + "）";
          this.mErrors .addOneError(tError) ;
          return false;
        }
		System.out.println("tUWPopedom"+tUWPopedom) ;
      }
    }
    return true;
  }

  /**
   * 校验是否有撤销投保申请
   * @return
   */
  private boolean checkBackPol()
  {
//    LCApplyRecallPolDB tLCApplyRecallPolDB = new LCApplyRecallPolDB();
//    LCApplyRecallPolSet tLCApplyRecallPolSet = new LCApplyRecallPolSet();
//
//    tLCApplyRecallPolDB.setProposalNo(mPolNo);
//    tLCApplyRecallPolDB.setInputState("0");
//
//    tLCApplyRecallPolSet = tLCApplyRecallPolDB.query();
//
//    if(tLCApplyRecallPolSet.size() > 0)
//    {
//      CError tError = new CError();
//      tError.moduleName = "UWManuNormChkBL";
//      tError.functionName = "checkBackPol";
//      tError.errorMessage = "有撤单申请不能下此结论！";
//      this.mErrors .addOneError(tError);
//      return false;
//    }

    return true;
  }

  /**
   * 校验申请附加险
   * @return
   */
  private boolean checkAddPol()
  {
    LCAddPolDB tLCAddPolDB = new LCAddPolDB();
    LCAddPolSet tLCAddPolSet = new LCAddPolSet();

    tLCAddPolDB.setMainPolNo(mPolNo);
    tLCAddPolDB.setInputState("0");

    tLCAddPolSet = tLCAddPolDB.query();

    if(tLCAddPolSet.size() > 0)
    {
      CError tError = new CError();
      tError.moduleName = "UWManuNormChkBL";
      tError.functionName = "checkAddPol";
      tError.errorMessage = "有附加险申请，不能下此结论！";
      this.mErrors .addOneError(tError);
      return false;
    }

    return true;
  }
  /**
   * 校验该主险的附加险是否已核保通过
   */
  private boolean checkMain()
  {
    if(mLCPolSchema.getMainPolNo().equals(mLCPolSchema.getPolNo()))
    {
      String tsql = "select * from lcpol where mainpolno = '"+mLCPolSchema.getPolNo()+"' and mainpolno <> polno";
      System.out.println("附险查询:"+tsql);
      LCPolDB tLCPolDB = new LCPolDB();
      LCPolSet tLCPolSet = new LCPolSet();

      tLCPolSet = tLCPolDB.executeQuery(tsql);

      if(tLCPolSet.size()>0)
      {
        for(int i = 1;i<=tLCPolSet.size();i++)
        {
          LCPolSchema tLCPolSchema = new LCPolSchema();

          tLCPolSchema = tLCPolSet.get(i);
          if(!tLCPolSchema.getUWFlag().equals("4")&&!tLCPolSchema.getUWFlag().equals("9"))
          {
            CError tError = new CError();
            tError.moduleName = "UWManuNormChkBL";
            tError.functionName = "checkMain";
            tError.errorMessage = "该主险有附加险尚未核保通过！";
            this.mErrors .addOneError(tError);
            return false;
          }
        }
      }
    }
    return true;
  }

  /**
   * 个人单核保数据准备
   * 输出：如果发生错误则返回false,否则返回true
   */
  private void CheckPolInit(LCPolSchema tLCPolSchema)
  {
    mCalBase = new CalBase();
    mCalBase.setPrem( tLCPolSchema.getPrem() );
    mCalBase.setGet( tLCPolSchema.getAmnt() );
    mCalBase.setMult( tLCPolSchema.getMult() );
    //mCalBase.setYears( tLCPolSchema.getYears() );
    mCalBase.setAppAge( tLCPolSchema.getInsuredAppAge() );
    mCalBase.setSex( tLCPolSchema.getInsuredSex() );
    mCalBase.setJob( tLCPolSchema.getOccupationType() );
    mCalBase.setCount( tLCPolSchema.getInsuredPeoples() );
    mCalBase.setPolNo( tLCPolSchema.getPolNo() );
  }

  /**
   * 个人单核保
   * 输出：如果发生错误则返回false,否则返回true
   */
  private String CheckPol()
  {
    //准备数据
    CheckPolInit(mLCPolSchema);

    String tUWGrade = "";

    // 计算
    Calculator mCalculator = new Calculator();
    mCalculator.setCalCode( mCalCode );
    //增加基本要素
    mCalculator.addBasicFactor("Get", mCalBase.getGet() );
    mCalculator.addBasicFactor("Mult", mCalBase.getMult() );
    mCalculator.addBasicFactor("Prem", mCalBase.getPrem() );
    //mCalculator.addBasicFactor("PayIntv", mCalBase.getPayIntv() );
    //mCalculator.addBasicFactor("GetIntv", mCalBase.getGetIntv() );
    mCalculator.addBasicFactor("AppAge", mCalBase.getAppAge() );
    mCalculator.addBasicFactor("Sex", mCalBase.getSex() );
    mCalculator.addBasicFactor("Job", mCalBase.getJob() );
    mCalculator.addBasicFactor("PayEndYear", mCalBase.getPayEndYear() );
    mCalculator.addBasicFactor("GetStartDate", "");
    //mCalculator.addBasicFactor("GetYear", mCalBase.getGetYear() );
    mCalculator.addBasicFactor("Years", mCalBase.getYears() );
    mCalculator.addBasicFactor("Grp","");
    mCalculator.addBasicFactor("GetFlag","");
    mCalculator.addBasicFactor("ValiDate","");
    mCalculator.addBasicFactor("Count", mCalBase.getCount() );
    mCalculator.addBasicFactor("FirstPayDate","");
    //mCalculator.addBasicFactor("AddRate", mCalBase.getAddRate() );
    //mCalculator.addBasicFactor("GDuty", mCalBase.getGDuty() );
    mCalculator.addBasicFactor("PolNo", mCalBase.getPolNo() );
    mCalculator.addBasicFactor("InsuredNo",mLCPolSchema.getInsuredNo());;

    String tStr = "";
    tStr = mCalculator.calculate() ;
    if (tStr.trim().equals(""))
      tUWGrade = "";
    else
      tUWGrade = tStr.trim();

    System.out.println("AmntGrade:"+tUWGrade);

    return tUWGrade;
  }

  /**
   * 核保险种信息校验,准备核保算法
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean CheckKinds(String tFlag)
  {
    mLMUWSet.clear();
    LMUWSchema tLMUWSchema = new LMUWSchema();
    //查询算法编码
    if(tFlag.equals("1"))
      tLMUWSchema.setUWType("13");//非标准体

    if(tFlag.equals("2"))
      tLMUWSchema.setUWType("14");//拒保延期

    tLMUWSchema.setRiskCode("000000");
    tLMUWSchema.setRelaPolType("I");


    LMUWDB tLMUWDB = new LMUWDB();
    tLMUWDB.setSchema(tLMUWSchema);

    mLMUWSet = tLMUWDB.query();
    if (tLMUWDB.mErrors.needDealError() == true)
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tLMUWDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "UWManuNormChkBL";
      tError.functionName = "CheckKinds";
      tError.errorMessage = "核保级别校验算法读取失败!";
      this.mErrors.addOneError(tError);
      //mLMUWDBSet.clear();
      return false;
    }
    return true;
  }

  /**
   * 次标准体校验核保员级别
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean checkStandGrade()
  {
    CheckKinds("1");

    LCUWMasterSchema tLCUWMasterSchema = new LCUWMasterSchema();
    LCUWMasterDB tLCUWMasterDB = new LCUWMasterDB();
    LCUWMasterSet tLCUWMasterSet = new LCUWMasterSet();

    tLCUWMasterDB.setProposalNo( mOldPolNo );

    if(tLCUWMasterDB.getInfo() == false)
    {
      // @@错误处理
      this.mErrors.copyAllErrors( tLCUWMasterDB.mErrors );
      CError tError = new CError();
      tError.moduleName = "UWManuNormChkBL";
      tError.functionName = "checkStandGrade";
      tError.errorMessage = "LCUWMaster表取数失败!";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    else
    {
      tLCUWMasterSchema = tLCUWMasterDB.getSchema();
    }

    //有特约，加费，保险计划变更为次标准体
    if(!tLCUWMasterSchema.getSpecFlag().equals("0")||!tLCUWMasterSchema.getChangePolFlag().equals("0"))
    {
      if(mLMUWSet.size() > 0)
      {
        for(int i = 1;i <= mLMUWSet.size();i++)
        {
          LMUWSchema tLMUWSchema = new LMUWSchema();
          tLMUWSchema = mLMUWSet.get(i);

          mCalCode = tLMUWSchema.getCalCode(); //次标准体计算公式代码
          String tempuwgrade = CheckPol();
          if(tempuwgrade != null)
          {
            if(mUWPopedom.compareTo(tempuwgrade) < 0)
            {
              CError tError = new CError();
              tError.moduleName = "UWManuNormChkBL";
              tError.functionName = "prepareAllPol";
              tError.errorMessage = "无此次标准体投保件核保权限，需要上报上级核保师!";
              this.mErrors .addOneError(tError) ;
              return false;
            }
          }

        }
      }
    }
    return true;
  }

  /**
   * 拒保，撤单校验核保员级别
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean checkUserGrade()
  {
    CheckKinds("2");

    if(mLMUWSet.size() > 0)
    {
      for(int i = 1;i <= mLMUWSet.size();i++)
      {
        LMUWSchema tLMUWSchema = new LMUWSchema();
        tLMUWSchema = mLMUWSet.get(i);

        mCalCode = tLMUWSchema.getCalCode(); //延期拒保计算公式代码
        String tempuwgrade = CheckPol();
        if(tempuwgrade != null)
        {
          if(mUWPopedom.compareTo(tempuwgrade) < 0)
          {
            CError tError = new CError();
            tError.moduleName = "UWManuNormChkBL";
            tError.functionName = "prepareAllPol";
            tError.errorMessage = "无此单拒保，延期权限，需上报上级核保师!";
            this.mErrors .addOneError(tError) ;
            return false;
          }
        }
      }
    }

    return true;
  }

  /**
   * 校验是否有返回业务员问题件
   * @return
   */
  private boolean checkAgentQuest(LCPolSchema tLCPolSchema)
  {
    String tsql = "";
    LCIssuePolDB tLCIssuePolDB = new LCIssuePolDB();
    LCIssuePolSet tLCIssuePolSet = new LCIssuePolSet();

    tsql = "select * from lcissuepol where proposalno = '"+tLCPolSchema.getProposalNo()+"' and backobjtype = '2' and replyresult is null and needprint = 'Y'";

    tLCIssuePolSet = tLCIssuePolDB.executeQuery(tsql);
    if(tLCIssuePolSet.size()>0)
    {
      mAgentPrintFlag = "1";
      mAgentQuesFlag = "1";
    }
    return true;
  }

  /**
   * 校验是否有返回保户问题件
   * @return
   */
  private boolean checkQuest(LCPolSchema tLCPolSchema)
  {
    String tsql = "";
    LCIssuePolDB tLCIssuePolDB = new LCIssuePolDB();
    LCIssuePolSet tLCIssuePolSet = new LCIssuePolSet();

    tsql = "select * from lcissuepol where proposalno = '"+tLCPolSchema.getProposalNo()+"' and backobjtype = '3' and replyresult is null and needprint = 'Y'";

    tLCIssuePolSet = tLCIssuePolDB.executeQuery(tsql);
    if(tLCIssuePolSet.size()>0)
    {
      mQuesFlag = "1";
    }
    return true;
  }

  /**
   * 校验是否有返回机构问题件
   * @return
   */
  private boolean checkQuestOrg(LCPolSchema tLCPolSchema)
  {
    String tsql = "";
    LCIssuePolDB tLCIssuePolDB = new LCIssuePolDB();
    LCIssuePolSet tLCIssuePolSet = new LCIssuePolSet();

    tsql = "select * from lcissuepol where proposalno = '"+tLCPolSchema.getProposalNo()+"' and backobjtype = '4' and replyresult is null";

    tLCIssuePolSet = tLCIssuePolDB.executeQuery(tsql);
    if(tLCIssuePolSet.size()>0)
    {
      mQuesOrgFlag = "1";
    }
    return true;
  }

  /**
   * 校验是否有返操作员问题件
   * @return
   */
  private boolean checkQuestOpe(LCPolSchema tLCPolSchema)
  {
    String tsql = "";
    LCIssuePolDB tLCIssuePolDB = new LCIssuePolDB();
    LCIssuePolSet tLCIssuePolSet = new LCIssuePolSet();

    tsql = "select * from lcissuepol where proposalno = '"+tLCPolSchema.getProposalNo()+"' and backobjtype = '1' and replyresult is null";

    tLCIssuePolSet = tLCIssuePolDB.executeQuery(tsql);
    if(tLCIssuePolSet.size()>0)
    {
      mQuesOpeFlag = "1";
    }
    return true;
  }


  /**
   * 准备保单信息
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean preparePol()
  {
    mLCPolSchema.setUWFlag(mUWFlag);
	mLCPolSchema.setUWCode(mOperate);

    mLCPolSchema.setUWDate(PubFun.getCurrentDate());
    if (mUWFlag.equals("7")||mUWFlag.equals("8"))//mUWFlag.equals("8")是冗余的
    {
      //mLCPolSchema.setApproveFlag("1");
    }
    else
    {
      mLCPolSchema.setApproveFlag("9");
    }

    mLCPolSet.clear();
    mLCPolSet.add(mLCPolSchema);

    return true;
  }

  /**
   * 准备主附险保单信息
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean prepareAllPol()
  {
    LCPolDB tLCPolDB = new LCPolDB();
    LCPolSet tLCPolSet = new LCPolSet();
    mLCPolSet.clear();

    tLCPolDB.setMainPolNo(mPolNo);
    tLCPolSet = tLCPolDB.query();

    for(int i = 1;i<= tLCPolSet.size();i++)
    {
      LCPolSchema tLCPolSchema = new LCPolSchema();

      tLCPolSchema = tLCPolSet.get(i);

      if(tLCPolSchema.getApproveFlag().equals("0"))
      {
        CError tError = new CError();
        tError.moduleName = "UWManuNormChkBL";
        tError.functionName = "prepareAllPol";
        tError.errorMessage = "有附加险没有复核!";
        this.mErrors .addOneError(tError) ;
        return false;
      }

      if(tLCPolSchema.getApproveFlag().equals("1"))
      {
        CError tError = new CError();
        tError.moduleName = "UWManuNormChkBL";
        tError.functionName = "prepareAllPol";
        tError.errorMessage = "有附加险正进行复核修改或问题修改!";
        this.mErrors .addOneError(tError) ;
        return false;
      }


      if(tLCPolSchema.getUWFlag().equals("0"))
      {
        CError tError = new CError();
        tError.moduleName = "UWManuNormChkBL";
        tError.functionName = "prepareAllPol";
        tError.errorMessage = "有附加险没有自动核保!";
        this.mErrors .addOneError(tError) ;
        return false;
      }

      if(mUWFlag.equals("z"))
      {
        tLCPolSchema.setUWFlag("5");
      }
      else if (mUWFlag.equals("8"))
	  {
        tLCPolSchema.setUWFlag("8");
      }
      else
      {
        if (tLCPolSchema.getUWFlag().equals("3")) {
          if (tLCPolSchema.getMainPolNo().equals(tLCPolSchema.getPolNo())) tLCPolSchema.setUWFlag(mUWFlag);
        }
        else {
          tLCPolSchema.setUWFlag(mUWFlag);
        }

      }

      tLCPolSchema.setUWCode(mOperate);
      tLCPolSchema.setUWDate(PubFun.getCurrentDate());
      if (mUWFlag.equals("7")||mUWFlag.equals("8"))//mUWFlag.equals("7")是冗余的
      {
      }
      else
      {
        tLCPolSchema.setApproveFlag("9");
      }

      mLCPolSet.add(tLCPolSchema);
    }


    return true;
  }

  /**
   * 准备核保信息
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean prepareUW()
  {
    LCUWMasterSchema tLCUWMasterSchema = new LCUWMasterSchema();
    LCUWMasterDB tLCUWMasterDB = new LCUWMasterDB();
    tLCUWMasterDB.setPolNo( mOldPolNo );
    LCUWMasterSet tLCUWMasterSet = new LCUWMasterSet();
    tLCUWMasterSet = tLCUWMasterDB.query();
    if (tLCUWMasterDB.mErrors.needDealError())
    {
      // @@错误处理
      this.mErrors.copyAllErrors( tLCUWMasterDB.mErrors );
      CError tError = new CError();
      tError.moduleName = "UWManuNormChkBL";
      tError.functionName = "prepareUW";
      tError.errorMessage = "LCUWMaster表取数失败!";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    int n = tLCUWMasterSet.size();
    System.out.println("该投保单的核保主表记录条数:   "+n);
    if (n == 1)
    {
      tLCUWMasterSchema = tLCUWMasterSet.get(1);

      //tLCUWMasterSchema.setUWNo(tLCUWMasterSchema.getUWNo()+1);核保主表中的UWNo表示该投保单经过几次人工核保(等价于经过几次自动核保次数),而不是人工核保结论(包括核保通知书,上报等)下过几次.所以将其注释.sxy-2003-09-19
      tLCUWMasterSchema.setPassFlag(mUWFlag); //通过标志
      tLCUWMasterSchema.setState(mUWFlag);
      tLCUWMasterSchema.setUWIdea(mUWIdea);
      tLCUWMasterSchema.setAutoUWFlag("2"); // 1 自动核保 2 人工核保
      tLCUWMasterSchema.setUWGrade(mUWPopedom);
      tLCUWMasterSchema.setAppGrade(mAppGrade);
	  if(mUWFlag.equals("6"))
	  {
		tLCUWMasterSchema.setOperator(mUPUWCode); //上报核保指定核保师功能的实现借助将当前核保师改为待核保师方式实现
	  }
	  else
	  {
        tLCUWMasterSchema.setOperator(mOperate);
	  } //操作员
      tLCUWMasterSchema.setModifyDate(PubFun.getCurrentDate());
      tLCUWMasterSchema.setModifyTime(PubFun.getCurrentTime());

      //延期
      if (mUWFlag.equals("2"))//此代码冗余
      {
        tLCUWMasterSchema.setPostponeDay(mpostday);
        tLCUWMasterSchema.setPostponeDate(mvalidate);
      }

      //条件承保
      if ( mUWFlag.equals("3"))
      {
        tLCUWMasterSchema.setSpecReason(mSpecReason);//特约原因
        tLCUWMasterSchema.setAddPremReason(mReason);
      }

      if (mUWFlag.equals("3"))
      {
        if (tLCUWMasterSchema.getPrintFlag().equals("1"))
        {
          CError tError = new CError();
          tError.moduleName = "UWManuNormChkBL";
          tError.functionName = "prepareUW";
          tError.errorMessage = "已经发核保通知不可录入!";
          this.mErrors .addOneError(tError) ;
          return false;
        }
        tLCUWMasterSchema.setSpecFlag("1");

        //承保计划变更结论录入
        if (mUWFlag.equals("b"))//此代码冗余
        {
          if (tLCUWMasterSchema.getPrintFlag().equals("1"))
          {
            CError tError = new CError();
            tError.moduleName = "UWManuNormChkBL";
            tError.functionName = "prepareAllUW";
            tError.errorMessage = "已经发核保通知不可录入!";
            this.mErrors .addOneError(tError) ;
            return false;
          }

          tLCUWMasterSchema.setUWIdea("");
          tLCUWMasterSchema.setChangePolFlag("1");
          tLCUWMasterSchema.setChangePolReason(mUWIdea);
        }
      }
    }
    else
    {
      // @@错误处理
      this.mErrors.copyAllErrors( tLCUWMasterDB.mErrors );
      CError tError = new CError();
      tError.moduleName = "UWManuNormChkBL";
      tError.functionName = "prepareUW";
      tError.errorMessage = "LCUWMaster表取数据不唯一!";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    mLCUWMasterSet.clear();
    mLCUWMasterSet.add(tLCUWMasterSchema);

    // 核保轨迹表
    LCUWSubSchema tLCUWSubSchema = new LCUWSubSchema();
    LCUWSubDB tLCUWSubDB = new LCUWSubDB();
    tLCUWSubDB.setPolNo( mOldPolNo );
    LCUWSubSet tLCUWSubSet = new LCUWSubSet();
    tLCUWSubSet = tLCUWSubDB.query();
    if (tLCUWSubDB.mErrors.needDealError())
    {
      // @@错误处理
      this.mErrors.copyAllErrors( tLCUWSubDB.mErrors );
      CError tError = new CError();
      tError.moduleName = "UWManuNormChkBL";
      tError.functionName = "prepareUW";
      tError.errorMessage = "LCUWSub表取数失败!";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    int m = tLCUWSubSet.size();
    System.out.println("subcount="+m);
    if (m > 0)
    {
      m++; //核保次数
      tLCUWSubSchema = new LCUWSubSchema();
      tLCUWSubSchema.setUWNo(m); //第几次核保
	  tLCUWSubSchema.setProposalNo(tLCUWMasterSchema.getProposalNo()) ;
	  tLCUWSubSchema.setPolNo(tLCUWMasterSchema.getPolNo()) ;
//	  tLCUWSubSchema.setUWFlag(tLCUWMasterSchema.getState()); //核保意见
      tLCUWSubSchema.setUWGrade(tLCUWMasterSchema.getUWGrade()); //核保级别
      tLCUWSubSchema.setAppGrade(tLCUWMasterSchema.getAppGrade()); //申请级别
      tLCUWSubSchema.setAutoUWFlag(tLCUWMasterSchema.getAutoUWFlag());
      tLCUWSubSchema.setState(tLCUWMasterSchema.getState());
      tLCUWSubSchema.setUWIdea(tLCUWMasterSchema.getUWIdea());
      tLCUWSubSchema.setOperator(tLCUWMasterSchema.getOperator()); //操作员
	  tLCUWSubSchema.setManageCom(tLCUWMasterSchema.getManageCom()) ;
	  tLCUWSubSchema.setMakeDate(PubFun.getCurrentDate()) ;
	  tLCUWSubSchema.setMakeTime(PubFun.getCurrentTime()) ;
      tLCUWSubSchema.setModifyDate(PubFun.getCurrentDate() );
      tLCUWSubSchema.setModifyTime(PubFun.getCurrentTime());

      //条件承保
      if ( mUWFlag.equals("3"))
      {
        tLCUWSubSchema.setSpecReason(tLCUWMasterSchema.getSpecReason());//特约原因
        tLCUWSubSchema.setAddPremReason(tLCUWMasterSchema.getAddPremReason());
      }

      //承保计划变更结论录入
      if (mUWFlag.equals("b"))//此代码冗余(如果入口条件正确时)
      {
        tLCUWSubSchema.setChangePolReason(tLCUWMasterSchema.getUWIdea());
      }
	  //上报核保
	  if (mUWFlag.equals("6"))
	  {
		tLCUWSubSchema.setUpReportContent(tLCUWMasterSchema.getUWIdea());
      }

    }
    else
    {
      // @@错误处理
      this.mErrors.copyAllErrors( tLCUWSubDB.mErrors );
      CError tError = new CError();
      tError.moduleName = "UWManuNormChkBL";
      tError.functionName = "prepareUW";
      tError.errorMessage = "LCUWSub表取数失败!";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    mLCUWSubSet.clear();
    mLCUWSubSet.add(tLCUWSubSchema);

    return true;
  }

  /**
   * 准备主附险核保信息
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean prepareAllUW()
  {
    //判断是不是所有保单都不需要发核保通知书
    String tPrintFlag = "0";
    //取判断是否需要调只返回给操作员标记的校验函数标志
    mBackFlag = "1";
    mAgentPrintFlag = "0";
    //是否有投保单有加费特约
    String tSpecFlag = "0";

    mLCUWMasterSet.clear();
    mLCUWSubSet.clear();
    LCPolSet tLCPolSet = new LCPolSet();

    for(int i = 1;i<= mLCPolSet.size();i++)
    {
      LCPolSchema tLCPolSchema = new LCPolSchema();

      tLCPolSchema = mLCPolSet.get(i);

      LCUWMasterSchema tLCUWMasterSchema = new LCUWMasterSchema();
      LCUWMasterDB tLCUWMasterDB = new LCUWMasterDB();
      tLCUWMasterDB.setPolNo(tLCPolSchema.getPolNo());
      LCUWMasterSet tLCUWMasterSet = new LCUWMasterSet();
      tLCUWMasterSet = tLCUWMasterDB.query();
      if (tLCUWMasterDB.mErrors.needDealError())
      {
        // @@错误处理
        this.mErrors.copyAllErrors( tLCUWMasterDB.mErrors );
        CError tError = new CError();
        tError.moduleName = "UWManuNormChkBL";
        tError.functionName = "prepareAllUW";
        tError.errorMessage = "LCUWMaster表取数失败!";
        this.mErrors .addOneError(tError) ;
        return false;
      }

      int n = tLCUWMasterSet.size();
      System.out.println("该投保单的核保主表当前记录条数:  "+n);
      if (n == 1)
      {
        tLCUWMasterSchema = tLCUWMasterSet.get(1);

        //为核保订正回退保存核保级别和核保人
        mBackUWGrade = tLCUWMasterSchema.getUWGrade();
        mBackAppGrade = tLCUWMasterSchema.getAppGrade();
        mOperator = tLCUWMasterSchema.getOperator();

        //tLCUWMasterSchema.setUWNo(tLCUWMasterSchema.getUWNo()+1);核保主表中的UWNo表示该投保单经过几次人工核保(等价于经过几次自动核保次数),而不是人工核保结论(包括核保通知书,上报等)下过几次.所以将其注释.sxy-2003-09-19
        tLCUWMasterSchema.setPassFlag(mUWFlag); //通过标志
        tLCUWMasterSchema.setState(mUWFlag);
        tLCUWMasterSchema.setUWIdea(mUWIdea);
        tLCUWMasterSchema.setAutoUWFlag("2"); // 1 自动核保 2 人工核保
        tLCUWMasterSchema.setUWGrade(mUWPopedom);
        tLCUWMasterSchema.setAppGrade(mAppGrade);
        tLCUWMasterSchema.setOperator(mOperate); //操作员
        tLCUWMasterSchema.setModifyDate(PubFun.getCurrentDate());
        tLCUWMasterSchema.setModifyTime(PubFun.getCurrentTime());

        if (mUWFlag.equals("2"))
        {
          tLCUWMasterSchema.setPostponeDay(mpostday);
          tLCUWMasterSchema.setPostponeDate(mvalidate);
        }

        if ( mUWFlag.equals("3"))
        {
          tLCUWMasterSchema.setAddPremReason(mReason);
        }

        //发核保通知
        if (mUWFlag.equals("8"))
        {
          //检查复核，核保是不是有录问题件
          mQuesFlag = "0";
          mQuesOrgFlag = "0";
          mAgentQuesFlag = "0";
          mQuesOpeFlag = "0";
          checkQuest(tLCPolSchema);
          checkQuestOrg(tLCPolSchema);
          checkAgentQuest(tLCPolSchema);
          checkQuestOpe(tLCPolSchema);

          if (tLCUWMasterSchema.getPrintFlag().equals("1")||tLCUWMasterSchema.getPrintFlag().equals("4"))
          {
            CError tError = new CError();
            tError.moduleName = "UWManuNormChkBL";
            tError.functionName = "prepareAllUW";
            tError.errorMessage = "已经发核保通知书,但未打印或本保单的问题件均已回复,但相关的主附保单还有问题件未回复,故不可录入!";
            this.mErrors .addOneError(tError) ;
            return false;
          }

          //有特约则不再需要校验是不是返回给操作员
          if(tLCUWMasterSchema.getSpecFlag().equals("1")||tLCUWMasterSchema.getChangePolFlag().equals("1")||mQuesFlag.equals("1"))
          {
            mBackFlag = "0";
            tSpecFlag = "1";
          }

          if ((tLCUWMasterSchema.getSpecFlag().equals("0")||tLCUWMasterSchema.getSpecFlag().equals("2"))&&(tLCUWMasterSchema.getChangePolFlag().equals("0")||tLCUWMasterSchema.getChangePolFlag().equals("2"))&&mQuesFlag.equals("0")&&mQuesOrgFlag.equals("0")&&mAgentQuesFlag.equals("0")&&mQuesOpeFlag.equals("0"))
          {
            tLCUWMasterSchema.setPrintFlag("4");
          }
          else
          {
            tPrintFlag = "1";
            //有需要发核保通知书条件置状态为1
            if (tLCUWMasterSchema.getSpecFlag().equals("1")||mQuesFlag.equals("1")||mAgentQuesFlag.equals("1")||mQuesOpeFlag.equals("1")||mQuesOrgFlag.equals("1"))
              tLCUWMasterSchema.setPrintFlag("1");
          }

          //有问题件
          if(mQuesFlag.equals("1")||mQuesOrgFlag.equals("1")||mAgentQuesFlag.equals("1")||mQuesOpeFlag.equals("1"))
          {
            //复核标记
            tLCPolSchema.setApproveFlag("2");
          }
          tLCPolSet.add(tLCPolSchema);

          //取判断是否需要调只返回给操作员标记的函数标志
          if(tLCUWMasterSchema.getQuesFlag().equals("1")&&(tLCUWMasterSchema.getSpecFlag().equals("0")||tLCUWMasterSchema.getSpecFlag().equals("2"))&&tSpecFlag.equals("0")&&mQuesFlag.equals("0")&&mQuesOrgFlag.equals("0"))
          {
            mBackFlag = "1";
          }

	   }

        if (mUWFlag.equals("3"))
        {
          if (tLCUWMasterSchema.getPrintFlag().equals("1"))
          {
            CError tError = new CError();
            tError.moduleName = "UWManuNormChkBL";
            tError.functionName = "prepareAllUW";
            tError.errorMessage = "已经发核保通知,未打印状态下,不可再录入!";
            this.mErrors .addOneError(tError) ;
            return false;
          }
          tLCUWMasterSchema.setSpecFlag("1");
        }

        //承保计划变更结论录入
        if (mUWFlag.equals("b"))
        {

          if (tLCUWMasterSchema.getPrintFlag().equals("1"))
          {
            CError tError = new CError();
            tError.moduleName = "UWManuNormChkBL";
            tError.functionName = "prepareAllUW";
            tError.errorMessage = "已经发核保通知,未打印状态下不可录入!";
            this.mErrors .addOneError(tError) ;
            return false;
          }

          tLCUWMasterSchema.setUWIdea("");
          tLCUWMasterSchema.setChangePolFlag("1");
          tLCUWMasterSchema.setChangePolReason(mUWIdea);
        }

        //核保订正
        if (mUWFlag.equals("z"))
        {
          tLCUWMasterSchema.setAutoUWFlag("1");
          tLCUWMasterSchema.setState("5");
          tLCUWMasterSchema.setPassFlag("5");
          //恢复核保级别和核保员
          tLCUWMasterSchema.setUWGrade(mBackUWGrade);
          tLCUWMasterSchema.setAppGrade(mBackAppGrade);
          tLCUWMasterSchema.setOperator(mOperator);
          //解锁
          LDSysTraceSchema tLDSysTraceSchema = new LDSysTraceSchema();
          tLDSysTraceSchema.setPolNo(mPolNo);
          tLDSysTraceSchema.setCreatePos("人工核保");
          tLDSysTraceSchema.setPolState("1001");
          LDSysTraceSet inLDSysTraceSet = new LDSysTraceSet();
          inLDSysTraceSet.add(tLDSysTraceSchema);

          VData tVData = new VData();
          tVData.add(mGlobalInput);
          tVData.add(inLDSysTraceSet);

          LockTableBL LockTableBL1 = new LockTableBL();
          if (!LockTableBL1.submitData(tVData, "DELETE")) {
            System.out.println("解锁失败！");
          }
        }
      }
      else
      {
        // @@错误处理
        this.mErrors.copyAllErrors( tLCUWMasterDB.mErrors );
        CError tError = new CError();
        tError.moduleName = "UWManuNormChkBL";
        tError.functionName = "prepareAllUW";
        tError.errorMessage = "LCUWMaster表取数据不唯一!";
        this.mErrors .addOneError(tError) ;
        return false;
      }

      mLCUWMasterSet.add(tLCUWMasterSchema);

      // 核保轨迹表
      LCUWSubSchema tLCUWSubSchema = new LCUWSubSchema();
      LCUWSubDB tLCUWSubDB = new LCUWSubDB();
      tLCUWSubDB.setPolNo( tLCPolSchema.getPolNo() );
      LCUWSubSet tLCUWSubSet = new LCUWSubSet();
      tLCUWSubSet = tLCUWSubDB.query();
      if (tLCUWSubDB.mErrors.needDealError())
      {
        // @@错误处理
        this.mErrors.copyAllErrors( tLCUWSubDB.mErrors );
        CError tError = new CError();
        tError.moduleName = "UWManuNormChkBL";
        tError.functionName = "prepareAllUW";
        tError.errorMessage = "LCUWSub表取数失败!";
        this.mErrors .addOneError(tError) ;
        return false;
      }

      int m = tLCUWSubSet.size();
      System.out.println("subcount="+m);
      if (m > 0)
      {
        m++; //核保次数
		tLCUWSubSchema = new LCUWSubSchema();
		tLCUWSubSchema.setUWNo(m); //第几次核保
		tLCUWSubSchema.setProposalNo(tLCUWMasterSchema.getProposalNo()) ;
		tLCUWSubSchema.setPolNo(tLCUWMasterSchema.getPolNo()) ;
		if (mUWFlag != null && mUWFlag.equals("z"))//核保订正
        {
//		  tLCUWSubSchema.setUWFlag(mUWFlag); //核保意见
		  tLCUWSubSchema.setUWGrade(mUWPopedom); //核保级别
		  tLCUWSubSchema.setAppGrade(mAppGrade); //申请级别
		  tLCUWSubSchema.setAutoUWFlag("2");
		  tLCUWSubSchema.setState(mUWFlag);
		  tLCUWSubSchema.setUWIdea(mUWIdea);
		  tLCUWSubSchema.setOperator(mOperate); //操作员
        }
		else{
//		  tLCUWSubSchema.setUWFlag(tLCUWMasterSchema.getState()); //核保意见
		  tLCUWSubSchema.setUWGrade(tLCUWMasterSchema.getUWGrade()); //核保级别
		  tLCUWSubSchema.setAppGrade(tLCUWMasterSchema.getAppGrade()); //申请级别
		  tLCUWSubSchema.setAutoUWFlag(tLCUWMasterSchema.getAutoUWFlag());
		  tLCUWSubSchema.setState(tLCUWMasterSchema.getState());
		  tLCUWSubSchema.setUWIdea(tLCUWMasterSchema.getUWIdea());
		  tLCUWSubSchema.setOperator(tLCUWMasterSchema.getOperator()); //操作员
		}

		tLCUWSubSchema.setManageCom(tLCUWMasterSchema.getManageCom()) ;
		tLCUWSubSchema.setMakeDate(PubFun.getCurrentDate()) ;
		tLCUWSubSchema.setMakeTime(PubFun.getCurrentTime()) ;
		tLCUWSubSchema.setModifyDate(PubFun.getCurrentDate() );
		tLCUWSubSchema.setModifyTime(PubFun.getCurrentTime());


		//条件承保
		if ( mUWFlag.equals("3"))
		{
		  tLCUWSubSchema.setSpecReason(mSpecReason);//特约原因
		  tLCUWSubSchema.setAddPremReason(mReason);
		}
        //承保计划变更结论录入
        if (mUWFlag.equals("b"))
        {
          tLCUWSubSchema.setChangePolReason(tLCUWMasterSchema.getUWIdea());
        }
		if (mUWFlag.equals("6"))
		{
		  tLCUWSubSchema.setUpReportContent(tLCUWMasterSchema.getUWIdea());
		}
      }
      else
      {
        // @@错误处理
        this.mErrors.copyAllErrors( tLCUWSubDB.mErrors );
        CError tError = new CError();
        tError.moduleName = "UWManuNormChkBL";
        tError.functionName = "prepareAllUW";
        tError.errorMessage = "LCUWSub表取数失败!";
        this.mErrors .addOneError(tError) ;
        return false;
      }

      mLCUWSubSet.add(tLCUWSubSchema);
    }

    if(mUWFlag.equals("8"))
    {
      mLCPolSet.clear();
      mLCPolSet.add(tLCPolSet);
    }

    //没有保单需要发核保通知书
    if(mUWFlag.equals("8")&&tPrintFlag.equals("0"))
    {
      CError tError = new CError();
      tError.moduleName = "UWManuNormChkBL";
      tError.functionName = "prepareAllUW";
      tError.errorMessage = "没有核保通知书内容，不需要发核保通知书!";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    return true;
  }


  /**
   * 准备核保信息
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean CondAccept()
  {
    int n;
    //int i =0;
    int max;
    String sql;
    String specno = "";

    System.out.println("--dealData-dealOneData-CondAccept---");

    //形成特约表信息
    //n=0;
    n = mLCSpecSet.size();
	System.out.println("   特约条数:  "+n);
    if (n> 0)
    {
	  LCSpecSchema tLCSpecSchema = new LCSpecSchema();
//      tLCSpecSchema.setSpecNo(PubFun1.CreateMaxNo("SpecNo",PubFun.getNoLimit(mGlobalInput.ComCode)));
      tLCSpecSchema.setPolNo(mPolNo);
//      tLCSpecSchema.setPolType("1");
      tLCSpecSchema.setEndorsementNo("");
      tLCSpecSchema.setSpecType(mLCSpecSet.get(1).getSpecType());
      tLCSpecSchema.setSpecCode(mLCSpecSet.get(1).getSpecCode());
      tLCSpecSchema.setSpecContent(mLCSpecSet.get(1).getSpecContent());
      tLCSpecSchema.setPrtFlag("1");
      tLCSpecSchema.setBackupType("");
      tLCSpecSchema.setOperator(mOperate);
      tLCSpecSchema.setMakeDate(PubFun.getCurrentDate());
      tLCSpecSchema.setMakeTime(PubFun.getCurrentTime());
      tLCSpecSchema.setModifyDate(PubFun.getCurrentDate());
      tLCSpecSchema.setModifyTime(PubFun.getCurrentTime());

      LCSpecDB tLCSpecDB = new LCSpecDB();
	  LCSpecSet tLCSpecSet = new LCSpecSet();
	  LBSpecSet tLBSpecSet = new LBSpecSet();
	  System.out.println("mPolNo:"+mPolNo) ;
	  tLCSpecDB.setPolNo(mPolNo) ;
	  tLCSpecSet = tLCSpecDB.query() ;
	  System.out.println("tLCSpecSet.size()"+tLCSpecSet.size()) ;
	  for(int i=1 ; i<= tLCSpecSet.size(); i++)
	  {
		LBSpecSchema tLBSpecSchema = new LBSpecSchema();
		mReflections.transFields(tLBSpecSchema,tLCSpecSet.get(i) );

		tLBSpecSchema.setModifyDate(PubFun.getCurrentDate()) ;
		tLBSpecSchema.setModifyTime(PubFun.getCurrentTime()) ;
		tLBSpecSet.add(tLBSpecSchema) ;
	  }

	  System.out.println("tLBSpecSet.size()"+tLBSpecSet.size()) ;
	  mLCSpecSet.clear();
	  mLBSpecSet.clear() ;
	  mLBSpecSet.set(tLBSpecSet);
	  mLCSpecSet.add(tLCSpecSchema);
	  System.out.println("mLBSpecSet.size()"+mLBSpecSet.size()) ;
    }

	//形成特约加费信息
	n = mLCPremSet.size();
	System.out.println("   特约加费条数:  "+n);
    if( n > 0)
    {
      //取责任信息
      LCDutyDB tLCDutyDB = new LCDutyDB();
      tLCDutyDB.setPolNo(mLCPolSchema.getPolNo());
      mLCDutySet = tLCDutyDB.query();

      //更新责任项
      if(mLCDutySet.size() > 0)
      {
        for(int m = 1;m <= mLCDutySet.size();m++)
        {
          int maxno = 0;
          LCDutySchema tLCDutySchema = new LCDutySchema();
          tLCDutySchema = mLCDutySet.get(m);

          //减去该责任的原加费金额
          sql = "select * from LCPrem where payplancode  like '000000%' and polno = '"+mLCPolSchema.getPolNo().trim()+"' and dutycode = '"+tLCDutySchema.getDutyCode().trim()+"'";
          LCPremDB t2LCPremDB = new LCPremDB();
          LCPremSet t2LCPremSet = new LCPremSet();

          t2LCPremSet = t2LCPremDB.executeQuery(sql);

          if(t2LCPremSet.size() > 0)
          {
            for(int j = 1;j<=t2LCPremSet.size();j++)
            {
              LCPremSchema t2LCPremSchema = new LCPremSchema();
              t2LCPremSchema = t2LCPremSet.get(j);

              tLCDutySchema.setPrem(tLCDutySchema.getPrem()-t2LCPremSchema.getPrem());
              mLCPolSchema.setPrem(mLCPolSchema.getPrem()-t2LCPremSchema.getPrem());
            }
          }

         //为投保单表和责任表加上本次的特约加费.同时形成特约加费信息
          for (int i = 1;i<= n;i++)
          {
            LCPremSchema ttLCPremSchema = new LCPremSchema();
            ttLCPremSchema = mLCPremSet.get(i);
            LCPremSchema tLCPremSchema = new LCPremSchema();
            double tPrem;

            if(ttLCPremSchema.getDutyCode().equals(tLCDutySchema.getDutyCode()))
            {
              maxno = maxno + 1;
              LCPremDB tLCPremDB = new LCPremDB();

              tLCPremDB.setPolNo(mLCPolSchema.getPolNo());
              //tLCPremDB.setDutyCode(tLCPremSchema.getDutyCode());//原whn的
			  tLCPremDB.setDutyCode(tLCDutySchema.getDutyCode());//SXY添加的
              LCPremSet tLCPremSet = tLCPremDB.query();
              tLCPremSchema = tLCPremSet.get(1);

              String PayPlanCode = "";
              PayPlanCode = String.valueOf(maxno);
              for (int j = PayPlanCode.length();j<8;j++)
              {
                PayPlanCode = "0"+PayPlanCode;
              }

              System.out.println("payplancode"+PayPlanCode);

              //保单总保费
              tPrem = mLCPolSchema.getPrem() + ttLCPremSchema.getPrem();

              //tLCPremSchema.setPolNo(mLCPolSchema.getPolNo());//以下注销处表明其信息是沿用已前该责任加费信息
              //tLCPremSchema.setDutyCode(mmaxDutyCode);
              tLCPremSchema.setPayPlanCode(PayPlanCode);
              //tLCPremSchema.setGrpPolNo(mLCPolSchema.get);
              tLCPremSchema.setPayPlanType(ttLCPremSchema.getPayPlanType());
              //tLCPremSchema.setPayTimes();
              //tLCPremSchema.setPayIntv();
              //tLCPremSchema.setMult();
              tLCPremSchema.setStandPrem(ttLCPremSchema.getPrem());
              tLCPremSchema.setPrem(ttLCPremSchema.getPrem());
              //tLCPremSchema.setSumPrem();
              //tLCPremSchema.setRate();
              tLCPremSchema.setPayStartDate(ttLCPremSchema.getPayStartDate());
              tLCPremSchema.setPayEndDate(ttLCPremSchema.getPayEndDate());
              //tLCPremSchema.setPaytoDate();
              tLCPremSchema.setState("1");//承保加费标示为2 modify by sxy at 2004=-03-11
              //tLCPremSchema.setBankCode();
              //tLCPremSchema.setBankAccNo();
              //tLCPremSchema.setAppntNo();
              //tLCPremSchema.setAppntType("1"); //投保人类型
		/*Lis5.3 upgrade get
  tLCPremSchema.setSuppRiskScore(ttLCPremSchema.getSuppRiskScore());
              */
                          tLCPremSchema.setModifyDate(PubFun.getCurrentDate());
              tLCPremSchema.setModifyTime(PubFun.getCurrentTime());

              //更新保险责任
              mLCDutySet.remove(tLCDutySchema);
              tLCDutySchema.setPrem(tLCDutySchema.getPrem()+tLCPremSchema.getPrem());
              mLCDutySet.add(tLCDutySchema);

              mmLCPremSet.add(tLCPremSchema);

              //更新保单数据
              mLCPolSchema.setPrem(tPrem);

            }

          }

        }
      }

    }
    return true;
  }

  /**
   * 检查是不是需要送核保通知书到打印队列
   * @return
   */
  private String checkBackOperator(String tPrintFlag)
  {
    LCPolSet tLCPolSet = new LCPolSet();

    for(int i = 1;i<= mLCPolSet.size();i++)
    {
      LCPolSchema tLCPolSchema = new LCPolSchema();

      tLCPolSchema = mLCPolSet.get(i);

      //有返回保户需要打印
      //String tsql = "select * from lcpol where  ProposalNo in ( select ProposalNo from LCIssuePol where ((makedate >= (select max(makedate) from lcissuepol where backobjtype in ('1','4') and ProposalNo = '"+tLCPolSchema.getPolNo()+"' and makedate is not null)) or ((select max(makedate) from lcissuepol where backobjtype in ('1','4') and ProposalNo = '"+tLCPolSchema.getPolNo()+"') is null))"
      String tsql = "select * from lcpol where  ProposalNo in ( select ProposalNo from LCIssuePol where 1 = 1 "
                  + " and backobjtype  = '3'"
                  + " and ProposalNo = '"+tLCPolSchema.getPolNo()+"'"
                  + " and makedate is not null"
                  + " and replyresult is null"
                  + " and needprint = 'Y')";

      System.out.println("printchecksql:"+tsql);
      LCPolDB tLCPolDB = new LCPolDB();
      LCPolSet t2LCPolSet = new LCPolSet();
      t2LCPolSet = tLCPolDB.executeQuery(tsql);
      if(t2LCPolSet.size()>0)
      {
        tPrintFlag = "2";
      }



      //只返回给操作员,机构不打印
      //tsql = "select * from lcpol where  ProposalNo in ( select ProposalNo from LCIssuePol where ((makedate >= (select max(makedate) from lcissuepol where backobjtype in ('2','3') and ProposalNo = '"+tLCPolSchema.getPolNo()+"' and makedate is not null)) or ((select max(makedate) from lcissuepol where backobjtype in ('3','2') and ProposalNo = '"+tLCPolSchema.getPolNo()+"') is null))"
      tsql = "select * from lcpol where  ProposalNo in ( select ProposalNo from LCIssuePol where 1 = 1 "
           + " and backobjtype = '1'"
           + " and ProposalNo = '"+tLCPolSchema.getPolNo()+"'"
           + " and makedate is not null"
           + " and replyresult is null)"
           + " and ProposalNo not in ( select ProposalNo from LCIssuePol where 1 = 1 "
           + " and backobjtype in ('2','3')"
           + " and ProposalNo = '"+tLCPolSchema.getPolNo()+"'"
           + " and makedate is not null"
           + " and replyresult is null"
           + " and needprint = 'Y')"
           + " and ProposalNo not in ( select ProposalNo from LCIssuePol where 1 = 1 "
           + " and backobjtype = '4'"
           + " and ProposalNo = '"+tLCPolSchema.getPolNo()+"'"
           + " and makedate is not null"
           + " and replyresult is null)";

      System.out.println("printchecksql2:"+tsql);
      tLCPolDB = new LCPolDB();
      t2LCPolSet = new LCPolSet();

      t2LCPolSet = tLCPolDB.executeQuery(tsql);
      if(t2LCPolSet.size()>0)
      {
        //复核标记
        tLCPolSchema.setApproveFlag("1");
      }

      tLCPolSet.add(tLCPolSchema);

    }
    mLCPolSet.clear();
    mLCPolSet.add(tLCPolSet);

    if(tPrintFlag.equals("2"))
    {
      tPrintFlag = "0";
    }
    else
    {
      tPrintFlag = "1";
    }

    return tPrintFlag;
  }

  /**
   * 打印信息表
   * @return
   */
  private boolean print()
  {
    String tIfPrintFlag = "0";

    LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();

    tLOPRTManagerSchema.setOtherNo(mPolNo);
    System.out.println("polno:"+mPolNo);
    tLOPRTManagerSchema.setManageCom(mLCPolSchema.getManageCom());
    tLOPRTManagerSchema.setPrtType(PrintManagerBL.PT_FRONT);
    tLOPRTManagerSchema.setAgentCode(mLCPolSchema.getAgentCode());
    tLOPRTManagerSchema.setOtherNoType(PrintManagerBL.ONT_INDPOL);
    if(mUWFlag.equals("1")) //拒保通知书
    {
      tLOPRTManagerSchema.setCode(PrintManagerBL.CODE_DECLINE);
      tLOPRTManagerSchema.setOtherNo(mGetNoticeNo);
      tLOPRTManagerSchema.setStandbyFlag1(mPolNo);
    }
    if(mUWFlag.equals("2")) //延期通知书
    {
      tLOPRTManagerSchema.setCode(PrintManagerBL.CODE_DEFER);
      tLOPRTManagerSchema.setOtherNo(mGetNoticeNo);
      tLOPRTManagerSchema.setStandbyFlag1(mPolNo);
    }
    if(mUWFlag.equals("8")) //核保通知书
    {
      tLOPRTManagerSchema.setCode(PrintManagerBL.CODE_UW);
      //只有问题件的时候校验
      tIfPrintFlag = "0";
      if(mBackFlag.equals("1"))
      {
        tIfPrintFlag = checkBackOperator(tIfPrintFlag);
      }
    }
    if(mUWFlag.equals("a")) //撤单
    {
      tLOPRTManagerSchema.setCode(PrintManagerBL.CODE_WITHDRAW);
      tLOPRTManagerSchema.setOtherNo(mGetNoticeNo);
      tLOPRTManagerSchema.setStandbyFlag1(mPolNo);
    }


    VData tVData = new VData();
    PrintManagerBL tPrintManagerBL = new PrintManagerBL();

    tVData.add(tLOPRTManagerSchema);
    tVData.add( mGlobalInput);

    System.out.println("Start PrintManagerBL Submit...");
    if(tIfPrintFlag.equals("0")) //只返回给操作员问题件无需发核保通知书
    {
      if(!tPrintManagerBL.submitData(tVData,"REQUEST"))
      {
        // @@错误处理
        this.mErrors.copyAllErrors(tPrintManagerBL.mErrors);
        CError tError = new CError();
        tError.moduleName = "UWManuNormChkBL";
        tError.functionName = "print";
        tError.errorMessage = "数据提交失败!";
        this.mErrors .addOneError(tError) ;
        return false;
      }
    }

    mLOPRTManagerSet.add(tLOPRTManagerSchema);
    return true;
  }

  /**
   * 返回业务员问题件件送打印队列
   * @return
   */
  private boolean printAgent()
  {
    LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();

    tLOPRTManagerSchema.setOtherNo(mPolNo);
    tLOPRTManagerSchema.setManageCom(mLCPolSchema.getManageCom());
    tLOPRTManagerSchema.setPrtType(PrintManagerBL.PT_FRONT);
    tLOPRTManagerSchema.setAgentCode(mLCPolSchema.getAgentCode());
    tLOPRTManagerSchema.setOtherNoType(PrintManagerBL.ONT_INDPOL);
    tLOPRTManagerSchema.setCode(PrintManagerBL.CODE_AGEN_QUEST);

    VData tVData = new VData();
    PrintManagerBL tPrintManagerBL = new PrintManagerBL();

    tVData.add(tLOPRTManagerSchema);
    tVData.add( mGlobalInput);

    System.out.println("Start PrintManagerBL Submit...");
    if(!tPrintManagerBL.submitData(tVData,"REQUEST"))
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tPrintManagerBL.mErrors);
      CError tError = new CError();
      tError.moduleName = "UWManuNormChkBL";
      tError.functionName = "print";
      tError.errorMessage = "数据提交失败!";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    return true;
  }


  /**
   * 延期承保
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean TimeAccept()
  {
    Date temp;
    String temp1 = "D";
    Date temp2;

    FDate tFDate = new FDate();
    temp = null;
    temp2 = tFDate.getDate(mLCPolSchema.getCValiDate());
//delete by yt 20030719,将延期日期的类型修改为字符串，该部分代码不再有效
//    mvalidate = PubFun.calDate(temp2,mpostday,temp1,temp);

    //System.out.println("---TimeAccept---");
    //mLCPolSchema.setCValiDate(mvalidate);
    //System.out.println("---mvalidate---"+mvalidate);
    return true;
  }

  /**
   * 待上级核保
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean uplevel()
  {
    LCUWErrorDB tLCUWErrorDB = new LCUWErrorDB();
    tLCUWErrorDB.setPolNo(mLCPolSchema.getPolNo());
    String tpolno = mLCPolSchema.getPolNo();
    String tsql = "select * from lcuwerror where polno = '"+tpolno.trim()+"' and uwno = (select max(uwno) from lcuwerror where polno = '"+tpolno.trim()+"')";
    LCUWErrorSet tLCUWErrorSet = tLCUWErrorDB.executeQuery(tsql);
    String tcurrgrade = "A";
    String terrgrade = "";
    System.out.println(" in uplevel()");
    int errno = tLCUWErrorSet.size();
    int j = 0;
    if (errno > 0)
    {
      for( int i = 1; i <= errno; i++)
      {

        LCUWErrorSchema tLCUWErrorSchema = new LCUWErrorSchema();
        tLCUWErrorSchema = tLCUWErrorSet.get(i);
        terrgrade = tLCUWErrorSchema.getUWGrade();
		System.out.println("上报级别:terrgrade"+terrgrade);
        if (j == 0 && mUWPopedom.compareTo(terrgrade) < 0)
        {
          j++;
          tcurrgrade = terrgrade;
        }
        else
        {
          if(mUWPopedom.compareTo(terrgrade) < 0 && terrgrade.compareTo(tcurrgrade) > 0)
          {
            tcurrgrade = terrgrade;
          }
        }
		System.out.println("上报级别:tcurrgrade"+tcurrgrade);
      }
      mAppGrade = tcurrgrade;
    }
    System.out.println("上报级别:mAppGrade"+mAppGrade);
    if(errno == 0||(mUWPopedom.compareTo(mAppGrade) >= 0&&mUWPopedom.compareTo("L") < 0))
    {
      char temp[];
      char tempgrade;
      temp = mUWPopedom.toCharArray();
      tempgrade = (char)((int)temp[0]+1);
      System.out.println("上报级别:"+tempgrade);
      mAppGrade = String.valueOf(tempgrade);
    }
	//指定上报级别
	System.out.println("上报级别:mUPUWCode+mOperate"+mUPUWCode+mOperate);
    if(mUPUWCode!=null&&!mUPUWCode.equals(mOperate) )
	{
	  LDUserDB  tLDUserDB = new LDUserDB();
	   tsql = "select * from lduser where usercode = '"+mUPUWCode+"'";
	  LDUserSet tLDUserSet = tLDUserDB.executeQuery(tsql);
	  if(tLDUserSet.size() != 1)
	  {
		CError tError = new CError();
		tError.moduleName = "UWManuNormChkBL";
		tError.functionName = "uplever";
		tError.errorMessage = "指定核保师信息有误!";
		this.mErrors .addOneError(tError) ;
        return false;
	  }
	  else
	  {
		System.out.println("上报级别:mAppGrade+tLDUserSet.get(1).getUWPopedom()"+mAppGrade+tLDUserSet.get(1).getUWPopedom());
	   if(mAppGrade.compareTo(tLDUserSet.get(1).getUWPopedom())>0 )
	   {
		 CError tError = new CError();
		  tError.moduleName = "UWManuNormChkBL";
		  tError.functionName = "uplever";
		  tError.errorMessage = "指定核保师级别太底!";
		  this.mErrors .addOneError(tError) ;
          return false;
	   }
	   else
	    mAppGrade = tLDUserSet.get(1).getUWPopedom();
	  }

	}
	System.out.println("上报级别:mAppGrade"+mAppGrade);

    //撤销核保申请锁
    mLDSysTraceSchema.setPolNo(mPolNo);
    mLDSysTraceSchema.setOperator(mOperate);
    mLDSysTraceSchema.setPolState(1001);

    mLDSysTraceSet.add(mLDSysTraceSchema);
	return true;
  }


  /**
   * 准备需要保存的数据
   */
  private void prepareOutputData()
  {
    mInputData.clear();
    mInputData.add( mAllLCPolSet );
    mInputData.add( mAllLCUWMasterSet );
    mInputData.add( mAllLCUWSubSet );
    mInputData.add( mAllLCSpecSet );
	mInputData.add( mAllLBSpecSet );
    mInputData.add( mAllLCPremSet );
    mInputData.add( mAllLCGetSet );
    mInputData.add( mAllLCDutySet );
    mInputData.add( mAllLOPRTManagerSet);
    mInputData.add( mAllLDSysTraceSet);

    mInputData.add(outLJSPaySet);
    mInputData.add(outLJTempFeeSet);
    mInputData.add(outLBTempFeeSet);
    mInputData.add(outLJTempFeeClassSet);
    mInputData.add(outLBTempFeeClassSet);

  }

}
