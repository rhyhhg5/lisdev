package com.sinosoft.lis.cbcheck;

import java.util.*;
import java.lang.Math.*;
import java.text.SimpleDateFormat;
import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
/**
 * <p>Title: 通用函数</p>
 * <p>Description:</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author WHN
 * @version 1.0
 */
public class UWPubFun
{
  public UWPubFun() {
  }
  public static void main(String[] args) {
    PubFun pubfun = new PubFun();
  }

  /**
   * 得到规定格式日期 author: WHN
   * @return 日期格式为"yyyy-MM-dd"
   */
  public static String getFixedDate(Date today)
  {
    String pattern="yyyy-MM-dd";
    SimpleDateFormat df = new SimpleDateFormat(pattern);
    String tString = df.format(today);
    return tString;
  }

  /**
   * 复核修改问题件校验
   * input: ProposalNo
   * return: true or false
   */
  public static boolean checkQues(String tProposalNo)
  {
    LCIssuePolDB tLCIssuePolDB = new LCIssuePolDB();
    LCIssuePolSet tLCIssuePolSet = new LCIssuePolSet();

    String tsql = "select * from lcissuepol where proposalno = '"+tProposalNo+"' and replyresult is null and operatepos in ('1','5') and backobjtype = '1'";
    tLCIssuePolSet = tLCIssuePolDB.executeQuery(tsql);

    if(tLCIssuePolSet.size() > 0)
      return false;

    return true;
  }
}
