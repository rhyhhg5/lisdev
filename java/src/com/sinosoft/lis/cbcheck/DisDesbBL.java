package com.sinosoft.lis.cbcheck;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: SinoSoft</p>
 * @author HYQ
 * @version 1.0
 */

public class DisDesbBL
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    private VData mInputData;
    private GlobalInput tGI = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;
    private String mOperator;
    private String mManageCom;

    /** 业务操作类 */
    private LCPENoticeResultSet mLCPENoticeResultSet =new LCPENoticeResultSet();
    private LCDiseaseResultSet mLCDiseaseResultSet = new LCDiseaseResultSet();

    /** 业务数据 */
    private String mName = "";
    private String mDelSql= "";

    public DisDesbBL()
    {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        System.out.println("Operate==" + cOperate);
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;
        System.out.println("After getinputdata");

        if (!checkData())
            return false;

        //进行业务处理
        if (!dealData())
            return false;
        System.out.println("After dealData！");
        //准备往后台的数据
        if (!prepareOutputData())
            return false;
        System.out.println("After prepareOutputData");

        System.out.println("Start DisDesbBL Submit...");

        PubSubmit tSubmit = new PubSubmit();

        if (!tSubmit.submitData(mResult, ""))
        {
                // @@错误处理
                this.mErrors.copyAllErrors(tSubmit.mErrors);

                CError tError = new CError();
                tError.moduleName = "GrpUWAutoChkBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);

                return false;
        }

        System.out.println("DisDesbBL end");

        return true;
    }


    /**
     * prepareOutputData
     *
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        mResult.clear();
        MMap map = new MMap();

        map.put(mDelSql,"DELETE");
        map.put(mLCPENoticeResultSet,"INSERT");
        map.put(mLCDiseaseResultSet,"INSERT");

        mResult.add(map);

        return true;
    }


    /**
     * dealData
     * 业务逻辑处理
     * @return boolean
     */
    private boolean dealData()
    {
        int tDisSerialNo;
        LCDiseaseResultSchema tLCDiseaseResultSchema;
        LCDiseaseResultDB tLCDiseaseResultDB = new LCDiseaseResultDB();
        tLCDiseaseResultDB.setContNo(mLCPENoticeResultSet.get(1).getContNo());
        tLCDiseaseResultDB.setCustomerNo(mLCPENoticeResultSet.get(1).getCustomerNo());
        LCDiseaseResultSet tLCDiseaseResultSet = tLCDiseaseResultDB.query();
        if(tLCDiseaseResultSet.size()==0)
        {
            tDisSerialNo = 0;
        }
        else
        {
            tDisSerialNo = tLCDiseaseResultSet.size() + 1;
        }
        //先删除已经存过的数据
        mDelSql = "delete from lcpenoticeresult where 1=1 "
                  + " and ProposalContNo= '" + mLCPENoticeResultSet.get(1).getProposalContNo() + "'"
                  + " and prtseq = '" + mLCPENoticeResultSet.get(1).getPrtSeq() + "'"
                  ;
        for (int i = 1; i <= mLCPENoticeResultSet.size(); i++)
        {
            mLCPENoticeResultSet.get(i).setName(mName);
            mLCPENoticeResultSet.get(i).setSerialNo(""+tDisSerialNo);
            mLCPENoticeResultSet.get(i).setOperator(mOperator);
            mLCPENoticeResultSet.get(i).setMakeDate(PubFun.getCurrentDate());
            mLCPENoticeResultSet.get(i).setMakeTime(PubFun.getCurrentTime());
            mLCPENoticeResultSet.get(i).setModifyDate(PubFun.getCurrentDate());
            mLCPENoticeResultSet.get(i).setModifyTime(PubFun.getCurrentTime());


            tLCDiseaseResultSchema = new LCDiseaseResultSchema();
            tLCDiseaseResultSchema.setSerialNo(""+tDisSerialNo);
            tLCDiseaseResultSchema.setContNo(mLCPENoticeResultSet.get(i).getContNo());
            tLCDiseaseResultSchema.setProposalContNo(mLCPENoticeResultSet.get(i).getContNo());
            tLCDiseaseResultSchema.setName(mName);
            tLCDiseaseResultSchema.setCustomerNo(mLCPENoticeResultSet.get(i).getCustomerNo());
            tLCDiseaseResultSchema.setDisDesb(mLCPENoticeResultSet.get(i).getDisDesb());
            tLCDiseaseResultSchema.setDisResult(mLCPENoticeResultSet.get(i).getDisResult());
            tLCDiseaseResultSchema.setDiseaseSource("2");                //体检结果分析
            tLCDiseaseResultSchema.setICDCode(mLCPENoticeResultSet.get(i).getICDCode());
            tLCDiseaseResultSchema.setDiseaseCode(mLCPENoticeResultSet.get(i).getDiseaseCode());
            tLCDiseaseResultSchema.setMakeDate(PubFun.getCurrentDate());
            tLCDiseaseResultSchema.setMakeTime(PubFun.getCurrentTime());
            tLCDiseaseResultSchema.setModifyDate(PubFun.getCurrentDate());
            tLCDiseaseResultSchema.setModifyTime(PubFun.getCurrentTime());
            tLCDiseaseResultSchema.setOperator(mOperator);

            mLCDiseaseResultSet.add(tLCDiseaseResultSchema);

            tDisSerialNo++;

        }

        return true;
    }


    /**
     * checkData
     * 数据校验
     * @return boolean
     */
    private boolean checkData()
    {
        LDPersonDB tLDPersonDB = new LDPersonDB();
        LDPersonSchema tLDPersonSchema =new LDPersonSchema();
        tLDPersonDB.setCustomerNo(mLCPENoticeResultSet.get(1).getCustomerNo());
        if(!tLDPersonDB.getInfo())
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCGrpContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "DisDesbBL";
            tError.functionName = "checkData";
            tError.errorMessage = "客户信息查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        tLDPersonSchema=tLDPersonDB.getSchema();
        mName = tLDPersonSchema.getName();
        return true;
    }


    /**
     * getInputData
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        // 公用变量
        tGI = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        mLCPENoticeResultSet =(LCPENoticeResultSet) cInputData.getObjectByObjectName("LCPENoticeResultSet", 0);

        mOperator = tGI.Operator;
        if(mOperator==null||mOperator.length()<=0)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCGrpContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "DisDesbBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据Operator失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //取得疾病信息
        if(mLCPENoticeResultSet==null||mLCPENoticeResultSet.size()<= 0)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCGrpContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "DisDesbBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输数据LCPENoticeResultSet失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 返回结果
     * @return VData
     */
    public VData getResult()
    {
        return mResult;
    }

}
