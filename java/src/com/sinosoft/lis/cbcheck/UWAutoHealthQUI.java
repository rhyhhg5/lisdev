package com.sinosoft.lis.cbcheck;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
/**
 * <p>Title: Web业务系统体检资料录入结论部分</p>
 * <p>Description:承保个人单自动核保功能类
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author WHN
 * @version 1.0
 */
public class UWAutoHealthQUI
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  public UWAutoHealthQUI() {}

// @Main
  public static void main(String[] args)
  {
  GlobalInput tG = new GlobalInput();
  tG.Operator = "aaa";

  LCPolSchema p = new LCPolSchema();
  p.setProposalNo( "130000000000609" );
  //p.setPolNo("130000000000609");

  LCPolSet tLCPolSet = new LCPolSet();
  tLCPolSet.add(p);

  //
  LCPENoticeSchema tLCPENoticeSchema = new LCPENoticeSchema();

  tLCPENoticeSchema.setContNo("86110020030110003050");
  tLCPENoticeSchema.setCustomerNo("33");
  tLCPENoticeSchema.setPEAddress("001000000");
  tLCPENoticeSchema.setPEDate("2002-10-31");
  tLCPENoticeSchema.setPEBeforeCond("0");
  tLCPENoticeSchema.setRemark("TEST");

  LCPENoticeSet tLCPENoticeSet = new LCPENoticeSet();
  tLCPENoticeSet.add(tLCPENoticeSchema);

  //
  LCPENoticeItemSchema tLCPENoticeItemSchema = new LCPENoticeItemSchema();

  tLCPENoticeItemSchema.setPEItemCode("001");
  tLCPENoticeItemSchema.setPEItemName("普通体检");

  LCPENoticeItemSet tLCPENoticeItemSet = new LCPENoticeItemSet();
  tLCPENoticeItemSet.add(tLCPENoticeItemSchema);

  tLCPENoticeItemSchema = new LCPENoticeItemSchema();
  tLCPENoticeItemSchema.setPEItemCode("002");
  tLCPENoticeItemSchema.setPEItemName("X光");
  tLCPENoticeItemSet.add(tLCPENoticeItemSchema);

  VData tVData = new VData();
  tVData.add( tLCPolSet );
  tVData.add( tLCPENoticeSet);
  tVData.add( tLCPENoticeItemSet);
  tVData.add( tG );
  UWAutoHealthQUI ui = new UWAutoHealthQUI();
  if( ui.submitData( tVData, "" ) == true )
      System.out.println("---ok---");
  else
      System.out.println("---NO---");
  }

  /**
  传输数据的公共方法
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;

    UWAutoHealthQBL tUWAutoHealthQBL = new UWAutoHealthQBL();

    System.out.println("---UWAutoHealthQBL UI BEGIN---");
    if (tUWAutoHealthQBL.submitData(cInputData,mOperate) == false)
	{
  		// @@错误处理
      this.mErrors.copyAllErrors(tUWAutoHealthQBL.mErrors);
      CError tError = new CError();
      tError.moduleName = "UWAutoHealthQUI";
      tError.functionName = "submitData";
      tError.errorMessage = "数据查询失败!";
      this.mErrors .addOneError(tError) ;
      mResult.clear();
      return false;
	}
    return true;
  }
}