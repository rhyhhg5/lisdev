package com.sinosoft.lis.cbcheck;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import java.text.DecimalFormat;

/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class FeeRateBL {
  public FeeRateBL() {
  }

  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors = new CErrors();
  private VData mResult = new VData();

  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  private MMap map = new MMap();

  /** 全局数据 */
  private GlobalInput mGlobalInput;
  private LCGrpPolSet mLCGrpPolSet;

  /** 数据操作字符串 */
  private String mOperate;
  private String FORMATMODOL = "0.00"; //精确位数
  private DecimalFormat mDecimalFormat = new DecimalFormat(FORMATMODOL); //数字转换对象

  // private String mComFeeRate;//总公司费用率
  // private String mBranchFeeRate;//分公司费用率
  /** 业务处理相关变量 */

  /**
   * ＵＩ接口
   * @param cInputData VData
   * @param cOperate String
   * @return boolean
   */
  public boolean submitData(VData cInputData, String cOperate) {
    mInputData = cInputData;
    mOperate = cOperate;
    if (!getInputData()) {
      return false;
    }
    if (!checkData()) {
      return false;
    }
    if (!dealData()) {
      return false;
    }
    if (!prepareOutputData()) {
      return false;
    }

    PubSubmit tPubsubmit = new PubSubmit();
    if (!tPubsubmit.submitData(this.mResult, "INSERT")) {
      this.mErrors.copyAllErrors(tPubsubmit.mErrors);
      return false;
    }
    return true;
  }

  /**
   * getInputData
   *
   * @return boolean
   */
  private boolean getInputData() {
    mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
        "GlobalInput", 0);
    mLCGrpPolSet = (LCGrpPolSet) mInputData.getObjectByObjectName(
        "LCGrpPolSet", 0);

    return true;
  }

  /**
   * checkData
   *
   * @return boolean
   */
  private boolean checkData() {
    if (this.mGlobalInput == null) {
      buildError("checkData", "传入管理机构及人员信息为空,请重新登陆！");
      return false;
    }
    if (this.mLCGrpPolSet == null) {
      buildError("checkData", "传入的险种信息为空！");
      return false;
    }
    if (this.mLCGrpPolSet.size() <= 0) {
      buildError("checkData", "传入的险种信息为空！");
      return false;
    }
    for (int i = 1; i <= this.mLCGrpPolSet.size(); i++) {
      if (this.mLCGrpPolSet.get(i).getGrpContNo().equals("")) {
        buildError("checkData", "传入的合同信息为空！");
        return false;
      }
      if (this.mLCGrpPolSet.get(i).getRiskCode().equals("")) {
        buildError("checkData", "传入的险种信息为空！");
        return false;
      }
//      if (this.mLCGrpPolSet.get(i).getComFeeRate() <= 0) {
//        buildError("checkData", "传入的总公司管理费为0！");
//        return false;
//      }
//      if (this.mLCGrpPolSet.get(i).getBranchFeeRate() <= 0) {
//        buildError("checkData", "传入的分公司的管理费小于等于0！");
//        return false;
//      }

    }
    if (this.mOperate == null) {
      buildError("checkData", "传入的操作符为空！");
      return false;
    }
    return true;
  }

  /**
   * dealData
   *
   * @return boolean
   */
  private boolean dealData() {
    if (this.mOperate.equals("UPDATE||FEERATE")) {
      if (!updateFeeRate()) {
        return false;
      }
    }
    return true;
  }

  /**
   * updateFeeRate
   *
   * @return boolean
   */
  private boolean updateFeeRate() {
    for (int i = 1; i <= this.mLCGrpPolSet.size(); i++) {
      LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
      tLCGrpPolDB.setGrpContNo(mLCGrpPolSet.get(i).getGrpContNo());
      tLCGrpPolDB.setRiskCode(mLCGrpPolSet.get(i).getRiskCode());
      LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.query();
      if (tLCGrpPolSet.size() <= 0) {
        buildError("updateFeeRate", "查询险种信息失败！");
        return false;
      }
      if (tLCGrpPolSet.size() > 1) {
        buildError("updateFeeRate", "查询险种信息为多条！");
        return false;
      }
      LCGrpPolSchema tLCGrpPolSchema = tLCGrpPolSet.get(1);

      //aDisCount 表示界面传输的折扣比例
      String aDisCount = mLCGrpPolSet.get(i).getStandbyFlag3();
      if (aDisCount.equals(null) || aDisCount.equals("")) {
        buildError("updateFeeRate", "界面传输的折扣比例为空,不允许保存！");
        return false;
      }
      double DisCount = Double.parseDouble(aDisCount);
      if (DisCount <= 0) {
        buildError("checkData", "传入的折扣比例小于等于0！");
        return false;
      }
      /**
       *  如果保险责任中存在标准保费为零的情况,，那么这时应该比较系统自动计算的折扣和标准保费是否一致
       *  不一致提示错误信息
       */

      String tExist = (new ExeSQL()).getOneValue(
          "select distinct 1 from lcduty where polno in (select polno from lcpol where grppolno = '" +
          tLCGrpPolSchema.getGrpPolNo() + "')  and standprem = 0");
      if (!tExist.equals("1")) {
        String tStandprem = (new ExeSQL()).getOneValue(
            "select sum(standprem) from lcpol where GrpPolno = '" +
            tLCGrpPolSchema.getGrpPolNo() + "'");

        if (tStandprem == null || tStandprem.equals("") ||
            tStandprem.equals("0")) {
          buildError("checkData", "在取得该保单标准保费的时候发生错误");
          return false;
        }
        double standprem = Double.parseDouble(tStandprem);
        double tStandDisCount = tLCGrpPolSchema.getPrem() / standprem;
        if (tStandDisCount <= 0) {
          buildError("checkData", "在取得该保单标准折扣的时候发生错误");
          return false;
        }
        if (Math.abs(tStandDisCount - DisCount) >= 0.01) {
          buildError("checkData",
                     "险种" + tLCGrpPolSchema.getRiskCode() + "界面录入的折扣比例" +
                     DisCount + "与系统自动计算的折扣比例" + tStandDisCount + "不符,不允许保存! ");
          return false;
        }
      }
      /**
       * 否则计算分公司费用率
       */
      else {

        //折扣底线
        String tMinDisCount = (new ExeSQL()).getOneValue(
            " select popedomvalue from lduwgrade where "
            + " popedomtype='107' and uwgrade='1' and riskcode='" +
            mLCGrpPolSet.get(i).getRiskCode() + "' and managecom =SubStr('" +
            tLCGrpPolSchema.getManageCom() + "',1,4) ");
        if (tMinDisCount.equals(null) || tMinDisCount.equals("") ||
            tMinDisCount.equals("null")) {
          buildError("updateFeeRate", "产品未定义此费用率,不能修改！");
          return false;
        }
        double aMinDisCount = Double.parseDouble(tMinDisCount);
        /**
         * 分公司费用率
         * 公式：（实折－折扣底线）/实折
         * DisCount:实折  aMinDisCont:折扣底线
         */
        if (DisCount - aMinDisCount < 0) {
          buildError("updateFeeRate", "录入折扣比例小于系统折扣底线！");
          return false;
        }

        double aBranchFeeRate =Arith.round((DisCount - aMinDisCount) / DisCount, 2);
        tLCGrpPolSchema.setBranchFeeRate(aBranchFeeRate);
       }
        tLCGrpPolSchema.setOperator(this.mGlobalInput.Operator);
        tLCGrpPolSchema.setModifyDate(PubFun.getCurrentDate());
        tLCGrpPolSchema.setModifyTime(PubFun.getCurrentTime());
        tLCGrpPolSchema.setStandbyFlag3(aDisCount);
        mLCGrpPolSet.set(i, tLCGrpPolSchema);

    }
    map.put(this.mLCGrpPolSet, "UPDATE");
    return true;
  }

  /**
   * prepareOutputData
   *
   * @return boolean
   */
  private boolean prepareOutputData() {
    this.mResult.add(this.map);
    return true;
  }

  /**
   * 出错处理
   * @param szFunc String
   * @param szErrMsg String
   */
  private void buildError(String szFunc, String szErrMsg) {
    CError cError = new CError();
    cError.moduleName = "FeeRateBL";
    cError.functionName = szFunc;
    cError.errorMessage = szErrMsg;
    this.mErrors.addOneError(cError);
    System.out.println("程序报错：" + cError.errorMessage);
  }

  /**
   * getResult
   *
   * @return VData
   */
  public VData getResult() {
    return this.mResult;
  }

}
