package com.sinosoft.lis.cbcheck;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 核保记事本</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 1.0
 */
import java.util.*;

import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;



public class UWExplainBL {
    /** 传入参数 */
    private VData mInputData;

    /** 传入操作符 */
    private String mOperate;

    /** 登陆信息 */
    private GlobalInput mGlobalInput;

    /** 报错存储对象 */
    public CErrors mErrors = new CErrors();

    /** 最后保存结果 */
    private VData mResult = new VData();

    /** 最后递交Map */
    private MMap map = new MMap();

    /** 传入的业务数据 */
    private LCNotePadSchema mLCNotePadSchema = new LCNotePadSchema();

    /**
     * submitData
     *
     * @param nInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData nInputData, String cOperate) {
        System.out.println("into BriefCardSignBL...");
        this.mInputData = nInputData;
        this.mOperate = cOperate;

        if (!getInputData()) {
            return false;
        }
        if (!checkData()) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
        if (!prepareOutputData()) {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(this.mResult, "INSERT")) {
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        System.out.println("BriefCardSignBL finished...");
        return true;
    }

    /**
     * getInputData
     *
     * @return boolean
     */
    private boolean getInputData() {
        System.out.println("into BriefCardSignBL.getInputData()...");
        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mLCNotePadSchema = (LCNotePadSchema) mInputData.
                           getObjectByObjectName(
                                   "LCNotePadSchema", 0);

        return true;
    }

    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData() {
        System.out.println("into BriefCardSignBL.checkData()...");
        if (this.mGlobalInput == null) {
            String str = "登陆信息为null，可能是页面超时，请重新登陆!";
            buildError("checkData", str);
            return false;
        }
        return true;
    }

    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData() {
        System.out.println("into BriefCardSignBL.dealData()...");
        if (mOperate.equals("INSERT")) {

            LCContDB tLCContDB = new LCContDB();
            if (mLCNotePadSchema.getContNo() == null ||
                mLCNotePadSchema.getContNo().equals("")) {
                String str = "合同号为空!";
                buildError("dealData", str);
                System.out.println("在程序UWExplainBL.dealData() - 152 : " +
                                   str);
                return false;
            }
            tLCContDB.setContNo(mLCNotePadSchema.getContNo());
            if (!tLCContDB.getInfo()) {
                String str = "查询合同信息失败!";
                buildError("dealData", str);
                System.out.println("在程序UWExplainBL.dealData() - 132 : " + str);
                return false;
            }
            mLCNotePadSchema.setGrpContNo(tLCContDB.getGrpContNo());
            mLCNotePadSchema.setProposalContNo(tLCContDB.getProposalContNo());
            mLCNotePadSchema.setOperatePos("3");
            String tSerialNo = PubFun1.CreateMaxNo("LCNotePad", 20);
            mLCNotePadSchema.setSerialNo(tSerialNo);
            mLCNotePadSchema.setOperator(mGlobalInput.Operator);
            mLCNotePadSchema.setMakeDate(PubFun.getCurrentDate());
            mLCNotePadSchema.setMakeTime(PubFun.getCurrentTime());
            mLCNotePadSchema.setModifyDate(PubFun.getCurrentDate());
            mLCNotePadSchema.setModifyTime(PubFun.getCurrentTime());
            map.put(mLCNotePadSchema, "INSERT");
        }

        return true;
    }

    /**
     * prepareOutputData
     *
     * @return boolean
     */
    private boolean prepareOutputData() {
        System.out.println("into BriefCardSignBL.prepareOutputData()...");
        this.mResult.add(map);
        return true;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "BriefCardSignBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }


}
