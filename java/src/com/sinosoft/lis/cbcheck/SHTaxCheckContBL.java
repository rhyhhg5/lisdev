package com.sinosoft.lis.cbcheck;

import com.cbsws.obj.SHRspSaleInfo;
import com.sinosoft.httpclient.inf.SHGetPlatformCustomerNo;
import com.sinosoft.httpclient.inf.SHGetTaxVerificationCode;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCContSubDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class SHTaxCheckContBL {

	/** 传入数据的容器 */
	private VData mInputData = new VData();

	private TransferData mTransferData = new TransferData();

	private GlobalInput mGlobalInput = new GlobalInput();

	private MMap map = new MMap();
	
	/** 往前面传输数据的容器 */
    private String mResult = "";

	private String mContNo = "";

	private String mPrtNo = "";
//	private String mPrtNo = "16150928601";  //测试用

	private ExeSQL mExeSQL = new ExeSQL();

	private LCContSubDB mLCContSubDB = null;

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	public SHRspSaleInfo rspSaleInfo = new SHRspSaleInfo();// 该类中的属性都是调用第三方接口返回来的

	public boolean submitData(VData cInputData, String cOperate) {

		// 数据过滤器
		if (!getInputData(cInputData, cOperate)) {
			return false;
		}

		// 进行业务处理
		if (!dealData()) {
			return false;
		}
		
//		this.prepareOutputData();
//
//		PubSubmit tPubSubmit = new PubSubmit();
//
//		if (!tPubSubmit.submitData(mInputData, cOperate)) {
//			// @@错误处理
//			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
//
//			CError tError = new CError();
//			tError.moduleName = "ContBL";
//			tError.functionName = "submitData";
//			tError.errorMessage = "数据提交失败!";
//
//			this.mErrors.addOneError(tError);
//			return false;
//		}

		return true;
	}

	private boolean dealData() {

		LCContDB tLCContDB = new LCContDB();
		tLCContDB.setContNo(mContNo);
		if (!tLCContDB.getInfo()) {
			CError tError = new CError();
			tError.moduleName = "TaxCheckContBL";
			tError.functionName = "dealData";
			tError.errorMessage = "保单信息查询失败！";
			this.mErrors.addOneError(tError);
			return false;
		}

		SSRS tSSRS = new SSRS();
		String tsql = "select 1 from lcpol where contno='"+ mContNo + "' "
		            + " and exists (select 1 from lmriskapp where riskcode=lcpol.riskcode and TaxOptimal='Y') ";
		tSSRS = mExeSQL.execSQL(tsql);
		if (tSSRS.getMaxRow() == 0) {
			CError tError = new CError();
			tError.moduleName = "TaxCheckContBL";
			tError.functionName = "dealData";
			tError.errorMessage = "不为税优保单，不需要税优唯一验证！";
			this.mErrors.addOneError(tError);
			return false;
		}

		mLCContSubDB = new LCContSubDB();
		mLCContSubDB.setPrtNo(mPrtNo);
		if (!mLCContSubDB.getInfo()) {
			CError tError = new CError();
			tError.moduleName = "TaxCheckContBL";
			tError.functionName = "dealData";
			tError.errorMessage = "保单税优信息失败，请检查是否已录入！";
			this.mErrors.addOneError(tError);
			return false;

		}

		String tCustomerNo= tLCContDB.getAppntNo();
		tsql = " select ld.customerno,ld.PlatformCustomerNo from ldperson ld where ld.customerno='"+ tCustomerNo + "' ";
		tSSRS = mExeSQL.execSQL(tsql);
		if (tSSRS.getMaxRow() > 0){
			String tplatno = tSSRS.GetText(1, 2);
			if("".equals(tplatno)||null==tplatno){
				getSYCustomerNo(tCustomerNo);
			}
		}else{
			CError tError = new CError();
			tError.moduleName = "SHTaxCheckContBL";
			tError.functionName = "dealData";
			tError.errorMessage = "投保人信息查询失败！";
			this.mErrors.addOneError(tError);
			return false;
		}
		
		if (!checkContFlag()) {
			return false;
		}
		
		return true;
	}

	// 获取客户平台号,并存储
	private boolean getSYCustomerNo(String cCustomerNo) {
		TransferData mTransferData = new TransferData();
		mTransferData.setNameAndValue("CustomerNo", cCustomerNo);
		VData tVData = new VData();
		tVData.add(mTransferData);
		SHGetPlatformCustomerNo tGetPlatformCustomerNo = new SHGetPlatformCustomerNo();
		if(!tGetPlatformCustomerNo.submitData(tVData,"ZBXPT")){
			System.out.println("获取平台客户号失败！");
		}
		return true;
	}

	// 团体客户、个人客户获取完税优码后校验是否已购买保单，用于返回信息
	private boolean checkContFlag() {
		VData tVData = new VData();
		TransferData mTransferData = new TransferData();
		String tSql1 = "select appntname,appntsex,appntbirthday,idtype,idno,case nativeplace when 'OS' then nativecity when 'HK' then nativecity else 'ML' end  from lcappnt where prtno='"+mPrtNo+"'";
		SSRS tSSRS1 = new SSRS();
		tSSRS1 = new ExeSQL().execSQL(tSql1);
		if(tSSRS1!=null&&tSSRS1.getMaxRow()>0){
			mTransferData.setNameAndValue("Name", tSSRS1.GetText(1, 1));
			mTransferData.setNameAndValue("Gender", tSSRS1.GetText(1, 2));
			mTransferData.setNameAndValue("Birthday", tSSRS1.GetText(1, 3));
			mTransferData.setNameAndValue("CertiType", tSSRS1.GetText(1, 4));
			mTransferData.setNameAndValue("CertiNo", tSSRS1.GetText(1, 5));
			mTransferData.setNameAndValue("Nationality", tSSRS1.GetText(1, 6));
		}
		mTransferData.setNameAndValue("ProposalNo", mPrtNo);
		String tPolicySource = new ExeSQL().getOneValue("select transflag from lccontsub where prtno='"+mPrtNo+"'");
		String tCoverageEffectiveData = new ExeSQL().getOneValue("select distinct cvalidate from lccont where prtno='"+mPrtNo+"'");
		mTransferData.setNameAndValue("PolicySource", tPolicySource);
		mTransferData.setNameAndValue("CoverageEffectiveDate", tCoverageEffectiveData);
		tVData.add(mTransferData);
		SHGetTaxVerificationCode tGetTaxVerificationCode = new SHGetTaxVerificationCode();
		if(!tGetTaxVerificationCode.submitData(tVData,"ZBXPT")){
			System.out.println("客户验证失败！");
			CError tError = new CError();
			tError = tGetTaxVerificationCode.mErrors.getError(0);
			this.mErrors.addOneError(tError);
			return false;
			
		}
		mResult = tGetTaxVerificationCode.getResult();
		return true;
	}

//	private void prepareOutputData() {
//		mInputData.clear();
//		mInputData.add(map);
//
//	}

	private boolean getInputData(VData cInputData, String cOperate) {
		// 从VData容器中取出对象TransferData
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0));
		mTransferData = (TransferData) cInputData.getObjectByObjectName(
				"TransferData", 0);

		// 分别对TransferData中数据进行过滤
		mContNo = (String) mTransferData.getValueByName("ContNo");
		if (mContNo == null || "".equals(mContNo)) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "TaxCheckContBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "保单号获取失败！";
			this.mErrors.addOneError(tError);
			return false;
		}

		mPrtNo = (String) mTransferData.getValueByName("PrtNo");
		if (mPrtNo == null || "".equals(mPrtNo)) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "TaxCheckContBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "印刷号获取失败！";
			this.mErrors.addOneError(tError);
			return false;
		}

		return true;
	}
	
	/**
     * 得到处理后的结果集
     * @return 结果集
     */

    public String getResult()
    {
        return mResult;
    }
    
    public static void main(String[] args) {
    	SHTaxCheckContBL tTest = new SHTaxCheckContBL();
    	tTest.checkContFlag();
	}
}


