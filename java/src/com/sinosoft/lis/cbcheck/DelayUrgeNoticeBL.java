/*
 * <p>ClassName: DelayUrgeNoticeBL </p>
 * <p>Description: DelayUrgeNoticeBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-03-20 18:03:36
 */
package com.sinosoft.lis.cbcheck;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.cbcheck.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.sys.*;

public class DelayUrgeNoticeBL
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors = new CErrors();
  private VData mResult = new VData();


  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();


  /** 全局数据 */
  private GlobalInput mGlobalInput = new GlobalInput();


  /** 数据操作字符串 */
  private String mOperate;


  /** 业务处理相关变量 */
  private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
  private LOPRTManagerSet mLOPRTManagerSet = new LOPRTManagerSet();
  private LOPRTManagerSet mNewLOPRTManagerSet = new LOPRTManagerSet();

  private MMap map = new MMap();


  /** 数据操作字符串 */
  private String mOperator;
  private String mManageCom;


  public DelayUrgeNoticeBL()
  {
  }

  public static void main(String[] args)
  {
  }


  /**
   * 传输数据的公共方法
   * @param: cInputData 输入的数据
   *         cOperate 数据操作
   * @return:
   */
  public boolean submitData(VData cInputData, String cOperate)
  {
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
      return false;

    //校验数据
    if (!checkData())
      return false;

    System.out.println("Start  dealData...");

    //进行业务处理
    if (!dealData())
      return false;

    System.out.println("dealData successful!");

    //准备往后台的数据
    if (!prepareOutputData())
      return false;

    System.out.println("Start  Submit...");

    //提交数据
    if (cOperate.equals("submit"))
    {
      PubSubmit tSubmit = new PubSubmit();
      if (!tSubmit.submitData(mResult, ""))
      {
        // @@错误处理
        this.mErrors.copyAllErrors(tSubmit.mErrors);
        CError tError = new CError();
        tError.moduleName = "LCInsuredUWBL";
        tError.functionName = "submitData";
        tError.errorMessage = "数据提交失败!";
        this.mErrors.addOneError(tError);
        return false;
      }
    }

    return true;
  }


  /**
   * 根据前面的输入数据，进行BL逻辑处理
   * 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean dealData()
  {
    LOPRTManagerDB tLOPRTManagerDB;
    LOPRTManagerSchema tLOPRTManagerSchema;
    for (int i = 1; i <= mLOPRTManagerSet.size(); i++)
    {
      tLOPRTManagerDB = new LOPRTManagerDB();
      tLOPRTManagerSchema = new LOPRTManagerSchema();
      tLOPRTManagerDB.setPrtSeq(mLOPRTManagerSet.get(i).getPrtSeq());
      if (!tLOPRTManagerDB.getInfo())
      {
        CError tError = new CError();
        tError.moduleName = "LCInsuredBL";
        tError.functionName = "checkData";
        tError.errorMessage = "查询打印管理表失败!";
        this.mErrors.addOneError(tError);
        return false;
      }
      tLOPRTManagerSchema = tLOPRTManagerDB.getSchema();
      tLOPRTManagerSchema.setForMakeDate(mLOPRTManagerSet.get(i).getForMakeDate());

      mNewLOPRTManagerSet.add(tLOPRTManagerSchema);
    }

    return true;
  }


  /**
   * 校验业务数据
   * @return
   */
  private boolean checkData()
  {
    return true;
  }


  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {

    this.mLOPRTManagerSet.set((LOPRTManagerSet) cInputData.
                              getObjectByObjectName("LOPRTManagerSet", 0));
    this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
        "GlobalInput", 0));
    mInputData = cInputData;
    if (mGlobalInput == null)
    {
      // @@错误处理
      //this.mErrors.copyAllErrors( tLCContDB.mErrors );
      CError tError = new CError();
      tError.moduleName = "LCInsuredBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "前台传输全局公共数据失败!";
      this.mErrors.addOneError(tError);
      return false;
    }

    //获得操作员编码
    mOperator = mGlobalInput.Operator;
    if (mOperator == null || mOperator.trim().equals(""))
    {
      // @@错误处理
      //this.mErrors.copyAllErrors( tLCContDB.mErrors );
      CError tError = new CError();
      tError.moduleName = "LCInsuredBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "前台传输全局公共数据mOperator失败!";
      this.mErrors.addOneError(tError);
      return false;
    }

    //获得管理机构编码
    mManageCom = mGlobalInput.ManageCom;
    if (mManageCom == null || mManageCom.trim().equals(""))
    {
      // @@错误处理
      //this.mErrors.copyAllErrors( tLCContDB.mErrors );
      CError tError = new CError();
      tError.moduleName = "LCInsuredBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "前台传输全局公共数据mOperator失败!";
      this.mErrors.addOneError(tError);
      return false;
    }

    return true;

  }


  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */

  private boolean prepareOutputData()
  {
    mResult.clear();

    map.put(mNewLOPRTManagerSet,"UPDATE");

    mResult.add(map);

    return true;
  }

  public VData getResult()
  {
    return this.mResult;
  }
}
