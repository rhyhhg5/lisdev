/*
 * <p>ClassName: OLDHospitalBL </p>
 * <p>Description: OLDHospitalBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-01-15 14:25:18
 */
package com.sinosoft.lis.cbcheck;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.LARateChargeSchema;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class AgentcyBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    private MMap map = new MMap();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;

    /** 业务处理相关变量 */
    private LARateChargeSchema mLARateChargeSchema = new LARateChargeSchema();

    //private LDHospitalSet mLDHospitalSet=new LDHospitalSet();
    public AgentcyBL()
    {
    }

    public static void main(String[] args)
    {
        LARateChargeSchema tLARateChargeSchema = new LARateChargeSchema();
        AgentcyUI tAgentcyUI = new AgentcyUI();
        GlobalInput tG = new GlobalInput();
        tG.Operator = "001";
        tG.ManageCom = "86";
        tG.ComCode = "86";
        tLARateChargeSchema.setOtherNo("20050002");
        tLARateChargeSchema.setOtherNoType("2");
        tLARateChargeSchema.setCalType("51");
        tLARateChargeSchema.setRate("100");
        tLARateChargeSchema.setRiskCode("0000");
        VData tVData = new VData();
        tVData.add(tLARateChargeSchema);
        tVData.add(tG);

        tAgentcyUI.submitData(tVData, "INSERT||AGENTCY");
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        this.mInputData = cInputData;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDHospitalBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LDHospitalBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
            return false;
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            System.out.println("Start LDHospitalBL Submit...");
            PubSubmit tPubSubmit = new PubSubmit();
            System.out.println("Start LDHospitalBL Submit...");

            if (!tPubSubmit.submitData(mInputData, mOperate))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);

                CError tError = new CError();
                tError.moduleName = "OLHCustomHealthStatusBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";

                this.mErrors.addOneError(tError);
                return false;
            }

        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        if (this.mOperate.equals("INSERT||AGENTCY"))
        {
            if (mLARateChargeSchema.getEndDate() == null
                    || "".equals(mLARateChargeSchema.getEndDate()))
                mLARateChargeSchema.setEndDate(PubFun.getCurrentDate());

            if (mLARateChargeSchema.getStartDate() == null
                    || "".equals(mLARateChargeSchema.getStartDate()))
                mLARateChargeSchema.setStartDate(PubFun.getCurrentDate());

            mLARateChargeSchema.setEndYear(0);
            mLARateChargeSchema.setStartYear(0);
            mLARateChargeSchema.setOperator(mGlobalInput.Operator);
            mLARateChargeSchema.setMakeDate(PubFun.getCurrentDate());
            mLARateChargeSchema.setMakeTime(PubFun.getCurrentTime());
            mLARateChargeSchema.setModifyDate(PubFun.getCurrentDate());
            mLARateChargeSchema.setModifyTime(PubFun.getCurrentTime());
            map.put(mLARateChargeSchema, "DELETE&INSERT");
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean updateData()
    {
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLARateChargeSchema.setSchema((LARateChargeSchema) cInputData
                .getObjectByObjectName("LARateChargeSchema", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData
                .getObjectByObjectName("GlobalInput", 0));
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        LARateChargeDB tLARateChargeDB = new LARateChargeDB();
        tLARateChargeDB.setSchema(this.mLARateChargeSchema);
        //如果有需要处理的错误，则返回
        if (tLARateChargeDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLARateChargeDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LARateChargeBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData.clear();
            this.mInputData.add(this.mLARateChargeSchema);
            this.mInputData.add(map);
            mResult.clear();
            mResult.add(this.mLARateChargeSchema);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LARateChargeBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }
}
