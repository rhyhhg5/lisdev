/*
 * <p>ClassName: OLCPENoticeItemUI </p>
 * <p>Description: OLCPENoticeItemUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-01-15 13:07:18
 */
package com.sinosoft.lis.cbcheck;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.pubfun.*;
public class LCPENoticeItemUI {
/** 错误处理类，每个需要错误处理的类中都放置该类 */
public  CErrors mErrors=new CErrors();
private VData mResult = new VData();
/** 往后面传输数据的容器 */
private VData mInputData =new VData();
/** 数据操作字符串 */
private String mOperate;
//业务处理相关变量
 /** 全局数据 */
private LCPENoticeItemSchema mLCPENoticeItemSchema=new LCPENoticeItemSchema();
private LCPENoticeItemSet mLCPENoticeItemSet=new LCPENoticeItemSet();

public LCPENoticeItemUI ()
{
}
 /**
传输数据的公共方法
*/
public boolean submitData(VData cInputData,String cOperate)
{
  //将操作数据拷贝到本类中
  this.mOperate =cOperate;
  this.mInputData = cInputData;
  System.out.println("sdsjfjs;fljsjsdfjsfjs;fj  " +mInputData.size());
  //得到外部传入的数据,将数据备份到本类中
//  if (!getInputData(cInputData))
//    return false;
//
//  //进行业务处理
//  if (!dealData())
//    return false;
//
//  //准备往后台的数据
//  if (!prepareOutputData())
//    return false;
System.out.println(mInputData);
  mLCPENoticeItemSet.set((LCPENoticeItemSet) mInputData.getObjectByObjectName(
          "LCPENoticeItemSet", 0));
     System.out.println("a;sjdfajajf;ajj : "+ mLCPENoticeItemSet.encode());
  LCPENoticeItemBL tLCPENoticeItemBL = new LCPENoticeItemBL();

  //System.out.println("Start OLCPENoticeItem UI Submit...");
  tLCPENoticeItemBL.submitData(mInputData,mOperate);
  //System.out.println("End OLCPENoticeItem UI Submit...");
  //如果有需要处理的错误，则返回
  if (tLCPENoticeItemBL.mErrors .needDealError() )
  {
     // @@错误处理
     this.mErrors.copyAllErrors(tLCPENoticeItemBL.mErrors);
     CError tError = new CError();
     tError.moduleName = "LCPENoticeItemUI";
     tError.functionName = "submitData";
     tError.errorMessage = "数据提交失败!";
     this.mErrors .addOneError(tError) ;
     return false;
  }
  if (mOperate.equals("INSERT||MAIN"))
  {
     this.mResult.clear();
     this.mResult=tLCPENoticeItemBL.getResult();
  }
  mInputData=null;
  return true;
  }
  public static void main(String[] args)
  {
//      LCPENoticeItemSchema tLCPENoticeItemSchema   = new LCPENoticeItemSchema();
//      LCPENoticeItemUI tLCPENoticeItemUI   = new LCPENoticeItemUI();
//          //输出参数
//          CErrors tError = null;
//          String tRela  = "";
//          String FlagStr = "";
//          String Content = "";
//          String transact = "INSERT||MAIN";
//
//          String tG="GI";
//
//          //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
//          transact = "fmtransact";
//    tLCPENoticeItemSchema.setSIOrganCode("DocHM随风倒光荣感第三国如果如果让公司热狗UploadContSIOrganCode");
//    tLCPENoticeItemSchema.setSuperSIOrganCode("DocHMUploadContSuperSIOrganCode");
//    tLCPENoticeItemSchema.setSIOrganName("DocHMUploadContSIOrganName");
//    tLCPENoticeItemSchema.setAreaCode("DocHMUploadContAreaCode");
//    tLCPENoticeItemSchema.setAddress("Address");
//    tLCPENoticeItemSchema.setZipCode("ZipCode");
//    tLCPENoticeItemSchema.setPhone("Phone");
//    tLCPENoticeItemSchema.setWebAddress("WebAddress");
//    tLCPENoticeItemSchema.setFax("Fax");
//    tLCPENoticeItemSchema.setbankCode("bankCode");
//    tLCPENoticeItemSchema.setAccName("AccName");
//    tLCPENoticeItemSchema.setbankAccNO("bankAccNO");
//    tLCPENoticeItemSchema.setSatrapName("SatrapName");
//    tLCPENoticeItemSchema.setLinkman("Linkman");
//    tLCPENoticeItemSchema.setLastModiDate("LastModiDate");
//
//  // 准备传输数据 VData
//        VData tVData = new VData();
//        tVData.add(tLCPENoticeItemSchema);
//        tVData.add(tG);
//    tLCPENoticeItemUI.submitData(tVData,"INSERT||MAIN");

  }
  /**
  * 准备往后层输出所需要的数据
  * 输出：如果准备数据时发生错误则返回false,否则返回true
  */
 private boolean prepareOutputData()
 {
    try
    {
      mInputData.clear();
      mInputData.add(this.mLCPENoticeItemSchema);
    }
    catch(Exception ex)
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="LCPENoticeItemUI";
      tError.functionName="prepareData";
      tError.errorMessage="在准备往后层处理所需要的数据时出错。";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
}
/**
 * 根据前面的输入数据，进行UI逻辑处理
  * 如果在处理过程中出错，则返回false,否则返回true
  */
 private boolean dealData()
 {
      boolean tReturn =false;
      //此处增加一些校验代码
      tReturn=true;
      return tReturn ;
}
 /**
 * 从输入数据中得到所有对象
 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 */
private boolean getInputData(VData cInputData)
{
  //全局变量
  this.mLCPENoticeItemSchema.setSchema((LCPENoticeItemSchema)cInputData.getObjectByObjectName("LCPENoticeItemSchema",0));
  return true;
}
public VData getResult()
{
  return this.mResult;
}
}
