package com.sinosoft.lis.cbcheck;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import java.util.*;

import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.certify.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.cbcheck.UWHealthCancelBL;
import com.sinosoft.lis.cbcheck.UWHealthCancelUI;



/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 契约模块</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author
 * @version 1.0
 */
public class UWHealthCancelUI {
    /** 传入数据的容器 */
    private VData mInputData = new VData();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    public UWHealthCancelUI() {
    }

    /**
     * 不执行任何操作，只传递数据给下一层
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        // 数据操作字符串拷贝到本类中
        this.mOperate = cOperate;

        UWHealthCancelBL tUWHealthCancelBL = new UWHealthCancelBL();

        if (tUWHealthCancelBL.submitData(cInputData, mOperate) == false) {
            // @@错误处理
            this.mErrors.copyAllErrors(tUWHealthCancelBL.mErrors);
            return false;
        }
//                else
//                {
//                        mResult = tUWHealthCancelBL.getResult();
//                }

        return true;
    }

    /**
     * 获取从BL层取得的结果
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }

    public static void main(String[] agrs) {
        LOPRTManagerSchema tLOPrtManagerSchema = new LOPRTManagerSchema(); //集体保单
        GlobalInput mGlobalInput = new GlobalInput();
        mGlobalInput.ManageCom = "86";
        mGlobalInput.Operator = "group";

        tLOPrtManagerSchema.setPrtSeq("44000002508");
        tLOPrtManagerSchema.setManageCom("86110000");
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("Prtseq", "44000002508");

        VData tVData = new VData();
        tVData.add(tLOPrtManagerSchema);
        tVData.add(mGlobalInput);
        tVData.add(tTransferData);

        UWHealthCancelUI tgrlbl = new UWHealthCancelUI();
        tgrlbl.submitData(tVData, "DELETE");

        if (tgrlbl.mErrors.needDealError()) {
            System.out.println(tgrlbl.mErrors.getFirstError());
        }
    }
}
