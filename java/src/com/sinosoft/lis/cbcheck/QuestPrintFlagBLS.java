package com.sinosoft.lis.cbcheck;

import java.sql.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统问题件打印标记设置部分 </p>
 * <p>Description: 数据库功能类</p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft< /p>
 * @author WHN
 * @version 1.0
 */

public class QuestPrintFlagBLS
{
  //是否存在需要人工核保保单
  int merrno = 0;
  //传输数据类
  private VData mInputData ;
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors = new CErrors();

  //
  private LCUWErrorSet mLCUWErrorSet = new LCUWErrorSet();

  public QuestPrintFlagBLS() {}

  public static void main(String[] args)
  {
  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
	//首先将数据在本类中做一个备份
	mInputData=(VData)cInputData.clone() ;

	System.out.println("Start QuestPrintFlagBLS Submit...");
	if (!this.saveData())
   return false;
 System.out.println("End QuestPrintFlagBLS Submit...");

 mInputData=null;
 return true;
  }

  private boolean saveData()
  {

	LCIssuePolSet mLCIssuePolSet = (LCIssuePolSet)mInputData.getObjectByObjectName("LCIssuePolSet",0);
	LCUWMasterSet mLCUWMasterSet = (LCUWMasterSet)mInputData.getObjectByObjectName("LCUWMasterSet",0);
	Connection conn = DBConnPool.getConnection();
	//conn = .getDefaultConnection();

	if (conn==null)
	{
	  // @@错误处理
	  CError tError = new CError();
	  tError.moduleName = "QuestPrintFlagBLS";
	  tError.functionName = "saveData";
	  tError.errorMessage = "数据库连接失败!";
	  this.mErrors .addOneError(tError) ;
	  return false;
	}

	try
	{
	  conn.setAutoCommit(false);

	  // 删除部分
	  int polCount = mLCIssuePolSet.size();
	  if (polCount > 0)
	  {
		for(int i = 1;i<=polCount;i++)
		{
		  LCIssuePolSchema tLCIssuePolSchema = new LCIssuePolSchema();
		  tLCIssuePolSchema = ( LCIssuePolSchema )mLCIssuePolSet.get(i);
//		  String tProposalNo = tLCIssuePolSchema.getProposalNo();

		  // 核保主表
		  LCIssuePolDB tLCIssuePolDB = new LCIssuePolDB( conn );
//		  tLCIssuePolDB.setProposalNo(tLCIssuePolSchema.getProposalNo());
		  tLCIssuePolDB.setIssueType(tLCIssuePolSchema.getIssueType());
		  tLCIssuePolDB.setOperatePos(tLCIssuePolSchema.getOperatePos());
		  if (tLCIssuePolDB.deleteSQL() == false)
		  {
			// @@错误处理
			this.mErrors.copyAllErrors(tLCIssuePolDB.mErrors);
			CError tError = new CError();
			tError.moduleName = "QuestPrintFlagBLS";
			tError.functionName = "saveData";
			tError.errorMessage = "LCIssuePol表删除失败!";
			this.mErrors .addOneError(tError) ;
			conn.rollback() ;
			conn.close();
			return false;
		  }

		  // 保存部分
		  System.out.println("-----------DDD--------------");
		  tLCIssuePolDB.setSchema(tLCIssuePolSchema);

		  if(tLCIssuePolDB.insert() == false)
		  {
			// @@错误处理
			this.mErrors.copyAllErrors(tLCIssuePolDB.mErrors);
			CError tError = new CError();
			tError.moduleName = "QuestPrintFlagBLS";
			tError.functionName = "saveData";
			tError.errorMessage = "LCIssuePol表保存失败!";
			this.mErrors .addOneError(tError) ;
			conn.rollback() ;
			conn.close();
			return false;
		  }

		}// end of for

		if(mLCUWMasterSet.size() > 0)
		{
		  for(int i = 1;i <= mLCUWMasterSet.size();i++)
		  {
			LCUWMasterSchema tLCUWMasterSchema = new LCUWMasterSchema();
			LCUWMasterDB tLCUWMasterDB = new LCUWMasterDB(conn);

			tLCUWMasterSchema = mLCUWMasterSet.get(i);
			tLCUWMasterDB.setSchema(tLCUWMasterSchema);
			if(!tLCUWMasterDB.update())
			{
			  this.mErrors.copyAllErrors(tLCUWMasterDB.mErrors);
			  CError tError = new CError();
			  tError.moduleName = "QuestPrintFlagBLS";
			  tError.functionName = "saveData";
			  tError.errorMessage = "UWMaster表保存失败!";
			  this.mErrors .addOneError(tError) ;
			  conn.rollback() ;
			  conn.close();
			  return false;
			}
		  }
		}
		//修改问题件打印标志不会影响核保通知书的QuestionFlag.因为在前台控制如果已发核保通知书后,未打印状态下，不再容许修改问题件标志。2004-02-25 sxy


	  } //end of if

	  conn.commit() ;
	  conn.close();
	} // end of try

	catch (Exception ex)
	{
	  // @@错误处理
	  CError tError = new CError();
	  tError.moduleName = "QuestPrintFlagBLS";
	  tError.functionName = "submitData";
	  tError.errorMessage = ex.toString();
	  this.mErrors .addOneError(tError);
	  try{conn.rollback() ;} catch(Exception e){}
	  return false;
	}
	return true;
  }
}
