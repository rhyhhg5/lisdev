package com.sinosoft.lis.cbcheck;

import java.util.*;

import utils.system;

import com.ibm.db2.jcc.a.bo;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.f1print.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统个人单人工核保送打印队列部分</p>
 * <p>Description: 逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author WHN
 * @version 1.0
 */
public class UWSendPrintBLS
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    private String mManageCom;

    /** 业务处理相关变量 */

    private String mContNo = "";

    private String mOldPolNo = "";

    private GlobalInput mGlobalInput = new GlobalInput();

    /**打印队列表**/
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

    private LOPRTManagerSet mLOPRTManagerSet = new LOPRTManagerSet();

    /**打印处理函数**/
    private PrintManagerBL tPrintManagerBL = new PrintManagerBL();

    private LMUWSet mLMUWSet = new LMUWSet();

    /**标记自核通过*/
    private String AutoUWFlag = "";
    private String UWFlag = "";

    /** 是否打印通知书标志。 */
    private String mNoPrtFlag = null;

    public UWSendPrintBLS()
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        int flag = 0; //判断是不是所有数据都不成功
        int j = 0; //符合条件数据个数

        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();

        System.out.println("---1---");
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        System.out.println("---UWSendPrintBLS getInputData---");

        mOldPolNo = mLOPRTManagerSchema.getOtherNo();
        mContNo = mLOPRTManagerSchema.getOtherNo();
        //校验
        if (!checkUW())
        {
            return false;
        }
        //校验
        if (!checkPrint())
        {
            return false;
        }
        System.out.println("---UWSendPrintBLS checkData---");
        // 数据操作业务处理
        if (!dealData())
        {
            return false;
        }

        System.out.println("---UWSendPrintBLS dealData---");

        System.out.println("---UWSendPrintBLS prepareOutputData---");

        for (int i = 1; i <= mLOPRTManagerSet.size(); i++)
        {
            LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
            tLOPRTManagerSchema = mLOPRTManagerSet.get(i);

            mInputData.clear();
            mInputData.add(mGlobalInput);
            mInputData.add(tLOPRTManagerSchema);
            System.out.println("tLOPRTManagerSchema : " + tLOPRTManagerSchema);
            if (!tPrintManagerBL.submitData(mInputData, "REQUEST"))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tPrintManagerBL.mErrors);
                CError tError = new CError();
                tError.moduleName = "UWSendPrintBLS";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }

        System.out.println("---UWSendPrintBLS commitData---");
        return true;
    }

	private boolean checkUW() {
		UWFlag = new ExeSQL().getOneValue("select 1 from lwmission lw where missionprop2='"
					+ mContNo+ "' and activityid='0000001104' and exists (select 1 from lwmission lwm where lwm.missionid=lw.missionid and activityid='0000001110')  union all    select 1 from lwmission lw where missionprop2='"
					+ mContNo+ "' and activityid='0000001200'");
		System.out.println("UWFlag================"+UWFlag);
		return true;
	}

	/**
     * 数据操作类业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean dealData()
    {

        if (dealOnePol() == false)
        {
            return false;
        }

        return true;
    }

    /**
     * 操作一张保单的业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean dealOnePol()
    {
        // 健康信息
        if (preparePrint() == false)
        {
            return false;
        }

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mOperate = mGlobalInput.Operator;
        mManageCom = mGlobalInput.ManageCom;
        if (mGlobalInput == null)
        {
            buildError("getInputData", "传入的操作信息为空！");
            return false;
        }
        mLOPRTManagerSchema.setSchema((LOPRTManagerSchema) cInputData
                .getObjectByObjectName("LOPRTManagerSchema", 0));
        TransferData tTransferData = new TransferData();
        tTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        if (tTransferData != null)
        {
            if (!StrTool.cTrim(
                    (String) tTransferData.getValueByName("AutoUWFlag"))
                    .equals(""))
            {
                this.AutoUWFlag = (String) tTransferData
                        .getValueByName("AutoUWFlag");
            }

            mNoPrtFlag = (String) tTransferData.getValueByName("NoPrtFlag");
        }
        if (mLOPRTManagerSchema == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "UWSendPrintBLS";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有传入数据!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 校验是否打印
     * @return
     */
    private boolean checkPrint()
    {
        //生成流水号
        String tsql = "select distinct 1 from LOPRTManager where otherno = '"
                + mContNo
                + "' and code = '"
                + mLOPRTManagerSchema.getCode().trim()
                + "' and stateflag = '0' and prtseq = (select max(prtseq) from LOPRTManager where otherno = '"
                + mContNo + "' and code = '"
                + mLOPRTManagerSchema.getCode().trim() + "')";
        System.out.println(tsql);

        ExeSQL tExeSQL = new ExeSQL();
        String tflag = tExeSQL.getOneValue(tsql);

        if (tflag.trim().equals("1"))
        {
            CError tError = new CError();
            tError.moduleName = "UWSendPrintBLS";
            tError.functionName = "checkPrint";
            tError.errorMessage = "已在打印队列尚未打印!";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (mLOPRTManagerSchema.getCode().equals("15"))
        {
            tsql = "select distinct 1 from LOPRTManager where otherno = '"
                    + mContNo + "' and code = '"
                    + mLOPRTManagerSchema.getCode().trim() + "'";
            System.out.println(tsql);

            tExeSQL = new ExeSQL();
            tflag = tExeSQL.getOneValue(tsql);
            System.out.println("tflag" + tflag);
            if (tflag.trim().equals("1"))
            {
                System.out.println("tflag" + tflag);
                CError tError = new CError();
                tError.moduleName = "UWSendPrintBLS";
                tError.functionName = "checkPrint";
                tError.errorMessage = "在签单处已发缴费催办通知书!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        return true;
    }

    /**
     * 校验是不是主险
     * @return
     */
    private boolean checkMain15()
    {
        //计算已交保费
        ExeSQL tExeSQL = new ExeSQL();
        double tTemperFee = 0.0;
        String tRealPayMoney = "";
        String tStrSQL = "select sum(PayMoney) from ("
                + "select sum(PayMoney) PayMoney from LJTempFee where "
                + "TempFeeType in ('0','6','1') and otherno in (select contno from lccont where ContNo='"
                + mLOPRTManagerSchema.getOtherNo()
                + "') and EnterAccDate is not null"
                + " union "
                + "select sum(PayMoney) PayMoney from LJTempFee where "
                + "TempFeeType in ('1','5') and otherno in (select prtno from lCGrpcont where GrpContNo='"
                + mLOPRTManagerSchema.getOtherNo()
                + "') and EnterAccDate is not null" + ") as a";

        tRealPayMoney = tExeSQL.getOneValue(tStrSQL);

        System.out.println("实际交的金额:" + tRealPayMoney);
        if (tRealPayMoney == null || tRealPayMoney.trim().equals(""))
        {
            tTemperFee = 0.0;
        }
        else
        {
            tTemperFee = Double.parseDouble(tRealPayMoney);
        }

        System.out.println("实际交的金额:" + tTemperFee);
        //计算应交保费
        double tPrem = 0.0;
        String tShouldPayMoney = "";
        tStrSQL = "select prem from lccont where ContNo='"
                + mLOPRTManagerSchema.getOtherNo() + "'";

        tShouldPayMoney = tExeSQL.getOneValue(tStrSQL);

        System.out.println("应该交的金额:" + tShouldPayMoney);

        if (tShouldPayMoney == null || tShouldPayMoney.trim().equals(""))
        {
            tPrem = 0.0;
        }
        else
        {
            tPrem = Double.parseDouble(tShouldPayMoney);
        }

        //实交与应交进行比较
        if (tTemperFee >= tPrem)
        {
            CError tError = new CError();
            tError.moduleName = "UWSendPrintBLS";
            tError.functionName = "checkMain";
            tError.errorMessage = "该投保单到帐的金额已足,不必再发缴费通知书";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 准备打印信息
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean preparePrint()
    {
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mLOPRTManagerSchema.getOtherNo());
        if (tLCContDB.getInfo() == false)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCContDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "UWSendPrintBLS";
            tError.functionName = "preparePrint";
            tError.errorMessage = "没有保单信息!";
            this.mErrors.addOneError(tError);
            return false;
        }

        LCContSchema tLCContSchema = new LCContSchema();
        tLCContSchema = tLCContDB.getSchema();

        if ("1".equals(mNoPrtFlag))
        {
            System.out.println(tLCContSchema.getPrtNo() + "：不需要打印缴费通知书。");
            return true;
        }

        mLOPRTManagerSchema.setAgentCode(tLCContSchema.getAgentCode());
        mLOPRTManagerSchema.setPrtType(PrintManagerBL.PT_FRONT);
        mLOPRTManagerSchema.setManageCom(tLCContSchema.getManageCom());

        //通知书类型
        if (mLOPRTManagerSchema.getOtherNoType().equals("00"))
        {
            mLOPRTManagerSchema.setOtherNoType(PrintManagerBL.ONT_INDPOL);
        }
        if (mLOPRTManagerSchema.getOtherNoType().equals("01"))
        {
            mLOPRTManagerSchema.setOtherNoType(PrintManagerBL.ONT_GRPPOL);
        }
        if (mLOPRTManagerSchema.getOtherNoType().equals("02"))
        {
            mLOPRTManagerSchema.setOtherNoType(PrintManagerBL.ONT_CONT);
        }
        if (mLOPRTManagerSchema.getOtherNoType().equals("03"))
        {
            mLOPRTManagerSchema.setOtherNoType(PrintManagerBL.ONT_EDOR);
        }

        //单据类型
        if (mLOPRTManagerSchema.getCode().equals("06"))
        {
            if (prepareURGE(mLOPRTManagerSchema) == false)
            {
                return false;
            }
        }
        if (mLOPRTManagerSchema.getCode().equals("07"))
        {
            // 判断保单是否需要打印通知书。
            if (!checkCanSendFirstPay(tLCContSchema))
                return true;

            System.out.println("缴费方式为:" + tLCContSchema.getPayMode());

            //生成首期交费通知书号码，不再使用打印流水号
            String prtSeq = "";

            prtSeq = PubFun1.CreateMaxNo("PAYNOTICENO", tLCContSchema
                    .getPrtNo());

            if (prtSeq == null || "".equals(prtSeq))
            {
                CError tError = new CError();
                tError.moduleName = "UWSendPrintBLS";
                tError.functionName = "preparePrint";
                tError.errorMessage = "获取缴费通知书号失败!";
                this.mErrors.addOneError(tError);
                return false;
            }

            mLOPRTManagerSchema.setPrtSeq(prtSeq);
            System.out.println("按PICC规则的首期交费通知书号:"
                    + mLOPRTManagerSchema.getPrtSeq());

            LDSysVarDB tLDSysVarDB = new LDSysVarDB();
            tLDSysVarDB.setSysVar("URGEInterval");

            if (tLDSysVarDB.getInfo() == false)
            {
                CError tError = new CError();
                tError.moduleName = "UWSendPrintBLS";
                tError.functionName = "prepareURGE";
                tError.errorMessage = "没有描述催发间隔!";
                this.mErrors.addOneError(tError);
                return false;
            }
            FDate tFDate = new FDate();
            int tInterval = Integer.parseInt(tLDSysVarDB.getSysVarValue());
            Date tDate = PubFun.calDate(
                    tFDate.getDate(PubFun.getCurrentDate()), tInterval, "D",
                    null);

            mLOPRTManagerSchema.setCode(PrintManagerBL.CODE_FIRSTPAY);
            mLOPRTManagerSchema.setForMakeDate(tDate);

            mLOPRTManagerSet.add(mLOPRTManagerSchema);
        }

        if (mLOPRTManagerSchema.getCode().equals("15"))
        {
            if (!checkMain15())
            {
                return false;
            }

            mLOPRTManagerSchema.setCode(PrintManagerBL.CODE_URGE_PB);

            mLOPRTManagerSet.add(mLOPRTManagerSchema);
        }
        return true;
    }

    /**
     * 取催发数据
     */
    private boolean prepareURGE(LOPRTManagerSchema tLOPRTManagerSchema)
    {
        LDSysVarDB tLDSysVarDB = new LDSysVarDB();
        tLDSysVarDB.setSysVar("URGEInterval");

        if (tLDSysVarDB.getInfo() == false)
        {
            CError tError = new CError();
            tError.moduleName = "UWSendPrintBLS";
            tError.functionName = "prepareURGE";
            tError.errorMessage = "没有描述催发间隔!";
            this.mErrors.addOneError(tError);
            return false;
        }

        FDate tFDate = new FDate();
        int tInterval = Integer.parseInt(tLDSysVarDB.getSysVarValue());
        System.out.println(tInterval);

        Date tDate = PubFun.calDate(tFDate.getDate(PubFun.getCurrentDate()),
                tInterval, "D", null);
        System.out.println(tDate);

        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        LOPRTManagerSet tLOPRTManagerSet = new LOPRTManagerSet();
        String tsql = "select * from loprtmanager where otherno = '"
                + tLOPRTManagerSchema.getOtherNo()
                + "' and code in ('03','05','07') and stateflag = '2' and donedate = "
                + tDate;
        System.out.println(tsql);
        tLOPRTManagerSet = tLOPRTManagerDB.executeQuery(tsql);
        if (tLOPRTManagerSet.size() > 0)
        {
            for (int i = 1; i <= tLOPRTManagerSet.size(); i++)
            {
                LOPRTManagerSchema t2LOPRTManagerSchema = new LOPRTManagerSchema();
                t2LOPRTManagerSchema = tLOPRTManagerSet.get(i);

                mLOPRTManagerSchema.setCode(t2LOPRTManagerSchema.getCode());
                mLOPRTManagerSet.add(mLOPRTManagerSchema);
            }
        }
        return true;
    }

    /**
     * 校验保单是否需要发送缴费通知书。
     * 目前原则：足额，不发缴费通知书。
     * @return true-需发送；false-不需要发送
     */
    private boolean checkCanSendFirstPay(LCContSchema tLCContSchema)
    {
        double tDifMoney = 0;
        tDifMoney = calDifPrem(tLCContSchema, AutoUWFlag);

        return tDifMoney < 0;
    }

    /**
     * 获取暂收保费与应缴保费差值。
     * 原则：差值 = 暂收保费 - 应缴保费
     * @param tContInfo
     * @return
     */
    private double calDifPrem(LCContSchema tContInfo, String tAutoUWFlag)
    {
        ExeSQL tExeSQL = new ExeSQL();
        String tStrSql = null;
        String tResult = null;

        // 校验到帐保费是否足额。
        if (StrTool.cTrim(tAutoUWFlag).equals("1"))
        {
            // 自核通过的情况
            double tContSumPrem = 0;
            double tContTempFeeSumMoney = 0;
            double tContTheWarMoney = 0;

            // 获取整单保费，由于自核通过时，uwflag尚未更新数据库，因此视为该单下全部包含险种的总保费即为整单保费
            String tContNo = tContInfo.getContNo();
            tResult = null;
            tStrSql = null;
            tStrSql = "select nvl(sum(nvl(lcp.Prem, 0) + nvl(lcp.SupplementaryPrem, 0)), 0) from LCPol lcp "
                    + " where lcp.ContNo = '" + tContNo + "' ";
            tResult = tExeSQL.getOneValue(tStrSql);
            tContSumPrem = Double.parseDouble(tResult);
            tResult = null;
            tStrSql = null;

            System.out.println("tContSumPrem:" + tContSumPrem);

            // 获取暂收总保费。
            String tPrtNo = tContInfo.getPrtNo();
            tResult = null;
            tStrSql = null;
            tStrSql = " select nvl(sum(ljtf.PayMoney), 0) from LJTempFee ljtf "
                    + " where ljtf.OtherNoType = '4' " // 个单暂收核销前标志
                    // + " and ljtf.RiskCode != '000000' " // 除去保全相关暂收数据
                    + " and ljtf.ConfFlag = '0' " // 可用销暂收数据
                    + " and ljtf.EnterAccDate is not null " // 财务到帐
                    + " and ljtf.OtherNo = '" + tPrtNo + "' ";
            tResult = tExeSQL.getOneValue(tStrSql);
            tContTempFeeSumMoney = Double.parseDouble(tResult);
            tResult = null;
            tStrSql = null;
            // --------------------
            
            tResult = null;
            tStrSql = null;
            tStrSql = " select nvl(sum(SumDuePayMoney),0) from ljspay ljs "
                    + " where ljs.BankOnTheWayFlag='1' and ljs.OtherNoType in ('16','1') " // 个单暂收核销前标志
                    + " and otherno = '"+tPrtNo+"'";
            tResult = tExeSQL.getOneValue(tStrSql);
            tContTheWarMoney = Double.parseDouble(tResult);
            tResult = null;
            tStrSql = null;
            
            System.out.println("tContTempFeeSumMoney:" + tContTempFeeSumMoney);
            System.out.println("Dif:" + (tContTempFeeSumMoney - tContSumPrem));

            return (tContTempFeeSumMoney - tContSumPrem + tContTheWarMoney);
        }
        else
        {
            String tPrtNo = tContInfo.getPrtNo();
            tResult = null;
            tStrSql = null;
            tStrSql = "select LF_PayMoneyNo('" + tPrtNo + "') from Dual";
            tResult = new ExeSQL().getOneValue(tStrSql);
            if ("".equals(tResult))
            {
                // 不会出现这种情况，为防止万一，如果产生，后台报错，但不阻断，不发通知书。
                String tStrErr = "以印刷号查询，未查到合同号。";
                System.out.println(tStrErr);
                buildError("getDifPrem", tStrErr);
                return 0;
            }
            double tOutPrem = 0;
            tOutPrem = Double.parseDouble(tResult);
            System.out.println("DifPrem:" + tOutPrem);
            tResult = null;
            tStrSql = null;
            return tOutPrem;
        }
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "UWSendPrintBLS";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }

}
