package com.sinosoft.lis.cbcheck;

import com.sinosoft.lis.db.LBMissionDB;
import com.sinosoft.lis.db.LCCUWMasterDB;
import com.sinosoft.lis.db.LCCUWSubDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LCUWMasterDB;
import com.sinosoft.lis.db.LCUWSubDB;
import com.sinosoft.lis.db.LJAGetTempFeeDB;
import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.db.LJTempFeeClassDB;
import com.sinosoft.lis.db.LJTempFeeDB;
import com.sinosoft.lis.db.LWFieldMapDB;
import com.sinosoft.lis.db.LWMissionDB;
import com.sinosoft.lis.db.LWProcessInstanceDB;
import com.sinosoft.lis.f1print.PrintManagerBL;
import com.sinosoft.lis.f1print.PrintPDFManagerBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LBMissionSchema;
import com.sinosoft.lis.schema.LCApplyRecallPolSchema;
import com.sinosoft.lis.schema.LCCUWMasterSchema;
import com.sinosoft.lis.schema.LCCUWSubSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LCUWMasterSchema;
import com.sinosoft.lis.schema.LCUWSubSchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.schema.LWFieldMapSchema;
import com.sinosoft.lis.vschema.LBMissionSet;
import com.sinosoft.lis.vschema.LCApplyRecallPolSet;
import com.sinosoft.lis.vschema.LCCUWMasterSet;
import com.sinosoft.lis.vschema.LCCUWSubSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LCUWMasterSet;
import com.sinosoft.lis.vschema.LCUWSubSet;
import com.sinosoft.lis.vschema.LJAGetTempFeeSet;
import com.sinosoft.lis.vschema.LJSPaySet;
import com.sinosoft.lis.vschema.LJTempFeeClassSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.lis.vschema.LWFieldMapSet;
import com.sinosoft.lis.vschema.LWMissionSet;
import com.sinosoft.lis.vschema.LWProcessInstanceSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author zhangtao
 * @version 1.0
 */

public class ApplyRecallPolBL
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    private VData mInputData;

    private String mOperate;

    private VData mResult = new VData();

    private GlobalInput mGlobalInput = new GlobalInput();

    private MMap map = new MMap();

    private LCApplyRecallPolSchema mLCApplyRecallPolSchema = new LCApplyRecallPolSchema();

    private String CurrentDate = PubFun.getCurrentDate();

    private String CurrentTime = PubFun.getCurrentTime();

    private String mPrtNo = "";

    private String mContNo = "";

    private String mProposalContNo = "";

    private String mMissionID = "";

    //业务处理相关变量
    private LCApplyRecallPolSet mLCApplyRecallPolSet = new LCApplyRecallPolSet();

    private LCApplyRecallPolSet outDelLCApplyRecallPolSet = new LCApplyRecallPolSet();

    private LCPolSet mLCPolSet = new LCPolSet();

    private LCContSchema mLCContSchema = new LCContSchema();

    public ApplyRecallPolBL()
    {
    }

    public static void main(String[] args)
    {
    }

    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();
        mOperate = cOperate;

        if (!getInputData())
        {
            return false;
        }

        if (!checkData())
        {
            return false;
        }

        //进行业务处理
        if (!dealData(mOperate))
        {
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }

        //数据提交
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, ""))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "ApplyRecallPolBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;

        return true;
    }

    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData()
    {
        /** 首先校验财务数据 */
        //        LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
        //        tLJTempFeeDB.setOtherNo(this.mPrtNo);
        //        LJTempFeeSet tLJTempFeeSet = tLJTempFeeDB.query();
        //        LJTempFeeClassSet tLJTempFeeClassSet;
        //        if (tLJTempFeeSet.size() > 0)
        //        {
        //            System.out.println("财务表存在数据,需要判断是否到账,是否存在应收数据");
        //            int chk = 0;
        //            for (int i = 1; i <= tLJTempFeeSet.size(); i++)
        //            {
        //                LJTempFeeSchema tLJTempFeeSchema = tLJTempFeeSet.get(i);
        //                if (tLJTempFeeSchema.getEnterAccDate() != null)
        //                {
        //                    LJAGetTempFeeDB tLJAGetTempFeeDB = new LJAGetTempFeeDB();
        //                    tLJAGetTempFeeDB.setTempFeeNo(tLJTempFeeSchema
        //                            .getTempFeeNo());
        //                    LJAGetTempFeeSet tLJAGetTempFeeSet = tLJAGetTempFeeDB
        //                            .query();
        //                    //添加对财务退费的校验
        //                    if (tLJTempFeeSchema.getConfDate() != null
        //                            && !tLJTempFeeSchema.getConfDate().equals("")
        //                            && tLJAGetTempFeeSet.size() > 0)
        //                    {
        //                        System.out.println("财务已退费，可以做撤单处理！");
        //                    }
        //                    else
        //                    {
        //                        System.out.println("财务已到帐！");
        //                        buildError("checkData", "发生错误,原因是有险种保费已经到帐无法撤销处理！");
        //                        return false;
        //                    }
        //                }
        //            }
        //            LJTempFeeClassDB tLJTempFeeClassDB = new LJTempFeeClassDB();
        //            tLJTempFeeClassDB.setTempFeeNo(tLJTempFeeSet.get(1).getTempFeeNo());
        //            tLJTempFeeClassSet = tLJTempFeeClassDB.query();
        //由于先收费未回盘时，没有生成 LJTempFeeClass 数据，故取消对 LJTempFeeClass 的长度校验
        //if (tLJTempFeeClassSet.size() <= 0) {
        //    buildError("checkData", "查询缴费分类失败！");
        //    return false;
        //}
        //        LJSPayDB tLJSPayDB = new LJSPayDB();
        //        tLJSPayDB.setOtherNo(this.mPrtNo);
        //        LJSPaySet tLJSPaySet = tLJSPayDB.query();
        //        if (tLJSPaySet.size() > 0)
        //        {
        //            for (int i = 1; i <= tLJSPaySet.size(); i++)
        //            {
        //                if (tLJSPaySet.get(i).getBankOnTheWayFlag().equals("1"))
        //                {
        //                    buildError("checkData", "银行在途不能撤销申请！");
        //                    return false;
        //                }
        //            }
        //        }
        //        //            this.map.put(tLJTempFeeSet, "DELETE");
        //        //            this.map.put(tLJTempFeeClassSet, "DELETE");
        //        this.map.put(tLJSPaySet, "DELETE");
        // 处理财务暂收
        //        if (!dealTempFeeDatas(mLCContSchema))
        //        {
        //            return false;
        //        }
        // --------------------
        //        }
        return true;
    }

    /**
     * 处理财务数据，未到帐确认
     * @return boolean
     */
    private boolean prepareFinFee(LCContSchema cContInfo)
    {
        LJSPayDB tLJSPayDB = new LJSPayDB();
        tLJSPayDB.setOtherNo(cContInfo.getPrtNo());
        LJSPaySet tLJSPaySet = tLJSPayDB.query();
        if (tLJSPaySet.size() > 0)
        {
            for (int i = 1; i <= tLJSPaySet.size(); i++)
            {
                if (StrTool.cTrim(tLJSPaySet.get(i).getBankOnTheWayFlag())
                        .equals("1"))
                {
                    CError tError = new CError();
                    tError.moduleName = "ReManuUWAfterInitService";
                    tError.functionName = "getInputData";
                    tError.errorMessage = "银行在途，不能核保订正！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
            }
        }
        this.map.put(tLJSPaySet, "DELETE");

        // 处理财务暂收
        MMap tTempFeeMap = dealTempFeeDatas(cContInfo);
        if (tTempFeeMap == null)
        {
            return false;
        }
        map.add(tTempFeeMap);
        // --------------------

        return true;
    }

    //根据前面的输入数据，进行逻辑处理
    private boolean dealData(String Operate)
    {
        //查询合同信息
        LCContSet tLCContSet = new LCContSet();
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setPrtNo(mPrtNo);
        tLCContSet.set(tLCContDB.query());
        if (tLCContDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCContDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ApplyRecallPolBL";
            tError.functionName = "prepareContUW";
            tError.errorMessage = "合同信息查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLCContSchema = tLCContSet.get(1).getSchema();
        if (tLCContSet == null || tLCContSet.size() != 1)
        {
            CError tError = new CError();
            tError.moduleName = "ApplyRecallPolBL";
            tError.functionName = "prepareContUW";
            tError.errorMessage = "没有找到该印刷号对应的合同信息，请确认!";
            this.mErrors.addOneError(tError);
            return false;
        }
        LCContSchema tLCContSchema = tLCContSet.get(1);
        mContNo = tLCContSchema.getContNo();
        mProposalContNo = tLCContSchema.getProposalContNo();
        tLCContSchema.setUWFlag("a");

        // 先处理财务数据
        if (!prepareFinFee(mLCContSchema))
        {
            return false;
        }
        // --------------------

        //准备合同核保信息
        //        if (!prepareContUW(tLCContSchema)) {
        //            return false;
        //        }

        //查询该保单下所有险种保单
        LCPolSet tLCPolSet = new LCPolSet();
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPrtNo(mPrtNo);
        tLCPolSet.set(tLCPolDB.query());
        if (tLCPolSet == null || tLCPolSet.size() == 0)
        {
            CError tError = new CError();
            tError.moduleName = "ApplyRecallPolBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有找到该印刷号对应的险种保单，请确认!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //生成保单撤单申请记录

        LCApplyRecallPolSchema tLCApplyRecallPolSchema;
        for (int j = 1; j <= tLCPolSet.size(); j++)
        {
            LCPolSchema aLCPolSchema = new LCPolSchema();
            aLCPolSchema.setSchema(tLCPolSet.get(j));
            aLCPolSchema.setUWFlag("a");
            mLCPolSet.add(aLCPolSchema);
            //            //准备险种核保信息
            //            if (!preparePolUW(aLCPolSchema)) {
            //                return false;
            //            }

            tLCApplyRecallPolSchema = new LCApplyRecallPolSchema();

            tLCApplyRecallPolSchema.setPrtNo(aLCPolSchema.getPrtNo());
            tLCApplyRecallPolSchema.setProposalNo(aLCPolSchema.getProposalNo());
            tLCApplyRecallPolSchema.setMainPolNo(aLCPolSchema.getMainPolNo());
            tLCApplyRecallPolSchema.setRemark(mLCApplyRecallPolSchema
                    .getRemark());
            tLCApplyRecallPolSchema.setApplyType(mLCApplyRecallPolSchema
                    .getApplyType()); //Add by Minim
            tLCApplyRecallPolSchema.setInputState("0");
            tLCApplyRecallPolSchema.setOperator(mGlobalInput.Operator);
            tLCApplyRecallPolSchema.setManageCom(mGlobalInput.ManageCom);
            tLCApplyRecallPolSchema.setMakeDate(CurrentDate);
            tLCApplyRecallPolSchema.setMakeTime(CurrentTime);
            tLCApplyRecallPolSchema.setModifyDate(CurrentDate);
            tLCApplyRecallPolSchema.setModifyTime(CurrentTime);
            // tLCApplyRecallPolSchema.setApplyType(mLCApplyRecallPolSchema.getApplyType());
            mLCApplyRecallPolSet.add(tLCApplyRecallPolSchema);

            LCApplyRecallPolSchema outDelLCApplyRecallPolSchema = new LCApplyRecallPolSchema();
            outDelLCApplyRecallPolSchema.setProposalNo(aLCPolSchema
                    .getProposalNo());
            outDelLCApplyRecallPolSet.add(outDelLCApplyRecallPolSchema.getDB()
                    .query());

        }

        map.put(mLCPolSet, "UPDATE");
        map.put(outDelLCApplyRecallPolSet, "DELETE");
        map.put(mLCApplyRecallPolSet, "INSERT");
        map.put(tLCContSchema, "UPDATE");

        //获取工作流任务ID
        if (!getMissionID())
        {
            return false;
        }

        //删除保单工作流节点 [备份]
        LWMissionSet tLWMissionSet = new LWMissionSet();
        LWMissionDB tLWMissionDB = new LWMissionDB();
        tLWMissionDB.setMissionID(mMissionID);
        tLWMissionSet.set(tLWMissionDB.query());
        if (tLWMissionDB.mErrors.needDealError() == true)
        {
            // @@错误处理
            mErrors.copyAllErrors(tLWMissionDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "GrpGiveEnsureBL";
            tError.functionName = "getBaseData";
            tError.errorMessage = "保单工作流任务查询失败!" + "印刷号：" + mPrtNo + "任务号："
                    + mMissionID;
            mErrors.addOneError(tError);
            return false;
        }
        Reflections tReflections = new Reflections();
        LBMissionSet tLBMissionSet = new LBMissionSet();
        LBMissionSchema tLBMissionSchema;
        String tSerielNo = "";
        for (int i = 1; i <= tLWMissionSet.size(); i++)
        {
            tSerielNo = PubFun1.CreateMaxNo("MissionSerielNo", 10);

            tLBMissionSchema = new LBMissionSchema();
            tReflections.transFields(tLBMissionSchema, tLWMissionSet.get(i));

            tLBMissionSchema.setSerialNo(tSerielNo);
            tLBMissionSchema.setActivityStatus("3"); //节点任务执行完毕
            tLBMissionSchema.setLastOperator(mGlobalInput.Operator);
            tLBMissionSchema.setMakeDate(CurrentDate);
            tLBMissionSchema.setMakeTime(CurrentTime);
            tLBMissionSchema.setModifyDate(CurrentDate);
            tLBMissionSchema.setModifyTime(CurrentTime);
            tLBMissionSet.add(tLBMissionSchema);
        }
        map.put(tLBMissionSet, "INSERT");
        map.put(tLWMissionSet, "DELETE");

        /**
         * 生成撤保退费通知书
         */
        //        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        //        String tLimit = mPrtNo;
        //        String tPrtSeq = PubFun1.CreateMaxNo("UWRESULTNOTICE", tLimit);
        //        tLOPRTManagerSchema.setCode(PrintManagerBL.CODE_DEFER);
        //        tLOPRTManagerSchema.setStandbyFlag1(this.mLCContSchema
        //                .getProposalContNo());
        //        tLOPRTManagerSchema.setPrtSeq(tPrtSeq);
        //        System.out.println("生成号 : " + tPrtSeq);
        //        tLOPRTManagerSchema.setOldPrtSeq(tPrtSeq);
        //        tLOPRTManagerSchema.setOtherNo(this.mLCContSchema.getProposalContNo());
        //
        //        tLOPRTManagerSchema.setManageCom(mLCContSchema.getManageCom());
        //        tLOPRTManagerSchema.setPrtType(PrintManagerBL.PT_FRONT);
        //        tLOPRTManagerSchema.setAgentCode(mLCContSchema.getAgentCode());
        //        tLOPRTManagerSchema.setOtherNoType(PrintManagerBL.ONT_CONT);
        //        tLOPRTManagerSchema.setReqOperator(mGlobalInput.Operator);
        //        tLOPRTManagerSchema.setReqCom(mGlobalInput.ManageCom);
        //        tLOPRTManagerSchema.setManageCom(mLCContSchema.getManageCom());
        //        tLOPRTManagerSchema.setStateFlag("0"); //打印标志“0”表示未打
        //
        //        tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        //        tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
        //
        //        map.put(tLOPRTManagerSchema, "INSERT");
        // 撤单成功下发拒保撤销通知书。
        MMap tTmpMap = sendDeferPrint(mLCContSchema);
        if (tTmpMap == null)
        {
            return false;
        }
        map.add(tTmpMap);
        // --------------------

        return true;
    }

    //从输入数据中得到所有对象
    //输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
    private boolean getInputData()
    {
        mLCApplyRecallPolSchema = (LCApplyRecallPolSchema) mInputData
                .getObjectByObjectName("LCApplyRecallPolSchema", 0);

        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);

        if (mLCApplyRecallPolSchema == null
                || mLCApplyRecallPolSchema.getPrtNo() == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ApplyRecallPolBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据!";
            mErrors.addOneError(tError);
            return false;
        }

        mPrtNo = mLCApplyRecallPolSchema.getPrtNo();

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    //准备往后层输出所需要的数据
    private boolean prepareOutputData()
    {
        try
        {
            mInputData.clear();
            mInputData.add(map);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ApplyRecallPolBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错:" + ex.toString();
            mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 准备合同核保信息
     */
    private boolean prepareContUW(LCContSchema tLCContSchema)
    {
        Reflections ref = new Reflections();
        //合同核保主表
        LCCUWMasterSet tLCCUWMasterSet = new LCCUWMasterSet();
        LCCUWMasterSchema tLCCUWMasterSchema = new LCCUWMasterSchema();
        LCCUWMasterDB tLCCUWMasterDB = new LCCUWMasterDB();
        tLCCUWMasterDB.setContNo(mContNo);
        tLCCUWMasterSet.set(tLCCUWMasterDB.query());
        if (tLCCUWMasterDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCCUWMasterDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ApplyRecallPolBL";
            tError.functionName = "prepareContUW";
            tError.errorMessage = "合同核保总表查询失败!" + "合同号：" + mContNo;
            this.mErrors.addOneError(tError);
            return false;
        }

        if (tLCCUWMasterSet.size() == 0)
        {
            tLCCUWMasterSchema.setContNo(mContNo);
            tLCCUWMasterSchema.setGrpContNo(tLCContSchema.getGrpContNo());
            tLCCUWMasterSchema.setProposalContNo(tLCContSchema
                    .getProposalContNo());
            tLCCUWMasterSchema.setUWNo(1);
            tLCCUWMasterSchema.setInsuredNo(tLCContSchema.getInsuredNo());
            tLCCUWMasterSchema.setInsuredName(tLCContSchema.getInsuredName());
            tLCCUWMasterSchema.setAppntNo(tLCContSchema.getAppntNo());
            tLCCUWMasterSchema.setAppntName(tLCContSchema.getAppntName());
            tLCCUWMasterSchema.setAgentCode(tLCContSchema.getAgentCode());
            tLCCUWMasterSchema.setAgentGroup(tLCContSchema.getAgentGroup());
            tLCCUWMasterSchema.setPostponeDay("");
            tLCCUWMasterSchema.setPostponeDate("");
            tLCCUWMasterSchema.setAutoUWFlag("1"); // 1 自动核保 2 人工核保
            tLCCUWMasterSchema.setHealthFlag("0");
            tLCCUWMasterSchema.setSpecFlag("0");
            tLCCUWMasterSchema.setQuesFlag("0");
            tLCCUWMasterSchema.setReportFlag("0");
            tLCCUWMasterSchema.setChangePolFlag("0");
            tLCCUWMasterSchema.setPrintFlag("0");
            tLCCUWMasterSchema.setPrintFlag2("0");
            tLCCUWMasterSchema.setManageCom(tLCContSchema.getManageCom());
            tLCCUWMasterSchema.setUWIdea("");
            tLCCUWMasterSchema.setUpReportContent("");
            tLCCUWMasterSchema.setPassFlag("a");
            tLCCUWMasterSchema.setOperator(mGlobalInput.Operator); //操作员
            tLCCUWMasterSchema.setMakeDate(CurrentDate);
            tLCCUWMasterSchema.setMakeTime(CurrentTime);
            tLCCUWMasterSchema.setModifyDate(CurrentDate);
            tLCCUWMasterSchema.setModifyTime(CurrentTime);
        }
        else
        {
            tLCCUWMasterSchema = tLCCUWMasterSet.get(1);
            tLCCUWMasterSchema.setUWNo(tLCCUWMasterSchema.getUWNo() + 1);
            tLCCUWMasterSchema.setPassFlag("a");
            tLCCUWMasterSchema.setAutoUWFlag("1"); // 1 自动核保 2 人工核保
            tLCCUWMasterSchema.setOperator(mGlobalInput.Operator); //操作员
            tLCCUWMasterSchema.setModifyDate(CurrentDate);
            tLCCUWMasterSchema.setModifyTime(CurrentTime);
        }
        tLCCUWMasterSet.clear();
        tLCCUWMasterSet.add(tLCCUWMasterSchema);

        //合同核保轨迹表
        LCCUWSubSet tLCCUWSubSet = new LCCUWSubSet();
        LCCUWSubSchema tLCCUWSubSchema = new LCCUWSubSchema();
        LCCUWSubDB tLCCUWSubDB = new LCCUWSubDB();
        tLCCUWSubDB.setContNo(mContNo);
        tLCCUWSubSet = tLCCUWSubDB.query();
        if (tLCCUWSubDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCCUWSubDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ApplyRecallPolBL";
            tError.functionName = "prepareContUW";
            tError.errorMessage = mContNo + "合同核保轨迹表查失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        ref.transFields(tLCCUWSubSchema, tLCCUWMasterSchema);
        int nUWNo = tLCCUWSubSet.size();
        if (nUWNo > 0)
        {
            tLCCUWSubSchema.setUWNo(++nUWNo); //第n次核保
        }
        else
        {
            tLCCUWSubSchema.setUWNo(1); //第1次核保
        }

        tLCCUWSubSchema.setPassFlag("a");
        tLCCUWSubSchema.setOperator(tLCCUWMasterSchema.getOperator()); //操作员
        tLCCUWSubSchema.setManageCom(tLCCUWMasterSchema.getManageCom());
        tLCCUWSubSchema.setMakeDate(CurrentDate);
        tLCCUWSubSchema.setMakeTime(CurrentTime);
        tLCCUWSubSchema.setModifyDate(CurrentDate);
        tLCCUWSubSchema.setModifyTime(CurrentTime);

        tLCCUWSubSet.clear();
        tLCCUWSubSet.add(tLCCUWSubSchema);
        map.put(tLCCUWMasterSet, "DELETE&INSERT");
        map.put(tLCCUWSubSet, "INSERT");

        return true;
    }

    /**
     * 准备险种核保信息
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean preparePolUW(LCPolSchema tLCPolSchema)
    {
        int tuwno = 0;
        String mOldPolNo = tLCPolSchema.getPolNo();
        LCUWMasterSet tLCUWMasterSet = new LCUWMasterSet();
        LCUWMasterSchema tLCUWMasterSchema = new LCUWMasterSchema();
        LCUWMasterDB tLCUWMasterDB = new LCUWMasterDB();
        tLCUWMasterDB.setPolNo(mOldPolNo);
        tLCUWMasterSet = tLCUWMasterDB.query();
        if (tLCUWMasterDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCUWMasterDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "UWAtuoChkBL";
            tError.functionName = "prepareUW";
            tError.errorMessage = mOldPolNo + "个人核保总表取数失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        int n = tLCUWMasterSet.size();
        if (n == 0)
        {
            tLCUWMasterSchema.setContNo(mContNo);
            tLCUWMasterSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
            tLCUWMasterSchema.setPolNo(mOldPolNo);
            tLCUWMasterSchema.setProposalContNo(mProposalContNo);
            tLCUWMasterSchema.setProposalNo(tLCPolSchema.getProposalNo());
            tLCUWMasterSchema.setUWNo(1);
            tLCUWMasterSchema.setInsuredNo(tLCPolSchema.getInsuredNo());
            tLCUWMasterSchema.setInsuredName(tLCPolSchema.getInsuredName());
            tLCUWMasterSchema.setAppntNo(tLCPolSchema.getAppntNo());
            tLCUWMasterSchema.setAppntName(tLCPolSchema.getAppntName());
            tLCUWMasterSchema.setAgentCode(tLCPolSchema.getAgentCode());
            tLCUWMasterSchema.setAgentGroup(tLCPolSchema.getAgentGroup());
            tLCUWMasterSchema.setPostponeDay("");
            tLCUWMasterSchema.setPostponeDate("");
            tLCUWMasterSchema.setAutoUWFlag("1"); // 1 自动核保 2 人工核保
            tLCUWMasterSchema.setHealthFlag("0");
            tLCUWMasterSchema.setSpecFlag("0");
            tLCUWMasterSchema.setQuesFlag("0");
            tLCUWMasterSchema.setReportFlag("0");
            tLCUWMasterSchema.setChangePolFlag("0");
            tLCUWMasterSchema.setPrintFlag("0");
            tLCUWMasterSchema.setManageCom(tLCPolSchema.getManageCom());
            tLCUWMasterSchema.setUWIdea("");
            tLCUWMasterSchema.setUpReportContent("");
            tLCUWMasterSchema.setPassFlag("a");
            tLCUWMasterSchema.setOperator(mGlobalInput.Operator); //操作员
            tLCUWMasterSchema.setMakeDate(CurrentDate);
            tLCUWMasterSchema.setMakeTime(CurrentTime);
            tLCUWMasterSchema.setModifyDate(CurrentDate);
            tLCUWMasterSchema.setModifyTime(CurrentTime);
        }
        else if (n == 1)
        {
            tLCUWMasterSchema = tLCUWMasterSet.get(1);

            tuwno = tLCUWMasterSchema.getUWNo();
            tuwno = tuwno + 1;

            tLCUWMasterSchema.setUWNo(tuwno);
            tLCUWMasterSchema.setProposalContNo(mProposalContNo);
            tLCUWMasterSchema.setPassFlag("a");
            tLCUWMasterSchema.setAutoUWFlag("1"); // 1 自动核保 2 人工核保
            tLCUWMasterSchema.setOperator(mGlobalInput.Operator); //操作员
            tLCUWMasterSchema.setModifyDate(CurrentDate);
            tLCUWMasterSchema.setModifyTime(CurrentTime);
        }
        else
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCUWMasterDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "UWAtuoChkBL";
            tError.functionName = "prepareUW";
            tError.errorMessage = mOldPolNo + "个人核保总表取数据不唯一!";
            this.mErrors.addOneError(tError);
            return false;
        }

        tLCUWMasterSet.clear();
        tLCUWMasterSet.add(tLCUWMasterSchema);

        // 核保轨迹表
        LCUWSubSet tLCUWSubSet = new LCUWSubSet();
        LCUWSubSchema tLCUWSubSchema = new LCUWSubSchema();
        LCUWSubDB tLCUWSubDB = new LCUWSubDB();
        tLCUWSubDB.setPolNo(mOldPolNo);
        tLCUWSubSet = tLCUWSubDB.query();
        if (tLCUWSubDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCUWSubDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "UWAtuoChkBL";
            tError.functionName = "prepareUW";
            tError.errorMessage = mOldPolNo + "个人核保轨迹表查失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        Reflections ref = new Reflections();
        ref.transFields(tLCUWSubSchema, tLCUWMasterSchema);
        int m = tLCUWSubSet.size();
        if (m > 0)
        {
            tLCUWSubSchema.setUWNo(++m); //第n次核保
        }

        else
        {
            tLCUWSubSchema.setUWNo(1); //第1次核保
        }

        tLCUWSubSchema.setPassFlag("a");
        tLCUWSubSchema.setOperator(tLCUWMasterSchema.getOperator());
        tLCUWSubSchema.setManageCom(tLCUWMasterSchema.getManageCom());
        tLCUWSubSchema.setMakeDate(CurrentDate);
        tLCUWSubSchema.setMakeTime(CurrentTime);
        tLCUWSubSchema.setModifyDate(CurrentDate);
        tLCUWSubSchema.setModifyTime(CurrentTime);

        tLCUWSubSet.clear();
        tLCUWSubSet.add(tLCUWSubSchema);
        map.put(tLCUWMasterSet, "DELETE&INSERT");
        map.put(tLCUWSubSet, "INSERT");
        return true;
    }

    private boolean getMissionID()
    {
        LWMissionSet tLWMissionSet = new LWMissionSet();
        LWMissionDB tLWMissionDB = new LWMissionDB();

        //        LBMissionSet tLBMissionSet = new LBMissionSet();
        //        LBMissionDB tLBMissionDB = new LBMissionDB();
        //
        //        LWProcessInstanceSet tLWProcessInstanceSet = new LWProcessInstanceSet();
        //        LWProcessInstanceDB tLWProcessInstanceDB = new LWProcessInstanceDB();
        //        tLWProcessInstanceDB.setProcessID("0000000003");
        //        tLWProcessInstanceDB.setStartType("0"); //根节点
        //        tLWProcessInstanceSet.set(tLWProcessInstanceDB.query());
        //        if (tLWProcessInstanceDB.mErrors.needDealError()) {
        //            // @@错误处理
        //            this.mErrors.copyAllErrors(tLWProcessInstanceDB.mErrors);
        //            CError tError = new CError();
        //            tError.moduleName = "LDContTimeBL";
        //            tError.functionName = "getContNo";
        //            tError.errorMessage = "工作流过程实例查询失败!";
        //            this.mErrors.addOneError(tError);
        //            return false;
        //        }
        //        if (tLWProcessInstanceSet == null || tLWProcessInstanceSet.size() == 0) {
        //            CError tError = new CError();
        //            tError.moduleName = "LDContTimeBL";
        //            tError.functionName = "getContNo";
        //            tError.errorMessage = "工作流过程实例查询失败!";
        //            this.mErrors.addOneError(tError);
        //            return false;
        //        }
        //
        //        //循环查找该过程所有根节点寻找MissionID
        //        String tActivityID;
        //        for (int i = 1; i <= tLWProcessInstanceSet.size(); i++) {
        //            tActivityID = tLWProcessInstanceSet.get(i).getTransitionStart();
        //
        //            LWFieldMapSet tLWFieldMapSet = new LWFieldMapSet();
        //            LWFieldMapDB tLWFieldMapDB = new LWFieldMapDB();
        //            tLWFieldMapDB.setActivityID(tActivityID);
        //            tLWFieldMapSet.set(tLWFieldMapDB.query());
        //            if (tLWFieldMapDB.mErrors.needDealError()) {
        //                // @@错误处理
        //                this.mErrors.copyAllErrors(tLWFieldMapDB.mErrors);
        //                CError tError = new CError();
        //                tError.moduleName = "LDContTimeBL";
        //                tError.functionName = "getContNo";
        //                tError.errorMessage = "工作流任务字段映射查询失败!";
        //                this.mErrors.addOneError(tError);
        //                return false;
        //            }
        //
        //            LWFieldMapSchema tLWFieldMapSchema = new LWFieldMapSchema();
        //            for (int j = 1; j <= tLWFieldMapSet.size(); j++) {
        //                tLWFieldMapSchema.setSchema(tLWFieldMapSet.get(j));
        //                //找到印刷号影射字段
        //                if (tLWFieldMapSchema.getSourFieldName().equals("PrtNo")) {
        //                    //匹配该印刷号在此节点是否有任务
        //                    tLWMissionSet.clear();
        //                    tLWMissionDB = new LWMissionDB();
        //                    tLWMissionDB.setActivityID(tActivityID);
        //                    tLWMissionDB.setV(tLWFieldMapSchema.getDestFieldName(),
        //                                      mPrtNo);
        //                    tLWMissionSet.set(tLWMissionDB.query());
        //                    if (tLWMissionDB.mErrors.needDealError() == true) {
        //                        // @@错误处理
        //                        mErrors.copyAllErrors(tLWMissionDB.mErrors);
        //                        CError tError = new CError();
        //                        tError.moduleName = "ApplyRecallPolBL";
        //                        tError.functionName = "getBaseData";
        //                        tError.errorMessage = "保单工作流任务ID查询失败!" +
        //                                              "印刷号：" + mPrtNo;
        //                        mErrors.addOneError(tError);
        //                        return false;
        //                    }
        //                    if (tLWMissionSet != null && tLWMissionSet.size() == 1) {
        //                        mMissionID = tLWMissionSet.get(1).getMissionID();
        //                        System.out.println("==MissionID==" + mMissionID);
        //                        return true;
        //                    }
        //                    //查找备份表
        //                    if (tLWMissionSet == null || tLWMissionSet.size() == 0) {
        //                        tLBMissionSet.clear();
        //                        tLBMissionDB = new LBMissionDB();
        //                        tLBMissionDB.setActivityID(tActivityID);
        //                        tLBMissionDB.setV(tLWFieldMapSchema.getDestFieldName(),
        //                                          mPrtNo);
        //                        tLBMissionSet.set(tLBMissionDB.query());
        //                        if (tLBMissionDB.mErrors.needDealError() == true) {
        //                            // @@错误处理
        //                            mErrors.copyAllErrors(tLBMissionDB.mErrors);
        //                            CError tError = new CError();
        //                            tError.moduleName = "ApplyRecallPolBL";
        //                            tError.functionName = "getBaseData";
        //                            tError.errorMessage = "保单工作流任务ID查询失败!" +
        //                                                  "印刷号：" + mPrtNo;
        //                            mErrors.addOneError(tError);
        //                            return false;
        //                        }
        //                        if (tLBMissionSet != null && tLBMissionSet.size() == 1) {
        //                            mMissionID = tLBMissionSet.get(1).getMissionID();
        //                            System.out.println("==MissionID==" + mMissionID);
        //                            return true;
        //                        }
        //                    }
        //                }
        //            }
        //        }

        //查询保单的工作流节点ID
        String strSql = "select distinct MissionId from ("
                + "select MissionId from LWMission where ProcessID in ('0000000003', '0000000007', '0000000009','0000000013') "
                + "and MissionProp1 = '"
                + mPrtNo
                + "' and ActivityId in "
                + "(select ActivityId from LWFieldMap where SourFieldName = 'PrtNo' and DestFieldName = 'MissionProp1') "
                + "union select MissionId from LWMission where ProcessID in ('0000000003', '0000000007', '0000000009','0000000013') "
                + "and MissionProp2 = '"
                + mPrtNo
                + "' and ActivityId in "
                + "(select ActivityId from LWFieldMap where SourFieldName = 'PrtNo' and DestFieldName = 'MissionProp2') "
                + "union select MissionId from LWMission where ProcessID in ('0000000003', '0000000007', '0000000009','0000000013') "
                + "and MissionProp5 = '"
                + mPrtNo
                + "' and ActivityId in "
                + "(select ActivityId from LWFieldMap where SourFieldName = 'PrtNo' and DestFieldName = 'MissionProp5') "
                + ") as X";
        System.out.println(strSql);
        SSRS tSSRS = new ExeSQL().execSQL(strSql);

        if (tSSRS.getMaxRow() != 1)
        {
            CError tError = new CError();
            tError.moduleName = "ApplyRecallPolBL";
            tError.functionName = "getBaseData";
            tError.errorMessage = "保单工作流任务ID查询失败！印刷号：" + mPrtNo;
            mErrors.addOneError(tError);
            return false;
        }
        mMissionID = tSSRS.GetText(1, 1);

        //如果有任务正处于申请处理中，不能撤单
        //tLWMissionSet.clear();
        tLWMissionDB = new LWMissionDB();
        tLWMissionDB.setMissionID(mMissionID);
        tLWMissionSet.set(tLWMissionDB.query());
        if (tLWMissionDB.mErrors.needDealError() == true)
        {
            // @@错误处理
            mErrors.copyAllErrors(tLWMissionDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ApplyRecallPolBL";
            tError.functionName = "getBaseData";
            tError.errorMessage = "保单工作流任务ID查询失败！印刷号：" + mPrtNo;
            mErrors.addOneError(tError);
            return false;
        }
        //去除保单正在申请处理的校验
        ////当前处理申请人
        //String tDefaultOperator;
        //for (int i = 1; i <= tLWMissionSet.size(); i++) {
        //    tDefaultOperator = tLWMissionSet.get(i).getDefaultOperator();
        //    if (tDefaultOperator != null && !"".equals(tDefaultOperator)) {
        //        CError tError = new CError();
        //        tError.moduleName = "ApplyRecallPolBL";
        //        tError.functionName = "getBaseData";
        //        tError.errorMessage = "保单工作流任务正处于申请处理中，不能撤单!" +
        //                              "印刷号：" + mPrtNo +
        //                              "申请人：" + tDefaultOperator;
        //        mErrors.addOneError(tError);
        //        return false;
        //    }
        //}
        return true;
    }

    /**
     * 处理暂收财务数据。
     * @param cContInfo
     * @return
     */
    private MMap dealTempFeeDatas(LCContSchema cContInfo)
    {
        MMap tMMap = new MMap();

        ExeSQL tExeSQL = new ExeSQL();
        String tResult = null;
        String tStrSql = null;

        // 如果存在银行在途，发盘数据未锁定，或财务录入但未作财务确认的数据时，不允许核保订正。
        tResult = null;
        tStrSql = null;
        tStrSql = " select 1 from LJTempFee ljtf "
                + " where 1 = 1 and ljtf.EnterAccDate is not null "
                + " and ljtf.ConfMakeDate is null and ljtf.OtherNoType = '4' "
                + " and ljtf.OtherNo = '"
                + cContInfo.getPrtNo()
                + "' union all "
                + " select 1 from LJSPay ljsp where 1 = 1 and (ljsp.BankOnTheWayFlag = '1' or ljsp.CanSendBank = '0' or ljsp.CanSendBank is null ) "
                + " and ljsp.OtherNo = '" + cContInfo.getPrtNo() + "' ";
        tResult = tExeSQL.getOneValue(tStrSql);
        if ("1".equals(tResult))
        {
            buildError("dealTempFeeDatas",
                    "存在银行在途、发盘数据未锁定，或财务录入但未作财务确认的数据，不允许核保订正。");
            return null;
        }
        tResult = null;
        tStrSql = null;
        // --------------------
        
        MMap mmap = new MMap();
    	mmap.put("update ljspay set CanSendBank = '1' where otherno = '"
                 + cContInfo.getPrtNo() + "' and (BankOnTheWayFlag is null or BankOnTheWayFlag = '0')", "UPDATE");
    	VData mVData = new VData();
        mVData.add(mmap);
        PubSubmit mPubSubmit = new PubSubmit();
        if (!mPubSubmit.submitData(mVData, "UPDATE"))
        {
            this.mErrors.copyAllErrors(mPubSubmit.mErrors);
            return null;
        }

        // 处理非到帐暂收
        tStrSql = null;

        String tPrtNo = cContInfo.getPrtNo();
        tStrSql = " select * from LJTempFee ljtf "
                + " where 1 = 1 and ljtf.EnterAccDate is null "
                + " and ljtf.OtherNo = '" + tPrtNo + "' ";
        LJTempFeeSet tTempFeeSet = new LJTempFeeDB().executeQuery(tStrSql);
        tMMap.put(tTempFeeSet, SysConst.DELETE);

        tStrSql = null;
        tStrSql = " select * from LJTempFeeClass ljtfc "
                + " where ljtfc.TempFeeNo in ("
                + " select ljtf.TempFeeNo from LJTempFee ljtf "
                + " where 1 = 1 and ljtf.EnterAccDate is null "
                + " and ljtf.OtherNo = '" + tPrtNo + "') ";
        LJTempFeeClassSet tTempFeeClassSet = new LJTempFeeClassDB()
                .executeQuery(tStrSql);
        tMMap.put(tTempFeeClassSet, SysConst.DELETE);
        tStrSql = null;
        // --------------------

        return tMMap;
    }

    /**
     * 生成拒保通知书待打印数据。
     * @return
     */
    private MMap sendDeferPrint(LCContSchema cContInfo)
    {
        MMap tMMap = new MMap();

        LOPRTManagerSchema tPayPrintInfo = new LOPRTManagerSchema();

        // 获取结算单缴费凭证号
        String tPrtSeq = null;
        tPrtSeq = PubFun1.CreateMaxNo("UWRESULTNOTICE", cContInfo.getPrtNo());

        if (tPrtSeq == null || "".equals(tPrtSeq))
        {
            String tStrErr = "获取拒保通知书凭证号失败。";
            buildError("sendDeferPrint", tStrErr);
            return null;
        }
        tPayPrintInfo.setPrtSeq(tPrtSeq);
        // ----------------------------------

        tPayPrintInfo.setOtherNo(cContInfo.getContNo());
        tPayPrintInfo.setOtherNoType(PrintPDFManagerBL.ONT_CONT);

        tPayPrintInfo.setCode("05_T");

        tPayPrintInfo.setAgentCode(cContInfo.getAgentCode());
        tPayPrintInfo.setManageCom(cContInfo.getManageCom());

        tPayPrintInfo.setReqCom(mGlobalInput.ManageCom);
        tPayPrintInfo.setReqOperator(mGlobalInput.Operator);

        tPayPrintInfo.setPrtType(PrintPDFManagerBL.PT_FRONT);

        tPayPrintInfo.setStateFlag("0");

        tPayPrintInfo.setMakeDate(PubFun.getCurrentDate());
        tPayPrintInfo.setMakeTime(PubFun.getCurrentTime());

        tMMap.put(tPayPrintInfo, SysConst.INSERT);

        return tMMap;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "ApplyRecallPolBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }

}
