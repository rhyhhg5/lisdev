package com.sinosoft.lis.cbcheck;

import java.lang.*;
import java.util.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.cbcheck.*;


/**
 * <p>Title: Web业务系统客户查重功能部分</p>
 * <p>Description: 逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author WHN
 * @version 1.0
 */
public class PersonChkSureBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();


    /** 往后面传输数据的容器 */
    private VData mInputData;


    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    private MMap map = new MMap();


    /** 数据操作字符串 */
    private String mOperate;
    private String mIsueManageCom;
    private String mManageCom;
    private String mpassflag; //通过标记
    private int merrcount; //错误条数
    private String mCalCode; //计算编码
    private String mUser;
    private FDate fDate = new FDate();
    private double mValue;
    private String mAppntNo = ""; //原投保人客户号
    private String mInsuredNo = ""; //与投保人同一人的被保人客户号
    private String mBackObj = "";
    private String mflag = ""; //A 投保人  I b被保人
    private String mSameFlag = "N "; //Y投保人被保人相同 N 不同
    private String mTempFeeNo = ""; //交费收据号
    private String mOldCustomerNo = ""; //原来被保人客户号
    private String mCustomerNo = ""; //客户号


    /** 业务处理相关变量 */
    private LCContSet mAllLCContSet = new LCContSet();
    private LCContSet mLCContSet = new LCContSet();
    private LCContSchema mLCContSchema = new LCContSchema();
    private LCPolSet mLCPolSet = new LCPolSet();
    private LCPolSet mmLCPolSet = new LCPolSet();
    private LCPolSet m2LCPolSet = new LCPolSet();
    private LCPolSet mAllLCPolSet = new LCPolSet();
    private LCPolSchema mLCPolSchema = new LCPolSchema();
    private String mContNo = "";
    private String mPolNo = "";
    private String mOldPolNo = "";


    /** 集体险种单表 */
    private LCGrpPolSet mAllLCGrpPolSet = new LCGrpPolSet();
    private LCGrpPolSet mLCGrpPolSet = new LCGrpPolSet();
    private LCGrpPolSchema mLCGrpPolSchema = new LCGrpPolSchema();


    /** 保费项表 */
    private LCPremSet mLCPremSet = new LCPremSet();
    private LCPremSet mAllLCPremSet = new LCPremSet();


    /** 领取项表 */
    private LCGetSet mLCGetSet = new LCGetSet();
    private LCGetSet mAllLCGetSet = new LCGetSet();


    /** 责任表 */
    private LCDutySet mLCDutySet = new LCDutySet();
    private LCDutySet mAllLCDutySet = new LCDutySet();


    /** 特别约定表 */
    private LCSpecSet mLCSpecSet = new LCSpecSet();
    private LCSpecSet mAllLCSpecSet = new LCSpecSet();


    /** 特别约定注释表 */
    private LCSpecNoteSet mLCSpecNoteSet = new LCSpecNoteSet();
    private LCSpecNoteSet mAllLCSpecNoteSet = new LCSpecNoteSet();


    /** 核保主表 */
    private LCUWMasterSet mLCUWMasterSet = new LCUWMasterSet();
    private LCUWMasterSet mAllLCUWMasterSet = new LCUWMasterSet();
    private LCUWMasterSchema mLCUWMasterSchema = new LCUWMasterSchema();


    /** 核保子表 */
    private LCUWSubSet mLCUWSubSet = new LCUWSubSet();
    private LCUWSubSet mAllLCUWSubSet = new LCUWSubSet();
    private LCUWSubSchema mLCUWSubSchema = new LCUWSubSchema();


    /** 核保错误信息表 */
    private LCUWErrorSet mLCUWErrorSet = new LCUWErrorSet();
    private LCUWErrorSet mAllLCUWErrorSet = new LCUWErrorSet();


    /** 告知表 */
    private LCCustomerImpartSet mLCCustomerImpartSet = new LCCustomerImpartSet();
    private LCCustomerImpartSet mAllLCCustomerImpartSet = new
            LCCustomerImpartSet();


    /** 投保人表 */
    private LCAppntSet mLCAppntSet = new LCAppntSet();
    private LCAppntSet mAllLCAppntSet = new LCAppntSet();


    /** 受益人表 */
    private LCBnfSet mLCBnfSet = new LCBnfSet();
    private LCBnfSet mAllLCBnfSet = new LCBnfSet();


    /** 被保险人表 */
    private LCInsuredSet mLCInsuredSet = new LCInsuredSet();
    private LCInsuredSet mAllLCInsuredSet = new LCInsuredSet();


    /** 体检资料主表 */
    private LCPENoticeSet mLCPENoticeSet = new LCPENoticeSet();
    private LCPENoticeSet mAllLCPENoticeSet = new LCPENoticeSet();
    private LCPENoticeSchema mLCPENoticeSchema = new LCPENoticeSchema();


    /** 体检资料项目表 */
    private LCPENoticeItemSet mLCPENoticeItemSet = new LCPENoticeItemSet();
    private LCPENoticeItemSet mmLCPENoticeItemSet = new LCPENoticeItemSet();
    private LCPENoticeItemSet mAllLCPENoticeItemSet = new LCPENoticeItemSet();


    /** 问题件表 */
    private LCIssuePolSet mLCIssuePolSet = new LCIssuePolSet();
    private LCIssuePolSet mmLCIssuePolSet = new LCIssuePolSet();
    private LCIssuePolSet mAllLCIssuePolSet = new LCIssuePolSet();


    /** 暂交费表 */
    private LJTempFeeSet mLJTempFeeSet = new LJTempFeeSet();
    private LJTempFeeSet mAllLJTempFeeSet = new LJTempFeeSet();


    /** 暂交费关联表 */
    private LJTempFeeClassSchema mLJTempFeeClassSchema = new
            LJTempFeeClassSchema();
    private LJTempFeeClassSet mLJTempFeeClassSet = new LJTempFeeClassSet();
    private LJTempFeeClassSet mmLJTempFeeClassSet = new LJTempFeeClassSet();
    private LJTempFeeClassSet mAllLJTempFeeClassSet = new LJTempFeeClassSet();


    /** 分红计算表 */
    private LOBonusMainSchema mLOBonusMainSchema = new LOBonusMainSchema();
    private LOBonusMainSet mLOBonusMainSet = new LOBonusMainSet();
    private LOBonusMainSet mmLOBonusMainSet = new LOBonusMainSet();
    private LOBonusMainSet m2LOBonusMainSet = new LOBonusMainSet();
    private LOBonusMainSet mAllLOBonusMainSet = new LOBonusMainSet();


    /** 实收总表 */
    private LJAPaySet mLJAPaySet = new LJAPaySet();
    private LJAPaySet mAllLJAPaySet = new LJAPaySet();


    /** 个人实收表 */
    private LJAPayPersonSchema mLJAPayPersonSchema = new LJAPayPersonSchema();
    private LJAPayPersonSet mLJAPayPersonSet = new LJAPayPersonSet();
    private LJAPayPersonSet m2LJAPayPersonSet = new LJAPayPersonSet();
    private LJAPayPersonSet mAllLJAPayPersonSet = new LJAPayPersonSet();


    /** 给付总表*/
    private LJAGetSet mLJAGetSet = new LJAGetSet();
    private LJAGetSet mAllLJAGetSet = new LJAGetSet();


    /** 红利给付表 */
    private LJABonusGetSet mLJABonusGetSet = new LJABonusGetSet();
    private LJABonusGetSet mAllLJABonusGetSet = new LJABonusGetSet();


    /** 帐户表 */
    private LCInsureAccSet mLCInsureAccSet = new LCInsureAccSet();
    private LCInsureAccSet mAllLCInsureAccSet = new LCInsureAccSet();


    /** 帐户轨迹表 */
    private LCInsureAccTraceSet mLCInsureAccTraceSet = new LCInsureAccTraceSet();
    private LCInsureAccTraceSet mAllLCInsureAccTraceSet = new
            LCInsureAccTraceSet();


    /** 暂交费退费应付表 */
    private LJAGetTempFeeSet mLJAGetTempFeeSet = new LJAGetTempFeeSet();
    private LJAGetTempFeeSet mAllLJAGetTempFeeSet = new LJAGetTempFeeSet();


    /**计算公式表**/
    private LMUWSchema mLMUWSchema = new LMUWSchema();


    //private LMUWDBSet mLMUWDBSet = new LMUWDBSet();
    private LMUWSet mLMUWSet = new LMUWSet();

    private CalBase mCalBase = new CalBase();

    public PersonChkSureBL()
    {}


    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        int flag = 0; //判断是不是所有数据都不成功
        int j = 0; //符合条件数据个数

        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;
        System.out.println("---PersonChkSureBL getInputData---");

        // 数据操作业务处理
        if (!dealData())
            return false;
        else
        {
            flag = 1;
        }
        System.out.println("---PersonChkSureBL checkData---");

        if (flag == 0)
        {
            CError tError = new CError();
            tError.moduleName = "PersonChkSureBL";
            tError.functionName = "submitData";
            tError.errorMessage = "校验失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        System.out.println("---PersonChkSureBL dealData---");
        //准备给后台的数据
        prepareOutputData();

        System.out.println("---PersonChkSureBL prepareOutputData---");
        //　数据提交、保存

        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start tPRnewManualDunBLS Submit...");

        if (!tPubSubmit.submitData(mInputData, mOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            CError tError = new CError();
            tError.moduleName = "PersonChkSureBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("---PersonChkSureBL commitData---");

        return true;
    }


    /**
     * 数据操作类业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean dealData()
    {

        if (dealOnePol() == false)
            return false;

        return true;
    }


    /**
     * 操作一张保单的业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean dealOnePol()
    {
        // 健康信息
        if (mflag.equals("A"))
        {
            if (!getACont())
                return false;
            if (prepareAppnt() == false)
                return false;
        }
        if (mflag.equals("I"))
        {
            if (!getICont())
                return false;
            if (prepareInsured() == false)
                return false;
        }

        return true;
    }


    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mOperate = tGlobalInput.Operator;
        mManageCom = tGlobalInput.ManageCom;

        mLCContSchema.setSchema((LCContSchema) cInputData.getObjectByObjectName(
                "LCContSchema", 0));

        int flag = 0; //怕判断是不是所有保单都失败
        int j = 0; //符合条件保单个数

        if (mLCContSchema != null)
        {
            mContNo = mLCContSchema.getContNo();
            mflag = mLCContSchema.getAppFlag();
            if (mflag.equals("I"))
            {
                mOldCustomerNo = mLCContSchema.getInsuredNo(); //原来的被保人客户号
            }
            mCustomerNo = mLCContSchema.getAppntNo(); //要修改为的客户
            System.out.println("Contno:" + mContNo);
            System.out.println("flag:" + mflag);
            System.out.println("CustomerNo:" + mCustomerNo);

        }
        else
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "PersonChkSureBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有传入数据!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    /**
     * 准备更新投保人信息
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean prepareAppnt()
    {
        if (!checkissameA())
            return false;
        CompALCCont();
        CompALCPol();
        CompALCPrem();
        CompALCCustomerImpart(); //客户告知
        CompALCAppnt();
        CompALCUWError();
        CompALCUWMaster();
        CompALCInsureAcc();
        if (mSameFlag.equals("Y"))
        {
            CompALCInsured();
            CompALCGet();
            CompALCPENotice();
        }
        return true;
    }


    /**
     * 准备更新被保人信息
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean prepareInsured()
    {
        if (!checkissameI())
            return false;
        if (mSameFlag.equals("Y"))
        {
            CError tError = new CError();
            tError.moduleName = "PersonChkSureBL";
            tError.functionName = "saveData";
            tError.errorMessage = "要更新的被保人跟投保人是同一人，请选择'投保人校验'按钮!";
            this.mErrors.addOneError(tError);

            return false;
        }

        CompILCCont();
        CompILCPol();
        CompILCCustomerImpart(); //客户告知
        CompILCInsured();
        CompILCUWError();
        CompILCUWMaster();
        CompILCGet();
        CompILCInsureAcc();
        CompILCPENotice();

        return true;
    }


    /**
     * 取相关联保单信息
     */
    private boolean getICont()
    {

        return true;
    }


    /**
     * 取相关联保单信息
     */
    private boolean getACont()
    {
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mContNo);
        mLCContSet = tLCContDB.query();
        if (mLCContSet.size() <= 0)
        {
            return false;
        }
        return true;
    }

    private boolean checkissameA() //该函数作用有：1，查原投保人2，查同一人的被投保人3，置同标志
    {
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        tLCInsuredDB.setContNo(mContNo);
        LCContSchema tLCContSchema = (LCContSchema) mLCContSet.get(1);
        mAppntNo = tLCContSchema.getAppntNo();
        tLCInsuredDB.setInsuredNo(tLCContSchema.getAppntNo());
        if (tLCInsuredDB.getInfo())
        {
            mInsuredNo = tLCContSchema.getAppntNo(); //保存原来的投保人
            mSameFlag = "Y";
        }
        return true;

    }

    private boolean checkissameI() //该函数作用有
    {
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mContNo);
        mLCContSet=tLCContDB.query();
        LCContSchema tLCContSchema = (LCContSchema) mLCContSet.get(1);
        mAppntNo = tLCContSchema.getAppntNo();
        if (mAppntNo.equals(mOldCustomerNo))
        {
            mSameFlag = "Y";
        }
        return true;

    }

    private boolean checkifuseA()
    {
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setAppntNo(mAppntNo);
        LCPolSet tLCPolSet = new LCPolSet();
        tLCPolSet = tLCPolDB.query();
        LCPolDB t2LCPolDB = new LCPolDB();
        t2LCPolDB.setInsuredNo(mAppntNo);
        LCPolSet t2LCPolSet = new LCPolSet();
        t2LCPolSet = t2LCPolDB.query();
        if (tLCPolSet.size() <= 0 && t2LCPolSet.size() <= 0)
        {
            return false;
        }

        return true;

    }
    private boolean checkifuseI()
    {
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setAppntNo(mOldCustomerNo);
        LCPolSet tLCPolSet = new LCPolSet();
        tLCPolSet = tLCPolDB.query();
        LCPolDB t2LCPolDB = new LCPolDB();
        t2LCPolDB.setInsuredNo(mOldCustomerNo);
        LCPolSet t2LCPolSet = new LCPolSet();
        t2LCPolSet = t2LCPolDB.query();
        if (tLCPolSet.size() <= 0 && t2LCPolSet.size() <= 0)
        {
            return false;
        }

        return true;

    }


    /**
     * 更新相关表信息
     */
    private boolean CompALCCont()
    { //保单表
        LCContSchema tLCContSchema = new LCContSchema();
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mContNo);
        if (tLCContDB.getInfo() == false)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PersonChkSureBL";
            tError.functionName = "CompLCCont";
            tError.errorMessage = "没有" + tLCContSchema.getContNo() + "保单!";
            this.mErrors.addOneError(tError);
            return false;
        }
        tLCContSchema = tLCContDB.getSchema();
        if (tLCContSchema.getAppntNo().equals(tLCContSchema.getInsuredNo()))
        {
            tLCContSchema.setAppntNo(mCustomerNo);
            tLCContSchema.setInsuredNo(mCustomerNo);
        }
        else
        {
            tLCContSchema.setAppntNo(mCustomerNo);
        }
        map.put(tLCContSchema, "UPDATE");
        return true;
    }

    private boolean CompALCPol()
    { //险种保单表
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setContNo(mContNo);
        mLCPolSet = tLCPolDB.query();
        for (int i = 1; i <= mLCPolSet.size(); i++)
        {
            LCPolSchema tLCPolSchema = new LCPolSchema();
            tLCPolSchema = mLCPolSet.get(i);

            if (tLCPolSchema.getAppntNo().equals(tLCPolSchema.getInsuredNo()))
            {
                tLCPolSchema.setAppntNo(mCustomerNo);
                tLCPolSchema.setInsuredNo(mCustomerNo);
            }
            else
            {
                tLCPolSchema.setAppntNo(mCustomerNo);
            }
            m2LCPolSet.add(tLCPolSchema);
        }
        map.put(m2LCPolSet, "UPDATE");
        return true;
    }

    private boolean CompALCPrem()
    { //保费
        String sqlstr = "update lcprem set AppntNo='" + mCustomerNo +
                        "' where contno='" + mContNo + "'";
        map.put(sqlstr, "UPDATE");
        return true;
    }

    private boolean CompALCCustomerImpart()
    { //客户告知
        String sqlstr = "";
        if (mSameFlag.equals("Y"))
        {
            sqlstr = "update LCCustomerImpart set CustomerNo='" +
                     mCustomerNo +
                     "' where contno='" + mContNo +
                     "' and CustomerNo='" + mInsuredNo + "'";

        }
        else
        {
            sqlstr = "update LCCustomerImpart set CustomerNo='" +
                     mCustomerNo +
                     "' where contno='" + mContNo +
                     "' and CustomerNoType='0'";
        }
        map.put(sqlstr, "UPDATE");

        return true;
    }

    private boolean CompALCInsured()
    { //被保人表
        String sqlstr = "update LCInsured set InsuredNo='" + mCustomerNo +
                        "' where contno='" + mContNo + "'" +
                        " and InsuredNo='" + mInsuredNo + "'";
        map.put(sqlstr, "UPDATE");

        return true;
    }

    private boolean CompALCAppnt()
    { //投保人信息表
        String sqlstr = "update LCAppnt set AppntNo='" + mCustomerNo +
                        "' where contno='" + mContNo + "'";
        map.put(sqlstr, "UPDATE");
        if (!checkifuseA())
        {
            String sqlstr1 = "delete from LDPerson where CustomerNo='" +
                             mAppntNo + "'";
            map.put(sqlstr1, "DELETE");
        }
        return true;
    }

    private boolean CompALCUWError()
    { //核保错误信息表
        LCUWErrorDB tLCUWErrorDB = new LCUWErrorDB();
        tLCUWErrorDB.setContNo(mContNo);
        LCUWErrorSet tLCUWErrorSet = new LCUWErrorSet();
        tLCUWErrorSet = tLCUWErrorDB.query();
        if (tLCUWErrorSet.size() > 0)
        {
            for (int j = 1; j <= tLCUWErrorSet.size(); j++)
            {
                LCUWErrorSchema tLCUWErrorSchema = new LCUWErrorSchema();
                tLCUWErrorSchema = tLCUWErrorSet.get(j);
                if (tLCUWErrorSchema.getAppntNo().equals(tLCUWErrorSchema.
                        getInsuredNo()))
                {
                    tLCUWErrorSchema.setAppntNo(mCustomerNo);
                    tLCUWErrorSchema.setInsuredNo(mCustomerNo);
                    mLCUWErrorSet.add(tLCUWErrorSchema);
                }
                else
                {
                    tLCUWErrorSchema.setAppntNo(mCustomerNo);
                    mLCUWErrorSet.add(tLCUWErrorSchema);
                }

            }
            map.put(mLCUWErrorSet, "UPDATE");
        }
        return true;
    }

    private boolean CompALCUWMaster()
    { //核保总表
        LCUWMasterDB tLCUWMasterDB = new LCUWMasterDB();
        tLCUWMasterDB.setContNo(mContNo);
        LCUWMasterSet tLCUWMasterSet = new LCUWMasterSet();
        tLCUWMasterSet = tLCUWMasterDB.query();
        if (tLCUWMasterSet.size() > 0)
        {
            for (int j = 1; j <= tLCUWMasterSet.size(); j++)
            {
                LCUWMasterSchema tLCUWMasterSchema = new LCUWMasterSchema();
                tLCUWMasterSchema = tLCUWMasterSet.get(j);
                if (tLCUWMasterSchema.getAppntNo().equals(tLCUWMasterSchema.
                        getInsuredNo()))
                {
                    tLCUWMasterSchema.setAppntNo(mCustomerNo);
                    tLCUWMasterSchema.setInsuredNo(mCustomerNo);
                    mLCUWMasterSet.add(tLCUWMasterSchema);
                }
                else
                {
                    tLCUWMasterSchema.setAppntNo(mCustomerNo);
                    mLCUWMasterSet.add(tLCUWMasterSchema);
                }

            }
            map.put(mLCUWMasterSet, "UPDATE");
        }
        return true;
    }

    private boolean CompALCGet()
    { //领取项目表
        String sqlstr = "update LCGet set InsuredNo='" + mCustomerNo +
                        "' where contno='" + mContNo + "'" +
                        " and InsuredNo='" + mInsuredNo + "'";
        map.put(sqlstr, "UPDATE");

        return true;
    }

    private boolean CompALCInsureAcc()
    { //账户表
        String sqlstr = "update LCInsureAcc set AppntNo='" + mCustomerNo +
                        "' where contno='" + mContNo + "'" +
                        " and AppntNo='" + mInsuredNo + "'";
        map.put(sqlstr, "UPDATE");
        if (mSameFlag.equals("Y"))
        {
            String sqlstr1 = "update LCInsureAcc set InsuredNo='" + mCustomerNo +
                             "' where contno='" + mContNo + "'" +
                             " and InsuredNo='" + mInsuredNo + "'";
            map.put(sqlstr1, "UPDATE");
        }
        return true;
    }

    private boolean CompALCPENotice()
    { //健康告知表
        String sqlstr = "update LCPENotice set CustomerNo='" + mCustomerNo +
                        "' where contno='" + mContNo + "'" +
                        " and CustomerNo='" + mInsuredNo + "'";
        map.put(sqlstr, "UPDATE");

        return true;
    }


    /**
     * 更新相关被保人表信息
     */
    private boolean CompILCCont()
    { //保单表

        LCContSchema tLCContSchema = new LCContSchema();
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mContNo);
        tLCContDB.setInsuredNo(mOldCustomerNo);
        if (tLCContDB.getInfo() == true)
        {
            tLCContSchema = tLCContDB.getSchema();
            if (mOldCustomerNo.equals(tLCContSchema.getInsuredNo()))
            {
                tLCContSchema.setInsuredNo(mCustomerNo);
                mLCContSet.add(tLCContSchema);
                map.put(mLCContSet, "UPDATE");
            }
        }

        return true;
    }

    private boolean CompILCPol()
    { //险种保单表
        String sqlstr = "update LCPol set InsuredNo='" + mCustomerNo +
                        "' where contno='" + mContNo + "'" +
                        " and InsuredNo='" + mOldCustomerNo + "'";
        map.put(sqlstr, "UPDATE");

        return true;
    }


    private boolean CompILCCustomerImpart()
    { //客户告知
        String sqlstr = "update LCCustomerImpart set CustomerNo='" +
                        mCustomerNo +
                        "' where contno='" + mContNo +
                        "' and CustomerNoType='1' and CustomerNo='" +
                        mOldCustomerNo + "'";

        map.put(sqlstr, "UPDATE");

        return true;
    }

    private boolean CompILCInsured()
    { //被保人表
        String sqlstr = "update LCInsured set InsuredNo='" + mCustomerNo +
                        "' where contno='" + mContNo + "' and InsuredNo='" +
                        mOldCustomerNo + "'";
        map.put(sqlstr, "UPDATE");
        if (!checkifuseI())
        {
            String sqlstr1 = "delete from LDPerson where CustomerNo='" +
                             mAppntNo + "'";
            map.put(sqlstr1, "DELETE");
        }
        return true;
    }

    private boolean CompILCUWError()
    { //核保错误信息表
        String sqlstr = "update LCUWError set InsuredNo='" +
                        mCustomerNo +
                        "' where contno='" + mContNo +
                        "'  and InsuredNo='" + mOldCustomerNo + "'";

        map.put(sqlstr, "UPDATE");

        return true;
    }

    private boolean CompILCUWMaster()
    { //核保总表
        String sqlstr = "update LCUWMaster set InsuredNo='" +
                        mCustomerNo +
                        "' where contno='" + mContNo +
                        "'  and InsuredNo='" + mOldCustomerNo + "'";

        map.put(sqlstr, "UPDATE");

        return true;
    }

    private boolean CompILCGet()
    { //领取项目表
        String sqlstr = "update LCGet set InsuredNo='" +
                        mCustomerNo +
                        "' where contno='" + mContNo +
                        "'  and InsuredNo='" + mOldCustomerNo + "'";

        map.put(sqlstr, "UPDATE");

        return true;
    }

    private boolean CompILCInsureAcc()
    { //账户表
        String sqlstr = "update LCInsureAcc set InsuredNo='" +
                        mCustomerNo +
                        "' where contno='" + mContNo +
                        "'  and InsuredNo='" + mOldCustomerNo + "'";

        map.put(sqlstr, "UPDATE");

        return true;
    }

    private boolean CompILCPENotice()
    { //健康告知表
        String sqlstr = "update LCPENotice set CustomerNo='" +
                        mCustomerNo +
                        "' where contno='" + mContNo +
                        "'  and CustomerNo='" + mOldCustomerNo + "'";

        map.put(sqlstr, "UPDATE");

        return true;
    }


    /**
     *准备需要保存的数据
     **/
    private void prepareOutputData()
    {
        mInputData.clear();
        mInputData.add(map);
    }

    public VData getResult()
    {
        return mInputData;
    }
}
