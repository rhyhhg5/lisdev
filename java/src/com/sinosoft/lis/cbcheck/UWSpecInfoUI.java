package com.sinosoft.lis.cbcheck;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.schema.LAAccountsSchema;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 销售管理系统</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author
 * @version 1.0
 */
public class UWSpecInfoUI {
        /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;

    public UWSpecInfoUI() {
    }
  /**
   * 向BL传递的接口
   *
   * @param cInputData VData
   * @param cOperate String
   * @return boolean
   */
  public boolean submitData(VData cInputData, String cOperate) {
          //将操作数据拷贝到本类中
        this.mOperate = cOperate;
      UWSpecInfoBL tUWSpecInfoBL = new UWSpecInfoBL();
      if (!tUWSpecInfoBL.submitData(cInputData, cOperate)) {
          this.mErrors.copyAllErrors(tUWSpecInfoBL.mErrors);
          return false;
      }
      return true;
  }

}
