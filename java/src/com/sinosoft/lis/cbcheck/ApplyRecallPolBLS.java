package com.sinosoft.lis.cbcheck;

import com.sinosoft.utility.*;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.JdbcUrl;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import java.sql.*;
import com.sinosoft.lis.pubfun.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author
 * @version 1.0
 */

public class ApplyRecallPolBLS  {
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors=new CErrors();
  private VData mInputData ;
//  private LCIssuePolSet mLCIssuePolSet ;

  public ApplyRecallPolBLS() {
  }
  public static void main(String[] args) {
//    AddAdditionalRiskBLS proposalBLS1 = new AddAdditionalRiskBLS();
  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    boolean tReturn =false;
    //首先将数据在本类中做一个备份
    mInputData=(VData)cInputData.clone() ;

    tReturn=save();
    if (tReturn)
      System.out.println("Save sucessful");
    else
        System.out.println("Save failed") ;

    mInputData=null;
    return tReturn;
  }

  private boolean save()
  {
    System.out.println("Start Save...");
    Connection conn=DBConnPool.getConnection();
    if (conn==null)
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "ApplyRecallPolBLS";
      tError.functionName = "save";
      tError.errorMessage = "数据库连接失败!";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    try{
      conn.setAutoCommit(false);
      System.out.println("Start 信息...");

      //先删除
      LCApplyRecallPolSet delLCApplyRecallPolSet=((LCApplyRecallPolSet)mInputData.getObjectByObjectName("LCApplyRecallPolSet",1));
      LCApplyRecallPolDBSet tLCApplyRecallPolDBSet = new LCApplyRecallPolDBSet(conn);
      tLCApplyRecallPolDBSet.set(delLCApplyRecallPolSet);
      if (!tLCApplyRecallPolDBSet.delete()) {
        try{ conn.rollback() ;} catch(Exception e){}
        conn.close();
        System.out.println("LCApplyRecallPol Delete Failed");
        return false;
      }

      //撤销申请数据保存
      LCApplyRecallPolSet tLCApplyRecallPolSet = new LCApplyRecallPolSet();
      tLCApplyRecallPolSet=((LCApplyRecallPolSet)mInputData.getObjectByObjectName("LCApplyRecallPolSet",0));
      for (int j=1;j<=tLCApplyRecallPolSet.size();j++)
      {

      LCApplyRecallPolDB tLCApplyRecallPolDB=new LCApplyRecallPolDB(conn);
      tLCApplyRecallPolDB.setSchema(tLCApplyRecallPolSet.get(j));
      if(!tLCApplyRecallPolDB.insert())
      {
        // @@错误处理
        this.mErrors.copyAllErrors(tLCApplyRecallPolDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "ApplyRecallPolBLS";
        tError.functionName = "saveData";
        tError.errorMessage = "撤销申请数据保存失败!";
        this.mErrors .addOneError(tError) ;
        conn.rollback() ;
        conn.close();
        return false;
      }
      }
          conn.commit() ;
          conn.close();
    }
    catch (Exception ex)
    {
      System.out.println("Exception:"+ex.toString());
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="ApplyRecallPolBLS";
      tError.functionName="submitData";
      tError.errorMessage=ex.toString() ;
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
  }

}