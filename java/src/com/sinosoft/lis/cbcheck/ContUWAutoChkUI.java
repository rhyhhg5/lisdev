package com.sinosoft.lis.cbcheck;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description:承保团体自动核保功能类
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author WHN
 * @version 1.0
 */
public class ContUWAutoChkUI {
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors = new CErrors();

  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();

  /** 往界面传输数据的容器 */
  private VData mResult = new VData();

  /** 数据操作字符串 */
  private String mOperate;

  public ContUWAutoChkUI() {}

// @Main
  public static void main(String[] args) {
    GlobalInput tG = new GlobalInput();
    tG.Operator = "aaa";
    LCPolSchema p = new LCPolSchema();
    LCGrpPolSchema g = new LCGrpPolSchema();

    p.setProposalNo("00010220020110000002");
    p.setPolNo("00010220020110000002");

    g.setGrpProposalNo("00010120020120000005");
    g.setGrpPolNo("00010120020120000005");

    LCPolSet tLCPolSet = new LCPolSet();
    LCGrpPolSet tLCGrpPolSet = new LCGrpPolSet();

    tLCPolSet.add(p);
    tLCGrpPolSet.add(g);

    VData tVData = new VData();
    tVData.add(tLCPolSet);
    tVData.add(tLCGrpPolSet);
    tVData.add(tG);

    GrpUWAutoChkUI ui = new GrpUWAutoChkUI();
    if (ui.submitData(tVData, "") == true)
      System.out.println("---ok---");
    else
      System.out.println("---NO---");
  }

  /**
     传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate) {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;

    ContUWAutoChkBL tContUWAutoChkBL = new ContUWAutoChkBL();

    System.out.println("---ContUWAutoChkBL UI BEGIN---");
    if (tContUWAutoChkBL.submitData(cInputData, mOperate) == false) {
      // @@错误处理
      this.mErrors.copyAllErrors(tContUWAutoChkBL.mErrors);
      CError tError = new CError();
      tError.moduleName = "ContUWAutoChkUI";
      tError.functionName = "submitData";
      tError.errorMessage = "数据查询失败!";
      this.mErrors.addOneError(tError);
      showerror(mErrors);
      mResult.clear();
      return false;
    }

    return true;
  }

  public void showerror(CErrors tErrors) {
    String terror = "";
    CError tError = new CError();
    int n = tErrors.getErrorCount();
    if (n > 0) {
      for (int i = 0; i < n; i++) {
        tError = tErrors.getError(i);
        terror = terror + i + ". " + tError.errorMessage.trim() + ".";
      }
      System.out.println("errormessage: " + terror);
    }
  }
}
