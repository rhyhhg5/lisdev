package com.sinosoft.lis.cbcheck;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
/**
 * <p>Title: Web业务系统承保个人单状态查询部分</p>
 * <p>Description:接口功能类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author WHN
 * @version 1.0
 */
public class PolStatusChkUI
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  public PolStatusChkUI() {}

// @Main
  public static void main(String[] args)
  {
  GlobalInput tG = new GlobalInput();
  tG.Operator = "picch";
  LCContSchema p = new LCContSchema();
  p.setProposalContNo( "13000016717" );
  p.setContNo("13000016717");

  LCContSet tLCContSet = new LCContSet();
  tLCContSet.add(p);

  //p = new LCContSchema();
  //p.setProposalNo( "86220020020110000001" );
  //p.setPolNo("86220020020110000001");
  //tLCContSet.add(p);

  VData tVData = new VData();
  tVData.add( tLCContSet );
  tVData.add( tG );
  PolStatusChkUI ui = new PolStatusChkUI();
  if( ui.submitData( tVData, "" ) == true )
      System.out.println("---ok---");
  else
      System.out.println("---NO---");


  CErrors tError = ui.mErrors;
  int n = tError.getErrorCount();

  }

  /**
  传输数据的公共方法
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;

    PolStatusChkBL tPolStatusChkBL = new PolStatusChkBL();

    System.out.println("---PolStatusChkUI BEGIN---");
    if (tPolStatusChkBL.submitData(cInputData,mOperate) == false)
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tPolStatusChkBL.mErrors);
      CError tError = new CError();
      tError.moduleName = "PolStatusChkUI";
      tError.functionName = "submitData";
      //tError.errorMessage = "数据查询失败!";
      this.mErrors .addOneError(tError) ;
      mResult.clear();
      return false;
    }
    else
    {
      mResult = tPolStatusChkBL.getResult();
      this.mErrors.copyAllErrors(tPolStatusChkBL.mErrors);
    }

    return true;
  }

  public VData getResult()
  {
    return mResult;
  }
}
