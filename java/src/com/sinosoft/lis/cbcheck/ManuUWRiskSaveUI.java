package com.sinosoft.lis.cbcheck;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCUWMasterSchema;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.LCPremSchema;
import com.sinosoft.lis.vschema.LCPremSet;
import com.sinosoft.lis.schema.LCSpecSchema;
import com.sinosoft.lis.vschema.LCSpecSet;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: SinoSoft</p>
 * @author HYQ
 * @version 1.0
 */

public class ManuUWRiskSaveUI {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();


    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    private VData mInputData;

    /** 数据操作字符串 */
    private String mOperate;

    public ManuUWRiskSaveUI() {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        this.mOperate = cOperate;

        ManuUWRiskSaveBL tManuUWRiskSaveBL = new ManuUWRiskSaveBL();

        System.out.println("--------DisDesb Start!---------");
        tManuUWRiskSaveBL.submitData(cInputData, cOperate);
        System.out.println("--------DisDesb End!---------");

        //如果有需要处理的错误，则返回
        if (tManuUWRiskSaveBL.mErrors.needDealError()) {
            this.mErrors.copyAllErrors(tManuUWRiskSaveBL.mErrors);
        }
        System.out.println("error num=" + mErrors.getErrorCount());
        mInputData = null;

        return true;
    }

    /**
     * 返回结果方法
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }


    public static void main(String[] args) {
        VData tVData = new VData();
        GlobalInput mGlobalInput = new GlobalInput();

        /** 全局变量 */
        mGlobalInput.Operator = "001";
        mGlobalInput.ComCode = "86";
        mGlobalInput.ManageCom = "86";
        TransferData mTransferData = new TransferData();

        /**
         * 免责信息
         */
//        LCSpecSchema tLCSpecSchema = new LCSpecSchema();
//        LCSpecSet tLCSpecSet = new LCSpecSet();
//        tLCSpecSchema.setSpecCode("2");
//        tLCSpecSchema.setSpecContent("");
//        //tLCSpecSchema.setPayStartDate(tSpecStartDate[i]);
//        //tLCSpecSchema.setPayEndDate(tSpecEndDate[i]);
//        tLCSpecSet.add(tLCSpecSchema);
//
//        tLCSpecSchema = new LCSpecSchema();
//        tLCSpecSchema.setSpecCode("3");
//        tLCSpecSchema.setSpecContent("");
//        tLCSpecSet.add(tLCSpecSchema);
//
//        tLCSpecSchema = new LCSpecSchema();
//        tLCSpecSchema.setSpecCode("4");
//        tLCSpecSchema.setSpecContent("");
//        tLCSpecSet.add(tLCSpecSchema);
//        tVData.add(tLCSpecSet);


        LCUWMasterSchema tLCUWMasterSchema = new LCUWMasterSchema();
        /**
         * 加费
         */
        LCPremSchema tLCPremSchema = new LCPremSchema();
        LCPremSet tLCPremSet = new LCPremSet();
        //tLCPremSchema.setPrem("500");
        tLCPremSchema.setRate("0.15");
//        tLCPremSchema.setPayStartDate("2005-01-01");
//        tLCPremSchema.setPayEndDate("2005-12-31");
        tLCPremSet.add(tLCPremSchema);
        mTransferData.setNameAndValue("AddFeeFlag", "L");
        tVData.add(tLCPremSet);

        tLCUWMasterSchema.setPolNo("11000048528");
        tLCUWMasterSchema.setProposalNo("11000048528");
        tLCUWMasterSchema.setPassFlag("4");
        tLCUWMasterSchema.setUWIdea("52145832dfsdfsf");
        tLCUWMasterSchema.setSugPassFlag("S");

//        mTransferData.setNameAndValue("SpecFlag","E");

//        mTransferData.setNameAndValue("AddPrem","200");
//        mTransferData.setNameAndValue("SpecReason","sdfsdfsdfsdfsdf");
        tVData.add(mGlobalInput);
//
        tVData.add(tLCUWMasterSchema);
        tVData.add(mTransferData);
        ManuUWRiskSaveUI tManuUWRiskSaveUI = new ManuUWRiskSaveUI();
        try {
            if (tManuUWRiskSaveUI.submitData(tVData, "")) {

            } else {
                System.out.println("error:" +
                                   tManuUWRiskSaveUI.mErrors.getError(0).
                                   errorMessage);
            }
        } catch (Exception e) {
            System.out.println("error:" + e);
        }

    }

}
