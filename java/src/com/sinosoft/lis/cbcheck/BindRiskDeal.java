package com.sinosoft.lis.cbcheck;

import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LCUWMasterDB;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LCUWMasterSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * 绑定产品变更承保客户回复处理类
 * 
 * @author 张成轩
 */
public class BindRiskDeal {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	private CErrors mErrors = new CErrors();

	/** 待更新LCPolSet */
	private LCPolSet mLCPolSet = new LCPolSet();

	/** 待更新LCUWMasterSet */
	private LCUWMasterSet mLCUWMasterSet = new LCUWMasterSet();

	/**
	 * 获取绑定产品险种信息(个险)
	 * 
	 * @param tLCPolSchema
	 *            险种信息Schema集合
	 * @return 保单下绑定险种LCPolSet集合
	 */
	public boolean bindRiskDeal(LCContSchema tLCContSchema) {
		// 校验传入的字段是否为空
		if (tLCContSchema == null || tLCContSchema.getContNo() == null) {
			addError("传入的查询信息为空", "bindRiskDeal");
			return false;
		}

		// 获取保单号
		String contNo = tLCContSchema.getContNo();
		// 查询是否有变更承保且客户意见为不同意的险种mainpolno,对一组绑定产品仅有一个mainpolno
		String querySql = "select distinct polno from LCUWMaster where contno='"
				+ contNo + "' and passflag='4' and CustomerReply='01'";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = tExeSQL.execSQL(querySql);
		System.out.println("--记录数--" + tSSRS.MaxRow);

		// 对每个mainpolno依此循环
		for (int row = 1; row <= tSSRS.MaxRow; row++) {
			LCPolSchema tLCPolSchema = new LCPolSchema();
			tLCPolSchema.setPolNo(tSSRS.GetText(row, 1));
			GetBindRisk tGetBindRisk = new GetBindRisk();
			// 获取绑定产品集合
			LCPolSet tLCPolSet = tGetBindRisk.getBindRisk(tLCPolSchema);
			if (tLCPolSet == null) {
				this.mErrors.copyAllErrors(tGetBindRisk.getErrors());
				return false;
			}
			// 对绑定产品每个险种依此进行校验
			for (int setRow = 1; setRow <= tLCPolSet.size(); setRow++) {
				String polno = tLCPolSet.get(setRow).getPolNo();
				// 校验核保结论及客户回复意见
				LCUWMasterDB tLCUWMasterDB = new LCUWMasterDB();
				tLCUWMasterDB.setProposalNo(polno);
				if (!tLCUWMasterDB.getInfo()) {
					addError("查询险种信息核保结论异常", "bindRiskDeal");
					return false;
				}
				String passFlag = tLCUWMasterDB.getPassFlag();
				if (!passFlag.matches("a|1|8")) {
					// 若不为拒保类修改核保结论及客户回复意见
					tLCUWMasterDB.setPassFlag("4");
					// 客户回复意见为不同意
					tLCUWMasterDB.setCustomerReply("01");

					LCPolDB tLCPolDB = new LCPolDB();
					tLCPolDB.setPolNo(polno);
					if (!tLCPolDB.getInfo()) {
						addError("查询险种信息异常", "bindRiskDeal");
						return false;
					}
					// 将险种核保结论修改为撤单
					tLCPolDB.setUWFlag("a");

					// 设置待更新返回数据
					mLCUWMasterSet.add(tLCUWMasterDB.getSchema());
					mLCPolSet.add(tLCPolDB.getSchema());
				}
			}
		}
		return true;
	}

	/**
	 * 获取待更新LCUWMasterSet
	 * 
	 * @return
	 */
	public LCUWMasterSet getLCUWMasterSet() {
		return this.mLCUWMasterSet;
	}

	/**
	 * 获取待更新LCPolSet
	 * 
	 * @return
	 */
	public LCPolSet getLCPolSet() {
		return this.mLCPolSet;
	}

	/**
	 * 添加错误信息
	 * 
	 * @param message
	 *            错误信息
	 * @param function
	 *            错误产生函数名
	 */
	private void addError(String message, String function) {
		// 添加错误信息
		CError tError = new CError();
		tError.moduleName = "CheckBindRisk";
		tError.functionName = function;
		tError.errorMessage = message;
		this.mErrors.addOneError(tError);
		System.out.println(message);
	}

	/**
	 * 返回错误
	 * 
	 * @return 错误信息集合
	 */
	public CErrors getErrors() {
		return this.mErrors;
	}

	/**
	 * 调用示例
	 */
	public static void main(String[] arr) {

	}
}
