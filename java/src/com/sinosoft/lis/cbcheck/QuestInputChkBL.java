package com.sinosoft.lis.cbcheck;

import java.lang.*;
import java.util.*;

import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.cbcheck.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;


/**
 * <p>Title: Web业务系统问题件录入部分</p>
 * <p>Description: 逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author WHN
 * @modified by ZhangRong  2004.11
 * @version 1.0
 */
public class QuestInputChkBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 往界面传输数据的容器 */
    private MMap mMap = new MMap();
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperator;
    private String mOperate;
    private String mIsueManageCom;
    private String mManageCom;
    private FDate fDate = new FDate();
    private int mflag = 0; //1:个单 2:团单

    /** 业务处理相关变量 */
    private LCContSet mLCContSet = new LCContSet();
    private LCContSet mAllLCContSet = new LCContSet();
    private LCContSchema mLCContSchema = new LCContSchema();
    private String mContNo = "";
    private String mOperatorPos = "";

    /** 核保主表 */
    private LCCUWMasterSchema mLCCUWMasterSchema = null; //new LCCUWMasterSchema();
    private LCGCUWMasterSchema mLCGCUWMasterSchema = null; //new LCGCUWMasterSchema();

    /** 问题件表 */
    private LCIssuePolSet mLCIssuePolSet = new LCIssuePolSet();
    private LCIssuePolSet mmLCIssuePolSet = new LCIssuePolSet();
    private LCIssuePolSet mAllLCIssuePolSet = new LCIssuePolSet();

    public QuestInputChkBL() {
    }

    /**
     * 传输数据的公共方法
     *
     * @param: cInputData 输入的数据 cOperate 数据操作
     * @return:
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        int flag = 0; //判断是不是所有数据都不成功

        //将操作数据拷贝到本类中，此类做数据提交使用
        mInputData = (VData) cInputData.clone();
        mOperate = cOperate;
        System.out.println("---1---");

        //得到外部传入的数据,将数据备份到本类中
        System.out.println("---QuestInputChkBL calling getInputData---");

        if (!getInputData(cInputData)) {
            return false;
        }

        //取所在机构号码
        if (!GetManageCom(mContNo)) {
            return false;
        }

        System.out.println("---QuestInputChkBL checkData---");
        if (!checkData()) {
            return false;
        }

        System.out.println("---QuestInputChkBL dealData---");

        // 数据操作业务处理
        if (!dealData(mLCContSchema)) {
            return false;
        } else {
            flag = 1;
        }

        if (flag == 0) {
            CError tError = new CError();
            tError.moduleName = "QuestInputChkBL";
            tError.functionName = "submitData";
            tError.errorMessage = "没有自动通过保单!";
            this.mErrors.addOneError(tError);

            return false;
        }

        System.out.println("---QuestInputChkBL dealData---");

        //准备给后台的数据
        prepareOutputData();

        System.out.println("---QuestInputChkBL prepareOutputData---");

        //数据提交
        mResult.add(mMap);

        PubSubmit tSubmit = new PubSubmit();

        if (!tSubmit.submitData(mResult, "")) {
            // @@错误处理
            this.mErrors.copyAllErrors(tSubmit.mErrors);

            CError tError = new CError();
            tError.moduleName = "QuestInputChkBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        System.out.println("---QuestInputChkBL commitData---");

        return true;
    }

    /**
     * 数据操作类业务处理 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     *
     * @param tLCContSchema LCContSchema
     * @return boolean
     */
    private boolean dealData(LCContSchema tLCContSchema) {
        if (dealOnePol() == false) {
            return false;
        }

        return true;
    }

    /**
     * 操作一张保单的业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     *
     * @return boolean
     */
    private boolean dealOnePol() {
        // 健康信息
        if (prepareQuest() == false) {
            return false;
        }

        LCIssuePolSet tLCIssuePolSet = new LCIssuePolSet();
        tLCIssuePolSet.set(mLCIssuePolSet);
        mAllLCIssuePolSet.add(tLCIssuePolSet);

        LCContSet tLCContSet = new LCContSet();
        //tLCContSet.set(mLCContSet);
        mAllLCContSet.add(tLCContSet);

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mOperator = tGlobalInput.Operator;
        mManageCom = tGlobalInput.ManageCom;

        mLCContSet.set((LCContSet) cInputData.getObjectByObjectName("LCContSet",
                0));
        mLCIssuePolSet.set((LCIssuePolSet) cInputData.getObjectByObjectName(
                "LCIssuePolSet", 0));

        System.out.println(mLCIssuePolSet.get(1).getQuestionObj());

        if ((mLCContSet != null) && (mLCContSet.size() > 0)) {
            mLCContSchema = (LCContSchema) mLCContSet.get(1);
            mContNo = mLCContSchema.getContNo();
            System.out.println("mContNo=" + mContNo);

            LCContDB tLCContDB = new LCContDB();
            tLCContDB.setContNo(mContNo);

            LCContSet tLCContSet = tLCContDB.query();

            if (tLCContSet.size() <= 0) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "QuestInputChkBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "在合同表中无法查到合同号为" + mContNo + " 的合同信息!";
                this.mErrors.addOneError(tError);

                return false;
            }

            mLCContSchema.setSchema(tLCContSet.get(1));
        } else {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "QuestInputChkBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有传入合同信息!";
            this.mErrors.addOneError(tError);

            return false;
        }

        if (mLCIssuePolSet.size() <= 0) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "QuestInputChkBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有传入问题件信息!";
            this.mErrors.addOneError(tError);

            return false;
        }

        return true;
    }

    /**
     * 取所在机构
     *
     * @param tContNo String
     * @return boolean
     */
    private boolean GetManageCom(String tContNo) {
        LCIssuePolSchema tLCIssuePolSchema = new LCIssuePolSchema();
        tLCIssuePolSchema = mLCIssuePolSet.get(1);
        tLCIssuePolSchema.setGrpContNo(mLCContSchema.getGrpContNo());
        tLCIssuePolSchema.setContNo(mLCContSchema.getContNo());
        tLCIssuePolSchema.setProposalContNo(mLCContSchema.getContNo());
        if (!tContNo.equals("")) {
            if (mLCContSchema.getGrpContNo().equals("00000000000000000000")) {
                mflag = 1; //个单
            } else {
                mflag = 2; //团单
            }

            if (mflag == 1) { // 目前只针对团体单( flag.equals("12"))录入问题件所以该分支目前是不会的到执行
                mIsueManageCom = mLCContSchema.getManageCom();

                if (tLCIssuePolSchema.getOperatePos().equals("0")) { //如果从录单界面进入问题件录入

                    if (tLCIssuePolSchema.getBackObjType().equals("1")) { //如果问题件转给录单员本人

                        CError tError = new CError();
                        tError.moduleName = "QuestInputChkBL";
                        tError.functionName = "GetManageCom";
                        tError.errorMessage = "严重警告：系统禁止操作员录入返回给自己（操作员）的问题件!";
                        this.mErrors.addOneError(tError);

                        return false;
                    }
                    return true;
                }

                if (tLCIssuePolSchema.getOperatePos().equals("1")) {
                }

                //复核处录入了返回给操作员的问题件
                if (tLCIssuePolSchema.getOperatePos().equals("5") &&
                    tLCIssuePolSchema.getBackObjType().equals("1")) {
                    mLCContSchema.setUWFlag("0");
                    mLCContSchema.setApproveCode(mOperator);
                    mLCContSchema.setApproveFlag("1");
                    mLCContSchema.setApproveDate(PubFun.getCurrentDate());
                }
            }

            if (mflag == 2) { //目前团体单录入问题件不置任何标志,只是在问题件表中添加一条记录
                mIsueManageCom = mLCContSchema.getManageCom();

                if ((tLCIssuePolSchema.getOperatePos().equals("5") &&
                     tLCIssuePolSchema.getBackObjType().equals("1"))) {
                    //新单复核处给该团体单录入了返回给操作员的问题件
                }
            }
        }

        return true;
    }

    private boolean Getbackobj() {
        return true;
    }

    /**
     * 准备体检资料信息 输出：如果发生错误则返回false,否则返回true
     *
     * @return boolean
     */
    private boolean prepareQuest() {
        String tPrint = "";
        LCIssuePolSchema tLCIssuePolSchema = mLCIssuePolSet.get(1);
        mOperatorPos = tLCIssuePolSchema.getOperatePos();
        String tSerialNo = "";
        String tPrintSerialNo = "";
        if(StrTool.cTrim(this.mOperate).equals("INSERT")){
            tSerialNo = PubFun1.CreateMaxNo("QustSerlNo", 20);
            tPrintSerialNo = PubFun1.CreateMaxNo("PRTSEQ2NO", 20);
        }else{

        }

        //    //个人单默认打印标记
        if (tLCIssuePolSchema.getBackObjType().equals("3") ||
            tLCIssuePolSchema.getBackObjType().equals("2")) { //保户,业务员
            tPrint = "Y";
        } else {
            tPrint = "N";
        }

        //操作员
        tLCIssuePolSchema.setBackObj(mLCContSchema.getOperator());

        //tLCIssuePolSchema.setContNo(mContNo);
        //tLCIssuePolSchema.setGrpContNo(mLCContSchema.getGrpContNo());
        //tLCIssuePolSchema.setProposalContNo(mLCContSchema.getProposalContNo());
        tLCIssuePolSchema.setIssueType(tLCIssuePolSchema.getIssueType().trim()); //设置问题类型
        if (StrTool.cTrim(this.mOperate).equals("INSERT")) {
            tLCIssuePolSchema.setSerialNo(tSerialNo.toString());
            tLCIssuePolSchema.setPrtSeq(tPrintSerialNo.toString());
        }

        tLCIssuePolSchema.setIsueManageCom(mIsueManageCom);
        tLCIssuePolSchema.setPrintCount(0);
        tLCIssuePolSchema.setNeedPrint(tPrint);
        tLCIssuePolSchema.setReplyMan("");
        tLCIssuePolSchema.setReplyResult("");
        tLCIssuePolSchema.setState("");
        tLCIssuePolSchema.setOperator(mOperator);
        tLCIssuePolSchema.setManageCom(mManageCom);
        tLCIssuePolSchema.setMakeDate(PubFun.getCurrentDate());
        tLCIssuePolSchema.setMakeTime(PubFun.getCurrentTime());
        tLCIssuePolSchema.setModifyDate(PubFun.getCurrentDate());
        tLCIssuePolSchema.setModifyTime(PubFun.getCurrentTime());

        mLCIssuePolSet.clear();
        mLCIssuePolSet.add(tLCIssuePolSchema);

        if (mOperatorPos.equals("1")) {
            //核保主表信息
            if (mflag == 1) {
                LCCUWMasterDB tLCCUWMasterDB = new LCCUWMasterDB();
                tLCCUWMasterDB.setContNo(mContNo);
                //校验是否以发核保通知书
                if (tLCCUWMasterDB.getPrintFlag() == null) {
                } else {
                    if (tLCCUWMasterDB.getPrintFlag().equals("1") ||
                        tLCCUWMasterDB.getPrintFlag().equals("4")) {
                        // @@错误处理
                        CError tError = new CError();
                        tError.moduleName = "QuestInputChkBL";
                        tError.functionName = "prepareQuest";
                        tError.errorMessage = "已经发核保通知书，不可录入!";
                        this.mErrors.addOneError(tError);

                        return false;
                    }
                }

                mLCCUWMasterSchema = tLCCUWMasterDB.getSchema();
                mLCCUWMasterSchema.setQuesFlag("1");
            } else if (mflag == 2) {
                LCGCUWMasterDB tLCGCUWMasterDB = new LCGCUWMasterDB();
                tLCGCUWMasterDB.setProposalGrpContNo(mLCContSchema.getGrpContNo());
                System.out.println("----grppolno:----" + mContNo);
                System.out.println("----State:----" + tLCGCUWMasterDB.getState());

                //校验是否以发核保通知书(目前团单下录入的问题件与核保通知书无任何关系)
                if (tLCGCUWMasterDB.getState() == null) {
                } else {
                }

                mLCGCUWMasterSchema = tLCGCUWMasterDB.getSchema();
            }
        }

        return true;
    }

    /**
     *准备需要保存的数据
     **/
    private void prepareOutputData() {
        if(StrTool.cTrim(this.mOperate).equals("INSERT")){
            mMap.put(mAllLCIssuePolSet, "INSERT");
            mMap.put(mAllLCContSet, "UPDATE");
        }
        if(StrTool.cTrim(this.mOperate).equals("DELETE")){
            mMap.put(mAllLCIssuePolSet, "DELETE");
            //mMap.put(mAllLCContSet, "UPDATE");
        }
        if (mLCCUWMasterSchema != null) {
//			mMap.put(mLCCUWMasterSchema, "UPDATE");
        }

        if (mLCGCUWMasterSchema != null) {
//			mMap.put(mLCGCUWMasterSchema, "UPDATE");
        }
    }

    private boolean checkData() {

        String cflag = String.valueOf(mflag);
        if (cflag.equals("1")) { //判断是否为个单
            if (mOperate.equals("DELETE")) {
                String tflag = "";
                for (int i = 1; i <= mLCIssuePolSet.size(); i++) {
                    if ((StrTool.cTrim(mLCIssuePolSet.get(i).getState()).equals(
                            "1")) ||
                        (StrTool.cTrim(mLCIssuePolSet.get(i).getState()).equals(
                                "2")) ||
                        (StrTool.cTrim(mLCIssuePolSet.get(i).getState()).equals(
                                "3"))) {
                        tflag = "1";
                    }
                }
                if (tflag.equals("1")) {
                    CError tError = new CError();
                    tError.moduleName = "QuestInputChkBL";
                    tError.functionName = "checkData";
                    tError.errorMessage = "已经下发过了问题件，不能删除!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
            }

        }
        return true;
    }
}
