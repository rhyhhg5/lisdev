package com.sinosoft.lis.cbcheck;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title:核保抽检上报通用程序 </p>
 * <p>Description: 新契约个险和团险投保单的核保抽检、上报、下发等处理程序 </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: SinoSoft</p>
 * @author HYQ
 * @version 1.0
 * 修 改 人：闫少杰
 * 修改日期：2006-02-24
 * 修改内容:
 *          1 增加系统自动上报时对核保上报原因实体表处理上报原因记录的保存
 *                说明： 主动上报不在核保上报原因实体表保存上报原因记录；
 */

public class UWSendTraceAllBL
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    //private VData mInputData;
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /** 往工作流引擎中传输数据的容器 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private TransferData mTransferData = new TransferData();

    /** 数据操作字符串 */
    private String mOperator;

    private String mManageCom;

    //private String mOperate;
    private String mActivityID;

    private String mNeedSendNotice;

    /** 业务逻辑类*/
    private LCUWSendTraceSchema mLCUWSendTraceSchema = new LCUWSendTraceSchema();

    private LWMissionSchema mLWMissionSchema = new LWMissionSchema();

    private LCCUWMasterSchema mLCCUWMasterSchema = new LCCUWMasterSchema();

    private LCCUWSubSchema mLCCUWSubSchema = new LCCUWSubSchema();

    private LDUWUserSchema mLDUWUserSchema = new LDUWUserSchema();

    private LDSpotTrackSet mLDSpotTrackSet = new LDSpotTrackSet();

    private LCGCUWMasterSchema mLCGCUWMasterSchema = new LCGCUWMasterSchema();

    private LCGCUWSubSchema mLCGCUWSubSchema = new LCGCUWSubSchema();

    private LDSpotUWRateSchema mLDSpotUWRateSchema = new LDSpotUWRateSchema();

    private LCCUWMasterSchema mNewLCCUWMasterSchema = new LCCUWMasterSchema();

    private LCGCUWMasterSchema mNewLCGCUWMasterSchema = new LCGCUWMasterSchema();

    private LCGUWMasterSet mLCGUWMasterSet = new LCGUWMasterSet();

    private LCGUWSubSet mLCGUWSubSet = new LCGUWSubSet();

    //2006-02-24 闫少杰 增加系统自动上报时对核保上报原因实体表处理上报原因记录的保存 ----- START
    private LCUWSendReasonSchema mLCUWSendReasonSchema;

    private LCUWSendReasonSet mLCUWSendReasonSet = new LCUWSendReasonSet();

    //2006-02-24 闫少杰 增加系统自动上报时对核保上报原因实体表处理上报原因记录的保存 ----- END

    /** 业务数据字符串 */
    private String mOtherNoType = "0"; //其他编码类型，用于表示是何种类型上报或抽检。  1-个险新契约，2-团险新契约

    private String mSendFlag = "0"; //上报标志

    private String mSendType = ""; //上报类型标志，1-普通上报 2-抽检上报

    private String strSql = ""; //查询SQL语句字符串

    private String mYesOrNo; //是否同意标志，表示核保师本人是否同意上报、打回、转发的核保任务。 ====== 是否同意标志

    private String mUserCode; //用户代码,用于记录需要跳转到的用户

    private String mGrpSendUpFlag; //团单主动上报标志

    private String mPSendUpFlag; //个单主动上报标志

    public UWSendTraceAllBL()
    {
    }

    /**
     * 传输数据的公共方法
     *
     * @param: cInputData 输入的数据 cOperate 数据操作
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData, cOperate))
        {
            return false;
        }

        //校验前台传入数据是否有效业务数据
        if (!checkData())
        {
            return false;
        }

        System.out.println("Start  dealData...");
        //进行业务处理
        if (!dealData())
        {
            return false;
        }
        System.out.println("dealData successful!");

        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }

        System.out.println("Start  Submit...");
        //提交数据
        if (cOperate.equals("submit"))
        {
            PubSubmit tSubmit = new PubSubmit();
            if (!tSubmit.submitData(mResult, ""))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "UWSendTraceAllBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }

        return true;
    }

    /**
     * 准备返回前台统一存储数据 输出：如果发生错误则返回false,否则返回true
     *
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        mResult.clear();
        MMap map = new MMap();

        if (mOtherNoType.equals("1"))
        { //个险
            System.out.println("在准备向数据库中插入数值时 : " + mSendFlag);
            if (mSendFlag.equals("1")
                    || (mSendFlag.equals("9") && mYesOrNo.equals("N")))
            {
                if (!this.strSql.equals("") && this.strSql != null)
                {
                    map.put(this.strSql, "DELETE");
                }
                map.put(mLWMissionSchema, "DELETE&INSERT");
                map.put(mNewLCCUWMasterSchema, "UPDATE");
                map.put(mLCCUWSubSchema, "INSERT");
            }
        }
        else if (mOtherNoType.equals("2"))
        { //团险
            if (mSendFlag.equals("1")
                    || (mSendFlag.equals("9") && mYesOrNo.equals("N")))
            {
                map.put(mLWMissionSchema, "UPDATE");
                map.put(mNewLCGCUWMasterSchema, "UPDATE");
                map.put(mLCGCUWSubSchema, "INSERT");
                map.put(mLCGUWMasterSet, "UPDATE");
                map.put(mLCGUWSubSet, "INSERT");
            }
            //2006-02-24 闫少杰 增加系统自动上报时对核保上报原因实体表处理上报原因记录的保存 ----- START
            if (mSendType.equals("1") || mSendFlag.equals("1"))
            {
                map.put(mLCUWSendReasonSet, "INSERT");
            }
            //2006-02-24 闫少杰 增加系统自动上报时对核保上报原因实体表处理上报原因记录的保存 ----- END
        }

        if (mLCUWSendTraceSchema != null
                && mLCUWSendTraceSchema.getOtherNo() != null
                && mLCUWSendTraceSchema.getOtherNoType() != null)
        {
            map.put(mLCUWSendTraceSchema, "INSERT");
        }

        map.put(mLDSpotTrackSet, "INSERT");
        mTransferData.setNameAndValue("SendFlag", mSendFlag);
        mTransferData.setNameAndValue("UserCode", mUserCode);
        mTransferData.setNameAndValue("SendType", mSendType);
        mTransferData.removeByName("NeedSendNotice");
        mResult.add(mTransferData);
        mResult.add(map);
        return true;
    }

    /**
     * 校验业务数据
     *
     * @return boolean
     */
    private boolean checkData()
    {
        LDSpotUWRateDB tLDSpotUWRateDB = new LDSpotUWRateDB();

        String tUWType = "";
        if ("1".equals(mOtherNoType))
        {
            tUWType = "01";
        }
        else if ("2".equals(mOtherNoType))
        {
            tUWType = "02";
        }

        //校验核保师权限表
        LDUWUserDB tLDUWUserDB = new LDUWUserDB();
        tLDUWUserDB.setUserCode(mOperator);
        tLDUWUserDB.setUWType(tUWType);
        if (!tLDUWUserDB.getInfo())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "UWSendTraceAllBL";
            tError.functionName = "checkData";
            tError.errorMessage = "未定义核保师权限!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLDUWUserSchema = tLDUWUserDB.getSchema();
        this.mUserCode = mLDUWUserSchema.getUpUserCode();
        if (mOtherNoType.equals("1"))
        {
            //校验核保主表
            LCCUWMasterDB tLCCUWMasterDB = new LCCUWMasterDB();
            tLCCUWMasterDB.setProposalContNo(mLCCUWMasterSchema
                    .getProposalContNo());
            System.out.println("mLCCUWMasterSchema.getContNo() "
                    + mLCCUWMasterSchema.getContNo());
            //            if (!tLCCUWMasterDB.getInfo()) {
            //                // @@错误处理
            //                CError tError = new CError();
            //                tError.moduleName = "UWSendTraceAllBL";
            //                tError.functionName = "checkData";
            //                tError.errorMessage = "查询核保主表失败!";
            //                this.mErrors.addOneError(tError);
            //                return false;
            //            }
            LCCUWMasterSet tLCCUWMasterSet = tLCCUWMasterDB.query();
            if (tLCCUWMasterSet.size() <= 0 || tLCCUWMasterSet.size() > 1)
            {
                buildError("checkData", "查询核保主表失败！");
                return false;
            }
            mNewLCCUWMasterSchema = tLCCUWMasterSet.get(1).getSchema();

            //校验核保师抽检比例
            tLDSpotUWRateDB.setUserCode(mOperator);
            tLDSpotUWRateDB.setUWType("01"); //个险抽检类型为01
            if (!tLDSpotUWRateDB.getInfo())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "UWSendTraceAllBL";
                tError.functionName = "checkData";
                tError.errorMessage = "核保师个险抽检比例未定义!";
                this.mErrors.addOneError(tError);
                return false;
            }
            mLDSpotUWRateSchema = tLDSpotUWRateDB.getSchema();

        }
        else if (mOtherNoType.equals("2"))
        {
            LCGCUWMasterDB tLCGCUWMasterDB = new LCGCUWMasterDB();
            tLCGCUWMasterDB.setProposalGrpContNo(mLCGCUWMasterSchema
                    .getGrpContNo());
            if (!tLCGCUWMasterDB.getInfo())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "UWSendTraceAllBL";
                tError.functionName = "checkData";
                tError.errorMessage = "查询集体核保主表失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            String newPassFlag = mLCGCUWMasterSchema.getPassFlag();
            String newUWIdea = mLCGCUWMasterSchema.getUWIdea();
            mNewLCGCUWMasterSchema = tLCGCUWMasterDB.getSchema();
            mNewLCGCUWMasterSchema.setPassFlag(newPassFlag);
            mNewLCGCUWMasterSchema.setUWIdea(newUWIdea);

            //校验核保师抽检比例
            tLDSpotUWRateDB.setUserCode(mOperator);
            tLDSpotUWRateDB.setUWType("02"); //团险抽检类型为02
            if (!tLDSpotUWRateDB.getInfo())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "UWSendTraceAllBL";
                tError.functionName = "checkData";
                tError.errorMessage = "核保师团险抽检比例未定义!";
                this.mErrors.addOneError(tError);
                return false;
            }
            mLDSpotUWRateSchema = tLDSpotUWRateDB.getSchema();

        }

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    private boolean getInputData(VData cInputData, String cOperate)
    {
        //从输入数据中得到所有对象
        //获得全局公共数据
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        mSendType = StrTool.cTrim((String) mTransferData
                .getValueByName("SendType"));
        if (mSendType.equals(""))
        {
            mSendType = "1";
        }
        if (mGlobalInput == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "UWSendTraceAllBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //获得操作员编码
        mOperator = mGlobalInput.Operator;
        if (mOperator == null || mOperator.trim().equals(""))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "UWSendTraceAllBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据mOperator失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //获得管理机构编码
        mManageCom = mGlobalInput.ManageCom;
        if (mManageCom == null || mManageCom.trim().equals(""))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "UWSendTraceAllBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据mOperator失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //获得上报轨迹表数据
        mLCUWSendTraceSchema = (LCUWSendTraceSchema) mTransferData
                .getValueByName("LCUWSendTraceSchema");

        if (mLCUWSendTraceSchema == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "UWSendTraceAllBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据mLCUWSendTraceSchema失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //获得抽检类型标志  1-个险，2-团险
        mOtherNoType = mLCUWSendTraceSchema.getOtherNoType();
        if (mOtherNoType == null
                || mOtherNoType.trim().equals("")
                || (!mOtherNoType.trim().equals("1") && !mOtherNoType.trim()
                        .equals("2")))
        {
            CError tError = new CError();
            tError.moduleName = "UWSendTraceAllBL";
            tError.functionName = "getInputData";
            // @@错误处理
            tError.errorMessage = "前台传输保单类型标志失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //获取外部数据时，考虑到程序的通用型，每种类型写一个getInputData方法。
        if (mOtherNoType.equals("1"))
        { //新契约个险
            if (!getInputPersonData())
            { //获得个险核保主表数据
                return false;
            }
        }
        else if (mOtherNoType.equals("2"))
        { //新契约团险
            if (!getInputGrpData())
            { //获得团险核保主表数据
                return false;
            }
        }

        //获取是否同意标志，通过该字段来标识初次进行核保校验（核保权限不通过则自动上报），还是主动上报
        mYesOrNo = mLCUWSendTraceSchema.getYesOrNo();
        System.out.println("同意标志 " + mYesOrNo);
        if (mYesOrNo == null || mYesOrNo.length() == 0)
        {
            mYesOrNo = "First"; //如果同意标志为空或为0则表示是初次进行上报检查，置为"First"
        }
        mActivityID = (String) mTransferData.getValueByName("ActivityID");
        mGrpSendUpFlag = (String) mTransferData.getValueByName("GrpSendUpFlag");
        mNeedSendNotice = (String) mTransferData
                .getValueByName("NeedSendNotice");
        System.out.println(" mGrpSendUpFlag : " + mGrpSendUpFlag);
        //判断个单是否为主动上报
        mPSendUpFlag = (String) mTransferData.getValueByName("PSendUpFlag");
        System.out.println(" mPSendUpFlag : " + mPSendUpFlag);
        return true;
    }

    /**
     * getInputGrpData
     * 获得团险数据
     * @return boolean
     */
    private boolean getInputGrpData()
    {
        //获得集体核保主表数据
        mLCGCUWMasterSchema = (LCGCUWMasterSchema) mTransferData
                .getValueByName("LCGCUWMasterSchema");
        if (mLCGCUWMasterSchema == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "UWSendTraceAllBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输集体核保主表数据失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * getInputPersonData
     * 获得个险数据
     * @return boolean
     */
    private boolean getInputPersonData()
    {
        //获得上核保主表表数据
        mLCCUWMasterSchema = (LCCUWMasterSchema) mTransferData
                .getValueByName("LCCUWMasterSchema");
        if (mLCCUWMasterSchema == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "UWSendTraceAllBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输核保主表数据失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
     *
     * @return boolean
     */
    private boolean dealData()
    {
    	//对绑定险种进行校验      by zhangchengxuan 2012-6-20
    	if(!checkBindRisk()){
    		return false;
    	}
    	
        if (mYesOrNo.equals("First"))
        { //如果为第一次进入，需要进行校验核保级别，然后进行抽检。
            //校验核保级别，如果核保师级别不够，则上报
            if (!checkGrade())
            {
                return false;
            }

            //抽检
            if (!spotUW())
            {
                return false;
            }
            
            if(!checkMarkettypeUW())
            {
            	return false;
            }

            //准备核保业务数据
            if (!prepareUW())
            {
                return false;
            }

            //准备核保轨迹表数据
            if (!prepareSendTrace())
            {
                return false;
            }
        }
        else if (mYesOrNo.equals("Y"))
        {
            //校验核保级别，如果核保师级别不够，则上报
            if (!checkGrade())
            {
                return false;
            }
            
            if(!checkMarkettypeUW())
            {
            	return false;
            }

            //准备核保业务数据
            if (!prepareUW())
            {
                return false;
            }

            //准备核保轨迹表数据
            if (!prepareSendTrace())
            {
                return false;
            }
        }
        else if (mYesOrNo.equals("N"))
        {
            //准备核保业务数据
            if (!prepareUW())
            {
                return false;
            }

            //准备核保轨迹表数据
            if (!prepareSendTrace())
            {
                return false;
            }

            //将上报标志置为下报，下报即打回
            mSendFlag = "9";
        }

        //准备工作流表
        if (!prepareMission())
        {
            return false;
        }

        return true;
    }

    /**
     * prepareMission
     * 准备工作流轨迹表
     * @return boolean
     */
    private boolean prepareMission()
    {
        if (mSendFlag.equals("1")
                || (mSendFlag.equals("9") && (mYesOrNo.equals("N"))))
        { //核保上报
            LWMissionDB tLWMissionDB = new LWMissionDB();
            if (mOtherNoType.equals("1"))
            {
                tLWMissionDB.setActivityID(mActivityID); //人工核保工作流节点
                tLWMissionDB.setMissionProp2(mLCUWSendTraceSchema.getOtherNo());
            }
            else if (mOtherNoType.equals("2"))
            {
                tLWMissionDB.setActivityID("0000002004"); //人工核保工作流节点
                tLWMissionDB.setMissionProp1(mLCUWSendTraceSchema.getOtherNo());
            }

            LWMissionSet tLWMissionSet = tLWMissionDB.query();
            if (tLWMissionSet == null || tLWMissionSet.size() <= 0)
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "UWSendTraceAllBL";
                tError.functionName = "checkData";
                tError.errorMessage = "查询核保主表失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            mLWMissionSchema = tLWMissionSet.get(1);
            if (mOtherNoType.equals("1"))
            {
                strSql = "delete from LWMission where activityid='0000001100' and "
                        + "MissionProp2 ='"
                        + mLCUWSendTraceSchema.getOtherNo()
                        + "'";
                String UpReport = "";
                if (mYesOrNo.equals("N"))
                {
                    mLWMissionSchema.setActivityID("0000001160");
                    mLWMissionSchema.setDefaultOperator(mUserCode);
                    mLWMissionSchema.setMissionProp11(this.mOperator);

                    //0--上报 1--下报
                    mLWMissionSchema.setMissionProp12("1");
                }
                else
                {
                    mLWMissionSchema.setActivityID("0000001160");
                    mLWMissionSchema.setDefaultOperator(mLDUWUserSchema
                            .getUpUserCode());
                    mLWMissionSchema.setMissionProp11(mLDUWUserSchema
                            .getUpUserCode());
                    mUserCode = mLDUWUserSchema.getUpUserCode();
                    /**
                     * 区分受检上报和普通上报 2--抽检上报
                     */
                    if (mSendType.equals("2"))
                    {
                        UpReport = "2";
                    }
                    else
                    {
                        UpReport = "0";
                    }
                    mLWMissionSchema.setMissionProp12(UpReport);
                }
                mLWMissionSchema.setMissionProp13(this.mNeedSendNotice);
            }
            if (mOtherNoType.equals("2"))
            {
                strSql = "delete from LWMission where activityid='0000002004' and "
                        + "MissionProp1 ='"
                        + mNewLCGCUWMasterSchema.getGrpContNo() + "'";
                String UpReport = "";
                if (mYesOrNo.equals("N"))
                {
                    mLWMissionSchema.setActivityID("0000002004");
                    mLWMissionSchema.setDefaultOperator(mUserCode);
                    mLWMissionSchema.setMissionProp11(this.mOperator);

                    //0--上报 1--下报
                    mLWMissionSchema.setMissionProp12("1");
                }
                else
                {
                    mLWMissionSchema.setActivityID("0000002004");
                    mLWMissionSchema.setDefaultOperator(mLDUWUserSchema
                            .getUpUserCode());
                    mLWMissionSchema.setMissionProp11(mLDUWUserSchema
                            .getUpUserCode());
                    mUserCode = mLDUWUserSchema.getUpUserCode();
                    /**
                     * 区分受检上报和普通上报 2--抽检上报
                     */
                    if (mSendType.equals("2"))
                    {
                        UpReport = "2";
                    }
                    else
                    {
                        UpReport = "0";
                    }
                    mLWMissionSchema.setMissionProp12(UpReport);
                }
            }
        }

        return true;
    }

    /**
     * prepareSendTrace
     * 准备核保处理轨迹表数据
     * @return boolean
     */
    private boolean prepareSendTrace()
    {
        int uwno = 0; //核保顺序号
        LCUWSendTraceDB tLCUWSendTraceDB = new LCUWSendTraceDB();
        //        tLCUWSendTraceDB.setOtherNo(mLCUWSendTraceSchema.getOtherNo());
        //        tLCUWSendTraceDB.setOtherNoType(mOtherNoType);
        StringBuffer sql = new StringBuffer(255);
        sql.append("select * from lcuwsendtrace where otherno='");
        sql.append(mLCUWSendTraceSchema.getOtherNo());
        sql.append("' and othernotype='");
        sql.append(mOtherNoType);
        sql.append("' order by uwno desc");
        LCUWSendTraceSet tLCUWSendTraceSet = new LCUWSendTraceSet();
        tLCUWSendTraceSet = tLCUWSendTraceDB.executeQuery(sql.toString());
        if (tLCUWSendTraceSet == null || tLCUWSendTraceSet.size() == 0)
        {
            uwno = 1;
            mLCUWSendTraceSchema.setUWCode(mOperator);
            mLCUWSendTraceSchema.setUWPopedom(getPopedom(mOperator, "01"));
        }
        else
        {
            sql = new StringBuffer();
            sql.append("select downuwcode  from  lcuwsendtrace where otherno='");
            sql.append(this.mLCUWSendTraceSchema.getOtherNo());
            sql.append("' and sendtype='1' and upusercode='");
            sql.append(this.mOperator);
            sql.append("'");

            mUserCode = (new ExeSQL()).getOneValue(sql.toString());
            if (mUserCode == null || mUserCode.equals("")
                    || mUserCode.equals("null"))
            {
                //buildError("prepareSendTrace", "没有找到下级操作员编码！");
                //return false;
                mUserCode = tLCUWSendTraceSet.get(1).getDownUWCode();
            }

            uwno = tLCUWSendTraceSet.get(1).getUWNo() + 1;

            mLCUWSendTraceSchema.setUWCode(mUserCode);
            mLCUWSendTraceSchema.setUWPopedom(getPopedom(mUserCode, "01"));
        }

        mLCUWSendTraceSchema.setUWNo(uwno);
        mLCUWSendTraceSchema.setOperator(mOperator);
        mLCUWSendTraceSchema.setManageCom(mManageCom);
        mLCUWSendTraceSchema.setMakeDate(PubFun.getCurrentDate());
        mLCUWSendTraceSchema.setMakeTime(PubFun.getCurrentTime());
        mLCUWSendTraceSchema.setModifyDate(PubFun.getCurrentDate());
        mLCUWSendTraceSchema.setModifyTime(PubFun.getCurrentTime());
        System.out.println("YYYYYY" + mSendType);
        mLCUWSendTraceSchema.setSendType(mSendType);
        if (mLCUWSendTraceSchema.getYesOrNo() != null
                && mLCUWSendTraceSchema.getYesOrNo().equals("N"))
        {
            mLCUWSendTraceSchema.setSendFlag("0");
            mLCUWSendTraceSchema.setUpUserCode(mOperator);
            mLCUWSendTraceSchema.setUpUWPopedom(getPopedom(mOperator, "01"));
            mLCUWSendTraceSchema.setDownUWCode(mUserCode);
            mLCUWSendTraceSchema.setDownUWPopedom(getPopedom(mOperator, "01"));
        }
        else
        {
            mLCUWSendTraceSchema.setSendFlag("1");
            mLCUWSendTraceSchema.setUpUserCode(mLDUWUserSchema.getUpUserCode());
            mLCUWSendTraceSchema.setUpUWPopedom(getPopedom(mLDUWUserSchema
                    .getUpUserCode(), "01"));
            mLCUWSendTraceSchema.setDownUWCode(mOperator);
            mLCUWSendTraceSchema.setDownUWPopedom(getPopedom(mOperator, "01"));
        }

        //2006-02-24 闫少杰 增加系统自动上报时对核保上报原因实体表处理上报原因记录的保存 ----- START
        if (mLCUWSendReasonSet.size() > 0)
        {
            for (int i = 1; i <= mLCUWSendReasonSet.size(); i++)
            {
                mLCUWSendReasonSet.get(i).setOtherNo(
                        mLCUWSendTraceSchema.getOtherNo());
                mLCUWSendReasonSet.get(i).setOtherNoType(mOtherNoType);
                mLCUWSendReasonSet.get(i).setUWNo(uwno);
                mLCUWSendReasonSet.get(i).setSerialNo(
                        PubFun1.CreateMaxNo("UWSendReasonSN", 20));
                mLCUWSendReasonSet.get(i).setSendType(mSendType);
                mLCUWSendReasonSet.get(i).setUpUserCode(mUserCode);
                System.out.println("系统上报原因" + i + "是："
                        + mLCUWSendReasonSet.get(i).getReason());
                mLCUWSendReasonSet.get(i).setOperator(mOperator);
                mLCUWSendReasonSet.get(i).setManageCom(mManageCom);
                mLCUWSendReasonSet.get(i).setMakeDate(PubFun.getCurrentDate());
                mLCUWSendReasonSet.get(i).setMakeTime(PubFun.getCurrentTime());
                mLCUWSendReasonSet.get(i)
                        .setModifyDate(PubFun.getCurrentDate());
                mLCUWSendReasonSet.get(i)
                        .setModifyTime(PubFun.getCurrentTime());
            }
        }
        //2006-02-24 闫少杰 增加系统自动上报时对核保上报原因实体表处理上报原因记录的保存 ----- END

        return true;
    }

    /**
     * prepareUW
     * 准备核保业务数据
     * @return boolean
     */
    private boolean prepareUW()
    {
        if (mOtherNoType.equals("1"))
        {
            if (!preparePersonUW())
            {
                return false;
            }
            System.out.println("在处理prepareUW 中" + this.mSendFlag);
        }
        else if (mOtherNoType.equals("2"))
        {
            if (!prepareGrpUW())
            {
                return false;
            }
        }
        return true;
    }

    /**
     * prepareGrpUW
     * 准备团险数据
     * @return boolean
     */
    private boolean prepareGrpUW()
    {
        mNewLCGCUWMasterSchema.setModifyDate(PubFun.getCurrentDate());
        mNewLCGCUWMasterSchema.setModifyTime(PubFun.getCurrentTime());
        mNewLCGCUWMasterSchema.setOperator(mOperator);
        //        mNewLCGCUWMasterSchema.setPassFlag("6"); //上报
        mNewLCGCUWMasterSchema.setUWIdea(mLCUWSendTraceSchema.getUWIdea());
        mNewLCGCUWMasterSchema.setUWNo(mNewLCGCUWMasterSchema.getUWNo() + 1);

        // 合同核保轨迹表
        LCGCUWSubDB tLCGCUWSubDB = new LCGCUWSubDB();
        tLCGCUWSubDB.setGrpContNo(mNewLCGCUWMasterSchema.getGrpContNo());
        LCGCUWSubSet tLCGCUWSubSet = new LCGCUWSubSet();
        tLCGCUWSubSet = tLCGCUWSubDB.query();
        if (tLCGCUWSubDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCGCUWSubDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "UWSendTraceAllBL";
            tError.functionName = "prepareUW";
            tError.errorMessage = mLCCUWMasterSchema.getContNo()
                    + "合同核保轨迹表查失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        int nUWNo = tLCGCUWSubSet.size();
        if (nUWNo > 0)
        {
            nUWNo++;
            mLCGCUWSubSchema.setUWNo(nUWNo); //第几次核保
        }
        else
        {
            mLCGCUWSubSchema.setUWNo(1); //第1次核保
        }

        mLCGCUWSubSchema = tLCGCUWSubSet.get(1).getSchema();
        mLCGCUWSubSchema.setUWNo(nUWNo);
        //mLCGCUWSubSchema.setPassFlag("6");
        mLCGCUWSubSchema.setUWIdea(mLCUWSendTraceSchema.getUWIdea());
        mLCGCUWSubSchema.setOperator(mOperator);
        mLCGCUWSubSchema.setMakeDate(PubFun.getCurrentDate());
        mLCGCUWSubSchema.setMakeTime(PubFun.getCurrentTime());
        mLCGCUWSubSchema.setModifyDate(PubFun.getCurrentDate());
        mLCGCUWSubSchema.setModifyTime(PubFun.getCurrentTime());

        LCGUWSubDB tLCGUWSubDB;
        LCGUWSubSchema tLCGUWSubSchema = new LCGUWSubSchema();
        int uwno = 0;
        LCGUWMasterDB tLCGUWMasterDB = new LCGUWMasterDB();
        tLCGUWMasterDB.setGrpContNo(mNewLCGCUWMasterSchema.getGrpContNo());
        mLCGUWMasterSet = tLCGUWMasterDB.query();
        if (mLCGUWMasterSet == null)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCGCUWSubDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "UWSendTraceAllBL";
            tError.functionName = "prepareUW";
            tError.errorMessage = "集体险种核保轨迹表查失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        for (int i = 1; i <= mLCGUWMasterSet.size(); i++)
        {
            uwno = mLCGUWMasterSet.get(i).getUWNo(); //获得核保序号
            //mLCGUWMasterSet.get(i).setPassFlag("6");
            mLCGUWMasterSet.get(i).setUWIdea(mLCUWSendTraceSchema.getUWIdea());
            mLCGUWMasterSet.get(i).setModifyDate(PubFun.getCurrentDate());
            mLCGUWMasterSet.get(i).setUWNo(uwno + 1);
            mLCGUWMasterSet.get(i).setModifyTime(PubFun.getCurrentTime());
            mLCGUWMasterSet.get(i).setOperator(mOperator);

            tLCGUWSubDB = new LCGUWSubDB();
            tLCGUWSubDB.setGrpProposalNo(mLCGUWMasterSet.get(i)
                    .getGrpProposalNo());
            tLCGUWSubDB.setGrpPolNo(mLCGUWMasterSet.get(i).getGrpPolNo());
            tLCGUWSubDB.setUWNo(uwno);
            if (!tLCGUWSubDB.getInfo())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLCGCUWSubDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "UWSendTraceAllBL";
                tError.functionName = "prepareUW";
                tError.errorMessage = "集体险种核保轨迹表查失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            tLCGUWSubSchema = tLCGUWSubDB.getSchema();
            //tLCGUWSubSchema.setPassFlag("6");
            tLCGUWSubSchema.setUWIdea(mLCUWSendTraceSchema.getUWIdea());
            tLCGUWSubSchema.setModifyDate(PubFun.getCurrentDate());
            tLCGUWSubSchema.setUWNo(uwno + 1);
            tLCGUWSubSchema.setModifyTime(PubFun.getCurrentTime());
            tLCGUWSubSchema.setOperator(mOperator);

            mLCGUWSubSet.add(tLCGUWSubSchema);
        }

        return true;
    }

    /**
     * preparePersonUW
     * 准备个险数据
     * @return boolean
     */
    private boolean preparePersonUW()
    {
        mNewLCCUWMasterSchema.setUWNo(mNewLCCUWMasterSchema.getUWNo() + 1);
        mNewLCCUWMasterSchema.setState(mLCUWSendTraceSchema.getUWFlag());
        mNewLCCUWMasterSchema.setAutoUWFlag("2"); // 1 自动核保 2 人工核保
        mNewLCCUWMasterSchema.setOperator(mOperator); //操作员
        mNewLCCUWMasterSchema.setModifyDate(PubFun.getCurrentDate());
        mNewLCCUWMasterSchema.setModifyTime(PubFun.getCurrentTime());
        //mNewLCCUWMasterSchema.setPassFlag("6"); //上报
        mNewLCCUWMasterSchema.setSugPassFlag(mLCUWSendTraceSchema.getUWFlag());
        mNewLCCUWMasterSchema.setSugUWIdea(mLCUWSendTraceSchema.getUWIdea());
        // 合同核保轨迹表
        LCCUWSubDB tLCCUWSubDB = new LCCUWSubDB();
        tLCCUWSubDB.setContNo(mNewLCCUWMasterSchema.getContNo());
        LCCUWSubSet tLCCUWSubSet = new LCCUWSubSet();
        tLCCUWSubSet = tLCCUWSubDB.query();
        if (tLCCUWSubDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCCUWSubDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "UWSendTraceAllBL";
            tError.functionName = "prepareUW";
            tError.errorMessage = mLCCUWMasterSchema.getContNo()
                    + "合同核保轨迹表查失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        int nUWNo = tLCCUWSubSet.size();
        if (nUWNo > 0)
        {
            nUWNo++;
            mLCCUWSubSchema.setUWNo(nUWNo); //第几次核保
        }
        else
        {
            mLCCUWSubSchema.setUWNo(1); //第1次核保
        }

        mLCCUWSubSchema.setContNo(mNewLCCUWMasterSchema.getContNo());
        mLCCUWSubSchema.setGrpContNo(mNewLCCUWMasterSchema.getGrpContNo());
        mLCCUWSubSchema.setProposalContNo(mNewLCCUWMasterSchema
                .getProposalContNo());
        mLCCUWSubSchema.setInsuredNo(mNewLCCUWMasterSchema.getInsuredNo());
        mLCCUWSubSchema.setInsuredName(mNewLCCUWMasterSchema.getInsuredName());
        mLCCUWSubSchema.setAppntNo(mNewLCCUWMasterSchema.getAppntNo());
        mLCCUWSubSchema.setAppntName(mNewLCCUWMasterSchema.getAppntName());
        mLCCUWSubSchema.setAgentCode(mNewLCCUWMasterSchema.getAgentCode());
        mLCCUWSubSchema.setAgentGroup(mNewLCCUWMasterSchema.getAgentGroup());
        mLCCUWSubSchema.setUWGrade(mNewLCCUWMasterSchema.getUWGrade()); //核保级别
        mLCCUWSubSchema.setAppGrade(mNewLCCUWMasterSchema.getAppGrade()); //申请级别
        mLCCUWSubSchema.setAutoUWFlag(mNewLCCUWMasterSchema.getAutoUWFlag());
        mLCCUWSubSchema.setState(mNewLCCUWMasterSchema.getState());
        mLCCUWSubSchema.setPassFlag(mNewLCCUWMasterSchema.getState());
        mLCCUWSubSchema.setPostponeDay(mNewLCCUWMasterSchema.getPostponeDay());
        mLCCUWSubSchema
                .setPostponeDate(mNewLCCUWMasterSchema.getPostponeDate());
        mLCCUWSubSchema.setUpReportContent(mNewLCCUWMasterSchema
                .getUpReportContent());
        mLCCUWSubSchema.setHealthFlag(mNewLCCUWMasterSchema.getHealthFlag());
        mLCCUWSubSchema.setSpecFlag(mNewLCCUWMasterSchema.getSpecFlag());
        mLCCUWSubSchema.setSpecReason(mNewLCCUWMasterSchema.getSpecReason());
        mLCCUWSubSchema.setQuesFlag(mNewLCCUWMasterSchema.getQuesFlag());
        mLCCUWSubSchema.setReportFlag(mNewLCCUWMasterSchema.getReportFlag());
        mLCCUWSubSchema.setChangePolFlag(mNewLCCUWMasterSchema
                .getChangePolFlag());
        mLCCUWSubSchema.setChangePolReason(mNewLCCUWMasterSchema
                .getChangePolReason());
        mLCCUWSubSchema.setAddPremReason(mNewLCCUWMasterSchema
                .getAddPremReason());
        mLCCUWSubSchema.setPrintFlag(mNewLCCUWMasterSchema.getPrintFlag());
        mLCCUWSubSchema.setPrintFlag2(mNewLCCUWMasterSchema.getPrintFlag2());
        mLCCUWSubSchema.setUWIdea(mNewLCCUWMasterSchema.getUWIdea());
        mLCCUWSubSchema.setOperator(mNewLCCUWMasterSchema.getOperator()); //操作员
        mLCCUWSubSchema.setManageCom(mNewLCCUWMasterSchema.getManageCom());
        mLCCUWSubSchema.setMakeDate(PubFun.getCurrentDate());
        mLCCUWSubSchema.setMakeTime(PubFun.getCurrentTime());
        mLCCUWSubSchema.setModifyDate(PubFun.getCurrentDate());
        mLCCUWSubSchema.setModifyTime(PubFun.getCurrentTime());
        mLCCUWSubSchema.setSugPassFlag(mLCUWSendTraceSchema.getUWFlag());
        mLCCUWSubSchema.setSugUWIdea(mLCUWSendTraceSchema.getUWIdea());

        return true;
    }

    /**
     * spotUW
     * 是否需要抽检
     * @return boolean
     */
    private boolean spotUW()
    {
        //不需要上报则进行抽检
        if (mSendFlag.equals("0"))
        {
            SpotPrepare tSpotPrepare = new SpotPrepare();
            if (!tSpotPrepare.PrepareData(mLCUWSendTraceSchema.getOtherNo(),
                    mOperator, getPopedom(mOperator, "01"), mLDSpotUWRateSchema
                            .getUWType(), "1", true))
            { //编码规则见pdm
                if (tSpotPrepare.mErrors.needDealError())
                {
                    this.mErrors.copyAllErrors(tSpotPrepare.mErrors);
                }
                else
                {
                    mLDSpotTrackSet = tSpotPrepare.getLDSpotTrackSet();
                }
            }
            else
            {
                mLDSpotTrackSet = tSpotPrepare.getLDSpotTrackSet();
                mSendFlag = "1"; //1-上报 0-不上报或下报
                mSendType = "2"; //1-普通上报 2-抽检上报
            }

            mLCUWSendTraceSchema.setSendType(mSendType);
        }

        return true;
    }

    /**
     * 绑定产品拒保类核保结论校验
     * 
     * @return 校验标志
     */
    private boolean checkBindRisk(){
    	// 仅对个险进行处理
    	if (mOtherNoType.equals("1")){ 
    		CheckBindRisk tCheckBindRisk = new CheckBindRisk();
    		LCContSchema tLCContSchema = new LCContSchema();
    		tLCContSchema.setContNo(mNewLCCUWMasterSchema.getContNo());
    		if (!tCheckBindRisk.checkUWFlag(tLCContSchema)) {
    			this.mErrors.addOneError(tCheckBindRisk.getErrors().getError(0));
    			return false;
    		}
        }
        return true;
    }
    
    /**
     * checkGrade
     * 校验核保级别，如果核保师级别不够，则上报
     * @return boolean
     */
    private boolean checkGrade()
    {

        //获取外部数据时，考虑到程序的通用型，每种类型写一个getInputData方法。
        if (mOtherNoType.equals("1"))
        { //新契约个险
            LCContDB tLCContDB = new LCContDB();
            tLCContDB.setContNo(mNewLCCUWMasterSchema.getContNo());
            if (!tLCContDB.getInfo())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "UWSendTraceAllBL";
                tError.functionName = "checkGrade";
                tError.errorMessage = "合同表查失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            LCContSchema tLCContSchema = tLCContDB.getSchema();
            LCPolDB tLCPolDB = new LCPolDB();
            tLCPolDB.setContNo(tLCContSchema.getContNo());
            LCPolSet tLCPolSet = tLCPolDB.query();
            if (tLCPolSet.size() <= 0)
            {
                //System.out.println("程序第856行出错，请检查UWSendTraceAllBL.java中的checkGrade方法！");
                CError tError = new CError();
                tError.moduleName = "UWSendTraceAllBL.java";
                tError.functionName = "checkGrade";
                tError.errorMessage = "查询该保单下险种信息失败！";
                this.mErrors.addOneError(tError);
                return false;
            }
            //判断个单是否为主动上报，是则直接上报
            System.out.println("---------mPSendUpFlag---------" + mPSendUpFlag);
            if (StrTool.cTrim(mLDUWUserSchema.getPopUWFlag()).equals("1"))
            {
                //超级权限,不校验核保权限
                mSendFlag = "0"; //不需要上报
                return true;
            }

            if (StrTool.cTrim(mPSendUpFlag).equals("3"))
            {
                mSendFlag = "1"; //上报
                mSendType = "1"; //主动上报;
                System.out.println("mmmmmm" + mSendFlag);
                return true;
            }
            //根据险种校验，加费，免责核保级别是否对应。
            //处理方式：查询出加费最多，免责最多的数据，
            //根据最大值校验核保级别
            System.out.println("开始查询加费信息和免责信息");
            String uwGrade = mLDUWUserSchema.getUWPopedom();
            LCUWMasterDB tLCUWMasterDB = new LCUWMasterDB();
            tLCUWMasterDB.setContNo(tLCContSchema.getContNo());
            LCUWMasterSet tLCUWMasterSet = tLCUWMasterDB.query();
            int specCount = 0;
            double addFee = 0.0;
            for (int i = 1; i <= tLCUWMasterSet.size(); i++)
            {
                if (!StrTool.cTrim(tLCUWMasterSet.get(i).getSpecFlag()).equals(
                        "0"))
                {
                    StringBuffer sql = new StringBuffer();
                    sql
                            .append("select count(1) from lcspec where ProposalNo='");
                    sql.append(tLCUWMasterSet.get(i).getProposalNo());
                    sql.append("'");
                    ExeSQL tExeSQL = new ExeSQL();
                    if (specCount < Integer.parseInt(tExeSQL.getOneValue(sql
                            .toString())))
                    {
                        specCount = Integer.parseInt(tExeSQL.getOneValue(sql
                                .toString()));
                    }
                }
                if (!StrTool.cTrim(tLCUWMasterSet.get(i).getAddPremFlag())
                        .equals("0")
                        && !StrTool.cTrim(
                                tLCUWMasterSet.get(i).getAddPremFlag()).equals(
                                ""))
                {
                    StringBuffer sql = new StringBuffer();
                    sql.append("select prem from lcprem where PolNo='");
                    sql.append(tLCUWMasterSet.get(i).getProposalNo());
                    sql.append("' and payplancode like '000000%'");
                    ExeSQL tExeSQL = new ExeSQL();
                    String strAddFee = tExeSQL.getOneValue(sql.toString());
                    String POlsql = "select sum(prem),riskcode from lcpol where polno='"
                            + tLCUWMasterSet.get(i).getProposalNo()
                            + "' group by riskcode";
                    SSRS strPOlFee = tExeSQL.execSQL(POlsql);
                    double addfee = 0;
                    double POlFee = 0;
                    if (!strAddFee.equals(""))
                    {
                        addfee = Double.parseDouble(strAddFee);
                    }
                    if (!strPOlFee.GetText(1, 1).equals(""))
                    {
                        POlFee = Double.parseDouble(strPOlFee.GetText(1, 1));
                    }
                    //效验保单险种一级加费比例
                    System.out.println("此险种最大加费 ： " + addfee);

                    double RiskaddFeeRate = (addfee / (POlFee - addfee)) * 100;
                    LDUWGradeDB tRiskLDUWGradeDB = new LDUWGradeDB();
                    tRiskLDUWGradeDB.setUWGrade(uwGrade);
                    tRiskLDUWGradeDB.setContFlag("1");
                    tRiskLDUWGradeDB.setRiskType(getRiskType(strPOlFee.GetText(
                            1, 2)));
                    LDUWGradeSet tLDUWGradeSet = tRiskLDUWGradeDB.query();
                    //首先校验加费免责是否超过级别
                    if (RiskaddFeeRate > tLDUWGradeSet.get(1).getAddPrem())
                    {
                        //加费超过核保权限
                        mSendFlag = "1"; //上报
                        mSendType = "1"; //普通上报
                        /** 需要记录下上报的原因 */

                        return true;
                    }

                    if (addFee < addfee)
                    {
                        addFee = Double.parseDouble(tExeSQL.getOneValue(sql
                                .toString()));
                    }
                }
            }
            System.out.println("此单最大加费 ： " + addFee);
            System.out.println("此单最大免责数 ： " + specCount);
            double addFeeRate = (addFee / (tLCContSchema.getPrem() - addFee)) * 100;

            for (int i = 1; i <= tLCPolSet.size(); i++)
            {
                LDUWGradeDB tLDUWGradeDB = new LDUWGradeDB();
                tLDUWGradeDB.setUWGrade(uwGrade);
                tLDUWGradeDB.setContFlag("1");
                tLDUWGradeDB.setRiskType(getRiskType(tLCPolSet.get(i)
                        .getRiskCode()));
                LDUWGradeSet tLDUWGradeSet = tLDUWGradeDB.query();
                //首先校验加费免责是否超过级别
                if (addFeeRate > tLDUWGradeSet.get(1).getAddPrem())
                {
                    //加费超过核保权限
                    mSendFlag = "1"; //上报
                    mSendType = "1"; //普通上报
                    /** 需要记录下上报的原因 */

                    return true;
                }
                if (specCount > tLDUWGradeSet.get(1).getSpecItemNum())
                {
                    //免责超过核保级别
                    mSendFlag = "1"; //上报
                    mSendType = "1"; //普通上报
                    return true;
                }
                for (int m = 1; m <= tLDUWGradeSet.size(); m++)
                {
                    if (!StrTool.cTrim(tLCPolSet.get(i).getUWFlag())
                            .equals("z"))
                    {
                        //判断是否是拒保，如果不是拒保判断是保额权限还是保费权限
                        if (tLDUWGradeSet.get(m).getPopedomType().equals("001"))
                        {
                            if (tLCPolSet.get(i).getPrem() > tLDUWGradeSet.get(
                                    m).getPopedomValue())
                            {
                                mSendFlag = "1"; //上报
                                mSendType = "1"; //普通上报
                                return true;
                            }
                        }
                        if (tLDUWGradeSet.get(m).getPopedomType().equals("003"))
                        {
                            if (tLCPolSet.get(i).getAmnt() > tLDUWGradeSet.get(
                                    m).getPopedomValue())
                            {
                                mSendFlag = "1"; //上报
                                mSendType = "1"; //普通上报
                                return true;
                            }
                        }
                    }
                    if (StrTool.cTrim(tLCPolSet.get(i).getUWFlag()).equals("1"))
                    {
                        //判断是否是拒保，如果不是拒保判断是保额权限还是保费权限
                        if (tLDUWGradeSet.get(m).getPopedomType().equals("002"))
                        {
                            if (tLCPolSet.get(i).getPrem() > tLDUWGradeSet.get(
                                    m).getPopedomValue())
                            {
                                mSendFlag = "1"; //上报
                                mSendType = "1"; //普通上报
                                return true;
                            }
                        }
                        if (tLDUWGradeSet.get(m).getPopedomType().equals("004"))
                        {
                            if (tLCPolSet.get(i).getAmnt() > tLDUWGradeSet.get(
                                    m).getPopedomValue())
                            {
                                mSendFlag = "1"; //上报
                                mSendType = "1"; //普通上报
                                return true;
                            }
                        }
                    }
                }
            }
        }
        //新契约团险核保师级别校验处理
        else if (mOtherNoType.equals("2"))
        { //新契约团险
            mSendFlag = "0";
            String uwGrade = mLDUWUserSchema.getUWPopedom();
            LCGrpContDB tLCGrpContDB = new LCGrpContDB();
            tLCGrpContDB.setGrpContNo(mNewLCGCUWMasterSchema.getGrpContNo());
            if (!tLCGrpContDB.getInfo())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "UWSendTraceAllBL";
                tError.functionName = "checkGrade";
                tError.errorMessage = "集体合同表查失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            //判断是否为主动上报，是则直接上报
            System.out.println("---------mGrpSendUpFlag---------"
                    + mGrpSendUpFlag);
            if (StrTool.cTrim(mGrpSendUpFlag).equals("3"))
            {
                mSendFlag = "1"; //上报
                mSendType = "3"; //主动上报;
                return true;
            }
            if (StrTool.cTrim(mLDUWUserSchema.getPopUWFlag()).equals("1")
                    && !mSendFlag.equals("1"))
            {
                mSendFlag = "0"; //不需要上报
                return true;
            }
            //需要“增加系统自动上报时对核保上报原因实体表处理上报原因记录的保存“ ----- START
            //下面处理根据核保师权限判断是否需要上报处理，目前只针对团单进行处理
            else
            {
                //
                //                tAmnt = tLCGrpContDB.getPrem();
                //                tGrade = tPicch_UWGrade.getUWGrade(tAmnt, "2");

                //add by zhangxing 增加团险核保“5人强制上报”
                if (!mGlobalInput.ManageCom.equals("86"))
                {
                    // 去掉团险核保“5人强制上报”的功能。
                    //                    if (tLCGrpContDB.getPeoples2() < 5)
                    //                    {
                    //                        AddLCUWSendReason("该团体投保单的人数小于5人，该核保师没有核保该投保单的权限！");
                    //                        mSendFlag = "1"; //上报
                    //                        mSendType = "1"; //普通上报
                    //                        mUserCode = mLDUWUserSchema.getUpUserCode();
                    //                    }
                    // -------------------------------------
                    
                    /*
                     String strSql =
                     "select count(1) from lccont where grpcontno='" +
                     tLCGrpContDB.getGrpContNo() + "' and poltype='1'";
                     int chk = Integer.parseInt(new ExeSQL().getOneValue(strSql));
                     if (chk > 0) {
                     AddLCUWSendReason("无名单上报");
                     mSendFlag = "1"; //上报
                     mSendType = "1"; //普通上报
                     mUserCode = mLDUWUserSchema.getUpUserCode();
                     }
                     */

                }

                //开始查询险种分类
                StringBuffer sql = new StringBuffer();
                sql.append("select riskcode from lcgrppol where grpcontno='");
                sql.append(tLCGrpContDB.getGrpContNo());
                sql.append("'");
                ExeSQL tExeSQL = new ExeSQL();
                SSRS ssrs = tExeSQL.execSQL(sql.toString());
                VData riskKind = new VData();
                for (int i = 1; i <= ssrs.getMaxRow(); i++)
                {
                    sql = new StringBuffer();
                    sql
                            .append("select RiskType9 from lmriskapp where riskcode='");
                    sql.append(ssrs.GetText(i, 1));
                    sql.append("'");
                    //tExeSQL.execSQL(sql.toString());
                    String strRiskKind = tExeSQL.getOneValue(sql.toString());
                    if (riskKind.size() == 0)
                    {
                        riskKind.add(strRiskKind);
                    }
                    else
                    {
                        for (int m = 1; m <= riskKind.size(); m++)
                        {
                            if (!((String) riskKind.get(m - 1))
                                    .equals(strRiskKind))
                            {
                                riskKind.add(strRiskKind);
                            }
                        }
                    }
                }
                //查询出险种类型
                System.out.println("当前团体投保单中投保险种数量为 ： " + riskKind.size());
                //开始根据险种类型查询出
                //      固定保额类:  个人最高职业类别、最高保额限度
                //      费用类：     医疗类本类总保费、基金类本类账户总额
                for (int i = 1; i <= riskKind.size(); i++)
                {
                    //1.1 先查权限表，检查核保师对固定保额类中最高职业类别权限是否超出；
                    LDUWGradeDB tLDUWGradeDB = new LDUWGradeDB();
                    tLDUWGradeDB.setUWGrade(uwGrade);
                    tLDUWGradeDB.setContFlag("2");
                    tLDUWGradeDB.setPopedomType("102");
                    tLDUWGradeDB
                            .setRiskType((String) riskKind.getObject(i - 1));
                    LDUWGradeSet tLDUWGradeSet = tLDUWGradeDB.query();
                    if (tLDUWGradeSet.size() > 0)
                    { //查询职业类别权限
                        sql = new StringBuffer();
                        sql
                                .append("select to_zero(integer(max(a.OccupationType))) from lcpol a,lmriskapp b where a.grpcontno='");
                        sql.append(tLCGrpContDB.getGrpContNo());
                        sql
                                .append("' and a.riskcode=b.riskcode and b.risktype9='");
                        sql.append((String) riskKind.getObject(i - 1));
                        sql.append("'");
                        //当前险种最大职业类别
                        String maxOccupation = tExeSQL.getOneValue(sql
                                .toString());
                        if (StrTool.cTrim(maxOccupation).equals(""))
                        {
                            maxOccupation = "0";
                        }
                        if (Double.parseDouble(maxOccupation) > tLDUWGradeSet
                                .get(1).getPopedomValue())
                        {
                            //2006-02-24 闫少杰 增加系统自动上报时对核保上报原因实体表处理上报原因记录的保存 ----- START
                            AddLCUWSendReason("该团体投保单的最高职业类别为" + maxOccupation
                                    + "，该核保师没有核保该投保单的权限！");
                            //2006-02-24 闫少杰 增加系统自动上报时对核保上报原因实体表处理上报原因记录的保存 ----- END

                            mSendFlag = "1"; //上报
                            mSendType = "1"; //普通上报
                            mUserCode = mLDUWUserSchema.getUpUserCode();
                            //return true;
                        }
                    }
                    //1.2 再查业务表，检查核保师对固定保额类中个人最高保额权限是否超出；
                    tLDUWGradeDB = new LDUWGradeDB();
                    tLDUWGradeDB.setUWGrade(uwGrade);
                    tLDUWGradeDB.setContFlag("2");
                    tLDUWGradeDB.setPopedomType("101");
                    tLDUWGradeDB
                            .setRiskType((String) riskKind.getObject(i - 1));
                    tLDUWGradeSet = tLDUWGradeDB.query();
                    if (tLDUWGradeSet.size() > 0)
                    { //查询保额权限
                        sql = new StringBuffer();
                        sql
                                .append("select max(amnt) from lcpol a,lmriskapp b where a.grpcontno='");
                        sql.append(tLCGrpContDB.getGrpContNo());
                        sql
                                .append("' and a.riskcode=b.riskcode and b.risktype9='");
                        sql.append((String) riskKind.get(i - 1));
                        sql.append("'");
                        //当前险种最大职业类别
                        String maxAmnt = tExeSQL.getOneValue(sql.toString());
                        if (StrTool.cTrim(maxAmnt).equals(""))
                        {
                            maxAmnt = "0";
                        }
                        if (Double.parseDouble(maxAmnt) > tLDUWGradeSet.get(1)
                                .getPopedomValue())
                        {
                            //2006-02-24 闫少杰 增加系统自动上报时对核保上报原因实体表处理上报原因记录的保存 ----- START
                            AddLCUWSendReason("该团体投保单的个人最高保额为" + maxAmnt
                                    + "，该核保师没有核保该投保单的权限！");
                            //2006-02-24 闫少杰 增加系统自动上报时对核保上报原因实体表处理上报原因记录的保存 ----- END

                            mSendFlag = "1"; //上报
                            mSendType = "1"; //普通上报
                            mUserCode = mLDUWUserSchema.getUpUserCode();
                            //return true;
                        }
                    }
                    //1.3 再查业务表，检查核保师对费用类中本类总保额权限是否超出；
                    tLDUWGradeDB = new LDUWGradeDB();
                    tLDUWGradeDB.setUWGrade(uwGrade);
                    tLDUWGradeDB.setContFlag("2");
                    tLDUWGradeDB.setPopedomType("103");
                    tLDUWGradeDB
                            .setRiskType((String) riskKind.getObject(i - 1));
                    tLDUWGradeSet = tLDUWGradeDB.query();
                    if (tLDUWGradeSet.size() > 0)
                    { //查询本类总保额
                        sql = new StringBuffer();
                        sql
                                .append("select sum(amnt) from lcpol a,lmriskapp b where a.grpcontno='");
                        sql.append(tLCGrpContDB.getGrpContNo());
                        sql
                                .append("' and a.riskcode=b.riskcode and b.risktype9='");
                        sql.append((String) riskKind.get(i - 1));
                        sql.append("'");
                        String sumAmnt = tExeSQL.getOneValue(sql.toString());
                        if (StrTool.cTrim(sumAmnt).equals(""))
                        {
                            sumAmnt = "0";
                        }
                        if (Double.parseDouble(sumAmnt) > tLDUWGradeSet.get(1)
                                .getPopedomValue())
                        {
                            //2006-02-24 闫少杰 增加系统自动上报时对核保上报原因实体表处理上报原因记录的保存 ----- START
                            AddLCUWSendReason("该团体投保单的本类总保额为" + sumAmnt
                                    + "，该核保师没有核保该投保单的权限！");
                            //2006-02-24 闫少杰 增加系统自动上报时对核保上报原因实体表处理上报原因记录的保存 ----- END

                            mSendFlag = "1"; //上报
                            mSendType = "1"; //普通上报
                            this.mUserCode = mLDUWUserSchema.getUpUserCode();
                            return true; //如果本类总保额不足则不再往下做判断
                        }
                    }
                    //1.4 再查业务表，检查核保师对费用类中医疗类总保费权限是否超出；
                    tLDUWGradeDB = new LDUWGradeDB();
                    tLDUWGradeDB.setUWGrade(uwGrade);
                    tLDUWGradeDB.setContFlag("2");
                    tLDUWGradeDB.setPopedomType("104");
                    tLDUWGradeDB
                            .setRiskType((String) riskKind.getObject(i - 1));
                    tLDUWGradeSet = tLDUWGradeDB.query();
                    if (tLDUWGradeSet.size() > 0)
                    { //查询本类总保费
                        sql = new StringBuffer();
                        sql
                                .append("select sum(prem) from lcpol a,lmriskapp b where a.grpcontno='");
                        sql.append(tLCGrpContDB.getGrpContNo());
                        sql
                                .append("' and a.riskcode=b.riskcode and b.risktype9='");
                        sql.append((String) riskKind.getObject(i - 1));
                        sql.append("'");
                        String sumPrem = tExeSQL.getOneValue(sql.toString());
                        if (StrTool.cTrim(sumPrem).equals(""))
                        {
                            sumPrem = "0";
                        }
                        if (Double.parseDouble(sumPrem) > tLDUWGradeSet.get(1)
                                .getPopedomValue())
                        {
                            //2006-02-24 闫少杰 增加系统自动上报时对核保上报原因实体表处理上报原因记录的保存 ----- START
                            AddLCUWSendReason("该团体投保单的医疗类总保费为" + sumPrem
                                    + "，该核保师没有核保该投保单的权限！");
                            //2006-02-24 闫少杰 增加系统自动上报时对核保上报原因实体表处理上报原因记录的保存 ----- END

                            mSendFlag = "1"; //上报
                            mSendType = "1"; //普通上报
                            mUserCode = mLDUWUserSchema.getUpUserCode();
                        }
                    }
                    // 再查业务表，检查核保师对整单总保费权限是否超出；
                    tLDUWGradeDB = new LDUWGradeDB();
                    tLDUWGradeDB.setUWGrade(uwGrade);
                    tLDUWGradeDB.setContFlag("2");
                    tLDUWGradeDB.setPopedomType("106");
                    tLDUWGradeDB
                            .setRiskType((String) riskKind.getObject(i - 1));
                    tLDUWGradeSet = tLDUWGradeDB.query();
                    if (tLDUWGradeSet.size() > 0)
                    { //查询本单总保费
                        sql = new StringBuffer();
                        sql
                                .append("select sum(prem) from lcpol where grpcontno='");
                        sql.append(tLCGrpContDB.getGrpContNo());
                        sql.append("'");
                        String sumPrem = tExeSQL.getOneValue(sql.toString());
                        if (StrTool.cTrim(sumPrem).equals(""))
                        {
                            sumPrem = "0";
                        }
                        if (Double.parseDouble(sumPrem) > tLDUWGradeSet.get(1)
                                .getPopedomValue())
                        {
                            //2006-02-24  增加系统自动上报时对核保上报原因实体表处理上报原因记录的保存 ----- START
                            AddLCUWSendReason("该团体投保单的总保费为" + sumPrem
                                    + "，该核保师没有核保该投保单的权限！");
                            //2006-02-24  增加系统自动上报时对核保上报原因实体表处理上报原因记录的保存 ----- END

                            mSendFlag = "1"; //上报
                            mSendType = "1"; //普通上报
                            mUserCode = mLDUWUserSchema.getUpUserCode();
                        }
                    }
                    //1.5 再查业务表，检查核保师对团险本类帐户总额权限是否超出；
                    tLDUWGradeDB = new LDUWGradeDB();
                    tLDUWGradeDB.setUWGrade(uwGrade);
                    tLDUWGradeDB.setContFlag("2");
                    tLDUWGradeDB.setPopedomType("105");
                    tLDUWGradeDB
                            .setRiskType((String) riskKind.getObject(i - 1));
                    tLDUWGradeSet = tLDUWGradeDB.query();
                    if (tLDUWGradeSet.size() > 0)
                    { //查询本类总保额
                        sql = new StringBuffer();
                        sql
                                .append("select sum(prem) from lcpol a,lmriskapp b where a.grpcontno='");
                        sql.append(tLCGrpContDB.getGrpContNo());
                        sql
                                .append("' and a.riskcode=b.riskcode and b.risktype9='");
                        sql.append((String) riskKind.getObject(i - 1));
                        sql.append("'");
                        String sumPrem = tExeSQL.getOneValue(sql.toString());
                        if (StrTool.cTrim(sumPrem).equals(""))
                        {
                            sumPrem = "0";
                        }
                        if (Double.parseDouble(sumPrem) > tLDUWGradeSet.get(1)
                                .getPopedomValue())
                        {
                            //2006-02-24 闫少杰 增加系统自动上报时对核保上报原因实体表处理上报原因记录的保存 ----- START
                            AddLCUWSendReason("该团体投保单的基金类账户总额为" + sumPrem
                                    + "，该核保师没有核保该投保单的权限！");
                            //2006-02-24 闫少杰 增加系统自动上报时对核保上报原因实体表处理上报原因记录的保存 ----- END

                            mSendFlag = "1"; //上报
                            mSendType = "1"; //普通上报
                            this.mUserCode = mLDUWUserSchema.getUpUserCode();
                            return true;
                        }
                    }
                }
                /** 以下是根据险种校验权限,不区分管理机构 */
                LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
                tLCGrpPolDB.setGrpContNo(mNewLCGCUWMasterSchema.getGrpContNo());
                LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.query();
                if (tLCGrpPolSet.size() <= 0)
                {
                    buildError("checkGrade", "查询险种信息失败！");
                    return false;
                }
                for (int i = 1; i <= tLCGrpPolSet.size(); i++)
                {
                    String riskCode = tLCGrpPolSet.get(i).getRiskCode();
                    System.out.println("开始处理险种权限: " + riskCode);
                    LDUWGradeDB tLDUWGradeDB = new LDUWGradeDB();
                    tLDUWGradeDB.setRiskCode(riskCode);
                    //tLDUWGradeDB.setPopedomType("106");
                    tLDUWGradeDB.setManageCom("0");
                    tLDUWGradeDB.setUWGrade(uwGrade);
                    LDUWGradeSet tLDUWGradeSet = tLDUWGradeDB.query();
                    if (tLDUWGradeSet.size() <= 0)
                    {
                        return true;
                    }
                    //                    if (tLDUWGradeSet.size() > 1) {
                    //                        buildError("checkGrade",
                    //                                   "每个核保师级别针对每一个险种只有一个保费限制,多于一个限制属于逻辑错误！");
                    //                        return false;
                    //                    }
                    for (int m = 1; m <= tLDUWGradeSet.size(); m++)
                    {
                        if (!dealGrade(tLDUWGradeSet.get(m), tLCGrpPolSet
                                .get(i)))
                        {
                            return false;
                        }
                    }
                    tLDUWGradeDB = new LDUWGradeDB();
                    tLDUWGradeDB.setRiskCode(riskCode);
                    //tLDUWGradeDB.setPopedomType("106");
                    tLDUWGradeDB.setManageCom(getManageCom(tLCGrpPolSet.get(i)
                            .getManageCom()));
                    tLDUWGradeDB.setUWGrade(uwGrade);
                    tLDUWGradeSet = tLDUWGradeDB.query();
                    for (int m = 1; m <= tLDUWGradeSet.size(); m++)
                    {
                        if (!dealGrade(tLDUWGradeSet.get(m), tLCGrpPolSet
                                .get(i)))
                        {
                            return false;
                        }
                    }
                }

            }
            //需要“增加系统自动上报时对核保上报原因实体表处理上报原因记录的保存“ ----- END
        }
        System.out.println("在checkGrade中 : " + mSendFlag);
        return true;
    }

    //2006-02-24 闫少杰 增加系统自动上报时对核保上报原因实体表处理上报原因记录的保存 ----- START
    /**
     * 增加系统自动上报时对核保上报原因实体表处理上报原因记录的保存
     * @param strReason String
     */
    private void AddLCUWSendReason(String strReason)
    {
        LCUWSendReasonSchema tLCUWSendReasonSchema = new LCUWSendReasonSchema();
        tLCUWSendReasonSchema.setReason(strReason);
        mLCUWSendReasonSet.add(tLCUWSendReasonSchema);
    }

    //2006-02-24 闫少杰 增加系统自动上报时对核保上报原因实体表处理上报原因记录的保存 ----- END

    /**
     * getManageCom
     *
     * @param tManageCom String
     * @return String
     */
    private String getManageCom(String tManageCom)
    {
        if (StrTool.cTrim(tManageCom).equals(""))
        {
            buildError("getManageCom", "保单管理机构录入错误！");
            return "";
        }
        if (tManageCom.length() > 4)
        {
            return tManageCom.substring(0, 4);
        }
        return "";
    }

    /**
     * dealGrade
     *
     * @param tLDUWGradeSchema LDUWGradeSchema
     * @param tLCGrpPolSchema LCGrpPolSchema
     * @return boolean
     */
    private boolean dealGrade(LDUWGradeSchema tLDUWGradeSchema,
            LCGrpPolSchema tLCGrpPolSchema)
    {
        if (StrTool.cTrim(tLDUWGradeSchema.getPopedomDirectType()).equals("2"))
        {
            double Value1 = tLDUWGradeSchema.getPopedomValue();
            double Value2 = tLDUWGradeSchema.getPopedomValue2();
            double Compare = getCompareValue(tLDUWGradeSchema, tLCGrpPolSchema);
            if (dealTrivial(tLDUWGradeSchema.getPopedomDirect(),
                    tLDUWGradeSchema.getPopedomDirect2(), Value1, Compare,
                    Value2, Compare))
            {
                mSendFlag = "1"; //上报
                mSendType = "1"; //普通上报
                mUserCode = mLDUWUserSchema.getUpUserCode();
                return true;
            }
        }
        else if (StrTool.cTrim(tLDUWGradeSchema.getPopedomDirectType()).equals(
                "1"))
        {
            double Value1 = tLDUWGradeSchema.getPopedomValue();
            double Compare = getCompareValue(tLDUWGradeSchema, tLCGrpPolSchema);
            if (dealTraivialDetail(tLDUWGradeSchema.getPopedomDirect(), Value1,
                    Compare))
            {
                mSendFlag = "1"; //上报
                mSendType = "1"; //普通上报
                mUserCode = mLDUWUserSchema.getUpUserCode();
                return true;
            }
        }
        return false;
    }

    /**
     * getCompareValue
     *
     * @param tLDUWGradeSchema LDUWGradeSchema
     * @param tLCGrpPolSchema LCGrpPolSchema
     * @return double
     */
    private double getCompareValue(LDUWGradeSchema tLDUWGradeSchema,
            LCGrpPolSchema tLCGrpPolSchema)
    {
        /** 106校验整单总保费 */
        if (tLDUWGradeSchema.getPopedomType().equals("106"))
        {
            String strSql = "select sum(prem) from lcpol where grpcontno ='"
                    + tLCGrpPolSchema.getGrpContNo() + "'";
            String sumPrem = (new ExeSQL()).getOneValue(strSql);
            if (StrTool.cTrim(sumPrem).equals("")
                    || StrTool.cTrim(sumPrem).equals("null"))
            {
                buildError("getCompareValue", "插旬总保费失败！");
                return 0;
            }
            return Double.parseDouble(sumPrem);
        }
        /** 险种折扣,取出保险计划下的险种的最低折扣 */
        if (tLDUWGradeSchema.getPopedomType().equals("107"))
        {
            String strSql = getSql();
            String sell = "";
            try
            {
                sell = (new ExeSQL()).getOneValue(strSql);
            }
            catch (Exception ex)
            {
                buildError("getCompareValue", "生成折扣失败！");
                return 0;
            }
            if (StrTool.cTrim(sell).equals("")
                    || StrTool.cTrim(sell).equals("null"))
            {
                buildError("getCompareValue", "生成折扣失败！");
                return 0;
            }
            return Double.parseDouble(sell);
        }

        return 0.0;
    }

    /**
     * getSql
     *
     * @return String
     */
    private String getSql()
    {
        StringBuffer strSql = new StringBuffer();
        strSql.append("select min(decimal(decimal(d)/decimal(e),12,2)) from ");
        strSql
                .append("(select a.contplancode b,riskcode c,sum(c.prem) d,sum(c.standprem) e from lcinsured a ,lcpol b ");
        strSql.append(",lcduty c ");
        strSql
                .append(" where a.contno=b.contno and b.conttype='2' and a.insuredno=b.insuredno ");
        strSql.append(" and b.polno=c.polno ");
        strSql
                .append("and a.contplancode<>'11' and a.contplancode<>'00' and a.grpcontno='");
        strSql.append(this.mNewLCGCUWMasterSchema.getGrpContNo());
        strSql.append("' group by a.contplancode,riskcode order by b ) as x");
        return strSql.toString();
    }

    /**
     * dealTrivial
     *
     * @param tDirect1 String 判断第一个权限值与比较值之间的判断规则
     * @param tDirect2 String 判断第二个权限值与比较值之间的判断规则
     * @param tValue1 double  第一个权限值
     * @param compare1 double 第一个比较值
     * @param tValue2 double 第二个权限值
     * @param compare2 double 第二个比较值
     * @return boolean 0 不需要上报 1 上报
     */
    private boolean dealTrivial(String tDirect1, String tDirect2,
            double tValue1, double compare1, double tValue2, double compare2)
    {
        if (tDirect1 == null || tDirect2 == null)
        {
            buildError("dealTrivial", "区间校验出错,原因是,区间校验必须给出两个值得比较方向！");
            return false;
        }
        if (dealTraivialDetail(tDirect1, tValue1, compare1))
        {
            return true;
        }
        if (dealTraivialDetail(tDirect2, tValue2, compare2))
        {
            return true;
        }
        return false;
    }

    /**
     * dealTraivialDetail
     *
     * @param tDirect String
     * @param tValue double
     * @param compare double
     * @return boolean
     */
    private boolean dealTraivialDetail(String tDirect, double tValue,
            double compare)
    {
        /** 权限方向是指当前比较值与权限值应满足的关系 */
        /** 录入如果 比较方向为<,比较值500,如果权限值600时满足权限,(500<600) */
        /** 如果权限值为400,上报(500<400) */
        if (tDirect.equals(">"))
        {
            return compare > tValue;
        }
        else if (tDirect.equals(">="))
        {
            return compare >= tValue;
        }
        else if (tDirect.equals("="))
        {
            return compare == tValue;
        }
        else if (tDirect.equals("<"))
        {
            return compare < tValue;
        }
        else if (tDirect.equals("<="))
        {
            return compare <= tValue;
        }
        else
        {
            buildError("dealTrivial", "未知的计算方向！");
            return false;
        }
    }

    /**
     * getRiskType
     *
     * @param tRiskCode String
     * @return String
     */
    private String getRiskType(String tRiskCode)
    {
        LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
        tLMRiskAppDB.setRiskCode(tRiskCode);
        if (!tLMRiskAppDB.getInfo())
        {
            this.mErrors.copyAllErrors(tLMRiskAppDB.mErrors);
            return "";
        }
        return tLMRiskAppDB.getRiskType9();
    }

    /**
     * getPopedom 得到核保师级别
     *
     * @param tUserCode String
     * @return String
     */
    private String getPopedom(String tUserCode, String tType)
    {
        LDUWUserDB tLDUWUserDB = new LDUWUserDB();
        tLDUWUserDB.setUserCode(tUserCode);
        tLDUWUserDB.setUWType(tType);
        tLDUWUserDB.getInfo();

        return tLDUWUserDB.getUWPopedom();
    }

    /**
     * 返回结果
     * @return VData
     */
    public VData getResult()
    {
        return mResult;
    }

    /**
     * 返回错误
     * @return VData
     */
    public CErrors getErrors()
    {
        return mErrors;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "UWSendTraceAllBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }
    
    /**
     * by gzh 20120829
     * 政府委托，市场类型不是商业和企事业团体补充医疗，需上报到86机构的核保师。
     * @return boolean
     */
    private boolean checkMarkettypeUW()
    {
//      获取核保师管理机构
    	String tSql = "select comcode from lduser where usercode = '"+mOperator+"' ";
    	String tManagecom = new ExeSQL().getOneValue(tSql);
    	if(tManagecom == null || "".equals(tManagecom)){
//    		 @@错误处理
            CError tError = new CError();
            tError.moduleName = "UWSendTraceAllBL";
            tError.functionName = "checkMarkettypeUW";
            tError.errorMessage = "查询核保师所属管理机构失败!";
            this.mErrors.addOneError(tError);
            return false;
    	}
//    	核保师总公司机构，通过
    	if("86".equals(tManagecom)){
    		return true;
    	}
    	
//    	获取保单市场类型
    	String  mSQL = "select 1 from lcgrpcont lgc" +
    			" where lgc.grpcontno = '"+mLCGCUWMasterSchema.getGrpContNo()+"' " +
    			" and lgc.markettype not in (select code from ldcode where codetype = 'uwmarkettype' ) " +
    			" and not exists (select 1 from lcgrppol where grpcontno = lgc.grpcontno and riskcode = '370101' ) ";
    	String tFlag = new ExeSQL().getOneValue(mSQL);
    	if("1".equals(tFlag)){
    		mSendFlag = "1"; //1-上报 0-不上报或下报
            mSendType = "1"; //1-普通上报 2-抽检上报
    	}
        return true;
    }
}
