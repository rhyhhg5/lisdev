package com.sinosoft.lis.cbcheck;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCReInsurTaskSchema;
import com.sinosoft.lis.schema.LCReInsurUWIdeaSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.db.LCReInsurTaskDB;
import com.sinosoft.lis.db.LCReInsurUWIdeaDB;
import com.sinosoft.lis.vschema.LCReInsurTaskSet;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.vschema.LCReInsurUWIdeaSet;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LCGrpPolDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.utility.StrTool;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 新契约</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author
 * @version 1.0
 */
public class SendUWReInsureBL
{
    public SendUWReInsureBL()
    {
    }

    /** 传入参数 */
    private VData mInputData;
    /** 传入操作符 */
    private String mOperate;
    /** 登陆信息 */
    private GlobalInput mGlobalInput;
    /** 报错存储对象 */
    public CErrors mErrors = new CErrors();
    /** 最后保存结果 */
    private VData mResult = new VData();
    /** 最后递交Map */
    private MMap map = new MMap();
    /** 数据操作字符串 */
    private String mOperator;
    private String mManageCom;
    private String mPolNo;
    private String mGrpPolNo;
    private String mGrpContNo;
    private String mRemark;
    private String mFilePath;
    private String mFileName;
    private String mRiskType;
    private int mNo; //实际申请的个数
    private TransferData mTransferData = new TransferData();
    /**再保审核任务表*/
    private LCReInsurTaskSchema mLCReInsurTaskSchema = new LCReInsurTaskSchema();

    /**再保审核任务核保意见表*/
    private LCReInsurUWIdeaSchema mLCReInsurUWIdeaSchema = new
            LCReInsurUWIdeaSchema();


    /**
     * submitData
     *
     * @param nInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData nInputData, String cOperate)
    {
        System.out.println("into SendUWReInsurBL...");
        this.mInputData = nInputData;
        this.mOperate = cOperate;

        if (!getInputData())
        {
            return false;
        }
        if (!checkData())
        {
            return false;
        }
        if (!dealData())
        {
            return false;
        }
        if (!prepareOutputData())
        {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(this.mResult, "INSERT"))
        {
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        System.out.println("SendUWReInsurBL finished...");
        return true;
    }

    /**
     * getInputData
     *
     * @return boolean
     */

    private boolean getInputData()
    {
        System.out.println("into SendUWReInsurBL.getInputData()...");
        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
        this.mTransferData = (TransferData) mInputData.getObjectByObjectName(
                "TransferData", 0);
        if (mGlobalInput == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "SendUWReInsurBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData()
    {
        System.out.println("into SendUWReInsurBL.checkData()...");
        if (this.mGlobalInput == null)
        {
            String str = "登陆信息为null，可能是页面超时，请重新登陆!";
            buildError("checkData", str);
            return false;
        }
        return true;
    }

    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData()
    {
        System.out.println("into dealData");
        if (this.mOperate.equals("INSERT||MAIN"))
        {
            if (!insertData())
            {
                return false;
            }
        }
        if (this.mOperate.equals("FINISH"))
        {

        }
        return true;
    }

    /**
     * insertData
     *
     * @return boolean
     */
    private boolean insertData()
    {
        System.out.println("into SendUWReInsurBL.dealData()...");
        mRiskType = (String) mTransferData.getValueByName("RiskType");
        System.out.println("标记" + mRiskType);
        if (mRiskType.equals("1"))
        {
            LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
            mGrpPolNo = (String) mTransferData.getValueByName("GrpPolNo");
            mRemark = (String) mTransferData.getValueByName("Remark");
            mFilePath = (String) mTransferData.getValueByName("FilePath");
            mFileName = (String) mTransferData.getValueByName("FileName");
            System.out.println(mFileName);
            tLCGrpPolDB.setGrpPolNo(mGrpPolNo);
            if (!tLCGrpPolDB.getInfo())
            {
                String str = "查询险种信息失败 !";
                buildError("dealData", str);
                System.out.println("在程序SendUWReInsurBL.dealData() - 189 : " +
                                   str);
                return false;
            }

            int uwno = 0; //核保顺序号
            LCReInsurTaskDB tLCReInsurTaskDB = new LCReInsurTaskDB();
            StringBuffer sql = new StringBuffer(255);
            sql.append("select * from LCReInsurTask where polno='");
            sql.append(mGrpPolNo);
            sql.append("' order by uwno desc");
            LCReInsurTaskSet tLCReInsurTaskSet = new LCReInsurTaskSet();
            tLCReInsurTaskSet = tLCReInsurTaskDB.executeQuery(sql.toString());
            if (tLCReInsurTaskSet == null || tLCReInsurTaskSet.size() == 0)
            {
                uwno = 1;
            } else
            {
                uwno = tLCReInsurTaskSet.size() + 1;

            }

            //再保审核任务表
            mLCReInsurTaskSchema.setUWNo(uwno);
            mLCReInsurTaskSchema.setPolNo(mGrpPolNo);
            mLCReInsurTaskSchema.setState("00");
            mLCReInsurTaskSchema.setOperator(mGlobalInput.Operator);
            mLCReInsurTaskSchema.setManageCom(mGlobalInput.ManageCom);
            mLCReInsurTaskSchema.setMakeDate(PubFun.getCurrentDate());
            mLCReInsurTaskSchema.setMakeTime(PubFun.getCurrentTime());
            mLCReInsurTaskSchema.setModifyDate(PubFun.getCurrentDate());
            mLCReInsurTaskSchema.setModifyTime(PubFun.getCurrentTime());

            //再保审核任务核保意见表
            int tuwno = 0; //核保顺序号
            LCReInsurUWIdeaDB tLCReInsurUWIdeaDB = new LCReInsurUWIdeaDB();
            StringBuffer tsql = new StringBuffer(255);
            tsql.append("select * from LCReInsurUWIdea where polno='");
            tsql.append(mGrpPolNo);
            tsql.append("' order by uwno desc");
            LCReInsurUWIdeaSet tLCReInsurUWIdeaSet = new LCReInsurUWIdeaSet();
            tLCReInsurUWIdeaSet = tLCReInsurUWIdeaDB.executeQuery(tsql.toString());
            if (tLCReInsurUWIdeaSet == null || tLCReInsurUWIdeaSet.size() == 0)
            {
                tuwno = 1;
            } else
            {

                tuwno = tLCReInsurUWIdeaSet.size() + 1;

            }
            //String strOthNo[] = mFileName.split("=");
            mLCReInsurUWIdeaSchema.setUWNo(tuwno);
            mLCReInsurUWIdeaSchema.setPolNo(mGrpPolNo);
            mLCReInsurUWIdeaSchema.setAdjunctPath(StrTool.unicodeToGBK(
                    mFilePath + mFileName));
            mLCReInsurUWIdeaSchema.setUWOperator(mGlobalInput.Operator);
            mLCReInsurUWIdeaSchema.setOperator(mGlobalInput.Operator);
            mLCReInsurUWIdeaSchema.setUWIdea(mRemark);
            mLCReInsurUWIdeaSchema.setManageCom(mGlobalInput.ManageCom);
            mLCReInsurUWIdeaSchema.setMakeDate(PubFun.getCurrentDate());
            mLCReInsurUWIdeaSchema.setMakeTime(PubFun.getCurrentTime());
            mLCReInsurUWIdeaSchema.setModifyDate(PubFun.getCurrentDate());
            mLCReInsurUWIdeaSchema.setModifyTime(PubFun.getCurrentTime());

            map.put("update LWMission set  ActivityStatus='" + 2 +
                    "' where Activityid in ('0000002004') and missionprop2 ='" +
                    tLCGrpPolDB.getGrpProposalNo() + "'", "UPDATE");
            map.put("update LCPol set  reinsureflag='" + 2 +
                    "' where  grppolno ='" + tLCGrpPolDB.getGrpPolNo() + "'",
                    "UPDATE");

            return true;

        } else if (mRiskType.equals("9"))
        {
            LCGrpContDB tLCGrpContDB = new LCGrpContDB();
            mGrpContNo = (String) mTransferData.getValueByName("GrpContNo");
            System.out.println("保单号是" + mGrpContNo);
            String tRemark = (String) mTransferData.getValueByName(
                    "ApplyRemark");
            String tLFState1No = (String) mTransferData.getValueByName(
                    "LFState1No");
            mFilePath = (String) mTransferData.getValueByName("FilePath");
            mFileName = (String) mTransferData.getValueByName("FileName");
            System.out.println(mFileName);
            System.out.println("临分状态" + tLFState1No);
            tLCGrpContDB.setGrpContNo(mGrpContNo);
            if (!tLCGrpContDB.getInfo())
            {
                String str = "查询保单信息失败 !";
                buildError("dealData", str);
                System.out.println("在程序SendUWReInsurBL.dealData() - 189 : " +
                                   str);
                return false;
            }

            int uwno = 0; //核保顺序号
            LCReInsurTaskDB tLCReInsurTaskDB = new LCReInsurTaskDB();
            StringBuffer sql = new StringBuffer(255);
            sql.append("select * from LCReInsurTask where polno='");
            sql.append(mGrpContNo);
            sql.append("' order by uwno desc");
            LCReInsurTaskSet tLCReInsurTaskSet = new LCReInsurTaskSet();
            tLCReInsurTaskSet = tLCReInsurTaskDB.executeQuery(sql.toString());
            if (tLCReInsurTaskSet == null || tLCReInsurTaskSet.size() == 0)
            {
                uwno = 1;
            } else
            {
                uwno = tLCReInsurTaskSet.size() + 1;

            }

            //再保审核任务表
            mLCReInsurTaskSchema.setUWNo(uwno);
            mLCReInsurTaskSchema.setPolNo(mGrpContNo);
            mLCReInsurTaskSchema.setState(tLFState1No);
            mLCReInsurTaskSchema.setOperator(mGlobalInput.Operator);
            mLCReInsurTaskSchema.setManageCom(mGlobalInput.ManageCom);
            mLCReInsurTaskSchema.setMakeDate(PubFun.getCurrentDate());
            mLCReInsurTaskSchema.setMakeTime(PubFun.getCurrentTime());
            mLCReInsurTaskSchema.setModifyDate(PubFun.getCurrentDate());
            mLCReInsurTaskSchema.setModifyTime(PubFun.getCurrentTime());

            //再保审核任务核保意见表
            int tuwno = 0; //核保顺序号
            LCReInsurUWIdeaDB tLCReInsurUWIdeaDB = new LCReInsurUWIdeaDB();
            StringBuffer tsql = new StringBuffer(255);
            tsql.append("select * from LCReInsurUWIdea where polno='");
            tsql.append(mGrpContNo);
            tsql.append("' order by uwno desc");
            LCReInsurUWIdeaSet tLCReInsurUWIdeaSet = new LCReInsurUWIdeaSet();
            tLCReInsurUWIdeaSet = tLCReInsurUWIdeaDB.executeQuery(tsql.toString());
            if (tLCReInsurUWIdeaSet == null || tLCReInsurUWIdeaSet.size() == 0)
            {
                tuwno = 1;
            } else
            {
                tuwno = tLCReInsurUWIdeaSet.size() + 1;
            }
            //String strOthNo[] = mFileName.split("=");
            mLCReInsurUWIdeaSchema.setUWNo(tuwno);
            mLCReInsurUWIdeaSchema.setPolNo(mGrpContNo);
            mLCReInsurUWIdeaSchema.setAdjunctPath(StrTool.unicodeToGBK(
                    mFilePath + mFileName));
            mLCReInsurUWIdeaSchema.setUWOperator(mGlobalInput.Operator);
            mLCReInsurUWIdeaSchema.setOperator(mGlobalInput.Operator);
            mLCReInsurUWIdeaSchema.setUWIdea(tRemark);
            mLCReInsurUWIdeaSchema.setManageCom(mGlobalInput.ManageCom);
            mLCReInsurUWIdeaSchema.setMakeDate(PubFun.getCurrentDate());
            mLCReInsurUWIdeaSchema.setMakeTime(PubFun.getCurrentTime());
            mLCReInsurUWIdeaSchema.setModifyDate(PubFun.getCurrentDate());
            mLCReInsurUWIdeaSchema.setModifyTime(PubFun.getCurrentTime());
            return true;
        } else if (mRiskType.equals("8"))
        {
            LCGrpContDB tLCGrpContDB = new LCGrpContDB();
            mGrpContNo = (String) mTransferData.getValueByName("GrpContNo");
            System.out.println("保单号是" + mGrpContNo);
            String sRemark = (String) mTransferData.getValueByName("BJRemark");
            String tLFStateNo = (String) mTransferData.getValueByName(
                    "LFStateNo");
            System.out.println("办结结论" + sRemark);
            tLCGrpContDB.setGrpContNo(mGrpContNo);
            if (!tLCGrpContDB.getInfo())
            {
                String str = "查询保单信息失败 !";
                buildError("dealData", str);
                System.out.println("在程序SendUWReInsurBL.dealData() - 189 : " +
                                   str);
                return false;
            }

            int uwno = 0; //核保顺序号
            LCReInsurTaskDB tLCReInsurTaskDB = new LCReInsurTaskDB();
            StringBuffer sql = new StringBuffer(255);
            sql.append("select * from LCReInsurTask where polno='");
            sql.append(mGrpContNo + "BJ");
            sql.append("' order by uwno desc");
            LCReInsurTaskSet tLCReInsurTaskSet = new LCReInsurTaskSet();
            tLCReInsurTaskSet = tLCReInsurTaskDB.executeQuery(sql.toString());
            if (tLCReInsurTaskSet == null || tLCReInsurTaskSet.size() == 0)
            {
                uwno = 1;
            } else
            {
                uwno = tLCReInsurTaskSet.size() + 1;
            }

            //再保审核任务表
            mLCReInsurTaskSchema.setUWNo(uwno);
            mLCReInsurTaskSchema.setPolNo(mGrpContNo + "BJ");
            mLCReInsurTaskSchema.setState(tLFStateNo);
            mLCReInsurTaskSchema.setOperator(mGlobalInput.Operator);
            mLCReInsurTaskSchema.setManageCom(mGlobalInput.ManageCom);
            mLCReInsurTaskSchema.setMakeDate(PubFun.getCurrentDate());
            mLCReInsurTaskSchema.setMakeTime(PubFun.getCurrentTime());
            mLCReInsurTaskSchema.setModifyDate(PubFun.getCurrentDate());
            mLCReInsurTaskSchema.setModifyTime(PubFun.getCurrentTime());

            //再保审核任务核保意见表
            int tuwno = 0; //核保顺序号
            LCReInsurUWIdeaDB tLCReInsurUWIdeaDB = new LCReInsurUWIdeaDB();
            StringBuffer tsql = new StringBuffer(255);
            tsql.append("select * from LCReInsurUWIdea where polno='");
            tsql.append(mGrpContNo + "BJ");
            tsql.append("' order by uwno desc");
            LCReInsurUWIdeaSet tLCReInsurUWIdeaSet = new LCReInsurUWIdeaSet();
            tLCReInsurUWIdeaSet = tLCReInsurUWIdeaDB.executeQuery(tsql.toString());
            if (tLCReInsurUWIdeaSet == null || tLCReInsurUWIdeaSet.size() == 0)
            {
                tuwno = 1;
            } else
            {
                tuwno = tLCReInsurUWIdeaSet.size() + 1;
            }
            //String strOthNo[] = mFileName.split("=");
            mLCReInsurUWIdeaSchema.setUWNo(tuwno);
            mLCReInsurUWIdeaSchema.setPolNo(mGrpContNo + "BJ");
            mLCReInsurUWIdeaSchema.setAdjunctPath(StrTool.unicodeToGBK(
                    mFilePath + mFileName));
            mLCReInsurUWIdeaSchema.setUWOperator(mGlobalInput.Operator);
            mLCReInsurUWIdeaSchema.setOperator(mGlobalInput.Operator);
            mLCReInsurUWIdeaSchema.setUWIdea(sRemark);
            mLCReInsurUWIdeaSchema.setManageCom(mGlobalInput.ManageCom);
            mLCReInsurUWIdeaSchema.setMakeDate(PubFun.getCurrentDate());
            mLCReInsurUWIdeaSchema.setMakeTime(PubFun.getCurrentTime());
            mLCReInsurUWIdeaSchema.setModifyDate(PubFun.getCurrentDate());
            mLCReInsurUWIdeaSchema.setModifyTime(PubFun.getCurrentTime());
            return true;
        } else
        {
            System.out.println("个险就不行了?????" + mRiskType);
            LCPolDB tLCPolDB = new LCPolDB();
            LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
            mPolNo = (String) mTransferData.getValueByName("PolNo");
            mGrpPolNo = (String) mTransferData.getValueByName("GrpPolNo");
            mRemark = (String) mTransferData.getValueByName("Remark");
            mFilePath = (String) mTransferData.getValueByName("FilePath");
            mFileName = (String) mTransferData.getValueByName("FileName");
            System.out.println(mFileName);
            tLCPolDB.setPolNo(mPolNo);
            tLCGrpPolDB.setGrpPolNo(mGrpPolNo);
            if (!tLCPolDB.getInfo())
            {
                String str = "查询险种信息失败 !";
                buildError("dealData", str);
                System.out.println("在程序SendUWReInsurBL.dealData() - 189 : " +
                                   str);
                return false;
            }

            int uwno = 0; //核保顺序号
            LCReInsurTaskDB tLCReInsurTaskDB = new LCReInsurTaskDB();
            StringBuffer sql = new StringBuffer(255);
            sql.append("select * from LCReInsurTask where polno='");
            sql.append(mPolNo);
            sql.append("' order by uwno desc");
            LCReInsurTaskSet tLCReInsurTaskSet = new LCReInsurTaskSet();
            tLCReInsurTaskSet = tLCReInsurTaskDB.executeQuery(sql.toString());
            if (tLCReInsurTaskSet == null || tLCReInsurTaskSet.size() == 0)
            {
                uwno = 1;
            } else
            {
                uwno = tLCReInsurTaskSet.size() + 1;
            }

            //再保审核任务表
            mLCReInsurTaskSchema.setUWNo(uwno);
            mLCReInsurTaskSchema.setPolNo(mPolNo);
            mLCReInsurTaskSchema.setState("00");
            mLCReInsurTaskSchema.setOperator(mGlobalInput.Operator);
            mLCReInsurTaskSchema.setManageCom(mGlobalInput.ManageCom);
            mLCReInsurTaskSchema.setMakeDate(PubFun.getCurrentDate());
            mLCReInsurTaskSchema.setMakeTime(PubFun.getCurrentTime());
            mLCReInsurTaskSchema.setModifyDate(PubFun.getCurrentDate());
            mLCReInsurTaskSchema.setModifyTime(PubFun.getCurrentTime());

            //再保审核任务核保意见表
            int tuwno = 0; //核保顺序号
            LCReInsurUWIdeaDB tLCReInsurUWIdeaDB = new LCReInsurUWIdeaDB();
            StringBuffer tsql = new StringBuffer(255);
            tsql.append("select * from LCReInsurUWIdea where polno='");
            tsql.append(mPolNo);
            tsql.append("' order by uwno desc");
            LCReInsurUWIdeaSet tLCReInsurUWIdeaSet = new LCReInsurUWIdeaSet();
            tLCReInsurUWIdeaSet = tLCReInsurUWIdeaDB.executeQuery(tsql.toString());
            if (tLCReInsurUWIdeaSet == null || tLCReInsurUWIdeaSet.size() == 0)
            {
                tuwno = 1;
            } else
            {
                tuwno = tLCReInsurUWIdeaSet.size() + 1;
            }
            //String strOthNo[] = mFileName.split("=");
            mLCReInsurUWIdeaSchema.setUWNo(tuwno);
            mLCReInsurUWIdeaSchema.setPolNo(mPolNo);
            mLCReInsurUWIdeaSchema.setAdjunctPath(StrTool.unicodeToGBK(
                    mFilePath +
                    mFileName));
            mLCReInsurUWIdeaSchema.setUWOperator(mGlobalInput.Operator);
            mLCReInsurUWIdeaSchema.setOperator(mGlobalInput.Operator);
            mLCReInsurUWIdeaSchema.setUWIdea(mRemark);
            mLCReInsurUWIdeaSchema.setManageCom(mGlobalInput.ManageCom);
            mLCReInsurUWIdeaSchema.setMakeDate(PubFun.getCurrentDate());
            mLCReInsurUWIdeaSchema.setMakeTime(PubFun.getCurrentTime());
            mLCReInsurUWIdeaSchema.setModifyDate(PubFun.getCurrentDate());
            mLCReInsurUWIdeaSchema.setModifyTime(PubFun.getCurrentTime());

            map.put("update LWMission set  ActivityStatus='" + 2 +
                    "' where Activityid in ('0000001100','0000001160') and missionprop2 ='" +
                    tLCPolDB.getProposalContNo() + "'", "UPDATE");
            map.put("update LCPol set  reinsureflag='" + 2 +
                    "' where  polno ='" + tLCPolDB.getPolNo() + "'", "UPDATE");

            return true;
        }
    }

    /**
     * prepareOutputData
     *
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        System.out.println("into BriefCardSignBL.prepareOutputData()...");
        map.put(mLCReInsurTaskSchema, "INSERT");
        map.put(mLCReInsurUWIdeaSchema, "INSERT");
        this.mResult.add(map);
        return true;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "BriefCardSignBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}
