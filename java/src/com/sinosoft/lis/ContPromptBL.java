package com.sinosoft.lis;

import com.sinosoft.lis.f1print.PrintService;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class ContPromptBL implements PrintService {
	public CErrors mErrors = new CErrors();
	private VData mResult = new VData();
	private SSRS mSSRS;
	private SSRS mSSRS1;
	private GlobalInput mGlobalInput = new GlobalInput();

	TextTag textTag = new TextTag();
	XmlExport xmlexport = new XmlExport();
	private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

	public CErrors getErrors() {

		return mErrors;
	}

	public VData getResult() {

		return mResult;
	}

	public boolean submitData(VData cInputData, String cOperate) {

		System.out.println("ContPromptBL begin");

		if (!getInputData(cInputData)) {
			return false;
		}

		mResult.clear();
		if (!getListData()) {
			return false;
		}
		if (!dealPrintMag()) {
			return false;
		}
		if (!getPrintData()) {
			return false;
		}
		System.out.println("ContPromptBL end");
		return true;
	}

	@SuppressWarnings("unchecked")
	private boolean getPrintData() {
		try {
			textTag.add("JetFormType", "Prompt");
			textTag.add("Today", PubFun.getCurrentDate());
			if ("86".equals(mGlobalInput.ManageCom)) {
				textTag.add("ManageComLength4", "8600");
			} else {
				textTag.add("ManageComLength4", mGlobalInput.ManageCom
						.substring(0, 4));
			}
			textTag.add("Year", mSSRS.GetText(1, 1));
			textTag.add("Quarter", mSSRS.GetText(1, 2));
			textTag.add("Solvency", mSSRS.GetText(1, 3));
			textTag.add("YorN", mSSRS.GetText(1, 4));
			textTag.add("CYear", mSSRS1.GetText(1, 1));
			textTag.add("CQuarter", mSSRS1.GetText(1, 2));
			textTag.add("Riskrate", mSSRS1.GetText(1, 3));
			xmlexport.createDocument("ContPrompt", "");
			xmlexport.addTextTag(textTag);
			mResult.clear();
			mResult.add(mLOPRTManagerSchema);
			mResult.addElement(xmlexport);

		} catch (Exception e) {
		}
		return true;

	}

	private boolean dealPrintMag() {
		 mLOPRTManagerSchema.setPrtSeq("Prompt");
		 mLOPRTManagerSchema.setOtherNo("Prompt");
		 mLOPRTManagerSchema.setOtherNoType("00");
		 mLOPRTManagerSchema.setCode("Prompt");
		 mLOPRTManagerSchema.setManageCom("86110000");
		 mLOPRTManagerSchema.setAgentCode("1101000001");
		 mLOPRTManagerSchema.setReqCom("86110000");
		 mLOPRTManagerSchema.setReqOperator("gzh");
		 mLOPRTManagerSchema.setPrtType("0");
		 mLOPRTManagerSchema.setStateFlag("0");
		 mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
		 mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
		 mLOPRTManagerSchema.setStandbyFlag2("");
		 mLOPRTManagerSchema.setStandbyFlag1("");
		 mResult.addElement(mLOPRTManagerSchema);

//		mLOPRTManagerSchema.setPrtSeq("Prompt");
//		mLOPRTManagerSchema.setOtherNo("Prompt");
//		mLOPRTManagerSchema.setOtherNoType("");
//		mLOPRTManagerSchema.setCode("Prompt");
//		mLOPRTManagerSchema.setManageCom("");
//		mLOPRTManagerSchema.setAgentCode("");
//		mLOPRTManagerSchema.setReqCom("");
//		mLOPRTManagerSchema.setReqOperator("");
//		mLOPRTManagerSchema.setPrtType("");
//		mLOPRTManagerSchema.setStateFlag("");
//		mLOPRTManagerSchema.setMakeDate("");
//		mLOPRTManagerSchema.setMakeTime("");
//		mLOPRTManagerSchema.setStandbyFlag2("");
//		mLOPRTManagerSchema.setStandbyFlag1("");
//		mResult.addElement(mLOPRTManagerSchema);

		return true;
	}

	private boolean getListData() {
		String sql = "  select Year,quarter,solvency||'%',case when flag='1' then '达到' else '未达到' end  from  ldriskrate "
				+ "where  codetype='CFNL' and state='1'  order by Year desc,quarter desc fetch first row only  with ur ";
		ExeSQL tExeSQL = new ExeSQL();

		mSSRS = tExeSQL.execSQL(sql.toString());
		if (tExeSQL.mErrors.needDealError()) {
			CError tError = new CError();
			tError.moduleName = "ContPromptBL";
			tError.functionName = "getListData";
			tError.errorMessage = "未查到综合偿付能力充足率！";
			this.mErrors.addOneError(tError);
			return false;
		}
		String sql1 = "   select Year,quarter,riskrate  from  ldriskrate  where  codetype='FXDJ' and state='1' "
			+" order by Year desc,quarter desc fetch first row only  with ur  ";
		ExeSQL ExeSQL = new ExeSQL();

		mSSRS1 = ExeSQL.execSQL(sql1.toString());
		if (tExeSQL.mErrors.needDealError()) {
			CError tError = new CError();
			tError.moduleName = "ContPromptBL";
			tError.functionName = "getListData";
			tError.errorMessage = "未查到风险综合评级！";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	private boolean getInputData(VData cInputData) {
		mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0);
		return true;
	}

}
