package com.sinosoft.lis.brieftelesales;

import java.io.*;
import javax.xml.parsers.*;

import org.w3c.dom.*;

import com.sinosoft.lis.brieftb.BriefSingleContInputUI;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.tb.ContDeleteUI;
import com.sinosoft.lis.tb.LCPolImpInfo;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.workflow.brieftb.BriefTbWorkFlowUI;
import com.sinosoft.workflow.tb.*;

/*
 * <p>Title: Web业务系统</p>
 * <p>ClassName:TSParseGuideIn </p>
 * <p>Description: 个单磁盘投保入口类文件 </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: Sinosoft </p>
 * @author：zhangtao
 * @version：1.0
 * @CreateDate：2005-01-05
 */

public class TSParseGuideIn
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    public CErrors logErrors = new CErrors();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /**内存文件暂存*/
    private GlobalInput mGlobalInput = new GlobalInput();

    private TransferData mTransferData = new TransferData();

    private String FileName;

    private String XmlFileName;

    private String mPreFilePath;

    private String FilePath = "E:/temp/";

    private String ParseRootPath = "/DATASET/BATCHNO";

    private String ParsePath = "/DATASET/CONTTABLE/ROW";

    //配置文件Xml节点描述
    private String ImportFileName;

    private String ConfigFileName;

    private String mBatchNo = "";

    private String mPrtNo = "";

    private String mContID = null;

    private org.w3c.dom.Document m_doc = null;

    private LCPolImpInfo m_LCPolImpInfo = new LCPolImpInfo();

    private String[] m_strDataFiles = null;

    private boolean ShowSchedule = false;

    boolean mIsBPO = false;

    public TSParseGuideIn()
    {
        bulidDocument();
    }

    public TSParseGuideIn(String fileName)
    {
        bulidDocument();
        FileName = fileName;
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        mInputData = (VData) cInputData.clone();
        getInputData();
        if (!checkData())
        {
            return false;
        }
        System.out.println("开始时间:" + PubFun.getCurrentTime());
        try
        {
            if (!parseVts())
            {
                CError tError = new CError();
                tError.moduleName = "TSParseGuideIn";
                tError.functionName = "checkData";
                tError.errorMessage = this.mErrors.getFirstError();
                logErrors.addOneError(tError);
                return false;
            }

            for (int nIndex = 0; nIndex < m_strDataFiles.length; nIndex++)
            {
                XmlFileName = m_strDataFiles[nIndex];
                if (!ParseXml())
                {
                    CError tError = new CError();
                    tError.moduleName = "TSParseGuideIn";
                    tError.functionName = "checkData";
                    tError.errorMessage = this.mErrors.getFirstError();
                    logErrors.addOneError(tError);
                    return false;
                }
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "TSParseGuideIn";
            tError.functionName = "checkData";
            tError.errorMessage = "导入文件格式有误!";
            logErrors.addOneError(tError);
        }

        mErrors = logErrors;
        System.out.println("结束时间:" + PubFun.getCurrentTime());
        if (mErrors.getErrorCount() > 0)
        {
            return false;
        }

        return true;
    }

    /**
     * 得到传入数据
     */
    private void getInputData()
    {
        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mTransferData = (TransferData) mInputData.getObjectByObjectName(
                "TransferData", 0);
        mPreFilePath = (String) mTransferData.getValueByName("FilePath");
        System.out.println(mPreFilePath);
    }

    /**
     * 校验传输数据
     *
     * @return boolean
     */
    private boolean checkData()
    {
        if (mGlobalInput == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ParseGuideIn";
            tError.functionName = "checkData";
            tError.errorMessage = "无操作员信息，请重新登录!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (mTransferData == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ParseGuideIn";
            tError.functionName = "checkData";
            tError.errorMessage = "无导入文件信息，请重新导入!";
            this.mErrors.addOneError(tError);
            return false;
        }
        else
        {
            FileName = (String) mTransferData.getValueByName("FileName");
            System.out.println("FileName" + FileName);
        }
        return true;
    }

    /**
     * 得到生成文件路径
     *
     * @return boolean
     */
    private boolean getFilePath()
    {
        LDSysVarDB tLDSysVarDB = new LDSysVarDB();
        tLDSysVarDB.setSysVar("XmlPath");
        if (!tLDSysVarDB.getInfo())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ParseGuideIn";
            tError.functionName = "checkData";
            tError.errorMessage = "缺少文件导入路径!";
            this.mErrors.addOneError(tError);
            return false;
        }
        else
        {
            FilePath = tLDSysVarDB.getSysVarValue();
        }

        return true;
    }

    /**
     * 检验文件是否存在
     *
     * @return boolean
     */
    private boolean checkXmlFileName()
    {
        File tFile = new File(XmlFileName);
        if (!tFile.exists())
        {
            LDSysVarDB tLDSysVarDB = new LDSysVarDB();
            tLDSysVarDB.setSysVar("XmlPath");
            if (!tLDSysVarDB.getInfo())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ParseGuideIn";
                tError.functionName = "checkData";
                tError.errorMessage = "缺少文件导入路径!";
                this.mErrors.addOneError(tError);
                return false;
            }
            else
            {
                FilePath = tLDSysVarDB.getSysVarValue();
            }

            File tFile1 = new File(FilePath);
            if (!tFile1.exists())
            {
                tFile1.mkdirs();
            }
            XmlFileName = FilePath + XmlFileName;
            File tFile2 = new File(XmlFileName);
            if (!tFile2.exists())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ParseGuideIn";
                tError.functionName = "checkData";
                tError.errorMessage = "请上传相应的数据文件到指定路径" + FilePath + "!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }

        //得到批次号
        XMLPathTool tXPT = new XMLPathTool(XmlFileName);
        if (!getBatchNo(tXPT))
        {
            return false;
        }

        return true;
    }

    /**
     * 检查导入配置文件是否存在
     *
     * @return boolean
     */
    private boolean checkImportConfig()
    {
        this.getFilePath();

        String filePath = mPreFilePath + FilePath;
        //String filePath = "E:/v.1.1/ui/temp/";
        File tFile1 = new File(filePath);
        if (!tFile1.exists())
        {
            //初始化创建目录
            tFile1.mkdirs();
        }

        ConfigFileName = filePath + "TSImport.xml";
        File tFile2 = new File(ConfigFileName);
        if (!tFile2.exists())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ParseGuideIn";
            tError.functionName = "checkData";
            tError.errorMessage = "请上传配置文件到指定路径" + FilePath + "!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 初始化上传文件
     *
     * @return boolean
     */
    private boolean initImportFile()
    {
        this.getFilePath();
        ImportFileName = mPreFilePath + FilePath + FileName;
        //ImportFileName = "E:/v.1.1/ui/temp/" + FileName;

        File tFile = new File(ImportFileName);
        if (!tFile.exists())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "TSParseGuideIn";
            tError.functionName = "checkData";
            tError.errorMessage = "未上传文件到指定路径" + FilePath + "!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("-----导入文件");
        return true;
    }

    /**
     * 解析excel并转换成xml文件
     *
     * @return boolean
     * @throws Exception
     */
    private boolean parseVts() throws Exception
    {
        //初始化导入文件
        if (!this.initImportFile())
        {
            return false;
        }
        //检查导入配置文件是否存在
        if (!this.checkImportConfig())
        {
            return false;
        }

        TSPolVTSParser lcpvp = new TSPolVTSParser();

        if (!lcpvp.setFileName(ImportFileName))
        {
            mErrors.copyAllErrors(lcpvp.mErrors);
            return false;
        }
        if (!lcpvp.setConfigFileName(ConfigFileName))
        {
            mErrors.copyAllErrors(lcpvp.mErrors);
            return false;
        }
        //转换excel到xml
        if (!lcpvp.transform())
        {
            mErrors.copyAllErrors(lcpvp.mErrors);
            return false;
        }

        // 得到生成的XML文件名列表
        m_strDataFiles = lcpvp.getDataFiles();
        return true;
    }

    /**
     * 解析xml
     *
     * @return boolean
     */
    private boolean ParseXml()
    {
        if (!checkXmlFileName())
        {
            return false;
        }
        this.mErrors.clearErrors();

        //得到保单的传入信息
        XMLPathTool tXPT = new XMLPathTool(XmlFileName);
        NodeList nodeList = tXPT.parseN(ParsePath);

        //生成日志
        if (!dealImportLog(nodeList, tXPT))
        {
            return false;
        }

        //循环处理每个保单节点，生成保单数据
        try
        {
            if (!createCont(nodeList))
            {
                return false;
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            mErrors.addOneError("生成保单信息失败");
        }

        if (this.mErrors.needDealError())
        {
            System.out.println(this.mErrors.getErrorCount() + "error:"
                    + this.mErrors.getFirstError());
        }

        //解析完删除XML文件
        File tFile = new File(XmlFileName);
        if (!mIsBPO && tFile.exists())
        {
            tXPT = null;
            if (!tFile.delete())
            {
                System.out.println("删除Ｘｍｌ文件失败！");
            }
            else
            {
                System.out.println("删除Ｘｍｌ文件成功！");
            }
        }

        return true;
    }

    /**
     * getBatchNo
     * 得到批次信息BatchNo、BPOBatchNo
     * @return boolean
     */
    private boolean getBatchNo(XMLPathTool tXPT)
    {
        try
        {
            //批次号
            mBatchNo = tXPT.parseX(ParseRootPath).getFirstChild()
                    .getNodeValue();

            if (mBatchNo == null || mBatchNo.equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "TSParseGuideIn";
                tError.functionName = "ParseXml";
                tError.errorMessage = "没有获得批次号";
                mErrors.addOneError(tError);
                System.out.println(tError.errorMessage);
                return false;
            }

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "TSParseGuideIn";
            tError.functionName = "getBatchNo";
            tError.errorMessage = "获取批次号信息出现异常。";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }

    /**
     * createCont
     * 循环处理每个保单节点，生成保单数据
     * @return boolean
     */
    private boolean createCont(NodeList nodeList)
    {
    	TSPolParser tTSPolParser = new TSPolParser();
        //循环每个保单
    	boolean allFlag = true;
        for (int i = 0; i < nodeList.getLength(); i++)
        {
        	this.mErrors.clearErrors();
        	BriefSingleContInputUI tBriefSingleContInputUI = new BriefSingleContInputUI();
        	mPrtNo = "";

            Node node = nodeList.item(i);

            try
            {
                node = transformNode(node);
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                node = null;
            }

            NodeList contNodeList = node.getChildNodes();
            if (contNodeList.getLength() <= 0)
            {
                continue;
            }

            TransferData tContData;
            //解析保单数据
            try
            {
                tContData = dealOneCont(tTSPolParser, contNodeList,String.valueOf(i+1));
                mContID = String.valueOf(i+1);
                if("".equals(StrTool.cTrim(mPrtNo))){
                	String error = "保单"+mContID+"的印刷号为空！";
                	buildError("createCont",error);
                    allFlag = false;
                    continue;
                }
                LCContSet tLCContSet = (LCContSet) tContData.getValueByName("tLCContSet"); //合同
                LCContSchema tLCContSchema = new LCContSchema();
                if(tLCContSet.size()!=1){
                	String error = "保单"+mPrtNo+"的保单节点数量错误！";
                	buildError("createCont",error);
                    allFlag = false;
                    continue;
                }else{
                	String tSql = "select 1 from lccont where prtno = '"+mPrtNo+"' ";
                	String havCont = new ExeSQL().getOneValue(tSql);
                	if(havCont != null && !"".equals(havCont)){
                		String error = "保单"+mPrtNo+"已存在，请核实该单印刷号码是否正确！";
                    	buildError("createCont",error);
                        allFlag = false;
                        continue;
                	}
                	tLCContSchema = tLCContSet.get(1);
                	tLCContSchema.setCValiDate(PubFun.calDate(PubFun.getCurrentDate(), 1, "D", null));
                	String tError = checkLCCont(tLCContSchema);
                	if(tError != null && !"".equals(tError)){
                		buildError("createCont",tError);
                        allFlag = false;
                        continue;
                	}
                }
                VData tAppntVData = (VData) tContData.getValueByName("tAppntVData"); //投保人
                LCAppntSet tLCAppntSet = (LCAppntSet) tAppntVData.getObjectByObjectName("LCAppntSet", 0);
                LCAddressSet mAppntAddressSet = (LCAddressSet) tAppntVData.getObjectByObjectName("LCAddressSet", 0);
                LCAddressSchema mAppntAddressSchema = new LCAddressSchema();
                LCAppntSchema tLCAppntSchema = new LCAppntSchema();
                if(tLCAppntSet.size()!=1){
                	String error = "保单"+mPrtNo+"的投保人节点数量错误！";
                	buildError("createCont",error);
                    allFlag = false;
                    continue;
                }else{
                	tLCAppntSchema = tLCAppntSet.get(1);
//                    	将客户身份证号码中的x转换成大写（投保人）
                	if (tLCAppntSchema.getIDType() != null && tLCAppntSchema.getIDNo() != null)
                    {
                        if (tLCAppntSchema.getIDType().equals("0"))
                        {
                            String tLCAppntIdNo = tLCAppntSet.get(1).getIDNo().toUpperCase();
                            tLCAppntSchema.setIDNo(tLCAppntIdNo);
                        }
                    }
                	mAppntAddressSchema = mAppntAddressSet.get(1);
                	String tError = checkLCAppnt(tLCAppntSchema,mAppntAddressSchema);
                	if(tError != null && !"".equals(tError)){
                		buildError("createCont",tError);
                        allFlag = false;
                        continue;
                	}
                }
                VData tInsuredVData = (VData) tContData.getValueByName("tInsuredVData"); //被保人
                LCInsuredSet tLCInsuredSet = (LCInsuredSet) tInsuredVData.getObjectByObjectName("LCInsuredSet", 0);
                LCAddressSet mInsuredAddressSet = (LCAddressSet) tInsuredVData.getObjectByObjectName("LCAddressSet", 0);
                LCAddressSchema mInsuredAddressSchema  = new LCAddressSchema();
                LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
                if(tLCInsuredSet.size()!=1){
                	String error = "保单"+mPrtNo+"的被保人节点数量错误！";
                	buildError("createCont",error);
                    allFlag = false;
                    continue;
                }else{
                	tLCInsuredSchema = tLCInsuredSet.get(1);
//                    	将客户身份证号码中的x转换成大写（被保险人）
                	if (tLCInsuredSchema.getIDType() != null && tLCInsuredSchema.getIDNo() != null)
                    {
                        if (tLCInsuredSchema.getIDType().equals("0"))
                        {
                            String tLCInsuredIdNo = tLCInsuredSchema.getIDNo().toUpperCase();
                            tLCInsuredSchema.setIDNo(tLCInsuredIdNo);
                        }
                    }
                	mInsuredAddressSchema = mInsuredAddressSet.get(1);
                	String tError = checkLCInsrued(tLCInsuredSchema,mInsuredAddressSchema);
                	if(tError != null && !"".equals(tError)){
                		buildError("createCont",tError);
                        allFlag = false;
                        continue;
                	}
                }
                
                LCBnfSet tLCBnfSet = (LCBnfSet) tContData.getValueByName("tLCBnfSet"); //受益人
                LCRiskDutyWrapSet tLCRiskDutyWrapSet = (LCRiskDutyWrapSet) tContData.getValueByName("tLCRiskDutyWrapSet"); //产品
                if(tLCRiskDutyWrapSet.size()<1){
                	String error = "保单"+mPrtNo+"的产品节点数据错误！";
                	buildError("createCont",error);
                    allFlag = false;
                    continue;
                }

            	System.out.println("开始进行电销保单工作流创建！");
            	VData tWVData = new VData();
            	TransferData tWTransferData = new TransferData();
            	tWTransferData.setNameAndValue("PrtNo",mPrtNo);
            	tWTransferData.setNameAndValue("ManageCom",mGlobalInput.ManageCom);
            	tWTransferData.setNameAndValue("InputDate",PubFun.getCurrentDate());
            	tWTransferData.setNameAndValue("Operator",mGlobalInput.Operator);
            	tWTransferData.setNameAndValue("ContType","5");
            	tWVData.add( tWTransferData );
            	tWVData.add( mGlobalInput );
                String tActivityID = "9999997101";//默认为电销开始节点标识
        		BriefTbWorkFlowUI tBriefTbWorkFlowUI = new BriefTbWorkFlowUI();
        		if (tBriefTbWorkFlowUI.submitData(tWVData,tActivityID) == false)
        		{
        			System.out.println("电销保单工作流创建失败！");
					int n = tBriefTbWorkFlowUI.mErrors.getErrorCount();
					String error = "";
					for (int j = 0; j < n; j++)
					{
						System.out.println("Error: "+tBriefTbWorkFlowUI.mErrors.getError(j).errorMessage);
					  	error += tBriefTbWorkFlowUI.mErrors.getError(j).errorMessage;
					}
					
					buildError("createCont",error);
					allFlag = false;
					continue;
        		}
        		else       
                {
        			System.out.println("电销工作流完成后开始生成保单数据！");
        			VData tVData = new VData();
                	TransferData tTransferData = new TransferData();
                    tTransferData.setNameAndValue("AppntAddress", mAppntAddressSchema);
                    tTransferData.setNameAndValue("InsuredAddress", mInsuredAddressSchema);
                    tTransferData.setNameAndValue("WorkName", mAppntAddressSchema.getGrpName());
                    tTransferData.setNameAndValue("MissionProp5", "5");
                    tTransferData.setNameAndValue("WrapCode", tLCRiskDutyWrapSet.get(1).getRiskWrapCode());
                    tTransferData.setNameAndValue("ContType", "5");
                	tVData.add(tLCContSchema);
                    tVData.add(tLCInsuredSchema);
                    tVData.add(tLCAppntSchema);
                    tVData.add(mGlobalInput);
                    tVData.add(tTransferData);
                    tVData.add(tLCBnfSet);
                    tVData.add(tLCRiskDutyWrapSet);
                    
                    LCNationSet tLCNationSet = new LCNationSet();
                    tVData.add(tLCNationSet);
                    if(tBriefSingleContInputUI.submitData(tVData, "INSERT||MAIN")){
                    	String tError = AutoUwOneCont();
                    	if(!"".equals(tError)){
                    		System.out.println("电销自核失败，tError："+tError);
                    		buildError("createCont",tError);
                        	allFlag = false;
//        						回滚数据
    						ContDeleteUI tContDeleteUI = new ContDeleteUI();
    						TransferData tDeTransferData = new TransferData();
    						tDeTransferData.setNameAndValue("DeleteReason",tError);
    						VData tDeVData = new VData();
    						tDeVData.add( tLCContSchema );
    						tDeVData.add( tDeTransferData );
    						tDeVData.add( mGlobalInput );
    						if (tContDeleteUI.submitData(tDeVData,"DELETE") == false)
    						{
    							System.out.println("回滚数据时失败！");
    							String error = "回滚数据时失败";
    							buildError("createCont",error);
    						}
    						
							continue;
                    	}else{
                    		MMap tMap = m_LCPolImpInfo.logSucc(mBatchNo, mContID, mPrtNo, "", "", mGlobalInput);
                    		VData tempVData = new VData();
                            tempVData.add(tMap);
                            PubSubmit tPubSubmit = new PubSubmit();
                            if (!tPubSubmit.submitData(tempVData, ""))
                            {
                                allFlag= false;
                                String error = "保单"+mPrtNo+"自核成功后，保存日志信息失败！";
                                buildError("createCont",error);
                            }
                    	}
                    }else{
                    	System.out.println("电销保单保存失败！");
                    	String error = "";
                    	int n = tBriefSingleContInputUI.mErrors.getErrorCount();
                    	for (int j = 0; j < n; j++)
						{
							System.out.println("Error: "+tBriefSingleContInputUI.mErrors.getError(j).errorMessage);
						  	error += tBriefSingleContInputUI.mErrors.getError(j).errorMessage;
						}
						buildError("createCont",error);
                    	allFlag = false;
//    						回滚数据
                    	String tSQL = "select 1 from lccont where prtno = '"+mPrtNo+"' ";
                    	String tHavCont = new ExeSQL().getOneValue(tSQL);
                    	if("1".equals(tHavCont)){
                    		ContDeleteUI tContDeleteUI = new ContDeleteUI();
    						TransferData tDeTransferData = new TransferData();
    						tDeTransferData.setNameAndValue("DeleteReason",error);
    						VData tDeVData = new VData();
    						tDeVData.add( tLCContSchema );
    						tDeVData.add( tDeTransferData );
    						tDeVData.add( mGlobalInput );
    						if (tContDeleteUI.submitData(tDeVData,"DELETE") == false)
    						{
    							System.out.println("回滚数据时失败！");
    							error = "回滚数据时失败";
    							buildError("createCont",error);
    						}
                    	}else{
                    		String delWM = "delete from lwmission where missionprop1 = '"+mPrtNo+"' ";
                    		new ExeSQL().execUpdateSQL(delWM);
                    	}
						
						continue;
                    }
                }
                	
            }
            catch (NumberFormatException ex)
            {
                CError.buildErr(this, "数据类型转换失败:" + ex.getMessage());
                ex.printStackTrace();
                m_LCPolImpInfo.logError(mBatchNo, mContID, "", "", "", null,
                        mErrors, mGlobalInput, mPrtNo);
                this.logErrors.copyAllErrors(tTSPolParser.mErrors);
                this.mErrors.clearErrors();
            }
            catch (Exception ex)
            {
                CError.buildErr(this, "未知异常导致数据导入失败：" + ex.getMessage());
                ex.printStackTrace();
                m_LCPolImpInfo.logError(mBatchNo, mContID, "", "", "", null,
                        mErrors, mGlobalInput, mPrtNo);
                this.logErrors.copyAllErrors(tTSPolParser.mErrors);
                this.mErrors.clearErrors();
            }

        }
//      删除保单工作流节点\
		MMap tempMMap = new MMap();
		String tSql = "select missionid from lwmission where missionprop1 = '"+mBatchNo+"'";
        LWMissionDB tLWMissionDB = new LWMissionDB();
        tLWMissionDB.setMissionID(new ExeSQL().getOneValue(tSql));
        LWMissionSet tLWMissionSet = tLWMissionDB.query();
        //备份工作流节点
        LBMissionSet tLBMissionSet = new LBMissionSet();
        LBMissionSchema tLBMissionSchema;
        
        Reflections tReflections = new Reflections();
        for (int k = 1; k <= tLWMissionSet.size(); k++) {
            tLBMissionSchema = new LBMissionSchema();
            tReflections.transFields(tLBMissionSchema, tLWMissionSet.get(k));

            String serialNo = PubFun1.CreateMaxNo("MissionSerielNo", 10);
            tLBMissionSchema.setSerialNo(serialNo);
            tLBMissionSchema.setActivityStatus("3"); //节点任务执行完毕
            tLBMissionSchema.setLastOperator(mGlobalInput.Operator);
            tLBMissionSchema.setMakeDate(PubFun.getCurrentDate());
            tLBMissionSchema.setMakeTime(PubFun.getCurrentTime());
            tLBMissionSchema.setModifyDate(PubFun.getCurrentDate());
            tLBMissionSchema.setModifyTime(PubFun.getCurrentTime());
            tLBMissionSet.add(tLBMissionSchema);
        }
        tempMMap.put(tLBMissionSet, SysConst.DELETE_AND_INSERT);
        tempMMap.put(tLWMissionSet, SysConst.DELETE);
        VData tempVData = new VData();
        tempVData.add(tempMMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(tempVData, ""))
        {
            allFlag= false;
            String error = "批次"+mBatchNo+"下保单自核完成后，删除该批次工作流失败！";
            buildError("createCont",error);
        }
        return allFlag;
    }
    
    /**
     * dealOneCont
     */
    private TransferData dealOneCont(TSPolParser tTSPolParser,NodeList contNodeList,String ContID)
    {
        TransferData tTransferData = new TransferData();

        Node contNode = null;
        String nodeName = "";
        mContID = ContID;

        for (int j = 0; j < contNodeList.getLength(); j++)
        {
            contNode = contNodeList.item(j);
            nodeName = contNode.getNodeName();
            if (nodeName.equals("#text"))
            {
                continue;
            }
            if (nodeName.equals("PRTNO"))
            {
                mPrtNo = contNode.getFirstChild().getNodeValue();
                System.out.println( "保单PrtNo:" + mPrtNo);
                if (mPrtNo == null || mPrtNo.equals("")){
                	System.out.println("第"+j+"个保单的印刷号码为空！");
                	CError tError = new CError();
                    tError.moduleName = "TSParseGuideIn";
                    tError.functionName = "dealOneCont";
                    tError.errorMessage = "第"+j+"个保单的印刷号码为空！";
                    mErrors.addOneError(tError);
                    System.out.println(tError.errorMessage);
                	if(!m_LCPolImpInfo.logError(mBatchNo, mContID, "", "", "", null,
                            mErrors, mGlobalInput, mPrtNo)){
                		System.out.println( "印刷号为空时，保存错误日志信息失败。");
                		continue;
                	}
                }
                continue;
            }
            
            if (nodeName.equals("LCCONTTABLE"))
            {
                //解析一个险种保单节点
            	
            	LCContSet tLCContSet = tTSPolParser.getLCContSet(contNode);
                tTransferData.setNameAndValue("tLCContSet", tLCContSet);
                continue;

            }

            if (nodeName.equals("APPNTTABLE"))
            {
                //解析投保人XML
                VData tAppntVData = tTSPolParser.getLCAppntData(contNode);
                tTransferData.setNameAndValue("tAppntVData", tAppntVData);
                continue;
            }

            if (nodeName.equals("INSUREDTABLE"))
            {
                //解析被保险人XML
                VData tInsuredVData = tTSPolParser.getLCInsuredData(contNode);
                tTransferData.setNameAndValue("tInsuredVData", tInsuredVData);
                continue;
            }
            
            if (nodeName.equals("BNFTABLE"))
            {
                //解析受益人XML
                LCBnfSet tLCBnfSet = (LCBnfSet) tTSPolParser.getLCBnfSet(contNode);
                tTransferData.setNameAndValue("tLCBnfSet", tLCBnfSet);

                continue;
            }
            if (nodeName.equals("WRAPTABLE"))
            {
                //解析客户告知XML
            	LCRiskDutyWrapSet tLCRiskDutyWrapSet = (LCRiskDutyWrapSet) tTSPolParser.getLCRiskDutyWrapSet(contNode);
                tTransferData.setNameAndValue("tLCRiskDutyWrapSet",tLCRiskDutyWrapSet);
                continue;
            }

        }

        return tTransferData;
    }

    /**
     * 生成日志
     * 若批次号mBatchNo已导入，则报错
     * @param nodeList NodeList：保单节点
     * @param tXPT XMLPathTool
     * @return boolean
     */
    private boolean dealImportLog(NodeList nodeList, XMLPathTool tXPT)
    {
        LCGrpImportLogDB dLCGrpImportLogDB = new LCGrpImportLogDB();
        LCGrpImportLogSet dLCGrpImportLogSet = new LCGrpImportLogSet();
        dLCGrpImportLogDB.setBatchNo(mBatchNo);
        //        dLCGrpImportLogDB.setBatchNo("X");
        dLCGrpImportLogSet = dLCGrpImportLogDB.query();
        if (dLCGrpImportLogSet.size() > 0)
        {
            CError tError = new CError();
            tError.moduleName = "ParseGuideIn";
            tError.functionName = "checkData";
            tError.errorMessage = "该文件已经导入过，请更改文件名后再导入！";
            this.mErrors.addOneError(tError);
            return false;
        }

        int nNodeCount = 0;

        if (nodeList != null)
        {
            nNodeCount = nodeList.getLength();
        }

        //循环中，每一次都调用一次承包后台程序，首先挑出一条保单纪录，
        //再通过ID号找到保费项，责任，给付等，组合成Set集合
        //此时新单是没有投保单号的，因此在保单，保费项，
        //责任等纪录中投保单号为空,后台自动生成投保单号
        //最后将数据放入VData中，传给GrpPollmport模块。一次循环完毕
        /**
         * yangming:为显示进度条要将投保文件的合同数存储在日志表中
         */
        if (!ShowSchedule)
        {
            MMap map = new MMap();
            /**
             * 第一个sql语句用记录xml文件中的合同数,ContID=C代表合同数
             * 第二个sql语句用于记录磁盘文件解析成为几个xml文件,ContID=X代表Xml数
             */
            String strSql1 = "insert into LCGrpImportLog (GRPCONTNO,BATCHNO,CONTID,ERRORINFO,OPERATOR,MAKEDATE,MAKETIME)"
                    + " values ('00000000000000000000','"
                    + mBatchNo
                    + "','C','"
                    + nNodeCount
                    + "','"
                    + mGlobalInput.Operator
                    + "','"
                    + PubFun.getCurrentDate()
                    + "','"
                    + PubFun.getCurrentTime() + "' )";
            String strSql2 = "insert into LCGrpImportLog (GRPCONTNO,BATCHNO,CONTID,ERRORINFO,OPERATOR,MAKEDATE,MAKETIME)"
                    + " values ('00000000000000000000','"
                    + mBatchNo
                    + "','X','"
                    + m_strDataFiles.length
                    + "','"
                    + mGlobalInput.Operator
                    + "','"
                    + PubFun.getCurrentDate()
                    + "','" + PubFun.getCurrentTime() + "' )";
            String delSql1 = "delete from LCGrpImportLog where BatchNo='"
                    + mBatchNo + "' and contid='X'";
            String delSql2 = "delete from LCGrpImportLog where BatchNo='"
                    + mBatchNo + "' and contid='C'";
            map.put(delSql1, "DELETE");
            map.put(delSql2, "DELETE");
            map.put(strSql1, "INSERT");
            map.put(strSql2, "INSERT");
            VData tempVData = new VData();
            tempVData.add(map);
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(tempVData, "INSERT"))
            {
                CError tError = new CError();
                tError.moduleName = "ParseGuideIn";
                tError.functionName = "checkData";
                tError.errorMessage = "在准备反显进度条数据时发生错误！";
                this.mErrors.addOneError(tError);
                return false;
            }
            ShowSchedule = true;
        }

        return true;
    }




    public String getExtendFileName(String aFileName)
    {
        File tFile = new File(aFileName);
        String aExtendFileName = "";
        String name = tFile.getName();
        for (int i = name.length() - 1; i >= 0; i--)
        {
            if (i < 1)
            {
                i = 1;
            }
            if (name.substring(i - 1, i).equals("."))
            {
                aExtendFileName = name.substring(i, name.length());
                System.out.println("ExtendFileName;" + aExtendFileName);
                return aExtendFileName;
            }
        }
        return aExtendFileName;
    }

    /**
     * 字符串替换
     *
     * @param s1 String
     * @param OriginStr String
     * @param DestStr String
     * @return java.lang.String
     */
    public static String replace(String s1, String OriginStr, String DestStr)
    {
        s1 = s1.trim();
        int mLenOriginStr = OriginStr.length();
        for (int j = 0; j < s1.length(); j++)
        {
            int befLen = s1.length();
            if (s1.substring(j, j + 1) == null
                    || s1.substring(j, j + 1).trim().equals(""))
            {
                continue;
            }
            else
            {
                if (OriginStr != null && DestStr != null)
                {
                    if (j + mLenOriginStr <= s1.length())
                    {

                        if (s1.substring(j, j + mLenOriginStr)
                                .equals(OriginStr))
                        {

                            OriginStr = s1.substring(j, j + mLenOriginStr);

                            String startStr = s1.substring(0, j);
                            String endStr = s1.substring(j + mLenOriginStr, s1
                                    .length());

                            s1 = startStr + DestStr + endStr;

                            j = j + s1.length() - befLen;
                            if (j < 0)
                            {
                                j = 0;
                            }
                        }
                    }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
        }
        return s1;
    }

    /**
     * 得到日志显示结果
     *
     * @return com.sinosoft.utility.VData
     */
    public VData getResult()
    {
        return mResult;
    }

    public static void main(String[] args)
    {

        TSParseGuideIn tPGI = new TSParseGuideIn();

        VData tVData = new VData();
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("FileName", "TS8600000020120220006.xls");
        tTransferData.setNameAndValue("FilePath", "D:/PICCH/ui");
        GlobalInput tG = new GlobalInput();
        tG.Operator = "PolImport";
        tG.ManageCom = "86110000";
        tG.ComCode = "86110000";
        tVData.add(tTransferData);
        tVData.add(tG);

        String strStartTime = PubFun.getCurrentTime();

        tPGI.submitData(tVData, ""); //提交
        
        if (tPGI.mErrors.needDealError())
        {
            System.out.println("tPGI.mErrors : " + tPGI.mErrors.getFirstError());
        }
        String strEndTime = PubFun.getCurrentTime();

        System.out.println("===============================================");
        System.out.println("      the start time is : " + strStartTime);
        System.out.println("===============================================");

        System.out.println("===============================================");
        System.out.println("       the end time is : " + strEndTime);
        System.out.println("===============================================");

    }

    /**
     * Build a instance document object for function transfromNode()
     */
    private void bulidDocument()
    {
        DocumentBuilderFactory dfactory = DocumentBuilderFactory.newInstance();
        dfactory.setNamespaceAware(true);

        try
        {
            m_doc = dfactory.newDocumentBuilder().newDocument();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    /**
     * Detach node from original document, fast XPathAPI process.
     *
     * @param node Node
     * @return org.w3c.dom.Node
     * @throws Exception
     */
    private org.w3c.dom.Node transformNode(org.w3c.dom.Node node)
            throws Exception
    {
        Node nodeNew = m_doc.importNode(node, true);

        return nodeNew;
    }

    /**
     * 创建错误日志
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "ParseGuideIn";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        CErrors tCErrors = new CErrors();
        tCErrors.addOneError(cError);
        m_LCPolImpInfo.logError(mBatchNo, mContID, "", "", "",
                null, tCErrors, mGlobalInput, mPrtNo);
    }
    
    /**
     * 对保存后的保单数据进行核保
     *
     * @param 
     * @param 
     * @return 
     */
    
    private String AutoUwOneCont(){
    	String tError = "";
    	  String tLoadFlag="INPUT_PAGES";
    	  String tSQL = "select lcc.contno,lwm.missionid,lwm.submissionid,lwm.activityid,lcc.agentcode,lcc.inputdate " +
    	  		" from lccont lcc inner join LWMission lwm on lcc.prtno = lwm.missionprop1 " +
    	  		" where lcc.prtno = '"+mPrtNo+"'";
    	  SSRS tSSRS = new ExeSQL().execSQL(tSQL);
    	  if(tSSRS ==null || tSSRS.MaxRow !=1){
    		  
    		  tError = "电销业务自核时，获取保单和工作流数据失败！";
    		  return tError;
    	  }
    	  
    	  String tContNo = tSSRS.GetText(1, 1);
    	  String tMissionID = tSSRS.GetText(1, 2);
    	  String tSubMissionID = tSSRS.GetText(1, 3);
    	  String tActivityID = tSSRS.GetText(1, 4);
    	  String tAgentCode = tSSRS.GetText(1, 5);
    	  String tInputDate = tSSRS.GetText(1, 6);
    	  if(tContNo ==null || "".equals(tContNo)){
    		  tError = "电销业务自核时，获取保单号码失败！";
    		  return tError;
    	  }
    	  if(tMissionID ==null || "".equals(tMissionID)){
    		  tError = "电销业务自核时，获取工作流MissionID号码失败！";
    		  return tError;
    	  }
    	  if(tSubMissionID ==null || "".equals(tSubMissionID)){
    		  tError = "电销业务自核时，获取工作流SubMissionID失败！";
    		  return tError;
    	  }
    	  if(tActivityID ==null || "".equals(tActivityID)){
    		  tError = "电销业务自核时，获取工作流ActivityID号码失败！";
    		  return tError;
    	  }
    	  if(tContNo ==null || "".equals(tContNo)){
    		  tError = "电销业务自核时，获取保单业务员编码失败！";
    		  return tError;
    	  }
    	  System.out.println("开始电销自核，tActivityID："+tActivityID);
    	  TransferData tTransferData = new TransferData();
    	  tTransferData.setNameAndValue("MissionID",tMissionID);
    	  tTransferData.setNameAndValue("SubMissionID",tSubMissionID);
    	  tTransferData.setNameAndValue("ContNo",tContNo);
    	  tTransferData.setNameAndValue("PrtNo",mPrtNo);
    	  tTransferData.setNameAndValue("ManageCom",mGlobalInput.ManageCom);
    	  tTransferData.setNameAndValue("AgentCode",tAgentCode);
    	  tTransferData.setNameAndValue("InputDate",tInputDate);
    	  tTransferData.setNameAndValue("Operator",mGlobalInput.Operator);
    	  tTransferData.setNameAndValue("ContType","5");
    	  tTransferData.setNameAndValue("LoadFlag",tLoadFlag);  
    	  
    	  //为了兼容签单处理因此需要调用此信息。
    	  LCContSchema tLCContSchema = new LCContSchema();
    	  tLCContSchema.setContNo(tContNo);
//    	  String szTemplatePath = application.getRealPath("f1print/template/") + "/";	//模板路径
//    	  String sOutXmlPath = application.getRealPath("");	//xml文件输出路径
//    	  tTransferData.setNameAndValue("TemplatePath",szTemplatePath );
//    	  tTransferData.setNameAndValue("OutXmlPath",sOutXmlPath );
    	  
    	  VData tVData = new VData();
    	  tVData.add(tTransferData);
    	  tVData.add( tLCContSchema );
    	  tVData.add(mGlobalInput);
    	  
    	  TbWorkFlowUI tTbWorkFlowUI = new TbWorkFlowUI();
    	  if(!tTbWorkFlowUI.submitData(tVData,tActivityID))
    	  {
    		  tError = " 处理失败，原因是: " + tTbWorkFlowUI.mErrors.getFirstError();
    	      return tError;
    	  }
    	return tError;
    }
    
    //校验必录项
    private String checkLCCont(LCContSchema aLCContSchema){
    	String tError = "";
    	if("".equals(StrTool.cTrim(aLCContSchema.getPrtNo()))){
    		tError += "保单层数据印刷号不能为空。";
    	}
    	if("".equals(StrTool.cTrim(aLCContSchema.getManageCom()))){
    		tError += "保单层数据管理机构不能为空。";
    	}
    	String tSaleChnl = StrTool.cTrim(aLCContSchema.getSaleChnl());
    	if("".equals(tSaleChnl)){
    		tError += "保单层数据销售渠道不能为空。";
    	}
    	if(("10".equals(tSaleChnl) || "04".equals(tSaleChnl) || "08".equals(tSaleChnl) ) && "".equals(StrTool.cTrim(aLCContSchema.getAgentCom()))){
    		tError += "保单层数据销售渠道为中介时，中介机构不能为空。";
    	}
    	if("".equals(StrTool.cTrim(aLCContSchema.getAgentCode()))){
    		tError += "保单层数据业务员代码不能为空。";
    	}
    	if("".equals(StrTool.cTrim(aLCContSchema.getPolApplyDate()))){
    		tError += "保单层数据申请日期不能为空。";
    	}else if(!PubFun.validateDate(aLCContSchema.getPolApplyDate())){
    		tError += "保单层数据申请日期录入有误。";
    	}
    	if("".equals(StrTool.cTrim(aLCContSchema.getReceiveDate()))){
    		tError += "保单层数据收单日期不能为空。";
    	}else if(!PubFun.validateDate(aLCContSchema.getReceiveDate())){
    		tError += "保单层数据收单日期录入有误。";
    	}
    	if("".equals(StrTool.cTrim(String.valueOf(aLCContSchema.getPayIntv())))){
    		tError += "保单层数据缴费频次不能为空。";
    	}
    	if("".equals(StrTool.cTrim(aLCContSchema.getPayMode()))){
    		tError += "保单层数据缴费方式不能为空。";
    	}
    	
    	return tError;
    }
    
//  校验必录项
    private String checkLCAppnt(LCAppntSchema aLCAppntSchema,LCAddressSchema aLCAddressSchema){
    	String tError = "";
    	if("".equals(StrTool.cTrim(aLCAppntSchema.getPrtNo()))){
    		tError += "投保人数据印刷号不能为空。";
    	}
    	if("".equals(StrTool.cTrim(aLCAppntSchema.getAppntName()))){
    		tError += "投保人数据投保人姓名不能为空。";
    	}
    	String tSaleChnl = StrTool.cTrim(aLCAppntSchema.getAppntSex());
    	if("".equals(tSaleChnl)){
    		tError += "投保人数据投保人性别不能为空。";
    	}
    	if("".equals(StrTool.cTrim(aLCAppntSchema.getAppntBirthday())) || "1900-01-01".equals(aLCAppntSchema.getAppntBirthday())){
    		tError += "投保人数据投保人出生日期不能为空。";
    	}
    	if("".equals(StrTool.cTrim(aLCAppntSchema.getIDType()))){
    		tError += "投保人数据投保人证件类型不能为空。";
    	}
    	if("".equals(StrTool.cTrim(aLCAppntSchema.getIDNo()))){
    		tError += "投保人数据投保人证件号码不能为空。";
    	}
    	if("".equals(StrTool.cTrim(aLCAddressSchema.getPostalAddress()))){
    		tError += "投保人数据联系地址不能为空。";
    	}
    	if("".equals(StrTool.cTrim(aLCAddressSchema.getZipCode()))){
    		tError += "投保人数据邮政编码不能为空。";
    	}
    	
    	return tError;
    }
    
//  校验必录项
    private String checkLCInsrued(LCInsuredSchema aLCInsuredSchema,LCAddressSchema aLCAddressSchema){
    	String tError = "";
    	if("".equals(StrTool.cTrim(aLCInsuredSchema.getPrtNo()))){
    		tError += "被保人数据印刷号不能为空。";
    	}
    	if("".equals(StrTool.cTrim(aLCInsuredSchema.getRelationToAppnt()))){
    		tError += "被保人数据被保人与投保人关系不能为空。";
    	}
    	if("".equals(StrTool.cTrim(aLCInsuredSchema.getName()))){
    		tError += "被保人数据被保人姓名不能为空。";
    	}
    	String tSaleChnl = StrTool.cTrim(aLCInsuredSchema.getSex());
    	if("".equals(tSaleChnl)){
    		tError += "被保人数据被保人性别不能为空。";
    	}
    	if("".equals(StrTool.cTrim(aLCInsuredSchema.getBirthday())) || "1900-01-01".equals(aLCInsuredSchema.getBirthday()) ){
    		tError += "被保人数据被保人出生日期不能为空。";
    	}
    	if("".equals(StrTool.cTrim(aLCInsuredSchema.getIDType()))){
    		tError += "被保人数据被保人证件类型不能为空。";
    	}
    	if("".equals(StrTool.cTrim(aLCInsuredSchema.getIDNo()))){
    		tError += "被保人数据被保人证件号码不能为空。";
    	}
    	if("".equals(StrTool.cTrim(aLCAddressSchema.getPostalAddress()))){
    		tError += "被保人数据联系地址不能为空。";
    	}
    	if("".equals(StrTool.cTrim(aLCAddressSchema.getZipCode()))){
    		tError += "被保人数据邮政编码不能为空。";
    	}
    	
    	return tError;
    }

}