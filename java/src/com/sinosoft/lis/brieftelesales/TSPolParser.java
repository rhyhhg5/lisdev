/*
 * @(#)LCPolParser.java	2005-01-15
 *
 * Copyright 2005 Sinosoft Co. Ltd. All rights reserved.
 *  All right reserved.
 */

package com.sinosoft.lis.brieftelesales;

import org.w3c.dom.*;

import org.apache.xpath.XPathAPI;
import org.apache.xpath.objects.XObject;


import com.sinosoft.lis.db.LDOccupationDB;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;


/**
 * <p>Title: Web业务系统 </p>
 * <p>Description: 个单磁盘投保的解析类，从一个XML数据流中解析出被保险人信息，
 * 险种保单，保费项信息，责任项信息，受益人信息 </p>
 * <p>Copyright: Copyright (c) 2005 </p>
 * <p>Company: Sinosoft </p>
 * @author zhangtao
 * @version 1.0
 * @date 2005-01-15
 */
public class TSPolParser {

//	险种保单 XML Path
    private static String PATH_LCCONT = "ROW";
    private static String PATH_LCCONT_ContID = "ContID";
    private static String PATH_LCCONT_PrtNo = "PrtNo";
    private static String PATH_LCCONT_ManageCom = "ManageCom";
    private static String PATH_LCCONT_SaleChnl = "SaleChnl";
    private static String PATH_LCCONT_AgentCom = "AgentCom";
    private static String PATH_LCCONT_AgentCode = "AgentCode";
    private static String PATH_LCCONT_PolApplyDate = "PolApplyDate";
    private static String PATH_LCCONT_ReceiveDate = "ReceiveDate";
    private static String PATH_LCCONT_PayIntv = "PayIntv";
    private static String PATH_LCCONT_PayMode = "PayMode";
    private static String PATH_LCCONT_BankCode = "BankCode";
    private static String PATH_LCCONT_BankAccNo = "BankAccNo";
    private static String PATH_LCCONT_AccName = "AccName";
    
	//投保人 XML Path
    private static String PATH_APPNT = "ROW";
    private static String PATH_APPNT_AppntID = "AppntID";
    private static String PATH_APPNT_ContID = "ContID";
    private static String PATH_APPNT_PrtNo = "PrtNo";
    private static String PATH_APPNT_CustomerNo = "CustomerNo";
    private static String PATH_APPNT_Name = "Name";
    private static String PATH_APPNT_Sex = "Sex";
    private static String PATH_APPNT_Birthday = "Birthday";
    private static String PATH_APPNT_IDType = "IDType";
    private static String PATH_APPNT_IDNo = "IDNo";
    private static String PATH_APPNT_NativePlace = "NativePlace";
    private static String PATH_APPNT_RgtAddress = "RgtAddress";
    private static String PATH_APPNT_Marriage = "Marriage";
    private static String PATH_APPNT_Nationality = "Nationality";
    private static String PATH_APPNT_OccupationCode = "OccupationCode";
    private static String PATH_APPNT_OccupationType = "OccupationType";
    private static String PATH_APPNT_WorkType = "WorkType";
    private static String PATH_APPNT_PluralityType = "PluralityType";
    private static String PATH_APPNT_BankCode = "BankCode";
    private static String PATH_APPNT_BankAccNo = "BankAccNo";
    private static String PATH_APPNT_AccName = "AccName";
    private static String PATH_APPNT_SmokeFlag = "SmokeFlag";
    private static String PATH_APPNT_PostalAddress = "PostalAddress";
    private static String PATH_APPNT_ZipCode = "ZipCode";
    private static String PATH_APPNT_Phone = "Phone";
    private static String PATH_APPNT_Fax = "Fax";
    private static String PATH_APPNT_Mobile = "Mobile";
    private static String PATH_APPNT_EMail = "EMail";
    private static String PATH_APPNT_HomeAddress = "HomeAddress";
    private static String PATH_APPNT_HomeZipCode = "HomeZipCode";
    private static String PATH_APPNT_HomePhone = "HomePhone";
    private static String PATH_APPNT_HomeFax = "HomeFax";
    private static String PATH_APPNT_CompanyAddress = "CompanyAddress";
    private static String PATH_APPNT_CompanyZipCode = "CompanyZipCode";
    private static String PATH_APPNT_CompanyPhone = "CompanyPhone";
    private static String PATH_APPNT_CompanyFax = "CompanyFax";
    private static String PATH_APPNT_GrpNo = "GrpNo";
    private static String PATH_APPNT_GrpName = "GrpName";
    private static String PATH_APPNT_JoinCompanyDate = "JoinCompanyDate";
    private static String PATH_APPNT_Degree = "Degree";
    private static String PATH_APPNT_Position = "Position";
    private static String PATH_APPNT_Salary = "Salary";
    private static String PATH_APPNT_Remark = "Remark";
    private static String PATH_APPNT_PremScope = "PremScope";
    private static String PATH_APPNT_DueFeeMsgFlag = "DueFeeMsgFlag";

    //被保险人 XML Path
    private static String PATH_INSURED = "ROW";
    private static String PATH_INSURED_InsuredID = "InsuredID";
    private static String PATH_INSURED_ContID = "ContID";
    private static String PATH_INSURED_CustomerNo = "CustomerNo";
    private static String PATH_INSURED_RelationToAppnt = "RelationToAppnt";
    private static String PATH_INSURED_Name = "Name";
    private static String PATH_INSURED_Sex = "Sex";
    private static String PATH_INSURED_Birthday = "Birthday";
    private static String PATH_INSURED_IDType = "IDType";
    private static String PATH_INSURED_IDNo = "IDNo";
    private static String PATH_INSURED_NativePlace = "NativePlace";
    private static String PATH_INSURED_RgtAddress = "RgtAddress";
    private static String PATH_INSURED_Marriage = "Marriage";
    private static String PATH_INSURED_Nationality = "Nationality";
    private static String PATH_INSURED_OccupationCode = "OccupationCode";
    private static String PATH_INSURED_OccupationType = "OccupationType";
    private static String PATH_INSURED_WorkType = "WorkType";
    private static String PATH_INSURED_PluralityType = "PluralityType";
    private static String PATH_INSURED_BankCode = "BankCode";
    private static String PATH_INSURED_BankAccNo = "BankAccNo";
    private static String PATH_INSURED_AccName = "AccName";
    private static String PATH_INSURED_SmokeFlag = "SmokeFlag";
    private static String PATH_INSURED_PostalAddress = "PostalAddress";
    private static String PATH_INSURED_ZipCode = "ZipCode";
    private static String PATH_INSURED_Phone = "Phone";
    private static String PATH_INSURED_Fax = "Fax";
    private static String PATH_INSURED_Mobile = "Mobile";
    private static String PATH_INSURED_EMail = "EMail";
    private static String PATH_INSURED_HomeAddress = "HomeAddress";
    private static String PATH_INSURED_HomeZipCode = "HomeZipCode";
    private static String PATH_INSURED_HomePhone = "HomePhone";
    private static String PATH_INSURED_HomeFax = "HomeFax";
    private static String PATH_INSURED_GrpName = "GrpName";
    private static String PATH_INSURED_CompanyAddress = "CompanyAddress";
    private static String PATH_INSURED_CompanyZipCode = "CompanyZipCode";
    private static String PATH_INSURED_CompanyPhone = "CompanyPhone";
    private static String PATH_INSURED_CompanyFax = "CompanyFax";
    private static String PATH_INSURED_GrpNo = "GrpNo";
    private static String PATH_INSURED_JoinCompanyDate = "JoinCompanyDate";
    private static String PATH_INSURED_Degree = "Degree";
    private static String PATH_INSURED_Position = "Position";
    private static String PATH_INSURED_Salary = "Salary";

    //受益人 XML Path
    private static String PATH_BNF = "ROW";
    private static String PATH_BNF_ContId = "ContId";
    private static String PATH_BNF_PolID = "PolID";
    private static String PATH_BNF_InsuredId = "InsuredId";
    private static String PATH_BNF_RiskCode = "RiskCode";
    private static String PATH_BNF_BnfType = "BnfType";
    private static String PATH_BNF_Name = "Name";
    private static String PATH_BNF_Sex = "Sex";
    private static String PATH_BNF_IDType = "IDType";
    private static String PATH_BNF_IDNo = "IDNo";
    private static String PATH_BNF_Birthday = "Birthday";
    private static String PATH_BNF_RelationToInsured = "RelationToInsured";
    private static String PATH_BNF_BnfGrade = "BnfGrade";
    private static String PATH_BNF_BnfLot = "BnfLot";

    //客户告知 XML Path
    private static String PATH_WRAP = "ROW";
    private static String PATH_WRAP_WrapId = "WrapId";
    private static String PATH_WRAP_PrtNo = "PrtNo";
    private static String PATH_WRAP_WrapCode = "WrapCode";
    private static String PATH_WRAP_WrapName = "WrapName";
    private static String PATH_WRAP_InsuYear = "InsuYear";
    private static String PATH_WRAP_InsuYearFlag = "InsuYearFlag";
    private static String PATH_WRAP_PayEndYear = "PayEndYear";
    private static String PATH_WRAP_PayEndYearFlag = "PayEndYearFlag";
    private static String PATH_WRAP_Amnt = "Amnt";
    private static String PATH_WRAP_Prem = "Prem";
    private static String PATH_WRAP_Mult = "Mult";
    private static String PATH_WRAP_Copys = "Copys";
    
    public CErrors mErrors = new CErrors();
    
    /**
     * 构造函数
     */
    public TSPolParser() {

    }

    /**
     * 利用XPathAPI取得某个节点的节点值
     *
     * @param node Node
     * @param strPath String
     * @return java.lang.String
     */
    private static String parseNode(Node node,
                                    String strPath) {
        String strValue = "";

        try {
            XObject xobj = XPathAPI.eval(node, strPath);
            strValue = xobj == null ? "" : xobj.toString();
            if(strValue.startsWith("\n ") || strValue.startsWith("\n\r "))
            {
                strValue = "";
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return strValue;
    }



    /**
     * 从XML中解析保单信息
     *
     * @param node Node
     * @return com.sinosoft.lis.vschema.LCBnfSet
     */
    public LCContSet getLCContSet(Node node) {
    	NodeList nodeList = null;
    	try {
            nodeList = XPathAPI.selectNodeList(node, PATH_LCCONT);
        } catch (Exception ex) {
            ex.printStackTrace();
            nodeList = null;
            return null;
        }
        int nLength = nodeList == null ? 0 : nodeList.getLength();
        LCContSet tLCContSet = new LCContSet();
        for (int nIndex = 0; nIndex < nLength; nIndex++) {
        	Node nodeLCCont = nodeList.item(nIndex);
        	LCContSchema tLCContSchema = new LCContSchema();
//    		Node lccontNode = lccontNodes.item(1);
    		tLCContSchema.setPrtNo(parseNode(nodeLCCont,PATH_LCCONT_PrtNo));
    		tLCContSchema.setManageCom(parseNode(nodeLCCont,PATH_LCCONT_ManageCom));
    		tLCContSchema.setSaleChnl(parseNode(nodeLCCont,PATH_LCCONT_SaleChnl));
    		tLCContSchema.setAgentCom(parseNode(nodeLCCont,PATH_LCCONT_AgentCom));
    		tLCContSchema.setAgentCode(parseNode(nodeLCCont,PATH_LCCONT_AgentCode));
    		tLCContSchema.setPolApplyDate(parseNode(nodeLCCont,PATH_LCCONT_PolApplyDate));
    		tLCContSchema.setReceiveDate(parseNode(nodeLCCont,PATH_LCCONT_ReceiveDate));
    		tLCContSchema.setPayIntv(parseNode(nodeLCCont,PATH_LCCONT_PayIntv));
    		tLCContSchema.setPayMode(parseNode(nodeLCCont,PATH_LCCONT_PayMode));
    		tLCContSchema.setBankCode(parseNode(nodeLCCont,PATH_LCCONT_BankCode));
    		tLCContSchema.setBankAccNo(parseNode(nodeLCCont,PATH_LCCONT_BankAccNo));
    		tLCContSchema.setAccName(parseNode(nodeLCCont,PATH_LCCONT_AccName));
    		tLCContSet.add(tLCContSchema);
        }
        
        return tLCContSet;
    }

    /**
     * 从XML中解析投保人信息
     *
     * @param node Node
     * @return com.sinosoft.utility.VData
     */
    public VData getLCAppntData(Node node) {
        NodeList nodeList = null;

        try {
            nodeList = XPathAPI.selectNodeList(node, PATH_APPNT);
        } catch (Exception ex) {
            ex.printStackTrace();
            nodeList = null;
            return null;
        }

        int nLength = nodeList == null ? 0 : nodeList.getLength();

        VData tAppntVData = new VData();
        LCAppntSet tLCAppntSet = new LCAppntSet();
        LCAddressSet tLCAppntAddressSet = new LCAddressSet();

        for (int nIndex = 0; nIndex < nLength; nIndex++) {

        	Node nodeInsured = nodeList.item(nIndex);
            LCAppntSchema tLCAppntSchema = new LCAppntSchema();
            LCAddressSchema tLCAppntAddressSchema = new LCAddressSchema();
            //借用prtNo保存投保人序号
            tLCAppntSchema.setPrtNo(parseNode(nodeInsured,
            		PATH_APPNT_PrtNo));
            tLCAppntSchema.setContNo(parseNode(nodeInsured,
                                               PATH_APPNT_ContID));
            tLCAppntSchema.setAppntNo(parseNode(nodeInsured,
                                                PATH_APPNT_CustomerNo));
            tLCAppntSchema.setAppntName(parseNode(nodeInsured,
                                                  PATH_APPNT_Name));
            tLCAppntSchema.setAppntSex(parseNode(nodeInsured,
                                                 PATH_APPNT_Sex));
            tLCAppntSchema.setAppntBirthday(parseNode(nodeInsured,
                    PATH_APPNT_Birthday));
            tLCAppntSchema.setIDType(parseNode(nodeInsured,
                                               PATH_APPNT_IDType));
            tLCAppntSchema.setIDNo(parseNode(nodeInsured,
                                             PATH_APPNT_IDNo));
            tLCAppntSchema.setNativePlace(parseNode(nodeInsured,
                    PATH_APPNT_NativePlace));
            tLCAppntSchema.setRgtAddress(parseNode(nodeInsured,
                    PATH_APPNT_RgtAddress));
            tLCAppntSchema.setMarriage(parseNode(nodeInsured,
                                                 PATH_APPNT_Marriage));
            tLCAppntSchema.setNationality(parseNode(nodeInsured,
                    PATH_APPNT_Nationality));
            tLCAppntSchema.setOccupationCode(parseNode(nodeInsured,
                    PATH_APPNT_OccupationCode));
            /**
             * picc磁盘投保文件中没有职业类别因此根据职业代码查询职业类别
             */
            LDOccupationDB tLDOccupationDB = new LDOccupationDB();
            tLDOccupationDB.setOccupationCode(tLCAppntSchema.getOccupationCode());
            tLDOccupationDB.getInfo();

            tLCAppntSchema.setOccupationType(tLDOccupationDB.getOccupationType());

            tLCAppntSchema.setWorkType(parseNode(nodeInsured,
                                                 PATH_APPNT_WorkType));
            tLCAppntSchema.setPluralityType(parseNode(nodeInsured,
                    PATH_APPNT_PluralityType));
            tLCAppntSchema.setBankCode(parseNode(nodeInsured,
                                                 PATH_APPNT_BankCode));
            tLCAppntSchema.setBankAccNo(parseNode(nodeInsured,
                                                  PATH_APPNT_BankAccNo));
            tLCAppntSchema.setAccName(parseNode(nodeInsured,
                                                PATH_APPNT_AccName));
            tLCAppntSchema.setSmokeFlag(parseNode(nodeInsured,
                                                  PATH_APPNT_SmokeFlag));
            //用CustomerNo保存 投保人ID 做索引
            tLCAppntAddressSchema.setCustomerNo(parseNode(nodeInsured,
                    PATH_APPNT_AppntID));

            tLCAppntAddressSchema.setPostalAddress(parseNode(nodeInsured,
                    PATH_APPNT_PostalAddress));
            tLCAppntAddressSchema.setZipCode(parseNode(nodeInsured,
                    PATH_APPNT_ZipCode));
            tLCAppntAddressSchema.setPhone(parseNode(nodeInsured,
                    PATH_APPNT_Phone));
            tLCAppntAddressSchema.setFax(parseNode(nodeInsured,
                    PATH_APPNT_Fax));
            tLCAppntAddressSchema.setMobile(parseNode(nodeInsured,
                    PATH_APPNT_Mobile));
            tLCAppntAddressSchema.setEMail(parseNode(nodeInsured,
                    PATH_APPNT_EMail));

            tLCAppntAddressSchema.setHomeAddress(parseNode(nodeInsured,
                    PATH_APPNT_HomeAddress));
            tLCAppntAddressSchema.setHomeZipCode(parseNode(nodeInsured,
                    PATH_APPNT_HomeZipCode));
            tLCAppntAddressSchema.setHomePhone(parseNode(nodeInsured,
                    PATH_APPNT_HomePhone));
            tLCAppntAddressSchema.setHomeFax(parseNode(nodeInsured,
                    PATH_APPNT_HomeFax));
            tLCAppntAddressSchema.setCompanyAddress(parseNode(nodeInsured,
                    PATH_APPNT_CompanyAddress));
            tLCAppntAddressSchema.setCompanyPhone(parseNode(nodeInsured,
                    PATH_APPNT_CompanyPhone));
            tLCAppntAddressSchema.setCompanyZipCode(parseNode(nodeInsured,
                    PATH_APPNT_CompanyZipCode));
            tLCAppntAddressSchema.setCompanyFax(parseNode(nodeInsured,
                    PATH_APPNT_CompanyFax));
            tLCAppntAddressSchema.setGrpName(parseNode(nodeInsured,
                    PATH_APPNT_GrpName));
//单位编码
//            tLCAppntSchema.setGrpNo(parseNode(nodeInsured,
//                                                 PATH_APPNT_GrpNo));
            tLCAppntSchema.setJoinCompanyDate(parseNode(nodeInsured,
                    PATH_APPNT_JoinCompanyDate));
            tLCAppntSchema.setDegree(parseNode(nodeInsured,
                                               PATH_APPNT_Degree));
            tLCAppntSchema.setPosition(parseNode(nodeInsured,
                                                 PATH_APPNT_Position));
            if(StrTool.cTrim(parseNode(nodeInsured,PATH_APPNT_Salary)).equals("")){
                tLCAppntSchema.setSalary(-1);
            }else{
            tLCAppntSchema.setSalary(parseNode(nodeInsured,PATH_APPNT_Salary));
            }
            /**
             *PICC中磁盘投保没有职业工种，因此借用职位这个字段暂存特约信息
             */
            tLCAppntSchema.setWorkType(parseNode(nodeInsured,
                                                 this.PATH_APPNT_Remark));
            tLCAppntSchema.setBMI(parseNode(nodeInsured,
                                                 this.PATH_APPNT_PremScope));
            
//            获取DueFeeMsgFlag
            //中英,通过身份证算生日性别
            if ("0".equals(StrTool.cTrim(tLCAppntSchema.getIDType()))
                && "".equals(StrTool.cTrim(tLCAppntSchema.getAppntBirthday()))
                && "".equals(StrTool.cTrim(tLCAppntSchema.getAppntSex()))) {
                tLCAppntSchema.setAppntBirthday(
                        PubFun.getBirthdayFromId(
                                tLCAppntSchema.getIDNo()));
                tLCAppntSchema.setAppntSex(
                        PubFun.getSexFromId(
                                tLCAppntSchema.getIDNo()));
            }

            if (tLCAppntSchema.getAppntBirthday() == null) {
                FDate fdate = new FDate();
                tLCAppntSchema.setAppntBirthday(
                        fdate.getDate("1900-01-01"));
            }
            tLCAppntSet.add(tLCAppntSchema);
            tLCAppntAddressSet.add(tLCAppntAddressSchema);
        }
        tAppntVData.add(tLCAppntSet);
        tAppntVData.add(tLCAppntAddressSet);

        return tAppntVData;
    }


    /**
     * 从XML中解析被保人信息
     *
     * @param node Node
     * @return com.sinosoft.utility.VData
     */
    public VData getLCInsuredData(Node node) {
        NodeList nodeList = null;
        try {
            nodeList = XPathAPI.selectNodeList(node, PATH_INSURED);
        } catch (Exception ex) {
            ex.printStackTrace();
            nodeList = null;
            return null;
        }

        int nLength = nodeList == null ? 0 : nodeList.getLength();

        VData tInsuredVData = new VData();
        LCInsuredSet tLCInsuredSet = new LCInsuredSet();
        LCAddressSet tLCInsuredAddressSet = new LCAddressSet();

        for (int nIndex = 0; nIndex < nLength; nIndex++) {
            Node nodeInsured = nodeList.item(nIndex);

            LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
            LCAddressSchema tLCInsuredAddressSchema = new LCAddressSchema();
            //借用prtNo保存被保人序号
            tLCInsuredSchema.setPrtNo(parseNode(nodeInsured,
                                                PATH_INSURED_InsuredID));
            tLCInsuredSchema.setContNo(parseNode(nodeInsured,
                                                 PATH_INSURED_ContID));
            tLCInsuredSchema.setInsuredNo(parseNode(nodeInsured,
                    PATH_INSURED_CustomerNo));
            tLCInsuredSchema.setName(parseNode(nodeInsured,
                                               PATH_INSURED_Name));
            tLCInsuredSchema.setSex(parseNode(nodeInsured,
                                              PATH_INSURED_Sex));
            tLCInsuredSchema.setBirthday(parseNode(nodeInsured,
                    PATH_INSURED_Birthday));
            tLCInsuredSchema.setIDType(parseNode(nodeInsured,
                                                 PATH_INSURED_IDType));
            tLCInsuredSchema.setIDNo(parseNode(nodeInsured,
                                               PATH_INSURED_IDNo));
            tLCInsuredSchema.setNativePlace(parseNode(nodeInsured,
                    PATH_INSURED_NativePlace));
            tLCInsuredSchema.setRgtAddress(parseNode(nodeInsured,
                    PATH_INSURED_RgtAddress));
            tLCInsuredSchema.setMarriage(parseNode(nodeInsured,
                    PATH_INSURED_Marriage));
            tLCInsuredSchema.setNationality(parseNode(nodeInsured,
                    PATH_INSURED_Nationality));
            tLCInsuredSchema.setOccupationCode(parseNode(nodeInsured,
                    PATH_INSURED_OccupationCode));

            tLCInsuredSchema.setOccupationType(parseNode(nodeInsured,
                    PATH_INSURED_OccupationType));

            tLCInsuredSchema.setWorkType(parseNode(nodeInsured,
                    PATH_INSURED_WorkType));
            tLCInsuredSchema.setPluralityType(parseNode(nodeInsured,
                    PATH_INSURED_PluralityType));
            tLCInsuredSchema.setBankCode(parseNode(nodeInsured,
                    PATH_INSURED_BankCode));
            tLCInsuredSchema.setBankAccNo(parseNode(nodeInsured,
                    PATH_INSURED_BankAccNo));
            tLCInsuredSchema.setAccName(parseNode(nodeInsured,
                                                  PATH_INSURED_AccName));
            tLCInsuredSchema.setSmokeFlag(parseNode(nodeInsured,
                    PATH_INSURED_SmokeFlag));
            tLCInsuredSchema.setRelationToAppnt(parseNode(nodeInsured,
            		PATH_INSURED_RelationToAppnt));
            //用CustomerNo保存被保险人ID做索引
            tLCInsuredAddressSchema.setCustomerNo(parseNode(nodeInsured,
                    PATH_INSURED_InsuredID));
            tLCInsuredAddressSchema.setPostalAddress(parseNode(nodeInsured,
                    PATH_INSURED_PostalAddress));
            tLCInsuredAddressSchema.setPhone(parseNode(nodeInsured,
                    PATH_INSURED_Phone));
            tLCInsuredAddressSchema.setZipCode(parseNode(nodeInsured,
                    PATH_INSURED_ZipCode));
            tLCInsuredAddressSchema.setFax(parseNode(nodeInsured,
                    PATH_INSURED_Fax));
            tLCInsuredAddressSchema.setMobile(parseNode(nodeInsured,
                    PATH_INSURED_Mobile));
            tLCInsuredAddressSchema.setEMail(parseNode(nodeInsured,
                    PATH_INSURED_EMail));
            tLCInsuredAddressSchema.setHomeAddress(parseNode(nodeInsured,
                    PATH_INSURED_HomeAddress));
            tLCInsuredAddressSchema.setHomeZipCode(parseNode(nodeInsured,
                    PATH_INSURED_HomeZipCode));
            tLCInsuredAddressSchema.setHomePhone(parseNode(nodeInsured,
                    PATH_INSURED_HomePhone));
            tLCInsuredAddressSchema.setHomeFax(parseNode(nodeInsured,
                    PATH_INSURED_HomeFax));
            tLCInsuredAddressSchema.setCompanyAddress(parseNode(nodeInsured,
                    PATH_INSURED_CompanyAddress));
            tLCInsuredAddressSchema.setCompanyPhone(parseNode(nodeInsured,
                    PATH_INSURED_CompanyPhone));
            tLCInsuredAddressSchema.setCompanyZipCode(parseNode(nodeInsured,
                    PATH_INSURED_CompanyZipCode));
            tLCInsuredAddressSchema.setCompanyFax(parseNode(nodeInsured,
                    PATH_INSURED_CompanyFax));
            tLCInsuredAddressSchema.setGrpName(parseNode(nodeInsured,
                    PATH_INSURED_GrpName));
            //单位编码
//            tLCInsuredAddressSchema.setGrpNo(parseNode(nodeInsured,
//                                                 PATH_INSURED_GrpNo));
            tLCInsuredSchema.setJoinCompanyDate(parseNode(nodeInsured,
                    PATH_INSURED_JoinCompanyDate));
            tLCInsuredSchema.setDegree(parseNode(nodeInsured,
                                                 PATH_INSURED_Degree));
            tLCInsuredSchema.setPosition(parseNode(nodeInsured,
                    PATH_INSURED_Position));
         if(StrTool.cTrim(parseNode(nodeInsured,PATH_INSURED_Salary)).equals("")){
             tLCInsuredSchema.setSalary(-1);
             }else{
             tLCInsuredSchema.setSalary(parseNode(nodeInsured,PATH_INSURED_Salary));
             }
            //与主被保险人的关系，暂时取默认
            tLCInsuredSchema.setRelationToMainInsured("00");

            //中英,通过身份证算生日性别
            if ("0".equals(StrTool.cTrim(tLCInsuredSchema.getIDType()))
                && "".equals(StrTool.cTrim(tLCInsuredSchema.getBirthday()))
                && "".equals(StrTool.cTrim(tLCInsuredSchema.getSex()))) {
                tLCInsuredSchema.setBirthday(
                        PubFun.getBirthdayFromId(
                                tLCInsuredSchema.getIDNo()));
                tLCInsuredSchema.setSex(
                        PubFun.getSexFromId(
                                tLCInsuredSchema.getIDNo()));
            }

            if (tLCInsuredSchema.getBirthday() == null) {
                FDate fdate = new FDate();
                tLCInsuredSchema.setBirthday(
                        fdate.getDate("1900-01-01"));
            }

            tLCInsuredSet.add(tLCInsuredSchema);
            tLCInsuredAddressSet.add(tLCInsuredAddressSchema);
        }
        tInsuredVData.add(tLCInsuredSet);
        tInsuredVData.add(tLCInsuredAddressSet);
        return tInsuredVData;
    }

    /**
     * 从XML中解析受益人的信息
     *
     * @param node Node
     * @return com.sinosoft.lis.vschema.LCBnfSet
     */
    public LCBnfSet getLCBnfSet(Node node) {
        NodeList nodeList = null;
        try {
            nodeList = XPathAPI.selectNodeList(node, PATH_BNF);
        } catch (Exception ex) {
            ex.printStackTrace();
            nodeList = null;
        }

        int nLength = nodeList == null ? 0 : nodeList.getLength();
        LCBnfSet tLCBnfSet = new LCBnfSet();

        for (int nIndex = 0; nIndex < nLength; nIndex++) {
            Node nodeBnf = nodeList.item(nIndex);
            LCBnfSchema tLCBnfSchema = new LCBnfSchema();

            tLCBnfSchema.setContNo(parseNode(nodeBnf,
                                             PATH_BNF_ContId));
            if(tLCBnfSchema.getContNo() == null
                || tLCBnfSchema.getContNo().equals(""))
            {
                tLCBnfSchema.setContNo(parseNode(nodeBnf, "ContID"));  //在做外包录入节点
            }

            tLCBnfSchema.setPolNo(parseNode(nodeBnf,
                                            PATH_BNF_PolID));
            tLCBnfSchema.setInsuredNo(parseNode(nodeBnf,
                                                PATH_BNF_InsuredId));
            if(tLCBnfSchema.getInsuredNo() == null
                || tLCBnfSchema.getInsuredNo().equals(""))
            {
                tLCBnfSchema.setInsuredNo(parseNode(nodeBnf, "InsuredID"));
            }

            //借用 BnfNo 缓存 险种代码
            if(!StrTool.cTrim(parseNode(nodeBnf,
                      PATH_BNF_RiskCode)).equals("")){
              tLCBnfSchema.setBnfNo(Integer.parseInt(parseNode(nodeBnf,
                      PATH_BNF_RiskCode)));
          }else{
             tLCBnfSchema.setBnfNo(1);
          }
          System.out.println("BnfNo"+tLCBnfSchema.getBnfNo());

            tLCBnfSchema.setBnfType(parseNode(nodeBnf,
                                              PATH_BNF_BnfType));
            tLCBnfSchema.setName(parseNode(nodeBnf,
                                           PATH_BNF_Name));
            tLCBnfSchema.setSex(parseNode(nodeBnf,
                                          PATH_BNF_Sex));
            tLCBnfSchema.setIDType(parseNode(nodeBnf,
                                             PATH_BNF_IDType));
            tLCBnfSchema.setIDNo(parseNode(nodeBnf,
                                           PATH_BNF_IDNo));
            tLCBnfSchema.setBirthday(parseNode(nodeBnf,
                                               PATH_BNF_Birthday));
            tLCBnfSchema.setRelationToInsured(parseNode(nodeBnf,
                    PATH_BNF_RelationToInsured));
            tLCBnfSchema.setBnfGrade(parseNode(nodeBnf,
                                               PATH_BNF_BnfGrade));
            tLCBnfSchema.setBnfLot(parseNode(nodeBnf,
                                             PATH_BNF_BnfLot));

            tLCBnfSet.add(tLCBnfSchema);
        }

        return tLCBnfSet;
    }

//======ADD===2005-03-22======ZHANGTAO============BGN========================
    /**
     * 从XML中解析客户告知信息
     *
     * @param node Node
     * @return com.sinosoft.lis.vschema.LCCustomerImpartDetailSet
     */
    /**
     * 从XML中解析产品信息
     *
     * @param node Node
     * @return com.sinosoft.lis.vschema.LCBnfSet
     */
    public LCRiskDutyWrapSet getLCRiskDutyWrapSet(Node node) {
    	NodeList nodeList = null;
    	try {
            nodeList = XPathAPI.selectNodeList(node, PATH_WRAP);
        } catch (Exception ex) {
            ex.printStackTrace();
            nodeList = null;
            return null;
        }
        int nLength = nodeList == null ? 0 : nodeList.getLength();
        LCRiskDutyWrapSet tLCRiskDutyWrapSet = new LCRiskDutyWrapSet();
        for (int nIndex = 0; nIndex < nLength; nIndex++) {
        	Node nodeWRAP = nodeList.item(nIndex);
        	if(parseNode(nodeWRAP,PATH_WRAP_InsuYear) != null && !"".equals(parseNode(nodeWRAP,PATH_WRAP_InsuYear))){
        		LCRiskDutyWrapSchema tLCRiskDutyWrapSchema = new LCRiskDutyWrapSchema();
        		tLCRiskDutyWrapSchema.setRiskWrapCode(parseNode(nodeWRAP,PATH_WRAP_WrapCode));
  	          	tLCRiskDutyWrapSchema.setCalFactor(PATH_WRAP_InsuYear);
  	          	tLCRiskDutyWrapSchema.setCalFactorValue(parseNode(nodeWRAP,PATH_WRAP_InsuYear));
  	          	tLCRiskDutyWrapSchema.setRiskCode("000000");
  	          	tLCRiskDutyWrapSchema.setDutyCode("000000");
        		tLCRiskDutyWrapSet.add(tLCRiskDutyWrapSchema);
        		
        		tLCRiskDutyWrapSchema = new LCRiskDutyWrapSchema();
        		tLCRiskDutyWrapSchema.setRiskWrapCode(parseNode(nodeWRAP,PATH_WRAP_WrapCode));
  	          	tLCRiskDutyWrapSchema.setCalFactor(PATH_WRAP_InsuYearFlag);
  	          	tLCRiskDutyWrapSchema.setCalFactorValue(parseNode(nodeWRAP,PATH_WRAP_InsuYearFlag));
  	          	tLCRiskDutyWrapSchema.setRiskCode("000000");
  	          	tLCRiskDutyWrapSchema.setDutyCode("000000");
        		tLCRiskDutyWrapSet.add(tLCRiskDutyWrapSchema);
        		
        	}
        	if(parseNode(nodeWRAP,PATH_WRAP_PayEndYear) != null && !"".equals(parseNode(nodeWRAP,PATH_WRAP_PayEndYear))){
        		LCRiskDutyWrapSchema tLCRiskDutyWrapSchema = new LCRiskDutyWrapSchema();
        		tLCRiskDutyWrapSchema.setRiskWrapCode(parseNode(nodeWRAP,PATH_WRAP_WrapCode));
  	          	tLCRiskDutyWrapSchema.setCalFactor(PATH_WRAP_PayEndYear);
  	          	tLCRiskDutyWrapSchema.setCalFactorValue(parseNode(nodeWRAP,PATH_WRAP_PayEndYear));
  	          	tLCRiskDutyWrapSchema.setRiskCode("000000");
  	          	tLCRiskDutyWrapSchema.setDutyCode("000000");
        		tLCRiskDutyWrapSet.add(tLCRiskDutyWrapSchema);
        		
        		tLCRiskDutyWrapSchema = new LCRiskDutyWrapSchema();
        		tLCRiskDutyWrapSchema.setRiskWrapCode(parseNode(nodeWRAP,PATH_WRAP_WrapCode));
  	          	tLCRiskDutyWrapSchema.setCalFactor(PATH_WRAP_PayEndYearFlag);
  	          	tLCRiskDutyWrapSchema.setCalFactorValue(parseNode(nodeWRAP,PATH_WRAP_PayEndYearFlag));
  	          	tLCRiskDutyWrapSchema.setRiskCode("000000");
  	          	tLCRiskDutyWrapSchema.setDutyCode("000000");
        		tLCRiskDutyWrapSet.add(tLCRiskDutyWrapSchema);
        		
        	}
        	if(parseNode(nodeWRAP,PATH_WRAP_Amnt) != null && !"".equals(parseNode(nodeWRAP,PATH_WRAP_Amnt))){
        		LCRiskDutyWrapSchema tLCRiskDutyWrapSchema = new LCRiskDutyWrapSchema();
        		tLCRiskDutyWrapSchema.setRiskWrapCode(parseNode(nodeWRAP,PATH_WRAP_WrapCode));
  	          	tLCRiskDutyWrapSchema.setCalFactor(PATH_WRAP_Amnt);
  	          	tLCRiskDutyWrapSchema.setCalFactorValue(parseNode(nodeWRAP,PATH_WRAP_Amnt));
  	          	tLCRiskDutyWrapSchema.setRiskCode("000000");
  	          	tLCRiskDutyWrapSchema.setDutyCode("000000");
        		tLCRiskDutyWrapSet.add(tLCRiskDutyWrapSchema);
        		
        	}
        	if(parseNode(nodeWRAP,PATH_WRAP_Prem) != null && !"".equals(parseNode(nodeWRAP,PATH_WRAP_Prem))){
        		LCRiskDutyWrapSchema tLCRiskDutyWrapSchema = new LCRiskDutyWrapSchema();
        		tLCRiskDutyWrapSchema.setRiskWrapCode(parseNode(nodeWRAP,PATH_WRAP_WrapCode));
  	          	tLCRiskDutyWrapSchema.setCalFactor(PATH_WRAP_Prem);
  	          	tLCRiskDutyWrapSchema.setCalFactorValue(parseNode(nodeWRAP,PATH_WRAP_Prem));
  	          	tLCRiskDutyWrapSchema.setRiskCode("000000");
  	          	tLCRiskDutyWrapSchema.setDutyCode("000000");
        		tLCRiskDutyWrapSet.add(tLCRiskDutyWrapSchema);
        	}
        	if(parseNode(nodeWRAP,PATH_WRAP_Mult) != null && !"".equals(parseNode(nodeWRAP,PATH_WRAP_Mult))){
        		LCRiskDutyWrapSchema tLCRiskDutyWrapSchema = new LCRiskDutyWrapSchema();
        		tLCRiskDutyWrapSchema.setRiskWrapCode(parseNode(nodeWRAP,PATH_WRAP_WrapCode));
  	          	tLCRiskDutyWrapSchema.setCalFactor(PATH_WRAP_Mult);
  	          	tLCRiskDutyWrapSchema.setCalFactorValue(parseNode(nodeWRAP,PATH_WRAP_Mult));
  	          	tLCRiskDutyWrapSchema.setRiskCode("000000");
  	          	tLCRiskDutyWrapSchema.setDutyCode("000000");
        		tLCRiskDutyWrapSet.add(tLCRiskDutyWrapSchema);
        		
        	}
        	if(parseNode(nodeWRAP,PATH_WRAP_Copys) != null && !"".equals(parseNode(nodeWRAP,PATH_WRAP_Copys))){
        		LCRiskDutyWrapSchema tLCRiskDutyWrapSchema = new LCRiskDutyWrapSchema();
        		tLCRiskDutyWrapSchema.setRiskWrapCode(parseNode(nodeWRAP,PATH_WRAP_WrapCode));
  	          	tLCRiskDutyWrapSchema.setCalFactor(PATH_WRAP_Copys);
  	          	tLCRiskDutyWrapSchema.setCalFactorValue(parseNode(nodeWRAP,PATH_WRAP_Copys));
  	          	tLCRiskDutyWrapSchema.setRiskCode("000000");
  	          	tLCRiskDutyWrapSchema.setDutyCode("000000");
        		tLCRiskDutyWrapSet.add(tLCRiskDutyWrapSchema);
        	}
        }
        
        return tLCRiskDutyWrapSet;
    }

//======ADD===2005-03-22======ZHANGTAO============END========================
    /**
     * 创建错误信息
     * @param szFunc String 函数名称
     * @param szErrMsg String 错误信息
     */
    private void buildError(String szFunc,
                            String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "LCPolParser";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}

