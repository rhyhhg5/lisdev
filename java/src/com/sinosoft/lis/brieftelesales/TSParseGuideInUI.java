package com.sinosoft.lis.brieftelesales;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Description:个单磁盘投保界面调用
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author zhangtao
 * @version 1.0
 */
public class TSParseGuideInUI
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  public TSParseGuideInUI() {}

  /**
  传输数据的公共方法
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    TSParseGuideIn tLCParseGuideIn = new TSParseGuideIn();
    System.out.println("--- TSParseGuideInUI BEGIN ---"+mOperate);
    if (tLCParseGuideIn.submitData(cInputData,cOperate) == false)
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tLCParseGuideIn.mErrors);
      return false;
    }
    else
      mResult = tLCParseGuideIn.getResult();
    return true;
  }

  public VData getResult()
  {
    return mResult;
  }

}
