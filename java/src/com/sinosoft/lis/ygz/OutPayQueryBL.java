package com.sinosoft.lis.ygz;

import java.util.ArrayList;
import java.util.List;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LYOutPayDetailSchema;
import com.sinosoft.lis.schema.LYOutPayQueryDetailSchema;
import com.sinosoft.lis.vschema.LYOutPayDetailSet;
import com.sinosoft.lis.vschema.LYOutPayQueryDetailSet;
import com.sinosoft.lis.ygz.obj.OutPayQueryBillInfo;
import com.sinosoft.lis.ygz.obj.OutPayQuerySendResult;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

/**
 * <p>Title: 查询接口处理类</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company: Sinosoft</p>
 * @author Alex
 * @version 1.0
 */
public class OutPayQueryBL {

  private GlobalInput tGI= new GlobalInput();
	  
  private LYOutPayDetailSet tLYOutPayDetailSet = null;
	  
  public CErrors mErrors = new CErrors();
	  
  private String operator ;
	  
  private int sysCount = 1000;//默认每一千条数据发送一次请求
	  
  private MMap resultMMap = new MMap();
	  
  private String zBatchNo = "" ;
  
  VData mVData=new VData();
  
  public OutPayQueryBL(){
	  
  }

  public static void main(String[] args) {
//	  OutPayQueryBL tt=new OutPayQueryBL();
//	  LYOutPayDetailDB tLYOutPayDetailDB=new LYOutPayDetailDB();
//	  LYOutPayDetailSet set=tLYOutPayDetailDB.executeQuery("select * from LYOutPayDetail where busino in ('1500000600680131122001','201605200002')");//1500000600680131520601
//	  VData data=new VData();
//	  data.add(set);
//	  GlobalInput mgl=new GlobalInput();
//	  mgl.Operator="ser";
//	  data.add(mgl);
//	  tt.getSubmit(data, "");
//	  MMap map=tt.getResult();
//	  VData tVData=new VData();
//	  tVData.add(map);
//	  
//	  PubSubmit tPubSubmit=new PubSubmit();
//	  tPubSubmit.submitData(tVData, "");
	  
  }
  
  /**
   * 接收LYOutPayDetail数据进行价税分离
   * 数据量最好不要超过1000条
   * @param tVData
   * @param operator 可为空""
   * @return
   */
  public boolean getSubmit(VData tVData,String operator){
	  
	  if(!getInputData(tVData)){
		  return false;
	  }
	  
	  if(!dealData()){
		  return false;
	  }
	  
	  // 装配处理好的数据，准备给后台进行保存
      prepareOutputData();

	  PubSubmit tPubSubmit = new PubSubmit();
      if (!tPubSubmit.submitData(mVData,"")) {
          // @@错误处理
          this.mErrors.copyAllErrors(tPubSubmit.mErrors);
          CError tError = new CError();
          tError.moduleName = "OutPayQueryBL";
          tError.functionName = "submitData";
          tError.errorMessage = "数据提交失败!";

          this.mErrors.addOneError(tError);
          return false;
      }
	  return true;
  }
  
  /**
   * 获取数据
   * @param mInputData
   * @return
   */
  private boolean getInputData(VData mInputData) {
      tGI = ((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));

      tLYOutPayDetailSet = ((LYOutPayDetailSet) mInputData.getObjectByObjectName("LYOutPayDetailSet",0));
      
      if(null == tLYOutPayDetailSet || tLYOutPayDetailSet.size()==0){
      	  CError tError = new CError();
          tError.moduleName = "OutPayUploadBL";
          tError.functionName = "getInputData";
          tError.errorMessage = "没有接收到保单信息!";
          this.mErrors.addOneError(tError);
      }

      if (tGI == null) {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "OutPayUploadBL";
          tError.functionName = "getInputData";
          tError.errorMessage = "没有得到足够的数据，请您确认!";
          this.mErrors.addOneError(tError);

          return false;
      }
      operator = tGI.Operator;

      return true;
  }
  
  /**
   * 封装数据调用查询分离接口
   * @return
   */
  private boolean dealData(){
	  LYOutPayQueryDetailSet  resLYOutPayQueryDetailSet=new LYOutPayQueryDetailSet();
	  LYOutPayDetailSet resLYOutPayDetailSet=new LYOutPayDetailSet();
	  LYOutPayDetailSet mLYOutPayDetailSet =new LYOutPayDetailSet();
	  List<OutPayQueryBillInfo> billInfoList= new  ArrayList<OutPayQueryBillInfo>();
	  int count=0;
	  
	  for(int i=1; i<=tLYOutPayDetailSet.size(); i++){
		  LYOutPayDetailSchema tdetailSchema=tLYOutPayDetailSet.get(i);
		  mLYOutPayDetailSet.add(tdetailSchema);
		  count++;
	   if(count == sysCount || i == tLYOutPayDetailSet.size()){
		  count=0;
	   try{
		  for(int j=1;j<=mLYOutPayDetailSet.size();j++){
		  LYOutPayDetailSchema detailSchema= mLYOutPayDetailSet.get(j);
		  OutPayQueryBillInfo bill=new OutPayQueryBillInfo();
		  bill.setId(detailSchema.getBusiNo());//业务流水号
		  bill.setTranserial(detailSchema.gettranserial());
		  billInfoList.add(bill);
		  }	 
	  
	  //调用接口进行查询分离
	  OutPayQueryInterface tOutPayQueryInterface=new OutPayQueryInterface();
	  List<OutPayQuerySendResult> resultList=tOutPayQueryInterface.callInterface(billInfoList);
	  billInfoList.clear();
	 
	  //对分离结果进行信息封装LYOutPayDetail数据
	  for( int n=0; n<resultList.size(); n++){
		  OutPayQuerySendResult tResult= resultList.get(n);
		  
		  String tPrintde=tResult.getPrintdate();//开票日期
		  if(!"".equals(tPrintde) && null !=tPrintde){
			  String tId  = tResult.getId();
			  String tIsprint=tResult.getIsprint();
			  String tBillnum=tResult.getBillnum();
			  String tVstatus=tResult.getVstatus();
			  if("Y".equals(tIsprint)){
				  tIsprint="02";
			  }else{
				  tIsprint="01";
			  }
			  for(int s=1; s<=mLYOutPayDetailSet.size(); s++){
				  LYOutPayDetailSchema xdetailSchema=mLYOutPayDetailSet.get(s);
				  if(tId.equals(xdetailSchema.getBusiNo())){
					  xdetailSchema.setbillprintdate(tPrintde);
					  xdetailSchema.setbillprintno(tBillnum);
					  xdetailSchema.setprintstate(tIsprint);
					  xdetailSchema.setvstatus(tVstatus);
					  String xTrandate=xdetailSchema.gettrandate();//交易日期
					  String xDate=PubFun.getLaterDate(xTrandate, tPrintde);
					  if(xDate.equals(tPrintde)){
							  xdetailSchema.setcvalistate("01");
					  }else{
							  xdetailSchema.setcvalistate("02");
					  }
			
					  xdetailSchema.setModifyDate(PubFun.getCurrentDate());
					  xdetailSchema.setModifyTime(PubFun.getCurrentTime());
					  resLYOutPayDetailSet.add(xdetailSchema);
					  //删除已获取结果的
					  mLYOutPayDetailSet.remove(mLYOutPayDetailSet.get(s));
					
			  }
			  
		  }
		  continue;
	  }
		  
	  }	  
	  //对分离结果进行结息封装LYOutPayQueryDetail数据
	  for(int m=0; m<resultList.size(); m++){
		  OutPayQuerySendResult result=resultList.get(m);
		  String id  = result.getId();
		  String resultcode  = result.getResultcode();
		  String printdate  = result.getPrintdate();
		  String billnum  = result.getBillnum();
		  String isprint  = result.getIsprint();
		  if("Y".equals(isprint)){
			  isprint="02";
		  }else{
			  isprint="01";
		  }
		  String vstatus  = result.getVstatus();
		//返回结果代码
		  LYOutPayQueryDetailSchema queryDetailSchema = new LYOutPayQueryDetailSchema();
		  zBatchNo =PubFun.getCurrentDate2()+PubFun1.CreateMaxNo("OUTQ"+PubFun.getCurrentDate2(), 10);
		  queryDetailSchema.setBatchNo(zBatchNo);
		  if("Y".equals(resultcode)){
			  queryDetailSchema.setBusilNo(id);
			  queryDetailSchema.setbillnum(billnum);
			  queryDetailSchema.setisprint(isprint);
			  queryDetailSchema.setvstatus(vstatus);
			  queryDetailSchema.setprintdate(printdate);
			  queryDetailSchema.setMakeDate(PubFun.getCurrentDate());
			  queryDetailSchema.setMakeTime(PubFun.getCurrentTime());
			  queryDetailSchema.setModifyDate(PubFun.getCurrentDate());
			  queryDetailSchema.setModifyTime(PubFun.getCurrentTime());
			  resLYOutPayQueryDetailSet.add(queryDetailSchema);
			  
		  	} 
	    
		  }
	  
		}catch(Exception e){
			billInfoList.clear();
			e.printStackTrace();
		}
	  
	   mLYOutPayDetailSet.clear();
	  }
		 	  
  }
	  resultMMap.put(resLYOutPayQueryDetailSet,SysConst.DELETE_AND_INSERT);
	  resultMMap.put(resLYOutPayDetailSet,SysConst.DELETE_AND_INSERT);
	  
	  return true;
  }

  /**
   * 根据业务逻辑对数据进行处理
   * @param: 无
   * @return: void
   */
  private void prepareOutputData() {
	  mVData.clear();
	  mVData.add(resultMMap);
  }
  
//  public MMap getResult(){
//	  return resultMMap;
//  }
  
}
