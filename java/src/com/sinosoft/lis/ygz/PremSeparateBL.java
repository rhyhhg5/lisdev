package com.sinosoft.lis.ygz;

import java.util.ArrayList;
import java.util.List;

import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.db.LYPremSeparateDetailDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LYPremSeparateDetailSchema;
import com.sinosoft.lis.vschema.LYPremSeparateDetailSet;
import com.sinosoft.lis.ygz.obj.BillInfo;
import com.sinosoft.lis.ygz.obj.Content;
import com.sinosoft.lis.ygz.obj.SendResult;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

/**
 * <p>Title: 营改增，价税分离处理类</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author lzy
 * @version 1.0
 */

public class PremSeparateBL {
	
  private GlobalInput tGI= new GlobalInput();
  
  private LYPremSeparateDetailSet tLYPremSeparateDetailSet = null;
  
  public CErrors mErrors = new CErrors();
  
  private String operator ;
  
  private int sysCount = 1000;//默认每一千条数据发送一次请求
  
  private MMap resultMMap = new MMap();

  public PremSeparateBL() {
	  
  }
  
  public static void main(String[] args) {
	  PremSeparateBL tt=new PremSeparateBL();
	  LYPremSeparateDetailDB tLYPremSeparateDetailDB=new LYPremSeparateDetailDB();
	  LYPremSeparateDetailSet set=tLYPremSeparateDetailDB.executeQuery("select * from LYPremSeparateDetail where state = '01'");
	  VData data=new VData();
	  data.add(set);
	  GlobalInput mgl=new GlobalInput();
	  mgl.Operator="ser";
	  data.add(mgl);
	  tt.getSubmit(data, "");
	  MMap map=tt.getResult();
	  VData tVData=new VData();
	  tVData.add(map);
	  
	  PubSubmit tPubSubmit=new PubSubmit();
	  tPubSubmit.submitData(tVData, "");
	  
  }
  
  /**
   * 接收LYPremSeparateDetail数据进行价税分离
   * 数据量最好不要超过1000条
   * @param tVData
   * @param operator 可为空""
   * @return
   */
  public boolean getSubmit(VData tVData,String operator){
	  
	  if(!getInputData(tVData)){
		  return false;
	  }
	  
	  if(!dealData()){
		  return false;
	  }
	  
	  return true;
  }
  
  /**
   * 获取数据
   * @param mInputData
   * @return
   */
  private boolean getInputData(VData mInputData) {
      tGI = ((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));

      tLYPremSeparateDetailSet = ((LYPremSeparateDetailSet) mInputData.getObjectByObjectName("LYPremSeparateDetailSet",0));
      
      if(null == tLYPremSeparateDetailSet || tLYPremSeparateDetailSet.size()==0){
      	  CError tError = new CError();
          tError.moduleName = "PremSeparateBL";
          tError.functionName = "getInputData";
          tError.errorMessage = "没有接收到保单信息!";
          this.mErrors.addOneError(tError);
      }

      if (tGI == null) {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "PremSeparateBL";
          tError.functionName = "getInputData";
          tError.errorMessage = "没有得到足够的数据，请您确认!";
          this.mErrors.addOneError(tError);

          return false;
      }
      operator = tGI.Operator;

      return true;
  }
  
  
  /**
   * 封装数据调用价税分离接口
   * @return
   */
  private boolean dealData(){
	  LYPremSeparateDetailSet resLYPremSeparateDetailSet=new LYPremSeparateDetailSet();
	  LYPremSeparateDetailSet mLYPremSeparateDetailSet=new LYPremSeparateDetailSet();
	  int count = 0;
	  for(int i = 1; i <= tLYPremSeparateDetailSet.size();i++){
		  LYPremSeparateDetailSchema mLYPremSeparateDetailSchema = tLYPremSeparateDetailSet.get(i);
		  mLYPremSeparateDetailSet.add(mLYPremSeparateDetailSchema);
		  count++;
		  if(count == sysCount || i == tLYPremSeparateDetailSet.size()){
			  count=0;
			  List<BillInfo> billInfoList= new  ArrayList<BillInfo>();
			  for(int j=1; j<=mLYPremSeparateDetailSet.size(); j++){
				  LYPremSeparateDetailSchema detailSchema=mLYPremSeparateDetailSet.get(j);
				  BillInfo bill=new BillInfo();
				  bill.setSequenceNo(detailSchema.getBusiNo());
		//		  bill.setPk_jiashuifl(detailSchema.getBusiNo());
				  bill.setId(detailSchema.getBusiNo());//业务流水号
				  bill.setBaodanno(detailSchema.getPolicyNo());
				  bill.setShouzhitype(detailSchema.getBusiType());//收支类型
				  bill.setXianzhongcode(detailSchema.getRiskCode());
				  //收支类型为09-工本费的 险种代码传 000000
				  if("09".equals(detailSchema.getBusiType())){
					  bill.setXianzhongcode(BQ.FILLDATA);
				  }
				  
				  bill.setJigoucode(detailSchema.getManageCom());
				  bill.setHanshuiamount(detailSchema.getPayMoney());//含税金额
				  //期间类型 1-当期 2-全额
				  String def2;
				  if("4".equals(detailSchema.getOtherState())){
					  def2 = "1";
				  }else if("5".equals(detailSchema.getOtherState())){
					  def2 = "2";
				  }
				  bill.setDef2(detailSchema.getOtherState());
				  bill.setDef3(detailSchema.getPrtNo());
				  String autoFlag="Y";
				  if("03".equals(detailSchema.getOtherState())){
					  autoFlag = "N";
				  }
				  bill.setDef4(autoFlag);
				  
				  billInfoList.add(bill);
			  }
			  //调用接口进行价税分离
			  PremSeparateInterface tPremSeparateInterface=new PremSeparateInterface();
			  List<SendResult> resultList=tPremSeparateInterface.callInterface(billInfoList);
			  
			  //对分离结果进行结息封装LYPremSeparateDetail数据
			  for(int n=0; n<resultList.size(); n++){
				  SendResult result=resultList.get(n);
				  String resCode = result.getResultcode();
				  String resDesc = result.getResultdescription();
				  Content content=result.getContent();
				  String resultID =content.getId();
				  String bdocid = result.getBdocid();
				  if(null==resultID){
					  resultID = bdocid;
				  }
				  //返回结果代码为失败
//				  if(!"1".equals(resCode) || null == resultID){
//			          System.out.println(resCode+"返回结果失败:"+resDesc);
//			          continue;
//				  }
				  
				  System.out.println("busino="+resultID);
				  for(int k=1; k<=mLYPremSeparateDetailSet.size(); k++){
					  LYPremSeparateDetailSchema detailSchema=mLYPremSeparateDetailSet.get(k);
					  
					  if(resultID.equals(detailSchema.getBusiNo())){
						  //返回结果失败，保存失败原因
						  if(!"1".equals(resCode) || null == resultID){
					          System.out.println(resCode+"返回结果失败:"+resDesc);
					          if(null!=resDesc && resDesc.length()>200){
					        	  resDesc = resDesc.substring(0, 200);
					          }
					          detailSchema.setErrorInfo(resDesc+"");
							  resLYPremSeparateDetailSet.add(detailSchema);
							  //已经获取结果的remove掉
							  mLYPremSeparateDetailSet.remove(mLYPremSeparateDetailSet.get(k));
							  break;
						  }
						  detailSchema.setTaxRate(content.getRate());
						  detailSchema.setMoneyNoTax(content.getWushuiamount());
						  detailSchema.setMoneyTax(content.getTaxes());
						  
						  //实际没有得到价税分离结果的不改状态
						  if(null == detailSchema.getMoneyTax() && null == detailSchema.getMoneyNoTax()){
							  detailSchema.setErrorInfo(resDesc+"");
							  System.out.println(resultID+",实际没有获取到价税分离数据");
						  }else{
							  detailSchema.setState("02");
							  detailSchema.setErrorInfo(null);
						  }
		//				  detailSchema.setModifyDate(PubFun.getCurrentDate());
		//				  detailSchema.setModifyTime(PubFun.getCurrentTime());
						  resLYPremSeparateDetailSet.add(detailSchema);
						  //已经获取结果的remove掉
						  mLYPremSeparateDetailSet.remove(mLYPremSeparateDetailSet.get(k));
						  break;
					  }
				  }
			  }
			  mLYPremSeparateDetailSet.clear();
		  }
	  }
	  resultMMap.put(resLYPremSeparateDetailSet,SysConst.DELETE_AND_INSERT);

	  
	  return true;
	
  }
  
  public MMap getResult(){
	  return resultMMap;
  }
  

}
