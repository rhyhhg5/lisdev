package com.sinosoft.lis.ygz;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class GetPayPGMoneyUI {

	/** �������� */
    public CErrors mErrors = new CErrors();
    
    public GetPayPGMoneyUI(){}
    
    public boolean submitData(VData cInputData, String cOperate){
    	
    	try{
    		GetPayPGMoneyBL tGetPayPGMoneyBL = new GetPayPGMoneyBL();
            if (!tGetPayPGMoneyBL.submitData(cInputData, cOperate)){
                this.mErrors.copyAllErrors(tGetPayPGMoneyBL.mErrors);
                return false;
            }
            return true;
        }catch (Exception e){
            e.printStackTrace();
            this.mErrors.addOneError(e.getMessage());
            return false;
        }
    }
}
