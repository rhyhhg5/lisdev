package com.sinosoft.lis.ygz;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LYOutPayDetailSchema;
import com.sinosoft.lis.vschema.LYOutPayDetailSet;
import com.sinosoft.lis.ygz.OutPayUploadBL;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

public class LJAPayGrpInvoiceDataSend{
	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    
    public LJAPayGrpInvoiceDataSend(){}
    
    private SSRS mSSRS;
    
    private LYOutPayDetailSet mLYOutPayDetailSet = null;
    private GlobalInput mGlobalInput = new GlobalInput();
    private MMap mMap = new MMap();
    private VData mVData = new VData();
    
    private String mCurrentDate = PubFun.getCurrentDate();
	private String mCurrentTime = PubFun.getCurrentTime();
    
    public void run() {
        System.out.println("---GrpInvoiceDateSend开始---");
        submitData();
        System.out.println("---GrpInvoiceDateSend正常结束---");
    }
    
    private boolean submitData(){
    	
    	mSSRS = getData1();
    	
    	if(mSSRS==null){
    		buildError("getData","没有获得需要推送的数据");
    		return false;
    	}
    	
    	if(!dealData()){
    		buildError("dealData","数据准备失败");
    		return false;
    	}
    	
    	if(!callService()){
    		return false;
    	}
    	
    	if(!submit()){
    		return false;
    	}
    	
        return true;
    }
    
    private boolean callService(){
    	
    	OutPayUploadBL tOutPayUploadBL = new OutPayUploadBL();
    	VData tVData = new VData();
    	tVData.add(mLYOutPayDetailSet);
    	tVData.add(mGlobalInput);
    	if(!tOutPayUploadBL.getSubmit(tVData, "")){
    		buildError("callService", "调用OutPayUploadBL接口失败。");
    		return false;
    	}
    	mMap = new MMap();
    	mMap = tOutPayUploadBL.getResult();
    	    	
    	return true;
    }
    
    private boolean submit(){
    	
    	mVData.add(mMap);
    	PubSubmit p = new PubSubmit();
    	if (!p.submitData(mVData, SysConst.INSERT)){
    		System.out.println("提交数据失败");
    		buildError("submitData", "提交数据失败");
    		return false;
    	}

        return true;
    	
    }
    
    private boolean dealData(){
    	
    	mLYOutPayDetailSet = new LYOutPayDetailSet();
    	for(int i=1; i<=mSSRS.getMaxRow(); i++){
    		
    		LYOutPayDetailSchema tLYOutPayDetailSchema = new LYOutPayDetailSchema();
    		
    		// 是否更新客户信息
    		tLYOutPayDetailSchema.setisupdate("Y");
    		// 数据日期 
    		tLYOutPayDetailSchema.setvdata(mCurrentDate);
    		// 集团  传"00"，,00是集团编码
    		tLYOutPayDetailSchema.setpkgroup("00");
    		tLYOutPayDetailSchema.setgroup("00");
    		// 组织
    		tLYOutPayDetailSchema.setpkorg(mSSRS.GetText(i, 9));
    		tLYOutPayDetailSchema.setorg(mSSRS.GetText(i, 9));
    		tLYOutPayDetailSchema.setorgv(mSSRS.GetText(i, 9));
    		// 税率 
    		tLYOutPayDetailSchema.settaxrate(mSSRS.GetText(i, 15));
    		// 不含税金额（原币） 
    		tLYOutPayDetailSchema.setoriamt(mSSRS.GetText(i, 1));
    		// 税额（原币） 
    		tLYOutPayDetailSchema.setoritax(mSSRS.GetText(i, 2));
    		// 不含税金额（本币） 
    		tLYOutPayDetailSchema.setlocalamt(mSSRS.GetText(i, 1));
    		// 税额（本币）
    		tLYOutPayDetailSchema.setlocaltax(mSSRS.GetText(i, 2));
    		// 客户号
    		tLYOutPayDetailSchema.setcustcode(mSSRS.GetText(i, 3));
    		tLYOutPayDetailSchema.setcode(mSSRS.GetText(i, 3));
    		// 客户的名称 
    		tLYOutPayDetailSchema.setcustname(mSSRS.GetText(i, 13));
    		tLYOutPayDetailSchema.setname(mSSRS.GetText(i, 13));
    		// 客户的类型 团体客户为"1"
    		// 需求调整为 客户类型统一为"2"
    		tLYOutPayDetailSchema.setcusttype("2");
    		tLYOutPayDetailSchema.setcustomertype("2");
    		// 价税分离项目  传"01"用来匹配商品服务档案
    		tLYOutPayDetailSchema.setptsitem("01");
    		// 申报类型  “0”表示正常申报
    		tLYOutPayDetailSchema.setdectype("0");
    		// 来源的系统  传“01”，代表核心系统
			tLYOutPayDetailSchema.setsrcsystem("01");
			// 金额是否含税  “0”表示含税，“1”表示不含税
			tLYOutPayDetailSchema.settaxtype("0");
			// 收付标志  “0”表示收入，“1”表示支出
			tLYOutPayDetailSchema.setpaymentflag("0");
			// 境内外标示  “0”表示境内，“1”表示境外
			tLYOutPayDetailSchema.setoverseasflag("0");
			// 是否开票   传“Y/N”，必须传
			tLYOutPayDetailSchema.setisbill("Y");
			// 汇总地类型  “0”表示汇总，“1”表示属地，后续纳税
			tLYOutPayDetailSchema.setareatype("0");
    		// 凭证id -- 保单号
    		tLYOutPayDetailSchema.setvoucherid(mSSRS.GetText(i, 4));
    		// 交易流水号 
    		tLYOutPayDetailSchema.settranserial(mSSRS.GetText(i, 5));
    		// 交易日期--确认核销日期:取保单的生效与签单日期孰后日期
    		tLYOutPayDetailSchema.settrandate(mSSRS.GetText(i, 6));
    		// 产品代码--险种编码前四位
    		tLYOutPayDetailSchema.setprocode(mSSRS.GetText(i, 7));
    		// 交易币种
    		tLYOutPayDetailSchema.settrancurrency("CNY");
    		// 交易金额 
    		tLYOutPayDetailSchema.settranamt(mSSRS.GetText(i, 8));
    		// 备注 -- 保单号+险种号
    		tLYOutPayDetailSchema.setvdef1("保单号:"+mSSRS.GetText(i, 4)+";险种:"+mSSRS.GetText(i, 14));
    		// vdef2 -- 保单号
    		tLYOutPayDetailSchema.setvdef2(mSSRS.GetText(i, 4));
    		// 部门 -- 管理机构
    		tLYOutPayDetailSchema.setdeptdoc(mSSRS.GetText(i, 9));
    		// 主键 --流水号
    		tLYOutPayDetailSchema.setBusiNo(mSSRS.GetText(i, 5));
    		// 业务项目编码--价税分离表的流水号
    		tLYOutPayDetailSchema.setbusipk(mSSRS.GetText(i, 5));
    		// 保单生效日期
    		tLYOutPayDetailSchema.setbilleffectivedate(mSSRS.GetText(i, 10));
    		// 投保单号
    		tLYOutPayDetailSchema.setinsureno(mSSRS.GetText(i, 11));
    		// 预打发票标识 : 01--预打 02--非预打
    		tLYOutPayDetailSchema.setprebilltype("02");
    		// 收付类型 ： 01--收费  02--付费
    		tLYOutPayDetailSchema.setmoneytype("01");
    		// 收付费号  
    		tLYOutPayDetailSchema.setmoneyno(mSSRS.GetText(i, 12));
    		// 业务类型代码  01--保费收入
    		tLYOutPayDetailSchema.setbusitype("01");
    		// makedate,maketime,modifydate,modifytime
    		tLYOutPayDetailSchema.setMakeDate(mCurrentDate);
			tLYOutPayDetailSchema.setMakeTime(mCurrentTime);
			tLYOutPayDetailSchema.setModifyDate(mCurrentDate);
			tLYOutPayDetailSchema.setModifyTime(mCurrentTime);
			
			// 邮件说明要新添字段
			tLYOutPayDetailSchema.setinvtype("0"); // 默认为0
			tLYOutPayDetailSchema.setvaddress("0"); // 默认为0
			tLYOutPayDetailSchema.setvsrcsystem("01"); // 默认为01
			tLYOutPayDetailSchema.settaxpayertype("01"); // 默认为01
			tLYOutPayDetailSchema.setenablestate("2"); // 默认为2
    	
			mLYOutPayDetailSet.add(tLYOutPayDetailSchema);
    	}
    	
    	return true;
    }
    
    /** 获取需要推送的签单完成的团单 */
    private SSRS getData1(){
    	
    	mGlobalInput.ComCode = "86";
    	mGlobalInput.Operator = "001";
    	SSRS tSSRS = new SSRS();
    	ExeSQL tExeSQL = new ExeSQL();
    	String SQL = " select lyp.moneynotax, lyp.moneytax, lcg.appntno, lcg.grpcontno, trim(lyp.grppolno)||trim(lyp.payno)||trim(lyp.paytype) "
    			   + " ,(case when lcg.cvalidate>=lcg.signdate then lcg.cvalidate else lcg.signdate end) "
    			   + " ,substr(lyp.riskcode,1,4),lyp.sumactupaymoney,lcg.managecom,lcg.cvalidate,lcg.prtno "
    			   + " ,lyp.payno,lcg.grpname "
    			   + " ,(select riskname from lmriskapp where riskcode=lyp.riskcode) "
    			   + " ,lyp.taxrate "
    			   + " from lcgrpcont lcg "
    			   + " inner join ljapaygrp lyp "
    			   + " on lcg.grpcontno = lyp.grpcontno "	
    			   + " where lcg.appflag='1' "
    			   + " and lyp.moneynotax is not null and lyp.moneynotax != ''"
    			   + " and lyp.paytype ='ZC' and lyp.payno in (select payno from ljapay where incomeno = lyp.grpcontno and duefeetype = '0') "
    			   + " and not exists (select 1 from LYOutPayDetail where busino = trim(lyp.grppolno)||trim(lyp.payno)||trim(lyp.paytype) and state != '03') "
    			   + " and not exists (select 1 from LYOutPayDetail where busino in (select busino from lypremseparatedetail where prtno = lcg.prtno and otherstate = '03'))"
    			   + " and lyp.riskcode not in(select riskcode from lmriskapp where RiskType8 = '6')"
    			   + " union "
    			   + " select lyp.moneynotax, lyp.moneytax, lcg.appntno, lcg.grpcontno, trim(SerialNo)||trim(moneytype) "
    			   + " ,(case when lcg.cvalidate>=lcg.signdate then lcg.cvalidate else lcg.signdate end) "
    			   + " ,substr(lyp.riskcode,1,4),lyp.fee,lcg.managecom,lcg.cvalidate,lcg.prtno "
    			   + " ,lyp.payno,lcg.grpname "
    			   + " ,(select riskname from lmriskapp where riskcode=lyp.riskcode) "
    			   + " ,lyp.taxrate "
    			   + " from lcgrpcont lcg "
    			   + " inner join LCInsureAccFeeTrace lyp "
    			   + " on lcg.grpcontno = lyp.grpcontno "	
    			   + " where lcg.appflag='1' and lyp.grpcontno <> '00000000000000000000'"
    			   + " and lyp.moneytype in ('GL','KF')"
    			   + " and lyp.moneynotax is not null and lyp.moneynotax != ''"
    			   + " and not exists (select 1 from LYOutPayDetail where busino=trim(lyp.SerialNo)||trim(lyp.moneytype) and state != '03') "
    			   ;
    	tSSRS = tExeSQL.execSQL(SQL);
    	return tSSRS;
    }
    private void buildError(String szFunc, String szErrMsg){
        CError cError = new CError();
        cError.moduleName = "GrpInvoiceDateSend";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    
    public static void main(String[] args) {
    	LJAPayGrpInvoiceDataSend instance = new LJAPayGrpInvoiceDataSend();
    	instance.run();
	}
}
