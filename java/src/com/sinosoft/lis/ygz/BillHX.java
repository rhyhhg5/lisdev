package com.sinosoft.lis.ygz;

import java.util.List;


import org.jdom.Document;
import com.sinosoft.lis.db.LYOutPayDetailDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LYOutPayDetailSchema;
import com.sinosoft.lis.schema.LYOutPayQueryDetailSchema;
import com.sinosoft.lis.vschema.LYOutPayDetailSet;
import com.sinosoft.lis.vschema.LYOutPayQueryDetailSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;
import com.sinosoft.wasforwardxml.obj.MsgHead;
import com.sinosoft.wasforwardxml.xml.ctrl.ABusLogic;
import com.sinosoft.wasforwardxml.xml.ctrl.MsgCollection;

/**
 * 
 * @author liyt add by 2016-06-28 营改增 发票打印实时回写
 *
 */
public class BillHX extends ABusLogic {
	public String BatchNo = "";// 批次号

	public String MsgType = "";// 报文类型

	public String Operator = "";// 操作者

	public GlobalInput mGlobalInput = new GlobalInput();// 公共信息
	
	public BILLTable cBILLTable;
	
	public String mError = "";// 处理过程中的错误信息

	public CErrors mErrors = new CErrors();

	private MMap resultMMap = new MMap();

	private String zBatchNo = "";

	VData mVData = new VData();

	protected boolean deal(MsgCollection cMsgInfos) {
		System.out.println("开始处理营改增发票打印实时回写");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		MsgType = tMsgHead.getMsgType();
		Operator = tMsgHead.getSendOperator();
		mGlobalInput.Operator = tMsgHead.getSendOperator();
		LYOutPayQueryDetailSet resLYOutPayQueryDetailSet = new LYOutPayQueryDetailSet();
		LYOutPayDetailSet resLYOutPayDetailSet=new LYOutPayDetailSet();
		LYOutPayDetailSet mLYOutPayDetailSet = new LYOutPayDetailSet();
		try {
			List tBillList = cMsgInfos.getBodyByFlag("BILLTable");
			if (tBillList == null || tBillList.size() == 0 ) {
				errLog("获取发票回写信息失败。");
				return false;
			}
			for (int i = 0; i < tBillList.size(); i++) {
				cBILLTable = (BILLTable) tBillList.get(i);
				String tPrintde = cBILLTable.getPrintdate();// 开票日期
				if (!"".equals(tPrintde) && null != tPrintde) {
					String tId = cBILLTable.getId();
					String tIsprint = cBILLTable.getIsprint();
					String tBillnum = cBILLTable.getBillnum();
					String tVstatus = cBILLTable.getVstatus();
					if ("Y".equals(tIsprint)) {
						tIsprint = "02";
					} else {
						tIsprint = "01";
					}
					LYOutPayDetailDB tLYOutPayDetailDB = new LYOutPayDetailDB();
					tLYOutPayDetailDB.setBusiNo(tId);
					mLYOutPayDetailSet = tLYOutPayDetailDB.query();
					for (int j = 1; j <= mLYOutPayDetailSet.size(); j++) {
						LYOutPayDetailSchema xdetailSchema = mLYOutPayDetailSet.get(j);
						if (tId.equals(xdetailSchema.getBusiNo())) {
							  xdetailSchema.setbillprintdate(tPrintde);
							  xdetailSchema.setbillprintno(tBillnum);
							  xdetailSchema.setprintstate(tIsprint);
							  xdetailSchema.setvstatus(tVstatus);
							  String xTrandate=xdetailSchema.gettrandate();//交易日期
							  String xDate=PubFun.getLaterDate(xTrandate, tPrintde);
							  if(xDate.equals(tPrintde)){
									  xdetailSchema.setcvalistate("01");
							  }else{
									  xdetailSchema.setcvalistate("02");
							  }
					
							  xdetailSchema.setModifyDate(PubFun.getCurrentDate());
							  xdetailSchema.setModifyTime(PubFun.getCurrentTime());
							  resLYOutPayDetailSet.add(xdetailSchema);
							  //删除已获取结果的
							  mLYOutPayDetailSet.remove(mLYOutPayDetailSet.get(j));
						}
					}
				}
			}
			
			//对分离结果进行结息封装LYOutPayQueryDetail数据
			  for(int m=0; m<tBillList.size(); m++){
				  cBILLTable=(BILLTable) tBillList.get(m);
				  String id  = cBILLTable.getId();
				  String resultcode  = cBILLTable.getResultcode();
				  String printdate  = cBILLTable.getPrintdate();
				  String billnum  = cBILLTable.getBillnum();
				  String isprint  = cBILLTable.getIsprint();
				  if("Y".equals(isprint)){
					  isprint="02";
				  }else{
					  isprint="01";
				  }
				  String vstatus  = cBILLTable.getVstatus();
				//返回结果代码
				  LYOutPayQueryDetailSchema queryDetailSchema = new LYOutPayQueryDetailSchema();
				  zBatchNo =PubFun.getCurrentDate2()+PubFun1.CreateMaxNo("OUTQ"+PubFun.getCurrentDate2(), 10);
				  queryDetailSchema.setBatchNo(zBatchNo);
				  if("Y".equals(resultcode)){
					  queryDetailSchema.setBusilNo(id);
					  queryDetailSchema.setbillnum(billnum);
					  queryDetailSchema.setisprint(isprint);
					  queryDetailSchema.setvstatus(vstatus);
					  queryDetailSchema.setprintdate(printdate);
					  queryDetailSchema.setMakeDate(PubFun.getCurrentDate());
					  queryDetailSchema.setMakeTime(PubFun.getCurrentTime());
					  queryDetailSchema.setModifyDate(PubFun.getCurrentDate());
					  queryDetailSchema.setModifyTime(PubFun.getCurrentTime());
					  resLYOutPayQueryDetailSet.add(queryDetailSchema);
					  
				  	} 
			    
				  }
			  mLYOutPayDetailSet.clear();
			  
			  resultMMap.put(resLYOutPayQueryDetailSet,SysConst.DELETE_AND_INSERT);
			  resultMMap.put(resLYOutPayDetailSet,SysConst.DELETE_AND_INSERT);
				
				// 装配处理好的数据，准备给后台进行保存
			  prepareOutputData();

			  PubSubmit tPubSubmit = new PubSubmit();
			  if (!tPubSubmit.submitData(mVData,"")) {
			        // @@错误处理
			        this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			        CError tError = new CError();
			        tError.moduleName = "BillHX";
			        tError.functionName = "submitData";
			        tError.errorMessage = "数据提交失败!";

			        this.mErrors.addOneError(tError);
			        return false;
			   }
		} catch (Exception ex) {
			return false;
		}
		
		return true;
	}

	protected boolean deal(MsgCollection cMsgInfos, Document cInXmlDoc) {
		return false;
	}
    
	  /**
	   * 根据业务逻辑对数据进行处理
	   * @param: 无
	   * @return: void
	   */
	  private void prepareOutputData() {
		  mVData.clear();
		  mVData.add(resultMMap);
	  }
}
