package com.sinosoft.lis.ygz;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LYPremSeparateDetailSchema;
import com.sinosoft.lis.vschema.LYPremSeparateDetailSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 
 * @author add by liyt 2016-07-14
 * 提取契约当天承保当天退保数据数据
 *
 */
public class GetTBSeparateBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	/** 数据操作字符串 */
	private String mOperate;
	/** 开始日期*/
	private String mStartDate;
	/** 结束日期*/
	private String mEndDate;
	/** 印刷号*/
	private String mPrtNo;
	//业务处理相关变量
	private MMap mMap = new MMap();
	
	/** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
	
	private String mCurrentDate = PubFun.getCurrentDate();
	private String mCurrentTime = PubFun.getCurrentTime();
	
	public boolean submitData(VData cInputData, String cOperate) {
		System.out.println(mCurrentDate+":开始提取契约价税分离数据。");
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		// 进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GetTBSeparateBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败GetTBSeparateBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}

		// 准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}

		// 保存数据
		PubSubmit tPubSubmit = new PubSubmit();

		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			System.out.print("保存数据返回");
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "GetTBSeparateBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mInputData = null;
		System.out.println(mCurrentDate+":提取契约价税分离数据结束。");
		return true;
	}

	// 获得输出数据
	private boolean getInputData(VData cInputData) {
		TransferData tTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
		mStartDate = (String) tTransferData.getValueByName("StartDate");
		mEndDate = (String) tTransferData.getValueByName("EndDate");
		mPrtNo = (String) tTransferData.getValueByName("PrtNo");
		return true;
	}
	
//  业务处理
	private boolean dealData() {
		LYPremSeparateDetailSet tLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
		ExeSQL tExeSQL = new ExeSQL();
		String tWhereSQL = "";
		String tWherePSQL = "";
		String tWhereTSQL = "";
		String tDealDate = "2016-01-01";
		if(mStartDate != null && !"".equals(mStartDate)){
			tWhereSQL += " and enteraccdate >= '" + mStartDate + "'";
		}
		if(mEndDate != null && !"".equals(mEndDate)){
			tWhereSQL += " and enteraccdate <= '" + mEndDate + "'";
		}
		if("".equals(tWhereSQL)){
			tWhereSQL += "and enteraccdate >= current date - 7 day ";
		}
		if(mPrtNo != null && !"".equals(mPrtNo)){
			tWherePSQL += " and lcp.PrtNo = '" + mPrtNo + "'";
			tWhereTSQL += " and TempFeeData.PrtNo = '" + mPrtNo + "'";
			
		}
		
		
		/**
		 * add by liyt 2016-07-14
		 * 
		 * 当天承保当天退保不到险种的暂收表契约数据
		 * 
		 */
		String tSQL = " select "  
				+ " TempFeeData.BusiNo||trim(RiskData.riskcode),"
				+ " TempFeeData.TempFeeNo,TempFeeData.TempFeeType,TempFeeData.OtherNo,TempFeeData.OtherNoType,TempFeeData.PrtNo,"
				+ " TempFeeData.PolicyNo,RiskData.RiskCode,RiskData.ManageCom,RiskData.RiskPrem "
				+ " from "
				+ " ( "
				+ " select "
				+ " trim(ljt.TempFeeNo)||trim(ljt.TempFeeType) BusiNo,"
				+ " ljt.TempFeeNo TempFeeNo,ljt.TempFeeType TempFeeType,ljt.OtherNo OtherNo,ljt.OtherNoType OtherNoType,"
				+ " case when ljt.OtherNoType in ('4','5','11') then ljt.otherno else ("
				+ " select prtno from lbcont where conttype = '1' and contno = ljt.otherno "
				+ " union select prtno from lbgrpcont where grpcontno = ljt.otherno "
				+ " ) end PrtNo,"
				+ " case when ljt.OtherNoType in ('0','1','2','7') then ljt.otherno else ("
				+ " select contno from lbcont where prtno = ljt.otherno and conttype = '1' "
				+ " union select grpcontno from lbgrpcont where prtno = ljt.otherno "
				+ " ) end PolicyNo "
				+ " from ljtempfee ljt "
				+ " where 1=1 "
				+ " and ljt.riskcode = '000000' "
				+ " and tempfeetype in (select code from ldcode where codetype = 'ygzqyttype') "
				+ " and trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||trim(ljt.RiskCode) not in (select busino from LYPremSeparateDetail) " 
				+ tWhereSQL
				+ " ) as TempFeeData "
				+ " inner join "
				+ " (select lcp.contno PolicyNo,lcp.managecom ManageCom,lcp.riskcode RiskCode,sum(prem) RiskPrem "
				+ " from lbpol lcp "
				+ " where lcp.conttype = '1' "
				+ " and lcp.Makedate >= '"+tDealDate+"' "
				+ tWherePSQL
				+ " group by lcp.contno,lcp.managecom,lcp.riskcode) as RiskData "
				+ " on TempFeeData.PolicyNo = RiskData.PolicyNo "
				+ " and not exists (select 1 from lbgrpcont where prtno = TempFeeData.prtno and coinsuranceflag ='1')"
				+ " and TempFeeData.BusiNo||trim(RiskData.riskcode) not in (select busino from LYPremSeparateDetail) "
				+ " and not exists (select 1 from LYPremSeparateDetail where prtno = TempFeeData.prtno and tempfeetype = 'N') "
				+ "";
		SSRS tZeroSSRS = tExeSQL.execSQL(tSQL);
		if(tZeroSSRS != null && tZeroSSRS.getMaxRow() > 0){
			System.out.println("当天承保当天退保不到险种的暂收表契约数据==="+tZeroSSRS.getMaxRow());
			for (int i = 1; i <= tZeroSSRS.getMaxRow(); i++) {
				LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
				tLYPremSeparateDetailSchema.setBusiNo(tZeroSSRS.GetText(i, 1));
				tLYPremSeparateDetailSchema.setTempFeeNo(tZeroSSRS.GetText(i, 2));
				tLYPremSeparateDetailSchema.setTempFeeType(tZeroSSRS.GetText(i, 3));
				tLYPremSeparateDetailSchema.setOtherNo(tZeroSSRS.GetText(i, 4));
				tLYPremSeparateDetailSchema.setOtherNoType(tZeroSSRS.GetText(i, 5));
				tLYPremSeparateDetailSchema.setPrtNo(tZeroSSRS.GetText(i, 6));
				tLYPremSeparateDetailSchema.setPolicyNo(tZeroSSRS.GetText(i, 7));
				tLYPremSeparateDetailSchema.setRiskCode(tZeroSSRS.GetText(i, 8));
				tLYPremSeparateDetailSchema.setBusiType("01");
				tLYPremSeparateDetailSchema.setManageCom(tZeroSSRS.GetText(i, 9));
				tLYPremSeparateDetailSchema.setPayMoney(tZeroSSRS.GetText(i, 10));
				tLYPremSeparateDetailSchema.setState("01");
				tLYPremSeparateDetailSchema.setOtherState("01");
				tLYPremSeparateDetailSchema.setMoneyType("01");
				tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
				tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
				tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
			}
		}else{
			System.out.println("当天承保当天退保不到险种的暂收表契约数据为0！");
		}
		
		/**
		 * add by liyt 2016-07-14
		 * 
		 * 当天承保当天退保期缴。----个单
		 */
		tSQL = " select "  
				+ " 'QJ'||TempFeeData.BusiNo||trim(RiskData.riskcode),"
				+ " TempFeeData.TempFeeNo,TempFeeData.TempFeeType,TempFeeData.OtherNo,TempFeeData.OtherNoType,TempFeeData.PrtNo,"
				+ " TempFeeData.PolicyNo,RiskData.RiskCode,RiskData.ManageCom,RiskData.RiskPrem,"
				+ " RiskData.PayIntv,timestampdiff (64, char(timestamp(RiskData.PayendDate) - timestamp(RiskData.Cvalidate)))  "
				+ " from "
				+ " ( "
				+ " select "
				+ " trim(ljt.TempFeeNo)||trim(ljt.TempFeeType) BusiNo,"
				+ " ljt.TempFeeNo TempFeeNo,ljt.TempFeeType TempFeeType,ljt.OtherNo OtherNo,ljt.OtherNoType OtherNoType,"
				+ " case when ljt.OtherNoType in ('4','5','11') then ljt.otherno else ("
				+ " select prtno from lbcont where conttype = '1' and contno = ljt.otherno "
				+ " union select prtno from lbgrpcont where grpcontno = ljt.otherno "
				+ " ) end PrtNo,"
				+ " case when ljt.OtherNoType in ('0','1','2','7') then ljt.otherno else ("
				+ " select contno from lbcont where prtno = ljt.otherno and conttype = '1' "
				+ " union select grpcontno from lbgrpcont where prtno = ljt.otherno "
				+ " ) end PolicyNo "
				+ " from ljtempfee ljt "
				+ " where 1=1 "
				+ " and tempfeetype in (select code from ldcode where codetype = 'ygzqyttype') "
				+ " and 'QJ'||trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||trim(ljt.RiskCode) not in (select busino from LYPremSeparateDetail) " 
				+ tWhereSQL
				+ " group by ljt.TempFeeNo,ljt.TempFeeType,ljt.OtherNo,ljt.OtherNoType "
				+ " ) as TempFeeData "
				+ " inner join "
				+ " (select lcp.contno PolicyNo,lcp.managecom ManageCom,lcp.riskcode RiskCode,sum(prem) RiskPrem, "
				+ " lcp.payintv PayIntv,lcp.cvalidate Cvalidate,lcp.payenddate PayendDate "
				+ " from lbpol lcp "
				+ " where lcp.conttype = '1' "
				+ " and payintv != 0 and payintv != -1 "
				+ tWherePSQL
				+ " and lcp.Makedate >= '"+tDealDate+"' "
				+ " group by lcp.contno,lcp.managecom,lcp.riskcode,lcp.payintv,lcp.cvalidate,lcp.payenddate "
				+ " ) as RiskData "
				+ " on TempFeeData.PolicyNo = RiskData.PolicyNo "
				+ " and not exists ( select 1 from lbgrpcont where prtno = TempFeeData.prtno and coinsuranceflag ='1')"
				+ " and 'QJ'||TempFeeData.BusiNo||trim(RiskData.riskcode) not in (select busino from LYPremSeparateDetail) "
				+ " and not exists (select 1 from LYPremSeparateDetail where prtno = TempFeeData.prtno and tempfeetype = 'N') "
				+ " order by 'QJ'||TempFeeData.BusiNo||trim(RiskData.riskcode) ";
		SSRS tQJSSRS = tExeSQL.execSQL(tSQL);
		if(tQJSSRS != null && tQJSSRS.getMaxRow() > 0){
			LYPremSeparateDetailSet tempLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
			for (int i = 1; i <= tQJSSRS.getMaxRow(); i++) {
				LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
				tLYPremSeparateDetailSchema.setBusiNo(tQJSSRS.GetText(i, 1));
				tLYPremSeparateDetailSchema.setTempFeeNo(tQJSSRS.GetText(i, 2));
				tLYPremSeparateDetailSchema.setTempFeeType(tQJSSRS.GetText(i, 3));
				tLYPremSeparateDetailSchema.setOtherNo(tQJSSRS.GetText(i, 4));
				tLYPremSeparateDetailSchema.setOtherNoType(tQJSSRS.GetText(i, 5));
				tLYPremSeparateDetailSchema.setPrtNo(tQJSSRS.GetText(i, 6));
				tLYPremSeparateDetailSchema.setPolicyNo(tQJSSRS.GetText(i, 7));
				tLYPremSeparateDetailSchema.setRiskCode(tQJSSRS.GetText(i, 8));
				tLYPremSeparateDetailSchema.setBusiType("01");
				tLYPremSeparateDetailSchema.setManageCom(tQJSSRS.GetText(i, 9));
				String tPayMoney = getQJRiskSumPrem(tQJSSRS.GetText(i, 10),tQJSSRS.GetText(i, 11),tQJSSRS.GetText(i, 12));
				if("".equals(tPayMoney) || tPayMoney == null){
					System.out.println("个单期缴保费获取险种总保费失败！");
					continue;
				}
				tLYPremSeparateDetailSchema.setPayMoney(tPayMoney);
				tLYPremSeparateDetailSchema.setState("01");
				tLYPremSeparateDetailSchema.setOtherState("04");
				tLYPremSeparateDetailSchema.setMoneyType("01");
				tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
				tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
				tempLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
			}
			
			//可能payendyearflag为A，且为多被保人情况，被保人年龄不一致，导致费用没有合并的处理
			if(tempLYPremSeparateDetailSet != null && tempLYPremSeparateDetailSet.size()>0 ){
				for (int i = 1; i <= tempLYPremSeparateDetailSet.size(); i++) {
					String tBusiNo = tempLYPremSeparateDetailSet.get(i).getBusiNo();
					for (int j = i+1; j <= tempLYPremSeparateDetailSet.size(); j++) {
						if(tBusiNo.equals(tempLYPremSeparateDetailSet.get(j).getBusiNo())){
							String tPayMoney1 =  tempLYPremSeparateDetailSet.get(i).getPayMoney();
							String tPayMoney2 =  tempLYPremSeparateDetailSet.get(j).getPayMoney();
							String tempPayMoney = String.valueOf(Double.parseDouble(tPayMoney1) + Double.parseDouble(tPayMoney2));
							tempLYPremSeparateDetailSet.get(i).setPayMoney(tempPayMoney);
							tempLYPremSeparateDetailSet.remove(tempLYPremSeparateDetailSet.get(j));
							i--;
							break;
						}
					}
				}
				System.out.println("当天承保当天退保个单总保费数据==="+tempLYPremSeparateDetailSet.size());
				tLYPremSeparateDetailSet.add(tempLYPremSeparateDetailSet);
			}
		}else{
			System.out.println("当天承保当天退保个单总保费数据为0！");
		}
		
		/**
		 * add by liyt 2016-07-14 
		 * 
		 * 当天承保当天退保期缴.---------团险
		 */
		tSQL = " select "  
				+ " 'QJ'||TempFeeData.BusiNo||trim(RiskData.riskcode),"
				+ " TempFeeData.TempFeeNo,TempFeeData.TempFeeType,TempFeeData.OtherNo,TempFeeData.OtherNoType,TempFeeData.PrtNo,"
				+ " TempFeeData.PolicyNo,RiskData.RiskCode,RiskData.ManageCom,RiskData.RiskPrem,"
				+ " RiskData.PayIntv,timestampdiff (64, char(timestamp(RiskData.PayendDate) - timestamp(RiskData.Cvalidate))) "
				+ " from "
				+ " ( "
				+ " select "
				+ " trim(ljt.TempFeeNo)||trim(ljt.TempFeeType) BusiNo,"
				+ " ljt.TempFeeNo TempFeeNo,ljt.TempFeeType TempFeeType,ljt.OtherNo OtherNo,ljt.OtherNoType OtherNoType,"
				+ " case when ljt.OtherNoType in ('4','5','11') then ljt.otherno else ("
				+ " select prtno from lbcont where conttype = '1' and contno = ljt.otherno "
				+ " union select prtno from lbgrpcont where grpcontno = ljt.otherno "
				+ " ) end PrtNo,"
				+ " case when ljt.OtherNoType in ('0','1','2','7') then ljt.otherno else ("
				+ " select contno from lbcont where prtno = ljt.otherno and conttype = '1' "
				+ " union select grpcontno from lbgrpcont where prtno = ljt.otherno "
				+ " ) end PolicyNo "
				+ " from ljtempfee ljt "
				+ " where 1=1 "
				+ " and tempfeetype in (select code from ldcode where codetype = 'ygzqyttype') "
				+ " and 'QJ'||trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||trim(ljt.RiskCode) not in (select busino from LYPremSeparateDetail) " 
				+ tWhereSQL
				+ " group by ljt.TempFeeNo,ljt.TempFeeType,ljt.OtherNo,ljt.OtherNoType "
				+ " ) as TempFeeData "
				+ " inner join "
				+ " (select lcp.grpcontno PolicyNo,lcp.managecom ManageCom,lcp.riskcode RiskCode,sum(prem) RiskPrem, "
				+ " lcp.payintv PayIntv,lcp.cvalidate Cvalidate,lcp.payenddate PayendDate "
				+ " from lbpol lcp "
				+ " where lcp.conttype = '2' "
				+ " and payintv != 0 and payintv != -1 "
				+ tWherePSQL
				+ " and lcp.Makedate >= '"+tDealDate+"' "
				+ " group by lcp.grpcontno,lcp.managecom,lcp.riskcode,lcp.payintv,lcp.cvalidate,lcp.payenddate "
				+ " ) as RiskData "
				+ " on TempFeeData.PolicyNo = RiskData.PolicyNo "
				+ " and not exists ( select 1 from lbgrpcont where prtno = TempFeeData.prtno and coinsuranceflag = '1')"
				+ " and 'QJ'||TempFeeData.BusiNo||trim(RiskData.riskcode) not in (select busino from LYPremSeparateDetail) "
				// add by liyt 2016-07-01 排除发票预打数据
				+ " and not exists (select 1 from LYPremSeparateDetail where prtno = TempFeeData.prtno and tempfeetype = 'N') "
				+ " order by 'QJ'||TempFeeData.BusiNo||trim(RiskData.riskcode) ";
		SSRS tQJTSSRS = tExeSQL.execSQL(tSQL);
		if(tQJTSSRS != null && tQJTSSRS.getMaxRow() > 0){
			LYPremSeparateDetailSet tempLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
			for (int i = 1; i <= tQJTSSRS.getMaxRow(); i++) {
				LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
				tLYPremSeparateDetailSchema.setBusiNo(tQJTSSRS.GetText(i, 1));
				tLYPremSeparateDetailSchema.setTempFeeNo(tQJTSSRS.GetText(i, 2));
				tLYPremSeparateDetailSchema.setTempFeeType(tQJTSSRS.GetText(i, 3));
				tLYPremSeparateDetailSchema.setOtherNo(tQJTSSRS.GetText(i, 4));
				tLYPremSeparateDetailSchema.setOtherNoType(tQJTSSRS.GetText(i, 5));
				tLYPremSeparateDetailSchema.setPrtNo(tQJTSSRS.GetText(i, 6));
				tLYPremSeparateDetailSchema.setPolicyNo(tQJTSSRS.GetText(i, 7));
				tLYPremSeparateDetailSchema.setRiskCode(tQJTSSRS.GetText(i, 8));
				tLYPremSeparateDetailSchema.setBusiType("01");
				tLYPremSeparateDetailSchema.setManageCom(tQJTSSRS.GetText(i, 9));
				String tPayMoney = getQJRiskSumPrem(tQJTSSRS.GetText(i, 10),tQJTSSRS.GetText(i, 11),tQJTSSRS.GetText(i, 12));
				if("".equals(tPayMoney) || tPayMoney == null){
					System.out.println("团单期缴保费获取险种总保费失败！");
					continue;
				}
				tLYPremSeparateDetailSchema.setPayMoney(tPayMoney);
				tLYPremSeparateDetailSchema.setState("01");
				tLYPremSeparateDetailSchema.setOtherState("04");
				tLYPremSeparateDetailSchema.setMoneyType("01");
				tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
				tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
				tempLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
			}
			//可能payendyearflag为A，且为多被保人情况，被保人年龄不一致，导致费用没有合并的处理
			if(tempLYPremSeparateDetailSet != null && tempLYPremSeparateDetailSet.size()>0 ){
				for (int i = 1; i <= tempLYPremSeparateDetailSet.size(); i++) {
					String tBusiNo = tempLYPremSeparateDetailSet.get(i).getBusiNo();
					for (int j = i+1; j <= tempLYPremSeparateDetailSet.size(); j++) {
						if(tBusiNo.equals(tempLYPremSeparateDetailSet.get(j).getBusiNo())){
							String tPayMoney1 =  tempLYPremSeparateDetailSet.get(i).getPayMoney();
							String tPayMoney2 =  tempLYPremSeparateDetailSet.get(j).getPayMoney();
							String tempPayMoney = String.valueOf(Double.parseDouble(tPayMoney1) + Double.parseDouble(tPayMoney2));
							tempLYPremSeparateDetailSet.get(i).setPayMoney(tempPayMoney);
							tempLYPremSeparateDetailSet.remove(tempLYPremSeparateDetailSet.get(j));
							i--;
							break;
						}
					}
				}
				System.out.println("当天承保当天退保团单总保费数据==="+tempLYPremSeparateDetailSet.size());
				tLYPremSeparateDetailSet.add(tempLYPremSeparateDetailSet);
			}
		}else{
			System.out.println("当天承保当天退保团单总保费数据为0！");
		}
		
		
		
		mMap.put(tLYPremSeparateDetailSet, SysConst.INSERT);
		
		return true;
	}
	/**
	 * 准备后台的数据
	 * @return boolean
	 */
	private boolean prepareOutputData() {
		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GetTBSeparateBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
	
	//获取期缴保费总保费
		private String getQJRiskSumPrem(String aPrem,String aPayIntv,String tDifMonth){
			String tAllRiskSumPrem = "";
			int tPayTimes = 1;
			int tPayIntv = Integer.parseInt(aPayIntv);
			double tPrem = Double.parseDouble(aPrem);
//			if("Y".equals(aPayEndYearFlag)){
//				tPayTimes = (12/tPayIntv)*tPayEndYear;
//			}else if("M".equals(aPayEndYearFlag)){
//				tPayTimes = tPayEndYear/tPayIntv;
//			}else if("D".equals(aPayEndYearFlag)){
//				tPayTimes = (12/tPayIntv)*(tPayEndYear/365);
//			}else if("A".equals(aPayEndYearFlag)){
//				tPayTimes = (12/tPayIntv)*(tPayEndYear - Integer.parseInt(aInsuredAppAge));
//			}
			tPayTimes = Integer.parseInt(tDifMonth)/tPayIntv;
			if(tPayTimes < 1){
				tPayTimes = 1;
			}
			tAllRiskSumPrem = String.valueOf(tPrem*tPayTimes);
			return tAllRiskSumPrem;
		}
	
	public static void main(String[] args) {
		GetTBSeparateBL tGetTBSeparateBL = new GetTBSeparateBL();
		VData cInputData = new VData();
		TransferData tTransferData = new TransferData();
		String tStartDate = "2016-05-01";
		String tEndDate = "";
		String tPrtNo = "";
		tTransferData.setNameAndValue("StartDate", tStartDate);
		tTransferData.setNameAndValue("EndDate", tEndDate);
		tTransferData.setNameAndValue("PrtNo", tPrtNo);
		cInputData.add(tTransferData);
		tGetTBSeparateBL.submitData(cInputData, "");
	}
}
