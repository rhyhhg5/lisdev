package com.sinosoft.lis.ygz;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LYPremSeparateDetailSchema;
import com.sinosoft.lis.tb.GrpPayPlanDetailBL;
import com.sinosoft.lis.vschema.LCGrpPayPlanDetailSet;
import com.sinosoft.lis.vschema.LYPremSeparateDetailSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class GetTempFeeSeparateBL {
	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	/** 数据操作字符串 */
	private String mOperate;
	/** 开始日期*/
	private String mStartDate;
	/** 结束日期*/
	private String mEndDate;
	/** 印刷号*/
	private String mPrtNo;
	//业务处理相关变量
	private MMap mMap = new MMap();
	
	/** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
	
	private String mCurrentDate = PubFun.getCurrentDate();
	private String mCurrentTime = PubFun.getCurrentTime();
	
	public boolean submitData(VData cInputData, String cOperate) {
		System.out.println(mCurrentDate+":开始提取契约价税分离数据。");
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		// 进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GetTempFeeSeparateBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败GetTempFeeSeparateBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}

		// 准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}

		// 保存数据
		PubSubmit tPubSubmit = new PubSubmit();

		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			System.out.print("保存数据返回");
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "GetTempFeeSeparateBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mInputData = null;
		System.out.println(mCurrentDate+":提取契约价税分离数据结束。");
		return true;
	}

	// 获得输出数据
	private boolean getInputData(VData cInputData) {
		TransferData tTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
		mStartDate = (String) tTransferData.getValueByName("StartDate");
		mEndDate = (String) tTransferData.getValueByName("EndDate");
		mPrtNo = (String) tTransferData.getValueByName("PrtNo");
		return true;
	}
	
//  业务处理
	private boolean dealData() {
		LYPremSeparateDetailSet tLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
		ExeSQL tExeSQL = new ExeSQL();
		String tWhereSQL = "";
		String tWherePSQL = "";
		String tWhereTSQL = "";
		String tDealDate = "2016-01-01";
		if(mStartDate != null && !"".equals(mStartDate)){
			tWhereSQL += " and enteraccdate >= '" + mStartDate + "'";
		}
		if(mEndDate != null && !"".equals(mEndDate)){
			tWhereSQL += " and enteraccdate <= '" + mEndDate + "'";
		}
		if("".equals(tWhereSQL)){
			tWhereSQL += "and enteraccdate >= current date - 7 day ";
		}
		if(mPrtNo != null && !"".equals(mPrtNo)){
			tWherePSQL += " and lcp.PrtNo = '" + mPrtNo + "'";
			tWhereTSQL += " and TempFeeData.PrtNo = '" + mPrtNo + "'";
			
		}
		
		/**
		 * modify liyt 2016-06-23 排除共保保单数据
		 * 
		 * 暂收表到险种的契约数据
		 */
		String tSQL = " select * " 
					+ " from ( "
					+ " select "
                    + " trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||trim(ljt.RiskCode),"
                    + " ljt.TempFeeNo,ljt.TempFeeType,ljt.OtherNo,ljt.OtherNoType,"
					+ " case when ljt.OtherNoType in ('4','5','11') then ljt.otherno else ("
					+ " select prtno from lccont where conttype = '1' and contno = ljt.otherno "
					+ " union select prtno from lbcont where conttype = '1' and contno = ljt.otherno "
//					+ " union select prtno from lobcont where conttype = '1' and contno = ljt.otherno "
					+ " union select prtno from lcgrpcont where grpcontno = ljt.otherno "
					+ " union select prtno from lbgrpcont where grpcontno = ljt.otherno "
//					+ " union select prtno from lobgrpcont where grpcontno = ljt.otherno "
					+ " ) end PrtNo,"
					+ " case when ljt.OtherNoType in ('0','1','2','7') then ljt.otherno else ("
					+ " select contno from lccont where prtno = ljt.otherno and conttype = '1' "
					+ " union select contno from lbcont where prtno = ljt.otherno and conttype = '1' "
//					+ " union select contno from lobcont where prtno = ljt.otherno and conttype = '1' "
					+ " union select grpcontno from lcgrpcont where prtno = ljt.otherno "
					+ " union select grpcontno from lbgrpcont where prtno = ljt.otherno "
//					+ " union select grpcontno from lobgrpcont where prtno = ljt.otherno "
					+ " ) end PolicyNo,"
					+ " ljt.riskcode,ljt.managecom,ljt.paymoney "
					+ " from ljtempfee ljt "
					+ " where ljt.riskcode != '000000' "
					+ " and tempfeetype in (select code from ldcode where codetype = 'ygzqyttype') "
					+ " and trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||trim(ljt.RiskCode) not in (select busino from LYPremSeparateDetail) "
					+ tWhereSQL
					+ " ) TempFeeData "
					+ " where 1=1 "
					+ " and not exists (select 1 from lcgrpcont where prtno = TempFeeData.prtno and coinsuranceflag ='1')"					
					+ tWhereTSQL
					// add by liyt 2016-07-01 排除发票预打数据
					+ " and not exists (select 1 from LYPremSeparateDetail where prtno = TempFeeData.prtno and tempfeetype = 'N') "
//					+ " and exists ( "
//					+ " select 1 from lccont lcc where lcc.contno = TempFeeData.PolicyNo and lcc.conttype = '1' and lcc.Makedate >= '"+tDealDate+"' "
//					+ " union select 1 from lcgrpcont lgc where lgc.GrpContNo = TempFeeData.PolicyNo and lgc.Makedate >= '"+tDealDate+"' "
//					+ " ) "
					+ " ";
		SSRS tNoZeroSSRS = tExeSQL.execSQL(tSQL);
		if(tNoZeroSSRS != null && tNoZeroSSRS.getMaxRow() > 0){
			System.out.println("暂收表到险种的契约数据==="+tNoZeroSSRS.getMaxRow());
			for (int i = 1; i <= tNoZeroSSRS.getMaxRow(); i++) {
				LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
				tLYPremSeparateDetailSchema.setBusiNo(tNoZeroSSRS.GetText(i, 1));
				tLYPremSeparateDetailSchema.setTempFeeNo(tNoZeroSSRS.GetText(i, 2));
				tLYPremSeparateDetailSchema.setTempFeeType(tNoZeroSSRS.GetText(i, 3));
				tLYPremSeparateDetailSchema.setOtherNo(tNoZeroSSRS.GetText(i, 4));
				tLYPremSeparateDetailSchema.setOtherNoType(tNoZeroSSRS.GetText(i, 5));
				tLYPremSeparateDetailSchema.setPrtNo(tNoZeroSSRS.GetText(i, 6));
				tLYPremSeparateDetailSchema.setPolicyNo(tNoZeroSSRS.GetText(i, 7));
				tLYPremSeparateDetailSchema.setRiskCode(tNoZeroSSRS.GetText(i, 8));
				tLYPremSeparateDetailSchema.setBusiType("01");
				tLYPremSeparateDetailSchema.setManageCom(tNoZeroSSRS.GetText(i, 9));
				tLYPremSeparateDetailSchema.setPayMoney(tNoZeroSSRS.GetText(i, 10));
				tLYPremSeparateDetailSchema.setState("01");
				tLYPremSeparateDetailSchema.setOtherState("01");
				tLYPremSeparateDetailSchema.setMoneyType("01");
				tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
				tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
				tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
			}
			
		}else{
			System.out.println("暂收表到险种的契约数据为0！");
		}
		
		/**
		 * modify liyt 2016-06-23 排除共保保单数据
		 * 
		 * 暂收表不到险种非删除表的契约数据
		 */
		tSQL = " select "  
				+ " TempFeeData.BusiNo||trim(RiskData.riskcode),"
				+ " TempFeeData.TempFeeNo,TempFeeData.TempFeeType,TempFeeData.OtherNo,TempFeeData.OtherNoType,TempFeeData.PrtNo,"
				+ " TempFeeData.PolicyNo,RiskData.RiskCode,RiskData.ManageCom,RiskData.RiskPrem "
				+ " from "
				+ " ( "
				+ " select "
				+ " trim(ljt.TempFeeNo)||trim(ljt.TempFeeType) BusiNo,"
				+ " ljt.TempFeeNo TempFeeNo,ljt.TempFeeType TempFeeType,ljt.OtherNo OtherNo,ljt.OtherNoType OtherNoType,"
				+ " case when ljt.OtherNoType in ('4','5','11') then ljt.otherno else ("
				+ " select prtno from lccont where conttype = '1' and contno = ljt.otherno "
				+ " union select prtno from lbcont where conttype = '1' and contno = ljt.otherno "
//				+ " union select prtno from lobcont where conttype = '1' and contno = ljt.otherno "
				+ " union select prtno from lcgrpcont where grpcontno = ljt.otherno "
				+ " union select prtno from lbgrpcont where grpcontno = ljt.otherno "
//				+ " union select prtno from lobgrpcont where grpcontno = ljt.otherno "
				+ " ) end PrtNo,"
				+ " case when ljt.OtherNoType in ('0','1','2','7') then ljt.otherno else ("
				+ " select contno from lccont where prtno = ljt.otherno and conttype = '1' "
				+ " union select contno from lbcont where prtno = ljt.otherno and conttype = '1' "
//				+ " union select contno from lobcont where prtno = ljt.otherno and conttype = '1' "
				+ " union select grpcontno from lcgrpcont where prtno = ljt.otherno "
				+ " union select grpcontno from lbgrpcont where prtno = ljt.otherno "
//				+ " union select grpcontno from lobgrpcont where prtno = ljt.otherno "
				+ " ) end PolicyNo "
				+ " from ljtempfee ljt "
				+ " where 1=1 "
				+ " and ljt.riskcode = '000000' "
				+ " and tempfeetype in (select code from ldcode where codetype = 'ygzqyttype') "
				+ " and trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||trim(ljt.RiskCode) not in (select busino from LYPremSeparateDetail) " 
				+ tWhereSQL
				+ " ) as TempFeeData "
				+ " inner join "
				+ " (select lcp.contno PolicyNo,lcp.managecom ManageCom,lcp.riskcode RiskCode,sum(prem) RiskPrem "
				+ " from lcpol lcp "
				+ " where lcp.conttype = '1' "
				+ " and lcp.uwflag not in ('1','2','8','a')"
				+ " and lcp.Makedate >= '"+tDealDate+"' "
				+ tWherePSQL
				+ " group by lcp.contno,lcp.managecom,lcp.riskcode) as RiskData "
				+ " on TempFeeData.PolicyNo = RiskData.PolicyNo "
				+ " and not exists (select 1 from lcgrpcont where prtno = TempFeeData.prtno and coinsuranceflag ='1')"
				+ " and TempFeeData.BusiNo||trim(RiskData.riskcode) not in (select busino from LYPremSeparateDetail) "
				// add by liyt 2016-07-01 排除发票预打数据
				+ " and not exists (select 1 from LYPremSeparateDetail where prtno = TempFeeData.prtno and tempfeetype = 'N') "
				+ "";
		SSRS tZeroSSRS = tExeSQL.execSQL(tSQL);
		if(tZeroSSRS != null && tZeroSSRS.getMaxRow() > 0){
			System.out.println("暂收表不到险种的契约数据==="+tZeroSSRS.getMaxRow());
			for (int i = 1; i <= tZeroSSRS.getMaxRow(); i++) {
				LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
				tLYPremSeparateDetailSchema.setBusiNo(tZeroSSRS.GetText(i, 1));
				tLYPremSeparateDetailSchema.setTempFeeNo(tZeroSSRS.GetText(i, 2));
				tLYPremSeparateDetailSchema.setTempFeeType(tZeroSSRS.GetText(i, 3));
				tLYPremSeparateDetailSchema.setOtherNo(tZeroSSRS.GetText(i, 4));
				tLYPremSeparateDetailSchema.setOtherNoType(tZeroSSRS.GetText(i, 5));
				tLYPremSeparateDetailSchema.setPrtNo(tZeroSSRS.GetText(i, 6));
				tLYPremSeparateDetailSchema.setPolicyNo(tZeroSSRS.GetText(i, 7));
				tLYPremSeparateDetailSchema.setRiskCode(tZeroSSRS.GetText(i, 8));
				tLYPremSeparateDetailSchema.setBusiType("01");
				tLYPremSeparateDetailSchema.setManageCom(tZeroSSRS.GetText(i, 9));
				tLYPremSeparateDetailSchema.setPayMoney(tZeroSSRS.GetText(i, 10));
				tLYPremSeparateDetailSchema.setState("01");
				tLYPremSeparateDetailSchema.setOtherState("01");
				tLYPremSeparateDetailSchema.setMoneyType("01");
				tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
				tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
				tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
			}
		}else{
			System.out.println("暂收表不到险种非删除表的契约数据为0！");
		}
		
		/**
		 * modify liyt 2016-06-23 排除共保保单数据
		 * 
		 * 期缴--需险种表数据。----个单
		 */
		tSQL = " select "  
				+ " 'QJ'||TempFeeData.BusiNo||trim(RiskData.riskcode),"
				+ " TempFeeData.TempFeeNo,TempFeeData.TempFeeType,TempFeeData.OtherNo,TempFeeData.OtherNoType,TempFeeData.PrtNo,"
				+ " TempFeeData.PolicyNo,RiskData.RiskCode,RiskData.ManageCom,RiskData.RiskPrem,"
				+ " RiskData.PayIntv,timestampdiff (64, char(timestamp(RiskData.PayendDate) - timestamp(RiskData.Cvalidate)))  "
				+ " from "
				+ " ( "
				+ " select "
				+ " trim(ljt.TempFeeNo)||trim(ljt.TempFeeType) BusiNo,"
				+ " ljt.TempFeeNo TempFeeNo,ljt.TempFeeType TempFeeType,ljt.OtherNo OtherNo,ljt.OtherNoType OtherNoType,"
				+ " case when ljt.OtherNoType in ('4','5','11') then ljt.otherno else ("
				+ " select prtno from lccont where conttype = '1' and contno = ljt.otherno "
				+ " union select prtno from lbcont where conttype = '1' and contno = ljt.otherno "
//				+ " union select prtno from lobcont where conttype = '1' and contno = ljt.otherno "
				+ " union select prtno from lcgrpcont where grpcontno = ljt.otherno "
				+ " union select prtno from lbgrpcont where grpcontno = ljt.otherno "
//				+ " union select prtno from lobgrpcont where grpcontno = ljt.otherno "
				+ " ) end PrtNo,"
				+ " case when ljt.OtherNoType in ('0','1','2','7') then ljt.otherno else ("
				+ " select contno from lccont where prtno = ljt.otherno and conttype = '1' "
				+ " union select contno from lbcont where prtno = ljt.otherno and conttype = '1' "
//				+ " union select contno from lobcont where prtno = ljt.otherno and conttype = '1' "
				+ " union select grpcontno from lcgrpcont where prtno = ljt.otherno "
				+ " union select grpcontno from lbgrpcont where prtno = ljt.otherno "
//				+ " union select grpcontno from lobgrpcont where prtno = ljt.otherno "
				+ " ) end PolicyNo "
				+ " from ljtempfee ljt "
				+ " where 1=1 "
				+ " and tempfeetype in (select code from ldcode where codetype = 'ygzqyttype') "
				+ " and 'QJ'||trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||trim(ljt.RiskCode) not in (select busino from LYPremSeparateDetail) " 
				+ tWhereSQL
				+ " group by ljt.TempFeeNo,ljt.TempFeeType,ljt.OtherNo,ljt.OtherNoType "
				+ " ) as TempFeeData "
				+ " inner join "
				+ " (select lcp.contno PolicyNo,lcp.managecom ManageCom,lcp.riskcode RiskCode,sum(prem) RiskPrem, "
				+ " lcp.payintv PayIntv,lcp.cvalidate Cvalidate,lcp.payenddate PayendDate "
				+ " from lcpol lcp "
				+ " where lcp.conttype = '1' "
				+ " and lcp.uwflag not in ('1','2','8','a')"
				+ " and payintv != 0 and payintv != -1 "
				+ tWherePSQL
				+ " and lcp.Makedate >= '"+tDealDate+"' "
				+ " group by lcp.contno,lcp.managecom,lcp.riskcode,lcp.payintv,lcp.cvalidate,lcp.payenddate "
				+ " ) as RiskData "
				+ " on TempFeeData.PolicyNo = RiskData.PolicyNo "
				+ " and not exists ( select 1 from lcgrpcont where prtno = TempFeeData.prtno and coinsuranceflag ='1')"
				+ " and 'QJ'||TempFeeData.BusiNo||trim(RiskData.riskcode) not in (select busino from LYPremSeparateDetail) "
				// add by liyt 2016-07-01 排除发票预打数据
				+ " and not exists (select 1 from LYPremSeparateDetail where prtno = TempFeeData.prtno and tempfeetype = 'N') "
				+ " order by 'QJ'||TempFeeData.BusiNo||trim(RiskData.riskcode) ";
		SSRS tQJSSRS = tExeSQL.execSQL(tSQL);
		if(tQJSSRS != null && tQJSSRS.getMaxRow() > 0){
			LYPremSeparateDetailSet tempLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
			for (int i = 1; i <= tQJSSRS.getMaxRow(); i++) {
				LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
				tLYPremSeparateDetailSchema.setBusiNo(tQJSSRS.GetText(i, 1));
				tLYPremSeparateDetailSchema.setTempFeeNo(tQJSSRS.GetText(i, 2));
				tLYPremSeparateDetailSchema.setTempFeeType(tQJSSRS.GetText(i, 3));
				tLYPremSeparateDetailSchema.setOtherNo(tQJSSRS.GetText(i, 4));
				tLYPremSeparateDetailSchema.setOtherNoType(tQJSSRS.GetText(i, 5));
				tLYPremSeparateDetailSchema.setPrtNo(tQJSSRS.GetText(i, 6));
				tLYPremSeparateDetailSchema.setPolicyNo(tQJSSRS.GetText(i, 7));
				tLYPremSeparateDetailSchema.setRiskCode(tQJSSRS.GetText(i, 8));
				tLYPremSeparateDetailSchema.setBusiType("01");
				tLYPremSeparateDetailSchema.setManageCom(tQJSSRS.GetText(i, 9));
				String tPayMoney = getQJRiskSumPrem(tQJSSRS.GetText(i, 10),tQJSSRS.GetText(i, 11),tQJSSRS.GetText(i, 12));
				if("".equals(tPayMoney) || tPayMoney == null){
					System.out.println("个单期缴保费获取险种总保费失败！");
					continue;
				}
				tLYPremSeparateDetailSchema.setPayMoney(tPayMoney);
				tLYPremSeparateDetailSchema.setState("01");
				tLYPremSeparateDetailSchema.setOtherState("04");
				tLYPremSeparateDetailSchema.setMoneyType("01");
				tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
				tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
				tempLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
			}
			
			//可能payendyearflag为A，且为多被保人情况，被保人年龄不一致，导致费用没有合并的处理
			if(tempLYPremSeparateDetailSet != null && tempLYPremSeparateDetailSet.size()>0 ){
				for (int i = 1; i <= tempLYPremSeparateDetailSet.size(); i++) {
					String tBusiNo = tempLYPremSeparateDetailSet.get(i).getBusiNo();
					for (int j = i+1; j <= tempLYPremSeparateDetailSet.size(); j++) {
						if(tBusiNo.equals(tempLYPremSeparateDetailSet.get(j).getBusiNo())){
							String tPayMoney1 =  tempLYPremSeparateDetailSet.get(i).getPayMoney();
							String tPayMoney2 =  tempLYPremSeparateDetailSet.get(j).getPayMoney();
							String tempPayMoney = String.valueOf(Double.parseDouble(tPayMoney1) + Double.parseDouble(tPayMoney2));
							tempLYPremSeparateDetailSet.get(i).setPayMoney(tempPayMoney);
							tempLYPremSeparateDetailSet.remove(tempLYPremSeparateDetailSet.get(j));
							i--;
							break;
						}
					}
				}
				System.out.println("个单期缴保费总保费数据==="+tempLYPremSeparateDetailSet.size());
				tLYPremSeparateDetailSet.add(tempLYPremSeparateDetailSet);
			}
		}else{
			System.out.println("暂收表期缴表保费总保费数据为0！");
		}
		
		/**
		 * modify liyt 2016-06-23 排除共保保单数据
		 * 
		 * 期缴--需险种表数据。----团单非删除表
		 */
		tSQL = " select "  
				+ " 'QJ'||TempFeeData.BusiNo||trim(RiskData.riskcode),"
				+ " TempFeeData.TempFeeNo,TempFeeData.TempFeeType,TempFeeData.OtherNo,TempFeeData.OtherNoType,TempFeeData.PrtNo,"
				+ " TempFeeData.PolicyNo,RiskData.RiskCode,RiskData.ManageCom,RiskData.RiskPrem,"
				+ " RiskData.PayIntv,timestampdiff (64, char(timestamp(RiskData.PayendDate) - timestamp(RiskData.Cvalidate))) "
				+ " from "
				+ " ( "
				+ " select "
				+ " trim(ljt.TempFeeNo)||trim(ljt.TempFeeType) BusiNo,"
				+ " ljt.TempFeeNo TempFeeNo,ljt.TempFeeType TempFeeType,ljt.OtherNo OtherNo,ljt.OtherNoType OtherNoType,"
				+ " case when ljt.OtherNoType in ('4','5','11') then ljt.otherno else ("
				+ " select prtno from lccont where conttype = '1' and contno = ljt.otherno "
				+ " union select prtno from lbcont where conttype = '1' and contno = ljt.otherno "
//				+ " union select prtno from lobcont where conttype = '1' and contno = ljt.otherno " --不能union，会有删除、签单、退保并存的情况
				+ " union select prtno from lcgrpcont where grpcontno = ljt.otherno "
				+ " union select prtno from lbgrpcont where grpcontno = ljt.otherno "
//				+ " union select prtno from lobgrpcont where grpcontno = ljt.otherno "  --不能union，会有删除、签单、退保并存的情况
				+ " ) end PrtNo,"
				+ " case when ljt.OtherNoType in ('0','1','2','7') then ljt.otherno else ("
				+ " select contno from lccont where prtno = ljt.otherno and conttype = '1' "
				+ " union select contno from lbcont where prtno = ljt.otherno and conttype = '1' "
//				+ " union select contno from lobcont where prtno = ljt.otherno and conttype = '1' "
				+ " union select grpcontno from lcgrpcont where prtno = ljt.otherno "
				+ " union select grpcontno from lbgrpcont where prtno = ljt.otherno "
//				+ " union select grpcontno from lobgrpcont where prtno = ljt.otherno "
				+ " ) end PolicyNo "
				+ " from ljtempfee ljt "
				+ " where 1=1 "
				+ " and tempfeetype in (select code from ldcode where codetype = 'ygzqyttype') "
				+ " and 'QJ'||trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||trim(ljt.RiskCode) not in (select busino from LYPremSeparateDetail) " 
				+ tWhereSQL
				+ " group by ljt.TempFeeNo,ljt.TempFeeType,ljt.OtherNo,ljt.OtherNoType "
				+ " ) as TempFeeData "
				+ " inner join "
				+ " (select lcp.grpcontno PolicyNo,lcp.managecom ManageCom,lcp.riskcode RiskCode,sum(prem) RiskPrem, "
				+ " lcp.payintv PayIntv,lcp.cvalidate Cvalidate,lcp.payenddate PayendDate "
				+ " from lcpol lcp "
				+ " where lcp.conttype = '2' "
				+ " and lcp.uwflag not in ('1','2','8','a')"
				+ " and payintv != 0 and payintv != -1 "
				+ tWherePSQL
				+ " and lcp.Makedate >= '"+tDealDate+"' "
				+ " group by lcp.grpcontno,lcp.managecom,lcp.riskcode,lcp.payintv,lcp.cvalidate,lcp.payenddate "
				+ " ) as RiskData "
				+ " on TempFeeData.PolicyNo = RiskData.PolicyNo "
				+ " and not exists ( select 1 from lcgrpcont where prtno = TempFeeData.prtno and coinsuranceflag = '1')"
				+ " and 'QJ'||TempFeeData.BusiNo||trim(RiskData.riskcode) not in (select busino from LYPremSeparateDetail) "
				// add by liyt 2016-07-01 排除发票预打数据
				+ " and not exists (select 1 from LYPremSeparateDetail where prtno = TempFeeData.prtno and tempfeetype = 'N') "
				+ " order by 'QJ'||TempFeeData.BusiNo||trim(RiskData.riskcode) ";
		SSRS tQJTSSRS = tExeSQL.execSQL(tSQL);
		if(tQJTSSRS != null && tQJTSSRS.getMaxRow() > 0){
			LYPremSeparateDetailSet tempLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
			for (int i = 1; i <= tQJTSSRS.getMaxRow(); i++) {
				LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
				tLYPremSeparateDetailSchema.setBusiNo(tQJTSSRS.GetText(i, 1));
				tLYPremSeparateDetailSchema.setTempFeeNo(tQJTSSRS.GetText(i, 2));
				tLYPremSeparateDetailSchema.setTempFeeType(tQJTSSRS.GetText(i, 3));
				tLYPremSeparateDetailSchema.setOtherNo(tQJTSSRS.GetText(i, 4));
				tLYPremSeparateDetailSchema.setOtherNoType(tQJTSSRS.GetText(i, 5));
				tLYPremSeparateDetailSchema.setPrtNo(tQJTSSRS.GetText(i, 6));
				tLYPremSeparateDetailSchema.setPolicyNo(tQJTSSRS.GetText(i, 7));
				tLYPremSeparateDetailSchema.setRiskCode(tQJTSSRS.GetText(i, 8));
				tLYPremSeparateDetailSchema.setBusiType("01");
				tLYPremSeparateDetailSchema.setManageCom(tQJTSSRS.GetText(i, 9));
				String tPayMoney = getQJRiskSumPrem(tQJTSSRS.GetText(i, 10),tQJTSSRS.GetText(i, 11),tQJTSSRS.GetText(i, 12));
				if("".equals(tPayMoney) || tPayMoney == null){
					System.out.println("团单期缴保费获取险种总保费失败！");
					continue;
				}
				tLYPremSeparateDetailSchema.setPayMoney(tPayMoney);
				tLYPremSeparateDetailSchema.setState("01");
				tLYPremSeparateDetailSchema.setOtherState("04");
				tLYPremSeparateDetailSchema.setMoneyType("01");
				tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
				tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
				tempLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
			}
			//可能payendyearflag为A，且为多被保人情况，被保人年龄不一致，导致费用没有合并的处理
			if(tempLYPremSeparateDetailSet != null && tempLYPremSeparateDetailSet.size()>0 ){
				for (int i = 1; i <= tempLYPremSeparateDetailSet.size(); i++) {
					String tBusiNo = tempLYPremSeparateDetailSet.get(i).getBusiNo();
					for (int j = i+1; j <= tempLYPremSeparateDetailSet.size(); j++) {
						if(tBusiNo.equals(tempLYPremSeparateDetailSet.get(j).getBusiNo())){
							String tPayMoney1 =  tempLYPremSeparateDetailSet.get(i).getPayMoney();
							String tPayMoney2 =  tempLYPremSeparateDetailSet.get(j).getPayMoney();
							String tempPayMoney = String.valueOf(Double.parseDouble(tPayMoney1) + Double.parseDouble(tPayMoney2));
							tempLYPremSeparateDetailSet.get(i).setPayMoney(tempPayMoney);
							tempLYPremSeparateDetailSet.remove(tempLYPremSeparateDetailSet.get(j));
							i--;
							break;
						}
					}
				}
				System.out.println("团单暂收表期缴保费总保费数据==="+tempLYPremSeparateDetailSet.size());
				tLYPremSeparateDetailSet.add(tempLYPremSeparateDetailSet);
			}
		}else{
			System.out.println("暂收表期缴表保费总保费数据为0！");
		}
		
		/**
		 * modify liyt 2016-06-23 排除共保保单数据
		 * 
		 * 从暂收表生成的约定缴费的总保费数据
		 */
		tSQL = " select * " 
				+ " from ( "
				+ " select "
                + " 'YD'||trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||trim(ljt.RiskCode),"
                + " ljt.TempFeeNo,ljt.TempFeeType,ljt.OtherNo,ljt.OtherNoType,"
                + " case when ljt.OtherNoType in ('4','5','11') then ljt.otherno else ("
				+ " select prtno from lccont where conttype = '1' and contno = ljt.otherno "
				+ " union select prtno from lbcont where conttype = '1' and contno = ljt.otherno "
//				+ " union select prtno from lobcont where conttype = '1' and contno = ljt.otherno "
				+ " union select prtno from lcgrpcont where grpcontno = ljt.otherno "
				+ " union select prtno from lbgrpcont where grpcontno = ljt.otherno "
//				+ " union select prtno from lobgrpcont where grpcontno = ljt.otherno "
				+ " ) end PrtNo,"
				+ " case when ljt.OtherNoType in ('0','1','2','7') then ljt.otherno else ("
				+ " select contno from lccont where prtno = ljt.otherno and conttype = '1' "
				+ " union select contno from lbcont where prtno = ljt.otherno and conttype = '1' "
//				+ " union select contno from lobcont where prtno = ljt.otherno and conttype = '1' "
				+ " union select grpcontno from lcgrpcont where prtno = ljt.otherno "
				+ " union select grpcontno from lbgrpcont where prtno = ljt.otherno "
//				+ " union select grpcontno from lobgrpcont where prtno = ljt.otherno "
				+ " ) end PolicyNo,"
				+ " ljt.riskcode,ljt.managecom,ljt.paymoney "
				+ " from ljtempfee ljt "
				+ " where ljt.riskcode != '000000' "
				+ " and tempfeetype in (select code from ldcode where codetype = 'ygzqyttype') "
				+ " and 'YD'||trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||trim(ljt.RiskCode) not in (select busino from LYPremSeparateDetail) "
				+ tWhereSQL
				+ " ) TempFeeData "
				+ " where 1=1 "
				+ " and not exists (select 1 from lcgrpcont where prtno = TempFeeData.prtno and coinsuranceflag ='1') "
				+ tWhereTSQL
				+ " and exists ( "
//				+ " select 1 from lcgrpcont lgc where lgc.GrpContNo = TempFeeData.PolicyNo and payintv = '-1' and (lgc.CvaliDate >= '"+tDealDate+"' or lgc.SignDate >= '"+tDealDate+"') "
				+ " select 1 from lcgrpcont lgc where lgc.GrpContNo = TempFeeData.PolicyNo and payintv = '-1' "
				+ " ) "
				// add by liyt 2016-07-01 排除发票预打数据
				+ " and not exists (select 1 from LYPremSeparateDetail where prtno = TempFeeData.prtno and tempfeetype = 'N') "
				+ " ";
		SSRS tYDSSRS = tExeSQL.execSQL(tSQL);
		if(tYDSSRS != null && tYDSSRS.getMaxRow() > 0){
			System.out.println("暂收表约定缴费契约数据==="+tYDSSRS.getMaxRow());
			for (int i = 1; i <= tYDSSRS.getMaxRow(); i++) {
				LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
				tLYPremSeparateDetailSchema.setBusiNo(tYDSSRS.GetText(i, 1));
				tLYPremSeparateDetailSchema.setTempFeeNo(tYDSSRS.GetText(i, 2));
				tLYPremSeparateDetailSchema.setTempFeeType(tYDSSRS.GetText(i, 3));
				tLYPremSeparateDetailSchema.setOtherNo(tYDSSRS.GetText(i, 4));
				tLYPremSeparateDetailSchema.setOtherNoType(tYDSSRS.GetText(i, 5));
				tLYPremSeparateDetailSchema.setPrtNo(tYDSSRS.GetText(i, 6));
				tLYPremSeparateDetailSchema.setPolicyNo(tYDSSRS.GetText(i, 7));
				tLYPremSeparateDetailSchema.setRiskCode(tYDSSRS.GetText(i, 8));
				tLYPremSeparateDetailSchema.setBusiType("01");
				tLYPremSeparateDetailSchema.setManageCom(tYDSSRS.GetText(i, 9));
				String tPayMoney = getAllRiskSumPrem(tYDSSRS.GetText(i, 6),tYDSSRS.GetText(i, 8));
				if("".equals(tPayMoney) || tPayMoney == null){
					System.out.println("约定缴费获取险种总保费失败！");
					continue;
				}
				tLYPremSeparateDetailSchema.setPayMoney(tPayMoney);
				tLYPremSeparateDetailSchema.setState("01");
				tLYPremSeparateDetailSchema.setOtherState("05");
				tLYPremSeparateDetailSchema.setMoneyType("01");
				tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
				tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
				tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
			}
		}else{
			System.out.println("暂收表约定缴费契约数据为0！");
		}		
		
		/**
		 * add by liyt 2016-06-23 
		 * 
		 * 增加暂收表到险种的契约共保数据
		 */
//		 tSQL = " select * " 
//					+ " from ( "
//					+ " select "
//                  + " trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||trim(ljt.RiskCode),"
//                  + " ljt.TempFeeNo,ljt.TempFeeType,ljt.OtherNo,ljt.OtherNoType,"
//					+ " case when ljt.OtherNoType in ('4','5','11') then ljt.otherno else ("
//					+ " select prtno from lccont where conttype = '1' and contno = ljt.otherno "
//					+ " union select prtno from lbcont where conttype = '1' and contno = ljt.otherno "
//					+ " union select prtno from lobcont where conttype = '1' and contno = ljt.otherno "
//					+ " union select prtno from lcgrpcont where grpcontno = ljt.otherno "
//					+ " union select prtno from lbgrpcont where grpcontno = ljt.otherno "
//					+ " union select prtno from lobgrpcont where grpcontno = ljt.otherno "
//					+ " ) end PrtNo,"
//					+ " case when ljt.OtherNoType in ('0','1','2','7') then ljt.otherno else ("
//					+ " select contno from lccont where prtno = ljt.otherno and conttype = '1' "
//					+ " union select contno from lbcont where prtno = ljt.otherno and conttype = '1' "
//					+ " union select contno from lobcont where prtno = ljt.otherno and conttype = '1' "
//					+ " union select grpcontno from lcgrpcont where prtno = ljt.otherno "
//					+ " union select grpcontno from lbgrpcont where prtno = ljt.otherno "
//					+ " union select grpcontno from lobgrpcont where prtno = ljt.otherno "
//					+ " ) end PolicyNo,"
//					+ " ljt.riskcode,ljt.managecom,ljt.paymoney,"
//					+ " 1 - nvl( (select sum(rate) from lccoinsuranceparam where prtno = ljt.otherno or grpcontno = ljt.otherno),0) as rate"
//					+ " from ljtempfee ljt "
//					+ " where ljt.riskcode != '000000' "
//					+ " and tempfeetype in (select code from ldcode where codetype = 'ygzqyttype') "
//					+ " and trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||trim(ljt.RiskCode) not in (select busino from LYPremSeparateDetail) "
//					+ tWhereSQL
//					+ " ) TempFeeData "
//					+ " where 1=1 "
//					+ " and exists (select 1 from lcgrpcont where prtno = TempFeeData.prtno and coinsuranceflag ='1')"
//		            + " and not exists (select 1 from LYPremSeparateDetail where prtno = TempFeeData.prtno and tempfeetype = 'N') "
//					+ tWhereTSQL
//					+ " ";
//		SSRS tNoGBZeroSSRS = tExeSQL.execSQL(tSQL);
//		if(tNoGBZeroSSRS != null && tNoGBZeroSSRS.getMaxRow() > 0){
//			System.out.println("暂收表到险种的契约数据==="+tNoGBZeroSSRS.getMaxRow());
//			for (int i = 1; i <= tNoGBZeroSSRS.getMaxRow(); i++) {
//				LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
//				tLYPremSeparateDetailSchema.setBusiNo(tNoGBZeroSSRS.GetText(i, 1));
//				tLYPremSeparateDetailSchema.setTempFeeNo(tNoGBZeroSSRS.GetText(i, 2));
//				tLYPremSeparateDetailSchema.setTempFeeType(tNoGBZeroSSRS.GetText(i, 3));
//				tLYPremSeparateDetailSchema.setOtherNo(tNoGBZeroSSRS.GetText(i, 4));
//				tLYPremSeparateDetailSchema.setOtherNoType(tNoGBZeroSSRS.GetText(i, 5));
//				tLYPremSeparateDetailSchema.setPrtNo(tNoGBZeroSSRS.GetText(i, 6));
//				tLYPremSeparateDetailSchema.setPolicyNo(tNoGBZeroSSRS.GetText(i, 7));
//				tLYPremSeparateDetailSchema.setRiskCode(tNoGBZeroSSRS.GetText(i, 8));
//				tLYPremSeparateDetailSchema.setBusiType("01");
//				tLYPremSeparateDetailSchema.setManageCom(tNoGBZeroSSRS.GetText(i, 9));
//				Double gbPayMoney = Double.parseDouble(tNoGBZeroSSRS.GetText(i, 10));//共保保额
//				Double gbRate = Double.parseDouble(tNoGBZeroSSRS.GetText(i, 11));//共保比例
//				String payMoney = String.valueOf(gbPayMoney * gbRate);//共保分摊保额
//				tLYPremSeparateDetailSchema.setPayMoney(payMoney);
//				tLYPremSeparateDetailSchema.setState("01");
//				tLYPremSeparateDetailSchema.setOtherState("09");//09-共保保单
//				tLYPremSeparateDetailSchema.setMoneyType("01");
//				tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
//				tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
//				tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
//				tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
//				tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
//			}
//			
//		}else{
//			System.out.println("暂收表到险种的契约共保数据为0！");
//		}
		
		/**
		 * add by liyt 2016-07-25
		 * 
		 * 契约首期追加保费
		 */
		tSQL = " select "  
				+ " TempFeeData.BusiNo||trim(RiskData.riskcode),"
				+ " TempFeeData.TempFeeNo,TempFeeData.TempFeeType,TempFeeData.OtherNo,TempFeeData.OtherNoType,TempFeeData.PrtNo,"
				+ " TempFeeData.PolicyNo,RiskData.RiskCode,RiskData.ManageCom,RiskData.RiskPrem "
				+ " from "
				+ " ( "
				+ " select "
				+ " trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||'ZB' BusiNo,"
				+ " ljt.TempFeeNo TempFeeNo,ljt.TempFeeType TempFeeType,ljt.OtherNo OtherNo,ljt.OtherNoType OtherNoType,"
				+ " case when ljt.OtherNoType in ('4','5','11') then ljt.otherno else ("
				+ " select prtno from lccont where conttype = '1' and contno = ljt.otherno "
				+ " union select prtno from lbcont where conttype = '1' and contno = ljt.otherno "
				+ " ) end PrtNo,"
				+ " case when ljt.OtherNoType in ('0','1','2','7') then ljt.otherno else ("
				+ " select contno from lccont where prtno = ljt.otherno and conttype = '1' "
				+ " union select contno from lbcont where prtno = ljt.otherno and conttype = '1' "
				+ " ) end PolicyNo "
				+ " from ljtempfee ljt "
				+ " where 1=1 "
				+ " and tempfeetype in (select code from ldcode where codetype = 'ygzqyttype') "
				+ " and trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||trim(ljt.RiskCode) not in (select busino from LYPremSeparateDetail) " 
				+ tWhereSQL
				+ " ) as TempFeeData "
				+ " inner join "
				+ " (select lcp.contno PolicyNo,lcp.managecom ManageCom,lcp.riskcode RiskCode,sum(supplementaryprem) RiskPrem "
				+ " from lcpol lcp "
				+ " where lcp.conttype = '1' "
				+ " and lcp.uwflag not in ('1','2','8','a')"
				+ " and supplementaryprem > 0"
				+ " and lcp.Makedate >= '"+tDealDate+"' "
				+ tWherePSQL
				+ " group by lcp.contno,lcp.managecom,lcp.riskcode) as RiskData "
				+ " on TempFeeData.PolicyNo = RiskData.PolicyNo "
				+ " and not exists (select 1 from lcgrpcont where prtno = TempFeeData.prtno and coinsuranceflag ='1')"
				+ " and TempFeeData.BusiNo||trim(RiskData.riskcode) not in (select busino from LYPremSeparateDetail) "
				+ " and not exists (select 1 from LYPremSeparateDetail where prtno = TempFeeData.prtno and tempfeetype = 'N') "
				+ "";
		SSRS tZBSSRS = tExeSQL.execSQL(tSQL);
		if(tZBSSRS != null && tZBSSRS.getMaxRow() > 0){
			System.out.println("契约首期追加保费数据==="+tZBSSRS.getMaxRow());
			for (int i = 1; i <= tZBSSRS.getMaxRow(); i++) {
				LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
				tLYPremSeparateDetailSchema.setBusiNo(tZBSSRS.GetText(i, 1));
				tLYPremSeparateDetailSchema.setTempFeeNo(tZBSSRS.GetText(i, 2));
				tLYPremSeparateDetailSchema.setTempFeeType(tZBSSRS.GetText(i, 3));
				tLYPremSeparateDetailSchema.setOtherNo(tZBSSRS.GetText(i, 4));
				tLYPremSeparateDetailSchema.setOtherNoType(tZBSSRS.GetText(i, 5));
				tLYPremSeparateDetailSchema.setPrtNo(tZBSSRS.GetText(i, 6));
				tLYPremSeparateDetailSchema.setPolicyNo(tZBSSRS.GetText(i, 7));
				tLYPremSeparateDetailSchema.setRiskCode(tZBSSRS.GetText(i, 8));
				tLYPremSeparateDetailSchema.setBusiType("01");
				tLYPremSeparateDetailSchema.setManageCom(tZBSSRS.GetText(i, 9));
				tLYPremSeparateDetailSchema.setPayMoney(tZBSSRS.GetText(i, 10));
				tLYPremSeparateDetailSchema.setState("01");
				tLYPremSeparateDetailSchema.setOtherState("17");//契约首期追加保费
				tLYPremSeparateDetailSchema.setMoneyType("01");
				tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
				tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
				tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
			}
		}else{
			System.out.println("契约首期追加保费数据为0！");
		}
		
		mMap.put(tLYPremSeparateDetailSet, SysConst.INSERT);
		
		return true;
	}
	/**
	 * 准备后台的数据
	 * @return boolean
	 */
	private boolean prepareOutputData() {
		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GetTempFeeSeparateBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
	
	//获取约定缴费各险种拆分后总保费
	private String getAllRiskSumPrem(String aPrtNo,String aRiskCode){
		String tAllRiskSumPrem = "";
		double tRiskSumPrem = 0;
		String tPGSql = "select ProposalGrpContNo from lcgrpcont where prtno = '"+aPrtNo+"' ";
		String tProposalGrpContNo = new ExeSQL().getOneValue(tPGSql);
		LCGrpPayPlanDetailSet tLCGrpPayPlanDetailSet = new LCGrpPayPlanDetailSet();
		GrpPayPlanDetailBL tGrpPayPlanDetailBL = new GrpPayPlanDetailBL();
        VData tVData = new VData();
    	
    	TransferData mTransferData = new TransferData();
    	mTransferData.setNameAndValue("ProposalGrpContNo", tProposalGrpContNo);
    	tVData.add(mGlobalInput);
    	tVData.add(mTransferData);
        if(!tGrpPayPlanDetailBL.submitData(tVData, "")){
        	CError.buildErr(this, "拆分约定缴费计划有误", tGrpPayPlanDetailBL.mErrors);
        	System.out.println("保单印刷号："+aPrtNo+",拆分约定缴费计划有误！");
            return tAllRiskSumPrem;
        }
        MMap tmpMap = tGrpPayPlanDetailBL.getMMMap();
        tLCGrpPayPlanDetailSet = (LCGrpPayPlanDetailSet)tmpMap.getObjectByObjectName("LCGrpPayPlanDetailSet", 0);
        if(tLCGrpPayPlanDetailSet == null || tLCGrpPayPlanDetailSet.size() == 0){
        	System.out.println("保单印刷号："+aPrtNo+",约定缴费拆分失败！");
        	CError.buildErr(this, "保单印刷号："+aPrtNo+",约定缴费拆分失败！", tGrpPayPlanDetailBL.mErrors);
        	return tAllRiskSumPrem;
        }
        for(int j = 1;j<=tLCGrpPayPlanDetailSet.size();j++){
        	if(aRiskCode.equals(tLCGrpPayPlanDetailSet.get(j).getRiskCode())){
        		tRiskSumPrem = tRiskSumPrem +  tLCGrpPayPlanDetailSet.get(j).getPrem();
        	}
        }
		return tRiskSumPrem+"";
	}
	
	//获取期缴保费总保费
		private String getQJRiskSumPrem(String aPrem,String aPayIntv,String tDifMonth){
			String tAllRiskSumPrem = "";
			int tPayTimes = 1;
			int tPayIntv = Integer.parseInt(aPayIntv);
			double tPrem = Double.parseDouble(aPrem);
//			if("Y".equals(aPayEndYearFlag)){
//				tPayTimes = (12/tPayIntv)*tPayEndYear;
//			}else if("M".equals(aPayEndYearFlag)){
//				tPayTimes = tPayEndYear/tPayIntv;
//			}else if("D".equals(aPayEndYearFlag)){
//				tPayTimes = (12/tPayIntv)*(tPayEndYear/365);
//			}else if("A".equals(aPayEndYearFlag)){
//				tPayTimes = (12/tPayIntv)*(tPayEndYear - Integer.parseInt(aInsuredAppAge));
//			}
			tPayTimes = Integer.parseInt(tDifMonth)/tPayIntv;
			if(tPayTimes < 1){
				tPayTimes = 1;
			}
			tAllRiskSumPrem = String.valueOf(tPrem*tPayTimes);
			return tAllRiskSumPrem;
		}
	
	public static void main(String[] args) {
		GetTempFeeSeparateBL tGetTempFeeSeparateBL = new GetTempFeeSeparateBL();
		VData cInputData = new VData();
		TransferData tTransferData = new TransferData();
		String tStartDate = "2016-05-01";
		String tEndDate = "";
		String tPrtNo = "";
		tTransferData.setNameAndValue("StartDate", tStartDate);
		tTransferData.setNameAndValue("EndDate", tEndDate);
		tTransferData.setNameAndValue("PrtNo", tPrtNo);
		cInputData.add(tTransferData);
		tGetTempFeeSeparateBL.submitData(cInputData, "");
	}
}
