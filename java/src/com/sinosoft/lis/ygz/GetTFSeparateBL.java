package com.sinosoft.lis.ygz;


import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LYPremSeparateDetailSchema;
import com.sinosoft.lis.tb.GrpPayPlanDetailBL;
import com.sinosoft.lis.vschema.LCGrpPayPlanDetailSet;
import com.sinosoft.lis.vschema.LYPremSeparateDetailSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 
 * @author add by liyt 2016-06-22
 * 提取契约暂收退费数据
 *
 */
public class GetTFSeparateBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	/** 往后面传输数据的容器 */
	public VData mInputData = new VData();
	/** 数据操作字符串 */
	public String mOperate;
	/** 开始日期*/
	public String mStartDate;
	/** 结束日期*/
	public String mEndDate;
	/** 印刷号*/
	private String mPrtNo;
	//业务处理相关变量
	private MMap mMap = new MMap();
	
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();
	
	/** 系统当前日期 */
	private String mCurrentDate = PubFun.getCurrentDate();
	/** 系统当前时间 */
	private String mCurrentTime = PubFun.getCurrentTime();
	
	public boolean submitData(VData cInputData,String cOperate){
		System.out.println(mCurrentDate+":开始提取契约价税分离数据。");
		//将操作数据拷贝到本类中
		this.mOperate = cOperate;
		//得到外部传入的数据,将数据备份到本类中
		if(!getInputData(cInputData)){
			return false;
		}
		//业务处理
		if(!dealData()){
			//处理错误
			CError tError = new CError();
			tError.moduleName="GetTFSeparateBL";
			tError.functionName="submitData";
			tError.errorMessage="数据处理失败GetTFSeparateBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}
		//准备往后台的数据
		if(!prepareOutputData()){
			return false;
		}
		//保存数据
		PubSubmit tPubSubmit = new PubSubmit();
		if(!tPubSubmit.submitData(mInputData, mOperate)){
			System.out.print("保存数据返回");
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "GetTFSeparateBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mInputData =null;
		System.out.println(mCurrentDate+":提取契约价税分离数据结束。");
		return true;
	}
	
	//获得传入的数据
	public boolean getInputData(VData cInputData){
		TransferData tTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
		mStartDate = (String) tTransferData.getValueByName("StartDate");
		mEndDate = (String) tTransferData.getValueByName("EndDate");
		mPrtNo = (String) tTransferData.getValueByName("PrtNo");
		return true;
	}
	
	//业务处理
	public boolean dealData(){
		LYPremSeparateDetailSet tLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
		ExeSQL tExeSQL = new ExeSQL();
		String tWhereSQL = "";
		String tWherePSQL = "";
		String tWhereTSQL = "";
		String tDealDate = "2016-01-01";
		if(mStartDate !=null && !"".equals(mStartDate)){
			tWhereSQL +=" and ljt.enteraccdate >= '2016-05-01' and ljt.confdate >= '"+ mStartDate +"'";
		}
		if(mEndDate !=null && !"".equals(mEndDate)){
			tWhereSQL +=" and ljt.enteraccdate >= '2016-05-01' and ljt.confdate <= '"+ mEndDate +"'";
		}
		if("".equals(tWhereSQL)){
			tWhereSQL +=" and ljt.enteraccdate >= '2016-05-01' and ljt.confdate >= current date - 7 day";
		} 
		if(mPrtNo != null && !"".equals(mPrtNo)){
			tWherePSQL +=" and lcp.PrtNo = '"+ mPrtNo +"'";
			tWhereTSQL +=" and TempFeeData.PrtNo = '"+ mPrtNo +"'";
		} 
		
		//暂收表到险种的契约数据
		String tSQL  = " select * " 
				+ " from ( "
				+ " select "
                + " trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||trim(ljt.RiskCode)||'TF',"
                + " ljt.TempFeeNo,ljt.TempFeeType,ljt.OtherNo,ljt.OtherNoType,"
				+ " case when ljt.OtherNoType in ('4','5','11') then ljt.otherno else (select prtno from lccont where conttype = '1' and contno = ljt.otherno union select prtno from lcgrpcont where grpcontno = ljt.otherno union select prtno from lobcont where conttype ='1' and contno = ljt.otherno union select prtno from lobgrpcont where grpcontno = ljt.otherno ) end PrtNo,"
				+ " case when ljt.OtherNoType in ('0','1','2','7') then ljt.otherno else (select contno from lccont where prtno = ljt.otherno and conttype = '1' union select grpcontno from lcgrpcont where prtno = ljt.otherno union select contno from lobcont where conttype ='1' and contno = ljt.otherno union select grpcontno from lobgrpcont where prtno = ljt.otherno ) end PolicyNo,"
				+ " ljt.riskcode,ljt.managecom,ljt.paymoney "
				+ ",ljtf.actugetno ActugetNo "
				+ " from ljtempfee ljt "
				+ " inner join ljagettempfee ljtf on ljt.tempfeeno = ljtf.tempfeeno and ljt.riskcode = ljtf.riskcode"
				+ " where ljt.riskcode != '000000' "
				+ " and ljt.tempfeetype in (select code from ldcode where codetype = 'ygzqyttype') "
				+ " and trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||trim(ljt.RiskCode)||'TF' not in (select busino from LYPremSeparateDetail) "
				+ tWhereSQL
				+ " ) as TempFeeData "
				+ " where 1=1 "
				+ " and not exists (select 1 from lcgrpcont where prtno = TempFeeData.prtno and coinsuranceflag ='1' union select 1 from lobgrpcont where prtno = TempFeeData.prtno and coinsuranceflag ='1')"
				+ tWhereTSQL+"";
		SSRS tNoZeroSSRS = tExeSQL.execSQL(tSQL);
		if(tNoZeroSSRS != null && tNoZeroSSRS.getMaxRow() > 0){
			System.out.println("暂收表退费到险种的契约数据==="+tNoZeroSSRS.getMaxRow());
			for(int i = 1; i <= tNoZeroSSRS.getMaxRow();i++){
				LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
				tLYPremSeparateDetailSchema.setBusiNo(tNoZeroSSRS.GetText(i, 1));
				tLYPremSeparateDetailSchema.setTempFeeNo(tNoZeroSSRS.GetText(i, 2));
				tLYPremSeparateDetailSchema.setTempFeeType(tNoZeroSSRS.GetText(i, 3));
				tLYPremSeparateDetailSchema.setOtherNo(tNoZeroSSRS.GetText(i, 4));
				tLYPremSeparateDetailSchema.setOtherNoType(tNoZeroSSRS.GetText(i, 5));
				tLYPremSeparateDetailSchema.setPrtNo(tNoZeroSSRS.GetText(i, 6));
				tLYPremSeparateDetailSchema.setPolicyNo(tNoZeroSSRS.GetText(i, 7));
				tLYPremSeparateDetailSchema.setRiskCode(tNoZeroSSRS.GetText(i, 8));
				tLYPremSeparateDetailSchema.setManageCom(tNoZeroSSRS.GetText(i, 9));
				tLYPremSeparateDetailSchema.setPayMoney(tNoZeroSSRS.GetText(i, 10));
				tLYPremSeparateDetailSchema.setMoneyNo(tNoZeroSSRS.GetText(i, 11));
				tLYPremSeparateDetailSchema.setBusiType("01");//01-保单收费  02-保单付费
				tLYPremSeparateDetailSchema.setState("01");//01-提取
				tLYPremSeparateDetailSchema.setOtherState("06");//06-暂收退费
				tLYPremSeparateDetailSchema.setMoneyType("02");//收费
				tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
				tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
				tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
			}
		}else{
			System.out.println("暂收表退费到险种的契约数据为0！");
		}
		
		
		//暂收表不到险种的契约数据--非删除表数据
		tSQL = " select "  
				+ " TempFeeData.BusiNo||trim(RiskData.riskcode)||'TF',"
				+ " TempFeeData.TempFeeNo,TempFeeData.TempFeeType,TempFeeData.OtherNo,TempFeeData.OtherNoType,TempFeeData.PrtNo,"
				+ " TempFeeData.PolicyNo,RiskData.RiskCode,RiskData.ManageCom,RiskData.RiskPrem "
				+ " ,TempFeeData.ActugetNo "
				+ " from "
				+ " ( "
				+ " select "
				+ " trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||'TF' BusiNo,"
				+ " ljt.TempFeeNo TempFeeNo,ljt.TempFeeType TempFeeType,ljt.OtherNo OtherNo,ljt.OtherNoType OtherNoType,"
				+ " case when ljt.OtherNoType in ('4','5','11') then ljt.otherno else (select prtno from lccont where conttype = '1' and contno = ljt.otherno union select prtno from lcgrpcont where grpcontno = ljt.otherno) end PrtNo, "
				+ " case when ljt.OtherNoType in ('0','1','2','7') then ljt.otherno else (select contno from lccont where prtno = ljt.otherno and conttype = '1' union select grpcontno from lcgrpcont where prtno = ljt.otherno) end PolicyNo "
				+ ",ljtf.actugetno ActugetNo "
				+ " from ljtempfee ljt "
				+ " inner join ljagettempfee ljtf on ljt.tempfeeno = ljtf.tempfeeno "
				+ " where 1=1 "
				+ " and ljt.riskcode = '000000' "
				+ " and ljt.tempfeetype in (select code from ldcode where codetype = 'ygzqyttype') "
				+ " and trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||trim(ljt.RiskCode)||'TF' not in (select busino from LYPremSeparateDetail) " 
				+ tWhereSQL
				+ " ) as TempFeeData "
				+ " inner join "
				+ " (select lcp.contno PolicyNo,lcp.managecom ManageCom,lcp.riskcode RiskCode,sum(prem) RiskPrem "
				+ " from lcpol lcp "
				+ " where lcp.conttype = '1' "
				+ " and lcp.Makedate >= '"+tDealDate+"' "
				+ tWherePSQL
				+ " group by lcp.contno,lcp.managecom,lcp.riskcode) as RiskData "
				+ " on TempFeeData.PolicyNo = RiskData.PolicyNo "
				+ " and not exists (select 1 from lcgrpcont where prtno = TempFeeData.prtno and coinsuranceflag ='1' union select 1 from lobgrpcont where prtno = TempFeeData.prtno and coinsuranceflag ='1')"
				+ " and TempFeeData.BusiNo||trim(RiskData.riskcode)||'TF' not in (select busino from LYPremSeparateDetail) "
				+ "";
		SSRS tZeroSSRS = tExeSQL.execSQL(tSQL);
		if(tZeroSSRS != null && tZeroSSRS.getMaxRow() > 0){
			System.out.println("暂收表不到险种的契约数据==="+tZeroSSRS.getMaxRow());
				for (int i = 1; i <= tZeroSSRS.getMaxRow(); i++) {
					LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
					tLYPremSeparateDetailSchema.setBusiNo(tZeroSSRS.GetText(i, 1));
					tLYPremSeparateDetailSchema.setTempFeeNo(tZeroSSRS.GetText(i, 2));
					tLYPremSeparateDetailSchema.setTempFeeType(tZeroSSRS.GetText(i, 3));
					tLYPremSeparateDetailSchema.setOtherNo(tZeroSSRS.GetText(i, 4));
					tLYPremSeparateDetailSchema.setOtherNoType(tZeroSSRS.GetText(i, 5));
					tLYPremSeparateDetailSchema.setPrtNo(tZeroSSRS.GetText(i, 6));
					tLYPremSeparateDetailSchema.setPolicyNo(tZeroSSRS.GetText(i, 7));
					tLYPremSeparateDetailSchema.setRiskCode(tZeroSSRS.GetText(i, 8));
					tLYPremSeparateDetailSchema.setManageCom(tZeroSSRS.GetText(i, 9));
					tLYPremSeparateDetailSchema.setPayMoney(tZeroSSRS.GetText(i, 10));
					tLYPremSeparateDetailSchema.setMoneyNo(tZeroSSRS.GetText(i, 11));
					tLYPremSeparateDetailSchema.setBusiType("01");
					tLYPremSeparateDetailSchema.setState("01");
					tLYPremSeparateDetailSchema.setOtherState("06");
					tLYPremSeparateDetailSchema.setMoneyType("02");
					tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
					tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
					tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
					tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
					tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
				}
			}else{
				System.out.println("暂收表退费不到险种非删除表的契约数据为0！");
			}
		
		
		//期缴--需险种表数据。----个单非删除表数据
		tSQL = " select "  
				+ " 'QJ'||TempFeeData.BusiNo||trim(RiskData.riskcode)||'TF',"
				+ " TempFeeData.TempFeeNo,TempFeeData.TempFeeType,TempFeeData.OtherNo,TempFeeData.OtherNoType,TempFeeData.PrtNo,"
				+ " TempFeeData.PolicyNo,RiskData.RiskCode,RiskData.ManageCom,RiskData.RiskPrem,"
				+ " RiskData.PayIntv,timestampdiff (64, char(timestamp(RiskData.PayendDate) - timestamp(RiskData.Cvalidate)))  "
				+ " ,TempFeeData.ActugetNo "
				+ " from "
				+ " ( "
				+ " select "
				+ " trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||'TF' BusiNo,"
				+ " ljt.TempFeeNo TempFeeNo,ljt.TempFeeType TempFeeType,ljt.OtherNo OtherNo,ljt.OtherNoType OtherNoType,"
				+ " case when ljt.OtherNoType in ('4','5','11') then ljt.otherno else (select prtno from lccont where conttype = '1' and contno = ljt.otherno union select prtno from lcgrpcont where grpcontno = ljt.otherno) end PrtNo, "
				+ " case when ljt.OtherNoType in ('0','1','2','7') then ljt.otherno else (select contno from lccont where prtno = ljt.otherno and conttype = '1' union select grpcontno from lcgrpcont where prtno = ljt.otherno) end PolicyNo "
				+ ",ljtf.actugetno ActugetNo "
				+ " from ljtempfee ljt "
				+ " inner join ljagettempfee ljtf on ljt.tempfeeno = ljtf.tempfeeno "
				+ " where 1=1 "
				+ " and ljt.tempfeetype in (select code from ldcode where codetype = 'ygzqyttype') "
				+ " and 'QJ'||trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||trim(ljt.RiskCode)||'TF' not in (select busino from LYPremSeparateDetail) " 
				+ tWhereSQL
				+ " group by ljt.TempFeeNo,ljt.TempFeeType,ljt.OtherNo,ljt.OtherNoType,ljtf.actugetno "
				+ " ) as TempFeeData "
				+ " inner join "
				+ " (select lcp.contno PolicyNo,lcp.managecom ManageCom,lcp.riskcode RiskCode,sum(prem) RiskPrem, "
				+ " lcp.payintv PayIntv,lcp.cvalidate Cvalidate,lcp.payenddate PayendDate "
				+ " from lcpol lcp "
				+ " where lcp.conttype = '1' "
				+ " and payintv != 0 and payintv != -1 "
				+ tWherePSQL
				+ " and lcp.Makedate >= '"+tDealDate+"' "
				+ " group by lcp.contno,lcp.managecom,lcp.riskcode,lcp.payintv,lcp.cvalidate,lcp.payenddate "
				+ " ) as RiskData "
				+ " on TempFeeData.PolicyNo = RiskData.PolicyNo "
				+ " and not exists (select 1 from lcgrpcont where prtno = TempFeeData.prtno and coinsuranceflag ='1' union select 1 from lobgrpcont where prtno = TempFeeData.prtno and coinsuranceflag ='1')"
				+ " and 'QJ'||TempFeeData.BusiNo||trim(RiskData.riskcode)||'TF' not in (select busino from LYPremSeparateDetail) "
				+ " order by 'QJ'||TempFeeData.BusiNo||trim(RiskData.riskcode)||'TF' ";
		SSRS tQJSSRS = tExeSQL.execSQL(tSQL);
			if(tQJSSRS != null && tQJSSRS.getMaxRow() > 0){
				LYPremSeparateDetailSet tempLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
				for (int i = 1; i <= tQJSSRS.getMaxRow(); i++) {
					LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
					tLYPremSeparateDetailSchema.setBusiNo(tQJSSRS.GetText(i, 1));
					tLYPremSeparateDetailSchema.setTempFeeNo(tQJSSRS.GetText(i, 2));
					tLYPremSeparateDetailSchema.setTempFeeType(tQJSSRS.GetText(i, 3));
					tLYPremSeparateDetailSchema.setOtherNo(tQJSSRS.GetText(i, 4));
					tLYPremSeparateDetailSchema.setOtherNoType(tQJSSRS.GetText(i, 5));
					tLYPremSeparateDetailSchema.setPrtNo(tQJSSRS.GetText(i, 6));
					tLYPremSeparateDetailSchema.setPolicyNo(tQJSSRS.GetText(i, 7));
					tLYPremSeparateDetailSchema.setRiskCode(tQJSSRS.GetText(i, 8));
					tLYPremSeparateDetailSchema.setBusiType("01");
					tLYPremSeparateDetailSchema.setManageCom(tQJSSRS.GetText(i, 9));
					String tPayMoney = getQJRiskSumPrem(tQJSSRS.GetText(i, 10),tQJSSRS.GetText(i, 11),tQJSSRS.GetText(i, 12));
					if("".equals(tPayMoney) || tPayMoney == null){
						System.out.println("个单期缴保费获取险种总保费失败！");
						continue;
					}
					tLYPremSeparateDetailSchema.setPayMoney(tPayMoney);
					tLYPremSeparateDetailSchema.setMoneyNo(tQJSSRS.GetText(i, 13));
					tLYPremSeparateDetailSchema.setState("01");
					tLYPremSeparateDetailSchema.setOtherState("07");//07-暂收退费期缴
					tLYPremSeparateDetailSchema.setMoneyType("02");
					tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
					tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
					tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
					tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
					tempLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
				}
					
					//可能payendyearflag为A，且为多被保人情况，被保人年龄不一致，导致费用没有合并的处理
					if(tempLYPremSeparateDetailSet != null && tempLYPremSeparateDetailSet.size()>0 ){
						for (int i = 1; i <= tempLYPremSeparateDetailSet.size(); i++) {
							String tBusiNo = tempLYPremSeparateDetailSet.get(i).getBusiNo();
							for (int j = i+1; j <= tempLYPremSeparateDetailSet.size(); j++) {
								if(tBusiNo.equals(tempLYPremSeparateDetailSet.get(j).getBusiNo())){
									String tPayMoney1 =  tempLYPremSeparateDetailSet.get(i).getPayMoney();
									String tPayMoney2 =  tempLYPremSeparateDetailSet.get(j).getPayMoney();
									String tempPayMoney = String.valueOf(Double.parseDouble(tPayMoney1) + Double.parseDouble(tPayMoney2));
									tempLYPremSeparateDetailSet.get(i).setPayMoney(tempPayMoney);
									tempLYPremSeparateDetailSet.remove(tempLYPremSeparateDetailSet.get(j));
									i--;
									break;
								}
							}
						}
						System.out.println("个单期缴保费总保费数据==="+tempLYPremSeparateDetailSet.size());
						tLYPremSeparateDetailSet.add(tempLYPremSeparateDetailSet);
					}
				}else{
					System.out.println("暂收表非删除表数据期缴保费总保费数据为0！");
				}
			
				
				//期缴--需险种表数据。----团单非删除表数据
				tSQL = " select "  
						+ " 'QJ'||TempFeeData.BusiNo||trim(RiskData.riskcode)||'TF',"
						+ " TempFeeData.TempFeeNo,TempFeeData.TempFeeType,TempFeeData.OtherNo,TempFeeData.OtherNoType,TempFeeData.PrtNo,"
						+ " TempFeeData.PolicyNo,RiskData.RiskCode,RiskData.ManageCom,RiskData.RiskPrem,"
						+ " RiskData.PayIntv,timestampdiff (64, char(timestamp(RiskData.PayendDate) - timestamp(RiskData.Cvalidate))) "
						+ " ,TempFeeData.ActugetNo "
						+ " from "
						+ " ( "
						+ " select "
						+ " trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||'TF' BusiNo,"
						+ " ljt.TempFeeNo TempFeeNo,ljt.TempFeeType TempFeeType,ljt.OtherNo OtherNo,ljt.OtherNoType OtherNoType,"
						+ " case when ljt.OtherNoType in ('4','5','11') then ljt.otherno else (select prtno from lccont where conttype = '1' and contno = ljt.otherno union select prtno from lcgrpcont where grpcontno = ljt.otherno) end PrtNo, "
						+ " case when ljt.OtherNoType in ('0','1','2','7') then ljt.otherno else (select contno from lccont where prtno = ljt.otherno and conttype = '1' union select grpcontno from lcgrpcont where prtno = ljt.otherno) end PolicyNo "
						+ ",ljtf.actugetno ActugetNo "
						+ " from ljtempfee ljt "
						+ " inner join ljagettempfee ljtf on ljt.tempfeeno = ljtf.tempfeeno "
						+ " where 1=1 "
						+ " and ljt.tempfeetype in (select code from ldcode where codetype = 'ygzqyttype') "
						+ " and 'QJ'||trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||trim(ljt.RiskCode)||'TF' not in (select busino from LYPremSeparateDetail) " 
						+ tWhereSQL
						+ " group by ljt.TempFeeNo,ljt.TempFeeType,ljt.OtherNo,ljt.OtherNoType,ljtf.actugetno "
						+ " ) as TempFeeData "
						+ " inner join "
						+ " (select lcp.grpcontno PolicyNo,lcp.managecom ManageCom,lcp.riskcode RiskCode,sum(prem) RiskPrem, "
						+ " lcp.payintv PayIntv,lcp.cvalidate Cvalidate,lcp.payenddate PayendDate "
						+ " from lcpol lcp "
						+ " where lcp.conttype = '2' "
						+ " and payintv != 0 and payintv != -1 "
						+ tWherePSQL
						+ " and lcp.Makedate >= '"+tDealDate+"' "
						+ " group by lcp.grpcontno,lcp.managecom,lcp.riskcode,lcp.payintv,lcp.cvalidate,lcp.payenddate "
						+ " ) as RiskData "
						+ " on TempFeeData.PolicyNo = RiskData.PolicyNo "
						+ " and not exists (select 1 from lcgrpcont where prtno = TempFeeData.prtno and coinsuranceflag ='1' union select 1 from lobgrpcont where prtno = TempFeeData.prtno and coinsuranceflag ='1')"
						+ " and 'QJ'||TempFeeData.BusiNo||trim(RiskData.riskcode)||'TF' not in (select busino from LYPremSeparateDetail) "
						+ " order by 'QJ'||TempFeeData.BusiNo||trim(RiskData.riskcode)||'TF' ";
				SSRS tQJTSSRS = tExeSQL.execSQL(tSQL);
				if(tQJTSSRS != null && tQJTSSRS.getMaxRow() > 0){
					LYPremSeparateDetailSet tempLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
					for (int i = 1; i <= tQJTSSRS.getMaxRow(); i++) {
						LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
						tLYPremSeparateDetailSchema.setBusiNo(tQJTSSRS.GetText(i, 1));
						tLYPremSeparateDetailSchema.setTempFeeNo(tQJTSSRS.GetText(i, 2));
						tLYPremSeparateDetailSchema.setTempFeeType(tQJTSSRS.GetText(i, 3));
						tLYPremSeparateDetailSchema.setOtherNo(tQJTSSRS.GetText(i, 4));
						tLYPremSeparateDetailSchema.setOtherNoType(tQJTSSRS.GetText(i, 5));
						tLYPremSeparateDetailSchema.setPrtNo(tQJTSSRS.GetText(i, 6));
						tLYPremSeparateDetailSchema.setPolicyNo(tQJTSSRS.GetText(i, 7));
						tLYPremSeparateDetailSchema.setRiskCode(tQJTSSRS.GetText(i, 8));
						tLYPremSeparateDetailSchema.setBusiType("01");
						tLYPremSeparateDetailSchema.setManageCom(tQJTSSRS.GetText(i, 9));
						String tPayMoney = getQJRiskSumPrem(tQJTSSRS.GetText(i, 10),tQJTSSRS.GetText(i, 11),tQJTSSRS.GetText(i, 12));
						if("".equals(tPayMoney) || tPayMoney == null){
							System.out.println("团单期缴保费获取险种总保费失败！");
							continue;
						}
						tLYPremSeparateDetailSchema.setPayMoney(tPayMoney);
						tLYPremSeparateDetailSchema.setMoneyNo(tQJTSSRS.GetText(i, 13));
						tLYPremSeparateDetailSchema.setState("01");
						tLYPremSeparateDetailSchema.setOtherState("07");//07-暂交退费期缴
						tLYPremSeparateDetailSchema.setMoneyType("02");
						tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
						tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
						tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
						tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
						tempLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
					}
					//可能payendyearflag为A，且为多被保人情况，被保人年龄不一致，导致费用没有合并的处理
					if(tempLYPremSeparateDetailSet != null && tempLYPremSeparateDetailSet.size()>0 ){
						for (int i = 1; i <= tempLYPremSeparateDetailSet.size(); i++) {
							String tBusiNo = tempLYPremSeparateDetailSet.get(i).getBusiNo();
							for (int j = i+1; j <= tempLYPremSeparateDetailSet.size(); j++) {
								if(tBusiNo.equals(tempLYPremSeparateDetailSet.get(j).getBusiNo())){
									String tPayMoney1 =  tempLYPremSeparateDetailSet.get(i).getPayMoney();
									String tPayMoney2 =  tempLYPremSeparateDetailSet.get(j).getPayMoney();
									String tempPayMoney = String.valueOf(Double.parseDouble(tPayMoney1) + Double.parseDouble(tPayMoney2));
									tempLYPremSeparateDetailSet.get(i).setPayMoney(tempPayMoney);
									tempLYPremSeparateDetailSet.remove(tempLYPremSeparateDetailSet.get(j));
									i--;
									break;
								}
							}
						}
						System.out.println("团单暂收表期缴保费总保费数据==="+tempLYPremSeparateDetailSet.size());
						tLYPremSeparateDetailSet.add(tempLYPremSeparateDetailSet);
					}
				}else{
					System.out.println("团单暂收表非删除表期缴保费总保费数据为0！");
				}
				
				
				//从暂收表生成的约定缴费的总保费数据
				tSQL = " select * " 
						+ " from ( "
						+ " select "
		                + " 'YD'||trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||trim(ljt.RiskCode)||'TF',"
		                + " ljt.TempFeeNo,ljt.TempFeeType,ljt.OtherNo,ljt.OtherNoType,"
						+ " case when ljt.OtherNoType in ('4','5','11') then ljt.otherno else (select prtno from lccont where conttype = '1' and contno = ljt.otherno "
						+ " union select prtno from lcgrpcont where grpcontno = ljt.otherno "
						+ "	union select prtno from lobcont where conttype = '1' and contno = ljt.otherno "
						+ " union select prtno from lobgrpcont where grpcontno = ljt.otherno ) end PrtNo,"
						+ " case when ljt.OtherNoType in ('0','1','2','7') then ljt.otherno else (select contno from lccont where conttype = '1' and prtno = ljt.otherno "
						+ " union select grpcontno from lcgrpcont where prtno = ljt.otherno"
						+ " union select contno from lobcont where conttype='1' and prtno = ljt.otherno"
						+ " union select grpcontno from lobgrpcont where prtno = ljt.otherno ) end PolicyNo,"
						+ " ljt.riskcode,ljt.managecom,ljt.paymoney "
						+ ",ljtf.actugetno ActugetNo "
						+ " from ljtempfee ljt "
						+ " inner join ljagettempfee ljtf on ljt.tempfeeno = ljtf.tempfeeno and ljt.riskcode = ljtf.riskcode"
						+ " where ljt.riskcode != '000000' "
						+ " and ljt.tempfeetype in (select code from ldcode where codetype = 'ygzqyttype') "
						+ " and 'YD'||trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||trim(ljt.RiskCode)||'TF' not in (select busino from LYPremSeparateDetail) "
						+ tWhereSQL
						+ " ) TempFeeData "
						+ " where 1=1 "
						+ " and not exists (select 1 from lcgrpcont where prtno = TempFeeData.prtno and coinsuranceflag ='1' union select 1 from lobgrpcont where prtno = TempFeeData.prtno and coinsuranceflag ='1')"
						+ tWhereTSQL
						+ " and exists ( "
//						+ " select 1 from lcgrpcont lgc where lgc.GrpContNo = TempFeeData.PolicyNo and payintv = '-1' and (lgc.CvaliDate >= '"+tDealDate+"' or lgc.SignDate >= '"+tDealDate+"') "
						+ " select 1 from lcgrpcont lgc where lgc.GrpContNo = TempFeeData.PolicyNo and payintv = '-1' "
						+ " union select 1 from lobgrpcont lgc where lgc.GrpContNo = TempFeeData.PolicyNo and payintv = '-1' "
						+ " ) "
						+ " ";
				SSRS tYDSSRS = tExeSQL.execSQL(tSQL);
				if(tYDSSRS != null && tYDSSRS.getMaxRow() > 0){
					for (int i = 1; i <= tYDSSRS.getMaxRow(); i++) {
						LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
						tLYPremSeparateDetailSchema.setBusiNo(tYDSSRS.GetText(i, 1));
						tLYPremSeparateDetailSchema.setTempFeeNo(tYDSSRS.GetText(i, 2));
						tLYPremSeparateDetailSchema.setTempFeeType(tYDSSRS.GetText(i, 3));
						tLYPremSeparateDetailSchema.setOtherNo(tYDSSRS.GetText(i, 4));
						tLYPremSeparateDetailSchema.setOtherNoType(tYDSSRS.GetText(i, 5));
						tLYPremSeparateDetailSchema.setPrtNo(tYDSSRS.GetText(i, 6));
						tLYPremSeparateDetailSchema.setPolicyNo(tYDSSRS.GetText(i, 7));
						tLYPremSeparateDetailSchema.setRiskCode(tYDSSRS.GetText(i, 8));
						tLYPremSeparateDetailSchema.setBusiType("01");
						tLYPremSeparateDetailSchema.setManageCom(tYDSSRS.GetText(i, 9));
						String tPayMoney = getAllRiskSumPrem(tYDSSRS.GetText(i, 6),tYDSSRS.GetText(i, 8));
						if("".equals(tPayMoney) || tPayMoney == null){
							System.out.println("约定缴费获取险种总保费失败！");
							continue;
						}
						tLYPremSeparateDetailSchema.setPayMoney(tPayMoney);
						tLYPremSeparateDetailSchema.setMoneyNo(tYDSSRS.GetText(i, 11));
						tLYPremSeparateDetailSchema.setState("01");
						tLYPremSeparateDetailSchema.setOtherState("08");//暂收退费不定期交
						tLYPremSeparateDetailSchema.setMoneyType("02");
						tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
						tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
						tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
						tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
						tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
					}
				}else{
					System.out.println("暂收表约定缴费的契约数据为0！");
				}
				
				/**
				 * add by 2016-06-23
				 * 增加暂收表不到险种的契约数据--删除表数据
				 * 
				 */
				tSQL = " select "  
						+ " TempFeeData.BusiNo||trim(RiskData.riskcode)||'TF',"
						+ " TempFeeData.TempFeeNo,TempFeeData.TempFeeType,TempFeeData.OtherNo,TempFeeData.OtherNoType,TempFeeData.PrtNo,"
						+ " TempFeeData.PolicyNo,RiskData.RiskCode,RiskData.ManageCom,RiskData.RiskPrem "
						+ " ,TempFeeData.ActugetNo "
						+ " from "
						+ " ( "
						+ " select "
						+ " trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||'TF' BusiNo,"
						+ " ljt.TempFeeNo TempFeeNo,ljt.TempFeeType TempFeeType,ljt.OtherNo OtherNo,ljt.OtherNoType OtherNoType,"
						+ " case when ljt.OtherNoType in ('4','5','11') then ljt.otherno else (select prtno from lobcont where conttype = '1' and contno = ljt.otherno "
						+ " union select prtno from lobgrpcont where grpcontno = ljt.otherno) end PrtNo, "
						+ " case when ljt.OtherNoType in ('0','1','2','7') then ljt.otherno else (select contno from lobcont where prtno = ljt.otherno and conttype = '1' "
						+ " union select grpcontno from lobgrpcont where prtno = ljt.otherno) end PolicyNo "
						+ ",ljtf.actugetno ActugetNo "
						+ " from ljtempfee ljt "
						+ " inner join ljagettempfee ljtf on ljt.tempfeeno = ljtf.tempfeeno "
						+ " where 1=1 "
						+ " and ljt.riskcode = '000000' "
						+ " and ljt.tempfeetype in (select code from ldcode where codetype = 'ygzqyttype') "
						+ " and trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||trim(ljt.RiskCode)||'TF' not in (select busino from LYPremSeparateDetail) " 
						+ tWhereSQL
						+ " ) as TempFeeData "
						+ " inner join "
						+ " (select lobp.contno PolicyNo,lobp.managecom ManageCom,lobp.riskcode RiskCode,sum(prem) RiskPrem "
						+ " from lobpol lobp "
						+ " where lobp.conttype = '1' "
						+ " and lobp.Makedate >= '"+tDealDate+"' "
						+ tWherePSQL
						+ " group by lobp.contno,lobp.managecom,lobp.riskcode) as RiskData "
						+ " on TempFeeData.PolicyNo = RiskData.PolicyNo "
						+ " and not exists (select 1 from lcgrpcont where prtno = TempFeeData.prtno and coinsuranceflag ='1' union select 1 from lobgrpcont where prtno = TempFeeData.prtno and coinsuranceflag ='1')"
						+ " and TempFeeData.BusiNo||trim(RiskData.riskcode)||'TF' not in (select busino from LYPremSeparateDetail) "
						+ "";
				SSRS tOBZeroSSRS = tExeSQL.execSQL(tSQL);
				if(tOBZeroSSRS != null && tOBZeroSSRS.getMaxRow() > 0){
					System.out.println("暂收表不到险种的契约数据==="+tOBZeroSSRS.getMaxRow());
						for (int i = 1; i <= tOBZeroSSRS.getMaxRow(); i++) {
							LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
							tLYPremSeparateDetailSchema.setBusiNo(tOBZeroSSRS.GetText(i, 1));
							tLYPremSeparateDetailSchema.setTempFeeNo(tOBZeroSSRS.GetText(i, 2));
							tLYPremSeparateDetailSchema.setTempFeeType(tOBZeroSSRS.GetText(i, 3));
							tLYPremSeparateDetailSchema.setOtherNo(tOBZeroSSRS.GetText(i, 4));
							tLYPremSeparateDetailSchema.setOtherNoType(tOBZeroSSRS.GetText(i, 5));
							tLYPremSeparateDetailSchema.setPrtNo(tOBZeroSSRS.GetText(i, 6));
							tLYPremSeparateDetailSchema.setPolicyNo(tOBZeroSSRS.GetText(i, 7));
							tLYPremSeparateDetailSchema.setRiskCode(tOBZeroSSRS.GetText(i, 8));
							tLYPremSeparateDetailSchema.setManageCom(tOBZeroSSRS.GetText(i, 9));
							tLYPremSeparateDetailSchema.setPayMoney(tOBZeroSSRS.GetText(i, 10));
							tLYPremSeparateDetailSchema.setMoneyNo(tOBZeroSSRS.GetText(i, 11));
							tLYPremSeparateDetailSchema.setBusiType("01");
							tLYPremSeparateDetailSchema.setState("01");
							tLYPremSeparateDetailSchema.setOtherState("06");
							tLYPremSeparateDetailSchema.setMoneyType("02");
							tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
							tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
							tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
							tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
							tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
						}
					}else{
						System.out.println("暂收表退费不到险种删除表的契约数据为0！");
					}
		
				/**
				 * add by 2016-06-23
				 * 增加 期缴--需险种表数据。----个单删除表数据
				 */
				tSQL = " select "  
						+ " 'QJ'||TempFeeData.BusiNo||trim(RiskData.riskcode)||'TF',"
						+ " TempFeeData.TempFeeNo,TempFeeData.TempFeeType,TempFeeData.OtherNo,TempFeeData.OtherNoType,TempFeeData.PrtNo,"
						+ " TempFeeData.PolicyNo,RiskData.RiskCode,RiskData.ManageCom,RiskData.RiskPrem,"
						+ " RiskData.PayIntv,timestampdiff (64, char(timestamp(RiskData.PayendDate) - timestamp(RiskData.Cvalidate)))  "
						+ " ,TempFeeData.ActugetNo "
						+ " from "
						+ " ( "
						+ " select "
						+ " trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||'TF' BusiNo,"
						+ " ljt.TempFeeNo TempFeeNo,ljt.TempFeeType TempFeeType,ljt.OtherNo OtherNo,ljt.OtherNoType OtherNoType,"
						+ " case when ljt.OtherNoType in ('4','5','11') then ljt.otherno else (select prtno from lobcont where conttype = '1' and contno = ljt.otherno "
						+ " union select prtno from lobgrpcont where grpcontno = ljt.otherno) end PrtNo, "
						+ " case when ljt.OtherNoType in ('0','1','2','7') then ljt.otherno else (select contno from lobcont where prtno = ljt.otherno and conttype = '1' "
						+ " union select grpcontno from lobgrpcont where prtno = ljt.otherno) end PolicyNo "
						+ ",ljtf.actugetno ActugetNo "
						+ " from ljtempfee ljt "
						+ " inner join ljagettempfee ljtf on ljt.tempfeeno = ljtf.tempfeeno "
						+ " where 1=1 "
						+ " and ljt.tempfeetype in (select code from ldcode where codetype = 'ygzqyttype') "
						+ " and 'QJ'||trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||trim(ljt.RiskCode)||'TF' not in (select busino from LYPremSeparateDetail) " 
						+ tWhereSQL
						+ " group by ljt.TempFeeNo,ljt.TempFeeType,ljt.OtherNo,ljt.OtherNoType,ljtf.actugetno "
						+ " ) as TempFeeData "
						+ " inner join "
						+ " (select lobp.contno PolicyNo,lobp.managecom ManageCom,lobp.riskcode RiskCode,sum(prem) RiskPrem, "
						+ " lobp.payintv PayIntv,lobp.cvalidate Cvalidate,lobp.payenddate PayendDate "
						+ " from lobpol lobp "
						+ " where lobp.conttype = '1' "
						+ " and payintv != 0 and payintv != -1 "
						+ tWherePSQL
						+ " and lobp.Makedate >= '"+tDealDate+"' "
						+ " group by lobp.contno,lobp.managecom,lobp.riskcode,lobp.payintv,lobp.cvalidate,lobp.payenddate "
						+ " ) as RiskData "
						+ " on TempFeeData.PolicyNo = RiskData.PolicyNo "
						+ " and not exists (select 1 from lcgrpcont where prtno = TempFeeData.prtno and coinsuranceflag ='1' union select 1 from lobgrpcont where prtno = TempFeeData.prtno and coinsuranceflag ='1')"
						+ " and 'QJ'||TempFeeData.BusiNo||trim(RiskData.riskcode)||'TF' not in (select busino from LYPremSeparateDetail) "
						+ " order by 'QJ'||TempFeeData.BusiNo||trim(RiskData.riskcode)||'TF' ";
				SSRS tQJOBSSRS = tExeSQL.execSQL(tSQL);
					if(tQJOBSSRS != null && tQJOBSSRS.getMaxRow() > 0){
						LYPremSeparateDetailSet tempLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
						for (int i = 1; i <= tQJOBSSRS.getMaxRow(); i++) {
							LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
							tLYPremSeparateDetailSchema.setBusiNo(tQJOBSSRS.GetText(i, 1));
							tLYPremSeparateDetailSchema.setTempFeeNo(tQJOBSSRS.GetText(i, 2));
							tLYPremSeparateDetailSchema.setTempFeeType(tQJOBSSRS.GetText(i, 3));
							tLYPremSeparateDetailSchema.setOtherNo(tQJOBSSRS.GetText(i, 4));
							tLYPremSeparateDetailSchema.setOtherNoType(tQJOBSSRS.GetText(i, 5));
							tLYPremSeparateDetailSchema.setPrtNo(tQJOBSSRS.GetText(i, 6));
							tLYPremSeparateDetailSchema.setPolicyNo(tQJOBSSRS.GetText(i, 7));
							tLYPremSeparateDetailSchema.setRiskCode(tQJOBSSRS.GetText(i, 8));
							tLYPremSeparateDetailSchema.setBusiType("01");
							tLYPremSeparateDetailSchema.setManageCom(tQJOBSSRS.GetText(i, 9));
							String tPayMoney = getQJRiskSumPrem(tQJOBSSRS.GetText(i, 10),tQJOBSSRS.GetText(i, 11),tQJOBSSRS.GetText(i, 12));
							if("".equals(tPayMoney) || tPayMoney == null){
								System.out.println("个单期缴保费获取险种总保费失败！");
								continue;
							}
							tLYPremSeparateDetailSchema.setPayMoney(tPayMoney);
							tLYPremSeparateDetailSchema.setMoneyNo(tQJOBSSRS.GetText(i, 13));
							tLYPremSeparateDetailSchema.setState("01");
							tLYPremSeparateDetailSchema.setOtherState("07");//07-暂收退费期缴
							tLYPremSeparateDetailSchema.setMoneyType("02");
							tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
							tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
							tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
							tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
							tempLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
						}
							
							//可能payendyearflag为A，且为多被保人情况，被保人年龄不一致，导致费用没有合并的处理
							if(tempLYPremSeparateDetailSet != null && tempLYPremSeparateDetailSet.size()>0 ){
								for (int i = 1; i <= tempLYPremSeparateDetailSet.size(); i++) {
									String tBusiNo = tempLYPremSeparateDetailSet.get(i).getBusiNo();
									for (int j = i+1; j <= tempLYPremSeparateDetailSet.size(); j++) {
										if(tBusiNo.equals(tempLYPremSeparateDetailSet.get(j).getBusiNo())){
											String tPayMoney1 =  tempLYPremSeparateDetailSet.get(i).getPayMoney();
											String tPayMoney2 =  tempLYPremSeparateDetailSet.get(j).getPayMoney();
											String tempPayMoney = String.valueOf(Double.parseDouble(tPayMoney1) + Double.parseDouble(tPayMoney2));
											tempLYPremSeparateDetailSet.get(i).setPayMoney(tempPayMoney);
											tempLYPremSeparateDetailSet.remove(tempLYPremSeparateDetailSet.get(j));
											i--;
											break;
										}
									}
								}
								System.out.println("个单期缴保费总保费数据==="+tempLYPremSeparateDetailSet.size());
								tLYPremSeparateDetailSet.add(tempLYPremSeparateDetailSet);
							}
						}else{
							System.out.println("暂收表删除表数据期缴保费总保费数据为0！");
						}
					
					
					/**
					 * add by 2016-06-23
					 * 增加期缴--需险种表数据。----团单删除表数据
					 */
					tSQL = " select "  
							+ " 'QJ'||TempFeeData.BusiNo||trim(RiskData.riskcode)||'TF',"
							+ " TempFeeData.TempFeeNo,TempFeeData.TempFeeType,TempFeeData.OtherNo,TempFeeData.OtherNoType,TempFeeData.PrtNo,"
							+ " TempFeeData.PolicyNo,RiskData.RiskCode,RiskData.ManageCom,RiskData.RiskPrem,"
							+ " RiskData.PayIntv,timestampdiff (64, char(timestamp(RiskData.PayendDate) - timestamp(RiskData.Cvalidate))) "
							+ " ,TempFeeData.ActugetNo "
							+ " from "
							+ " ( "
							+ " select "
							+ " trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||'TF' BusiNo,"
							+ " ljt.TempFeeNo TempFeeNo,ljt.TempFeeType TempFeeType,ljt.OtherNo OtherNo,ljt.OtherNoType OtherNoType,"
							+ " case when ljt.OtherNoType in ('4','5','11') then ljt.otherno else (select prtno from lobcont where conttype = '1' and contno = ljt.otherno "
							+ " union select prtno from lobgrpcont where grpcontno = ljt.otherno) end PrtNo, "
							+ " case when ljt.OtherNoType in ('0','1','2','7') then ljt.otherno else (select contno from lobcont where prtno = ljt.otherno and conttype = '1' "
							+ " union select grpcontno from lobgrpcont where prtno = ljt.otherno) end PolicyNo "
							+ ",ljtf.actugetno ActugetNo "
							+ " from ljtempfee ljt "
							+ " inner join ljagettempfee ljtf on ljt.tempfeeno = ljtf.tempfeeno "
							+ " where 1=1 "
							+ " and ljt.tempfeetype in (select code from ldcode where codetype = 'ygzqyttype') "
							+ " and 'QJ'||trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||trim(ljt.RiskCode)||'TF' not in (select busino from LYPremSeparateDetail) " 
							+ tWhereSQL
							+ " group by ljt.TempFeeNo,ljt.TempFeeType,ljt.OtherNo,ljt.OtherNoType,ljtf.actugetno "
							+ " ) as TempFeeData "
							+ " inner join "
							+ " (select lobp.grpcontno PolicyNo,lobp.managecom ManageCom,lobp.riskcode RiskCode,sum(prem) RiskPrem, "
							+ " lobp.payintv PayIntv,lobp.cvalidate Cvalidate,lobp.payenddate PayendDate "
							+ " from lcpol lobp "
							+ " where lobp.conttype = '2' "
							+ " and payintv != 0 and payintv != -1 "
							+ tWherePSQL
							+ " and lobp.Makedate >= '"+tDealDate+"' "
							+ " group by lobp.grpcontno,lobp.managecom,lobp.riskcode,lobp.payintv,lobp.cvalidate,lobp.payenddate "
							+ " ) as RiskData "
							+ " on TempFeeData.PolicyNo = RiskData.PolicyNo "
							+ " and not exists (select 1 from lcgrpcont where prtno = TempFeeData.prtno and coinsuranceflag ='1' union select 1 from lobgrpcont where prtno = TempFeeData.prtno and coinsuranceflag ='1')"
							+ " and 'QJ'||TempFeeData.BusiNo||trim(RiskData.riskcode)||'TF' not in (select busino from LYPremSeparateDetail) "
							+ " order by 'QJ'||TempFeeData.BusiNo||trim(RiskData.riskcode)||'TF' ";
					SSRS tQJOBTSSRS = tExeSQL.execSQL(tSQL);
					if(tQJOBTSSRS != null && tQJOBTSSRS.getMaxRow() > 0){
						LYPremSeparateDetailSet tempLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
						for (int i = 1; i <= tQJTSSRS.getMaxRow(); i++) {
							LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
							tLYPremSeparateDetailSchema.setBusiNo(tQJOBTSSRS.GetText(i, 1));
							tLYPremSeparateDetailSchema.setTempFeeNo(tQJOBTSSRS.GetText(i, 2));
							tLYPremSeparateDetailSchema.setTempFeeType(tQJOBTSSRS.GetText(i, 3));
							tLYPremSeparateDetailSchema.setOtherNo(tQJOBTSSRS.GetText(i, 4));
							tLYPremSeparateDetailSchema.setOtherNoType(tQJOBTSSRS.GetText(i, 5));
							tLYPremSeparateDetailSchema.setPrtNo(tQJOBTSSRS.GetText(i, 6));
							tLYPremSeparateDetailSchema.setPolicyNo(tQJOBTSSRS.GetText(i, 7));
							tLYPremSeparateDetailSchema.setRiskCode(tQJOBTSSRS.GetText(i, 8));
							tLYPremSeparateDetailSchema.setBusiType("01");
							tLYPremSeparateDetailSchema.setManageCom(tQJOBTSSRS.GetText(i, 9));
							String tPayMoney = getQJRiskSumPrem(tQJOBTSSRS.GetText(i, 10),tQJOBTSSRS.GetText(i, 11),tQJOBTSSRS.GetText(i, 12));
							if("".equals(tPayMoney) || tPayMoney == null){
								System.out.println("团单期缴保费获取险种总保费失败！");
								continue;
							}
							tLYPremSeparateDetailSchema.setPayMoney(tPayMoney);
							tLYPremSeparateDetailSchema.setMoneyNo(tQJOBTSSRS.GetText(i, 13));
							tLYPremSeparateDetailSchema.setState("01");
							tLYPremSeparateDetailSchema.setOtherState("07");//07-暂交退费期缴
							tLYPremSeparateDetailSchema.setMoneyType("02");
							tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
							tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
							tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
							tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
							tempLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
						}
						//可能payendyearflag为A，且为多被保人情况，被保人年龄不一致，导致费用没有合并的处理
						if(tempLYPremSeparateDetailSet != null && tempLYPremSeparateDetailSet.size()>0 ){
							for (int i = 1; i <= tempLYPremSeparateDetailSet.size(); i++) {
								String tBusiNo = tempLYPremSeparateDetailSet.get(i).getBusiNo();
								for (int j = i+1; j <= tempLYPremSeparateDetailSet.size(); j++) {
									if(tBusiNo.equals(tempLYPremSeparateDetailSet.get(j).getBusiNo())){
										String tPayMoney1 =  tempLYPremSeparateDetailSet.get(i).getPayMoney();
										String tPayMoney2 =  tempLYPremSeparateDetailSet.get(j).getPayMoney();
										String tempPayMoney = String.valueOf(Double.parseDouble(tPayMoney1) + Double.parseDouble(tPayMoney2));
										tempLYPremSeparateDetailSet.get(i).setPayMoney(tempPayMoney);
										tempLYPremSeparateDetailSet.remove(tempLYPremSeparateDetailSet.get(j));
										i--;
										break;
									}
								}
							}
							System.out.println("团单暂收表期缴保费总保费数据==="+tempLYPremSeparateDetailSet.size());
							tLYPremSeparateDetailSet.add(tempLYPremSeparateDetailSet);
						}
					}else{
						System.out.println("团单暂收表删除表期缴保费总保费数据为0！");
					}
				
				/**
				 * add by 2016-06-23
				 * 增加暂收退费表到险种的契约共保数据
				 */
//				tSQL  = " select * " 
//						+ " from ( "
//						+ " select "
//		                + " trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||trim(ljt.RiskCode)||'TF',"
//		                + " ljt.TempFeeNo,ljt.TempFeeType,ljt.OtherNo,ljt.OtherNoType,"
//						+ " case when ljt.OtherNoType in ('4','5','11') then ljt.otherno else ( "
//						+ "	select prtno from lcgrpcont where grpcontno = ljt.otherno "
//						+ " union select prtno from lobgrpcont where grpcontno = ljt.otherno ) end PrtNo,"
//						+ " case when ljt.OtherNoType in ('0','1','2','7') then ljt.otherno else ( "
//						+ " select grpcontno from lcgrpcont where prtno = ljt.otherno"
//						+ " union select grpcontno from lobgrpcont where prtno = ljt.otherno ) end PolicyNo,"
//						+ " ljt.riskcode,ljt.managecom,ljt.paymoney, "
//						+ " 1 - nvl((select sum(rate) from lccoinsuranceparam where prtno = ljt.otherno or grpcontno = ljt.otherno),0) as rate"
//						+ " from ljtempfee ljt "
//						+ " where ljt.riskcode != '000000' "
//						+ " and ljt.TempFeeNo in (select TempFeeNo from ljagettempfee) "
//						+ " and ljt.tempfeetype in (select code from ldcode where codetype = 'ygzqyttype') "
//						+ " and trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||trim(ljt.RiskCode)||'TF' not in (select busino from LYPremSeparateDetail) "
//						+ tWhereSQL
//						+ " ) as TempFeeData "
//						+ " where 1=1 "
//						+ " and exists (select 1 from lcgrpcont where prtno = TempFeeData.prtno and coinsuranceflag ='1' union select 1 from lobgrpcont where prtno = TempFeeData.prtno and coinsuranceflag ='1')"
//						+ tWhereTSQL+"";
//				SSRS tNoZeroGBSSRS = tExeSQL.execSQL(tSQL);
//				if(tNoZeroGBSSRS != null && tNoZeroGBSSRS.getMaxRow() > 0){
//					System.out.println("暂收表退费到险种的契约数据==="+tNoZeroGBSSRS.getMaxRow());
//					for(int i = 1; i < tNoZeroGBSSRS.getMaxRow();i++){
//						LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
//						tLYPremSeparateDetailSchema.setBusiNo(tNoZeroGBSSRS.GetText(i, 1));
//						tLYPremSeparateDetailSchema.setTempFeeNo(tNoZeroGBSSRS.GetText(i, 2));
//						tLYPremSeparateDetailSchema.setTempFeeType(tNoZeroGBSSRS.GetText(i, 3));
//						tLYPremSeparateDetailSchema.setOtherNo(tNoZeroGBSSRS.GetText(i, 4));
//						tLYPremSeparateDetailSchema.setOtherNoType(tNoZeroGBSSRS.GetText(i, 5));
//						tLYPremSeparateDetailSchema.setPrtNo(tNoZeroGBSSRS.GetText(i, 6));
//						tLYPremSeparateDetailSchema.setPolicyNo(tNoZeroGBSSRS.GetText(i, 7));
//						tLYPremSeparateDetailSchema.setRiskCode(tNoZeroGBSSRS.GetText(i, 8));
//						tLYPremSeparateDetailSchema.setManageCom(tNoZeroGBSSRS.GetText(i, 9));
//						Double gbPayMoney = Double.parseDouble(tNoZeroGBSSRS.GetText(i, 10));//共保保额
//						Double gbRate = Double.parseDouble(tNoZeroGBSSRS.GetText(i, 11));//共保比例
//						String payMoney = String.valueOf(gbPayMoney*gbRate);//共保分摊保额
//						tLYPremSeparateDetailSchema.setPayMoney(payMoney);
//						tLYPremSeparateDetailSchema.setBusiType("02");//01-保单收费
//						tLYPremSeparateDetailSchema.setState("01");//01-提取
//						tLYPremSeparateDetailSchema.setOtherState("10");//10-暂收退费共保保单
//						tLYPremSeparateDetailSchema.setMoneyType("01");//收费
//						tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
//						tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
//						tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
//						tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
//						tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
//					}
//				}else{
//					System.out.println("暂收表退费到险种共保的契约数据为0！");
//				}
				
		mMap.put(tLYPremSeparateDetailSet, SysConst.INSERT);
				
		return true ;
	}
	//准备后台处理
	public boolean prepareOutputData(){
		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception e) {
			CError tError = new CError();
			tError.moduleName="GetTFSeparateBL";
			tError.functionName="prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
		}
		return true;
	}
	
	
	//获取约定缴费各险种拆分后总保费
		private String getAllRiskSumPrem(String aPrtNo,String aRiskCode){
			String tAllRiskSumPrem = "";
			double tRiskSumPrem = 0;
			String tPGSql = "select ProposalGrpContNo from lcgrpcont where prtno = '"+aPrtNo+"' ";
			String tProposalGrpContNo = new ExeSQL().getOneValue(tPGSql);
			LCGrpPayPlanDetailSet tLCGrpPayPlanDetailSet = new LCGrpPayPlanDetailSet();
			GrpPayPlanDetailBL tGrpPayPlanDetailBL = new GrpPayPlanDetailBL();
	        VData tVData = new VData();
	    	
	    	TransferData mTransferData = new TransferData();
	    	mTransferData.setNameAndValue("ProposalGrpContNo", tProposalGrpContNo);
	    	tVData.add(mGlobalInput);
	    	tVData.add(mTransferData);
	        if(!tGrpPayPlanDetailBL.submitData(tVData, "")){
	        	CError.buildErr(this, "拆分约定缴费计划有误", tGrpPayPlanDetailBL.mErrors);
	        	System.out.println("保单印刷号："+aPrtNo+",拆分约定缴费计划有误！");
	            return tAllRiskSumPrem;
	        }
	        MMap tmpMap = tGrpPayPlanDetailBL.getMMMap();
	        tLCGrpPayPlanDetailSet = (LCGrpPayPlanDetailSet)tmpMap.getObjectByObjectName("LCGrpPayPlanDetailSet", 0);
	        if(tLCGrpPayPlanDetailSet == null || tLCGrpPayPlanDetailSet.size() == 0){
	        	System.out.println("保单印刷号："+aPrtNo+",约定缴费拆分失败！");
	        	CError.buildErr(this, "保单印刷号："+aPrtNo+",约定缴费拆分失败！", tGrpPayPlanDetailBL.mErrors);
	        	return tAllRiskSumPrem;
	        }
	        for(int j = 1;j<=tLCGrpPayPlanDetailSet.size();j++){
	        	if(aRiskCode.equals(tLCGrpPayPlanDetailSet.get(j).getRiskCode())){
	        		tRiskSumPrem = tRiskSumPrem +  tLCGrpPayPlanDetailSet.get(j).getPrem();
	        	}
	        }
			return tRiskSumPrem+"";
		}
		
		//获取期缴保费总保费
			private String getQJRiskSumPrem(String aPrem,String aPayIntv,String tDifMonth){
				String tAllRiskSumPrem = "";
				int tPayTimes = 1;
				int tPayIntv = Integer.parseInt(aPayIntv);
				double tPrem = Double.parseDouble(aPrem);
//				if("Y".equals(aPayEndYearFlag)){
//					tPayTimes = (12/tPayIntv)*tPayEndYear;
//				}else if("M".equals(aPayEndYearFlag)){
//					tPayTimes = tPayEndYear/tPayIntv;
//				}else if("D".equals(aPayEndYearFlag)){
//					tPayTimes = (12/tPayIntv)*(tPayEndYear/365);
//				}else if("A".equals(aPayEndYearFlag)){
//					tPayTimes = (12/tPayIntv)*(tPayEndYear - Integer.parseInt(aInsuredAppAge));
//				}
				tPayTimes = Integer.parseInt(tDifMonth)/tPayIntv;
				if(tPayTimes < 1){
					tPayTimes = 1;
				}
				tAllRiskSumPrem = String.valueOf(tPrem*tPayTimes);
				return tAllRiskSumPrem;
			}
	public static void main(String[] args) {
		GetTFSeparateBL tGetTFSeparateBL = new GetTFSeparateBL();
		VData cInputData = new VData();
		TransferData tTransferData = new TransferData();
		String tStartDate = "2016-08-25";
		String tEndDate = "";
		String tPrtNo = "";
		tTransferData.setNameAndValue("StartDate", tStartDate);
		tTransferData.setNameAndValue("EndDate", tEndDate);
		tTransferData.setNameAndValue("PrtNo", tPrtNo);
		cInputData.add(tTransferData);
		tGetTFSeparateBL.submitData(cInputData, "");
	}		
}
