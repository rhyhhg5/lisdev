package com.sinosoft.lis.ygz;

import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LYOutPayDetailSchema;
import com.sinosoft.lis.schema.LYPremSeparateDetailSchema;
import com.sinosoft.lis.vschema.LYOutPayDetailSet;
import com.sinosoft.lis.vschema.LYPremSeparateDetailSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 续期保费发票报送处理
 * @author abc
 * 
 */
public class GetXqPremBillSZBL {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	/** 数据操作字符串 */
	private String mOperate;
	
	private GlobalInput mGI = new GlobalInput();
	/** 开始日期 */
	private String mStartDate;
	/** 结束日期 */
	private String mEndDate;
	/** 页面手工提取标记*/
	private String mHandFlag ;
	private String mPushDate ;//页面批处理
	
	private String mWherePart ;
	
	// 业务处理相关变量
	private MMap mMap = new MMap();

	private String mCurrentDate = PubFun.getCurrentDate();
	private String mCurrentTime = PubFun.getCurrentTime();
	private boolean mIsclycle= true;

	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		// 进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GetBqPremBillBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败GetTempFeeBqSeparateBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}

//		// 准备往后台的数据
//		if (!prepareOutputData()) {
//			return false;
//		}

//		// 保存数据
//		PubSubmit tPubSubmit = new PubSubmit();
//
//		if (!tPubSubmit.submitData(mInputData, mOperate)) {
//			System.out.print("保存数据返回");
//			// @@错误处理
//			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
//			CError tError = new CError();
//			tError.moduleName = "GetBqPremBillBL";
//			tError.functionName = "submitData";
//			tError.errorMessage = "数据提交失败!";
//			this.mErrors.addOneError(tError);
//			return false;
//		}
//		mInputData = null;
		return true;
	}

	// 获得输出数据
	private boolean getInputData(VData cInputData) {
		TransferData tTransferData = (TransferData) cInputData
				.getObjectByObjectName("TransferData", 0);
		mStartDate = (String) tTransferData.getValueByName("StartDate");
		mEndDate = (String) tTransferData.getValueByName("EndDate");
		mPushDate = (String) tTransferData.getValueByName("PushDate");
		mHandFlag = (String) tTransferData.getValueByName("HandWorkFlag");
		mWherePart = "";
		
		if(null != mHandFlag && !"".equals(mHandFlag)){
			LYPremSeparateDetailSet tLYPremSeparateDetailSet=(LYPremSeparateDetailSet) cInputData
			.getObjectByObjectName("LYPremSeparateDetailSet", 0);
			if(null != tLYPremSeparateDetailSet){
				String lisStr="";
				for(int i=1; i<=tLYPremSeparateDetailSet.size();i++){
					lisStr += "'"+ tLYPremSeparateDetailSet.get(i).getTempFeeNo()+"'";
					if(i<tLYPremSeparateDetailSet.size()){
						lisStr += ",";
					}
				}
				mWherePart = " and lyp.tempfeeno in ("+lisStr+") ";
			}
		}
		 
		
		return true;
	}

	// 业务处理
	private boolean dealData() {

		LYOutPayDetailSet tLYOutPayDetailSet = new LYOutPayDetailSet();
		String tWhereSQL  = "";
		if(null != mPushDate && !mPushDate.equals("") ){
			tWhereSQL= " and lja.confdate = '"+mPushDate+"' ";
		}else {
		 tWhereSQL = " and lja.confdate = current date - 1 days ";
		}
//	    for (int a=6;a>=0;a--){
//	        String checkDate=PubFun.calDate(mCurrentDate,-a,"D",null);
//	        tWhereSQL = " and lja.confdate = '"+checkDate+"' ";
	    while(mIsclycle)
		  {
	    //深圳上海个单发票数据提取
	        String tSQL2 = " select "
					+ " lyp.busino,"//流水号
					+ " lcc.managecom  , "//所属组织
					+ " (select appntno from lcappnt where contno=lcc.contno ) , "//客户号
					+ " (select appntname from lcappnt where contno=lcc.contno ) , "
					+ " '2', "//客户类型:“1”代表机构，“2”代表个人
					+ " '01', "//价税分离项目:固定值
					+ " lyp.taxrate,"
					+ " lyp.moneyNoTax,"
					+ " lyp.moneyTax, "
					+ " '0', "//申报类型：0正常申报，固定值
					+ " '01', "//来源系统:固定值
					+ " lja.confdate,"//交易日期
					+ " lyp.riskcode,"//产品
					+ " lyp.policyno,"
					+ " lyp.busitype,"//业务类型
					+ " 'CNY',"//交易币种
					+ " lyp.paymoney,"//交易金额
					+ " '0', "//金额是否含税:“0”表示含税，“1”表示不含税
					+ " lyp.moneytype,"//收付标志
					+ " '0',"//境内外标示:“0”表示境内，“1”表示境外
					+ " 'Y',"//是否开票
					+ " '0',"//汇总地类型:“0”表示汇总，“1”表示属地，后续纳税申报需要
					+ " lyp.prtno, "//投保单号
					+ " lcc.signdate  ,"//签单号
	//				+ " lja.incomeno, "//批单号
					+ " lyp.moneyno moneyno, "
					+ " (select riskname from lmriskapp where riskcode=lyp.riskcode),"
					+ " (select Phone from LCAddress where CustomerNo = (select AppntNo from LCAppnt where ContNo = lcc.contno) and AddressNo = (select AddressNo from LCAppnt where ContNo = lcc.contno )),"//联系电话
					+ " (select Mobile from LCAddress where CustomerNo = (select AppntNo from LCAppnt where ContNo = lcc.contno) and AddressNo = (select AddressNo from LCAppnt where ContNo = lcc.contno)),"//移动电话
					+ " (select EMail from LCAddress where CustomerNo = (select AppntNo from LCAppnt where ContNo = lcc.contno) and AddressNo = (select AddressNo from LCAppnt where ContNo = lcc.contno)),"//电子邮箱
					+ " lcc.AppntIDType , "//证件类型
					+ " lcc.AppntIDNo  "//证件号码
					+ " from LCCont lcc,LYPremSeparateDetail lyp,ljapay lja "
					+ " where 1=1 " 
					+ " and lcc.conttype='1' "
					+ " and lcc.appflag='1' "
					+ " and lcc.contno=Lyp.policyno "
					+ " and lcc.contno=lja.incomeno "
					+ " and lyp.tempfeeno=lja.getnoticeno "
					+ " and lyp.policyno=lja.incomeno "
					+ " and lyp.tempfeetype ='2' "//续期
					+ " and (lyp.feefinatype <>'YEL' or lyp.feefinatype is null) "
					+ " and lyp.state='02' "//已价税分离
					+ " and not exists (select 1 from LYOutPayDetail where busino=lyp.busino ) "
//					+ " and exists (select 1 from lccont where contno=lyp.policyno union select 1 from lbcont where contno=lyp.policyno) "
					+ " and (lcc.managecom like '8631%' or lcc.managecom like '8695%') "
					+ " and lja.duefeetype='1' "
					+ " and lja.incometype='2' "
					+ mWherePart	
					+ tWhereSQL
					+ " union  "
					+ " select "
					+ " lyp.busino,"//流水号
					+ " lcc.managecom  , "//所属组织
					+ " (select appntno from lbappnt where contno=lcc.contno ) , "//客户号
					+ " (select appntname from lbappnt where contno=lcc.contno ) , "
					+ " '2', "//客户类型:“1”代表机构，“2”代表个人
					+ " '01', "//价税分离项目:固定值
					+ " lyp.taxrate,"
					+ " lyp.moneyNoTax,"
					+ " lyp.moneyTax, "
					+ " '0', "//申报类型：0正常申报，固定值
					+ " '01', "//来源系统:固定值
					+ " lja.confdate,"//交易日期
					+ " lyp.riskcode,"//产品
					+ " lyp.policyno,"
					+ " lyp.busitype,"//业务类型
					+ " 'CNY',"//交易币种
					+ " lyp.paymoney,"//交易金额
					+ " '0', "//金额是否含税:“0”表示含税，“1”表示不含税
					+ " lyp.moneytype,"//收付标志
					+ " '0',"//境内外标示:“0”表示境内，“1”表示境外
					+ " 'Y',"//是否开票
					+ " '0',"//汇总地类型:“0”表示汇总，“1”表示属地，后续纳税申报需要
					+ " lyp.prtno, "//投保单号
					+ " lcc.signdate  ,"//签单号
	//				+ " lja.incomeno, "//批单号
					+ " lyp.moneyno moneyno, "
					+ " (select riskname from lmriskapp where riskcode=lyp.riskcode),"
					+ " (select Phone from LCAddress where CustomerNo = (select AppntNo from LbAppnt where ContNo = lcc.contno) and AddressNo = (select AddressNo from LbAppnt where ContNo = lcc.contno )),"//联系电话
					+ " (select Mobile from LCAddress where CustomerNo = (select AppntNo from LbAppnt where ContNo = lcc.contno) and AddressNo = (select AddressNo from LbAppnt where ContNo = lcc.contno)),"//移动电话
					+ " (select EMail from LCAddress where CustomerNo = (select AppntNo from LbAppnt where ContNo = lcc.contno) and AddressNo = (select AddressNo from LbAppnt where ContNo = lcc.contno)),"//电子邮箱
					+ " lcc.AppntIDType , "//证件类型
					+ " lcc.AppntIDNo  "//证件号码
					+ " from LBCont lcc,LYPremSeparateDetail lyp,ljapay lja "
					+ " where 1=1 " 
					+ " and lcc.conttype='1' "
					+ " and lcc.appflag='1' "
					+ " and lcc.contno=Lyp.policyno "
					+ " and lcc.contno=lja.incomeno "
					+ " and lyp.tempfeeno=lja.getnoticeno "
					+ " and lyp.policyno=lja.incomeno "
					+ " and lyp.tempfeetype ='2' "//续期
					+ " and (lyp.feefinatype <>'YEL' or lyp.feefinatype is null) "
					+ " and lyp.state='02' "//已价税分离
					+ " and not exists (select 1 from LYOutPayDetail where busino=lyp.busino ) "
//					+ " and exists (select 1 from lccont where contno=lyp.policyno union select 1 from lbcont where contno=lyp.policyno) "
					+ " and (lcc.managecom like '8631%' or lcc.managecom like '8695%') "
					+ " and lja.duefeetype='1' "
					+ " and lja.incometype='2' "
					+ mWherePart	
					+ tWhereSQL
					+ " fetch first 10000 rows only " 
					+ " with ur";
	        
					SSRS tSSRS2 = new ExeSQL().execSQL(tSQL2);
					if(tSSRS2.getMaxRow()==10000){
						mIsclycle = true;
					}else {
						mIsclycle = false;
					}					
					for (int i = 1; i <= tSSRS2.getMaxRow(); i++) {
						LYOutPayDetailSchema tLYOutPayDetailSchema = new LYOutPayDetailSchema();
						tLYOutPayDetailSchema.setBusiNo(tSSRS2.GetText(i, 1));
						tLYOutPayDetailSchema.settranserial(tSSRS2.GetText(i, 1));//交易流水号
						tLYOutPayDetailSchema.setbusipk(tSSRS2.GetText(i, 1));
						tLYOutPayDetailSchema.setisupdate("Y");//是否更新客户信息
						tLYOutPayDetailSchema.setgroup("00");//所属集团:固定值
						tLYOutPayDetailSchema.setpkgroup("00");//所属集团:固定值
						tLYOutPayDetailSchema.setpkorg(tSSRS2.GetText(i, 2));//所属组织
						tLYOutPayDetailSchema.setorg(tSSRS2.GetText(i, 2));//所属组织
						tLYOutPayDetailSchema.setorgv(tSSRS2.GetText(i, 2));//所属组织
						tLYOutPayDetailSchema.setdeptdoc(tSSRS2.GetText(i, 2));
						tLYOutPayDetailSchema.setcode(tSSRS2.GetText(i, 3));//客户编码
						tLYOutPayDetailSchema.setname(tSSRS2.GetText(i, 4));//客户名称
			//			tLYOutPayDetailSchema.setcustomertype(tSSRS2.GetText(i, 5));//客户类型
						tLYOutPayDetailSchema.setcustomertype("2");
						tLYOutPayDetailSchema.setcustcode(tSSRS2.GetText(i, 3));
						tLYOutPayDetailSchema.setcustname(tSSRS2.GetText(i, 4));
			//			tLYOutPayDetailSchema.setcusttype(tSSRS2.GetText(i, 5));
						tLYOutPayDetailSchema.setcusttype("2");
						
						tLYOutPayDetailSchema.setptsitem(tSSRS2.GetText(i, 6));
						tLYOutPayDetailSchema.settaxrate(tSSRS2.GetText(i, 7));//税率
						tLYOutPayDetailSchema.setoriamt(tSSRS2.GetText(i, 8));//不含税金额
						tLYOutPayDetailSchema.setoritax(tSSRS2.GetText(i, 9));//税额
						tLYOutPayDetailSchema.setlocalamt(tSSRS2.GetText(i, 8));//不含税金额（本币）
						tLYOutPayDetailSchema.setlocaltax(tSSRS2.GetText(i, 9));//税额（本币）
						tLYOutPayDetailSchema.setdectype(tSSRS2.GetText(i, 10));//申报类型
						tLYOutPayDetailSchema.setsrcsystem(tSSRS2.GetText(i, 11));
						tLYOutPayDetailSchema.settrandate(tSSRS2.GetText(i, 12));//交易日期
						String riskcode = tSSRS2.GetText(i, 13);
						String moneyType = tSSRS2.GetText(i, 15);
						if ("09".equals(moneyType)) {//工本费riskcode置0000
							riskcode = BQ.FILLDATA;
						} else {
							riskcode = riskcode.substring(0, 4);
						}
						tLYOutPayDetailSchema.setprocode(riskcode);
						tLYOutPayDetailSchema.setbusitype(moneyType);//同价税分离接口的收支类型
						tLYOutPayDetailSchema.settrancurrency(tSSRS2.GetText(i, 16));// 币种：人民币
						tLYOutPayDetailSchema.settranamt(tSSRS2.GetText(i, 17));//交易金额（含税）
						tLYOutPayDetailSchema.settaxtype(tSSRS2.GetText(i, 18));//金额是否含税
						String paymentflag="0";//“0”表示收入，“1”表示支出
						if("02".equals(tSSRS2.GetText(i, 19))){
							paymentflag="1";
						}
						tLYOutPayDetailSchema.setpaymentflag(paymentflag);
						tLYOutPayDetailSchema.setoverseasflag(tSSRS2.GetText(i, 20));
						tLYOutPayDetailSchema.setisbill(tSSRS2.GetText(i, 21));
						tLYOutPayDetailSchema.setareatype(tSSRS2.GetText(i, 22));
						tLYOutPayDetailSchema.setinsureno(tSSRS2.GetText(i, 23));//印刷号
						tLYOutPayDetailSchema.setbilleffectivedate(tSSRS2.GetText(i, 24));
						tLYOutPayDetailSchema.setvdata(mCurrentDate);
						tLYOutPayDetailSchema.setvdef2(tSSRS2.GetText(i, 14));
						tLYOutPayDetailSchema.setmoneyno(tSSRS2.GetText(i, 25));
						tLYOutPayDetailSchema.setmoneytype(tSSRS2.GetText(i, 19));
						//备注
						String vdef1="保单号:"+tSSRS2.GetText(i, 14)+";险种："+tSSRS2.GetText(i, 26);
						
						//2018-5-28 针对上海分公司增加
						if("8631".equals(tSSRS2.GetText(i, 2).substring(0, 4))){
							//个单新增3个字段
							tLYOutPayDetailSchema.settelephone(tSSRS2.GetText(i, 27));
							tLYOutPayDetailSchema.setmobile(tSSRS2.GetText(i, 28));
							tLYOutPayDetailSchema.setemail(tSSRS2.GetText(i, 29));							
						}
						
						
						tLYOutPayDetailSchema.setvdef1(vdef1);
						tLYOutPayDetailSchema.setvoucherid(tSSRS2.GetText(i, 14));//保单号
						tLYOutPayDetailSchema.setprebilltype("02");//非预打
						tLYOutPayDetailSchema.setinvtype("0");//默认0
						tLYOutPayDetailSchema.setvaddress("0");
						tLYOutPayDetailSchema.setvsrcsystem("01");
						tLYOutPayDetailSchema.settaxpayertype("01");//管理平台提供的默认值
						tLYOutPayDetailSchema.setenablestate("2");//默认值
						
						/**
						 * 关于电子发票推送模式及规则调整，现增加发票类型，团单——'1'纸质发票，个单——'2'电子发票
						 * 大连分公司统一推电子发票
						 */
						tLYOutPayDetailSchema.setbilltype("2");
						//2018-2-6新增电子发票客户邮箱和客户手机号
						tLYOutPayDetailSchema.setbuymailbox(tSSRS2.GetText(i, 29));
						tLYOutPayDetailSchema.setbuyphoneno(tSSRS2.GetText(i, 28));
						
						//2018-3-13新增证件类型和证件号码
						if("0".equals(tSSRS2.GetText(i, 30))){
							tLYOutPayDetailSchema.setbuyidtype("01");
						}else if("1".equals(tSSRS2.GetText(i, 30))){
							tLYOutPayDetailSchema.setbuyidtype("02");
						}else if("2".equals(tSSRS2.GetText(i, 30))){
							tLYOutPayDetailSchema.setbuyidtype("03");
						}else if("3".equals(tSSRS2.GetText(i, 30))||"4".equals(tSSRS2.GetText(i, 30))||"5".equals(tSSRS2.GetText(i, 30))||"6".equals(tSSRS2.GetText(i, 30))||"7".equals(tSSRS2.GetText(i, 30))||"8".equals(tSSRS2.GetText(i, 30))){
							tLYOutPayDetailSchema.setbuyidtype("07");
						}else{
							tLYOutPayDetailSchema.setbuyidtype("");
						}
						tLYOutPayDetailSchema.setbuyidnumber(tSSRS2.GetText(i, 31));
						
						tLYOutPayDetailSchema.setoperator(mGI.Operator);
						tLYOutPayDetailSchema.setMakeDate(mCurrentDate);
						tLYOutPayDetailSchema.setMakeTime(mCurrentTime);
						tLYOutPayDetailSchema.setModifyDate(mCurrentDate);
						tLYOutPayDetailSchema.setModifyTime(mCurrentTime);
						
						tLYOutPayDetailSet.add(tLYOutPayDetailSchema);
					}
		//			int count =0;
					LYOutPayDetailSet sLYOutPayDetailSet = new LYOutPayDetailSet();
					if (tLYOutPayDetailSet.size()>0){
						for (int i=1; i<=tLYOutPayDetailSet.size();i++){
							sLYOutPayDetailSet.add(tLYOutPayDetailSet.get(i));
//							count++;
//							if (count==5000 || i == tLYOutPayDetailSet.size()){
//								count =0;
								//调用发票接口上报数据
								VData tVData = new VData();
								tVData.add(mGI);
								tVData.add(sLYOutPayDetailSet);
								OutPayUploadBL tOutPayUploadBL = new OutPayUploadBL();
								if (!tOutPayUploadBL.getSubmit(tVData, "")) {
									this.mErrors = tOutPayUploadBL.mErrors;
									System.out.println("保全收付费发票数据上报错误:"+ mErrors.getErrContent());
								}else{
									//保存返回结果
									//mMap.add(tOutPayUploadBL.getResult()) ;
									mMap = tOutPayUploadBL.getResult();
									mInputData = new VData();
									this.mInputData.add(mMap);
									PubSubmit tPubSubmit = new PubSubmit();
								    if (!tPubSubmit.submitData(mInputData, SysConst.INSERT)){
								        this.mErrors.copyAllErrors(tPubSubmit.mErrors);
									    CError tError = new CError();
									    tError.moduleName = "GetBqPremBillBL";
									    tError.functionName = "submitData";
									    tError.errorMessage = "数据提交失败!";
									    this.mErrors.addOneError(tError);
								     }
								   mMap = new MMap();
								   mInputData.clear();
								   sLYOutPayDetailSet = new LYOutPayDetailSet();
								}
//					    	}
						}
					}
					System.out.println(mCurrentTime);
//			}
		  }
		return true;
	}

	/**
	 * 准备后台的数据
	 * 
	 * @return boolean
	 */
	private boolean prepareOutputData() {
		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GetBqPremBillBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	public static void main(String[] args) {
		GetXqPremBillSZBL tGetBqPremBillBL = new GetXqPremBillSZBL();
		VData cInputData = new VData();
		TransferData tTransferData = new TransferData();
		String tStartDate = "2016-07-20";
		String tEndDate = "2016-09-05";
		LYPremSeparateDetailSet set=new LYPremSeparateDetailSet();
		String [] str=new String[]{"31001798201"};//个单收费记录号
		for(int i=0;i<str.length; i++){
			LYPremSeparateDetailSchema Schema=new LYPremSeparateDetailSchema();
			Schema.setTempFeeNo(str[i]);
			set.add(Schema);
		}
		tTransferData.setNameAndValue("HandWorkFlag","Y");
		cInputData.add(set);
		tTransferData.setNameAndValue("StartDate", tStartDate);
		tTransferData.setNameAndValue("EndDate", tEndDate);
		cInputData.add(tTransferData);
		tGetBqPremBillBL.submitData(cInputData, "");
	}
}
