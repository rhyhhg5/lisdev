package com.sinosoft.lis.ygz;

import java.util.HashMap;
import java.util.Iterator;

import com.sinosoft.lis.db.LYPremSeparateDetailDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LYPremSeparateDetailSchema;
import com.sinosoft.lis.vschema.LYPremSeparateDetailSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
/**
 * 
 * 通过页面推送的保单，获取其价税分离后的数据
 *
 */
public class GetPayMoneyBL {
	
	/** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    private GlobalInput mGlobalInput = null;

    private LYPremSeparateDetailSet mLYPremSeparateDetailSet = null;
    
    private HashMap mHashMap = null;

    /**传输到后台处理的map*/
    private MMap mMap = new MMap();
    
    /** 数据操作字符串 */
    private String mOperate = "";
    
    private String mCurrentDate = PubFun.getCurrentDate();
	private String mCurrentTime = PubFun.getCurrentTime();
    
    public boolean submitData(VData cInputData, String cOperate){
    	
    	//准备数据
        if(getSubmitMap(cInputData, cOperate) == null){
            return false;
        }
        
        //数据提交
        if(!submit()){
            return false;
        }
        
        return true;
    }
    
    public MMap getSubmitMap(VData cInputData, String cOperate){
        if (!getInputData(cInputData, cOperate)){
            return null;
        }

        if (!checkData()){
            return null;
        }

        if (!dealData()){
            return null;
        }

        return mMap;
    }
    
    private boolean getInputData(VData cInputData, String cOperate){
    	
    	mOperate = cOperate;
    	if(!"1".equals(mOperate) && !"2".equals(mOperate)){
    		buildError("getInputData","不识别的操作符。");
    		return false;
    	}
    	
    	mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
    	if(mGlobalInput == null){
    		buildError("getInputData","处理超时，请重新登录。");
    		return false;
    	}
    	
    	mHashMap = (HashMap)cInputData.getObjectByObjectName("HashMap", 0);
    	if(mHashMap == null || mHashMap.size()<=0){
    		buildError("getInputData", "所需参数不完整。");
            return false;
    	}
    	
    	return true;
    }
    
    private boolean checkData(){
    	return true;
    }
    
    private boolean dealData(){
    	/* 用于封装调用价税分离接口的数据 */
    	LYPremSeparateDetailSet totalLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
    	//1、将从前面传过来的保单，封装在价税分离表 callLYPremSeparateDetailSet 中。
    	Iterator iter = mHashMap.keySet().iterator();
    	while (iter.hasNext()) {
    		Object key = iter.next();
    		String tPrtNo = (String)mHashMap.get(key);
    		System.out.println("tPrtNo:" + tPrtNo);
    		LYPremSeparateDetailSet aLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
    		aLYPremSeparateDetailSet = dealPolicy(tPrtNo,mOperate);
    		totalLYPremSeparateDetailSet.add(aLYPremSeparateDetailSet);
    	}
    	//2、筛选数据:对已经分离了费用的数据，不调用接口，否则调用
    	LYPremSeparateDetailSet callLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
    	LYPremSeparateDetailSet noCallLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
    	if(totalLYPremSeparateDetailSet!=null && totalLYPremSeparateDetailSet.size()>0){
    		for(int i=1; i<=totalLYPremSeparateDetailSet.size(); i++){
    			LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
    			tLYPremSeparateDetailSchema = totalLYPremSeparateDetailSet.get(i);
    			//state:[01:提取;02:价税分离]
    			//对于已经价税分离过的数据(state='02')，不再调用价税分离接口;否则(state='01'),准备调用价税分离接口的数据
    			if(tLYPremSeparateDetailSchema.getState()!=null && "02".equals(tLYPremSeparateDetailSchema.getState())){
    				noCallLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
    			}else if("01".equals(tLYPremSeparateDetailSchema.getState())){
    				tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
    				tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
    				callLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
    			}else{
    				System.out.println("数据状态未知！");
    			}
    		}
    	}else{
    		System.out.println("没有获得数据！");
    	}   	
    	//3、调用价税分离接口,获得分离了费用的价税分离数据
    	PremSeparateBL tPremSeparateBL = new PremSeparateBL();
    	VData tVData = new VData();
    	tVData.add(callLYPremSeparateDetailSet);
    	tVData.add(mGlobalInput);
    	if(!tPremSeparateBL.getSubmit(tVData, "")){
    		buildError("dealData", "调用PremSeparateBL接口失败。");
    		return false;
    	}
    	MMap tMMap = new MMap();
    	tMMap = tPremSeparateBL.getResult();
    	//将价税分离接口返回的数据封装进MMap。
    	mMap.add(tMMap);
    	//经询问，不需要再次保存已经做过价税分离的数据，注释掉下一行代码。
//    	mMap.put(noCallLYPremSeparateDetailSet, SysConst.DELETE_AND_INSERT);
    	
    	return true;
    }
    
    private LYPremSeparateDetailSet dealPolicy(String cPrtNo,String cOperator){
    	
    	if(cOperator==null || (!"1".equals(cOperator) && !"2".equals(cOperator))){
    		buildError("dealPolicy", "不支持的操作。");
            return null;
    	}
    	if(cPrtNo==null || "".equals(cPrtNo)){
    		buildError("dealPolicy", "没有获得印刷号。");
            return null;
    	}
    	
    	String tPrtNo = cPrtNo;
    	LYPremSeparateDetailSet tLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
    	
    	if("1".equals(cOperator)){//对个单的处理
    		//判断个单是否已经客户回销，跳过该单。
    		//modify liyt 2016-07-01 取消回执的校验
//    		String tCustomGetPolDate = new ExeSQL().getOneValue("select customgetpoldate from lccont where prtno = '"+tPrtNo+"'");
//    		if(tCustomGetPolDate==null){
//    			buildError("dealPolicy", "印刷号为:"+tPrtNo+"的个单客户尚未回执。");
//                return null;
//    		}
    		//个单的已经签单，判断是否已经存在价税分离，有的话获取数据，没有的话调用价税分离接口
    		LYPremSeparateDetailDB tLYPremSeparateDetailDB = new LYPremSeparateDetailDB();
    		tLYPremSeparateDetailSet = tLYPremSeparateDetailDB.executeQuery("select * from lypremseparatedetail where tempfeetype in (select code from ldcode where codetype = 'ygzqyttype') and prtno = '"+tPrtNo+"'");
    		//如果不存在价税分离的数据，调用郭总的GetTempFeeSeparateBL,获取价税分离表的数据
    		if(tLYPremSeparateDetailSet == null || tLYPremSeparateDetailSet.size()<1){
    			//需要传过去的参数有
    			//1、开始日期:将按照这个日期进行提取数据(因为郭总的接口不给日期就按照前一天进行提取，但我给的单子不一定是前一天的，所以此处需要加上日期)
    			//2、印刷号:通过印刷号获得数据，提交数据库
    			//GetTempFeeSeparateBL tGetTempFeeSeparateBL = new GetTempFeeSeparateBL();
    			GetTempFeeBL tGetTempFeeBL = new GetTempFeeBL();//二期sql优化
    			VData cInputData = new VData();
    			TransferData tTransferData = new TransferData();
    			tTransferData.setNameAndValue("StartDate", "2016-05-01");
    			tTransferData.setNameAndValue("PrtNo", tPrtNo);
    			cInputData.add(tTransferData);
    			//如果返回为false,表示没有成功获得数据提交数据库
    			if(!tGetTempFeeBL.submitData(cInputData, "")){
    				buildError("dealPolicy", "印刷号为:"+tPrtNo+"的个单没有成功生成价税分离表数据。");
                    return null;
    			}else{//否则拿取数据封装进Set，准备后续调用志粤的价税分离接口
    				tLYPremSeparateDetailSet.add(tLYPremSeparateDetailDB.executeQuery("select * from lypremseparatedetail where tempfeetype in (select code from ldcode where codetype = 'ygzqyttype') and prtno = '"+tPrtNo+"'"));
    			}
    		}
    	}else if("2".equals(cOperator)){//对团单的处理
    		//判断团单是否已经下发核保结论，且核保结论为:4-变更承保或9-正常承保
    		//如果是则可以做价税分离，否则不允许
    		String tUWFlag = new ExeSQL().getOneValue("select uwflag from lcgrpcont where prtno = '"+tPrtNo+"'");
    		if(tUWFlag==null || (!"4".equals(tUWFlag) && !"9".equals(tUWFlag))){
    			buildError("dealPolicy", "印刷号为:"+tPrtNo+"的团单尚未下发核保结论或核保结论为不通过。");
                return null;
    		}
    		//add by liyt 2016-07-11 如果价税分离表里数据已提取，则不需要再次提取
    		LYPremSeparateDetailDB tLYPremSeparateDetailDB = new LYPremSeparateDetailDB();
    		tLYPremSeparateDetailSet = tLYPremSeparateDetailDB.executeQuery("select * from lypremseparatedetail where tempfeetype in (select code from ldcode where codetype='ygzqyttype')  and state = '01' and prtno = '"+tPrtNo+"'");
    		if(tLYPremSeparateDetailSet.size() > 0 ){
    			
    		}else{
    			
    		SSRS tSSRS = new SSRS();
    		tSSRS = new ExeSQL().execSQL("select grpcontno,riskcode,prem,managecom from lcgrppol where prtno ='"+tPrtNo+"'");
    		
    		for(int i=1; i<=tSSRS.MaxRow; i++){
    			
    			LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
    			
    			tLYPremSeparateDetailSchema.setBusiNo(tPrtNo+"T"+tSSRS.GetText(i, 2));
    			if("8201".equals(tSSRS.GetText(i, 2).substring(0,4))){
    				tLYPremSeparateDetailSchema.setBusiType("08");//健康守望推送发票类型为08
    			}else{
    				tLYPremSeparateDetailSchema.setBusiType("01");//01，保单保费;02，工本费
    			}
    			
        		tLYPremSeparateDetailSchema.setTempFeeNo(tPrtNo+"801");
        		tLYPremSeparateDetailSchema.setTempFeeType("N");//此时尚未有暂收数据，新定义类型'N'
        		tLYPremSeparateDetailSchema.setPayMoney(tSSRS.GetText(i, 3));
        		tLYPremSeparateDetailSchema.setPolicyNo(tSSRS.GetText(i, 1));
        		tLYPremSeparateDetailSchema.setRiskCode(tSSRS.GetText(i, 2));
        		tLYPremSeparateDetailSchema.setPrtNo(tPrtNo);
        		tLYPremSeparateDetailSchema.setOtherNo(tPrtNo);
        		tLYPremSeparateDetailSchema.setOtherNoType("5");//5表示团单印刷号
        		tLYPremSeparateDetailSchema.setState("01");//01：提取;02：价税分离
        		tLYPremSeparateDetailSchema.setOtherState("03");//01：从暂收生成的数据;02：从实付生成的数据;03：预打发票生成的数据
        		tLYPremSeparateDetailSchema.setMoneyType("1");//1 ---收费;2 ---付费
        		
        		tLYPremSeparateDetailSchema.setManageCom(tSSRS.GetText(i, 4));
        		tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
        		tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
        		tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
        		tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
        		
        		tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
    		}
    	  }
    	}
    	
    	return tLYPremSeparateDetailSet;
    }
    
    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "GetPayMoneyBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    
    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit(){
    	
    	VData data = new VData();
    	data.add(mMap);
    		
    	PubSubmit p = new PubSubmit();
    	if (!p.submitData(data, SysConst.DELETE_AND_INSERT)){
    		System.out.println("提交数据失败");
    		buildError("submitData", "提交数据失败");
    		return false;
    	}

        return true;
    }

    public VData getResult(){
        return mResult;
    }
}
