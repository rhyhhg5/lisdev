package com.sinosoft.lis.ygz;

import com.sinosoft.lis.db.LJAPayDB;
import com.sinosoft.lis.db.LJAPayPersonDB;
import com.sinosoft.lis.db.LYPremSeparateDetailDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LYPremSeparateDetailSchema;
import com.sinosoft.lis.vschema.LJAPayPersonSet;
import com.sinosoft.lis.vschema.LJAPaySet;
import com.sinosoft.lis.vschema.LYPremSeparateDetailSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class GetPayNoBL {
	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	/** 数据操作字符串 */
	private String mOperate;
	/** 开始日期*/
	private String mStartDate;
	/** 结束日期*/
	private String mEndDate;
	/** 保单号码*/
	private String mIncomeNo;
	//业务处理相关变量
	private MMap mMap = new MMap();
	
	private String mCurrentDate = PubFun.getCurrentDate();
	
	public boolean submitData(VData cInputData, String cOperate) {
		System.out.println(mCurrentDate+":开始提取实收数据回写价税分离表。");
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		// 进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCProjectMonthInfoBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败LCProjectCancelBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}

		// 准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}

		// 保存数据
		PubSubmit tPubSubmit = new PubSubmit();

		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			System.out.print("保存数据返回");
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "LCProjectCancelBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mInputData = null;
		System.out.println(mCurrentDate+":提取实收数据回写价税分离表结束。");
		return true;
	}

	// 获得输出数据
	private boolean getInputData(VData cInputData) {
		TransferData tTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
		mStartDate = (String) tTransferData.getValueByName("StartDate");
		mEndDate = (String) tTransferData.getValueByName("EndDate");
		mIncomeNo = (String) tTransferData.getValueByName("IncomeNo");
		return true;
	}
	
//  业务处理
	private boolean dealData() {
		String tWhereSQL = "";
		if(mStartDate != null && !"".equals(mStartDate)){
			tWhereSQL += " and confdate >= '" + mStartDate + "'";
		}
		if(mEndDate != null && !"".equals(mEndDate)){
			tWhereSQL += " and confdate <= '" + mEndDate + "'";
		}
		if(mIncomeNo != null && !"".equals(mIncomeNo)){
			tWhereSQL += " and IncomeNo = '" + mIncomeNo + "'";
		}
		if("".equals(tWhereSQL)){
			tWhereSQL += "and confdate >= current date - 7 day ";
		}
		
		//暂收表到险种的契约数据
		String tSQL = " select * from ljapay where duefeetype = '0' "
					+ " and incomeno in ( "
					+ " select policyno from LYPremSeparateDetail where tempfeetype in (select code from ldcode where codetype = 'ygzqyttype') and otherstate<>'17' and (MoneyNo is null or MoneyNo = '') "
					+ " union "
					+ " select (select lgc.grpcontno from lcgrpcont lgc where lgc.prtno = lpsd.prtno) from LYPremSeparateDetail lpsd where lpsd.tempfeetype in (select code from ldcode where codetype = 'ygzqyttype') and lpsd.otherstate<>'17' and (lpsd.MoneyNo is null or lpsd.MoneyNo = '') and lpsd.policyno like '1%' "
					+ " union "
					+ " select (select lcc.contno from lccont lcc where lcc.prtno = lpsd.prtno and lcc.conttype = '1') from LYPremSeparateDetail lpsd where lpsd.tempfeetype in (select code from ldcode where codetype = 'ygzqyttype') and (lpsd.MoneyNo is null or lpsd.MoneyNo = '') and lpsd.policyno like '1%' "
					+ " ) "
//					+ " and incomeno = '00286879000001' "
//					+ tWhereSQL
					;
		LJAPayDB tLJAPayDB = new LJAPayDB();
		LJAPaySet tLJAPaySet = tLJAPayDB.executeQuery(tSQL);
		if(tLJAPaySet != null && tLJAPaySet.size() != 0){
			LYPremSeparateDetailDB tLYPremSeparateDetailDB  = new LYPremSeparateDetailDB ();
			String tCurrentDate = PubFun.getCurrentDate();
			String tCurrentTime = PubFun.getCurrentTime();
			for(int i=1;i<=tLJAPaySet.size();i++){
				String tSSql = " select * from LYPremSeparateDetail "
						 + " where tempfeetype in (select code from ldcode where codetype = 'ygzqyttype') "
						 + " and (MoneyNo is null or MoneyNo = '') "
						 + " and prtno = ("
						 + " select lgc.prtno from lcgrpcont lgc where lgc.grpcontno = '"+tLJAPaySet.get(i).getIncomeNo()+"' "
						 + " union "
						 + " select lgc.prtno from lbgrpcont lgc where lgc.grpcontno = '"+tLJAPaySet.get(i).getIncomeNo()+"' "
						 + " union "
						 + " select lcc.prtno from lccont lcc where lcc.conttype = '1' and lcc.contno = '"+tLJAPaySet.get(i).getIncomeNo()+"'"
						 + " union "
						 + " select lcc.prtno from lbcont lcc where lcc.conttype = '1' and lcc.contno = '"+tLJAPaySet.get(i).getIncomeNo()+"'"
						 + " ) "
						 + " and otherstate <>'17'"
						 ;
				LYPremSeparateDetailSet tLYPremSeparateDetailSet = tLYPremSeparateDetailDB.executeQuery(tSSql);
				LYPremSeparateDetailSet mLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
				if(tLYPremSeparateDetailSet!=null && tLYPremSeparateDetailSet.size()>0){
					for(int j=1;j<=tLYPremSeparateDetailSet.size();j++){
						LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = tLYPremSeparateDetailSet.get(j);
						if(tLYPremSeparateDetailSchema.getPayMoney()!=null && !"".equals(tLYPremSeparateDetailSchema.getPayMoney())
							&& String.valueOf(tLJAPaySet.get(i).getSumActuPayMoney())!= null && !"".equals(tLJAPaySet.get(i).getSumActuPayMoney()) ){
							if(Double.valueOf(tLYPremSeparateDetailSchema.getPayMoney()) * Double.valueOf(tLJAPaySet.get(i).getSumActuPayMoney()) >=0 ){
								tLYPremSeparateDetailSchema.setMoneyNo(tLJAPaySet.get(i).getPayNo());
								tLYPremSeparateDetailSchema.setMoneyType("01");
								tLYPremSeparateDetailSchema.setModifyDate(tCurrentDate);
								tLYPremSeparateDetailSchema.setModifyTime(tCurrentTime);
								mLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
							}
						}
					}
				}
				mMap.put(mLYPremSeparateDetailSet, SysConst.UPDATE);
			}
			
		}
		
		//契约首期追加保费契约数据
				String tZBSQL = " select * from ljapayperson where paytype ='ZB' "
						+ " and contno in ( "
						+ " select policyno from LYPremSeparateDetail where tempfeetype in (select code from ldcode where codetype = 'ygzqyttype') and otherstate = '17' and (MoneyNo is null or MoneyNo = '')) "
						+ " and payno in (select payno from ljapay where incomeno = contno and duefeetype = '0') "
						;
				System.out.println("===  "+tZBSQL);
				LJAPayPersonDB tLJAPayPersonDB = new LJAPayPersonDB();
				LJAPayPersonSet tLJAPayPersonSet = tLJAPayPersonDB.executeQuery(tZBSQL);
				if(tLJAPayPersonSet != null && tLJAPayPersonSet.size() != 0){
					LYPremSeparateDetailSet tZBLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
					LYPremSeparateDetailDB tZBLYPremSeparateDetailDB  = new LYPremSeparateDetailDB ();
					String tZBCurrentDate = PubFun.getCurrentDate();
					String tZBCurrentTime = PubFun.getCurrentTime();
					for(int i=1;i<=tLJAPayPersonSet.size();i++){
						String tZBSSql = " select * from LYPremSeparateDetail lyp"
								 + " where tempfeetype in (select code from ldcode where codetype = 'ygzqyttype') "
								 + " and (MoneyNo is null or MoneyNo = '') "
								 + " and prtno = ("
								 + " select lcc.prtno from lccont lcc where lcc.conttype = '1' and lcc.contno = '"+tLJAPayPersonSet.get(i).getContNo()+"'"
								 + " union "
								 + " select lcc.prtno from lbcont lcc where lcc.conttype = '1' and lcc.contno = '"+tLJAPayPersonSet.get(i).getContNo()+"'"
								 + " ) "
								 + " and otherstate ='17'"
								 ;
						tZBLYPremSeparateDetailSet= tZBLYPremSeparateDetailDB.executeQuery(tZBSSql);
						LYPremSeparateDetailSet mZBLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
						if(tZBLYPremSeparateDetailSet!=null && tZBLYPremSeparateDetailSet.size()>0){
							for(int j=1;j<=tZBLYPremSeparateDetailSet.size();j++){
									LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = tZBLYPremSeparateDetailSet.get(j);
									if(tLJAPayPersonSet.get(i).getRiskCode().equals(tLYPremSeparateDetailSchema.getRiskCode())){
										tLYPremSeparateDetailSchema.setMoneyNo(tLJAPayPersonSet.get(i).getPayNo());
										tLYPremSeparateDetailSchema.setMoneyType("01");
										tLYPremSeparateDetailSchema.setModifyDate(tZBCurrentDate);
										tLYPremSeparateDetailSchema.setModifyTime(tZBCurrentTime);
										mZBLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
								}
							}
						}
						mMap.put(mZBLYPremSeparateDetailSet, SysConst.UPDATE);
					}
				}
		return true;
	}
		
	/**
	 * 准备后台的数据
	 * @return boolean
	 */
	private boolean prepareOutputData() {
		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCProjectCancelBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
	
	public static void main(String[] args) {
		GetPayNoBL tGetTempFeeSeparateBL = new GetPayNoBL();
		VData cInputData = new VData();
		TransferData tTransferData = new TransferData();
		String tStartDate = "";
		String tEndDate = "";
		String tIncomeNo = "00014720000002";
		tTransferData.setNameAndValue("StartDate", tStartDate);
		tTransferData.setNameAndValue("EndDate", tEndDate);
		tTransferData.setNameAndValue("IncomeNo", tIncomeNo);
		cInputData.add(tTransferData);
		tGetTempFeeSeparateBL.submitData(cInputData, "");
	}
}
