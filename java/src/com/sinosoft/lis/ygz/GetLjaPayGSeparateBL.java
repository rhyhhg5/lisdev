package com.sinosoft.lis.ygz;

import java.util.ArrayList;
import java.util.List;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LJAPayGrpSchema;
import com.sinosoft.lis.vschema.LJAPayGrpSet;
import com.sinosoft.lis.vschema.LYPremSeparateDetailSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class GetLjaPayGSeparateBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	/** 数据操作字符串 */
	private String mOperate;
	/** 开始日期*/
	private String mStartDate;
	/** 结束日期*/
	private String mEndDate;
	/** 印刷号*/
	private String mContNo;
	//业务处理相关变量
	private MMap mMap = new MMap();
	
	/** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
	
	private String mCurrentDate = PubFun.getCurrentDate();
	private String mCurrentTime = PubFun.getCurrentTime();
	public boolean submitData(VData cInputData, String cOperate) {
		System.out.println(mCurrentDate+":开始提取LjaPayGrp价税分离数据。");
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		// 进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GetLjaPayGSeparateBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败GetLjaPayGSeparateBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}

		mInputData = null;
		System.out.println(mCurrentDate+":提取LjaPayGrp价税分离数据结束。");
		return true;
	}

	// 获得输出数据
	private boolean getInputData(VData cInputData) {
		TransferData tTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
		mStartDate = (String) tTransferData.getValueByName("StartDate");
		mEndDate = (String) tTransferData.getValueByName("EndDate");
		mContNo = (String) tTransferData.getValueByName("ContNo");
		mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
    	if(mGlobalInput == null){
    		buildError("getInputData","处理超时，请重新登录。");
    		return false;
    	}
		return true;
	}
	
//  业务处理
	private boolean dealData() {
		YGZ mYGZ = new YGZ();
		ExeSQL tExeSQL = new ExeSQL();
		String tWhereSQL = "";
		String tWhereTSQL = "";
		if(mStartDate != null && !"".equals(mStartDate)){
			tWhereSQL += " and confdate  >= '" + mStartDate + "'";
		}
		if(mEndDate != null && !"".equals(mEndDate)){
			tWhereSQL += " and confdate  <= '" + mEndDate + "'";
		}
		if(mContNo != null && !"".equals(mContNo)){
			tWhereTSQL += " and grpcontno ='" + mContNo + "' " ;
		}
		if("".equals(tWhereSQL)&&"".equals(tWhereTSQL)){
			tWhereSQL += "and confdate >= current date - 7 day ";
		}
		
		/**
		 * 实收集体交费表数据
		 */
		String tSQL = " select *"
					+ " from ljapaygrp lja "
					+ " where lja.paytype = 'ZC' "
					+ " and (lja.MoneyNoTax is null or lja.MoneyNoTax = '') and (lja.MoneyTax is null or lja.MoneyTax = '') and sumactupaymoney <> '0' "
					+ tWhereSQL
					+ tWhereTSQL
					+ " union "//实收保费转出的反冲数据应该重新价税
					+ " select * "
					+ " from ljapaygrp lja "
					+ " where lja.paytype = 'ZC' "
					+ " and (MoneyNoTax is not null and MoneyNoTax <> '') "
					+ " and abs(double(moneynotax)+double(moneytax)-SumActuPayMoney)>=0.01 and sumactupaymoney <> '0' "
					+ tWhereSQL
					+ tWhereTSQL
					+ " with ur";
		RSWrapper rsWrapper = new RSWrapper();
		LJAPayGrpSet tLJAPayGrpSet = new LJAPayGrpSet();
		if (!rsWrapper.prepareData(tLJAPayGrpSet, tSQL)) {
			System.out.println("价税分离明细ljapaygrp数据准备失败！");
			return false;
		}
		do {
			rsWrapper.getData();
			if (null == tLJAPayGrpSet
					|| tLJAPayGrpSet.size() < 1) {
				break;
			}
			LYPremSeparateDetailSet tLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
			for(int i=1; i<=tLJAPayGrpSet.size(); i++){
				PremSeparateDetailHX tPremSeparateDetailHXSchema = new PremSeparateDetailHX();
				String busino ="";
				String policyno = "";
				String tBusiType = "";
				LJAPayGrpSchema tLJAPayGrpSchema = tLJAPayGrpSet.get(i);
				//根据schema获取主键字段名信息
				String[] schemaPK = tLJAPayGrpSchema.getPK();
				//将主键的值存储到集合中
				List pklist =new ArrayList();
				for(int k=0; k<schemaPK.length; k++){
					pklist.add(tLJAPayGrpSchema.getV(schemaPK[k]));
					busino +=tLJAPayGrpSchema.getV(schemaPK[k]);
				}
				tPremSeparateDetailHXSchema.setpKList(pklist);
				if(busino.length()>=20){
					busino = busino.substring(0, 20);
				}
				System.out.println("busino="+busino);
				tPremSeparateDetailHXSchema.setBusiNo(busino);
				policyno = tLJAPayGrpSchema.getGrpContNo();
				tPremSeparateDetailHXSchema.setOtherNo(policyno);
				tPremSeparateDetailHXSchema.setOtherNoType("2");
				tPremSeparateDetailHXSchema.setPrtNo("000000");
				tPremSeparateDetailHXSchema.setPolicyNo(policyno);
				tPremSeparateDetailHXSchema.setRiskCode(tLJAPayGrpSchema.getRiskCode());
				tPremSeparateDetailHXSchema.setManageCom(tLJAPayGrpSchema.getManageCom());
				tPremSeparateDetailHXSchema.setPayMoney(""+tLJAPayGrpSchema.getSumActuPayMoney());
				tPremSeparateDetailHXSchema.setMoneyNo(tLJAPayGrpSchema.getPayNo());
				tBusiType = mYGZ.getBusinType("", "BF", tLJAPayGrpSchema.getRiskCode()); 
				tPremSeparateDetailHXSchema.setBusiType(tBusiType);
				tPremSeparateDetailHXSchema.setState("01");
				tPremSeparateDetailHXSchema.setOtherState("19");//从实收集体交费表提取数据 
				tPremSeparateDetailHXSchema.setMoneyType("01");
				tLYPremSeparateDetailSet.add(tPremSeparateDetailHXSchema);
			}
		//调用价税分离接口,获得分离了费用的价税分离数据
		if(tLYPremSeparateDetailSet.size() > 0){
			PremSeparateBL tPremSeparateBL = new PremSeparateBL();
			VData tVData = new VData();
			tVData.add(tLYPremSeparateDetailSet);
			tVData.add(mGlobalInput);
			if(!tPremSeparateBL.getSubmit(tVData, "")){
	    		buildError("dealData", "调用PremSeparateBL接口失败。");
	    		return false;
	    	}
			MMap tMMap = new MMap();
	    	tMMap = tPremSeparateBL.getResult();
	    	//将价税分离接口返回的数据封装进MMap。
	    	LYPremSeparateDetailSet mLYPremSeparateDetailSet = (LYPremSeparateDetailSet) tMMap.getObjectByObjectName("LYPremSeparateDetailSet", 0);
	    	for(int i = 1; i <= mLYPremSeparateDetailSet.size();i++){
	    		PremSeparateDetailHX mPremSeparateDetailHXSchema = (PremSeparateDetailHX) mLYPremSeparateDetailSet.get(i);
	    		if(mPremSeparateDetailHXSchema.getMoneyNoTax() != null && !"".equals(mPremSeparateDetailHXSchema.getMoneyNoTax())){
		    		String moneyNoTax = mPremSeparateDetailHXSchema.getMoneyNoTax();
		    		String moneyTax = mPremSeparateDetailHXSchema.getMoneyTax();
		    		String busiType = mPremSeparateDetailHXSchema.getBusiType();
		    		String taxRate = mPremSeparateDetailHXSchema.getTaxRate();//税率
		    		String whereSQL = " 1=1 ";
		    		LJAPayGrpSchema tempLJAPayGrpSchema = new LJAPayGrpSchema();
		    		//从表的schema中获取主键字段
		    		String[] tempPk = tempLJAPayGrpSchema.getPK();
		    		//根据主键的值拼接where条件
		    		List pkList = mPremSeparateDetailHXSchema.getpKList();
		    		for(int k=0; k< tempPk.length; k++){
		    			whereSQL += " and "+tempPk[k]+" = '"+pkList.get(k)+"' ";
		    		}
		    		//lzy 为保险起见加个校验
		    		if(" 1=1 ".equals(whereSQL)){
		    			return false;
		    		}
		    		String sql = "update LjaPayGrp lja set MoneyNotax = '"+ moneyNoTax +"',"
		    				   	+ " Moneytax = '" + moneyTax+"',"
		    				   	+ " ModifyDate = '" + mCurrentDate +"',"
		    				   	+ " ModifyTime = '" + mCurrentTime +"',"
		    				   	+ " BusiType = '" + busiType +"',"
		    				   	+ " TaxRate = '" + taxRate +"'"
		    				   	+ " where " + whereSQL
		    				   	;
		    		mMap.put(sql, SysConst.UPDATE);
	    		}
	    	}
		}
		
		//在这里提交
		if(!submit()){
			System.out.println("数据执行失败，运行下一批次");
		}
		
		}while (null != tLJAPayGrpSet
			&& tLJAPayGrpSet.size() > 0);
		
    	return true;
	}
	/**
	 * 准备后台的数据
	 * @return boolean
	 */
	private boolean submit() {
		try {
			VData tData = new VData();
			tData.add(mMap);
			PubSubmit tPubSubmit = new PubSubmit();
			if (!tPubSubmit.submitData(tData, "")) {
				mMap =new MMap();
				System.out.print("保存数据返回");
				// @@错误处理
				this.mErrors.copyAllErrors(tPubSubmit.mErrors);
				CError tError = new CError();
				tError.moduleName = "GetTempFeeSeparateBL";
				tError.functionName = "submitData";
				tError.errorMessage = "数据提交失败!";
				this.mErrors.addOneError(tError);
				return false;
			}
		} catch (Exception ex) {
			mMap =new MMap();
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GetTempFeeSeparateBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		mMap =new MMap();
		return true;
	}
	/**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "GrtLjaPayPSeparateBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    
	public static void main(String[] args) {
		GetLjaPayGSeparateBL tGetLjaPayGSeparateBL = new GetLjaPayGSeparateBL();
		VData cInputData = new VData();
		TransferData tTransferData = new TransferData();
		String tStartDate = "2016-05-01";
		String tEndDate = "2017-03-31";
		String tContno = "";
		tTransferData.setNameAndValue("StartDate", tStartDate);
		tTransferData.setNameAndValue("EndDate", tEndDate);
//		tTransferData.setNameAndValue("ContNo", tContno);
		GlobalInput mGlobalInput = new GlobalInput();
		cInputData.add(tTransferData);
		cInputData.add(mGlobalInput);
		tGetLjaPayGSeparateBL.submitData(cInputData, "");
	}
}
