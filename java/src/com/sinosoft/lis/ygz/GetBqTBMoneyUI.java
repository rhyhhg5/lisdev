package com.sinosoft.lis.ygz;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * 续期、保全价税分离明细实收号提取
 * @author lzy
 *
 */
public class GetBqTBMoneyUI {
	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	
	public boolean submitData(VData cInputData, String cOperate) {
		GetBqTBMoneyBL tGetBqTBMoneyBL=new GetBqTBMoneyBL();
		if(!tGetBqTBMoneyBL.submitData(cInputData, cOperate)){
		      this.mErrors.copyAllErrors(tGetBqTBMoneyBL.mErrors);
		      CError tError = new CError();
		      tError.moduleName = "PEdorADDetailUI";
		      tError.functionName = "submitData";
		      tError.errorMessage = "数据查询失败!";
		      this.mErrors .addOneError(tError) ;
			return false;
		}
		return true;
	}

}
