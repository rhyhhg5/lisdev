package com.sinosoft.lis.ygz;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.vschema.LYPremSeparateDetailSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class GetLPInsureAccFeeTSeparateBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	/** 数据操作字符串 */
	private String mOperate;
	/** 开始日期*/
	private String mStartDate;
	/** 结束日期*/
	private String mEndDate;
	/** 印刷号*/
	private String mPrtNo;
	//业务处理相关变量
	private MMap mMap = new MMap();
	
	/** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
	
	private String mCurrentDate = PubFun.getCurrentDate();
	private String mCurrentTime = PubFun.getCurrentTime();
	public boolean submitData(VData cInputData, String cOperate) {
		System.out.println(mCurrentDate+":开始提取LPInsureAccFeeTrace价税分离数据。");
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		// 进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GetLPInsureAccFeeTSeparateBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败GetLPInsureAccFeeTSeparateBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}

		// 准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}

		// 保存数据
		PubSubmit tPubSubmit = new PubSubmit();

		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			System.out.print("保存数据返回");
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "GetLPInsureAccFeeTSeparateBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mInputData = null;
		System.out.println(mCurrentDate+":提取LPInsureAccFeeTrace价税分离数据结束。");
		return true;
	}

	// 获得输出数据
	private boolean getInputData(VData cInputData) {
		TransferData tTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
		mStartDate = (String) tTransferData.getValueByName("StartDate");
		mEndDate = (String) tTransferData.getValueByName("EndDate");
		mPrtNo = (String) tTransferData.getValueByName("PrtNo");
		mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
    	if(mGlobalInput == null){
    		buildError("getInputData","处理超时，请重新登录。");
    		return false;
    	}
		return true;
	}
	
//  业务处理
	private boolean dealData() {
		LYPremSeparateDetailSet tLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
		YGZ mYGZ = new YGZ();
		ExeSQL tExeSQL = new ExeSQL();
		String tWhereSQL = "";
		String tWherePSQL = "";
		String tWhereTSQL = "";
		String tDealDate = "2016-01-01";
		if(mStartDate != null && !"".equals(mStartDate)){
			tWhereSQL += " and makedate >= '" + mStartDate + "'";
		}
		if(mEndDate != null && !"".equals(mEndDate)){
			tWhereSQL += " and makedate <= '" + mEndDate + "'";
		}
		if("".equals(tWhereSQL)){
			tWhereSQL += "and makedate >= current date - 7 day ";
		}
		if(mPrtNo != null && !"".equals(mPrtNo)){
			tWherePSQL += " and lcp.PrtNo = '" + mPrtNo + "'";
			tWhereTSQL += " and InsureAccFeeTData.PrtNo = '" + mPrtNo + "'";
		}
		
		/**
		 * 保险账户管理履历表数据
		 */
		String tSQL = " select *"
					+ " from ( "
					+ " select "
                    + " trim(lci.EDORNO)||trim(lci.EDORTYPE)||trim(lci.Serialno) as busino,"
					+ " case when lci.grpcontno ='00000000000000000000' then lci.contno else lci.grpcontno end as PolicyNo,"
					+ " lci.riskcode,lci.managecom,"
					+ " lci.fee as paymoney,lci.moneytype,"
					+ " case when othertype='10' then (select distinct edortype from lpedoritem where lci.otherno = edorno)"
					+ " when othertype='3' then (select distinct edortype from lpgrpedoritem where lci.otherno = edorno)" 
					+ " else null end,"
					+ " lci.EDORNO,lci.EDORTYPE,lci.Serialno" 
					+ " from LPInsureAccFeeTrace lci"
					+ " where (lci.MoneyNoTax is null or lci.MoneyNoTax ='') and (lci.MoneyTax is null or lci.MoneyTax ='')"
					+ " and moneytype in ('MF','KF','GL')"
					+ tWhereSQL
					+ " ) as InsureAccFeeTData "
					+ tWhereTSQL
					+ " ";
		SSRS tGPayPerSonSSRS = tExeSQL.execSQL(tSQL);
		if(tGPayPerSonSSRS != null && tGPayPerSonSSRS.getMaxRow() > 0){
			System.out.println("保全保险账户管理履历P表数据==="+tGPayPerSonSSRS.getMaxRow());
			for (int i = 1; i <= tGPayPerSonSSRS.getMaxRow(); i++) {
				PremSeparateDetailHX tPremSeparateDetailHXSchema = new PremSeparateDetailHX();
				tPremSeparateDetailHXSchema.setBusiNo(tGPayPerSonSSRS.GetText(i, 1));
//				tPremSeparateDetailHXSchema.setOtherNo(tGPayPerSonSSRS.GetText(i, 2));
//				tPremSeparateDetailHXSchema.setOtherNoType("2");
				tPremSeparateDetailHXSchema.setPrtNo("000000");
				tPremSeparateDetailHXSchema.setPolicyNo(tGPayPerSonSSRS.GetText(i, 2));
				tPremSeparateDetailHXSchema.setRiskCode(tGPayPerSonSSRS.GetText(i, 3));
				tPremSeparateDetailHXSchema.setManageCom(tGPayPerSonSSRS.GetText(i, 4));			   
				tPremSeparateDetailHXSchema.setPayMoney(tGPayPerSonSSRS.GetText(i, 5));		
				String edortype;
			        edortype= tGPayPerSonSSRS.GetText(i,7);
				String tBusiType = mYGZ.getBusinType(edortype, tGPayPerSonSSRS.GetText(i, 6), tGPayPerSonSSRS.GetText(i, 3));
			//	String tBusiType = mYGZ.getBusinType(tGPayPerSonSSRS.GetText(i, 6), "MF", tGPayPerSonSSRS.GetText(i, 3)); 
				tPremSeparateDetailHXSchema.setBusiType(tBusiType);
	//			tPremSeparateDetailHXSchema.setState("01");
				tPremSeparateDetailHXSchema.setOtherState("24");//从保险帐户管理费履历表提取数据
	//			tPremSeparateDetailHXSchema.setMoneyType("01");
				tPremSeparateDetailHXSchema.setPk1(tGPayPerSonSSRS.GetText(i, 8));//主键EDORNO
				tPremSeparateDetailHXSchema.setPk2(tGPayPerSonSSRS.GetText(i, 9));//主键EDORTYPE
				tPremSeparateDetailHXSchema.setPk3(tGPayPerSonSSRS.GetText(i, 10));//主键Serialno
				tLYPremSeparateDetailSet.add(tPremSeparateDetailHXSchema);
			}
			
		}else{
			System.out.println("保全保险账户管理履历表数据--数据为0！");
		}
		
		//调用价税分离接口,获得分离了费用的价税分离数据
		if(tLYPremSeparateDetailSet.size()>0){
			PremSeparateBL tPremSeparateBL = new PremSeparateBL();
			VData tVData = new VData();
			tVData.add(tLYPremSeparateDetailSet);
			tVData.add(mGlobalInput);
			if(!tPremSeparateBL.getSubmit(tVData, "")){
	    		buildError("dealData", "调用PremSeparateBL接口失败。");
	    		return false;
	    	}
			MMap tMMap = new MMap();
	    	tMMap = tPremSeparateBL.getResult();
	    	
	    	//将价税分离接口返回的数据封装进MMap。
	    	LYPremSeparateDetailSet mLYPremSeparateDetailSet = (LYPremSeparateDetailSet) tMMap.getObjectByObjectName("LYPremSeparateDetailSet", 0);
		    for(int i = 1; i <= mLYPremSeparateDetailSet.size();i++){
		    	PremSeparateDetailHX mPremSeparateDetailHXSchema = (PremSeparateDetailHX) mLYPremSeparateDetailSet.get(i);
		    	if(mPremSeparateDetailHXSchema.getMoneyNoTax() != null && !"".equals(mPremSeparateDetailHXSchema.getMoneyNoTax())){
		    		String moneyNoTax = mPremSeparateDetailHXSchema.getMoneyNoTax();
		    		String moneyTax = mPremSeparateDetailHXSchema.getMoneyTax();
		    		String busiType = mPremSeparateDetailHXSchema.getBusiType();
		    		String taxRate = mPremSeparateDetailHXSchema.getTaxRate();//税率
		    		String sql = "update LPInsureAccFeeTrace  set MoneyNotax = '"+ moneyNoTax +"',"
		    				   	+ " Moneytax = '" + moneyTax+"',"
		    				   	+ " ModifyDate = '" + mCurrentDate +"',"
		    				   	+ " ModifyTime = '" + mCurrentTime +"',"
		    				   	+ " BusiType = '" + busiType +"',"
		    				   	+ " TaxRate = '" + taxRate +"'"
		    				   	+ " where EdorNo = '"+mPremSeparateDetailHXSchema.getPk1()+"'"
		    				   	+ " and EdorType = '"+mPremSeparateDetailHXSchema.getPk2()+"'"
		    				   	+ " and SerialNo = '"+mPremSeparateDetailHXSchema.getPk3()+"'"
		    				   	;
		    		mMap.put(sql, SysConst.UPDATE);
		    	}
	    	}
		}
    	return true;
	}
	/**
	 * 准备后台的数据
	 * @return boolean
	 */
	private boolean prepareOutputData() {
		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GrtLjaPayPSeparateBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
	/**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "GrtLjaPayPSeparateBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    
	public static void main(String[] args) {
		GetLPInsureAccFeeTSeparateBL tGetLPInsureAccFeeTSeparateBL = new GetLPInsureAccFeeTSeparateBL();
		VData cInputData = new VData();
		TransferData tTransferData = new TransferData();
		String tStartDate = "2016-08-01";
		String tEndDate = "2016-08-25";
		String tPrtNo = "";
		tTransferData.setNameAndValue("StartDate", tStartDate);
		tTransferData.setNameAndValue("EndDate", tEndDate);
		tTransferData.setNameAndValue("PrtNo", tPrtNo);
		GlobalInput mGlobalInput = new GlobalInput();
		cInputData.add(tTransferData);
		cInputData.add(mGlobalInput);
		tGetLPInsureAccFeeTSeparateBL.submitData(cInputData, "");
	}
}
