package com.sinosoft.lis.ygz;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.vschema.LYPremSeparateDetailSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class GetLPInsureAccCSeparateBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	/** 数据操作字符串 */
	private String mOperate;
	/** 开始日期 */
	private String mStartDate;
	/** 结束日期 */
	private String mEndDate;
	/** 印刷号*/
	private String mPrtNo;

	// 业务处理相关变量
	MMap mMap = new MMap();
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();
	private String mCurrentDate = PubFun.getCurrentDate();
	private String mCurrentTime = PubFun.getCurrentTime();

	public boolean submitData(VData cInputData, String cOperate) {
		System.out.println(mCurrentDate+":开始提取LPInsureAccClassFee价税分离数据。");
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		if (!dealData()) {
			// @@错误处理
			buildError("submitData",
					"数据处理失败GetLPInsureAccCSeparateBL-->dealData!");
			return false;
		}
		// 准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}
		// 保存数据
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "GetLPInsureAccCSeparateBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mInputData = null;
		System.out.println(mCurrentDate+":提取LPInsureAccClassFee价税分离数据结束。");
		return true;
	}

	private boolean getInputData(VData cInputData) {
		TransferData tTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
		mStartDate = (String) tTransferData.getValueByName("StartDate");
		mEndDate = (String) tTransferData.getValueByName("EndDate");
		mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
	  	if(mGlobalInput == null){
    		buildError("getInputData","处理超时，请重新登录。");
    		return false;
    	}
		return true;
	}

	private boolean dealData() {
		LYPremSeparateDetailSet tLyPremSeparateDetailSet = new LYPremSeparateDetailSet();
		ExeSQL tExeSQL = new ExeSQL();
		String tWhereSQL = "";
		String tWhereTSQL = "";
		if(mStartDate != null && !"".equals(mStartDate)){
			tWhereSQL += " and makedate >= '" + mStartDate + "'";
		}
		if(mEndDate != null && !"".equals(mEndDate)){
			tWhereSQL += " and makedate <= '" + mEndDate + "'";
		}
		if("".equals(tWhereSQL)){
			tWhereSQL += "and makedate >= current date - 7 day ";
		}	
		if(mPrtNo != null && !"".equals(mPrtNo)){
			tWhereTSQL += " and InsureAccClassData.PrtNo = '" + mPrtNo + "'";
		}
		/*保险账户管理费分类表*/
		String tSQL = " select *"
				+ " from ( "
				+ " select  trim(EdorNo)||trim(EdorType)||trim(PolNo) as busino,"
				+ " (case when grpcontno like '000000%' then contno else grpcontno end) as PolicyNo,riskcode, managecom, "
				+ " Fee as paymoney,EdorNo,EdorType,PolNo ,InsuAccNo, PayPlanCode,OtherNo,AccAscription  from LPInsureAccClassFee as lci"
				+ " where (lci.MoneyNoTax is null or lci.MoneyNoTax = '')"
				+ " and (lci.MoneyTax is null or lci.MoneyTax = '')" 
				+ " and lci.fee<> 0 " 
				+ " and riskcode in ('1605','170101','170106','690101','690201') "
				+ " and exists (select 1 from ljagetendorse where grpcontno=lci.grpcontno " 
				+ " and endorsementno=lci.otherno and lci.grppolno=grppolno and feeoperationtype='LQ')"
				+ tWhereSQL
				+ " ) as InsureAccClassData " + tWhereTSQL+" ";
		System.out.println(tSQL);
		SSRS tLCGrpPayASSRS = tExeSQL.execSQL(tSQL);
		YGZ tYgz = new YGZ();
		if (tLCGrpPayASSRS != null && tLCGrpPayASSRS.getMaxRow() > 0) {
			System.out.println("保全保险账户管理费分类P表===数据为： "+tLCGrpPayASSRS.getMaxRow());
			for (int i = 1; i <= tLCGrpPayASSRS.getMaxRow(); i++) {
				PremSeparateDetailHX tPremSeparateDetailHX = new PremSeparateDetailHX();
				tPremSeparateDetailHX.setBusiNo(tLCGrpPayASSRS.GetText(i, 1));
				tPremSeparateDetailHX.setPolicyNo(tLCGrpPayASSRS.GetText(i, 2));
				tPremSeparateDetailHX.setPrtNo("000000");
				tPremSeparateDetailHX.setRiskCode(tLCGrpPayASSRS.GetText(i, 3));
				tPremSeparateDetailHX.setManageCom(tLCGrpPayASSRS.GetText(i, 4));
				tPremSeparateDetailHX.setOtherState("28");
				tPremSeparateDetailHX.setPayMoney(tLCGrpPayASSRS.GetText(i, 5));
//				String tBusiType = tYgz.getBusinType(null, "MF", tLCGrpPayASSRS.GetText(i, 3));
				String tBusiType = tYgz.getBusinType("LQ", "MF", tLCGrpPayASSRS.GetText(i, 3));
				tPremSeparateDetailHX.setBusiType(tBusiType);
				tPremSeparateDetailHX.setPk1(tLCGrpPayASSRS.GetText(i, 6));//主键EdorNo
				tPremSeparateDetailHX.setPk2(tLCGrpPayASSRS.GetText(i, 7));//主键EdorType
				tPremSeparateDetailHX.setPk3(tLCGrpPayASSRS.GetText(i, 8));//主键PolNo
				tPremSeparateDetailHX.setPk4(tLCGrpPayASSRS.GetText(i, 9));//主键InsuAccNo
				tPremSeparateDetailHX.setPk5(tLCGrpPayASSRS.GetText(i, 10));//主键PayPlanCode
				tPremSeparateDetailHX.setPk6(tLCGrpPayASSRS.GetText(i, 11));//主键OtherNo
				tPremSeparateDetailHX.setPk7(tLCGrpPayASSRS.GetText(i, 12));//主键AccAscription
				tLyPremSeparateDetailSet.add(tPremSeparateDetailHX);

			}
		} else {
			System.out.println("保全保险账户管理费分类P表===数据为：0 ");
		}
		if (tLyPremSeparateDetailSet != null && tLyPremSeparateDetailSet.size() > 0) {

			PremSeparateBL tPremSeparateBL = new PremSeparateBL();
			VData tVData = new VData();
			tVData.add(mGlobalInput);
			tVData.add(tLyPremSeparateDetailSet);
			if (!tPremSeparateBL.getSubmit(tVData, "")) {
				this.mErrors.copyAllErrors(tPremSeparateBL.mErrors);
				buildError("dealData", "调用PremSeparateBL接口失败。");
				return false;
			}

			MMap tmMap = new MMap();
			tmMap = tPremSeparateBL.getResult();
			LYPremSeparateDetailSet tPremSeparateDetailSet = (LYPremSeparateDetailSet) tmMap.getObjectByObjectName("LYPremSeparateDetailSet", 0);
			if (tPremSeparateDetailSet != null && tPremSeparateDetailSet.size() > 0) {
				for (int i = 1; i <= tPremSeparateDetailSet.size(); i++) {
					PremSeparateDetailHX tPremSeparateDetailHX = (PremSeparateDetailHX) tPremSeparateDetailSet.get(i);
						if(tPremSeparateDetailHX.getMoneyNoTax() != null && !"".equals(tPremSeparateDetailHX.getMoneyNoTax())){
						String pk = tPremSeparateDetailHX.getPk1()+tPremSeparateDetailHX.getPk2()+
								tPremSeparateDetailHX.getPk3()
						+tPremSeparateDetailHX.getPk4();
						String moneyNoTax = tPremSeparateDetailHX.getMoneyNoTax();
						String moneyTax = tPremSeparateDetailHX.getMoneyTax();
						String busiType = tPremSeparateDetailHX.getBusiType();
						String taxrate = tPremSeparateDetailHX.getTaxRate();
						String sql = "update LPInsureAccClassFee set MoneyNotax = '"+ moneyNoTax+ "',"
								+ " Moneytax = '"+ moneyTax+ "',"
								+ " ModifyDate = '"+ mCurrentDate+ "',"
								+ " ModifyTime = '"+ mCurrentTime+ "',"
							  	+ " BusiType = '"+ busiType+ "',"
							  	+ " Taxrate = '"+ taxrate+ "'"
								+ " where EdorNo = '" + tPremSeparateDetailHX.getPk1() + "'"
								+ " and EdorType = '" + tPremSeparateDetailHX.getPk2() + "'"
								+ " and PolNo = '" + tPremSeparateDetailHX.getPk3() + "'"
								+ " and InsuAccNo = '" + tPremSeparateDetailHX.getPk4() + "'"
								+ " and PayPlanCode = '" + tPremSeparateDetailHX.getPk5() + "'"
								+ " and OtherNo = '" + tPremSeparateDetailHX.getPk6() + "'"
								+ " and AccAscription = '" + tPremSeparateDetailHX.getPk7() + "'"
								;
						mMap.put(sql, SysConst.UPDATE);
					 }
				}
			}
		}
		return true;
	}

	private boolean prepareOutputData() {

		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			buildError("prepareData", "在准备往后层处理所需要的数据时出错。");
			return false;
		}
		return true;
	}

	private void buildError(String sfunName, String serromes) {
		CError tError = new CError();
		tError.moduleName = "GetLPInsureAccCSeparateBL";
		tError.functionName = sfunName;
		tError.errorMessage = serromes;
		this.mErrors.addOneError(tError);
	}

	public static void main(String[] args) {
		GetLPInsureAccCSeparateBL tGetLPInsureAccCSeparateBL = new GetLPInsureAccCSeparateBL();
		VData cInputData = new VData();
		TransferData tTransferData = new TransferData();
		String tStartDate = "2011-03-01";
		String tEndDate = "2016-07-19";
		String tPrtNo = "";
		tTransferData.setNameAndValue("StartDate", tStartDate);
		tTransferData.setNameAndValue("EndDate", tEndDate);
		tTransferData.setNameAndValue("PrtNo", tPrtNo);
		GlobalInput mGlobalInput = new GlobalInput();
		cInputData.add(tTransferData);
		cInputData.add(mGlobalInput);
		tGetLPInsureAccCSeparateBL.submitData(cInputData, "");
	}

}
