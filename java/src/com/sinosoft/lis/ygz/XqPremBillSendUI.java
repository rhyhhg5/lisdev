package com.sinosoft.lis.ygz;


import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class XqPremBillSendUI {

	/**
	 * @param args
	 */
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	
	public boolean submitData(VData cInputData, String cOperate) {
		XqPremBillSendBL tXqPremBillSendBL=new XqPremBillSendBL();
		if(!tXqPremBillSendBL.submitData(cInputData, cOperate)){
		      this.mErrors.copyAllErrors(tXqPremBillSendBL.mErrors);
		      CError tError = new CError();
		      tError.moduleName = "XqPremBillSendUI";
		      tError.functionName = "submitData";
		      tError.errorMessage = "数据查询失败!";
		      this.mErrors .addOneError(tError) ;
			return false;
		}
		return true;
	}

}
