package com.sinosoft.lis.ygz;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.xml.rpc.ParameterMode;

import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.ygz.obj.BillInfo;
import com.sinosoft.lis.ygz.obj.Content;
import com.sinosoft.lis.ygz.obj.SendResult;
import com.sinosoft.lis.ygz.obj.UfinterfaceHead;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * <p>
 * Title: 营改增，价税分离接口
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author lzy
 * @version 1.0
 */

public class PremSeparateInterface {

	public CErrors mErrors = new CErrors();

	private String requestURL = "";
	
	private String reponseURL = "";
	
	private String mBatchNo = "" ;
	
	private String targetEendPoint = "";
	
	private String targetNameSpace = "";
	
	private UfinterfaceHead head = null;
	
	public PremSeparateInterface() {

	}

	/**
	 * 依据请求报文对象集合调用接口
	 * 并获取价税分离结果，返回SendResult对象
	 * 
	 */
	public List<SendResult> callInterface(List<BillInfo> billInfoList){
		//数据准备
		if(!initData()){
			return null;
		}
		//封装请求报文
		Document requestDoc = packageRequest(head, billInfoList);
		//发送请求
		Document responseDoc=request(requestDoc);
		if(null == responseDoc){
			System.out.println("获取返回结果失败");
		}
		//解析返回报文
		List<SendResult> result=parseDoc(responseDoc);
		
		return result;
	}

	/**
	 * create by fangrui
	 * date 2016-08-21
	 * 价税分离接口调用
	 * param document对象，包括报文头UfinterfaceHead和报文体BillInfo
	 * 报文头为参数由服务提供方指定（固定参数）
	 * 报文体参数说明，可详见billinfo的参数描述
	 */
	private Document request(Document requestDoc) {
		String filePath =requestURL + mBatchNo + ".xml";
		try {
			//请求报文保存至应用服务器
			XMLOutputter outPutter = new XMLOutputter("  ", true, "UTF-8");
			FileOutputStream outStream = new FileOutputStream(filePath);
			outPutter.output(requestDoc, outStream);
			
			//读取报文调用接口
			InputStream mIs = new FileInputStream(filePath);
			byte[] mInXmlBytes = InputStreamToBytes(mIs);
			String mInXmlStr = new String(mInXmlBytes, "UTF-8");
//			System.out.println("请求报文："+mInXmlStr);
			
			Service service = new Service();
			Call call = null;
			call = (Call) service.createCall();
			call.setTargetEndpointAddress(new URL(targetEendPoint));
			call.setOperationName("synchronizeData");// 接口方法名称
			call.addParameter("string", org.apache.axis.Constants.XSD_STRING,
					ParameterMode.IN);
			call.setReturnType(org.apache.axis.Constants.XSD_STRING);
			String result =(String) call.invoke(new Object[] { mInXmlStr });
			
//			System.out.println("价税分离返回报文:" + result);
			
			StringReader tInXmlReader = new StringReader(result);
			SAXBuilder tSAXBuilder = new SAXBuilder();
			Document responseDoc = tSAXBuilder.build(tInXmlReader);
			
			String reponseFilePath = reponseURL + mBatchNo + ".xml";
			outStream = new FileOutputStream(reponseFilePath);
			outPutter.output(responseDoc, outStream);
			//modify by zxs 删除发送报文
			File file = new File(filePath);
			if (file.isFile() && file.exists()) {
			file.delete();
			}
			//modify by zxs 删除返回报文
			File resFile = new File(reponseFilePath);
			if (resFile.isFile() && resFile.exists()) {
				resFile.delete();
			}
			return responseDoc;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}
	}
	
	private boolean initData(){
		//获取系统根目录
		String str = "select sysvarvalue from LDSysVar where sysvar='ServerRoot'";
		String url = new ExeSQL().getOneValue(str);
//		String url ="G:/";
		requestURL = url+"ygzxml/request/"+PubFun.getCurrentDate2()+"/";
		reponseURL = url+"ygzxml/reponse/"+PubFun.getCurrentDate2()+"/";
		File mFileDir = new File(requestURL);
		if (!mFileDir.exists()) {
			if (!mFileDir.mkdirs()) {
				CError tError = new CError();
				tError.moduleName = "PremSeparateInterface";
				tError.functionName = "initData";
				tError.errorMessage = "创建目录[" + url.toString() + "]失败！"+ mFileDir.getPath();
				this.mErrors.addOneError(tError);
				return false;
			}
		}
		mFileDir = new File(reponseURL);
		if (!mFileDir.exists()) {
			if (!mFileDir.mkdirs()) {
				CError tError = new CError();
				tError.moduleName = "PremSeparateInterface";
				tError.functionName = "initData";
				tError.errorMessage = "创建目录[" + url.toString() + "]失败！"+ mFileDir.getPath();
				this.mErrors.addOneError(tError);
				return false;
			}
		}
		
		SSRS tSSRS= new ExeSQL().execSQL("select codename,codealias from ldcode where codetype='premseparate' ");
		if(null == tSSRS || tSSRS.MaxRow == 0){
			CError tError = new CError();
			tError.moduleName = "PremSeparateInterface";
			tError.functionName = "initData";
			tError.errorMessage = "获取目标接口信息失败";
			this.mErrors.addOneError(tError);
			return false;
		}
		targetEendPoint=tSSRS.GetText(1, 1);
		targetNameSpace=tSSRS.GetText(1, 2);
		
		
		//为本次请求生成批次号
		String tCurrentDate2=PubFun.getCurrentDate2();
		String tCurrentTime2=PubFun.getCurrentTime2();
		mBatchNo = tCurrentDate2+tCurrentTime2+PubFun1.CreateMaxNo("YGZT"+tCurrentDate2, 6);
		System.out.println("本次请求批次号："+mBatchNo);
		
		//初始化报文头
		head =  new UfinterfaceHead();
		head.setDefault();
		head.setSender("01");
//		head.setFilename(mBatchNo+".xml");
		
		return true;
	}
	
	private static byte[] InputStreamToBytes(InputStream pIns) {
		ByteArrayOutputStream mByteArrayOutputStream = new ByteArrayOutputStream();
		
		try {
			byte[] tBytes = new byte[8*1024];
			for (int tReadSize; -1 != (tReadSize=pIns.read(tBytes)); ) {
				mByteArrayOutputStream.write(tBytes, 0, tReadSize);
			}
		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		} finally {
			try {
				pIns.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		
		return mByteArrayOutputStream.toByteArray();
	}

	private Document packageRequest(UfinterfaceHead head, List billInfoList) {
		if (null == head || billInfoList.size() == 0) {
			return null;
		}
		// 根节点
		Element ufinterface = new Element("ufinterface");
		Document Doc = new Document(ufinterface);
		// 节点属性
		ufinterface.addAttribute("account", head.getAccount());
		ufinterface.addAttribute("billtype", head.getBilltype());
		ufinterface.addAttribute("filename", head.getFilename());
		ufinterface.addAttribute("groupcode", head.getGroupcode());
		ufinterface.addAttribute("isexchange", head.getIsexchange());
		ufinterface.addAttribute("replace", head.getReplace());
		ufinterface.addAttribute("roottag", head.getRoottag());
		ufinterface.addAttribute("sender", head.getSender());

		// 对每笔业务数据进行封装
		for (int i = 0; i < billInfoList.size(); i++) {
			BillInfo tBillInfo = (BillInfo) billInfoList.get(i);
			Element billElement = new Element("bill");
			billElement.addAttribute("id", tBillInfo.getSequenceNo());

			Element billHeadElement = new Element("billhead");
			billHeadElement.addContent(new Element("pk_jiashuifl")
					.setText(tBillInfo.getPk_jiashuifl()));
			billHeadElement.addContent(new Element("id").setText(tBillInfo
					.getId()));
			billHeadElement.addContent(new Element("baodanno")
					.setText(tBillInfo.getBaodanno()));
			billHeadElement.addContent(new Element("shouzhitype")
					.setText(tBillInfo.getShouzhitype()));
			billHeadElement.addContent(new Element("xianzhongcode")
					.setText(tBillInfo.getXianzhongcode()));
			billHeadElement.addContent(new Element("jigoucode")
					.setText(tBillInfo.getJigoucode()));
			billHeadElement.addContent(new Element("hanshuiamount")
					.setText(tBillInfo.getHanshuiamount()));
			billHeadElement.addContent(new Element("def1").setText(tBillInfo
					.getDef1()));
			billHeadElement.addContent(new Element("def2").setText(tBillInfo
					.getDef2()));
			billHeadElement.addContent(new Element("def3").setText(tBillInfo
					.getDef3()));
			billHeadElement.addContent(new Element("def4").setText(tBillInfo
					.getDef4()));
			billHeadElement.addContent(new Element("def5").setText(tBillInfo
					.getDef5()));
			billHeadElement.addContent(new Element("def6").setText(tBillInfo
					.getDef6()));
			billHeadElement.addContent(new Element("def7").setText(tBillInfo
					.getDef7()));
			billHeadElement.addContent(new Element("def8").setText(tBillInfo
					.getDef8()));
			billHeadElement.addContent(new Element("def9").setText(tBillInfo
					.getDef9()));
			billHeadElement.addContent(new Element("def10").setText(tBillInfo
					.getDef10()));
			billHeadElement.addContent(new Element("rate").setText(tBillInfo
					.getRate()));
			billHeadElement.addContent(new Element("wushuiamount")
					.setText(tBillInfo.getWushuiamount()));
			billHeadElement.addContent(new Element("taxes").setText(tBillInfo
					.getTaxes()));

			billElement.addContent(billHeadElement);
			ufinterface.addContent(billElement);
		}

		return Doc;
	}
	
	private List<SendResult> parseDoc(Document document){
		Document tXmlDoc = (Document) document.clone();
		//1、对报文头进行解析
		Element rootElement=tXmlDoc.getRootElement();
		UfinterfaceHead head=new UfinterfaceHead();
		head.setAccount(getValueByAttrName(rootElement,"account"));
		head.setBilltype(getValueByAttrName(rootElement,"billtype"));
		head.setFilename(getValueByAttrName(rootElement,"filename"));
		head.setGroupcode(getValueByAttrName(rootElement,"groupcode"));
		head.setIsexchange(getValueByAttrName(rootElement,"isexchange"));
		head.setReplace(getValueByAttrName(rootElement,"replace"));
		head.setRoottag(getValueByAttrName(rootElement,"roottag"));
		head.setSender(getValueByAttrName(rootElement,"sender"));
		
		
		System.out.println("22222--"+head.getAccount());
		//2、解析报文返回结果
		List resEleList = rootElement.getChildren("sendresult");
		
		List<SendResult> sendResultList =new ArrayList<SendResult>();
		for(int i=0; i<resEleList.size(); i++ ){
			SendResult tSendresult=new SendResult();
//			List<Content> contentList=new ArrayList<Content>();
			Element resultEle=(Element)resEleList.get(i);
			tSendresult.setBillpk(getTextByNodeName(resultEle,"billpk"));
			tSendresult.setBdocid(getTextByNodeName(resultEle,"bdocid"));
			tSendresult.setFilename(getTextByNodeName(resultEle,"filename"));
			tSendresult.setResultcode(getTextByNodeName(resultEle,"resultcode"));
			tSendresult.setResultdescription(getTextByNodeName(resultEle,"resultdescription"));
			List contentEleList=resultEle.getChildren("content");
			if(contentEleList.size()!=1){
				System.out.println("或返回结果与约定报文格式不一致");
			}
			
			Element contentElement=(Element)contentEleList.get(0);
			Content tContent=new Content();
			tContent.setId(getTextByNodeName(contentElement,"id"));
			tContent.setRate(getTextByNodeName(contentElement,"rate"));
			tContent.setTaxes(getTextByNodeName(contentElement,"taxes"));
			tContent.setWushuiamount(getTextByNodeName(contentElement,"wushuiamount"));
			tSendresult.setContent(tContent);
			sendResultList.add(tSendresult);
		}
		
		return sendResultList;
	}
	
	private String getTextByNodeName(Element element,String childName){
		Element child=element.getChild(childName);
		if(null == child){
			return null;
		}
		return child.getTextTrim();
	}
	
	private String getValueByAttrName(Element element,String attrName){
		Attribute attr= element.getAttribute(attrName);
		if(null == attr){
			return null;
		}else{
			return attr.getValue();
		}
	}
	
	public static void main(String[] args) {
		PremSeparateInterface tt=new PremSeparateInterface();
		String mInFilePath = "G:/vtsfile/premSeparate/request/20160422150350000006.xml";
		InputStream mIs;
		try {
			mIs = new FileInputStream(mInFilePath);
			byte[] mInXmlBytes = tt.InputStreamToBytes(mIs);
			String mInXmlStr = new String(mInXmlBytes, "GBK");
			
			StringReader tInXmlReader = new StringReader(mInXmlStr);
			SAXBuilder tSAXBuilder = new SAXBuilder();
			Document responseDoc = tSAXBuilder.build(tInXmlReader);
			
			tt.parseDoc(responseDoc);
		} catch (FileNotFoundException e) {
			
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (JDOMException e) {
			e.printStackTrace();
		}
		
	}

}
