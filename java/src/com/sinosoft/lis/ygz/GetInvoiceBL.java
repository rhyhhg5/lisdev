package com.sinosoft.lis.ygz;

import java.util.HashMap;
import java.util.Iterator;

import com.sinosoft.lis.db.LYPremSeparateDetailDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LYOutPayDetailSchema;
import com.sinosoft.lis.vschema.LYOutPayDetailSet;
import com.sinosoft.lis.vschema.LYPremSeparateDetailSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

public class GetInvoiceBL {

	/** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    private GlobalInput mGlobalInput = null;
    
    private HashMap mHashMap = null;
    
    /**传输到后台处理的map*/
    private MMap mMap = new MMap();
    private VData mVData = new VData();
    
    /** 数据操作字符串 */
    private String mOperate = "";
    
    private String mCurrentDate = PubFun.getCurrentDate();
	private String mCurrentTime = PubFun.getCurrentTime();
    
    public boolean submitData(VData cInputData, String cOperate){
    	
    	if (!getInputData(cInputData, cOperate)){
            return false;
        }

        if (!checkData()){
            return false;
        }

        if (!dealData()){
            return false;
        }
        
        if (!prepareData()){
        	return false;
        }
        
        if(!submit()){
            return false;
        }
        
        return true;
    }
    
    private boolean getInputData(VData cInputData, String cOperate){
    	mOperate = cOperate;
    	
    	mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
    	if(mGlobalInput == null){
    		buildError("getInputData","处理超时，请重新登录。");
    		return false;
    	}
    	
    	mHashMap = (HashMap)cInputData.getObjectByObjectName("HashMap", 0);
    	if(mHashMap == null || mHashMap.size()<=0){
    		buildError("getInputData", "所需参数不完整。");
            return false;
    	}
    	
    	return true;
    }
    
    private boolean checkData(){
    	return true;
    }
    
    private boolean dealData(){
    	
    	LYOutPayDetailSet tLYOutPayDetailSet = new LYOutPayDetailSet();
    	Iterator iter = mHashMap.keySet().iterator();
    	while (iter.hasNext()) {
    		Object key = iter.next();
    		String tPrtNo = (String)mHashMap.get(key);
    		System.out.println("tPrtNo:" + tPrtNo);
    		tLYOutPayDetailSet.add(dealPolicy(tPrtNo));
    	}
    	// 调用发票推送数据接口
    	OutPayUploadBL tOutPayUploadBL = new OutPayUploadBL();
    	VData tVData = new VData();
    	tVData.add(tLYOutPayDetailSet);
    	tVData.add(mGlobalInput);
    	if(!tOutPayUploadBL.getSubmit(tVData, "")){
    		buildError("dealData", "调用OutPayUploadBL接口失败。");
    		return false;
    	}
    	MMap tMMap = new MMap();
    	tMMap = tOutPayUploadBL.getResult();
    	//数据封装，准备提交
    	
    	//该行为不调用发票推送接口时，测试使用， 现在删除掉
//    	mMap.put(tLYOutPayDetailSet,SysConst.INSERT);
    	
    	mMap.add(tMMap);
    	return true;
    }
    
    private boolean prepareData(){
    	
    	mVData.add(mMap);
    	
    	return true;
    }
    
    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit(){
    	
    	PubSubmit p = new PubSubmit();
    	if (!p.submitData(mVData, SysConst.INSERT)){
    		System.out.println("提交数据失败");
    		buildError("submitData", "提交数据失败");
    		return false;
    	}

        return true;
    }
    
    private LYOutPayDetailSet dealPolicy(String cPrtNo){
    	String tPrtNo = cPrtNo;
    	String tSQL = " select contno,appntno,appntname,managecom,cvalidate,"
    				+ " case when signdate is null then null when cvalidate>=signdate then cvalidate else signdate end, "
    				+ " agentcom,(select name from lacom where agentcom=lccont.agentcom),(select groupagentcode from laagent where agentcode=lccont.agentcode) "
    				+ ",'1' "
    				+ ",(select EMail from lcaddress where customerno=lccont.appntno and addressno=(select addressno from lcappnt where contno=lccont.contno)) "
    				+ ",(select Mobile from lcaddress where customerno=lccont.appntno and addressno=(select addressno from lcappnt where contno=lccont.contno)) "
    				+ ",appntidtype,appntidno, "
    				+ "(select Phone from lcaddress where customerno=lccont.appntno and addressno=(select addressno from lcappnt where contno=lccont.contno))"
    				+ ",'','','','','','','' "
    				+ " from lccont where prtno='"+tPrtNo+"' and conttype='1' "
    				+ " union select grpcontno,appntno,grpname,managecom,cvalidate,"
    				+ " case when signdate is null then null when cvalidate>=signdate then cvalidate else signdate end, "
    				+ " agentcom,(select name from lacom where agentcom=lcgrpcont.agentcom),(select groupagentcode from laagent where agentcode=lcgrpcont.agentcode) "
    				+ ",(case when managecom like '8691%' then '3' else '2' end) "
    				+ ",(select E_Mail1 from lcgrpaddress where customerno=lcgrpcont.appntno and addressno=(select addressno from lcgrpappnt where grpcontno=lcgrpcont.grpcontno)) "
    				+ ",(select Mobile1 from lcgrpaddress where customerno=lcgrpcont.appntno and addressno=(select addressno from lcgrpappnt where grpcontno=lcgrpcont.grpcontno)) "
    				+ ",'','', "
    				+"(select phone from lcgrpappnt where grpcontno=lcgrpcont.grpcontno),  "
    				+"(select TaxpayerType from lcgrpappnt where grpcontno=lcgrpcont.grpcontno),  "
    				+"(select TaxNo from lcgrpappnt where grpcontno=lcgrpcont.grpcontno),  "
    				+"(select PostalAddress from lcgrpappnt where grpcontno=lcgrpcont.grpcontno),  "
    				+"(select CustomerBankCode from lcgrpappnt where grpcontno=lcgrpcont.grpcontno),  "
    				+"(select CustomerBankAccNo from lcgrpappnt where grpcontno=lcgrpcont.grpcontno),  "
    				+"(select UnifiedSocialCreditNo from lcgrpappnt where grpcontno=lcgrpcont.grpcontno),  "
    				+"(select OrgancomCode from lcgrpappnt where grpcontno=lcgrpcont.grpcontno)  "
    				+ " from lcgrpcont where prtno='"+tPrtNo+"' "
    				+ " union select contno,appntno,appntname,managecom,cvalidate,"
    				+ " case when signdate is null then null when cvalidate>=signdate then cvalidate else signdate end, "
    				+ " agentcom,(select name from lacom where agentcom=lbcont.agentcom),(select groupagentcode from laagent where agentcode=lbcont.agentcode) "
    				+ ",'1' "
    				+ ",(select EMail from lcaddress where customerno=lbcont.appntno and addressno=(select addressno from lbappnt where contno=lbcont.contno)) "
    				+ ",(select Mobile from lcaddress where customerno=lbcont.appntno and addressno=(select addressno from lbappnt where contno=lbcont.contno)) "
    				+ ",appntidtype,appntidno, "
    				+ "(select Phone from lcaddress where customerno=lbcont.appntno and addressno=(select addressno from lbappnt where contno=lbcont.contno))"
    				+ ",'','','','','','','' "
    				+ " from lbcont where prtno='"+tPrtNo+"' and conttype='1' "
    				+ " union select grpcontno,appntno,grpname,managecom,cvalidate,"
    				+ " case when signdate is null then null when cvalidate>=signdate then cvalidate else signdate end, "
    				+ " agentcom,(select name from lacom where agentcom=lbgrpcont.agentcom),(select groupagentcode from laagent where agentcode=lbgrpcont.agentcode) "
    				+ ",(case when managecom like '8691%' then '3' else '2' end) "
    				+ ",(select E_Mail1 from lcgrpaddress where customerno=lbgrpcont.appntno and addressno=(select addressno from lbgrpappnt where grpcontno=lbgrpcont.grpcontno)) "
    				+ ",(select Mobile1 from lcgrpaddress where customerno=lbgrpcont.appntno and addressno=(select addressno from lbgrpappnt where grpcontno=lbgrpcont.grpcontno)) "
    				+ ",'','', "
    				+"(select phone from lbgrpappnt where grpcontno=lbgrpcont.grpcontno),  "
    				+"(select TaxpayerType from lbgrpappnt where grpcontno=lbgrpcont.grpcontno),  "
    				+"(select TaxNo from lbgrpappnt where grpcontno=lbgrpcont.grpcontno),  "
    				+"(select PostalAddress from lbgrpappnt where grpcontno=lbgrpcont.grpcontno),  "
    				+"(select CustomerBankCode from lbgrpappnt where grpcontno=lbgrpcont.grpcontno),  "
    				+"(select CustomerBankAccNo from lbgrpappnt where grpcontno=lbgrpcont.grpcontno),  "
    				+"(select UnifiedSocialCreditNo from lbgrpappnt where grpcontno=lbgrpcont.grpcontno),  "
    				+"(select OrgancomCode from lbgrpappnt where grpcontno=lbgrpcont.grpcontno) "    				
    				+ " from lbgrpcont where prtno='"+tPrtNo+"' ";
    	SSRS tSSRS = new ExeSQL().execSQL(tSQL);
    	String tPolicyNo = tSSRS.GetText(1, 1);
    	String tCustomerNo = tSSRS.GetText(1, 2);
    	String tCustomerName = tSSRS.GetText(1, 3);
    	String tManageCom = tSSRS.GetText(1, 4);
    	String tCvalidate = tSSRS.GetText(1, 5);
    	String tTransDate = tSSRS.GetText(1, 6);
    	String vdef6 = tSSRS.GetText(1, 7);
    	String vdef7 = tSSRS.GetText(1, 8);
    	String vdef8=tSSRS.GetText(1, 9);
    	String conttype=tSSRS.GetText(1, 10);
    	String tEMail=tSSRS.GetText(1, 11);
    	String tMobile=tSSRS.GetText(1, 12);
    	String tIdtype=tSSRS.GetText(1, 13);
    	String tIdno=tSSRS.GetText(1, 14);
    	String tPhone=tSSRS.GetText(1, 15);
    	String tTaxpayerType = tSSRS.GetText(1, 16);
    	String tTaxNo = tSSRS.GetText(1, 17);
    	String tPostalAddress = tSSRS.GetText(1, 18);
    	String tCustomerBankCode= tSSRS.GetText(1, 19);
    	String tCustomerBankAccNo = tSSRS.GetText(1, 20);
    	String tUnifiedSocialCreditNo=tSSRS.GetText(1, 21);
    	String tOrgancomCode=tSSRS.GetText(1, 22);
    	

    	System.out.println(conttype);
    	String tNoSQL = "select payno from ljapay where incomeno = '"+tPolicyNo+"' ";
		String tMoneyNo = new ExeSQL().getOneValue(tNoSQL);
    	LYOutPayDetailSet tLYOutPayDetailSet = new LYOutPayDetailSet();
    	LYPremSeparateDetailDB tLYPremSeparateDetailDB = new LYPremSeparateDetailDB();
    	LYPremSeparateDetailSet tLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
    	String sql = "select * from lypremseparatedetail lyp where prtno = '"+tPrtNo+"' and otherstate not in ('04','05') "
				+ " and tempfeetype in (select code from ldcode where codetype = 'ygzqyttype') "
				+ " and (otherstate='17' or otherstate = (case when (select count(*)from lypremseparatedetail where prtno = '"+tPrtNo+"' and otherstate not in ('04', '05')"
				+ " and tempfeetype in (select code from ldcode where codetype = 'ygzqyttype') and otherstate = '03'"
				+ " and riskcode not in(select riskcode from lmriskapp where RiskType8 = '6'))>0 then '03'"
				+ " when exists (select 1 from lcgrpcont where prtno = '"+tPrtNo+"' and coinsuranceflag ='1' union select 1 from lbgrpcont where prtno = '"+tPrtNo+"' and coinsuranceflag ='1') then '09' else '01' end ) )"
				+ " and riskcode not in(select riskcode from lmriskapp where RiskType8 = '6')"//add by liyt 2016-07-18 排除全额的，otherstate 不等于11的
				+ " and not exists (select 1 from lyoutpaydetail where busino=lyp.busino and state='02') "
				+ " union"//add by liyt2016-07-18 union 委托管理和特需的
				+ " select * from lypremseparatedetail where prtno = '"+tPrtNo+"' and otherstate ='11'"
				+ " and otherstate = (case when (select count(*)from lypremseparatedetail where prtno = '"+tPrtNo+"' and otherstate not in ('04', '05')"
				+ " and tempfeetype in (select code from ldcode where codetype = 'ygzqyttype') and otherstate = '03'"
				+ " and riskcode not in(select riskcode from lmriskapp where RiskType8 = '6'))>0 then '03' "
				+ " when exists (select 1 from lcgrpcont where prtno = '"+tPrtNo+"' and coinsuranceflag ='1' union select 1 from lbgrpcont where prtno = '"+tPrtNo+"' and coinsuranceflag ='1') then '09' else '01' end )"
				+ " and riskcode in(select riskcode from lmriskapp where RiskType8 = '6' or risktype3 ='7')";
    	System.out.println("=====   "+sql);
    	tLYPremSeparateDetailSet = tLYPremSeparateDetailDB.executeQuery(sql);
		for(int i=1 ; i<=tLYPremSeparateDetailSet.size(); i++){
			LYOutPayDetailSchema tLYOutPayDetailSchema = new LYOutPayDetailSchema();
			// 是否更新客户信息
			tLYOutPayDetailSchema.setisupdate("Y");
			// 集团  传“00”，,00是集团编码
			tLYOutPayDetailSchema.setpkgroup("00");
			tLYOutPayDetailSchema.setgroup("00");
			// 组织
			tLYOutPayDetailSchema.setpkorg(tManageCom);
			tLYOutPayDetailSchema.setorg(tManageCom);
			tLYOutPayDetailSchema.setorgv(tManageCom);
			//客户号
			tLYOutPayDetailSchema.setcode(tCustomerNo);
			
//			发票推送customertype，custtype规则调整，团单推1，个单推2
			if("1".equals(conttype)){
				tLYOutPayDetailSchema.setcustomertype("2");
				// 客户的类型
				tLYOutPayDetailSchema.setcusttype("2");
			}else {
				tLYOutPayDetailSchema.setcustomertype("1");
				// 客户的类型
				tLYOutPayDetailSchema.setcusttype("1");
			}
			//客户名
			tLYOutPayDetailSchema.setname(tCustomerName);
			// 价税分离项目  传“01”用来匹配商品服务档案
			tLYOutPayDetailSchema.setptsitem("01");
			// 数据日期 
			tLYOutPayDetailSchema.setvdata(mCurrentDate);
			// 税率 
			tLYOutPayDetailSchema.settaxrate(tLYPremSeparateDetailSet.get(i).getTaxRate());
			// 不含税金额（原币） 
			tLYOutPayDetailSchema.setoriamt(tLYPremSeparateDetailSet.get(i).getMoneyNoTax());
			// 税额（原币） 
			tLYOutPayDetailSchema.setoritax(tLYPremSeparateDetailSet.get(i).getMoneyTax());
			// 不含税金额（本币） 
			tLYOutPayDetailSchema.setlocalamt(tLYPremSeparateDetailSet.get(i).getMoneyNoTax());
			// 税额（本币） 
			tLYOutPayDetailSchema.setlocaltax(tLYPremSeparateDetailSet.get(i).getMoneyTax());
			// 客户号
			tLYOutPayDetailSchema.setcustcode(tCustomerNo);
			// 客户的名称 
			tLYOutPayDetailSchema.setcustname(tCustomerName);

			// 申报类型  “0”表示正常申报
			tLYOutPayDetailSchema.setdectype("0");
			// 来源的系统  传“01”，代表核心系统
			tLYOutPayDetailSchema.setsrcsystem("01");
			// 金额是否含税  “0”表示含税，“1”表示不含税
			tLYOutPayDetailSchema.settaxtype("0");
			// 收付标志  “0”表示收入，“1”表示支出
			tLYOutPayDetailSchema.setpaymentflag("0");
			// 境内外标示  “0”表示境内，“1”表示境外
			tLYOutPayDetailSchema.setoverseasflag("0");
			// 是否开票   传“Y/N”，必须传
			tLYOutPayDetailSchema.setisbill("Y");
			// 汇总地类型  “0”表示汇总，“1”表示属地，后续纳税
			tLYOutPayDetailSchema.setareatype("1");
			// 凭证id -- 保单号
			tLYOutPayDetailSchema.setvoucherid(tPolicyNo);
			// 交易流水号 
			tLYOutPayDetailSchema.settranserial(tLYPremSeparateDetailSet.get(i).getBusiNo());
			// 交易日期--确认核销日期:取保单的生效与签单日期孰后日期
			tLYOutPayDetailSchema.settrandate(tTransDate);
			// 产品代码--险种编码前四位
			String productCode = tLYPremSeparateDetailSet.get(i).getRiskCode().substring(0,4);
			String productName = new ExeSQL().getOneValue("select riskname from lmriskapp where riskcode='"+tLYPremSeparateDetailSet.get(i).getRiskCode()+"' ");
			tLYOutPayDetailSchema.setprocode(productCode);
			// 交易币种  传“CNY”
			tLYOutPayDetailSchema.settrancurrency("CNY");
			// 备注 -- 保单号+险种号
			tLYOutPayDetailSchema.setvdef1("保单号:"+tPolicyNo+";险种:"+productName);
			// vdef2 -- 保单号
			tLYOutPayDetailSchema.setvdef2(tLYPremSeparateDetailSet.get(i).getPolicyNo());
			// 交易金额 
			tLYOutPayDetailSchema.settranamt(tLYPremSeparateDetailSet.get(i).getPayMoney());
			// 部门
			tLYOutPayDetailSchema.setdeptdoc(tManageCom);
			// 主键 --流水号
			tLYOutPayDetailSchema.setBusiNo(tLYPremSeparateDetailSet.get(i).getBusiNo());
			// 业务类型代码  对于契约来说，都是保费收入吧
			//取值逻辑修改为取价税分离表中该字段
			tLYOutPayDetailSchema.setbusitype(tLYPremSeparateDetailSet.get(i).getBusiType());
			// 业务项目编码--价税分离表的流水号
			tLYOutPayDetailSchema.setbusipk(tLYPremSeparateDetailSet.get(i).getBusiNo());
			// 保单生效日期
			tLYOutPayDetailSchema.setbilleffectivedate(tCvalidate);
			// 投保单号
			tLYOutPayDetailSchema.setinsureno(tPrtNo);
			// 预打发票标识 : 01--预打 02--非预打
			tLYOutPayDetailSchema.setprebilltype("01");
			// 收付类型和收付费号
			if(tMoneyNo!=null || !"".equals(tMoneyNo)){
				// 收付类型 ： 01--收费  02--付费
				tLYOutPayDetailSchema.setmoneytype("01");
				// 收付费号  
				tLYOutPayDetailSchema.setmoneyno(tMoneyNo);
			}
			// makedate,maketime,modifydate,modifytime
			tLYOutPayDetailSchema.setMakeDate(mCurrentDate);
			tLYOutPayDetailSchema.setMakeTime(mCurrentTime);
			tLYOutPayDetailSchema.setModifyDate(mCurrentDate);
			tLYOutPayDetailSchema.setModifyTime(mCurrentTime);
			
			// 邮件说明要新添字段
			tLYOutPayDetailSchema.setinvtype("0"); // 默认为0
			tLYOutPayDetailSchema.setvaddress("0"); // 默认为0
			tLYOutPayDetailSchema.setvsrcsystem("01"); // 默认为01
			tLYOutPayDetailSchema.settaxpayertype("01"); // 默认为01
			tLYOutPayDetailSchema.setenablestate("2"); // 默认为2
			
			//新增代理机构编码vdef6、代理机构名称vdef7 20170531 zqt
			tLYOutPayDetailSchema.setvdef6(vdef6);
			tLYOutPayDetailSchema.setvdef7(vdef7);
			tLYOutPayDetailSchema.setvdef8(vdef8);
			
//			增值税发票推送规则 打印类型billtype 1-纸质发票，2-电子发票  个单推送电子发票，团单推送纸质发票
			if("1".equals(conttype)){
				tLYOutPayDetailSchema.setbilltype("2");
			}else if ("2".equals(conttype)){
				tLYOutPayDetailSchema.setbilltype("1");
			}else if ("3".equals(conttype)){
				tLYOutPayDetailSchema.setbilltype("2");
			}
			
			tLYOutPayDetailSchema.setbuymailbox(tEMail);
			tLYOutPayDetailSchema.setbuyphoneno(tMobile);
//			增值税发票推送规则  证件类型 证件号码
			tLYOutPayDetailSchema.setbuyidnumber(tIdno);
			if(tIdtype==null||"".equals(tIdtype)){
				tLYOutPayDetailSchema.setbuyidtype("");
			}else if ("0".equals(tIdtype)){
				tLYOutPayDetailSchema.setbuyidtype("01");
			}else if ("1".equals(tIdtype)){
				tLYOutPayDetailSchema.setbuyidtype("02");
			}else if ("2".equals(tIdtype)){
				tLYOutPayDetailSchema.setbuyidtype("03");
			}else {
				tLYOutPayDetailSchema.setbuyidtype("07");
			}
			
			//新增核心系统发票推送功能上海分公司增加纳税人识别号字段 20180525 zy
//			因核心纳税人类型为1，2，3，4 但营改增平台只能接收01，02，03类型 保存时默认加0
			if(("8631").equals(tManageCom.substring(0, 4))){
			if(tUnifiedSocialCreditNo!=null&&!"".equals(tUnifiedSocialCreditNo)){
				tLYOutPayDetailSchema.settaxnumber(tUnifiedSocialCreditNo);
			}else if(tTaxNo!=null&&!"".equals(tTaxNo)){
				tLYOutPayDetailSchema.settaxnumber(tTaxNo);
			}else if(tOrgancomCode!=null&&!"".equals(tOrgancomCode)){
				tLYOutPayDetailSchema.settaxnumber(tOrgancomCode);
			}
			if(tTaxpayerType==null||"".equals(tTaxpayerType)){
			}else {
				tLYOutPayDetailSchema.settaxpayertype("0"+tTaxpayerType);
			}
			tLYOutPayDetailSchema.settelephone(tPhone);
			tLYOutPayDetailSchema.setaddressee(tPostalAddress);
			tLYOutPayDetailSchema.setbank(tCustomerBankCode);
			tLYOutPayDetailSchema.setbankaccount(tCustomerBankAccNo);
			tLYOutPayDetailSchema.setmobile(tMobile);
			tLYOutPayDetailSchema.setemail(tEMail);
			}
			
			tLYOutPayDetailSet.add(tLYOutPayDetailSchema);
		}
    	
    	return tLYOutPayDetailSet;
    }
    
    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "GetInvoiceBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    
    public static void main(String[] args) {
		GetInvoiceBL tGetInvoiceBL = new GetInvoiceBL();
		tGetInvoiceBL.dealPolicy("22020210033");
	}
}
