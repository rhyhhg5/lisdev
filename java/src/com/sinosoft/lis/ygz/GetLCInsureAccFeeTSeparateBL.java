package com.sinosoft.lis.ygz;

import java.util.ArrayList;
import java.util.List;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCInsureAccFeeTraceSchema;
import com.sinosoft.lis.vschema.LCInsureAccFeeTraceSet;
import com.sinosoft.lis.vschema.LYPremSeparateDetailSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class GetLCInsureAccFeeTSeparateBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	/** 数据操作字符串 */
	private String mOperate;
	/** 开始日期*/
	private String mStartDate;
	/** 结束日期*/
	private String mEndDate;
	/** 印刷号*/
	private String mContNo;
	//业务处理相关变量
	private MMap mMap = new MMap();
	
	/** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
	
	private String mCurrentDate = PubFun.getCurrentDate();
	private String mCurrentTime = PubFun.getCurrentTime();
	public boolean submitData(VData cInputData, String cOperate) {
		System.out.println(mCurrentDate+":开始提取LcInsureAccFeeTrace价税分离数据。");
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		// 进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GetLCInsureAccFeeTSeparateBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败GetLCInsureAccFeeTSeparateBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}

		mInputData = null;
		System.out.println(mCurrentDate+":提取LcInsureAccFeeTrace价税分离数据结束。");
		return true;
	}

	// 获得输出数据
	private boolean getInputData(VData cInputData) {
		TransferData tTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
		mStartDate = (String) tTransferData.getValueByName("StartDate");
		mEndDate = (String) tTransferData.getValueByName("EndDate");
		mContNo = (String) tTransferData.getValueByName("ContNo");
		mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
    	if(mGlobalInput == null){
    		buildError("getInputData","处理超时，请重新登录。");
    		return false;
    	}
		return true;
	}
	
//  业务处理
	private boolean dealData() {
		YGZ mYGZ = new YGZ();
		ExeSQL tExeSQL = new ExeSQL();
		String tWhereSQL = "";
		String tWhereTSQL = "";
		String tWhereEdorSQL = "";
		if(mStartDate != null && !"".equals(mStartDate)){
			tWhereSQL += "  and (makedate >= '" + mStartDate +"'"+ " or paydate>='" + mStartDate +"')";
			tWhereEdorSQL += " and lpp.confdate >= '" + mStartDate +"'";
		}
		if(mEndDate != null && !"".equals(mEndDate)){
			tWhereSQL += " and (makedate <= '" + mEndDate +"'"+ " or paydate<='" + mEndDate +"')";
			tWhereEdorSQL += " and lpp.confdate <= '" + mEndDate +"'";
		}
		if("".equals(tWhereSQL)&&(mContNo==null || "".equals(mContNo))){
			tWhereSQL += "and (makedate >= current date - 7 day or paydate >= current date - 7 day) ";
			tWhereEdorSQL += " and lpp.confdate >= current date - 7 days ";
		}
		if(mContNo != null && !"".equals(mContNo)){
			tWhereTSQL += " and (lci.contno = '" + mContNo + "' or lci.grpcontno ='" + mContNo + "' )" ;
		}
		
		/**
		 * 保险账户管理履历表数据
		 */
		String tSQL = " select *"
//					+ " from ( "
//					+ " select "
//                    + " trim(lci.Serialno) as busino,"
//					+ " case when lci.grpcontno ='00000000000000000000' then lci.contno else lci.grpcontno end as PolicyNo,"
//					+ " lci.riskcode,lci.managecom,"
//					+ " lci.fee as paymoney,lci.moneytype,"
//					+ " case when othertype='10' then (select distinct edortype from lpedoritem where lci.otherno = edorno)"
//					+ " when othertype='3' then (select distinct edortype from lpgrpedoritem where lci.otherno = edorno)" 
//					+ " else null end" 
					+ " from LcInsureAccFeeTrace lci"
			        + " where (lci.MoneyNoTax is null or lci.MoneyNoTax ='') and (lci.MoneyTax is null or lci.MoneyTax ='')"
					+ " and moneytype in ('MF','KF','GL','LQ')"
					+ " and fee <> 0 "
					+ tWhereSQL
					+ tWhereTSQL
					+ " union "//保全相关轨迹makedate不准，使用工单结案时间提取
					+ " select lci.* "
					+ " from LcInsureAccFeeTrace as lci,lpedorapp lpp"	
					+ " where lci.otherno=lpp.edoracceptno and " 
					+ " (lci.MoneyNoTax is null or lci.MoneyNoTax ='') and (lci.MoneyTax is null or lci.MoneyTax ='')"
					+ " and moneytype in ('MF','KF','GL','LQ')"
					+ " and fee <> 0 "
					+ " and lci.othertype in ('3','10') "
					+ tWhereEdorSQL
					+ tWhereTSQL
					+ " with ur";
		RSWrapper rsWrapper = new RSWrapper();
		LCInsureAccFeeTraceSet tLCInsureAccFeeTraceSet = new LCInsureAccFeeTraceSet();
		if (!rsWrapper.prepareData(tLCInsureAccFeeTraceSet, tSQL)) {
			System.out.println("价税分离明细LcInsureAccFeeTrace数据准备失败！");
			return false;
		}
		do {
			rsWrapper.getData();
			if (null == tLCInsureAccFeeTraceSet
					|| tLCInsureAccFeeTraceSet.size() < 1) {
				break;
			}
			LYPremSeparateDetailSet tLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
			for(int i=1; i<=tLCInsureAccFeeTraceSet.size(); i++){
				PremSeparateDetailHX tPremSeparateDetailHXSchema = new PremSeparateDetailHX();
				String busino ="";
				String policyno = "";
				String tBusiType = "";
				LCInsureAccFeeTraceSchema tLCInsureAccFeeTraceSchema = tLCInsureAccFeeTraceSet.get(i);
				//根据schema获取主键字段名信息
				String[] schemaPK = tLCInsureAccFeeTraceSchema.getPK();
				//将主键的值存储到集合中
				List pklist =new ArrayList();
				for(int k=0; k<schemaPK.length; k++){
					pklist.add(tLCInsureAccFeeTraceSchema.getV(schemaPK[k]));
					busino +=tLCInsureAccFeeTraceSchema.getV(schemaPK[k]);
				}
				tPremSeparateDetailHXSchema.setpKList(pklist);
				System.out.println("busino="+busino);
				if(busino.length()>=20){
					busino = busino.substring(0, 20);
				}
				if(null == tLCInsureAccFeeTraceSchema.getGrpContNo() || "".equals(tLCInsureAccFeeTraceSchema.getGrpContNo()) 
						|| "00000000000000000000".equals(tLCInsureAccFeeTraceSchema.getGrpContNo())){
					policyno = tLCInsureAccFeeTraceSchema.getContNo();
				}else{
					policyno = tLCInsureAccFeeTraceSchema.getGrpContNo();
				}
				tPremSeparateDetailHXSchema.setBusiNo(busino);
				tPremSeparateDetailHXSchema.setOtherNo(policyno);
				tPremSeparateDetailHXSchema.setOtherNoType("2");
				tPremSeparateDetailHXSchema.setPrtNo("000000");
				tPremSeparateDetailHXSchema.setPolicyNo(policyno);
				tPremSeparateDetailHXSchema.setRiskCode(tLCInsureAccFeeTraceSchema.getRiskCode());
				tPremSeparateDetailHXSchema.setManageCom(tLCInsureAccFeeTraceSchema.getManageCom());			  
				tPremSeparateDetailHXSchema.setPayMoney(""+tLCInsureAccFeeTraceSchema.getFee());	
				String otherType = tLCInsureAccFeeTraceSchema.getOtherType();
				String edortype = "";
				if("10".equals(otherType)){
					edortype = tExeSQL.getOneValue("select distinct edortype from lpedoritem where  edorno = '"+tLCInsureAccFeeTraceSchema.getOtherNo()+"'");
				}else if("3".equals(otherType)){
					edortype = tExeSQL.getOneValue("select distinct edortype from lpgrpedoritem where  edorno = '"+tLCInsureAccFeeTraceSchema.getOtherNo()+"'");
				}else{
					edortype = "";
				}
				tBusiType = mYGZ.getBusinType(edortype, tLCInsureAccFeeTraceSchema.getMoneyType(), tLCInsureAccFeeTraceSchema.getRiskCode());
				tPremSeparateDetailHXSchema.setBusiType(tBusiType);
				tPremSeparateDetailHXSchema.setState("01");
				tPremSeparateDetailHXSchema.setOtherState("24");//从保险帐户管理费履历表提取数据
				tPremSeparateDetailHXSchema.setMoneyType("01");
				tLYPremSeparateDetailSet.add(tPremSeparateDetailHXSchema);
			}
			
		//调用价税分离接口,获得分离了费用的价税分离数据
		if(tLYPremSeparateDetailSet.size()>0){
			PremSeparateBL tPremSeparateBL = new PremSeparateBL();
			VData tVData = new VData();
			tVData.add(tLYPremSeparateDetailSet);
			tVData.add(mGlobalInput);
			if(!tPremSeparateBL.getSubmit(tVData, "")){
	    		buildError("dealData", "调用PremSeparateBL接口失败。");
	    		return false;
	    	}
			MMap tMMap = new MMap();
	    	tMMap = tPremSeparateBL.getResult();
	    	
	    	//将价税分离接口返回的数据封装进MMap。
	    	LYPremSeparateDetailSet mLYPremSeparateDetailSet = (LYPremSeparateDetailSet) tMMap.getObjectByObjectName("LYPremSeparateDetailSet", 0);
		    for(int i = 1; i <= mLYPremSeparateDetailSet.size();i++){
		    	PremSeparateDetailHX mPremSeparateDetailHXSchema = (PremSeparateDetailHX) mLYPremSeparateDetailSet.get(i);
		    	if(mPremSeparateDetailHXSchema.getMoneyNoTax() != null && !"".equals(mPremSeparateDetailHXSchema.getMoneyNoTax())){
		    		String moneyNoTax = mPremSeparateDetailHXSchema.getMoneyNoTax();
		    		String moneyTax = mPremSeparateDetailHXSchema.getMoneyTax();
		    		String busiType = mPremSeparateDetailHXSchema.getBusiType();
		    		String taxRate = mPremSeparateDetailHXSchema.getTaxRate();//税率
		    		String whereSQL = " 1=1 ";
		    		LCInsureAccFeeTraceSchema tempLCInsureAccFeeTraceSchema = new LCInsureAccFeeTraceSchema();
		    		//从表的schema中获取主键字段
		    		String[] tempPk = tempLCInsureAccFeeTraceSchema.getPK();
		    		//根据主键的值拼接where条件
		    		List pkList = mPremSeparateDetailHXSchema.getpKList();
		    		for(int k=0; k< tempPk.length; k++){
		    			whereSQL += " and "+tempPk[k]+" = '"+pkList.get(k)+"' ";
		    		}
		    		//lzy 为保险起见加个校验
		    		if(" 1=1 ".equals(whereSQL)){
		    			return false;
		    		}
		    		String sql = "update LcInsureAccFeeTrace  set MoneyNotax = '"+ moneyNoTax +"',"
		    				   	+ " Moneytax = '" + moneyTax+"',"
		    				   	+ " ModifyDate = '" + mCurrentDate +"',"
		    				   	+ " ModifyTime = '" + mCurrentTime +"',"
		    				   	+ " BusiType = '" + busiType +"',"
		    				   	+ " TaxRate = '" + taxRate +"'"
		    				   	+ " where " + whereSQL
		    				   	;
		    		mMap.put(sql, SysConst.UPDATE);
		    	}
	    	}
		}

		//在这里提交
		if(!submit()){
			System.out.println("数据执行失败，运行下一批次");
		}
		
		}while (null != tLCInsureAccFeeTraceSet
			&& tLCInsureAccFeeTraceSet.size() > 0);
    	return true;
	}
	/**
	 * 准备后台的数据
	 * @return boolean
	 */
	private boolean submit() {
		try {
			VData tData = new VData();
			tData.add(mMap);
			PubSubmit tPubSubmit = new PubSubmit();
			if (!tPubSubmit.submitData(tData, "")) {
				mMap =new MMap();
				System.out.print("保存数据返回");
				// @@错误处理
				this.mErrors.copyAllErrors(tPubSubmit.mErrors);
				CError tError = new CError();
				tError.moduleName = "GetTempFeeSeparateBL";
				tError.functionName = "submitData";
				tError.errorMessage = "数据提交失败!";
				this.mErrors.addOneError(tError);
				return false;
			}
		} catch (Exception ex) {
			mMap =new MMap();
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GetTempFeeSeparateBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		mMap =new MMap();
		return true;
	}
	
	/**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "GrtLjaPayPSeparateBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    
	public static void main(String[] args) {
		GetLCInsureAccFeeTSeparateBL tGetLCInsureAccFeeTSeparateBL = new GetLCInsureAccFeeTSeparateBL();
		VData cInputData = new VData();
		TransferData tTransferData = new TransferData();
		String tStartDate = "2016-04-02";
		String tEndDate = "2016-09-12";
		String tContNo = "";
		tTransferData.setNameAndValue("StartDate", tStartDate);
		tTransferData.setNameAndValue("EndDate", tEndDate);
		tTransferData.setNameAndValue("ContNo", tContNo);
		GlobalInput mGlobalInput = new GlobalInput();
		cInputData.add(tTransferData);
		cInputData.add(mGlobalInput);
		tGetLCInsureAccFeeTSeparateBL.submitData(cInputData, "");
	}
}
