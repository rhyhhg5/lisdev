package com.sinosoft.lis.ygz;

import java.util.HashMap;
import java.util.Iterator;

import com.sinosoft.lis.db.LJAPayGrpDB;
import com.sinosoft.lis.db.LJAPayPersonDB;
import com.sinosoft.lis.db.LYPremSeparateDetailDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LYPremSeparateDetailSchema;
import com.sinosoft.lis.vschema.LJAPayGrpSet;
import com.sinosoft.lis.vschema.LJAPayPersonSet;
import com.sinosoft.lis.vschema.LYPremSeparateDetailSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
/**
 * 
 * 通过页面推送的保单，获取其价税分离后的数据
 *
 */
public class GetPayPGMoneyBL {
	
	/** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    private GlobalInput mGlobalInput = null;

    private LYPremSeparateDetailSet mLYPremSeparateDetailSet = null;
    
    private HashMap mHashMap = null;

    /**传输到后台处理的map*/
    private MMap mMap = new MMap();
    
    /** 数据操作字符串 */
    private String mOperate = "";
    
    private String mCurrentDate = PubFun.getCurrentDate();
	private String mCurrentTime = PubFun.getCurrentTime();
    
    public boolean submitData(VData cInputData, String cOperate){
    	
    	//准备数据
        if(getSubmitMap(cInputData, cOperate) == null){
            return false;
        }
        
        //数据提交
        if(!submit()){
            return false;
        }
        
        return true;
    }
    
    public MMap getSubmitMap(VData cInputData, String cOperate){
        if (!getInputData(cInputData, cOperate)){
            return null;
        }

        if (!checkData()){
            return null;
        }

        if (!dealData()){
            return null;
        }

        return mMap;
    }
    
    private boolean getInputData(VData cInputData, String cOperate){
    	
    	mOperate = cOperate;
    	if(!"1".equals(mOperate) && !"2".equals(mOperate)){
    		buildError("getInputData","不识别的操作符。");
    		return false;
    	}
    	
    	mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
    	if(mGlobalInput == null){
    		buildError("getInputData","处理超时，请重新登录。");
    		return false;
    	}
    	
    	mHashMap = (HashMap)cInputData.getObjectByObjectName("HashMap", 0);
    	if(mHashMap == null || mHashMap.size()<=0){
    		buildError("getInputData", "所需参数不完整。");
            return false;
    	}
    	
    	return true;
    }
    
    private boolean checkData(){
    	return true;
    }
    
    private boolean dealData(){
    	/* 用于封装调用价税分离接口的数据 */
    	LYPremSeparateDetailSet totalLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
    	LJAPayPersonSet totalLJAPayPersonSet = new LJAPayPersonSet();
    	LJAPayGrpSet totalLJAPayGrpSet = new LJAPayGrpSet();
    	//1、将从前面传过来的保单，封装在价税分离表 callLYPremSeparateDetailSet 中。
    	Iterator iter = mHashMap.keySet().iterator();
    	String flag = "";
    	while (iter.hasNext()) {
    		Object key = iter.next();
    		String tContNo = (String)mHashMap.get(key);
    		System.out.println("tContNo:" + tContNo);
    		LJAPayPersonSet aLJAPayPersonSet = new LJAPayPersonSet();
    		if(mOperate.equals("1")){//个单
    			aLJAPayPersonSet = dealPolicyG(tContNo);
    			totalLJAPayPersonSet.add(aLJAPayPersonSet);
    		}
    		LYPremSeparateDetailSet aLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
    		if(mOperate.equals("2")){//团单
    			String sql = "select appflag from lcgrpcont where grpcontno ='"+tContNo+"'";
    			String tAppflag = new ExeSQL().getOneValue(sql);
    			flag = tAppflag;
    			if(tAppflag.equals("1")){
    				LJAPayGrpSet aLJAPayGrpSet = new LJAPayGrpSet();
    				aLJAPayGrpSet = dealPolicyGrpT(tContNo);
    				totalLJAPayGrpSet.add(aLJAPayGrpSet);
    			}else{
    				aLYPremSeparateDetailSet = dealPolicyT(tContNo);
    			}
    			totalLYPremSeparateDetailSet.add(aLYPremSeparateDetailSet);
    		}
    	}
    	if(mOperate.equals("2")&&!"1".equals(flag)){
	    	//2、筛选数据:对已经分离了费用的数据，不调用接口，否则调用个单
	    	LYPremSeparateDetailSet callLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
	    	LYPremSeparateDetailSet noCallLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
	    	if(totalLYPremSeparateDetailSet!=null && totalLYPremSeparateDetailSet.size()>0){
	    		for(int i=1; i<=totalLYPremSeparateDetailSet.size(); i++){
	    			LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
	    			tLYPremSeparateDetailSchema = totalLYPremSeparateDetailSet.get(i);
	    			//state:[01:提取;02:价税分离]
	    			//对于已经价税分离过的数据(state='02')，不再调用价税分离接口;否则(state='01'),准备调用价税分离接口的数据
	    			if(tLYPremSeparateDetailSchema.getState()!=null && "02".equals(tLYPremSeparateDetailSchema.getState())){
	    				noCallLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
	    			}else if("01".equals(tLYPremSeparateDetailSchema.getState())){
	    				tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
	    				tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
	    				callLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
	    			}else{
	    				System.out.println("数据状态未知！");
	    			}
	    		}
	    	}else{
	    		System.out.println("没有获得数据！");
	    	}   
	    	
	    	//3、调用价税分离接口,获得分离了费用的价税分离数据
	    	PremSeparateBL tPremSeparateBL = new PremSeparateBL();
	    	VData tVData = new VData();
	    	tVData.add(callLYPremSeparateDetailSet);
	    	tVData.add(mGlobalInput);
	    	if(!tPremSeparateBL.getSubmit(tVData, "")){
	    		buildError("dealData", "调用PremSeparateBL接口失败。");
	    		return false;
	    	}
	    	MMap tMMap = new MMap();
	    	tMMap = tPremSeparateBL.getResult();
	    	//将价税分离接口返回的数据封装进MMap。
	    	mMap.add(tMMap);
    	}
    	return true;
    }
    
    private LYPremSeparateDetailSet dealPolicyT(String cContNo) {
    	LYPremSeparateDetailSet tLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
    	String tContNo = cContNo;
    	SSRS tSSRS = new SSRS();
		tSSRS = new ExeSQL().execSQL("select grpcontno,riskcode,prem,managecom,prtno from lcgrppol where grpcontno ='"+tContNo+"'");
		
		for(int i=1; i<=tSSRS.MaxRow; i++){
			LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
			tLYPremSeparateDetailSchema.setBusiNo(tSSRS.GetText(i, 5)+"T"+tSSRS.GetText(i, 2));
			tLYPremSeparateDetailSchema.setBusiType("01");//01，保单保费;02，工本费
    		tLYPremSeparateDetailSchema.setTempFeeNo(tSSRS.GetText(i, 5)+"801");
    		tLYPremSeparateDetailSchema.setTempFeeType("N");//此时尚未有暂收数据，新定义类型'N'
    		tLYPremSeparateDetailSchema.setPayMoney(tSSRS.GetText(i, 3));
    		tLYPremSeparateDetailSchema.setPolicyNo(tSSRS.GetText(i, 1));
    		tLYPremSeparateDetailSchema.setRiskCode(tSSRS.GetText(i, 2));
    		tLYPremSeparateDetailSchema.setPrtNo(tSSRS.GetText(i, 5));
    		tLYPremSeparateDetailSchema.setOtherNo(tSSRS.GetText(i, 5));
    		tLYPremSeparateDetailSchema.setOtherNoType("5");//5表示团单印刷号
    		tLYPremSeparateDetailSchema.setState("01");//01：提取;02：价税分离
    		tLYPremSeparateDetailSchema.setOtherState("03");//01：从暂收生成的数据;02：从实付生成的数据;03：预打发票生成的数据
    		tLYPremSeparateDetailSchema.setMoneyType("1");//1 ---收费;2 ---付费
    		tLYPremSeparateDetailSchema.setManageCom(tSSRS.GetText(i, 4));
    		tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
    		tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
    		tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
    		tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
    		tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
		}
		return tLYPremSeparateDetailSet;
	}

    private LJAPayGrpSet dealPolicyGrpT(String cContNo) {
    	LJAPayGrpSet tLJAPayGrpSet = new LJAPayGrpSet();
    	LJAPayGrpDB tLJAPayGrpDB = new LJAPayGrpDB();
    	String tContNo = cContNo;
    	String sql = "select * from Ljapaygrp where (MoneyNoTax is not null and MoneyNoTax != '') and  grpcontno = '"+ tContNo +"'";
		System.out.println("====GetPayPGMoneyBL 团单 ==    "+sql);
		tLJAPayGrpSet = tLJAPayGrpDB.executeQuery(sql);
		if(tLJAPayGrpSet == null || tLJAPayGrpSet.size() < 1){
			//调用GetLjaPayGSeparateBL价税分离
			GetLjaPayGSeparateBL tGetLjaPayGSeparateBL = new GetLjaPayGSeparateBL();
			VData cInputData = new VData();
			TransferData tTransferData = new TransferData();
			tTransferData.setNameAndValue("StartDate", "2016-05-01");
			tTransferData.setNameAndValue("ContNo", tContNo);
			cInputData.add(tTransferData);
			GlobalInput mGlobalInput = new GlobalInput();
			cInputData.add(mGlobalInput);
			//如果返回为false,表示没有成功获得数据提交数据库
			if(!tGetLjaPayGSeparateBL.submitData(cInputData, "")){
				buildError("dealPolicy", "保单号为:"+tContNo+"的个单没有成功生成价税分离数据。");
                return null;
			}else{
				tLJAPayGrpSet.add(tLJAPayGrpDB.executeQuery(sql));
			}
		}
		return tLJAPayGrpSet;
	}
    
	private LJAPayPersonSet dealPolicyG(String cContNo) {
		String tContNo = cContNo;
		LJAPayPersonSet tLJAPayPersonSet = new LJAPayPersonSet();
		LJAPayPersonDB tLJAPayPersonDB = new LJAPayPersonDB();
		String sql = "select * from LJAPayPerson where (MoneyNoTax is not null and MoneyNoTax !='') and Contno = '"+ tContNo +"'";
		System.out.println("====GetPayPGMoneyBL 个单==    "+sql);
		tLJAPayPersonSet = tLJAPayPersonDB.executeQuery(sql);
		if(tLJAPayPersonSet == null || tLJAPayPersonSet.size() < 1 ){
			//调用GetLjaPayPSeparateBL价税分离
			GetLjaPayPSeparateBL tGetLjaPayPSeparateBL = new GetLjaPayPSeparateBL();
			VData cInputData = new VData();
			TransferData tTransferData = new TransferData();
			tTransferData.setNameAndValue("StartDate", "2016-05-01");
			tTransferData.setNameAndValue("ContNo", tContNo);
			cInputData.add(tTransferData);
			GlobalInput mGlobalInput = new GlobalInput();
			cInputData.add(mGlobalInput);
			//如果返回为false,表示没有成功获得数据提交数据库
			if(!tGetLjaPayPSeparateBL.submitData(cInputData, "")){
				buildError("dealPolicy", "保单号为:"+tContNo+"的个单没有成功生成价税分离数据。");
                return null;
			}else{
				tLJAPayPersonSet.add(tLJAPayPersonDB.executeQuery(sql));
			}
		}
		return tLJAPayPersonSet;
	}

    
    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "GetPayMoneyBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    
    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit(){
    	
    	VData data = new VData();
    	data.add(mMap);
    		
    	PubSubmit p = new PubSubmit();
    	if (!p.submitData(data, SysConst.DELETE_AND_INSERT)){
    		System.out.println("提交数据失败");
    		buildError("submitData", "提交数据失败");
    		return false;
    	}

        return true;
    }

    public VData getResult(){
        return mResult;
    }
}
