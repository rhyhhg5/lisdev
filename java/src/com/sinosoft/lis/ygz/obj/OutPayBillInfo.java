package com.sinosoft.lis.ygz.obj;

import java.util.Date;

/**
 * <p>
 * Title: 营改增，开票接口实体类
 * </p>
 * <p>
 * Description:请求报文中的bill节点及子节点billhead信息的封装
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author Alex
 * @version 1.0
 */

public class OutPayBillInfo {

	public OutPayBillInfo() {

	}

	/**
	 * bill id
	 */
	private String SequenceNo;
	/**
	 * 以下字段均为 billhead 节点的字段
	 */
	
	private String pk_custinfo;
	
	private String item;
	
	private String pk_custinfo1;
	/**
	 * 是否更新客户信息
	 */
	private String isupdate;
	/**
	 *  集团
	 */
	private String pk_pkgroup;
	/**
	 * 组织
	 */
	private String pk_pkorg;
	/**
	 * 客户编码
	 */
	private String code;
	/**
	 * 客户名称
	 */
	private String name;
	/**
	 * 创建人
	 */
	private String creator;
	/**
	 * 创建时间
	 */
	private String creationtime;
	/**
	 * 修改人
	 */
	private String modifier;
	/**
	 * 修改时间
	 */
	private String modifiedtime;
	/**
	 * 纳税人类型
	 */
	private String taxpayertype;
	/**
	 * 开票类型
	 */
	private String invtype;
	/**
	 * 开票周期
	 */
	private String makefreq;
	/**
	 * 开票间隔
	 */
	private String makespace;
	/**
	 * 联系电话
	 */
	private String telephone;
	/**
	 * 手机号码
	 */
	private String mobile;
	/**
	 * 寄送方式
	 */
	private String sendway;
	/**
	 * 发票收件人
	 */
	private String addressee;
	/**
	 * 邮编
	 */
	private String postcode;
	/**
	 * 邮寄地址
	 */
	private String mailingaddress;
	/**
	 * 客户类型
	 */
	private String customertype;
	/**
	 * 所属国家
	 */
	private String country;
	/**
	 * 法人名称
	 */
	private String legalperson;
	/**
	 * 客户税号
	 */
	private String taxnumber;
	/**
	 * 开户银行
	 */
	private String bank;
	/**
	 * 银行账户
	 */
	private String bankaccount;
	/**
	 * 开票日期
	 */
	private String makeinvdate;
	/**
	 * 收票人
	 */
	private String receiveperson;
	/**
	 * 是否开具纸质发票
	 */
	private String ispaperyinv;
	/**
	 * 启用状态
	 */
	private String enablestate;
	
	/**
	 * 是否允许提前开票
	 */
	private String isearly;
	/**
	 * 开票方式
	 */
	private String billmode;
	/**
	 * 开票地点
	 */
	private String vaddress;
	/**
	 * 微信openid（电子）
	 */
	private String openid;
	/**
	 * 发票通账号（电子）
	 */
	private String billaccount;
	/**
	 * 电子邮箱（电子）
	 */
	private String email;
	/**
	 * 个险客户编号
	 */
	private String vpsnid;
	/**
	 * 保单生效日期(个险)
	 */
	private String veffectdate;
	/**
	 * 保单开至日期(个险)
	 */
	private String vautodate;
	/**
	 * 投递省份
	 */
	private String vmailprovince;
	/**
	 * 投递城市
	 */
	private String vmailcity;
	/**
	 * 来源系统
	 */
	private String vsrcsystem;
	/**
	 * 来源数据id
	 */
	private String vsrcrowid;
	/**
	 * 所属集团
	 */
	private String pk_group;
	/**
	 * 所属组织
	 */
	private String pk_org;
	/**
	 * 所属组织版本
	 */
	private String pk_org_v;
	/**
	 * 计算流水号
	 */
	private String calnum;
	/**
	 * 计算日期
	 */
	private String caldate;
	/**
	 * 数据日期
	 */
	private String vdata;
	/**
	 * 汇率
	 */
	private String rate;
	/**
	 * 价税分离项目
	 */
	private String pk_ptsitem;
	/**
	 * 计税方法
	 */
	private int methodcal;
	/**
	 * 是否即征即退
	 */
	private String drawback;
	/**
	 * 税率
	 */
	private String taxrate;
	/**
	 * 不含税金额（原币）
	 */
	private String oriamt;
	/**
	 * 税额（原币）
	 */
	private String oritax;
	/**
	 * 不含税金额（本币）
	 */
	private String localamt;
	/**
	 * 税额（本币）
	 */
	private String localtax;
	/**
	 * 申报日期
	 */
	private String decldate;
	/**
	 * 业务日期
	 */
	private String busidate;
	/**
	 * 客户号
	 */
	private String custcode;
	/**
	 * 客户名称
	 */
	private String custname;
	/**
	 * 客户类型
	 */
	private String custtype;
	/**
	 * 申报类型
	 */
	private int dectype;
	/**
	 * 来源系统
	 */
	private String srcsystem;
	/**
	 * 凭证id
	 */
	private String voucherid;
	/**
	 * 交易流水号
	 */
	private String transerial;
	/**
	 * 交易批次号
	 */
	private String tranbatch;
	/**
	 * 交易日期
	 */
	private String trandate;
	/**
	 * 交易时间
	 */
	private String trantime;
	/**
	 * 交易机构
	 */
	private String tranorg;
	/**
	 * 交易渠道
	 */
	private String tranchannel;
	/**
	 * 交易柜员
	 */
	private String trancounte;
	/**
	 * 客户经理
	 */
	private String custmanager;
	/**
	 * 交易代码
	 */
	private String trantype;
	/**
	 * 产品代码
	 */
	private String procode;
	/**
	 * 业务类型代码
	 */
	private String busino;
	/**
	 * 交易摘要
	 */
	private String tranremark;
	/**
	 * 账户号
	 */
	private String accountno;
	/**
	 * 交易币种
	 */
	private String trancurrency;
	/**
	 * 交易金额
	 */
	private String tranamt;
	/**
	 * 金额是否含税
	 */
	private int taxtype;
	/**
	 * 被冲抹标识
	 */
	private String cztype;
	/**
	 * 冲抹原交易日期
	 */
	private String czolddate;
	/**
	 * 是否冲正
	 */
	private String iscurrent;
	/**
	 * 冲抹源交易流水号
	 */
	private String czbusino;
	/**
	 * 会计机构
	 */
	private String accno;
	/**
	 * 借贷标志
	 */
	private int loanflag;
	/**
	 * 科目编码
	 */
	private String acccode;
	/**
	 * 收付标志
	 */
	private int paymentflag;
	/**
	 * 摊销委托编号
	 */
	private String amortno;
	/**
	 * 中间业务合同编号
	 */
	private String midcontno;
	/**
	 * 是否我行账号
	 */
	private String isinnerbank;
	/**
	 * 境内外标示
	 */
	private int overseasflag;
	/**
	 * 税金计算方式
	 */
	private int taxcalmethod;
	/**
	 * 创建人
	 */
	private String creator1;
	/**
	 * 创建时间
	 */
	private String creationtime1;
	/**
	 * 修改人
	 */
	private String modifier1;
	/**
	 * 修改时间
	 */
	private String modifiedtime1;
	/**
	 * 证券类别
	 */
	private String negtype;
	/**
	 * 交易场所
	 */
	private String tranplace;
	/**
	 *部门 
	 */
	private String deptdoc;
	/**
	 * 收支确认类型
	 */
	private int confimtype;
	/**
	 * 是否价税分离
	 */
	private String ispts;
	/**
	 * 是否开票
	 */
	private String isbill;
	/**
	 * 汇总地类型
	 */
	private int areatype;
	/**
	 * 是否来自异常表
	 */
	private String isptserror;
	/**
	 * 接收日期
	 */
	private String receivedate;
	/**
	 * 是否允许提前开票
	 */
	private String isalloweadvance;
	/**
	 * 业务交易流水号
	 */
	private String busipk;
	/**
	 * 业务项目编码
	 */
	private String busiitem;
	/**
	 * 财务险种
	 */
	private String fininsance;
	/**
	 * 财务险别
	 */
	private String fininstype;
	/**
	 * 纳税类型
	 */
	private String istaxfree;
	/**
	 * 单位
	 */
	private String bunit;
	/**
	 * 单价
	 */
	private String bunivalent;
	/**
	 * 数量
	 */
	private String bnumber;
	/**
	 * 发票类型
	 */
	private String billtype;
	/**
	 * 允许开票日期
	 */
	private String billdate;
	/**
	 * 商品服务档案/应税服务项目
	 */
	private String servicedoc;
	/**
	 * 是否历史保单
	 */
	private String ishistory;
	/**
	 * 见费情况
	 */
	private String ismoneny;
	/**
	 * 投保单号
	 */
	private String insureno;
	/**
	 * 是否首期保费
	 */
	private String isfirst;
	/**
	 * 个人保单号
	 */
	private String perspolicyno;
	/**
	 * 个人保单保费核销日期
	 */
	private String ppveridate;
	/**
	 * 保费开始日期
	 */
	private String billstartdate;
	/**
	 * 保费结束日期
	 */
	private String billenddate;
	/**
	 * 缴费方式
	 */
	private String paytype;
	/**
	 * 是否垫交保费
	 */
	private String ismat;
	/**
	 * 是否委托缴费
	 */
	private String isentrust;
	/**
	 * 保单生效日期
	 */
	private String billeffectivedate;
	/**
	 * 确认收入日期
	 */
	private String recoincomdate;
	/**
	 * 开票标识
	 */
	private String billfalg;
	/**
	 * 收费冲销日期
	 */
	private String offsetdate;
	/**
	 * 批单号（保全）
	 */
	private String batchno;
	/**
	 * 是否视同销售
	 */
	private String issale;
	

	/**
	 * 备注
	 */
	private String vdef1;
	/**
	 * 保单号
	 */
	private String vdef2;
	/**
	 * 代理机构编码
	 */
	private String vdef6;
	/**
	 * 代理机构编码
	 */
	private String vdef7;
	/**
	 * groupagentcode
	 */
	private String vdef8;
	/**
	 * 电子发票客户邮箱
	 */
	private String buymailbox;
	/**
	 * 电子发票客户手机号
	 */
	private String buyphoneno;
	/**
	 * 证件类型
	 */
	private String buyidtype;
	/**
	 * 证件号码
	 */
	private String buyidnumber;
	
	public String getBuyidtype() {
		return buyidtype;
	}

	public void setBuyidtype(String buyidtype) {
		this.buyidtype = buyidtype;
	}

	public String getBuyidnumber() {
		return buyidnumber;
	}

	public void setBuyidnumber(String buyidnumber) {
		this.buyidnumber = buyidnumber;
	}

	public String getVdef8() {
		return vdef8;
	}

	public void setVdef8(String vdef8) {
		this.vdef8 = vdef8;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public String getPk_custinfo1() {
		return pk_custinfo1;
	}

	public void setPk_custinfo1(String pk_custinfo1) {
		this.pk_custinfo1 = pk_custinfo1;
	}

	public String getIsupdate() {
		return isupdate;
	}

	public void setIsupdate(String isupdate) {
		this.isupdate = isupdate;
	}

	public String getPk_pkgroup() {
		return pk_pkgroup;
	}

	public void setPk_pkgroup(String pk_pkgroup) {
		this.pk_pkgroup = pk_pkgroup;
	}

	public String getPk_pkorg() {
		return pk_pkorg;
	}

	public void setPk_pkorg(String pk_pkorg) {
		this.pk_pkorg = pk_pkorg;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getCreationtime() {
		return creationtime;
	}

	public void setCreationtime(String creationtime) {
		this.creationtime = creationtime;
	}

	public String getModifier() {
		return modifier;
	}

	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	public String getModifiedtime() {
		return modifiedtime;
	}

	public void setModifiedtime(String modifiedtime) {
		this.modifiedtime = modifiedtime;
	}

	public String getTaxpayertype() {
		return taxpayertype;
	}

	public void setTaxpayertype(String taxpayertype) {
		this.taxpayertype = taxpayertype;
	}

	public String getInvtype() {
		return invtype;
	}

	public void setInvtype(String invtype) {
		this.invtype = invtype;
	}

	public String getMakefreq() {
		return makefreq;
	}

	public void setMakefreq(String makefreq) {
		this.makefreq = makefreq;
	}

	public String getMakespace() {
		return makespace;
	}

	public void setMakespace(String makespace) {
		this.makespace = makespace;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getSendway() {
		return sendway;
	}

	public void setSendway(String sendway) {
		this.sendway = sendway;
	}

	public String getAddressee() {
		return addressee;
	}

	public void setAddressee(String addressee) {
		this.addressee = addressee;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getMailingaddress() {
		return mailingaddress;
	}

	public void setMailingaddress(String mailingaddress) {
		this.mailingaddress = mailingaddress;
	}

	public String getCustomertype() {
		return customertype;
	}

	public void setCustomertype(String customertype) {
		this.customertype = customertype;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getLegalperson() {
		return legalperson;
	}

	public void setLegalperson(String legalperson) {
		this.legalperson = legalperson;
	}

	public String getTaxnumber() {
		return taxnumber;
	}

	public void setTaxnumber(String taxnumber) {
		this.taxnumber = taxnumber;
	}

	public String getBank() {
		return bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public String getBankaccount() {
		return bankaccount;
	}

	public void setBankaccount(String bankaccount) {
		this.bankaccount = bankaccount;
	}

	public String getMakeinvdate() {
		return makeinvdate;
	}

	public void setMakeinvdate(String makeinvdate) {
		this.makeinvdate = makeinvdate;
	}

	public String getReceiveperson() {
		return receiveperson;
	}

	public void setReceiveperson(String receiveperson) {
		this.receiveperson = receiveperson;
	}

	public String getIspaperyinv() {
		return ispaperyinv;
	}

	public void setIspaperyinv(String ispaperyinv) {
		this.ispaperyinv = ispaperyinv;
	}

	public String getEnablestate() {
		return enablestate;
	}

	public void setEnablestate(String enablestate) {
		this.enablestate = enablestate;
	}

	public String getSequenceNo() {
		return SequenceNo;
	}
	
	public void setSequenceNo(String sequenceNo) {
		SequenceNo = sequenceNo;
	}
	
	public String getPk_custinfo() {
		return pk_custinfo;
	}
	
	public void setPk_custinfo(String pkCustinfo) {
		pk_custinfo = pkCustinfo;
	}

	public String getIsearly() {
		return isearly;
	}

	public void setIsearly(String isearly) {
		this.isearly = isearly;
	}

	public String getBillmode() {
		return billmode;
	}

	public void setBillmode(String billmode) {
		this.billmode = billmode;
	}

	public String getVaddress() {
		return vaddress;
	}

	public void setVaddress(String vaddress) {
		this.vaddress = vaddress;
	}

	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	public String getBillaccount() {
		return billaccount;
	}

	public void setBillaccount(String billaccount) {
		this.billaccount = billaccount;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getVpsnid() {
		return vpsnid;
	}

	public void setVpsnid(String vpsnid) {
		this.vpsnid = vpsnid;
	}

	public String getVeffectdate() {
		return veffectdate;
	}

	public void setVeffectdate(String veffectdate) {
		this.veffectdate = veffectdate;
	}

	public String getVautodate() {
		return vautodate;
	}

	public void setVautodate(String vautodate) {
		this.vautodate = vautodate;
	}

	public String getVmailprovince() {
		return vmailprovince;
	}

	public void setVmailprovince(String vmailprovince) {
		this.vmailprovince = vmailprovince;
	}

	public String getVmailcity() {
		return vmailcity;
	}

	public void setVmailcity(String vmailcity) {
		this.vmailcity = vmailcity;
	}

	public String getVsrcsystem() {
		return vsrcsystem;
	}

	public void setVsrcsystem(String vsrcsystem) {
		this.vsrcsystem = vsrcsystem;
	}

	public String getVsrcrowid() {
		return vsrcrowid;
	}

	public void setVsrcrowid(String vsrcrowid) {
		this.vsrcrowid = vsrcrowid;
	}

	public String getPk_group() {
		return pk_group;
	}

	public void setPk_group(String pk_group) {
		this.pk_group = pk_group;
	}

	public String getPk_org() {
		return pk_org;
	}

	public void setPk_org(String pk_org) {
		this.pk_org = pk_org;
	}

	public String getPk_org_v() {
		return pk_org_v;
	}

	public void setPk_org_v(String pk_org_v) {
		this.pk_org_v = pk_org_v;
	}

	public String getCalnum() {
		return calnum;
	}

	public void setCalnum(String calnum) {
		this.calnum = calnum;
	}

	public String getCaldate() {
		return caldate;
	}

	public void setCaldate(String caldate) {
		this.caldate = caldate;
	}

	public String getVdata() {
		return vdata;
	}

	public void setVdata(String vdata) {
		this.vdata = vdata;
	}

	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	public String getPk_ptsitem() {
		return pk_ptsitem;
	}

	public void setPk_ptsitem(String pk_ptsitem) {
		this.pk_ptsitem = pk_ptsitem;
	}

	public int getMethodcal() {
		return methodcal;
	}

	public void setMethodcal(int methodcal) {
		this.methodcal = methodcal;
	}

	public String getDrawback() {
		return drawback;
	}

	public void setDrawback(String drawback) {
		this.drawback = drawback;
	}

	public String getTaxrate() {
		return taxrate;
	}

	public void setTaxrate(String taxrate) {
		this.taxrate = taxrate;
	}

	public String getOriamt() {
		return oriamt;
	}

	public void setOriamt(String oriamt) {
		this.oriamt = oriamt;
	}

	public String getOritax() {
		return oritax;
	}

	public void setOritax(String oritax) {
		this.oritax = oritax;
	}

	public String getLocalamt() {
		return localamt;
	}

	public void setLocalamt(String localamt) {
		this.localamt = localamt;
	}

	public String getLocaltax() {
		return localtax;
	}

	public void setLocaltax(String localtax) {
		this.localtax = localtax;
	}

	public String getDecldate() {
		return decldate;
	}

	public void setDecldate(String decldate) {
		this.decldate = decldate;
	}

	public String getBusidate() {
		return busidate;
	}

	public void setBusidate(String busidate) {
		this.busidate = busidate;
	}

	public String getCustcode() {
		return custcode;
	}

	public void setCustcode(String custcode) {
		this.custcode = custcode;
	}

	public String getCustname() {
		return custname;
	}

	public void setCustname(String custname) {
		this.custname = custname;
	}

	public String getCusttype() {
		return custtype;
	}

	public void setCusttype(String custtype) {
		this.custtype = custtype;
	}

	public int getDectype() {
		return dectype;
	}

	public void setDectype(int dectype) {
		this.dectype = dectype;
	}

	public String getSrcsystem() {
		return srcsystem;
	}

	public void setSrcsystem(String srcsystem) {
		this.srcsystem = srcsystem;
	}

	public String getVoucherid() {
		return voucherid;
	}

	public void setVoucherid(String voucherid) {
		this.voucherid = voucherid;
	}

	public String getTranserial() {
		return transerial;
	}

	public void setTranserial(String transerial) {
		this.transerial = transerial;
	}

	public String getTranbatch() {
		return tranbatch;
	}

	public void setTranbatch(String tranbatch) {
		this.tranbatch = tranbatch;
	}

	public String getTrandate() {
		return trandate;
	}

	public void setTrandate(String trandate) {
		this.trandate = trandate;
	}

	public String getTrantime() {
		return trantime;
	}

	public void setTrantime(String trantime) {
		this.trantime = trantime;
	}

	public String getTranorg() {
		return tranorg;
	}

	public void setTranorg(String tranorg) {
		this.tranorg = tranorg;
	}

	public String getTranchannel() {
		return tranchannel;
	}

	public void setTranchannel(String tranchannel) {
		this.tranchannel = tranchannel;
	}

	public String getTrancounte() {
		return trancounte;
	}

	public void setTrancounte(String trancounte) {
		this.trancounte = trancounte;
	}

	public String getCustmanager() {
		return custmanager;
	}

	public void setCustmanager(String custmanager) {
		this.custmanager = custmanager;
	}

	public String getTrantype() {
		return trantype;
	}

	public void setTrantype(String trantype) {
		this.trantype = trantype;
	}

	public String getProcode() {
		return procode;
	}

	public void setProcode(String procode) {
		this.procode = procode;
	}



	public String getBusino() {
		return busino;
	}

	public void setBusino(String busino) {
		this.busino = busino;
	}

	public String getTranremark() {
		return tranremark;
	}

	public void setTranremark(String tranremark) {
		this.tranremark = tranremark;
	}

	public String getAccountno() {
		return accountno;
	}

	public void setAccountno(String accountno) {
		this.accountno = accountno;
	}

	public String getTrancurrency() {
		return trancurrency;
	}

	public void setTrancurrency(String trancurrency) {
		this.trancurrency = trancurrency;
	}

	public String getTranamt() {
		return tranamt;
	}

	public void setTranamt(String tranamt) {
		this.tranamt = tranamt;
	}

	public int getTaxtype() {
		return taxtype;
	}

	public void setTaxtype(int taxtype) {
		this.taxtype = taxtype;
	}

	public String getCztype() {
		return cztype;
	}

	public void setCztype(String cztype) {
		this.cztype = cztype;
	}

	public String getCzolddate() {
		return czolddate;
	}

	public void setCzolddate(String czolddate) {
		this.czolddate = czolddate;
	}

	public String getIscurrent() {
		return iscurrent;
	}

	public void setIscurrent(String iscurrent) {
		this.iscurrent = iscurrent;
	}

	public String getCzbusino() {
		return czbusino;
	}

	public void setCzbusino(String czbusino) {
		this.czbusino = czbusino;
	}

	public String getAccno() {
		return accno;
	}

	public void setAccno(String accno) {
		this.accno = accno;
	}

	public int getLoanflag() {
		return loanflag;
	}

	public void setLoanflag(int loanflag) {
		this.loanflag = loanflag;
	}

	public String getAcccode() {
		return acccode;
	}

	public void setAcccode(String acccode) {
		this.acccode = acccode;
	}

	public int getPaymentflag() {
		return paymentflag;
	}

	public void setPaymentflag(int paymentflag) {
		this.paymentflag = paymentflag;
	}

	public String getAmortno() {
		return amortno;
	}

	public void setAmortno(String amortno) {
		this.amortno = amortno;
	}

	public String getMidcontno() {
		return midcontno;
	}

	public void setMidcontno(String midcontno) {
		this.midcontno = midcontno;
	}

	public String getIsinnerbank() {
		return isinnerbank;
	}

	public void setIsinnerbank(String isinnerbank) {
		this.isinnerbank = isinnerbank;
	}

	public int getOverseasflag() {
		return overseasflag;
	}

	public void setOverseasflag(int overseasflag) {
		this.overseasflag = overseasflag;
	}

	public int getTaxcalmethod() {
		return taxcalmethod;
	}

	public void setTaxcalmethod(int taxcalmethod) {
		this.taxcalmethod = taxcalmethod;
	}

	public String getCreator1() {
		return creator1;
	}

	public void setCreator1(String creator1) {
		this.creator1 = creator1;
	}

	public String getCreationtime1() {
		return creationtime1;
	}

	public void setCreationtime1(String creationtime1) {
		this.creationtime1 = creationtime1;
	}

	public String getModifier1() {
		return modifier1;
	}

	public void setModifier1(String modifier1) {
		this.modifier1 = modifier1;
	}

	public String getModifiedtime1() {
		return modifiedtime1;
	}

	public void setModifiedtime1(String modifiedtime1) {
		this.modifiedtime1 = modifiedtime1;
	}

	public String getNegtype() {
		return negtype;
	}

	public void setNegtype(String negtype) {
		this.negtype = negtype;
	}

	public String getTranplace() {
		return tranplace;
	}

	public void setTranplace(String tranplace) {
		this.tranplace = tranplace;
	}

	public String getDeptdoc() {
		return deptdoc;
	}

	public void setDeptdoc(String deptdoc) {
		this.deptdoc = deptdoc;
	}

	public int getConfimtype() {
		return confimtype;
	}

	public void setConfimtype(int confimtype) {
		this.confimtype = confimtype;
	}

	public String getIspts() {
		return ispts;
	}

	public void setIspts(String ispts) {
		this.ispts = ispts;
	}

	public String getIsbill() {
		return isbill;
	}

	public void setIsbill(String isbill) {
		this.isbill = isbill;
	}

	public int getAreatype() {
		return areatype;
	}

	public void setAreatype(int areatype) {
		this.areatype = areatype;
	}

	public String getIsptserror() {
		return isptserror;
	}

	public void setIsptserror(String isptserror) {
		this.isptserror = isptserror;
	}

	public String getReceivedate() {
		return receivedate;
	}

	public void setReceivedate(String receivedate) {
		this.receivedate = receivedate;
	}

	public String getIsalloweadvance() {
		return isalloweadvance;
	}

	public void setIsalloweadvance(String isalloweadvance) {
		this.isalloweadvance = isalloweadvance;
	}

	public String getBusipk() {
		return busipk;
	}

	public void setBusipk(String busipk) {
		this.busipk = busipk;
	}

	public String getBusiitem() {
		return busiitem;
	}

	public void setBusiitem(String busiitem) {
		this.busiitem = busiitem;
	}

	public String getFininsance() {
		return fininsance;
	}

	public void setFininsance(String fininsance) {
		this.fininsance = fininsance;
	}

	public String getFininstype() {
		return fininstype;
	}

	public void setFininstype(String fininstype) {
		this.fininstype = fininstype;
	}

	public String getIstaxfree() {
		return istaxfree;
	}

	public void setIstaxfree(String istaxfree) {
		this.istaxfree = istaxfree;
	}

	public String getBunit() {
		return bunit;
	}

	public void setBunit(String bunit) {
		this.bunit = bunit;
	}

	public String getBunivalent() {
		return bunivalent;
	}

	public void setBunivalent(String bunivalent) {
		this.bunivalent = bunivalent;
	}

	public String getBnumber() {
		return bnumber;
	}

	public void setBnumber(String bnumber) {
		this.bnumber = bnumber;
	}

	public String getBilltype() {
		return billtype;
	}

	public void setBilltype(String billtype) {
		this.billtype = billtype;
	}

	public String getBilldate() {
		return billdate;
	}

	public void setBilldate(String billdate) {
		this.billdate = billdate;
	}

	public String getServicedoc() {
		return servicedoc;
	}

	public void setServicedoc(String servicedoc) {
		this.servicedoc = servicedoc;
	}

	public String getIshistory() {
		return ishistory;
	}

	public void setIshistory(String ishistory) {
		this.ishistory = ishistory;
	}

	public String getIsmoneny() {
		return ismoneny;
	}

	public void setIsmoneny(String ismoneny) {
		this.ismoneny = ismoneny;
	}

	public String getInsureno() {
		return insureno;
	}

	public void setInsureno(String insureno) {
		this.insureno = insureno;
	}

	public String getIsfirst() {
		return isfirst;
	}

	public void setIsfirst(String isfirst) {
		this.isfirst = isfirst;
	}

	public String getPerspolicyno() {
		return perspolicyno;
	}

	public void setPerspolicyno(String perspolicyno) {
		this.perspolicyno = perspolicyno;
	}

	public String getPpveridate() {
		return ppveridate;
	}

	public void setPpveridate(String ppveridate) {
		this.ppveridate = ppveridate;
	}

	public String getBillstartdate() {
		return billstartdate;
	}

	public void setBillstartdate(String billstartdate) {
		this.billstartdate = billstartdate;
	}

	public String getBillenddate() {
		return billenddate;
	}

	public void setBillenddate(String billenddate) {
		this.billenddate = billenddate;
	}

	public String getPaytype() {
		return paytype;
	}

	public void setPaytype(String paytype) {
		this.paytype = paytype;
	}

	public String getIsmat() {
		return ismat;
	}

	public void setIsmat(String ismat) {
		this.ismat = ismat;
	}

	public String getIsentrust() {
		return isentrust;
	}

	public void setIsentrust(String isentrust) {
		this.isentrust = isentrust;
	}

	public String getBilleffectivedate() {
		return billeffectivedate;
	}

	public void setBilleffectivedate(String billeffectivedate) {
		this.billeffectivedate = billeffectivedate;
	}

	public String getRecoincomdate() {
		return recoincomdate;
	}

	public void setRecoincomdate(String recoincomdate) {
		this.recoincomdate = recoincomdate;
	}

	public String getBillfalg() {
		return billfalg;
	}

	public void setBillfalg(String billfalg) {
		this.billfalg = billfalg;
	}

	public String getOffsetdate() {
		return offsetdate;
	}

	public void setOffsetdate(String offsetdate) {
		this.offsetdate = offsetdate;
	}

	

	public String getBatchno() {
		return batchno;
	}

	public void setBatchno(String batchno) {
		this.batchno = batchno;
	}

	public String getIssale() {
		return issale;
	}

	public void setIssale(String issale) {
		this.issale = issale;
	}
	
	public String getVdef1() {
		return vdef1;
	}

	public void setVdef1(String vdef1) {
		this.vdef1 = vdef1;
	}

	public String getVdef2() {
		return vdef2;
	}

	public void setVdef2(String vdef2) {
		this.vdef2 = vdef2;
	}
	
	public String getVdef6() {
		return vdef6;
	}

	public void setVdef6(String vdef6) {
		this.vdef6 = vdef6;
	}
	
	public String getVdef7() {
		return vdef7;
	}

	public void setVdef7(String vdef7) {
		this.vdef7 = vdef7;
	}

	public String getBuymailbox() {
		return buymailbox;
	}

	public void setBuymailbox(String buymailbox) {
		this.buymailbox = buymailbox;
	}

	public String getBuyphoneno() {
		return buyphoneno;
	}

	public void setBuyphoneno(String buyphoneno) {
		this.buyphoneno = buyphoneno;
	}
}
