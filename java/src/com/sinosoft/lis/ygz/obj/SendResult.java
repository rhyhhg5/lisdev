package com.sinosoft.lis.ygz.obj;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * Title: 营改增，价税分离接口实体类
 * </p>
 * <p>
 * Description:返回报文sendresult节点的封装
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author lzy
 * @version 1.0
 */

public class SendResult {

	public SendResult() {

	}
	/**
	 * 
	 */
	private String billpk;
	/**
	 * 
	 */
	private String bdocid;
	/**
	 * 
	 */
	private String filename;
	/**
	 * 返回结果代码
	 */
	private String resultcode;
	/**
	 * 返回结果描述
	 */
	private String resultdescription;
	/**
	 * 价税分离结果
	 */
	private Content content;
	
	public Content getContent() {
		return content;
	}
	public void setContent(Content content) {
		this.content = content;
	}
	public String getBillpk() {
		return billpk;
	}
	public void setBillpk(String billpk) {
		this.billpk = billpk;
	}
	public String getBdocid() {
		return bdocid;
	}
	public void setBdocid(String bdocid) {
		this.bdocid = bdocid;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getResultcode() {
		return resultcode;
	}
	public void setResultcode(String resultcode) {
		this.resultcode = resultcode;
	}
	public String getResultdescription() {
		return resultdescription;
	}
	public void setResultdescription(String resultdescription) {
		this.resultdescription = resultdescription;
	}

}
