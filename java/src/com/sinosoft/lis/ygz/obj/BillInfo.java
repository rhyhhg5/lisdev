package com.sinosoft.lis.ygz.obj;

/**
 * <p>
 * Title: 营改增，价税分离接口实体类
 * </p>
 * <p>
 * Description:请求报文中的bill节点及子节点billhead信息的封装
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author lzy
 * @version 1.0
 */

public class BillInfo {

	public BillInfo() {

	}

	/**
	 * bill id
	 */
	private String SequenceNo;
	/**
	 * 以下字段均为 billhead 节点的字段
	 */
	private String pk_jiashuifl;
	/**
	 * 业务流水号
	 */
	private String id;
	/**
	 * 保单号
	 */
	private String baodanno;
	/**
	 * 收支类型
	 */
	private String shouzhitype;
	/**
	 * 业务险种编码
	 */
	private String xianzhongcode;
	/**
	 * 出单机构编码
	 */
	private String jigoucode;
	/**
	 * 含税金额
	 */
	private String hanshuiamount;
	/**
	 * 自定义项1
	 */
	private String def1;
	/**
	 * 自定义项
	 */
	private String def2;
	/**
	 * 自定义项
	 */
	private String def3;
	/**
	 * 自定义项
	 */
	private String def4;
	/**
	 * 自定义项
	 */
	private String def5;
	/**
	 * 自定义项
	 */
	private String def6;
	/**
	 * 自定义项
	 */
	private String def7;
	/**
	 * 自定义项
	 */
	private String def8;
	/**
	 * 自定义项
	 */
	private String def9;
	/**
	 * 自定义项
	 */
	private String def10;
	/**
	 * 自定义项
	 */
	private String rate;
	/**
	 * 自定义项
	 */
	private String wushuiamount;
	/**
	 * 自定义项
	 */
	private String taxes;

	public String getSequenceNo() {
		return SequenceNo;
	}

	public void setSequenceNo(String sequenceNo) {
		SequenceNo = sequenceNo;
	}

	public String getPk_jiashuifl() {
		return pk_jiashuifl;
	}

	public void setPk_jiashuifl(String pkJiashuifl) {
		pk_jiashuifl = pkJiashuifl;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBaodanno() {
		return baodanno;
	}

	public void setBaodanno(String baodanno) {
		this.baodanno = baodanno;
	}

	public String getShouzhitype() {
		return shouzhitype;
	}

	public void setShouzhitype(String shouzhitype) {
		this.shouzhitype = shouzhitype;
	}

	public String getXianzhongcode() {
		return xianzhongcode;
	}

	public void setXianzhongcode(String xianzhongcode) {
		this.xianzhongcode = xianzhongcode;
	}

	public String getJigoucode() {
		return jigoucode;
	}

	public void setJigoucode(String jigoucode) {
		this.jigoucode = jigoucode;
	}

	public String getHanshuiamount() {
		return hanshuiamount;
	}

	public void setHanshuiamount(String hanshuiamount) {
		this.hanshuiamount = hanshuiamount;
	}

	public String getDef1() {
		return def1;
	}

	public void setDef1(String def1) {
		this.def1 = def1;
	}

	public String getDef2() {
		return def2;
	}

	public void setDef2(String def2) {
		this.def2 = def2;
	}

	public String getDef3() {
		return def3;
	}

	public void setDef3(String def3) {
		this.def3 = def3;
	}

	public String getDef4() {
		return def4;
	}

	public void setDef4(String def4) {
		this.def4 = def4;
	}

	public String getDef5() {
		return def5;
	}

	public void setDef5(String def5) {
		this.def5 = def5;
	}

	public String getDef6() {
		return def6;
	}

	public void setDef6(String def6) {
		this.def6 = def6;
	}

	public String getDef7() {
		return def7;
	}

	public void setDef7(String def7) {
		this.def7 = def7;
	}

	public String getDef8() {
		return def8;
	}

	public void setDef8(String def8) {
		this.def8 = def8;
	}

	public String getDef9() {
		return def9;
	}

	public void setDef9(String def9) {
		this.def9 = def9;
	}

	public String getDef10() {
		return def10;
	}

	public void setDef10(String def10) {
		this.def10 = def10;
	}

	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	public String getWushuiamount() {
		return wushuiamount;
	}

	public void setWushuiamount(String wushuiamount) {
		this.wushuiamount = wushuiamount;
	}

	public String getTaxes() {
		return taxes;
	}

	public void setTaxes(String taxes) {
		this.taxes = taxes;
	}

}
