package com.sinosoft.lis.ygz.obj;

/**
 * <p>
 * Title: 查询接口报文头信息
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author Alex
 * @version 1.0
 */
public class OutPayQueryHead {
	
	public OutPayQueryHead(){
		
	}
	/**
	 * 对方接口设计实际没有单独的报文头
	 * 以下字段均为报文跟节点的所有属性字段
	 * 
	 */
	
	private String account;
	
	private String billtype;
	
	private String filename;
	
	private String groupcode;
	
	private String sender;

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getBilltype() {
		return billtype;
	}

	public void setBilltype(String billtype) {
		this.billtype = billtype;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getGroupcode() {
		return groupcode;
	}

	public void setGroupcode(String groupcode) {
		this.groupcode = groupcode;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}
	
	/**
	 * 设置默认字段
	 * @param head
	 * @return
	 */
	
	public void setDefault(){
		if(null == this.getAccount()){
			this.setAccount("develop");
		}
		if(null == this.getBilltype()){
			this.setBilltype("query_billopen");
		}
		
		if(null == this.getFilename()){
			this.setFilename("");
		}
		if(null == this.getGroupcode()){
			this.setGroupcode("00");
		}
		if(null ==this.getSender()){
			this.setSender("");
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
