package com.sinosoft.lis.ygz.obj;
/**
 * <p>
 * Title: 查询接口实体类
 * </p>
 * <p>
 * Description:请求报文中的bill节点及子节点billhead信息的封装
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author Alex
 * @version 1.0
 */
public class OutPayQueryBillInfo {
	
	public OutPayQueryBillInfo(){
		
	}
	
	/**
	 * 以下字段均为 billhead 节点的字段
	 */
	
	/**
	 * 
	 */
	private String id;
	
	/**
	 * 交易流水号
	 */
	private String transerial;
	
	
	
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTranserial() {
		return transerial;
	}

	public void setTranserial(String transerial) {
		this.transerial = transerial;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
}
