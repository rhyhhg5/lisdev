package com.sinosoft.lis.ygz.obj;

/**
 * <p>
 * Title: 营改增，价税分离接口实体类
 * </p>
 * <p>
 * Description:返回报文content节点的封装
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author lzy
 * @version 1.0
 */

public class Content {

	public Content() {

	}

	/**
	 * 业务流水号
	 */
	private String id;
	/**
	 * 税率
	 */
	private String rate;
	/**
	 * 无税金额
	 */
	private String wushuiamount;
	/**
	 * 税额
	 */
	private String taxes;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	public String getWushuiamount() {
		return wushuiamount;
	}

	public void setWushuiamount(String wushuiamount) {
		this.wushuiamount = wushuiamount;
	}

	public String getTaxes() {
		return taxes;
	}

	public void setTaxes(String taxes) {
		this.taxes = taxes;
	}

}
