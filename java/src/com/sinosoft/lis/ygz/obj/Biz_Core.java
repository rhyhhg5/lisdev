package com.sinosoft.lis.ygz.obj;

import oracle.sql.DATE;
import oracle.sql.NUMBER;

public class Biz_Core {
	public Biz_Core() {

	}

	private NUMBER ID;//主键
	private String LSH;//请求流水号
	private String DDBH;//订单编号
	private String HTBH;//合同编号(保单号)
	private String GMF_SFZJLX;//受票方(经办人)身份证件类型:01 身份证、02 护照、03 军官证、04 士兵证、05 港 澳居民来往内地通行证、 06 台湾居民来往内地通行 证、07 其他(包括税务登 记证等其他类型)
	private String GMF_SFZJHM;//受票方(经办人)身份证件号码
	private String GMF_XM;//个人姓名或企业名称
	private String GMF_SJH;//受票方(经办人)手机号码
	private String GMF_DZYX;//受票方(经办人)电子邮箱
	private String BMB_BBH;//编码表版本号
	private String ZSFS;//征收方式:0:普通征收 2:差额征收
	private String KPLX;//开票类型 0:蓝字发票 1:红字发票
	private String XSF_NSRSBH;//销售方纳税人识别号
	private String XSF_MC;//销售方名称
	private String XSF_DZDH;//销售方地址、电话
	private String XSF_YHZH;//销售方银行、账号
	private String GMF_NSRSBH;//购买方纳税人识别号
	private String GMF_MC;//购买方名称
	private String GMF_DZDH;//购买方地址、电话
	private String GMF_YHZH;//购买方银行、账号
	private String KPR;//开票人
	private String SKR;//收款人
	private String FHR;//复核人
	private String FP_DM;//发票代码(数据处理成功后反写)
	private String FP_HM;//发票号码(数据处理成功后反写)
	private String KPRQ;//开票日期(数据处理成功后反写)
	private String JYM;//校验码(数据处理成功后反写)
	private String YFP_DM;//原发票代码
	private String YFP_HM;//原发票号码
	private NUMBER JSHJ;//价税合计
	private NUMBER HJJE;//合计金额
	private NUMBER HJSE;//合计税额
	private NUMBER KCE;//扣除额
	private String BZ;//备注
	private String FPHXZ;//发票行性质0:正常行 1:折扣行 2:被折扣行
	private String SPBM;//商品编码
	private String ZXBM;//自行编码
	private NUMBER YHZCBS;//优惠政策标识0:不使用 1:使用
	private NUMBER LSLBS;//零税率标识  空:非零税率 1:免税 2:不征收 3:普通零税率
	private String ZZSTSGL;//增值税特殊管理如果 yhzcbs 为 1 时，此项 必填，具体信息取《商品和 服务税收分类与编码》.xls中的增值税特殊管理列
	private String XMMC;//项目名称
	private String DW;//计量单位
	private String GGXH;//规格型号
	private NUMBER XMSL;//项目数量
	private NUMBER XMDJ;//不含税单价，小数点后最多6位
	private NUMBER XMJE;//不含税项目金额
	private NUMBER SL;//税率，最多6 位小数，例 1%为 0.01
	private NUMBER SE;//税额，单位:元(2 位小数)
	private DATE CREATE_TIME;//创建时间	
	private DATE UPDATE_TIME;//修改时间
	private NUMBER STATE;//0:未处理，1:处理成功，2:请求税控失败，3:数据校验失败，4:百望云接入码或税控配置信息不全，5:版式文件生成失败,6:调用税控超时，7:税控开具成功未生成版式 8:系统异常或其他原因
	private String MSG;//返回信息，在处理失败后反写

	public NUMBER getID() {
		return ID;
	}

	public String getLSH() {
		return LSH;
	}

	public String getDDBH() {
		return DDBH;
	}

	public String getHTBH() {
		return HTBH;
	}

	public String getGMF_SFZJLX() {
		return GMF_SFZJLX;
	}

	public String getGMF_SFZJHM() {
		return GMF_SFZJHM;
	}

	public String getGMF_XM() {
		return GMF_XM;
	}

	public String getGMF_SJH() {
		return GMF_SJH;
	}

	public String getGMF_DZYX() {
		return GMF_DZYX;
	}

	public String getBMB_BBH() {
		return BMB_BBH;
	}

	public String getZSFS() {
		return ZSFS;
	}

	public String getKPLX() {
		return KPLX;
	}

	public String getXSF_NSRSBH() {
		return XSF_NSRSBH;
	}

	public String getXSF_MC() {
		return XSF_MC;
	}

	public String getXSF_DZDH() {
		return XSF_DZDH;
	}

	public String getXSF_YHZH() {
		return XSF_YHZH;
	}

	public String getGMF_NSRSBH() {
		return GMF_NSRSBH;
	}

	public String getGMF_MC() {
		return GMF_MC;
	}

	public String getGMF_DZDH() {
		return GMF_DZDH;
	}

	public String getGMF_YHZH() {
		return GMF_YHZH;
	}

	public String getKPR() {
		return KPR;
	}

	public String getSKR() {
		return SKR;
	}

	public String getFHR() {
		return FHR;
	}

	public String getFP_DM() {
		return FP_DM;
	}

	public String getFP_HM() {
		return FP_HM;
	}

	public String getKPRQ() {
		return KPRQ;
	}

	public String getJYM() {
		return JYM;
	}

	public String getYFP_DM() {
		return YFP_DM;
	}

	public String getYFP_HM() {
		return YFP_HM;
	}

	public NUMBER getJSHJ() {
		return JSHJ;
	}

	public NUMBER getHJJE() {
		return HJJE;
	}

	public NUMBER getHJSE() {
		return HJSE;
	}

	public NUMBER getKCE() {
		return KCE;
	}

	public String getBZ() {
		return BZ;
	}

	public String getFPHXZ() {
		return FPHXZ;
	}

	public String getSPBM() {
		return SPBM;
	}

	public String getZXBM() {
		return ZXBM;
	}

	public NUMBER getYHZCBS() {
		return YHZCBS;
	}

	public NUMBER getLSLBS() {
		return LSLBS;
	}

	public String getZZSTSGL() {
		return ZZSTSGL;
	}

	public String getXMMC() {
		return XMMC;
	}

	public String getDW() {
		return DW;
	}

	public String getGGXH() {
		return GGXH;
	}

	public NUMBER getXMSL() {
		return XMSL;
	}

	public NUMBER getXMDJ() {
		return XMDJ;
	}

	public NUMBER getXMJE() {
		return XMJE;
	}

	public NUMBER getSL() {
		return SL;
	}

	public NUMBER getSE() {
		return SE;
	}

	public DATE getCREATE_TIME() {
		return CREATE_TIME;
	}

	public DATE getUPDATE_TIME() {
		return UPDATE_TIME;
	}

	public NUMBER getSTATE() {
		return STATE;
	}

	public String getMSG() {
		return MSG;
	}

	public void setID(NUMBER id) {
		ID = id;
	}

	public void setLSH(String lsh) {
		LSH = lsh;
	}

	public void setDDBH(String ddbh) {
		DDBH = ddbh;
	}

	public void setHTBH(String htbh) {
		HTBH = htbh;
	}

	public void setGMF_SFZJLX(String gmf_sfzjlx) {
		GMF_SFZJLX = gmf_sfzjlx;
	}

	public void setGMF_SFZJHM(String gmf_sfzjhm) {
		GMF_SFZJHM = gmf_sfzjhm;
	}

	public void setGMF_XM(String gmf_xm) {
		GMF_XM = gmf_xm;
	}

	public void setGMF_SJH(String gmf_sjh) {
		GMF_SJH = gmf_sjh;
	}

	public void setGMF_DZYX(String gmf_dzyx) {
		GMF_DZYX = gmf_dzyx;
	}

	public void setBMB_BBH(String bmb_bbh) {
		BMB_BBH = bmb_bbh;
	}

	public void setZSFS(String zsfs) {
		ZSFS = zsfs;
	}

	public void setKPLX(String kplx) {
		KPLX = kplx;
	}

	public void setXSF_NSRSBH(String xsf_nsrsbh) {
		XSF_NSRSBH = xsf_nsrsbh;
	}

	public void setXSF_MC(String xsf_mc) {
		XSF_MC = xsf_mc;
	}

	public void setXSF_DZDH(String xsf_dzdh) {
		XSF_DZDH = xsf_dzdh;
	}

	public void setXSF_YHZH(String xsf_yhzh) {
		XSF_YHZH = xsf_yhzh;
	}

	public void setGMF_NSRSBH(String gmf_nsrsbh) {
		GMF_NSRSBH = gmf_nsrsbh;
	}

	public void setGMF_MC(String gmf_mc) {
		GMF_MC = gmf_mc;
	}

	public void setGMF_DZDH(String gmf_dzdh) {
		GMF_DZDH = gmf_dzdh;
	}

	public void setGMF_YHZH(String gmf_yhzh) {
		GMF_YHZH = gmf_yhzh;
	}

	public void setKPR(String kpr) {
		KPR = kpr;
	}

	public void setSKR(String skr) {
		SKR = skr;
	}

	public void setFHR(String fhr) {
		FHR = fhr;
	}

	public void setFP_DM(String fp_dm) {
		FP_DM = fp_dm;
	}

	public void setFP_HM(String fp_hm) {
		FP_HM = fp_hm;
	}

	public void setKPRQ(String kprq) {
		KPRQ = kprq;
	}

	public void setJYM(String jym) {
		JYM = jym;
	}

	public void setYFP_DM(String yfp_dm) {
		YFP_DM = yfp_dm;
	}

	public void setYFP_HM(String yfp_hm) {
		YFP_HM = yfp_hm;
	}

	public void setJSHJ(NUMBER jshj) {
		JSHJ = jshj;
	}

	public void setHJJE(NUMBER hjje) {
		HJJE = hjje;
	}

	public void setHJSE(NUMBER hjse) {
		HJSE = hjse;
	}

	public void setKCE(NUMBER kce) {
		KCE = kce;
	}

	public void setBZ(String bz) {
		BZ = bz;
	}

	public void setFPHXZ(String fphxz) {
		FPHXZ = fphxz;
	}

	public void setSPBM(String spbm) {
		SPBM = spbm;
	}

	public void setZXBM(String zxbm) {
		ZXBM = zxbm;
	}

	public void setYHZCBS(NUMBER yhzcbs) {
		YHZCBS = yhzcbs;
	}

	public void setLSLBS(NUMBER lslbs) {
		LSLBS = lslbs;
	}

	public void setZZSTSGL(String zzstsgl) {
		ZZSTSGL = zzstsgl;
	}

	public void setXMMC(String xmmc) {
		XMMC = xmmc;
	}

	public void setDW(String dw) {
		DW = dw;
	}

	public void setGGXH(String ggxh) {
		GGXH = ggxh;
	}

	public void setXMSL(NUMBER xmsl) {
		XMSL = xmsl;
	}

	public void setXMDJ(NUMBER xmdj) {
		XMDJ = xmdj;
	}

	public void setXMJE(NUMBER xmje) {
		XMJE = xmje;
	}

	public void setSL(NUMBER sl) {
		SL = sl;
	}

	public void setSE(NUMBER se) {
		SE = se;
	}

	public void setCREATE_TIME(DATE create_time) {
		CREATE_TIME = create_time;
	}

	public void setUPDATE_TIME(DATE update_time) {
		UPDATE_TIME = update_time;
	}

	public void setSTATE(NUMBER state) {
		STATE = state;
	}

	public void setMSG(String msg) {
		MSG = msg;
	}

}