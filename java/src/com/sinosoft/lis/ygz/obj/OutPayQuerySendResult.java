package com.sinosoft.lis.ygz.obj;
/**
 * <p>
 * Title: 查询接口实体类
 * </p>
 * <p>
 * Description:返回报文中sendresult节点的封装
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author Alex
 * @version 1.0
 */
public class OutPayQuerySendResult {
	
	public OutPayQuerySendResult(){
		
	}
	
	
	/**
	 * 
	 */
	private String id;
	
	private String resultcode;
	
	private String billnum;
	
	private String isprint;
	
	private String vstatus;
	
	private String printdate;

	public String getPrintdate() {
		return printdate;
	}

	public void setPrintdate(String printdate) {
		this.printdate = printdate;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getResultcode() {
		return resultcode;
	}

	public void setResultcode(String resultcode) {
		this.resultcode = resultcode;
	}

	public String getBillnum() {
		return billnum;
	}

	public void setBillnum(String billnum) {
		this.billnum = billnum;
	}

	public String getIsprint() {
		return isprint;
	}

	public void setIsprint(String isprint) {
		this.isprint = isprint;
	}

	public String getVstatus() {
		return vstatus;
	}

	public void setVstatus(String vstatus) {
		this.vstatus = vstatus;
	}
	
	
	

	
	
	
}
