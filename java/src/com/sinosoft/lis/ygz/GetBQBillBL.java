package com.sinosoft.lis.ygz;

import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LYOutPayDetailSchema;
import com.sinosoft.lis.schema.LYPremSeparateDetailSchema;
import com.sinosoft.lis.vschema.LJAGetEndorseSet;
import com.sinosoft.lis.vschema.LYOutPayDetailSet;
import com.sinosoft.lis.vschema.LYPremSeparateDetailSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 保全收费发票明细数据报送
 * @author fr
 * 
 */
public class GetBQBillBL {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	
	private GlobalInput mGI = new GlobalInput();
	/** 数据操作字符串 */
	private String mOperate;
	/** 开始日期 */
	private String mStartDate;
	/** 结束日期 */
	private String mEndDate;
	/** 页面手工提取标记*/
	private String mHandFlag ;
	
	private String mWherePart ;
	private String mPolicyType;
	
	// 业务处理相关变量
	private MMap mMap = new MMap();

	private String mCurrentDate = PubFun.getCurrentDate();
	private String mCurrentTime = PubFun.getCurrentTime();

	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		// 进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GetBqPremBillBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败GetTempFeeBqSeparateBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}

		// 准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}

		// 保存数据
		PubSubmit tPubSubmit = new PubSubmit();

		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			System.out.print("保存数据返回");
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "GetBqPremBillBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mInputData = null;
		return true;
	}

	// 获得输出数据
	private boolean getInputData(VData cInputData) {
		TransferData tTransferData = (TransferData) cInputData
				.getObjectByObjectName("TransferData", 0);
		mStartDate = (String) tTransferData.getValueByName("StartDate");
		mEndDate = (String) tTransferData.getValueByName("EndDate");
		if(null == mGI || null==mGI.Operator){
			mGI.Operator="server";
		}
		mPolicyType =  (String) tTransferData.getValueByName("PolicyType");			
     	mHandFlag = (String) tTransferData.getValueByName("HandWorkFlag");
	    mWherePart = "";
		
		if(null != mHandFlag && "1".equals(mHandFlag)){
	
			LJAGetEndorseSet tLJAGetEndorseSet=(LJAGetEndorseSet) cInputData
			.getObjectByObjectName("LJAGetEndorseSet", 0);
			if(null != tLJAGetEndorseSet){
				String lisStr="";
				String lisCont="";
				for(int i=1; i<=tLJAGetEndorseSet.size();i++){
					lisStr += "'"+ tLJAGetEndorseSet.get(i).getEndorsementNo()+"'";
					if(tLJAGetEndorseSet.get(i).getContNo()!= null && tLJAGetEndorseSet.get(i).getContNo().length()>1){
						lisCont += "'"+ tLJAGetEndorseSet.get(i).getContNo()+"'";
					}
					else{
						lisCont += "'"+ tLJAGetEndorseSet.get(i).getGrpContNo()+"'";
					}
					if(i<tLJAGetEndorseSet.size()){
						lisStr += ",";
						lisCont += ",";
					}
				}
				mWherePart = " and endorsementno in ("+lisStr+")  and (contno in ("+lisCont+") or grpcontno in ("+lisCont+"))";
			}
		
		}
		
		return true;
	}

	// 业务处理
	private boolean dealData() {

		LYOutPayDetailSet tLYOutPayDetailSet = new LYOutPayDetailSet();
		String tWhereSQL = "";
		if (null != mStartDate  && !"".equals(mStartDate)) {
			tWhereSQL += " and lja.confdate >= '2015-08-01' and lja.confdate >= " +
					"'" + mStartDate + "' ";
		}
		if (null != mEndDate && !"".equals(mEndDate)) {
			tWhereSQL += " and lja.confdate <= '" + mEndDate + "' ";
		}
		if ("".equals(tWhereSQL)&& "".equals(mWherePart)) {
			tWhereSQL += " and lja.confdate >= '2015-08-01' and lja.confdate >= current date - 7 days ";
		}

		//团单保全收费数据

		if(null == mPolicyType || "2".equals(mPolicyType) ){
		String tSQL = " select "
				//	+ " lyp.busino,"//流水号
			        + " trim(ljag.FeeOperationType)||trim(ljag.FeeFinaType)||trim(ljag.PolNo)||trim(ljag.DutyCode),"
					+ " (select managecom from lcgrpcont where grpcontno=ljag.grpcontno union select managecom from lbgrpcont where grpcontno=ljag.grpcontno) , "//所属组织
					+ " (select appntno from lcgrpcont where grpcontno=ljag.grpcontno union select appntno from lbgrpcont where grpcontno=ljag.grpcontno) , "//客户号
					+ " (select grpname from lcgrpcont where grpcontno=ljag.grpcontno union select grpname from lbgrpcont where grpcontno=ljag.grpcontno) , "
					+ " '1', "//客户类型:“1”代表机构，“2”代表个人
					+ " '01', "//价税分离项目:固定值
					+ " ljag.taxrate,"
					+ " ljag.moneyNoTax,"
					+ " ljag.moneyTax, "
					+ " '0', "//申报类型：0正常申报，固定值
					+ " '01', "//来源系统:固定值
					+ " lja.confdate,"//交易日期
					+ " ljag.riskcode,"//产品
					+ " ljag.grpcontno,"
					+ " ljag.busitype,"//业务类型
					+ " 'CNY',"//交易币种
					+ " ljag.GetMoney,"//交易金额
					+ " '0', "//金额是否含税:“0”表示含税，“1”表示不含税
					+ " '01',"//收付标志
//					+ " lyp.moneytype,"//收付标志
					+ " '0',"//境内外标示:“0”表示境内，“1”表示境外
					+ " 'Y',"//是否开票
					+ " '0',"//汇总地类型:“0”表示汇总，“1”表示属地，后续纳税申报需要
					+ " (select prtno from lcgrpcont where grpcontno=ljag.grpcontno union select prtno from lbgrpcont where grpcontno=ljag.grpcontno),"	//印刷号			
//					+ " lyp.prtno, "//投保单号
					+ " (select signdate from lcgrpcont where grpcontno=ljag.grpcontno union select signdate from lbgrpcont where grpcontno=ljag.grpcontno) ,"//签单号
					+ " lja.incomeno, "//批单号
					+ " ljag.ActugetNo moneyno, "
					+ " (select riskname from lmriskapp where riskcode=ljag.riskcode)"
					+ " from ljagetendorse ljag,ljapay lja "
					+ " where" 
				//	+ " lyp.tempfeeno=lja.getnoticeno "
					+ " ljag.EndorsementNo =lja.incomeno"
					+ " and lja.incometype ='3'"
//					+ " and ljag.OtherNoType='3'"
					+ " and ljag.ActugetNo=lja.payno "
					+ " and ljag.moneyNoTax is not null "
//					+ " and lja.confdate is not null "
			        + " and ljag.busitype is not null "
			        + " and ljag.FeeOperationType not in ('YEI','YEO','YS') "
					+ " and ljag.ActugetNo is not null "
					+ " and not exists (select 1 from LYOutPayDetail where busino=trim(ljag.FeeOperationType)||trim(ljag.FeeFinaType)||trim(ljag.PolNo)||trim(ljag.DutyCode) and state<>'03') "
					+ " and exists (select 1 from lcgrpcont where grpcontno=ljag.grpcontno union select 1 from lbgrpcont where grpcontno=ljag.grpcontno) "			
					+ tWhereSQL
					+ mWherePart					
					+ " with ur";
		
		SSRS tSSRS = new ExeSQL().execSQL(tSQL);
		if (tSSRS != null && tSSRS.getMaxRow() > 0) {
			for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
				LYOutPayDetailSchema tLYOutPayDetailSchema = new LYOutPayDetailSchema();
				tLYOutPayDetailSchema.setBusiNo(tSSRS.GetText(i, 1));
				tLYOutPayDetailSchema.settranserial(tSSRS.GetText(i, 1));//交易流水号
				tLYOutPayDetailSchema.setbusipk(tSSRS.GetText(i, 1));
				tLYOutPayDetailSchema.setisupdate("Y");//是否更新客户信息
				tLYOutPayDetailSchema.setgroup("00");//所属集团:固定值
				tLYOutPayDetailSchema.setpkgroup("00");//所属集团:固定值
				tLYOutPayDetailSchema.setpkorg(tSSRS.GetText(i, 2));//所属组织
				tLYOutPayDetailSchema.setorg(tSSRS.GetText(i, 2));//所属组织
				tLYOutPayDetailSchema.setorgv(tSSRS.GetText(i, 2));//所属组织
				tLYOutPayDetailSchema.setdeptdoc(tSSRS.GetText(i, 2));
				tLYOutPayDetailSchema.setcode(tSSRS.GetText(i, 3));//客户编码
				tLYOutPayDetailSchema.setname(tSSRS.GetText(i, 4));//客户名称
	//			tLYOutPayDetailSchema.setcustomertype(tSSRS.GetText(i, 5));//客户类型
				tLYOutPayDetailSchema.setcustomertype("2");//modify 20160604 管理平台提供的默认值
				tLYOutPayDetailSchema.setcustcode(tSSRS.GetText(i, 3));
				tLYOutPayDetailSchema.setcustname(tSSRS.GetText(i, 4));
	//			tLYOutPayDetailSchema.setcusttype(tSSRS.GetText(i, 5));
				tLYOutPayDetailSchema.setcusttype("2");
				
				tLYOutPayDetailSchema.setptsitem(tSSRS.GetText(i, 6));
				tLYOutPayDetailSchema.settaxrate(tSSRS.GetText(i, 7));//税率
				tLYOutPayDetailSchema.setoriamt(tSSRS.GetText(i, 8));//不含税金额
				tLYOutPayDetailSchema.setoritax(tSSRS.GetText(i, 9));//税额
				tLYOutPayDetailSchema.setlocalamt(tSSRS.GetText(i, 8));//不含税金额（本币）
				tLYOutPayDetailSchema.setlocaltax(tSSRS.GetText(i, 9));//税额（本币）
				tLYOutPayDetailSchema.setdectype(tSSRS.GetText(i, 10));//申报类型
				tLYOutPayDetailSchema.setsrcsystem(tSSRS.GetText(i, 11));
				tLYOutPayDetailSchema.settrandate(tSSRS.GetText(i, 12));//交易日期
				String riskcode = tSSRS.GetText(i, 13);
				String moneyType = tSSRS.GetText(i, 15);
				if ("09".equals(moneyType)) {//工本费riskcode置0000
					riskcode = BQ.FILLDATA;
				} else {
					riskcode = riskcode.substring(0, 4);
				}
				tLYOutPayDetailSchema.setprocode(riskcode);
				tLYOutPayDetailSchema.setbusitype(moneyType);//同价税分离接口的收支类型
				tLYOutPayDetailSchema.settrancurrency(tSSRS.GetText(i, 16));// 币种：人民币
				tLYOutPayDetailSchema.settranamt(tSSRS.GetText(i, 17));//交易金额（含税）
				tLYOutPayDetailSchema.settaxtype(tSSRS.GetText(i, 18));//金额是否含税
				String paymentflag="0";//“0”表示收入，“1”表示支出
				if("02".equals(tSSRS.GetText(i, 19))){//价税分离表01表示收费 02表示付费
					paymentflag="1";
				}
				tLYOutPayDetailSchema.setpaymentflag(paymentflag);
				tLYOutPayDetailSchema.setoverseasflag(tSSRS.GetText(i, 20));
				tLYOutPayDetailSchema.setisbill(tSSRS.GetText(i, 21));
				tLYOutPayDetailSchema.setareatype(tSSRS.GetText(i, 22));
				tLYOutPayDetailSchema.setinsureno(tSSRS.GetText(i, 23));//印刷号
				tLYOutPayDetailSchema.setbilleffectivedate(tSSRS.GetText(i, 24));
				tLYOutPayDetailSchema.setbqbatchno(tSSRS.GetText(i, 25));
				tLYOutPayDetailSchema.setvdata(mCurrentDate);
				tLYOutPayDetailSchema.setvdef2(tSSRS.GetText(i, 14));
				tLYOutPayDetailSchema.setmoneyno(tSSRS.GetText(i, 26));
				tLYOutPayDetailSchema.setmoneytype(tSSRS.GetText(i, 19));
				//备注
				String vdef1="保单号:"+tSSRS.GetText(i, 14)+";险种："+tSSRS.GetText(i, 27);
				tLYOutPayDetailSchema.setvdef1(vdef1);
				tLYOutPayDetailSchema.setvoucherid(tSSRS.GetText(i, 14));//保单号
				tLYOutPayDetailSchema.setprebilltype("02");//非预打
				tLYOutPayDetailSchema.setinvtype("0");//默认0
				tLYOutPayDetailSchema.setvaddress("0");
				tLYOutPayDetailSchema.setvsrcsystem("01");
				tLYOutPayDetailSchema.settaxpayertype("01");//管理平台提供的默认值
				tLYOutPayDetailSchema.setenablestate("2");//默认值
				
				tLYOutPayDetailSchema.setoperator(mGI.Operator);
				tLYOutPayDetailSchema.setMakeDate(mCurrentDate);
				tLYOutPayDetailSchema.setMakeTime(mCurrentTime);
				tLYOutPayDetailSchema.setModifyDate(mCurrentDate);
				tLYOutPayDetailSchema.setModifyTime(mCurrentTime);
				
				tLYOutPayDetailSet.add(tLYOutPayDetailSchema);
			}
		}
			System.out.println("保全收费发票数据提取完成："+tLYOutPayDetailSet.size());
			//团单保全付费数据
			tSQL = " select "
		        + " trim(ljag.FeeOperationType)||trim(ljag.FeeFinaType)||trim(ljag.PolNo)||trim(ljag.DutyCode),"
				+ " (select managecom from lcgrpcont where grpcontno=ljag.grpcontno union select managecom from lbgrpcont where grpcontno=ljag.grpcontno) , "//所属组织
				+ " (select appntno from lcgrpcont where grpcontno=ljag.grpcontno union select appntno from lbgrpcont where grpcontno=ljag.grpcontno) , "//客户号
				+ " (select grpname from lcgrpcont where grpcontno=ljag.grpcontno union select grpname from lbgrpcont where grpcontno=ljag.grpcontno) , "
				+ " '1', "//客户类型:“1”代表机构，“2”代表个人
				+ " '01', "//价税分离项目:固定值
				+ " ljag.taxrate,"
				+ " ljag.moneyNoTax,"
				+ " ljag.moneyTax, "
				+ " '0', "//申报类型：0正常申报，固定值
				+ " '01', "//来源系统:固定值
				+ " lja.confdate,"//交易日期
				+ " ljag.riskcode,"//产品
				+ " ljag.grpcontno,"
				+ " ljag.busitype,"//业务类型
				+ " 'CNY',"//交易币种
				+ " ljag.GetMoney,"//交易金额
				+ " '0', "//金额是否含税:“0”表示含税，“1”表示不含税
				+ " '02',"//收付标志
//				+ " lyp.moneytype,"//收付标志
				+ " '0',"//境内外标示:“0”表示境内，“1”表示境外
				+ " 'Y',"//是否开票
				+ " '0',"//汇总地类型:“0”表示汇总，“1”表示属地，后续纳税申报需要
				+ " (select prtno from lcgrpcont where grpcontno=ljag.grpcontno union select prtno from lbgrpcont where grpcontno=ljag.grpcontno),"	//印刷号			
//				+ " lyp.prtno, "//投保单号
				+ " (select signdate from lcgrpcont where grpcontno=ljag.grpcontno union select signdate from lbgrpcont where grpcontno=ljag.grpcontno) ,"//签单号
				+ " lja.otherno, "//批单号
//				+ " lyp.moneyno moneyno, "
				+ " ljag.ActugetNo moneyno, "
				+ " (select riskname from lmriskapp where riskcode=ljag.riskcode)"
				+ " from ljagetendorse ljag,ljaget lja "
				+ " where ljag.actugetno=lja.actugetno "
				+ " and lja.otherno = ljag.EndorsementNo"
				+ " and ljag.moneyNoTax is not null "
		        + " and ljag.busitype is not null "
				+ " and lja.othernotype='3' "//团单保全
		        + " and ljag.FeeOperationType not in ('YEI','YEO','YS') "
				+ " and ljag.ActugetNo is not null"
				+ " and not exists (select 1 from LYOutPayDetail where busino= trim(ljag.FeeOperationType)||trim(ljag.FeeFinaType)||trim(ljag.PolNo)||trim(ljag.DutyCode) and state<>'03') "
				+ " and exists (select 1 from lcgrpcont where grpcontno=ljag.grpcontno union select 1 from lbgrpcont where grpcontno=ljag.grpcontno) "	
				+ tWhereSQL	
				+ mWherePart
				+ " with ur";
			
			tSSRS = new ExeSQL().execSQL(tSQL);
			if (tSSRS != null && tSSRS.getMaxRow() > 0) {
				for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
					LYOutPayDetailSchema tLYOutPayDetailSchema = new LYOutPayDetailSchema();
					tLYOutPayDetailSchema.setBusiNo(tSSRS.GetText(i, 1));
					tLYOutPayDetailSchema.settranserial(tSSRS.GetText(i, 1));//交易流水号
					tLYOutPayDetailSchema.setbusipk(tSSRS.GetText(i, 1));
					tLYOutPayDetailSchema.setisupdate("Y");//是否更新客户信息
					tLYOutPayDetailSchema.setgroup("00");//所属集团:固定值
					tLYOutPayDetailSchema.setpkgroup("00");//所属集团:固定值
					tLYOutPayDetailSchema.setpkorg(tSSRS.GetText(i, 2));//所属组织
					tLYOutPayDetailSchema.setorg(tSSRS.GetText(i, 2));//所属组织
					tLYOutPayDetailSchema.setorgv(tSSRS.GetText(i, 2));//所属组织
					tLYOutPayDetailSchema.setdeptdoc(tSSRS.GetText(i, 2));
					tLYOutPayDetailSchema.setcode(tSSRS.GetText(i, 3));//客户编码
					tLYOutPayDetailSchema.setname(tSSRS.GetText(i, 4));//客户名称
	//				tLYOutPayDetailSchema.setcustomertype(tSSRS.GetText(i, 5));//客户类型
					tLYOutPayDetailSchema.setcustomertype("2");//modify 20160604 管理平台提供的默认值
					tLYOutPayDetailSchema.setcustcode(tSSRS.GetText(i, 3));
					tLYOutPayDetailSchema.setcustname(tSSRS.GetText(i, 4));
	//				tLYOutPayDetailSchema.setcusttype(tSSRS.GetText(i, 5));
					tLYOutPayDetailSchema.setcusttype("2");
					
					tLYOutPayDetailSchema.setptsitem(tSSRS.GetText(i, 6));
					tLYOutPayDetailSchema.settaxrate(tSSRS.GetText(i, 7));//税率
					tLYOutPayDetailSchema.setoriamt(tSSRS.GetText(i, 8));//不含税金额
					tLYOutPayDetailSchema.setoritax(tSSRS.GetText(i, 9));//税额
					tLYOutPayDetailSchema.setlocalamt(tSSRS.GetText(i, 8));//不含税金额（本币）
					tLYOutPayDetailSchema.setlocaltax(tSSRS.GetText(i, 9));//税额（本币）
					tLYOutPayDetailSchema.setdectype(tSSRS.GetText(i, 10));//申报类型
					tLYOutPayDetailSchema.setsrcsystem(tSSRS.GetText(i, 11));
					tLYOutPayDetailSchema.settrandate(tSSRS.GetText(i, 12));//交易日期
					String riskcode = tSSRS.GetText(i, 13);
					String moneyType = tSSRS.GetText(i, 15);
					if ("09".equals(moneyType)) {//工本费riskcode置0000
						riskcode = BQ.FILLDATA;
					} else {
						riskcode = riskcode.substring(0, 4);
					}
					tLYOutPayDetailSchema.setprocode(riskcode);
					tLYOutPayDetailSchema.setbusitype(moneyType);//同价税分离接口的收支类型
					tLYOutPayDetailSchema.settrancurrency(tSSRS.GetText(i, 16));// 币种：人民币
					tLYOutPayDetailSchema.settranamt(tSSRS.GetText(i, 17));//交易金额（含税）
					tLYOutPayDetailSchema.settaxtype(tSSRS.GetText(i, 18));//金额是否含税
					String paymentflag="0";//“0”表示收入，“1”表示支出
					if("02".equals(tSSRS.GetText(i, 19))){//价税分离表01表示收费 02表示付费
						paymentflag="1";
					}
					tLYOutPayDetailSchema.setpaymentflag(paymentflag);
					tLYOutPayDetailSchema.setoverseasflag(tSSRS.GetText(i, 20));
					tLYOutPayDetailSchema.setisbill(tSSRS.GetText(i, 21));
					tLYOutPayDetailSchema.setareatype(tSSRS.GetText(i, 22));
					tLYOutPayDetailSchema.setinsureno(tSSRS.GetText(i, 23));//印刷号
					tLYOutPayDetailSchema.setbilleffectivedate(tSSRS.GetText(i, 24));
					tLYOutPayDetailSchema.setbqbatchno(tSSRS.GetText(i, 25));
					tLYOutPayDetailSchema.setvdata(mCurrentDate);
					tLYOutPayDetailSchema.setvdef2(tSSRS.GetText(i, 14));
					tLYOutPayDetailSchema.setmoneyno(tSSRS.GetText(i, 26));
					tLYOutPayDetailSchema.setmoneytype(tSSRS.GetText(i, 19));
					//备注
					String vdef1="保单号:"+tSSRS.GetText(i, 14)+";险种："+tSSRS.GetText(i, 27);
					tLYOutPayDetailSchema.setvdef1(vdef1);
					tLYOutPayDetailSchema.setvoucherid(tSSRS.GetText(i, 14));//保单号
					tLYOutPayDetailSchema.setprebilltype("02");//非预打
					tLYOutPayDetailSchema.setinvtype("0");//默认0
					tLYOutPayDetailSchema.setvaddress("0");
					tLYOutPayDetailSchema.setvsrcsystem("01");
					tLYOutPayDetailSchema.settaxpayertype("01");//管理平台提供的默认值
					tLYOutPayDetailSchema.setenablestate("2");//默认值
					
					tLYOutPayDetailSchema.setoperator(mGI.Operator);
					tLYOutPayDetailSchema.setMakeDate(mCurrentDate);
					tLYOutPayDetailSchema.setMakeTime(mCurrentTime);
					tLYOutPayDetailSchema.setModifyDate(mCurrentDate);
					tLYOutPayDetailSchema.setModifyTime(mCurrentTime);
					
					tLYOutPayDetailSet.add(tLYOutPayDetailSchema);
				}
			}
			
		}
		
		if(null == mPolicyType || "1".equals(mPolicyType) ){

			//个单保全收费数据
			String tSQL2 = " select "
					+ " trim(ljag.FeeOperationType)||trim(ljag.FeeFinaType)||trim(ljag.PolNo)||trim(ljag.DutyCode),"
					+ " (select managecom from lccont where contno=ljag.contno union select managecom from lbcont where contno=ljag.contno) , "// 所属组织
					+ " (select appntno from lccont where contno=ljag.contno union select appntno from lbcont where contno=ljag.contno) , "// 客户号
					+ " (select appntname from lcappnt where contno=ljag.contno union select appntname from lbappnt where contno=ljag.contno)  , "
					+ " '2', "// 客户类型:“1”代表机构，“2”代表个人
					+ " '01', "// 价税分离项目:固定值
				    + " ljag.taxrate,"
					+ " ljag.moneyNoTax,"
					+ " ljag.moneyTax, "
					+ " '0', "// 申报类型：0正常申报，固定值
					+ " '01', "// 来源系统:固定值
					+ " lja.confdate,"// 交易日期
					+ " ljag.riskcode,"// 产品
					+ " ljag.contno,"
				    + " ljag.busitype,"//业务类型
					+ " 'CNY',"// 交易币种
					+ " ljag.GetMoney,"// 交易金额
					+ " '0', "// 金额是否含税:“0”表示含税，“1”表示不含税
					+ " '01',"// 收付标志
					// + " lyp.moneytype,"//收付标志
					+ " '0',"// 境内外标示:“0”表示境内，“1”表示境外
					+ " 'Y',"// 是否开票
					+ " '0',"// 汇总地类型:“0”表示汇总，“1”表示属地，后续纳税申报需要
					+ " (select prtno from lccont where contno=ljag.contno union select prtno from lbcont where contno=ljag.contno),"	//印刷号			
//					+ " lyp.prtno, "// 投保单号
					+ " (select signdate from lccont where contno=ljag.contno union select signdate from lbcont where contno=ljag.contno) ,"// 签单号
					+ " lja.incomeno, "// 批单号
					+ " ljag.ActugetNo moneyno, "
					+ " (select riskname from lmriskapp where riskcode=ljag.riskcode)"
					+ " from ljagetendorse ljag,ljapay lja "
					+ " where 1=1"
					+ " and ljag.EndorsementNo =lja.incomeno"
					+ " and lja.incometype ='10'"
			        + " and ljag.FeeOperationType not in ('YEI','YEO','YS') "
					+ " and ljag.ActugetNo=lja.payno "
					+ " and ljag.moneyNoTax is not null "
//					+ " and lja.confdate is not null "
			        + " and ljag.busitype is not null "
//					+ " and lyp.moneyno is not null "
					+ " and ljag.ActugetNo is not null"
					+ " and not exists (select 1 from LYOutPayDetail where busino= trim(ljag.FeeOperationType)||trim(ljag.FeeFinaType)||trim(ljag.PolNo)||trim(ljag.DutyCode) and state<>'03') "
					+ " and exists (select 1 from lccont where conttype ='1' and contno=ljag.contno union select 1 from lbcont where  conttype ='1' and  contno=ljag.contno) "
					+  mWherePart
					+ tWhereSQL	
					+ " with ur";
			
			SSRS tSSRS2 = new ExeSQL().execSQL(tSQL2);
			if (tSSRS2 != null && tSSRS2.getMaxRow() > 0) {
				for (int i = 1; i <= tSSRS2.getMaxRow(); i++) {
					LYOutPayDetailSchema tLYOutPayDetailSchema = new LYOutPayDetailSchema();
					tLYOutPayDetailSchema.setBusiNo(tSSRS2.GetText(i, 1));
					tLYOutPayDetailSchema.settranserial(tSSRS2.GetText(i, 1));//交易流水号
					tLYOutPayDetailSchema.setbusipk(tSSRS2.GetText(i, 1));
					tLYOutPayDetailSchema.setisupdate("Y");//是否更新客户信息
					tLYOutPayDetailSchema.setgroup("00");//所属集团:固定值
					tLYOutPayDetailSchema.setpkgroup("00");//所属集团:固定值
					tLYOutPayDetailSchema.setpkorg(tSSRS2.GetText(i, 2));//所属组织
					tLYOutPayDetailSchema.setorg(tSSRS2.GetText(i, 2));//所属组织
					tLYOutPayDetailSchema.setorgv(tSSRS2.GetText(i, 2));//所属组织
					tLYOutPayDetailSchema.setdeptdoc(tSSRS2.GetText(i, 2));
					tLYOutPayDetailSchema.setcode(tSSRS2.GetText(i, 3));//客户编码
					tLYOutPayDetailSchema.setname(tSSRS2.GetText(i, 4));//客户名称
	//				tLYOutPayDetailSchema.setcustomertype(tSSRS2.GetText(i, 5));//客户类型
					tLYOutPayDetailSchema.setcustomertype("2");//modify 20160604 管理平台提供的默认值
					tLYOutPayDetailSchema.setcustcode(tSSRS2.GetText(i, 3));
					tLYOutPayDetailSchema.setcustname(tSSRS2.GetText(i, 4));
	//				tLYOutPayDetailSchema.setcusttype(tSSRS2.GetText(i, 5));
					tLYOutPayDetailSchema.setcusttype("2");				
					tLYOutPayDetailSchema.setptsitem(tSSRS2.GetText(i, 6));
					tLYOutPayDetailSchema.settaxrate(tSSRS2.GetText(i, 7));//税率
					tLYOutPayDetailSchema.setoriamt(tSSRS2.GetText(i, 8));//不含税金额
					tLYOutPayDetailSchema.setoritax(tSSRS2.GetText(i, 9));//税额
					tLYOutPayDetailSchema.setlocalamt(tSSRS2.GetText(i, 8));//不含税金额（本币）
					tLYOutPayDetailSchema.setlocaltax(tSSRS2.GetText(i, 9));//税额（本币）
					tLYOutPayDetailSchema.setdectype(tSSRS2.GetText(i, 10));//申报类型
					tLYOutPayDetailSchema.setsrcsystem(tSSRS2.GetText(i, 11));
					tLYOutPayDetailSchema.settrandate(tSSRS2.GetText(i, 12));//交易日期
					String riskcode = tSSRS2.GetText(i, 13);
					String moneyType = tSSRS2.GetText(i, 15);
					if ("09".equals(moneyType)) {//工本费riskcode置0000
						riskcode = BQ.FILLDATA;
					} else {
						riskcode = riskcode.substring(0, 4);
					}
					tLYOutPayDetailSchema.setprocode(riskcode);
					tLYOutPayDetailSchema.setbusitype(moneyType);//同价税分离接口的收支类型
					tLYOutPayDetailSchema.settrancurrency(tSSRS2.GetText(i, 16));// 币种：人民币
					tLYOutPayDetailSchema.settranamt(tSSRS2.GetText(i, 17));//交易金额（含税）
					tLYOutPayDetailSchema.settaxtype(tSSRS2.GetText(i, 18));//金额是否含税
					String paymentflag="0";//“0”表示收入，“1”表示支出
					if("02".equals(tSSRS2.GetText(i, 19))){//价税分离表01表示收费 02表示付费
						paymentflag="1";
					}
					tLYOutPayDetailSchema.setpaymentflag(paymentflag);
					tLYOutPayDetailSchema.setoverseasflag(tSSRS2.GetText(i, 20));
					tLYOutPayDetailSchema.setisbill(tSSRS2.GetText(i, 21));
					tLYOutPayDetailSchema.setareatype(tSSRS2.GetText(i, 22));
					tLYOutPayDetailSchema.setinsureno(tSSRS2.GetText(i, 23));//印刷号
					tLYOutPayDetailSchema.setbilleffectivedate(tSSRS2.GetText(i, 24));
					tLYOutPayDetailSchema.setbqbatchno(tSSRS2.GetText(i, 25));
					tLYOutPayDetailSchema.setvdata(mCurrentDate);
					tLYOutPayDetailSchema.setvdef2(tSSRS2.GetText(i, 14));
					tLYOutPayDetailSchema.setmoneyno(tSSRS2.GetText(i, 26));
					tLYOutPayDetailSchema.setmoneytype(tSSRS2.GetText(i, 19));
					//备注
					String vdef1="保单号:"+tSSRS2.GetText(i, 14)+";险种："+tSSRS2.GetText(i, 27);
					tLYOutPayDetailSchema.setvdef1(vdef1);
					tLYOutPayDetailSchema.setvoucherid(tSSRS2.GetText(i, 14));//保单号
					tLYOutPayDetailSchema.setprebilltype("02");//非预打
					tLYOutPayDetailSchema.setinvtype("0");//默认0
					tLYOutPayDetailSchema.setvaddress("0");
					tLYOutPayDetailSchema.setvsrcsystem("01");
					tLYOutPayDetailSchema.settaxpayertype("01");//管理平台提供的默认值
					tLYOutPayDetailSchema.setenablestate("2");//默认值
					
					tLYOutPayDetailSchema.setoperator(mGI.Operator);
					tLYOutPayDetailSchema.setMakeDate(mCurrentDate);
					tLYOutPayDetailSchema.setMakeTime(mCurrentTime);
					tLYOutPayDetailSchema.setModifyDate(mCurrentDate);
					tLYOutPayDetailSchema.setModifyTime(mCurrentTime);
					
					tLYOutPayDetailSet.add(tLYOutPayDetailSchema);
				}
			}
			
			//个单保全付费数据
		      String tSQL = " select "
				    + " trim(ljag.FeeOperationType)||trim(ljag.FeeFinaType)||trim(ljag.PolNo)||trim(ljag.DutyCode),"
					+ " (select managecom from lccont where contno=ljag.contno union select managecom from lbcont where contno=ljag.contno) , "//所属组织
					+ " (select appntno from lccont where contno=ljag.contno union select appntno from lbcont where contno=ljag.contno) , "//客户号
					+ " (select appntname from lcappnt where contno=ljag.contno union select appntname from lbappnt where contno=ljag.contno) , "
					+ " '2', "//客户类型:“1”代表机构，“2”代表个人
					+ " '01', "//价税分离项目:固定值
					+ " ljag.taxrate,"
					+ " ljag.moneyNoTax,"
					+ " ljag.moneyTax, "
					+ " '0', "//申报类型：0正常申报，固定值
					+ " '01', "//来源系统:固定值
					+ " lja.confdate,"//交易日期
					+ " ljag.riskcode,"//产品
					+ " ljag.contno,"
					+ " ljag.busitype,"//业务类型
					+ " 'CNY',"//交易币种
					+ " ljag.GetMoney,"//交易金额
					+ " '0', "//金额是否含税:“0”表示含税，“1”表示不含税
					+ " '02',"//收付标志
//					+ " lyp.moneytype,"//收付标志
					+ " '0',"//境内外标示:“0”表示境内，“1”表示境外
					+ " 'Y',"//是否开票
					+ " '0',"//汇总地类型:“0”表示汇总，“1”表示属地，后续纳税申报需要
					+ " (select prtno from lccont where contno=ljag.contno union select prtno from lbcont where contno=ljag.contno),"	//印刷号			
//					+ " lyp.prtno, "//投保单号
					+ " (select signdate from lccont where contno=ljag.contno union select signdate from lbcont where contno=ljag.contno) ,"//签单号
					+ " lja.otherno, "//批单号
//					+ " lyp.moneyno moneyno, "
					+ " ljag.ActugetNo moneyno, "
					+ " (select riskname from lmriskapp where riskcode=ljag.riskcode)"
				    + " from ljagetendorse ljag,ljaget lja "
					+ " where ljag.actugetno=lja.actugetno "
					+ " and lja.otherno = ljag.EndorsementNo"
					+ " and ljag.moneyNoTax is not null "
                    + " and ljag.busitype is not null "                  
					+ " and lja.othernotype='10' "//个单保全
			        + " and ljag.FeeOperationType not in ('YEI','YEO','YS') "
//					+ " and lja.confdate is not null "//实付后   	//财务接口有 开票-付费的场景
					+ " and ljag.ActugetNo is not null"
				    + " and not exists (select 1 from LYOutPayDetail where busino=trim(ljag.FeeOperationType)||trim(ljag.FeeFinaType)||trim(ljag.PolNo)||trim(ljag.DutyCode) and state<>'03') "
				    + " and exists (select 1 from lccont where conttype ='1' and contno=ljag.contno union select 1 from lbcont where conttype ='1' and  contno=ljag.contno) "
				    + mWherePart		
				    + tWhereSQL	
				    + " with ur";
			
		     SSRS tSSRS = new ExeSQL().execSQL(tSQL);
			if (tSSRS != null && tSSRS.getMaxRow() > 0) {
				for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
					LYOutPayDetailSchema tLYOutPayDetailSchema = new LYOutPayDetailSchema();
					tLYOutPayDetailSchema.setBusiNo(tSSRS.GetText(i, 1));
					tLYOutPayDetailSchema.settranserial(tSSRS.GetText(i, 1));//交易流水号
					tLYOutPayDetailSchema.setbusipk(tSSRS.GetText(i, 1));
					tLYOutPayDetailSchema.setisupdate("Y");//是否更新客户信息
					tLYOutPayDetailSchema.setgroup("00");//所属集团:固定值
					tLYOutPayDetailSchema.setpkgroup("00");//所属集团:固定值
					tLYOutPayDetailSchema.setpkorg(tSSRS.GetText(i, 2));//所属组织
					tLYOutPayDetailSchema.setorg(tSSRS.GetText(i, 2));//所属组织
					tLYOutPayDetailSchema.setorgv(tSSRS.GetText(i, 2));//所属组织
					tLYOutPayDetailSchema.setdeptdoc(tSSRS.GetText(i, 2));
					tLYOutPayDetailSchema.setcode(tSSRS.GetText(i, 3));//客户编码
					tLYOutPayDetailSchema.setname(tSSRS.GetText(i, 4));//客户名称
//					tLYOutPayDetailSchema.setcustomertype(tSSRS.GetText(i, 5));//客户类型
					tLYOutPayDetailSchema.setcustomertype("2");//modify 20160604 管理平台提供的默认值
					tLYOutPayDetailSchema.setcustcode(tSSRS.GetText(i, 3));
					tLYOutPayDetailSchema.setcustname(tSSRS.GetText(i, 4));
//					tLYOutPayDetailSchema.setcusttype(tSSRS.GetText(i, 5));
					tLYOutPayDetailSchema.setcusttype("2");
					
					tLYOutPayDetailSchema.setptsitem(tSSRS.GetText(i, 6));
					tLYOutPayDetailSchema.settaxrate(tSSRS.GetText(i, 7));//税率
					tLYOutPayDetailSchema.setoriamt(tSSRS.GetText(i, 8));//不含税金额
					tLYOutPayDetailSchema.setoritax(tSSRS.GetText(i, 9));//税额
					tLYOutPayDetailSchema.setlocalamt(tSSRS.GetText(i, 8));//不含税金额（本币）
					tLYOutPayDetailSchema.setlocaltax(tSSRS.GetText(i, 9));//税额（本币）
					tLYOutPayDetailSchema.setdectype(tSSRS.GetText(i, 10));//申报类型
					tLYOutPayDetailSchema.setsrcsystem(tSSRS.GetText(i, 11));
					tLYOutPayDetailSchema.settrandate(tSSRS.GetText(i, 12));//交易日期
					String riskcode = tSSRS.GetText(i, 13);

					String moneyType = tSSRS.GetText(i, 15);
					if ("09".equals(moneyType)) {//工本费riskcode置0000
						riskcode = BQ.FILLDATA;
					} else {
						riskcode = riskcode.substring(0, 4);
					}
					tLYOutPayDetailSchema.setprocode(riskcode);
					tLYOutPayDetailSchema.setbusitype(moneyType);//同价税分离接口的收支类型
					tLYOutPayDetailSchema.settrancurrency(tSSRS.GetText(i, 16));// 币种：人民币
					tLYOutPayDetailSchema.settranamt(tSSRS.GetText(i, 17));//交易金额（含税）
					tLYOutPayDetailSchema.settaxtype(tSSRS.GetText(i, 18));//金额是否含税
					String paymentflag="0";//“0”表示收入，“1”表示支出
					if("02".equals(tSSRS.GetText(i, 19))){//价税分离表01表示收费 02表示付费
						paymentflag="1";
					}
					tLYOutPayDetailSchema.setpaymentflag(paymentflag);
					tLYOutPayDetailSchema.setoverseasflag(tSSRS.GetText(i, 20));
					tLYOutPayDetailSchema.setisbill(tSSRS.GetText(i, 21));
					tLYOutPayDetailSchema.setareatype(tSSRS.GetText(i, 22));
					tLYOutPayDetailSchema.setinsureno(tSSRS.GetText(i, 23));//印刷号
					tLYOutPayDetailSchema.setbilleffectivedate(tSSRS.GetText(i, 24));
					tLYOutPayDetailSchema.setbqbatchno(tSSRS.GetText(i, 25));
					tLYOutPayDetailSchema.setvdata(mCurrentDate);
					tLYOutPayDetailSchema.setvdef2(tSSRS.GetText(i, 14));
					tLYOutPayDetailSchema.setmoneyno(tSSRS.GetText(i, 26));
					tLYOutPayDetailSchema.setmoneytype(tSSRS.GetText(i, 19));
					//备注
					String vdef1="保单号:"+tSSRS.GetText(i, 14)+";险种："+tSSRS.GetText(i, 27);
					tLYOutPayDetailSchema.setvdef1(vdef1);
					tLYOutPayDetailSchema.setvoucherid(tSSRS.GetText(i, 14));//保单号
					tLYOutPayDetailSchema.setprebilltype("02");//非预打
					tLYOutPayDetailSchema.setinvtype("0");//默认0
					tLYOutPayDetailSchema.setvaddress("0");
					tLYOutPayDetailSchema.setvsrcsystem("01");
					tLYOutPayDetailSchema.settaxpayertype("01");//管理平台提供的默认值
					tLYOutPayDetailSchema.setenablestate("2");//默认值
					
					tLYOutPayDetailSchema.setoperator(mGI.Operator);
					tLYOutPayDetailSchema.setMakeDate(mCurrentDate);
					tLYOutPayDetailSchema.setMakeTime(mCurrentTime);
					tLYOutPayDetailSchema.setModifyDate(mCurrentDate);
					tLYOutPayDetailSchema.setModifyTime(mCurrentTime);
					
					tLYOutPayDetailSet.add(tLYOutPayDetailSchema);
				}
			}
			
			System.out.println("保全收付费发票数据提取完成："+tLYOutPayDetailSet.size());
		}
		//调用发票接口上报数据
		VData tVData = new VData();
		tVData.add(mGI);
		tVData.add(tLYOutPayDetailSet);
		OutPayUploadBL tOutPayUploadBL = new OutPayUploadBL();
		if (!tOutPayUploadBL.getSubmit(tVData, "")) {
			this.mErrors = tOutPayUploadBL.mErrors;
			System.out.println("保全收付费发票数据上报错误:"+ mErrors.getErrContent());
		}else{
			//保存返回结果
			mMap = tOutPayUploadBL.getResult();
		}
		
		return true;
	}

	/**
	 * 准备后台的数据
	 * 
	 * @return boolean
	 */
	private boolean prepareOutputData() {
		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GetBqPremBillBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	public static void main(String[] args) {
		GetBQBillBL tGetBQBillBL = new GetBQBillBL();
		VData cInputData = new VData();
		TransferData tTransferData = new TransferData();
		String tStartDate = "2016-01-01";
		String tEndDate = "2016-08-01";
		tTransferData.setNameAndValue("StartDate", tStartDate);
		tTransferData.setNameAndValue("EndDate", tEndDate);
//		LYPremSeparateDetailSet set=new LYPremSeparateDetailSet();
//		String [] str=new String[]{"20160418000824","20160615000899"};//个单收费记录号
//		for(int i=0;i<str.length; i++){
//			LYPremSeparateDetailSchema Schema=new LYPremSeparateDetailSchema();
//			Schema.setOtherNo(str[i]);
//			set.add(Schema);
//		}
//		tTransferData.setNameAndValue("HandWorkFlag","Y");
//		cInputData.add(set);
		cInputData.add(tTransferData);
		tGetBQBillBL.submitData(cInputData, "");
	}
}
