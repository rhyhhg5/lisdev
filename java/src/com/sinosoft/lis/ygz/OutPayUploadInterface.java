package com.sinosoft.lis.ygz;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.xml.rpc.ParameterMode;
import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.ygz.obj.OutPayBillInfo;
import com.sinosoft.lis.ygz.obj.OutPayHead;
import com.sinosoft.lis.ygz.obj.OutPaySendResult;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * <p>
 * Title: 营改增，开票接口
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author Alex
 * @version 1.0
 */

public class OutPayUploadInterface {

	public CErrors mErrors = new CErrors();

	private String requestURL = "";
	
	private String reponseURL = "";
	
	private String mBatchNo = "" ;
	
	private String targetEendPoint = "";
	
	private String targetNameSpace = "";
	
	private OutPayHead head = null;
	
	public OutPayUploadInterface() {

	}

	/**
	 * 依据请求报文对象集合调用接口
	 * 并获取发票分离结果，返回SendResult对象
	 * 
	 */
	public List<OutPaySendResult> callInterface(List<OutPayBillInfo> billInfoList){
		//数据准备
		if(!initData()){
			return null;
		}
		//封装请求报文
		Document requestDoc = packageRequest(head, billInfoList);
		//发送请求
		Document responseDoc=request(requestDoc);
		if(null == responseDoc){
			System.out.println("获取返回结果失败");
		}
		//解析返回报文
		List<OutPaySendResult> result=parseDoc(responseDoc);
		
		return result;
	}

	private Document request(Document requestDoc) {
		String filePath =requestURL + mBatchNo + ".xml";
		try {
			//请求报文保存至应用服务器
			XMLOutputter outPutter = new XMLOutputter("  ", true, "UTF-8");
			FileOutputStream outStream = new FileOutputStream(filePath);
			outPutter.output(requestDoc, outStream);
			
			//读取报文调用接口
			InputStream mIs = new FileInputStream(filePath);
			byte[] mInXmlBytes = InputStreamToBytes(mIs);
			String mInXmlStr = new String(mInXmlBytes, "UTF-8");
			System.out.println("发票打印请求报文："+mInXmlStr);
			
			Service service = new Service();
			Call call = null;
			call = (Call) service.createCall();
			call.setTargetEndpointAddress(new URL(targetEendPoint));
			call.setOperationName("synchronizeData");// 接口方法名称
			call.addParameter("string", org.apache.axis.Constants.XSD_STRING,
					ParameterMode.IN);
			call.setReturnType(org.apache.axis.Constants.XSD_STRING);
			String result =(String) call.invoke(new Object[] { mInXmlStr });
			
			//System.out.println("发票打印返回报文:" + result);
			
			StringReader tInXmlReader = new StringReader(result);
			SAXBuilder tSAXBuilder = new SAXBuilder();
			Document responseDoc = tSAXBuilder.build(tInXmlReader);
			
			String reponseFilePath = reponseURL + mBatchNo + ".xml";
			outStream = new FileOutputStream(reponseFilePath);
			outPutter.output(responseDoc, outStream);
			//modify by zxs 删除发送报文
			File file = new File(filePath);
			if (file.isFile() && file.exists()) {
			file.delete();
			}
			return responseDoc;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}
		
	}
	
	private boolean initData(){
		//获取系统根目录
		String str = "select sysvarvalue from LDSysVar where sysvar='ServerRoot'";
		String url = new ExeSQL().getOneValue(str);
//		String url ="F:/";
		requestURL = url+"ygzpayuxml/request/"+PubFun.getCurrentDate2()+"/";
		reponseURL = url+"ygzpayuxml/reponse/"+PubFun.getCurrentDate2()+"/";
		File mFileDir = new File(requestURL);
		if (!mFileDir.exists()) {
			if (!mFileDir.mkdirs()) {
				CError tError = new CError();
				tError.moduleName = "OutPayUploadInterface";
				tError.functionName = "initData";
				tError.errorMessage = "创建目录[" + url.toString() + "]失败！"+ mFileDir.getPath();
				this.mErrors.addOneError(tError);
				return false;
			}
		}
		mFileDir = new File(reponseURL);
		if (!mFileDir.exists()) {
			if (!mFileDir.mkdirs()) {
				CError tError = new CError();
				tError.moduleName = "OutPayUploadInterface";
				tError.functionName = "initData";
				tError.errorMessage = "创建目录[" + url.toString() + "]失败！"+ mFileDir.getPath();
				this.mErrors.addOneError(tError);
				return false;
			}
		}
		
		SSRS tSSRS= new ExeSQL().execSQL("select codename,codealias from ldcode where codetype='outpayupload' ");
		if(null == tSSRS || tSSRS.MaxRow == 0){
			CError tError = new CError();
			tError.moduleName = "OutPayUploadInterface";
			tError.functionName = "initData";
			tError.errorMessage = "获取目标接口信息失败";
			this.mErrors.addOneError(tError);
			return false;
		}
		targetEendPoint=tSSRS.GetText(1, 1);
		targetNameSpace=tSSRS.GetText(1, 2);
		
		
		//为本次请求生成批次号
		String tCurrentDate2=PubFun.getCurrentDate2();
		String tCurrentTime2=PubFun.getCurrentTime2();
		mBatchNo = tCurrentTime2+PubFun1.CreateMaxNo("OUTA"+tCurrentDate2, 6);
		System.out.println("本次请求批次号："+mBatchNo);
		
		//初始化报文头
		head =  new OutPayHead();
		head.setDefault();
		head.setSender("01");
//		head.setFilename(mBatchNo+".xml");
		
		return true;
	}
	
	private static byte[] InputStreamToBytes(InputStream pIns) {
		ByteArrayOutputStream mByteArrayOutputStream = new ByteArrayOutputStream();
		
		try {
			byte[] tBytes = new byte[8*1024];
			for (int tReadSize; -1 != (tReadSize=pIns.read(tBytes)); ) {
				mByteArrayOutputStream.write(tBytes, 0, tReadSize);
			}
		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		} finally {
			try {
				pIns.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		
		return mByteArrayOutputStream.toByteArray();
	}

	private Document packageRequest(OutPayHead head, List billInfoList) {
		if (null == head || billInfoList.size() == 0) {
			return null;
		}
		// 根节点
		Element ufinterface = new Element("ufinterface");
		Document Doc = new Document(ufinterface);
		// 节点属性
		ufinterface.addAttribute("account", head.getAccount());
		ufinterface.addAttribute("billtype", head.getBilltype());
		ufinterface.addAttribute("filename", head.getFilename());
		ufinterface.addAttribute("groupcode", head.getGroupcode());
		ufinterface.addAttribute("isexchange", head.getIsexchange());
		ufinterface.addAttribute("replace", head.getReplace());
		ufinterface.addAttribute("roottag", head.getRoottag());
		ufinterface.addAttribute("sender", head.getSender());

		// 对每笔业务数据进行封装
		for (int i = 0; i < billInfoList.size(); i++) {
			OutPayBillInfo tBillInfo = (OutPayBillInfo) billInfoList.get(i);
			Element billElement = new Element("bill");
			billElement.addAttribute("id", tBillInfo.getSequenceNo());

			Element billHeadElement = new Element("billhead");
			Element billContentElement=new Element("pk_custinfo");
			Element itemHeadElement = new Element("item"); 
			itemHeadElement.addContent(new Element("pk_custinfo").setText(tBillInfo.getPk_custinfo1()));
			itemHeadElement.addContent(new Element("isupdate").setText(tBillInfo.getIsupdate()));
			itemHeadElement.addContent(new Element("pk_group").setText(tBillInfo.getPk_pkgroup()));
			itemHeadElement.addContent(new Element("pk_org").setText(tBillInfo.getPk_pkorg()));
			itemHeadElement.addContent(new Element("code").setText(tBillInfo.getCode()));
			itemHeadElement.addContent(new Element("name").setText(tBillInfo.getName()));
			itemHeadElement.addContent(new Element("creator").setText(tBillInfo.getCreator()));
			itemHeadElement.addContent(new Element("creationtime").setText(tBillInfo.getCreationtime()));
			itemHeadElement.addContent(new Element("modifier").setText(tBillInfo.getModifier()));
			itemHeadElement.addContent(new Element("modifiedtime").setText(tBillInfo.getModifiedtime()));
			itemHeadElement.addContent(new Element("taxpayertype").setText(tBillInfo.getTaxpayertype()));
			itemHeadElement.addContent(new Element("invtype").setText(tBillInfo.getInvtype()));
			itemHeadElement.addContent(new Element("makefreq").setText(tBillInfo.getMakefreq()));
			itemHeadElement.addContent(new Element("makespace").setText(tBillInfo.getMakespace()));
			itemHeadElement.addContent(new Element("telephone").setText(tBillInfo.getTelephone()));
			itemHeadElement.addContent(new Element("mobile").setText(tBillInfo.getMobile()));
			itemHeadElement.addContent(new Element("sendway").setText(tBillInfo.getSendway()));
			itemHeadElement.addContent(new Element("addressee").setText(tBillInfo.getAddressee()));
			itemHeadElement.addContent(new Element("postcode").setText(tBillInfo.getPostcode()));
			itemHeadElement.addContent(new Element("mailingaddress").setText(tBillInfo.getMailingaddress()));
			itemHeadElement.addContent(new Element("customertype").setText(tBillInfo.getCustomertype()));
			itemHeadElement.addContent(new Element("country").setText(tBillInfo.getCountry()));
			itemHeadElement.addContent(new Element("legalperson").setText(tBillInfo.getLegalperson()));
			itemHeadElement.addContent(new Element("taxnumber").setText(tBillInfo.getTaxnumber()));
			itemHeadElement.addContent(new Element("bank").setText(tBillInfo.getBank()));
			itemHeadElement.addContent(new Element("bankaccount").setText(tBillInfo.getBankaccount()));
			itemHeadElement.addContent(new Element("makeinvdate").setText(tBillInfo.getMakeinvdate()));
			itemHeadElement.addContent(new Element("receiveperson").setText(tBillInfo.getReceiveperson()));
			itemHeadElement.addContent(new Element("ispaperyinv").setText(tBillInfo.getIspaperyinv()));
			itemHeadElement.addContent(new Element("enablestate").setText(tBillInfo.getEnablestate()));
			itemHeadElement.addContent(new Element("isearly").setText(tBillInfo.getIsearly()));
			itemHeadElement.addContent(new Element("billmode").setText(tBillInfo.getBillmode()));
			itemHeadElement.addContent(new Element("vaddress").setText(tBillInfo.getVaddress()));
			itemHeadElement.addContent(new Element("openid").setText(tBillInfo.getOpenid()));
			itemHeadElement.addContent(new Element("billaccount").setText(tBillInfo.getBillaccount()));
			itemHeadElement.addContent(new Element("email").setText(tBillInfo.getEmail()));
			itemHeadElement.addContent(new Element("vpsnid").setText(tBillInfo.getVpsnid()));
			itemHeadElement.addContent(new Element("veffectdate").setText(tBillInfo.getVeffectdate()));
			itemHeadElement.addContent(new Element("vautodate").setText(tBillInfo.getVautodate()));
			itemHeadElement.addContent(new Element("vmailprovince").setText(tBillInfo.getVmailprovince()));
			itemHeadElement.addContent(new Element("vmailcity").setText(tBillInfo.getVmailcity()));
			itemHeadElement.addContent(new Element("srcsystem").setText(tBillInfo.getVsrcsystem()));
			itemHeadElement.addContent(new Element("vsrcrowid").setText(tBillInfo.getVsrcrowid()));
			itemHeadElement.addContent(new Element("buymailbox").setText(tBillInfo.getBuymailbox()));
			itemHeadElement.addContent(new Element("buyphoneno").setText(tBillInfo.getBuyphoneno()));
			itemHeadElement.addContent(new Element("buyidtype").setText(tBillInfo.getBuyidtype()));
			itemHeadElement.addContent(new Element("buyidnumber").setText(tBillInfo.getBuyidnumber()));
			billContentElement.addContent(itemHeadElement);
			billHeadElement.addContent(billContentElement);
			billHeadElement.addContent(new Element("pk_group").setText(tBillInfo.getPk_group()));
			billHeadElement.addContent(new Element("pk_org").setText(tBillInfo.getPk_org()));
			billHeadElement.addContent(new Element("pk_org_v").setText(tBillInfo.getPk_org_v()));
			billHeadElement.addContent(new Element("calnum").setText(tBillInfo.getCalnum()));
			billHeadElement.addContent(new Element("caldate").setText(tBillInfo.getCaldate()));
			billHeadElement.addContent(new Element("vdata").setText(tBillInfo.getVdata()));
			billHeadElement.addContent(new Element("rate").setText(tBillInfo.getRate()));
			billHeadElement.addContent(new Element("pk_ptsitem").setText(tBillInfo.getPk_ptsitem()));
			billHeadElement.addContent(new Element("methodcal").setText(String.valueOf(tBillInfo.getMethodcal())));
			billHeadElement.addContent(new Element("drawback").setText(tBillInfo.getDrawback()));
			billHeadElement.addContent(new Element("taxrate").setText(tBillInfo.getTaxrate()));
			billHeadElement.addContent(new Element("oriamt").setText(tBillInfo.getOriamt()));
			billHeadElement.addContent(new Element("oritax").setText(tBillInfo.getOritax()));
			billHeadElement.addContent(new Element("localamt").setText(tBillInfo.getLocalamt()));
			billHeadElement.addContent(new Element("localtax").setText(tBillInfo.getLocaltax()));
			billHeadElement.addContent(new Element("decldate").setText(tBillInfo.getDecldate()));
			billHeadElement.addContent(new Element("busidate").setText(tBillInfo.getBusidate()));
			billHeadElement.addContent(new Element("custcode").setText(tBillInfo.getCustcode()));
			billHeadElement.addContent(new Element("custname").setText(tBillInfo.getCustname()));
			billHeadElement.addContent(new Element("custtype").setText(tBillInfo.getCusttype()));
			billHeadElement.addContent(new Element("dectype").setText(String.valueOf(tBillInfo.getDectype())));
			billHeadElement.addContent(new Element("srcsystem").setText(tBillInfo.getSrcsystem()));
			billHeadElement.addContent(new Element("voucherid").setText(tBillInfo.getVoucherid()));
			billHeadElement.addContent(new Element("transerial").setText(tBillInfo.getTranserial()));
			billHeadElement.addContent(new Element("tranbatch").setText(tBillInfo.getTranbatch()));
			billHeadElement.addContent(new Element("trandate").setText(tBillInfo.getTrandate()));
			billHeadElement.addContent(new Element("trantime").setText(tBillInfo.getTrantime()));
			billHeadElement.addContent(new Element("tranorg").setText(tBillInfo.getTranorg()));
			billHeadElement.addContent(new Element("tranchannel").setText(tBillInfo.getTranchannel()));
			billHeadElement.addContent(new Element("trancounte").setText(tBillInfo.getTrancounte()));
			billHeadElement.addContent(new Element("custmanager").setText(tBillInfo.getCustmanager()));
			billHeadElement.addContent(new Element("trantype").setText(tBillInfo.getTrantype()));
			billHeadElement.addContent(new Element("procode").setText(tBillInfo.getProcode()));
			billHeadElement.addContent(new Element("busino").setText(tBillInfo.getBusino()));
			billHeadElement.addContent(new Element("tranremark").setText(tBillInfo.getTranremark()));
			billHeadElement.addContent(new Element("accountno").setText(tBillInfo.getAccountno()));
			billHeadElement.addContent(new Element("trancurrency").setText(tBillInfo.getTrancurrency()));
			billHeadElement.addContent(new Element("tranamt").setText(tBillInfo.getTranamt()));
			billHeadElement.addContent(new Element("taxtype").setText(String.valueOf(tBillInfo.getTaxtype())));
			billHeadElement.addContent(new Element("cztype").setText(tBillInfo.getCztype()));
			billHeadElement.addContent(new Element("czolddate").setText(tBillInfo.getCzolddate()));
			billHeadElement.addContent(new Element("iscurrent").setText(tBillInfo.getIscurrent()));
			billHeadElement.addContent(new Element("czbusino").setText(tBillInfo.getCzbusino()));
			billHeadElement.addContent(new Element("accno").setText(tBillInfo.getAccno()));
			billHeadElement.addContent(new Element("loanflag").setText(String.valueOf(tBillInfo.getLoanflag())));
			billHeadElement.addContent(new Element("acccode").setText(tBillInfo.getAcccode()));
			billHeadElement.addContent(new Element("paymentflag").setText(String.valueOf(tBillInfo.getPaymentflag())));
			billHeadElement.addContent(new Element("amortno").setText(tBillInfo.getAmortno()));
			billHeadElement.addContent(new Element("midcontno").setText(tBillInfo.getMidcontno()));
			billHeadElement.addContent(new Element("isinnerbank").setText(tBillInfo.getIsinnerbank()));
			billHeadElement.addContent(new Element("overseasflag").setText(String.valueOf(tBillInfo.getOverseasflag())));
			billHeadElement.addContent(new Element("taxcalmethod").setText(String.valueOf(tBillInfo.getTaxcalmethod())));
			billHeadElement.addContent(new Element("creator").setText(tBillInfo.getCreator1()));
			billHeadElement.addContent(new Element("creationtime").setText(tBillInfo.getCreationtime1()));
			billHeadElement.addContent(new Element("modifier").setText(tBillInfo.getModifier1()));
			billHeadElement.addContent(new Element("modifiedtime").setText(tBillInfo.getModifiedtime1()));
			billHeadElement.addContent(new Element("negtype").setText(tBillInfo.getNegtype()));
			billHeadElement.addContent(new Element("tranplace").setText(tBillInfo.getTranplace()));
			billHeadElement.addContent(new Element("deptdoc").setText(tBillInfo.getDeptdoc()));
			billHeadElement.addContent(new Element("confimtype").setText(String.valueOf(tBillInfo.getConfimtype())));
			billHeadElement.addContent(new Element("ispts").setText(tBillInfo.getIspts()));
			billHeadElement.addContent(new Element("isbill").setText(tBillInfo.getIsbill()));
			billHeadElement.addContent(new Element("areatype").setText(String.valueOf(tBillInfo.getAreatype())));
			billHeadElement.addContent(new Element("isptserror").setText(tBillInfo.getIsptserror()));
			billHeadElement.addContent(new Element("receivedate").setText(tBillInfo.getReceivedate()));
			billHeadElement.addContent(new Element("isalloweadvance").setText(tBillInfo.getIsalloweadvance()));
			billHeadElement.addContent(new Element("busipk").setText(tBillInfo.getBusipk()));
			billHeadElement.addContent(new Element("busiitem").setText(tBillInfo.getBusiitem()));
			billHeadElement.addContent(new Element("fininsance").setText(tBillInfo.getFininsance()));
			billHeadElement.addContent(new Element("fininstype").setText(tBillInfo.getFininstype()));
			billHeadElement.addContent(new Element("istaxfree").setText(tBillInfo.getIstaxfree()));
			billHeadElement.addContent(new Element("bunit").setText(tBillInfo.getBunit()));
			billHeadElement.addContent(new Element("bunivalent").setText(tBillInfo.getBunivalent()));
			billHeadElement.addContent(new Element("bnumber").setText(tBillInfo.getBnumber()));
			billHeadElement.addContent(new Element("billtype").setText(tBillInfo.getBilltype()));
			billHeadElement.addContent(new Element("billdate").setText(tBillInfo.getBilldate()));
			billHeadElement.addContent(new Element("servicedoc").setText(tBillInfo.getServicedoc()));
			billHeadElement.addContent(new Element("ishistory").setText(tBillInfo.getIshistory()));
			billHeadElement.addContent(new Element("ismoneny").setText(tBillInfo.getIsmoneny()));
			billHeadElement.addContent(new Element("insureno").setText(tBillInfo.getInsureno()));
			billHeadElement.addContent(new Element("isfirst").setText(tBillInfo.getIsfirst()));
			billHeadElement.addContent(new Element("perspolicyno").setText(tBillInfo.getPerspolicyno()));
			billHeadElement.addContent(new Element("ppveridate").setText(tBillInfo.getPpveridate()));
			billHeadElement.addContent(new Element("billstartdate").setText(tBillInfo.getBillstartdate()));
			billHeadElement.addContent(new Element("billenddate").setText(tBillInfo.getBillenddate()));
			billHeadElement.addContent(new Element("paytype").setText(tBillInfo.getPaytype()));
			billHeadElement.addContent(new Element("ismat").setText(tBillInfo.getIsmat()));
			billHeadElement.addContent(new Element("isentrust").setText(tBillInfo.getIsentrust()));
			billHeadElement.addContent(new Element("billeffectivedate").setText(tBillInfo.getBilleffectivedate()));
			billHeadElement.addContent(new Element("recoincomdate").setText(tBillInfo.getRecoincomdate()));
			billHeadElement.addContent(new Element("billfalg").setText(tBillInfo.getBillfalg()));
			billHeadElement.addContent(new Element("offsetdate").setText(tBillInfo.getOffsetdate()));
			billHeadElement.addContent(new Element("batchno").setText(tBillInfo.getBatchno()));
			billHeadElement.addContent(new Element("issale").setText(tBillInfo.getIssale()));
			billHeadElement.addContent(new Element("vdef1").setText(tBillInfo.getVdef1()));
			billHeadElement.addContent(new Element("vdef2").setText(tBillInfo.getVdef2()));
			billHeadElement.addContent(new Element("vdef6").setText(tBillInfo.getVdef6()));
			billHeadElement.addContent(new Element("vdef7").setText(tBillInfo.getVdef7()));
			billHeadElement.addContent(new Element("vdef8").setText(tBillInfo.getVdef8()));
			billElement.addContent(billHeadElement);
			ufinterface.addContent(billElement);
		}

		return Doc;
	}
	
	private List<OutPaySendResult> parseDoc(Document document){
		Document tXmlDoc = (Document) document.clone();
		//1、对报文头进行解析
		Element rootElement=tXmlDoc.getRootElement();
		OutPayHead head=new OutPayHead();
//		head.setAccount(getValueByAttrName(rootElement,"account"));
		head.setBilltype(getValueByAttrName(rootElement,"billtype"));
		head.setFilename(getValueByAttrName(rootElement,"filename"));
//		head.setGroupcode(getValueByAttrName(rootElement,"groupcode"));
		head.setIsexchange(getValueByAttrName(rootElement,"isexchange"));
		head.setReplace(getValueByAttrName(rootElement,"replace"));
		head.setRoottag(getValueByAttrName(rootElement,"roottag"));
		head.setSender(getValueByAttrName(rootElement,"sender"));
		
		System.out.println("22222--"+head.getAccount());
		//2、解析报文返回结果
		List resEleList = rootElement.getChildren("sendresult");
		
		List<OutPaySendResult> sendResultList =new ArrayList<OutPaySendResult>();
		for(int i=0; i<resEleList.size(); i++ ){
			OutPaySendResult tSendresult=new OutPaySendResult();
//			List<Content> contentList=new ArrayList<Content>();
			Element resultEle=(Element)resEleList.get(i);
			tSendresult.setBillpk(getTextByNodeName(resultEle,"billpk"));
			tSendresult.setBdocid(getTextByNodeName(resultEle,"bdocid"));
			tSendresult.setFilename(getTextByNodeName(resultEle,"filename"));
			tSendresult.setResultcode(getTextByNodeName(resultEle,"resultcode"));
			tSendresult.setResultdescription(getTextByNodeName(resultEle,"resultdescription"));
			tSendresult.setContent(getTextByNodeName(resultEle,"content"));

			sendResultList.add(tSendresult);
		}
		
		return sendResultList;
	}
	
	private String getTextByNodeName(Element element,String childName){
		Element child=element.getChild(childName);
		if(null == child){
			return null;
		}
		return child.getTextTrim();
	}
	
	private String getValueByAttrName(Element element,String attrName){
		Attribute attr= element.getAttribute(attrName);
		if(null == attr){
			return null;
		}else{
			return attr.getValue();
		}
	}
	
	public static void main(String[] args) {
		OutPayUploadInterface tt=new OutPayUploadInterface();
		String mInFilePath = "G:/vtsfile/premSeparate/request/20160422150350000006.xml";
		InputStream mIs;
		try {
			mIs = new FileInputStream(mInFilePath);
			byte[] mInXmlBytes = tt.InputStreamToBytes(mIs);
			String mInXmlStr = new String(mInXmlBytes, "GBK");
			
			StringReader tInXmlReader = new StringReader(mInXmlStr);
			SAXBuilder tSAXBuilder = new SAXBuilder();
			Document responseDoc = tSAXBuilder.build(tInXmlReader);
			
			tt.parseDoc(responseDoc);
		} catch (FileNotFoundException e) {
			
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (JDOMException e) {
			e.printStackTrace();
		}
		
	}

}
