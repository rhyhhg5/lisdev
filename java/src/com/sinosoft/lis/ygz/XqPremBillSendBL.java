package com.sinosoft.lis.ygz;

import com.sinosoft.lis.db.LYPremSeparateDetailDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LYPremSeparateDetailSchema;
import com.sinosoft.lis.taskservice.PremSeparateTask;
import com.sinosoft.lis.vschema.LYPremSeparateDetailSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class XqPremBillSendBL {

	/**
	 * @param args
	 */
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	/** 数据操作字符串 */
	private String mOperate;
	/** 开始日期*/
	private String mStartDate;
	/** 结束日期*/
	private String mEndDate; 
	
	private String mWherePart;
	
	private GlobalInput mGI = new GlobalInput();
	
	//业务处理相关变量
	private MMap mMap = new MMap();
	
	private String mCurrentDate = PubFun.getCurrentDate();
	private String mCurrentTime = PubFun.getCurrentTime();
	
	
	public boolean submitData(VData cInputData, String cOperate){
		//将操作数据拷贝到本类中
		this.mOperate=cOperate;
		//得到外部传入的数据，拷贝到本类中
		if(!getInputDate(cInputData)){
			return false;
		}
		
		//业务处理
		if(!dealDate()){
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GetTempFeeBqSeparateBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败GetTempFeeBqSeparateBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}
		
		// 准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}
		
		return true;
	}
	public boolean getInputDate(VData cInputData){
		mInputData=cInputData;
		TransferData tTransferData=(TransferData)cInputData.getObjectByObjectName("TransferData", 0);
		LYPremSeparateDetailSet tLYPremSeparateDetailSet=(LYPremSeparateDetailSet) cInputData
		.getObjectByObjectName("LYPremSeparateDetailSet", 0);
		if(null != tLYPremSeparateDetailSet){
			String lisStr="";
			for(int i=1; i<=tLYPremSeparateDetailSet.size();i++){
				lisStr += "'"+ tLYPremSeparateDetailSet.get(i).getTempFeeNo()+"'";
				if(i<tLYPremSeparateDetailSet.size()){
					lisStr += ",";
				}
			}
			mWherePart = " and tempfeeno in ("+lisStr+") ";
		}
		return true;
	}
	
	public boolean dealDate(){

		try {
			/**
			 * 续期暂收数据提取
			 */
			GetXqTempFeeBL tGetXqTempFeeBL = new GetXqTempFeeBL();
			if(!tGetXqTempFeeBL.submitData(mInputData, "")){
				this.mErrors.copyAllErrors(tGetXqTempFeeBL.mErrors);
				System.out.println("续期从暂收表生成价税分离数据报错："+mErrors.getFirstError());
				return false;
			}
		} catch (Exception e) {
			System.out.println("续期从暂收表生成价税分离数据报错：");
			e.printStackTrace();
		}
		
		//调用价税分离接口
		try {
			System.out.println("=====开始价税分离=====");
			if(!premSwparate()){
				System.out.println("价税分离发生报错："+mErrors.getFirstError());
				return false;
			}
		} catch (Exception e) {
			System.out.println("价税分离发生报错！");
			e.printStackTrace();
		}

		try {
			/**
			 * 续期回写实收号码
			 */
			GetBqMoneyNoBL tGetBqMoneyNoBL = new GetBqMoneyNoBL(); 
			if(!tGetBqMoneyNoBL.submitData(mInputData, "")){
				this.mErrors.copyAllErrors(tGetBqMoneyNoBL.mErrors);
				System.out.println("续期、保全回写实收号报错："+mErrors.getFirstError());
				return false;
			}
		} catch (Exception e) {
			System.out.println("续期、保全回写实收号报错");
			e.printStackTrace();
		}

		try {
			/**
			 * 续期发票数据提取
			 */
			GetXqPremBillBL tGetXqPremBillBL = new GetXqPremBillBL(); 
			if(!tGetXqPremBillBL.submitData(mInputData, "")){
				this.mErrors.copyAllErrors(tGetXqPremBillBL.mErrors);
				System.out.println("续期发票数据提取报错："+mErrors.getFirstError());
				return false;
			}
		} catch (Exception e) {
			System.out.println("续期发票数据提取报错：");
			e.printStackTrace();
		}
		return true;
	}
	
	public boolean premSwparate(){
		//提取待分离的数据
		String sqlStr = "select * from LYPremSeparateDetail "
				+ " where state='01' " 
				+ mWherePart
				+ " with ur ";
		System.out.println(sqlStr);
			
		LYPremSeparateDetailSet tLYPremSeparateDetailSet=new LYPremSeparateDetailSet();
		LYPremSeparateDetailDB tLYPremSeparateDetailDB=new LYPremSeparateDetailDB();
		tLYPremSeparateDetailSet=tLYPremSeparateDetailDB.executeQuery(sqlStr);

		if(tLYPremSeparateDetailSet.size()>0){
			
		VData tVData = new VData();
		tVData.add(mGI);
		tVData.add(tLYPremSeparateDetailSet);
		PremSeparateBL tPremSeparateBL = new PremSeparateBL();
		if (!tPremSeparateBL.getSubmit(tVData, "")) {
			this.mErrors = tPremSeparateBL.mErrors;
			System.out.println("价税分离发生错误:"+ mErrors.getErrContent());
			tLYPremSeparateDetailSet.clear();
		} else {
			tLYPremSeparateDetailSet.clear();

			MMap tMMap = tPremSeparateBL.getResult();
			VData vdata = new VData();
			vdata.add(tMMap);
			PubSubmit tPubSubmit = new PubSubmit();
			if (!tPubSubmit.submitData(vdata, "")) {
				CError cError = new CError();
				cError.moduleName = "InsuAccBala";
				cError.functionName = "run";
				cError.errorMessage = "数据提交失败";
				this.mErrors.addOneError(cError);
			}
		}
		}
		return true;
	}
	
	public boolean prepareOutputData(){

		return true;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		XqPremBillSendBL tXqPremBillSendBL = new XqPremBillSendBL();
		VData cInputData = new VData();
		TransferData tTransferData = new TransferData();
		String tStartDate = "2016-05-01";
		String tEndDate = "2016-05-30";
		String HandWorkFlag = "1";
		tTransferData.setNameAndValue("StartDate", tStartDate);
		tTransferData.setNameAndValue("EndDate", tEndDate);
		tTransferData.setNameAndValue("HandWorkFlag", HandWorkFlag);
		cInputData.add(tTransferData);
		tXqPremBillSendBL.submitData(cInputData, "");
	}

}
