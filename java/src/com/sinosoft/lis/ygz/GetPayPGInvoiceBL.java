package com.sinosoft.lis.ygz;

import java.util.HashMap;
import java.util.Iterator;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LYOutPayDetailSchema;
import com.sinosoft.lis.vschema.LYOutPayDetailSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

public class GetPayPGInvoiceBL {

	/** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    private GlobalInput mGlobalInput = null;
    
    private HashMap mHashMap = null;
    
    /**传输到后台处理的map*/
    private MMap mMap = new MMap();
    private VData mVData = new VData();
    
    /** 数据操作字符串 */
    private String mOperate = "";
    
    private String mCurrentDate = PubFun.getCurrentDate();
	private String mCurrentTime = PubFun.getCurrentTime();
    
    public boolean submitData(VData cInputData, String cOperate){
    	
    	if (!getInputData(cInputData, cOperate)){
            return false;
        }

        if (!checkData()){
            return false;
        }

        if (!dealData()){
            return false;
        }
        
        if (!prepareData()){
        	return false;
        }
        
        if(!submit()){
            return false;
        }
        
        return true;
    }
    
    private boolean getInputData(VData cInputData, String cOperate){
    	mOperate = cOperate;
    	
    	mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
    	if(mGlobalInput == null){
    		buildError("getInputData","处理超时，请重新登录。");
    		return false;
    	}
    	
    	mHashMap = (HashMap)cInputData.getObjectByObjectName("HashMap", 0);
    	if(mHashMap == null || mHashMap.size()<=0){
    		buildError("getInputData", "所需参数不完整。");
            return false;
    	}
    	
    	return true;
    }
    
    private boolean checkData(){
    	return true;
    }
    
    private boolean dealData(){
    	
    	LYOutPayDetailSet tLYOutPayDetailSet = new LYOutPayDetailSet();
    	Iterator iter = mHashMap.keySet().iterator();
    	while (iter.hasNext()) {
    		Object key = iter.next();
    		String tPrtNo = (String)mHashMap.get(key);
    		System.out.println("tPrtNo:" + tPrtNo);
    		if(mOperate.equals("1")){//个单
    			tLYOutPayDetailSet.add(dealPolicyG(tPrtNo));
    		}
    		if(mOperate.equals("2")){//团单
    			tLYOutPayDetailSet.add(dealPolicyT(tPrtNo));
    		}
    	}
    	// 调用发票推送数据接口
    	OutPayUploadBL tOutPayUploadBL = new OutPayUploadBL();
    	VData tVData = new VData();
    	tVData.add(tLYOutPayDetailSet);
    	tVData.add(mGlobalInput);
    	if(!tOutPayUploadBL.getSubmit(tVData, "")){
    		buildError("dealData", "调用OutPayUploadBL接口失败。");
    		return false;
    	}
    	MMap tMMap = new MMap();
    	tMMap = tOutPayUploadBL.getResult();
    	//数据封装，准备提交
    	
    	//该行为不调用发票推送接口时，测试使用， 现在删除掉
//    	mMap.put(tLYOutPayDetailSet,SysConst.INSERT);
    	
    	mMap.add(tMMap);
    	return true;
    }
    
	private boolean prepareData(){
    	
    	mVData.add(mMap);
    	
    	return true;
    }
    
    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit(){
    	
    	PubSubmit p = new PubSubmit();
    	if (!p.submitData(mVData, SysConst.INSERT)){
    		System.out.println("提交数据失败");
    		buildError("submitData", "提交数据失败");
    		return false;
    	}

        return true;
    }
    private LYOutPayDetailSet dealPolicyG(String cPrtNo) {
    	String tPrtNo = cPrtNo;
    	String tSQL = " select contno,appntno,appntname,managecom,cvalidate,"
    				+ " case when signdate is null then null when cvalidate>=signdate then cvalidate else signdate end "
    				+ " from lccont where prtno='"+tPrtNo+"' and conttype='1' "
    				+ " union select contno,appntno,appntname,managecom,cvalidate,"
    				+ " case when signdate is null then null when cvalidate>=signdate then cvalidate else signdate end "
    				+ " from lbcont where prtno='"+tPrtNo+"' and conttype='1' ";
    	SSRS tSSRS = new ExeSQL().execSQL(tSQL);
    	String tPolicyNo = tSSRS.GetText(1, 1);
    	String tCustomerNo = tSSRS.GetText(1, 2);
    	String tCustomerName = tSSRS.GetText(1, 3);
    	String tManageCom = tSSRS.GetText(1, 4);
    	String tCvalidate = tSSRS.GetText(1, 5);
    	String tTransDate = tSSRS.GetText(1, 6);
    	String tNoSQL = "select payno from ljapay where incomeno = '"+tPolicyNo+"' ";
		String tMoneyNo = new ExeSQL().getOneValue(tNoSQL);
		LYOutPayDetailSet tLYOutPayDetailSet = new LYOutPayDetailSet();
    	String sql ="select lja.MoneyNoTax,lja.MoneyTax,trim(lja.PolNo)||trim(lja.PayNo)||trim(lja.DutyCode)||trim(lja.PayPlanCode)||trim(lja.PayType),lja.Riskcode,lja.sumactupaymoney,lja.contno,lja.taxrate from ljapayperson lja "
    			+ " where lja.contno = '"+tPolicyNo+"' and lja.paytype = 'ZC'"
    			+ " and lja.moneynotax is not null and lja.moneynotax != '' and lja.grpcontno = '00000000000000000000'"
    			+ " and lja.payno in (select payno from ljapay where incomeno = lja.contno and duefeetype = '0')"
    			+ " and lja.riskcode not in(select riskcode from lmriskapp where RiskType8 = '6')"
    			+ " union"
    			+ " select lci.MoneyNoTax,lci.MoneyTax,trim(lci.SerialNo)||trim(lci.moneytype),lci.Riskcode,lci.fee,lci.contno,lci.taxrate from LCInsureAccFeeTrace lci where lci.contno = '"+tPolicyNo+"'"
				+ " and lci.grpcontno = '00000000000000000000'"
				+ " and lci.moneytype in ('GL','KF')"
				+ " and lci.moneynotax is not null and lci.moneynotax != ''"
 			   	+ " and not exists (select 1 from LYOutPayDetail where busino=trim(lci.SerialNo)||trim(lci.moneytype) and state != '03') "
    			;
    	SSRS gSSRS = new SSRS();
    	ExeSQL exeSQL = new ExeSQL();
    	gSSRS = exeSQL.execSQL(sql);
		for(int i=1 ; i<=gSSRS.getMaxRow(); i++){
			LYOutPayDetailSchema tLYOutPayDetailSchema = new LYOutPayDetailSchema();
			// 是否更新客户信息
			tLYOutPayDetailSchema.setisupdate("Y");
			// 集团  传“00”，,00是集团编码
			tLYOutPayDetailSchema.setpkgroup("00");
			tLYOutPayDetailSchema.setgroup("00");
			// 组织
			tLYOutPayDetailSchema.setpkorg(tManageCom);
			tLYOutPayDetailSchema.setorg(tManageCom);
			tLYOutPayDetailSchema.setorgv(tManageCom);
			//客户号
			tLYOutPayDetailSchema.setcode(tCustomerNo);
			tLYOutPayDetailSchema.setcustomertype("2"); // customertype客户类型不再区分“机构”和“个人”，统一默认为“2”
			//客户名
			tLYOutPayDetailSchema.setname(tCustomerName);
			// 价税分离项目  传“01”用来匹配商品服务档案
			tLYOutPayDetailSchema.setptsitem("01");
			// 数据日期 
			tLYOutPayDetailSchema.setvdata(mCurrentDate);
			// 税率 
			tLYOutPayDetailSchema.settaxrate(gSSRS.GetText(i, 7));
			// 不含税金额（原币） 
			tLYOutPayDetailSchema.setoriamt(gSSRS.GetText(i, 1));
			// 税额（原币） 
			tLYOutPayDetailSchema.setoritax(gSSRS.GetText(i, 2));
			// 不含税金额（本币） 
			tLYOutPayDetailSchema.setlocalamt(gSSRS.GetText(i, 1));
			// 税额（本币） 
			tLYOutPayDetailSchema.setlocaltax(gSSRS.GetText(i, 2));
			// 客户号
			tLYOutPayDetailSchema.setcustcode(tCustomerNo);
			// 客户的名称 
			tLYOutPayDetailSchema.setcustname(tCustomerName);
			// 客户的类型
			tLYOutPayDetailSchema.setcusttype("2");
			// 申报类型  “0”表示正常申报
			tLYOutPayDetailSchema.setdectype("0");
			// 来源的系统  传“01”，代表核心系统
			tLYOutPayDetailSchema.setsrcsystem("01");
			// 金额是否含税  “0”表示含税，“1”表示不含税
			tLYOutPayDetailSchema.settaxtype("0");
			// 收付标志  “0”表示收入，“1”表示支出
			tLYOutPayDetailSchema.setpaymentflag("0");
			// 境内外标示  “0”表示境内，“1”表示境外
			tLYOutPayDetailSchema.setoverseasflag("0");
			// 是否开票   传“Y/N”，必须传
			tLYOutPayDetailSchema.setisbill("Y");
			// 汇总地类型  “0”表示汇总，“1”表示属地，后续纳税
			tLYOutPayDetailSchema.setareatype("1");
			// 凭证id -- 保单号
			tLYOutPayDetailSchema.setvoucherid(tPolicyNo);
			// 交易流水号 
			tLYOutPayDetailSchema.settranserial(gSSRS.GetText(i, 3));
			// 交易日期--确认核销日期:取保单的生效与签单日期孰后日期
			tLYOutPayDetailSchema.settrandate(tTransDate);
			// 产品代码--险种编码前四位
			String productCode = gSSRS.GetText(i, 4).substring(0,4);
			String productName = new ExeSQL().getOneValue("select riskname from lmriskapp where riskcode='"+gSSRS.GetText(i, 4)+"' ");
			tLYOutPayDetailSchema.setprocode(productCode);
			// 交易币种  传“CNY”
			tLYOutPayDetailSchema.settrancurrency("CNY");
			// 备注 -- 保单号+险种号
			tLYOutPayDetailSchema.setvdef1("保单号:"+tPolicyNo+";险种:"+productName);
			// vdef2 -- 保单号
			tLYOutPayDetailSchema.setvdef2(gSSRS.GetText(i, 6));
			// 交易金额 
			tLYOutPayDetailSchema.settranamt(gSSRS.GetText(i, 5));
			// 部门
			tLYOutPayDetailSchema.setdeptdoc(tManageCom);
			// 主键 --流水号
			tLYOutPayDetailSchema.setBusiNo(gSSRS.GetText(i, 3));
			// 业务类型代码  对于契约来说，都是保费收入吧
			tLYOutPayDetailSchema.setbusitype("01");
			// 业务项目编码--价税分离表的流水号
			tLYOutPayDetailSchema.setbusipk(gSSRS.GetText(i, 3));
			// 保单生效日期
			tLYOutPayDetailSchema.setbilleffectivedate(tCvalidate);
			// 投保单号
			tLYOutPayDetailSchema.setinsureno(tPrtNo);
			// 预打发票标识 : 01--预打 02--非预打
			tLYOutPayDetailSchema.setprebilltype("01");
			// 收付类型和收付费号
			if(tMoneyNo!=null || !"".equals(tMoneyNo)){
				// 收付类型 ： 01--收费  02--付费
				tLYOutPayDetailSchema.setmoneytype("01");
				// 收付费号  
				tLYOutPayDetailSchema.setmoneyno(tMoneyNo);
			}
			// makedate,maketime,modifydate,modifytime
			tLYOutPayDetailSchema.setMakeDate(mCurrentDate);
			tLYOutPayDetailSchema.setMakeTime(mCurrentTime);
			tLYOutPayDetailSchema.setModifyDate(mCurrentDate);
			tLYOutPayDetailSchema.setModifyTime(mCurrentTime);
			
			// 邮件说明要新添字段
			tLYOutPayDetailSchema.setinvtype("0"); // 默认为0
			tLYOutPayDetailSchema.setvaddress("0"); // 默认为0
			tLYOutPayDetailSchema.setvsrcsystem("01"); // 默认为01
			tLYOutPayDetailSchema.settaxpayertype("01"); // 默认为01
			tLYOutPayDetailSchema.setenablestate("2"); // 默认为2
			
			tLYOutPayDetailSet.add(tLYOutPayDetailSchema);
		}
    	
    	return tLYOutPayDetailSet;
	}
    
    private LYOutPayDetailSet dealPolicyT(String cPrtNo){
    	String tPrtNo = cPrtNo;
    	String tSQL = 
    				" select grpcontno,appntno,grpname,managecom,cvalidate,"
    				+ " case when signdate is null then null when cvalidate>=signdate then cvalidate else signdate end,"
    				+ " appflag"
    				+ " from lcgrpcont where prtno='"+tPrtNo+"' "
    				+ " union select grpcontno,appntno,grpname,managecom,cvalidate,"
    				+ " case when signdate is null then null when cvalidate>=signdate then cvalidate else signdate end,"
    				+ " appflag"
    				+ " from lbgrpcont where prtno='"+tPrtNo+"' "
    				;
    	SSRS tSSRS = new ExeSQL().execSQL(tSQL);
    	String tPolicyNo = tSSRS.GetText(1, 1);
    	String tCustomerNo = tSSRS.GetText(1, 2);
    	String tCustomerName = tSSRS.GetText(1, 3);
    	String tManageCom = tSSRS.GetText(1, 4);
    	String tCvalidate = tSSRS.GetText(1, 5);
    	String tTransDate = tSSRS.GetText(1, 6);
    	String tAppflag = tSSRS.GetText(1, 7);
    	String tNoSQL = "select payno from ljapay where incomeno = '"+tPolicyNo+"' ";
		String tMoneyNo = new ExeSQL().getOneValue(tNoSQL);
    	LYOutPayDetailSet tLYOutPayDetailSet = new LYOutPayDetailSet();
    	if(tAppflag.equals("1")){
	    	String sql = "select lyp.MoneyNoTax,lyp.MoneyTax,trim(lyp.grppolno)||trim(lyp.payno)||trim(lyp.paytype),lyp.Riskcode,lyp.sumactupaymoney,lyp.grpcontno,lyp.taxrate from ljapaygrp lyp where lyp.grpcontno = '"+tPolicyNo+"'"
	    				+ " and lyp.riskcode not in(select riskcode from lmriskapp where RiskType8 = '6')"
	    				+ " and lyp.moneynotax is not null and lyp.moneynotax != ''"
	    				+ " and lyp.paytype ='ZC' and lyp.payno in (select payno from ljapay where incomeno = lyp.grpcontno and duefeetype = '0') "
	    				+ " and not exists (select 1 from LYOutPayDetail where busino=trim(lyp.grppolno)||trim(lyp.payno)||trim(lyp.paytype) and state != '03') "
	     			    + " and not exists (select 1 from LYOutPayDetail where busino in (select busino from lypremseparatedetail where prtno = '"+tPrtNo+"' and otherstate = '03'))"
	    				+ " union"
	    				+ " select lci.MoneyNoTax,lci.MoneyTax,trim(lci.SerialNo)||trim(lci.moneytype),lci.Riskcode,lci.fee,lci.grpcontno,lci.taxrate from LCInsureAccFeeTrace lci where lci.grpcontno = (select grpcontno from lcgrpcont where appflag = '1' and prtno = '"+tPrtNo+"')"
	    				+ " and lci.grpcontno <> '00000000000000000000'"
	    				+ " and lci.moneytype in ('GL','KF')"
	    				+ " and lci.moneynotax is not null and lci.moneynotax != ''"
	     			   	+ " and not exists (select 1 from LYOutPayDetail where busino=trim(lci.SerialNo)||trim(lci.moneytype) and state != '03') "
	    				;
	    	SSRS tSSRSsrs = new SSRS();
	    	ExeSQL tExeSQL = new ExeSQL();
	    	tSSRSsrs = tExeSQL.execSQL(sql);
			for(int i=1 ; i<=tSSRSsrs.getMaxRow(); i++){
				LYOutPayDetailSchema tLYOutPayDetailSchema = new LYOutPayDetailSchema();
				// 是否更新客户信息
				tLYOutPayDetailSchema.setisupdate("Y");
				// 集团  传“00”，,00是集团编码
				tLYOutPayDetailSchema.setpkgroup("00");
				tLYOutPayDetailSchema.setgroup("00");
				// 组织
				tLYOutPayDetailSchema.setpkorg(tManageCom);
				tLYOutPayDetailSchema.setorg(tManageCom);
				tLYOutPayDetailSchema.setorgv(tManageCom);
				//客户号
				tLYOutPayDetailSchema.setcode(tCustomerNo);
				tLYOutPayDetailSchema.setcustomertype("2"); // customertype客户类型不再区分“机构”和“个人”，统一默认为“2”
				//客户名
				tLYOutPayDetailSchema.setname(tCustomerName);
				// 价税分离项目  传“01”用来匹配商品服务档案
				tLYOutPayDetailSchema.setptsitem("01");
				// 数据日期 
				tLYOutPayDetailSchema.setvdata(mCurrentDate);
				// 税率 
				tLYOutPayDetailSchema.settaxrate(tSSRSsrs.GetText(i, 7));
				// 不含税金额（原币） 
				tLYOutPayDetailSchema.setoriamt(tSSRSsrs.GetText(i, 1));
				// 税额（原币） 
				tLYOutPayDetailSchema.setoritax(tSSRSsrs.GetText(i, 2));
				// 不含税金额（本币） 
				tLYOutPayDetailSchema.setlocalamt(tSSRSsrs.GetText(i, 1));
				// 税额（本币） 
				tLYOutPayDetailSchema.setlocaltax(tSSRSsrs.GetText(i, 2));
				// 客户号
				tLYOutPayDetailSchema.setcustcode(tCustomerNo);
				// 客户的名称 
				tLYOutPayDetailSchema.setcustname(tCustomerName);
				// 客户的类型
				tLYOutPayDetailSchema.setcusttype("2");
				// 申报类型  “0”表示正常申报
				tLYOutPayDetailSchema.setdectype("0");
				// 来源的系统  传“01”，代表核心系统
				tLYOutPayDetailSchema.setsrcsystem("01");
				// 金额是否含税  “0”表示含税，“1”表示不含税
				tLYOutPayDetailSchema.settaxtype("0");
				// 收付标志  “0”表示收入，“1”表示支出
				tLYOutPayDetailSchema.setpaymentflag("0");
				// 境内外标示  “0”表示境内，“1”表示境外
				tLYOutPayDetailSchema.setoverseasflag("0");
				// 是否开票   传“Y/N”，必须传
				tLYOutPayDetailSchema.setisbill("Y");
				// 汇总地类型  “0”表示汇总，“1”表示属地，后续纳税
				tLYOutPayDetailSchema.setareatype("1");
				// 凭证id -- 保单号
				tLYOutPayDetailSchema.setvoucherid(tPolicyNo);
				// 交易流水号 
				tLYOutPayDetailSchema.settranserial(tSSRSsrs.GetText(i, 3));
				// 交易日期--确认核销日期:取保单的生效与签单日期孰后日期
				tLYOutPayDetailSchema.settrandate(tTransDate);
				// 产品代码--险种编码前四位
				String productCode = tSSRSsrs.GetText(i, 4).substring(0,4);
				String productName = new ExeSQL().getOneValue("select riskname from lmriskapp where riskcode='"+tSSRSsrs.GetText(i, 4)+"' ");
				tLYOutPayDetailSchema.setprocode(productCode);
				// 交易币种  传“CNY”
				tLYOutPayDetailSchema.settrancurrency("CNY");
				// 备注 -- 保单号+险种号
				tLYOutPayDetailSchema.setvdef1("保单号:"+tPolicyNo+";险种:"+productName);
				// vdef2 -- 保单号
				tLYOutPayDetailSchema.setvdef2(tSSRSsrs.GetText(i, 6));
				// 交易金额 
				tLYOutPayDetailSchema.settranamt(tSSRSsrs.GetText(i, 5));
				// 部门
				tLYOutPayDetailSchema.setdeptdoc(tManageCom);
				// 主键 --流水号
				tLYOutPayDetailSchema.setBusiNo(tSSRSsrs.GetText(i, 3));
				// 业务类型代码  对于契约来说，都是保费收入吧
				tLYOutPayDetailSchema.setbusitype("01");
				// 业务项目编码--价税分离表的流水号
				tLYOutPayDetailSchema.setbusipk(tSSRSsrs.GetText(i, 3));
				// 保单生效日期
				tLYOutPayDetailSchema.setbilleffectivedate(tCvalidate);
				// 投保单号
				tLYOutPayDetailSchema.setinsureno(tPrtNo);
				// 预打发票标识 : 01--预打 02--非预打
				tLYOutPayDetailSchema.setprebilltype("01");
				// 收付类型和收付费号
				if(tMoneyNo!=null || !"".equals(tMoneyNo)){
					// 收付类型 ： 01--收费  02--付费
					tLYOutPayDetailSchema.setmoneytype("01");
					// 收付费号  
					tLYOutPayDetailSchema.setmoneyno(tMoneyNo);
				}
				// makedate,maketime,modifydate,modifytime
				tLYOutPayDetailSchema.setMakeDate(mCurrentDate);
				tLYOutPayDetailSchema.setMakeTime(mCurrentTime);
				tLYOutPayDetailSchema.setModifyDate(mCurrentDate);
				tLYOutPayDetailSchema.setModifyTime(mCurrentTime);
				
				// 邮件说明要新添字段
				tLYOutPayDetailSchema.setinvtype("0"); // 默认为0
				tLYOutPayDetailSchema.setvaddress("0"); // 默认为0
				tLYOutPayDetailSchema.setvsrcsystem("01"); // 默认为01
				tLYOutPayDetailSchema.settaxpayertype("01"); // 默认为01
				tLYOutPayDetailSchema.setenablestate("2"); // 默认为2
				
				tLYOutPayDetailSet.add(tLYOutPayDetailSchema);
			}
		}else{
			String sql = "select lyp.MoneyNoTax,lyp.MoneyTax,lyp.BusiNo,lyp.Riskcode,lyp.PayMoney,lyp.policyno,lyp.taxrate "
					+ " from lypremseparatedetail lyp where lyp.prtno = '"+tPrtNo+"' and lyp.otherstate = '03'  "
    				;
    	SSRS tSSRSsrs = new SSRS();
    	ExeSQL tExeSQL = new ExeSQL();
    	tSSRSsrs = tExeSQL.execSQL(sql);
		for(int i=1 ; i<=tSSRSsrs.getMaxRow(); i++){
			LYOutPayDetailSchema tLYOutPayDetailSchema = new LYOutPayDetailSchema();
			// 是否更新客户信息
			tLYOutPayDetailSchema.setisupdate("Y");
			// 集团  传“00”，,00是集团编码
			tLYOutPayDetailSchema.setpkgroup("00");
			tLYOutPayDetailSchema.setgroup("00");
			// 组织
			tLYOutPayDetailSchema.setpkorg(tManageCom);
			tLYOutPayDetailSchema.setorg(tManageCom);
			tLYOutPayDetailSchema.setorgv(tManageCom);
			//客户号
			tLYOutPayDetailSchema.setcode(tCustomerNo);
			tLYOutPayDetailSchema.setcustomertype("2"); // customertype客户类型不再区分“机构”和“个人”，统一默认为“2”
			//客户名
			tLYOutPayDetailSchema.setname(tCustomerName);
			// 价税分离项目  传“01”用来匹配商品服务档案
			tLYOutPayDetailSchema.setptsitem("01");
			// 数据日期 
			tLYOutPayDetailSchema.setvdata(mCurrentDate);
			// 税率 
			tLYOutPayDetailSchema.settaxrate(tSSRSsrs.GetText(i, 7));
			// 不含税金额（原币） 
			tLYOutPayDetailSchema.setoriamt(tSSRSsrs.GetText(i, 1));
			// 税额（原币） 
			tLYOutPayDetailSchema.setoritax(tSSRSsrs.GetText(i, 2));
			// 不含税金额（本币） 
			tLYOutPayDetailSchema.setlocalamt(tSSRSsrs.GetText(i, 1));
			// 税额（本币） 
			tLYOutPayDetailSchema.setlocaltax(tSSRSsrs.GetText(i, 2));
			// 客户号
			tLYOutPayDetailSchema.setcustcode(tCustomerNo);
			// 客户的名称 
			tLYOutPayDetailSchema.setcustname(tCustomerName);
			// 客户的类型
			tLYOutPayDetailSchema.setcusttype("2");
			// 申报类型  “0”表示正常申报
			tLYOutPayDetailSchema.setdectype("0");
			// 来源的系统  传“01”，代表核心系统
			tLYOutPayDetailSchema.setsrcsystem("01");
			// 金额是否含税  “0”表示含税，“1”表示不含税
			tLYOutPayDetailSchema.settaxtype("0");
			// 收付标志  “0”表示收入，“1”表示支出
			tLYOutPayDetailSchema.setpaymentflag("0");
			// 境内外标示  “0”表示境内，“1”表示境外
			tLYOutPayDetailSchema.setoverseasflag("0");
			// 是否开票   传“Y/N”，必须传
			tLYOutPayDetailSchema.setisbill("Y");
			// 汇总地类型  “0”表示汇总，“1”表示属地，后续纳税
			tLYOutPayDetailSchema.setareatype("1");
			// 凭证id -- 保单号
			tLYOutPayDetailSchema.setvoucherid(tPolicyNo);
			// 交易流水号 
			tLYOutPayDetailSchema.settranserial(tSSRSsrs.GetText(i, 3));
			// 交易日期--确认核销日期:取保单的生效与签单日期孰后日期
			tLYOutPayDetailSchema.settrandate(tTransDate);
			// 产品代码--险种编码前四位
			String productCode = tSSRSsrs.GetText(i, 4).substring(0,4);
			String productName = new ExeSQL().getOneValue("select riskname from lmriskapp where riskcode='"+tSSRSsrs.GetText(i, 4)+"' ");
			tLYOutPayDetailSchema.setprocode(productCode);
			// 交易币种  传“CNY”
			tLYOutPayDetailSchema.settrancurrency("CNY");
			// 备注 -- 保单号+险种号
			tLYOutPayDetailSchema.setvdef1("保单号:"+tPolicyNo+";险种:"+productName);
			// vdef2 -- 保单号
			tLYOutPayDetailSchema.setvdef2(tSSRSsrs.GetText(i, 6));
			// 交易金额 
			tLYOutPayDetailSchema.settranamt(tSSRSsrs.GetText(i, 5));
			// 部门
			tLYOutPayDetailSchema.setdeptdoc(tManageCom);
			// 主键 --流水号
			tLYOutPayDetailSchema.setBusiNo(tSSRSsrs.GetText(i, 3));
			// 业务类型代码  对于契约来说，都是保费收入吧
			tLYOutPayDetailSchema.setbusitype("01");
			// 业务项目编码--价税分离表的流水号
			tLYOutPayDetailSchema.setbusipk(tSSRSsrs.GetText(i, 3));
			// 保单生效日期
			tLYOutPayDetailSchema.setbilleffectivedate(tCvalidate);
			// 投保单号
			tLYOutPayDetailSchema.setinsureno(tPrtNo);
			// 预打发票标识 : 01--预打 02--非预打
			tLYOutPayDetailSchema.setprebilltype("01");
			// 收付类型和收付费号
			if(tMoneyNo!=null || !"".equals(tMoneyNo)){
				// 收付类型 ： 01--收费  02--付费
				tLYOutPayDetailSchema.setmoneytype("01");
				// 收付费号  
				tLYOutPayDetailSchema.setmoneyno(tMoneyNo);
			}
			// makedate,maketime,modifydate,modifytime
			tLYOutPayDetailSchema.setMakeDate(mCurrentDate);
			tLYOutPayDetailSchema.setMakeTime(mCurrentTime);
			tLYOutPayDetailSchema.setModifyDate(mCurrentDate);
			tLYOutPayDetailSchema.setModifyTime(mCurrentTime);
			
			// 邮件说明要新添字段
			tLYOutPayDetailSchema.setinvtype("0"); // 默认为0
			tLYOutPayDetailSchema.setvaddress("0"); // 默认为0
			tLYOutPayDetailSchema.setvsrcsystem("01"); // 默认为01
			tLYOutPayDetailSchema.settaxpayertype("01"); // 默认为01
			tLYOutPayDetailSchema.setenablestate("2"); // 默认为2
			
			tLYOutPayDetailSet.add(tLYOutPayDetailSchema);
		}
		}
    	
    	return tLYOutPayDetailSet;
    }
    
    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "GetInvoiceBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    
    public static void main(String[] args) {
		GetPayPGInvoiceBL tGetInvoiceBL = new GetPayPGInvoiceBL();
		tGetInvoiceBL.dealPolicyG("19000147330");
	}
}
