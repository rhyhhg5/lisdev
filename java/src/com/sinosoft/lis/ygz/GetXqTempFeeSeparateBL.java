package com.sinosoft.lis.ygz;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LYPremSeparateDetailSchema;
import com.sinosoft.lis.vschema.LYPremSeparateDetailSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 
 * @author abc
 *
 */
public class GetXqTempFeeSeparateBL {
	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	/** 数据操作字符串 */
	private String mOperate;
	/** 开始日期*/
	private String mStartDate;
	/** 结束日期*/
	private String mEndDate; 
	//页面手工提取标志
	private String mHandFlag;
	
	private String mWherePart;
	
	private String mWhereSQL;
	//业务处理相关变量
	private MMap mMap = new MMap();
	
	private String mCurrentDate = PubFun.getCurrentDate();
	private String mCurrentTime = PubFun.getCurrentTime();
	private int sysCount = 5000;//默认每五千条数据发送一次请求
	
	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		// 进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GetTempFeeBqSeparateBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败GetTempFeeBqSeparateBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}

		// 准备往后台的数据
//		if (!prepareOutputData()) {
//			return false;
//		}
//
//		// 保存数据
//		PubSubmit tPubSubmit = new PubSubmit();
//
//		if (!tPubSubmit.submitData(mInputData, mOperate)) {
//			System.out.print("保存数据返回");
//			// @@错误处理
//			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
//			CError tError = new CError();
//			tError.moduleName = "LCProjectCancelBL";
//			tError.functionName = "submitData";
//			tError.errorMessage = "数据提交失败!";
//			this.mErrors.addOneError(tError);
//			return false;
//		}
//		mInputData = null;
		return true;
	}

	// 获得输出数据
	private boolean getInputData(VData cInputData) {
		TransferData tTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
		mStartDate = (String) tTransferData.getValueByName("StartDate");
		mEndDate = (String) tTransferData.getValueByName("EndDate");
		
		mHandFlag = (String) tTransferData.getValueByName("HandWorkFlag");
		mWherePart = "";
		mWhereSQL = "";
		if(null != mHandFlag && !"".equals(mHandFlag)){
			LYPremSeparateDetailSet tLYPremSeparateDetailSet=(LYPremSeparateDetailSet) cInputData
			.getObjectByObjectName("LYPremSeparateDetailSet", 0);
			if(null != tLYPremSeparateDetailSet){
				String lisStr="";
				for(int i=1; i<=tLYPremSeparateDetailSet.size();i++){
					lisStr += "'"+ tLYPremSeparateDetailSet.get(i).getTempFeeNo()+"'";
					if(i<tLYPremSeparateDetailSet.size()){
						lisStr += ",";
					}
				}
				mWherePart = " and ljt.tempfeeno in ("+lisStr+") ";
				mWhereSQL = " and ljs.getnoticeno in ("+lisStr+")";
			}
		}
		return true;
	}
	
//  业务处理
	private boolean dealData() {
		YGZ tYGZ=new YGZ();
		LYPremSeparateDetailSet tLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
		String tWhereSQL = "";
		String tWherePart = "";
		String tDealDate = "2016-05-01";
		if(mStartDate != null && !"".equals(mStartDate)){
			tWhereSQL += " and confmakedate >= '" + mStartDate + "' ";
			tWherePart += " and lja.confdate >= '" + mStartDate + "' ";
		}
		if(mEndDate != null && !"".equals(mEndDate)){
			tWhereSQL += " and confmakedate <= '" + mEndDate + "' ";
			tWherePart += " and lja.confdate <= '" + mEndDate + "' ";
		}
		if("".equals(tWhereSQL)){
			tWhereSQL = " and (confmakedate >= current date - 7 days "//modify 20160615 前期放开不会对效率影响太大
					  + " or (confmakedate < '"+tDealDate+"' and  exists (select 1 from ljapay where getnoticeno=ljt.tempfeeno and confdate>=current date - 7 days and sumactupaymoney>=0))) ";
			tWherePart += " and lja.confdate >= current date - 7 days ";
		}
		if(null != mHandFlag && !"".equals(mHandFlag)){
			tWhereSQL ="";
			tWherePart ="";
		}
		//暂收表到险种的续期数据
		String tSQL = " select "
                    + " trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||trim(ljt.RiskCode),"
                    + " ljt.TempFeeNo,ljt.TempFeeType,ljt.OtherNo,ljt.OtherNoType,"
					+ " (select prtno from lccont where conttype='1' and contno = ljt.otherno "
					+ " 	union select prtno from lcgrpcont where grpcontno = ljt.otherno"
					+ " 	union select prtno from lbcont where conttype='1' and contno = ljt.otherno "
					+ "		union select prtno from lbgrpcont where grpcontno = ljt.otherno) PrtNo, "
					+ " ljt.otherno PolicyNo,"
					+ " ljt.riskcode,"
					+ " (select managecom from lccont where conttype='1' and contno = ljt.otherno "
					+ " 	union select managecom from lcgrpcont where grpcontno = ljt.otherno"
					+ " 	union select managecom from lbcont where conttype='1' and contno = ljt.otherno "
					+ "		union select managecom from lbgrpcont where grpcontno = ljt.otherno) managecom, "
					+ " ljt.paymoney "
					+ " from ljtempfee ljt "
					+ " where ljt.riskcode != '000000' "
					+ " and tempfeetype ='2' "
					+ " and not exists (select busino from LYPremSeparateDetail where busino=(trim(ljt.TempFeeNo) || trim(ljt.TempFeeType) ||trim(ljt.RiskCode))) "
					+ " and not exists (select 1 from lcgrpcont where grpcontno = ljt.otherno and coinsuranceflag ='1' "
					+ " union select 1 from lbgrpcont where grpcontno = ljt.otherno and coinsuranceflag ='1' ) "
//					+ " and enteraccdate >= '"+tDealDate+"' "
					+ tWhereSQL
					+ mWherePart
					+ " and exists (select 1 from ljspayb where getnoticeno=ljt.tempfeeno) "
					+ " with ur";
		SSRS tNoZeroSSRS = new ExeSQL().execSQL(tSQL);
		if(tNoZeroSSRS != null && tNoZeroSSRS.getMaxRow() > 0){
			for (int i = 1; i <= tNoZeroSSRS.getMaxRow(); i++) {
				LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
				tLYPremSeparateDetailSchema.setBusiNo(tNoZeroSSRS.GetText(i, 1));
				tLYPremSeparateDetailSchema.setTempFeeNo(tNoZeroSSRS.GetText(i, 2));
				tLYPremSeparateDetailSchema.setTempFeeType(tNoZeroSSRS.GetText(i, 3));
				tLYPremSeparateDetailSchema.setOtherNo(tNoZeroSSRS.GetText(i, 4));
				tLYPremSeparateDetailSchema.setOtherNoType(tNoZeroSSRS.GetText(i, 5));
				tLYPremSeparateDetailSchema.setPrtNo(tNoZeroSSRS.GetText(i, 6));
				tLYPremSeparateDetailSchema.setPolicyNo(tNoZeroSSRS.GetText(i, 7));
				tLYPremSeparateDetailSchema.setRiskCode(tNoZeroSSRS.GetText(i, 8));
				tLYPremSeparateDetailSchema.setBusiType(tYGZ.getBusinType(null, "BF", tLYPremSeparateDetailSchema.getRiskCode()));
				tLYPremSeparateDetailSchema.setManageCom(tNoZeroSSRS.GetText(i, 9));
				tLYPremSeparateDetailSchema.setPayMoney(tNoZeroSSRS.GetText(i, 10));
				tLYPremSeparateDetailSchema.setState("01");
				tLYPremSeparateDetailSchema.setOtherState("01");
				tLYPremSeparateDetailSchema.setMoneyType("01");
				tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
				tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
				tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
			}
		}
		System.out.println("暂收表到险种的续期数据:"+tNoZeroSSRS.getMaxRow());
		
		//暂收表到险种的续期数据（共保）
		 tSQL = " select "
                    + " trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||trim(ljt.RiskCode),"
                    + " ljt.TempFeeNo,ljt.TempFeeType,ljt.OtherNo,ljt.OtherNoType,"
					+ " (select prtno from lccont where conttype='1' and contno = ljt.otherno "
					+ " 	union select prtno from lcgrpcont where grpcontno = ljt.otherno"
					+ " 	union select prtno from lbcont where conttype='1' and contno = ljt.otherno "
					+ "		union select prtno from lbgrpcont where grpcontno = ljt.otherno) PrtNo, "
					+ " ljt.otherno PolicyNo,"
					+ " ljt.riskcode,"
					+ " (select managecom from lccont where conttype='1' and contno = ljt.otherno "
					+ " 	union select managecom from lcgrpcont where grpcontno = ljt.otherno"
					+ " 	union select managecom from lbcont where conttype='1' and contno = ljt.otherno "
					+ "		union select managecom from lbgrpcont where grpcontno = ljt.otherno) managecom, "
					+ " ljt.paymoney "
					+ " from ljtempfee ljt "
					+ " where ljt.riskcode != '000000' "
					+ " and tempfeetype ='2' "
					+ " and not exists (select busino from LYPremSeparateDetail where busino=(trim(ljt.TempFeeNo) || trim(ljt.TempFeeType) ||trim(ljt.RiskCode))) "
					+ " and  exists (select 1 from lcgrpcont where grpcontno = ljt.otherno and coinsuranceflag ='1' "
					+ " union select 1 from lbgrpcont where grpcontno = ljt.otherno and coinsuranceflag ='1' ) "
//					+ " and enteraccdate >= '"+tDealDate+"' "
					+ tWhereSQL
					+ mWherePart
					+ " and exists (select 1 from ljspayb where getnoticeno=ljt.tempfeeno) "
					+ " with ur";
		SSRS tNoSSRS6 = new ExeSQL().execSQL(tSQL);
		if(tNoSSRS6 != null && tNoSSRS6.getMaxRow() > 0){
			for (int i = 1; i <= tNoSSRS6.getMaxRow(); i++) {
				LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
				tLYPremSeparateDetailSchema.setBusiNo(tNoSSRS6.GetText(i, 1));
				tLYPremSeparateDetailSchema.setTempFeeNo(tNoSSRS6.GetText(i, 2));
				tLYPremSeparateDetailSchema.setTempFeeType(tNoSSRS6.GetText(i, 3));
				tLYPremSeparateDetailSchema.setOtherNo(tNoSSRS6.GetText(i, 4));
				tLYPremSeparateDetailSchema.setOtherNoType(tNoSSRS6.GetText(i, 5));
				tLYPremSeparateDetailSchema.setPrtNo(tNoSSRS6.GetText(i, 6));
				tLYPremSeparateDetailSchema.setPolicyNo(tNoSSRS6.GetText(i, 7));
				tLYPremSeparateDetailSchema.setRiskCode(tNoSSRS6.GetText(i, 8));
				tLYPremSeparateDetailSchema.setBusiType(tYGZ.getBusinType(null, "BF", tLYPremSeparateDetailSchema.getRiskCode()));
				tLYPremSeparateDetailSchema.setManageCom(tNoSSRS6.GetText(i, 9));
				tLYPremSeparateDetailSchema.setPayMoney(tNoSSRS6.GetText(i, 10));
				tLYPremSeparateDetailSchema.setState("01");
				tLYPremSeparateDetailSchema.setOtherState("09");
				tLYPremSeparateDetailSchema.setMoneyType("01");
				tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
				tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
				tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
			}
		}
		System.out.println("暂收表到险种的共保续期数据:"+tNoSSRS6.getMaxRow());
		//暂收不到险种的续期数据（个单）
		tSQL =" select trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||trim(ljs.RiskCode) BusiNo, "
			+ " ljt.TempFeeNo TempFeeNo,ljt.TempFeeType TempFeeType,ljt.OtherNo OtherNo,ljt.OtherNoType OtherNoType, "
			+ " (select prtno from lccont where contno = ljt.otherno union select prtno from lbcont where contno = ljt.otherno) PrtNo,  "
			+ " ljt.otherno PolicyNo, "
			+ " ljs.riskcode,ljs.managecom,sum(ljs.sumactupaymoney) "
			+ " from ljtempfee ljt ,ljspaypersonb ljs "
			+ " where 1=1 "
			+ " and ljt.otherno = ljs.contno "
			+ " and ljt.tempfeeno = ljs.getnoticeno "
			+ " and ljs.paytype = 'ZC' "
			+ " and ljt.riskcode = '000000'  "
			+ " and tempfeetype ='2'  "
			+ " and not exists (select busino from LYPremSeparateDetail where busino=(trim(ljt.TempFeeNo) || trim(ljt.TempFeeType) ||trim(ljs.RiskCode))) "
			+ " and exists (select 1 from lccont where conttype='1' and appflag='1' and contno=ljs.contno "
			+ " 		union select 1 from lbcont where conttype='1' and contno=ljs.contno ) "
//			+ " and ljt.enteraccdate >= '"+tDealDate+"' "
			+ tWhereSQL
			+ mWherePart
			+ " group by ljt.TempFeeNo,ljt.TempFeeType,ljt.TempFeeNo,ljt.OtherNo,ljt.OtherNoType,ljs.riskcode,ljs.managecom "
			+ " with ur";
		SSRS tSSRSNo2 = new ExeSQL().execSQL(tSQL);
		if(tSSRSNo2 != null && tSSRSNo2.getMaxRow() > 0){
			for (int i = 1; i <= tSSRSNo2.getMaxRow(); i++) {
				LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
				tLYPremSeparateDetailSchema.setBusiNo(tSSRSNo2.GetText(i, 1));
				tLYPremSeparateDetailSchema.setTempFeeNo(tSSRSNo2.GetText(i, 2));
				tLYPremSeparateDetailSchema.setTempFeeType(tSSRSNo2.GetText(i, 3));
				tLYPremSeparateDetailSchema.setOtherNo(tSSRSNo2.GetText(i, 4));
				tLYPremSeparateDetailSchema.setOtherNoType(tSSRSNo2.GetText(i, 5));
				tLYPremSeparateDetailSchema.setPrtNo(tSSRSNo2.GetText(i, 6));
				tLYPremSeparateDetailSchema.setPolicyNo(tSSRSNo2.GetText(i, 7));
				tLYPremSeparateDetailSchema.setRiskCode(tSSRSNo2.GetText(i, 8));
				tLYPremSeparateDetailSchema.setBusiType(tYGZ.getBusinType(null, "BF", tLYPremSeparateDetailSchema.getRiskCode()));
				tLYPremSeparateDetailSchema.setManageCom(tSSRSNo2.GetText(i, 9));
				tLYPremSeparateDetailSchema.setPayMoney(tSSRSNo2.GetText(i, 10));
				tLYPremSeparateDetailSchema.setState("01");
				tLYPremSeparateDetailSchema.setOtherState("01");
				tLYPremSeparateDetailSchema.setMoneyType("01");
				tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
				tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
				tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
			}
		}
		System.out.println("暂收不到险种的续期数据:"+tSSRSNo2.getMaxRow());
		
		//暂收不到险种的续期数据（团单）
		tSQL =" select trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||trim(ljs.RiskCode) BusiNo, "
			+ " ljt.TempFeeNo TempFeeNo,ljt.TempFeeType TempFeeType,ljt.OtherNo OtherNo,ljt.OtherNoType OtherNoType, "
			+ " (select prtno from lcgrpcont where grpcontno = ljt.otherno union select prtno from lbgrpcont where grpcontno = ljt.otherno) PrtNo,  "
			+ " ljt.otherno PolicyNo, "
			+ " ljs.riskcode,ljs.managecom,sum(ljs.sumactupaymoney) "
			+ " from ljtempfee ljt ,ljspaygrpb ljs "
			+ " where 1=1  "
			+ " and ljt.otherno = ljs.grpcontno "
			+ " and ljt.tempfeeno = ljs.getnoticeno "
			+ " and ljs.paytype = 'ZC' "
			+ " and ljt.riskcode = '000000'  "
			+ " and tempfeetype ='2'  "
			+ " and not exists (select busino from LYPremSeparateDetail where busino=(trim(ljt.TempFeeNo) || trim(ljt.TempFeeType) ||trim(ljs.RiskCode))) "
			+ " and not exists (select 1 from lcgrpcont where  appflag='1' and grpcontno=ljs.grpcontno  and CoInsuranceFlag='1'"//共保
			+ " union select 1 from lbgrpcont where grpcontno=ljs.grpcontno  and CoInsuranceFlag='1') "
//			+ " and ljt.enteraccdate >= '"+tDealDate+"' "
			+ tWhereSQL
			+ mWherePart
			+ " group by ljt.TempFeeNo,ljt.TempFeeType,ljt.TempFeeNo,ljt.OtherNo,ljt.OtherNoType,ljs.riskcode,ljs.managecom "
			+ " ";
		SSRS tSSRSNo3 = new ExeSQL().execSQL(tSQL);
		if(tSSRSNo3 != null && tSSRSNo3.getMaxRow() > 0){
			for (int i = 1; i <= tSSRSNo3.getMaxRow(); i++) {
				LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
				tLYPremSeparateDetailSchema.setBusiNo(tSSRSNo3.GetText(i, 1));
				tLYPremSeparateDetailSchema.setTempFeeNo(tSSRSNo3.GetText(i, 2));
				tLYPremSeparateDetailSchema.setTempFeeType(tSSRSNo3.GetText(i, 3));
				tLYPremSeparateDetailSchema.setOtherNo(tSSRSNo3.GetText(i, 4));
				tLYPremSeparateDetailSchema.setOtherNoType(tSSRSNo3.GetText(i, 5));
				tLYPremSeparateDetailSchema.setPrtNo(tSSRSNo3.GetText(i, 6));
				tLYPremSeparateDetailSchema.setPolicyNo(tSSRSNo3.GetText(i, 7));
				tLYPremSeparateDetailSchema.setRiskCode(tSSRSNo3.GetText(i, 8));
				tLYPremSeparateDetailSchema.setBusiType("01");
				tLYPremSeparateDetailSchema.setManageCom(tSSRSNo3.GetText(i, 9));
				tLYPremSeparateDetailSchema.setPayMoney(tSSRSNo3.GetText(i, 10));
				tLYPremSeparateDetailSchema.setState("01");
				tLYPremSeparateDetailSchema.setOtherState("01");
				tLYPremSeparateDetailSchema.setMoneyType("01");
				tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
				tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
				tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
			}
		}
		System.out.println("暂收不到险种的续期数据:"+tSSRSNo3.getMaxRow());
		
		//暂收不到险种的续期数据（共保团单）
		tSQL =" select trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||trim(ljs.RiskCode) BusiNo, "
			+ " ljt.TempFeeNo TempFeeNo,ljt.TempFeeType TempFeeType,ljt.OtherNo OtherNo,ljt.OtherNoType OtherNoType, "
			+ " (select prtno from lcgrpcont where grpcontno = ljt.otherno union select prtno from lbgrpcont where grpcontno = ljt.otherno) PrtNo,  "
			+ " ljt.otherno PolicyNo, "
			+ " ljs.riskcode,ljs.managecom,sum(ljs.sumactupaymoney) "
			+ " from ljtempfee ljt ,ljspaygrpb ljs "
			+ " where 1=1  "
			+ " and ljt.otherno = ljs.grpcontno "
			+ " and ljt.tempfeeno = ljs.getnoticeno "
			+ " and ljs.paytype = 'ZC' "
			+ " and ljt.riskcode = '000000'  "
			+ " and tempfeetype ='2'  "
			+ " and not exists (select busino from LYPremSeparateDetail where busino=(trim(ljt.TempFeeNo) || trim(ljt.TempFeeType) ||trim(ljs.RiskCode))) "
			+ " and exists (select 1 from lcgrpcont where  appflag='1' and grpcontno=ljs.grpcontno  and CoInsuranceFlag='1'"//共保
			+ " union select 1 from lbgrpcont where grpcontno=ljs.grpcontno  and CoInsuranceFlag='1') "
//			+ " and ljt.enteraccdate >= '"+tDealDate+"' "
			+ tWhereSQL
			+ mWherePart
			+ " group by ljt.TempFeeNo,ljt.TempFeeType,ljt.TempFeeNo,ljt.OtherNo,ljt.OtherNoType,ljs.riskcode,ljs.managecom "
			+ " ";
		SSRS tSSRSNo7 = new ExeSQL().execSQL(tSQL);
		if(tSSRSNo7 != null && tSSRSNo7.getMaxRow() > 0){
			for (int i = 1; i <= tSSRSNo7.getMaxRow(); i++) {
				LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
				tLYPremSeparateDetailSchema.setBusiNo(tSSRSNo7.GetText(i, 1));
				tLYPremSeparateDetailSchema.setTempFeeNo(tSSRSNo7.GetText(i, 2));
				tLYPremSeparateDetailSchema.setTempFeeType(tSSRSNo7.GetText(i, 3));
				tLYPremSeparateDetailSchema.setOtherNo(tSSRSNo7.GetText(i, 4));
				tLYPremSeparateDetailSchema.setOtherNoType(tSSRSNo7.GetText(i, 5));
				tLYPremSeparateDetailSchema.setPrtNo(tSSRSNo7.GetText(i, 6));
				tLYPremSeparateDetailSchema.setPolicyNo(tSSRSNo7.GetText(i, 7));
				tLYPremSeparateDetailSchema.setRiskCode(tSSRSNo7.GetText(i, 8));
				tLYPremSeparateDetailSchema.setBusiType("01");
				tLYPremSeparateDetailSchema.setManageCom(tSSRSNo7.GetText(i, 9));
				tLYPremSeparateDetailSchema.setPayMoney(tSSRSNo7.GetText(i, 10));
				tLYPremSeparateDetailSchema.setState("01");
				tLYPremSeparateDetailSchema.setOtherState("09");
				tLYPremSeparateDetailSchema.setMoneyType("01");
				tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
				tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
				tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
			}
		}
		System.out.println("暂收不到险种的续期数据（共保团单）:"+tSSRSNo7.getMaxRow());
		
		//个单续期收费完全从投保人账户抵扣的
		tSQL = "select trim(lja.payno)||trim(lja.RiskCode) BusiNo, "
			 + " lja.getnoticeno TempFeeNo,'2' TempFeeType,ljs.OtherNo OtherNo,'0' OtherNoType, "
			 + " (select prtno from lccont where contno = lja.contno union select prtno from lbcont where contno = lja.contno) PrtNo,  "
			 + " lja.contno PolicyNo, "
			 + " lja.riskcode,lja.managecom,sum(lja.sumactupaymoney),lja.payno "
			 + " from ljspayb ljs ,ljapayperson lja"
			 + " where 1=1 "
			 + " and ljs.otherno=lja.contno"
			 + " and ljs.getnoticeno = lja.getnoticeno "
			 + " and (lja.grpcontno ='00000000000000000000' or lja.grpcontno is null)"
			 + " and lja.paytype = 'ZC' "
			 + " and ljs.sumduepaymoney=0"
			 + " and exists (select 1 from ljspaypersonb where getnoticeno=lja.getnoticeno and contno=lja.contno and paytype='YEL')"
			 + " and not exists (select busino from LYPremSeparateDetail where busino=(trim(lja.payno)||trim(lja.RiskCode))) "
			 + " and lja.confdate>='"+tDealDate+"' "
			 + tWherePart
			 + mWhereSQL
			 + " group by lja.getnoticeno,lja.RiskCode,ljs.OtherNo,lja.contno,lja.managecom,lja.payno "
			 + " with ur";
		SSRS tSSRSNo4 = new ExeSQL().execSQL(tSQL);
		if(tSSRSNo4 != null && tSSRSNo4.getMaxRow() > 0){
			for (int i = 1; i <= tSSRSNo4.getMaxRow(); i++) {
				LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
				tLYPremSeparateDetailSchema.setBusiNo(tSSRSNo4.GetText(i, 1));
				tLYPremSeparateDetailSchema.setTempFeeNo(tSSRSNo4.GetText(i, 2));
				tLYPremSeparateDetailSchema.setTempFeeType(tSSRSNo4.GetText(i, 3));
				tLYPremSeparateDetailSchema.setOtherNo(tSSRSNo4.GetText(i, 4));
				tLYPremSeparateDetailSchema.setOtherNoType(tSSRSNo4.GetText(i, 5));
				tLYPremSeparateDetailSchema.setPrtNo(tSSRSNo4.GetText(i, 6));
				tLYPremSeparateDetailSchema.setPolicyNo(tSSRSNo4.GetText(i, 7));
				tLYPremSeparateDetailSchema.setRiskCode(tSSRSNo4.GetText(i, 8));
//				tLYPremSeparateDetailSchema.setBusiType("01");
				tLYPremSeparateDetailSchema.setBusiType(tYGZ.getBusinType(null, "BF", tLYPremSeparateDetailSchema.getRiskCode()));
				tLYPremSeparateDetailSchema.setManageCom(tSSRSNo4.GetText(i, 9));
				tLYPremSeparateDetailSchema.setPayMoney(tSSRSNo4.GetText(i, 10));
				tLYPremSeparateDetailSchema.setState("01");
				tLYPremSeparateDetailSchema.setOtherState("01");
				tLYPremSeparateDetailSchema.setMoneyType("01");
				tLYPremSeparateDetailSchema.setMoneyNo(tSSRSNo4.GetText(i, 11));
				
				tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
				tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
				tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
			}
		}
		System.out.println("个单续期收费完全从投保人账户抵扣的:"+tSSRSNo4.getMaxRow());
		
		
		//团单续期收费完全从投保人账户抵扣的
		tSQL = "select trim(lja.payno)||trim(lja.RiskCode) BusiNo, "
		  + " lja.getnoticeno TempFeeNo,'2' TempFeeType,ljs.OtherNo OtherNo,'0' OtherNoType, "
		  + " (select prtno from lcgrpcont where grpcontno = lja.grpcontno union select prtno from lbgrpcont where grpcontno = lja.grpcontno) PrtNo,  "
		  + " lja.grpcontno PolicyNo, "
		  + " lja.riskcode,lja.managecom,sum(lja.sumactupaymoney),lja.payno"
		  + " from ljspayb ljs ,ljapaygrp lja"
		  + " where 1=1 "
		  + " and ljs.otherno=lja.grpcontno"
		  + " and ljs.getnoticeno = lja.getnoticeno "
		  + " and lja.paytype = 'ZC' "
		  + " and ljs.sumduepaymoney=0"
		  + " and exists (select 1 from ljspaygrpb where getnoticeno=ljs.getnoticeno and paytype='YEL')"
		  + " and not exists (select busino from LYPremSeparateDetail where busino=(trim(lja.payno)||trim(lja.RiskCode))) "
		  + " and not exists (select 1 from lcgrpcont where grpcontno = lja.grpcontno  and CoInsuranceFlag='1'"//共保
			+ " union select 1 from lbgrpcont where grpcontno = lja.grpcontno and CoInsuranceFlag='1' ) "
		  + " and lja.confdate>='"+tDealDate+"' "
		  + tWherePart
		  + mWhereSQL
		  + " group by lja.getnoticeno,lja.RiskCode,ljs.OtherNo,lja.grpcontno,lja.managecom,lja.payno "
		  + " with ur";
		SSRS tSSRSNo5 = new ExeSQL().execSQL(tSQL);
		if(tSSRSNo5 != null && tSSRSNo5.getMaxRow() > 0){
			for (int i = 1; i <= tSSRSNo5.getMaxRow(); i++) {
				LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
				tLYPremSeparateDetailSchema.setBusiNo(tSSRSNo5.GetText(i, 1));
				tLYPremSeparateDetailSchema.setTempFeeNo(tSSRSNo5.GetText(i, 2));
				tLYPremSeparateDetailSchema.setTempFeeType(tSSRSNo5.GetText(i, 3));
				tLYPremSeparateDetailSchema.setOtherNo(tSSRSNo5.GetText(i, 4));
				tLYPremSeparateDetailSchema.setOtherNoType(tSSRSNo5.GetText(i, 5));
				tLYPremSeparateDetailSchema.setPrtNo(tSSRSNo5.GetText(i, 6));
				tLYPremSeparateDetailSchema.setPolicyNo(tSSRSNo5.GetText(i, 7));
				tLYPremSeparateDetailSchema.setRiskCode(tSSRSNo5.GetText(i, 8));
				tLYPremSeparateDetailSchema.setBusiType("01");
				tLYPremSeparateDetailSchema.setManageCom(tSSRSNo5.GetText(i, 9));
				tLYPremSeparateDetailSchema.setPayMoney(tSSRSNo5.GetText(i, 10));
				tLYPremSeparateDetailSchema.setState("01");
				tLYPremSeparateDetailSchema.setOtherState("01");
				tLYPremSeparateDetailSchema.setMoneyType("01");
				tLYPremSeparateDetailSchema.setMoneyNo(tSSRSNo5.GetText(i, 11));
				
				tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
				tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
				tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
			}
		}
		System.out.println("团单续期收费完全从投保人账户抵扣的:"+tSSRSNo5.getMaxRow());
		
		//共保团单续期收费完全从投保人账户抵扣的
		tSQL = "select trim(lja.payno)||trim(lja.RiskCode) BusiNo, "
		  + " lja.getnoticeno TempFeeNo,'2' TempFeeType,ljs.OtherNo OtherNo,'0' OtherNoType, "
		  + " (select prtno from lcgrpcont where grpcontno = lja.grpcontno union select prtno from lbgrpcont where grpcontno = lja.grpcontno) PrtNo,  "
		  + " lja.grpcontno PolicyNo, "
		  + " lja.riskcode,lja.managecom,sum(lja.sumactupaymoney),lja.payno"
		  + " from ljspayb ljs ,ljapaygrp lja"
		  + " where 1=1 "
		  + " and ljs.otherno=lja.grpcontno"
		  + " and ljs.getnoticeno = lja.getnoticeno "
		  + " and lja.paytype = 'ZC' "
		  + " and ljs.sumduepaymoney=0"
		  + " and exists (select 1 from ljspaygrpb where getnoticeno=ljs.getnoticeno and paytype='YEL')"
		  + " and not exists (select busino from LYPremSeparateDetail where busino=(trim(lja.payno)||trim(lja.RiskCode))) "
		  + " and  exists (select 1 from lcgrpcont where grpcontno = lja.grpcontno  and CoInsuranceFlag='1'"//共保
			+ " union select 1 from lbgrpcont where grpcontno = lja.grpcontno and CoInsuranceFlag='1' ) "
		  + " and lja.confdate>='"+tDealDate+"' "
		  + tWherePart
		  + mWhereSQL
		  + " group by lja.getnoticeno,lja.RiskCode,ljs.OtherNo,lja.grpcontno,lja.managecom,lja.payno "
		  + " with ur";
		SSRS tSSRSNo8 = new ExeSQL().execSQL(tSQL);
		if(tSSRSNo8 != null && tSSRSNo8.getMaxRow() > 0){
			for (int i = 1; i <= tSSRSNo8.getMaxRow(); i++) {
				LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
				tLYPremSeparateDetailSchema.setBusiNo(tSSRSNo8.GetText(i, 1));
				tLYPremSeparateDetailSchema.setTempFeeNo(tSSRSNo8.GetText(i, 2));
				tLYPremSeparateDetailSchema.setTempFeeType(tSSRSNo8.GetText(i, 3));
				tLYPremSeparateDetailSchema.setOtherNo(tSSRSNo8.GetText(i, 4));
				tLYPremSeparateDetailSchema.setOtherNoType(tSSRSNo8.GetText(i, 5));
				tLYPremSeparateDetailSchema.setPrtNo(tSSRSNo8.GetText(i, 6));
				tLYPremSeparateDetailSchema.setPolicyNo(tSSRSNo8.GetText(i, 7));
				tLYPremSeparateDetailSchema.setRiskCode(tSSRSNo8.GetText(i, 8));
				tLYPremSeparateDetailSchema.setBusiType("01");
				tLYPremSeparateDetailSchema.setManageCom(tSSRSNo8.GetText(i, 9));
				tLYPremSeparateDetailSchema.setPayMoney(tSSRSNo8.GetText(i, 10));
				tLYPremSeparateDetailSchema.setState("01");
				tLYPremSeparateDetailSchema.setOtherState("09");
				tLYPremSeparateDetailSchema.setMoneyType("01");
				tLYPremSeparateDetailSchema.setMoneyNo(tSSRSNo8.GetText(i, 11));
				
				tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
				tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
				tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
			}
		}
		System.out.println("共保团单续期收费完全从投保人账户抵扣的:"+tSSRSNo8.getMaxRow());
		
		System.out.println("续期暂收价税分离明细数据共:"+tLYPremSeparateDetailSet.size());
		
		//账户部分扣费不到险种的续期数据（个单）
		tSQL =" select trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||trim(ljs.RiskCode)||trim(ljs.paytype) BusiNo, "
			+ " ljt.TempFeeNo TempFeeNo,ljt.TempFeeType TempFeeType,ljt.OtherNo OtherNo,ljt.OtherNoType OtherNoType, "
			+ " (select prtno from lccont where contno = ljt.otherno union select prtno from lbcont where contno = ljt.otherno) PrtNo,  "
			+ " ljt.otherno PolicyNo, "
			+ " ljs.riskcode,ljs.managecom,sum(ljs.sumactupaymoney) "
			+ " from ljtempfee ljt ,ljspaypersonb ljs "
			+ " where 1=1 "
			+ " and ljt.otherno = ljs.contno "
			+ " and ljt.tempfeeno = ljs.getnoticeno "
			+ " and ljs.paytype = 'YEL' "
			+ " and ljt.riskcode = '000000'  "
			+ " and tempfeetype ='2'  "
			+ " and not exists (select busino from LYPremSeparateDetail where busino=(trim(ljt.TempFeeNo) || trim(ljt.TempFeeType) ||trim(ljs.RiskCode)||trim(ljs.paytype))) "
			+ " and exists (select 1 from lccont where conttype='1' and appflag='1' and contno=ljs.contno "
			+ " 		union select 1 from lbcont where conttype='1' and contno=ljs.contno ) "

//			+ " and ljt.enteraccdate >= '"+tDealDate+"' "
			+ tWhereSQL
			+ mWherePart
			+ " group by ljt.TempFeeNo,ljt.TempFeeType,ljt.TempFeeNo,ljt.OtherNo,ljt.OtherNoType,ljs.riskcode,ljs.managecom,ljs.RiskCode,ljs.paytype "
			+ " with ur";
		SSRS tSSRSNo9 = new ExeSQL().execSQL(tSQL);
		if(tSSRSNo9 != null && tSSRSNo9.getMaxRow() > 0){
			for (int i = 1; i <= tSSRSNo9.getMaxRow(); i++) {
				LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
				tLYPremSeparateDetailSchema.setBusiNo(tSSRSNo9.GetText(i, 1));
				tLYPremSeparateDetailSchema.setTempFeeNo(tSSRSNo9.GetText(i, 2));
				tLYPremSeparateDetailSchema.setTempFeeType(tSSRSNo9.GetText(i, 3));
				tLYPremSeparateDetailSchema.setOtherNo(tSSRSNo9.GetText(i, 4));
				tLYPremSeparateDetailSchema.setOtherNoType(tSSRSNo9.GetText(i, 5));
				tLYPremSeparateDetailSchema.setPrtNo(tSSRSNo9.GetText(i, 6));
				tLYPremSeparateDetailSchema.setPolicyNo(tSSRSNo9.GetText(i, 7));
				tLYPremSeparateDetailSchema.setRiskCode(tSSRSNo9.GetText(i, 8));
				tLYPremSeparateDetailSchema.setBusiType("01");
				tLYPremSeparateDetailSchema.setManageCom(tSSRSNo9.GetText(i, 9));
				tLYPremSeparateDetailSchema.setPayMoney(tSSRSNo9.GetText(i, 10));
				tLYPremSeparateDetailSchema.setState("02");
				tLYPremSeparateDetailSchema.setOtherState("01");
				
				tLYPremSeparateDetailSchema.setMoneyType("01");
				tLYPremSeparateDetailSchema.setTaxRate("0.00");			
				tLYPremSeparateDetailSchema.setMoneyNoTax(tSSRSNo9.GetText(i, 10));
				tLYPremSeparateDetailSchema.setMoneyTax("0.00");
				tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
				tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);

				tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
			}
		}
		System.out.println("账户部分扣费不到险种的续期数据（个单）:"+tSSRSNo9.getMaxRow());
		
		//账户部分扣费不到险种的续期数据（团单）
		tSQL =" select trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||trim(ljs.RiskCode)||trim(ljs.paytype) BusiNo, "
			+ " ljt.TempFeeNo TempFeeNo,ljt.TempFeeType TempFeeType,ljt.OtherNo OtherNo,ljt.OtherNoType OtherNoType, "
			+ " (select prtno from lcgrpcont where grpcontno = ljt.otherno union select prtno from lbgrpcont where grpcontno = ljt.otherno) PrtNo,  "
			+ " ljt.otherno PolicyNo, "
			+ " ljs.riskcode,ljs.managecom,sum(ljs.sumactupaymoney) "
			+ " from ljtempfee ljt ,ljspaygrpb ljs "
			+ " where 1=1  "
			+ " and ljt.otherno = ljs.grpcontno "
			+ " and ljt.tempfeeno = ljs.getnoticeno "
			+ " and ljs.paytype in( 'YEL' ,'YET')"
			+ " and ljt.riskcode = '000000'  "
			+ " and tempfeetype ='2'  "
			+ " and not exists (select busino from LYPremSeparateDetail where busino=(trim(ljt.TempFeeNo) || trim(ljt.TempFeeType) ||trim(ljs.RiskCode)||trim(ljs.paytype))) "
			+ " and not exists (select 1 from lcgrpcont where  appflag='1' and grpcontno=ljs.grpcontno  and CoInsuranceFlag='1'"//共保
			+ " union select 1 from lbgrpcont where grpcontno=ljs.grpcontno  and CoInsuranceFlag='1') "
//			+ " and ljt.enteraccdate >= '"+tDealDate+"' "
			+ tWhereSQL
			+ mWherePart
			+ " group by ljt.TempFeeNo,ljt.TempFeeType,ljt.TempFeeNo,ljt.OtherNo,ljt.OtherNoType,ljs.riskcode,ljs.managecom,ljs.RiskCode, ljs.paytype "
			+ " ";
		SSRS tSSRSNo10 = new ExeSQL().execSQL(tSQL);
		if(tSSRSNo10 != null && tSSRSNo10.getMaxRow() > 0){
			for (int i = 1; i <= tSSRSNo10.getMaxRow(); i++) {
				LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
				tLYPremSeparateDetailSchema.setBusiNo(tSSRSNo10.GetText(i, 1));
				tLYPremSeparateDetailSchema.setTempFeeNo(tSSRSNo10.GetText(i, 2));
				tLYPremSeparateDetailSchema.setTempFeeType(tSSRSNo10.GetText(i, 3));
				tLYPremSeparateDetailSchema.setOtherNo(tSSRSNo10.GetText(i, 4));
				tLYPremSeparateDetailSchema.setOtherNoType(tSSRSNo10.GetText(i, 5));
				tLYPremSeparateDetailSchema.setPrtNo(tSSRSNo10.GetText(i, 6));
				tLYPremSeparateDetailSchema.setPolicyNo(tSSRSNo10.GetText(i, 7));
				tLYPremSeparateDetailSchema.setRiskCode(tSSRSNo10.GetText(i, 8));
				tLYPremSeparateDetailSchema.setBusiType("01");
				tLYPremSeparateDetailSchema.setManageCom(tSSRSNo10.GetText(i, 9));
				tLYPremSeparateDetailSchema.setPayMoney(tSSRSNo10.GetText(i, 10));
				tLYPremSeparateDetailSchema.setState("02");
				tLYPremSeparateDetailSchema.setOtherState("01");
				tLYPremSeparateDetailSchema.setMoneyType("01");
				tLYPremSeparateDetailSchema.setTaxRate("0.00");			
				tLYPremSeparateDetailSchema.setMoneyNoTax(tSSRSNo10.GetText(i, 10));
				tLYPremSeparateDetailSchema.setMoneyTax("0.00");
				tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
				tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
				tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
			}
		}
		System.out.println("账户部分扣费不到险种的续期数据（团单）:"+tSSRSNo10.getMaxRow());
		
//		if(tLYPremSeparateDetailSet.size()>1000){
			mapsubmit(tLYPremSeparateDetailSet);	
			tLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
//		}
		
//		mMap.put(tLYPremSeparateDetailSet, SysConst.INSERT);
		
		return true;
	}
	/**
	 * 准备后台的数据
	 * @return boolean
	 */
	private boolean prepareOutputData() {
		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCProjectCancelBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
	/**
	 * 数据分批提交
	 * @param aPrtNo
	 * @param aRiskCode
	 * @return
	 */
	private boolean mapsubmit(LYPremSeparateDetailSet tLYPremSeparateDetailSet){
		int count=0;
		LYPremSeparateDetailSet tDetailSet = new LYPremSeparateDetailSet();
		if(null == tLYPremSeparateDetailSet
				|| tLYPremSeparateDetailSet.size() < 1){
			System.out.println("===没有需要价税分离的数据！===");
		}
		for(int i = 1; i <= tLYPremSeparateDetailSet.size(); i++){
			LYPremSeparateDetailSchema detailSchema=tLYPremSeparateDetailSet.get(i).getSchema();
			detailSchema.setModifyDate(PubFun.getCurrentDate());
			detailSchema.setModifyTime(PubFun.getCurrentTime());
			tDetailSet.add(detailSchema);
			count ++;
			if(count >= sysCount || i == tLYPremSeparateDetailSet.size()){
				count = 0;
				mMap=new MMap();
				mMap.put(tDetailSet, SysConst.INSERT);
				tDetailSet=new LYPremSeparateDetailSet();
				if (!prepareOutputData()) {
					return false;
				}

				// 保存数据
				PubSubmit tPubSubmit = new PubSubmit();

				if (!tPubSubmit.submitData(mInputData, mOperate)) {
					System.out.print("保存数据返回");
					// @@错误处理
					this.mErrors.copyAllErrors(tPubSubmit.mErrors);
					CError tError = new CError();
					tError.moduleName = "GetTempFeeSeparateBL";
					tError.functionName = "submitData";
					tError.errorMessage = "数据提交失败!";
					this.mErrors.addOneError(tError);
//					return false;
				}
				mInputData = null;
			}
		}
		System.out.println("结束！！！！");
		return true;
	}
	public static void main(String[] args) {
		GetXqTempFeeSeparateBL tGetTempFeeSeparateBL = new GetXqTempFeeSeparateBL();
		VData cInputData = new VData();
		TransferData tTransferData = new TransferData();
		String tStartDate = "2018-09-19";
		String tEndDate = "2018-09-19";
		tTransferData.setNameAndValue("StartDate", tStartDate);
		tTransferData.setNameAndValue("EndDate", tEndDate);
		cInputData.add(tTransferData);
		tGetTempFeeSeparateBL.submitData(cInputData, "");
	}
}
