package com.sinosoft.lis.ygz;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LYPremSeparateDetailSchema;
import com.sinosoft.lis.vschema.LYPremSeparateDetailSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 保全暂收价税分离数据提数
 * @author lzy
 *
 */
public class GetBqTempFeeBL {
	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	/** 数据操作字符串 */
	private String mOperate;
	/** 开始日期*/
	private String mStartDate;
	/** 结束日期*/
	private String mEndDate;
	//页面手工提取标志
	private String mHandFlag;
	
	private String mWherePart;
	
	private int sysCount = 5000;//默认每一千条数据发送一次请求

	
	
	//业务处理相关变量
	private MMap mMap = new MMap();
	
	private String mCurrentDate = PubFun.getCurrentDate();
	private String mCurrentTime = PubFun.getCurrentTime();
	
	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		// 进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GetTempFeeBqSeparateBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败GetTempFeeBqSeparateBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}

/*		// 准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}

		// 保存数据
		PubSubmit tPubSubmit = new PubSubmit();

		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			System.out.print("保存数据返回");
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "LCProjectCancelBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mInputData = null;*/
		return true;
	}

	// 获得输出数据
	private boolean getInputData(VData cInputData) {
		TransferData tTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
		mStartDate = (String) tTransferData.getValueByName("StartDate");
		mEndDate = (String) tTransferData.getValueByName("EndDate");
		
		mHandFlag = (String) tTransferData.getValueByName("HandWorkFlag");
		mWherePart = "";
		if(null != mHandFlag && !"".equals(mHandFlag)){
			LYPremSeparateDetailSet tLYPremSeparateDetailSet=(LYPremSeparateDetailSet) cInputData
			.getObjectByObjectName("LYPremSeparateDetailSet", 0);
			if(null != tLYPremSeparateDetailSet){
				String lisStr="";
				for(int i=1; i<=tLYPremSeparateDetailSet.size();i++){
					lisStr += "'"+ tLYPremSeparateDetailSet.get(i).getOtherNo()+"'";
					if(i<tLYPremSeparateDetailSet.size()){
						lisStr += ",";
					}
				}
				mWherePart = " and lja.endorsementno in ("+lisStr+") ";
			}
		}
		return true;
	}
	
//  业务处理
	private boolean dealData() {
		YGZ mYGZ = new YGZ();
		LYPremSeparateDetailSet tLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
		String tWhereSQL = "";
		String tWherePart = "";
		String tDealDate = "2016-05-01";
		if(mStartDate != null && !"".equals(mStartDate)){
			tWhereSQL += " and ljt.confmakedate >= '" + mStartDate + "' ";
			tWherePart += " and ljap.confdate >= '" + mStartDate + "' ";
		}
		if(mEndDate != null && !"".equals(mEndDate)){
			tWhereSQL += " and ljt.confmakedate <= '" + mEndDate + "' ";
			tWherePart += " and ljap.confdate <= '" + mEndDate + "' ";
		}
		if("".equals(tWhereSQL)){
			tWhereSQL = " and ljt.confmakedate >='"+tDealDate+"' and ljt.confmakedate >= current date - 7 days ";
			tWherePart = " and ljap.confdate >= '"+tDealDate+"' and ljap.confdate >= current date - 7 days ";
		}
		
		if(null != mHandFlag && !"".equals(mHandFlag)){
			tWhereSQL = "";
			tWherePart ="";
		}
		//20161215暂收价税分离调整了主键信息，为防止页面功能重复提数，增加校验
		tWhereSQL += " and not exists (select distinct 1 from LYPremSeparateDetail where tempfeeno=ljt.tempfeeno and tempfeetype in ('4','7','9') ) ";
		tWherePart +=" and not exists (select distinct 1 from LYPremSeparateDetail where otherno=ljap.incomeno and tempfeetype in ('4','7','9') ) ";
		
//		//1 已结案保全的暂收--个单
		String tSQL =  " select trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||trim(nvl(lja.polno,''))||trim(lja.feeoperationtype)||trim(lja.feefinatype) BusiNo, "
			+ " ljt.TempFeeNo TempFeeNo,ljt.TempFeeType TempFeeType,ljt.OtherNo OtherNo,ljt.OtherNoType OtherNoType, "
			+ " 	(select prtno from lccont where contno = lja.contno union select prtno from lbcont where contno = lja.contno) PrtNo, "
			+ " lja.contno PolicyNo, "
			+ " lja.riskcode,ljt.managecom,sum(lja.getmoney),lja.feeoperationtype,lja.feefinatype "
			+ " from ljtempfee ljt ,ljagetendorse lja "
			+ " where 1=1 "
			+ " and ljt.otherno = lja.endorsementno "
			+ " and ljt.tempfeetype ='4' "
			+ " and ljt.othernotype ='10' "//个单保全收费
			+ " and (lja.grpcontno is null or lja.grpcontno='00000000000000000000') "
//			+ " and lja.feeoperationtype not in ('YS') "//预收保全收费不拆分
			+ " and not exists (select busino from LYPremSeparateDetail where busino=trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||trim(nvl(lja.polno,''))||trim(lja.feeoperationtype)||trim(lja.feefinatype))  "
			+ " and not exists (select busino from LYPremSeparateDetail where busino=trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||trim(nvl(lja.riskcode,''))||trim(lja.feeoperationtype)||trim(lja.feefinatype)) "//根据一期主键过滤
			+ tWhereSQL
			+ mWherePart
			+ " group by ljt.TempFeeNo,ljt.TempFeeType,ljt.TempFeeNo,ljt.OtherNo,ljt.OtherNoType,lja.riskcode,ljt.managecom,lja.contno,lja.feeoperationtype,lja.feefinatype,lja.polno "
			+ " with ur";
		
		SSRS tNoZeroSSRS = new ExeSQL().execSQL(tSQL);
		if(tNoZeroSSRS != null && tNoZeroSSRS.getMaxRow() > 0){
			for (int i = 1; i <= tNoZeroSSRS.getMaxRow(); i++) {
				LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
				tLYPremSeparateDetailSchema.setBusiNo(tNoZeroSSRS.GetText(i, 1));
				tLYPremSeparateDetailSchema.setTempFeeNo(tNoZeroSSRS.GetText(i, 2));
				tLYPremSeparateDetailSchema.setTempFeeType(tNoZeroSSRS.GetText(i, 3));
				tLYPremSeparateDetailSchema.setOtherNo(tNoZeroSSRS.GetText(i, 4));
				tLYPremSeparateDetailSchema.setOtherNoType(tNoZeroSSRS.GetText(i, 5));
				tLYPremSeparateDetailSchema.setPrtNo(tNoZeroSSRS.GetText(i, 6));
				tLYPremSeparateDetailSchema.setPolicyNo(tNoZeroSSRS.GetText(i, 7));
				tLYPremSeparateDetailSchema.setRiskCode(tNoZeroSSRS.GetText(i, 8));
				tLYPremSeparateDetailSchema.setManageCom(tNoZeroSSRS.GetText(i, 9));
				tLYPremSeparateDetailSchema.setPayMoney(tNoZeroSSRS.GetText(i, 10));
				tLYPremSeparateDetailSchema.setState("01");//数据提取状态
				tLYPremSeparateDetailSchema.setOtherState("01");//暂收提取的数据
				tLYPremSeparateDetailSchema.setMoneyType("01");//收付费类型:01-收费；02-付费
				tLYPremSeparateDetailSchema.setFeeOperationType(tNoZeroSSRS.GetText(i, 11));
				tLYPremSeparateDetailSchema.setFeeFinaType(tNoZeroSSRS.GetText(i, 12));
//				tLYPremSeparateDetailSchema.setMoneyNo(tNoZeroSSRS.GetText(i, 13));
				tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
				tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
				String BusiType="01";//默认为保费
				BusiType = mYGZ.getBusinType(tLYPremSeparateDetailSchema.getFeeOperationType(), tLYPremSeparateDetailSchema.getFeeFinaType(), tLYPremSeparateDetailSchema.getRiskCode());
				if("YEI".equals(tLYPremSeparateDetailSchema.getFeeFinaType())
						|| "YEO".equals(tLYPremSeparateDetailSchema.getFeeFinaType()) 
						|| "YS".equals(tLYPremSeparateDetailSchema.getFeeOperationType()) ){
					BusiType ="01";//账户抵扣默认01,不需要再通过接口价税分离
					tLYPremSeparateDetailSchema.setState("02");//数据提取状态
					tLYPremSeparateDetailSchema.setTaxRate("0.00");
					tLYPremSeparateDetailSchema.setMoneyNoTax(tLYPremSeparateDetailSchema.getPayMoney());
					tLYPremSeparateDetailSchema.setMoneyTax("0.00");
				}
				tLYPremSeparateDetailSchema.setBusiType(BusiType);
				
				tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
			}
		}
		System.out.println("保全暂收数据提取：1、已结案保全的暂收--个单："+tNoZeroSSRS.getMaxRow());
		//2 已结案保全的暂收 --团单
		tSQL = " select trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||trim(nvl(lja.polno,''))||trim(lja.feeoperationtype)||trim(lja.feefinatype) BusiNo, "
			+ " ljt.TempFeeNo TempFeeNo,ljt.TempFeeType TempFeeType,ljt.OtherNo OtherNo,ljt.OtherNoType OtherNoType, "
			+ " (select prtno from lcgrpcont where grpcontno = lja.grpcontno union select prtno from lbgrpcont where grpcontno = lja.grpcontno) PrtNo, "
			+ " lja.grpcontno PolicyNo, "
			+ " lja.riskcode,ljt.managecom,sum(lja.getmoney),lja.feeoperationtype,lja.feefinatype, "
			+ " (select CoInsuranceFlag from lcgrpcont where appflag='1' and grpcontno=lja.grpcontno "
			+ "  union select CoInsuranceFlag from lbgrpcont where grpcontno=lja.grpcontno ) CoInsuranceFlag "
			+ " from ljtempfee ljt ,ljagetendorse lja "
			+ " where 1=1  "
			+ " and ljt.otherno = lja.endorsementno "
			+ " and ljt.tempfeetype in ('4','7','9') "
			+ " and (lja.grpcontno is not null and lja.grpcontno<>'00000000000000000000') "
			+ " and ljt.othernotype ='3' "//团单保全收费
//			+ " and lja.feeoperationtype not in ('YS') "//预收保全收费不拆分
			+ " and not exists (select busino from LYPremSeparateDetail where busino=trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||trim(nvl(lja.polno,''))||trim(lja.feeoperationtype)||trim(lja.feefinatype))  "
			+ " and not exists (select busino from LYPremSeparateDetail where busino=trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||trim(nvl(lja.riskcode,''))||trim(lja.feeoperationtype)||trim(lja.feefinatype)) "//根据一期主键过滤
			+ tWhereSQL
			+ mWherePart
			+ " group by ljt.TempFeeNo,ljt.TempFeeType,ljt.TempFeeNo,ljt.OtherNo,ljt.OtherNoType,lja.riskcode,ljt.managecom,lja.grpcontno,lja.feeoperationtype,lja.feefinatype,lja.polno "
			+ " with ur";
		
		SSRS tSSRSNo2 = new ExeSQL().execSQL(tSQL);
		if(tSSRSNo2 != null && tSSRSNo2.getMaxRow() > 0){
			for (int i = 1; i <= tSSRSNo2.getMaxRow(); i++) {
				LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
				tLYPremSeparateDetailSchema.setBusiNo(tSSRSNo2.GetText(i, 1));
				tLYPremSeparateDetailSchema.setTempFeeNo(tSSRSNo2.GetText(i, 2));
				tLYPremSeparateDetailSchema.setTempFeeType(tSSRSNo2.GetText(i, 3));
				tLYPremSeparateDetailSchema.setOtherNo(tSSRSNo2.GetText(i, 4));
				tLYPremSeparateDetailSchema.setOtherNoType(tSSRSNo2.GetText(i, 5));
				tLYPremSeparateDetailSchema.setPrtNo(tSSRSNo2.GetText(i, 6));
				tLYPremSeparateDetailSchema.setPolicyNo(tSSRSNo2.GetText(i, 7));
				tLYPremSeparateDetailSchema.setRiskCode(tSSRSNo2.GetText(i, 8));
				tLYPremSeparateDetailSchema.setManageCom(tSSRSNo2.GetText(i, 9));
				tLYPremSeparateDetailSchema.setPayMoney(tSSRSNo2.GetText(i, 10));
				tLYPremSeparateDetailSchema.setState("01");//数据提取状态
				tLYPremSeparateDetailSchema.setOtherState("01");//暂收提取的数据
				tLYPremSeparateDetailSchema.setMoneyType("01");//收付费类型:01-收费；02-付费
				tLYPremSeparateDetailSchema.setFeeOperationType(tSSRSNo2.GetText(i, 11));
				tLYPremSeparateDetailSchema.setFeeFinaType(tSSRSNo2.GetText(i, 12));
				//共保标识
				String coInsuranceFlag = tSSRSNo2.GetText(i, 13);
				if("1".equals(coInsuranceFlag)){
					tLYPremSeparateDetailSchema.setOtherState("09");
				}
				tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
				tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
				String BusiType="01";//默认为保费
				BusiType = mYGZ.getBusinType(tLYPremSeparateDetailSchema.getFeeOperationType(), tLYPremSeparateDetailSchema.getFeeFinaType(), tLYPremSeparateDetailSchema.getRiskCode());
				if("YEI".equals(tLYPremSeparateDetailSchema.getFeeFinaType())
						|| "YEO".equals(tLYPremSeparateDetailSchema.getFeeFinaType()) 
						|| "YS".equals(tLYPremSeparateDetailSchema.getFeeOperationType())){
					BusiType ="01";//账户抵扣默认01,不需要再通过接口价税分离
					tLYPremSeparateDetailSchema.setBusiNo(tLYPremSeparateDetailSchema.getTempFeeNo()+tLYPremSeparateDetailSchema.getTempFeeType()
							+tLYPremSeparateDetailSchema.getFeeOperationType()+tLYPremSeparateDetailSchema.getFeeFinaType());
					tLYPremSeparateDetailSchema.setState("02");//数据提取状态
					tLYPremSeparateDetailSchema.setTaxRate("0.00");
					tLYPremSeparateDetailSchema.setMoneyNoTax(tLYPremSeparateDetailSchema.getPayMoney());
					tLYPremSeparateDetailSchema.setMoneyTax("0.00");
				}
				tLYPremSeparateDetailSchema.setBusiType(BusiType);
				
				tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
			}
		}
		System.out.println("保全暂收数据提取：2、已结案保全的暂收--团单："+tSSRSNo2.getMaxRow());
		
		
		//3 保全未结案的数据--个单
		tSQL = " select trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||trim(nvl(lja.polno,''))||trim(lja.feeoperationtype)||trim(lja.feefinatype) BusiNo, "
			+ " ljt.TempFeeNo TempFeeNo,ljt.TempFeeType TempFeeType,ljt.OtherNo OtherNo,ljt.OtherNoType OtherNoType, "
			+ " (select prtno from lccont where contno = lja.contno union select prtno from lbcont where contno = lja.contno) PrtNo, "
			+ " lja.contno PolicyNo, "
			+ " lja.riskcode,ljt.managecom,sum(lja.getmoney),lja.feeoperationtype,lja.feefinatype "
			+ " from ljtempfee ljt ,ljsgetendorse lja "
			+ " where 1=1  "
			+ " and ljt.otherno = lja.endorsementno "
			+ " and ljt.tempfeetype ='4' "
			+ " and ljt.othernotype ='10' "//个单保全收费
			+ " and (lja.grpcontno is null or lja.grpcontno='00000000000000000000') "
//			+ " and lja.feeoperationtype not in ('YS') "//预收保全收费不拆分
//			+ " and ljt.confdate is null "//未核销
			+ " and not exists (select busino from LYPremSeparateDetail where busino=trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||trim(nvl(lja.polno,''))||trim(lja.feeoperationtype)||trim(lja.feefinatype))  "
			+ " and not exists (select busino from LYPremSeparateDetail where busino=trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||trim(nvl(lja.riskcode,''))||trim(lja.feeoperationtype)||trim(lja.feefinatype)) "//根据一期主键过滤
			+ tWhereSQL
			+ mWherePart
			+ " group by ljt.TempFeeNo,ljt.TempFeeType,ljt.TempFeeNo,ljt.OtherNo,ljt.OtherNoType,lja.riskcode,ljt.managecom,lja.contno,lja.feeoperationtype,lja.feefinatype,lja.polno "
			+ " with ur";
		
		SSRS tSSRSNo3 = new ExeSQL().execSQL(tSQL);
		if(tSSRSNo3 != null && tSSRSNo3.getMaxRow() > 0){
			for (int i = 1; i <= tSSRSNo3.getMaxRow(); i++) {
				LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
				tLYPremSeparateDetailSchema.setBusiNo(tSSRSNo3.GetText(i, 1));
				tLYPremSeparateDetailSchema.setTempFeeNo(tSSRSNo3.GetText(i, 2));
				tLYPremSeparateDetailSchema.setTempFeeType(tSSRSNo3.GetText(i, 3));
				tLYPremSeparateDetailSchema.setOtherNo(tSSRSNo3.GetText(i, 4));
				tLYPremSeparateDetailSchema.setOtherNoType(tSSRSNo3.GetText(i, 5));
				tLYPremSeparateDetailSchema.setPrtNo(tSSRSNo3.GetText(i, 6));
				tLYPremSeparateDetailSchema.setPolicyNo(tSSRSNo3.GetText(i, 7));
				tLYPremSeparateDetailSchema.setRiskCode(tSSRSNo3.GetText(i, 8));
				tLYPremSeparateDetailSchema.setManageCom(tSSRSNo3.GetText(i, 9));
				tLYPremSeparateDetailSchema.setPayMoney(tSSRSNo3.GetText(i, 10));
				tLYPremSeparateDetailSchema.setState("01");//数据提取状态
				tLYPremSeparateDetailSchema.setOtherState("01");//暂收提取的数据
				tLYPremSeparateDetailSchema.setMoneyType("01");//收付费类型:01-收费；02-付费
				tLYPremSeparateDetailSchema.setFeeOperationType(tSSRSNo3.GetText(i, 11));
				tLYPremSeparateDetailSchema.setFeeFinaType(tSSRSNo3.GetText(i, 12));
				tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
				tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
				String BusiType="01";//默认为保费
				BusiType = mYGZ.getBusinType(tLYPremSeparateDetailSchema.getFeeOperationType(), tLYPremSeparateDetailSchema.getFeeFinaType(), tLYPremSeparateDetailSchema.getRiskCode());
				if("YEI".equals(tLYPremSeparateDetailSchema.getFeeFinaType())
						|| "YEO".equals(tLYPremSeparateDetailSchema.getFeeFinaType())
						|| "YS".equals(tLYPremSeparateDetailSchema.getFeeOperationType())){
					BusiType ="01";//账户抵扣默认01,不需要再通过接口价税分离
					tLYPremSeparateDetailSchema.setState("02");//数据提取状态
					tLYPremSeparateDetailSchema.setTaxRate("0.00");
					tLYPremSeparateDetailSchema.setMoneyNoTax(tLYPremSeparateDetailSchema.getPayMoney());
					tLYPremSeparateDetailSchema.setMoneyTax("0.00");
				}
				tLYPremSeparateDetailSchema.setBusiType(BusiType);
				
				tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
			}
		}
		System.out.println("保全暂收数据提取：3、未结案保全的暂收--个单："+tSSRSNo3.getMaxRow());
		
		//4 保全未结案的数据--团单
		tSQL = " select trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||trim(nvl(lja.polno,''))||trim(lja.feeoperationtype)||trim(lja.feefinatype) BusiNo, "
			+ " ljt.TempFeeNo TempFeeNo,ljt.TempFeeType TempFeeType,ljt.OtherNo OtherNo,ljt.OtherNoType OtherNoType, "
			+ " 	(select prtno from lcgrpcont where grpcontno = lja.grpcontno union select prtno from lbgrpcont where grpcontno = lja.grpcontno) PrtNo, "
			+ " lja.grpcontno PolicyNo, "
			+ " lja.riskcode,ljt.managecom,sum(lja.getmoney),lja.feeoperationtype,lja.feefinatype, "
			+ " (select CoInsuranceFlag from lcgrpcont where appflag='1' and grpcontno=lja.grpcontno "
			+ "  union select CoInsuranceFlag from lbgrpcont where grpcontno=lja.grpcontno ) CoInsuranceFlag "
			+ " from ljtempfee ljt ,ljsgetendorse lja "
			+ " where 1=1  "
			+ " and ljt.otherno = lja.endorsementno "
			+ " and ljt.tempfeetype in ('4','7','9') "
			+ " and ljt.othernotype ='3' "//团单保全收费
			+ " and (lja.grpcontno is not null and lja.grpcontno<>'00000000000000000000') "
//			+ " and lja.feeoperationtype not in ('YS') "//预收保全收费不拆分
			+ " and ljt.confdate is null "//未核销
			+ " and not exists (select busino from LYPremSeparateDetail where busino=trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||trim(nvl(lja.polno,''))||trim(lja.feeoperationtype)||trim(lja.feefinatype)) "
			+ " and not exists (select busino from LYPremSeparateDetail where busino=trim(ljt.TempFeeNo)||trim(ljt.TempFeeType)||trim(nvl(lja.riskcode,''))||trim(lja.feeoperationtype)||trim(lja.feefinatype)) "//根据一期主键过滤
			+ tWhereSQL
			+ mWherePart
			+ " group by ljt.TempFeeNo,ljt.TempFeeType,ljt.TempFeeNo,ljt.OtherNo,ljt.OtherNoType,lja.riskcode,ljt.managecom,lja.grpcontno,lja.feeoperationtype,lja.feefinatype,lja.polno "
			+ " with ur";
		
		SSRS tSSRSNo4 = new ExeSQL().execSQL(tSQL);
		if(tSSRSNo4 != null && tSSRSNo4.getMaxRow() > 0){
			for (int i = 1; i <= tSSRSNo4.getMaxRow(); i++) {
				LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
				tLYPremSeparateDetailSchema.setBusiNo(tSSRSNo4.GetText(i, 1));
				tLYPremSeparateDetailSchema.setTempFeeNo(tSSRSNo4.GetText(i, 2));
				tLYPremSeparateDetailSchema.setTempFeeType(tSSRSNo4.GetText(i, 3));
				tLYPremSeparateDetailSchema.setOtherNo(tSSRSNo4.GetText(i, 4));
				tLYPremSeparateDetailSchema.setOtherNoType(tSSRSNo4.GetText(i, 5));
				tLYPremSeparateDetailSchema.setPrtNo(tSSRSNo4.GetText(i, 6));
				tLYPremSeparateDetailSchema.setPolicyNo(tSSRSNo4.GetText(i, 7));
				tLYPremSeparateDetailSchema.setRiskCode(tSSRSNo4.GetText(i, 8));
				tLYPremSeparateDetailSchema.setManageCom(tSSRSNo4.GetText(i, 9));
				tLYPremSeparateDetailSchema.setPayMoney(tSSRSNo4.GetText(i, 10));
				tLYPremSeparateDetailSchema.setState("01");//数据提取状态
				tLYPremSeparateDetailSchema.setOtherState("01");//暂收提取的数据
				tLYPremSeparateDetailSchema.setMoneyType("01");//收付费类型:01-收费；02-付费
				tLYPremSeparateDetailSchema.setFeeOperationType(tSSRSNo4.GetText(i, 11));
				tLYPremSeparateDetailSchema.setFeeFinaType(tSSRSNo4.GetText(i, 12));
				//共保标识
				String coInsuranceFlag = tSSRSNo4.GetText(i, 13);
				if("1".equals(coInsuranceFlag)){
					tLYPremSeparateDetailSchema.setOtherState("09");
				}
				tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
				tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
				String BusiType="01";//默认为保费
				BusiType = mYGZ.getBusinType(tLYPremSeparateDetailSchema.getFeeOperationType(), tLYPremSeparateDetailSchema.getFeeFinaType(), tLYPremSeparateDetailSchema.getRiskCode());
				if("YEI".equals(tLYPremSeparateDetailSchema.getFeeFinaType())
						|| "YEO".equals(tLYPremSeparateDetailSchema.getFeeFinaType())
						|| "YS".equals(tLYPremSeparateDetailSchema.getFeeOperationType())){
					BusiType ="01";//账户抵扣默认01,不需要再通过接口价税分离
					tLYPremSeparateDetailSchema.setState("02");//数据提取状态
					tLYPremSeparateDetailSchema.setTaxRate("0.00");
					tLYPremSeparateDetailSchema.setMoneyNoTax(tLYPremSeparateDetailSchema.getPayMoney());
					tLYPremSeparateDetailSchema.setMoneyTax("0.00");
				}
				tLYPremSeparateDetailSchema.setBusiType(BusiType);
				
				tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
			}
		}
		System.out.println("保全暂收数据提取：4、未结案保全的暂收--团单："+tSSRSNo4.getMaxRow());
		
		
		//个单保全收费完全从投保人账户抵扣
		tSQL =  "select trim(ljap.payno)||trim(lja.polno)||trim(lja.feeoperationtype)||trim(lja.feefinatype) BusiNo, "
		  + " ljap.getnoticeno TempFeeNo,'4' TempFeeType,ljap.incomeno OtherNo,ljap.incometype OtherNoType, "
		  + " 	(select prtno from lccont where contno = lja.contno union select prtno from lbcont where contno = lja.contno) PrtNo, "
		  + " lja.contno PolicyNo, "
		  + " lja.riskcode,lja.managecom,sum(lja.getmoney),lja.feeoperationtype,lja.feefinatype,ljap.payno "
		  + " from ljapay ljap ,ljagetendorse lja "
		  + " where 1=1 "
		  + " and ljap.incomeno = lja.endorsementno "
		  + " and ljap.incometype ='10' "//个单保全收费
		  + " and ljap.sumactupaymoney =0 " //完全从投保人账户抵扣的
		  + " and (lja.grpcontno is null or lja.grpcontno='00000000000000000000') "
		  + " and lja.feeoperationtype not in ('YS','YEI','YEO') "//预收保全收费不拆分
		  + " and exists (select 1 from ljagetendorse where endorsementno=lja.endorsementno and actugetno=lja.actugetno and feeoperationtype='YEO')"
		  + " and not exists (select busino from LYPremSeparateDetail where busino=trim(ljap.payno)||trim(lja.polno)||trim(lja.feeoperationtype)||trim(lja.feefinatype)) "
		  + " and not exists (select busino from LYPremSeparateDetail where busino=trim(ljap.payno)||trim(lja.riskcode)||trim(lja.feeoperationtype)||trim(lja.feefinatype)) "//根据一期主键过滤
		  + tWherePart
		  + mWherePart
		  + " group by ljap.payno,ljap.incomeno,ljap.incometype,lja.riskcode,lja.managecom,lja.contno,lja.feeoperationtype,lja.feefinatype,ljap.getnoticeno,lja.polno "
		  + " with ur";
		SSRS tSSRSNo6 = new ExeSQL().execSQL(tSQL);
		if(tSSRSNo6 != null && tSSRSNo6.getMaxRow() > 0){
			for (int i = 1; i <= tSSRSNo6.getMaxRow(); i++) {
				LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
				tLYPremSeparateDetailSchema.setBusiNo(tSSRSNo6.GetText(i, 1));
				tLYPremSeparateDetailSchema.setTempFeeNo(tSSRSNo6.GetText(i, 2));
				tLYPremSeparateDetailSchema.setTempFeeType(tSSRSNo6.GetText(i, 3));
				tLYPremSeparateDetailSchema.setOtherNo(tSSRSNo6.GetText(i, 4));
				tLYPremSeparateDetailSchema.setOtherNoType(tSSRSNo6.GetText(i, 5));
				tLYPremSeparateDetailSchema.setPrtNo(tSSRSNo6.GetText(i, 6));
				tLYPremSeparateDetailSchema.setPolicyNo(tSSRSNo6.GetText(i, 7));
				tLYPremSeparateDetailSchema.setRiskCode(tSSRSNo6.GetText(i, 8));
				tLYPremSeparateDetailSchema.setManageCom(tSSRSNo6.GetText(i, 9));
				tLYPremSeparateDetailSchema.setPayMoney(tSSRSNo6.GetText(i, 10));
				tLYPremSeparateDetailSchema.setState("01");//数据提取状态
				tLYPremSeparateDetailSchema.setOtherState("01");//暂收提取的数据
				tLYPremSeparateDetailSchema.setMoneyType("01");//收付费类型:01-收费；02-付费
				tLYPremSeparateDetailSchema.setFeeOperationType(tSSRSNo6.GetText(i, 11));
				tLYPremSeparateDetailSchema.setFeeFinaType(tSSRSNo6.GetText(i, 12));
				tLYPremSeparateDetailSchema.setMoneyNo(tSSRSNo6.GetText(i, 13));
				tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
				tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
				String BusiType="01";//默认为保费
				BusiType = mYGZ.getBusinType(tLYPremSeparateDetailSchema.getFeeOperationType(), tLYPremSeparateDetailSchema.getFeeFinaType(), tLYPremSeparateDetailSchema.getRiskCode());
				tLYPremSeparateDetailSchema.setBusiType(BusiType);
				
				tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
			}
		}
		System.out.println("保全暂收数据提取：个单保全收费完全从投保人账户抵扣："+tSSRSNo6.getMaxRow());
		
		//团单保全收费完全从投保人账户抵扣
		tSQL="select trim(ljap.payno)||trim(lja.polno)||trim(lja.feeoperationtype)||trim(lja.feefinatype) BusiNo, "
			+ " ljap.getnoticeno TempFeeNo,'4' TempFeeType,ljap.incomeno OtherNo,ljap.incometype OtherNoType, "
			+ " 	(select prtno from lcgrpcont where grpcontno = lja.grpcontno union select prtno from lbgrpcont where grpcontno = lja.grpcontno) PrtNo, "
			+ " lja.grpcontno PolicyNo, "
			+ " lja.riskcode,lja.managecom,sum(lja.getmoney),lja.feeoperationtype,lja.feefinatype,ljap.payno, "
			+ " (select CoInsuranceFlag from lcgrpcont where appflag='1' and grpcontno=lja.grpcontno "
			+ "  union select CoInsuranceFlag from lbgrpcont where grpcontno=lja.grpcontno ) CoInsuranceFlag "
			+ " from ljapay ljap ,ljagetendorse lja "
			+ " where 1=1  "
			+ " and ljap.incomeno = lja.endorsementno  "
			+ " and ljap.incometype ='3' "//团单保全收费
			+ " and ljap.sumactupaymoney =0 "
			+ " and (lja.grpcontno is not null and lja.grpcontno<>'00000000000000000000') "
			+ " and lja.feeoperationtype not in ('YS','YEI','YEO') "//预收保全收费不拆分
			+ " and exists (select 1 from ljagetendorse where endorsementno=lja.endorsementno and actugetno=lja.actugetno and feeoperationtype='YEO') "
			+ " and not exists (select busino from LYPremSeparateDetail where busino=(trim(ljap.payno)||trim(lja.polno)||trim(lja.feeoperationtype)||trim(lja.feefinatype)))"
			+ " and not exists (select busino from LYPremSeparateDetail where busino=trim(ljap.payno)||trim(lja.riskcode)||trim(lja.feeoperationtype)||trim(lja.feefinatype)) "//根据一期主键过滤
			+ tWherePart
			+ mWherePart
			+ " group by ljap.payno,ljap.incomeno,ljap.incometype,lja.riskcode,lja.managecom,lja.grpcontno,lja.feeoperationtype,lja.feefinatype,ljap.getnoticeno,lja.polno "
			+ " with ur";
		SSRS tSSRSNo7 = new ExeSQL().execSQL(tSQL);
		if(tSSRSNo7 != null && tSSRSNo7.getMaxRow() > 0){
			for (int i = 1; i <= tSSRSNo7.getMaxRow(); i++) {
				LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
				tLYPremSeparateDetailSchema.setBusiNo(tSSRSNo7.GetText(i, 1));
				tLYPremSeparateDetailSchema.setTempFeeNo(tSSRSNo7.GetText(i, 2));
				tLYPremSeparateDetailSchema.setTempFeeType(tSSRSNo7.GetText(i, 3));
				tLYPremSeparateDetailSchema.setOtherNo(tSSRSNo7.GetText(i, 4));
				tLYPremSeparateDetailSchema.setOtherNoType(tSSRSNo7.GetText(i, 5));
				tLYPremSeparateDetailSchema.setPrtNo(tSSRSNo7.GetText(i, 6));
				tLYPremSeparateDetailSchema.setPolicyNo(tSSRSNo7.GetText(i, 7));
				tLYPremSeparateDetailSchema.setRiskCode(tSSRSNo7.GetText(i, 8));
				tLYPremSeparateDetailSchema.setManageCom(tSSRSNo7.GetText(i, 9));
				tLYPremSeparateDetailSchema.setPayMoney(tSSRSNo7.GetText(i, 10));
				tLYPremSeparateDetailSchema.setState("01");//数据提取状态
				tLYPremSeparateDetailSchema.setOtherState("01");//暂收提取的数据
				tLYPremSeparateDetailSchema.setMoneyType("01");//收付费类型:01-收费；02-付费
				tLYPremSeparateDetailSchema.setFeeOperationType(tSSRSNo7.GetText(i, 11));
				tLYPremSeparateDetailSchema.setFeeFinaType(tSSRSNo7.GetText(i, 12));
				tLYPremSeparateDetailSchema.setMoneyNo(tSSRSNo7.GetText(i, 13));
				//共保标识
				String coInsuranceFlag = tSSRSNo7.GetText(i, 14);
				if("1".equals(coInsuranceFlag)){
					tLYPremSeparateDetailSchema.setOtherState("09");
				}
				tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
				tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
				String BusiType="01";//默认为保费
				BusiType = mYGZ.getBusinType(tLYPremSeparateDetailSchema.getFeeOperationType(), tLYPremSeparateDetailSchema.getFeeFinaType(), tLYPremSeparateDetailSchema.getRiskCode());
				tLYPremSeparateDetailSchema.setBusiType(BusiType);
				
				tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
			}
		}
		System.out.println("保全暂收数据提取：团单保全收费完全从投保人账户抵扣："+tSSRSNo7.getMaxRow());
		
		System.out.println("保全暂收数据提取完成，共"+tLYPremSeparateDetailSet.size()+" 条");
			
//		mMap.put(tLYPremSeparateDetailSet, SysConst.INSERT);
		mapsubmit(tLYPremSeparateDetailSet);	

		
		return true;
	}
	
	/**
	 * 数据分批提交
	 * @param aPrtNo
	 * @param aRiskCode
	 * @return
	 */
	private boolean mapsubmit(LYPremSeparateDetailSet tLYPremSeparateDetailSet){
		int count=0;
		LYPremSeparateDetailSet tDetailSet = new LYPremSeparateDetailSet();
		if(null == tLYPremSeparateDetailSet
				|| tLYPremSeparateDetailSet.size() < 1){
			System.out.println("===没有需要价税分离的数据！===");
		}
		for(int i = 1; i <= tLYPremSeparateDetailSet.size(); i++){
			LYPremSeparateDetailSchema detailSchema=tLYPremSeparateDetailSet.get(i).getSchema();
			detailSchema.setModifyDate(PubFun.getCurrentDate());
			detailSchema.setModifyTime(PubFun.getCurrentTime());
			tDetailSet.add(detailSchema);
			count ++;
			if(count >= sysCount || i == tLYPremSeparateDetailSet.size()){
				count = 0;
				mMap=new MMap();
				mMap.put(tDetailSet, SysConst.INSERT);
				tDetailSet=new LYPremSeparateDetailSet();
				if (!prepareOutputData()) {
					return false;
				}

				// 保存数据
				PubSubmit tPubSubmit = new PubSubmit();

				if (!tPubSubmit.submitData(mInputData, mOperate)) {
					System.out.print("保存数据返回");
					// @@错误处理
					this.mErrors.copyAllErrors(tPubSubmit.mErrors);
					CError tError = new CError();
					tError.moduleName = "GetTempFeeSeparateBL";
					tError.functionName = "submitData";
					tError.errorMessage = "数据提交失败!";
					this.mErrors.addOneError(tError);
//					return false;
				}
				mInputData = null;
			}
		}
		System.out.println("结束！！！！");
		return true;
	}
	
	
	
	
	
	
	/**
	 * 准备后台的数据
	 * @return boolean
	 */
	private boolean prepareOutputData() {
		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GetBqTempFeeSeparateBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
	
	
	public static void main(String[] args) {
		GetBqTempFeeBL tGetTempFeeSeparateBL = new GetBqTempFeeBL();
		VData cInputData = new VData();
		TransferData tTransferData = new TransferData();
		String tStartDate = "2017-01-03";
		String tEndDate = "2017-01-03";
		tTransferData.setNameAndValue("StartDate", tStartDate);
		tTransferData.setNameAndValue("EndDate", tEndDate);
		cInputData.add(tTransferData);
		tGetTempFeeSeparateBL.submitData(cInputData, "");
	}
}
