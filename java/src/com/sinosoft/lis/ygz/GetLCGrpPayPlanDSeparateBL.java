package com.sinosoft.lis.ygz;

import java.util.ArrayList;
import java.util.List;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCGrpPayDueDetailSchema;
import com.sinosoft.lis.schema.LCGrpPayPlanDetailSchema;
import com.sinosoft.lis.schema.LJAPayPersonSchema;
import com.sinosoft.lis.vschema.LCGrpPayDueDetailSet;
import com.sinosoft.lis.vschema.LCGrpPayPlanDetailSet;
import com.sinosoft.lis.vschema.LYPremSeparateDetailSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class GetLCGrpPayPlanDSeparateBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	/** 数据操作字符串 */
	private String mOperate;
	/** 开始日期 */
	private String mStartDate;
	/** 结束日期 */
	private String mEndDate;
	/** 印刷号*/
	private String mPrtNo;

	// 业务处理相关变量
	MMap mMap = new MMap();
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();
	private String mCurrentDate = PubFun.getCurrentDate();
	private String mCurrentTime = PubFun.getCurrentTime();

	public boolean submitData(VData cInputData, String cOperate) {
		System.out.println(mCurrentDate+":开始提取LCGrpPayPlanDetail价税分离数据。");
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		if (!dealData()) {
			// @@错误处理
			buildError("submitData", "数据处理失败GetLCGrpPayPlanDSeparateBl-->dealData!");
			return false;
		}
		// 准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}

//		// 保存数据
//		PubSubmit tPubSubmit = new PubSubmit();
//		if (!tPubSubmit.submitData(mInputData, mOperate)) {
//			// @@错误处理
//			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
//			CError tError = new CError();
//			tError.moduleName = "GetLCGrpPayPlanDSeparateBl";
//			tError.functionName = "submitData";
//			tError.errorMessage = "数据提交失败!";
//			this.mErrors.addOneError(tError);
//			return false;
//		}
		mInputData = null;
		System.out.println(mCurrentDate+":提取LCGrpPayPlanDetail价税分离数据结束。");
		return true;
	}

	private boolean getInputData(VData cInputData) {
		TransferData tTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
		mStartDate = (String) tTransferData.getValueByName("StartDate");
		mEndDate = (String) tTransferData.getValueByName("EndDate");
		mPrtNo = (String) tTransferData.getValueByName("PrtNo");
		mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
    	if(mGlobalInput == null){
    		buildError("getInputData","处理超时，请重新登录。");
    		return false;
    	}
		return true;
	}

	private boolean dealData() {
		LYPremSeparateDetailSet tLyPremSeparateDetailSet = new LYPremSeparateDetailSet();
		ExeSQL tExeSQL = new ExeSQL();
		String tWhereSQL = "";
		String tWhereTSQL = "";
		if(mStartDate != null && !"".equals(mStartDate)){
			tWhereSQL += " and makedate >= '" + mStartDate + "'";
		}
		if(mEndDate != null && !"".equals(mEndDate)){
			tWhereSQL += " and makedate <= '" + mEndDate + "'";
		}
		if("".equals(tWhereSQL)){
			tWhereSQL += "and makedate >= current date - 7 day ";
		}	
		if(mPrtNo != null && !"".equals(mPrtNo)){
			tWhereTSQL += " and PrtNo = '" + mPrtNo + "'";
		}
		/*缴费计划明细表*/
		String tSQL = " select *"
				+ " from LCGrpPayPlanDetail as lcG"
				+ " where (lcG.MoneyNoTax is null or lcG.MoneyNoTax = '') "
				+ " and (lcG.MoneyTax is null or lcG.MoneyTax = '') " 
				+ tWhereSQL
				+ tWhereTSQL
				+ " union "
				+ " select *"
				+ " from LCGrpPayPlanDetail as lcG"
				+ " where ( lcG.MoneyNoTax is not null and lcG.MoneyNoTax <> '') "
				+ " and abs(double(moneynotax)+double(moneytax)-prem)>0.01 " 
				+ tWhereSQL
				+ tWhereTSQL
				+ " with ur";
		YGZ tYGZ = new YGZ();
		ExeSQL exeSQL = new ExeSQL();
		RSWrapper rsWrapper = new RSWrapper();
		LCGrpPayPlanDetailSet tLCGrpPayPlanDetailSet = new LCGrpPayPlanDetailSet();
		if (!rsWrapper.prepareData(tLCGrpPayPlanDetailSet, tSQL)) {
			System.out.println("价税分离明细LCGrpPayPlanDetail数据准备失败！");
			return false;
		}
		do {
			rsWrapper.getData();
			if (null == tLCGrpPayPlanDetailSet
					|| tLCGrpPayPlanDetailSet.size() < 1) {
				break;
			}
			LYPremSeparateDetailSet tLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
			for(int i=1; i<=tLCGrpPayPlanDetailSet.size(); i++){
				PremSeparateDetailHX tPremSeparateDetailHXSchema = new PremSeparateDetailHX();
				String busino ="";
				String policyno = "";
				String tBusiType = "";
				LCGrpPayPlanDetailSchema tLCGrpPayPlanDetailSchema = tLCGrpPayPlanDetailSet.get(i);
				//根据schema获取主键字段名信息
				String[] schemaPK = tLCGrpPayPlanDetailSchema.getPK();
				//将主键的值存储到集合中
				List pklist =new ArrayList();
				for(int k=0; k<schemaPK.length; k++){
					pklist.add(tLCGrpPayPlanDetailSchema.getV(schemaPK[k]));
					busino +=tLCGrpPayPlanDetailSchema.getV(schemaPK[k]);
				}
				tPremSeparateDetailHXSchema.setpKList(pklist);
				System.out.println("busino="+busino);
				if(busino.length()>=20){
					busino = busino.substring(0, 20);
				}
				tPremSeparateDetailHXSchema.setBusiNo(busino);
				
			  	String txString ="select GrpContNo from LCGrpCont where prtno = '"+ tLCGrpPayPlanDetailSchema.getPrtNo()+"' union  select GrpContNo from LBGrpCont where prtno = '"+ tLCGrpPayPlanDetailSchema.getPrtNo()+"'with ur " ;
			  	policyno = exeSQL.getOneValue(txString);
				tPremSeparateDetailHXSchema.setPolicyNo(policyno);
				tPremSeparateDetailHXSchema.setPrtNo("000000");
				tPremSeparateDetailHXSchema.setRiskCode(tLCGrpPayPlanDetailSchema.getRiskCode());
			  	String txString2 ="select managecom from LCGrpCont where prtno = '"+ tLCGrpPayPlanDetailSchema.getPrtNo()+"' union select managecom from LBGrpCont where prtno = '"+ tLCGrpPayPlanDetailSchema.getPrtNo()+"' with ur " ;
			   	String tManageCom = exeSQL.getOneValue(txString2);
			   	if(tManageCom != null ){
				tPremSeparateDetailHXSchema.setManageCom(tManageCom);
			   	}
				tPremSeparateDetailHXSchema.setOtherState("23");
				tPremSeparateDetailHXSchema.setPayMoney("" +tLCGrpPayPlanDetailSchema.getPrem());
				String sBusiType = tYGZ.getBusinType(null, "BF", tLCGrpPayPlanDetailSchema.getRiskCode());
				tPremSeparateDetailHXSchema.setBusiType(sBusiType);
				tLyPremSeparateDetailSet.add(tPremSeparateDetailHXSchema);		
			}		
	
		if (tLyPremSeparateDetailSet != null && tLyPremSeparateDetailSet.size() > 0) {

			PremSeparateBL tPremSeparateBL = new PremSeparateBL();
			VData tVData = new VData();
			tVData.add(mGlobalInput);
			tVData.add(tLyPremSeparateDetailSet);
			if (!tPremSeparateBL.getSubmit(tVData, "")) {
				this.mErrors.copyAllErrors(tPremSeparateBL.mErrors);
				buildError("dealData", "调用PremSeparateBL接口失败。");
				return false;
			}

			MMap tMMap = new MMap();
	    	tMMap = tPremSeparateBL.getResult();
	    	
	    	//将价税分离接口返回的数据封装进MMap。
	    	LYPremSeparateDetailSet mLYPremSeparateDetailSet = (LYPremSeparateDetailSet) tMMap.getObjectByObjectName("LYPremSeparateDetailSet", 0);
	    	for(int i = 1; i <= mLYPremSeparateDetailSet.size();i++){
	    		PremSeparateDetailHX mPremSeparateDetailHXSchema = (PremSeparateDetailHX) mLYPremSeparateDetailSet.get(i);
	    		if(mPremSeparateDetailHXSchema.getMoneyNoTax()!=null && !"".equals(mPremSeparateDetailHXSchema.getMoneyNoTax())){
		    		String moneyNoTax = mPremSeparateDetailHXSchema.getMoneyNoTax();
		    		String moneyTax = mPremSeparateDetailHXSchema.getMoneyTax();
		    		String busiType = mPremSeparateDetailHXSchema.getBusiType();
		    		String taxRate = mPremSeparateDetailHXSchema.getTaxRate();//税率
		    		String whereSQL = " 1=1 ";
		    		LCGrpPayPlanDetailSchema tempLCGrpPayPlanDetailSchema = new LCGrpPayPlanDetailSchema();
		    		//从表的schema中获取主键字段
		    		String[] tempPk = tempLCGrpPayPlanDetailSchema.getPK();
		    		//根据主键的值拼接where条件
		    		List pkList = mPremSeparateDetailHXSchema.getpKList();
		    		for(int k=0; k< tempPk.length; k++){
		    			whereSQL += " and "+tempPk[k]+" = '"+pkList.get(k)+"' ";
		    		}
		    		//lzy 为保险起见加个校验
		    		if(" 1=1 ".equals(whereSQL)){
		    			return false;
		    		}
						String sql = "update LCGrpPayPlanDetail set MoneyNotax = '"+ moneyNoTax+ "',"
								+ " Moneytax = '"+ moneyTax+ "',"
								+ " ModifyDate = '"+ mCurrentDate+ "',"
								+ " ModifyTime = '"+ mCurrentTime+ "',"
								+ " BusiType = '"+ busiType+ "',"
								+ " TaxRate = '" + taxRate +"'"
								+ " where "+ whereSQL;
						mMap.put(sql, SysConst.UPDATE);
					}
				}
			}
		
		//在这里提交
		if(!submit()){
			System.out.println("数据执行失败，运行下一批次");
		}
		
	}while (null != tLCGrpPayPlanDetailSet
			&& tLCGrpPayPlanDetailSet.size() > 0);
	
	return true;
 }		
		
	private boolean prepareOutputData() {

		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			buildError("prepareData", "在准备往后层处理所需要的数据时出错。");
			return false;
		}
		return true;
	}

	private void buildError(String sfunName, String serromes) {
		CError tError = new CError();
		tError.moduleName = "GetLCGrpPayPlanDSeparateBl";
		tError.functionName = sfunName;
		tError.errorMessage = serromes;
		this.mErrors.addOneError(tError);
	}

	  /**
	 * 
	 * @return boolean
	 */
	private boolean submit() {
		try {
			VData tData = new VData();
			tData.add(mMap);
			PubSubmit tPubSubmit = new PubSubmit();
			if (!tPubSubmit.submitData(tData, "")) {
				mMap =new MMap();
				System.out.print("保存数据返回");
				// @@错误处理
				this.mErrors.copyAllErrors(tPubSubmit.mErrors);
				CError tError = new CError();
				tError.moduleName = "GetLCGrpPayPlanDSeparateBL";
				tError.functionName = "submitData";
				tError.errorMessage = "数据提交失败!";
				this.mErrors.addOneError(tError);
				return false;
			}
		} catch (Exception ex) {
			mMap =new MMap();
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GetLCGrpPayPlanDSeparateBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		mMap =new MMap();
		return true;
	}

	
	
	public static void main(String[] args) {
		GetLCGrpPayPlanDSeparateBL tGetLCGrpPayPlanDSeparateBl = new GetLCGrpPayPlanDSeparateBL();
		VData cInputData = new VData();
		TransferData tTransferData = new TransferData();
		String tStartDate = "2017-03-01";
		String tEndDate = "2017-03-01";
		String tPrtNo = "";
		tTransferData.setNameAndValue("StartDate", tStartDate);
		tTransferData.setNameAndValue("EndDate", tEndDate);
		tTransferData.setNameAndValue("PrtNo", tPrtNo);
		GlobalInput mGlobalInput = new GlobalInput();
		cInputData.add(tTransferData);
		cInputData.add(mGlobalInput);
		tGetLCGrpPayPlanDSeparateBl.submitData(cInputData, "");
	}

}
