package com.sinosoft.lis.ygz;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class GetPayPGInvoiceUI {

	/** �������� */
    public CErrors mErrors = new CErrors();
    
    public GetPayPGInvoiceUI(){}
    
    public boolean submitData(VData cInputData, String cOperate){
    	
    	try{
    		GetPayPGInvoiceBL tGetPayPGInvoiceBL = new GetPayPGInvoiceBL();
            if (!tGetPayPGInvoiceBL.submitData(cInputData, cOperate)){
                this.mErrors.copyAllErrors(tGetPayPGInvoiceBL.mErrors);
                return false;
            }
            return true;
        }catch (Exception e){
            e.printStackTrace();
            this.mErrors.addOneError(e.getMessage());
            return false;
        }
    }
}
