package com.sinosoft.lis.ygz;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.vschema.LYOutPayDetailSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.OperateOracle;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * <p>Title: 营改增，开票接口处理类</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company: Sinosoft</p>
 * @author Alex
 * @version 1.0
 */

public class InvoicePushBL {

	private GlobalInput tGI= new GlobalInput();

	private LYOutPayDetailSet tLYOutPayDetailSet = null;
	private SSRS mSSRS = null;

	public CErrors mErrors = new CErrors();

	private String operator ;

	private int sysCount = 1000;//默认每一千条数据发送一次请求

	private MMap resultMMap = new MMap();

	private String oBatchNo = "" ;

	public InvoicePushBL() {

	}


	/**
	 * 接收LYOutPayDetail数据进行价税分离
	 * 数据量最好不要超过1000条
	 * @param tVData
	 * @param operator 可为空""
	 * @return
	 */
	public boolean getSubmit(VData tVData,String operator){

		if(!getInputData(tVData)){
			return false;
		}

		if(!dealData()){
			return false;
		}

		return true;
	}

	/**
	 * 获取数据
	 * @param mInputData
	 * @return
	 */
	private boolean getInputData(VData mInputData) {
		tGI = ((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));

		tLYOutPayDetailSet = ((LYOutPayDetailSet) mInputData.getObjectByObjectName("LYOutPayDetailSet",0));
		mSSRS = (SSRS) mInputData.getObjectByObjectName("SSRS",0);

		if(null == tLYOutPayDetailSet || tLYOutPayDetailSet.size()==0){
			CError tError = new CError();
			tError.moduleName = "OutPayUploadBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "没有接收到保单信息!";
			this.mErrors.addOneError(tError);
		}

		if (tGI == null) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "OutPayUploadBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "没有得到足够的数据，请您确认!";
			this.mErrors.addOneError(tError);

			return false;
		}
		operator = tGI.Operator;

		return true;
	}


	/**
	 * 封装数据调用发票分离接口
	 * @return
	 */
	private boolean dealData(){

		try{
			OperateOracle operateOracle = new OperateOracle();
			operateOracle.AddData(tLYOutPayDetailSet,mSSRS);
			resultMMap = operateOracle.getResult();
		}catch(Exception e){
			e.printStackTrace();
		}
		return true;
	}


	public MMap getResult(){
		return resultMMap;
	}


}
