package com.sinosoft.lis.ygz;

import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LYPremSeparateDetailSchema;
import com.sinosoft.lis.vschema.LYPremSeparateDetailSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class GetYjGLMoneyBL {

	/**
	 * @param args
	 */
	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	/** 数据操作字符串 */
	private String mOperate;
	
	/** 开始日期*/
	private String mStartDate;
	/** 结束日期*/
	private String mEndDate;
	
	private String tWhereSQL;
	//业务处理相关变量
	private MMap mMap = new MMap();
	
	private LYPremSeparateDetailSet mLYPremSeparateDetailSet;
	
	private String mCurrentDate = PubFun.getCurrentDate();
	
	private String mCurrentTime = PubFun.getCurrentTime();
	
	public boolean submitData(VData cInputData, String cOperate){
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}
		
		// 进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GetTempFeeSeparateBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败GetTempFeeSeparateBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}
		
		//准备往后台的数据
		if(!prepareOutputData()){
			
			return false;
		}
		// 保存数据
		PubSubmit tPubSubmit = new PubSubmit();

		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			System.out.print("保存数据返回");
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "GetYjMoneyBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		
		return true;
	}
	
	// 获得输出数据
	private boolean getInputData(VData cInputData) {
		TransferData tTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
	    mStartDate = (String) tTransferData.getValueByName("StartDate");
		mEndDate = (String) tTransferData.getValueByName("EndDate");
		return true;
	}
	
	//
	private boolean dealData(){
		LYPremSeparateDetailSet tLYPremSeparateDetailSet=new LYPremSeparateDetailSet();
		YGZ tYGZ=new YGZ();
		String tWhereSQL="";
		if(mStartDate != null && !"".equals(mStartDate)){
			tWhereSQL += " and lc.makedate >= '" + mStartDate + "'";
		}
		if(mEndDate != null && !"".equals(mEndDate)){
			tWhereSQL += " and lc.makedate <= '" + mEndDate + "'";
		}
		if("".equals(tWhereSQL)){
			tWhereSQL += "and lc.makedate >= current date - 3 days  ";
		}

		//个险万能保全产生的管理费  
		String yjSQL1="select "
					+" lc.serialno busino,lc.polno tempfeeno,lc.otherno otherno,lc.othertype othernotype, "
					+" (select prtno from lccont where contno = lc.contno ) PrtNo,  "
					+" lc.contno policyno,lc.riskcode riskcode,lc.managecom managecom,lc.fee paymoney," 
					+" lc.moneytype feefinatype,(select edortype from lpedoritem where edorno=lc.otherno) feeoperationtype "
					+" from lcinsureaccfeetrace  lc "
					+" where lc.moneytype in ('MF','LQ')  "
					+" and lc.othertype='10'  "
					+" and lc.fee <>0 "
					+tWhereSQL
					+" and exists (select 1 from lpedoritem where edorno=lc.otherno) "
					+" and (lc.grpcontno ='00000000000000000000' or lc.grpcontno is null) "
					+" and not exists (select busino from LYPremSeparateDetail where busino=lc.serialno and policyno=lc.contno)  " 
					+" union "
					+"select "
					+" lc.serialno busino,lc.polno tempfeeno,lc.otherno otherno,lc.othertype othernotype, "
					+" ( select prtno from lbcont where contno = lc.contno) PrtNo,  "
					+" lc.contno policyno,lc.riskcode riskcode,lc.managecom managecom,lc.fee paymoney,"
					+" lc.moneytype feefinatype,(select edortype from lpedoritem where edorno=lc.otherno) feeoperationtype" 
					+" from lbinsureaccfeetrace  lc "
					+" where lc.moneytype in ('MF','LQ','GL','TM') "
					+" and lc.othertype='10'  "
					+" and lc.fee <>0 "	
					+tWhereSQL
					+" and exists (select 1 from lpedoritem where edorno=lc.otherno) "
					+" and (lc.grpcontno ='00000000000000000000' or lc.grpcontno is null) "
					+" and not exists (select busino from LYPremSeparateDetail where busino=lc.serialno and policyno=lc.contno) " 
					+" with ur";
		
		SSRS tSSRS1 = new ExeSQL().execSQL(yjSQL1);
		if(tSSRS1!=null && tSSRS1.getMaxRow()>0){
			for (int i=1;i<=tSSRS1.getMaxRow();i++){
				LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
				tLYPremSeparateDetailSchema.setBusiNo(tSSRS1.GetText(i, 1));
				tLYPremSeparateDetailSchema.setTempFeeNo(tSSRS1.GetText(i, 2));
				tLYPremSeparateDetailSchema.setOtherNo(tSSRS1.GetText(i, 3));
				tLYPremSeparateDetailSchema.setOtherNoType(tSSRS1.GetText(i, 4));
				tLYPremSeparateDetailSchema.setPrtNo(tSSRS1.GetText(i, 5));
				tLYPremSeparateDetailSchema.setPolicyNo(tSSRS1.GetText(i, 6));
				tLYPremSeparateDetailSchema.setRiskCode(tSSRS1.GetText(i, 7));
				tLYPremSeparateDetailSchema.setManageCom(tSSRS1.GetText(i, 8));
				tLYPremSeparateDetailSchema.setPayMoney(tSSRS1.GetText(i, 9));
				tLYPremSeparateDetailSchema.setFeeFinaType(tSSRS1.GetText(i, 10));	
				tLYPremSeparateDetailSchema.setFeeOperationType(tSSRS1.GetText(i, 11));

				String tBusiType=tYGZ.getBusinType(tSSRS1.GetText(i, 11), tSSRS1.GetText(i,10), tSSRS1.GetText(i, 7));
				if("LQ".equals(tSSRS1.GetText(i, 10))){
					tBusiType=tYGZ.getBusinType(tSSRS1.GetText(i, 11), "MF",  tSSRS1.GetText(i, 7));
				}
				tLYPremSeparateDetailSchema.setBusiType(tBusiType);
				
				tLYPremSeparateDetailSchema.setState("01");//数据提取状态
				tLYPremSeparateDetailSchema.setMoneyType("01");//收付费类型:01-收费；02-付费
				tLYPremSeparateDetailSchema.setOtherState("13");
				tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
				tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);

				tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
			}
		}
		
		//团险万能保全生成的管理费
		String yjSQL2="select "
			+" lc.serialno busino,lc.grppolno  tempfeeno,lc.otherno otherno,lc.othertype othernotype, "
			+" (select prtno from lcgrpcont where grpcontno = lc.grpcontno ) PrtNo, "
			+" lc.grpcontno policyno,lc.riskcode riskcode,lc.managecom managecom,lc.fee paymoney," 
			+" lc.moneytype feefinatype,(select edortype from lpgrpedoritem where edorno=lc.otherno) feeoperationtype "
			+" from lcinsureaccfeetrace  lc "
			+" where lc.moneytype='MF' "
			+" and lc.othertype ='3' "
			+" and lc.fee <>0 "
			+tWhereSQL
			+" and exists (select 1 from lpgrpedoritem where edorno=lc.otherno) "
			+" and not exists (select busino from LYPremSeparateDetail where busino=lc.serialno and policyno=lc.grpcontno) "
			+" union " 
			+" select "
			+" lc.serialno busino,lc.grppolno tempfeeno,lc.otherno otherno,lc.othertype othernotype, "
			+" (select prtno from lbgrpcont where grpcontno = lc.grpcontno) PrtNo, "
			+" lc.grpcontno policyno,lc.riskcode riskcode,lc.managecom managecom,lc.fee paymoney,"
			+" lc.moneytype feefinatype,(select edortype from lpgrpedoritem where edorno=lc.otherno) feeoperationtype "
			+" from lbinsureaccfeetrace  lc "
			+" where lc.moneytype in ('MF','GL','TM') "
			+" and lc.othertype ='3' "
			+" and lc.fee <>0 "
			+tWhereSQL
			+" and exists (select 1 from lpgrpedoritem where edorno=lc.otherno) "
			+" and not exists (select busino from LYPremSeparateDetail where busino=lc.serialno and policyno=lc.grpcontno) "
			+" with ur";
		SSRS tSSRS2 = new ExeSQL().execSQL(yjSQL2);
		if(tSSRS2!=null && tSSRS2.getMaxRow()>0){
			for (int i=1;i<=tSSRS2.getMaxRow();i++){
				LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
				tLYPremSeparateDetailSchema.setBusiNo(tSSRS2.GetText(i, 1));
				tLYPremSeparateDetailSchema.setTempFeeNo(tSSRS2.GetText(i, 2));
				tLYPremSeparateDetailSchema.setOtherNo(tSSRS2.GetText(i, 3));
				tLYPremSeparateDetailSchema.setOtherNoType(tSSRS2.GetText(i, 4));
				tLYPremSeparateDetailSchema.setPrtNo(tSSRS2.GetText(i, 5));
				tLYPremSeparateDetailSchema.setPolicyNo(tSSRS2.GetText(i, 6));
				tLYPremSeparateDetailSchema.setRiskCode(tSSRS2.GetText(i, 7));
				tLYPremSeparateDetailSchema.setManageCom(tSSRS2.GetText(i, 8));
				tLYPremSeparateDetailSchema.setPayMoney(tSSRS2.GetText(i, 9));
				tLYPremSeparateDetailSchema.setFeeFinaType(tSSRS2.GetText(i, 10));
				tLYPremSeparateDetailSchema.setFeeOperationType(tSSRS2.GetText(i, 11));
				
				String tBusiType=tYGZ.getBusinType(tSSRS2.GetText(i, 11), tSSRS2.GetText(i, 10), tSSRS2.GetText(i, 7));
				tLYPremSeparateDetailSchema.setBusiType(tBusiType);
				
				tLYPremSeparateDetailSchema.setState("01");//数据提取状态
				tLYPremSeparateDetailSchema.setMoneyType("01");//收付费类型:01-收费；02-付费
				tLYPremSeparateDetailSchema.setOtherState("13");
				tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
				tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);

				tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
			}
		}
		
//		团险万能保全在lcinsureaccclassfee中生成的部分领取的管理费
		String yjSQL5="select "
			+" trim(lc.polno)||trim(lc.otherno) busino,lc.grppolno  tempfeeno,lc.otherno otherno,lc.othertype othernotype, "
			+" (select prtno from lcgrpcont where grpcontno = lc.grpcontno ) PrtNo, "
			+" lc.grpcontno policyno,lc.riskcode riskcode,lc.managecom managecom,lc.fee paymoney," 
			+" 'MF' feefinatype,lp.edortype feeoperationtype "
			+" from lcinsureaccclassfee lc,lpgrpedoritem lp "
			+" where lc.otherno=lp.edorno  "
			+" and lc.othertype ='3' "
			+" and lp.edortype='LQ' "
			+" and lc.fee <>0 "
			+tWhereSQL
			+" and not exists (select busino from LYPremSeparateDetail where busino=trim(lc.polno)||trim(lc.otherno) and policyno=lc.grpcontno) "
			+" union " 
			+" select "
			+" trim(lc.polno)||trim(lc.otherno) busino,lc.grppolno tempfeeno,lc.otherno otherno,lc.othertype othernotype, "
			+" (select prtno from lbgrpcont where grpcontno = lc.grpcontno) PrtNo, "
			+" lc.grpcontno policyno,lc.riskcode riskcode,lc.managecom managecom,lc.fee paymoney,"
			+" 'MF' feefinatype,lp.edortype  feeoperationtype "
			+" from lbinsureaccclassfee lc,lpgrpedoritem lp "
			+" where lc.otherno=lp.edorno "
			+" and lc.othertype ='3' "
			+" and lp.edortype='LQ' "
			+" and lc.fee <>0 "
			+tWhereSQL
			+" and not exists (select busino from LYPremSeparateDetail where busino=trim(lc.polno)||trim(lc.otherno) and policyno=lc.grpcontno) "
			+" with ur";
		SSRS tSSRS5 = new ExeSQL().execSQL(yjSQL5);
		if(tSSRS5!=null && tSSRS5.getMaxRow()>0){
			for (int i=1;i<=tSSRS5.getMaxRow();i++){
				LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
				tLYPremSeparateDetailSchema.setBusiNo(tSSRS5.GetText(i, 1));
				tLYPremSeparateDetailSchema.setTempFeeNo(tSSRS5.GetText(i, 2));
				tLYPremSeparateDetailSchema.setOtherNo(tSSRS5.GetText(i, 3));
				tLYPremSeparateDetailSchema.setOtherNoType(tSSRS5.GetText(i, 4));
				tLYPremSeparateDetailSchema.setPrtNo(tSSRS5.GetText(i, 5));
				tLYPremSeparateDetailSchema.setPolicyNo(tSSRS5.GetText(i, 6));
				tLYPremSeparateDetailSchema.setRiskCode(tSSRS5.GetText(i, 7));
				tLYPremSeparateDetailSchema.setManageCom(tSSRS5.GetText(i, 8));
				tLYPremSeparateDetailSchema.setPayMoney(tSSRS5.GetText(i, 9));
				tLYPremSeparateDetailSchema.setFeeFinaType(tSSRS5.GetText(i, 10));
				tLYPremSeparateDetailSchema.setFeeOperationType(tSSRS5.GetText(i, 11));
				
				String tBusiType=tYGZ.getBusinType(tSSRS5.GetText(i, 11), tSSRS5.GetText(i, 10), tSSRS5.GetText(i, 7));
				tLYPremSeparateDetailSchema.setBusiType(tBusiType);
				
				tLYPremSeparateDetailSchema.setState("01");//数据提取状态
				tLYPremSeparateDetailSchema.setMoneyType("01");//收付费类型:01-收费；02-付费
				tLYPremSeparateDetailSchema.setOtherState("13");
				tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
				tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);

				tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
			}
		}
		
		//个险万能月结账户管理费
		String yjSQL3="select "
			+" lc.serialno busino,lc.polno tempfeeno,lc.otherno otherno,lc.othertype othernotype, "
			+" (select prtno from lccont where contno = lc.contno) PrtNo, "
			+" lc.contno policyno,lc.riskcode riskcode,lc.managecom managecom,lc.money paymoney,lc.moneytype feefinatype "
			+" from lcinsureacctrace  lc "
			+" where lc.moneytype='MF' "
			+" and lc.othertype ='6' "
			+" and lc.money <>0 "
			+" and lc.state <>'temp' "
			+tWhereSQL
			+" and (lc.grpcontno ='00000000000000000000' or lc.grpcontno is null) "
			+" and not exists (select busino from LYPremSeparateDetail where busino=lc.serialno and policyno=lc.contno) "
			+" union "
			+" select "
			+" lc.serialno busino,lc.polno tempfeeno,lc.otherno otherno,lc.othertype othernotype, "
			+" (select prtno from lbcont where contno = lc.contno) PrtNo, "
			+" lc.contno policyno,lc.riskcode riskcode,lc.managecom managecom,lc.money paymoney,lc.moneytype feefinatype "
			+" from lbinsureacctrace  lc "
			+" where lc.moneytype='MF' "
			+" and lc.othertype ='6' "
			+" and lc.money <>0 "
			+" and lc.state <>'temp' "
			+tWhereSQL
			+" and (lc.grpcontno ='00000000000000000000' or lc.grpcontno is null) "
			+" and not exists (select busino from LYPremSeparateDetail where busino=lc.serialno and policyno=lc.contno) "
//			+" fetch first 10000 rows only " 
			+" with ur";
		SSRS tSSRS3 = new ExeSQL().execSQL(yjSQL3);
		if(tSSRS3!=null && tSSRS3.getMaxRow()>0){
			for (int i=1;i<=tSSRS3.getMaxRow();i++){
				LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
				tLYPremSeparateDetailSchema.setBusiNo(tSSRS3.GetText(i, 1));
				tLYPremSeparateDetailSchema.setTempFeeNo(tSSRS3.GetText(i, 2));
				tLYPremSeparateDetailSchema.setOtherNo(tSSRS3.GetText(i, 3));
				tLYPremSeparateDetailSchema.setOtherNoType(tSSRS3.GetText(i, 4));
				tLYPremSeparateDetailSchema.setPrtNo(tSSRS3.GetText(i, 5));
				tLYPremSeparateDetailSchema.setPolicyNo(tSSRS3.GetText(i, 6));
				tLYPremSeparateDetailSchema.setRiskCode(tSSRS3.GetText(i, 7));
				tLYPremSeparateDetailSchema.setManageCom(tSSRS3.GetText(i, 8));
				tLYPremSeparateDetailSchema.setPayMoney(tSSRS3.GetText(i, 9));
				tLYPremSeparateDetailSchema.setFeeFinaType(tSSRS3.GetText(i, 10));
				
				String tBusiType=tYGZ.getBusinType(null, tSSRS3.GetText(i, 10), tSSRS3.GetText(i, 7));
				tLYPremSeparateDetailSchema.setBusiType(tBusiType);
				
				tLYPremSeparateDetailSchema.setState("01");//数据提取状态
				tLYPremSeparateDetailSchema.setMoneyType("01");//收付费类型:01-收费；02-付费
				tLYPremSeparateDetailSchema.setOtherState("14");
				tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
				tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);

				tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
			}
		}
		//团险万能月结账户管理费
		String yjSQL4="select "
			+" lc.serialno busino,lc.grppolno tempfeeno,lc.otherno  otherno,lc.othertype othernotype, "
			+" (select prtno from lcgrpcont where grpcontno = lc.grpcontno) PrtNo, "
			+" lc.grpcontno policyno,lc.riskcode riskcode,lc.managecom managecom,lc.money paymoney,lc.moneytype feefinatype "
			+" from lcinsureacctrace  lc "
			+" where lc.moneytype='MF' "
			+" and lc.othertype ='6' "
			+" and lc.money <>0 "
			+" and lc.state <>'temp' "
			+tWhereSQL
			+" and exists (select 1 from lcgrpcont where grpcontno =lc.grpcontno) "
			+" and not exists (select busino from LYPremSeparateDetail where busino=lc.serialno and policyno=lc.grpcontno) "
			+" union "
			+" select "
			+" lc.serialno busino,lc.grppolno tempfeeno,lc.otherno otherno,lc.othertype othernotype, "
			+" (select prtno from lbgrpcont where grpcontno = lc.grpcontno) PrtNo, "
			+" lc.grpcontno policyno,lc.riskcode riskcode,lc.managecom managecom,lc.money paymoney,lc.moneytype feefinatype "
			+" from lbinsureacctrace  lc "
			+" where lc.moneytype='MF' "
			+" and lc.othertype ='6' "
 			+" and lc.money <>0 "
 			+" and lc.state <>'temp' "
			+tWhereSQL
			+" and  exists (select 1 from lbgrpcont where grpcontno =lc.grpcontno) "
			+" and not exists (select busino from LYPremSeparateDetail where busino=lc.serialno and policyno=lc.grpcontno) "
			+" with ur";
		SSRS tSSRS4 = new ExeSQL().execSQL(yjSQL4);
		if(tSSRS4!=null && tSSRS4.getMaxRow()>0){
			for (int i=1;i<=tSSRS4.getMaxRow();i++){
				LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
				tLYPremSeparateDetailSchema.setBusiNo(tSSRS4.GetText(i, 1));
				tLYPremSeparateDetailSchema.setTempFeeNo(tSSRS4.GetText(i, 2));
				tLYPremSeparateDetailSchema.setOtherNo(tSSRS4.GetText(i, 3));
				tLYPremSeparateDetailSchema.setOtherNoType(tSSRS4.GetText(i, 4));
				tLYPremSeparateDetailSchema.setPrtNo(tSSRS4.GetText(i, 5));
				tLYPremSeparateDetailSchema.setPolicyNo(tSSRS4.GetText(i, 6));
				tLYPremSeparateDetailSchema.setRiskCode(tSSRS4.GetText(i, 7));
				tLYPremSeparateDetailSchema.setManageCom(tSSRS4.GetText(i, 8));
				tLYPremSeparateDetailSchema.setPayMoney(tSSRS4.GetText(i, 9));
				tLYPremSeparateDetailSchema.setFeeFinaType(tSSRS4.GetText(i, 10));
				
				String tBusiType=tYGZ.getBusinType(null, tSSRS4.GetText(i, 10), tSSRS4.GetText(i, 7));
				tLYPremSeparateDetailSchema.setBusiType(tBusiType);
				
				tLYPremSeparateDetailSchema.setState("01");//数据提取状态
				tLYPremSeparateDetailSchema.setMoneyType("01");//收付费类型:01-收费；02-付费
				tLYPremSeparateDetailSchema.setOtherState("14");
				tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
				tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);

				tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
			}
		}

		mMap.put(tLYPremSeparateDetailSet, SysConst.INSERT);
		return true;
	}
	
	
	private boolean prepareOutputData(){
		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GetYjMoneyBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
	
	
	public static void main(String[] args) {
		GetYjGLMoneyBL tGetYjMoneyBL = new GetYjGLMoneyBL();
		VData cInputData = new VData();
		TransferData tTransferData = new TransferData();
		String tStartDate = "2016-5-1";
		String tEndDate = "2016-7-15";
		String tPrtNo = "";
		tTransferData.setNameAndValue("StartDate", tStartDate);
		tTransferData.setNameAndValue("EndDate", tEndDate);
		cInputData.add(tTransferData);
		tGetYjMoneyBL.submitData(cInputData, "");
	}

}
