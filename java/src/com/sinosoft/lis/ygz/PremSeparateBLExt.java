package com.sinosoft.lis.ygz;

import java.util.ArrayList;
import java.util.List;

import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.db.LYPremSeparateDetailDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LJTempFeeExtSchema;
import com.sinosoft.lis.vschema.LJTempFeeExtSet;
import com.sinosoft.lis.vschema.LYPremSeparateDetailSet;
import com.sinosoft.lis.ygz.obj.BillInfo;
import com.sinosoft.lis.ygz.obj.Content;
import com.sinosoft.lis.ygz.obj.SendResult;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

/**
 * <p>
 * Title:价税分离处理类
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author zxs
 * @version 1.0
 */

public class PremSeparateBLExt {

	private GlobalInput tGI = new GlobalInput();

	private LJTempFeeExtSet tLJTempFeeExtSet = null;

	public CErrors mErrors = new CErrors();

	private String operator;

	private int sysCount = 1000;// 默认每一千条数据发送一次请求

	private MMap resultMMap = new MMap();

	public PremSeparateBLExt() {

	}

	public static void main(String[] args) {
		PremSeparateBLExt tt = new PremSeparateBLExt();
		LYPremSeparateDetailDB tLYPremSeparateDetailDB = new LYPremSeparateDetailDB();
		LYPremSeparateDetailSet set = tLYPremSeparateDetailDB
				.executeQuery("select * from LYPremSeparateDetail where state = '01'");
		VData data = new VData();
		data.add(set);
		GlobalInput mgl = new GlobalInput();
		mgl.Operator = "ser";
		data.add(mgl);
		tt.getSubmit(data, "");
		MMap map = tt.getResult();
		VData tVData = new VData();
		tVData.add(map);

		PubSubmit tPubSubmit = new PubSubmit();
		tPubSubmit.submitData(tVData, "");

	}

	/**
	 * 接收LYPremSeparateDetail数据进行价税分离 数据量最好不要超过1000条
	 * 
	 * @param tVData
	 * @param operator
	 *            可为空""
	 * @return
	 */
	public boolean getSubmit(VData tVData, String operator) {

		if (!getInputData(tVData)) {
			return false;
		}

		if (!dealData()) {
			return false;
		}

		return true;
	}

	/**
	 * 获取数据
	 * 
	 * @param mInputData
	 * @return
	 */
	private boolean getInputData(VData mInputData) {
		tGI = ((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));

		tLJTempFeeExtSet = ((LJTempFeeExtSet) mInputData.getObjectByObjectName("LJTempFeeExtSet", 0));

		if (null == tLJTempFeeExtSet || tLJTempFeeExtSet.size() == 0) {
			CError tError = new CError();
			tError.moduleName = "PremSeparateBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "没有接收到保单信息!";
			this.mErrors.addOneError(tError);
		}

		if (tGI == null) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "PremSeparateBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "没有得到足够的数据，请您确认!";
			this.mErrors.addOneError(tError);

			return false;
		}
		operator = tGI.Operator;

		return true;
	}

	/**
	 * 封装数据调用价税分离接口
	 * 
	 * @return
	 */
	private boolean dealData() {
		LJTempFeeExtSet resLJTempFeeExtSet = new LJTempFeeExtSet();
		LJTempFeeExtSet mLJTempFeeExtSet = new LJTempFeeExtSet();
		int count = 0;
		// boolean isExist = false;
		for (int i = 1; i <= tLJTempFeeExtSet.size(); i++) {
			LJTempFeeExtSchema mLJTempFeeExtSchema = tLJTempFeeExtSet.get(i);
			mLJTempFeeExtSet.add(mLJTempFeeExtSchema);
			count++;
			if (count == sysCount || i == tLJTempFeeExtSet.size()) {
				count = 0;
				List<BillInfo> billInfoList = new ArrayList<BillInfo>();
				for (int j = 1; j <= mLJTempFeeExtSet.size(); j++) {
					LJTempFeeExtSchema ExtSchema = mLJTempFeeExtSet.get(j);
					BillInfo bill = new BillInfo();
					String sequenceNO = ExtSchema.getTempFeeNo() + ExtSchema.getRiskCode() + ExtSchema.getPayMode();
					bill.setSequenceNo(sequenceNO);
					// bill.setPk_jiashuifl(detailSchema.getBusiNo());
					bill.setId(sequenceNO);// 业务流水号
					bill.setBaodanno(ExtSchema.getOtherNo()); // ---------------------
					bill.setShouzhitype(ExtSchema.getBusiType());// 收支类型
					bill.setXianzhongcode(ExtSchema.getRiskCode());
					// 收支类型为09-工本费的 险种代码传 000000
					if ("09".equals(ExtSchema.getBusiType())) {
						bill.setXianzhongcode(BQ.FILLDATA);
					}
					String sql = "select prtno,ManageCom from lccont where contno = '" + ExtSchema.getOtherNo() + "'";
					SSRS ssrs = new ExeSQL().execSQL(sql);
					String managecom = "";
					String prtno = "";
					if (ssrs != null && ssrs.getMaxRow() > 0) {
						prtno = ssrs.GetText(1, 1);
						managecom = ssrs.GetText(1, 2);
					} else {
						// isExist = true;
						System.out.println("保单层没有该保单相关信息！");
						// break;
					}
					bill.setJigoucode(managecom);
					bill.setHanshuiamount(Double.toString(ExtSchema.getPayMoney()));// 含税金额
					bill.setDef2("01");
					bill.setDef3(prtno);
					String autoFlag = "Y";
					bill.setDef4(autoFlag);

					billInfoList.add(bill);
				}
				// if (isExist) {
				// break;
				// }
				// 调用接口进行价税分离
				PremSeparateInterface tPremSeparateInterface = new PremSeparateInterface();
				List<SendResult> resultList = tPremSeparateInterface.callInterface(billInfoList);

				// 对分离结果进行结息封装LJTempFeeExtSet数据
				for (int n = 0; n < resultList.size(); n++) {
					SendResult result = resultList.get(n);
					String resCode = result.getResultcode();
					String resDesc = result.getResultdescription();
					Content content = result.getContent();
					String resultID = content.getId();
					String bdocid = result.getBdocid();
					if (null == resultID) {
						resultID = bdocid;
					}

					System.out.println("busino=" + resultID);
					for (int k = 1; k <= mLJTempFeeExtSet.size(); k++) {
						LJTempFeeExtSchema ExtSchema = mLJTempFeeExtSet.get(k);
						String sequenceNO = ExtSchema.getTempFeeNo() + ExtSchema.getRiskCode() + ExtSchema.getPayMode();
						if (resultID.equals(sequenceNO)) {
							// 返回结果失败，保存失败原因
							if (!"1".equals(resCode) || null == resultID) {
								System.out.println(resCode + "返回结果失败:" + resDesc);
								if (null != resDesc && resDesc.length() > 200) {
									resDesc = resDesc.substring(0, 200);
								}
								// ExtSchema.setErrorInfo(resDesc + "");
								resLJTempFeeExtSet.add(ExtSchema);
								// 已经获取结果的remove掉
								mLJTempFeeExtSet.remove(mLJTempFeeExtSet.get(k));
								break;
							}
							ExtSchema.setTaxRate(content.getRate());
							ExtSchema.setMoneyNoTax(content.getWushuiamount());
							ExtSchema.setMoneyTax(content.getTaxes());

							// 实际没有得到价税分离结果的不改状态
							if (null == ExtSchema.getMoneyTax() && null == ExtSchema.getMoneyNoTax()) {
								// ExtSchema.setErrorInfo(resDesc + "");
								System.out.println(resultID + ",实际没有获取到价税分离数据");
							} else {
								ExtSchema.setState("02");
								// ExtSchema.setErrorInfo(null);
							}
							// detailSchema.setModifyDate(PubFun.getCurrentDate());
							// detailSchema.setModifyTime(PubFun.getCurrentTime());
							resLJTempFeeExtSet.add(ExtSchema);
							// 已经获取结果的remove掉
							mLJTempFeeExtSet.remove(mLJTempFeeExtSet.get(k));
							break;
						}
					}
				}
				mLJTempFeeExtSet.clear();
			}
		}
		resultMMap.put(resLJTempFeeExtSet, SysConst.DELETE_AND_INSERT);

		return true;

	}

	public MMap getResult() {
		return resultMMap;
	}

}
