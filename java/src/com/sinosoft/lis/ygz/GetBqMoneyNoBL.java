package com.sinosoft.lis.ygz;

import com.sinosoft.lis.db.LYPremSeparateDetailDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LYPremSeparateDetailSchema;
import com.sinosoft.lis.vschema.LYPremSeparateDetailSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 续期、保全价税分离明细实收号提取
 * @author lzy
 *
 */
public class GetBqMoneyNoBL {
	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	/** 数据操作字符串 */
	private String mOperate;
	//页面手工提取标志
	private String mHandFlag;
	
	private String mWherePart;
	//业务处理相关变量
	private MMap mMap = new MMap();
	
	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		// 进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GetBqPayNoBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败GetBqPayNoBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}

		// 准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}

		// 保存数据
		PubSubmit tPubSubmit = new PubSubmit();

		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			System.out.print("保存数据返回");
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "GetBqPayNoBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mInputData = null;
		return true;
	}

	// 获得输出数据
	private boolean getInputData(VData cInputData) {
		TransferData tTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
		
		mHandFlag = (String) tTransferData.getValueByName("HandWorkFlag");
		mWherePart = "";
		
		if(null != mHandFlag && !"".equals(mHandFlag)){
			LYPremSeparateDetailSet tLYPremSeparateDetailSet=(LYPremSeparateDetailSet) cInputData
			.getObjectByObjectName("LYPremSeparateDetailSet", 0);
			if(null != tLYPremSeparateDetailSet){
				String lisStr1="";
				String lisStr2="";
				for(int i=1; i<=tLYPremSeparateDetailSet.size();i++){
					lisStr1 += "'"+ tLYPremSeparateDetailSet.get(i).getTempFeeNo()+"'";
					if(i<tLYPremSeparateDetailSet.size()){
						lisStr1 += ",";
					}
				}
				for(int i=1; i<=tLYPremSeparateDetailSet.size();i++){
					lisStr2 += "'"+ tLYPremSeparateDetailSet.get(i).getOtherNo()+"'";
					if(i<tLYPremSeparateDetailSet.size()){
						lisStr2 += ",";
					}
				}

				if(null != lisStr1 && !lisStr1.startsWith("'null'")){
					mWherePart = " and  a.tempfeeno in ("+lisStr1+") ";//续期收费实收号回写
				}else 
				{
					mWherePart = " and  a.otherno in ("+lisStr2+") ";//保全收费实收号回写
				}
			}
		}
		return true;
	}
	
//  业务处理
	private boolean dealData() {
		//续期价税分离数据
		String tSQL = "select distinct a.tempfeeno,b.payno "
			+ " from LYPremSeparateDetail a,ljapay b "
			+ " where 1=1  "
			+ " and a.tempfeeno = b.getnoticeno "
			+ " and a.moneytype='01' "
			+ " and b.sumactupaymoney>=0 "
//			+ " and a.tempfeetype in ('2','4','7','9') "//个团续期：2;个单保全：4;团单保全：4、7、9;
			+ " and a.tempfeetype = '2' " //续期
			+ " and (a.moneyno is null or a.moneyno='') "
			+ mWherePart
			+ " with ur";
		
		SSRS tSSRS = new ExeSQL().execSQL(tSQL);
		if(null != tSSRS && tSSRS.getMaxRow() > 0){
			for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
				LYPremSeparateDetailDB tLYPremSeparateDetailDB  = new LYPremSeparateDetailDB ();
				
				String tempFeeNo = tSSRS.GetText(i, 1);
				String payNo = tSSRS.GetText(i, 2);
				String queryStr="select * from LYPremSeparateDetail "
							+ " where tempfeeno = '"+tempFeeNo+"' and (moneyno is null or moneyno='') ";
				LYPremSeparateDetailSet tLYPremSeparateDetailSet = tLYPremSeparateDetailDB.executeQuery(queryStr);
				for (int j = 1; j <= tLYPremSeparateDetailSet.size(); j++) {
					LYPremSeparateDetailSchema detailSchema=tLYPremSeparateDetailSet.get(j).getSchema();
					detailSchema.setModifyDate(PubFun.getCurrentDate());
					detailSchema.setModifyTime(PubFun.getCurrentTime());
					detailSchema.setMoneyNo(payNo);
					
					mMap.put(detailSchema,SysConst.UPDATE);
				}
			}
		}
		//保全的价税分离数据
		String tSQL2 = "select distinct a.tempfeeno,b.payno "
			+ " from LYPremSeparateDetail a,ljapay b,lpedorapp c "
			+ " where 1=1  "
			+ " and a.otherno = c.edoracceptno "
			+ " and b.incomeno = c.edoracceptno "
			+ " and a.moneytype='01' "
			+ " and a.tempfeetype in ('4','7','9') "//个团续期：2;个单保全：4;团单保全：4、7、9;
			+ " and (a.moneyno is null or a.moneyno='') "
			+ mWherePart
			+ " with ur";
		
		SSRS tSSRS2 = new ExeSQL().execSQL(tSQL2);
		if(null != tSSRS2 && tSSRS2.getMaxRow() > 0){
			for (int i = 1; i <= tSSRS2.getMaxRow(); i++) {
				LYPremSeparateDetailDB tLYPremSeparateDetailDB  = new LYPremSeparateDetailDB ();
				
				String tempFeeNo = tSSRS2.GetText(i, 1);
				String payNo = tSSRS2.GetText(i, 2);
				String queryStr="select * from LYPremSeparateDetail "
							+ " where tempfeeno = '"+tempFeeNo+"' and (moneyno is null or moneyno='') ";
				LYPremSeparateDetailSet tLYPremSeparateDetailSet = tLYPremSeparateDetailDB.executeQuery(queryStr);
				for (int j = 1; j <= tLYPremSeparateDetailSet.size(); j++) {
					LYPremSeparateDetailSchema detailSchema=tLYPremSeparateDetailSet.get(j).getSchema();
					detailSchema.setModifyDate(PubFun.getCurrentDate());
					detailSchema.setModifyTime(PubFun.getCurrentTime());
					detailSchema.setMoneyNo(payNo);
					
					mMap.put(detailSchema,SysConst.UPDATE);
				}
			}
		}
		return true;
	}
	/**
	 * 准备后台的数据
	 * @return boolean
	 */
	private boolean prepareOutputData() {
		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GetBqPayNoBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
	
	public static void main(String[] args) {
		GetBqMoneyNoBL tGetBqPayNoBL = new GetBqMoneyNoBL();
		VData cInputData = new VData();
		TransferData tTransferData = new TransferData();
		cInputData.add(tTransferData);
		tGetBqPayNoBL.submitData(cInputData, "");
	}
}
