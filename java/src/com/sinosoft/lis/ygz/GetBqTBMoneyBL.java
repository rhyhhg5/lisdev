package com.sinosoft.lis.ygz;

import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LYPremSeparateDetailSchema;
import com.sinosoft.lis.vschema.LYPremSeparateDetailSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

/**
 * 续期、保全价税分离明细实收号提取
 * @author lzy
 *
 */
public class GetBqTBMoneyBL {
	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	/** 数据操作字符串 */
	private String mOperate;
	//业务处理相关变量
	private MMap mMap = new MMap();
	
	private LYPremSeparateDetailSet mLYPremSeparateDetailSet;
	
	private String mCurrDate = PubFun.getCurrentDate();
	
	private String mCurrTime = PubFun.getCurrentTime();
	
	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		// 进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GetBqPayNoBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败GetBqPayNoBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}

		// 准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}

		// 保存数据
		PubSubmit tPubSubmit = new PubSubmit();

		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			System.out.print("保存数据返回");
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "GetBqPayNoBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mInputData = null;
		return true;
	}

	// 获得输出数据
	private boolean getInputData(VData cInputData) {
		mLYPremSeparateDetailSet =(LYPremSeparateDetailSet)cInputData.getObjectByObjectName("LYPremSeparateDetailSet", 0);
		if(null == mLYPremSeparateDetailSet || mLYPremSeparateDetailSet.size()==0 ){
			CError tError = new CError();
			tError.moduleName = "GetBqPayNoBL";
			tError.functionName = "submitData";
			tError.errorMessage = "传送的数据不能为空";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
	
	//  业务处理
	private boolean dealData() {
		String edorNoList="";
		for(int k=1;k<=mLYPremSeparateDetailSet.size(); k++){
			edorNoList +="'"+mLYPremSeparateDetailSet.get(k).getOtherNo()+"'";
			if(k<mLYPremSeparateDetailSet.size()){
				edorNoList += ",";
			}
		}
		String getWherePart=" and endorse.endorsementno in ("+edorNoList+") ";
		String strSQL ="";
		//个单退保
		if("1".equals(mOperate)){
			strSQL="select trim(lja.actugetno)||trim(lja.othernotype)||trim(endorse.riskcode)||trim(endorse.feeoperationtype)||trim(endorse.feefinatype) BusiNo, "
				+ " lja.ActuGetNo ActuGetNo,lja.OtherNo OtherNo,lja.OtherNoType OtherNoType, "
				+ " (select prtno from lccont where contno = endorse.contno union select prtno from lbcont where contno = endorse.contno) PrtNo, "
				+ " endorse.ContNo PolicyNo, "
				+ " endorse.riskcode,lja.managecom,sum(endorse.getmoney),endorse.feeoperationtype,endorse.feefinatype "
				+ " from ljaget lja,ljagetendorse endorse "
				+ " where 1=1 "
				+ " and lja.actugetno=endorse.actugetno "
				+ " and lja.otherno=endorse.endorsementno "
				+ " and lja.othernotype='10' "
				+ " and (endorse.grpcontno is null or endorse.grpcontno='00000000000000000000')"
				+ getWherePart
				+ " group by lja.actugetno,lja.otherno,lja.othernotype,endorse.riskcode,endorse.contno,lja.managecom,endorse.feeoperationtype,endorse.feefinatype "
				+ " with ur";
		}else if("2".equals(mOperate)){
			strSQL="select trim(lja.actugetno)||trim(lja.othernotype)||trim(endorse.riskcode)||trim(endorse.feeoperationtype)||trim(endorse.feefinatype) BusiNo, "
				+ " lja.ActuGetNo ActuGetNo,lja.OtherNo OtherNo,lja.OtherNoType OtherNoType, "
				+ " (select prtno from lcgrpcont where grpcontno = endorse.grpcontno union select prtno from lbgrpcont where grpcontno = endorse.grpcontno) PrtNo, "
				+ " endorse.GrpContNo PolicyNo, "
				+ " endorse.riskcode,lja.managecom,sum(endorse.getmoney),endorse.feeoperationtype,endorse.feefinatype "
				+ " from ljaget lja,ljagetendorse endorse "
				+ " where 1=1 "
				+ " and lja.actugetno=endorse.actugetno "
				+ " and lja.otherno=endorse.endorsementno "
				+ " and lja.othernotype='3' "
				+ getWherePart
				+ " group by lja.actugetno,lja.otherno,lja.othernotype,endorse.riskcode,endorse.GrpContno,lja.managecom,endorse.feeoperationtype,endorse.feefinatype "
				+ " with ur";
		}
		SSRS tSSRS=new ExeSQL().execSQL(strSQL);
		LYPremSeparateDetailSet detailSet=new LYPremSeparateDetailSet();
		for(int i=1; i<tSSRS.getMaxRow(); i++){
			LYPremSeparateDetailSchema detailSchema=new LYPremSeparateDetailSchema();
			detailSchema.setBusiNo(tSSRS.GetText(i, 1));
			detailSchema.setMoneyNo(tSSRS.GetText(i, 2));//退费凭证号
			detailSchema.setOtherNo(tSSRS.GetText(i, 3));
			detailSchema.setOtherNoType(tSSRS.GetText(i, 3));
			detailSchema.setPrtNo(tSSRS.GetText(i, 5));
			detailSchema.setPolicyNo(tSSRS.GetText(i, 6));
			detailSchema.setRiskCode(tSSRS.GetText(i, 7));
			detailSchema.setManageCom(tSSRS.GetText(i, 8));
			detailSchema.setPayMoney(tSSRS.GetText(i, 9));
			detailSchema.setState("01");//数据提取状态
			detailSchema.setOtherState("02");//实付提取数据
			detailSchema.setMoneyType("02");//收付费类型:01-收费；02-付费
			detailSchema.setFeeOperationType(tSSRS.GetText(i, 10));
			detailSchema.setFeeFinaType(tSSRS.GetText(i, 11));
			detailSchema.setMakeDate(mCurrDate);
			detailSchema.setMakeTime(mCurrTime);
			detailSchema.setModifyDate(mCurrDate);
			detailSchema.setModifyTime(mCurrTime);
			String BusiType="01";//默认为保费
			
			if(BQ.FEEFINATYPE_GB.equals(tSSRS.GetText(i, 11))){
				BusiType = "02";
			}
			detailSchema.setBusiType(BusiType);
			detailSet.add(detailSchema);
		}
		mMap.put(detailSet, SysConst.INSERT);
		return true;
	}
	/**
	 * 准备后台的数据
	 * @return boolean
	 */
	private boolean prepareOutputData() {
		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GetBqPayNoBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
	
	public static void main(String[] args) {
		GetBqTBMoneyBL tGetBqPayNoBL = new GetBqTBMoneyBL();
		VData cInputData = new VData();
		tGetBqPayNoBL.submitData(cInputData, "");
	}
}
