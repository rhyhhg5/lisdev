package com.sinosoft.lis.ygz;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class GetInvoiceUI {

	/** �������� */
    public CErrors mErrors = new CErrors();
    
    public GetInvoiceUI(){}
    
    public boolean submitData(VData cInputData, String cOperate){
    	
    	try{
    		GetInvoiceBL tGetInvoiceBL = new GetInvoiceBL();
            if (!tGetInvoiceBL.submitData(cInputData, cOperate)){
                this.mErrors.copyAllErrors(tGetInvoiceBL.mErrors);
                return false;
            }
            return true;
        }catch (Exception e){
            e.printStackTrace();
            this.mErrors.addOneError(e.getMessage());
            return false;
        }
    }
}
