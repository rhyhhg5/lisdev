package com.sinosoft.lis.ygz;

import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LYOutPayDetailSchema;
import com.sinosoft.lis.schema.LYPremSeparateDetailSchema;
import com.sinosoft.lis.vschema.LJAGetEndorseSet;
import com.sinosoft.lis.vschema.LJAPayGrpSet;
import com.sinosoft.lis.vschema.LJAPayPersonSet;
import com.sinosoft.lis.vschema.LYOutPayDetailSet;
import com.sinosoft.lis.vschema.LYPremSeparateDetailSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 续期保费发票报送处理
 * @author fr
 * 
 */
public class GetXQBillBL {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	/** 数据操作字符串 */
	private String mOperate;
	
	private GlobalInput mGI = new GlobalInput();
	/** 开始日期 */
	private String mStartDate;
	/** 结束日期 */
	private String mEndDate;
	/** 页面手工提取标记*/
	private String mHandFlag ;
	
	
	private String mPolicyType;
	private String mWherePartPerson ;
	private String mWherePartGrp ;
	// 业务处理相关变量
	private MMap mMap = new MMap();

	private String mCurrentDate = PubFun.getCurrentDate();
	private String mCurrentTime = PubFun.getCurrentTime();

	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		// 进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GetBqPremBillBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败GetTempFeeBqSeparateBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}

		// 准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}

		// 保存数据
		PubSubmit tPubSubmit = new PubSubmit();

		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			System.out.print("保存数据返回");
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "GetXQBillBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mInputData = null;
		return true;
	}

	// 获得输出数据
	private boolean getInputData(VData cInputData) {
		TransferData tTransferData = (TransferData) cInputData
				.getObjectByObjectName("TransferData", 0);
		mStartDate = (String) tTransferData.getValueByName("StartDate");
		mEndDate = (String) tTransferData.getValueByName("EndDate");
		mPolicyType =  (String) tTransferData.getValueByName("PolicyType");	
	  	mHandFlag = (String) tTransferData.getValueByName("HandWorkFlag");
	  	mWherePartPerson = "";
	  	mWherePartGrp = "";
		
		if(null != mHandFlag && "1".equals(mHandFlag)){
			LJAPayPersonSet tLJAPayPersonSet=(LJAPayPersonSet) cInputData
			.getObjectByObjectName("LJAPayPersonSet", 0);
			if(mPolicyType.equals("1")){
				
			if(null != tLJAPayPersonSet){
				String lisStr="";
				String lisStrnotice="";
				
				for(int i=1; i<=tLJAPayPersonSet.size();i++){
					lisStr += "'"+ tLJAPayPersonSet.get(i).getContNo()+"'";
					lisStrnotice += "'"+ tLJAPayPersonSet.get(i).getGetNoticeNo()+"'";
					if(i<tLJAPayPersonSet.size()){
						lisStr += ",";
						lisStrnotice += ",";
					}
				}
				mWherePartPerson = " and ljp.contno in ("+lisStr+")  and ljp.getNoticeNo in ("+lisStrnotice+") ";
			}
				
			}
			
			else if(mPolicyType.equals("2")){
				
				LJAPayGrpSet tLJAPayGrpSet=(LJAPayGrpSet) cInputData
				.getObjectByObjectName("LJAPayGrpSet", 0);
				if(null != tLJAPayGrpSet){
					String lisStr="";
					String lisStrnotice="";
					
					for(int i=1; i<=tLJAPayGrpSet.size();i++){
						lisStr += "'"+ tLJAPayGrpSet.get(i).getGrpContNo()+"'";
						lisStrnotice += "'"+ tLJAPayGrpSet.get(i).getGetNoticeNo()+"'";
						if(i<tLJAPayGrpSet.size()){
							lisStr += ",";
							lisStrnotice += ",";
						}
					}
					mWherePartGrp = " and ljp.grpcontno in ("+lisStr+")  and ljp.getNoticeNo in ("+lisStrnotice+")  ";
				}
				
			}
			}
				
		return true;
	}

	// 业务处理
	private boolean dealData() {

		LYOutPayDetailSet tLYOutPayDetailSet = new LYOutPayDetailSet();
		String tWhereSQL = "";
		if (null != mStartDate  && !"".equals(mStartDate)) {
			tWhereSQL += " and lja.confdate >= '" + mStartDate + "' ";
		}
		if (null != mEndDate && !"".equals(mEndDate)) {
			tWhereSQL += " and lja.confdate <= '" + mEndDate + "' ";
		}
		
		if( !"".equals(mWherePartPerson)|| !"".equals(mWherePartGrp)){
		}else{
			if ("".equals(tWhereSQL) ) {
				tWhereSQL += " and lja.confdate >= current date - 7 days ";
			}
		}
	
//		if(null != mHandFlag && !"".equals(mHandFlag)){
//			tWhereSQL ="";
//			
//		}
		//团单发票数据提取
		if(null == mPolicyType || "2".equals(mPolicyType) ){
			
			String tSQL = " select "
			        + " trim(ljp.grppolno)||trim(ljp.payno)||trim(ljp.PayType), " 
				//	+ " lyp.busino,"//流水号
					+ " ljp.managecom,"
				//	+ " (select managecom from lccont where contno=ljp.policyno union select managecom from lbcont where contno=lyp.policyno) , "//所属组织
				    + " (select appntno from lcappnt where GrpContNo=ljp.GrpContNo union select appntno from lbappnt where GrpContNo=ljp.GrpContNo) , "//客户号
					+ " (select appntname from lcappnt where GrpContNo=ljp.GrpContNo union select appntname from lbappnt where GrpContNo=ljp.GrpContNo) , "
					+ " '1', "//客户类型:“1”代表机构，“2”代表个人
					+ " '01', "//价税分离项目:固定值
					+ " ljp.taxrate,"
					+ " ljp.moneyNoTax,"
					+ " ljp.moneyTax, "
					+ " '0', "//申报类型：0正常申报，固定值
					+ " '01', "//来源系统:固定值
					+ " lja.confdate,"//交易日期
					+ " ljp.riskcode,"//产品
					+ " ljp.grpcontno,"
			        + " ljp.busitype,"//业务类型
					+ " 'CNY',"//交易币种
					+ " ljp.sumactupaymoney,"//交易金额
					+ " '0', "//金额是否含税:“0”表示含税，“1”表示不含税
					+ " '01',"//收付标志
				//  + " lyp.moneytype,"//收付标志
					+ " '0',"//境内外标示:“0”表示境内，“1”表示境外
					+ " 'Y',"//是否开票
					+ " '0',"//汇总地类型:“0”表示汇总，“1”表示属地，后续纳税申报需要
					+ " (select prtno from lcgrpcont where grpcontno=ljp.grpcontno union select prtno from lbgrpcont where grpcontno=ljp.grpcontno),"	//印刷号			
				//	+ " lyp.prtno, "//投保单号
					+ " (select signdate from lcgrpcont where grpcontno=ljp.grpcontno union select signdate from lbgrpcont where grpcontno=ljp.grpcontno) ,"//签单号
	//				+ " lja.incomeno, "//批单号
				    + " ljp.payno moneyno,"
				//	+ " lyp.moneyno moneyno, "
					+ " (select riskname from lmriskapp where riskcode=ljp.riskcode)"
					+ " from Ljapaygrp ljp,ljapay lja "
					+ " where ljp.grpcontno=lja.incomeno "
					+ " and lja.incometype='1' "
					+ " and ljp.payno=lja.payno "
					+ " and ljp.moneyNoTax is not null "
					+ " and lja.duefeetype='1' "
					+ " and ljp.payno is not null "
					+ " and ljp.paytype = 'ZC'"
	//				+ " and exists (select 1 from ljspayb where getnoticeno=lyp.tempfeeno) "				
					+ " and not exists (select 1 from LYOutPayDetail where busino= trim(ljp.grppolno)||trim(ljp.payno)||trim(ljp.PayType) and state='02') "
					+ " and exists (select 1 from lcgrpcont where grpcontno=ljp.grpcontno union select 1 from lbgrpcont where grpcontno=ljp.grpcontno) "
					+ mWherePartGrp	
					+ tWhereSQL
					+ " with ur";
			SSRS tSSRS = new ExeSQL().execSQL(tSQL);
			if (tSSRS != null && tSSRS.getMaxRow() > 0) {
				for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
					LYOutPayDetailSchema tLYOutPayDetailSchema = new LYOutPayDetailSchema();
					tLYOutPayDetailSchema.setBusiNo(tSSRS.GetText(i, 1));
					tLYOutPayDetailSchema.settranserial(tSSRS.GetText(i, 1));//交易流水号
					tLYOutPayDetailSchema.setbusipk(tSSRS.GetText(i, 1));
					tLYOutPayDetailSchema.setisupdate("Y");//是否更新客户信息
					tLYOutPayDetailSchema.setgroup("00");//所属集团:固定值
					tLYOutPayDetailSchema.setpkgroup("00");//所属集团:固定值
					tLYOutPayDetailSchema.setpkorg(tSSRS.GetText(i, 2));//所属组织
					tLYOutPayDetailSchema.setorg(tSSRS.GetText(i, 2));//所属组织
					tLYOutPayDetailSchema.setorgv(tSSRS.GetText(i, 2));//所属组织
					tLYOutPayDetailSchema.setdeptdoc(tSSRS.GetText(i, 2));
					tLYOutPayDetailSchema.setcode(tSSRS.GetText(i, 3));//客户编码
					tLYOutPayDetailSchema.setname(tSSRS.GetText(i, 4));//客户名称
	//				tLYOutPayDetailSchema.setcustomertype(tSSRS.GetText(i, 5));//客户类型
					tLYOutPayDetailSchema.setcustomertype("2");
					tLYOutPayDetailSchema.setcustcode(tSSRS.GetText(i, 3));
					tLYOutPayDetailSchema.setcustname(tSSRS.GetText(i, 4));
	//				tLYOutPayDetailSchema.setcusttype(tSSRS.GetText(i, 5));
					tLYOutPayDetailSchema.setcusttype("2");				
					tLYOutPayDetailSchema.setptsitem(tSSRS.GetText(i, 6));	
					tLYOutPayDetailSchema.settaxrate(tSSRS.GetText(i, 7));//税率
					tLYOutPayDetailSchema.setoriamt(tSSRS.GetText(i, 8));//不含税金额
					tLYOutPayDetailSchema.setoritax(tSSRS.GetText(i, 9));//税额
					tLYOutPayDetailSchema.setlocalamt(tSSRS.GetText(i, 8));//不含税金额（本币）
					tLYOutPayDetailSchema.setlocaltax(tSSRS.GetText(i, 9));//税额（本币）
					tLYOutPayDetailSchema.setdectype(tSSRS.GetText(i, 10));//申报类型
					tLYOutPayDetailSchema.setsrcsystem(tSSRS.GetText(i, 11));
					tLYOutPayDetailSchema.settrandate(tSSRS.GetText(i, 12));//交易日期
					String riskcode = tSSRS.GetText(i, 13);
					String moneyType = tSSRS.GetText(i, 15);
					if ("09".equals(moneyType)) {//工本费riskcode置0000
						riskcode = BQ.FILLDATA;
					} else {
						riskcode = riskcode.substring(0, 4);
					}
					tLYOutPayDetailSchema.setprocode(riskcode);
					tLYOutPayDetailSchema.setbusitype(moneyType);//同价税分离接口的收支类型
					tLYOutPayDetailSchema.settrancurrency(tSSRS.GetText(i, 16));// 币种：人民币
					tLYOutPayDetailSchema.settranamt(tSSRS.GetText(i, 17));//交易金额（含税）
					tLYOutPayDetailSchema.settaxtype(tSSRS.GetText(i, 18));//金额是否含税
					String paymentflag="0";//“0”表示收入，“1”表示支出
					if("02".equals(tSSRS.GetText(i, 19))){
						paymentflag="1";
					}
					tLYOutPayDetailSchema.setpaymentflag(paymentflag);
					tLYOutPayDetailSchema.setoverseasflag(tSSRS.GetText(i, 20));
					tLYOutPayDetailSchema.setisbill(tSSRS.GetText(i, 21));
					tLYOutPayDetailSchema.setareatype(tSSRS.GetText(i, 22));
					tLYOutPayDetailSchema.setinsureno(tSSRS.GetText(i, 23));//印刷号
					tLYOutPayDetailSchema.setbilleffectivedate(tSSRS.GetText(i, 24));
					tLYOutPayDetailSchema.setvdata(mCurrentDate);
					tLYOutPayDetailSchema.setvdef2(tSSRS.GetText(i, 14));
					tLYOutPayDetailSchema.setmoneyno(tSSRS.GetText(i, 25));
					tLYOutPayDetailSchema.setmoneytype(tSSRS.GetText(i, 19));
					//备注
					String vdef1="保单号:"+tSSRS.GetText(i, 14)+";险种："+tSSRS.GetText(i, 26);
					tLYOutPayDetailSchema.setvdef1(vdef1);
					tLYOutPayDetailSchema.setvoucherid(tSSRS.GetText(i, 14));//保单号
					tLYOutPayDetailSchema.setprebilltype("02");//非预打
					tLYOutPayDetailSchema.setinvtype("0");//默认0
					tLYOutPayDetailSchema.setvaddress("0");
					tLYOutPayDetailSchema.setvsrcsystem("01");
					tLYOutPayDetailSchema.settaxpayertype("01");//管理平台提供的默认值
					tLYOutPayDetailSchema.setenablestate("2");//默认值
					
					tLYOutPayDetailSchema.setoperator(mGI.Operator);
					tLYOutPayDetailSchema.setMakeDate(mCurrentDate);
					tLYOutPayDetailSchema.setMakeTime(mCurrentTime);
					tLYOutPayDetailSchema.setModifyDate(mCurrentDate);
					tLYOutPayDetailSchema.setModifyTime(mCurrentTime);
					
					tLYOutPayDetailSet.add(tLYOutPayDetailSchema);
				}
			//批处理不上报个单发票
			}
		}
		
			if(null == mPolicyType || "1".equals(mPolicyType) ){
				//个单发票数据提取
				String tSQL2 = " select "
					+ " trim(ljp.polno)||trim(ljp.payno)||trim(ljp.DutyCode),"//流水号
				//	+ " lyp.busino,"//流水号
					+ " ljp.managecom,"
				//	+ " (select managecom from lccont where contno=ljp.policyno union select managecom from lbcont where contno=lyp.policyno) , "//所属组织
			    	+ " (select appntno from lcappnt where contno=ljp.contno union select appntno from lbappnt where contno=ljp.contno) , "//客户号
					+ " (select appntname from lcappnt where contno=ljp.contno union select appntname from lbappnt where contno=ljp.contno) , "
					+ " '2', "//客户类型:“1”代表机构，“2”代表个人
					+ " '01', "//价税分离项目:固定值
					+ " ljp.taxrate,"
					+ " ljp.moneyNoTax,"
					+ " ljp.moneyTax, "
					+ " '0', "//申报类型：0正常申报，固定值
					+ " '01', "//来源系统:固定值
					+ " lja.confdate,"//交易日期
					+ " ljp.riskcode,"//产品
					+ " ljp.contno,"
			        + " ljp.busitype,"//业务类型
					+ " 'CNY',"//交易币种
					+ " ljp.sumactupaymoney,"//交易金额
			    //  + " lyp.paymoney,"//交易金额
					+ " '0', "//金额是否含税:“0”表示含税，“1”表示不含税
					+ " '01',"
//				    + " ljp.moneytype,"//收付标志
					+ " '0',"//境内外标示:“0”表示境内，“1”表示境外
					+ " 'Y',"//是否开票
					+ " '0',"//汇总地类型:“0”表示汇总，“1”表示属地，后续纳税申报需要
					+ " (select prtno from lccont where contno=ljp.contno union select prtno from lbcont where contno=ljp.contno),"	//印刷号			
				//	+ " lyp.prtno, "//投保单号
					+ " (select signdate from lccont where contno=ljp.contno union select signdate from lbcont where contno=ljp.contno) ,"//签单号
	//				+ " lja.incomeno, "//批单号
				    + " ljp.payno moneyno,"
				//	+ " lyp.moneyno moneyno, "
					+ " (select riskname from lmriskapp where riskcode=ljp.riskcode)"
					+ " from Ljapayperson ljp,ljapay lja "
					+ " where ljp.contno=lja.incomeno "
					+ " and lja.incometype='2' "
					+ " and ljp.payno=lja.payno "
					+ " and ljp.PayAimClass='1'"//个单
					+ " and ljp.grpcontno like '000000%'"
	//				+ " and lja.confdate is not null "
				//	+ " and lyp.tempfeetype ='2' "//续期
					+ " and ljp.moneyNoTax is not null "
					+ " and ljp.moneyTax is not null"
					+ " and ljp.paytype = 'ZC'"
				//	+ " and lyp.state='02' "//已价税分离
					+ " and lja.duefeetype ='1' "
					+ " and ljp.payno is not null "
	//				+ " and exists (select 1 from ljspayb where getnoticeno=lyp.tempfeeno) "
					+ " and not exists (select 1 from LYOutPayDetail where busino= trim(ljp.polno)||trim(ljp.payno)||trim(ljp.DutyCode)  and state='02') "
					+ " and exists (select 1 from lccont where contno=ljp.contno union select 1 from lbcont where contno=ljp.contno) "
					+ mWherePartPerson	
					+ tWhereSQL
					+ " with ur";
				SSRS tSSRS2 = new ExeSQL().execSQL(tSQL2);
				if (tSSRS2 != null && tSSRS2.getMaxRow() > 0) {
					for (int i = 1; i <= tSSRS2.getMaxRow(); i++) {
						LYOutPayDetailSchema tLYOutPayDetailSchema = new LYOutPayDetailSchema();
						tLYOutPayDetailSchema.setBusiNo(tSSRS2.GetText(i, 1));
						tLYOutPayDetailSchema.settranserial(tSSRS2.GetText(i, 1));//交易流水号
						tLYOutPayDetailSchema.setbusipk(tSSRS2.GetText(i, 1));
						tLYOutPayDetailSchema.setisupdate("Y");//是否更新客户信息
						tLYOutPayDetailSchema.setgroup("00");//所属集团:固定值
						tLYOutPayDetailSchema.setpkgroup("00");//所属集团:固定值
						tLYOutPayDetailSchema.setpkorg(tSSRS2.GetText(i, 2));//所属组织
						tLYOutPayDetailSchema.setorg(tSSRS2.GetText(i, 2));//所属组织
						tLYOutPayDetailSchema.setorgv(tSSRS2.GetText(i, 2));//所属组织
						tLYOutPayDetailSchema.setdeptdoc(tSSRS2.GetText(i, 2));
						tLYOutPayDetailSchema.setcode(tSSRS2.GetText(i, 3));//客户编码
						tLYOutPayDetailSchema.setname(tSSRS2.GetText(i, 4));//客户名称
			//			tLYOutPayDetailSchema.setcustomertype(tSSRS2.GetText(i, 5));//客户类型
						tLYOutPayDetailSchema.setcustomertype("2");
						tLYOutPayDetailSchema.setcustcode(tSSRS2.GetText(i, 3));
						
						tLYOutPayDetailSchema.setcustname(tSSRS2.GetText(i, 4));
			//			tLYOutPayDetailSchema.setcusttype(tSSRS2.GetText(i, 5));
						tLYOutPayDetailSchema.setcusttype("2");
						
						tLYOutPayDetailSchema.setptsitem(tSSRS2.GetText(i, 6));
						tLYOutPayDetailSchema.settaxrate(tSSRS2.GetText(i, 7));//税率
						tLYOutPayDetailSchema.setoriamt(tSSRS2.GetText(i, 8));//不含税金额
						tLYOutPayDetailSchema.setoritax(tSSRS2.GetText(i, 9));//税额
						tLYOutPayDetailSchema.setlocalamt(tSSRS2.GetText(i, 8));//不含税金额（本币）
						tLYOutPayDetailSchema.setlocaltax(tSSRS2.GetText(i, 9));//税额（本币）
						tLYOutPayDetailSchema.setdectype(tSSRS2.GetText(i, 10));//申报类型
						tLYOutPayDetailSchema.setsrcsystem(tSSRS2.GetText(i, 11));
						tLYOutPayDetailSchema.settrandate(tSSRS2.GetText(i, 12));//交易日期
						String riskcode = tSSRS2.GetText(i, 13);
						String moneyType = tSSRS2.GetText(i, 15);
						if ("09".equals(moneyType)) {//工本费riskcode置0000
							riskcode = BQ.FILLDATA;
						} else {
							riskcode = riskcode.substring(0, 4);
						}
						tLYOutPayDetailSchema.setprocode(riskcode);
						tLYOutPayDetailSchema.setbusitype(moneyType);//同价税分离接口的收支类型
						tLYOutPayDetailSchema.settrancurrency(tSSRS2.GetText(i, 16));// 币种：人民币
						tLYOutPayDetailSchema.settranamt(tSSRS2.GetText(i, 17));//交易金额（含税）
						tLYOutPayDetailSchema.settaxtype(tSSRS2.GetText(i, 18));//金额是否含税
						String paymentflag="0";//“0”表示收入，“1”表示支出
						if("02".equals(tSSRS2.GetText(i, 19))){
							paymentflag="1";
						}
						tLYOutPayDetailSchema.setpaymentflag(paymentflag);
						tLYOutPayDetailSchema.setoverseasflag(tSSRS2.GetText(i, 20));
						tLYOutPayDetailSchema.setisbill(tSSRS2.GetText(i, 21));
						tLYOutPayDetailSchema.setareatype(tSSRS2.GetText(i, 22));
						tLYOutPayDetailSchema.setinsureno(tSSRS2.GetText(i, 23));//印刷号
						tLYOutPayDetailSchema.setbilleffectivedate(tSSRS2.GetText(i, 24));
						tLYOutPayDetailSchema.setvdata(mCurrentDate);
						tLYOutPayDetailSchema.setvdef2(tSSRS2.GetText(i, 14));
						tLYOutPayDetailSchema.setmoneyno(tSSRS2.GetText(i, 25));
						tLYOutPayDetailSchema.setmoneytype(tSSRS2.GetText(i, 19));
						//备注
						String vdef1="保单号:"+tSSRS2.GetText(i, 14)+";险种："+tSSRS2.GetText(i, 26);
						tLYOutPayDetailSchema.setvdef1(vdef1);
						tLYOutPayDetailSchema.setvoucherid(tSSRS2.GetText(i, 14));//保单号
						tLYOutPayDetailSchema.setprebilltype("02");//非预打
						tLYOutPayDetailSchema.setinvtype("0");//默认0
						tLYOutPayDetailSchema.setvaddress("0");
						tLYOutPayDetailSchema.setvsrcsystem("01");
						tLYOutPayDetailSchema.settaxpayertype("01");//管理平台提供的默认值
						tLYOutPayDetailSchema.setenablestate("2");//默认值
						
						tLYOutPayDetailSchema.setoperator(mGI.Operator);
						tLYOutPayDetailSchema.setMakeDate(mCurrentDate);
						tLYOutPayDetailSchema.setMakeTime(mCurrentTime);
						tLYOutPayDetailSchema.setModifyDate(mCurrentDate);
						tLYOutPayDetailSchema.setModifyTime(mCurrentTime);
						
						tLYOutPayDetailSet.add(tLYOutPayDetailSchema);
					}
				}
		}
			//调用发票接口上报数据
			VData tVData = new VData();
			tVData.add(mGI);
			tVData.add(tLYOutPayDetailSet);
			OutPayUploadBL tOutPayUploadBL = new OutPayUploadBL();
			if (!tOutPayUploadBL.getSubmit(tVData, "")) {
				this.mErrors = tOutPayUploadBL.mErrors;
				System.out.println("续期收付费发票数据上报错误:"+ mErrors.getErrContent());
			}else{
				//保存返回结果
				mMap = tOutPayUploadBL.getResult();
			}

		return true;
	}

	/**
	 * 准备后台的数据
	 * 
	 * @return boolean
	 */
	private boolean prepareOutputData() {
		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GetBQBillBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	public static void main(String[] args) {
		GetXQBillBL tGetBQBillBL = new GetXQBillBL();
		VData cInputData = new VData();
		TransferData tTransferData = new TransferData();
		String tStartDate = "2016-05-19";
		String tEndDate = "2016-06-29";
		LYPremSeparateDetailSet set=new LYPremSeparateDetailSet();
//		String [] str=new String[]{"31001798201"};//个单收费记录号
//		for(int i=0;i<str.length; i++){
//			LYPremSeparateDetailSchema Schema=new LYPremSeparateDetailSchema();
//			Schema.setTempFeeNo(str[i]);
//			set.add(Schema);
//		}
	//	tTransferData.setNameAndValue("HandWorkFlag","Y");
//		cInputData.add(set);
		tTransferData.setNameAndValue("StartDate", tStartDate);
		tTransferData.setNameAndValue("EndDate", tEndDate);
		cInputData.add(tTransferData);
		tGetBQBillBL.submitData(cInputData, "");
	}
}
