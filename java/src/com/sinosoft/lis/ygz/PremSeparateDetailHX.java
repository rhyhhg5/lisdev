package com.sinosoft.lis.ygz;

import java.util.Date;
import java.util.List;

import com.sinosoft.lis.schema.LYPremSeparateDetailSchema;

public class PremSeparateDetailHX extends LYPremSeparateDetailSchema {
	/**
	 * 自定义项
	 */
	private String pk1;
	/**
	 * 自定义项
	 */
	private String pk2;
	/**
	 * 自定义项
	 */
	private String pk3;
	/**
	 * 自定义项
	 */
	private String pk4;
	/**
	 * 自定义项
	 */
	private String pk5;
	/**
	 * 自定义项
	 */
	private String pk6;
	/**
	 * 自定义项
	 */
	private String pk7;
	/**
	 * 自定义项
	 */
	private String pk8;
	/**
	 * 自定义项
	 */
	private String pk9;
	/**
	 * 自定义项
	 */
	private String pk10;

	private List pKList;
	
	public List getpKList() {
		return pKList;
	}

	public void setpKList(List pKList) {
		this.pKList = pKList;
	}

	public String getPk1() {
		return pk1;
	}

	public void setPk1(String pk1) {
		this.pk1 = pk1;
	}

	public String getPk2() {
		return pk2;
	}

	public void setPk2(String pk2) {
		this.pk2 = pk2;
	}

	public String getPk3() {
		return pk3;
	}

	public void setPk3(String pk3) {
		this.pk3 = pk3;
	}

	public String getPk4() {
		return pk4;
	}

	public void setPk4(String pk4) {
		this.pk4 = pk4;
	}

	public String getPk5() {
		return pk5;
	}

	public void setPk5(String pk5) {
		this.pk5 = pk5;
	}

	public String getPk6() {
		return pk6;
	}

	public void setPk6(String pk6) {
		this.pk6 = pk6;
	}

	public String getPk7() {
		return pk7;
	}

	public void setPk7(String pk7) {
		this.pk7 = pk7;
	}

	public String getPk8() {
		return pk8;
	}

	public void setPk8(String pk8) {
		this.pk8 = pk8;
	}

	public String getPk9() {
		return pk9;
	}

	public void setPk9(String pk9) {
		this.pk9 = pk9;
	}

	public String getPk10() {
		return pk10;
	}

	public void setPk10(String pk10) {
		this.pk10 = pk10;
	}

}
