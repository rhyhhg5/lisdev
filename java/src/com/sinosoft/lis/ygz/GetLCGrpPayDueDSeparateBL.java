package com.sinosoft.lis.ygz;

import java.util.ArrayList;
import java.util.List;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCGrpPayActuDetailSchema;
import com.sinosoft.lis.schema.LCGrpPayDueDetailSchema;
import com.sinosoft.lis.schema.LJAPayPersonSchema;
import com.sinosoft.lis.vschema.LCGrpPayActuDetailSet;
import com.sinosoft.lis.vschema.LCGrpPayDueDetailSet;
import com.sinosoft.lis.vschema.LYPremSeparateDetailSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class GetLCGrpPayDueDSeparateBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	/** 数据操作字符串 */
	private String mOperate;
	/** 开始日期 */
	private String mStartDate;
	/** 结束日期 */
	private String mEndDate;
	/** 印刷号*/
	private String mPrtNo;

	// 业务处理相关变量
	MMap mMap = new MMap();
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();
	private String mCurrentDate = PubFun.getCurrentDate();
	private String mCurrentTime = PubFun.getCurrentTime();

	public boolean submitData(VData cInputData, String cOperate) {
		System.out.println(mCurrentDate+":开始提取LCGrpPayActuDetail价税分离数据。");
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		if (!dealData()) {
			// @@错误处理
			buildError("submitData", "数据处理失败GetLCGrpPayDueDSeparateBL-->dealData!");
			return false;
		}

		if (!prepareOutputData()) {

			return false;
		}
//		// 保存数据
//		PubSubmit tPubSubmit = new PubSubmit();
//		if (!tPubSubmit.submitData(mInputData, mOperate)) {
//			// @@错误处理
//			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
//			CError tError = new CError();
//			tError.moduleName = "GetLCGrpPayDueDSeparateBl";
//			tError.functionName = "submitData";
//			tError.errorMessage = "数据提交失败!";
//			this.mErrors.addOneError(tError);
//			return false;
//		}
		mInputData = null;
		System.out.println(mCurrentDate+":提取LCGrpPayActuDetail价税分离数据结束。");
		return true;
	}

	private boolean getInputData(VData cInputData) {
		TransferData tTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
		mStartDate = (String) tTransferData.getValueByName("StartDate");
		mEndDate = (String) tTransferData.getValueByName("EndDate");
		mPrtNo = (String) tTransferData.getValueByName("PrtNo");
		mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
    	if(mGlobalInput == null){
    		buildError("getInputData","处理超时，请重新登录。");
    		return false;
    	}
		return true;
	}

	private boolean dealData() {
		LYPremSeparateDetailSet tLyPremSeparateDetailSet = new LYPremSeparateDetailSet();
		ExeSQL tExeSQL = new ExeSQL();
		String tWhereSQL = "";
		String tWhereTSQL = "";
		if(mStartDate != null && !"".equals(mStartDate)){
			tWhereSQL += " and confdate >= '" + mStartDate + "'";
		}
		if(mEndDate != null && !"".equals(mEndDate)){
			tWhereSQL += " and confdate <= '" + mEndDate + "'";
		}
		if("".equals(tWhereSQL)){
			tWhereSQL += "and confdate >= current date - 7 day ";
		}	
		if(mPrtNo != null && !"".equals(mPrtNo)){
			tWhereTSQL += " and PrtNo = '" + mPrtNo + "'";
		}
		/*缴费计划对应明细表*/
		String tSQL = " select *"
				+ " from LCGrpPayDueDetail as lcG" 
				+ " where (lcG.MoneyNoTax is null or lcG.MoneyNoTax = '')" 
				+ " and (lcG.MoneyTax is null or lcG.MoneyTax = '' )" 
				+ tWhereSQL
				+ tWhereTSQL
				+ " union "
				+ " select *"
				+ " from LCGrpPayDueDetail as lcG" 
				+ " where ( lcG.MoneyNoTax is not null and lcG.MoneyNoTax <> '')	" 
				+ " and abs(double(moneynotax)+double(moneytax)-prem)>0.01 "
				+ tWhereSQL
				+ tWhereTSQL
				+ " with ur";
		YGZ tYGZ = new YGZ();
		ExeSQL exeSQL = new ExeSQL();
		RSWrapper rsWrapper = new RSWrapper();
		LCGrpPayDueDetailSet tLCGrpPayDueDetailSet = new LCGrpPayDueDetailSet();
		if (!rsWrapper.prepareData(tLCGrpPayDueDetailSet, tSQL)) {
			System.out.println("价税分离明细LCGrpPayDueDetail数据准备失败！");
			return false;
		}
		do {
			rsWrapper.getData();
			if (null == tLCGrpPayDueDetailSet
					|| tLCGrpPayDueDetailSet.size() < 1) {
				break;
			}
			LYPremSeparateDetailSet tLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
			for(int i=1; i<=tLCGrpPayDueDetailSet.size(); i++){
				PremSeparateDetailHX tPremSeparateDetailHXSchema = new PremSeparateDetailHX();
				String busino ="";
				String policyno = "";
				String tBusiType = "";
				LCGrpPayDueDetailSchema tLCGrpPayDueDetailSchema = tLCGrpPayDueDetailSet.get(i);
				//根据schema获取主键字段名信息
				String[] schemaPK = tLCGrpPayDueDetailSchema.getPK();
				//将主键的值存储到集合中
				List pklist =new ArrayList();
				for(int k=0; k<schemaPK.length; k++){
					pklist.add(tLCGrpPayDueDetailSchema.getV(schemaPK[k]));
					busino +=tLCGrpPayDueDetailSchema.getV(schemaPK[k]);
				}
				tPremSeparateDetailHXSchema.setpKList(pklist);
				System.out.println("busino="+busino);
				if(busino.length()>=20){
					busino = busino.substring(0, 20);
				}
				tPremSeparateDetailHXSchema.setBusiNo(busino);
				policyno = tLCGrpPayDueDetailSchema.getGrpContNo();
				tPremSeparateDetailHXSchema.setPolicyNo(policyno);
				tPremSeparateDetailHXSchema.setPrtNo("000000");
				tPremSeparateDetailHXSchema.setRiskCode(tLCGrpPayDueDetailSchema.getRiskCode());
			  	String txString ="select managecom from LCGrpCont where GrpContNo = '"+ tLCGrpPayDueDetailSchema.getGrpContNo()+"' union select managecom from LBGrpCont where GrpContNo = '"+ tLCGrpPayDueDetailSchema.getGrpContNo()+"' with ur " ;
			  	String tManageCom = exeSQL.getOneValue(txString);
			   	if(tManageCom!= null ){			   		
			   		tPremSeparateDetailHXSchema.setManageCom(tManageCom);
			   	}
				tPremSeparateDetailHXSchema.setOtherState("22");
				tPremSeparateDetailHXSchema.setPayMoney("" +tLCGrpPayDueDetailSchema.getPrem());
				String sBusiType = tYGZ.getBusinType(null, "BF", tLCGrpPayDueDetailSchema.getRiskCode());
				tPremSeparateDetailHXSchema.setBusiType(sBusiType);
				tLyPremSeparateDetailSet.add(tPremSeparateDetailHXSchema);		
			}		
	
		if (tLyPremSeparateDetailSet != null && tLyPremSeparateDetailSet.size() > 0) {

			PremSeparateBL tPremSeparateBL = new PremSeparateBL();
			VData tVData = new VData();
			tVData.add(mGlobalInput);
			tVData.add(tLyPremSeparateDetailSet);
			if (!tPremSeparateBL.getSubmit(tVData, "")) {
				this.mErrors.copyAllErrors(tPremSeparateBL.mErrors);
				buildError("dealData", "调用PremSeparateBL接口失败。");
				return false;
			}

			MMap tMMap = new MMap();
	    	tMMap = tPremSeparateBL.getResult();
	    	
	    	//将价税分离接口返回的数据封装进MMap。
	    	LYPremSeparateDetailSet mLYPremSeparateDetailSet = (LYPremSeparateDetailSet) tMMap.getObjectByObjectName("LYPremSeparateDetailSet", 0);
	    	for(int i = 1; i <= mLYPremSeparateDetailSet.size();i++){
	    		PremSeparateDetailHX mPremSeparateDetailHXSchema = (PremSeparateDetailHX) mLYPremSeparateDetailSet.get(i);
	    		if(mPremSeparateDetailHXSchema.getMoneyNoTax()!=null && !"".equals(mPremSeparateDetailHXSchema.getMoneyNoTax())){
		    		String moneyNoTax = mPremSeparateDetailHXSchema.getMoneyNoTax();
		    		String moneyTax = mPremSeparateDetailHXSchema.getMoneyTax();
		    		String busiType = mPremSeparateDetailHXSchema.getBusiType();
		    		String taxRate = mPremSeparateDetailHXSchema.getTaxRate();//税率
		    		String whereSQL = " 1=1 ";
		    		LCGrpPayDueDetailSchema tempLCGrpPayDueDetailSchema = new LCGrpPayDueDetailSchema();
		    		//从表的schema中获取主键字段
		    		String[] tempPk = tempLCGrpPayDueDetailSchema.getPK();
		    		//根据主键的值拼接where条件
		    		List pkList = mPremSeparateDetailHXSchema.getpKList();
		    		for(int k=0; k< tempPk.length; k++){
		    			whereSQL += " and "+tempPk[k]+" = '"+pkList.get(k)+"' ";
		    		}
		    		//lzy 为保险起见加个校验
		    		if(" 1=1 ".equals(whereSQL)){
		    			return false;
		    		}
						String sql = "update LCGrpPayDueDetail set MoneyNotax = '"+ moneyNoTax+ "',"
								+ " Moneytax = '"+ moneyTax+ "',"
								+ " ModifyDate = '"+ mCurrentDate+ "',"
								+ " ModifyTime = '"+ mCurrentTime+ "',"
								+ " BusiType = '"+ busiType+ "',"
								+ " TaxRate = '" + taxRate +"'"
								+ " where "+ whereSQL;
						mMap.put(sql, SysConst.UPDATE);
					}
				}
			}
		
		//在这里提交
		if(!submit()){
			System.out.println("数据执行失败，运行下一批次");
		}
		
	}while (null != tLCGrpPayDueDetailSet
			&& tLCGrpPayDueDetailSet.size() > 0);
	
	return true;
 }		
		
	private boolean prepareOutputData() {
		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			buildError("prepareData", "在准备往后层处理所需要的数据时出错。");
			return false;
		}
		return true;
	}

	private void buildError(String sfunName, String serromes) {
		CError tError = new CError();
		tError.moduleName = "GetLCGrpPayDueDSeparateBl";
		tError.functionName = sfunName;
		tError.errorMessage = serromes;
		this.mErrors.addOneError(tError);
	}
	

	  /**
	 * 
	 * @return boolean
	 */
	private boolean submit() {
		try {
			VData tData = new VData();
			tData.add(mMap);
			PubSubmit tPubSubmit = new PubSubmit();
			if (!tPubSubmit.submitData(tData, "")) {
				mMap =new MMap();
				System.out.print("保存数据返回");
				// @@错误处理
				this.mErrors.copyAllErrors(tPubSubmit.mErrors);
				CError tError = new CError();
				tError.moduleName = "GetLCGrpPayDueDSeparateBL";
				tError.functionName = "submitData";
				tError.errorMessage = "数据提交失败!";
				this.mErrors.addOneError(tError);
				return false;
			}
		} catch (Exception ex) {
			mMap =new MMap();
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GetLCGrpPayDueDSeparateBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		mMap =new MMap();
		return true;
	}

	
	public static void main(String[] args) {
		GetLCGrpPayDueDSeparateBL tGetLCGrpPayDueDSeparateBL = new GetLCGrpPayDueDSeparateBL();
		VData cInputData = new VData();
		TransferData tTransferData = new TransferData();
		String tStartDate = "2017-03-01";
		String tEndDate = "2017-03-01";
		String tContNo = "";
		tTransferData.setNameAndValue("StartDate", tStartDate);
		tTransferData.setNameAndValue("EndDate", tEndDate);
		tTransferData.setNameAndValue("ContNo", tContNo);
		GlobalInput mGlobalInput = new GlobalInput();
		cInputData.add(tTransferData);
		cInputData.add(mGlobalInput);
		tGetLCGrpPayDueDSeparateBL.submitData(cInputData, "");
	}

}
