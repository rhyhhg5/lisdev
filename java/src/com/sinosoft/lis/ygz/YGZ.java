package com.sinosoft.lis.ygz;

import java.util.ArrayList;
import java.util.List;

import com.sinosoft.lis.bq.BQ;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * <p>Title: 营改增常量</p>
 * <p>Description: 存放暂收提取状态等常量</p>
 * <p>Copyright: Copyright (c) 2005 </p>
 * <p>Company: Sinosoft </p>
 * @author lzy
 * @version 1.0
 */
public class YGZ
{
	/** 存放万能险集合*/
	private List riskUliList = new ArrayList();
	
	/** 存放委托管理产品*/
	private List riskWtList  = new ArrayList();
	
	/** 存放旧特需产品*/
	private List riskOldTxList  = new ArrayList();
	
	/** 存放新特需产品*/
	private List riskNewTxList  = new ArrayList();
	
	/** 存放新特需产品*/
	private List riskHealthList  = new ArrayList();
	
	/** 存放健康守望B款产品*/
	private List riskJKSWList  = new ArrayList();
	
    public YGZ() {
    	//为收支类型方法准备数据
    	//万能
    	String uliString = "select trim(riskcode) from lmriskapp where risktype4='4' ";
    	SSRS tSSRS=new ExeSQL().execSQL(uliString);
    	for(int i=1; i<= tSSRS.getMaxRow(); i++){
    		riskUliList.add(tSSRS.GetText(i, 1));
    	}
    	// 委托管理产品
    	riskWtList.add("690101");
    	riskWtList.add("690201");
    	
    	//老特需产品
    	riskOldTxList.add("1605");
    	riskOldTxList.add("170101");
    	riskOldTxList.add("170106");
    	
    	//健康管理产品
    	riskHealthList.add("6201");
    	riskHealthList.add("6202");
    	riskHealthList.add("6203");
    	riskHealthList.add("6204");
    	riskHealthList.add("6601");
    	
    	
    	// 新特需产品
    	String txString ="select trim(riskcode) from lmriskapp where risktype3='7' and riskcode not in ('1605','170101','170106') ";
    	SSRS tSSRS2=new ExeSQL().execSQL(txString);
    	for(int i=1; i<= tSSRS2.getMaxRow(); i++){
    		riskNewTxList.add(tSSRS2.GetText(i, 1));
    	}
    	
    	//健康守望B款套餐
    	String jkswString = "select trim(riskcode) from lmriskapp where riskcode like '8201%' ";
    	SSRS tSSRS3=new ExeSQL().execSQL(jkswString);
    	for(int i=1; i<= tSSRS3.getMaxRow(); i++){
    		riskJKSWList.add(tSSRS3.GetText(i, 1));
    	}
    	
    }

    /** 从暂收生成的数据 */
    public static final String OTHERSTATE_01 = "01";
    
    /** 从实付生成的数据 */
    public static final String OTHERSTATE_02 = "02";
                                                  
    /** 预打发票生成的数据 */                               
    public static final String OTHERSTATE_03 = "03";
                                                  
    /** 期缴的总保费 */                               
    public static final String OTHERSTATE_04 = "04";
                                                  
    /** 约定保费的总保费 */                               
    public static final String OTHERSTATE_05 = "05";
                                                  
    /** 暂收退费 */
    public static final String OTHERSTATE_06 = "06";
                                                  
    /** 暂收退费期缴的总保费 */                               
    public static final String OTHERSTATE_07 = "07";
                                                      
    /** 暂收退费约定保费的总保费 */                                   
    public static final String OTHERSTATE_08 = "08";
                                                      
    /** 暂收退费共保保单 */                                   
    public static final String OTHERSTATE_09 = "09";
                                                      
    /** 共保保单 */                                   
    public static final String OTHERSTATE_10 = "10";
                                                      
    /** 万能、特需初始扣费  */
    public static final String OTHERSTATE_11 = "11";
                                                      
    /** 万能、特需（保全）初始扣费  */
    public static final String OTHERSTATE_12 = "12";
    
    
    
    /**保费收入 */
    public static final String BUSITYPE_01 = "01";
    /**管理费收入 */
    public static final String BUSITYPE_02 = "02";
    /**部分领取手续费 */
    public static final String BUSITYPE_03 = "03";
    /**初始扣费 */
    public static final String BUSITYPE_04 = "04";
    /**解约管理费 */
    public static final String BUSITYPE_05 = "05";
    /**退保费用 */
    public static final String BUSITYPE_06 = "06";
    /**帐户管理费 */
    public static final String BUSITYPE_07 = "07";
    /**健康保险咨询服务 */
    public static final String BUSITYPE_08 = "08";
    /**工本费 */
    public static final String BUSITYPE_09 = "09";
    /**交叉销售业务收入 */
    public static final String BUSITYPE_10 = "10";
    /**利息收入 */
    public static final String BUSITYPE_11 = "11";
    /**金融商品转让 */
    public static final String BUSITYPE_12 = "12";    
    /**健康管理物品销售 */
    public static final String BUSITYPE_13 = "13";
    /**出租-简易 */
    public static final String BUSITYPE_14 = "14";
    /**出租-一般 */
    public static final String BUSITYPE_15 = "15";
    /**销售固定资产-简易 */
    public static final String BUSITYPE_16 = "16";
    /**销售固定资产-一般 */
    public static final String BUSITYPE_17 = "17";
    /**销售使用过的物品 */
    public static final String BUSITYPE_18 = "18";
    /**保单管理费 */
    public static final String BUSITYPE_19 = "19";
    /**查勘服务费 */
    public static final String BUSITYPE_20 = "20";
    /**健康管理服务 */
    public static final String BUSITYPE_21 = "21";
    
    /***
     * 获取收支类型
     * @param feeoperationtype
     * @param feefinatype
     * @param riskcode
     * @return 收支类型：BusiType
     */
	public String getBusinType(String edorType,String feeFinaType,String riskcode){
		//利息收入
		if("RFLX".equals(feeFinaType) || "FXLX".equals(feeFinaType)){
			return BUSITYPE_11;
		}
		
		//工本费
		if(BQ.FEEFINATYPE_GB.equals(feeFinaType)){
			return BUSITYPE_09;
		}
		
		//管理费、保单管理费
		if(BQ.FEEFINATYPE_MF.equals(feeFinaType)){
			//委托管理险：02-管理费收入		个、团万能险、新老特需： 19-保单管理费
			if(BQ.EDORTYPE_ZB.equals(edorType)){
				if(riskUliList.contains(riskcode)){
					return BUSITYPE_04;
				}
				if(riskWtList.contains(riskcode) || riskOldTxList.contains(riskcode)){
					return BUSITYPE_02;
				}
				if(riskNewTxList.contains(riskcode)){
					return BUSITYPE_07;
				}
				//委托管理追加管理费
			}else if(BQ.EDORTYPE_ZG.equals(edorType)){
				if(riskWtList.contains(riskcode)){
					return BUSITYPE_02;
				}
				//部分领取
			}else if (BQ.EDORTYPE_LQ.equals(edorType)){
				if(riskOldTxList.contains(riskcode) || riskWtList.contains(riskcode)){
					return BUSITYPE_02;
				}else if(riskNewTxList.contains(riskcode) || riskUliList.contains(riskcode)){
					return BUSITYPE_03;
				}
				//团险万能追加保费
			}else if(BQ.EDORTYPE_TY.equals(edorType)){
				return BUSITYPE_04;
			}
			//账户轨迹等记录
//			if(null == edorType || "".equals(edorType)){
				if(riskUliList.contains(riskcode)){
					return BUSITYPE_19;
				}else if(riskWtList.contains(riskcode)){
					return BUSITYPE_02;
				}else if(riskOldTxList.contains(riskcode)){
					return BUSITYPE_07;
				}else if(riskNewTxList.contains(riskcode)){
					return BUSITYPE_06;
				}
//			}
		}
		//部分领取
		if (BQ.EDORTYPE_LQ.equals(edorType) && BQ.EDORTYPE_LQ.equals(feeFinaType)){
			if(riskOldTxList.contains(riskcode) || riskWtList.contains(riskcode)){
				return BUSITYPE_02;
			}else if(riskNewTxList.contains(riskcode) || riskUliList.contains(riskcode)){
				return BUSITYPE_03;
			}
		}
		
		if(BQ.FEEFINATYPE_BF.equals(feeFinaType) 
				|| BQ.FEEFINATYPE_TF.equals(feeFinaType)
				|| BQ.FEEFINATYPE_TB.equals(feeFinaType)){//TB/TF的收支类型应与BF对应
			//健康管理产品
			if(riskHealthList.contains(riskcode)){
				return BUSITYPE_08;
			}
			//健康守望
			if(riskJKSWList.contains(riskcode)){
				return BUSITYPE_21;
			}
		}
		
		//初始扣费
		if("GL".equals(feeFinaType) || "KF".equals(feeFinaType)){
			if(riskUliList.contains(riskcode)){
				return BUSITYPE_04;
			}
			if(riskOldTxList.contains(riskcode) || riskNewTxList.contains(riskcode)){
				return BUSITYPE_07;
			}
			if(riskWtList.contains(riskcode) ){
				return BUSITYPE_02;
			}
		}
		if(riskUliList.contains(riskcode)){
			if((BQ.FEEFINATYPE_TF.equals(feeFinaType) && "WX".equals(edorType)) 
					|| (BQ.FEEFINATYPE_MF.equals(feeFinaType) && "TY".equals(edorType))){
				return BUSITYPE_04;
			}
		}
		//解约管理费
		if("TM".equals(feeFinaType)){
			return BUSITYPE_06;
		}
		
		return BUSITYPE_01;
	}
	
	public static void main(String[] args) {
		
	}

}
