package com.sinosoft.lis.ygz;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LYPremSeparateDetailSchema;
import com.sinosoft.lis.vschema.LYPremSeparateDetailSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 
 * @author add by liyt 2016-06-24
 * 提取实收交费表数据
 *
 */
public class GetFCSeparateBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	/** 数据操作字符串 */
	private String mOperate;
	/** 开始日期*/
	private String mStartDate;
	/** 结束日期*/
	private String mEndDate;
	/** 印刷号*/
	private String mContNo;
	//业务处理相关变量
	private MMap mMap = new MMap();
	
	/** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
	
	private String mCurrentDate = PubFun.getCurrentDate();
	private String mCurrentTime = PubFun.getCurrentTime();
	public boolean submitData(VData cInputData, String cOperate) {
		System.out.println(mCurrentDate+":开始提取契约价税分离数据。");
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		// 进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GetFCSeparateBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败GetFCSeparateBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}

		// 准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}

		// 保存数据
		PubSubmit tPubSubmit = new PubSubmit();

		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			System.out.print("保存数据返回");
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "GetFCSeparateBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mInputData = null;
		System.out.println(mCurrentDate+":提取契约价税分离数据结束。");
		return true;
	}

	// 获得输出数据
	private boolean getInputData(VData cInputData) {
		TransferData tTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
		mStartDate = (String) tTransferData.getValueByName("StartDate");
		mEndDate = (String) tTransferData.getValueByName("EndDate");
		mContNo = (String) tTransferData.getValueByName("ContNo");
		return true;
	}
	
//  业务处理
	private boolean dealData() {
		LYPremSeparateDetailSet tLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
		ExeSQL tExeSQL = new ExeSQL();
		String tWhereSQL = "";
		String tSFWhereSQL = "";
		String tWhereContSQL = "";
		String tWhereGrpContSQL = "";
		if(mStartDate != null && !"".equals(mStartDate)){
			tWhereSQL += " and enteraccdate >= '2016-05-01' and confdate >= '" + mStartDate + "'";
		}
		if(mEndDate != null && !"".equals(mEndDate)){
			tWhereSQL += " and enteraccdate >= '2016-05-01' and confdate <= '" + mEndDate + "'";
		}
		if("".equals(tWhereSQL)&&("".equals(mContNo)||mContNo == null)){
			tWhereSQL += " and enteraccdate >= '2016-05-01' and confdate >= current date - 7 day ";
		}
		if(mContNo != null && !"".equals(mContNo)){
			tWhereContSQL += " and lja.contno = '" + mContNo + "'";
			tWhereGrpContSQL += " and lja.grpcontno = '" + mContNo + "'";
		}
		
		if(mStartDate != null && !"".equals(mStartDate)){
			tSFWhereSQL += " and lja.makedate >= '2016-05-01' and confdate >= '" + mStartDate + "'";
		}
		if(mEndDate != null && !"".equals(mEndDate)){
			tSFWhereSQL += " and lja.makedate >= '2016-05-01' and confdate <= '" + mEndDate + "'";
		}
		if("".equals(tWhereSQL)){
			tSFWhereSQL += " and lja.makedate >= '2016-05-01' and confdate >= current date - 7 day ";
		}
		
		/**
		 * 实收表冲负数据--个单
		 */
		String tSQL = " select *"
					+ " from ( "
					+ " select "
                    + " trim(lja.PayNo)||trim(lja.RiskCode)||'FC' as busino,"
					+ " (select prtno from lccont where contno = lja.contno union select prtno from lbcont where contno = lja.contno union select prtno from lobcont where contno = lja.contno) as PrtNo,"
					+ " lja.contno as PolicyNo, lja.riskcode,lja.managecom,"
					+ " sum(sumactupaymoney) as paymoney,"
					+ " lja.PayNo as MoneyNo"
					+ " from ljapayperson lja "
					+ " where lja.grpcontno = '00000000000000000000' "
					+ " and lja.sumactupaymoney < 0 "
					+ " and lja.paytype = 'ZC' "
					+ " and trim(lja.PayNo)||trim(lja.RiskCode)||'FC' not in (select busino from LYPremSeparateDetail) "
					+ tWhereSQL
					+ tWhereContSQL
					+ " group by lja.payno,lja.riskcode,lja.contno,lja.managecom"
					+ " ) as PayPerSonData "
					+ " where policyno not in(select otherno from ljtempfee where tempfeetype = '17' and otherno = policyno)"
					+ " ";
		SSRS tGPayPerSonSSRS = tExeSQL.execSQL(tSQL);
		if(tGPayPerSonSSRS != null && tGPayPerSonSSRS.getMaxRow() > 0){
			System.out.println("实收表冲负数据--个单==="+tGPayPerSonSSRS.getMaxRow());
			for (int i = 1; i <= tGPayPerSonSSRS.getMaxRow(); i++) {
				LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
				tLYPremSeparateDetailSchema.setBusiNo(tGPayPerSonSSRS.GetText(i, 1));
				tLYPremSeparateDetailSchema.setOtherNo(tGPayPerSonSSRS.GetText(i, 3));
				tLYPremSeparateDetailSchema.setOtherNoType("2");
				tLYPremSeparateDetailSchema.setPrtNo(tGPayPerSonSSRS.GetText(i, 2));
				tLYPremSeparateDetailSchema.setPolicyNo(tGPayPerSonSSRS.GetText(i, 3));
				tLYPremSeparateDetailSchema.setRiskCode(tGPayPerSonSSRS.GetText(i, 4));
				tLYPremSeparateDetailSchema.setManageCom(tGPayPerSonSSRS.GetText(i, 5));
				tLYPremSeparateDetailSchema.setPayMoney(tGPayPerSonSSRS.GetText(i, 6));
				tLYPremSeparateDetailSchema.setMoneyNo(tGPayPerSonSSRS.GetText(i, 7));
				tLYPremSeparateDetailSchema.setBusiType("01");
				tLYPremSeparateDetailSchema.setState("01");
				tLYPremSeparateDetailSchema.setOtherState("01");//从实收表生成的负数据
				tLYPremSeparateDetailSchema.setMoneyType("01");
				tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
				tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
				tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
			}
			
		}else{
			System.out.println("实收表冲负数据--个单数据为0！");
		}
		
		/**
		 * 实收表冲负数据--团单
		 */
		tSQL = " select * " 
					+ " from ( "
					+ " select "
                    + " trim(lja.PayNo)||trim(lja.RiskCode)||'FC' as busino,"
					+ " (select prtno from lcgrpcont where  grpcontno = lja.grpcontno union select prtno from lbgrpcont where  grpcontno = lja.grpcontno union select prtno from lbgrpcont where  grpcontno = lja.grpcontno) as PrtNo,"
					+ " lja.grpcontno as PolicyNo,lja.riskcode,lja.managecom,"
					+ " sum(sumactupaymoney) as paymoney, "
					+ " lja.PayNo as MoneyNo"
					+ " from ljapayperson lja "
					+ " where lja.grpcontno != '00000000000000000000' "
					+ " and lja.sumactupaymoney < 0 "
					+ " and lja.paytype = 'ZC' "
					+ " and trim(lja.PayNo)||trim(lja.RiskCode)||'FC' not in (select busino from LYPremSeparateDetail) "
					+ tWhereSQL
					+ tWhereGrpContSQL
					+ " group by lja.payno,lja.riskcode,lja.grpcontno,lja.managecom"
					+ " ) PayPerSonData "
					+ " where 1=1 "
					+ " ";
		SSRS tTPayPerSonSSRS = tExeSQL.execSQL(tSQL);
		if(tTPayPerSonSSRS != null && tTPayPerSonSSRS.getMaxRow() > 0){
			System.out.println("实收表冲负数据--团单==="+tTPayPerSonSSRS.getMaxRow());
			for (int i = 1; i <= tTPayPerSonSSRS.getMaxRow(); i++) {
				LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
				tLYPremSeparateDetailSchema.setBusiNo(tTPayPerSonSSRS.GetText(i, 1));
				tLYPremSeparateDetailSchema.setOtherNo(tTPayPerSonSSRS.GetText(i, 3));
				tLYPremSeparateDetailSchema.setOtherNoType("7");
				tLYPremSeparateDetailSchema.setPrtNo(tTPayPerSonSSRS.GetText(i, 2));
				tLYPremSeparateDetailSchema.setPolicyNo(tTPayPerSonSSRS.GetText(i, 3));
				tLYPremSeparateDetailSchema.setRiskCode(tTPayPerSonSSRS.GetText(i, 4));
				tLYPremSeparateDetailSchema.setManageCom(tTPayPerSonSSRS.GetText(i, 5));
				tLYPremSeparateDetailSchema.setPayMoney(tTPayPerSonSSRS.GetText(i, 6));
				tLYPremSeparateDetailSchema.setMoneyNo(tTPayPerSonSSRS.GetText(i, 7));
				tLYPremSeparateDetailSchema.setBusiType("01");
				tLYPremSeparateDetailSchema.setState("01");
				String prtno = tTPayPerSonSSRS.GetText(i, 2);
				String gb = tExeSQL.getOneValue("select coinsuranceflag from lcgrpcont where prtno = '"+prtno+"' "
						+ "union select coinsuranceflag from lobgrpcont where prtno = '"+prtno+"' "
						+ "union select coinsuranceflag from lbgrpcont where prtno = '"+prtno+"' "
						 );
				if(gb!=null&&!"".equals(gb)&& gb.equals("1")){
					tLYPremSeparateDetailSchema.setOtherState("09");//反冲共保
				}else{
					tLYPremSeparateDetailSchema.setOtherState("01");//从实收表生成的负数据
				}
				tLYPremSeparateDetailSchema.setMoneyType("01");
				tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
				tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
				tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
			}
			
		}else{
			System.out.println("实收表冲负数据--团单数据为0！");
		}
		
		/**
		 * 实收表CZ数据--个单
		 */
		String tSQLCZ = " select *"
					+ " from ( "
					+ " select "
                    + " trim(lja.PayNo)||trim(lja.RiskCode)||'FCCZ' as busino,"
					+ " (select prtno from lccont where contno = lja.contno union select prtno from lbcont where contno = lja.contno union select prtno from lobcont where contno = lja.contno) as PrtNo,"
					+ " lja.contno as PolicyNo, lja.riskcode,lja.managecom,"
					+ " sum(sumactupaymoney) as paymoney,"
					+ " lja.PayNo as MoneyNo"
					+ " from ljapayperson lja "
					+ " where lja.grpcontno = '00000000000000000000' "
					+ " and lja.sumactupaymoney > 0 "
					+ " and lja.paytype = 'ZC' "
					+ " and trim(lja.PayNo)||trim(lja.RiskCode)||'FCCZ' not in (select busino from LYPremSeparateDetail) "
					+ " and lja.payno not in(select moneyno from LYPremSeparateDetail lyp where lyp.prtno = (select prtno from lccont where contno = lja.contno union select prtno from lbcont where contno = lja.contno union select prtno from lobcont where contno = lja.contno))"
					+ " and exists (select * from ljapayperson where sumactupaymoney < 0 and contno = lja.contno)"
					+ tWhereSQL
					+ tWhereContSQL
					+ " group by lja.payno,lja.riskcode,lja.contno,lja.managecom"
					+ " ) as PayPerSonData "
					+ " where policyno not in(select otherno from ljtempfee where tempfeetype = '17' and otherno = policyno)"
					;
		SSRS tGPayPerSonSSRSCZ = tExeSQL.execSQL(tSQLCZ);
		if(tGPayPerSonSSRSCZ != null && tGPayPerSonSSRSCZ.getMaxRow() > 0){
			System.out.println("实收表冲正数据--个单==="+tGPayPerSonSSRSCZ.getMaxRow());
			for (int i = 1; i <= tGPayPerSonSSRSCZ.getMaxRow(); i++) {
				LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
				tLYPremSeparateDetailSchema.setBusiNo(tGPayPerSonSSRSCZ.GetText(i, 1));
				tLYPremSeparateDetailSchema.setOtherNo(tGPayPerSonSSRSCZ.GetText(i, 3));
				tLYPremSeparateDetailSchema.setOtherNoType("2");
				tLYPremSeparateDetailSchema.setPrtNo(tGPayPerSonSSRSCZ.GetText(i, 2));
				tLYPremSeparateDetailSchema.setPolicyNo(tGPayPerSonSSRSCZ.GetText(i, 3));
				tLYPremSeparateDetailSchema.setRiskCode(tGPayPerSonSSRSCZ.GetText(i, 4));
				tLYPremSeparateDetailSchema.setManageCom(tGPayPerSonSSRSCZ.GetText(i, 5));
				tLYPremSeparateDetailSchema.setPayMoney(tGPayPerSonSSRSCZ.GetText(i, 6));
				tLYPremSeparateDetailSchema.setMoneyNo(tGPayPerSonSSRSCZ.GetText(i, 7));
				tLYPremSeparateDetailSchema.setBusiType("01");
				tLYPremSeparateDetailSchema.setState("01");
				tLYPremSeparateDetailSchema.setOtherState("01");//从实收表生成的负数据
				tLYPremSeparateDetailSchema.setMoneyType("01");
				tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
				tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
				tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
			}
			
		}else{
			System.out.println("实收表冲正数据--个单数据为0！");
		}
		
		/**
		 * 实收表冲正数据--团单
		 */
		tSQL = " select * " 
					+ " from ( "
					+ " select "
                    + " trim(lja.PayNo)||trim(lja.RiskCode)||'FCCZ' as busino,"
					+ " (select prtno from lcgrpcont where  grpcontno = lja.grpcontno union select prtno from lbgrpcont where  grpcontno = lja.grpcontno union select prtno from lbgrpcont where  grpcontno = lja.grpcontno) as PrtNo,"
					+ " lja.grpcontno as PolicyNo,lja.riskcode,lja.managecom,"
					+ " sum(sumactupaymoney) as paymoney, "
					+ " lja.PayNo as MoneyNo"
					+ " from ljapayperson lja "
					+ " where lja.grpcontno != '00000000000000000000' "
					+ " and lja.sumactupaymoney > 0 "
					+ " and lja.paytype = 'ZC' "
					+ " and trim(lja.PayNo)||trim(lja.RiskCode)||'FCCZ' not in (select busino from LYPremSeparateDetail) "
					+ " and lja.payno not in(select moneyno from LYPremSeparateDetail lyp where lyp.prtno = (select prtno from lcgrpcont where grpcontno = lja.grpcontno union select prtno from lbgrpcont where  grpcontno = lja.grpcontno union select prtno from lbgrpcont where  grpcontno = lja.grpcontno) )"
					+ " and exists (select * from ljapayperson where sumactupaymoney < 0 and grpcontno = lja.grpcontno)"
					+ tWhereSQL
					+ tWhereGrpContSQL
					+ " group by lja.payno,lja.riskcode,lja.grpcontno,lja.managecom"
					+ " ) PayPerSonData "
					+ " where 1=1 "
					+ " ";
		SSRS tTPayPerSonSSRSCZ = tExeSQL.execSQL(tSQL);
		if(tTPayPerSonSSRSCZ != null && tTPayPerSonSSRSCZ.getMaxRow() > 0){
			System.out.println("实收表冲正数据--团单==="+tTPayPerSonSSRSCZ.getMaxRow());
			for (int i = 1; i <= tTPayPerSonSSRSCZ.getMaxRow(); i++) {
				LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
				tLYPremSeparateDetailSchema.setBusiNo(tTPayPerSonSSRSCZ.GetText(i, 1));
				tLYPremSeparateDetailSchema.setOtherNo(tTPayPerSonSSRSCZ.GetText(i, 3));
				tLYPremSeparateDetailSchema.setOtherNoType("7");
				tLYPremSeparateDetailSchema.setPrtNo(tTPayPerSonSSRSCZ.GetText(i, 2));
				tLYPremSeparateDetailSchema.setPolicyNo(tTPayPerSonSSRSCZ.GetText(i, 3));
				tLYPremSeparateDetailSchema.setRiskCode(tTPayPerSonSSRSCZ.GetText(i, 4));
				tLYPremSeparateDetailSchema.setManageCom(tTPayPerSonSSRSCZ.GetText(i, 5));
				tLYPremSeparateDetailSchema.setPayMoney(tTPayPerSonSSRSCZ.GetText(i, 6));
				tLYPremSeparateDetailSchema.setMoneyNo(tTPayPerSonSSRSCZ.GetText(i, 7));
				tLYPremSeparateDetailSchema.setBusiType("01");
				tLYPremSeparateDetailSchema.setState("01");
				String prtno = tTPayPerSonSSRSCZ.GetText(i, 2);
				String gb = tExeSQL.getOneValue("select coinsuranceflag from lcgrpcont where prtno = '"+prtno+"' "
						+ "union select coinsuranceflag from lobgrpcont where prtno = '"+prtno+"' "
						+ "union select coinsuranceflag from lbgrpcont where prtno = '"+prtno+"' "
						 );
				if(gb!=null&&!"".equals(gb)&& gb.equals("1")){
					tLYPremSeparateDetailSchema.setOtherState("09");//反冲共保
				}else{
					tLYPremSeparateDetailSchema.setOtherState("01");//从实收表生成的负数据
				}
				tLYPremSeparateDetailSchema.setMoneyType("01");
				tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
				tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
				tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
			}
			
		}else{
			System.out.println("实收表冲正数据--团单数据为0！");
		}
		
		
//		/**
//		 * 实付表数据--个单
//		 * modify liyt 2016-06-29 取消实付数据提取
//		 */
//		tSQL = " select *"
//					+ " from ( "
//					+ " select "
//                    + " trim(lja.actugetno)||trim(ljd.RiskCode)||'FC' as busino,"
//					+ " (select prtno from lccont where contno = ljd.contno union select prtno from lbcont where contno = ljd.contno) as PrtNo,"
//					+ " ljd.contno as PolicyNo, ljd.riskcode,ljd.managecom,"
//					+ " sum(getmoney) as paymoney,"
//					+ " lja.actugetno as MoneyNo"
//					+ " from ljaget lja,ljagetendorse ljd "
//					+ " where lja.actugetno=ljd.actugetno "
//					+ " and lja.otherno=ljd.endorsementno "
//					+ " and lja.othernotype='10' "
//					+ " and ljd.grpcontno = '00000000000000000000' "
//					+ " and lja.sumgetmoney <= 0 "
//					+ " and ljd.feeoperationtype not in ('YEL','YEO') "
//					+ " and trim(lja.actugetno)||trim(ljd.RiskCode)||'FC' not in (select busino from LYPremSeparateDetail) "
//					+ tSFWhereSQL
//					+ " group by lja.actugetno,ljd.endorsementno,ljd.riskcode,ljd.contno,ljd.managecom"
//					+ " ) as PayPerSonData "
//					+ tWhereTSQL
//					+ " ";
//		SSRS tGGetenDorseSSRS = tExeSQL.execSQL(tSQL);
//		if(tGGetenDorseSSRS != null && tGGetenDorseSSRS.getMaxRow() > 0){
//			System.out.println("实付表数据--个单==="+tGGetenDorseSSRS.getMaxRow());
//			for (int i = 1; i <= tGGetenDorseSSRS.getMaxRow(); i++) {
//				LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
//				tLYPremSeparateDetailSchema.setBusiNo(tGGetenDorseSSRS.GetText(i, 1));
//				tLYPremSeparateDetailSchema.setOtherNo(tGGetenDorseSSRS.GetText(i, 3));
//				tLYPremSeparateDetailSchema.setOtherNoType("10");
//				tLYPremSeparateDetailSchema.setPrtNo(tGGetenDorseSSRS.GetText(i, 2));
//				tLYPremSeparateDetailSchema.setPolicyNo(tGGetenDorseSSRS.GetText(i, 3));
//				tLYPremSeparateDetailSchema.setRiskCode(tGGetenDorseSSRS.GetText(i, 4));
//				tLYPremSeparateDetailSchema.setManageCom(tGGetenDorseSSRS.GetText(i, 5));
//				tLYPremSeparateDetailSchema.setPayMoney(tGGetenDorseSSRS.GetText(i, 6));
//				tLYPremSeparateDetailSchema.setMoneyNo(tGGetenDorseSSRS.GetText(i, 7));
//				tLYPremSeparateDetailSchema.setBusiType("01");
//				tLYPremSeparateDetailSchema.setState("01");
//				tLYPremSeparateDetailSchema.setOtherState("16");//从实付表生成的负数据
//				tLYPremSeparateDetailSchema.setMoneyType("01");
//				tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
//				tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
//				tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
//				tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
//				tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
//			}
//			
//		}else{
//			System.out.println("实付表数据--个单数据为0！");
//		}
//		
//		/**
//		 * 实付表数据--团单
//		 * modify liyt 2016-06-29 取消实付数据提取
//		 */
//		tSQL = " select *"
//					+ " from ( "
//					+ " select "
//                    + " trim(lja.actugetno)||trim(ljd.RiskCode)||'FC' as busino,"
//					+ " (select prtno from lcgrpcont where grpcontno = ljd.grpcontno union select prtno from lbgrpcont where grpcontno = ljd.grpcontno) as PrtNo,"
//					+ " ljd.grpcontno as PolicyNo, ljd.riskcode,ljd.managecom,"
//					+ " sum(getmoney) as paymoney,"
//					+ " lja.actugetno as MoneyNo"
//					+ " from ljaget lja,ljagetendorse ljd "
//					+ " where lja.actugetno=ljd.actugetno "
//					+ " and lja.otherno=ljd.endorsementno "
//					+ " and lja.othernotype='3' "
//					+ " and ljd.grpcontno != '00000000000000000000' "
//					+ " and lja.sumgetmoney <= 0 "
//					+ " and ljd.feeoperationtype not in ('YEL','YEO') "
//					+ " and trim(lja.actugetno)||trim(ljd.RiskCode)||'FC' not in (select busino from LYPremSeparateDetail) "
//					+ tSFWhereSQL
//					+ " group by lja.actugetno,ljd.endorsementno,ljd.riskcode,ljd.grpcontno,ljd.managecom"
//					+ " ) as PayPerSonData "
//					+ tWhereTSQL
//					+ " ";
//		SSRS tTGetenDorseSSRS = tExeSQL.execSQL(tSQL);
//		if(tTGetenDorseSSRS != null && tTGetenDorseSSRS.getMaxRow() > 0){
//			System.out.println("实付表数据--团单==="+tTGetenDorseSSRS.getMaxRow());
//			for (int i = 1; i <= tTGetenDorseSSRS.getMaxRow(); i++) {
//				LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
//				tLYPremSeparateDetailSchema.setBusiNo(tTGetenDorseSSRS.GetText(i, 1));
//				tLYPremSeparateDetailSchema.setOtherNo(tTGetenDorseSSRS.GetText(i, 3));
//				tLYPremSeparateDetailSchema.setOtherNoType("3");
//				tLYPremSeparateDetailSchema.setPrtNo(tTGetenDorseSSRS.GetText(i, 2));
//				tLYPremSeparateDetailSchema.setPolicyNo(tTGetenDorseSSRS.GetText(i, 3));
//				tLYPremSeparateDetailSchema.setRiskCode(tTGetenDorseSSRS.GetText(i, 4));
//				tLYPremSeparateDetailSchema.setManageCom(tTGetenDorseSSRS.GetText(i, 5));
//				tLYPremSeparateDetailSchema.setPayMoney(tTGetenDorseSSRS.GetText(i, 6));
//				tLYPremSeparateDetailSchema.setMoneyNo(tTGetenDorseSSRS.GetText(i, 7));
//				tLYPremSeparateDetailSchema.setBusiType("01");
//				tLYPremSeparateDetailSchema.setState("01");
//				tLYPremSeparateDetailSchema.setOtherState("16");//从实付表生成的负数据
//				tLYPremSeparateDetailSchema.setMoneyType("01");
//				tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
//				tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
//				tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
//				tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
//				tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
//			}
//			
//		}else{
//			System.out.println("实付表数据--团单数据为0！");
//		}
		
		mMap.put(tLYPremSeparateDetailSet, SysConst.INSERT);
		
		return true;
	}
	/**
	 * 准备后台的数据
	 * @return boolean
	 */
	private boolean prepareOutputData() {
		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GetFCSeparateBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
	
	public static void main(String[] args) {
		GetFCSeparateBL tGetFCSeparateBL = new GetFCSeparateBL();
		VData cInputData = new VData();
		TransferData tTransferData = new TransferData();
//		String tStartDate = "";
//		String tEndDate = "";
//		String tContNo = "";
//		tTransferData.setNameAndValue("StartDate", tStartDate);
//		tTransferData.setNameAndValue("EndDate", tEndDate);
//		tTransferData.setNameAndValue("ContNo", tContNo);
		cInputData.add(tTransferData);
		tGetFCSeparateBL.submitData(cInputData, "");
	}
}
