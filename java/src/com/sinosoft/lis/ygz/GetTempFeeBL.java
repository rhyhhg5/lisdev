package com.sinosoft.lis.ygz;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LYPremSeparateDetailSchema;
import com.sinosoft.lis.tb.GrpPayPlanDetailBL;
import com.sinosoft.lis.vschema.LCGrpPayPlanDetailSet;
import com.sinosoft.lis.vschema.LYPremSeparateDetailSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class GetTempFeeBL {
	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	/** 数据操作字符串 */
	private String mOperate;
	/** 开始日期*/
	private String mStartDate;
	/** 结束日期*/
	private String mEndDate;
	/** 印刷号*/
	private String mPrtNo;
	//业务处理相关变量
	private MMap mMap = new MMap();
	
	/** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
	
	private String mCurrentDate = PubFun.getCurrentDate();
	private String mCurrentTime = PubFun.getCurrentTime();
	private int sysCount = 5000;//默认每一千条数据发送一次请求
	
	public boolean submitData(VData cInputData, String cOperate) {
		System.out.println(mCurrentDate+":开始提取契约价税分离数据。");
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		// 进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GetTempFeeSeparateBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败GetTempFeeSeparateBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}
		System.out.println(mCurrentDate+":提取契约价税分离数据结束。");
		return true;
	}

	// 获得输出数据
	private boolean getInputData(VData cInputData) {
		TransferData tTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
		mStartDate = 
				(String) tTransferData.getValueByName("StartDate");
		mEndDate = 
				(String) tTransferData.getValueByName("EndDate");
		mPrtNo = (String) tTransferData.getValueByName("PrtNo");
		return true;
	}
	
//  业务处理
	private boolean dealData() {
		LYPremSeparateDetailSet tLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
		ExeSQL tExeSQL = new ExeSQL();
		String tWhereSQL = "";
		String tWhereGSQL = "";
		String tWhereTSQL = "";
		if(mStartDate != null && !"".equals(mStartDate)){
			tWhereSQL += " and confmakedate >= '" + mStartDate + "'";
		}
		if(mEndDate != null && !"".equals(mEndDate)){
			tWhereSQL += " and confmakedate <= '" + mEndDate + "'";
		}
		if(mPrtNo != null && !"".equals(mPrtNo)){
			tWhereGSQL += " and (ljt.otherno = '" + mPrtNo + "' or ljt.otherno = (select contno from lbcont where edorno not like 'xb%' and conttype = '1' and prtno = '"+mPrtNo+"' union select contno from lobcont where conttype = '1' and prtno = '"+mPrtNo+"' union select contno from lccont where conttype = '1' and prtno = '"+mPrtNo+"' fetch first rows only))";
			tWhereTSQL += " and (ljt.otherno = '" + mPrtNo + "' or ljt.otherno = (select grpcontno from lbgrpcont where edorno not like 'xb%' and prtno = '"+mPrtNo+"' union select grpcontno from lobgrpcont where prtno = '"+mPrtNo+"' union select grpcontno from lcgrpcont where prtno = '"+mPrtNo+"' fetch first rows only))";
		}
		if("".equals(tWhereSQL) && "".equals(tWhereGSQL) && "".equals(tWhereTSQL)){
			tWhereSQL += "and confmakedate >= current date - 7 day ";
		}
		
		String tTempfeetypeg = "";
		String tTempfeetypet = "";
		String tFlag = "1";
		String tTempfeetype = " select code,othersign from ldcode where codetype='ygzqyttype' ";
		SSRS tTempfeetypeSSRS = tExeSQL.execSQL(tTempfeetype);
		if(tTempfeetypeSSRS != null && tTempfeetypeSSRS.getMaxRow()>0){
			for(int i=1;i<tTempfeetypeSSRS.getMaxRow();i++){
				if(tTempfeetypeSSRS.GetText(i, 2).equals("1")){
					tTempfeetypeg += "'"+ tTempfeetypeSSRS.GetText(i, 1) +"',";
				}else if(tTempfeetypeSSRS.GetText(i, 2).equals("2")){
					tTempfeetypet += "'"+ tTempfeetypeSSRS.GetText(i, 1) +"',";
				}else if(tTempfeetypeSSRS.GetText(i, 2).equals("3")){
					tTempfeetypeg += "'"+ tTempfeetypeSSRS.GetText(i, 1) +"',";
					tTempfeetypet += "'"+ tTempfeetypeSSRS.GetText(i, 1) +"',";
				}
			}
		}
		tTempfeetypeg = tTempfeetypeg.substring(0,tTempfeetypeg.length()-1);
		tTempfeetypet = tTempfeetypet.substring(0,tTempfeetypet.length()-1);
				
		/**
		 * 从暂收表发起--个单数据
		 */
		String tTempDataSQL = " select distinct ljt.TempFeeNo,ljt.TempFeeType,ljt.OtherNo,ljt.OtherNoType "
					+ " from ljtempfee ljt "
					+ " where 1=1 "
					+ " and ljt.paymoney <> 0"
					+ " and ljt.tempfeetype in (" + tTempfeetypeg + ") "
					+ " and ljt.othernotype in ('2','4') "
					+ " and not exists (select 1 from LYPremSeparateDetail a where a.tempfeeno = ljt.tempfeeno) "
					+ tWhereSQL
					+ tWhereGSQL
					+ " ";
		SSRS tTempDataSSRS = tExeSQL.execSQL(tTempDataSQL);
		if(tTempDataSSRS != null && tTempDataSSRS.getMaxRow()>0){
			System.out.println("====  暂收表个单数据： "+tTempDataSSRS.getMaxRow());
			for (int i = 1; i <= tTempDataSSRS.getMaxRow(); i++) {
				tFlag = "1";
				String tempBusiNo = tTempDataSSRS.GetText(i, 1) + tTempDataSSRS.GetText(i, 2);
				String tOtherNo = tTempDataSSRS.GetText(i, 3);
				String mTempfeeno = tTempDataSSRS.GetText(i, 1);
//				暂收个单数据，并调整取数逻辑，关联保单层，考虑保单层与险种层状态不一致的情况。by chengzhou  modifydate 2016-07-29
				String tPolSQL  = "";
				SSRS tPolSSRS = new SSRS();
				tPolSQL = " "
					    + " select '"+ tempBusiNo + "'|| lcp.riskcode || lcp.polno, "
					    + " lcp.prtno,lcp.contno,lcp.riskcode,lcp.managecom,sum(lcp.prem), sum(nvl(lcp.supplementaryprem,0)), lcp.polno "
					    + " from lccont lcc inner join lcpol lcp on lcp.prtno = lcc.prtno "
					    + " where 1=1 "
					    + " and lcc.conttype = '1' "
					    + " and lcp.conttype = '1' "
					    + " and (lcc.uwflag = 'a' or (lcc.uwflag != 'a' and lcp.uwflag in ('3','4','9'))) "
					    + " and (lcp.prtno = '"+tOtherNo+"' or lcp.contno = '"+tOtherNo+"' )"
					    + " and not exists(select 1 from LYPremSeparateDetail where busino='"+ tempBusiNo + "'|| lcp.riskcode || lcp.polno)"
					    + " group by lcp.prtno,lcp.contno,lcp.riskcode,lcp.ManageCom,lcp.polno";
			    tPolSSRS = tExeSQL.execSQL(tPolSQL);
				if(tPolSSRS == null || tPolSSRS.getMaxRow() == 0){
					tFlag="2";
					tPolSQL = " "
						    + " select '"+ tempBusiNo + "'|| lcp.riskcode || lcp.polno, "
						    + " lcp.prtno,lcp.contno,lcp.riskcode,lcp.managecom,sum(lcp.prem), sum(lcp.supplementaryprem),lcp.polno "
						    + " from lbpol lcp "
						    + " where 1=1 "
						    + " and lcp.conttype = '1' "
						    + " and lcp.uwflag in ('3','4','9') "
						    + " and lcp.edorno not like 'xb%' "
						    + " and (lcp.prtno = '"+tOtherNo+"' or lcp.contno = '"+tOtherNo+"' )"
						    + " and not exists(select 1 from LYPremSeparateDetail where busino='"+ tempBusiNo + "'|| lcp.riskcode || lcp.polno)"
						    + " group by lcp.prtno,lcp.contno,lcp.riskcode,lcp.ManageCom,lcp.polno ";
					tPolSSRS = tExeSQL.execSQL(tPolSQL);
					if(tPolSSRS == null || tPolSSRS.getMaxRow() == 0){
						tFlag="3";
						tPolSQL = " "
							    + " select '"+ tempBusiNo + "'|| lcp.riskcode || lcp.polno, "
							    + " lcp.prtno,lcp.contno,lcp.riskcode,lcp.managecom,sum(lcp.prem), sum(lcp.supplementaryprem),lcp.polno "
							    + " from lobpol lcp "
							    + " where 1=1 "
							    + " and lcp.conttype = '1' "
							    + " and lcp.uwflag in ('3','4','9') "
							    + " and (lcp.prtno = '"+tOtherNo+"' or lcp.contno = '"+tOtherNo+"' )"
							    + " and not exists(select 1 from LYPremSeparateDetail where busino='"+ tempBusiNo + "'|| lcp.riskcode || lcp.polno)"
							    + " group by lcp.prtno,lcp.contno,lcp.riskcode,lcp.ManageCom,lcp.polno ";
						tPolSSRS = tExeSQL.execSQL(tPolSQL);
					}
				}
				
				SSRS ybtSSRS = new SSRS();
				if(tPolSSRS != null && tPolSSRS.getMaxRow() > 0){
					String ybtSQL = "select riskcode from ljtempfee where tempfeetype ='17' and paymoney < 0 and tempfeeno ='"+mTempfeeno+"'";
					ybtSSRS = new ExeSQL().execSQL(ybtSQL);
					System.out.println("===暂收个险数据==="+tPolSSRS.getMaxRow());
					for (int j = 1; j <= tPolSSRS.getMaxRow(); j++) {
						LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
						tLYPremSeparateDetailSchema.setBusiNo(tPolSSRS.GetText(j, 1));
						tLYPremSeparateDetailSchema.setTempFeeNo(tTempDataSSRS.GetText(i, 1));
						tLYPremSeparateDetailSchema.setTempFeeType(tTempDataSSRS.GetText(i, 2));
						tLYPremSeparateDetailSchema.setOtherNo(tOtherNo);
						tLYPremSeparateDetailSchema.setOtherNoType(tTempDataSSRS.GetText(i, 4));
						tLYPremSeparateDetailSchema.setPrtNo(tPolSSRS.GetText(j, 2));
						tLYPremSeparateDetailSchema.setPolicyNo(tPolSSRS.GetText(j, 3));
						tLYPremSeparateDetailSchema.setRiskCode(tPolSSRS.GetText(j, 4));
						tLYPremSeparateDetailSchema.setBusiType("01");
						tLYPremSeparateDetailSchema.setManageCom(tPolSSRS.GetText(j, 5));
						if(ybtSSRS != null && ybtSSRS.getMaxRow()>0){
							for(int n = 1; n <= ybtSSRS.getMaxRow();n++){
								if(ybtSSRS.GetText(n, 1).equals(tLYPremSeparateDetailSchema.getRiskCode())){
									String mPayMoney = String.valueOf(Double.valueOf(tPolSSRS.GetText(j, 6))*-1);//银保通反冲数据
								tLYPremSeparateDetailSchema.setPayMoney(mPayMoney);
								}
							}
						}else{
							tLYPremSeparateDetailSchema.setPayMoney(tPolSSRS.GetText(j, 6));
						}
						tLYPremSeparateDetailSchema.setState("01");
						tLYPremSeparateDetailSchema.setOtherState("01");
						tLYPremSeparateDetailSchema.setMoneyType("01");
						tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
						tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
						tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
						tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
						tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
						
						String tSupplementaryprem = tPolSSRS.GetText(j, 7);
						if(tSupplementaryprem !=null && !"".equals(tSupplementaryprem) && Double.parseDouble(tSupplementaryprem) > 0){
							System.out.println("===暂收个险数据追加保费："+tSupplementaryprem);
							LYPremSeparateDetailSchema tZBLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
							tZBLYPremSeparateDetailSchema.setBusiNo("ZB"+tPolSSRS.GetText(j, 1));
							tZBLYPremSeparateDetailSchema.setTempFeeNo(tTempDataSSRS.GetText(i, 1));
							tZBLYPremSeparateDetailSchema.setTempFeeType(tTempDataSSRS.GetText(i, 2));
							tZBLYPremSeparateDetailSchema.setOtherNo(tOtherNo);
							tZBLYPremSeparateDetailSchema.setOtherNoType(tTempDataSSRS.GetText(i, 4));
							tZBLYPremSeparateDetailSchema.setPrtNo(tPolSSRS.GetText(j, 2));
							tZBLYPremSeparateDetailSchema.setPolicyNo(tPolSSRS.GetText(j, 3));
							tZBLYPremSeparateDetailSchema.setRiskCode(tPolSSRS.GetText(j, 4));
							tZBLYPremSeparateDetailSchema.setBusiType("01");
							tZBLYPremSeparateDetailSchema.setManageCom(tPolSSRS.GetText(j, 5));
							tZBLYPremSeparateDetailSchema.setPayMoney(tSupplementaryprem);
							tZBLYPremSeparateDetailSchema.setState("01");
							tZBLYPremSeparateDetailSchema.setOtherState("17");
							tZBLYPremSeparateDetailSchema.setMoneyType("01");
							tZBLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
							tZBLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
							tZBLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
							tZBLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
							tLYPremSeparateDetailSet.add(tZBLYPremSeparateDetailSchema);
						}
					}
				}else{
					System.out.println("===暂收个险数据===0！");
				}
				
//				暂收个单期缴数据
				String tQJPolSQL  = "";
				SSRS tQJPolSSRS = new SSRS();
				if(tFlag.equals("1")){
					tQJPolSQL = " "
						    + " select '"+ tempBusiNo + "'|| lcp.riskcode || lcp.polno, "
						    + " lcp.prtno,lcp.contno,lcp.riskcode,lcp.managecom,sum(lcp.prem), "
						    + " lcp.payintv,timestampdiff (64, char(timestamp(lcp.PayendDate) - timestamp(lcp.Cvalidate))),lcp.polno "
						    + " from lccont lcc inner join lcpol lcp on lcp.prtno = lcc.prtno "
						    + " where 1=1 "
						    + " and lcc.conttype = '1' "
						    + " and lcp.conttype = '1' "
						    + " and (lcc.uwflag = 'a' or (lcc.uwflag != 'a' and lcp.uwflag in ('3','4','9'))) "
						    + " and lcp.payintv != 0 and lcp.payintv != -1 "
						    + " and (lcp.prtno = '"+tOtherNo+"' or lcp.contno = '"+tOtherNo+"' )"
						    + " and not exists(select 1 from LYPremSeparateDetail where busino='QJ"+ tempBusiNo + "'|| lcp.riskcode || lcp.polno)"
						    + " group by lcp.prtno,lcp.contno,lcp.riskcode,lcp.ManageCom,lcp.payintv,lcp.cvalidate,lcp.payenddate,lcp.polno ";
				}else if(tFlag.equals("2")){
					tQJPolSQL = " "
						    + " select '"+ tempBusiNo + "'|| lcp.riskcode || lcp.polno, "
						    + " lcp.prtno,lcp.contno,lcp.riskcode,lcp.managecom,sum(lcp.prem), "
						    + " lcp.payintv,timestampdiff (64, char(timestamp(lcp.PayendDate) - timestamp(lcp.Cvalidate))),lcp.polno "
						    + " from lbpol lcp "
						    + " where 1=1 "
						    + " and lcp.conttype = '1' "
						    + " and lcp.uwflag in ('3','4','9') "
						    + " and lcp.payintv != 0 and lcp.payintv != -1 "
						    + " and lcp.edorno not like 'xb%' "
						    + " and (lcp.prtno = '"+tOtherNo+"' or lcp.contno = '"+tOtherNo+"' )"
						    + " and not exists(select 1 from LYPremSeparateDetail where busino='QJ"+ tempBusiNo + "'|| lcp.riskcode || lcp.polno)"
						    + " group by lcp.prtno,lcp.contno,lcp.riskcode,lcp.ManageCom,lcp.payintv,lcp.cvalidate,lcp.payenddate,lcp.polno ";
				}else if(tFlag.equals("3")){
					tQJPolSQL = " "
						    + " select '"+ tempBusiNo + "'|| lcp.riskcode || lcp.polno, "
						    + " lcp.prtno,lcp.contno,lcp.riskcode,lcp.managecom,sum(lcp.prem), "
						    + " lcp.payintv,timestampdiff (64, char(timestamp(lcp.PayendDate) - timestamp(lcp.Cvalidate))),lcp.polno "
						    + " from lobpol lcp "
						    + " where 1=1 "
						    + " and lcp.conttype = '1' "
						    + " and lcp.uwflag in ('3','4','9') "
						    + " and lcp.payintv != 0 and lcp.payintv != -1 "
						    + " and (lcp.prtno = '"+tOtherNo+"' or lcp.contno = '"+tOtherNo+"' )"
						    + " and not exists(select 1 from LYPremSeparateDetail where busino='QJ"+ tempBusiNo + "'|| lcp.riskcode || lcp.polno)"
						    + " group by lcp.prtno,lcp.contno,lcp.riskcode,lcp.ManageCom,lcp.payintv,lcp.cvalidate,lcp.payenddate,lcp.polno ";
				}
				tQJPolSSRS = tExeSQL.execSQL(tQJPolSQL);
				if(tQJPolSSRS != null && tQJPolSSRS.getMaxRow() > 0){
					System.out.println("===暂收个险期缴数据==="+tQJPolSSRS.getMaxRow());
//					LYPremSeparateDetailSet tempLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
					for (int j = 1; j <= tQJPolSSRS.getMaxRow(); j++) {
						LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
						tLYPremSeparateDetailSchema.setBusiNo("QJ"+tQJPolSSRS.GetText(j, 1));
						tLYPremSeparateDetailSchema.setTempFeeNo(tTempDataSSRS.GetText(i, 1));
						tLYPremSeparateDetailSchema.setTempFeeType(tTempDataSSRS.GetText(i, 2));
						tLYPremSeparateDetailSchema.setOtherNo(tOtherNo);
						tLYPremSeparateDetailSchema.setOtherNoType(tTempDataSSRS.GetText(i, 4));
						tLYPremSeparateDetailSchema.setPrtNo(tQJPolSSRS.GetText(j, 2));
						tLYPremSeparateDetailSchema.setPolicyNo(tQJPolSSRS.GetText(j, 3));
						tLYPremSeparateDetailSchema.setRiskCode(tQJPolSSRS.GetText(j, 4));
						tLYPremSeparateDetailSchema.setBusiType("01");
						tLYPremSeparateDetailSchema.setManageCom(tQJPolSSRS.GetText(j, 5));
						String tPayMoney = getQJRiskSumPrem(tQJPolSSRS.GetText(j, 6),tQJPolSSRS.GetText(j, 7),tQJPolSSRS.GetText(j, 8));
						if("".equals(tPayMoney) || tPayMoney == null){
							System.out.println("个单期缴保费获取险种总保费失败！");
							continue;
						}
						if(ybtSSRS != null && ybtSSRS.getMaxRow()>0){
							for(int n = 1; n <= ybtSSRS.getMaxRow();n++){
								if(ybtSSRS.GetText(n, 1).equals(tLYPremSeparateDetailSchema.getRiskCode())){
									tLYPremSeparateDetailSchema.setPayMoney(String.valueOf(Double.valueOf(tPayMoney)*-1));//银保通反冲数据
								}
							}
						}else{
							tLYPremSeparateDetailSchema.setPayMoney(tPayMoney);
						}
						tLYPremSeparateDetailSchema.setState("01");
						tLYPremSeparateDetailSchema.setOtherState("04");
						tLYPremSeparateDetailSchema.setMoneyType("01");
						tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
						tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
						tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
						tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
						tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
					}
					
					//可能payendyearflag为A，且为多被保人情况，被保人年龄不一致，导致费用没有合并的处理
//					if(tempLYPremSeparateDetailSet != null && tempLYPremSeparateDetailSet.size()>0 ){
//						for (int m = 1; m <= tempLYPremSeparateDetailSet.size(); m++) {
//							String tBusiNo = tempLYPremSeparateDetailSet.get(m).getBusiNo();
//							for (int n = m+1; n <= tempLYPremSeparateDetailSet.size(); n++) {
//								if(tBusiNo.equals(tempLYPremSeparateDetailSet.get(n).getBusiNo())){
//									String tPayMoney1 =  tempLYPremSeparateDetailSet.get(m).getPayMoney();
//									String tPayMoney2 =  tempLYPremSeparateDetailSet.get(n).getPayMoney();
//									String tempPayMoney = String.valueOf(Double.parseDouble(tPayMoney1) + Double.parseDouble(tPayMoney2));
//									tempLYPremSeparateDetailSet.get(m).setPayMoney(tempPayMoney);
//									tempLYPremSeparateDetailSet.remove(tempLYPremSeparateDetailSet.get(n));
//									m--;
//									break;
//								}
//							}
//						}
//						System.out.println("===个单期缴保费总保费数据==="+tempLYPremSeparateDetailSet.size());
//						tLYPremSeparateDetailSet.add(tempLYPremSeparateDetailSet);
//					}
				}else{
					System.out.println("===暂收个险期缴数据===0！");
				}
				if(tLYPremSeparateDetailSet.size()>1000){
					mapsubmit(tLYPremSeparateDetailSet);	
					tLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
				}
			}
		}else{
			System.out.println("===没有需要价税分离的个险数据！===");
		}		
		
		mapsubmit(tLYPremSeparateDetailSet);	
		tLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
		
		/**
		 * 从暂收表发起--团单数据
		 */
		tTempDataSQL = " select distinct ljt.TempFeeNo,ljt.TempFeeType,ljt.OtherNo,ljt.OtherNoType "
					+ " from ljtempfee ljt "
					+ " where 1=1 "
					+ " and ljt.tempfeetype in (" + tTempfeetypet + ") "
					+ " and ljt.othernotype in ('5','7') "
					+ " and ljt.paymoney <> 0"
					+ " and not exists (select 1 from LYPremSeparateDetail a where a.tempfeeno = ljt.tempfeeno and a.otherstate <>'03') "
					+ tWhereSQL
					+ tWhereTSQL
					+ " ";
		tTempDataSSRS = tExeSQL.execSQL(tTempDataSQL);
		if(tTempDataSSRS != null && tTempDataSSRS.getMaxRow()>0){
			System.out.println("=======暂收表团单数据："+tTempDataSSRS.getMaxRow());
			String riskSql="select riskcode  from lmriskapp where 1=1 and riskcode like '820%'";
			SSRS ssrs=new SSRS();
			ssrs=tExeSQL.execSQL(riskSql);
			String risks="";
			if(ssrs!= null && ssrs.getMaxRow()>0){
				for (int h = 1; h <= ssrs.getMaxRow(); h++) {
					String risk=ssrs.GetText(h, 1);
					risks=risks+risk+",";
				}
			}
			for (int i = 1; i <= tTempDataSSRS.getMaxRow(); i++) {
				tFlag = "1";
				String tempBusiNo = tTempDataSSRS.GetText(i, 1) + tTempDataSSRS.GetText(i, 2);
				String tOtherNo = tTempDataSSRS.GetText(i, 3);
//				暂收团单数据
				String tPolSQL  = "";
				SSRS tPolSSRS = new SSRS();
				tPolSQL = " "
					    + " select '"+ tempBusiNo + "'|| lcp.riskcode, "
					    + " lcp.prtno,lcp.grpcontno,lcp.riskcode,lcp.managecom,sum(lcp.prem) "
					    + " from lcgrppol lcp "
					    + " where 1=1 "
					    + " and lcp.uwflag in ('3','4','9') "
					    + " and (lcp.prtno = '"+tOtherNo+"' or lcp.grpcontno = '"+tOtherNo+"' )"
					    + " and not exists (select 1 from lcgrpcont lgc where lgc.prtno = lcp.prtno and lgc.coinsuranceflag ='1') "	
					    + " and not exists(select 1 from LYPremSeparateDetail where busino='"+ tempBusiNo + "' || lcp.riskcode) "
					    + " group by lcp.prtno,lcp.grpcontno,lcp.riskcode,lcp.ManageCom ";
			    tPolSSRS = tExeSQL.execSQL(tPolSQL);
				if(tPolSSRS == null || tPolSSRS.getMaxRow() == 0){
					tFlag="2";
					tPolSQL = " "
						    + " select '"+ tempBusiNo + "'|| lcp.riskcode, "
						    + " lcp.prtno,lcp.grpcontno,lcp.riskcode,lcp.managecom,sum(lcp.prem) "
						    + " from lbgrppol lcp "
						    + " where 1=1 "
						    + " and lcp.uwflag in ('3','4','9') "
						    + " and lcp.edorno not like 'xb%' "
						    + " and (lcp.prtno = '"+tOtherNo+"' or lcp.grpcontno = '"+tOtherNo+"' )"
						    + " and not exists (select 1 from lbgrpcont lgc where lgc.prtno = lcp.prtno and lgc.coinsuranceflag ='1') "	
						    + " and not exists(select 1 from LYPremSeparateDetail where busino='"+ tempBusiNo + "' || lcp.riskcode) "
						    + " group by lcp.prtno,lcp.grpcontno,lcp.riskcode,lcp.ManageCom ";
					tPolSSRS = tExeSQL.execSQL(tPolSQL);
					if(tPolSSRS == null || tPolSSRS.getMaxRow() == 0){
						tFlag="3";
						tPolSQL = " "
							    + " select '"+ tempBusiNo + "'|| lcp.riskcode, "
							    + " lcp.prtno,lcp.grpcontno,lcp.riskcode,lcp.managecom,sum(lcp.prem) "
							    + " from lobgrppol lcp "
							    + " where 1=1 "
							    + " and lcp.uwflag in ('3','4','9') "
							    + " and (lcp.prtno = '"+tOtherNo+"' or lcp.grpcontno = '"+tOtherNo+"' ) "
							    + " and not exists (select 1 from lobgrpcont lgc where lgc.prtno = lcp.prtno and lgc.coinsuranceflag ='1') "	
							    + " and not exists(select 1 from LYPremSeparateDetail where busino='"+ tempBusiNo + "' || lcp.riskcode) "
							    + " group by lcp.prtno,lcp.grpcontno,lcp.riskcode,lcp.ManageCom ";
						tPolSSRS = tExeSQL.execSQL(tPolSQL);
					}
				}
				
				if(tPolSSRS != null && tPolSSRS.getMaxRow() > 0){
					System.out.println("===暂收团险数据==="+tPolSSRS.getMaxRow());
					for (int j = 1; j <= tPolSSRS.getMaxRow(); j++) {
						LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
						tLYPremSeparateDetailSchema.setBusiNo(tPolSSRS.GetText(j, 1));
						tLYPremSeparateDetailSchema.setTempFeeNo(tTempDataSSRS.GetText(i, 1));
						tLYPremSeparateDetailSchema.setTempFeeType(tTempDataSSRS.GetText(i, 2));
						tLYPremSeparateDetailSchema.setOtherNo(tOtherNo);
						tLYPremSeparateDetailSchema.setOtherNoType(tTempDataSSRS.GetText(i, 4));
						tLYPremSeparateDetailSchema.setPrtNo(tPolSSRS.GetText(j, 2));
						tLYPremSeparateDetailSchema.setPolicyNo(tPolSSRS.GetText(j, 3));
						tLYPremSeparateDetailSchema.setRiskCode(tPolSSRS.GetText(j, 4));
						
//						tLYPremSeparateDetailSchema.setBusiType("01");
						tLYPremSeparateDetailSchema.setBusiType(risks.indexOf(tPolSSRS.GetText(j, 4))==-1?"01":"08");
						tLYPremSeparateDetailSchema.setManageCom(tPolSSRS.GetText(j, 5));
						tLYPremSeparateDetailSchema.setPayMoney(tPolSSRS.GetText(j, 6));
						tLYPremSeparateDetailSchema.setState("01");
						tLYPremSeparateDetailSchema.setOtherState("01");
						tLYPremSeparateDetailSchema.setMoneyType("01");
						tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
						tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
						tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
						tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
						tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
					}
				}else{
					System.out.println("===暂收团险数据===0！");
				}
				
//				暂收团单期缴数据
				String tQJPolSQL  = "";
				SSRS tQJPolSSRS = new SSRS();
				if(tFlag.equals("1")){
					tQJPolSQL = " "
						    + " select '"+ tempBusiNo + "'|| lcp.riskcode, "
						    + " lcp.prtno,lcp.grpcontno,lcp.riskcode,lcp.managecom,sum(lcp.prem), "
						    + " lcp.payintv,timestampdiff (64, char(timestamp(lcp.PayendDate) - timestamp(lcp.Cvalidate))) "
						    + " from lcpol lcp "
						    + " where 1=1 "
						    + " and lcp.conttype = '2' "
						    + " and lcp.uwflag in ('3','4','9') "
						    + " and lcp.payintv != 0 and lcp.payintv != -1 "
						    + " and (lcp.prtno = '"+tOtherNo+"' or lcp.grpcontno = '"+tOtherNo+"' )"
						    //+ " and not exists (select 1 from lcgrpcont lgc where lgc.prtno = lcp.prtno and lgc.coinsuranceflag ='1') "	
						    + " and not exists(select 1 from LYPremSeparateDetail where busino='QJ'||'"+ tempBusiNo + "' || lcp.riskcode) "
							+ " group by lcp.prtno,lcp.grpcontno,lcp.riskcode,lcp.ManageCom,lcp.payintv,lcp.cvalidate,lcp.payenddate ";
				}else if(tFlag.equals("2")){
					tQJPolSQL = " "
						    + " select '"+ tempBusiNo + "'|| lcp.riskcode, "
						    + " lcp.prtno,lcp.grpcontno,lcp.riskcode,lcp.managecom,sum(lcp.prem), "
						    + " lcp.payintv,timestampdiff (64, char(timestamp(lcp.PayendDate) - timestamp(lcp.Cvalidate))) "
						    + " from lbpol lcp "
						    + " where 1=1 "
						    + " and lcp.conttype = '2' "
						    + " and lcp.uwflag in ('3','4','9') "
						    + " and lcp.payintv != 0 and lcp.payintv != -1 "
						    + " and lcp.edorno not like 'xb%' "
						    + " and (lcp.prtno = '"+tOtherNo+"' or lcp.grpcontno = '"+tOtherNo+"' )"
						    //+ " and not exists (select 1 from lbgrpcont lgc where lgc.prtno = lcp.prtno and lgc.coinsuranceflag ='1') "	
						    + " and not exists(select 1 from LYPremSeparateDetail where busino='QJ'||'"+ tempBusiNo + "' || lcp.riskcode)"
							+ " group by lcp.prtno,lcp.grpcontno,lcp.riskcode,lcp.ManageCom,lcp.payintv,lcp.cvalidate,lcp.payenddate ";

				}else if(tFlag.equals("3")){
					tQJPolSQL = " "
						    + " select '"+ tempBusiNo + "'|| lcp.riskcode, "
						    + " lcp.prtno,lcp.grpcontno,lcp.riskcode,lcp.managecom,sum(lcp.prem), "
						    + " lcp.payintv,timestampdiff (64, char(timestamp(lcp.PayendDate) - timestamp(lcp.Cvalidate))) "
						    + " from lobpol lcp "
						    + " where 1=1 "
						    + " and lcp.conttype = '2' "
						    + " and lcp.uwflag in ('3','4','9') "
						    + " and lcp.payintv != 0 and lcp.payintv != -1 "
						    + " and (lcp.prtno = '"+tOtherNo+"' or lcp.grpcontno = '"+tOtherNo+"' )"
						    //+ " and not exists (select 1 from lobgrpcont lgc where lgc.prtno = lcp.prtno and lgc.coinsuranceflag ='1') "	
						    + " and not exists(select 1 from LYPremSeparateDetail where busino='QJ'||'"+ tempBusiNo + "' || lcp.riskcode)"
						    + " group by lcp.prtno,lcp.grpcontno,lcp.riskcode,lcp.ManageCom,lcp.payintv,lcp.cvalidate,lcp.payenddate ";

				}
				tQJPolSSRS = tExeSQL.execSQL(tQJPolSQL);
				if(tQJPolSSRS != null && tQJPolSSRS.getMaxRow() > 0){
					System.out.println("===暂收团险期缴数据==="+tQJPolSSRS.getMaxRow());
					LYPremSeparateDetailSet tempLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
					for (int j = 1; j <= tQJPolSSRS.getMaxRow(); j++) {
						LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
						tLYPremSeparateDetailSchema.setBusiNo("QJ"+tQJPolSSRS.GetText(j, 1));
						tLYPremSeparateDetailSchema.setTempFeeNo(tTempDataSSRS.GetText(i, 1));
						tLYPremSeparateDetailSchema.setTempFeeType(tTempDataSSRS.GetText(i, 2));
						tLYPremSeparateDetailSchema.setOtherNo(tOtherNo);
						tLYPremSeparateDetailSchema.setOtherNoType(tTempDataSSRS.GetText(i, 4));
						tLYPremSeparateDetailSchema.setPrtNo(tQJPolSSRS.GetText(j, 2));
						tLYPremSeparateDetailSchema.setPolicyNo(tQJPolSSRS.GetText(j, 3));
						tLYPremSeparateDetailSchema.setRiskCode(tQJPolSSRS.GetText(j, 4));
//						tLYPremSeparateDetailSchema.setBusiType("01");
						tLYPremSeparateDetailSchema.setBusiType(risks.indexOf(tQJPolSSRS.GetText(j, 4))==-1?"01":"08");
						tLYPremSeparateDetailSchema.setManageCom(tQJPolSSRS.GetText(j, 5));
						String tPayMoney = getQJRiskSumPrem(tQJPolSSRS.GetText(j, 6),tQJPolSSRS.GetText(j, 7),tQJPolSSRS.GetText(j, 8));
						if("".equals(tPayMoney) || tPayMoney == null){
							System.out.println("团单期缴保费获取险种总保费失败！");
							continue;
						}
						tLYPremSeparateDetailSchema.setPayMoney(tPayMoney);
						tLYPremSeparateDetailSchema.setState("01");
						tLYPremSeparateDetailSchema.setOtherState("04");
						tLYPremSeparateDetailSchema.setMoneyType("01");
						tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
						tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
						tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
						tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
						tempLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
					}
					
					//可能payendyearflag为A，且为多被保人情况，被保人年龄不一致，导致费用没有合并的处理
					if(tempLYPremSeparateDetailSet != null && tempLYPremSeparateDetailSet.size()>0 ){
						for (int m = 1; m <= tempLYPremSeparateDetailSet.size(); m++) {
							String tBusiNo = tempLYPremSeparateDetailSet.get(m).getBusiNo();
							for (int n = m+1; n <= tempLYPremSeparateDetailSet.size(); n++) {
								if(tBusiNo.equals(tempLYPremSeparateDetailSet.get(n).getBusiNo())){
									String tPayMoney1 =  tempLYPremSeparateDetailSet.get(m).getPayMoney();
									String tPayMoney2 =  tempLYPremSeparateDetailSet.get(n).getPayMoney();
									String tempPayMoney = String.valueOf(Double.parseDouble(tPayMoney1) + Double.parseDouble(tPayMoney2));
									tempLYPremSeparateDetailSet.get(m).setPayMoney(tempPayMoney);
									tempLYPremSeparateDetailSet.remove(tempLYPremSeparateDetailSet.get(n));
									m--;
									break;
								}
							}
						}
						System.out.println("===团单期缴保费总保费数据==="+tempLYPremSeparateDetailSet.size());
						tLYPremSeparateDetailSet.add(tempLYPremSeparateDetailSet);
					}
				}else{
					System.out.println("===暂收团险期缴数据===0！");
				}
				
//				暂收团单约定缴费数据
				String tYDPolSQL  = "";
				SSRS tYDPolSSRS = new SSRS();
				if(tFlag.equals("1")){
					tYDPolSQL = " "
						    + " select '"+ tempBusiNo + "'|| lcp.riskcode, "
						    + " lcp.prtno,lcp.grpcontno,lcp.riskcode,lcp.managecom "
						    + " from lcgrppol lcp "
						    + " where 1=1 "
						    + " and lcp.uwflag in ('3','4','9') "
						    + " and lcp.payintv = -1 "
						    + " and (lcp.prtno = '"+tOtherNo+"' or lcp.grpcontno = '"+tOtherNo+"' )"
						    + " and not exists(select 1 from LYPremSeparateDetail where busino='"+ tempBusiNo + "' || lcp.riskcode)"
						    //+ " and not exists (select 1 from lcgrpcont lgc where lgc.prtno = lcp.prtno and lgc.coinsuranceflag ='1') "	
							;
				}else if(tFlag.equals("2")){
					tYDPolSQL = " "
						    + " select '"+ tempBusiNo + "'|| lcp.riskcode, "
						    + " lcp.prtno,lcp.grpcontno,lcp.riskcode,lcp.managecom "
						    + " from lbgrppol lcp "
						    + " where 1=1 "
						    + " and lcp.uwflag in ('3','4','9') "
						    + " and lcp.payintv = -1 "
						    + " and lcp.edorno not like 'xb%' "
						    + " and (lcp.prtno = '"+tOtherNo+"' or lcp.grpcontno = '"+tOtherNo+"' )"
						    + " and not exists(select 1 from LYPremSeparateDetail where busino='"+ tempBusiNo + "' || lcp.riskcode)"
						    //+ " and not exists (select 1 from lbgrpcont lgc where lgc.prtno = lcp.prtno and lgc.coinsuranceflag ='1') "
						    ;
				}else if(tFlag.equals("3")){
					tYDPolSQL = " "
						    + " select '"+ tempBusiNo + "'|| lcp.riskcode, "
						    + " lcp.prtno,lcp.grpcontno,lcp.riskcode,lcp.managecom "
						    + " from lobgrppol lcp "
						    + " where 1=1 "
						    + " and lcp.uwflag in ('3','4','9') "
						    + " and lcp.payintv = -1 "
						    + " and (lcp.prtno = '"+tOtherNo+"' or lcp.grpcontno = '"+tOtherNo+"' )"
						    + " and not exists(select 1 from LYPremSeparateDetail where busino='"+ tempBusiNo + "' || lcp.riskcode)"
						    //+ " and not exists (select 1 from lobgrpcont lgc where lgc.prtno = lcp.prtno and lgc.coinsuranceflag ='1') "	
							;
				}
				tYDPolSSRS = tExeSQL.execSQL(tYDPolSQL);
				if(tYDPolSSRS != null && tYDPolSSRS.getMaxRow() > 0){
					System.out.println("===暂收团险约定缴费数据==="+tYDPolSSRS.getMaxRow());
					for (int j = 1; j <= tYDPolSSRS.getMaxRow(); j++) {
						LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
						tLYPremSeparateDetailSchema.setBusiNo("YD"+tYDPolSSRS.GetText(j, 1));
						tLYPremSeparateDetailSchema.setTempFeeNo(tTempDataSSRS.GetText(i, 1));
						tLYPremSeparateDetailSchema.setTempFeeType(tTempDataSSRS.GetText(i, 2));
						tLYPremSeparateDetailSchema.setOtherNo(tOtherNo);
						tLYPremSeparateDetailSchema.setOtherNoType(tTempDataSSRS.GetText(i, 4));
						tLYPremSeparateDetailSchema.setPrtNo(tYDPolSSRS.GetText(j, 2));
						tLYPremSeparateDetailSchema.setPolicyNo(tYDPolSSRS.GetText(j, 3));
						tLYPremSeparateDetailSchema.setRiskCode(tYDPolSSRS.GetText(j, 4));
//						tLYPremSeparateDetailSchema.setBusiType("01");
						tLYPremSeparateDetailSchema.setBusiType(risks.indexOf(tYDPolSSRS.GetText(j, 4))==-1?"01":"08");
						tLYPremSeparateDetailSchema.setManageCom(tYDPolSSRS.GetText(j, 5));
						String tPayMoney = getAllRiskSumPrem(tYDPolSSRS.GetText(j, 2),tYDPolSSRS.GetText(j, 4));
						if("".equals(tPayMoney) || tPayMoney == null){
							System.out.println("约定缴费获取险种总保费失败！");
							continue;
						}
						tLYPremSeparateDetailSchema.setPayMoney(tPayMoney);
						tLYPremSeparateDetailSchema.setState("01");
						tLYPremSeparateDetailSchema.setOtherState("05");
						tLYPremSeparateDetailSchema.setMoneyType("01");
						tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
						tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
						tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
						tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
						tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
					}
				}else{
					System.out.println("===暂收团险约定缴费数据===0！");
				}
				
//				团单共保
				String tGBPolSQL  = "";
				SSRS tGBPolSSRS = new SSRS();
				tGBPolSQL = " "
					    + " select '"+ tempBusiNo + "'|| lcp.riskcode, "
					    + " lcp.prtno,lcp.grpcontno,lcp.riskcode,lcp.managecom,lcp.prem "
					    + " from lcgrppol lcp "
					    + " where 1=1 "
					    + " and lcp.uwflag in ('3','4','9') "
					    + " and (lcp.prtno = '"+tOtherNo+"' or lcp.grpcontno = '"+tOtherNo+"' )"
					    + " and not exists(select 1 from LYPremSeparateDetail where busino='"+ tempBusiNo + "' || lcp.riskcode)"
					    + " and exists (select 1 from lcgrpcont lgc where lgc.prtno = lcp.prtno and lgc.coinsuranceflag ='1') "	
						;
				tGBPolSSRS = tExeSQL.execSQL(tGBPolSQL);
				if(tGBPolSSRS == null || tGBPolSSRS.getMaxRow() == 0){
					tGBPolSQL = " "
						    + " select '"+ tempBusiNo + "'|| lcp.riskcode, "
						    + " lcp.prtno,lcp.grpcontno,lcp.riskcode,lcp.managecom,lcp.prem "
						    + " from lbgrppol lcp "
						    + " where 1=1 "
						    + " and lcp.uwflag in ('3','4','9') "
						    + " and lcp.edorno not like 'xb%' "
						    + " and (lcp.prtno = '"+tOtherNo+"' or lcp.grpcontno = '"+tOtherNo+"' )"
						    + " and not exists(select 1 from LYPremSeparateDetail where busino='"+ tempBusiNo + "' || lcp.riskcode)"
						    + " and exists (select 1 from lbgrpcont lgc where lgc.prtno = lcp.prtno and lgc.coinsuranceflag ='1') "	
						    ;
					tGBPolSSRS = tExeSQL.execSQL(tGBPolSQL);
					if(tGBPolSSRS == null || tGBPolSSRS.getMaxRow() == 0){
						tGBPolSQL = " "
							    + " select '"+ tempBusiNo + "'|| lcp.riskcode, "
							    + " lcp.prtno,lcp.grpcontno,lcp.riskcode,lcp.managecom,lcp.prem "
							    + " from lobgrppol lcp "
							    + " where 1=1 "
							    + " and lcp.uwflag in ('3','4','9') "
							    + " and (lcp.prtno = '"+tOtherNo+"' or lcp.grpcontno = '"+tOtherNo+"' )"
							    + " and not exists(select 1 from LYPremSeparateDetail where busino='"+ tempBusiNo + "' || lcp.riskcode)"
							    + " and exists (select 1 from lobgrpcont lgc where lgc.prtno = lcp.prtno and lgc.coinsuranceflag ='1') "
							    ;
						tGBPolSSRS = tExeSQL.execSQL(tGBPolSQL);
					}
				}
					
				if(tGBPolSSRS != null && tGBPolSSRS.getMaxRow() > 0){
					System.out.println("===暂收团险共保数据==="+tGBPolSSRS.getMaxRow());
					for (int j = 1; j <= tGBPolSSRS.getMaxRow(); j++) {
						LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
						tLYPremSeparateDetailSchema.setBusiNo("GB"+tGBPolSSRS.GetText(j, 1));
						tLYPremSeparateDetailSchema.setTempFeeNo(tTempDataSSRS.GetText(i, 1));
						tLYPremSeparateDetailSchema.setTempFeeType(tTempDataSSRS.GetText(i, 2));
						tLYPremSeparateDetailSchema.setOtherNo(tOtherNo);
						tLYPremSeparateDetailSchema.setOtherNoType(tTempDataSSRS.GetText(i, 4));
						tLYPremSeparateDetailSchema.setPrtNo(tGBPolSSRS.GetText(j, 2));
						tLYPremSeparateDetailSchema.setPolicyNo(tGBPolSSRS.GetText(j, 3));
						tLYPremSeparateDetailSchema.setRiskCode(tGBPolSSRS.GetText(j, 4));
//						tLYPremSeparateDetailSchema.setBusiType("01");
						tLYPremSeparateDetailSchema.setBusiType(risks.indexOf(tGBPolSSRS.GetText(j, 4))==-1?"01":"08");
						tLYPremSeparateDetailSchema.setManageCom(tGBPolSSRS.GetText(j, 5));
						tLYPremSeparateDetailSchema.setPayMoney(tGBPolSSRS.GetText(j, 6));
						tLYPremSeparateDetailSchema.setState("01");
						tLYPremSeparateDetailSchema.setOtherState("09");
						tLYPremSeparateDetailSchema.setMoneyType("01");
						tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
						tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
						tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
						tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
						tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
					}
				}else{
					System.out.println("===暂收团险共保数据===0！");
				}
				if(tLYPremSeparateDetailSet.size()>1000){
					mapsubmit(tLYPremSeparateDetailSet);	
					tLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
				}
			}
		}else{
			System.out.println("===没有需要价税分离的团险数据！===");
		}
//		mMap.put(tLYPremSeparateDetailSet, SysConst.INSERT);
		mapsubmit(tLYPremSeparateDetailSet);	
		tLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
		return true;
	}
	/**
	 * 准备后台的数据
	 * @return boolean
	 */
	private boolean prepareOutputData() {
		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GetTempFeeSeparateBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
	/**
	 * 数据分批提交
	 * @param aPrtNo
	 * @param aRiskCode
	 * @return
	 */
	private boolean mapsubmit(LYPremSeparateDetailSet tLYPremSeparateDetailSet){
		int count=0;
		LYPremSeparateDetailSet tDetailSet = new LYPremSeparateDetailSet();
		if(null == tLYPremSeparateDetailSet
				|| tLYPremSeparateDetailSet.size() < 1){
			System.out.println("===没有需要价税分离的数据！===");
		}
		for(int i = 1; i <= tLYPremSeparateDetailSet.size(); i++){
			LYPremSeparateDetailSchema detailSchema=tLYPremSeparateDetailSet.get(i).getSchema();
			detailSchema.setModifyDate(PubFun.getCurrentDate());
			detailSchema.setModifyTime(PubFun.getCurrentTime());
			tDetailSet.add(detailSchema);
			count ++;
			if(count >= sysCount || i == tLYPremSeparateDetailSet.size()){
				count = 0;
				mMap=new MMap();
				mMap.put(tDetailSet, SysConst.INSERT);
				tDetailSet=new LYPremSeparateDetailSet();
				if (!prepareOutputData()) {
					return false;
				}

				// 保存数据
				PubSubmit tPubSubmit = new PubSubmit();

				if (!tPubSubmit.submitData(mInputData, mOperate)) {
					System.out.print("保存数据返回");
					// @@错误处理
					this.mErrors.copyAllErrors(tPubSubmit.mErrors);
					CError tError = new CError();
					tError.moduleName = "GetTempFeeSeparateBL";
					tError.functionName = "submitData";
					tError.errorMessage = "数据提交失败!";
					this.mErrors.addOneError(tError);
//					return false;
				}
				mInputData = null;
			}
		}
		System.out.println("结束！！！！");
		return true;
	}
	
	//获取约定缴费各险种拆分后总保费
	private String getAllRiskSumPrem(String aPrtNo,String aRiskCode){
		String tAllRiskSumPrem = "";
		double tRiskSumPrem = 0;
		String tPGSql = "select ProposalGrpContNo from lcgrpcont where prtno = '"+aPrtNo+"' ";
		String tProposalGrpContNo = new ExeSQL().getOneValue(tPGSql);
		LCGrpPayPlanDetailSet tLCGrpPayPlanDetailSet = new LCGrpPayPlanDetailSet();
		GrpPayPlanDetailBL tGrpPayPlanDetailBL = new GrpPayPlanDetailBL();
        VData tVData = new VData();
    	
    	TransferData mTransferData = new TransferData();
    	mTransferData.setNameAndValue("ProposalGrpContNo", tProposalGrpContNo);
    	tVData.add(mGlobalInput);
    	tVData.add(mTransferData);
        if(!tGrpPayPlanDetailBL.submitData(tVData, "")){
        	CError.buildErr(this, "拆分约定缴费计划有误", tGrpPayPlanDetailBL.mErrors);
        	System.out.println("保单印刷号："+aPrtNo+",拆分约定缴费计划有误！");
            return tAllRiskSumPrem;
        }
        MMap tmpMap = tGrpPayPlanDetailBL.getMMMap();
        tLCGrpPayPlanDetailSet = (LCGrpPayPlanDetailSet)tmpMap.getObjectByObjectName("LCGrpPayPlanDetailSet", 0);
        if(tLCGrpPayPlanDetailSet == null || tLCGrpPayPlanDetailSet.size() == 0){
        	System.out.println("保单印刷号："+aPrtNo+",约定缴费拆分失败！");
        	CError.buildErr(this, "保单印刷号："+aPrtNo+",约定缴费拆分失败！", tGrpPayPlanDetailBL.mErrors);
        	return tAllRiskSumPrem;
        }
        for(int j = 1;j<=tLCGrpPayPlanDetailSet.size();j++){
        	if(aRiskCode.equals(tLCGrpPayPlanDetailSet.get(j).getRiskCode())){
        		tRiskSumPrem = tRiskSumPrem +  tLCGrpPayPlanDetailSet.get(j).getPrem();
        	}
        }
		return tRiskSumPrem+"";
	}
	
	//获取期缴保费总保费
		private String getQJRiskSumPrem(String aPrem,String aPayIntv,String tDifMonth){
			String tAllRiskSumPrem = "";
			int tPayTimes = 1;
			int tPayIntv = Integer.parseInt(aPayIntv);
			double tPrem = Double.parseDouble(aPrem);
//			if("Y".equals(aPayEndYearFlag)){
//				tPayTimes = (12/tPayIntv)*tPayEndYear;
//			}else if("M".equals(aPayEndYearFlag)){
//				tPayTimes = tPayEndYear/tPayIntv;
//			}else if("D".equals(aPayEndYearFlag)){
//				tPayTimes = (12/tPayIntv)*(tPayEndYear/365);
//			}else if("A".equals(aPayEndYearFlag)){
//				tPayTimes = (12/tPayIntv)*(tPayEndYear - Integer.parseInt(aInsuredAppAge));
//			}
			tPayTimes = Integer.parseInt(tDifMonth)/tPayIntv;
			if(tPayTimes < 1){
				tPayTimes = 1;
			}
			tAllRiskSumPrem = String.valueOf(tPrem*tPayTimes);
			return tAllRiskSumPrem;
		}
		public static String convertLongToString(long value){
	        String msg="";  
	        Date date = new Date(value);  
	        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");  
	        msg+=sdf.format(date);  
	        return msg;
		}
	public static void main(String[] args) {
		long startTime1 = System.currentTimeMillis();
//		GetTempFeeBL tGetTempFeeSeparateBL = new GetTempFeeBL();
//		VData cInputData = new VData();
//		TransferData tTransferData = new TransferData();
//		String tStartDate = "";
//		String tEndDate = "";
		String s = "12000032806";
		String [] arr = null;
		arr = s.split(",");
		System.out.println("--------------------   "+arr.length);
		for(int i = 0; i < arr.length;i++){
			GetTempFeeBL tGetTempFeeSeparateBL = new GetTempFeeBL();
			System.out.println("============================   --------------------"+i);
			VData cInputData = new VData();
			TransferData tTransferData = new TransferData();
			String tStartDate = "2001-01-04";
			String tEndDate = "2017-09-29";
			String tPrtNo = "PR000014237";
			tTransferData.setNameAndValue("StartDate", tStartDate);
			tTransferData.setNameAndValue("EndDate", tEndDate);
			tTransferData.setNameAndValue("PrtNo", tPrtNo);
			cInputData.add(tTransferData);
			tGetTempFeeSeparateBL.submitData(cInputData, "");
		}
//		String tPrtNo = "18000295293";
//		tTransferData.setNameAndValue("StartDate", tStartDate);
//		tTransferData.setNameAndValue("EndDate", tEndDate);
//		tTransferData.setNameAndValue("PrtNo", tPrtNo);
//		cInputData.add(tTransferData);
//		tGetTempFeeSeparateBL.submitData(cInputData, "");
		long startTime2 = System.currentTimeMillis();
		System.out.println("======8888888888888            "+((startTime2 -startTime1)/1000)+"秒;"+((startTime2 -startTime1)/1000/60)+"分钟");
	}
}
