package com.sinosoft.lis.ygz;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LYPremSeparateDetailSchema;
import com.sinosoft.lis.vschema.LYPremSeparateDetailSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 
 * @author abc
 *
 */
public class GetXqTempFeeBL {
	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	/** 数据操作字符串 */
	private String mOperate;
	/** 开始日期*/
	private String mStartDate;
	/** 结束日期*/
	private String mEndDate; 
	private int sysCount = 5000;//默认每一千条数据发送一次请求

	//页面手工提取标志
	private String mHandFlag;
	
	private String mWherePart;
	
	private String mWhereSQL;
	//业务处理相关变量
	private MMap mMap = new MMap();
	
	private String mCurrentDate = PubFun.getCurrentDate();
	private String mCurrentTime = PubFun.getCurrentTime();
	private boolean mIsclycle= true;
	
	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		// 进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GetTempFeeBqSeparateBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败GetTempFeeBqSeparateBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}

//		// 准备往后台的数据
//		if (!prepareOutputData()) {
//			return false;
//		}
//
//		// 保存数据
//		PubSubmit tPubSubmit = new PubSubmit();
//
//		if (!tPubSubmit.submitData(mInputData, mOperate)) {
//			System.out.print("保存数据返回");
//			// @@错误处理
//			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
//			CError tError = new CError();
//			tError.moduleName = "LCProjectCancelBL";
//			tError.functionName = "submitData";
//			tError.errorMessage = "数据提交失败!";
//			this.mErrors.addOneError(tError);
//			return false;
//		}
//		mInputData = null;
		return true;
	}

	// 获得输出数据
	private boolean getInputData(VData cInputData) {
		TransferData tTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
		mStartDate = (String) tTransferData.getValueByName("StartDate");
		mEndDate = (String) tTransferData.getValueByName("EndDate");
		
		mHandFlag = (String) tTransferData.getValueByName("HandWorkFlag");
		mWherePart = "";
		mWhereSQL = "";
		if(null != mHandFlag && !"".equals(mHandFlag)){
			LYPremSeparateDetailSet tLYPremSeparateDetailSet=(LYPremSeparateDetailSet) cInputData
			.getObjectByObjectName("LYPremSeparateDetailSet", 0);
			if(null != tLYPremSeparateDetailSet){
				String lisStr="";
				for(int i=1; i<=tLYPremSeparateDetailSet.size();i++){
					lisStr += "'"+ tLYPremSeparateDetailSet.get(i).getTempFeeNo()+"'";
					if(i<tLYPremSeparateDetailSet.size()){
						lisStr += ",";
					}
				}
				 mWhereSQL = " and ljt.tempfeeno in ("+lisStr+") ";
				 mWherePart = " and ljs.getnoticeno in ("+lisStr+")";
			}
		}
		return true;
	}
	
//  业务处理
	private boolean dealData() {
		YGZ tYGZ=new YGZ();
		LYPremSeparateDetailSet tLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
		String tWhereSQL = "";
		String tWherePart = "";
		String tDealDate = "2017-01-05";
		if(mStartDate != null && !"".equals(mStartDate)){
			tWhereSQL += " and ljt.confmakedate >= '" + mStartDate + "' ";
			tWherePart += " and lja.confdate >= '" + mStartDate + "' ";
		}
		if(mEndDate != null && !"".equals(mEndDate)){
			tWhereSQL += " and ljt.confmakedate <= '" + mEndDate + "' ";
			tWherePart += " and lja.confdate <= '" + mEndDate + "' ";
		}
		if("".equals(tWhereSQL)){
			tWhereSQL += " and ljt.confmakedate >= current date - 7 days and ljt.confmakedate >='"+tDealDate +"' ";
			tWherePart += " and lja.confdate >= current date - 7 days and lja.confdate >='"+tDealDate +"' ";
		}
		if(null != mHandFlag && !"".equals(mHandFlag)){
			tWhereSQL = "";
			tWherePart = "";
		}
		//此次调整过滤已经提取过的暂收，防止重复提取。
		tWhereSQL +=" and not exists (select distinct 1 from LYPremSeparateDetail where tempfeeno=ljt.tempfeeno and tempfeetype='2') ";
		tWherePart +=" and not exists (select distinct 1 from LYPremSeparateDetail where tempfeeno=ljs.getnoticeno and tempfeetype='2') ";
		String tSQL = "";
		int count =0;
	    while(mIsclycle)
		  {
		
		//暂收个单续期数据
		     tSQL =" select trim(ljs.getnoticeno)||'2'||trim(ljs.polno)||trim(nvl(paytype,'')) BusiNo,  "
			+ " ljs.getnoticeno TempFeeNo,'2' TempFeeType,ljs.contno OtherNo,'0' OtherNoType,  "
			+ " (select prtno from lccont where contno = ljs.contno union select prtno from lbcont where contno = ljs.contno) PrtNo,   "
			+ " ljs.contno PolicyNo,  "
			+ " ljs.riskcode,ljs.managecom,sum(ljs.sumactupaymoney) "
			+ " from ljspaypersonb ljs "
			+ " where 1=1 "
			+ " and ljs.paytype in ('ZC','YEL') "
			+ " and (grpcontno='00000000000000000000' or grpcontno is null) "
			+ " and not exists (select busino from LYPremSeparateDetail where busino=(trim(ljs.getnoticeno)||'2'||trim(ljs.polno)||trim(nvl(paytype,'')))) "
			+ " and not exists (select busino from LYPremSeparateDetail where busino=(trim(ljs.getnoticeno) ||'2'||trim(ljs.RiskCode))) "//根据一期的主键过滤
			+ " and not exists (select busino from LYPremSeparateDetail where busino=(trim(ljs.getnoticeno) ||'2'||trim(ljs.RiskCode)||trim(ljs.paytype))) "
			+ " and exists (select 1 from lccont where conttype='1' and appflag='1' and contno=ljs.contno  "
			+ " 		union select 1 from lbcont where conttype='1' and contno=ljs.contno )  "
			+ " and exists (select 1 from ljtempfee ljt where tempfeeno=ljs.getnoticeno and tempfeetype='2' "//全部通过暂收或部分收费是暂收的
			+ tWhereSQL
			+ mWhereSQL
			+ " ) "
			+ " group by ljs.getnoticeno,ljs.contno,ljs.riskcode,ljs.managecom,ljs.polno,paytype  "
			+ "  fetch first 10000 rows only  with ur ";
		SSRS tSSRSNo = new ExeSQL().execSQL(tSQL);
		if(tSSRSNo != null && tSSRSNo.getMaxRow() > 0){
			  count++;
			if(tSSRSNo.getMaxRow()==10000){
				mIsclycle = true;
			}else {
				mIsclycle = false;
			}
			if(count ==30){
				mIsclycle = false;
			}

			for (int i = 1; i <= tSSRSNo.getMaxRow(); i++) {
				LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
				tLYPremSeparateDetailSchema.setBusiNo(tSSRSNo.GetText(i, 1));
				tLYPremSeparateDetailSchema.setTempFeeNo(tSSRSNo.GetText(i, 2));
				tLYPremSeparateDetailSchema.setTempFeeType(tSSRSNo.GetText(i, 3));
				tLYPremSeparateDetailSchema.setOtherNo(tSSRSNo.GetText(i, 4));
				tLYPremSeparateDetailSchema.setOtherNoType(tSSRSNo.GetText(i, 5));
				tLYPremSeparateDetailSchema.setPrtNo(tSSRSNo.GetText(i, 6));
				tLYPremSeparateDetailSchema.setPolicyNo(tSSRSNo.GetText(i, 7));
				tLYPremSeparateDetailSchema.setRiskCode(tSSRSNo.GetText(i, 8));
				tLYPremSeparateDetailSchema.setBusiType(tYGZ.getBusinType(null, "BF", tLYPremSeparateDetailSchema.getRiskCode()));
				tLYPremSeparateDetailSchema.setManageCom(tSSRSNo.GetText(i, 9));
				tLYPremSeparateDetailSchema.setPayMoney(tSSRSNo.GetText(i, 10));
				tLYPremSeparateDetailSchema.setState("01");
				tLYPremSeparateDetailSchema.setOtherState("01");
				tLYPremSeparateDetailSchema.setMoneyType("01");
				tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
				tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
				tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
			}
			mapsubmit(tLYPremSeparateDetailSet);	
			tLYPremSeparateDetailSet.clear();
			tLYPremSeparateDetailSet = null;
		}
		System.out.println("暂收个单续期数据："+tSSRSNo.getMaxRow());
	  }
		
		
		//暂收团单续期数据
		tSQL ="select trim(ljs.getnoticeno)||'2'||trim(ljs.grppolno) BusiNo,  "
			+ " ljs.getnoticeno TempFeeNo,'2' TempFeeType,ljs.grpcontno OtherNo,'0' OtherNoType,  "
			+ " (select prtno from lcgrpcont where grpcontno = ljs.grpcontno union select prtno from lbgrpcont where grpcontno = ljs.grpcontno) PrtNo,   "
			+ " ljs.grpcontno PolicyNo,  "
			+ " ljs.riskcode,ljs.managecom,sum(ljs.sumactupaymoney), "
			+ " (select 1 from lcgrpcont where  appflag='1' and grpcontno=ljs.grpcontno  and CoInsuranceFlag='1' "//共保标识
			+ " union select 1 from lbgrpcont where grpcontno=ljs.grpcontno  and CoInsuranceFlag='1')  "
			+ " from ljspaygrpb ljs  "
			+ " where 1=1 "
			+ " and ljs.paytype in ('ZC','YEL','YET')   "
			+ " and not exists (select busino from LYPremSeparateDetail where busino=(trim(ljs.getnoticeno)||'2'||trim(ljs.grppolno)))  "
			+ " and not exists (select busino from LYPremSeparateDetail where busino=(trim(ljs.getnoticeno)||'2'||trim(ljs.RiskCode))) "//根据一期的主键过滤
			+ " and not exists (select busino from LYPremSeparateDetail where busino=(trim(ljs.getnoticeno) ||'2'||trim(ljs.RiskCode)||trim(ljs.paytype))) "
			+ " and exists (select 1 from ljtempfee ljt where tempfeeno=ljs.getnoticeno and tempfeetype='2' "
			+ tWhereSQL
			+ mWhereSQL
			+ " ) "
			+ " group by ljs.getnoticeno,ljs.grpcontno,ljs.riskcode,ljs.managecom,ljs.grppolno "
			+ " with ur";
		SSRS tSSRSNo2 = new ExeSQL().execSQL(tSQL);
		if(tSSRSNo2 != null && tSSRSNo2.getMaxRow() > 0){
			for (int i = 1; i <= tSSRSNo2.getMaxRow(); i++) {
				LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
				tLYPremSeparateDetailSchema.setBusiNo(tSSRSNo2.GetText(i, 1));
				tLYPremSeparateDetailSchema.setTempFeeNo(tSSRSNo2.GetText(i, 2));
				tLYPremSeparateDetailSchema.setTempFeeType(tSSRSNo2.GetText(i, 3));
				tLYPremSeparateDetailSchema.setOtherNo(tSSRSNo2.GetText(i, 4));
				tLYPremSeparateDetailSchema.setOtherNoType(tSSRSNo2.GetText(i, 5));
				tLYPremSeparateDetailSchema.setPrtNo(tSSRSNo2.GetText(i, 6));
				tLYPremSeparateDetailSchema.setPolicyNo(tSSRSNo2.GetText(i, 7));
				tLYPremSeparateDetailSchema.setRiskCode(tSSRSNo2.GetText(i, 8));
				tLYPremSeparateDetailSchema.setBusiType("01");
				tLYPremSeparateDetailSchema.setManageCom(tSSRSNo2.GetText(i, 9));
				tLYPremSeparateDetailSchema.setPayMoney(tSSRSNo2.GetText(i, 10));
				String coInsuranceFlag = tSSRSNo2.GetText(i, 11);
				if("1".equals(coInsuranceFlag)){
					tLYPremSeparateDetailSchema.setOtherNoType("09");
				}
				tLYPremSeparateDetailSchema.setState("01");
				tLYPremSeparateDetailSchema.setOtherState("01");
				tLYPremSeparateDetailSchema.setMoneyType("01");
				tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
				tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
				tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
			}
		}
		System.out.println("暂收团单续期数据:"+tSSRSNo2.getMaxRow());
		
		//个单续期收费完全从投保人账户抵扣的
		tSQL = "select trim(lja.payno)||trim(lja.polno) BusiNo, "
			 + " lja.getnoticeno TempFeeNo,'2' TempFeeType,ljs.OtherNo OtherNo,'0' OtherNoType, "
			 + " (select prtno from lccont where contno = lja.contno union select prtno from lbcont where contno = lja.contno) PrtNo,  "
			 + " lja.contno PolicyNo, "
			 + " lja.riskcode,lja.managecom,sum(lja.sumactupaymoney),lja.payno "
			 + " from ljspayb ljs ,ljapayperson lja"
			 + " where 1=1 "
			 + " and ljs.otherno=lja.contno"
			 + " and ljs.getnoticeno = lja.getnoticeno "
			 + " and (lja.grpcontno ='00000000000000000000' or lja.grpcontno is null)"
			 + " and lja.paytype = 'ZC' "
			 + " and ljs.sumduepaymoney=0"
			 + " and exists (select 1 from ljspaypersonb where getnoticeno=lja.getnoticeno and contno=lja.contno and paytype='YEL')"
			 + " and not exists (select busino from LYPremSeparateDetail where busino=(trim(lja.payno)||trim(lja.polno))) "
			 + " and not exists (select busino from LYPremSeparateDetail where busino=(trim(lja.payno)||trim(lja.RiskCode))) "//根据一期主键过滤
			 + tWherePart
			 + mWherePart
			 + " group by lja.getnoticeno,lja.RiskCode,ljs.OtherNo,lja.contno,lja.managecom,lja.payno,lja.polno "
			 + " with ur";
		SSRS tSSRSNo4 = new ExeSQL().execSQL(tSQL);
		if(tSSRSNo4 != null && tSSRSNo4.getMaxRow() > 0){
			for (int i = 1; i <= tSSRSNo4.getMaxRow(); i++) {
				LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
				tLYPremSeparateDetailSchema.setBusiNo(tSSRSNo4.GetText(i, 1));
				tLYPremSeparateDetailSchema.setTempFeeNo(tSSRSNo4.GetText(i, 2));
				tLYPremSeparateDetailSchema.setTempFeeType(tSSRSNo4.GetText(i, 3));
				tLYPremSeparateDetailSchema.setOtherNo(tSSRSNo4.GetText(i, 4));
				tLYPremSeparateDetailSchema.setOtherNoType(tSSRSNo4.GetText(i, 5));
				tLYPremSeparateDetailSchema.setPrtNo(tSSRSNo4.GetText(i, 6));
				tLYPremSeparateDetailSchema.setPolicyNo(tSSRSNo4.GetText(i, 7));
				tLYPremSeparateDetailSchema.setRiskCode(tSSRSNo4.GetText(i, 8));
				tLYPremSeparateDetailSchema.setBusiType(tYGZ.getBusinType(null, "BF", tLYPremSeparateDetailSchema.getRiskCode()));
				tLYPremSeparateDetailSchema.setManageCom(tSSRSNo4.GetText(i, 9));
				tLYPremSeparateDetailSchema.setPayMoney(tSSRSNo4.GetText(i, 10));
				tLYPremSeparateDetailSchema.setState("01");
				tLYPremSeparateDetailSchema.setOtherState("01");
				tLYPremSeparateDetailSchema.setMoneyType("01");
				tLYPremSeparateDetailSchema.setMoneyNo(tSSRSNo4.GetText(i, 11));
				
				tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
				tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
				tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
			}
		}
		System.out.println("个单续期收费完全从投保人账户抵扣的:"+tSSRSNo4.getMaxRow());
		
		//团单续期收费完全从投保人账户抵扣的
		tSQL = "select trim(lja.payno)||trim(lja.grppolno) BusiNo, "
		  + " lja.getnoticeno TempFeeNo,'2' TempFeeType,ljs.OtherNo OtherNo,'0' OtherNoType, "
		  + " (select prtno from lcgrpcont where grpcontno = lja.grpcontno union select prtno from lbgrpcont where grpcontno = lja.grpcontno) PrtNo,  "
		  + " lja.grpcontno PolicyNo, "
		  + " lja.riskcode,lja.managecom,sum(lja.sumactupaymoney),lja.payno, "
		  + " (select CoInsuranceFlag from lcgrpcont where grpcontno = lja.grpcontno "//共保标识
		  + " union select CoInsuranceFlag from lbgrpcont where grpcontno = lja.grpcontno) "
		  + " from ljspayb ljs ,ljapaygrp lja"
		  + " where 1=1 "
		  + " and ljs.otherno=lja.grpcontno "
		  + " and ljs.getnoticeno = lja.getnoticeno "
		  + " and lja.paytype = 'ZC' "
		  + " and ljs.sumduepaymoney=0 "
		  + " and exists (select 1 from ljspaygrpb where getnoticeno=ljs.getnoticeno and paytype='YEL')"
		  + " and not exists (select busino from LYPremSeparateDetail where busino=(trim(lja.payno)||trim(lja.grppolno))) "
		  + " and not exists (select busino from LYPremSeparateDetail where busino=(trim(lja.payno)||trim(lja.RiskCode))) "//根据一期主键过滤
//		  + " and lja.confdate>='"+tDealDate+"' "
		  + tWherePart
		  + mWherePart
		  + " group by lja.getnoticeno,lja.RiskCode,ljs.OtherNo,lja.grpcontno,lja.managecom,lja.payno,lja.grppolno "
		  + " with ur";
		SSRS tSSRSNo5 = new ExeSQL().execSQL(tSQL);
		if(tSSRSNo5 != null && tSSRSNo5.getMaxRow() > 0){
			for (int i = 1; i <= tSSRSNo5.getMaxRow(); i++) {
				LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
				tLYPremSeparateDetailSchema.setBusiNo(tSSRSNo5.GetText(i, 1));
				tLYPremSeparateDetailSchema.setTempFeeNo(tSSRSNo5.GetText(i, 2));
				tLYPremSeparateDetailSchema.setTempFeeType(tSSRSNo5.GetText(i, 3));
				tLYPremSeparateDetailSchema.setOtherNo(tSSRSNo5.GetText(i, 4));
				tLYPremSeparateDetailSchema.setOtherNoType(tSSRSNo5.GetText(i, 5));
				tLYPremSeparateDetailSchema.setPrtNo(tSSRSNo5.GetText(i, 6));
				tLYPremSeparateDetailSchema.setPolicyNo(tSSRSNo5.GetText(i, 7));
				tLYPremSeparateDetailSchema.setRiskCode(tSSRSNo5.GetText(i, 8));
				tLYPremSeparateDetailSchema.setBusiType("01");
				tLYPremSeparateDetailSchema.setManageCom(tSSRSNo5.GetText(i, 9));
				tLYPremSeparateDetailSchema.setPayMoney(tSSRSNo5.GetText(i, 10));
				tLYPremSeparateDetailSchema.setState("01");
				tLYPremSeparateDetailSchema.setOtherState("01");
				tLYPremSeparateDetailSchema.setMoneyType("01");
				tLYPremSeparateDetailSchema.setMoneyNo(tSSRSNo5.GetText(i, 11));
				String coInsuranceFlag = tSSRSNo2.GetText(i, 12);
				if("1".equals(coInsuranceFlag)){
					tLYPremSeparateDetailSchema.setOtherNoType("09");
				}
				tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
				tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
				tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
			}
		}
		System.out.println("团单续期收费完全从投保人账户抵扣的:"+tSSRSNo5.getMaxRow());
		
//		mMap.put(tLYPremSeparateDetailSet, SysConst.INSERT);
		mapsubmit(tLYPremSeparateDetailSet);	
		return true;
	}
	
	/**
	 * 数据分批提交
	 * @param aPrtNo
	 * @param aRiskCode
	 * @return
	 */
	private boolean mapsubmit(LYPremSeparateDetailSet tLYPremSeparateDetailSet){
		int count=0;
		LYPremSeparateDetailSet tDetailSet = new LYPremSeparateDetailSet();
		if(null == tLYPremSeparateDetailSet
				|| tLYPremSeparateDetailSet.size() < 1){
			System.out.println("===没有需要价税分离的数据！===");
		}
		for(int i = 1; i <= tLYPremSeparateDetailSet.size(); i++){
			LYPremSeparateDetailSchema detailSchema=tLYPremSeparateDetailSet.get(i).getSchema();
			detailSchema.setModifyDate(PubFun.getCurrentDate());
			detailSchema.setModifyTime(PubFun.getCurrentTime());
			tDetailSet.add(detailSchema);
			count ++;
			if(count >= sysCount || i == tLYPremSeparateDetailSet.size()){
				count = 0;
				mMap=new MMap();
				mMap.put(tDetailSet, SysConst.INSERT);
				tDetailSet=new LYPremSeparateDetailSet();
				if (!prepareOutputData()) {
					return false;
				}

				// 保存数据
				PubSubmit tPubSubmit = new PubSubmit();

				if (!tPubSubmit.submitData(mInputData, mOperate)) {
					System.out.print("保存数据返回");
					// @@错误处理
					this.mErrors.copyAllErrors(tPubSubmit.mErrors);
					CError tError = new CError();
					tError.moduleName = "GetTempFeeSeparateBL";
					tError.functionName = "submitData";
					tError.errorMessage = "数据提交失败!";
					this.mErrors.addOneError(tError);
//					return false;
				}
				mInputData = null;
			}
		}
		System.out.println("结束！！！！");
		return true;
	}
	
	
	
	
	
	
	/**
	 * 准备后台的数据
	 * @return boolean
	 */
	private boolean prepareOutputData() {
		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCProjectCancelBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
	
	public static void main(String[] args) {
		GetXqTempFeeBL tGetTempFeeSeparateBL = new GetXqTempFeeBL();
		VData cInputData = new VData();
		TransferData tTransferData = new TransferData();
		String tStartDate = "2016-12-29";
		String tEndDate = "2017-01-09";
		tTransferData.setNameAndValue("StartDate", tStartDate);
		tTransferData.setNameAndValue("EndDate", tEndDate);
		cInputData.add(tTransferData);
		tGetTempFeeSeparateBL.submitData(cInputData, "");
	}
}
