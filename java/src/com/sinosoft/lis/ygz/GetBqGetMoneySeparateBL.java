package com.sinosoft.lis.ygz;

import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LYPremSeparateDetailSchema;
import com.sinosoft.lis.vschema.LYPremSeparateDetailSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 保全退费价税分离数据提数
 * @author lzy
 *
 */
public class GetBqGetMoneySeparateBL {
	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	/** 数据操作字符串 */
	private String mOperate;
	/** 开始日期*/
	private String mStartDate;
	/** 结束日期*/
	private String mEndDate;
	//页面手工提取标志
	private String mHandFlag;
	
	private String mWherePart;
	//业务处理相关变量
	private MMap mMap = new MMap();
	
	private String mCurrentDate = PubFun.getCurrentDate();
	private String mCurrentTime = PubFun.getCurrentTime();
	private boolean mIsclycle= true;
	private int sysCount = 5000;//默认5000
	
	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		// 进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GetTempFeeBqSeparateBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败GetTempFeeBqSeparateBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}

//		// 准备往后台的数据
//		if (!prepareOutputData()) {
//			return false;
//		}
//
//		// 保存数据
//		PubSubmit tPubSubmit = new PubSubmit();
//
//		if (!tPubSubmit.submitData(mInputData, mOperate)) {
//			System.out.print("保存数据返回");
//			// @@错误处理
//			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
//			CError tError = new CError();
//			tError.moduleName = "LCProjectCancelBL";
//			tError.functionName = "submitData";
//			tError.errorMessage = "数据提交失败!";
//			this.mErrors.addOneError(tError);
//			return false;
//		}
//		mInputData = null;
		return true;
	}

	// 获得输出数据
	private boolean getInputData(VData cInputData) {
		TransferData tTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
		mStartDate = (String) tTransferData.getValueByName("StartDate");
		mEndDate = (String) tTransferData.getValueByName("EndDate");
		
		mHandFlag = (String) tTransferData.getValueByName("HandWorkFlag");
		mWherePart = "";
		if(null != mHandFlag && !"".equals(mHandFlag)){
			LYPremSeparateDetailSet tLYPremSeparateDetailSet=(LYPremSeparateDetailSet) cInputData
			.getObjectByObjectName("LYPremSeparateDetailSet", 0);
			if(null != tLYPremSeparateDetailSet){
				String lisStr="";
				for(int i=1; i<=tLYPremSeparateDetailSet.size();i++){
					lisStr += "'"+ tLYPremSeparateDetailSet.get(i).getOtherNo()+"'";
					if(i<tLYPremSeparateDetailSet.size()){
						lisStr += ",";
					}
				}
				mWherePart = " and endorse.endorsementno in ("+lisStr+") ";
			}
		}
		return true;
	}
	
//  业务处理
	private boolean dealData() {
		YGZ mYGZ = new YGZ();
		LYPremSeparateDetailSet tLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
		String tWhereSQL = "";
		String tDealDate = "2016-05-01";
		if(mStartDate != null && !"".equals(mStartDate)){
			tWhereSQL += " and lja.makedate >= '" + mStartDate + "' ";
		}
		if(mEndDate != null && !"".equals(mEndDate)){
			tWhereSQL += " and lja.makedate <= '" + mEndDate + "' ";
		}
		if("".equals(tWhereSQL)){
			tWhereSQL += " and lja.makedate >='"+tDealDate+"' and lja.makedate >= current date - 7 days ";
		}
		
		if(null != mHandFlag && !"".equals(mHandFlag)){
			tWhereSQL ="";
		}
		String tSQL ="";
	    while(mIsclycle)
		  {
		//个单
	    	tSQL =  "select trim(lja.actugetno)||trim(lja.othernotype)||trim(endorse.riskcode)||trim(endorse.feeoperationtype)||trim(endorse.feefinatype) BusiNo, "
			+ " lja.ActuGetNo ActuGetNo,lja.OtherNo OtherNo,lja.OtherNoType OtherNoType, "
			+ " (select prtno from lccont where contno = endorse.contno union select prtno from lbcont where contno = endorse.contno) PrtNo, "
			+ " endorse.ContNo PolicyNo, "
			+ " endorse.riskcode,lja.managecom,sum(endorse.getmoney),endorse.feeoperationtype,endorse.feefinatype "
			+ " from ljaget lja,ljagetendorse endorse "
			+ " where 1=1 "
			+ " and lja.actugetno = endorse.actugetno "
			+ " and lja.otherno = endorse.endorsementno "
			+ " and lja.othernotype = '10' "
			+ " and endorse.feefinatype not in ('YEI','YEO') "
			+ " and (endorse.grpcontno is null or endorse.grpcontno='00000000000000000000') "
			+ " and not exists (select busino from LYPremSeparateDetail where busino=(trim(lja.actugetno)||trim(lja.othernotype)||trim(endorse.riskcode)||trim(endorse.feeoperationtype)||trim(endorse.feefinatype))) "
			+ " and exists (select 1 from lccont where conttype='1' and appflag='1' and contno=endorse.contno  "
			+ " 	union select 1 from lbcont where conttype='1' and contno=endorse.contno ) "
			+ tWhereSQL
			+ mWherePart
			+ " group by lja.actugetno,lja.otherno,lja.othernotype,endorse.riskcode,endorse.contno,lja.managecom,endorse.feeoperationtype,endorse.feefinatype "
			+ " fetch first 10000 rows only " 
			+ " with ur";
		
		SSRS tNoZeroSSRS = new ExeSQL().execSQL(tSQL);
		
		if(tNoZeroSSRS == null|| tNoZeroSSRS.getMaxRow()==0){
			mIsclycle = false;
		}

		if(tNoZeroSSRS.getMaxRow()==10000){
			mIsclycle = true;
		}else {
			mIsclycle = false;
		}	

		if(tNoZeroSSRS != null && tNoZeroSSRS.getMaxRow() > 0){
			for (int i = 1; i <= tNoZeroSSRS.getMaxRow(); i++) {
				LYPremSeparateDetailSchema detailSchema=new LYPremSeparateDetailSchema();
				detailSchema.setBusiNo(tNoZeroSSRS.GetText(i, 1));
				detailSchema.setMoneyNo(tNoZeroSSRS.GetText(i, 2));//退费凭证号
				detailSchema.setOtherNo(tNoZeroSSRS.GetText(i, 3));
				detailSchema.setOtherNoType(tNoZeroSSRS.GetText(i, 4));
				detailSchema.setPrtNo(tNoZeroSSRS.GetText(i, 5));
				detailSchema.setPolicyNo(tNoZeroSSRS.GetText(i, 6));
				detailSchema.setRiskCode(tNoZeroSSRS.GetText(i, 7));
				detailSchema.setManageCom(tNoZeroSSRS.GetText(i, 8));
				detailSchema.setPayMoney(tNoZeroSSRS.GetText(i, 9));
				detailSchema.setState("01");//数据提取状态
				detailSchema.setOtherState("02");//实付提取数据
				detailSchema.setMoneyType("02");//收付费类型:01-收费；02-付费
				detailSchema.setFeeOperationType(tNoZeroSSRS.GetText(i, 10));
				detailSchema.setFeeFinaType(tNoZeroSSRS.GetText(i, 11));
				detailSchema.setMakeDate(mCurrentDate);
				detailSchema.setMakeTime(mCurrentTime);
				detailSchema.setModifyDate(mCurrentDate);
				detailSchema.setModifyTime(mCurrentTime);
				String BusiType="01";//默认为退保费用
//				if(BQ.FEEFINATYPE_GB.equals(tNoZeroSSRS.GetText(i, 11))){
//					BusiType = "09";
//				}
//				if("RFLX".equals(tNoZeroSSRS.GetText(i, 11)) || "FXLX".equals(tNoZeroSSRS.GetText(i, 11))){//利息
//					BusiType = "11";
//				}
				BusiType = mYGZ.getBusinType(detailSchema.getFeeOperationType(), detailSchema.getFeeFinaType(), detailSchema.getRiskCode());
				detailSchema.setBusiType(BusiType);
				
				tLYPremSeparateDetailSet.add(detailSchema);

			}
		}
		mapsubmit(tLYPremSeparateDetailSet);	
		tLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
		
		System.out.println("保全付费数据提取：1、已结案保全付费--个单："+tNoZeroSSRS.getMaxRow());
  }
	    mIsclycle = true;
	    while(mIsclycle)
		  {
		//团单
		tSQL = "select trim(lja.actugetno)||trim(lja.othernotype)||trim(endorse.riskcode)||trim(endorse.feeoperationtype)||trim(endorse.feefinatype) BusiNo, "
			+ " lja.ActuGetNo ActuGetNo,lja.OtherNo OtherNo,lja.OtherNoType OtherNoType, "
			+ " (select prtno from lcgrpcont where grpcontno = endorse.grpcontno union select prtno from lbgrpcont where grpcontno = endorse.grpcontno) PrtNo, "
			+ " endorse.GrpContNo PolicyNo, "
			+ " endorse.riskcode,lja.managecom,sum(endorse.getmoney),endorse.feeoperationtype,endorse.feefinatype "
			+ " from ljaget lja,ljagetendorse endorse "
			+ " where 1=1 "
			+ " and lja.actugetno=endorse.actugetno "
			+ " and lja.otherno=endorse.endorsementno "
			+ " and lja.othernotype='3' "
			+ " and (endorse.grpcontno is not null and endorse.grpcontno<>'00000000000000000000' ) "
//			+ " and lja.paymode not in ('B','J') "//过滤白条工单 //20160622取消
			+ " and endorse.feefinatype not in ('YEI','YEO') "
			+ " and not exists (select busino from LYPremSeparateDetail where busino=(trim(lja.actugetno)||trim(lja.othernotype)||trim(endorse.riskcode)||trim(endorse.feeoperationtype)||trim(endorse.feefinatype))) "
//			+ " and exists (select 1 from lcgrpcont where  appflag='1' and grpcontno=endorse.grpcontno and coinsuranceflag <>'1' "
//			+ " 	union select 1 from lbgrpcont where grpcontno=endorse.grpcontno and coinsuranceflag <>'1') "
			+ tWhereSQL
			+ mWherePart
			+ " group by lja.actugetno,lja.otherno,lja.othernotype,endorse.riskcode,endorse.GrpContno,lja.managecom,endorse.feeoperationtype,endorse.feefinatype "
			+ " fetch first 10000 rows only " 
			+ " with ur";
		
		SSRS tSSRSNo2 = new ExeSQL().execSQL(tSQL);
		if(tSSRSNo2 == null|| tSSRSNo2.getMaxRow()==0){
			mIsclycle = false;
		}

		if(tSSRSNo2.getMaxRow()==10000){
			mIsclycle = true;
		}else {
			mIsclycle = false;
		}	
		
		if(tSSRSNo2 != null && tSSRSNo2.getMaxRow() > 0){
			for (int i = 1; i <= tSSRSNo2.getMaxRow(); i++) {
				LYPremSeparateDetailSchema detailSchema=new LYPremSeparateDetailSchema();
				detailSchema.setBusiNo(tSSRSNo2.GetText(i, 1));
				detailSchema.setMoneyNo(tSSRSNo2.GetText(i, 2));//退费凭证号
				detailSchema.setOtherNo(tSSRSNo2.GetText(i, 3));
				detailSchema.setOtherNoType(tSSRSNo2.GetText(i, 4));
				detailSchema.setPrtNo(tSSRSNo2.GetText(i, 5));
				detailSchema.setPolicyNo(tSSRSNo2.GetText(i, 6));
				detailSchema.setRiskCode(tSSRSNo2.GetText(i, 7));
				detailSchema.setManageCom(tSSRSNo2.GetText(i, 8));
				detailSchema.setPayMoney(tSSRSNo2.GetText(i, 9));
				detailSchema.setState("01");//数据提取状态
				detailSchema.setOtherState("02");//实付提取数据
				detailSchema.setMoneyType("02");//收付费类型:01-收费；02-付费
				detailSchema.setFeeOperationType(tSSRSNo2.GetText(i, 10));
				detailSchema.setFeeFinaType(tSSRSNo2.GetText(i, 11));
				detailSchema.setMakeDate(mCurrentDate);
				detailSchema.setMakeTime(mCurrentTime);
				detailSchema.setModifyDate(mCurrentDate);
				detailSchema.setModifyTime(mCurrentTime);
				String BusiType="01";//默认为保费
				
//				if(BQ.FEEFINATYPE_GB.equals(tSSRSNo2.GetText(i, 11))){
//					BusiType = "09";
//				}
				BusiType = mYGZ.getBusinType(detailSchema.getFeeOperationType(), detailSchema.getFeeFinaType(), detailSchema.getRiskCode());
				detailSchema.setBusiType(BusiType);
				
				tLYPremSeparateDetailSet.add(detailSchema);
			}
		}
		mapsubmit(tLYPremSeparateDetailSet);	
		tLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
		System.out.println("保全付费数据提取：2、已结案保全--团单："+tSSRSNo2.getMaxRow());
	 }	
		
		//团单保全退费转入投保人账户
		tSQL = "select trim(lja.payno)||trim(endorse.riskcode)||trim(endorse.feeoperationtype)||trim(endorse.feefinatype) BusiNo, "
			+ " lja.payno moneyno,lja.incomeno OtherNo,lja.incometype OtherNoType, "
			+ " (select prtno from lcgrpcont where grpcontno = endorse.grpcontno union select prtno from lbgrpcont where grpcontno = endorse.grpcontno) PrtNo, "
			+ " endorse.GrpContNo PolicyNo, "
			+ " endorse.riskcode,lja.managecom,sum(endorse.getmoney),endorse.feeoperationtype,endorse.feefinatype "
			+ " from ljapay lja,ljagetendorse endorse "
			+ " where 1=1 "
			+ " and lja.payno=endorse.actugetno "
			+ " and lja.incomeno=endorse.endorsementno "
			+ " and lja.incometype='3' "
			+ " and (endorse.grpcontno is not null and endorse.grpcontno<>'00000000000000000000' ) "
			+ " and lja.sumactupaymoney = 0 "//退费金额为0 则会生成实收为0 
			+ " and endorse.feefinatype not in ('YEI','YEO') "
			+ " and exists (select 1 from ljagetendorse where endorsementno=endorse.endorsementno and actugetno=endorse.actugetno and feefinatype='YEI' and getmoney>0) "//含有退费转入投保人账户记录的
			+ " and not exists (select busino from LYPremSeparateDetail where busino=(trim(lja.payno)||trim(endorse.riskcode)||trim(endorse.feeoperationtype)||trim(endorse.feefinatype)) ) "
			+ " and exists (select 1 from lcgrpcont where  appflag='1' and grpcontno=endorse.grpcontno and coinsuranceflag <>'1' "
			+ " 	union select 1 from lbgrpcont where grpcontno=endorse.grpcontno and coinsuranceflag <>'1') "
			+ tWhereSQL
			+ mWherePart
			+ " group by lja.payno,lja.incomeno,lja.incometype,endorse.riskcode,endorse.GrpContno,lja.managecom,endorse.feeoperationtype,endorse.feefinatype "
			+ " with ur";
		
		SSRS tSSRSNo3 = new ExeSQL().execSQL(tSQL);
		if(tSSRSNo3 != null && tSSRSNo3.getMaxRow() > 0){
			for (int i = 1; i <= tSSRSNo3.getMaxRow(); i++) {
				LYPremSeparateDetailSchema detailSchema=new LYPremSeparateDetailSchema();
				detailSchema.setBusiNo(tSSRSNo3.GetText(i, 1));
				detailSchema.setMoneyNo(tSSRSNo3.GetText(i, 2));//退费凭证号
				detailSchema.setOtherNo(tSSRSNo3.GetText(i, 3));
				detailSchema.setOtherNoType(tSSRSNo3.GetText(i, 4));
				detailSchema.setPrtNo(tSSRSNo3.GetText(i, 5));
				detailSchema.setPolicyNo(tSSRSNo3.GetText(i, 6));
				detailSchema.setRiskCode(tSSRSNo3.GetText(i, 7));
				detailSchema.setManageCom(tSSRSNo3.GetText(i, 8));
				detailSchema.setPayMoney(tSSRSNo3.GetText(i, 9));
				detailSchema.setState("01");//数据提取状态
				detailSchema.setOtherState("02");//实付提取数据
				detailSchema.setMoneyType("02");//收付费类型:01-收费；02-付费
				detailSchema.setFeeOperationType(tSSRSNo3.GetText(i, 10));
				detailSchema.setFeeFinaType(tSSRSNo3.GetText(i, 11));
				detailSchema.setMakeDate(mCurrentDate);
				detailSchema.setMakeTime(mCurrentTime);
				detailSchema.setModifyDate(mCurrentDate);
				detailSchema.setModifyTime(mCurrentTime);
				String BusiType="01";//默认为保费
				
//				if(BQ.FEEFINATYPE_GB.equals(tSSRSNo3.GetText(i, 11))){
//					BusiType = "09";
//				}
				BusiType = mYGZ.getBusinType(detailSchema.getFeeOperationType(), detailSchema.getFeeFinaType(), detailSchema.getRiskCode());
				detailSchema.setBusiType(BusiType);
				
				tLYPremSeparateDetailSet.add(detailSchema);
			}
		}
		System.out.println("保全付费数据提取：3、团单保全退费转入投保人账户："+tSSRSNo3.getMaxRow());
		mapsubmit(tLYPremSeparateDetailSet);	
		tLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
		
		//个单保全退费转入投保人账户的数据
		 tSQL =  "select trim(lja.payno)||trim(endorse.riskcode)||trim(endorse.feeoperationtype)||trim(endorse.feefinatype) BusiNo, "
			+ " lja.payno moneyno,lja.incomeno OtherNo,lja.incometype OtherNoType, "
			+ " (select prtno from lccont where contno = endorse.contno union select prtno from lbcont where contno = endorse.contno) PrtNo, "
			+ " endorse.ContNo PolicyNo, "
			+ " endorse.riskcode,lja.managecom,sum(endorse.getmoney),endorse.feeoperationtype,endorse.feefinatype "
			+ " from ljapay lja,ljagetendorse endorse "
			+ " where 1=1 "
			+ " and lja.payno=endorse.actugetno "
			+ " and lja.incomeno=endorse.endorsementno "
			+ " and lja.incometype='10' "
			+ " and endorse.feefinatype not in ('YEI','YEO') "
			+ " and lja.sumactupaymoney = 0 "//退费金额为0 则会生成实收为0
			+ " and exists (select 1 from ljagetendorse where endorsementno=endorse.endorsementno and actugetno=endorse.actugetno and feefinatype='YEI' and getmoney>0) "//含有退费转入投保人账户记录的
			+ " and (endorse.grpcontno is null or endorse.grpcontno='00000000000000000000') "
			+ " and not exists (select busino from LYPremSeparateDetail where busino=(trim(lja.payno)||trim(endorse.riskcode)||trim(endorse.feeoperationtype)||trim(endorse.feefinatype))) "
			+ " and exists (select 1 from lccont where conttype='1' and appflag='1' and contno=endorse.contno  "
			+ " 	union select 1 from lbcont where conttype='1' and contno=endorse.contno ) "
			+ tWhereSQL
			+ mWherePart
			+ " group by lja.payno,lja.incomeno,lja.incometype,endorse.riskcode,endorse.contno,lja.managecom,endorse.feeoperationtype,endorse.feefinatype "
			+ " with ur";
		
		SSRS tSSRSNo4 = new ExeSQL().execSQL(tSQL);
		if(tSSRSNo4 != null && tSSRSNo4.getMaxRow() > 0){
			for (int i = 1; i <= tSSRSNo4.getMaxRow(); i++) {
				LYPremSeparateDetailSchema detailSchema=new LYPremSeparateDetailSchema();
				detailSchema.setBusiNo(tSSRSNo4.GetText(i, 1));
				detailSchema.setMoneyNo(tSSRSNo4.GetText(i, 2));//退费凭证号
				detailSchema.setOtherNo(tSSRSNo4.GetText(i, 3));
				detailSchema.setOtherNoType(tSSRSNo4.GetText(i, 4));
				detailSchema.setPrtNo(tSSRSNo4.GetText(i, 5));
				detailSchema.setPolicyNo(tSSRSNo4.GetText(i, 6));
				detailSchema.setRiskCode(tSSRSNo4.GetText(i, 7));
				detailSchema.setManageCom(tSSRSNo4.GetText(i, 8));
				detailSchema.setPayMoney(tSSRSNo4.GetText(i, 9));
				detailSchema.setState("01");//数据提取状态
				detailSchema.setOtherState("02");//实付提取数据
				detailSchema.setMoneyType("02");//收付费类型:01-收费；02-付费
				detailSchema.setFeeOperationType(tSSRSNo4.GetText(i, 10));
				detailSchema.setFeeFinaType(tSSRSNo4.GetText(i, 11));
				detailSchema.setMakeDate(mCurrentDate);
				detailSchema.setMakeTime(mCurrentTime);
				detailSchema.setModifyDate(mCurrentDate);
				detailSchema.setModifyTime(mCurrentTime);
				String BusiType="01";//默认为退保费用
//				if(BQ.FEEFINATYPE_GB.equals(tSSRSNo4.GetText(i, 11))){
//					BusiType = "09";
//				}
//				if("RFLX".equals(tSSRSNo4.GetText(i, 11)) || "FXLX".equals(tSSRSNo4.GetText(i, 11))){//利息
//					BusiType = "11";
//				}
				BusiType = mYGZ.getBusinType(detailSchema.getFeeOperationType(), detailSchema.getFeeFinaType(), detailSchema.getRiskCode());
				detailSchema.setBusiType(BusiType);
				
				tLYPremSeparateDetailSet.add(detailSchema);
			}
		}
		System.out.println("保全付费数据提取：个单保全退费转入投保人账户："+tSSRSNo4.getMaxRow());
			
		mapsubmit(tLYPremSeparateDetailSet);	
		tLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
		
		return true;
	}
	/**
	 * 准备后台的数据
	 * @return boolean
	 */
	private boolean prepareOutputData() {
		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GetBqTempFeeSeparateBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
	private boolean mapsubmit(LYPremSeparateDetailSet tLYPremSeparateDetailSet){
		int count=0;
		LYPremSeparateDetailSet tDetailSet = new LYPremSeparateDetailSet();
		if(null == tLYPremSeparateDetailSet
				|| tLYPremSeparateDetailSet.size() < 1){
			System.out.println("===没有需要价税分离的数据！===");
		}
		for(int i = 1; i <= tLYPremSeparateDetailSet.size(); i++){
			LYPremSeparateDetailSchema detailSchema=tLYPremSeparateDetailSet.get(i).getSchema();
			detailSchema.setModifyDate(PubFun.getCurrentDate());
			detailSchema.setModifyTime(PubFun.getCurrentTime());
			tDetailSet.add(detailSchema);
			count ++;
			if(count >= sysCount || i == tLYPremSeparateDetailSet.size()){
				count = 0;
				mMap=new MMap();
				mMap.put(tDetailSet, SysConst.INSERT);
				tDetailSet=new LYPremSeparateDetailSet();
				if (!prepareOutputData()) {
					return false;
				}

				// 保存数据
				PubSubmit tPubSubmit = new PubSubmit();

				if (!tPubSubmit.submitData(mInputData, mOperate)) {
					System.out.print("保存数据返回");
					// @@错误处理
					this.mErrors.copyAllErrors(tPubSubmit.mErrors);
					CError tError = new CError();
					tError.moduleName = "GetTempFeeSeparateBL";
					tError.functionName = "submitData";
					tError.errorMessage = "数据提交失败!";
					this.mErrors.addOneError(tError);
//					return false;
				}
				mInputData = null;
			}
		}
		System.out.println("结束！！！！");
		return true;
	}
	public static void main(String[] args) {
		GetBqGetMoneySeparateBL tGetBqGetMoneySeparateBL = new GetBqGetMoneySeparateBL();
		VData cInputData = new VData();
		TransferData tTransferData = new TransferData();
		String tStartDate = "2019-02-11";
		String tEndDate = "2019-02-11";
		tTransferData.setNameAndValue("StartDate", tStartDate);
		tTransferData.setNameAndValue("EndDate", tEndDate);
		cInputData.add(tTransferData);
		tGetBqGetMoneySeparateBL.submitData(cInputData, "");
	}
}
