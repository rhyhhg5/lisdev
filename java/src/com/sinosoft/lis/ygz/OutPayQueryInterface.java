package com.sinosoft.lis.ygz;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.xml.rpc.ParameterMode;
import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.ygz.obj.OutPayQueryBillInfo;
import com.sinosoft.lis.ygz.obj.OutPayQueryHead;
import com.sinosoft.lis.ygz.obj.OutPayQuerySendResult;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
* <p>
* Title: 查询接口
* </p>
* <p>
* Description:
* </p>
* <p>
* Copyright: Copyright (c) 2002
* </p>
* <p>
* Company: Sinosoft
* </p>
* 
* @author Alex
* @version 1.0
*/
public class OutPayQueryInterface {
	
	public CErrors mErrors = new CErrors();

	private String requestURL = "";
	
	private String reponseURL = "";
	
	private String aBatchNo = "" ;
		
	private String targetEendPoint = "";
	
	private String targetNameSpace = "";
	
	private OutPayQueryHead head = null;
	
	
	/**
	 * 依据请求报文对象集合调用接口
	 * 并获取价税分离结果，返回SendResult对象
	 * 
	 */
	public List<OutPayQuerySendResult> callInterface(List<OutPayQueryBillInfo> billInfoList){
		//数据准备
		if(!initData()){
			return null;
		}
		//封装请求报文
		Document requestDoc = packageRequest(head, billInfoList);
		//发送请求
		Document responseDoc=request(requestDoc);

		if(null == responseDoc){
			System.out.println("获取返回结果失败");
		}
		//解析返回报文
		List<OutPayQuerySendResult> result=parseDoc(responseDoc);
		
		return result;
	}
	private Document request(Document requestDoc) {
		String filePath =requestURL +aBatchNo + ".xml";
		try {
			//请求报文保存至应用服务器
			XMLOutputter outPutter = new XMLOutputter("  ", true, "UTF-8");
			FileOutputStream outStream = new FileOutputStream(filePath);
			outPutter.output(requestDoc, outStream);
			
			//读取报文调用接口
			InputStream mIs = new FileInputStream(filePath);
			byte[] mInXmlBytes = InputStreamToBytes(mIs);
			String mInXmlStr = new String(mInXmlBytes, "UTF-8");
			System.out.println("发票查询请求报文："+mInXmlStr);
			
			Service service = new Service();
			Call call = null;
			call = (Call) service.createCall();
			call.setTargetEndpointAddress(new URL(targetEendPoint));
			call.setOperationName("exoSysQuery");// 接口方法名称 synchronizeData
			call.addParameter("string", org.apache.axis.Constants.XSD_STRING,
					ParameterMode.IN);
			call.setReturnType(org.apache.axis.Constants.XSD_STRING);
			String result =(String) call.invoke(new Object[] { mInXmlStr });
			
			System.out.println("发票查询返回报文:" + result);
			
			StringReader tInXmlReader = new StringReader(result);
			SAXBuilder tSAXBuilder = new SAXBuilder();
			Document responseDoc = tSAXBuilder.build(tInXmlReader);
			
			String reponseFilePath = reponseURL + aBatchNo + ".xml";
			outStream = new FileOutputStream(reponseFilePath);
			outPutter.output(responseDoc, outStream);
			return responseDoc;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}
	}
	
	private boolean initData(){
		//获取系统根目录
		String str = "select sysvarvalue from LDSysVar where sysvar='ServerRoot'";
		String url = new ExeSQL().getOneValue(str);
//		String url ="F:/";
		requestURL = url+"ygzpayqxml/request/"+PubFun.getCurrentDate2()+"/";
		reponseURL = url+"ygzpayqxml/reponse/"+PubFun.getCurrentDate2()+"/";
		File mFileDir = new File(requestURL);
		if (!mFileDir.exists()) {
			if (!mFileDir.mkdirs()) {
				CError tError = new CError();
				tError.moduleName = "OutPayQueryInterface";
				tError.functionName = "initData";
				tError.errorMessage = "创建目录[" + url.toString() + "]失败！"+ mFileDir.getPath();
				this.mErrors.addOneError(tError);
				return false;
			}
		}
		mFileDir = new File(reponseURL);
		if (!mFileDir.exists()) {
			if (!mFileDir.mkdirs()) {
				CError tError = new CError();
				tError.moduleName = "OutPayQueryInterface";
				tError.functionName = "initData";
				tError.errorMessage = "创建目录[" + url.toString() + "]失败！"+ mFileDir.getPath();
				this.mErrors.addOneError(tError);
				return false;
			}
		}
		
		SSRS tSSRS= new ExeSQL().execSQL("select codename,codealias from ldcode where codetype='outpayquery' ");
		if(null == tSSRS || tSSRS.MaxRow == 0){
			CError tError = new CError();
			tError.moduleName = "OutPayQueryInterface";
			tError.functionName = "initData";
			tError.errorMessage = "获取目标接口信息失败";
			this.mErrors.addOneError(tError);
			return false;
		}
		targetEendPoint=tSSRS.GetText(1, 1);
		targetNameSpace=tSSRS.GetText(1, 2);
		
		
		//为本次请求生成批次号
		String tCurrentDate2=PubFun.getCurrentDate2();
		String tCurrentTime2=PubFun.getCurrentTime2();
		aBatchNo =tCurrentDate2+tCurrentTime2+PubFun1.CreateMaxNo("OUTP"+tCurrentDate2, 6);
		System.out.println("本次请求批次号："+aBatchNo);
		
		//初始化报文头
		head =  new OutPayQueryHead();
		head.setDefault();
		head.setSender("01");
//		head.setFilename(mBatchNo+".xml");
		
		return true;
	}
	
	private static byte[] InputStreamToBytes(InputStream pIns) {
		ByteArrayOutputStream mByteArrayOutputStream = new ByteArrayOutputStream();
		
		try {
			byte[] tBytes = new byte[8*1024];
			for (int tReadSize; -1 != (tReadSize=pIns.read(tBytes)); ) {
				mByteArrayOutputStream.write(tBytes, 0, tReadSize);
			}
		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		} finally {
			try {
				pIns.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		
		return mByteArrayOutputStream.toByteArray();
	}

	private Document packageRequest(OutPayQueryHead head, List billInfoList) {
		if (null == head || billInfoList.size() == 0) {
			return null;
		}
		// 根节点
		Element ufinterface = new Element("ufinterface");
		Document Doc = new Document(ufinterface);
		// 节点属性
		ufinterface.addAttribute("account", head.getAccount());
		ufinterface.addAttribute("billtype", head.getBilltype());
		ufinterface.addAttribute("filename", head.getFilename());
		ufinterface.addAttribute("sender", head.getSender());
		ufinterface.addAttribute("groupcode", head.getGroupcode());
	

		// 对每笔业务数据进行封装
	
		for (int i = 0; i < billInfoList.size(); i++) {

		    OutPayQueryBillInfo tBillInfo = (OutPayQueryBillInfo) billInfoList.get(i);
			Element billElement = new Element("bill");
			billElement.addAttribute("id",tBillInfo.getId());
			Element billHeadElement = new Element("billhead");
			billHeadElement.addContent(new Element("transerial").setText(tBillInfo.getTranserial()));
			
			billElement.addContent(billHeadElement);
			ufinterface.addContent(billElement);
		}

		return Doc;
	}
	
	private List<OutPayQuerySendResult> parseDoc(Document document){
		Document tXmlDoc = (Document) document.clone();
		//1、对报文头进行解析
		Element rootElement=tXmlDoc.getRootElement();


		//2、解析报文返回结果
		List queryresultList = (List)rootElement.getChildren("queryresult");
		
		List<OutPayQuerySendResult> OutPayQuerySendResultList =new ArrayList<OutPayQuerySendResult>();
		for(int x=0; x<queryresultList.size(); x++ ){
			Element queryresult=(Element)queryresultList.get(x);
			
			String id = getValueByAttrName(queryresult,"id");
			
			Element head = queryresult.getChild("head");
			
			String resultcode = head.getChildTextTrim("resultcode");
			
			Element sendresults = queryresult.getChild("sendresults");
			
			List sendresult = (List)sendresults.getChildren("sendresult");
			
			
			for(int i=0; i<sendresult.size(); i++ ){
				OutPayQuerySendResult tOutPayQuerySendResult=new OutPayQuerySendResult();
	//			List<Content> contentList=new ArrayList<Content>();
				Element resultEle=(Element)sendresult.get(i);
				tOutPayQuerySendResult.setId(id);
				tOutPayQuerySendResult.setResultcode(resultcode);
				tOutPayQuerySendResult.setBillnum(getTextByNodeName(resultEle,"billnum"));
				tOutPayQuerySendResult.setIsprint(getTextByNodeName(resultEle,"isprint"));
				tOutPayQuerySendResult.setVstatus(getTextByNodeName(resultEle,"vstatus"));
				tOutPayQuerySendResult.setPrintdate(getTextByNodeName(resultEle,"printdate"));
	
				OutPayQuerySendResultList.add(tOutPayQuerySendResult);
			}
		}
		return OutPayQuerySendResultList;
	}
	
	private String getTextByNodeName(Element element,String childName){
		Element child=element.getChild(childName);
		if(null == child){
			return null;
		}
		return child.getTextTrim();
	}
	
	private String getValueByAttrName(Element element,String attrName){
		Attribute attr= element.getAttribute(attrName);
		if(null == attr){
			return null;
		}else{
			return attr.getValue();
		}
	}
	
	public static void main(String[] args) {
		OutPayQueryInterface tt=new OutPayQueryInterface();
		String mInFilePath = "F:/ygzxml/reponse/20160523141658000006.xml";
		InputStream mIs;
		try {
			mIs = new FileInputStream(mInFilePath);
			byte[] mInXmlBytes = tt.InputStreamToBytes(mIs);
			String mInXmlStr = new String(mInXmlBytes, "GBK");
			
			StringReader tInXmlReader = new StringReader(mInXmlStr);
			SAXBuilder tSAXBuilder = new SAXBuilder();
			Document responseDoc = tSAXBuilder.build(tInXmlReader);
			
			tt.parseDoc(responseDoc);
		} catch (FileNotFoundException e) {
			
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (JDOMException e) {
			e.printStackTrace();
		}
		
	}

}
