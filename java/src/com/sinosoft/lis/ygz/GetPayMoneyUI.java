package com.sinosoft.lis.ygz;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class GetPayMoneyUI {

	/** �������� */
    public CErrors mErrors = new CErrors();
    
    public GetPayMoneyUI(){}
    
    public boolean submitData(VData cInputData, String cOperate){
    	
    	try{
    		GetPayMoneyBL tGetPayMoneyBL = new GetPayMoneyBL();
            if (!tGetPayMoneyBL.submitData(cInputData, cOperate)){
                this.mErrors.copyAllErrors(tGetPayMoneyBL.mErrors);
                return false;
            }
            return true;
        }catch (Exception e){
            e.printStackTrace();
            this.mErrors.addOneError(e.getMessage());
            return false;
        }
    }
}
