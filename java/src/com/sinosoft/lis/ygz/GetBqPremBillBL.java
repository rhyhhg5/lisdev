package com.sinosoft.lis.ygz;

import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LYOutPayDetailSchema;
import com.sinosoft.lis.schema.LYPremSeparateDetailSchema;
import com.sinosoft.lis.vschema.LYOutPayDetailSet;
import com.sinosoft.lis.vschema.LYPremSeparateDetailSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 保全收费发票明细数据报送
 * @author lzy
 * 
 */
public class GetBqPremBillBL {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	
	private GlobalInput mGI = new GlobalInput();
	/** 数据操作字符串 */
	private String mOperate;
	/** 开始日期 */
	private String mStartDate;
	/** 结束日期 */
	private String mEndDate;
	/** 页面手工提取标记*/
	private String mHandFlag ;
	
	private String mWherePart ;
	
	// 业务处理相关变量
	private MMap mMap = new MMap();

	private String mCurrentDate = PubFun.getCurrentDate();
	private String mCurrentTime = PubFun.getCurrentTime();

	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		// 进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GetBqPremBillBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败GetTempFeeBqSeparateBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}

		// 准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}

		// 保存数据
		PubSubmit tPubSubmit = new PubSubmit();

		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			System.out.print("保存数据返回");
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "GetBqPremBillBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mInputData = null;
		return true;
	}

	// 获得输出数据
	private boolean getInputData(VData cInputData) {
		TransferData tTransferData = (TransferData) cInputData
				.getObjectByObjectName("TransferData", 0);
		mStartDate = (String) tTransferData.getValueByName("StartDate");
		mEndDate = (String) tTransferData.getValueByName("EndDate");
		if(null == mGI || null==mGI.Operator){
			mGI.Operator="server";
		}
		
		mHandFlag = (String) tTransferData.getValueByName("HandWorkFlag");
		mWherePart = "";
		
		if(null != mHandFlag && !"".equals(mHandFlag)){
			LYPremSeparateDetailSet tLYPremSeparateDetailSet=(LYPremSeparateDetailSet) cInputData
			.getObjectByObjectName("LYPremSeparateDetailSet", 0);
			if(null != tLYPremSeparateDetailSet){
				String lisStr="";
				for(int i=1; i<=tLYPremSeparateDetailSet.size();i++){
					lisStr += "'"+ tLYPremSeparateDetailSet.get(i).getOtherNo()+"'";
					if(i<tLYPremSeparateDetailSet.size()){
						lisStr += ",";
					}
				}
				mWherePart = " and lyp.otherno in ("+lisStr+") ";
			}
		}
		
		return true;
	}

	// 业务处理
	private boolean dealData() {

		LYOutPayDetailSet tLYOutPayDetailSet = new LYOutPayDetailSet();
		String tWhereSQL = "";
		if (null != mStartDate  && !"".equals(mStartDate)) {
			tWhereSQL += " and lja.confdate >= '2016-05-01' and lja.confdate >= '" + mStartDate + "' ";
		}
		if (null != mEndDate && !"".equals(mEndDate)) {
			tWhereSQL += " and lja.confdate >= '2016-05-01' and lja.confdate <= '" + mEndDate + "' ";
		}
		if ("".equals(tWhereSQL)) {
			tWhereSQL += " and lja.confdate >= '2016-05-01' and lja.confdate >= current date - 7 days ";
		}
		if( null != mWherePart && !"".equals(mWherePart)){
			tWhereSQL = "";
		}
		//团单保全收费数据
		String tSQL = " select "
					+ " lyp.busino,"//流水号
					+ " (select managecom from lcgrpcont where grpcontno=lyp.policyno union select managecom from lbgrpcont where grpcontno=lyp.policyno) , "//所属组织
					+ " (select appntno from lcgrpcont where grpcontno=lyp.policyno union select appntno from lbgrpcont where grpcontno=lyp.policyno) , "//客户号
					+ " (select grpname from lcgrpcont where grpcontno=lyp.policyno union select grpname from lbgrpcont where grpcontno=lyp.policyno) , "
					+ " '1', "//客户类型:“1”代表机构，“2”代表个人
					+ " '01', "//价税分离项目:固定值
					+ " lyp.taxrate,"
					+ " lyp.moneyNoTax,"
					+ " lyp.moneyTax, "
					+ " '0', "//申报类型：0正常申报，固定值
					+ " '01', "//来源系统:固定值
					+ " lja.confdate,"//交易日期
					+ " lyp.riskcode,"//产品
					+ " lyp.policyno,"
					+ " lyp.busitype,"//业务类型
					+ " 'CNY',"//交易币种
					+ " lyp.paymoney,"//交易金额
					+ " '0', "//金额是否含税:“0”表示含税，“1”表示不含税
					+ " lyp.moneytype,"//收付标志
					+ " '0',"//境内外标示:“0”表示境内，“1”表示境外
					+ " 'Y',"//是否开票
					+ " '0',"//汇总地类型:“0”表示汇总，“1”表示属地，后续纳税申报需要
					+ " lyp.prtno, "//投保单号
					+ " (select signdate from lcgrpcont where grpcontno=lyp.policyno union select signdate from lbgrpcont where grpcontno=lyp.policyno) ,"//签单号
					+ " lja.incomeno, "//批单号
					+ " lyp.moneyno moneyno, "
					+ " (select riskname from lmriskapp where riskcode=lyp.riskcode),"
					+ " (select TaxpayerType from LCGrpAppnt where GrpContNo = lyp.policyno union select TaxpayerType from LBGrpAppnt where GrpContNo = lyp.policyno),"//纳税人类型
					+ " nvl(nvl((select UnifiedSocialCreditNo from LCGrpAppnt where GrpContNo = lyp.policyno union select UnifiedSocialCreditNo from LBGrpAppnt where GrpContNo = lyp.policyno),(select TaxNo from LCGrpAppnt where GrpContNo = lyp.policyno union select TaxNo from LBGrpAppnt where GrpContNo = lyp.policyno)),(select OrgancomCode from LCGrpAppnt where GrpContNo = lyp.policyno union select OrgancomCode from LBGrpAppnt where GrpContNo = lyp.policyno)),"//纳税识别号
					+ " (select Phone from LCGrpAppnt where GrpContNo = lyp.policyno union select Phone from LBGrpAppnt where GrpContNo = lyp.policyno),"//联系电话
					+ " (select PostalAddress from LCGrpAppnt where GrpContNo = lyp.policyno union select PostalAddress from LBGrpAppnt where GrpContNo = lyp.policyno),"//客户地址
					+ " (select CustomerBankCode from LCGrpAppnt where GrpContNo = lyp.policyno union select CustomerBankCode from LBGrpAppnt where GrpContNo = lyp.policyno),"//开户银行
					+ " (select CustomerBankAccNo from LCGrpAppnt where GrpContNo = lyp.policyno union select CustomerBankAccNo from LBGrpAppnt where GrpContNo = lyp.policyno),"//银行账户
					+ " (select Mobile1 from LCGrpAddress where CustomerNo = (select CustomerNo from LCGrpAppnt where GrpContNo = lyp.policyno union select CustomerNo from LBGrpAppnt where GrpContNo = lyp.policyno) and AddressNo = (select AddressNo from LCGrpAppnt where GrpContNo = lyp.policyno union select AddressNo from LBGrpAppnt where GrpContNo = lyp.policyno)),"//移动电话
					+ " (select E_Mail1 from LCGrpAddress where CustomerNo = (select CustomerNo from LCGrpAppnt where GrpContNo = lyp.policyno union select CustomerNo from LBGrpAppnt where GrpContNo = lyp.policyno) and AddressNo = (select AddressNo from LCGrpAppnt where GrpContNo = lyp.policyno union select AddressNo from LBGrpAppnt where GrpContNo = lyp.policyno))"//电子邮箱
					+ " from LYPremSeparateDetail lyp,ljapay lja "
					+ " where 1=1 "//lyp.tempfeeno=lja.getnoticeno不靠谱
//					+ " and lyp.moneyno=lja.payno "
					+ " and lyp.otherno=lja.incomeno"
					+ " and lja.confdate is not null "
					+ " and lyp.tempfeetype in ('4','7','9') "//保全收费
					+ " and lyp.feefinatype not in ('YEI','YEO') "
					+ " and lyp.state='02' "//已经价税分离
//					+ " and lyp.moneyno is not null "
					+ " and not exists (select 1 from LYOutPayDetail where busino=lyp.busino and state<>'03') "
					+ " and exists (select 1 from lcgrpcont where grpcontno=lyp.policyno union select 1 from lbgrpcont where grpcontno=lyp.policyno) "
					+ tWhereSQL
					+ mWherePart
					+ " union "//20161102 a、保全收费完全从投保人账户抵扣的   b、保全退费转入投保人账户的
					+ " select "
					+ " lyp.busino,"//流水号
					+ " (select managecom from lcgrpcont where grpcontno=lyp.policyno union select managecom from lbgrpcont where grpcontno=lyp.policyno) , "//所属组织
					+ " (select appntno from lcgrpcont where grpcontno=lyp.policyno union select appntno from lbgrpcont where grpcontno=lyp.policyno) , "//客户号
					+ " (select grpname from lcgrpcont where grpcontno=lyp.policyno union select grpname from lbgrpcont where grpcontno=lyp.policyno) , "
					+ " '1', "//客户类型:“1”代表机构，“2”代表个人
					+ " '01', "//价税分离项目:固定值
					+ " lyp.taxrate,"
					+ " lyp.moneyNoTax,"
					+ " lyp.moneyTax, "
					+ " '0', "//申报类型：0正常申报，固定值
					+ " '01', "//来源系统:固定值
					+ " lja.confdate,"//交易日期
					+ " lyp.riskcode,"//产品
					+ " lyp.policyno,"
					+ " lyp.busitype,"//业务类型
					+ " 'CNY',"//交易币种
					+ " lyp.paymoney,"//交易金额
					+ " '0', "//金额是否含税:“0”表示含税，“1”表示不含税
					+ " lyp.moneytype,"//收付标志
					+ " '0',"//境内外标示:“0”表示境内，“1”表示境外
					+ " 'Y',"//是否开票
					+ " '0',"//汇总地类型:“0”表示汇总，“1”表示属地，后续纳税申报需要
					+ " lyp.prtno, "//投保单号
					+ " (select signdate from lcgrpcont where grpcontno=lyp.policyno union select signdate from lbgrpcont where grpcontno=lyp.policyno) ,"//签单号
					+ " lja.incomeno, "//批单号
					+ " lyp.moneyno moneyno, "
					+ " (select riskname from lmriskapp where riskcode=lyp.riskcode),"
					+ " (select TaxpayerType from LCGrpAppnt where GrpContNo = lyp.policyno union select TaxpayerType from LBGrpAppnt where GrpContNo = lyp.policyno),"//纳税人类型
					+ " (select TaxNo from LCGrpAppnt where GrpContNo = lyp.policyno union select TaxNo from LBGrpAppnt where GrpContNo = lyp.policyno),"//纳税识别号
					+ " (select Phone from LCGrpAppnt where GrpContNo = lyp.policyno union select Phone from LBGrpAppnt where GrpContNo = lyp.policyno),"//联系电话
					+ " (select PostalAddress from LCGrpAppnt where GrpContNo = lyp.policyno union select PostalAddress from LBGrpAppnt where GrpContNo = lyp.policyno),"//客户地址
					+ " (select CustomerBankCode from LCGrpAppnt where GrpContNo = lyp.policyno union select CustomerBankCode from LBGrpAppnt where GrpContNo = lyp.policyno),"//开户银行
					+ " (select CustomerBankAccNo from LCGrpAppnt where GrpContNo = lyp.policyno union select CustomerBankAccNo from LBGrpAppnt where GrpContNo = lyp.policyno),"//银行账户
					+ " (select Mobile1 from LCGrpAddress where CustomerNo = (select CustomerNo from LCGrpAppnt where GrpContNo = lyp.policyno union select CustomerNo from LBGrpAppnt where GrpContNo = lyp.policyno) and AddressNo = (select AddressNo from LCGrpAppnt where GrpContNo = lyp.policyno union select AddressNo from LBGrpAppnt where GrpContNo = lyp.policyno)),"//移动电话
					+ " (select E_Mail1 from LCGrpAddress where CustomerNo = (select CustomerNo from LCGrpAppnt where GrpContNo = lyp.policyno union select CustomerNo from LBGrpAppnt where GrpContNo = lyp.policyno) and AddressNo = (select AddressNo from LCGrpAppnt where GrpContNo = lyp.policyno union select AddressNo from LBGrpAppnt where GrpContNo = lyp.policyno))"//电子邮箱
					+ " from LYPremSeparateDetail lyp,ljapay lja "
					+ " where lyp.moneyno=lja.payno "
					+ " and lja.confdate is not null "
					+ " and lyp.tempfeetype is null "//全部从投保人账户抵扣的收费没有暂收
					+ " and lja.incometype ='3' "//团单保全
					+ " and lyp.feefinatype not in ('YEI','YEO') "
					+ " and lyp.state='02' "//已经价税分离
					+ " and lyp.moneyno is not null "
					+ " and not exists (select 1 from LYOutPayDetail where busino=lyp.busino and state<>'03') "
					+ " and exists (select 1 from lcgrpcont where grpcontno=lyp.policyno union select 1 from lbgrpcont where grpcontno=lyp.policyno) "
					+ tWhereSQL
					+ mWherePart
					+ " with ur";
		
		SSRS tSSRS = new ExeSQL().execSQL(tSQL);
		if (tSSRS != null && tSSRS.getMaxRow() > 0) {
			for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
				LYOutPayDetailSchema tLYOutPayDetailSchema = new LYOutPayDetailSchema();
				tLYOutPayDetailSchema.setBusiNo(tSSRS.GetText(i, 1));
				tLYOutPayDetailSchema.settranserial(tSSRS.GetText(i, 1));//交易流水号
				tLYOutPayDetailSchema.setbusipk(tSSRS.GetText(i, 1));
				tLYOutPayDetailSchema.setisupdate("Y");//是否更新客户信息
				tLYOutPayDetailSchema.setgroup("00");//所属集团:固定值
				tLYOutPayDetailSchema.setpkgroup("00");//所属集团:固定值
				tLYOutPayDetailSchema.setpkorg(tSSRS.GetText(i, 2));//所属组织
				tLYOutPayDetailSchema.setorg(tSSRS.GetText(i, 2));//所属组织
				tLYOutPayDetailSchema.setorgv(tSSRS.GetText(i, 2));//所属组织
				tLYOutPayDetailSchema.setdeptdoc(tSSRS.GetText(i, 2));
				tLYOutPayDetailSchema.setcode(tSSRS.GetText(i, 3));//客户编码
				tLYOutPayDetailSchema.setname(tSSRS.GetText(i, 4));//客户名称
	//			tLYOutPayDetailSchema.setcustomertype(tSSRS.GetText(i, 5));//客户类型
				tLYOutPayDetailSchema.setcustomertype("1");//modify 20160604 管理平台提供的默认值
				tLYOutPayDetailSchema.setcustcode(tSSRS.GetText(i, 3));
				tLYOutPayDetailSchema.setcustname(tSSRS.GetText(i, 4));
	//			tLYOutPayDetailSchema.setcusttype(tSSRS.GetText(i, 5));
				tLYOutPayDetailSchema.setcusttype("1");
				
				tLYOutPayDetailSchema.setptsitem(tSSRS.GetText(i, 6));
				tLYOutPayDetailSchema.settaxrate(tSSRS.GetText(i, 7));//税率
				tLYOutPayDetailSchema.setoriamt(tSSRS.GetText(i, 8));//不含税金额
				tLYOutPayDetailSchema.setoritax(tSSRS.GetText(i, 9));//税额
				tLYOutPayDetailSchema.setlocalamt(tSSRS.GetText(i, 8));//不含税金额（本币）
				tLYOutPayDetailSchema.setlocaltax(tSSRS.GetText(i, 9));//税额（本币）
				tLYOutPayDetailSchema.setdectype(tSSRS.GetText(i, 10));//申报类型
				tLYOutPayDetailSchema.setsrcsystem(tSSRS.GetText(i, 11));
				tLYOutPayDetailSchema.settrandate(tSSRS.GetText(i, 12));//交易日期
				String riskcode = tSSRS.GetText(i, 13);
				String moneyType = tSSRS.GetText(i, 15);
				if ("09".equals(moneyType)) {//工本费riskcode置0000
					riskcode = BQ.FILLDATA;
				} else {
					riskcode = riskcode.substring(0, 4);
				}
				tLYOutPayDetailSchema.setprocode(riskcode);
				tLYOutPayDetailSchema.setbusitype(moneyType);//同价税分离接口的收支类型
				tLYOutPayDetailSchema.settrancurrency(tSSRS.GetText(i, 16));// 币种：人民币
				tLYOutPayDetailSchema.settranamt(tSSRS.GetText(i, 17));//交易金额（含税）
				tLYOutPayDetailSchema.settaxtype(tSSRS.GetText(i, 18));//金额是否含税
				String paymentflag="0";//“0”表示收入，“1”表示支出
				if("02".equals(tSSRS.GetText(i, 19))){//价税分离表01表示收费 02表示付费
					paymentflag="1";
				}
				tLYOutPayDetailSchema.setpaymentflag(paymentflag);
				tLYOutPayDetailSchema.setoverseasflag(tSSRS.GetText(i, 20));
				tLYOutPayDetailSchema.setisbill(tSSRS.GetText(i, 21));
				tLYOutPayDetailSchema.setareatype(tSSRS.GetText(i, 22));
				tLYOutPayDetailSchema.setinsureno(tSSRS.GetText(i, 23));//印刷号
				tLYOutPayDetailSchema.setbilleffectivedate(tSSRS.GetText(i, 24));
				tLYOutPayDetailSchema.setbqbatchno(tSSRS.GetText(i, 25));
				tLYOutPayDetailSchema.setvdata(mCurrentDate);
				tLYOutPayDetailSchema.setvdef2(tSSRS.GetText(i, 14));
				tLYOutPayDetailSchema.setmoneyno(tSSRS.GetText(i, 26));
				tLYOutPayDetailSchema.setmoneytype(tSSRS.GetText(i, 19));
				//备注
				String vdef1="保单号:"+tSSRS.GetText(i, 14)+";险种："+tSSRS.GetText(i, 27);
				tLYOutPayDetailSchema.setvdef1(vdef1);
				tLYOutPayDetailSchema.setvoucherid(tSSRS.GetText(i, 14));//保单号
				tLYOutPayDetailSchema.setprebilltype("02");//非预打
				tLYOutPayDetailSchema.setinvtype("0");//默认0
				tLYOutPayDetailSchema.setvaddress("0");
				tLYOutPayDetailSchema.setvsrcsystem("01");
				tLYOutPayDetailSchema.settaxpayertype("01");//管理平台提供的默认值
				tLYOutPayDetailSchema.setenablestate("2");//默认值
				
				//2018-5-28 针对上海分公司增加
				if("8631".equals(tSSRS.GetText(i, 2).substring(0, 4))){
					//团单新增8个字段
					if(tSSRS.GetText(i, 28)==null||"".equals(tSSRS.GetText(i, 28))){
						tLYOutPayDetailSchema.settaxpayertype("01");
					}else{
						tLYOutPayDetailSchema.settaxpayertype("0"+tSSRS.GetText(i, 28));
					}
					
					tLYOutPayDetailSchema.settaxnumber(tSSRS.GetText(i, 29));
					tLYOutPayDetailSchema.settelephone(tSSRS.GetText(i, 30));
					tLYOutPayDetailSchema.setaddressee(tSSRS.GetText(i, 31));
					tLYOutPayDetailSchema.setbank(tSSRS.GetText(i, 32));
					tLYOutPayDetailSchema.setbankaccount(tSSRS.GetText(i, 33));
					tLYOutPayDetailSchema.setmobile(tSSRS.GetText(i, 34));
					tLYOutPayDetailSchema.setemail(tSSRS.GetText(i, 35));
				}
				
				/**
				 * 关于电子发票推送模式及规则调整，现增加发票类型，团单——1纸质发票，个单——2电子发票
				 * 大连分公司统一推电子发票
				 */
				if("8691".equals(tSSRS.GetText(i, 2).substring(0, 4))){
					tLYOutPayDetailSchema.setbilltype("2");
				}else{
					tLYOutPayDetailSchema.setbilltype("1");
				}
				//2018-2-6新增电子发票客户邮箱和客户手机号
				tLYOutPayDetailSchema.setbuymailbox(tSSRS.GetText(i, 35));
				tLYOutPayDetailSchema.setbuyphoneno(tSSRS.GetText(i, 34));
				
				//2018-3-13新增证件类型和证件号码
				tLYOutPayDetailSchema.setbuyidtype("");
				tLYOutPayDetailSchema.setbuyidnumber("");
				
				
				tLYOutPayDetailSchema.setoperator(mGI.Operator);
				tLYOutPayDetailSchema.setMakeDate(mCurrentDate);
				tLYOutPayDetailSchema.setMakeTime(mCurrentTime);
				tLYOutPayDetailSchema.setModifyDate(mCurrentDate);
				tLYOutPayDetailSchema.setModifyTime(mCurrentTime);
				
				tLYOutPayDetailSet.add(tLYOutPayDetailSchema);
			}
		}
			System.out.println("保全收费发票数据提取完成："+tLYOutPayDetailSet.size());
			//团单保全付费数据
			tSQL = " select "
				+ " lyp.busino,"//流水号
				+ " (select managecom from lcgrpcont where grpcontno=lyp.policyno union select managecom from lbgrpcont where grpcontno=lyp.policyno) , "//所属组织
				+ " (select appntno from lcgrpcont where grpcontno=lyp.policyno union select appntno from lbgrpcont where grpcontno=lyp.policyno) , "//客户号
				+ " (select grpname from lcgrpcont where grpcontno=lyp.policyno union select grpname from lbgrpcont where grpcontno=lyp.policyno) , "
				+ " '1', "//客户类型:“1”代表机构，“2”代表个人
				+ " '01', "//价税分离项目:固定值
				+ " lyp.taxrate,"
				+ " lyp.moneyNoTax,"
				+ " lyp.moneyTax, "
				+ " '0', "//申报类型：0正常申报，固定值
				+ " '01', "//来源系统:固定值
				+ " lja.confdate,"//交易日期
				+ " lyp.riskcode,"//产品
				+ " lyp.policyno,"
				+ " lyp.busitype,"//业务类型
				+ " 'CNY',"//交易币种
				+ " lyp.paymoney,"//交易金额
				+ " '0', "//金额是否含税:“0”表示含税，“1”表示不含税
				+ " lyp.moneytype,"//收付标志
				+ " '0',"//境内外标示:“0”表示境内，“1”表示境外
				+ " 'Y',"//是否开票
				+ " '0',"//汇总地类型:“0”表示汇总，“1”表示属地，后续纳税申报需要
				+ " lyp.prtno, "//投保单号
				+ " (select signdate from lcgrpcont where grpcontno=lyp.policyno union select signdate from lbgrpcont where grpcontno=lyp.policyno) ,"//签单号
				+ " lja.otherno, "//批单号
				+ " lyp.moneyno moneyno, "
				+ " (select riskname from lmriskapp where riskcode=lyp.riskcode),"
				+ " (select TaxpayerType from LCGrpAppnt where GrpContNo = lyp.policyno union select TaxpayerType from LBGrpAppnt where GrpContNo = lyp.policyno),"//纳税人类型
				+ " nvl(nvl((select UnifiedSocialCreditNo from LCGrpAppnt where GrpContNo = lyp.policyno union select UnifiedSocialCreditNo from LBGrpAppnt where GrpContNo = lyp.policyno),(select TaxNo from LCGrpAppnt where GrpContNo = lyp.policyno union select TaxNo from LBGrpAppnt where GrpContNo = lyp.policyno)),(select OrgancomCode from LCGrpAppnt where GrpContNo = lyp.policyno union select OrgancomCode from LBGrpAppnt where GrpContNo = lyp.policyno)),"//纳税识别号
				+ " (select Phone from LCGrpAppnt where GrpContNo = lyp.policyno union select Phone from LBGrpAppnt where GrpContNo = lyp.policyno),"//联系电话
				+ " (select PostalAddress from LCGrpAppnt where GrpContNo = lyp.policyno union select PostalAddress from LBGrpAppnt where GrpContNo = lyp.policyno),"//客户地址
				+ " (select CustomerBankCode from LCGrpAppnt where GrpContNo = lyp.policyno union select CustomerBankCode from LBGrpAppnt where GrpContNo = lyp.policyno),"//开户银行
				+ " (select CustomerBankAccNo from LCGrpAppnt where GrpContNo = lyp.policyno union select CustomerBankAccNo from LBGrpAppnt where GrpContNo = lyp.policyno),"//银行账户
				+ " (select Mobile1 from LCGrpAddress where CustomerNo = (select CustomerNo from LCGrpAppnt where GrpContNo = lyp.policyno union select CustomerNo from LBGrpAppnt where GrpContNo = lyp.policyno) and AddressNo = (select AddressNo from LCGrpAppnt where GrpContNo = lyp.policyno union select AddressNo from LBGrpAppnt where GrpContNo = lyp.policyno)),"//移动电话
				+ " (select E_Mail1 from LCGrpAddress where CustomerNo = (select CustomerNo from LCGrpAppnt where GrpContNo = lyp.policyno union select CustomerNo from LBGrpAppnt where GrpContNo = lyp.policyno) and AddressNo = (select AddressNo from LCGrpAppnt where GrpContNo = lyp.policyno union select AddressNo from LBGrpAppnt where GrpContNo = lyp.policyno))"//电子邮箱
				+ " from LYPremSeparateDetail lyp,ljaget lja "
				+ " where lyp.moneyno=lja.actugetno "
				+ " and lyp.moneytype='02' "//付费
				+ " and lja.othernotype='3' "//团单保全
				+ " and lyp.state='02' "
//				+ " and lja.confdate is not null "//实付后   	//财务接口有 开票-付费的场景
//				+ " and lyp.moneyno is not null "
				+ " and not exists (select 1 from LYOutPayDetail where busino=lyp.busino and state<>'03') "
				+ " and exists (select 1 from lcgrpcont where grpcontno=lyp.policyno union select 1 from lbgrpcont where grpcontno=lyp.policyno) "
				+ tWhereSQL	
				+ mWherePart
				+ " with ur";
			
			tSSRS = new ExeSQL().execSQL(tSQL);
			if (tSSRS != null && tSSRS.getMaxRow() > 0) {
				for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
					LYOutPayDetailSchema tLYOutPayDetailSchema = new LYOutPayDetailSchema();
					tLYOutPayDetailSchema.setBusiNo(tSSRS.GetText(i, 1));
					tLYOutPayDetailSchema.settranserial(tSSRS.GetText(i, 1));//交易流水号
					tLYOutPayDetailSchema.setbusipk(tSSRS.GetText(i, 1));
					tLYOutPayDetailSchema.setisupdate("Y");//是否更新客户信息
					tLYOutPayDetailSchema.setgroup("00");//所属集团:固定值
					tLYOutPayDetailSchema.setpkgroup("00");//所属集团:固定值
					tLYOutPayDetailSchema.setpkorg(tSSRS.GetText(i, 2));//所属组织
					tLYOutPayDetailSchema.setorg(tSSRS.GetText(i, 2));//所属组织
					tLYOutPayDetailSchema.setorgv(tSSRS.GetText(i, 2));//所属组织
					tLYOutPayDetailSchema.setdeptdoc(tSSRS.GetText(i, 2));
					tLYOutPayDetailSchema.setcode(tSSRS.GetText(i, 3));//客户编码
					tLYOutPayDetailSchema.setname(tSSRS.GetText(i, 4));//客户名称
	//				tLYOutPayDetailSchema.setcustomertype(tSSRS.GetText(i, 5));//客户类型
					tLYOutPayDetailSchema.setcustomertype("1");//modify 20160604 管理平台提供的默认值
					tLYOutPayDetailSchema.setcustcode(tSSRS.GetText(i, 3));
					tLYOutPayDetailSchema.setcustname(tSSRS.GetText(i, 4));
	//				tLYOutPayDetailSchema.setcusttype(tSSRS.GetText(i, 5));
					tLYOutPayDetailSchema.setcusttype("1");
					
					tLYOutPayDetailSchema.setptsitem(tSSRS.GetText(i, 6));
					tLYOutPayDetailSchema.settaxrate(tSSRS.GetText(i, 7));//税率
					tLYOutPayDetailSchema.setoriamt(tSSRS.GetText(i, 8));//不含税金额
					tLYOutPayDetailSchema.setoritax(tSSRS.GetText(i, 9));//税额
					tLYOutPayDetailSchema.setlocalamt(tSSRS.GetText(i, 8));//不含税金额（本币）
					tLYOutPayDetailSchema.setlocaltax(tSSRS.GetText(i, 9));//税额（本币）
					tLYOutPayDetailSchema.setdectype(tSSRS.GetText(i, 10));//申报类型
					tLYOutPayDetailSchema.setsrcsystem(tSSRS.GetText(i, 11));
					tLYOutPayDetailSchema.settrandate(tSSRS.GetText(i, 12));//交易日期
					String riskcode = tSSRS.GetText(i, 13);
					String moneyType = tSSRS.GetText(i, 15);
					if ("09".equals(moneyType)) {//工本费riskcode置0000
						riskcode = BQ.FILLDATA;
					} else {
						riskcode = riskcode.substring(0, 4);
					}
					tLYOutPayDetailSchema.setprocode(riskcode);
					tLYOutPayDetailSchema.setbusitype(moneyType);//同价税分离接口的收支类型
					tLYOutPayDetailSchema.settrancurrency(tSSRS.GetText(i, 16));// 币种：人民币
					tLYOutPayDetailSchema.settranamt(tSSRS.GetText(i, 17));//交易金额（含税）
					tLYOutPayDetailSchema.settaxtype(tSSRS.GetText(i, 18));//金额是否含税
					String paymentflag="0";//“0”表示收入，“1”表示支出
					if("02".equals(tSSRS.GetText(i, 19))){//价税分离表01表示收费 02表示付费
						paymentflag="1";
					}
					tLYOutPayDetailSchema.setpaymentflag(paymentflag);
					tLYOutPayDetailSchema.setoverseasflag(tSSRS.GetText(i, 20));
					tLYOutPayDetailSchema.setisbill(tSSRS.GetText(i, 21));
					tLYOutPayDetailSchema.setareatype(tSSRS.GetText(i, 22));
					tLYOutPayDetailSchema.setinsureno(tSSRS.GetText(i, 23));//印刷号
					tLYOutPayDetailSchema.setbilleffectivedate(tSSRS.GetText(i, 24));
					tLYOutPayDetailSchema.setbqbatchno(tSSRS.GetText(i, 25));
					tLYOutPayDetailSchema.setvdata(mCurrentDate);
					tLYOutPayDetailSchema.setvdef2(tSSRS.GetText(i, 14));
					tLYOutPayDetailSchema.setmoneyno(tSSRS.GetText(i, 26));
					tLYOutPayDetailSchema.setmoneytype(tSSRS.GetText(i, 19));
					//备注
					String vdef1="保单号:"+tSSRS.GetText(i, 14)+";险种："+tSSRS.GetText(i, 27);
					tLYOutPayDetailSchema.setvdef1(vdef1);
					tLYOutPayDetailSchema.setvoucherid(tSSRS.GetText(i, 14));//保单号
					tLYOutPayDetailSchema.setprebilltype("02");//非预打
					tLYOutPayDetailSchema.setinvtype("0");//默认0
					tLYOutPayDetailSchema.setvaddress("0");
					tLYOutPayDetailSchema.setvsrcsystem("01");
					tLYOutPayDetailSchema.settaxpayertype("01");//管理平台提供的默认值
					tLYOutPayDetailSchema.setenablestate("2");//默认值
					
					//2018-5-28 针对上海分公司增加
					if("8631".equals(tSSRS.GetText(i, 2).substring(0, 4))){
						//团单新增8个字段
						if(tSSRS.GetText(i, 28)==null||"".equals(tSSRS.GetText(i, 28))){
							tLYOutPayDetailSchema.settaxpayertype("01");
						}else{
							tLYOutPayDetailSchema.settaxpayertype("0"+tSSRS.GetText(i, 28));
						}
						
						tLYOutPayDetailSchema.settaxnumber(tSSRS.GetText(i, 29));
						tLYOutPayDetailSchema.settelephone(tSSRS.GetText(i, 30));
						tLYOutPayDetailSchema.setaddressee(tSSRS.GetText(i, 31));
						tLYOutPayDetailSchema.setbank(tSSRS.GetText(i, 32));
						tLYOutPayDetailSchema.setbankaccount(tSSRS.GetText(i, 33));
						tLYOutPayDetailSchema.setmobile(tSSRS.GetText(i, 34));
						tLYOutPayDetailSchema.setemail(tSSRS.GetText(i, 35));
					}
					
					/**
					 * 关于电子发票推送模式及规则调整，现增加发票类型，团单——1纸质发票，个单——2电子发票
					 * 大连分公司统一推电子发票
					 */
					if("8691".equals(tSSRS.GetText(i, 2).substring(0, 4))){
						tLYOutPayDetailSchema.setbilltype("2");
					}else{
						tLYOutPayDetailSchema.setbilltype("1");
					}
					//2018-2-6新增电子发票客户邮箱和客户手机号
					tLYOutPayDetailSchema.setbuymailbox(tSSRS.GetText(i, 35));
					tLYOutPayDetailSchema.setbuyphoneno(tSSRS.GetText(i, 34));
					
					//2018-3-13新增证件类型和证件号码
					tLYOutPayDetailSchema.setbuyidtype("");
					tLYOutPayDetailSchema.setbuyidnumber("");
					
					tLYOutPayDetailSchema.setoperator(mGI.Operator);
					tLYOutPayDetailSchema.setMakeDate(mCurrentDate);
					tLYOutPayDetailSchema.setMakeTime(mCurrentTime);
					tLYOutPayDetailSchema.setModifyDate(mCurrentDate);
					tLYOutPayDetailSchema.setModifyTime(mCurrentTime);
					
					tLYOutPayDetailSet.add(tLYOutPayDetailSchema);
				}
			}
		//批处理只推送上海分公司的个人业务发票		2018-5-28 增加批处理个单对深圳分公司的推送
		if(null != mHandFlag && !"".equals(mHandFlag)){
			System.out.println("页面推送");
		}else{
			tWhereSQL += " and (lyp.managecom like '8631%' or lyp.managecom like '8695%') ";
		}
			//个单保全收费数据
			String tSQL2 = " select "
						+ " lyp.busino,"//流水号
						+ " (select managecom from lccont where contno=lyp.policyno union select managecom from lbcont where contno=lyp.policyno) , "//所属组织
						+ " (select appntno from lcappnt where contno=lyp.policyno union select appntno from lbappnt where contno=lyp.policyno) , "//客户号
						+ " (select appntname from lcappnt where contno=lyp.policyno union select appntname from lbappnt where contno=lyp.policyno) , "
						+ " '2', "//客户类型:“1”代表机构，“2”代表个人
						+ " '01', "//价税分离项目:固定值
						+ " lyp.taxrate,"
						+ " lyp.moneyNoTax,"
						+ " lyp.moneyTax, "
						+ " '0', "//申报类型：0正常申报，固定值
						+ " '01', "//来源系统:固定值
						+ " lja.confdate,"//交易日期
						+ " lyp.riskcode,"//产品
						+ " lyp.policyno,"
						+ " lyp.busitype,"//业务类型
						+ " 'CNY',"//交易币种
						+ " lyp.paymoney,"//交易金额
						+ " '0', "//金额是否含税:“0”表示含税，“1”表示不含税
						+ " lyp.moneytype,"//收付标志
						+ " '0',"//境内外标示:“0”表示境内，“1”表示境外
						+ " 'Y',"//是否开票
						+ " '0',"//汇总地类型:“0”表示汇总，“1”表示属地，后续纳税申报需要
						+ " lyp.prtno, "//投保单号
						+ " (select signdate from lccont where contno=lyp.policyno union select signdate from lbcont where contno=lyp.policyno) ,"//签单号
						+ " lja.incomeno, "//批单号
						+ " lyp.moneyno moneyno, "
						+ " (select riskname from lmriskapp where riskcode=lyp.riskcode),"
						+ " (select Phone from LCAddress where CustomerNo = (select AppntNo from LCAppnt where ContNo = lyp.policyno union select AppntNo from LBAppnt where ContNo = lyp.policyno) and AddressNo = (select AddressNo from LCAppnt where ContNo = lyp.policyno union select AddressNo from LBAppnt where ContNo = lyp.policyno)),"//联系电话
						+ " (select Mobile from LCAddress where CustomerNo = (select AppntNo from LCAppnt where ContNo = lyp.policyno union select AppntNo from LBAppnt where ContNo = lyp.policyno) and AddressNo = (select AddressNo from LCAppnt where ContNo = lyp.policyno union select AddressNo from LBAppnt where ContNo = lyp.policyno)),"//移动电话
						+ " (select EMail from LCAddress where CustomerNo = (select AppntNo from LCAppnt where ContNo = lyp.policyno union select AppntNo from LBAppnt where ContNo = lyp.policyno) and AddressNo = (select AddressNo from LCAppnt where ContNo = lyp.policyno union select AddressNo from LBAppnt where ContNo = lyp.policyno)),"//电子邮箱
						+ " (select AppntIDType from lccont where contno=lyp.policyno union select AppntIDType from lbcont where contno=lyp.policyno),"//证件类型
						+ " (select AppntIDNo from lccont where contno=lyp.policyno union select AppntIDNo from lbcont where contno=lyp.policyno)"//证件号码
						+ " from LYPremSeparateDetail lyp,ljapay lja "
						+ " where 1=1"
//						+ " and lyp.tempfeeno=lja.getnoticeno "不准
						+ " and lyp.otherno=lja.incomeno "
//						+ " and lyp.moneyno=lja.payno "
						+ " and lja.confdate is not null "
						+ " and lyp.tempfeetype ='4' "//保全收费
						+ " and lyp.feefinatype not in ('YEI','YEO') "
						+ " and lyp.state='02' "//已经价税分离
//						+ " and lyp.moneyno is not null "
						+ " and not exists (select 1 from LYOutPayDetail where busino=lyp.busino and state<>'03') "
						+ " and exists (select 1 from lccont where contno=lyp.policyno union select 1 from lbcont where contno=lyp.policyno) "
						+ tWhereSQL
						+ mWherePart
						+ " union "
						+ " select "//20161102 a、保全收费完全从投保人账户抵扣   b、保全退费转入投保人账户
						+ " lyp.busino,"//流水号
						+ " (select managecom from lccont where contno=lyp.policyno union select managecom from lbcont where contno=lyp.policyno) , "//所属组织
						+ " (select appntno from lcappnt where contno=lyp.policyno union select appntno from lbappnt where contno=lyp.policyno) , "//客户号
						+ " (select appntname from lcappnt where contno=lyp.policyno union select appntname from lbappnt where contno=lyp.policyno) , "
						+ " '2', "//客户类型:“1”代表机构，“2”代表个人
						+ " '01', "//价税分离项目:固定值
						+ " lyp.taxrate,"
						+ " lyp.moneyNoTax,"
						+ " lyp.moneyTax, "
						+ " '0', "//申报类型：0正常申报，固定值
						+ " '01', "//来源系统:固定值
						+ " lja.confdate,"//交易日期
						+ " lyp.riskcode,"//产品
						+ " lyp.policyno,"
						+ " lyp.busitype,"//业务类型
						+ " 'CNY',"//交易币种
						+ " lyp.paymoney,"//交易金额
						+ " '0', "//金额是否含税:“0”表示含税，“1”表示不含税
						+ " lyp.moneytype,"//收付标志
						+ " '0',"//境内外标示:“0”表示境内，“1”表示境外
						+ " 'Y',"//是否开票
						+ " '0',"//汇总地类型:“0”表示汇总，“1”表示属地，后续纳税申报需要
						+ " lyp.prtno, "//投保单号
						+ " (select signdate from lccont where contno=lyp.policyno union select signdate from lbcont where contno=lyp.policyno) ,"//签单号
						+ " lja.incomeno, "//批单号
						+ " lyp.moneyno moneyno, "
						+ " (select riskname from lmriskapp where riskcode=lyp.riskcode),"
						+ " (select Phone from LCAddress where CustomerNo = (select AppntNo from LCAppnt where ContNo = lyp.policyno union select AppntNo from LBAppnt where ContNo = lyp.policyno) and AddressNo = (select AddressNo from LCAppnt where ContNo = lyp.policyno union select AddressNo from LBAppnt where ContNo = lyp.policyno)),"//联系电话
						+ " (select Mobile from LCAddress where CustomerNo = (select AppntNo from LCAppnt where ContNo = lyp.policyno union select AppntNo from LBAppnt where ContNo = lyp.policyno) and AddressNo = (select AddressNo from LCAppnt where ContNo = lyp.policyno union select AddressNo from LBAppnt where ContNo = lyp.policyno)),"//移动电话
						+ " (select EMail from LCAddress where CustomerNo = (select AppntNo from LCAppnt where ContNo = lyp.policyno union select AppntNo from LBAppnt where ContNo = lyp.policyno) and AddressNo = (select AddressNo from LCAppnt where ContNo = lyp.policyno union select AddressNo from LBAppnt where ContNo = lyp.policyno)),"//电子邮箱
						+ " (select AppntIDType from lccont where contno=lyp.policyno union select AppntIDType from lbcont where contno=lyp.policyno),"//证件类型
						+ " (select AppntIDNo from lccont where contno=lyp.policyno union select AppntIDNo from lbcont where contno=lyp.policyno)"//证件号码
						+ " from LYPremSeparateDetail lyp,ljapay lja "
						+ " where 1=1"
						+ " and lyp.otherno=lja.incomeno "
//						+ " and lyp.tempfeeno=lja.getnoticeno"
						+ " and lyp.moneyno=lja.payno "
						+ " and lja.incometype ='10' "
						+ " and lja.confdate is not null "
						+ " and lyp.tempfeetype is null "//转入或从投保人账户抵扣的没有暂收
						+ " and lyp.feefinatype not in ('YEI','YEO') "
						+ " and lyp.state='02' "//已经价税分离
//						+ " and lyp.moneyno is not null "
						+ " and not exists (select 1 from LYOutPayDetail where busino=lyp.busino and state<>'03') "
						+ " and exists (select 1 from lccont where contno=lyp.policyno union select 1 from lbcont where contno=lyp.policyno) "
						+ tWhereSQL
						+ mWherePart
						+ " with ur";
			
			SSRS tSSRS2 = new ExeSQL().execSQL(tSQL2);
			if (tSSRS2 != null && tSSRS2.getMaxRow() > 0) {
				for (int i = 1; i <= tSSRS2.getMaxRow(); i++) {
					LYOutPayDetailSchema tLYOutPayDetailSchema = new LYOutPayDetailSchema();
					tLYOutPayDetailSchema.setBusiNo(tSSRS2.GetText(i, 1));
					tLYOutPayDetailSchema.settranserial(tSSRS2.GetText(i, 1));//交易流水号
					tLYOutPayDetailSchema.setbusipk(tSSRS2.GetText(i, 1));
					tLYOutPayDetailSchema.setisupdate("Y");//是否更新客户信息
					tLYOutPayDetailSchema.setgroup("00");//所属集团:固定值
					tLYOutPayDetailSchema.setpkgroup("00");//所属集团:固定值
					tLYOutPayDetailSchema.setpkorg(tSSRS2.GetText(i, 2));//所属组织
					tLYOutPayDetailSchema.setorg(tSSRS2.GetText(i, 2));//所属组织
					tLYOutPayDetailSchema.setorgv(tSSRS2.GetText(i, 2));//所属组织
					tLYOutPayDetailSchema.setdeptdoc(tSSRS2.GetText(i, 2));
					tLYOutPayDetailSchema.setcode(tSSRS2.GetText(i, 3));//客户编码
					tLYOutPayDetailSchema.setname(tSSRS2.GetText(i, 4));//客户名称
	//				tLYOutPayDetailSchema.setcustomertype(tSSRS2.GetText(i, 5));//客户类型
					tLYOutPayDetailSchema.setcustomertype("2");//modify 20160604 管理平台提供的默认值
					tLYOutPayDetailSchema.setcustcode(tSSRS2.GetText(i, 3));
					tLYOutPayDetailSchema.setcustname(tSSRS2.GetText(i, 4));
	//				tLYOutPayDetailSchema.setcusttype(tSSRS2.GetText(i, 5));
					tLYOutPayDetailSchema.setcusttype("2");
					
					tLYOutPayDetailSchema.setptsitem(tSSRS2.GetText(i, 6));
					tLYOutPayDetailSchema.settaxrate(tSSRS2.GetText(i, 7));//税率
					tLYOutPayDetailSchema.setoriamt(tSSRS2.GetText(i, 8));//不含税金额
					tLYOutPayDetailSchema.setoritax(tSSRS2.GetText(i, 9));//税额
					tLYOutPayDetailSchema.setlocalamt(tSSRS2.GetText(i, 8));//不含税金额（本币）
					tLYOutPayDetailSchema.setlocaltax(tSSRS2.GetText(i, 9));//税额（本币）
					tLYOutPayDetailSchema.setdectype(tSSRS2.GetText(i, 10));//申报类型
					tLYOutPayDetailSchema.setsrcsystem(tSSRS2.GetText(i, 11));
					tLYOutPayDetailSchema.settrandate(tSSRS2.GetText(i, 12));//交易日期
					String riskcode = tSSRS2.GetText(i, 13);
					String moneyType = tSSRS2.GetText(i, 15);
					if ("09".equals(moneyType)) {//工本费riskcode置0000
						riskcode = BQ.FILLDATA;
					} else {
						riskcode = riskcode.substring(0, 4);
					}
					tLYOutPayDetailSchema.setprocode(riskcode);
					tLYOutPayDetailSchema.setbusitype(moneyType);//同价税分离接口的收支类型
					tLYOutPayDetailSchema.settrancurrency(tSSRS2.GetText(i, 16));// 币种：人民币
					tLYOutPayDetailSchema.settranamt(tSSRS2.GetText(i, 17));//交易金额（含税）
					tLYOutPayDetailSchema.settaxtype(tSSRS2.GetText(i, 18));//金额是否含税
					String paymentflag="0";//“0”表示收入，“1”表示支出
					if("02".equals(tSSRS2.GetText(i, 19))){//价税分离表01表示收费 02表示付费
						paymentflag="1";
					}
					tLYOutPayDetailSchema.setpaymentflag(paymentflag);
					tLYOutPayDetailSchema.setoverseasflag(tSSRS2.GetText(i, 20));
					tLYOutPayDetailSchema.setisbill(tSSRS2.GetText(i, 21));
					tLYOutPayDetailSchema.setareatype(tSSRS2.GetText(i, 22));
					tLYOutPayDetailSchema.setinsureno(tSSRS2.GetText(i, 23));//印刷号
					tLYOutPayDetailSchema.setbilleffectivedate(tSSRS2.GetText(i, 24));
					tLYOutPayDetailSchema.setbqbatchno(tSSRS2.GetText(i, 25));
					tLYOutPayDetailSchema.setvdata(mCurrentDate);
					tLYOutPayDetailSchema.setvdef2(tSSRS2.GetText(i, 14));
					tLYOutPayDetailSchema.setmoneyno(tSSRS2.GetText(i, 26));
					tLYOutPayDetailSchema.setmoneytype(tSSRS2.GetText(i, 19));
					//备注
					String vdef1="保单号:"+tSSRS2.GetText(i, 14)+";险种："+tSSRS2.GetText(i, 27);
					tLYOutPayDetailSchema.setvdef1(vdef1);
					tLYOutPayDetailSchema.setvoucherid(tSSRS2.GetText(i, 14));//保单号
					tLYOutPayDetailSchema.setprebilltype("02");//非预打
					tLYOutPayDetailSchema.setinvtype("0");//默认0
					tLYOutPayDetailSchema.setvaddress("0");
					tLYOutPayDetailSchema.setvsrcsystem("01");
					tLYOutPayDetailSchema.settaxpayertype("01");//管理平台提供的默认值
					tLYOutPayDetailSchema.setenablestate("2");//默认值
					
					//2018-5-28 针对上海分公司增加
					if("8631".equals(tSSRS2.GetText(i, 2).substring(0, 4))){
						//个单新增3个字段
						tLYOutPayDetailSchema.settelephone(tSSRS2.GetText(i, 28));
						tLYOutPayDetailSchema.setmobile(tSSRS2.GetText(i, 29));
						tLYOutPayDetailSchema.setemail(tSSRS2.GetText(i, 30));
					}
					
					/**
					 * 关于电子发票推送模式及规则调整，现增加发票类型，团单——'1'纸质发票，个单——'2'电子发票
					 * 大连分公司统一推电子发票
					 */
					tLYOutPayDetailSchema.setbilltype("2");
					//2018-2-6新增电子发票客户邮箱和客户手机号
					tLYOutPayDetailSchema.setbuymailbox(tSSRS2.GetText(i, 30));
					tLYOutPayDetailSchema.setbuyphoneno(tSSRS2.GetText(i, 29));
					
					//2018-3-13新增证件类型和证件号码
					if("0".equals(tSSRS2.GetText(i, 31))){//身份证
						tLYOutPayDetailSchema.setbuyidtype("01");
					}else if("1".equals(tSSRS2.GetText(i, 31))){//护照
						tLYOutPayDetailSchema.setbuyidtype("02");
					}else if("2".equals(tSSRS2.GetText(i, 31))){//军官证
						tLYOutPayDetailSchema.setbuyidtype("03");
					}else if("3".equals(tSSRS2.GetText(i, 31))||"4".equals(tSSRS2.GetText(i, 31))||"5".equals(tSSRS2.GetText(i, 31))||"6".equals(tSSRS2.GetText(i, 31))||"7".equals(tSSRS2.GetText(i, 31))||"8".equals(tSSRS2.GetText(i, 31))){//军官证
						tLYOutPayDetailSchema.setbuyidtype("07");
					}else{
						tLYOutPayDetailSchema.setbuyidtype("");
					}

					tLYOutPayDetailSchema.setbuyidnumber(tSSRS2.GetText(i, 32));
					
					
					tLYOutPayDetailSchema.setoperator(mGI.Operator);
					tLYOutPayDetailSchema.setMakeDate(mCurrentDate);
					tLYOutPayDetailSchema.setMakeTime(mCurrentTime);
					tLYOutPayDetailSchema.setModifyDate(mCurrentDate);
					tLYOutPayDetailSchema.setModifyTime(mCurrentTime);
					
					tLYOutPayDetailSet.add(tLYOutPayDetailSchema);
				}
			}
			
			//个单保全付费数据
			tSQL = " select "
				+ " lyp.busino,"//流水号
				+ " (select managecom from lccont where contno=lyp.policyno union select managecom from lbcont where contno=lyp.policyno) , "//所属组织
				+ " (select appntno from lccont where contno=lyp.policyno union select appntno from lbcont where contno=lyp.policyno) , "//客户号
				+ " (select appntname from lccont where contno=lyp.policyno union select appntname from lbcont where contno=lyp.policyno) , "
				+ " '2', "//客户类型:“1”代表机构，“2”代表个人
				+ " '01', "//价税分离项目:固定值
				+ " lyp.taxrate,"
				+ " lyp.moneyNoTax,"
				+ " lyp.moneyTax, "
				+ " '0', "//申报类型：0正常申报，固定值
				+ " '01', "//来源系统:固定值
				+ " lja.confdate,"//交易日期
				+ " lyp.riskcode,"//产品
				+ " lyp.policyno,"
				+ " lyp.busitype,"//业务类型
				+ " 'CNY',"//交易币种
				+ " lyp.paymoney,"//交易金额
				+ " '0', "//金额是否含税:“0”表示含税，“1”表示不含税
				+ " lyp.moneytype,"//收付标志
				+ " '0',"//境内外标示:“0”表示境内，“1”表示境外
				+ " 'Y',"//是否开票
				+ " '0',"//汇总地类型:“0”表示汇总，“1”表示属地，后续纳税申报需要
				+ " lyp.prtno, "//投保单号
				+ " (select signdate from lccont where contno=lyp.policyno union select signdate from lbcont where contno=lyp.policyno) ,"//签单号
				+ " lja.otherno, "//批单号
				+ " lyp.moneyno moneyno, "
				+ " (select riskname from lmriskapp where riskcode=lyp.riskcode),"
				+ " (select Phone from LCAddress where CustomerNo = (select AppntNo from LCAppnt where ContNo = lyp.policyno union select AppntNo from LBAppnt where ContNo = lyp.policyno) and AddressNo = (select AddressNo from LCAppnt where ContNo = lyp.policyno union select AddressNo from LBAppnt where ContNo = lyp.policyno)),"//联系电话
				+ " (select Mobile from LCAddress where CustomerNo = (select AppntNo from LCAppnt where ContNo = lyp.policyno union select AppntNo from LBAppnt where ContNo = lyp.policyno) and AddressNo = (select AddressNo from LCAppnt where ContNo = lyp.policyno union select AddressNo from LBAppnt where ContNo = lyp.policyno)),"//移动电话
				+ " (select EMail from LCAddress where CustomerNo = (select AppntNo from LCAppnt where ContNo = lyp.policyno union select AppntNo from LBAppnt where ContNo = lyp.policyno) and AddressNo = (select AddressNo from LCAppnt where ContNo = lyp.policyno union select AddressNo from LBAppnt where ContNo = lyp.policyno)),"//电子邮箱
				+ " (select AppntIDType from lccont where contno=lyp.policyno union select AppntIDType from lbcont where contno=lyp.policyno),"//证件类型
				+ " (select AppntIDNo from lccont where contno=lyp.policyno union select AppntIDNo from lbcont where contno=lyp.policyno)"//证件号码
				+ " from LYPremSeparateDetail lyp,ljaget lja "
				+ " where lyp.moneyno=lja.actugetno "
				+ " and lyp.moneytype='02' "//付费
				+ " and lja.othernotype='10' "//个单保全
				+ " and lyp.state='02' "
//				+ " and lja.confdate is not null "//实付后   	//财务接口有 开票-付费的场景
//				+ " and lyp.moneyno is not null "
				+ " and not exists (select 1 from LYOutPayDetail where busino=lyp.busino and state<>'03') "
				+ " and exists (select 1 from lccont where contno=lyp.policyno union select 1 from lbcont where contno=lyp.policyno) "
				+ tWhereSQL
				+ mWherePart
				+ " with ur";
			
			tSSRS = new ExeSQL().execSQL(tSQL);
			if (tSSRS != null && tSSRS.getMaxRow() > 0) {
				for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
					LYOutPayDetailSchema tLYOutPayDetailSchema = new LYOutPayDetailSchema();
					tLYOutPayDetailSchema.setBusiNo(tSSRS.GetText(i, 1));
					tLYOutPayDetailSchema.settranserial(tSSRS.GetText(i, 1));//交易流水号
					tLYOutPayDetailSchema.setbusipk(tSSRS.GetText(i, 1));
					tLYOutPayDetailSchema.setisupdate("Y");//是否更新客户信息
					tLYOutPayDetailSchema.setgroup("00");//所属集团:固定值
					tLYOutPayDetailSchema.setpkgroup("00");//所属集团:固定值
					tLYOutPayDetailSchema.setpkorg(tSSRS.GetText(i, 2));//所属组织
					tLYOutPayDetailSchema.setorg(tSSRS.GetText(i, 2));//所属组织
					tLYOutPayDetailSchema.setorgv(tSSRS.GetText(i, 2));//所属组织
					tLYOutPayDetailSchema.setdeptdoc(tSSRS.GetText(i, 2));
					tLYOutPayDetailSchema.setcode(tSSRS.GetText(i, 3));//客户编码
					tLYOutPayDetailSchema.setname(tSSRS.GetText(i, 4));//客户名称
//					tLYOutPayDetailSchema.setcustomertype(tSSRS.GetText(i, 5));//客户类型
					tLYOutPayDetailSchema.setcustomertype("2");//modify 20160604 管理平台提供的默认值
					tLYOutPayDetailSchema.setcustcode(tSSRS.GetText(i, 3));
					tLYOutPayDetailSchema.setcustname(tSSRS.GetText(i, 4));
//					tLYOutPayDetailSchema.setcusttype(tSSRS.GetText(i, 5));
					tLYOutPayDetailSchema.setcusttype("2");
					
					tLYOutPayDetailSchema.setptsitem(tSSRS.GetText(i, 6));
					tLYOutPayDetailSchema.settaxrate(tSSRS.GetText(i, 7));//税率
					tLYOutPayDetailSchema.setoriamt(tSSRS.GetText(i, 8));//不含税金额
					tLYOutPayDetailSchema.setoritax(tSSRS.GetText(i, 9));//税额
					tLYOutPayDetailSchema.setlocalamt(tSSRS.GetText(i, 8));//不含税金额（本币）
					tLYOutPayDetailSchema.setlocaltax(tSSRS.GetText(i, 9));//税额（本币）
					tLYOutPayDetailSchema.setdectype(tSSRS.GetText(i, 10));//申报类型
					tLYOutPayDetailSchema.setsrcsystem(tSSRS.GetText(i, 11));
					tLYOutPayDetailSchema.settrandate(tSSRS.GetText(i, 12));//交易日期
					String riskcode = tSSRS.GetText(i, 13);
					String moneyType = tSSRS.GetText(i, 15);
					if ("09".equals(moneyType)) {//工本费riskcode置0000
						riskcode = BQ.FILLDATA;
					} else {
						riskcode = riskcode.substring(0, 4);
					}
					tLYOutPayDetailSchema.setprocode(riskcode);
					tLYOutPayDetailSchema.setbusitype(moneyType);//同价税分离接口的收支类型
					tLYOutPayDetailSchema.settrancurrency(tSSRS.GetText(i, 16));// 币种：人民币
					tLYOutPayDetailSchema.settranamt(tSSRS.GetText(i, 17));//交易金额（含税）
					tLYOutPayDetailSchema.settaxtype(tSSRS.GetText(i, 18));//金额是否含税
					String paymentflag="0";//“0”表示收入，“1”表示支出
					if("02".equals(tSSRS.GetText(i, 19))){//价税分离表01表示收费 02表示付费
						paymentflag="1";
					}
					tLYOutPayDetailSchema.setpaymentflag(paymentflag);
					tLYOutPayDetailSchema.setoverseasflag(tSSRS.GetText(i, 20));
					tLYOutPayDetailSchema.setisbill(tSSRS.GetText(i, 21));
					tLYOutPayDetailSchema.setareatype(tSSRS.GetText(i, 22));
					tLYOutPayDetailSchema.setinsureno(tSSRS.GetText(i, 23));//印刷号
					tLYOutPayDetailSchema.setbilleffectivedate(tSSRS.GetText(i, 24));
					tLYOutPayDetailSchema.setbqbatchno(tSSRS.GetText(i, 25));
					tLYOutPayDetailSchema.setvdata(mCurrentDate);
					tLYOutPayDetailSchema.setvdef2(tSSRS.GetText(i, 14));
					tLYOutPayDetailSchema.setmoneyno(tSSRS.GetText(i, 26));
					tLYOutPayDetailSchema.setmoneytype(tSSRS.GetText(i, 19));
					//备注
					String vdef1="保单号:"+tSSRS.GetText(i, 14)+";险种："+tSSRS.GetText(i, 27);
					tLYOutPayDetailSchema.setvdef1(vdef1);
					tLYOutPayDetailSchema.setvoucherid(tSSRS.GetText(i, 14));//保单号
					tLYOutPayDetailSchema.setprebilltype("02");//非预打
					tLYOutPayDetailSchema.setinvtype("0");//默认0
					tLYOutPayDetailSchema.setvaddress("0");
					tLYOutPayDetailSchema.setvsrcsystem("01");
					tLYOutPayDetailSchema.settaxpayertype("01");//管理平台提供的默认值
					tLYOutPayDetailSchema.setenablestate("2");//默认值
					
					//2018-5-28 针对上海分公司增加
					if("8631".equals(tSSRS.GetText(i, 2).substring(0, 4))){
						//个单新增3个字段
						tLYOutPayDetailSchema.settelephone(tSSRS.GetText(i, 28));
						tLYOutPayDetailSchema.setmobile(tSSRS.GetText(i, 29));
						tLYOutPayDetailSchema.setemail(tSSRS.GetText(i, 30));
					}
					
					/**
					 * 关于电子发票推送模式及规则调整，现增加发票类型，团单——'1'纸质发票，个单——'2'电子发票
					 * 大连分公司统一推电子发票
					 */
					tLYOutPayDetailSchema.setbilltype("2");
					//2018-2-6新增电子发票客户邮箱和客户手机号
					tLYOutPayDetailSchema.setbuymailbox(tSSRS.GetText(i, 30));
					tLYOutPayDetailSchema.setbuyphoneno(tSSRS.GetText(i, 29));
					
					//2018-3-13新增证件类型和证件号码
					if("0".equals(tSSRS.GetText(i, 31))){//身份证
						tLYOutPayDetailSchema.setbuyidtype("01");
					}else if("1".equals(tSSRS.GetText(i, 31))){//护照
						tLYOutPayDetailSchema.setbuyidtype("02");
					}else if("2".equals(tSSRS.GetText(i, 31))){//军官证
						tLYOutPayDetailSchema.setbuyidtype("03");
					}else if("3".equals(tSSRS.GetText(i, 31))||"4".equals(tSSRS.GetText(i, 31))||"5".equals(tSSRS.GetText(i, 31))||"6".equals(tSSRS.GetText(i, 31))||"7".equals(tSSRS.GetText(i, 31))||"8".equals(tSSRS.GetText(i, 31))){//军官证
						tLYOutPayDetailSchema.setbuyidtype("07");
					}else{
						tLYOutPayDetailSchema.setbuyidtype("");
					}
					tLYOutPayDetailSchema.setbuyidnumber(tSSRS.GetText(i, 32));
					
					
					
					tLYOutPayDetailSchema.setoperator(mGI.Operator);
					tLYOutPayDetailSchema.setMakeDate(mCurrentDate);
					tLYOutPayDetailSchema.setMakeTime(mCurrentTime);
					tLYOutPayDetailSchema.setModifyDate(mCurrentDate);
					tLYOutPayDetailSchema.setModifyTime(mCurrentTime);
					
					tLYOutPayDetailSet.add(tLYOutPayDetailSchema);
				}
			}
		//调用发票接口上报数据
		VData tVData = new VData();
		tVData.add(mGI);
		tVData.add(tLYOutPayDetailSet);
		OutPayUploadBL tOutPayUploadBL = new OutPayUploadBL();
		if (!tOutPayUploadBL.getSubmit(tVData, "")) {
			this.mErrors = tOutPayUploadBL.mErrors;
			System.out.println("保全收付费发票数据上报错误:"+ mErrors.getErrContent());
		}else{
			//保存返回结果
			mMap = tOutPayUploadBL.getResult();
		}
		
		return true;
	}

	/**
	 * 准备后台的数据
	 * 
	 * @return boolean
	 */
	private boolean prepareOutputData() {
		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GetBqPremBillBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	public static void main(String[] args) {
		GetBqPremBillBL tGetBqPremBillBL = new GetBqPremBillBL();
		VData cInputData = new VData();
		TransferData tTransferData = new TransferData();
		String tStartDate = "2016-07-20";
		String tEndDate = "2016-09-05";
//		tTransferData.setNameAndValue("StartDate", tStartDate);
//		tTransferData.setNameAndValue("EndDate", tEndDate);
		LYPremSeparateDetailSet set=new LYPremSeparateDetailSet();
		String [] str=new String[]{"20160204000002","20160117000002","20160118000011"};//个单收费记录号
		for(int i=0;i<str.length; i++){
			LYPremSeparateDetailSchema Schema=new LYPremSeparateDetailSchema();
			Schema.setOtherNo(str[i]);
			set.add(Schema);
		}
//		tTransferData.setNameAndValue("HandWorkFlag","Y");
//		cInputData.add(set);
		cInputData.add(tTransferData);
		tGetBqPremBillBL.submitData(cInputData, "");
	}
}
