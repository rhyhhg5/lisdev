package com.sinosoft.lis.ygz;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LYPremSeparateDetailSchema;
import com.sinosoft.lis.vschema.LYPremSeparateDetailSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 万能、特需、委托管理产品初始扣费的价税分离
 * @author abc
 *
 */
public class GetULIInitPremSeparateBL {
	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	/** 数据操作字符串 */
	private String mOperate;
	/** 开始日期*/
	private String mStartDate;
	/** 结束日期*/
	private String mEndDate; 
	//业务处理相关变量
	private MMap mMap = new MMap();
	
	private String mCurrentDate = PubFun.getCurrentDate();
	private String mCurrentTime = PubFun.getCurrentTime();
	
	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		// 进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GetTempFeeBqSeparateBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败GetTempFeeBqSeparateBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}

		// 准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}

		// 保存数据
		PubSubmit tPubSubmit = new PubSubmit();

		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			System.out.print("保存数据返回");
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "LCProjectCancelBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mInputData = null;
		return true;
	}

	// 获得输出数据
	private boolean getInputData(VData cInputData) {
		TransferData tTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
		mStartDate = (String) tTransferData.getValueByName("StartDate");
		mEndDate = (String) tTransferData.getValueByName("EndDate");
		return true;
	}
	
//  业务处理
	private boolean dealData() {
		YGZ mygz = new YGZ();
		LYPremSeparateDetailSet tLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
		String tWhereSQL = "";
		String tDealDate = "2016-05-01";
		if(mStartDate != null && !"".equals(mStartDate)){
			tWhereSQL += " and lja.confdate >= '" + mStartDate + "' ";
		}
		if(mEndDate != null && !"".equals(mEndDate)){
			tWhereSQL += " and lja.confdate <= '" + mEndDate + "' ";
		}
		if("".equals(tWhereSQL)){
			tWhereSQL += " and lja.confdate >= '"+tDealDate+"' and lja.confdate >= current date - 3 days ";
		}
		//个险万能首期、续期初始扣费
		String tSQL = " select "
					+ " trim(lja.payno)||trim(lcin.serialno)||trim(lcin.moneytype) ,"
					+ " lja.getnoticeno ,null tempfeetype,lcin.polno otherno,lcin.othertype othernotype,"
					+ " (select prtno from lccont where conttype='1' and contno = lcin.contno union select prtno from lbcont where conttype='1' and contno = lcin.contno ) prtno,"
					+ " lcin.contno policyno,"
					+ " lcin.riskcode,"
					+ " lja.managecom,"
					+ " lcin.fee,"
					+ " lja.payno,"
					+ " lcin.moneytype"
					+ " from ljapay lja,lcinsureaccfeetrace lcin "
					+ " where 1=1 "
					+ " and lja.incomeno=lcin.contno"
					+ " and lcin.moneytype in ('GL','KF') "
					+ " and lja.DueFeeType in ('0','1', '2') "
					+ " and lcin.fee<>0 "
					+ " and ((lja.DueFeeType = '0' and lcin.othertype='1' and lcin.otherno=lcin.polno) "//契约
					+ " or (lcin.othertype='2' and lja.getnoticeno=lcin.otherno))"//续期
					+ " and exists (select 1 from lccont where contno=lcin.contno and conttype='1' and appflag='1' "
					+ " 			union select 1 from lbcont where contno=lcin.contno and conttype='1') "
					+ " and not exists (select busino from LYPremSeparateDetail where busino=(trim(lja.payno)||trim(lcin.serialno)||trim(lcin.moneytype)) ) "
					+ tWhereSQL
					+ " union "
					+ " select "
					+ " trim(lja.payno)||trim(lcin.serialno)||trim(lcin.moneytype) ,"
					+ " lja.getnoticeno ,null tempfeetype,lcin.polno otherno,lcin.othertype othernotype,"
					+ " (select prtno from lccont where conttype='1' and contno = lcin.contno union select prtno from lbcont where conttype='1' and contno = lcin.contno ) prtno,"
					+ " lcin.contno policyno,"
					+ " lcin.riskcode,"
					+ " lja.managecom,"
					+ " lcin.fee,"
					+ " lja.payno,"
					+ " lcin.moneytype"
					+ " from ljapay lja,lbinsureaccfeetrace lcin "
					+ " where 1=1 "
					+ " and lja.incomeno=lcin.contno"
					+ " and lcin.moneytype in ('GL','KF') "
					+ " and lja.DueFeeType in ('0','1', '2') "
					+ " and lcin.fee<>0 "
					+ " and ((lja.DueFeeType = '0' and lcin.othertype='1' and lcin.otherno=lcin.polno) "//契约
					+ " or (lcin.othertype='2' and lja.getnoticeno=lcin.otherno))"//续期
					+ " and exists (select 1 from lccont where contno=lcin.contno and conttype='1' and appflag='1' "
					+ " 			union select 1 from lbcont where contno=lcin.contno and conttype='1') "
					+ " and not exists (select busino from LYPremSeparateDetail where busino=(trim(lja.payno)||trim(lcin.serialno)||trim(lcin.moneytype)) ) "
					+ tWhereSQL
					+ " with ur";
		SSRS tNoZeroSSRS = new ExeSQL().execSQL(tSQL);
		if(tNoZeroSSRS != null && tNoZeroSSRS.getMaxRow() > 0){
			for (int i = 1; i <= tNoZeroSSRS.getMaxRow(); i++) {
				LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
				tLYPremSeparateDetailSchema.setBusiNo(tNoZeroSSRS.GetText(i, 1));
				tLYPremSeparateDetailSchema.setTempFeeNo(tNoZeroSSRS.GetText(i, 2));
				tLYPremSeparateDetailSchema.setTempFeeType(tNoZeroSSRS.GetText(i, 3));
				tLYPremSeparateDetailSchema.setOtherNo(tNoZeroSSRS.GetText(i, 4));
				tLYPremSeparateDetailSchema.setOtherNoType(tNoZeroSSRS.GetText(i, 5));
				tLYPremSeparateDetailSchema.setPrtNo(tNoZeroSSRS.GetText(i, 6));
				tLYPremSeparateDetailSchema.setPolicyNo(tNoZeroSSRS.GetText(i, 7));
				tLYPremSeparateDetailSchema.setRiskCode(tNoZeroSSRS.GetText(i, 8));
				tLYPremSeparateDetailSchema.setBusiType(YGZ.BUSITYPE_04);//初始扣费
				tLYPremSeparateDetailSchema.setManageCom(tNoZeroSSRS.GetText(i, 9));
				tLYPremSeparateDetailSchema.setPayMoney(tNoZeroSSRS.GetText(i, 10));
				tLYPremSeparateDetailSchema.setMoneyNo(tNoZeroSSRS.GetText(i, 11));
				tLYPremSeparateDetailSchema.setFeeFinaType(tNoZeroSSRS.GetText(i, 12));
				
				tLYPremSeparateDetailSchema.setState("01");
				tLYPremSeparateDetailSchema.setOtherState(YGZ.OTHERSTATE_11);//万能、特需初始扣费
				tLYPremSeparateDetailSchema.setMoneyType("01");
				tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
				tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
				tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
			}
		}
		System.out.println("个险万能首期、续期初始扣费:"+tNoZeroSSRS.getMaxRow());
		//团险万能、特需、委托管理初始扣费
		String tSQL2= "select "
					+ " trim(lja.payno)||trim(lcin.grppolno)||trim(lcin.moneytype) ,"
					+ " lja.getnoticeno ,null tempfeetype,lcin.grppolno otherno,lcin.othertype othernotype,"
					+ " (select prtno from lcgrpcont where grpcontno = lcin.grpcontno union select prtno from lbgrpcont where grpcontno = lcin.grpcontno ) prtno,"
					+ " lcin.grpcontno policyno,"
					+ " lcin.riskcode,"
					+ " lja.managecom,"
					+ " sum(lcin.fee),"
					+ " lja.payno,"
					+ " lcin.moneytype"
					+ " from ljapay lja,lcinsureaccfeetrace lcin "
					+ " where 1=1 "
					+ " and lja.incomeno=lcin.grpcontno "
					+ " and lcin.moneytype in ('GL','KF') "
					+ " and lcin.riskcode <>'170501' "//不提补充A
					+ " and ((lja.duefeetype='0' and lcin.othertype='1') or (lcin.othertype='2' and lja.getnoticeno=lcin.otherno))"//团险万能、特需也就有首期的初始扣费
					+ " and exists (select 1 from lcgrpcont where grpcontno=lcin.grpcontno and appflag='1'"
					+ " 			union select 1 from lbgrpcont where grpcontno=lcin.grpcontno )"
					+ " and not exists (select busino from LYPremSeparateDetail where busino=(trim(lja.payno)||trim(lcin.grppolno)||trim(lcin.moneytype)) ) "
					+ tWhereSQL
					+ " group by lja.payno,lcin.moneytype,lja.getnoticeno,lcin.grppolno,lcin.othertype,lcin.grpcontno,lcin.riskcode,lja.managecom,lcin.moneytype"
					+ " union "
					+ " select "
					+ " trim(lja.payno)||trim(lcin.grppolno)||trim(lcin.moneytype) ,"
					+ " lja.getnoticeno ,null tempfeetype,lcin.grppolno otherno,lcin.othertype othernotype,"
					+ " (select prtno from lcgrpcont where grpcontno = lcin.grpcontno union select prtno from lbgrpcont where grpcontno = lcin.grpcontno ) prtno,"
					+ " lcin.grpcontno policyno,"
					+ " lcin.riskcode,"
					+ " lja.managecom,"
					+ " sum(lcin.fee),"
					+ " lja.payno,"
					+ " lcin.moneytype"
					+ " from ljapay lja,lbinsureaccfeetrace lcin "
					+ " where 1=1 "
					+ " and lja.incomeno=lcin.grpcontno "
					+ " and lcin.moneytype in ('GL','KF') "
					+ " and lcin.riskcode <>'170501' "//不提补充A
					+ " and ((lja.duefeetype='0' and lcin.othertype='1') or (lcin.othertype='2' and lja.getnoticeno=lcin.otherno))"//团险万能、特需也就有首期的初始扣费
					+ " and exists (select 1 from lcgrpcont where grpcontno=lcin.grpcontno and appflag='1'"
					+ " 			union select 1 from lbgrpcont where grpcontno=lcin.grpcontno )"
					+ " and not exists (select busino from LYPremSeparateDetail where busino=(trim(lja.payno)||trim(lcin.grppolno)||trim(lcin.moneytype)) ) "
					+ tWhereSQL
					+ " group by lja.payno,lcin.moneytype,lja.getnoticeno,lcin.grppolno,lcin.othertype,lcin.grpcontno,lcin.riskcode,lja.managecom,lcin.moneytype"
					+ " with ur";
		SSRS tNoSSRS2 = new ExeSQL().execSQL(tSQL2);
		if(tNoSSRS2 != null && tNoSSRS2.getMaxRow() > 0){
			for (int i = 1; i <= tNoSSRS2.getMaxRow(); i++) {
				LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
				tLYPremSeparateDetailSchema.setBusiNo(tNoSSRS2.GetText(i, 1));
				tLYPremSeparateDetailSchema.setTempFeeNo(tNoSSRS2.GetText(i, 2));
				tLYPremSeparateDetailSchema.setTempFeeType(tNoSSRS2.GetText(i, 3));
				tLYPremSeparateDetailSchema.setOtherNo(tNoSSRS2.GetText(i, 4));
				tLYPremSeparateDetailSchema.setOtherNoType(tNoSSRS2.GetText(i, 5));
				tLYPremSeparateDetailSchema.setPrtNo(tNoSSRS2.GetText(i, 6));
				tLYPremSeparateDetailSchema.setPolicyNo(tNoSSRS2.GetText(i, 7));
				tLYPremSeparateDetailSchema.setRiskCode(tNoSSRS2.GetText(i, 8));
//				tLYPremSeparateDetailSchema.setBusiType(YGZ.BUSITYPE_04);//初始扣费
				tLYPremSeparateDetailSchema.setManageCom(tNoSSRS2.GetText(i, 9));
				tLYPremSeparateDetailSchema.setPayMoney(tNoSSRS2.GetText(i, 10));
				tLYPremSeparateDetailSchema.setMoneyNo(tNoSSRS2.GetText(i, 11));
				tLYPremSeparateDetailSchema.setFeeFinaType(tNoSSRS2.GetText(i, 12));
				
				tLYPremSeparateDetailSchema.setBusiType(mygz.getBusinType(null, tLYPremSeparateDetailSchema.getFeeFinaType(), tLYPremSeparateDetailSchema.getRiskCode()));// 初始扣费
				tLYPremSeparateDetailSchema.setState("01");
				tLYPremSeparateDetailSchema.setOtherState(YGZ.OTHERSTATE_11);//万能、特需初始扣费
				tLYPremSeparateDetailSchema.setMoneyType("01");
				tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
				tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
				tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
			}
		}
		System.out.println("团险万能、特需、委托管理初始扣费:"+tNoSSRS2.getMaxRow());
		
		//个险万能保全的初始扣费
		String tSQL3 = " select "
					+ " trim(lja.payno)||trim(lcin.serialno)||trim(lcin.moneytype) ,"
					+ " lja.getnoticeno ,null tempfeetype,lcin.polno otherno,lcin.othertype othernotype,"
					+ " (select prtno from lccont where conttype='1' and contno = lcin.contno union select prtno from lbcont where conttype='1' and contno = lcin.contno ) prtno,"
					+ " lcin.contno policyno,"
					+ " lcin.riskcode,"
					+ " lja.managecom,"
					+ " lcin.fee,"
					+ " lja.payno,"
					+ " lcin.moneytype,"
					+ " lp.edortype "
					+ " from lpedoritem lp,ljapay lja,lcinsureaccfeetrace lcin"
					+ " where 1=1 "
					+ " and lp.edorno = lja.incomeno "
					+ " and lp.contno = lcin.contno "
					+ " and lja.incomeno=lcin.otherno "
					+ " and lcin.moneytype in ('GL','KF')"
					+ " and lja.incometype = '10' "//个单保全
					+ " and lcin.fee<>0 "
					+ " and exists (select 1 from lccont where contno=lcin.contno and conttype='1' and appflag='1' "
					+ " 			union select 1 from lbcont where contno=lcin.contno and conttype='1') "
					+ " and not exists (select busino from LYPremSeparateDetail where busino=(trim(lja.payno)||trim(lcin.serialno)||trim(lcin.moneytype)) ) "
					+ tWhereSQL
					+ " union "
					+ " select "
					+ " trim(lja.payno)||trim(lcin.serialno)||trim(lcin.moneytype) ,"
					+ " lja.getnoticeno ,null tempfeetype,lcin.polno otherno,lcin.othertype othernotype,"
					+ " (select prtno from lccont where conttype='1' and contno = lcin.contno union select prtno from lbcont where conttype='1' and contno = lcin.contno ) prtno,"
					+ " lcin.contno policyno,"
					+ " lcin.riskcode,"
					+ " lja.managecom,"
					+ " lcin.fee,"
					+ " lja.payno,"
					+ " lcin.moneytype,"
					+ " lp.edortype "
					+ " from lpedoritem lp,ljapay lja,lbinsureaccfeetrace lcin"
					+ " where 1=1 "
					+ " and lp.edorno = lja.incomeno "
					+ " and lp.contno = lcin.contno "
					+ " and lja.incomeno=lcin.otherno "
					+ " and lcin.moneytype in ('GL','KF')"
					+ " and lja.incometype = '10' "//个单保全
					+ " and lcin.fee<>0 "
					+ " and exists (select 1 from lccont where contno=lcin.contno and conttype='1' and appflag='1' "
					+ " 			union select 1 from lbcont where contno=lcin.contno and conttype='1') "
					+ " and not exists (select busino from LYPremSeparateDetail where busino=(trim(lja.payno)||trim(lcin.serialno)||trim(lcin.moneytype)) ) "
					+ tWhereSQL
					+ " with ur";
		SSRS tNoSSRS3 = new ExeSQL().execSQL(tSQL3);
		if(tNoSSRS3 != null && tNoSSRS3.getMaxRow() > 0){
			for (int i = 1; i <= tNoSSRS3.getMaxRow(); i++) {
				LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
				tLYPremSeparateDetailSchema.setBusiNo(tNoSSRS3.GetText(i, 1));
				tLYPremSeparateDetailSchema.setTempFeeNo(tNoSSRS3.GetText(i, 2));
				tLYPremSeparateDetailSchema.setTempFeeType(tNoSSRS3.GetText(i, 3));
				tLYPremSeparateDetailSchema.setOtherNo(tNoSSRS3.GetText(i, 4));
				tLYPremSeparateDetailSchema.setOtherNoType(tNoSSRS3.GetText(i, 5));
				tLYPremSeparateDetailSchema.setPrtNo(tNoSSRS3.GetText(i, 6));
				tLYPremSeparateDetailSchema.setPolicyNo(tNoSSRS3.GetText(i, 7));
				tLYPremSeparateDetailSchema.setRiskCode(tNoSSRS3.GetText(i, 8));
				tLYPremSeparateDetailSchema.setBusiType(YGZ.BUSITYPE_04);//初始扣费
				tLYPremSeparateDetailSchema.setManageCom(tNoSSRS3.GetText(i, 9));
				tLYPremSeparateDetailSchema.setPayMoney(tNoSSRS3.GetText(i, 10));
				tLYPremSeparateDetailSchema.setMoneyNo(tNoSSRS3.GetText(i, 11));
				tLYPremSeparateDetailSchema.setFeeFinaType(tNoSSRS3.GetText(i, 12));
				
				tLYPremSeparateDetailSchema.setState("01");
				tLYPremSeparateDetailSchema.setOtherState(YGZ.OTHERSTATE_12);//万能、特需 保全初始扣费
				tLYPremSeparateDetailSchema.setMoneyType("01");
				tLYPremSeparateDetailSchema.setFeeOperationType(tNoSSRS3.GetText(i, 13));
				
				tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
				tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
				tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
			}
		}
		System.out.println("个险万能保全的初始扣费:"+tNoSSRS3.getMaxRow());
		
		//团险万能、特需保全的初始扣费
		String tSQL4 = " select "
					+ " trim(lja.payno)||trim(lcin.grppolno)||trim(lcin.moneytype),"
					+ " lja.getnoticeno ,null tempfeetype,lcin.grppolno otherno,lcin.othertype othernotype,"
					+ " (select prtno from lcgrpcont where grpcontno = lcin.grpcontno union select prtno from lbgrpcont where grpcontno = lcin.grpcontno ) prtno,"
					+ " lcin.grpcontno policyno,"
					+ " lcin.riskcode,"
					+ " lja.managecom,"
					+ " sum(lcin.fee),"
					+ " lja.payno,"
					+ " lcin.moneytype,"
					+ " lp.edortype "
					+ " from lpgrpedoritem lp,ljapay lja,lcinsureaccfeetrace lcin"
					+ " where 1=1 "
					+ " and lp.edorno = lja.incomeno "
					+ " and lp.grpcontno = lcin.grpcontno "
					+ " and lja.incomeno=lcin.otherno "
					+ " and lcin.moneytype in ('GL','KF')"
					+ " and lja.incometype = '3' "//团单保全
					+ " and lcin.riskcode <>'170501' "//不提补充A
					+ " and exists (select 1 from lcgrpcont where grpcontno=lcin.grpcontno and appflag='1' "
					+ " 			union select 1 from lbgrpcont where grpcontno=lcin.grpcontno ) "
					+ " and not exists (select busino from LYPremSeparateDetail where busino=(trim(lja.payno)||trim(lcin.serialno)||trim(lcin.moneytype)) ) "
					+ tWhereSQL
					+ " group by lja.payno,lcin.moneytype,lja.getnoticeno,lcin.grppolno,lcin.othertype,lcin.grpcontno,lcin.riskcode,lja.managecom,lcin.moneytype,lp.edortype "
					+ " union "
					+ " select "
					+ " trim(lja.payno)||trim(lcin.grppolno)||trim(lcin.moneytype),"
					+ " lja.getnoticeno ,null tempfeetype,lcin.grppolno otherno,lcin.othertype othernotype,"
					+ " (select prtno from lcgrpcont where grpcontno = lcin.grpcontno union select prtno from lbgrpcont where grpcontno = lcin.grpcontno ) prtno,"
					+ " lcin.grpcontno policyno,"
					+ " lcin.riskcode,"
					+ " lja.managecom,"
					+ " sum(lcin.fee),"
					+ " lja.payno,"
					+ " lcin.moneytype,"
					+ " lp.edortype "
					+ " from lpgrpedoritem lp,ljapay lja,lbinsureaccfeetrace lcin"
					+ " where 1=1 "
					+ " and lp.edorno = lja.incomeno "
					+ " and lp.grpcontno = lcin.grpcontno "
					+ " and lja.incomeno=lcin.otherno "
					+ " and lcin.moneytype in ('GL','KF')"
					+ " and lja.incometype = '3' "//团单保全
					+ " and lcin.riskcode <>'170501' "//不提补充A
					+ " and exists (select 1 from lcgrpcont where grpcontno=lcin.grpcontno and appflag='1' "
					+ " 			union select 1 from lbgrpcont where grpcontno=lcin.grpcontno ) "
					+ " and not exists (select busino from LYPremSeparateDetail where busino=(trim(lja.payno)||trim(lcin.serialno)||trim(lcin.moneytype)) ) "
					+ tWhereSQL
					+ " group by lja.payno,lcin.moneytype,lja.getnoticeno,lcin.grppolno,lcin.othertype,lcin.grpcontno,lcin.riskcode,lja.managecom,lcin.moneytype,lp.edortype "
					+ " with ur";
		SSRS tNoSSRS4 = new ExeSQL().execSQL(tSQL4);
		if(tNoSSRS4 != null && tNoSSRS4.getMaxRow() > 0){
			for (int i = 1; i <= tNoSSRS4.getMaxRow(); i++) {
				LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
				tLYPremSeparateDetailSchema.setBusiNo(tNoSSRS4.GetText(i, 1));
				tLYPremSeparateDetailSchema.setTempFeeNo(tNoSSRS4.GetText(i, 2));
				tLYPremSeparateDetailSchema.setTempFeeType(tNoSSRS4.GetText(i, 3));
				tLYPremSeparateDetailSchema.setOtherNo(tNoSSRS4.GetText(i, 4));
				tLYPremSeparateDetailSchema.setOtherNoType(tNoSSRS4.GetText(i, 5));
				tLYPremSeparateDetailSchema.setPrtNo(tNoSSRS4.GetText(i, 6));
				tLYPremSeparateDetailSchema.setPolicyNo(tNoSSRS4.GetText(i, 7));
				tLYPremSeparateDetailSchema.setRiskCode(tNoSSRS4.GetText(i, 8));
//				tLYPremSeparateDetailSchema.setBusiType(YGZ.BUSITYPE_04);//初始扣费
				tLYPremSeparateDetailSchema.setManageCom(tNoSSRS4.GetText(i, 9));
				tLYPremSeparateDetailSchema.setPayMoney(tNoSSRS4.GetText(i, 10));
				tLYPremSeparateDetailSchema.setMoneyNo(tNoSSRS4.GetText(i, 11));
				tLYPremSeparateDetailSchema.setFeeFinaType(tNoSSRS4.GetText(i, 12));
				
				tLYPremSeparateDetailSchema.setState("01");
				tLYPremSeparateDetailSchema.setOtherState(YGZ.OTHERSTATE_12);//万能、特需 保全初始扣费
				tLYPremSeparateDetailSchema.setMoneyType("01");
				tLYPremSeparateDetailSchema.setFeeOperationType(tNoSSRS4.GetText(i, 13));
				tLYPremSeparateDetailSchema.setBusiType(mygz.getBusinType(null, tLYPremSeparateDetailSchema.getFeeFinaType(), tLYPremSeparateDetailSchema.getRiskCode()));// 初始扣费
				
				tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
				tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
				tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
				tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
			}
		}
		System.out.println("团险万能保全的初始扣费:"+tNoSSRS4.getMaxRow());
		
		mMap.put(tLYPremSeparateDetailSet, SysConst.INSERT);
		
		return true;
	}
	/**
	 * 准备后台的数据
	 * @return boolean
	 */
	private boolean prepareOutputData() {
		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCProjectCancelBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
	
	public static void main(String[] args) {
		GetULIInitPremSeparateBL tt=new GetULIInitPremSeparateBL();
		VData cInputData = new VData();
		TransferData tTransferData = new TransferData();
		String tStartDate = "2016-07-01";
		String tEndDate = "2016-07-10";
		tTransferData.setNameAndValue("StartDate", tStartDate);
		tTransferData.setNameAndValue("EndDate", tEndDate);
		cInputData.add(tTransferData);
		tt.submitData(cInputData, "");
		
	}
}
