package com.sinosoft.lis.ygz;

import java.util.ArrayList;
import java.util.List;

import com.sinosoft.lis.db.LYOutPayDetailDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LYOutPayDetailSchema;
import com.sinosoft.lis.vschema.LYOutPayDetailSet;
import com.sinosoft.lis.ygz.obj.OutPayBillInfo;
import com.sinosoft.lis.ygz.obj.OutPaySendResult;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

/**
 * <p>Title: 营改增，开票接口处理类</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company: Sinosoft</p>
 * @author Alex
 * @version 1.0
 */

public class OutPayUploadBL {
	
  private GlobalInput tGI= new GlobalInput();
  
  private LYOutPayDetailSet tLYOutPayDetailSet = null;
  
  public CErrors mErrors = new CErrors();
  
  private String operator ;
  
  private int sysCount = 1000;//默认每一千条数据发送一次请求
  
  private MMap resultMMap = new MMap();
  
  private String oBatchNo = "" ;

  public OutPayUploadBL() {
	  
  }
  
  public static void main(String[] args) {
	  OutPayUploadBL tt=new OutPayUploadBL();
	  LYOutPayDetailDB tLYOutPayDetailDB=new LYOutPayDetailDB();
	  LYOutPayDetailSet set=tLYOutPayDetailDB.executeQuery("select * from LYOutPayDetail where busino in ('180009993218011162001','1800064994180111601','180006657938011560801')");
	  VData data=new VData();
	  data.add(set);
	  GlobalInput mgl=new GlobalInput();
	  mgl.Operator="ser";
	  data.add(mgl);
	  tt.getSubmit(data, "");
	  MMap map=tt.getResult();
	  VData tVData=new VData();
	  tVData.add(map);
	  
	  PubSubmit tPubSubmit=new PubSubmit();
	  tPubSubmit.submitData(tVData, "");
	  
  }
  
  /**
   * 接收LYOutPayDetail数据进行价税分离
   * 数据量最好不要超过1000条
   * @param tVData
   * @param operator 可为空""
   * @return
   */
  public boolean getSubmit(VData tVData,String operator){
	  
	  if(!getInputData(tVData)){
		  return false;
	  }
	  
	  if(!dealData()){
		  return false;
	  }
	  
	  return true;
  }
  
  /**
   * 获取数据
   * @param mInputData
   * @return
   */
  private boolean getInputData(VData mInputData) {
      tGI = ((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));

      tLYOutPayDetailSet = ((LYOutPayDetailSet) mInputData.getObjectByObjectName("LYOutPayDetailSet",0));
      
      if(null == tLYOutPayDetailSet || tLYOutPayDetailSet.size()==0){
      	  CError tError = new CError();
          tError.moduleName = "OutPayUploadBL";
          tError.functionName = "getInputData";
          tError.errorMessage = "没有接收到保单信息!";
          this.mErrors.addOneError(tError);
      }

      if (tGI == null) {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "OutPayUploadBL";
          tError.functionName = "getInputData";
          tError.errorMessage = "没有得到足够的数据，请您确认!";
          this.mErrors.addOneError(tError);

          return false;
      }
      operator = tGI.Operator;

      return true;
  }
  
  
  /**
   * 封装数据调用发票分离接口
   * @return
   */
  private boolean dealData(){
	  LYOutPayDetailSet resLYOutPayDetailSet=new LYOutPayDetailSet();
	  LYOutPayDetailSet mLYOutPayDetailSet =new LYOutPayDetailSet();
	  List<OutPayBillInfo> billInfoList= new  ArrayList<OutPayBillInfo>();
	  int count=0;   
	  oBatchNo = PubFun.getCurrentDate2()+PubFun1.CreateMaxNo("OUTU"+PubFun.getCurrentDate2(), 10);
	 
	  for(int i=1; i<=tLYOutPayDetailSet.size(); i++){
		  LYOutPayDetailSchema tdetailSchema=tLYOutPayDetailSet.get(i);
		  mLYOutPayDetailSet.add(tdetailSchema);
		  count++;
		 if(count == sysCount || i == tLYOutPayDetailSet.size()){
		  count=0;
		 try{
		  for(int j=1;j<=mLYOutPayDetailSet.size();j++){
		  LYOutPayDetailSchema detailSchema= mLYOutPayDetailSet.get(j);
		  OutPayBillInfo bill=new OutPayBillInfo();
		  bill.setSequenceNo(detailSchema.getBusiNo());
		  bill.setIsupdate(detailSchema.getisupdate());
		  bill.setPk_pkgroup(detailSchema.getpkgroup());
		  bill.setPk_pkorg(detailSchema.getpkorg());
		  bill.setCode(detailSchema.getcode());
		  bill.setName(detailSchema.getname());
		  bill.setCreator(detailSchema.getcreator());
		  bill.setCreationtime(detailSchema.getcreationtime());
		  bill.setModifier(detailSchema.getmodifier());
		  bill.setModifiedtime(detailSchema.getmodifiedtime());
		  bill.setTaxpayertype(detailSchema.gettaxpayertype());
		  bill.setInvtype(detailSchema.getinvtype());
		  bill.setMakefreq(detailSchema.getmakefreq());
		  bill.setMakespace(detailSchema.getmakespace());
		  bill.setTelephone(detailSchema.gettelephone());
		  bill.setMobile(detailSchema.getmobile());
		  bill.setSendway(detailSchema.getsendway());
		  bill.setAddressee(detailSchema.getaddressee());
		  bill.setPostcode(detailSchema.getpostcode());
		  bill.setMailingaddress(detailSchema.getmailingaddress());
		  bill.setCustomertype(detailSchema.getcustomertype());
		  bill.setCountry(detailSchema.getcountry());
		  bill.setLegalperson(detailSchema.getlegalperson());
		  bill.setTaxnumber(detailSchema.gettaxnumber());
		  bill.setBank(detailSchema.getbank());
		  bill.setBankaccount(detailSchema.getbankaccount());
		  bill.setMakeinvdate(detailSchema.getmakeinvdate());
		  bill.setReceiveperson(detailSchema.getreceiveperson());
		  bill.setIspaperyinv(detailSchema.getispaperyinv());
		  bill.setEnablestate(detailSchema.getenablestate());
		  bill.setIsearly(detailSchema.getisearly());
		  bill.setBillmode(detailSchema.getbillmode());
		  bill.setVaddress(detailSchema.getvaddress());
		  bill.setOpenid(detailSchema.getopenid());
		  bill.setBillaccount(detailSchema.getbillaccount());
		  bill.setEmail(detailSchema.getemail());
		  bill.setVpsnid(detailSchema.getvpsnid());
		  bill.setVeffectdate(detailSchema.getveffectdate());
		  bill.setVmailprovince(detailSchema.getvmailprovince());
		  bill.setVmailcity(detailSchema.getvmailcity());
		  bill.setVsrcsystem(detailSchema.getvsrcsystem());
		  bill.setVsrcrowid(detailSchema.getvsrcrowid());
		 
		  bill.setPk_group(detailSchema.getgroup());
		  bill.setPk_org(detailSchema.getorg());
		  bill.setPk_org_v(detailSchema.getorgv());
		  bill.setCalnum(detailSchema.getcalnum());
		  bill.setCaldate(detailSchema.getcaldate());
		  bill.setVdata(detailSchema.getvdata());
		  bill.setRate(detailSchema.getrate());
		  bill.setPk_ptsitem(detailSchema.getptsitem());
		  bill.setMethodcal(detailSchema.getmethodcal());
		  bill.setDrawback(detailSchema.getdrawback());
		  bill.setTaxrate(detailSchema.gettaxrate());
		  bill.setOriamt(detailSchema.getoriamt());
		  bill.setOritax(detailSchema.getoritax());
		  bill.setLocalamt(detailSchema.getlocalamt());
		  bill.setLocaltax(detailSchema.getlocaltax());
		  bill.setDecldate(detailSchema.getdecldate());
		  bill.setBusidate(detailSchema.getbusidate());
		  bill.setCustcode(detailSchema.getcustcode());
		  bill.setCustname(detailSchema.getcustname());
		  bill.setCusttype(detailSchema.getcusttype());
		  bill.setDectype(detailSchema.getdectype());
		  bill.setSrcsystem(detailSchema.getsrcsystem());
		  bill.setVoucherid(detailSchema.getvoucherid());
		  bill.setTranserial(detailSchema.gettranserial());
		  bill.setTranbatch(detailSchema.gettranbatch());
		  bill.setTrandate(detailSchema.gettrandate());
		  bill.setTrantime(detailSchema.gettrantime());
		  bill.setTranorg(detailSchema.gettranorg());
		  bill.setTranchannel(detailSchema.gettranchannel());
		  bill.setTrancounte(detailSchema.gettrancounte());
		  bill.setCustmanager(detailSchema.getcustmanager());
		  bill.setTrantype(detailSchema.gettrantype());
		  bill.setProcode(detailSchema.getprocode());
		  bill.setBusino(detailSchema.getbusitype());
		  bill.setTranremark(detailSchema.gettranremark());
		  bill.setAccountno(detailSchema.getaccountno());
		  bill.setTrancurrency(detailSchema.gettrancurrency());
		  bill.setTranamt(detailSchema.gettranamt());
		  bill.setTaxtype(detailSchema.gettaxtype());
		  bill.setCztype(detailSchema.getcztype());
		  bill.setCzolddate(detailSchema.getczolddate());
		  bill.setIscurrent(detailSchema.getiscurrent());
		  bill.setCzbusino(detailSchema.getczbusino());
		  bill.setAccno(detailSchema.getaccno());
		  bill.setLoanflag(detailSchema.getloanflag());
		  bill.setAcccode(detailSchema.getacccode());
		  bill.setPaymentflag(detailSchema.getpaymentflag());
		  bill.setAmortno(detailSchema.getamortno());
		  bill.setMidcontno(detailSchema.getmidcontno());
		  bill.setIsinnerbank(detailSchema.getisinnerbank());
		  bill.setOverseasflag(detailSchema.getoverseasflag());
		  bill.setTaxcalmethod(detailSchema.gettaxcalmethod());
		  bill.setCreator1(detailSchema.getcreator1());
		  bill.setCreationtime1(detailSchema.getcreationtime1());
		  bill.setModifier1(detailSchema.getmodifier1());
		  bill.setModifiedtime1(detailSchema.getmodifiedtime1());
		  bill.setNegtype(detailSchema.getnegtype());
		  bill.setTranplace(detailSchema.gettranplace());
		  bill.setDeptdoc(detailSchema.getdeptdoc());
		  bill.setConfimtype(detailSchema.getconfimtype());
		  bill.setIspts(detailSchema.getispts());
		  bill.setIsbill(detailSchema.getisbill());
		  bill.setAreatype(detailSchema.getareatype());
		  bill.setIsptserror(detailSchema.getisptserror());
		  bill.setReceivedate(detailSchema.getreceivedate());
		  bill.setIsalloweadvance(detailSchema.getisalloweadvance());
		  bill.setBusipk(detailSchema.getbusipk());
		  bill.setBusiitem(detailSchema.getbusiitem());
		  bill.setFininsance(detailSchema.getfininsance());
		  bill.setFininstype(detailSchema.getfininstype());
		  bill.setIstaxfree(detailSchema.getistaxfree());
		  bill.setBunit(detailSchema.getbunit());
		  bill.setBunivalent(detailSchema.getbunivalent());
		  bill.setBnumber(detailSchema.getbnumber());
		  bill.setBilltype(detailSchema.getbilltype());
		  bill.setBilldate(detailSchema.getbilldate());
		  bill.setServicedoc(detailSchema.getservicedoc());
		  bill.setIshistory(detailSchema.getishistory());
		  bill.setIsmoneny(detailSchema.getismoneny());
		  bill.setInsureno(detailSchema.getinsureno());
		  bill.setIsfirst(detailSchema.getisfirst());
		  bill.setPerspolicyno(detailSchema.getperspolicyno());
		  bill.setPpveridate(detailSchema.getppveridate());
		  bill.setBillstartdate(detailSchema.getbillstartdate());
		  bill.setBillenddate(detailSchema.getbillenddate());
		  bill.setPaytype(detailSchema.getpaytype());
		  bill.setIsmat(detailSchema.getismat());
		  bill.setIsentrust(detailSchema.getisentrust());
		  bill.setBilleffectivedate(detailSchema.getbilleffectivedate());
		  bill.setRecoincomdate(detailSchema.getrecoincomdate());
		  bill.setBillfalg(detailSchema.getbillfalg());
		  bill.setOffsetdate(detailSchema.getoffsetdate());
		  bill.setBatchno(detailSchema.getbqbatchno());
		  bill.setIssale(detailSchema.getissale());
		  bill.setVdef1(detailSchema.getvdef1());
		  bill.setVdef2(detailSchema.getvdef2());
		  bill.setVdef6(detailSchema.getvdef6());
		  bill.setVdef7(detailSchema.getvdef7());
		  bill.setVdef8(detailSchema.getvdef8());
		  bill.setBuymailbox(detailSchema.getbuymailbox());
		  bill.setBuyphoneno(detailSchema.getbuyphoneno());
		  bill.setBuyidtype(detailSchema.getbuyidtype());
		  bill.setBuyidnumber(detailSchema.getbuyidnumber());
		  billInfoList.add(bill);
		  }	 
	  //调用接口进行发票分离
	  OutPayUploadInterface tOutPayUploadInterface=new OutPayUploadInterface();
	  List<OutPaySendResult> resultList=tOutPayUploadInterface.callInterface(billInfoList);
	  billInfoList.clear();
	  //对分离结果进行结息封装LYOutPayDetail数据
	  for(int m=0; m<resultList.size(); m++){
		  OutPaySendResult result=resultList.get(m);
		  String resCode = result.getResultcode();
		  String resDesc = result.getResultdescription();
		  String content=result.getContent();
          String bdocid = result.getBdocid();
          
          //返回结果代码
         
		  for(int k=1; k<=mLYOutPayDetailSet.size(); k++){
			  LYOutPayDetailSchema mdetailSchema=mLYOutPayDetailSet.get(k);
			  if(bdocid.equals(mdetailSchema.getBusiNo())){
				  //为本次请求生成批次号
				  mdetailSchema.setBatchNo(oBatchNo); 
				  if("1".equals(resCode)){
						 //发票数据推送成功
						  mdetailSchema.setstate("02");
						  mdetailSchema.setErrorInfo(null);
						  System.out.println("==="+mdetailSchema.getstate());

			  }else{
				  if(resDesc != null && !"".equals(resDesc)){
					  if(resDesc.length()>200){
						  resDesc=resDesc.substring(0, 200);
					  }
					  	    mdetailSchema.setstate("03");
					  		mdetailSchema.setErrorInfo(resDesc);
					  		System.out.println("==="+mdetailSchema.getstate());
					  
				  }else{
					    mdetailSchema.setstate("03");
				  		mdetailSchema.setErrorInfo(resDesc);
				  		System.out.println("==="+mdetailSchema.getstate());
					  
				  }  
			  }
				  resLYOutPayDetailSet.add(mdetailSchema);
				  //已经获取结果的remove掉
				  mLYOutPayDetailSet.remove(mLYOutPayDetailSet.get(k));
				 
				  break; 
		  }

		  } 
	    
		  }
	  
		}catch(Exception e){
		  billInfoList.clear();
		  e.printStackTrace();
		 }
	   mLYOutPayDetailSet.clear();
	  }
		
  }
	 
	
	  resultMMap.put(resLYOutPayDetailSet,SysConst.DELETE_AND_INSERT);
	 
	  return true;
  }
  
  public MMap getResult(){
	  return resultMMap;
  }
  

}
