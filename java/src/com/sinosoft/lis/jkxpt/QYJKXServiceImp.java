package com.sinosoft.lis.jkxpt;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.yibaotong.JdomUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class QYJKXServiceImp extends JKXService {

	public boolean callJKXService(VData cInputData, String cOperate){
		
        //1 准备业务数据，将不同节点元素信息内容放入二维数组SSRS中。
		
		//2 通过SSRS数组，构建XML JDOM
		
		QYXmlTag tQYXmlTag = new QYXmlTag();
//		for (int i = 0; i < tQYXmlTag.PolicyNode.length; i++) {
//			System.out.println(tQYXmlTag.PolicyNode[i]);
//		}
		
		//3 重新组织XML数据结构，构建消息体
//		QYXmlTag tQYXmlTag = new QYXmlTag();
		String[] tData = new String[tQYXmlTag.PolicyNode.length];
		for(int i=1; i<=tQYXmlTag.PolicyNode.length;i++){
			tData[i-1] = "ElemContent"+i;
		}
		
		JKXService tJKXService = new JKXService();
		Element out1 = tJKXService.formJElem(tData, tQYXmlTag.PolicyNode, tQYXmlTag.policy);
//		JdomUtil.print(out1);
		
		ExeSQL tExeSQL = new ExeSQL();
		String tSql = "select '01','02','03','04','05','06','07','08','09','010','011','012','013','014','015','016','017','018','019' from dual "
                    + " union "
                    + " select '11','12','13','14','15','16','17','18','19','110','111','112','113','114','115','116','117','118','119' from dual";
		SSRS tSSRS = tExeSQL.execSQL(tSql);
		System.out.println(tQYXmlTag.policyInsuredNode.length);
		Element out2 = tJKXService.formJElem(tSSRS, tQYXmlTag.PolicyInsuredList, tQYXmlTag.policyInsuredNode, tQYXmlTag.PolicyInsured);
//		JdomUtil.print(out2);
		
		Element root = out1;
		root.addContent(out2);
//		root.addChild(out2);
		
		Document tDocument = formJDocMessage(root,"01","TransactionNo_123232XXX");
		JdomUtil.print(tDocument);
		
		//4 调用前置机SERVLET
		Document tInStdXmlDoc = tDocument;
		
		ReqJKXPreService tJKXPreServlet = new ReqJKXPreService();
		Document tReceiveXmlDoc = tJKXPreServlet.callService(tInStdXmlDoc);
		JdomUtil.print(tReceiveXmlDoc);
		return true;
	}
	
	/**
	 * 测试主程序
	 * @param args
	 */
	public static void main(String[] args) {
		QYJKXServiceImp tQYJKXServiceImp =  new QYJKXServiceImp();
		tQYJKXServiceImp.callJKXService(null, null);
	}
}
