/**
 * 2010-11-18
 */
package com.sinosoft.lis.jkxpt;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.jkxpt.JKXService;
import com.sinosoft.lis.jkxpt.ReqJKXPreService;
import com.sinosoft.lis.yibaotong.JdomUtil;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class LPJKXUpdateClaimStatusServiceImp extends JKXService
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private TransferData mTransferData = null;

    /** 数据操作字符串 */
    private String mOperate = "";

    private String mDataInfoKey = "";

    private Element mRoot = null;

    private String mContType = null;

    public boolean callJKXService(VData cInputData, String cOperate)
    {
        // 获取入口参数 
        if (!getInputData(cInputData, cOperate))
        {
            return false;
        }
        // --------------------

        // 取数逻辑处理        
        //        mRoot = dealBusiXml(mPolicyNo);
        mRoot = reportBusiXml(mDataInfoKey);
        // --------------------

        // 调用前置机
        if (!calService())
        {
            return false;
        }
        // --------------------

        return true;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mDataInfoKey = (String) mTransferData.getValueByName("DataInfoKey");
        if (mDataInfoKey == null || mDataInfoKey.equals(""))
        {
            buildError("getInputData", "获取案件号失败。");
            return false;
        }

        return true;
    }

    private Element reportBusiXml(String cDataInfoKey)
    {
        Element tRoot = new Element("ClaimInfoDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.ClaimInfoDTO");

        Element tTmpEle = null;

        SSRS tResClaimInfo = loadUpdateClaimStatus(cDataInfoKey);
        for (int idxPI = 1; idxPI <= tResClaimInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResClaimInfo.getRowData(idxPI);

            tTmpEle = new Element("ClaimNo");
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CancelDate");
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
        }

        return tRoot;
    }
    private SSRS loadUpdateClaimStatus(String cDataInfoKey)
    {
        SSRS tResult = null;
        StringBuffer tStrBuSql = new StringBuffer();
        
        tStrBuSql
                .append("select caseno ClaimNo,CancleDate CancelDate from llcase where caseno='"+cDataInfoKey+"'"
                		+" and rgtstate='14' with ur");

        tResult = new ExeSQL().execSQL(tStrBuSql.toString());
        return tResult;
    }

    /**
     * 调用前置机服务
     * @return
     */
    private boolean calService()
    {
        Document tDocument = formJDocMessage(mRoot, "12", mDataInfoKey);
        JdomUtil.print(tDocument);

        Document tInStdXmlDoc = tDocument;

        ReqJKXPreService tJKXPreServlet = new ReqJKXPreService();
        Document tReceiveXmlDoc = tJKXPreServlet.callService(tInStdXmlDoc);
        JdomUtil.print(tReceiveXmlDoc);

        return true;
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        System.out.println(szFunc + ": " + szErrMsg);

        CError cError = new CError();
        cError.moduleName = "AddCertifyListBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 测试主程序
     * 
     * @param args
     */
    public static void main(String[] args)
    {
        LPJKXUpdateClaimStatusServiceImp tLPJKXServiceImp = new LPJKXUpdateClaimStatusServiceImp();
        VData tVData = new VData();
        TransferData mTransferData = new TransferData();
        mTransferData.setNameAndValue("DataInfoKey", "C1100050707000009");  //状态不为 14的 C1100050707000009
        tVData.add(mTransferData);											//状态为 14的 C1100060113000016
        tLPJKXServiceImp.callJKXService(tVData, null);
    }
}
