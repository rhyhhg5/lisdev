package com.sinosoft.lis.jkxpt;

/**
 * 契约XML 标签描述
 * @author filon51
 *
 */
public final class QYXmlTag {

	final String userName = "userName";// 用户名
	final String applicationDate = "applicationDate";
	final String applicationFormNo = "applicationFormNo";
	final String contractNo = "contractNo";
	final String contractSource = "contractSource";
	final String contractStatus = "contractStatus";
	final String contractType = "contractType";
	final String effectiveInsuredNum = "effectiveInsuredNum";
	final String forceUpdate = "forceUpdate";
	final String forceUpdateNo = "forceUpdateNo";
	final String formerpolicyNo = "formerpolicyNo";
	final String groupMark = "groupMark";
	final String mainAttachedFlag = "mainAttachedFlag";
	final String password = "password";// 用户密码
	final String policyEndDate = "policyEndDate";

	final String policy = "policy";
	//此数组元素间顺序决定生成XML元素的先后次序
	final String[] PolicyNode = {
			userName, applicationDate, applicationFormNo,
			contractNo, contractSource, contractStatus, contractType,
			effectiveInsuredNum, forceUpdate, forceUpdateNo, formerpolicyNo,
			groupMark, mainAttachedFlag, password, policyEndDate 
			};
	
	final String anomalyInform = "anomalyInform";
	final String benefitDistribution = "benefitDistribution";
	final String birthday = "birthday";
	final String critCode = "critCode";
	final String critType = "critType";
	final String customerNo = "customerNo";
	final String empoyeeNo = "empoyeeNo";
	final String gender = "gender";
	final String healthFlag = "healthFlag";
	final String insuredGroupCode = "insuredGroupCode";
	final String insuredType = "insuredType";
	final String name = "name";
	final String occupationalCode = "occupationalCode";
	final String offerStatus = "offerStatus";
	final String relationshipWithPh = "relationshipWithPh";
	final String serviceMark = "serviceMark";
	final String socialcareNo = "socialcareNo";
	final String subStandard = "subStandard";
	final String workPlace = "workPlace";
    
	final String PolicyInsuredList = "policyInsuredList";
	final String PolicyInsured = "policyInsured";
	final String[] policyInsuredNode = {
			anomalyInform, benefitDistribution,
			birthday, critCode, critType, customerNo, empoyeeNo, gender,
			healthFlag, insuredGroupCode, insuredType, name, occupationalCode,
			offerStatus, relationshipWithPh, serviceMark, socialcareNo,
			subStandard, workPlace 
			};
}
