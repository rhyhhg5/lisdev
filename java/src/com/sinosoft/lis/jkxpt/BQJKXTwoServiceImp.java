/**
 * @author LY
 * 2010-11-18 create
 * 20120727 modify zhangm 只处理个单  实际上团个都能处理（团单有点问题）
 */
package com.sinosoft.lis.jkxpt;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.jkxpt.JKXService;
import com.sinosoft.lis.jkxpt.ReqJKXPreService;
import com.sinosoft.lis.jkxpt.qy.QYJKXTwoServiceImp;
import com.sinosoft.lis.yibaotong.JdomUtil;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class BQJKXTwoServiceImp extends JKXService
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private TransferData mTransferData = null;

    /** 数据操作字符串 */
    private String mOperate = "";

    private String mDataInfoKey = "";
    
    private String mEdorNo = ""; //保全受理号
    
    private String mEdorType = ""; //保全项目类型
    
    private String mEdorProp = ""; //个团标志 ：I-个单，G-团单
    
    private String mContractNo = ""; //合同号
    
    private String mEdorAppDate = ""; //申请日
    
    private String mEdorValiDate = ""; //生效日

    private Element mRoot = null;

    public boolean callJKXService(VData cInputData, String cOperate)
    {
        // 获取入口参数 
        if (!getInputData(cInputData, cOperate))
        {
            return false;
        }
        // --------------------
        
        // 取数逻辑处理        
        //        mRoot = dealBusiXml(mPolicyNo);
        mRoot = reportBusiXml(mDataInfoKey);
        // --------------------

        // 调用前置机
        if (!calService())
        {
            return false;
        }
        // --------------------

        return true;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mDataInfoKey = (String) mTransferData.getValueByName("DataInfoKey");
        if (mDataInfoKey == null || mDataInfoKey.equals(""))
        {
            buildError("getInputData", "获取保全参数信息失败。");
            return false;
        }
        
    	String [] tEdorInfo = mDataInfoKey.split("&");
		mEdorNo = tEdorInfo[0];
		mEdorType = tEdorInfo[1];
		mEdorProp = tEdorInfo[2];
		mContractNo = tEdorInfo[3];
		mEdorAppDate = tEdorInfo[4];
		mEdorValiDate = tEdorInfo[5];
		
		if (mEdorNo == null || mEdorNo.equals(""))
        {
            buildError("getInputData", "获取保全受理号EdorNo信息失败。");
            return false;
        }
		
		if (mEdorType == null || mEdorType.equals(""))
        {
            buildError("getInputData", "获取保全类型EdorType信息失败。");
            return false;
        }
		if (mEdorProp == null || mEdorProp.equals(""))
        {
            buildError("getInputData", "获取保全个团标志EdorProp信息失败。");
            return false;
        }
		if (mContractNo == null || mContractNo.equals(""))
        {
            buildError("getInputData", "获取合同号ContractNo信息失败。");
            return false;
        }
		
		System.out.println("受理号mEdorNo---"+mEdorNo);
		System.out.println("受理号mEdorType---"+mEdorType);
		System.out.println("受理号mEdorProp---"+mEdorProp);
		System.out.println("受理号mContractNo---"+mContractNo);
		System.out.println("受理号mEdorAppDate---"+mEdorAppDate);
		System.out.println("受理号mEdorValiDate---"+mEdorValiDate);

        return true;
    }

    private Element reportBusiXml(String cDataInfoKey) //保单详细信息
    {
        Element tRoot = new Element("PolicyLogDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.PolicyLogDTO");

        Element tTmpEle = null;

        SSRS tResPolicyLogInfo = loadPolicyLogInfo(cDataInfoKey);
        for (int idxPI = 1; idxPI <= tResPolicyLogInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyLogInfo.getRowData(idxPI);
            
            tTmpEle = new Element("Rejection");
            tTmpEle.setText(tDataInfo[0]);
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ApplicationFormNo");
            tTmpEle.setText(tDataInfo[1]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PolicyNo");
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("EndorsementType");
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("EndorsementNo");
            tTmpEle.setText(tDataInfo[4]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("EndorsementApplicationDate");
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("EffectiveDate");
            tTmpEle.setText(tDataInfo[6]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("OrganName");
            tTmpEle.setText(tDataInfo[7]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("ChannelType");
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("SalesType");
            tTmpEle.setText(tDataInfo[9]);
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("SalesCode");
            tTmpEle.setText(tDataInfo[10]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("SalesName");
            tTmpEle.setText(tDataInfo[11]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("SalesChannelCode");
            tTmpEle.setText(tDataInfo[12]);
           // tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SalesChannelName");
            tTmpEle.setText(tDataInfo[13]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("BusinessAddress");
            tTmpEle.setText(tDataInfo[14]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("ContractType");
            tTmpEle.setText(tDataInfo[15]);
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("PolicyStartDate");
            tTmpEle.setText(tDataInfo[16]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PolicyEndDate");
            tTmpEle.setText(tDataInfo[17]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("TerminationDate");
            tTmpEle.setText(tDataInfo[18]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SuspendDate");
            tTmpEle.setText(tDataInfo[19]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RecoverDate");
            tTmpEle.setText(tDataInfo[20]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ContractSource");
            tTmpEle.setText(tDataInfo[21]);
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ContractNo");
            tTmpEle.setText(tDataInfo[22]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RenewalTimes"); //续保次数
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tTmpEle.setText(tDataInfo[23]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RenewalMethod");
            tTmpEle.setText(tDataInfo[24]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ContractStatus");
            tTmpEle.setText(tDataInfo[25]);
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("TerminationReason");
            if(tDataInfo[25].equals("3")){
            	tTmpEle.setText("3"); //暂时设为 3 - 退保
            }else{
            	tTmpEle.setText(tDataInfo[26]);
                tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            }
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SumInsured");
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为“NUMBER”的字段，请增加描述
            tTmpEle.setText(tDataInfo[27]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("EffectiveSumInsured");
            tTmpEle.setText(tDataInfo[28]);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为“NUMBER”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("Premium");
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为“NUMBER”的字段，请增加描述
            tTmpEle.setText(tDataInfo[29]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CurrentPremium");
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为“NUMBER”的字段，请增加描述
            tTmpEle.setText(tDataInfo[30]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PaymentMethod");
            tTmpEle.setText(tDataInfo[31]);
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PaymentYears");
            tTmpEle.setText(tDataInfo[32]);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为“NUMBER”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("PaymentNo");
            tTmpEle.setText(tDataInfo[33]);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为“NUMBER”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PolicyHolderPro");
            tTmpEle.setText(tDataInfo[34]);
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PolicyHolderNum");
            tTmpEle.setText(tDataInfo[35]);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为“NUMBER”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("EffectiveInsuredNum");
            tTmpEle.setText(tDataInfo[36]);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为“NUMBER”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("FormerpolicyNo");
            tTmpEle.setText(tDataInfo[37]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SpecialRemark");
            tTmpEle.setText(tDataInfo[38]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RegularClearingMark");
            tTmpEle.setText(tDataInfo[39]);
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RegularClearing");
            tTmpEle.setText(tDataInfo[40]);
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RegularClearingDate");
            tTmpEle.setText(tDataInfo[41]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PremiumDueDate");
            tTmpEle.setText("");//最晚结算日期应为空
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RealTimeClaimFlag");
            tTmpEle.setText(tDataInfo[43]);
//            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("PolicyLoan");
            tTmpEle.setText(tDataInfo[44]);
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("AutoPaidUp");
            tTmpEle.setText(tDataInfo[45]);
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("CoInsurance");
            tTmpEle.setText(tDataInfo[46]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("LeadCoInsurance");
            tTmpEle.setText(tDataInfo[47]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("AcceptDate");
            tTmpEle.setText(tDataInfo[48]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("OrganId");
            tTmpEle.setText(tDataInfo[49]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("ApplicationDate");
            tTmpEle.setText(tDataInfo[50]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
           
//          当年年化保费
            tTmpEle = new Element("YearPremium");
            tTmpEle.setText(tDataInfo[51]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            //退保金
            if(tDataInfo.length==53){
	            tTmpEle = new Element("SurrenderCharge");
	            tTmpEle.setText(tDataInfo[52]);
	            tTmpEle.addAttribute("CData", "CBigDecimal");
	            tItemEle.addContent(tTmpEle);
	            tTmpEle = null;
            }
            
            
            
            if(mEdorProp.equals("G"))
            {
            	//险种数组
//            	tTmpEle = reportPolicyUpcoverageLogInfo(mContractNo);
//	   	        if (tTmpEle != null)
//	   	        {
//	   	            tItemEle.addContent(tTmpEle);
//	   	        }
	   	   //团体投保人数组
            	tTmpEle = reportPolicyGroupPHLogInfo(mContractNo);
                if (tTmpEle != null)
                {
                    tItemEle.addContent(tTmpEle);
                }
                tTmpEle = null;
            	
            }
            else
            {
            	//个人投保人数组
            	tTmpEle = reportPolicyPersonPHLogInfo(mContractNo);
                if (tTmpEle != null)
                {
                    tItemEle.addContent(tTmpEle);
                }
                tTmpEle = null;
                //个人保全类型
                tTmpEle = reportEndorsementTypeInfo(mEdorType);
                if (tTmpEle != null)
                {
                    tItemEle.addContent(tTmpEle);
                }
                tTmpEle = null;
            }
           
//          被保人数组          
            tTmpEle = reportPolicyInsuredLogInfo(mContractNo);
 	        if (tTmpEle != null)
 	        {
 	            tItemEle.addContent(tTmpEle);
 	        }
 	        tTmpEle = null;
        }

        return tRoot;
    }

    private SSRS loadPolicyLogInfo(String cDataInfoKey)  // 保单详细信息
    {
        SSRS tResult = null;
        StringBuffer tStrBSql = new StringBuffer();
        if(mEdorProp.equals("I"))
        {
              tStrBSql.append(" select ");
            //1 是否拒保
              tStrBSql.append(" UWFlag Rejection, "); 
              tStrBSql.append(" PrtNo ApplicationFormNo, ");
              tStrBSql.append(" a.ContNo PolicyNo, ");
              tStrBSql.append(" 'I'||'"+ mEdorType + "' EndorsementType, ");
              tStrBSql.append(" '"+ mEdorNo + "' EndorsementNo, ");
              tStrBSql.append(" '"+ mEdorAppDate + "' EndorsementApplicationDate, ");
              tStrBSql.append(" '"+ mEdorValiDate + "' EffectiveDate, ");
              tStrBSql.append(" '中国人民健康保险股份有限公司北京分公司' OrganName, ");
              tStrBSql.append(" Salechnl ChannelType, ");
              tStrBSql.append(" Salechnl SalesType, ");
            //11. 销售人员代码
              tStrBSql.append(" (select qualifno 资格证号 from LAQUALIFICATION where agentcode = a.AgentCode) SalesCode, "); 
              tStrBSql.append(" (select name from laagent where agentcode=a.agentcode) SalesName, ");
              tStrBSql.append(" agentcom SalesChannelCode, ");
              tStrBSql.append(" (select name from lacom  where agentcom=a.agentcom) SalesChannelName, ");
              tStrBSql.append(" (select address  from lacom  where agentcom=a.agentcom) BusinessAddress, ");
              tStrBSql.append(" ContType ContractType, ");
              tStrBSql.append(" CValidate PolicyStartDate, ");
              tStrBSql.append(" a.CInvalidate PolicyEndDate, ");
              if(mEdorType.equals("XT")||mEdorType.equals("CT")||mEdorType.equals("WT"))
              {
            	  tStrBSql.append(" '"+mEdorValiDate + "' TerminationDate, ");
              }
              else
              {
            	  tStrBSql.append("a.CInvalidate TerminationDate, ");
              }
              tStrBSql.append(" (select max(StartDate) from LCContState where ContNo = a.ContNo and PolNo = '000000' and StateType = 'Available') SuspendDate, ");
//            21 保单效力恢复日期
              if(mEdorType.equals("FX"))
              {
            	  tStrBSql.append(" '"+ mEdorValiDate + "' RecoverDate, ");
              }
              else
              {
            	  tStrBSql.append(" '' RecoverDate, ");
              }
              tStrBSql.append(" (case (select 1 from lcrnewstatelog where contno = a.contno fetch first 1 row only) when 1 then '02' else '01' end) ContractSource, ");
              tStrBSql.append(" a.ContNo ContractNo, ");
              tStrBSql.append(" (select count(distinct NewContNo) from LCRNewStateLog where ContNo = a.ContNo) RenewalTimes, ");
              tStrBSql.append(" '' RenewalMethod, ");//modify by zhangyige 2013-01-04
              tStrBSql.append(" StateFlag ContractStatus, ");
              tStrBSql.append(" (select max(StateReaSon) from LCContState where ContNo = a.ContNo and PolNo = '000000' and StateType = 'Terminate') TerminationReason, ");
              tStrBSql.append(" nvl((select amnt from lpcont where contno=a.ContNo and edorno=(select min(edorno) from lpcont where contno=a.ContNo)),a.amnt) SumInsured, ");
              tStrBSql.append(" Amnt EffectiveSumInsured, ");
              tStrBSql.append(" nvl((select prem from lpcont where contno=a.ContNo and edorno=(select min(edorno) from lpcont where contno=a.ContNo)),a.prem) Premium, ");
//             31 当前保费
              tStrBSql.append(" Prem CurrentPremium, ");
              tStrBSql.append(" Payintv PaymentMethod, ");
              tStrBSql.append(" (select payyears from lcpol where contno = a.ContNo and polno = mainpolno fetch first 1 rows only ) PaymentYears, ");
              tStrBSql.append(" (select count(distinct payno) from ljapayperson where contno = a.contno) PaymentNo, ");
              tStrBSql.append(" '2' PolicyHolderPro, ");
              tStrBSql.append(" (select count(1) from lcinsured where contno=a.contno) PolicyHolderNum,");
              tStrBSql.append(" (select count(1) from lcinsured where contno=a.contno) EffectiveInsuredNum, ");
              tStrBSql.append(" a.ContNo FormerpolicyNo, ");
              tStrBSql.append(" ReMark SpecialRemark, ");
              tStrBSql.append(" '' RegularClearingMark, ");
//            41 定期结算方式
              tStrBSql.append(" '' RegularClearing, ");
              tStrBSql.append(" '' RegularClearingDate, ");
              tStrBSql.append(" SignDate PremiumDueDate, ");
              tStrBSql.append(" '0' RealTimeClaimFlag, ");
              tStrBSql.append(" '' PolicyLoan, ");//是否存在保单借款
              tStrBSql.append(" '' AutoPaidUp, ");//是否自动垫交
              tStrBSql.append(" '0' CoInsurance, ");//是否共保
              tStrBSql.append(" '' LeadCoInsurance, ");//是否主共保 --48
              tStrBSql.append(" SignDate AcceptDate, ");//增加保单出单日期
              tStrBSql.append(" '000085110000' OrganId, ");//增加保单所属机构代码
              tStrBSql.append(" PolApplyDate ApplicationDate ,");//增加 保单投保日期 
              
              tStrBSql.append("( case when a.payintv='1'  then prem*12 when a.payintv='3'  then prem*4  when a.payintv='6'  then prem*2 when a.payintv='12' then prem   else 0 end )  YearPremium");//当前年化保费
              
              if(mEdorType.equals("XT")||mEdorType.equals("CT")||mEdorType.equals("WT"))
              {
            	  tStrBSql.append(" ,(select abs(nvl(sum(getmoney),0)) from ljagetendorse where feeoperationtype in ('CT','XT') and feefinatype in ('TF','TB') and contno=a.contno) SurrenderCharge ");
              }//modify by zhangyige 2012-12-28 退保金加上绝对值
             
             
              tStrBSql.append(" from LCCont a ");
              tStrBSql.append(" where ContNo = '" + mContractNo + "' ");
  //----------------            
              tStrBSql.append(" union all ");
  //----------------              
              tStrBSql.append(" select ");
              //1 是否拒保
              tStrBSql.append(" UWFlag Rejection, "); 
              tStrBSql.append(" PrtNo ApplicationFormNo, ");
              tStrBSql.append(" a.ContNo PolicyNo, ");
              tStrBSql.append(" 'I'||'" + mEdorType + "' EndorsementType, ");
              tStrBSql.append(" '"+ mEdorNo + "' EndorsementNo, ");
              tStrBSql.append(" '"+ mEdorAppDate + "' EndorsementApplicationDate, ");
              tStrBSql.append(" '"+ mEdorValiDate + "' EffectiveDate, ");
              tStrBSql.append(" '中国人民健康保险股份有限公司北京分公司' OrganName, ");
              tStrBSql.append(" Salechnl ChannelType, ");
              tStrBSql.append(" Salechnl SalesType, ");
            //11. 销售人员代码
              tStrBSql.append(" (select qualifno 资格证号 from LAQUALIFICATION where agentcode = a.AgentCode) SalesCode, "); 
              tStrBSql.append(" (select name from laagent where agentcode=a.agentcode) SalesName, ");
              tStrBSql.append(" agentcom SalesChannelCode, ");
              tStrBSql.append(" (select name from lacom  where agentcom=a.agentcom) SalesChannelName, ");
              tStrBSql.append(" (select address  from lacom  where agentcom=a.agentcom) BusinessAddress, ");
              tStrBSql.append(" ContType ContractType, ");
              tStrBSql.append(" CValidate PolicyStartDate, ");
              tStrBSql.append(" a.CInvalidate PolicyEndDate, ");
              if(mEdorType.equals("XT")||mEdorType.equals("CT")||mEdorType.equals("WT"))
              {
            	  tStrBSql.append(" '"+mEdorValiDate + "' TerminationDate, ");
              }
              else
              {
            	  tStrBSql.append("a.CInvalidate TerminationDate, ");
              }
              tStrBSql.append(" (select max(StartDate) from LCContState where ContNo = a.ContNo and PolNo = '000000' and StateType = 'Available') SuspendDate, ");
//            21 保单效力恢复日期
              if(mEdorType.equals("FX"))
              {
            	  tStrBSql.append(" '"+ mEdorValiDate + "' RecoverDate, ");
              }
              else
              {
            	  tStrBSql.append(" '' RecoverDate, ");
              }
              tStrBSql.append(" (case (select 1 from lcrnewstatelog where contno = a.contno fetch first 1 row only) when 1 then '02' else '01' end) ContractSource, ");
              tStrBSql.append(" a.ContNo ContractNo, ");
              tStrBSql.append(" (select count(distinct NewContNo) from LCRNewStateLog where ContNo = a.ContNo) RenewalTimes, ");
              tStrBSql.append(" '' RenewalMethod, ");//modify by zhangyige 2013-01-04
              tStrBSql.append(" StateFlag ContractStatus, ");
              tStrBSql.append(" (select max(StateReaSon) from LCContState where ContNo = a.ContNo and PolNo = '000000' and StateType = 'Terminate') TerminationReason, ");
              tStrBSql.append(" nvl((select amnt from lpcont where contno=a.ContNo and edorno=(select min(edorno) from lpcont where contno=a.ContNo)),a.amnt) SumInsured, ");
              tStrBSql.append(" Amnt EffectiveSumInsured, ");
              tStrBSql.append(" nvl((select prem from lpcont where contno=a.ContNo and edorno=(select min(edorno) from lpcont where contno=a.ContNo)),a.prem) Premium, ");
//             31 当前保费
              tStrBSql.append(" Prem CurrentPremium, ");
              tStrBSql.append(" Payintv PaymentMethod, ");
              tStrBSql.append(" (select payyears from LBpol where contno = a.ContNo and polno = mainpolno fetch first 1 rows only ) PaymentYears, ");
              tStrBSql.append(" (select count(distinct payno) from ljapayperson where contno = a.contno) PaymentNo, ");
              tStrBSql.append(" '2' PolicyHolderPro, ");
              tStrBSql.append(" (select count(1) from LBinsured where contno=a.contno) PolicyHolderNum,");
              tStrBSql.append(" (select count(1) from LBinsured where contno=a.contno) EffectiveInsuredNum, ");
              tStrBSql.append(" a.ContNo FormerpolicyNo, ");
              tStrBSql.append(" ReMark SpecialRemark, ");
              tStrBSql.append(" '' RegularClearingMark, ");
//            41 定期结算方式
              tStrBSql.append(" '' RegularClearing, ");
              tStrBSql.append(" '' RegularClearingDate, ");
              tStrBSql.append(" SignDate PremiumDueDate, ");
              tStrBSql.append(" '0' RealTimeClaimFlag, ");
              tStrBSql.append(" '' PolicyLoan, ");//是否存在保单借款
              tStrBSql.append(" '' AutoPaidUp, ");//是否自动垫交
              tStrBSql.append(" '0' CoInsurance, ");//是否共保
              tStrBSql.append(" '' LeadCoInsurance, ");//是否主共保 --48
              tStrBSql.append(" SignDate AcceptDate, ");//增加保单出单日期
              tStrBSql.append(" '000085110000' OrganId, ");//增加保单所属机构代码
              tStrBSql.append(" PolApplyDate ApplicationDate,");//增加 保单投保日期 
              //退保金
              tStrBSql.append("( case when a.payintv='1'  then prem*12 when a.payintv='3'  then prem*4  when a.payintv='6'  then prem*2 when a.payintv='12' then prem   else 0 end )  YearPremium");//当前年化保费
              
              if(mEdorType.equals("XT")||mEdorType.equals("CT")||mEdorType.equals("WT"))
              {
            	  tStrBSql.append(", (select abs(nvl(sum(getmoney),0)) from ljagetendorse where feeoperationtype in ('CT','XT') and feefinatype in ('TF','TB') and contno= a.contno) SurrenderCharge ");
              }//modify by zhangyige 2012-12-28 退保金加上绝对值
              tStrBSql.append(" from LBCont a ");
              tStrBSql.append(" where ContNo = '" + mContractNo + "' ");
        }
        else //团单
        {
        	tStrBSql.append(" select ");
            //1 是否拒保
              tStrBSql.append(" UWFlag Rejection, "); 
              tStrBSql.append(" PrtNo ApplicationFormNo, ");
              tStrBSql.append(" a.GrpContNo PolicyNo, ");
              tStrBSql.append(" '"+ mEdorType + "' EndorsementType, ");
              tStrBSql.append(" '"+ mEdorNo + "' EndorsementNo, ");
              tStrBSql.append(" '"+ mEdorAppDate + "' EndorsementApplicationDate, ");
              tStrBSql.append(" '"+ mEdorValiDate + "' EffectiveDate, ");
              tStrBSql.append(" '中国人民健康保险股份有限公司北京分公司' OrganName, ");
              tStrBSql.append(" Salechnl ChannelType, ");
              tStrBSql.append(" Salechnl SalesType, ");
            //11. 销售人员代码
              tStrBSql.append(" (select qualifno 资格证号 from LAQUALIFICATION where agentcode = a.AgentCode) SalesCode, "); 
              tStrBSql.append(" (select name from laagent where agentcode=a.agentcode) SalesName, ");
              tStrBSql.append(" agentcom SalesChannelCode, ");
              tStrBSql.append(" (select name from lacom  where agentcom=a.agentcom) SalesChannelName, ");
              tStrBSql.append(" (select address  from lacom  where agentcom=a.agentcom) BusinessAddress, ");
              tStrBSql.append(" '2' ContractType, ");
              tStrBSql.append(" CValidate PolicyStartDate, ");
              tStrBSql.append(" a.CInvalidate PolicyEndDate, ");
              if(mEdorType.equals("XT")||mEdorType.equals("CT")||mEdorType.equals("WT"))
              {
            	  tStrBSql.append(" '"+mEdorValiDate + "' TerminationDate, ");
              }
              else
              {
            	  tStrBSql.append("a.CInvalidate TerminationDate, ");
              }
              tStrBSql.append(" (select max(StartDate) from LCGrpContState where GrpContNo = a.GrpContNo and GrpPolNo = '000000' and StateType = 'Available') SuspendDate, ");
//            21 保单效力恢复日期
              if(mEdorType.equals("FX"))
              {
            	  tStrBSql.append(" '"+ mEdorValiDate + "' RecoverDate, ");
              }
              else
              {
            	  tStrBSql.append(" '' RecoverDate, ");
              }
              tStrBSql.append(" '01' ContractSource, ");
              tStrBSql.append(" a.GrpContNo ContractNo, ");
              tStrBSql.append(" (select count(distinct Newgrpcontno) from LCRNewStateLog where GrpContNo = a.GrpContNo) RenewalTimes, ");
              tStrBSql.append(" '' RenewalMethod, ");//modify by zhangyige 2013-01-04
              tStrBSql.append(" StateFlag ContractStatus, ");
              tStrBSql.append(" (select max(StateReaSon) from LCGrpContState where GrpContNo = a.GrpContNo and GrpPolNo = '000000' and StateType = 'Terminate') TerminationReason, ");
              tStrBSql.append(" nvl((select amnt from lpgrpcont where grpcontno=a.GrpContNo and edorno=(select min(edorno) from lpgrpcont where grpcontno=a.GrpContNo)),a.amnt) SumInsured, ");
              tStrBSql.append(" Amnt EffectiveSumInsured, ");
              tStrBSql.append(" nvl((select prem from lpgrpcont where grpcontno=a.GrpContNo and edorno=(select min(edorno) from lpgrpcont where grpcontno=a.GrpContNo)),a.prem) Premium, ");
//             31 当前保费
              tStrBSql.append(" Prem CurrentPremium, ");
              tStrBSql.append(" Payintv PaymentMethod, ");
              tStrBSql.append(" (select payyears from lcpol where grpcontno = '"+mContractNo+"' fetch first 1 rows only ) PaymentYears, ");
              tStrBSql.append(" (select count(distinct payno) from LJAPayGrp where GrpContNo = a.GrpContNo) PaymentNo, ");
              tStrBSql.append(" '1' PolicyHolderPro, ");
              tStrBSql.append(" (select count(1) from lcinsured where GrpContNo=a.GrpContNo) PolicyHolderNum,");
              tStrBSql.append(" (select count(1) from lcinsured where GrpContNo=a.GrpContNo) EffectiveInsuredNum, ");
              tStrBSql.append(" a.GrpContNo FormerpolicyNo, ");
              tStrBSql.append(" ReMark SpecialRemark, ");
              tStrBSql.append(" '' RegularClearingMark, ");
//            41 定期结算方式
              tStrBSql.append(" '' RegularClearing, ");
              tStrBSql.append(" '' RegularClearingDate, ");
              tStrBSql.append(" SignDate PremiumDueDate, ");
              tStrBSql.append(" '0' RealTimeClaimFlag, ");
              tStrBSql.append(" '' PolicyLoan, ");//是否存在保单借款
              tStrBSql.append(" '' AutoPaidUp, ");//是否自动垫交
              tStrBSql.append(" '0' CoInsurance, ");//是否共保
              tStrBSql.append(" '' LeadCoInsurance, ");//是否主共保 --48
              tStrBSql.append(" SignDate AcceptDate, ");//增加保单出单日期
              tStrBSql.append(" '000085110000' OrganId, ");//增加保单所属机构代码
              tStrBSql.append(" PolApplyDate ApplicationDate ,");//增加 保单投保日期 
              
              tStrBSql.append("( case when a.payintv='1'  then prem*12 when a.payintv='3'  then prem*4  when a.payintv='6'  then prem*2 when a.payintv='12' then prem   else 0 end )  YearPremium");//当前年化保费
              if(mEdorType.equals("XT")||mEdorType.equals("CT")||mEdorType.equals("WT"))
              {
            	  tStrBSql.append(", (select abs(nvl(sum(getmoney),0)) from ljagetendorse where feeoperationtype in ('CT','XT') and feefinatype in ('TF','TB') and a.grpcontno = grpcontno) SurrenderCharge ");
              }//modify by zhangyige 2012-12-28 退保金加上绝对值
              
              tStrBSql.append(" from LCGrpCont a ");
              tStrBSql.append(" where GrpContNo = '" + mContractNo + "' ");
  //----------------                
              tStrBSql.append(" union all ");
  //----------------   
              tStrBSql.append(" select ");
              //1 是否拒保
                tStrBSql.append(" UWFlag Rejection, "); 
                tStrBSql.append(" PrtNo ApplicationFormNo, ");
                tStrBSql.append(" a.GrpContNo PolicyNo, ");
                tStrBSql.append(" '"+ mEdorType + "' EndorsementType, ");
                tStrBSql.append(" '"+ mEdorNo + "' EndorsementNo, ");
                tStrBSql.append(" '"+ mEdorAppDate + "' EndorsementApplicationDate, ");
                tStrBSql.append(" '"+ mEdorValiDate + "' EffectiveDate, ");
                tStrBSql.append(" '中国人民健康保险股份有限公司北京分公司' OrganName, ");
                tStrBSql.append(" Salechnl ChannelType, ");
                tStrBSql.append(" Salechnl SalesType, ");
              //11. 销售人员代码
                tStrBSql.append(" (select qualifno 资格证号 from LAQUALIFICATION where agentcode = a.AgentCode) SalesCode, "); 
                tStrBSql.append(" (select name from laagent where agentcode=a.agentcode) SalesName, ");
                tStrBSql.append(" agentcom SalesChannelCode, ");
                tStrBSql.append(" (select name from lacom  where agentcom=a.agentcom) SalesChannelName, ");
                tStrBSql.append(" (select address  from lacom  where agentcom=a.agentcom) BusinessAddress, ");
                tStrBSql.append(" '2' ContractType, ");
                tStrBSql.append(" CValidate PolicyStartDate, ");
                tStrBSql.append(" a.CInvalidate PolicyEndDate, ");
                if(mEdorType.equals("XT")||mEdorType.equals("CT")||mEdorType.equals("WT"))
                {
              	  tStrBSql.append(" '"+mEdorValiDate + "' TerminationDate, ");
                }
                else
                {
              	  tStrBSql.append("a.CInvalidate TerminationDate, ");
                }
                tStrBSql.append(" (select max(StartDate) from LCGrpContState where GrpContNo = a.GrpContNo and GrpPolNo = '000000' and StateType = 'Available') SuspendDate, ");
//              21 保单效力恢复日期
                if(mEdorType.equals("FX"))
                {
              	  tStrBSql.append(" '"+ mEdorValiDate + "' RecoverDate, ");
                }
                else
                {
              	  tStrBSql.append(" '' RecoverDate, ");
                }
                tStrBSql.append(" '01' ContractSource, ");
                tStrBSql.append(" a.GrpContNo ContractNo, ");
                tStrBSql.append(" (select count(distinct Newgrpcontno) from LCRNewStateLog where GrpContNo = a.GrpContNo) RenewalTimes, ");
                tStrBSql.append(" '' RenewalMethod, ");//modify by zhangyige 2013-01-04
                tStrBSql.append(" StateFlag ContractStatus, ");
                tStrBSql.append(" (select max(StateReaSon) from LCGrpContState where GrpContNo = a.GrpContNo and GrpPolNo = '000000' and StateType = 'Terminate') TerminationReason, ");
                tStrBSql.append(" nvl((select amnt from lpgrpcont where grpcontno=a.GrpContNo and edorno=(select min(edorno) from lpgrpcont where grpcontno=a.GrpContNo)),a.amnt) SumInsured, ");
                tStrBSql.append(" Amnt EffectiveSumInsured, ");
                tStrBSql.append(" nvl((select prem from lpgrpcont where grpcontno=a.GrpContNo and edorno=(select min(edorno) from lpgrpcont where grpcontno=a.GrpContNo)),a.prem) Premium, ");
//               31 当前保费
                tStrBSql.append(" Prem CurrentPremium, ");
                tStrBSql.append(" Payintv PaymentMethod, ");
                tStrBSql.append(" (select payyears from lbpol where grpcontno = '"+mContractNo+"' fetch first 1 rows only ) PaymentYears, ");
                tStrBSql.append(" (select count(distinct payno) from LJAPayGrp where GrpContNo = a.GrpContNo) PaymentNo, ");
                tStrBSql.append(" '1' PolicyHolderPro, ");
                tStrBSql.append(" (select count(1) from lBinsured where GrpContNo=a.GrpContNo) PolicyHolderNum,");
                tStrBSql.append(" (select count(1) from lBinsured where GrpContNo=a.GrpContNo) EffectiveInsuredNum, ");
                tStrBSql.append(" a.GrpContNo FormerpolicyNo, ");
                tStrBSql.append(" ReMark SpecialRemark, ");
                tStrBSql.append(" '' RegularClearingMark, ");
//              41 定期结算方式
                tStrBSql.append(" '' RegularClearing, ");
                tStrBSql.append(" '' RegularClearingDate, ");
                tStrBSql.append(" SignDate PremiumDueDate, ");
                tStrBSql.append(" '0' RealTimeClaimFlag, ");
                tStrBSql.append(" '' PolicyLoan, ");//是否存在保单借款
                tStrBSql.append(" '' AutoPaidUp, ");//是否自动垫交
                tStrBSql.append(" '0' CoInsurance, ");//是否共保
                tStrBSql.append(" '' LeadCoInsurance, ");//是否主共保 --48
                tStrBSql.append(" SignDate AcceptDate, ");//增加保单出单日期
                tStrBSql.append(" '000085110000' OrganId, ");//增加保单所属机构代码
                tStrBSql.append(" PolApplyDate ApplicationDate ,");//增加 保单投保日期 
                
                tStrBSql.append("( case when a.payintv='1'  then prem*12 when a.payintv='3'  then prem*4  when a.payintv='6'  then prem*2 when a.payintv='12' then prem   else 0 end )  YearPremium");//当前年化保费
                
                if(mEdorType.equals("XT")||mEdorType.equals("CT")||mEdorType.equals("WT"))
                {
              	  tStrBSql.append(" ,(select abs(nvl(sum(getmoney),0)) from ljagetendorse where feeoperationtype in ('CT','XT') and feefinatype in ('TF','TB') and a.grpcontno = grpcontno) SurrenderCharge ");
                }//modify by zhangyige 2012-12-28 退保金加上绝对值
                
                tStrBSql.append(" from LBGrpCont a ");
                tStrBSql.append(" where GrpContNo = '" + mContractNo + "' ");
        }

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }
// 承保 ’险种’ 数组
    private Element reportPolicyUpcoverageLogInfo(String cContNo)
    {
        Element tRoot = new Element("PolicyUpcoverageLogDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.PolicyUpcoverageLogDTO");

        Element tTmpEle = null;

        SSRS tResPolicyUpcoverageLogInfo = loadPolicyUpcoverageLogInfo(cContNo);

        for (int idxPI = 1; idxPI <= tResPolicyUpcoverageLogInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyUpcoverageLogInfo.getRowData(idxPI);

            tTmpEle = new Element("CoverageType");
            tTmpEle.setText(tDataInfo[0]);
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageCode");
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ComCoverageCode");
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageName");
            tTmpEle.setText(tDataInfo[3]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageEffectiveDate");
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageExpireDate");
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("MainAttachedFlag");
            tTmpEle.setText(tDataInfo[6]);
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoveragePremium");
            tTmpEle.setText(tDataInfo[7]);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为“NUMBER”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("CoverageCurrentPremium");
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为“NUMBER”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageSuminsured");
            tTmpEle.setText(tDataInfo[9]);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为“NUMBER”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageEffectiveSuminsured");
            tTmpEle.setText(tDataInfo[10]);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为“NUMBER”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("PaymentMethod");
            tTmpEle.setText(tDataInfo[11]);
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("PaymentYears");
            tTmpEle.setText(tDataInfo[12]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("PaymentNo");
            tTmpEle.setText(tDataInfo[13]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SpecificBusiness");
            tTmpEle.setText(tDataInfo[14]);
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SpecificBusinessCode");
            tTmpEle.setText(tDataInfo[15]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("CoverageStatus");
            tTmpEle.setText(tDataInfo[16]);
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
//          获取险种下的责任信息
            String tPolNo = tDataInfo[tDataInfo.length-1];       
            
 // 险种期初日额保险金
            
            tTmpEle = new Element("coveragedayAmount"); 
            tTmpEle.setText(tDataInfo[18]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            //险种有效日额保险金
            
            tTmpEle = new Element("coveragedayEffectiveAmount"); 
            tTmpEle.setText(tDataInfo[19]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            //险种当前年化保费
            
            tTmpEle = new Element("coverageyearPremium"); 
            tTmpEle.setText(tDataInfo[20]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            //保费来源
            tTmpEle = new Element("premiumSource");
            tTmpEle.setText(tDataInfo[21]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            //退保金
            tTmpEle = new Element("coverageSurrenderCharge");
            tTmpEle.setText(tDataInfo[22]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            
            
            
            tTmpEle = reportPolicyCoverageLogInfo(tPolNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
            
//          核保结论                                    
            tTmpEle = reportUnderWritingLogInfo(tPolNo);
            if (tTmpEle != null)                        
            {                                           
                tItemEle.addContent(tTmpEle);           
            }                                           
            tTmpEle = null; 
        }

        return tRoot;
    }
    
//  承保 ’责任’ 数组
    private Element reportPolicyCoverageLogInfo(String cPolNo)
    {
        Element tRoot = new Element("PolicyCoverageLogDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.PolicyCoverageLogDTO");

        Element tTmpEle = null;

        SSRS tResPolicyCoverageLogInfo = loadPolicyCoverageLogInfo(cPolNo);

        for (int idxPI = 1; idxPI <= tResPolicyCoverageLogInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyCoverageLogInfo.getRowData(idxPI);

            tTmpEle = new Element("LiabilityClassification");
            tTmpEle.setText(tDataInfo[0]);
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityCode");
            tTmpEle.setText(tDataInfo[1]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityName");
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityEffectiveDate");
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityExpireDate");
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityStatus");
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityPremium");
            tTmpEle.setText(tDataInfo[6]);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为“NUMBER”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("LiabilityCurrentPremium");
            tTmpEle.setText(tDataInfo[7]);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为“NUMBER”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LimitAmount");
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为“NUMBER”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("EffectivelyAmount");
            tTmpEle.setText(tDataInfo[9]);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为“NUMBER”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("WaitingPeriod");
            tTmpEle.setText(tDataInfo[10]);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为“NUMBER”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("InsuredGroupCode");
            tTmpEle.setText(tDataInfo[11]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("GroupName");
            tTmpEle.setText(tDataInfo[12]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("liabilityDayAmount"); //责任期初日额保险金    
            tTmpEle.setText(tDataInfo[13]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("liabilitydayEffectiveAmount"); //责任有效日额保险金 
            tTmpEle.setText(tDataInfo[14]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            
//          赔付比例
            tTmpEle = reportPolicyAmountLogDTOInfo(cPolNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

        }

        return tRoot;
    }
       

//  承保 ’险种’ 数组
    private SSRS loadPolicyUpcoverageLogInfo(String cContNo)
    {
    	
    	String tContNo = cContNo;
    	
        SSRS tResult = null;
        StringBuffer tStrBSql = new StringBuffer();

        if(mEdorProp!=null&&mEdorProp.equals("I"))
        {  
	        tStrBSql.append(" select ");
	        tStrBSql.append(" lmra.RiskPeriod CoverageType, ");
	        tStrBSql.append(" lmra.RiskCode CoverageCode, ");
	        tStrBSql.append(" lcp.RiskCode ComCoverageCode, ");
	        tStrBSql.append(" lmra.RiskName CoverageName, ");
	        tStrBSql.append(" lcp.cvalidate CoverageEffectiveDate,  ");
	        tStrBSql.append(" lcp.enddate CoverageExpireDate, ");
	        tStrBSql.append(" lmra.SubRiskFlag MainAttachedFlag, ");
	        tStrBSql.append(" nvl((select prem from lppol where polno=lcp.polno and edorno=(select min(edorno) from lppol where polno=lcp.polno)),lcp.prem) CoveragePremium,  ");
	        tStrBSql.append(" lcp.prem CoverageCurrentPremium,  ");
	        tStrBSql.append(" nvl((select amnt from lppol where polno=lcp.polno and edorno=(select min(edorno) from lppol where polno=lcp.polno)),lcp.amnt)  CoverageSuminsured,  ");
	        tStrBSql.append(" lcp.amnt CoverageEffectiveSuminsured,  ");
	        tStrBSql.append(" lcp.PayIntv PaymentMethod, ");
	        tStrBSql.append(" (Case when lcp.PayIntv is null then '' else char(lcp.PayYears) end) PaymentYears, ");
	        tStrBSql.append(" (select count(distinct payno) from ljapayperson where polno = lcp.PolNo ) PaymentNo, "); 
	        tStrBSql.append(" '0' SpecificBusiness,  ");
	        tStrBSql.append(" '' SpecificBusinessCode, "); 
	        tStrBSql.append(" lcp.StateFlag CoverageStatus, "); 
	        tStrBSql.append(" lcp.polno polno,");  
	        
	        tStrBSql.append("(select case when lc.mult='1' then 50 when lc.mult='2' then 100 when lc.mult='3' then 150 else 0 end from lcduty lc where lcp.polno = polno and lcp.riskcode in ('1201','1601') fetch first 1 rows only) CoveragedayAmount,"); //险种期初日额保险金
            tStrBSql.append("(select case when lc.mult='1' then 50 when lc.mult='2' then 100 when lc.mult='3' then 150 else 0 end from lcduty lc where lcp.polno = polno and lcp.riskcode in ('1201','1601') fetch first 1 rows only) CoveragedayEffectiveAmount,");//险种有效日额保险金
            
            if(mEdorProp.equals("I"))
            {//个单
            	tStrBSql.append("( case when lcp.payintv='1'  then prem*12 when lcp.payintv='3'  then prem*4  when lcp.payintv='6'  then prem*2 when lcp.payintv='12' then prem   else 0 end ) CoverageyearPremium,");//险种当前年化保费
            	tStrBSql.append("'01' PremiumSource");//保费来源
            }else{
            	tStrBSql.append("( case when lcp.payintv='1'  then prem*12 when lcp.payintv='3'  then prem*4  when lcp.payintv='6'  then prem*2 when lcp.payintv='12' then prem   else 0 end ) CoverageyearPremium,");//险种当前年化保费
            	tStrBSql.append("'02' PremiumSource");//保费来源
            }
            //退保金
            if(mEdorType.equals("XT")||mEdorType.equals("CT")||mEdorType.equals("WT"))
            {
          	  tStrBSql.append(" ,(select abs(nvl(sum(getmoney),0)) from ljagetendorse where feeoperationtype in ('CT','XT') and feefinatype in ('TF','TB') and lcp.polno = polno) CoverageSurrenderCharge ");
            }
	        tStrBSql.append(" from LCPol lcp ");
	        tStrBSql.append(" inner join LCDuty lcd on lcd.PolNo = lcp.PolNo ");
	        tStrBSql.append(" inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode ");
	        tStrBSql.append(" inner join LMDuty lmd on lmd.DutyCode = lcd.DutyCode ");
	        tStrBSql.append(" where 1 = 1 ");
	        tStrBSql.append(" and lcp.ContNo = '" + tContNo + "' ");
	        
	        tStrBSql.append(" union all  ");
	        
	        tStrBSql.append(" select ");
	        tStrBSql.append(" lmra.RiskPeriod CoverageType, ");
	        tStrBSql.append(" lmra.RiskCode CoverageCode, ");
	        tStrBSql.append(" lcp.RiskCode ComCoverageCode, ");
	        tStrBSql.append(" lmra.RiskName CoverageName, ");
	        tStrBSql.append(" lcp.cvalidate CoverageEffectiveDate,  ");
	        tStrBSql.append(" lcp.enddate CoverageExpireDate, ");
	        tStrBSql.append(" lmra.SubRiskFlag MainAttachedFlag, ");
	        tStrBSql.append(" nvl((select prem from lppol where polno=lcp.polno and edorno=(select min(edorno) from lppol where polno=lcp.polno)),lcp.prem) CoveragePremium,  ");
	        tStrBSql.append(" lcp.prem CoverageCurrentPremium,  ");
	        tStrBSql.append(" nvl((select amnt from lppol where polno=lcp.polno and edorno=(select min(edorno) from lppol where polno=lcp.polno)),lcp.amnt) CoverageSuminsured,  ");
	        tStrBSql.append(" lcp.amnt CoverageEffectiveSuminsured,  ");
	        tStrBSql.append(" lcp.PayIntv PaymentMethod, ");
	        tStrBSql.append(" (Case when lcp.PayIntv is null then '' else char(lcp.PayYears) end) PaymentYears, ");
	        tStrBSql.append(" (select count(distinct payno) from ljapayperson where polno = lcp.PolNo ) PaymentNo, "); 
	        tStrBSql.append(" '0' SpecificBusiness,  ");
	        tStrBSql.append(" '' SpecificBusinessCode, "); 
	        tStrBSql.append(" lcp.StateFlag CoverageStatus, "); 
	        tStrBSql.append(" lcp.polno polno "); 
	        
	        tStrBSql.append("(select case when lc.mult='1' then 50 when lc.mult='2' then 100 when lc.mult='3' then 150 else 0 end from lbduty lc where lcp.polno = polno and lcp.riskcode in ('1201','1601') fetch first 1 rows only) CoveragedayAmount,"); //险种期初日额保险金
            tStrBSql.append("(select case when lc.mult='1' then 50 when lc.mult='2' then 100 when lc.mult='3' then 150 else 0 end from lbduty lc where lcp.polno = polno and lcp.riskcode in ('1201','1601') fetch first 1 rows only) CoveragedayEffectiveAmount,");//险种有效日额保险金
            
            if(mEdorProp.equals("I"))
            {//个单
            	tStrBSql.append("( case when lcp.payintv='1'  then prem*12 when lcp.payintv='3'  then prem*4  when lcp.payintv='6'  then prem*2 when lcp.payintv='12' then prem   else 0 end ) CoverageyearPremium,");//险种当前年化保费
            	tStrBSql.append("'01' PremiumSource");//保费来源
            }else{
            	tStrBSql.append("( case when lcp.payintv='1'  then prem*12 when lcp.payintv='3'  then prem*4  when lcp.payintv='6'  then prem*2 when lcp.payintv='12' then prem   else 0 end ) CoverageyearPremium,");//险种当前年化保费
            	tStrBSql.append("'02' PremiumSource");//保费来源
            }
            //退保金
            if(mEdorType.equals("XT")||mEdorType.equals("CT")||mEdorType.equals("WT"))
            {
          	  tStrBSql.append(" ,(select abs(nvl(sum(getmoney),0)) from ljagetendorse where feeoperationtype in ('CT','XT') and feefinatype in ('TF','TB') and lcp.polno = polno) CoverageSurrenderCharge ");
            }
	        tStrBSql.append(" from LBPol lcp ");
	        tStrBSql.append(" inner join LBDuty lcd on lcd.PolNo = lcp.PolNo ");
	        tStrBSql.append(" inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode ");
	        tStrBSql.append(" inner join LMDuty lmd on lmd.DutyCode = lcd.DutyCode ");
	        tStrBSql.append(" where 1 = 1 ");
	        tStrBSql.append(" and lcp.ContNo = '" + tContNo + "' ");
        }
        else
        {
        	tStrBSql=new StringBuffer();
        	tStrBSql.append(" select ");
	        tStrBSql.append(" lmra.RiskPeriod CoverageType, ");
	        tStrBSql.append(" lmra.RiskCode CoverageCode, ");
	        tStrBSql.append(" lcp.RiskCode ComCoverageCode, ");
	        tStrBSql.append(" lmra.RiskName CoverageName, ");
	        tStrBSql.append(" lcp.cvalidate CoverageEffectiveDate,  ");
	        tStrBSql.append(" lcp.payenddate CoverageExpireDate,  ");
	        tStrBSql.append(" lmra.SubRiskFlag MainAttachedFlag, ");
	        tStrBSql.append(" nvl((select prem from lpgrppol where grpcontno=lcp.GrpContNo and edorno=(select min(edorno) from lpgrppol where grpcontno=lcp.GrpContNo)),lcp.prem) CoveragePremium,  ");
	        tStrBSql.append(" lcp.prem CoverageCurrentPremium,  ");
	        tStrBSql.append(" nvl((select amnt from lpgrppol where grpcontno=lcp.GrpContNo and edorno=(select min(edorno) from lpgrppol where grpcontno=lcp.GrpContNo)),lcp.amnt) CoverageSuminsured,  ");
	        tStrBSql.append(" lcp.amnt CoverageEffectiveSuminsured,  ");
	        tStrBSql.append(" lcp.PayIntv PaymentMethod, ");
	        tStrBSql.append(" (select payyears from lcpol where Grppolno = lcp.Grppolno fetch first 1 rows only ) PaymentYears, ");
	        tStrBSql.append(" (select count(distinct payno) from ljapayGrp where Grppolno = lcp.Grppolno ) PaymentNo, "); 
	        tStrBSql.append(" '0' SpecificBusiness,  ");
	        tStrBSql.append(" '' SpecificBusinessCode, "); 
	        tStrBSql.append(" lcp.grppolno polno ");  
	        tStrBSql.append("(select case when lc.mult='1' then 50 when lc.mult='2' then 100 when lc.mult='3' then 150 else 0 end from lcpol lc where lc.riskcode in('1201','1601') and lc.grpcontno= '" + cContNo + "' fetch first 1 rows only) CoveragedayAmount,"); //险种期初日额保险金
            tStrBSql.append("(select case when lc.mult='1' then 50 when lc.mult='2' then 100 when lc.mult='3' then 150 else 0 end from lcpol lc where lc.riskcode in('1201','1601') and lc.grpcontno= '" + cContNo + "' fetch first 1 rows only) CoveragedayEffectiveAmount,");//险种有效日额保险金
            
            if(mEdorProp.equals("I"))
            {//个单
            	tStrBSql.append("( case when lcp.payintv='1'  then prem*12 when lcp.payintv='3'  then prem*4  when lcp.payintv='6'  then prem*2 when lcp.payintv='12' then prem   else 0 end ) CoverageyearPremium,");//险种当前年化保费
            	tStrBSql.append("'01' PremiumSource");//保费来源
            }else{
            	tStrBSql.append("( case when lcp.payintv='1'  then prem*12 when lcp.payintv='3'  then prem*4  when lcp.payintv='6'  then prem*2 when lcp.payintv='12' then prem   else 0 end ) CoverageyearPremium,");//险种当前年化保费
            	tStrBSql.append("'02' PremiumSource");//保费来源
            }
            //退保金
            if(mEdorType.equals("XT")||mEdorType.equals("CT")||mEdorType.equals("WT"))
            {
          	  tStrBSql.append(" ,(select abs(nvl(sum(getmoney),0)) from ljagetendorse where feeoperationtype in ('CT','XT') and feefinatype in ('TF','TB') and lcp.grppolno=grppolno) CoverageSurrenderCharge ");
            }
	        
	        tStrBSql.append(" from LCgrpPol lcp ");
	        tStrBSql.append(" inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode ");
	        tStrBSql.append(" where 1 = 1 ");
	        tStrBSql.append(" and lcp.GrpContNo = '" + tContNo + "' ");
	        
	        tStrBSql.append(" union all  ");
	        tStrBSql.append(" select ");
	        tStrBSql.append(" lmra.RiskPeriod CoverageType, ");
	        tStrBSql.append(" lmra.RiskCode CoverageCode, ");
	        tStrBSql.append(" lcp.RiskCode ComCoverageCode, ");
	        tStrBSql.append(" lmra.RiskName CoverageName, ");
	        tStrBSql.append(" lcp.cvalidate CoverageEffectiveDate,  ");
	        tStrBSql.append(" lcp.payenddate CoverageExpireDate,  ");
	        tStrBSql.append(" lmra.SubRiskFlag MainAttachedFlag, ");
	        tStrBSql.append(" nvl((select prem from lpgrppol where grpcontno=lcp.GrpContNo and edorno=(select min(edorno) from lpgrppol where grpcontno=lcp.GrpContNo)),lcp.prem) CoveragePremium,  ");
	        tStrBSql.append(" lcp.prem CoverageCurrentPremium,  ");
	        tStrBSql.append(" nvl((select amnt from lpgrppol where grpcontno=lcp.GrpContNo and edorno=(select min(edorno) from lpgrppol where grpcontno=lcp.GrpContNo)),lcp.amnt) CoverageSuminsured,  ");
	        tStrBSql.append(" lcp.amnt CoverageEffectiveSuminsured,  ");
	        tStrBSql.append(" lcp.PayIntv PaymentMethod, ");
	        tStrBSql.append(" (select payyears from lbpol where Grppolno = lcp.Grppolno fetch first 1 rows only ) PaymentYears, ");
	        tStrBSql.append(" (select count(distinct payno) from ljapayGrp where Grppolno = lcp.Grppolno ) PaymentNo, "); 
	        tStrBSql.append(" '0' SpecificBusiness,  ");
	        tStrBSql.append(" '' SpecificBusinessCode, "); 
	        tStrBSql.append(" lcp.grppolno polno ");  
	        
	        tStrBSql.append("(select case when lc.mult='1' then 50 when lc.mult='2' then 100 when lc.mult='3' then 150 else 0 end from lbpol lc where lc.riskcode in('1201','1601') and lc.grpcontno= '" + cContNo + "' fetch first 1 rows only) CoveragedayAmount,"); //险种期初日额保险金
            tStrBSql.append("(select case when lc.mult='1' then 50 when lc.mult='2' then 100 when lc.mult='3' then 150 else 0 end from lbpol lc where lc.riskcode in('1201','1601') and lc.grpcontno= '" + cContNo + "' fetch first 1 rows only) CoveragedayEffectiveAmount,");//险种有效日额保险金
            
            if(mEdorProp.equals("I"))
            {//个单
            	tStrBSql.append("( case when lcp.payintv='1'  then prem*12 when lcp.payintv='3'  then prem*4  when lcp.payintv='6'  then prem*2 when lcp.payintv='12' then prem   else 0 end ) CoverageyearPremium,");//险种当前年化保费
            	tStrBSql.append("'01' PremiumSource");//保费来源
            }else{
            	tStrBSql.append("( case when lcp.payintv='1'  then prem*12 when lcp.payintv='3'  then prem*4  when lcp.payintv='6'  then prem*2 when lcp.payintv='12' then prem   else 0 end ) CoverageyearPremium,");//险种当前年化保费
            	tStrBSql.append("'02' PremiumSource");//保费来源
            }
            //退保金
            if(mEdorType.equals("XT")||mEdorType.equals("CT")||mEdorType.equals("WT"))
            {
          	  tStrBSql.append(" ,(select abs(nvl(sum(getmoney),0)) from ljagetendorse where feeoperationtype in ('CT','XT') and feefinatype in ('TF','TB') and lcp.grppolno=grppolno) CoverageSurrenderCharge ");
            }
	        tStrBSql.append(" from LBgrpPol lcp ");
	        tStrBSql.append(" inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode ");
	        tStrBSql.append(" where 1 = 1 ");
	        tStrBSql.append(" and lcp.GrpContNo = '" + tContNo + "' ");
        }

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }
    
    //承保 ’责任’ 数组
    private SSRS loadPolicyCoverageLogInfo(String cPolNo)
    {
    	
    	String tPolNo = cPolNo;
    	
        SSRS tResult = null;
        StringBuffer tStrBSql = new StringBuffer();
        
        tStrBSql.append(" select ");               
        tStrBSql.append(" lcd.DutyCode LiabilityClassification, ");
        tStrBSql.append(" lcd.DutyCode LiabilityCode, lmd.DutyName LiabilityName, ");
        tStrBSql.append(" lcd.GetStartDate LiabilityEffectiveDate, (lcd.EndDate - 1 day) LiabilityExpireDate, ");
        tStrBSql.append(" '1' LiabilityStatus, ");        
        tStrBSql.append(" nvl((select prem from lpduty where polno=lcp.PolNo and edorno=(select min(edorno) from lpduty where polno=lcp.PolNo)),lcd.prem)  LiabilityPremium, "); 
        tStrBSql.append(" lcd.prem LiabilityCurrentPremium, "); 
        tStrBSql.append(" nvl((select amnt from lpduty where polno=lcp.PolNo and edorno=(select min(edorno) from lpduty where polno=lcp.PolNo)),lcd.amnt) LimitAmount, lcd.Amnt EffectivelyAmount, ");
        tStrBSql.append(" '0' WaitingPeriod, ");
        tStrBSql.append(" '' InsuredGroupCode, ");
        tStrBSql.append(" '' GroupName, ");
//      责任期初日额保险金
        tStrBSql.append("(select case when lc.mult='1' then 50 when lc.mult='2' then 100 when lc.mult='3' then 150 else 0 end from lcduty lc where lcp.polno = polno and lcp.riskcode in ('1201','1601','1602','160308') fetch first 1 rows only) liabilityDayAmount,");
//      责任有效日额保险金
        tStrBSql.append("(select case when lc.mult='1' then 50 when lc.mult='2' then 100 when lc.mult='3' then 150 else 0 end from lcduty lc where lcp.polno = polno and lcp.riskcode in ('1201','1601','1602','160308') fetch first 1 rows only) liabilitydayEffectiveAmount");
        tStrBSql.append(" from LCPol lcp ");
        tStrBSql.append(" inner join LCDuty lcd on lcd.PolNo = lcp.PolNo ");
        tStrBSql.append(" inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode ");
        tStrBSql.append(" inner join LMDuty lmd on lmd.DutyCode = lcd.DutyCode ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.PolNo = '" + tPolNo + "' ");
        tStrBSql.append(" union all  ");
        tStrBSql.append(" select ");               
        tStrBSql.append(" lcd.DutyCode LiabilityClassification, ");
        tStrBSql.append(" lcd.DutyCode LiabilityCode, lmd.DutyName LiabilityName, ");
        tStrBSql.append(" lcd.GetStartDate LiabilityEffectiveDate, (lcd.EndDate - 1 day) LiabilityExpireDate, ");
        tStrBSql.append(" '1' LiabilityStatus, ");        
        tStrBSql.append(" nvl((select prem from lpduty where polno=lcp.PolNo and edorno=(select min(edorno) from lpduty where polno=lcp.PolNo)),lcd.prem) LiabilityPremium, ");
        tStrBSql.append(" lcp.prem LiabilityCurrentPremium, ");
        tStrBSql.append(" nvl((select amnt from lpduty where polno=lcp.PolNo and edorno=(select min(edorno) from lpduty where polno=lcp.PolNo)),lcd.amnt) LimitAmount, 0 EffectivelyAmount, ");
        tStrBSql.append(" '0' WaitingPeriod, ");
        
        tStrBSql.append(" '' InsuredGroupCode, ");
        tStrBSql.append(" '' GroupName ");
        
//      责任期初日额保险金
        tStrBSql.append("(select case when lc.mult='1' then 50 when lc.mult='2' then 100 when lc.mult='3' then 150 else 0 end from lbduty lc where lcp.polno = polno and lcp.riskcode in ('1201','1601','1602','160308') fetch first 1 rows only) liabilityDayAmount,");
//      责任有效日额保险金
        tStrBSql.append("(select case when lc.mult='1' then 50 when lc.mult='2' then 100 when lc.mult='3' then 150 else 0 end from lbduty lc where lcp.polno = polno and lcp.riskcode in ('1201','1601','1602','160308') fetch first 1 rows only) liabilitydayEffectiveAmount,");
        
        
        tStrBSql.append(" from LBPol lcp ");
        tStrBSql.append(" inner join LBDuty lcd on lcd.PolNo = lcp.PolNo ");
        tStrBSql.append(" inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode ");
        tStrBSql.append(" inner join LMDuty lmd on lmd.DutyCode = lcd.DutyCode ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.PolNo = '" + tPolNo + "' ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }
 
    //核保信息
    private Element reportUnderWritingLogInfo(String cPolNo)
    {
        Element tRoot = new Element("UnderWritingLogDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.UnderWritingLogDTO");

        Element tTmpEle = null;

        SSRS tResUnderWritingLogInfo = loadUnderWritingLogInfo(cPolNo);

        for (int idxPI = 1; idxPI <= tResUnderWritingLogInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResUnderWritingLogInfo.getRowData(idxPI);

            tTmpEle = new Element("UnderwritingResult");
            tTmpEle.setText(tDataInfo[0]);
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("UnderwritingDate");
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("DeclinedReason");
            if(tDataInfo[0].equals("1")){
            	tTmpEle.setText(tDataInfo[2]);//当核保结论为50-拒保时，拒保原因必须上传 默认为99 - 其他 modify by zhangyige 2012-12-28
            }else{
            	tTmpEle.setText("");//当核保结论为不为拒保时，拒保原因传成空 modify by zhangyige 2012-12-28
            }
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("State");
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("PhysicalExamination");
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
        }

        return tRoot;
    }
    

    private SSRS loadUnderWritingLogInfo(String cPolNo)
    {
    	
    	String tPolNo = cPolNo;
    	
        SSRS tResult = null;
        StringBuffer tStrBSql = new StringBuffer();
        
        tStrBSql.append(" select ");
        tStrBSql.append(" UWFlag UnderwritingResult, UWDate UnderwritingDate, ");
        tStrBSql.append(" '99' DeclinedReason, '1' State, ");
        tStrBSql.append(" case when exists (select 1 from LCPENotice where contno = lcp.contno) then '1' else '0' end PhysicalExamination ");
        tStrBSql.append(" from LCPol lcp ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.PolNo = '" + tPolNo + "' ");
        tStrBSql.append(" union all  ");
        tStrBSql.append(" select ");
        tStrBSql.append(" UWFlag UnderwritingResult, UWDate UnderwritingDate, ");
        tStrBSql.append(" '99' DeclinedReason, '1' State, ");
        tStrBSql.append(" case when exists (select 1 from LCPENotice where contno = lcp.contno) then '1' else '0' end PhysicalExamination ");
        tStrBSql.append(" from LBPol lcp ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.PolNo = '" + tPolNo + "' ");
        tStrBSql.append(" union all  ");
        tStrBSql.append(" select ");
        tStrBSql.append(" UWFlag UnderwritingResult, UWDate UnderwritingDate, ");
        tStrBSql.append(" '99' DeclinedReason, '1' State, ");
        tStrBSql.append(" case when exists (select 1 from LCPENotice where contno = lcp.contno) then '1' else '0' end PhysicalExamination ");
        tStrBSql.append(" from LPPol lcp ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.PolNo = '" + tPolNo + "' and lcp.EdorNo = '"+mEdorNo+"' ");
        tStrBSql.append(" and exists (select 1 from lpuwmaster where edorno=lcp.edorno)");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }
//给付比例
    private Element reportPolicyAmountLogDTOInfo(String cPolNo)
    {
        Element tRoot = new Element("PolicyAmountLogDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.PolicyAmountLogDTO");

        Element tTmpEle = null;

        for (int idxPI = 1; idxPI <= 1; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            tTmpEle = new Element("ClaimRatio");
            tTmpEle.setText(null);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为“NUMBER”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Costend");
            tTmpEle.setText(null);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为“NUMBER”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("Coststart");
            tTmpEle.setText(null);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为“NUMBER”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Deductible");
            tTmpEle.setText(null);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为“NUMBER”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("SectionNo");
            tTmpEle.setText(null);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
        }

        return tRoot;
    }
    
//  团体投保人数组
    private Element reportPolicyGroupPHLogInfo(String cGrpContNo)
    {
        Element tRoot = new Element("PolicyGroupPHLogDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.PolicyGroupPHLogDTO");

        Element tTmpEle = null;

        SSRS tResPolicyGroupPHLogInfo = loadPolicyGroupPHLogInfo(cGrpContNo);

        for (int idxPI = 1; idxPI <= tResPolicyGroupPHLogInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyGroupPHLogInfo.getRowData(idxPI);
            
            tTmpEle = new Element("Name");
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("Address1");
            tTmpEle.setText(tDataInfo[1]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("Address2");
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("Address3");
            tTmpEle.setText(QYJKXTwoServiceImp.getTransAddress(tDataInfo[3]));
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("Address4");
            if(tDataInfo[4].equals("")){
            	tTmpEle.setText("北京市西城区阜成门外大街7号国投大厦10层");
            }else{
            	tTmpEle.setText(tDataInfo[4]);
            }
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritType");
//            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tTmpEle.setText(tDataInfo[5]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritCode");
            tTmpEle.setText(tDataInfo[6]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SocialSecurityNo");
            tTmpEle.setText(tDataInfo[7]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CompanyNature");
            tTmpEle.setText(tDataInfo[8]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("IndustryClassification");
            tTmpEle.setText(tDataInfo[9]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LegalRepresentative");
            tTmpEle.setText(tDataInfo[10]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Contact");
            tTmpEle.setText(tDataInfo[11]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("NumberOfUnits");
            tTmpEle.setText(tDataInfo[12]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
        }

        return tRoot;
    }
    
    private SSRS loadPolicyGroupPHLogInfo(String cGrpContNo)
    {
    	
        SSRS tResult = null;
        StringBuffer tStrBSql = new StringBuffer();
        
        tStrBSql.append(" select ");
        tStrBSql.append(" Name Name, ");
      //zyg 默认置为 北京（省）市辖（区） 西城（县）
        tStrBSql.append(" '110000' Address1, ");
        tStrBSql.append(" '110100' Address2, ");
        tStrBSql.append(" PostalAddress Address3, ");
        tStrBSql.append(" PostalAddress Address4, ");
        tStrBSql.append(" (case when OrganComCode is not null then '2' else '9' end) CritType,  ");
        tStrBSql.append(" (case when OrganComCode is null then '999999999' else OrganComCode end) CritCode, '' SocialSecurityNo, ");
        tStrBSql.append(" '' CompanyNature, '' IndustryClassification, '' LegalRepresentative,'' Contact, ");
        tStrBSql.append(" Peoples NumberOfUnits ");
        tStrBSql.append(" from LCGrpAppnt lci");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and GrpContNo = '" + cGrpContNo + "' ");
        tStrBSql.append(" union all  ");
        tStrBSql.append(" select ");
        tStrBSql.append(" Name Name, ");
      //zyg 默认置为 北京（省）市辖（区） 西城（县）
        tStrBSql.append(" '110000' Address1, ");
        tStrBSql.append(" '110100' Address2, ");
        tStrBSql.append(" PostalAddress Address3, ");
        tStrBSql.append(" PostalAddress Address4, ");
        tStrBSql.append(" (case when OrganComCode is not null then '2' else '9' end) CritType, ");
        tStrBSql.append(" (case when OrganComCode is null then '999999999' else OrganComCode end) CritCode, '' SocialSecurityNo, ");
        tStrBSql.append(" '' CompanyNature, '' IndustryClassification, '' LegalRepresentative,'' Contact, ");
        tStrBSql.append(" Peoples NumberOfUnits ");
        tStrBSql.append(" from LBGrpAppnt lci");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and GrpContNo = '" + cGrpContNo + "' ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }
    
//  个人投保人数组
    private Element reportPolicyPersonPHLogInfo(String cContNo)
    {
        Element tRoot = new Element("PolicyPersonPHLogDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.PolicyPersonPHLogDTO");

        Element tTmpEle = null;

        SSRS tResPolicyGroupPHLogInfo = loadPolicyPersonPHLogInfo(cContNo);

        for (int idxPI = 1; idxPI <= tResPolicyGroupPHLogInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyGroupPHLogInfo.getRowData(idxPI);
            
            
            tTmpEle = new Element("Name");
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Gender");
            tTmpEle.addAttribute("CData", "CBusCode");
            tTmpEle.setText(tDataInfo[1]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Birthday");
            tTmpEle.addAttribute("CData", "CDate");
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritType");
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tTmpEle.setText(tDataInfo[3]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritCode");
            tTmpEle.setText(tDataInfo[4]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RelationshipWithInsured");
            tTmpEle.setText(tDataInfo[5]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

        }

        return tRoot;
    }
    
    private SSRS loadPolicyPersonPHLogInfo(String cContNo)
    {
    	
        SSRS tResult = null;
        StringBuffer tStrBSql = new StringBuffer();
        
        tStrBSql.append(" select ");
        tStrBSql.append(" AppntName Name, AppntSex Gender, ");
        tStrBSql.append(" AppntBirthday Birthday, IDType CritType, ");
        tStrBSql.append(" IDNo CritCode, '' RelationshipWithInsured ");
        tStrBSql.append(" from LCAppnt ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and ContNo = '" + cContNo + "' ");
        tStrBSql.append(" union all  ");
        tStrBSql.append(" select ");
        tStrBSql.append(" AppntName Name, AppntSex Gender, ");
        tStrBSql.append(" AppntBirthday Birthday, IDType CritType, ");
        tStrBSql.append(" IDNo CritCode, '' RelationshipWithInsured ");
        tStrBSql.append(" from LBAppnt ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and ContNo = '" + cContNo + "' ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }
    
//  被保人
    private Element reportPolicyInsuredLogInfo(String cContNo)
    {
        Element tRoot = new Element("PolicyInsuredLogDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.PolicyInsuredLogDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInsuredLogInfo = loadPolicyInsuredLogInfo(cContNo);

        for (int idxPI = 1; idxPI <= tResPolicyInsuredLogInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInsuredLogInfo.getRowData(idxPI);

            tTmpEle = new Element("Name");
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Gender");
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tTmpEle.setText(tDataInfo[1]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Birthday");
            tTmpEle.addAttribute("CData", "CDate");
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritType");
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tTmpEle.setText(tDataInfo[3]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritCode");
            tTmpEle.setText(tDataInfo[4]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RelationshipWithPh");
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tTmpEle.setText(tDataInfo[5]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CustomerNo");
            tTmpEle.setText(tDataInfo[6]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("Address1");
            tTmpEle.setText(tDataInfo[7]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("Address2");
            tTmpEle.setText(tDataInfo[8]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("Address3");
            tTmpEle.setText(QYJKXTwoServiceImp.getTransAddress(tDataInfo[9]));
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("Address4");
            if(tDataInfo[10].equals("")){
            	tTmpEle.setText("北京市西城区阜成门外大街7号国投大厦10层");
            }else{
            	tTmpEle.setText(tDataInfo[10]);
            }
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("AnomalyInform");
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tTmpEle.setText(tDataInfo[11]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("OfferStatus");
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tTmpEle.setText(tDataInfo[12]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SubStartDate");
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tTmpEle.setText(tDataInfo[13]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SubEndDate");
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tTmpEle.setText(tDataInfo[14]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("InsuredGroupCode");
            tTmpEle.setText(tDataInfo[15]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("InsuredType");
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tTmpEle.setText(tDataInfo[16]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SnOfMainInsured");
            if(!tDataInfo[16].equals("00")){
            	tTmpEle.setText(tDataInfo[6]);
            }else{
            	tTmpEle.setText(tDataInfo[17]);
            }
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RelationshipWithMainInsured"); 
            if(tDataInfo[16].equals("00")){
            	tTmpEle.setText("");
            }else{
            	tTmpEle.setText(tDataInfo[18]);
            	tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            }
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ServiceMark");
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tTmpEle.setText(tDataInfo[19]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("HealthFlag");
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tTmpEle.setText(tDataInfo[20]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("SocialcareNo");
            tTmpEle.setText(tDataInfo[21]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("OccupationalCode");
/*          //zyg 置为默认 2147003 - 离退休人员（无兼职）
            if(tDataInfo[22].equals("")){
            	tTmpEle.setText("2147003");
            }else{
            	tTmpEle.setText(tDataInfo[22]);
                tTmpEle.addAttribute("CData", "CBusCode");
            }*/
            //zyg 职业代码采用固定值方式，采用平台代码“0001001”上传，内勤人员
            tTmpEle.setText("0001001");;
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("WorkPlace");
            tTmpEle.setText(tDataInfo[23]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("EmpoyeeNo");
            tTmpEle.setText(tDataInfo[24]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("AppointedBenefit");
            tTmpEle.setText(tDataInfo[25]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
          
            String tInsuredNo = tDataInfo[6];
            if(mEdorProp!=null&&mEdorType!=null&&mEdorType.equals("BC")){
            	tInsuredNo = tDataInfo[27];
            }
            String tContractNo = tDataInfo[26];
            //获取特别承保险种数组
            tTmpEle = reportPolInsUpCovInfo(tContractNo, tInsuredNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

            //获取受益人数组
            tTmpEle = reportPolInsBnfInfo(tContractNo, tInsuredNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
           
            
        }

        return tRoot;
        
    }
  // 从这为粘过来  从契约 受益人
    private SSRS loadPolInsBnfInfo(String cContNo ,String cInsuredNo)
    {
        SSRS tResult = null;

        StringBuffer tStrBSql = new StringBuffer();

        tStrBSql.append(" select name Name, sex Gender,birthday Birthday,idtype CritType,idno CritCode, ");
        tStrBSql.append(" relationtoinsured RelationshipWithInsured, '02' BenefitDistribution, bnfgrade OrderNo,bnflot Proportion ");
        tStrBSql.append(" from lcbnf ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and contno = '"+cContNo+"' and insuredno = '" + cInsuredNo + "' ");
        tStrBSql.append(" union all");
        tStrBSql.append(" select name Name, sex Gender,birthday Birthday,idtype CritType,idno CritCode, ");
        tStrBSql.append(" relationtoinsured RelationshipWithInsured, '02' BenefitDistribution, bnfgrade OrderNo,bnflot Proportion ");
        tStrBSql.append(" from lbbnf ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and contno = '"+cContNo+"' and insuredno = '" + cInsuredNo + "' ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }
    //受益人数组
    private Element reportPolInsBnfInfo(String cContNo,String cInsuredNo)
    {
        Element tRoot = new Element("PolicyBenefitLogDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.PolicyBenefitLogDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolInsBnfInfo(cContNo,cInsuredNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("Name"); //名称   
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Gender"); //性别      
            if(tDataInfo[1].equals("")){
            	tTmpEle.setText("0");
            }else{
            	tTmpEle.setText(tDataInfo[1]);
            }
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Birthday"); //出生日期      
            if(tDataInfo[2].equals("")){
            	tTmpEle.setText("1987-01-01");
            }else{
            	tTmpEle.setText(tDataInfo[2]);
            }
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritType"); //证件类别      
            if(tDataInfo[3].equals("")){
            	tTmpEle.setText("4");
            }else{
            	tTmpEle.setText(tDataInfo[3]);
            }
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritCode"); //证件号码     
            if(tDataInfo[4].equals("")){
            	tTmpEle.setText("111111");
            }else{
            	tTmpEle.setText(tDataInfo[4]);
            }
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RelationshipWithInsured"); //与被保险人关系
            if(tDataInfo[5].equals("")){
            	tTmpEle.setText("99");  //zyg 置为默认 其他
            }else{
            	tTmpEle.setText(tDataInfo[5]);
            }
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("BenefitDistribution"); //受益分配方式  
            tTmpEle.setText(tDataInfo[6]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("OrderNo"); //顺序号        
            tTmpEle.setText(tDataInfo[7]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Proportion"); //比例值
            if(tDataInfo[8].length()>4){
            	tTmpEle.setText(tDataInfo[8].substring(0, 4));
            }else{
            	tTmpEle.setText(tDataInfo[8]);
            }
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
        }

        return tRoot;
    }
  //获取特别承保 ’险种’ 数组
    private SSRS loadPolInsUpCovInfo(String cContNo, String cInsuredNo)
    {
        SSRS tResult = null;

        StringBuffer tStrBSql = new StringBuffer();
        	tStrBSql.append(" select ");
            tStrBSql.append(" lmra.RiskPeriod CoverageType, lmra.RiskCode CoverageCode, ");
            tStrBSql.append(" lcp.RiskCode ComCoverageCode, lmra.RiskName CoverageName, ");
            tStrBSql.append(" lcp.GetStartDate CoverageEffectiveDate, lcp.EndDate CoverageExpireDate, ");
            tStrBSql.append(" lmra.SubRiskFlag MainAttachedFlag, ");//+
            tStrBSql.append(" nvl((select prem from lppol where polno=lcp.polno and edorno=(select min(edorno) from lppol where polno=lcp.polno)),lcp.prem) CoveragePremium, ");
            tStrBSql.append(" lcp.Prem CoverageCurrentPremium, ");
            tStrBSql.append(" nvl((select amnt from lppol where polno=lcp.polno and edorno=(select min(edorno) from lppol where polno=lcp.polno)),lcp.amnt) CoverageSuminsured, lcp.Amnt CoverageEffectiveSuminsured, ");
            tStrBSql.append(" lcp.PayIntv PaymentMethod, (Case when lcp.PayIntv is null then '' else char(lcp.PayYears) end) PaymentYears, ");//+
            tStrBSql.append(" (select count(distinct payno) from ljapayperson where contno = lcp.contno) PaymentNo, ");
            tStrBSql.append(" '0' SpecificBusiness, '' SpecificBusinessCode, ");
            tStrBSql.append(" lcp.StateFlag CoverageStatus, ");
            tStrBSql.append(" lcp.PolNo, ");
            
            tStrBSql.append("(select case when lc.mult='1' then 50 when lc.mult='2' then 100 when lc.mult='3' then 150 else 0 end from lcduty lc where lcp.polno = polno and lcp.riskcode in ('1201','1601') fetch first 1 rows only) CoveragedayAmount,"); //险种期初日额保险金
            tStrBSql.append("(select case when lc.mult='1' then 50 when lc.mult='2' then 100 when lc.mult='3' then 150 else 0 end from lcduty lc where lcp.polno = polno and lcp.riskcode in ('1201','1601') fetch first 1 rows only) CoveragedayEffectiveAmount,");//险种有效日额保险金
            
            if(mEdorProp.equals("I"))
            {//个单
            	tStrBSql.append("( case when lcp.payintv='1'  then prem*12 when lcp.payintv='3'  then prem*4  when lcp.payintv='6'  then prem*2 when lcp.payintv='12' then prem   else 0 end ) CoverageyearPremium,");//险种当前年化保费
            	tStrBSql.append("'01' PremiumSource");//保费来源
            }else{
            	tStrBSql.append("( case when lcp.payintv='1'  then prem*12 when lcp.payintv='3'  then prem*4  when lcp.payintv='6'  then prem*2 when lcp.payintv='12' then prem   else 0 end ) CoverageyearPremium,");//险种当前年化保费
            	tStrBSql.append("'02' PremiumSource");//保费来源
            }
            //退保金
            if(mEdorType.equals("XT")||mEdorType.equals("CT")||mEdorType.equals("WT"))
            {
          	  tStrBSql.append(" ,(select abs(nvl(sum(getmoney),0)) from ljagetendorse where feeoperationtype in ('CT','XT') and feefinatype in ('TF','TB') and lcp.polno = polno) CoverageSurrenderCharge ");
            }
            
            tStrBSql.append(" from LCPol lcp ");
            tStrBSql.append(" inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode ");
            tStrBSql.append(" where 1 = 1 ");
            tStrBSql.append(" and lcp.ContNo = '" + cContNo + "' and lcp.InsuredNo = '" + cInsuredNo + "' ");
//-----------------
            tStrBSql.append(" union all ");
//          -----------------            
            tStrBSql.append(" select ");
            tStrBSql.append(" lmra.RiskPeriod CoverageType, lmra.RiskCode CoverageCode, ");
            tStrBSql.append(" lcp.RiskCode ComCoverageCode, lmra.RiskName CoverageName, ");
            tStrBSql.append(" lcp.GetStartDate CoverageEffectiveDate, lcp.EndDate CoverageExpireDate, ");
            tStrBSql.append(" lmra.SubRiskFlag MainAttachedFlag, ");//+
            tStrBSql.append(" nvl((select prem from lppol where polno=lcp.polno and edorno=(select min(edorno) from lppol where polno=lcp.polno)),lcp.prem) CoveragePremium, ");
            tStrBSql.append(" lcp.Prem CoverageCurrentPremium, ");
            tStrBSql.append(" nvl((select amnt from lppol where polno=lcp.polno and edorno=(select min(edorno) from lppol where polno=lcp.polno)),lcp.amnt) CoverageSuminsured, lcp.Amnt CoverageEffectiveSuminsured, ");
            tStrBSql.append(" lcp.PayIntv PaymentMethod, (Case when lcp.PayIntv is null then '' else char(lcp.PayYears) end) PaymentYears, ");//+
            tStrBSql.append(" (select count(distinct payno) from ljapayperson where contno = lcp.contno) PaymentNo, ");
            tStrBSql.append(" '0' SpecificBusiness, '' SpecificBusinessCode, ");
            tStrBSql.append(" lcp.StateFlag CoverageStatus, ");
            tStrBSql.append(" lcp.PolNo ,");
            
            tStrBSql.append("(select case when lc.mult='1' then 50 when lc.mult='2' then 100 when lc.mult='3' then 150 else 0 end from lbduty lc where lcp.polno = polno and lcp.riskcode in ('1201','1601') fetch first 1 rows only) CoveragedayAmount,"); //险种期初日额保险金
            tStrBSql.append("(select case when lc.mult='1' then 50 when lc.mult='2' then 100 when lc.mult='3' then 150 else 0 end from lbduty lc where lcp.polno = polno and lcp.riskcode in ('1201','1601') fetch first 1 rows only) CoveragedayEffectiveAmount,");//险种有效日额保险金
            
            if(mEdorProp.equals("I"))
            {//个单
            	tStrBSql.append("( case when lcp.payintv='1'  then prem*12 when lcp.payintv='3'  then prem*4  when lcp.payintv='6'  then prem*2 when lcp.payintv='12' then prem   else 0 end ) CoverageyearPremium,");//险种当前年化保费
            	tStrBSql.append("'01' PremiumSource");//保费来源
            }else{
            	tStrBSql.append("( case when lcp.payintv='1'  then prem*12 when lcp.payintv='3'  then prem*4  when lcp.payintv='6'  then prem*2 when lcp.payintv='12' then prem   else 0 end ) CoverageyearPremium,");//险种当前年化保费
            	tStrBSql.append("'02' PremiumSource");//保费来源
            }
            //退保金
           
            if(mEdorType.equals("XT")||mEdorType.equals("CT")||mEdorType.equals("WT"))
            {
          	  tStrBSql.append(" ,(select abs(nvl(sum(getmoney),0)) from ljagetendorse where feeoperationtype in ('CT','XT') and feefinatype in ('TF','TB') and lcp.polno = polno) CoverageSurrenderCharge ");
            }
            
            
            tStrBSql.append(" from LBPol lcp ");
            tStrBSql.append(" inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode ");
            tStrBSql.append(" where 1 = 1 ");
            tStrBSql.append(" and lcp.ContNo = '" + cContNo + "' and lcp.InsuredNo = '" + cInsuredNo + "' ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }
  //获取特别承保 ‘ 险种 ’数组
    private Element reportPolInsUpCovInfo(String cContNo, String cInsuredNo)
    {
        Element tRoot = new Element("SpecialPolicyUpcoverageLogDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.SpecialPolicyUpcoverageLogDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolInsUpCovInfo(cContNo, cInsuredNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("CoverageType"); //平台险种类型
            tTmpEle.setText(tDataInfo[0]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageCode"); //平台险种代码
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ComCoverageCode"); //公司险种代码
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageName"); //公司险种名称
            tTmpEle.setText(tDataInfo[3]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageEffectiveDate"); //险种起保日期
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageExpireDate"); //险种满期日期
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("MainAttachedFlag"); //主附险性质
            tTmpEle.setText(tDataInfo[6]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoveragePremium"); //险种保费    
            tTmpEle.setText(tDataInfo[7]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("CoverageCurrentPremium"); //险种当前保费 
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageSuminsured"); //险种保额    
            tTmpEle.setText(tDataInfo[9]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("CoverageEffectiveSuminsured"); //险种有效保额
            tTmpEle.setText(tDataInfo[10]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("PaymentMethod"); //缴费方式
            tTmpEle.setText(tDataInfo[11]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("PaymentYears"); //缴费年限
            tTmpEle.setText(tDataInfo[12]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("PaymentNo"); //缴费次数
            tTmpEle.setText(tDataInfo[13]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SpecificBusiness"); //特殊业务标识
            tTmpEle.setText(tDataInfo[14]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SpecificBusinessCode"); //特殊业务代码
            tTmpEle.setText(tDataInfo[15]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("CoverageStatus"); //险种状态
            tTmpEle.setText(tDataInfo[16]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            String tPolNo = tDataInfo[17];
          
            // 险种期初日额保险金
            
            tTmpEle = new Element("CoveragedayAmount"); 
            tTmpEle.setText(tDataInfo[18]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            //险种有效日额保险金
            
            tTmpEle = new Element("CoveragedayEffectiveAmount"); 
            tTmpEle.setText(tDataInfo[19]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            //险种当前年化保费
            
            tTmpEle = new Element("CoverageyearPremium"); 
            tTmpEle.setText(tDataInfo[20]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            //保费来源
            tTmpEle = new Element("PremiumSource");
            tTmpEle.setText(tDataInfo[21]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            //退保金
            if(tDataInfo.length==23){
	            tTmpEle = new Element("CoverageSurrenderCharge");
	            tTmpEle.setText(tDataInfo[22]);
	            tTmpEle.addAttribute("CData", "CBigDecimal");
	            tItemEle.addContent(tTmpEle);
	            tTmpEle = null;
            }
            
            //获取特别承保责任数组
            tTmpEle = reportPolInsCovInfo(tPolNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
            
          //获取特别核保结论数组
            tTmpEle = reportPolSpeUWInfo(tPolNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
        }

        return tRoot;
    }
  //获取特别承保 ‘ 责任 ’数组
    private SSRS loadPolInsCovInfo(String cPolNo)
    {
        SSRS tResult = null;

        StringBuffer tStrBSql = new StringBuffer();

        //        tStrBSql.append(" select ");
        //        tStrBSql
        //                .append(" '' LiabilityClassification, '' LiabilityCode, '' LiabilityName, '' LiabilityEffectiveDate, '' LiabilityExpireDate, '' LiabilityStatus, '' LiabilityPremium, '' LimitAmount, '' EffectivelyAmount, '' WaitingPeriod ");
        //        tStrBSql.append(" from dual ");
        //        tStrBSql.append(" where 1 = 2 ");

        tStrBSql.append(" select ");
        tStrBSql.append(" lcd.DutyCode LiabilityClassification, ");
        tStrBSql.append(" lcd.DutyCode LiabilityCode, lmd.DutyName LiabilityName, ");
        tStrBSql.append(" lcd.GetStartDate LiabilityEffectiveDate, (lcd.EndDate - 1 day) LiabilityExpireDate, ");
        tStrBSql.append(" lcp.StateFlag LiabilityStatus, nvl((select Prem from lpduty where polno=lcp.PolNo and edorno=(select min(edorno) from lpduty where polno=lcp.PolNo)),lcd.Prem) LiabilityPremium, ");
        tStrBSql.append(" lcd.Prem LiabilityCurrentPremium, ");
        tStrBSql.append(" nvl((select amnt from lpduty where polno=lcp.PolNo and edorno=(select min(edorno) from lpduty where polno=lcp.PolNo)),lcd.amnt) LimitAmount, lcd.Amnt EffectivelyAmount, ");
        tStrBSql.append(" '0' WaitingPeriod, ");
        
//      责任期初日额保险金
        tStrBSql.append("(select case when lc.mult='1' then 50 when lc.mult='2' then 100 when lc.mult='3' then 150 else 0 end from lcduty lc where lcp.polno = polno and lcp.riskcode in ('1201','1601','1602','160308') fetch first 1 rows only) LiabilityDayAmount,");
//      责任有效日额保险金
        tStrBSql.append("(select case when lc.mult='1' then 50 when lc.mult='2' then 100 when lc.mult='3' then 150 else 0 end from lcduty lc where lcp.polno = polno and lcp.riskcode in ('1201','1601','1602','160308') fetch first 1 rows only) LiabilitydayEffectiveAmount,");

        
        // tStrBSql.append(" trim(lcp.ContNo) || '_' || trim(lcp.InsuredNo) InsuredGroupCode, ");
        tStrBSql.append(" '' InsuredGroupCode, ");
        // tStrBSql.append(" trim(lcp.ContNo) || '_' || trim(lcp.InsuredNo) GroupName ");
        tStrBSql.append(" '' GroupName ");

        
        tStrBSql.append(" from LCPol lcp ");
        tStrBSql.append(" inner join LCDuty lcd on lcd.PolNo = lcp.PolNo ");
        tStrBSql.append(" inner join LMDuty lmd on lmd.DutyCode = lcd.DutyCode ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.polno = '" + cPolNo + "' ");
        
        tStrBSql.append(" union all ");
        tStrBSql.append(" select ");
        tStrBSql.append(" lcd.DutyCode LiabilityClassification, ");
        tStrBSql.append(" lcd.DutyCode LiabilityCode, lmd.DutyName LiabilityName, ");
        tStrBSql.append(" lcd.GetStartDate LiabilityEffectiveDate, (lcd.EndDate - 1 day) LiabilityExpireDate, ");
        tStrBSql.append(" lcp.StateFlag LiabilityStatus, nvl((select Prem from lpduty where polno=lcp.PolNo and edorno=(select min(edorno) from lpduty where polno=lcp.PolNo)),lcd.Prem)  LiabilityPremium, ");
        tStrBSql.append(" lcd.Prem LiabilityCurrentPremium, ");
        tStrBSql.append(" nvl((select amnt from lpduty where polno=lcp.PolNo and edorno=(select min(edorno) from lpduty where polno=lcp.PolNo)),lcd.amnt) LimitAmount, lcd.Amnt EffectivelyAmount, ");
        tStrBSql.append(" '0' WaitingPeriod, ");
        
//      责任期初日额保险金
        tStrBSql.append("(select case when lc.mult='1' then 50 when lc.mult='2' then 100 when lc.mult='3' then 150 else 0 end from lbduty lc where lcp.polno = polno and lcp.riskcode in ('1201','1601','1602','160308') fetch first 1 rows only) LiabilityDayAmount,");
//      责任有效日额保险金
        tStrBSql.append("(select case when lc.mult='1' then 50 when lc.mult='2' then 100 when lc.mult='3' then 150 else 0 end from lbduty lc where lcp.polno = polno and lcp.riskcode in ('1201','1601','1602','160308') fetch first 1 rows only) LiabilitydayEffectiveAmount,");
        // tStrBSql.append(" trim(lcp.ContNo) || '_' || trim(lcp.InsuredNo) InsuredGroupCode, ");
        tStrBSql.append(" '' InsuredGroupCode, ");
        // tStrBSql.append(" trim(lcp.ContNo) || '_' || trim(lcp.InsuredNo) GroupName ");
        tStrBSql.append(" '' GroupName ");

        tStrBSql.append(" from LBPol lcp ");
        tStrBSql.append(" inner join LBDuty lcd on lcd.PolNo = lcp.PolNo ");
        tStrBSql.append(" inner join LMDuty lmd on lmd.DutyCode = lcd.DutyCode ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.polno = '" + cPolNo + "' ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }
  //获取特别承保 ‘ 责任 ’数组
    private Element reportPolInsCovInfo(String cPolNo)
    {
        Element tRoot = new Element("SpecialPolicyCoverageLogDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.SpecialPolicyCoverageLogDTO");
        tRoot.addAttribute("ForceSyncFun", "setSpecialPolicyCoverageLogDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolInsCovInfo(cPolNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("LiabilityClassification"); //平台责任分类
            tTmpEle.setText(tDataInfo[0]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityCode"); //责任代码    
            tTmpEle.setText(tDataInfo[1]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityName"); //责任名称    
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityEffectiveDate"); //责任开始日期
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityExpireDate"); //责任终止日期
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityStatus"); //责任状态    
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityPremium"); //责任期初保费    
            tTmpEle.setText(tDataInfo[6]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("LiabilityCurrentPremium"); //责任累计保费    
            tTmpEle.setText(tDataInfo[7]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LimitAmount"); //责任保额    
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("EffectivelyAmount"); //责任有效保额
            tTmpEle.setText(tDataInfo[9]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("WaitingPeriod"); //免责期      
            tTmpEle.setText(tDataInfo[10]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("LiabilityDayAmount"); //责任期初日额保险金    
            tTmpEle.setText(tDataInfo[11]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("LiabilitydayEffectiveAmount"); //责任有效日额保险金 
            tTmpEle.setText(tDataInfo[12]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            //获取特别赔付比例分段数组
            tTmpEle = reportPolSpeAmInfo(cPolNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

        }

        return tRoot;
    }
    
    private SSRS loadPolSpeUWInfo(String tPolNo)
    {
        SSRS tResult = null;

        StringBuffer tStrBSql = new StringBuffer();

        //        tStrBSql.append(" select ");
        //        tStrBSql.append(" '' UnderwritingResult, '' UnderwritingDate ");
        //        tStrBSql.append(" from dual ");
        //        tStrBSql.append(" where 1 = 2 ");

        tStrBSql.append(" select ");
        tStrBSql.append(" UWFlag UnderwritingResult, UWDate UnderwritingDate, ");
        tStrBSql.append(" '99' DeclinedReason, '1' State, ");
        tStrBSql.append(" case when exists (select 1 from LCPENotice where contno = lcp.contno) then '1' else '0' end PhysicalExamination ");
        tStrBSql.append(" from LCPol lcp ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.PolNo = '" + tPolNo + "' ");
        
        tStrBSql.append(" union all ");
        tStrBSql.append(" select ");
        tStrBSql.append(" UWFlag UnderwritingResult, UWDate UnderwritingDate, ");
        tStrBSql.append(" '99' DeclinedReason, '1' State, ");
        tStrBSql.append(" case when exists (select 1 from LCPENotice where contno = lcp.contno) then '1' else '0' end PhysicalExamination ");
        tStrBSql.append(" from LBPol lcp ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.PolNo = '" + tPolNo + "' ");
        
        tStrBSql.append(" union all  ");
        tStrBSql.append(" select ");
        tStrBSql.append(" UWFlag UnderwritingResult, UWDate UnderwritingDate, ");
        tStrBSql.append(" '99' DeclinedReason, '1' State, ");
        tStrBSql.append(" case when exists (select 1 from LCPENotice where contno = lcp.contno) then '1' else '0' end PhysicalExamination ");
        tStrBSql.append(" from LPPol lcp ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.PolNo = '" + tPolNo + "' and lcp.EdorNo = '"+mEdorNo+"' ");
        tStrBSql.append(" and exists (select 1 from lpuwmaster where edorno=lcp.edorno) ");

        String tStrSql = tStrBSql.toString();
        System.out.println(tStrSql);
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }
    
    private Element reportPolSpeUWInfo(String cContNo)
    {
        Element tRoot = new Element("SpecialUnderWritingLogDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.SpecialUnderWritingLogDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolSpeUWInfo(cContNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("UnderwritingResult"); //核保结论
            tTmpEle.setText(tDataInfo[0]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("UnderwritingDate"); //核保结论日期
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("DeclinedReason");
            if(tDataInfo[0].equals("1")){
            	tTmpEle.setText(tDataInfo[2]);//当核保结论为50-拒保时，拒保原因必须上传 默认为99 - 其他 modify by zhangyige 2012-12-28
            }else{
            	tTmpEle.setText("");//当核保结论为不为拒保时，拒保原因传成空 modify by zhangyige 2012-12-28
            }
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("State");
            tTmpEle.setText(tDataInfo[3]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("PhysicalExamination");
            tTmpEle.setText(tDataInfo[4]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
        }

        return tRoot;
    }
    
    private SSRS loadPolSpeAmInfo(String cPolNo)
    {
        SSRS tResult = null;
        StringBuffer tStrBSql = new StringBuffer();
        tStrBSql.append(" select ");
        tStrBSql.append(" RowNumber() over() as SectionNo, ");
        tStrBSql.append(" 0 Coststart, 0 Costend, cast(lcd.GetLimit as decimal(12,2)) Deductible, cast(lcd.GetRate as decimal(12,2)) ClaimRatio ");
        tStrBSql.append(" from LCDuty lcd ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcd.PolNo = '" + cPolNo + "' ");
        
        tStrBSql.append(" union all ");
        tStrBSql.append(" select ");
        tStrBSql.append(" RowNumber() over() as SectionNo, ");
        tStrBSql.append(" 0 Coststart, 0 Costend, cast(lcd.GetLimit as decimal(12,2)) Deductible, cast(lcd.GetRate as decimal(12,2)) ClaimRatio ");
        tStrBSql.append(" from LBDuty lcd ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcd.PolNo = '" + cPolNo + "' ");
        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);
        return tResult;
    }

    private Element reportPolSpeAmInfo(String cContNo)
    {
        Element tRoot = new Element("SpecialPolicyAmountLogDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.SpecialPolicyAmountLogDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolSpeAmInfo(cContNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("SectionNo"); //段序    
            tTmpEle.setText(String.valueOf(idxPI));
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Coststart"); //费用起线
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("Costend"); //费用止线
            tTmpEle.setText(tDataInfo[2]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("Deductible"); //免赔额  
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("ClaimRatio"); //赔付比例
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

        }

        return tRoot;
    }
  //到此为粘过来 从契约  
    
    
    //被保人
    private SSRS loadPolicyInsuredLogInfo(String cContNo)
    {
    	
    	String tContNo = cContNo;
    	
        SSRS tResult = null;
        StringBuffer tStrBSql = new StringBuffer();
        
        if(mEdorProp.equals("I"))
        {
        	tStrBSql.append(" select ");
            tStrBSql.append(" Name Name, Sex Gender, ");
            tStrBSql.append(" BirthDay BirthDay, IDType CritType, IDNo CritCode, ");
            tStrBSql.append(" lci.RelationToAppnt RelationshipWithPh, ");
            tStrBSql.append(" InsuredNo CustomerNo, ");
          //zyg 默认置为 北京（省）市辖（区） 西城（县）
            tStrBSql.append(" '110000' Address1, ");
            tStrBSql.append(" '110100' Address2, ");
            tStrBSql.append(" (select PostalAddress from LCAddress where customerno = lci.insuredno and addressno = lci.addressno ) Address3, ");
            tStrBSql.append(" (select PostalAddress from LCAddress where customerno = lci.insuredno and addressno = lci.addressno ) Address4, ");
            tStrBSql.append(" '0' AnomalyInform, ");
            tStrBSql.append(" (select stateflag from lccont where contno= lci.contno) OfferStatus, '' SubStartDate, '' SubEndDate, ");
            tStrBSql.append(" '' InsuredGroupCode, Relationtomaininsured InsuredType, ");
            tStrBSql.append(" '' SnOfMainInsured, RelationToMainInsured RelationshipWithMainInsured, ");
            tStrBSql.append(" '' ServiceMark, '' HealthFlag, ");
            tStrBSql.append(" '' SocialcareNo , OccupationCode OccupationalCode, ");
            tStrBSql.append(" '' WorkPlace, '' EmpoyeeNo, ");
            tStrBSql.append(" case when exists (select 1 from LCBnf where contno = lci.contno and insuredno = lci.insuredno ) then '1' else '0' end AppointedBenefit, ");
            tStrBSql.append(" lci.ContNo ");
            tStrBSql.append(" from LCInsured lci");
            tStrBSql.append(" where 1 = 1 ");
            tStrBSql.append(" and ContNo = '" + tContNo + "' ");
            tStrBSql.append(" union all  ");
            tStrBSql.append(" select ");
            tStrBSql.append(" Name Name, Sex Gender, ");
            tStrBSql.append(" BirthDay BirthDay, IDType CritType, IDNo CritCode, ");
            tStrBSql.append(" lci.RelationToAppnt RelationshipWithPh, ");
            tStrBSql.append(" InsuredNo CustomerNo, ");
            //zyg 默认置为 北京（省）市辖（区） 西城（县）
            tStrBSql.append(" '110000' Address1, ");
            tStrBSql.append(" '110100' Address2, ");
            tStrBSql.append(" (select PostalAddress from LCAddress where customerno = lci.insuredno and addressno = lci.addressno ) Address3, ");
            tStrBSql.append(" (select PostalAddress from LCAddress where customerno = lci.insuredno and addressno = lci.addressno ) Address4, ");
            tStrBSql.append(" '0' AnomalyInform, ");
            tStrBSql.append(" (select stateflag from lbcont where contno= lci.contno) OfferStatus, '' SubStartDate, '' SubEndDate, ");
            tStrBSql.append(" '' InsuredGroupCode, Relationtomaininsured InsuredType, ");
            tStrBSql.append(" '' SnOfMainInsured, RelationToMainInsured RelationshipWithMainInsured, ");
            tStrBSql.append(" '' ServiceMark, '' HealthFlag, ");
            tStrBSql.append(" '' SocialcareNo , OccupationCode OccupationalCode, ");
            tStrBSql.append(" '' WorkPlace, '' EmpoyeeNo, ");
            tStrBSql.append(" case when exists (select 1 from LBBnf where contno = lci.contno and insuredno = lci.insuredno ) then '1' else '0' end AppointedBenefit, ");
            tStrBSql.append(" lci.ContNo ");
            tStrBSql.append(" from LBInsured lci");
            tStrBSql.append(" where 1 = 1 ");
            tStrBSql.append(" and ContNo = '" + tContNo + "' ");
        }
        else
        {
        	tStrBSql.append(" select ");
            tStrBSql.append(" Name Name, Sex Gender, ");
            tStrBSql.append(" BirthDay BirthDay, IDType CritType, IDNo CritCode, ");
            tStrBSql.append(" '29' RelationshipWithPh, ");
            tStrBSql.append(" InsuredNo CustomerNo, ");
          //zyg 默认置为 北京（省）市辖（区） 西城（县）
            tStrBSql.append(" '110000' Address1, ");
            tStrBSql.append(" '110100' Address2, ");
            tStrBSql.append(" (select PostalAddress from LCAddress where customerno = lci.insuredno and addressno = lci.addressno ) Address3, ");
            tStrBSql.append(" (select PostalAddress from LCAddress where customerno = lci.insuredno and addressno = lci.addressno ) Address4, ");
            tStrBSql.append(" '0' AnomalyInform, ");
            tStrBSql.append(" (select stateflag from lccont where contno= lci.contno) OfferStatus, '' SubStartDate, '' SubEndDate, ");
            tStrBSql.append(" '' InsuredGroupCode, Relationtomaininsured InsuredType, ");
            tStrBSql.append(" '' SnOfMainInsured, RelationToMainInsured RelationshipWithMainInsured, ");
            tStrBSql.append(" '' ServiceMark, '' HealthFlag, ");
            tStrBSql.append(" '' SocialcareNo , OccupationCode OccupationalCode, ");
            tStrBSql.append(" '' WorkPlace, '' EmpoyeeNo, ");
            tStrBSql.append(" case when exists (select 1 from LCBnf where contno = lci.contno and insuredno = lci.insuredno ) then '1' else '0' end AppointedBenefit, ");
            tStrBSql.append(" lci.ContNo ");
            tStrBSql.append(" from LCInsured lci");
            tStrBSql.append(" where 1 = 1 ");
            tStrBSql.append(" and GrpContNo = '" + tContNo + "' ");
            tStrBSql.append(" union all  ");
            tStrBSql.append(" select ");
            tStrBSql.append(" Name Name, Sex Gender, ");
            tStrBSql.append(" BirthDay BirthDay, IDType CritType, IDNo CritCode, ");
            tStrBSql.append(" '29' RelationshipWithPh, ");
            tStrBSql.append(" InsuredNo CustomerNo, ");
            //zyg 默认置为 北京（省）市辖（区） 西城（县）
            tStrBSql.append(" '110000' Address1, ");
            tStrBSql.append(" '110100' Address2, ");
            tStrBSql.append(" (select PostalAddress from LCAddress where customerno = lci.insuredno and addressno = lci.addressno ) Address3, ");
            tStrBSql.append(" (select PostalAddress from LCAddress where customerno = lci.insuredno and addressno = lci.addressno ) Address4, ");
            tStrBSql.append(" '0' AnomalyInform, ");
            tStrBSql.append(" (select stateflag from lbcont where contno= lci.contno) OfferStatus, '' SubStartDate, '' SubEndDate, ");
            tStrBSql.append(" '' InsuredGroupCode, Relationtomaininsured InsuredType, ");
            tStrBSql.append(" '' SnOfMainInsured, RelationToMainInsured RelationshipWithMainInsured, ");
            tStrBSql.append(" '' ServiceMark, '' HealthFlag, ");
            tStrBSql.append(" '' SocialcareNo , OccupationCode OccupationalCode, ");
            tStrBSql.append(" '' WorkPlace, '' EmpoyeeNo, ");
            tStrBSql.append(" case when exists (select 1 from LBBnf where contno = lci.contno and insuredno = lci.insuredno ) then '1' else '0' end AppointedBenefit, ");
            tStrBSql.append(" lci.ContNo ");
            tStrBSql.append(" from LBInsured lci ");
            tStrBSql.append(" where 1 = 1 ");
            tStrBSql.append(" and GrpContNo = '" + tContNo + "' ");
        }
        

        //是增减人的修改为增减人的人
        if(mEdorType!=null&&mEdorType.equals("ZT"))
        {
        	 tStrBSql=new StringBuffer();
        	 tStrBSql.append(" select ");
             tStrBSql.append(" Name Name, Sex Gender, ");
             tStrBSql.append(" BirthDay BirthDay, IDType CritType, IDNo CritCode, ");
             tStrBSql.append(" '29' RelationshipWithPh, ");
             tStrBSql.append(" InsuredNo CustomerNo, ");
             //zyg 默认置为 北京（省）市辖（区） 西城（县）
             tStrBSql.append(" '110000' Address1, ");
             tStrBSql.append(" '110100' Address2, ");
             tStrBSql.append(" (select PostalAddress from LCAddress where customerno = lci.insuredno and addressno = lci.addressno ) Address3, ");
             tStrBSql.append(" (select PostalAddress from LCAddress where customerno = lci.insuredno and addressno = lci.addressno ) Address4, ");
             tStrBSql.append(" '0' AnomalyInform, ");
             tStrBSql.append(" '3' OfferStatus, '' SubStartDate, (select cinvalidate from lbcont where contno = lci.contno fetch first 1 rows only) SubEndDate, ");
             tStrBSql.append(" '' InsuredGroupCode, Relationtomaininsured InsuredType, ");
             tStrBSql.append(" '' SnOfMainInsured, RelationToMainInsured RelationshipWithMainInsured, ");
             tStrBSql.append(" '' ServiceMark, '' HealthFlag, ");
             tStrBSql.append(" '' SocialcareNo , OccupationCode OccupationalCode, ");
             tStrBSql.append(" '' WorkPlace, '' EmpoyeeNo, ");
             tStrBSql.append(" case when exists (select 1 from LBBnf where contno = lci.contno and insuredno = lci.insuredno ) then '1' else '0' end AppointedBenefit, ");
             tStrBSql.append(" lci.ContNo ");
             tStrBSql.append(" from LBInsured lci ");
             tStrBSql.append(" where 1 = 1 ");
             tStrBSql.append(" and GrpContNo = '" + tContNo + "' and edorno='"+mEdorNo+"' ");
        }
        if(mEdorType!=null&&mEdorType.equals("NI"))
        {
        	 tStrBSql=new StringBuffer();
        	 tStrBSql.append(" select ");
             tStrBSql.append(" Name Name, Sex Gender, ");
             tStrBSql.append(" BirthDay BirthDay, IDType CritType, IDNo CritCode, ");
             tStrBSql.append(" '29' RelationshipWithPh, ");
             tStrBSql.append(" InsuredNo CustomerNo, ");
             //zyg 默认置为 北京（省）市辖（区） 西城（县）
             tStrBSql.append(" '110000' Address1, ");
             tStrBSql.append(" '110100' Address2, ");
             tStrBSql.append(" (select PostalAddress from LCAddress where customerno = lci.insuredno and addressno = lci.addressno ) Address3, ");
             tStrBSql.append(" (select PostalAddress from LCAddress where customerno = lci.insuredno and addressno = lci.addressno ) Address4, ");
             tStrBSql.append(" '0' AnomalyInform, ");
             tStrBSql.append(" '1' OfferStatus, (select cvalidate from lcgrpcont where grpcontno='" + tContNo + "') SubStartDate, (select cinvalidate from lcgrpcont where grpcontno='" + tContNo + "') SubEndDate, ");
             tStrBSql.append(" '' InsuredGroupCode, Relationtomaininsured  InsuredType, ");
             tStrBSql.append(" '' SnOfMainInsured, RelationToMainInsured RelationshipWithMainInsured, ");
             tStrBSql.append(" '' ServiceMark, '' HealthFlag, ");
             tStrBSql.append(" '' SocialcareNo , OccupationCode OccupationalCode, ");
             tStrBSql.append(" '' WorkPlace, '' EmpoyeeNo, ");
             tStrBSql.append(" case when exists (select 1 from LCBnf where contno = lci.contno and insuredno = lci.insuredno ) then '1' else '0' end AppointedBenefit, ");
             tStrBSql.append(" lci.ContNo ");
             tStrBSql.append(" from LcInsured lci ");
             tStrBSql.append(" where 1 = 1 ");
             tStrBSql.append(" and GrpContNo = '" + tContNo + "'  ");
             tStrBSql.append(" and exists (select contno from ljagetendorse where endorsementno='"+mEdorNo+"' and feeoperationtype='NI' and lci.contno=contno )");
            
        }
        //客户资料变更只报送改变人的信息
        else if(mEdorType!=null&&mEdorType.equals("CM"))
        {
        	 tStrBSql=new StringBuffer();
        	 tStrBSql.append(" select ");
             tStrBSql.append(" Name Name, Sex Gender, ");
             tStrBSql.append(" BirthDay BirthDay, IDType CritType, IDNo CritCode, ");
             tStrBSql.append(" lci.RelationToAppnt RelationshipWithPh, ");
             tStrBSql.append(" InsuredNo CustomerNo, ");
           //zyg 默认置为 北京（省）市辖（区） 西城（县）
             tStrBSql.append(" '110000' Address1, ");
             tStrBSql.append(" '110100' Address2, ");
             tStrBSql.append(" (select PostalAddress from LCAddress where customerno = lci.insuredno and addressno = lci.addressno ) Address3, ");
             tStrBSql.append(" (select PostalAddress from LCAddress where customerno = lci.insuredno and addressno = lci.addressno ) Address4, ");
             tStrBSql.append(" '0' AnomalyInform, ");
             tStrBSql.append(" (select stateflag from lccont where contno=lci.contno) OfferStatus, '' SubStartDate, '' SubEndDate, ");
             tStrBSql.append(" '' InsuredGroupCode, Relationtomaininsured InsuredType, ");
             tStrBSql.append(" '' SnOfMainInsured, RelationToMainInsured RelationshipWithMainInsured, ");
             tStrBSql.append(" '' ServiceMark, '' HealthFlag, ");
             tStrBSql.append(" '' SocialcareNo , OccupationCode OccupationalCode, ");
             tStrBSql.append(" '' WorkPlace, '' EmpoyeeNo, ");
             tStrBSql.append(" case when exists (select 1 from LCBnf where contno = lci.contno and insuredno = lci.insuredno ) then '1' else '0' end AppointedBenefit, ");
             tStrBSql.append(" lci.ContNo ");
             tStrBSql.append(" from LpInsured lci ");
             tStrBSql.append(" where 1 = 1 ");
             tStrBSql.append(" and edorno = '" + mEdorNo + "' ");
            
        }
        //受益人变更只报送受益人的信息
        if(mEdorProp!=null&&mEdorProp.equals("I")&&mEdorType!=null&&mEdorType.equals("BC"))
        {
        	 tStrBSql=new StringBuffer();
        	 tStrBSql.append(" select ");
             tStrBSql.append(" distinct Name Name, Sex Gender, ");
             tStrBSql.append(" BirthDay BirthDay, IDType CritType, IDNo CritCode, ");
             tStrBSql.append(" '30' RelationshipWithPh, "); //受益人默认置为30 - 未知
             tStrBSql.append(" trim(POLNO)||trim(INSUREDNO)||trim(BNFTYPE)||trim(char(BNFNO)) CustomerNo, ");
           //zyg默认置为 北京（省）市辖（区） 西城（县）
             tStrBSql.append(" '110000' Address1, ");
             tStrBSql.append(" '110100' Address2, ");
             tStrBSql.append(" (select PostalAddress from LCAddress where customerno = lci.insuredno and addressno = (select addressno from lcinsured where insuredno = lci.insuredno)) Address3, ");
             tStrBSql.append(" (select PostalAddress from LCAddress where customerno = lci.insuredno and addressno = (select addressno from lcinsured where insuredno = lci.insuredno)) Address4, ");
             tStrBSql.append(" '0' AnomalyInform, ");
             tStrBSql.append(" (select stateflag from lccont where contno=lci.contno) OfferStatus, '' SubStartDate, '' SubEndDate, ");
             tStrBSql.append(" '' InsuredGroupCode, RelationToInsured InsuredType, ");
             tStrBSql.append(" '' SnOfMainInsured, RelationToInsured RelationshipWithMainInsured, ");
             tStrBSql.append(" '' ServiceMark, '' HealthFlag, ");
             tStrBSql.append(" '' SocialcareNo , '' OccupationalCode, ");
             tStrBSql.append(" '' WorkPlace, '' EmpoyeeNo, ");
             tStrBSql.append(" '1' AppointedBenefit, ");
             tStrBSql.append(" lci.ContNo, ");
             tStrBSql.append(" InsuredNo CustomerNo1 ");
             tStrBSql.append(" from LcBnf lci ");
             tStrBSql.append(" where 1 = 1 ");
             tStrBSql.append(" and contno = '" + tContNo + "' ");
        }
        
        if(mEdorProp!=null&&mEdorProp.equals("G")&&mEdorType!=null&&mEdorType.equals("BC"))
        {
        	 tStrBSql=new StringBuffer();
        	 tStrBSql.append(" select ");
             tStrBSql.append(" distinct Name Name, Sex Gender, ");
             tStrBSql.append(" BirthDay BirthDay, IDType CritType, IDNo CritCode, ");
             tStrBSql.append(" '29' RelationshipWithPh, ");
             tStrBSql.append(" trim(POLNO)||trim(INSUREDNO)||trim(BNFTYPE)||trim(char(BNFNO)) CustomerNo, ");
           //zyg 默认置为 北京（省）市辖（区） 西城（县）
             tStrBSql.append(" '110000' Address1, ");
             tStrBSql.append(" '110100' Address2, ");
             tStrBSql.append(" (select PostalAddress from LCAddress where customerno = lci.insuredno and addressno = (select addressno from lcinsured where insuredno = lci.insuredno)) Address3, ");
             tStrBSql.append(" (select PostalAddress from LCAddress where customerno = lci.insuredno and addressno = (select addressno from lcinsured where insuredno = lci.insuredno)) Address4, ");
             tStrBSql.append(" '0' AnomalyInform, ");
             tStrBSql.append(" (select stateflag from lccont where contno=lci.contno) OfferStatus, '' SubStartDate, '' SubEndDate, ");
             tStrBSql.append(" '' InsuredGroupCode, RelationToInsured InsuredType, ");
             tStrBSql.append(" '' SnOfMainInsured, RelationToInsured RelationshipWithMainInsured, ");
             tStrBSql.append(" '' ServiceMark, '' HealthFlag, ");
             tStrBSql.append(" '' SocialcareNo , '' OccupationalCode, ");
             tStrBSql.append(" '' WorkPlace, '' EmpoyeeNo, ");
             tStrBSql.append(" '1' AppointedBenefit, ");
             tStrBSql.append(" lci.ContNo, ");
             tStrBSql.append(" InsuredNo CustomerNo1 ");
             tStrBSql.append(" from LcBnf lci ");
             tStrBSql.append(" where 1 = 1 ");
             tStrBSql.append(" and exists (select ContNo from LCCont where ContNo = lci.ContNo and GrpContNo = '" + tContNo + "') ");
        }
        
        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }
  //保全项目数组
    private Element reportEndorsementTypeInfo(String cEdorType)
    {
        Element tRoot = new Element("EndorsementTypeDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.EndorsementTypeDTO");

        Element tTmpEle = null;

        Element tItemEle = new Element("Item");
        tRoot.addContent(tItemEle);
        
        tTmpEle = new Element("EndorsementType");   
        tTmpEle.setText(mEdorProp+cEdorType);
        tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
        tItemEle.addContent(tTmpEle);
        tTmpEle = null;
        return tRoot;
    }
    
    /**
     * 调用前置机服务
     * @return
     */
    private boolean calService()
    {
        Document tDocument = formJDocMessage(mRoot, mEdorProp.equals("I")?"11":"02", mDataInfoKey);
        JdomUtil.print(tDocument);

        Document tInStdXmlDoc = tDocument;

        ReqJKXPreService tJKXPreServlet = new ReqJKXPreService();
        String tReceiveXmlDoc = tJKXPreServlet.callService(tInStdXmlDoc);
//        JdomUtil.print(tReceiveXmlDoc);
        System.out.println(tReceiveXmlDoc);

        return true;
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        System.out.println(szFunc + ": " + szErrMsg);

        CError cError = new CError();
        cError.moduleName = "BQJKXServiceImpBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    
    public static void main(String [] args)
    {
//测试环境 ，新契约传成功的保单 ，保全数据假的    	
//    	String tDataInfoKey = "20110512000010&BF&I&000000001000004&2011-05-12&2010-02-03"; //个 成功
//    	String tDataInfoKey = "20110512000011&BF&I&001009990000002&2011-05-12&2010-02-03"; //个 成功
//    	String tDataInfoKey = "20110512000012&AC&G&00000697000002&2011-05-12&2010-02-03"; //团 成功
//    	保全测试
//    	String tDataInfoKey = "20120808000025&BF&I&013936104000001&2012-08-09&2012-08-09";
//    	保全类别不存在
//    	String tDataInfoKey = "20120809000022&BC&I&013936166000001&2012-08-09&2012-08-09";
//    	原保单数据不存在,无法新建保全
//    	String tDataInfoKey = "20120809000023&CM&I&013936164000001&2012-08-09&2012-08-09";
//    	原保单数据不存在,无法新建保全
//    	String tDataInfoKey = "20120808000009&CM&G&00105575000003&2012-08-09&2012-08-09";
//    	成功
//    	String tDataInfoKey = "20120809000006&NI&G&00105530000004&2012-08-09&2012-08-09";
//    	保全主信息错误:|客户编号已经在系统中存在
//    	String tDataInfoKey = "20120809000024&BC&G&00001995000003&2012-08-09&2012-08-09";
//    	成功
    	
//    	String tDataInfoKey = "20120814000029&BC&I&013936286000001&2012-08-09&2012-08-09";
//    	String tDataInfoKey = "20120814000027&CM&I&013936276000001&2012-08-09&2012-08-09";
//    	String tDataInfoKey = "20120814000025&NI&G&00105576000003&2012-08-09&2012-08-09";
//    	成功

//    	String tDataInfoKey = "20121214000001&CT&I&013961300000001&2012-12-14&2012-12-14";
//    	String tDataInfoKey = "20121214000002&BF&I&013961302000001&2012-12-14&2012-12-14";
//    	String tDataInfoKey = "20121214000004&CT&G&00105916000005&2012-12-14&2012-12-14";
//    	String tDataInfoKey = "20121214000005&NI&G&00105916000006&2012-12-14&2012-12-14";
//    	String tDataInfoKey = "20121214000006&ZT&G&00105761000007&2012-12-14&2012-12-14";
//    	String tDataInfoKey = "20121220000010&NI&G&00106003000001&2012-12-20&2013-1-10";
//    	String tDataInfoKey = "20121220000016&CT&I&013961411000001&2012-12-20&2012-12-20";
//    	String tDataInfoKey = "20121231000001&CT&I&013962322000001&2012-12-31&2013-1-16";
//    	String tDataInfoKey = "20121231000002&BF&I&013962323000001&2012-12-31&2013-12-5";
//    	String tDataInfoKey = "20121231000003&NI&G&00106022000001&2012-12-31&2013-1-4";
    	
//    	String tDataInfoKey = "20130105000021&CT&I&013962512000001&2013-1-5&2013-1-5";
//    	String tDataInfoKey = "20130105000022&BF&I&013962513000001&2013-1-5&2013-1-5";
//    	String tDataInfoKey = "20130105000023&NI&G&00106028000001&2013-1-5&2013-1-5";
    	
//    	String tDataInfoKey = "20130106000013&CT&I&013963112000001&2013-1-6&2013-1-7";
//    	String tDataInfoKey = "20130106000015&NI&G&00106034000001&2013-1-6&2013-1-6";
//    	String tDataInfoKey = "20130106000014&BF&I&013963113000001&2013-1-6&2013-1-7";
    	
    	
//    	String tDataInfoKey = "20100419000014&BF&I&000943112000001&2010-4-19&2010-4-20";
    	
    	//-- 2013-01-17 15:58
    	String tDataInfoKey = "20140128000002&CT&G&00000427000019&2014-01-28&2014-04-01";
    	//个单退保
    	//tDataInfoKey="保全受理号&保全项目类型&(个：I，团：G)&保单号&保全受理日期&保全生效日期";
    	
    	TransferData tTransferData = new TransferData();
    	tTransferData.setNameAndValue("DataInfoKey", tDataInfoKey);
    	VData tVData = new VData();
    	tVData.add(tTransferData);

    	new BQJKXTwoServiceImp().callJKXService(tVData, ""); 
    }
}
