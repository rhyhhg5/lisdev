/**
 * 2010-11-18
 */
package com.sinosoft.lis.jkxpt;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.jkxpt.JKXService;
import com.sinosoft.lis.jkxpt.ReqJKXPreService;
import com.sinosoft.lis.yibaotong.JdomUtil;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class BQJKXServiceImp extends JKXService
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private TransferData mTransferData = null;

    /** 数据操作字符串 */
    private String mOperate = "";

    private String mDataInfoKey = "";
    
    private String mEdorNo = ""; //保全受理号
    
    private String mEdorType = ""; //保全项目类型
    
    private String mEdorProp = ""; //个团标志 ：I-个单，G-团单

    private Element mRoot = null;

    private String mContType = null;

    public boolean callJKXService(VData cInputData, String cOperate)
    {
        // 获取入口参数 
        if (!getInputData(cInputData, cOperate))
        {
            return false;
        }
        // --------------------
        
        // 取数逻辑处理        
        //        mRoot = dealBusiXml(mPolicyNo);
        mRoot = reportBusiXml(mDataInfoKey);
        // --------------------

        // 调用前置机
        if (!calService())
        {
            return false;
        }
        // --------------------

        return true;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mDataInfoKey = (String) mTransferData.getValueByName("DataInfoKey");
        if (mDataInfoKey == null || mDataInfoKey.equals(""))
        {
            buildError("getInputData", "获取保全参数信息失败。");
            return false;
        }
        
    	String [] tEdorInfo = mDataInfoKey.split("&");
		mEdorNo = tEdorInfo[0];
		mEdorType = tEdorInfo[1];
		mEdorProp = tEdorInfo[2];
		
		if (mEdorNo == null || mEdorNo.equals(""))
        {
            buildError("getInputData", "获取保全受理号EdorNo信息失败。");
            return false;
        }
		
		if (mEdorType == null || mEdorType.equals(""))
        {
            buildError("getInputData", "获取保全类型EdorType信息失败。");
            return false;
        }
		
		if (mEdorProp == null || mEdorProp.equals(""))
        {
            buildError("getInputData", "获取保全个团标志EdorProp信息失败。");
            return false;
        }

        return true;
    }

    private Element reportBusiXml(String cDataInfoKey)
    {
        Element tRoot = new Element("PolicyLogDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.PolicyLogDTO");

        Element tTmpEle = null;

        SSRS tResPolicyLogInfo = loadPolicyLogInfo(cDataInfoKey);
        for (int idxPI = 1; idxPI <= tResPolicyLogInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyLogInfo.getRowData(idxPI);
            
            tTmpEle = new Element("Rejection");
            tTmpEle.setText(tDataInfo[0]);
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ApplicationFormNo");
            tTmpEle.setText(tDataInfo[1]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PolicyNo");
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("EndorsementType");
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("EndorsementNo");
            tTmpEle.setText(tDataInfo[4]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("EndorsementApplicationDate");
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("EffectiveDate");
            tTmpEle.setText(tDataInfo[6]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ApplicationDate");
            tTmpEle.setText(tDataInfo[7]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ContractType");
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("GroupMark");
            tTmpEle.setText(tDataInfo[9]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PolicyStartDate");
            tTmpEle.setText(tDataInfo[10]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PolicyEndDate");
            tTmpEle.setText(tDataInfo[11]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("TerminationDate");
            tTmpEle.setText(tDataInfo[12]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SuspendDate");
            tTmpEle.setText(tDataInfo[13]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RecoverDate");
            tTmpEle.setText(tDataInfo[14]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ContractSource");
            tTmpEle.setText(tDataInfo[15]);
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ContractNo");
            tTmpEle.setText(tDataInfo[16]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RenewalTimes");
            tTmpEle.setText(tDataInfo[17]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RenewalMethod");
            tTmpEle.setText(tDataInfo[18]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("MainAttachedFlag");
            tTmpEle.setText(tDataInfo[19]);
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ContractStatus");
            tTmpEle.setText(tDataInfo[20]);
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("TerminationReason");
            tTmpEle.setText(tDataInfo[21]);
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SumInsured");
            tTmpEle.setText(tDataInfo[22]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Premium");
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为“NUMBER”的字段，请增加描述
            tTmpEle.setText(tDataInfo[23]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PaymentMethod");
            tTmpEle.setText(tDataInfo[24]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PaymentYears");
            tTmpEle.setText(tDataInfo[25]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PolicyHolderPro");
            tTmpEle.setText(tDataInfo[26]);
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PolicyHolderNum");
            tTmpEle.setText(tDataInfo[27]);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为“NUMBER”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("EffectiveInsuredNum");
            tTmpEle.setText(tDataInfo[28]);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为“NUMBER”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("FormerpolicyNo");
            tTmpEle.setText(tDataInfo[29]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SpecialRemark");
            tTmpEle.setText(tDataInfo[30]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RegularClearingMark");
            tTmpEle.setText(tDataInfo[31]);
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RegularClearing");
            tTmpEle.setText(tDataInfo[32]);
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RegularClearingDate");
            tTmpEle.setText(tDataInfo[33]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PremiumDueDate");
            tTmpEle.setText(tDataInfo[34]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RealTimeClaimFlag");
            tTmpEle.setText(tDataInfo[35]);
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            String tContNo = tDataInfo[16];
     
            //获取险种信息
            if(mEdorType!=null&&mEdorType.equals("NI"))
            {
	         
            }
            else
            {
            	 tTmpEle = reportPolicyUpcoverageLogInfo(tContNo);
	   	          if (tTmpEle != null)
	   	          {
	   	              tItemEle.addContent(tTmpEle);
	   	          }
            }
            // 获取责任信息
//            tTmpEle = reportPolicyCoverageLogInfo(tContNo);
//            if (tTmpEle != null)
//            {
//                tItemEle.addContent(tTmpEle);
//            }
            tTmpEle = null;
            // --------------------
            
            String tContType = tDataInfo[8];
            String tGrpContNo = tDataInfo[9];
            
            //做到这了----------------------------------------------------------------------------
            if(tContType.equals("2"))//团体投保人数组
            {
            	tTmpEle = reportPolicyGroupPHLogInfo(tGrpContNo);
                if (tTmpEle != null)
                {
                    tItemEle.addContent(tTmpEle);
                }
                tTmpEle = null;
            	
            }
            else//个人投保人数组
            {
            	tTmpEle = reportPolicyPersonPHLogInfo(tContNo);
                if (tTmpEle != null)
                {
                    tItemEle.addContent(tTmpEle);
                }
                tTmpEle = null;
            }
           
//          被保人数组          
            	tTmpEle = reportPolicyInsuredLogInfo(tContNo);
 	            if (tTmpEle != null)
 	            {
 	                tItemEle.addContent(tTmpEle);
 	            }
 	            tTmpEle = null;
 	            // --------------------
            
        }

        return tRoot;
    }
	private String getItem(String s)
	{
		String[] t=null;
		t=s.split("&");
		return t[t.length-2];
	}

    private SSRS loadPolicyLogInfo(String cDataInfoKey)
    {
        SSRS tResult = null;
        StringBuffer tStrBSql = new StringBuffer();
        
//        用户名	USER	VARCHAR2(30)	Y		
//        密码	PASSWORD	VARCHAR2(600)	Y		
//        是否拒保	REJECTION	VARCHAR2(1)		Y	《公用代码》1.1.32
//        投保单号	APPLICATIONFORMNO	VARCHAR2(30)			
//        保单号	POLICYNO	VARCHAR2(30)	Y		
//        保全类别	ENDORSEMENTTYPE	VARCHAR2(2)	Y	Y	《公用代码》1.1.30
//        保全批单号	ENDORSEMENTNO	VARCHAR2(30)	Y		
//        保全申请日期	APPLICATIONDATE	DATE	Y		
//        保全生效日期	EFFECTIVEDATE	DATE	Y		
//        投保申请日期	APPLICATIONDATE	DATE			
//        合同类别	CONTRACTTYPE	VARCHAR2(2)		Y	《公用代码》1.1.1
//        团单唯一标识	GROUPMARK	VARCHAR2(30)			当合同类别为“团单” 时，“团单唯一标识”必须非空；当合同类别为“个单” 时，“团单唯一标识”必须为空。
//        保单起保时间	POLICYSTARTDATE	DATE			保单的生效日期，责任的起始日期
//        保单满期时间	POLICYENDDATE	DATE			保单承保时所约定的责任满期终止日期
//        合同终止日期	TERMINATIONDATE	DATE			
//        合同中止日期	SUSPENDDATE	DATE			
//        保单效力恢复日期	RECOVERDATE	DATE			
//        合同来源	CONTRACTSOURCE	VARCHAR2(2)		  Y	《公用代码》1.1.2
//        合同号	CONTRACTNO	VARCHAR2(30)			
//        续保次数	RENEWALTIMES	NUMBER(8)			
//        连续投保方式	RENEWALMETHOD	VARCHAR2(30)			
//        主附险性质	MAINATTACHEDFLAG	VARCHAR2(1)		Y	《公用代码》1.1.3
//        合同状态	CONTRACTSTATUS	VARCHAR2(2)		Y	《公用代码》1.1.4
//        合同终止原因	TERMINATIONREASON	VARCHAR2(1)		Y	《公用代码》1.1.5
//        保额	SUMINSURED	NUMBER(20,2)			
//        保险费	PREMIUM	NUMBER(20,2)			
//        缴费方式	PAYMENTMETHOD	VARCHAR2(2)			《公用代码》1.1.6
//        缴费年限	PAYMENTYEARS	VARCHAR2(2)			
//        投保人类型	POLICYHOLDERPRO	VARCHAR2(1)		Y	《公用代码》1.1.7
//        投保人数	POLICYHOLDERNUM	NUMBER(8)			承保时的实际投保人数
//        有效被保人数	EFFECTIVEINSUREDNUM	NUMBER(8)			保单当前时被保险人数，承保时与投保人数一致
//        续保前保单号	FORMERPOLICYNO	VARCHAR2(30)			
//        特别约定	SPECIALREMARK	VARCHAR2(600)			
//        定期结算标识	REGULARCLEARINGMARK	VARCHAR2(1)		Y	《公用代码》1.1.32
//        定期结算方式	REGULARCLEARING	VARCHAR2(2)		Y	《公用代码》1.1.8
//        定期结算日期	REGULARCLEARINGDATE	DATE			
//        最晚结算日期	PREMIUMDUEDATE	DATE			
//        是否实时理赔	REALTIMECLAIMFLAG	VARCHAR2(1)		Y	《公用代码》1.1.32
//        承保责任数组	COVERAGE_ARRAY				详见下面描述
//        团体投保人数组	GROUPPH_ARRAY				详见下面描述。
//        当“合同类别”为团单时，必须提供“团体投保人数组”。
//        个人投保人数组	PERSONPH_ARRAY				当“合同类别”为个单时，必须提供“个人投保人数组”。
//        被保险人数组	INSURED_ARRAY				详见下面描述

        tStrBSql.append(" select ");
        tStrBSql.append(" (select UWFlag from LCCont where ContNo = a.ContNo union select UWFlag from LBCont where ContNo = a.ContNo) Rejection, ");
        tStrBSql.append(" (select PrtNo from LCCont where ContNo = a.ContNo union select PrtNo from LBCont where ContNo = a.ContNo) ApplicationFormNo, ");
        tStrBSql.append(" a.ContNo PolicyNo, EdorType EndorsementType, EdorNo EndorsementNo, ");
        tStrBSql.append(" EdorAppDate EndorsementApplicationDate, ");
        tStrBSql.append(" EdorValiDate EffectiveDate, ");
        tStrBSql.append(" (select PolApplyDate from LCCont where ContNo = a.ContNo union select PolApplyDate from LBCont where ContNo = a.ContNo) ApplicationDate, ");
        tStrBSql.append(" (select ContType from LCCont where ContNo = a.ContNo union select ContType from LBCont where ContNo = a.ContNo) ContractType, ");
        tStrBSql.append(" (select GrpContNo from LCCont where ContNo = a.ContNo union select GrpContNo from LBCont where ContNo = a.ContNo) GroupMark, ");
        tStrBSql.append(" (select CValidate from LCCont where ContNo = a.ContNo union select CValidate from LBCont where ContNo = a.ContNo) PolicyStartDate, ");
        tStrBSql.append(" (select CInvalidate from LCCont where ContNo = a.ContNo union select CInvalidate from LBCont where ContNo = a.ContNo) PolicyEndDate, ");
        tStrBSql.append(" (case when EdorType in ('XT','WT','CT') then EdorValiDate else (select max(StartDate) from LCContState where ContNo = a.ContNo and PolNo = '000000' and StateType = 'Terminate') end ) TerminationDate, ");
        tStrBSql.append(" (select max(StartDate) from LCContState where ContNo = a.ContNo and PolNo = '000000' and StateType = 'Available') SuspendDate, ");
        tStrBSql.append(" (select max(EdorValiDate) from LPEdorItem where ContNo = a.ContNo and EdorType = 'FX' and EdorState = '0') RecoverDate, ");
        tStrBSql.append(" (case (select 1 from lcrnewstatelog where contno = a.contno fetch first 1 row only) when 1 then '02' else '01' end) ContractSource, ");
        tStrBSql.append(" a.ContNo ContractNo, ");
        tStrBSql.append(" (select count(distinct NewContNo) from LCRNewStateLog where ContNo = a.ContNo) RenewalTimes, ");
        tStrBSql.append(" '01' RenewalMethod, 'M' MainAttachedFlag, ");
        tStrBSql.append(" (select StateFlag from LCCont where ContNo = a.ContNo union select StateFlag from LBCont where ContNo = a.ContNo) ContractStatus, ");
        tStrBSql.append(" (case when EdorType in ('XT','WT','CT') then '退保' else (select max(StateReaSon) from LCContState where ContNo = a.ContNo and PolNo = '000000' and StateType = 'Terminate') end ) TerminationReason, ");
        tStrBSql.append(" '' SumInsured, ");
        tStrBSql.append(" (case  when (select prem from lccont where contno=a.contno)  is null then (select prem from lbcont where contno=a.contno) else (select prem from lccont where contno=a.contno)end ) Premium, ");
        tStrBSql.append(" (select payintv from LCCont where ContNo = a.ContNo union select payintv from LBCont where ContNo = a.ContNo) PaymentMethod, ");
        tStrBSql.append(" '1' PaymentYears, ");
        tStrBSql.append(" (select (case when ContType = '1' then '02' else '01' end) from LCCont where ContNo = a.ContNo union select (case when ContType = '1' then '02' else '01' end) from LBCont where ContNo = a.ContNo) PolicyHolderPro, ");
        tStrBSql.append(" '' PolicyHolderNum, (select count(1) from lcinsured where contno=a.contno union select count(1) from lbinsured where contno=a.contno  ) EffectiveInsuredNum, ");
        tStrBSql.append(" '' FormerpolicyNo, '' SpecialRemark, ");
        tStrBSql.append(" '' RegularClearingMark, ");
//        tStrBSql.append(" '05-年（缴）' RegularClearing, ");
        tStrBSql.append(" '' RegularClearing, ");
        tStrBSql.append(" '' RegularClearingDate, ");
        tStrBSql.append(" (select SignDate from LCCont where ContNo = a.ContNo union select SignDate from LBCont where ContNo = a.ContNo) PremiumDueDate, ");
        tStrBSql.append(" '1' RealTimeClaimFlag, ");
        tStrBSql.append(" '' ");
        tStrBSql.append(" from LPEdorItem a ");
        tStrBSql.append(" where 1 = 1 and EdorState = '0' ");
//        tStrBSql.append(" and ManageCom like '8611%' ");
        tStrBSql.append(" and trim(a.EdorNo)||'&'||trim(a.EdorType)||'&I' = '" + cDataInfoKey + "' ");
        
        //团单
        tStrBSql.append(" union all ");
        tStrBSql.append(" select ");
        tStrBSql.append(" (select UWFlag from LCgrpCont where GRPContNo = a.grpContNo union select UWFlag from LBgrpCont where grpContNo = a.grpContNo) Rejection, ");
        tStrBSql.append(" (select PrtNo from LCgrpCont where grpContNo = a.grpContNo union select PrtNo from LBgrpCont where  grpContNo = a.grpContNo) ApplicationFormNo, ");
        tStrBSql.append(" a.grpContNo PolicyNo, EdorType EndorsementType, EdorNo EndorsementNo, ");
        tStrBSql.append(" EdorAppDate EndorsementApplicationDate, ");
        tStrBSql.append(" EdorValiDate EffectiveDate, ");
        tStrBSql.append(" (select PolApplyDate from LCgrpCont where grpContNo = a.grpContNo union select PolApplyDate from LBgrpCont where grpContNo = a.grpContNo) ApplicationDate, ");
        tStrBSql.append(" '2'  ContractType, ");
        tStrBSql.append(" a.GrpContNo  GroupMark, ");
        tStrBSql.append(" (select CValidate from LCgrpCont where grpContNo = a.grpContNo union select CValidate from LBgrpCont where grpContNo = a.grpContNo) PolicyStartDate, ");
        tStrBSql.append(" (select CInvalidate from LCgrpCont where grpContNo = a.grpContNo union select CInvalidate from LBgrpCont where grpContNo = a.grpContNo) PolicyEndDate, ");
        tStrBSql.append(" (case when EdorType in ('XT','WT','CT') then EdorValiDate else (select max(StartDate) from LCgrpContState where grpContNo = a.grpContNo and grpPolNo = '000000' and StateType = 'Terminate') end ) TerminationDate, ");
        tStrBSql.append(" (select max(StartDate) from LCgrpContState where grpContNo = a.grpContNo and grpPolNo = '000000' and StateType = 'Available') SuspendDate, ");
        tStrBSql.append(" (select max(EdorValiDate) from LPgrpEdorItem where grpContNo = a.grpContNo and EdorType = 'FX' and EdorState = '0') RecoverDate, ");
        tStrBSql.append(" (case (select 1 from lcrnewstatelog where grpcontno = a.grpcontno fetch first 1 row only) when 1 then '02' else '01' end) ContractSource, ");
        tStrBSql.append(" a.grpContNo ContractNo, ");
        tStrBSql.append(" (select count(distinct NewgrpContNo) from LCRNewStateLog where grpContNo = a.grpContNo) RenewalTimes, ");
        tStrBSql.append(" '01' RenewalMethod, 'M' MainAttachedFlag, ");
        tStrBSql.append(" (select StateFlag from LCgrpCont where grpContNo = a.grpContNo union select StateFlag from LBgrpCont where grpContNo = a.grpContNo) ContractStatus, ");
        tStrBSql.append(" (case when EdorType in ('XT','WT','CT') then '退保' else (select max(StateReaSon) from LCgrpContState where grpContNo = a.grpContNo and grpPolNo = '000000' and StateType = 'Terminate') end ) TerminationReason, ");
        tStrBSql.append(" '' SumInsured, ");
        tStrBSql.append(" (case when (select prem from lcgrpcont where grpcontno=a.grpcontno) is null then (select prem from lbgrpcont where grpcontno=a.grpcontno) else (select prem from lcgrpcont where grpcontno=a.grpcontno) end ) Premium, ");
        tStrBSql.append(" (select payintv from LCgrpCont where grpContNo = a.grpContNo union select payintv from LBgrpCont where grpContNo = a.grpContNo) PaymentMethod, ");
        tStrBSql.append(" '1' PaymentYears, ");
        tStrBSql.append(" '01' PolicyHolderPro, ");
        tStrBSql.append(" '' PolicyHolderNum, (select  peoples2 from lcgrpcont where grpcontno=a.grpcontno union select  peoples2 from lbgrpcont where grpcontno=a.grpcontno ) EffectiveInsuredNum, ");
        tStrBSql.append(" '' FormerpolicyNo, '' SpecialRemark, ");
        tStrBSql.append(" '' RegularClearingMark, ");
        tStrBSql.append(" '' RegularClearing, ");
        tStrBSql.append(" '' RegularClearingDate, ");
        tStrBSql.append(" (select SignDate from LCgrpCont where grpContNo = a.grpContNo union select SignDate from LBgrpCont where grpContNo = a.grpContNo) PremiumDueDate, ");
        tStrBSql.append(" '1' RealTimeClaimFlag, ");
        tStrBSql.append(" '' ");
        tStrBSql.append(" from LPgrpEdorItem a ");
        tStrBSql.append(" where 1 = 1 and EdorState = '0' ");
//        tStrBSql.append(" and ManageCom like '8611%' ");
        tStrBSql.append(" and trim(a.EdorNo)||'&'||trim(a.EdorType)||'&G' = '" + cDataInfoKey + "' ");
        
        
        
        

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }
//  险种
    private Element reportPolicyUpcoverageLogInfo(String cContNo)
    {
        Element tRoot = new Element("PolicyUpcoverageLogDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.PolicyUpcoverageLogDTO");

        Element tTmpEle = null;

        SSRS tResPolicyUpcoverageLogInfo = loadPolicyUpcoverageLogInfo(cContNo);

        for (int idxPI = 1; idxPI <= tResPolicyUpcoverageLogInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyUpcoverageLogInfo.getRowData(idxPI);

            tTmpEle = new Element("CoverageType");
            tTmpEle.setText(tDataInfo[0]);
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageCode");
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ComCoverageCode");
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageName");
            tTmpEle.setText(tDataInfo[3]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageEffectiveDate");
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageExpireDate");
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoveragePremium");
            tTmpEle.setText(tDataInfo[6]);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为“NUMBER”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageSuminsured");
            tTmpEle.setText(tDataInfo[7]);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为“NUMBER”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageEffectiveSuminsured");
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为“NUMBER”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SpecificBusiness");
            tTmpEle.setText(tDataInfo[9]);
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SpecificBusinessCode");
            tTmpEle.setText(tDataInfo[10]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
//          获取险种下的责任信息
            String tPolNo = tDataInfo[11];            
            tTmpEle = reportPolicyCoverageLogInfo1(tPolNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
            

        }

        return tRoot;
    }
    
//责任
    private Element reportPolicyCoverageLogInfo(String cContNo)
    {
        Element tRoot = new Element("PolicyCoverageLogDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.PolicyCoverageLogDTO");

        Element tTmpEle = null;

        SSRS tResPolicyCoverageLogInfo = loadPolicyCoverageLogInfo(cContNo);

        for (int idxPI = 1; idxPI <= tResPolicyCoverageLogInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyCoverageLogInfo.getRowData(idxPI);

            tTmpEle = new Element("CoverageType");
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageCode");
            tTmpEle.setText(tDataInfo[1]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ComCoverageCode");
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageName");
            tTmpEle.setText(tDataInfo[3]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityClassification");
            tTmpEle.setText(tDataInfo[4]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityCode");
            tTmpEle.setText(tDataInfo[5]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityName");
            tTmpEle.setText(tDataInfo[6]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityEffectiveDate");
            tTmpEle.setText(tDataInfo[7]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityExpireDate");
            tTmpEle.setText(tDataInfo[8]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityStatus");
            tTmpEle.setText(tDataInfo[9]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("InsuredGroupCode");
            tTmpEle.setText(tDataInfo[10]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("GroupName");
            tTmpEle.setText(tDataInfo[11]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LimitAmount");
            tTmpEle.setText(tDataInfo[12]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("EffectivelyAmount");
            tTmpEle.setText(tDataInfo[13]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("WaitingPeriod");
            tTmpEle.setText(tDataInfo[14]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            String tPolNo = tDataInfo[14];
            
//          核保结论
            tTmpEle = reportUnderWritingLogInfo(tPolNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
            
//          赔付比例
            tTmpEle = reportPolicyAmountLogDTOInfo(tPolNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

        }

        return tRoot;
    }
    
//  责任
    private Element reportPolicyCoverageLogInfo1(String cPolNo)
    {
        Element tRoot = new Element("PolicyCoverageLogDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.PolicyCoverageLogDTO");

        Element tTmpEle = null;

        SSRS tResPolicyCoverageLogInfo1 = loadPolicyCoverageLogInfo1(cPolNo);

        for (int idxPI = 1; idxPI <= tResPolicyCoverageLogInfo1.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyCoverageLogInfo1.getRowData(idxPI);

            tTmpEle = new Element("LiabilityClassification");
            tTmpEle.setText(tDataInfo[0]);
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityCode");
            tTmpEle.setText(tDataInfo[1]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityName");
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityEffectiveDate");
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityExpireDate");
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityStatus");
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityPremium");
            tTmpEle.setText(tDataInfo[6]);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为“NUMBER”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LimitAmount");
            tTmpEle.setText(tDataInfo[7]);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为“NUMBER”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("EffectivelyAmount");
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为“NUMBER”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("WaitingPeriod");
            tTmpEle.setText(tDataInfo[9]);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为“NUMBER”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("InsuredGroupCode");
            tTmpEle.setText(tDataInfo[10]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("GroupName");
            tTmpEle.setText(tDataInfo[11]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            String tPolNo = tDataInfo[12];
            
//          核保结论
            tTmpEle = reportUnderWritingLogInfo(tPolNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
            
//          赔付比例
            tTmpEle = reportPolicyAmountLogDTOInfo(tPolNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

        }

        return tRoot;
    }
       

    //得到险种信息
    private SSRS loadPolicyUpcoverageLogInfo(String cContNo)
    {
    	
    	String tContNo = cContNo;
    	
        SSRS tResult = null;
        StringBuffer tStrBSql = new StringBuffer();
        
        
        String gOrI=null;
        if(mDataInfoKey!=null)
        {
        	gOrI=getGOrI(mDataInfoKey);

        }
        
        
//        平台险种类型	COVERAGETYPE	VARCHAR2(1)		Y	《公用代码》1.1.9
//        平台险种代码	COVERAGECODE	VARCHAR2(5)		Y	《公用代码》1.1.10
//        公司险种代码	COMCOVERAGECODE	VARCHAR2(60)			
//        公司险种名称	COVERAGENAME	VARCHAR2(200)			
//        险种起保日期	COVERAGEEFFECTIVEDATE	DATE			
//        险种满期日期	COVERAGEEXPIREDATE	DATE			
//        险种保费	COVERAGEPREMIUM	NUMBER(20,2)			
//        险种保额	COVERAGESUMINSURED	NUMBER(20,2)			
//        险种有效保额	COVERAGEEFFECTIVESUMINSURED	NUMBER(20,2)			
//        特殊业务标识	SPECIFICBUSINESS	VARCHAR2(1)		Y	《公用代码》1.1.32
//        特殊业务代码	SPECIFICBUSINESSCODE	VARCHAR2(5)			《公用代码》1.1.33
//        如果“是否专项业务”为1，该字段必填
//        特别承保责任数组	SPECIALCOVERAGE_ARRAY				详见下面描述

        if(gOrI!=null&&gOrI.equals("I"))
        {  
	        tStrBSql.append(" select ");
	        tStrBSql.append(" (select RiskType1 from lmriskapp where riskcode=lcp.riskcode) CoverageType, (select RiskType from lmriskapp where riskcode=lcp.riskcode) CoverageCode, ");
	        tStrBSql.append(" lcp.RiskCode ComCoverageCode, lmra.RiskName CoverageName, ");
	        tStrBSql.append(" lcp.cvalidate CoverageEffectiveDate,  ");
	        tStrBSql.append(" lcp.enddate CoverageExpireDate,  ");
	        tStrBSql.append(" lcp.prem CoveragePremium,  ");
	        tStrBSql.append(" lcp.amnt CoverageSuminsured,  ");
	        tStrBSql.append(" lcp.amnt CoverageEffectiveSuminsured,  ");
	        tStrBSql.append(" '0' SpecificBusiness,  ");
	        tStrBSql.append(" '' SpecificBusinessCode, "); 
	        tStrBSql.append(" lcp.polno polno ");  
	        tStrBSql.append(" from LCPol lcp ");
	        tStrBSql.append(" inner join LCDuty lcd on lcd.PolNo = lcp.PolNo ");
	        tStrBSql.append(" inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode ");
	        tStrBSql.append(" inner join LMDuty lmd on lmd.DutyCode = lcd.DutyCode ");
	        tStrBSql.append(" where 1 = 1 ");
	        tStrBSql.append(" and lcp.ContNo = '" + tContNo + "' ");
	        
	        tStrBSql.append(" union all  ");
	        
	        tStrBSql.append(" select ");
	        tStrBSql.append(" (select RiskType1 from lmriskapp where riskcode=lcp.riskcode) CoverageType, (select RiskType from lmriskapp where riskcode=lcp.riskcode) CoverageCode, ");
	        tStrBSql.append(" lcp.RiskCode ComCoverageCode, lmra.RiskName CoverageName, ");
	        tStrBSql.append(" lcp.cvalidate CoverageEffectiveDate,  ");
	        tStrBSql.append(" lcp.enddate CoverageExpireDate,  ");
	        tStrBSql.append(" lcp.prem CoveragePremium,  ");
	        tStrBSql.append(" lcp.amnt CoverageSuminsured,  ");
	        tStrBSql.append(" lcp.amnt CoverageEffectiveSuminsured,  ");
	        tStrBSql.append(" '0' SpecificBusiness,  ");
	        tStrBSql.append(" '' SpecificBusinessCode, ");   
	        tStrBSql.append(" lcp.polno polno ");  
	        tStrBSql.append(" from LBPol lcp ");
	        tStrBSql.append(" inner join LBDuty lcd on lcd.PolNo = lcp.PolNo ");
	        tStrBSql.append(" inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode ");
	        tStrBSql.append(" inner join LMDuty lmd on lmd.DutyCode = lcd.DutyCode ");
	        tStrBSql.append(" where 1 = 1 ");
	        tStrBSql.append(" and lcp.ContNo = '" + tContNo + "' ");
        }
        else
        {
        	tStrBSql=new StringBuffer();
        	tStrBSql.append(" select ");
	        tStrBSql.append(" (select RiskType1 from lmriskapp where riskcode=lcp.riskcode) CoverageType, (select RiskType from lmriskapp where riskcode=lcp.riskcode) CoverageCode, ");
	        tStrBSql.append(" lcp.RiskCode ComCoverageCode, lmra.RiskName CoverageName, ");
	        tStrBSql.append(" lcp.cvalidate CoverageEffectiveDate,  ");
	        tStrBSql.append(" lcp.payenddate CoverageExpireDate,  ");
	        tStrBSql.append(" lcp.prem CoveragePremium,  ");
	        tStrBSql.append(" lcp.amnt CoverageSuminsured,  ");
	        tStrBSql.append(" lcp.amnt CoverageEffectiveSuminsured,  ");
	        tStrBSql.append(" '0' SpecificBusiness,  ");
	        tStrBSql.append(" '' SpecificBusinessCode, "); 
	        tStrBSql.append(" lcp.grppolno polno ");  
	        tStrBSql.append(" from LCgrpPol lcp ");
//	        tStrBSql.append(" inner join LCDuty lcd on lcd.PolNo = lcp.PolNo ");
	        tStrBSql.append(" inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode ");
//	        tStrBSql.append(" inner join LMDuty lmd on lmd.DutyCode = lcd.DutyCode ");
	        tStrBSql.append(" where 1 = 1 ");
	        tStrBSql.append(" and lcp.grpContNo = '" + tContNo + "' ");
	        
	        tStrBSql.append(" union all  ");
	        
	        tStrBSql.append(" select ");
	        tStrBSql.append(" (select RiskType1 from lmriskapp where riskcode=lcp.riskcode) CoverageType, (select RiskType from lmriskapp where riskcode=lcp.riskcode) CoverageCode, ");
	        tStrBSql.append(" lcp.RiskCode ComCoverageCode, lmra.RiskName CoverageName, ");
	        tStrBSql.append(" lcp.cvalidate CoverageEffectiveDate,  ");
	        tStrBSql.append(" lcp.payenddate CoverageExpireDate,  ");
	        tStrBSql.append(" lcp.prem CoveragePremium,  ");
	        tStrBSql.append(" lcp.amnt CoverageSuminsured,  ");
	        tStrBSql.append(" lcp.amnt CoverageEffectiveSuminsured,  ");
	        tStrBSql.append(" '0' SpecificBusiness,  ");
	        tStrBSql.append(" '' SpecificBusinessCode, ");   
	        tStrBSql.append(" lcp.grppolno polno ");  
	        tStrBSql.append(" from LBgrpPol lcp ");
//	        tStrBSql.append(" inner join LBDuty lcd on lcd.PolNo = lcp.PolNo ");
	        tStrBSql.append(" inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode ");
//	        tStrBSql.append(" inner join LMDuty lmd on lmd.DutyCode = lcd.DutyCode ");
	        tStrBSql.append(" where 1 = 1 ");
	        tStrBSql.append(" and lcp.grpContNo = '" + tContNo + "' ");
        }

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }
    
    private SSRS loadPolicyCoverageLogInfo(String cContNo)
    {
    	
    	String tContNo = cContNo;
    	
        SSRS tResult = null;
        StringBuffer tStrBSql = new StringBuffer();
        
//        平台险种类型	COVERAGETYPE	VARCHAR2(1)		Y	《公用代码》1.1.9
//        平台险种代码	COVERAGECODE	VARCHAR2(5)		Y	《公用代码》1.1.10
//        公司险种代码	COMCOVERAGECODE	VARCHAR2(60)			
//        公司险种名称	COVERAGENAME	VARCHAR2(200)			
//        平台责任分类	LIABILITYCLASSIFICATION	VARCHAR2(2)		Y	《公用代码》1.1.11
//        责任代码	LIABILITYCODE	VARCHAR2(60)			
//        责任名称	LIABILITYNAME	VARCHAR2(200)			
//        责任开始日期	LIABILITYEFFECTIVEDATE	DATE			
//        责任终止日期	LIABILITYEXPIREDATE	DATE			
//        责任状态	LIABILITYSTATUS	VARCHAR2(2)		Y	《公用代码》1.1.4
//        被保险人属组代码	INSUREDGROUPCODE	VARCHAR2(30)			若上传“承保责任数组”，则该字段不能为空；若上传“特别承保责任数组”，该字段必须为空
//        属组名称	GROUPNAME	VARCHAR2(100)			
//        责任保费	LIABILITYPREMIUM	NUMBER(20,2)	--	契约没有	--哪都没有
//        责任保额	LIMITAMOUNT	NUMBER(20,2)			
//        责任有效保额	EFFECTIVELYAMOUNT	NUMBER(20,2)			保额余额
//        免责期	WAITINGPERIOD	NUMBER(8)			以天为单位计算
//        核保结论数组	UNDERWRITING_ARRAY				详见下面描述
//        赔付比例分段数组	AMOUNT_ARRAY				详见下面描述

        tStrBSql.append(" select ");
        tStrBSql.append(" (select RiskType1 from lmriskapp where riskcode=lcp.riskcode) CoverageType, (select RiskType from lmriskapp where riskcode=lcp.riskcode) CoverageCode, ");
        tStrBSql.append(" lcp.RiskCode ComCoverageCode, lmra.RiskName CoverageName, ");
        tStrBSql.append(" '99' LiabilityClassification, ");
        tStrBSql.append(" lcd.DutyCode LiabilityCode, lmd.DutyName LiabilityName, ");
        tStrBSql.append(" lcd.GetStartDate LiabilityEffectiveDate, (lcd.EndDate - 1 day) LiabilityExpireDate, ");
        tStrBSql.append(" '1' LiabilityStatus, ");
        tStrBSql.append(" lcp.PolNo InsuredGroupCode, '' GroupName, ");
        tStrBSql.append(" lcd.Amnt LimitAmount, '' EffectivelyAmount, ");
        tStrBSql.append(" '0' WaitingPeriod, ");
        tStrBSql.append(" '' PolNo ");
        tStrBSql.append(" from LCPol lcp ");
        tStrBSql.append(" inner join LCDuty lcd on lcd.PolNo = lcp.PolNo ");
        tStrBSql.append(" inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode ");
        tStrBSql.append(" inner join LMDuty lmd on lmd.DutyCode = lcd.DutyCode ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.ContNo = '" + tContNo + "' ");
        tStrBSql.append(" union all  ");
        tStrBSql.append(" select ");
        tStrBSql.append(" (select RiskType1 from lmriskapp where riskcode=lcp.riskcode) CoverageType, (select RiskType from lmriskapp where riskcode=lcp.riskcode) CoverageCode, ");
        tStrBSql.append(" lcp.RiskCode ComCoverageCode, lmra.RiskName CoverageName, ");
        tStrBSql.append(" '99' LiabilityClassification, ");
        tStrBSql.append(" lcd.DutyCode LiabilityCode, lmd.DutyName LiabilityName, ");
        tStrBSql.append(" lcd.GetStartDate LiabilityEffectiveDate, (lcd.EndDate - 1 day) LiabilityExpireDate, ");
        tStrBSql.append(" '1' LiabilityStatus, ");
        tStrBSql.append(" lcp.PolNo InsuredGroupCode, '' GroupName, ");
        tStrBSql.append(" lcd.Amnt LimitAmount, '' EffectivelyAmount, ");
        tStrBSql.append(" '0' WaitingPeriod, ");
        tStrBSql.append(" '' PolNo ");
        tStrBSql.append(" from LBPol lcp ");
        tStrBSql.append(" inner join LBDuty lcd on lcd.PolNo = lcp.PolNo ");
        tStrBSql.append(" inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode ");
        tStrBSql.append(" inner join LMDuty lmd on lmd.DutyCode = lcd.DutyCode ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.ContNo = '" + tContNo + "' ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }
    
    
    //新责任接口
    private SSRS loadPolicyCoverageLogInfo1(String cPolNo)
    {
    	
    	String tPolNo = cPolNo;
    	
        SSRS tResult = null;
        StringBuffer tStrBSql = new StringBuffer();
//        平台责任分类	LIABILITYCLASSIFICATION	VARCHAR2(2)		Y	《公用代码》1.1.11
//        责任代码	LIABILITYCODE	VARCHAR2(60)			
//        责任名称	LIABILITYNAME	VARCHAR2(200)			
//        责任开始日期	LIABILITYEFFECTIVEDATE	DATE			
//        责任终止日期	LIABILITYEXPIREDATE	DATE			
//        责任状态	LIABILITYSTATUS	VARCHAR2(2)		Y	《公用代码》1.1.4       
//        责任保费	LIABILITYPREMIUM	NUMBER(20,2)	       
//        责任保额	LIMITAMOUNT	NUMBER(20,2)			
//        责任有效保额	EFFECTIVELYAMOUNT	NUMBER(20,2)			保额余额
//        免责期	WAITINGPERIOD	NUMBER(8)			以天为单位计算
//        被保险人属组代码	INSUREDGROUPCODE	VARCHAR2(30)			
//        属组名称	GROUPNAME	VARCHAR2(100)			
//        核保结论数组	UNDERWRITING_ARRAY				详见下面描述
//        赔付比例分段数组	AMOUNT_ARRAY				详见下面描述

        
        tStrBSql.append(" select ");               
        tStrBSql.append(" lcd.DutyCode LiabilityClassification, ");
        tStrBSql.append(" lcd.DutyCode LiabilityCode, lmd.DutyName LiabilityName, ");
        tStrBSql.append(" lcd.GetStartDate LiabilityEffectiveDate, (lcd.EndDate - 1 day) LiabilityExpireDate, ");
        tStrBSql.append(" '1' LiabilityStatus, ");        
        tStrBSql.append(" lcp.prem LiabilityPremium, ");        
        tStrBSql.append(" lcd.Amnt LimitAmount, '' EffectivelyAmount, ");
        tStrBSql.append(" '0' WaitingPeriod, ");
        tStrBSql.append(" '' InsuredGroupCode, ");
        tStrBSql.append(" '' GroupName, ");
        tStrBSql.append(" lcp.polno PolNo ");
        tStrBSql.append(" from LCPol lcp ");
        tStrBSql.append(" inner join LCDuty lcd on lcd.PolNo = lcp.PolNo ");
        tStrBSql.append(" inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode ");
        tStrBSql.append(" inner join LMDuty lmd on lmd.DutyCode = lcd.DutyCode ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.PolNo = '" + tPolNo + "' ");
        tStrBSql.append(" union all  ");
        tStrBSql.append(" select ");               
        tStrBSql.append(" lcd.DutyCode LiabilityClassification, ");
        tStrBSql.append(" lcd.DutyCode LiabilityCode, lmd.DutyName LiabilityName, ");
        tStrBSql.append(" lcd.GetStartDate LiabilityEffectiveDate, (lcd.EndDate - 1 day) LiabilityExpireDate, ");
        tStrBSql.append(" '1' LiabilityStatus, ");        
        tStrBSql.append(" lcp.prem LiabilityPremium, ");        
        tStrBSql.append(" lcd.Amnt LimitAmount, '' EffectivelyAmount, ");
        tStrBSql.append(" '0' WaitingPeriod, ");
        tStrBSql.append(" '' InsuredGroupCode, ");
        tStrBSql.append(" '' GroupName ,");
        tStrBSql.append(" lcp.polno PolNo ");
        tStrBSql.append(" from LBPol lcp ");
        tStrBSql.append(" inner join LBDuty lcd on lcd.PolNo = lcp.PolNo ");
        tStrBSql.append(" inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode ");
        tStrBSql.append(" inner join LMDuty lmd on lmd.DutyCode = lcd.DutyCode ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.PolNo = '" + tPolNo + "' ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }
 
    //核保信息
    private Element reportUnderWritingLogInfo(String cPolNo)
    {
        Element tRoot = new Element("UnderWritingLogDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.UnderWritingLogDTO");

        Element tTmpEle = null;

        SSRS tResUnderWritingLogInfo = loadUnderWritingLogInfo(cPolNo);

        for (int idxPI = 1; idxPI <= tResUnderWritingLogInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResUnderWritingLogInfo.getRowData(idxPI);

            tTmpEle = new Element("UnderwritingResult");
            tTmpEle.setText(tDataInfo[0]);
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("UnderwritingDate");
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
        }

        return tRoot;
    }
    

    private SSRS loadUnderWritingLogInfo(String cPolNo)
    {
    	
    	String tPolNo = cPolNo;
    	
        SSRS tResult = null;
        StringBuffer tStrBSql = new StringBuffer();
        
//        核保结论	UNDERWRITINGRESULT	VARCHAR2(2)		Y	《公用代码》1.1.21
//        核保结论日期	UNDERWRITINGDATE	DATE			

        tStrBSql.append(" select ");
        tStrBSql.append(" UWFlag UnderwritingResult, UWDate UnderwritingDate ");
        tStrBSql.append(" from LCPol lcp ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.PolNo = '" + tPolNo + "' ");
        tStrBSql.append(" union all  ");
        tStrBSql.append(" select ");
        tStrBSql.append(" UWFlag UnderwritingResult, UWDate UnderwritingDate ");
        tStrBSql.append(" from LBPol lcp ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.PolNo = '" + tPolNo + "' ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }
    

    private Element reportPolicyAmountLogDTOInfo(String cPolNo)
    {
        Element tRoot = new Element("PolicyAmountLogDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.PolicyAmountLogDTO");

        Element tTmpEle = null;

        for (int idxPI = 1; idxPI <= 1; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            tTmpEle = new Element("ClaimRatio");
            tTmpEle.setText(null);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为“NUMBER”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Costend");
            tTmpEle.setText(null);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为“NUMBER”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("Coststart");
            tTmpEle.setText(null);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为“NUMBER”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Deductible");
            tTmpEle.setText(null);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为“NUMBER”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("SectionNo");
            tTmpEle.setText(null);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
        }

        return tRoot;
    }
    
//  团体投保人数组
    private Element reportPolicyGroupPHLogInfo(String cGrpContNo)
    {
        Element tRoot = new Element("PolicyGroupPHLogDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.PolicyGroupPHLogDTO");

        Element tTmpEle = null;

        SSRS tResPolicyGroupPHLogInfo = loadPolicyGroupPHLogInfo(cGrpContNo);

        for (int idxPI = 1; idxPI <= tResPolicyGroupPHLogInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyGroupPHLogInfo.getRowData(idxPI);
            
            tTmpEle = new Element("Name");
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritType");
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tTmpEle.setText(tDataInfo[1]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritCode");
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SocialSecurityNo");
            tTmpEle.setText(tDataInfo[3]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CompanyNature");
            tTmpEle.setText(tDataInfo[4]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("IndustryClassification");
            tTmpEle.setText(tDataInfo[5]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LegalRepresentative");
            tTmpEle.setText(tDataInfo[6]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Contact");
            tTmpEle.setText(tDataInfo[7]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("NumberOfUnits");
            tTmpEle.setText(tDataInfo[8]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
        }

        return tRoot;
    }
    
    private SSRS loadPolicyGroupPHLogInfo(String cGrpContNo)
    {
    	
        SSRS tResult = null;
        StringBuffer tStrBSql = new StringBuffer();
        
//        名称	代码	数据类型	非空	编码	备注
//        单位名称	NAME	VARCHAR2(100)			
//        团体客户证件类别	CRITTYPE	VARCHAR2(2)		Y	《公用代码》1.1.12
//        团体客户证件号码	CRITCODE	VARCHAR2(30)			
//        社保保险登记证号	SOCIAlSECURITYNO	VARCHAR2(50)			
//        单位性质	COMPANYNATURE	VARCHAR2(3)		Y	《公用代码》1.1.13
//        行业分类	INDUSTRYCLASSIFICATION	VARCHAR2(4)		Y	《公用代码》1.1.14
//        法人代表	LEGALREPRESENTATIVE	VARCHAR2(50)			
//        联系人	CONTACT	VARCHAR2(50)			
//        单位总人数	NUMBEROFUNITS	NUMBER(10)			


        tStrBSql.append(" select ");
        tStrBSql.append(" Name Name, (case when OrganComCode is not null then '2' else '9' end) CritType,  ");
        tStrBSql.append(" (case when OrganComCode is null then '999999999' else OrganComCode end) CritCode, '' SocialSecurityNo, ");
        tStrBSql.append(" '' CompanyNature, '' IndustryClassification, '' LegalRepresentative,'' Contact, ");
        tStrBSql.append(" Peoples NumberOfUnits ");
        tStrBSql.append(" from LCGrpAppnt ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and GrpContNo = '" + cGrpContNo + "' ");
        tStrBSql.append(" union all  ");
        tStrBSql.append(" select ");
        tStrBSql.append(" Name Name, (case when OrganComCode is not null then '2' else '9' end) CritType, ");
        tStrBSql.append(" (case when OrganComCode is null then '999999999' else OrganComCode end) CritCode, '' SocialSecurityNo, ");
        tStrBSql.append(" '' CompanyNature, '' IndustryClassification, '' LegalRepresentative,'' Contact, ");
        tStrBSql.append(" Peoples NumberOfUnits ");
        tStrBSql.append(" from LBGrpAppnt ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and GrpContNo = '" + cGrpContNo + "' ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }
    
//  个人投保人数组
    private Element reportPolicyPersonPHLogInfo(String cContNo)
    {
        Element tRoot = new Element("PolicyPersonPHLogDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.PolicyPersonPHLogDTO");

        Element tTmpEle = null;

        SSRS tResPolicyGroupPHLogInfo = loadPolicyPersonPHLogInfo(cContNo);

        for (int idxPI = 1; idxPI <= tResPolicyGroupPHLogInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyGroupPHLogInfo.getRowData(idxPI);
            
            
            tTmpEle = new Element("Name");
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Gender");
            tTmpEle.setText(tDataInfo[1]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Birthday");
            tTmpEle.addAttribute("CData", "CDate");
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritType");
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tTmpEle.setText(tDataInfo[3]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritCode");
            tTmpEle.setText(tDataInfo[4]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RelationshipWithInsured");
            tTmpEle.setText(tDataInfo[5]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

        }

        return tRoot;
    }
    
    private SSRS loadPolicyPersonPHLogInfo(String cContNo)
    {
    	
        SSRS tResult = null;
        StringBuffer tStrBSql = new StringBuffer();
        
//        名称	代码	数据类型	非空	编码	备注
//        姓名	NAME	VARCHAR2(60)			
//        性别	GENDER	VARCHAR2(1)		Y	《公用代码》1.1.15
//        出生日期	BIRTHDAY	DATE			
//        证件类别	CRITTYPE	VARCHAR2(2)		Y	《公用代码》1.1.16
//        证件号码	CRITCODE	VARCHAR2(30)			
//        与被保险人关系	RELATIONSHIPWITHINSURED	VARCHAR2(2)		Y	《公用代码》1.1.17
			
        tStrBSql.append(" select ");
        tStrBSql.append(" AppntName Name, AppntSex Gender, ");
        tStrBSql.append(" AppntBirthday Birthday, IDType CritType, ");
        tStrBSql.append(" IDNo CritCode, '' RelationshipWithInsured ");
        tStrBSql.append(" from LCAppnt ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and ContNo = '" + cContNo + "' ");
        tStrBSql.append(" union all  ");
        tStrBSql.append(" select ");
        tStrBSql.append(" AppntName Name, AppntSex Gender, ");
        tStrBSql.append(" AppntBirthday Birthday, IDType CritType, ");
        tStrBSql.append(" IDNo CritCode, '' RelationshipWithInsured ");
        tStrBSql.append(" from LBAppnt ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and ContNo = '" + cContNo + "' ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }
    
//  被保人
    private Element reportPolicyInsuredLogInfo(String cContNo)
    {
        Element tRoot = new Element("PolicyInsuredLogDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.PolicyInsuredLogDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInsuredLogInfo = loadPolicyInsuredLogInfo(cContNo);

        for (int idxPI = 1; idxPI <= tResPolicyInsuredLogInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInsuredLogInfo.getRowData(idxPI);
            
            


            tTmpEle = new Element("Name");
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Gender");
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tTmpEle.setText(tDataInfo[1]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Birthday");
            tTmpEle.addAttribute("CData", "CDate");
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritType");
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tTmpEle.setText(tDataInfo[3]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritCode");
            tTmpEle.setText(tDataInfo[4]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RelationshipWithPh");
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tTmpEle.setText(tDataInfo[5]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CustomerNo");
            tTmpEle.setText(tDataInfo[6]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("AnomalyInform");
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tTmpEle.setText(tDataInfo[7]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            


            tTmpEle = new Element("OfferStatus");
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tTmpEle.setText(tDataInfo[8]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SubStartDate");
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tTmpEle.setText(tDataInfo[9]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SubEndDate");
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tTmpEle.setText(tDataInfo[10]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("InsuredGroupCode");
            tTmpEle.setText(tDataInfo[11]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("InsuredType");
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tTmpEle.setText(tDataInfo[12]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SnOfMainInsured");
            tTmpEle.setText(tDataInfo[13]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RelationshipWithMainInsured");
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tTmpEle.setText(tDataInfo[14]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ServiceMark");
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tTmpEle.setText(tDataInfo[15]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("HealthFlag");
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tTmpEle.setText(tDataInfo[16]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("SocialcareNo");
            tTmpEle.setText(tDataInfo[17]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("OccupationalCode");
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tTmpEle.setText(tDataInfo[18]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("WorkPlace");
            tTmpEle.setText(tDataInfo[19]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("EmpoyeeNo");
            tTmpEle.setText(tDataInfo[20]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
          
            String tInsuredNo = tDataInfo[6];
            //获取特别承保险种数组
            tTmpEle = reportPolInsUpCovInfo(cContNo, tInsuredNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

            //获取受益人数组
            tTmpEle = reportPolInsBnfInfo(cContNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
           
            
        }

        return tRoot;
        
    }
  // 从这为粘过来  从契约
    private SSRS loadPolInsBnfInfo(String cContNo)
    {
        SSRS tResult = null;

        StringBuffer tStrBSql = new StringBuffer();

        tStrBSql.append(" select ");
        tStrBSql
                .append(" '' Name, '' Gender, '' Birthday, '' CritType, '' CritCode, '' RelationshipWithInsured, '' BenefitDistribution, '' OrderNo, '' Proportion ");
        tStrBSql.append(" from dual ");
        tStrBSql.append(" where 1 = 2 ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }
    
    private Element reportPolInsBnfInfo(String cContNo)
    {
        Element tRoot = new Element("PolicyBenefitDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.PolicyBenefitDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolInsBnfInfo(cContNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("Name"); //名称   
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Gender"); //性别        
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Birthday"); //出生日期      
            tTmpEle.setText(tDataInfo[2]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritType"); //证件类别      
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritCode"); //证件号码     
            tTmpEle.setText(tDataInfo[4]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RelationshipWithInsured"); //与被保险人关系
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("BenefitDistribution"); //受益分配方式  
            tTmpEle.setText(tDataInfo[6]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("OrderNo"); //顺序号        
            tTmpEle.setText(tDataInfo[7]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Proportion"); //比例值
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
        }

        return tRoot;
    }
    
    private SSRS loadPolInsUpCovInfo(String cContNo, String cInsuredNo)
    {
        SSRS tResult = null;

        StringBuffer tStrBSql = new StringBuffer();

        //        tStrBSql.append(" select ");
        //        tStrBSql
        //                .append(" '' CoverageType, '' CoverageCode, '' ComCoverageCode, '' CoverageName, '' CoverageEffectiveDate, '' CoverageExpireDate, '' CoveragePremium, '' CoverageSuminsured, '' CoverageEffectiveSuminsured, '' SpecificBusiness, '' SpecificBusinessCode ");
        //        tStrBSql.append(" from dual ");
        //        tStrBSql.append(" where 1 = 2 ");

        tStrBSql.append(" select ");
        tStrBSql.append(" '2' CoverageType, lcp.RiskCode CoverageCode, ");
        tStrBSql.append(" lcp.RiskCode ComCoverageCode, lmra.RiskName CoverageName, ");
        tStrBSql.append(" lcp.GetStartDate CoverageEffectiveDate, lcp.EndDate CoverageExpireDate, ");
        tStrBSql.append(" lmra.SubRiskFlag MainAttachedFlag, ");//+
        tStrBSql.append(" lcp.Prem CoveragePremium, ");
        tStrBSql.append(" lcp.Amnt CoverageSuminsured, lcp.Amnt CoverageEffectiveSuminsured, ");
        tStrBSql.append(" lcp.PayIntv PaymentMethod, (Case when lcp.PayIntv is null then '' else char(lcp.PayYears) end) PaymentYears, ");//+
        tStrBSql.append(" '0' SpecificBusiness, '' SpecificBusinessCode, ");
        tStrBSql.append(" lcp.PolNo ");
        tStrBSql.append(" from LCPol lcp ");
        tStrBSql.append(" inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.ContNo = '" + cContNo + "' and lcp.InsuredNo = '" + cInsuredNo + "' ");
        tStrBSql.append(" union all ");
        tStrBSql.append(" select ");
        tStrBSql.append(" '2' CoverageType, lcp.RiskCode CoverageCode, ");
        tStrBSql.append(" lcp.RiskCode ComCoverageCode, lmra.RiskName CoverageName, ");
        tStrBSql.append(" lcp.GetStartDate CoverageEffectiveDate, lcp.EndDate CoverageExpireDate, ");
        tStrBSql.append(" lmra.SubRiskFlag MainAttachedFlag, ");//+
        tStrBSql.append(" lcp.Prem CoveragePremium, ");
        tStrBSql.append(" lcp.Amnt CoverageSuminsured, lcp.Amnt CoverageEffectiveSuminsured, ");
        tStrBSql.append(" lcp.PayIntv PaymentMethod, (Case when lcp.PayIntv is null then '' else char(lcp.PayYears) end) PaymentYears, ");//+
        tStrBSql.append(" '0' SpecificBusiness, '' SpecificBusinessCode, ");
        tStrBSql.append(" lcp.PolNo ");
        tStrBSql.append(" from LCPol lcp ");
        tStrBSql.append(" inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.GrpContNo = '" + cContNo + "' and lcp.InsuredNo = '" + cInsuredNo + "' ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }
    
    private Element reportPolInsUpCovInfo(String cContNo, String cInsuredNo)
    {
        Element tRoot = new Element("SpecialPolicyUpcoverageLogDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.SpecialPolicyUpcoverageLogDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolInsUpCovInfo(cContNo, cInsuredNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("CoverageType"); //平台险种类型
            tTmpEle.setText(tDataInfo[0]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageCode"); //平台险种代码
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ComCoverageCode"); //公司险种代码
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageName"); //公司险种名称
            tTmpEle.setText(tDataInfo[3]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageEffectiveDate"); //险种起保日期
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageExpireDate"); //险种满期日期
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            
            
            tTmpEle = new Element("MainAttachedFlag"); //主附险性质
            tTmpEle.setText(tDataInfo[6]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoveragePremium"); //险种保费    
            tTmpEle.setText(tDataInfo[7]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageSuminsured"); //险种保额    
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            

            tTmpEle = new Element("CoverageEffectiveSuminsured"); //险种有效保额
            tTmpEle.setText(tDataInfo[9]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("PaymentMethod"); //缴费方式
            tTmpEle.setText(tDataInfo[10]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("PaymentYears"); //缴费年限
            tTmpEle.setText(tDataInfo[11]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SpecificBusiness"); //特殊业务标识
            tTmpEle.setText(tDataInfo[12]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SpecificBusinessCode"); //特殊业务代码
            tTmpEle.setText(tDataInfo[13]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            String tPolNo = tDataInfo[14];
            //获取特别核保结论数组
          tTmpEle = reportPolSpeUWInfo(tPolNo);
          if (tTmpEle != null)
          {
              tItemEle.addContent(tTmpEle);
          }
          tTmpEle = null;
            //获取特别承保责任数组
            tTmpEle = reportPolInsCovInfo(tPolNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
        }

        return tRoot;
    }
    
    private SSRS loadPolInsCovInfo(String cPolNo)
    {
        SSRS tResult = null;

        StringBuffer tStrBSql = new StringBuffer();

        //        tStrBSql.append(" select ");
        //        tStrBSql
        //                .append(" '' LiabilityClassification, '' LiabilityCode, '' LiabilityName, '' LiabilityEffectiveDate, '' LiabilityExpireDate, '' LiabilityStatus, '' LiabilityPremium, '' LimitAmount, '' EffectivelyAmount, '' WaitingPeriod ");
        //        tStrBSql.append(" from dual ");
        //        tStrBSql.append(" where 1 = 2 ");

        tStrBSql.append(" select ");
        tStrBSql.append(" lcd.DutyCode LiabilityClassification, ");
        tStrBSql.append(" lcd.DutyCode LiabilityCode, lmd.DutyName LiabilityName, ");
        tStrBSql.append(" lcd.GetStartDate LiabilityEffectiveDate, (lcd.EndDate - 1 day) LiabilityExpireDate, ");
        tStrBSql.append(" lcp.StateFlag LiabilityStatus, lcd.Prem LiabilityPremium, ");
        tStrBSql.append(" lcd.Amnt LimitAmount, lcd.Amnt EffectivelyAmount, ");
        tStrBSql.append(" '0' WaitingPeriod, ");
        // tStrBSql.append(" trim(lcp.ContNo) || '_' || trim(lcp.InsuredNo) InsuredGroupCode, ");
        tStrBSql.append(" '' InsuredGroupCode, ");
        // tStrBSql.append(" trim(lcp.ContNo) || '_' || trim(lcp.InsuredNo) GroupName ");
        tStrBSql.append(" '' GroupName ");
        tStrBSql.append(" from LCPol lcp ");
        tStrBSql.append(" inner join LCDuty lcd on lcd.PolNo = lcp.PolNo ");
        tStrBSql.append(" inner join LMDuty lmd on lmd.DutyCode = lcd.DutyCode ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.polno = '" + cPolNo + "' ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }
    
    private Element reportPolInsCovInfo(String cPolNo)
    {
        Element tRoot = new Element("SpecialPolicyCoverageLogDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.SpecialPolicyCoverageLogDTO");
        tRoot.addAttribute("ForceSyncFun", "setSpecialPolicyCoverageLogDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolInsCovInfo(cPolNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("LiabilityClassification"); //平台责任分类
            tTmpEle.setText(tDataInfo[0]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityCode"); //责任代码    
            tTmpEle.setText(tDataInfo[1]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityName"); //责任名称    
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityEffectiveDate"); //责任开始日期
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityExpireDate"); //责任终止日期
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityStatus"); //责任状态    
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityPremium"); //责任保费    
            tTmpEle.setText(tDataInfo[6]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LimitAmount"); //责任保额    
            tTmpEle.setText(tDataInfo[7]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("EffectivelyAmount"); //责任有效保额
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("WaitingPeriod"); //免责期      
            tTmpEle.setText(tDataInfo[9]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("GroupName"); //属组名称      
            tTmpEle.setText(tDataInfo[11]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            //获取特别核保结论数组
//            tTmpEle = reportPolSpeUWInfo(cPolNo);
//            if (tTmpEle != null)
//            {
//                tItemEle.addContent(tTmpEle);
//            }
//            tTmpEle = null;

            //获取特别赔付比例分段数组
            tTmpEle = reportPolSpeAmInfo(cPolNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

        }

        return tRoot;
    }
    
    private SSRS loadPolSpeUWInfo(String tPolNo)
    {
        SSRS tResult = null;

        StringBuffer tStrBSql = new StringBuffer();

        //        tStrBSql.append(" select ");
        //        tStrBSql.append(" '' UnderwritingResult, '' UnderwritingDate ");
        //        tStrBSql.append(" from dual ");
        //        tStrBSql.append(" where 1 = 2 ");

        tStrBSql.append(" select ");
        tStrBSql.append(" lcp.UwFlag UnderwritingResult, lcp.UwDate UnderwritingDate ");
        tStrBSql.append(" from LCPol lcp ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.PolNo = '" + tPolNo + "' ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }
    
    private Element reportPolSpeUWInfo(String cContNo)
    {
        Element tRoot = new Element("SpecialUnderWritingLogDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.SpecialUnderWritingLogDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolSpeUWInfo(cContNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("UnderwritingResult"); //核保结论
            tTmpEle.setText(tDataInfo[0]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("UnderwritingDate"); //核保结论日期
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
        }

        return tRoot;
    }
    
    private SSRS loadPolSpeAmInfo(String cPolNo)
    {
        SSRS tResult = null;
        StringBuffer tStrBSql = new StringBuffer();
        tStrBSql.append(" select ");
        tStrBSql.append(" RowNumber() over() as SectionNo, ");
        tStrBSql.append(" '' Coststart, '' Costend, cast(lcd.GetLimit as decimal(12,2)) Deductible, cast(lcd.GetRate as decimal(12,2)) ClaimRatio ");
        tStrBSql.append(" from LCDuty lcd ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcd.PolNo = '" + cPolNo + "' ");
        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);
        return tResult;
    }

    private Element reportPolSpeAmInfo(String cContNo)
    {
        Element tRoot = new Element("SpecialPolicyAmountLogDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.SpecialPolicyAmountLogDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolSpeAmInfo(cContNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("SectionNo"); //段序    
            tTmpEle.setText(String.valueOf(idxPI));
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Coststart"); //费用起线
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("Costend"); //费用止线
            tTmpEle.setText(tDataInfo[2]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("Deductible"); //免赔额  
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("ClaimRatio"); //赔付比例
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

        }

        return tRoot;
    }
  //到此为粘过来 从契约  
    
    
    
    private SSRS loadPolicyInsuredLogInfo(String cContNo)
    {
    	
    	String tContNo = cContNo;
    	
        SSRS tResult = null;
        StringBuffer tStrBSql = new StringBuffer();
        
//        名称	代码	数据类型	非空	编码	备注
//        姓名	NAME	VARCHAR2(60)			
//        性别	GENDER	VARCHAR2(1)		Y	《公用代码》1.1.15
//        出生日期	BIRTHDAY	DATE			
//        被保险人证件类别	CRITTYPE	VARCHAR2(2)		Y	《公用代码》1.1.16
//        被保险人证件号码	CRITCODE	VARCHAR2(30)			
//        与投保人关系	RELATIONSHIPWITHPH	VARCHAR2(1)		Y	《公用代码》1.1.17，对于团险被保人，应为“07雇佣”
//        客户编号	CUSTOMERNO	VARCHAR2(30)			在同一团单下的人员序号
//        异常告知	ANOMALYINFORM	VARCHAR2(1)		Y	《公用代码》1.1.32
//        要约状态	OFFERSTATUS	VARCHAR2(2)		Y	《公用代码》1.1.4
//        被保险人责任开始日期	SUBSTARTDATE	DATE			
//        被保险人责任终止日期	SUBENDDATE	DATE			
//        被保险人属组代码	INSUREDGROUPCODE	VARCHAR2(30)			若无承保责任数组，则该字段可为空，但必须提供特别承保责任数组
//        被保险人类型	INSUREDTYPE	VARCHAR2(2)		Y	《公用代码》1.1.18
//        主被保人序号	SNOFMAININSURED	VARCHAR2(30)			
//        与主被保人关系	RELATIONSHIPWITHMAININSURED	VARCHAR2(1)		Y	《公用代码》1.1.17
//        在职标志	SERVICEMARK	VARCHAR2(2)		Y	《公用代码》1.1.19
//        医保标识	HEALTHFLAG	VARCHAR2(1)		Y	《公用代码》1.1.32
//        社保卡号	SOCIALCARENO	VARCHAR2(30)			
//        职业代码	OCCUPATIONALCODE	VARCHAR2(7)		Y	《公用代码》1.1.20
//        工作地点	WORKPLACE	VARCHAR2(60)			
//        工号	EMPOYEENO	VARCHAR2(30)			
//        特别承保险种数组	SPECIALUPCOVERAGE_ARRAY				详见下面描述，若“被保险人属组代码”为非空，则该数组必须为空
//        受益人数组	BENEFIT_ARRAY				详见下面描述


        tStrBSql.append(" select ");
        tStrBSql.append(" Name Name, Sex Gender, ");
        tStrBSql.append(" BirthDay BirthDay, IDType CritType, IDNo CritCode, ");
        tStrBSql.append(" '07' RelationshipWithPh, ");
        tStrBSql.append(" InsuredNo CustomerNo, '0' AnomalyInform, ");
        tStrBSql.append(" (select stateflag from lccont where contno='"+tContNo+" ') OfferStatus, '' SubStartDate, '' SubEndDate, ");
        tStrBSql.append(" '' InsuredGroupCode, '' insuredType, ");
        tStrBSql.append(" '' SnOfMainInsured, RelationToMainInsured RelationshipWithMainInsured, ");
        tStrBSql.append(" '' ServiceMark, '' HealthFlag, ");
        tStrBSql.append(" '' SocialcareNo , OccupationCode OccupationalCode, ");
        tStrBSql.append(" '' WorkPlace, '' EmpoyeeNo ");
        tStrBSql.append(" from LCInsured ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and ContNo = '" + tContNo + "' ");
        tStrBSql.append(" union all  ");
        tStrBSql.append(" select ");
        tStrBSql.append(" Name Name, Sex Gender, ");
        tStrBSql.append(" BirthDay BirthDay, IDType CritType, IDNo CritCode, ");
        tStrBSql.append(" '07' RelationshipWithPh, ");
        tStrBSql.append(" InsuredNo CustomerNo, '0' AnomalyInform, ");
        tStrBSql.append(" (select stateflag from lccont where contno='"+tContNo+" ') OfferStatus, '' SubStartDate, '' SubEndDate, ");
        tStrBSql.append(" '' InsuredGroupCode, '' insuredType, ");
        tStrBSql.append(" '' SnOfMainInsured, RelationToMainInsured RelationshipWithMainInsured, ");
        tStrBSql.append(" '' ServiceMark, '' HealthFlag, ");
        tStrBSql.append(" '' SocialcareNo , OccupationCode OccupationalCode, ");
        tStrBSql.append(" '' WorkPlace, '' EmpoyeeNo ");
        tStrBSql.append(" from LBInsured ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and ContNo = '" + tContNo + "' ");
        
        tStrBSql.append(" union all  ");
        tStrBSql.append(" select ");
        tStrBSql.append(" Name Name, Sex Gender, ");
        tStrBSql.append(" BirthDay BirthDay, IDType CritType, IDNo CritCode, ");
        tStrBSql.append(" '07' RelationshipWithPh, ");
        tStrBSql.append(" InsuredNo CustomerNo, '0' AnomalyInform, ");
        tStrBSql.append(" (select stateflag from lccont where contno='"+tContNo+" ') OfferStatus, '' SubStartDate, '' SubEndDate, ");
        tStrBSql.append(" '' InsuredGroupCode, '' insuredType, ");
        tStrBSql.append(" '' SnOfMainInsured, RelationToMainInsured RelationshipWithMainInsured, ");
        tStrBSql.append(" '' ServiceMark, '' HealthFlag, ");
        tStrBSql.append(" '' SocialcareNo , OccupationCode OccupationalCode, ");
        tStrBSql.append(" '' WorkPlace, '' EmpoyeeNo ");
        tStrBSql.append(" from LCInsured ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and GRPContNo = '" + tContNo + "' ");

        //是增人的修改为增人的人
        String itemNI=null;
        String edorNo=null;
        String gOrIFlag=null;
        if(mDataInfoKey!=null)
        {
          itemNI=getItem(mDataInfoKey) ; 
          edorNo=getEdorNo(mDataInfoKey);
          gOrIFlag=getGOrI(mDataInfoKey);
        }
        if(gOrIFlag!=null&&gOrIFlag.equals("G")&&(itemNI!=null&&(itemNI.equals("ZT"))))
        {
        	 tStrBSql=new StringBuffer();
        	 tStrBSql.append(" select ");
             tStrBSql.append(" Name Name, Sex Gender, ");
             tStrBSql.append(" BirthDay BirthDay, IDType CritType, IDNo CritCode, ");
             tStrBSql.append(" '07' RelationshipWithPh, ");
             tStrBSql.append(" InsuredNo CustomerNo, '0' AnomalyInform, ");
             tStrBSql.append(" '3' OfferStatus, '' SubStartDate, (select cinvalidate from lbcont where contno in (Select contno from lbinsured where edorno=a.edorno)  fetch first 1 rows only) SubEndDate, ");
             tStrBSql.append(" '' InsuredGroupCode, '' insuredType, ");
             tStrBSql.append(" '' SnOfMainInsured, RelationToMainInsured RelationshipWithMainInsured, ");
             tStrBSql.append(" '' ServiceMark, '' HealthFlag, ");
             tStrBSql.append(" '' SocialcareNo , OccupationCode OccupationalCode, ");
             tStrBSql.append(" '' WorkPlace, '' EmpoyeeNo ");
             tStrBSql.append(" from LBInsured a ");
             tStrBSql.append(" where 1 = 1 ");
             tStrBSql.append(" and GRPContNo = '" + tContNo + "' and edorno='"+edorNo+"' ");
        }
        if(itemNI!=null&&itemNI.equals("NI"))
        {
//        	 tStrBSql=new StringBuffer();
//        	 tStrBSql.append(" select ");
//             tStrBSql.append(" insuredName Name, Sex Gender, ");
//             tStrBSql.append(" BirthDay BirthDay, IDType CritType, IDNo CritCode, ");
//             tStrBSql.append(" '07' RelationshipWithPh, ");
//             tStrBSql.append(" InsuredNo CustomerNo, '1' AnomalyInform, ");
//             tStrBSql.append(" '1' OfferStatus, '' SubStartDate, '' SubEndDate, ");
//             tStrBSql.append(" '' InsuredGroupCode, '1' insuredType, ");
//             tStrBSql.append(" '' SnOfMainInsured, Relation RelationshipWithMainInsured, ");
//             tStrBSql.append(" '' ServiceMark, '' HealthFlag, ");
//             tStrBSql.append(" '' SocialcareNo , '' OccupationalCode, ");
//             tStrBSql.append(" '' WorkPlace, '' EmpoyeeNo ");
//             tStrBSql.append(" from lcinsuredlist ");
//             tStrBSql.append(" where 1 = 1 ");
//             tStrBSql.append(" and grpContNo = '" + tContNo + "' and edorno='"+edorNo+"' ");
        	 tStrBSql=new StringBuffer();
        	 tStrBSql.append(" select ");
             tStrBSql.append(" Name Name, Sex Gender, ");
             tStrBSql.append(" BirthDay BirthDay, IDType CritType, IDNo CritCode, ");
             tStrBSql.append(" '07' RelationshipWithPh, ");
             tStrBSql.append(" InsuredNo CustomerNo, '0' AnomalyInform, ");
             tStrBSql.append(" '1' OfferStatus, '' SubStartDate, '' SubEndDate, ");
             tStrBSql.append(" '' InsuredGroupCode, Relationtomaininsured  insuredType, ");
             tStrBSql.append(" '' SnOfMainInsured, RelationToMainInsured RelationshipWithMainInsured, ");
             tStrBSql.append(" '' ServiceMark, '' HealthFlag, ");
//             tStrBSql.append(" '' SocialcareNo , OccupationCode OccupationalCode, ");
             tStrBSql.append(" '' SocialcareNo , '0001001' OccupationalCode, ");
             tStrBSql.append(" '' WorkPlace, '' EmpoyeeNo ");
             tStrBSql.append(" from LcInsured a ");
             tStrBSql.append(" where 1 = 1 ");
             tStrBSql.append(" and GRPContNo = '" + tContNo + "'  ");
             tStrBSql.append(" and exists (select contno from ljagetendorse where endorsementno='"+edorNo+"' and feeoperationtype='NI' and a.contno=contno group by contno)");
            
        }
        //客户资料变更只报送改变人的信息
        else if(itemNI!=null&&itemNI.equals("CM"))
        {
        	 tStrBSql=new StringBuffer();
        	 tStrBSql.append(" select ");
             tStrBSql.append(" Name Name, Sex Gender, ");
             tStrBSql.append(" BirthDay BirthDay, IDType CritType, IDNo CritCode, ");
             tStrBSql.append(" '07' RelationshipWithPh, ");
             tStrBSql.append(" InsuredNo CustomerNo, '0' AnomalyInform, ");
             tStrBSql.append(" (select stateflag from lccont where contno=a.contno) OfferStatus, '' SubStartDate, '' SubEndDate, ");
             tStrBSql.append(" '' InsuredGroupCode, '' insuredType, ");
             tStrBSql.append(" '' SnOfMainInsured, RelationToMainInsured RelationshipWithMainInsured, ");
             tStrBSql.append(" '' ServiceMark, '' HealthFlag, ");
             tStrBSql.append(" '' SocialcareNo , OccupationCode OccupationalCode, ");
             tStrBSql.append(" '' WorkPlace, '' EmpoyeeNo ");
             tStrBSql.append(" from LpInsured a ");
             tStrBSql.append(" where 1 = 1 ");
             tStrBSql.append(" and edorno = '" + edorNo + "' ");
            
        }
        //受益人变更只报送受益人的信息
        else if(itemNI!=null&&itemNI.equals("BC"))
        {
        	 tStrBSql=new StringBuffer();
        	 tStrBSql.append(" select ");
             tStrBSql.append(" Name Name, Sex Gender, ");
             tStrBSql.append(" BirthDay BirthDay, IDType CritType, IDNo CritCode, ");
             tStrBSql.append(" '07' RelationshipWithPh, ");
             tStrBSql.append(" InsuredNo CustomerNo, '0' AnomalyInform, ");
             tStrBSql.append(" (select stateflag from lccont where contno=a.contno) OfferStatus, '' SubStartDate, '' SubEndDate, ");
             tStrBSql.append(" '' InsuredGroupCode, '' insuredType, ");
             tStrBSql.append(" '' SnOfMainInsured, RelationToInsured RelationshipWithMainInsured, ");
             tStrBSql.append(" '' ServiceMark, '' HealthFlag, ");
             tStrBSql.append(" '' SocialcareNo , '' OccupationalCode, ");
             tStrBSql.append(" '' WorkPlace, '' EmpoyeeNo ");
             tStrBSql.append(" from LcBnf a ");
             tStrBSql.append(" where 1 = 1 ");
             tStrBSql.append(" and contno = '" + tContNo + "' ");
            
        }
        
        
        
        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }
    //得到工单号
    private String getEdorNo(String s)
	{
		String[] t=null;
		t=s.split("&");
		return t[0];
	}
    //得到个或团的标志
    private String getGOrI(String s)
	{
		String[] t=null;
		t=s.split("&");
		return t[t.length-1];
	}
    /**
     * 调用前置机服务
     * @return
     */
    private boolean calService()
    {
        Document tDocument = formJDocMessage(mRoot, "02", mDataInfoKey);
        JdomUtil.print(tDocument);

        Document tInStdXmlDoc = tDocument;

        ReqJKXPreService tJKXPreServlet = new ReqJKXPreService();
        Document tReceiveXmlDoc = tJKXPreServlet.callService(tInStdXmlDoc);
        JdomUtil.print(tReceiveXmlDoc);

        return true;
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        System.out.println(szFunc + ": " + szErrMsg);

        CError cError = new CError();
        cError.moduleName = "BQJKXServiceImpBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    
    public static void main(String [] args)
    {
//    	String tDataInfoKey = "20101111000005&CM&G";
//    	String tDataInfoKey = "20101119000002&NI&G";
//    	String tDataInfoKey = "20060817000010&CM&G";//修改多个人
//    	String tDataInfoKey = "20110119000011&AC&G";
//    	String tDataInfoKey = "20110120000002&NI&G";//增人测试完毕            00003031000002
//    	String tDataInfoKey = "20110126000011&ZT&G";//减人测试完毕            00003031000002
//    	String tDataInfoKey = "20110126000012&AC&G";//投保单位资料变更测试完毕 00003031000002
//    	String tDataInfoKey = "20110126000013&CT&G";//团单解约测试完毕        00003031000003
//    	String tDataInfoKey = "20110401000006&AC&G";//投保单位资料变更测试完毕  00003031000003
    	String tDataInfoKey = "20110920000573&NI&G";//增人  正式
    	
    	TransferData tTransferData = new TransferData();
    	tTransferData.setNameAndValue("DataInfoKey", tDataInfoKey);
    	VData tVData = new VData();
    	tVData.add(tTransferData);

    	new BQJKXServiceImp().callJKXService(tVData, "");
    }
}
