/**
 * 2010-11-18
 */
package com.sinosoft.lis.jkxpt.qy;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.jkxpt.JKXService;
import com.sinosoft.lis.jkxpt.ReqJKXPreService;
import com.sinosoft.lis.yibaotong.JdomUtil;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class QYJKXTwoGrpServiceImp extends JKXService
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private TransferData mTransferData = null;

    /** 数据操作字符串 */
    private String mOperate = "";

    private String mDataInfoKey = null;

    private Element mRoot = null;

    public boolean callJKXService(VData cInputData, String cOperate)
    {
        // 获取入口参数 
        if (!getInputData(cInputData, cOperate))
        {
            return false;
        }
        // --------------------

        // 取数逻辑处理        
        //        mRoot = dealBusiXml(mPolicyNo);
        mRoot = reportBusiXml(mDataInfoKey);
        // --------------------

        // 调用前置机
        if (!calService())
        {
            return false;
        }
        // -------------------- 

        return true;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mDataInfoKey = (String) mTransferData.getValueByName("DataInfoKey");
        if (mDataInfoKey == null || mDataInfoKey.equals(""))
        {
            buildError("getInputData", "获取保单合同号码失败。");
            return false;
        }

        return true;
    }

    private Element reportBusiXml(String cDataInfoKey)//保单详细信息
    {
        Element tRoot = new Element("PolicyDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.PolicyDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolicyInfo(cDataInfoKey);
        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String tContNo = null;
            String tContType = null;

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);
            //start -------------------------
            tTmpEle = new Element("SplitPolicyNo");
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            //end --------------------------
            tTmpEle = new Element("Rejection");
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ApplicationFormNo");
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PolicyNo");
            tContNo = tDataInfo[3];
            tTmpEle.setText(tDataInfo[3]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ApplicationDate");
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            //start -------------------------
            
            tTmpEle = new Element("AcceptDate");
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("OrganId");
            tTmpEle.setText(tDataInfo[6]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("OrganName");
            tTmpEle.setText(tDataInfo[7]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("ChannelType");
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("SalesType");
            tTmpEle.setText(tDataInfo[9]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("SalesCode");
            tTmpEle.setText(tDataInfo[10]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("SalesName");
            tTmpEle.setText(tDataInfo[11]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("SalesChannelCode");
            tTmpEle.setText(tDataInfo[12]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("SalesChannelName");
            tTmpEle.setText(tDataInfo[13]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("BusinessAddress");
            tTmpEle.setText(tDataInfo[14]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            //end -------------------------
            tTmpEle = new Element("ContractType");
            tContType = tDataInfo[15];
            tTmpEle.setText(tDataInfo[15]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

//            tTmpEle = new Element("GroupMark");
//            // tTmpEle.setText(tDataInfo[5]);
//            tItemEle.addContent(tTmpEle);
//            tTmpEle = null;

            tTmpEle = new Element("PolicyStartDate");
            tTmpEle.setText(tDataInfo[16]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PolicyEndDate");
            tTmpEle.setText(tDataInfo[17]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("TerminationDate");
            tTmpEle.setText(tDataInfo[18]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SuspendDate");
            tTmpEle.setText(tDataInfo[19]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RecoverDate");
            tTmpEle.setText(tDataInfo[20]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ContractSource");
            tTmpEle.setText(tDataInfo[21]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ContractNo");
            tTmpEle.setText(tDataInfo[22]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RenewalTimes");
            tTmpEle.setText(tDataInfo[23]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RenewalMethod");
            tTmpEle.setText(tDataInfo[24]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

//            tTmpEle = new Element("MainAttachedFlag");
//            // tTmpEle.setText(tDataInfo[15]);
//            // tTmpEle.addAttribute("CData", "CBusCode");
//            tItemEle.addContent(tTmpEle);
//            tTmpEle = null;

            tTmpEle = new Element("ContractStatus");
            tTmpEle.setText(tDataInfo[26]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("TerminationReason");
            tTmpEle.setText(tDataInfo[27]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SumInsured");
            tTmpEle.setText(tDataInfo[28]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("EffectiveSumInsured");
            tTmpEle.setText(tDataInfo[29]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Premium");
            tTmpEle.setText(tDataInfo[30]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CurrentPremium");
            tTmpEle.setText(tDataInfo[31]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PaymentMethod");
            tTmpEle.setText(tDataInfo[32]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PaymentYears");
            tTmpEle.setText(tDataInfo[33]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            //start ----------------------------
            tTmpEle = new Element("PaymentNo");
            tTmpEle.setText(tDataInfo[34]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            //end ---------------------------------
            tTmpEle = new Element("PolicyHolderPro");
            tTmpEle.setText(tDataInfo[35]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PolicyHolderNum");
            tTmpEle.setText(tDataInfo[36]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("EffectiveInsuredNum");
            tTmpEle.setText(tDataInfo[37]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("FormerpolicyNo");
            tTmpEle.setText(tDataInfo[38]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SpecialRemark");
            tTmpEle.setText(tDataInfo[39]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RegularClearingMark");
            tTmpEle.setText(tDataInfo[40]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

/*            tTmpEle = new Element("RegularClearing");  //modify by zhangyige 2012-12-28 不需上传
            tTmpEle.setText(tDataInfo[41]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
*/
            tTmpEle = new Element("RegularClearingDate");
            tTmpEle.setText(tDataInfo[42]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

/*            tTmpEle = new Element("PremiumDueDate");
            tTmpEle.setText(tDataInfo[43]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;*/

            tTmpEle = new Element("RealTimeClaimFlag");
            tTmpEle.setText(tDataInfo[44]);
//            tTmpEle.addAttribute("CData", "CBusCode");//zhangyige 2012-12-28 传固定值 否
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            //start --------------------------
            
            tTmpEle = new Element("PolicyLoan");
            tTmpEle.setText(tDataInfo[45]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("AutoPaidUp");
            tTmpEle.setText(tDataInfo[46]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("CoInsurance");
            tTmpEle.setText(tDataInfo[47]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("LeadCoInsurance");
            tTmpEle.setText(tDataInfo[48]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            //end --------------------------
//            if("-1".equals(tDataInfo[32])){
//            	tDataInfo[17]-tDataInfo[16]
//            	
//            }else{
	            //          当前年化保费
	            tTmpEle = new Element("YearPremium");   
	            tTmpEle.setText(tDataInfo[49]);
	            tTmpEle.addAttribute("CData", "CBigDecimal");  
	            tItemEle.addContent(tTmpEle);
	            tTmpEle = null;
          //  }
            // 获取责任信息
            tTmpEle = reportPolUpCovInfo(tContNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

            if (tContType.equals("2"))
            {
                //获取团体投保人数组
                tTmpEle = reportPolGrpInfo(tContNo);
                if (tTmpEle != null)
                {
                    tItemEle.addContent(tTmpEle);
                }
                tTmpEle = null;
            }

            //被保险人数组
            tTmpEle = reportPolInsInfo(tContNo, tContType);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

            // --------------------

        }

        return tRoot;
    }

    private Element reportPolUpCovInfo(String cContNo)//承保‘ 险种’  数组中详细信息 （已经注释）
    {
        Element tRoot = new Element("PolicyUpcoverageDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.PolicyUpcoverageDTO");

        Element tTmpEle = null;
        String tPolNo = "";

//        SSRS tResPolicyInfo = loadPolUpCovInfo(cContNo);
//
//        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
//        {
//            Element tItemEle = new Element("Item");
//            tRoot.addContent(tItemEle);
//
//            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);
//
//            tTmpEle = new Element("CoverageType"); //平台险种类型
//            tTmpEle.setText(tDataInfo[0]);
//            tTmpEle.addAttribute("CData", "CBusCode");
//            tItemEle.addContent(tTmpEle);
//            tTmpEle = null;
//
//            tTmpEle = new Element("CoverageCode"); //平台险种代码
//            tTmpEle.setText(tDataInfo[1]);
//            tTmpEle.addAttribute("CData", "CBusCode");
//            tItemEle.addContent(tTmpEle);
//            tTmpEle = null;
//
//            tTmpEle = new Element("ComCoverageCode"); //公司险种代码
//            tTmpEle.setText(tDataInfo[2]);
//            tItemEle.addContent(tTmpEle);
//            tTmpEle = null;
//
//            tTmpEle = new Element("CoverageName"); //公司险种名称
//            tTmpEle.setText(tDataInfo[3]);
//            tItemEle.addContent(tTmpEle);
//            tTmpEle = null;
//
//            tTmpEle = new Element("CoverageEffectiveDate"); //险种生效日期
//            tTmpEle.setText(tDataInfo[4]);
//            tTmpEle.addAttribute("CData", "CDate");
//            tItemEle.addContent(tTmpEle);
//            tTmpEle = null;
//
//            tTmpEle = new Element("CoverageExpireDate"); //险种满期日期
//            tTmpEle.setText(tDataInfo[5]);
//            tTmpEle.addAttribute("CData", "CDate");
//            tItemEle.addContent(tTmpEle);
//            tTmpEle = null;
//
//            tTmpEle = new Element("MainAttachedFlag"); //主附险性质
//            tTmpEle.setText(tDataInfo[6]);
//            tTmpEle.addAttribute("CData", "CBusCode");
//            tItemEle.addContent(tTmpEle);
//            tTmpEle = null;
//
//            tTmpEle = new Element("CoveragePremium"); //险种期初保费
//            tTmpEle.setText(tDataInfo[7]);
//            tTmpEle.addAttribute("CData", "CBigDecimal");
//            tItemEle.addContent(tTmpEle);
//            tTmpEle = null;
//            
//            //start ----------------------------
//            tTmpEle = new Element("CoverageCurrentPremium"); //险种当前保费
//            tTmpEle.setText(tDataInfo[8]);
//            tTmpEle.addAttribute("CData", "CBigDecimal");
//            tItemEle.addContent(tTmpEle);
//            tTmpEle = null;
//            //end ----------------------------
//            
//            tTmpEle = new Element("CoverageSuminsured"); //险种保额 
//            tTmpEle.setText(tDataInfo[9]);
//            tTmpEle.addAttribute("CData", "CBigDecimal");
//            tItemEle.addContent(tTmpEle);
//            tTmpEle = null;
//
//            tTmpEle = new Element("CoverageEffectiveSuminsured"); //险种有效保额
//            tTmpEle.setText(tDataInfo[10]);
//            tTmpEle.addAttribute("CData", "CBigDecimal");
//            tItemEle.addContent(tTmpEle);
//            tTmpEle = null;
//
//            tTmpEle = new Element("PaymentMethod"); //缴费方式
//            tTmpEle.setText(tDataInfo[11]);
//            tTmpEle.addAttribute("CData", "CBusCode");
//            tItemEle.addContent(tTmpEle);
//            tTmpEle = null;
//
//            tTmpEle = new Element("PaymentYears"); //缴费年限
//            tTmpEle.setText(tDataInfo[12]);
//            tTmpEle.addAttribute("CData", "CBigDecimal");
//            tItemEle.addContent(tTmpEle);
//            tTmpEle = null;
//            
//            //start ---------------------------
//            tTmpEle = new Element("PaymentNo"); //缴费次数
//            tTmpEle.setText(tDataInfo[13]);
//            tTmpEle.addAttribute("CData", "CBigDecimal");
//            tItemEle.addContent(tTmpEle);
//            tTmpEle = null;
//            //end ---------------------------
//                        
//            tTmpEle = new Element("SpecificBusiness"); //特殊业务标识
//            tTmpEle.setText(tDataInfo[14]);
//            tTmpEle.addAttribute("CData", "CBusCode");
//            tItemEle.addContent(tTmpEle);
//            tTmpEle = null;
//
//            tTmpEle = new Element("SpecificBusinessCode"); //特殊业务代码
//            tTmpEle.setText(tDataInfo[15]);
//            tTmpEle.addAttribute("CData", "CBusCode");
//            tItemEle.addContent(tTmpEle);
//            tTmpEle = null;
//
//            //start ----------------------
//            tTmpEle = new Element("CoverageStatus"); //险种状态
//            tTmpEle.setText(tDataInfo[16]);
//            tItemEle.addContent(tTmpEle);
//            tTmpEle = null;
//            //end ----------------------
//            
//            tPolNo = tDataInfo[17];
//            
//
//            //获取承保责任数组
//            tTmpEle = reportPolCovInfo(tPolNo);
//            if (tTmpEle != null)
//            {
//                tItemEle.addContent(tTmpEle);
//            }
//            tTmpEle = null;
//            //获取核保结论数组
//            tTmpEle = reportPolUWInfo(tPolNo);
//            if (tTmpEle != null)
//            {
//                tItemEle.addContent(tTmpEle);
//            }
//            tTmpEle = null;
//        }

        return tRoot;
    }

    private Element reportPolCovInfo(String cPolNo)//承保 ‘ 责任 ’ 数组中的详细信息
    {
        Element tRoot = new Element("PolicyCoverageDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.PolicyCoverageDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolCovInfo(cPolNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("LiabilityClassification"); //平台责任分类    
            tTmpEle.setText(tDataInfo[0]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityCode"); //责任代码        
            tTmpEle.setText(tDataInfo[1]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityName"); //责任名称        
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityEffectiveDate"); //责任开始日期    
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityExpireDate"); //责任终止日期    
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityStatus"); //责任状态     
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityPremium"); //起初责任保费        
            tTmpEle.setText(tDataInfo[6]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            //start -----------------------
            tTmpEle = new Element("LiabilityCurrentPremium"); //当前责任保费        
            tTmpEle.setText(tDataInfo[7]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            //end -----------------------
            
            tTmpEle = new Element("LimitAmount"); //责任保额        
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("EffectivelyAmount"); //责任有效保额    
            tTmpEle.setText(tDataInfo[9]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("WaitingPeriod"); //免责期          
            tTmpEle.setText(tDataInfo[10]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("InsuredGroupCode"); //被保险人属组代码
            tTmpEle.setText(tDataInfo[11]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("GroupName"); //属组名称     
            tTmpEle.setText(tDataInfo[12]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityDayAmount"); //责任期初日额保险金    
            tTmpEle.setText(tDataInfo[13]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("LiabilitydayEffectiveAmount"); //责任有效日额保险金 
            tTmpEle.setText(tDataInfo[14]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            //获取赔付比例分段数组
            tTmpEle = reportPolAmoInfo(cPolNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

        }

        return tRoot;
    }

    private Element reportPolAmoInfo(String cPolNo)
    {
        Element tRoot = new Element("PolicyAmountDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.PolicyAmountDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolAmoInfo();

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("SectionNo"); //段序
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Coststart"); //费用起线
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Costend"); //费用止线
            tTmpEle.setText(tDataInfo[2]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Deductible"); //免赔额
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ClaimRatio"); //赔付比例
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
        }

        return tRoot;
    }

    private Element reportPolUWInfo(String cPolNo)
    {
        Element tRoot = new Element("UnderWritingDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.UnderWritingDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolUWInfo(cPolNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("UnderwritingResult"); //核保结论
            tTmpEle.setText(tDataInfo[0]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("UnderwritingDate"); //核保结论日期
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            //start ---------------------
            
            tTmpEle = new Element("DeclinedReason"); //拒保原因
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("State"); //业务流程状态
            tTmpEle.setText(tDataInfo[3]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("PhysicalExamination"); //是否体检
            tTmpEle.setText(tDataInfo[4]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            //end ---------------------
        }

        return tRoot;
    }

    private Element reportPolGrpInfo(String cContNo)
    {
        Element tRoot = new Element("PolicyGroupphDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.PolicyGroupphDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolGrpInfo(cContNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);
            tTmpEle = new Element("Name"); //单位名称   
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            //start -------------------
            
            tTmpEle = new Element("Address1"); //联系地址（省级）
            tTmpEle.setText(tDataInfo[1]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("Address2"); //联系地址（地级）
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("Address3"); //联系地址区（县级）
            tTmpEle.setText(QYJKXTwoServiceImp.getTransAddress(tDataInfo[3]));
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("Address4"); //具体联系地址
            if(tDataInfo[4].equals("")){
            	tTmpEle.setText("北京市西城区阜成门外大街7号国投大厦10层");
            }else{
            	tTmpEle.setText(tDataInfo[4]);
            }
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            //end -------------------
            
            tTmpEle = new Element("CritType"); //团体客户证件类型
            tTmpEle.setText(tDataInfo[5]);
//            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritCode"); //团体客户证件号码
            tTmpEle.setText(tDataInfo[6]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SocialSecurityNo"); //社保保险登记证号
            if(Integer.parseInt(tDataInfo[13])>0){
            	tTmpEle.setText(tDataInfo[7]);
            }else{
            	tTmpEle.setText("");
            }
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CompanyNature"); //单位性质       
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("IndustryClassification"); //行业分类     
            tTmpEle.setText(tDataInfo[9]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LegalRepresentative"); //法人代表        
            tTmpEle.setText(tDataInfo[10]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Contact"); //联系人      
            tTmpEle.setText(tDataInfo[11]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("NumberOfUnits"); //单位总人数     
            tTmpEle.setText(tDataInfo[12]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
        }

        return tRoot;
    }

    private Element reportPolPerInfo(String cContNo)
    {
        Element tRoot = new Element("PolicyPersonphDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.PolicyPersonphDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolPerInfo(cContNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("Name"); //姓名  
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Gender"); //性别       
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Birthday"); //出生日期      
            tTmpEle.setText(tDataInfo[2]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritType"); //证件类别      
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritCode"); //证件号码      
            tTmpEle.setText(tDataInfo[4]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RelationshipWithInsured"); //与被保险人关系
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
        }

        return tRoot;
    }

    private Element reportPolInsInfo(String cContNo, String cContType)
    {
        Element tRoot = new Element("PolicyInsuredDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.PolicyInsuredDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolInsInfo(cContNo, cContType);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);
            
            tTmpEle = new Element("Name"); //姓名    
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Gender"); //性别       
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Birthday"); //出生日期        
            tTmpEle.setText(tDataInfo[2]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritType"); //被保险人证件类别    
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritCode"); //被保险人证件号码   
            tTmpEle.setText(tDataInfo[4]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RelationshipWithPh"); //与投保人关系        
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CustomerNo"); //客户编号          
            tTmpEle.setText(tDataInfo[6]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("AnomalyInform"); //异常告知          
            tTmpEle.setText(tDataInfo[7]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("OfferStatus"); //要约状态            
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SubStartDate"); //被保险人责任开始日期
            tTmpEle.setText(tDataInfo[9]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SubEndDate"); //被保险人责任终止日期
            tTmpEle.setText(tDataInfo[10]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("InsuredGroupCode"); //被保险人属组代码  
            tTmpEle.setText(tDataInfo[11]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("InsuredType"); //被保险人类型    
            if(tDataInfo[12].equals("")){
            	tTmpEle.setText("09"); //关系为其他
            }else{
            	tTmpEle.setText(tDataInfo[12]);
                tTmpEle.addAttribute("CData", "CBusCode");
            }
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SnOfMainInsured"); //主被保人序号  
            tTmpEle.setText(tDataInfo[13]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RelationshipWithMainInsured"); //与主被保人关系 
            tTmpEle.setText(tDataInfo[14]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ServiceMark"); //在职标志    
            tTmpEle.setText(tDataInfo[15]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("HealthFlag"); //医保标识          
            tTmpEle.setText(tDataInfo[16]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SocialcareNo"); //社保卡号  
            tTmpEle.setText(tDataInfo[17]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("OccupationalCode"); //职业代码 
            //zyg 职业代码采用固定值方式，采用平台代码“0001001”上传，内勤人员
            tTmpEle.setText("0001001");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("WorkPlace"); //工作地点     
            tTmpEle.setText(tDataInfo[19]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("EmpoyeeNo"); //工号        
            tTmpEle.setText(tDataInfo[20]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
           
            //start -----------------------
            tTmpEle = new Element("AppointedBenefit"); //是否指定受益人 
            tTmpEle.setText(tDataInfo[21]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            //end -----------------------

            String tInsuredNo = tDataInfo[6];
            //获取特别承保险种数组
            tTmpEle = reportPolInsUpCovInfo(cContNo, tInsuredNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
	        if(!tDataInfo[21].equals("0")){
	            //获取受益人数组
	            tTmpEle = reportPolInsBnfInfo(cContNo);
	            if (tTmpEle != null)
	            {
	                tItemEle.addContent(tTmpEle);
	            }
	            tTmpEle = null;
	        }
        }

        return tRoot;
    }

    private Element reportPolInsUpCovInfo(String cContNo, String cInsuredNo)//特别承保 ‘险种 ’数组中详细信息
    {
        Element tRoot = new Element("SpecialPolicyUpcoverageDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.SpecialPolicyUpcoverageDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolInsUpCovInfo(cContNo, cInsuredNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("CoverageType"); //平台险种类型
            tTmpEle.setText(tDataInfo[0]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageCode"); //平台险种代码
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ComCoverageCode"); //公司险种代码
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageName"); //公司险种名称
            tTmpEle.setText(tDataInfo[3]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageEffectiveDate"); //险种起保日期
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageExpireDate"); //险种满期日期
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("MainAttachedFlag"); //主附险性质
            tTmpEle.setText(tDataInfo[6]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoveragePremium"); //险种保费    
            tTmpEle.setText(tDataInfo[7]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            //start ----------------------------
            tTmpEle = new Element("CoverageCurrentPremium"); //险种当前保费
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            //end ----------------------------
            
            tTmpEle = new Element("CoverageSuminsured"); //险种保额    
            tTmpEle.setText(tDataInfo[9]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageEffectiveSuminsured"); //险种有效保额
            tTmpEle.setText(tDataInfo[10]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PaymentMethod"); //缴费方式
            tTmpEle.setText(tDataInfo[11]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PaymentYears"); //缴费年限
            tTmpEle.setText(tDataInfo[12]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            //start ---------------------------
            tTmpEle = new Element("PaymentNo"); //缴费次数
            tTmpEle.setText(tDataInfo[13]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            //end ---------------------------
            
            tTmpEle = new Element("SpecificBusiness"); //特殊业务标识
            tTmpEle.setText(tDataInfo[14]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SpecificBusinessCode"); //特殊业务代码
            tTmpEle.setText(tDataInfo[15]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            //start ----------------------
            tTmpEle = new Element("CoverageStatus"); //险种状态
            tTmpEle.setText(tDataInfo[16]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            //end ----------------------
            
            String tPolNo = tDataInfo[17];
            
            //险种期初日额保险金
            
            tTmpEle = new Element("CoveragedayAmount"); 
            tTmpEle.setText(tDataInfo[18]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            //险种有效日额保险金
            
            tTmpEle = new Element("CoveragedayEffectiveAmount"); 
            tTmpEle.setText(tDataInfo[19]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            //险种当前年化保费
            
            tTmpEle = new Element("CoverageyearPremium"); 
            tTmpEle.setText(tDataInfo[20]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            //保费来源
            tTmpEle = new Element("PremiumSource");
            tTmpEle.setText(tDataInfo[21]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            
            //获取特别承保责任数组
            tTmpEle = reportPolInsCovInfo(tPolNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

            // 获取特别核保结论数组
            tTmpEle = reportPolSpeUWInfo(tPolNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

        }

        return tRoot;
    }

    private Element reportPolInsCovInfo(String cPolNo)//特别承保‘责任’数组中的详细信息
    {
        Element tRoot = new Element("SpecialPolicyCoverageDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.SpecialPolicyCoverageDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolInsCovInfo(cPolNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("LiabilityClassification"); //平台责任分类
            tTmpEle.setText(tDataInfo[0]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityCode"); //责任代码    
            tTmpEle.setText(tDataInfo[1]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityName"); //责任名称    
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityEffectiveDate"); //责任开始日期
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityExpireDate"); //责任终止日期
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityStatus"); //责任状态    
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityPremium"); //责任保费    
            tTmpEle.setText(tDataInfo[6]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
          
            tTmpEle = new Element("LiabilityCurrentPremium"); //当前责任保费 modify by zhangyige 2012-12-28
            tTmpEle.setText(tDataInfo[7]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;          
            
            /**
             * modify by zhangyige 2013-01-04
             * 低段保险金1300、中段保险金100000、高段保险金200000、超高段保险金500000；
             * 小额门急诊保险金1800 大额门急诊保险金20000
			 * 注：160308险种，期初保额及有效保额按以上保额设定上传；
             */
            String tAmount = "";
            if(tDataInfo[13].equals("160308")){
            	if(tDataInfo[1].equals("647002")||tDataInfo[1].equals("647009")){
            		tAmount = "1300";
            	}
            	if(tDataInfo[1].equals("647003")||tDataInfo[1].equals("647010")){
            		tAmount = "100000";
            	}
            	if(tDataInfo[1].equals("647004")||tDataInfo[1].equals("647011")||tDataInfo[1].equals("647013")){
            		tAmount = "200000";
            	}
            	if(tDataInfo[1].equals("647005")||tDataInfo[1].equals("647012")){
            		tAmount = "500000";
            	}
            	if(tDataInfo[1].equals("647006")){
            		tAmount = "1800";
            	}
            	if(tDataInfo[1].equals("647007")){
            		tAmount = "20000";
            	}
            }
            
            tTmpEle = new Element("LimitAmount"); //责任保额    
            if(tDataInfo[13].equals("160308")&&!tAmount.equals("")){
            	tTmpEle.setText(tAmount);
            }else{
            	tTmpEle.setText(tDataInfo[8]);
            }
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("EffectivelyAmount"); //责任有效保额
            if(tDataInfo[13].equals("160308")&&!tAmount.equals("")){
            	tTmpEle.setText(tAmount);
            }else{
            	tTmpEle.setText(tDataInfo[9]);
            }
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("WaitingPeriod"); //免责期      
            tTmpEle.setText(tDataInfo[10]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityDayAmount"); //责任期初日额保险金    
            tTmpEle.setText(tDataInfo[11]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("LiabilitydayEffectiveAmount"); //责任有效日额保险金 
            tTmpEle.setText(tDataInfo[12]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            //获取特别赔付比例分段数组
            tTmpEle = reportPolSpeAmInfo(cPolNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

        }

        return tRoot;
    }

    private Element reportPolSpeUWInfo(String cContNo)
    {
        Element tRoot = new Element("SpecialUnderWritingDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.SpecialUnderWritingDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolSpeUWInfo(cContNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("UnderwritingResult"); //核保结论
            tTmpEle.setText(tDataInfo[0]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("UnderwritingDate"); //核保结论日期
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("DeclinedReason"); //拒保原因
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("State"); //业务流程状态
            tTmpEle.setText(tDataInfo[3]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("PhysicalExamination"); //是否体检
            tTmpEle.setText(tDataInfo[4]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
        }

        return tRoot;
    }

    private Element reportPolSpeAmInfo(String cContNo)
    {
        Element tRoot = new Element("SpecialPolicyAmountDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.SpecialPolicyAmountDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolSpeAmInfo(cContNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("SectionNo"); //段序    
            tTmpEle.setText(String.valueOf(idxPI));
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Coststart"); //费用起线
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            String tAmount = "";
            if(tDataInfo[5].equals("160308")){
            	if(tDataInfo[6].equals("647002")||tDataInfo[6].equals("647009")){
            		tAmount = "1300";
            	}
            	if(tDataInfo[6].equals("647003")||tDataInfo[6].equals("647010")){
            		tAmount = "100000";
            	}
            	if(tDataInfo[6].equals("647004")||tDataInfo[6].equals("647011")||tDataInfo[6].equals("647013")){
            		tAmount = "200000";
            	}
            	if(tDataInfo[6].equals("647005")||tDataInfo[6].equals("647012")){
            		tAmount = "500000";
            	}
            	if(tDataInfo[6].equals("647006")){
            		tAmount = "1800";
            	}
            	if(tDataInfo[6].equals("647007")){
            		tAmount = "20000";
            	}
            }
            
            tTmpEle = new Element("Costend"); //费用止线
            if(tDataInfo[5].equals("160308")&&!tAmount.equals("")){
            	tTmpEle.setText(tAmount);
            }else{
            	if(tDataInfo[2].equals("")){
                	tTmpEle.setText("0");
                }else{
                    tTmpEle.setText(tDataInfo[2]);
                }
            }
            
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Deductible"); //免赔额  
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ClaimRatio"); //赔付比例
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
        }

        return tRoot;
    }

    private Element reportPolInsBnfInfo(String cContNo)
    {
        Element tRoot = new Element("PolicyBenefitDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.PolicyBenefitDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolInsBnfInfo(cContNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("Name"); //名称   
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Gender"); //性别        
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Birthday"); //出生日期      
            tTmpEle.setText(tDataInfo[2]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritType"); //证件类别      
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritCode"); //证件号码     
            tTmpEle.setText(tDataInfo[4]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RelationshipWithInsured"); //与被保险人关系
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("BenefitDistribution"); //受益分配方式  
            tTmpEle.setText(tDataInfo[6]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("OrderNo"); //顺序号        
            tTmpEle.setText(tDataInfo[7]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Proportion"); //比例值
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
        }

        return tRoot;
    }

    private SSRS loadPolInsBnfInfo(String cContNo)
    {
        SSRS tResult = null;

        StringBuffer tStrBSql = new StringBuffer();

        tStrBSql.append(" select name Name, sex Gender,birthday Birthday,idtype CritType,idno CritCode, ");
        tStrBSql.append(" relationtoinsured RelationshipWithInsured, '' BenefitDistribution, bnfgrade OrderNo,bnflot Proportion ");
        tStrBSql.append(" from lcbnf ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and contno = '"+cContNo+"'");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadPolSpeAmInfo(String cPolNo)
    {
        SSRS tResult = null;

        StringBuffer tStrBSql = new StringBuffer();

        tStrBSql.append(" select ");
        tStrBSql.append(" RowNumber() over() as SectionNo, ");
        //zyg 费用起线 费用止线 置为 0 
        /**
         * modify by zhangyige 2013-01-04
         * 低段保险金1300、中段保险金100000、高段保险金200000、超高段保险金500000；
         * 小额门急诊保险金1800 大额门急诊保险金20000
		 * 注：160308险种，期初保额及有效保额按以上保额设定上传；
         */
        tStrBSql.append(" '0' Coststart, ");
        tStrBSql.append(" (select calfactorvalue From LCContPlanDutyParam where grpcontno= lcp.grpcontno and contplancode = lcp.contplancode and riskcode = lcp.riskcode and dutycode = lcd.dutycode and calfactor='Amnt' ) Costend, ");
        tStrBSql.append(" cast(lcd.GetLimit as decimal(12,2)) Deductible, cast(lcd.GetRate as decimal(12,2)) ClaimRatio, ");
        tStrBSql.append(" (select riskcode from lcpol where polno='"+cPolNo+"') RiskCode, ");
        tStrBSql.append(" lcd.DutyCode DutyCode ");
        tStrBSql.append(" from LCDuty lcd ");
        tStrBSql.append(" inner join LCPol lcp on lcd.polno = lcp.polno ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcd.PolNo = '" + cPolNo + "' ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadPolSpeUWInfo(String tPolNo)
    {
        SSRS tResult = null;

        StringBuffer tStrBSql = new StringBuffer();

        //        tStrBSql.append(" select ");
        //        tStrBSql.append(" '' UnderwritingResult, '' UnderwritingDate ");
        //        tStrBSql.append(" from dual ");
        //        tStrBSql.append(" where 1 = 1 ");

        tStrBSql.append(" select ");
        tStrBSql.append(" lcp.UwFlag UnderwritingResult, lcp.UwDate UnderwritingDate, ");
        tStrBSql.append(" (select uwidea from lcuwmaster where polno ='"+tPolNo+"') DeclinedReason, '1' State, ");
        tStrBSql.append(" case when exists (select 1 from LCPENotice where contno = lcp.contno) then '1' else '0' end PhysicalExamination ");
        tStrBSql.append(" from LCPol lcp ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.PolNo = '" + tPolNo + "' ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadPolInsCovInfo(String cPolNo)//特别承保‘责任’数组中的详细信息
    {
        SSRS tResult = null;

        StringBuffer tStrBSql = new StringBuffer();

        //        tStrBSql.append(" select ");
        //        tStrBSql
        //                .append(" '' LiabilityClassification, '' LiabilityCode, '' LiabilityName, '' LiabilityEffectiveDate, '' LiabilityExpireDate, '' LiabilityStatus, '' LiabilityPremium, '' LimitAmount, '' EffectivelyAmount, '' WaitingPeriod ");
        //        tStrBSql.append(" from dual ");
        //        tStrBSql.append(" where 1 = 1 ");

        tStrBSql.append(" select ");
        tStrBSql.append(" lcd.DutyCode LiabilityClassification, ");
        tStrBSql.append(" lcd.DutyCode LiabilityCode, lmd.DutyName LiabilityName, ");
        tStrBSql.append(" lcd.GetStartDate LiabilityEffectiveDate, (lcd.EndDate - 1 day) LiabilityExpireDate, ");
        tStrBSql.append(" lcp.StateFlag LiabilityStatus, lcd.Prem LiabilityPremium, ");
        tStrBSql.append(" lcd.Prem LiabilityCurrentPremium, "); //当前责任保费
        tStrBSql.append(" lcd.Amnt LimitAmount, lcd.Amnt EffectivelyAmount, ");
        tStrBSql.append(" '0' WaitingPeriod, ");
        
        tStrBSql.append("(select case when lc.mult='1' then 50 when lc.mult='2' then 100 when lc.mult='3' then 150 else 0 end from lcduty lc where lcp.polno = polno and lcp.riskcode in ('1601','1602','160308') fetch first 1 rows only) LiabilityDayAmount,");//责任期初日额保险金
        tStrBSql.append("(select case when lc.mult='1' then 50 when lc.mult='2' then 100 when lc.mult='3' then 150 else 0 end from lcduty lc where lcp.polno = polno and lcp.riskcode in ('1601','1602','160308') fetch first 1 rows only) LiabilitydayEffectiveAmount,");//责任有效日额保险金
        
        // tStrBSql.append(" trim(lcp.ContNo) || '_' || trim(lcp.InsuredNo) InsuredGroupCode, ");
        tStrBSql.append(" '' InsuredGroupCode, ");
        // tStrBSql.append(" trim(lcp.ContNo) || '_' || trim(lcp.InsuredNo) GroupName ");
        tStrBSql.append(" '' GroupName, ");
        tStrBSql.append(" lcp.riskcode RiskCode ");
        
        tStrBSql.append(" from LCPol lcp ");
        tStrBSql.append(" inner join LCDuty lcd on lcd.PolNo = lcp.PolNo ");
        tStrBSql.append(" inner join LMDuty lmd on lmd.DutyCode = lcd.DutyCode ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.polno = '" + cPolNo + "' ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadPolInsUpCovInfo(String cContNo, String cInsuredNo)//特别承保 ‘险种’ 数组中详细信息
    {
        SSRS tResult = null;

        StringBuffer tStrBSql = new StringBuffer();

        //        tStrBSql.append(" select ");
        //        tStrBSql
        //                .append(" '' CoverageType, '' CoverageCode, '' ComCoverageCode, '' CoverageName, '' CoverageEffectiveDate, '' CoverageExpireDate, '' CoveragePremium, '' CoverageSuminsured, '' CoverageEffectiveSuminsured, '' SpecificBusiness, '' SpecificBusinessCode ");
        //        tStrBSql.append(" from dual ");
        //        tStrBSql.append(" where 1 = 1 ");

        tStrBSql.append(" select ");
        tStrBSql.append(" (select riskperiod from lmriskapp where riskcode = lcp.RiskCode) CoverageType, lcp.RiskCode CoverageCode, ");
        tStrBSql.append(" lcp.RiskCode ComCoverageCode, lmra.RiskName CoverageName, ");
        tStrBSql.append(" lcp.GetStartDate CoverageEffectiveDate, (lcp.EndDate - 1 days) CoverageExpireDate, ");
        tStrBSql.append(" lmra.SubRiskFlag MainAttachedFlag, ");
        tStrBSql.append(" lcp.Prem CoveragePremium, ");
        tStrBSql.append(" lcp.Prem CoverageCurrentPremium, ");//险种当前保费
        tStrBSql.append(" lcp.Amnt CoverageSuminsured, lcp.Amnt CoverageEffectiveSuminsured, ");
        tStrBSql
                .append(" lcp.PayIntv PaymentMethod, (Case when lcp.PayIntv is null then '' else char(lcp.PayYears) end) PaymentYears, ");
        tStrBSql.append(" '1' PaymentNo, ");//缴费次数
        tStrBSql.append(" (case lcp.RiskCode when '160308' then '1' else '0' end) SpecificBusiness, (case lcp.RiskCode when '160308' then 'ZX008' else '' end) SpecificBusinessCode, ");
        tStrBSql.append(" '1' CoverageStatus, ");//险种状态
        tStrBSql.append(" lcp.PolNo ,");
        
        tStrBSql.append("(select case when lc.mult='1' then 50 when lc.mult='2' then 100 when lc.mult='3' then 150 else 0 end from lcduty lc where lcp.polno = polno and lcp.riskcode = '1601' fetch first 1 rows only) CoveragedayAmount,"); //险种期初日额保险金
        tStrBSql.append("(select case when lc.mult='1' then 50 when lc.mult='2' then 100 when lc.mult='3' then 150 else 0 end from lcduty lc where lcp.polno = polno and lcp.riskcode = '1601' fetch first 1 rows only) CoveragedayEffectiveAmount,");//险种有效日额保险金
        //( case when lcp.payintv='1'  then prem*12 when lcp.payintv='3'  then prem*4  when lcp.payintv='6'  then prem*2 when lcp.payintv='12' then prem   else null end )
        tStrBSql.append("( case when lcp.payintv='1'  then prem*12 when lcp.payintv='3'  then prem*4  when lcp.payintv='6'  then prem*2 when lcp.payintv='12' then prem   else 0 end ) CoverageyearPremium,");//险种当前年化保费
        tStrBSql.append("'02' PremiumSource");//保费来源
        
        tStrBSql.append(" from LCPol lcp ");
        tStrBSql.append(" inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.GrpContNo = '" + cContNo + "' and lcp.InsuredNo = '" + cInsuredNo + "' ");

        String tStrSql = tStrBSql.toString();
        System.out.println("#"+tStrSql);
        tResult = new ExeSQL().execSQL(tStrSql);
        if(tResult==null){
        	System.out.println("===liuijian承保险种数组为空（团单）===");
        	
        }else{
        	System.out.println("===liuijian承保险种数组不是空（团单）===");
        }
        
        return tResult;
    }

    private SSRS loadPolInsInfo(String cContNo, String cContType)
    {
    	System.out.println("cContType:"+cContType);
        SSRS tResult = null;

        String tContNo = cContNo;

        StringBuffer tStrBSql = new StringBuffer();


        if (cContType.equals("2"))
        {
            //            tStrBSql.append(" select * from ( ");

            tStrBSql.append(" select ");
            tStrBSql.append(" lci.Name Name, lci.Sex Gender, lci.Birthday Birthday, ");
            tStrBSql.append(" lci.IDType CritType, lci.IDNo CritCode, '07' RelationshipWithPh, ");
            tStrBSql.append(" lci.InsuredNo CustomerNo, '0' AnomalyInform, (select stateflag from lcgrpcont where grpcontno= '"+tContNo+"') OfferStatus, ");
            tStrBSql.append(" '' SubStartDate, '' SubEndDate, ");
            //            tStrBSql.append(" trim(lci.ContNo) || '_' || trim(lci.InsuredNo) InsuredGroupCode, ");
            tStrBSql.append(" '' InsuredGroupCode, ");
            tStrBSql.append(" lci.Relationtomaininsured InsuredType, ");
            tStrBSql
                    .append(" (case when lci.Relationtomaininsured = '00' then '' else (select slci.InsuredNo from LCInsured slci where slci.ContNo = lci.ContNo and slci.Relationtomaininsured = '00') end) SnOfMainInsured, ");
            tStrBSql.append(" lci.Relationtomaininsured RelationshipWithMainInsured, ");
            tStrBSql.append(" lci.InsuredStat ServiceMark, '' HealthFlag, '' SocialcareNo, ");
            tStrBSql.append(" lci.OccupationCode OccupationalCode, '' WorkPlace, ");
            tStrBSql.append(" '' EmpoyeeNo, ");
            tStrBSql.append(" '0' AppointedBenefit ");
            tStrBSql.append(" from lcinsured lci ");
            tStrBSql.append(" where 1 = 1 ");
            tStrBSql.append(" and lci.GrpContNo = '" + tContNo + "' ");
            tStrBSql.append(" and not exists (select contno from ljagetendorse where feeoperationtype='NI' and lci.contno=contno )");

            //            tStrBSql.append(" ) as tmp fetch first row only");
        }

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);
        if(tResult==null){
        	System.out.println("===liuijian承保险种数组为空（团单）===");
        	
        }else{
        	System.out.println("===liuijian承保险种数组不是空（团单）===");
        }
        
        return tResult;
    }

    private SSRS loadPolPerInfo(String cContNo)
    {
        SSRS tResult = null;

        String tContNo = cContNo;

        StringBuffer tStrBSql = new StringBuffer();

        tStrBSql.append(" select ");
        tStrBSql.append(" lcap.AppntName Name, lcap.AppntSex Gender, ");
        tStrBSql.append(" lcap.AppntBirthday Birthday, lcap.IDType CritType, ");
        tStrBSql.append(" lcap.IDNo CritCode, '' RelationshipWithInsured ");
        tStrBSql.append(" from lcappnt lcap ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcap.ContNo = '" + tContNo + "' ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadPolGrpInfo(String cContNo)
    {
        SSRS tResult = null;

        String tContNo = cContNo;

        StringBuffer tStrBSql = new StringBuffer();

        tStrBSql.append(" select ");
        tStrBSql.append(" lgap.Name Name, ");
        tStrBSql.append(" '110000' Address1, ");
        tStrBSql.append(" '110100' Address2, ");
        tStrBSql.append(" PostalAddress Address3, ");
        tStrBSql.append(" PostalAddress Address4, ");
        tStrBSql.append(" (case when lgap.OrganComCode is not null then '2' else '9' end) CritType, ");
        tStrBSql
                .append(" (case when lgap.OrganComCode is null then '999999999' else lgap.OrganComCode end) CritCode, ");
        tStrBSql.append(" (select case impartparam when 'N' then '' else impartparam end from lccustomerimpartparams where grpcontno = '"+tContNo+"' and impartcode = '010' and impartver = '011' and impartparamno = '5') SocialSecurityNo, (select GrpNature from lcgrpcont where grpcontno = '"+tContNo+"') CompanyNature, ");
        //zyg 置为默认 70	- 保险业
        tStrBSql.append(" (select BusinessType from lcgrpcont where grpcontno='"+tContNo+"') IndustryClassification,"
//        tStrBSql.append(" '70' IndustryClassification,"
        		+" '' LegalRepresentative, ");
        tStrBSql.append(" '' Contact, lgap.Peoples NUMBEROFUNITS, ");
        tStrBSql.append(" (select count(1) from lcpol where grpcontno='"+tContNo+"'and riskcode='160308') ");
        tStrBSql.append(" from LCGrpAppnt lgap ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lgap.GrpContNo = '" + tContNo + "' ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadPolAmoInfo()
    {
        SSRS tResult = null;

        StringBuffer tStrBSql = new StringBuffer();
        //zyg 费用起线 费用止线 置为 0 
        tStrBSql.append(" select ");
        tStrBSql.append(" '' SectionNo, '0' Coststart, '0' Costend, '' Deductible, '' ClaimRatio ");
        tStrBSql.append(" from dual ");
        tStrBSql.append(" where 1 = 1 ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadPolUWInfo(String cPolNo)
    {
        SSRS tResult = null;

        String tPolNo = cPolNo;

        StringBuffer tStrBSql = new StringBuffer();

        tStrBSql.append(" select ");
        tStrBSql.append(" lcp.UwFlag UnderwritingResult, lcp.UwDate UnderwritingDate, ");
        tStrBSql.append(" (select uwidea from lcuwmaster where polno ='"+tPolNo+"') DeclinedReason, '1' State, ");
        tStrBSql.append(" case when exists (select 1 from LCPENotice where contno = lcp.contno) then '1' else '0' end PhysicalExamination ");
        tStrBSql.append(" from LCPol lcp ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.PolNo = '" + tPolNo + "' ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadPolCovInfo(String cPolNo)//承保 ‘责任 ’数组中的详细信息
    {
        SSRS tResult = null;

        String tPolNo = cPolNo;

        StringBuffer tStrBSql = new StringBuffer();

        tStrBSql.append(" select * from ( ");

        tStrBSql.append(" select ");
        tStrBSql.append(" lcd.DutyCode LiabilityClassification, ");
        tStrBSql.append(" lcd.DutyCode LiabilityCode, lmd.DutyName LiabilityName, ");
        tStrBSql.append(" lcd.GetStartDate LiabilityEffectiveDate, (lcd.EndDate - 1 day) LiabilityExpireDate, ");
        tStrBSql.append(" lcp.StateFlag LiabilityStatus, lcd.Prem LiabilityPremium, ");
        tStrBSql.append(" lcd.Prem LiabilityCurrentPremium, "); //当前责任保费
        tStrBSql.append(" lcd.Amnt LimitAmount, lcd.Amnt EffectivelyAmount, ");
        tStrBSql.append(" '0' WaitingPeriod, ");
        tStrBSql.append(" trim(lcp.ContNo) || '_' || trim(lcp.InsuredNo) InsuredGroupCode, ");
        //        tStrBSql.append(" '' InsuredGroupCode, ");
        tStrBSql.append(" trim(lcp.ContNo) || '_' || trim(lcp.InsuredNo) GroupName, ");
        //        tStrBSql.append(" '' GroupName ");
        
        tStrBSql.append("(select case when lc.mult='1' then '50' when lc.mult='2' then '100' when lc.mult='3' then '150' else '' end from lcpol lc where lc.riskcode in ('1601','1602','160308') and lc.polno= '" + tPolNo + "') LiabilityDayAmount,");//责任期初日额保险金
        tStrBSql.append("(select case when lc.mult='1' then '50' when lc.mult='2' then '100' when lc.mult='3' then '150' else '' end from lcpol lc where lc.riskcode in ('1601','1602','160308') and lc.polno= '" + tPolNo + "') LiabilitydayEffectiveAmount");//责任有效日额保险金

        tStrBSql.append(" from LCPol lcp ");
        tStrBSql.append(" inner join LCDuty lcd on lcd.PolNo = lcp.PolNo ");
        tStrBSql.append(" inner join LMDuty lmd on lmd.DutyCode = lcd.DutyCode ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.polno = '" + tPolNo + "' ");

        tStrBSql.append(" ) as tmp fetch first row only");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadPolUpCovInfo(String cContNo)//承保 ‘险种 ’数组中详细信息  （已经注释）
    {
        SSRS tResult = null;

        String tContNo = cContNo;

        StringBuffer tStrBSql = new StringBuffer();

        //        tStrBSql.append(" select * from ( ");


        tStrBSql.append(" select ");
        tStrBSql.append(" (select riskperiod from lmriskapp where riskcode = lcp.RiskCode) CoverageType, lcp.RiskCode CoverageCode, ");
        tStrBSql.append(" lcp.RiskCode ComCoverageCode, lmra.RiskName CoverageName, ");
        tStrBSql.append(" lcp.GetStartDate CoverageEffectiveDate, (lcp.EndDate - 1 days) CoverageExpireDate, ");//modify by zhangyige 2012-12-28 团单满期日减一天
        tStrBSql.append(" lmra.SubRiskFlag MainAttachedFlag, ");
        tStrBSql.append(" lcp.Prem CoveragePremium, ");
        tStrBSql.append(" lcp.Prem CoverageCurrentPremium, ");//险种当前保费
        tStrBSql.append(" lcp.Amnt CoverageSuminsured, lcp.Amnt CoverageEffectiveSuminsured, ");
        tStrBSql
                .append(" lcp.PayIntv PaymentMethod, (Case when lcp.PayIntv is null then '' else char(lcp.PayYears) end) PaymentYears, ");
        tStrBSql.append(" '1' PaymentNo, ");//缴费次数
        tStrBSql.append(" '0' SpecificBusiness, (case lcp.RiskCode when '160308' then 'ZX008' else '' end)SpecificBusinessCode, ");
        tStrBSql.append(" '1' CoverageStatus, ");//险种状态
        tStrBSql.append(" lcp.PolNo ");
        tStrBSql.append(" from LCPol lcp ");
        tStrBSql.append(" inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.GrpContNo = '" + tContNo + "' ");

        //        tStrBSql.append(" ) as tmp fetch first row only");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadPolicyInfo(String cDataInfoKey)//保单详细信息
    {
        SSRS tResult = null;

        String tContNo = cDataInfoKey;

        StringBuffer tStrBSql = new StringBuffer();
        tStrBSql.append(" select ");
        tStrBSql.append(" '0' SplitPolicyNo, ");//拆单标志
        tStrBSql.append(" lgc.UWFlag Rejection, lgc.PrtNo ApplicationFormNo, lgc.GrpContNo PolicyNo, ");
        tStrBSql.append(" lgc.PolApplyDate ApplicationDate, ");
        tStrBSql.append(" lgc.SignDate AcceptDate, ");//保单出单日期(承保日期)
        tStrBSql.append(" '000085110000' OrganId, ");//保单所属机构代码 --需确认中介机构代码 //zhangyige 2012-12-28 传固定值 否
        tStrBSql.append(" '中国人民健康保险股份有限公司北京分公司' OrganName, ");//保单所属机构名称 //zhangyige 2012-12-28 传固定值 否
        tStrBSql.append(" lgc.Salechnl ChannelType, ");//销售渠道类型
        tStrBSql.append(" lgc.Salechnl SalesType, ");//销售人员类型
        tStrBSql.append(" (select qualifno 资格证号 from LAQUALIFICATION where agentcode = lgc.AgentCode) SalesCode, ");//销售人员代码  
        tStrBSql.append(" (select name from laagent where agentcode = lgc.AgentCode) SalesName, ");//销售人员姓名
        tStrBSql.append(" lgc.AgentCom SalesChannelCode, ");//中介销售机构代码
        tStrBSql.append(" (select name from lacom where agentcom = lgc.AgentCom) SalesChannelName, ");//中介销售机构名称
        tStrBSql.append(" (select address from lacom where agentcom = lgc.AgentCom) BusinessAddress, ");//中介营业场所地址
        tStrBSql.append(" '2' ContractType, ");
//        tStrBSql.append(" lgc.GrpContNo GroupMark, ");//取消
        tStrBSql.append(" lgc.CValidate PolicyStartDate, ");
        tStrBSql.append(" lgc.CInvalidate PolicyEndDate, ");
        tStrBSql.append(" '' TerminationDate, '' SuspendDate, '' RecoverDate, ");
        tStrBSql.append(" '01' ContractSource, ");
        tStrBSql.append(" lgc.GrpContNo ContractNo, ");
        tStrBSql.append(" '0' RenewalTimes, '' RenewalMethod, 'M' MainAttachedFlag, ");
        tStrBSql.append(" lgc.StateFlag ContractStatus, '' TerminationReason, ");
        tStrBSql.append(" lgc.Amnt SumInsured, lgc.Amnt EffectiveSumInsured, ");
        tStrBSql.append(" lgc.Prem Premium, lgc.Prem CurrentPremium, ");
        tStrBSql.append(" lgc.PayIntv PaymentMethod, ");
        tStrBSql.append(" (select payyears from lcpol where grpcontno = '"+tContNo+"' and polno = mainpolno order by polno fetch first 1 rows only ) PaymentYears, ");//缴费年限
        tStrBSql.append(" '1' PaymentNo, ");//缴费次数
        tStrBSql.append(" '1' PolicyHolderPro, ");
        tStrBSql.append(" (select count(1) from lcinsured where grpcontno = '" + tContNo + "') PolicyHolderNum, ");
        tStrBSql.append(" (select count(1) from lcinsured where grpcontno = '" + tContNo + "') EffectiveInsuredNum, ");
        tStrBSql.append(" '' FormerpolicyNo, '' SpecialRemark, ");
        tStrBSql.append(" '' RegularClearingMark, ");
        tStrBSql.append(" '待定' RegularClearing, "); 
        tStrBSql.append(" '' RegularClearingDate, ");
        tStrBSql.append(" lgc.SignDate PremiumDueDate, ");
        tStrBSql.append(" '0' RealTimeClaimFlag, "); //zhangyige 2012-12-28 传固定值 否
        tStrBSql.append(" '' PolicyLoan, ");//是否存在保单借款
        tStrBSql.append(" '' AutoPaidUp, ");//是否自动垫交
        tStrBSql.append(" lgc.CoInsuranceFlag CoInsurance, ");//是否共保
        tStrBSql.append(" (case when lgc.CoInsuranceFlag = '1' then '1' else '' end ) LeadCoInsurance, ");//是否主共保
        
        //tStrBSql.append("(select sum(lcp.prem) from lcgrppayplan lcp where lcp.prtno=lgc.prtno),");
        
        tStrBSql.append(" ( case when lgc.payintv='1'  then prem*12 when lgc.payintv='3'  then prem*4  when lgc.payintv='6'  then prem*2 when lgc.payintv='12' then prem   else 0 end ) YearPremium");//当年年化保费
        
        tStrBSql.append(" from LCGrpCont lgc ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lgc.GrpContNo = '" + tContNo + "' ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    /**
     * 调用前置机服务
     * @return
     */
    private boolean calService()
    {
        Document tDocument = formJDocMessage(mRoot, "01", mDataInfoKey);
        JdomUtil.print(tDocument);

        Document tInStdXmlDoc = tDocument;

        ReqJKXPreService tJKXPreServlet = new ReqJKXPreService();
        String tReceiveXmlDoc = tJKXPreServlet.callService(tInStdXmlDoc);
//        JdomUtil.print(tReceiveXmlDoc);
        System.out.println(tReceiveXmlDoc);

        return true;
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        System.out.println(szFunc + ": " + szErrMsg);

        CError cError = new CError();
        cError.moduleName = "AddCertifyListBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public static void main(String args[])
    {
    	QYJKXTwoGrpServiceImp tBusLogic = new QYJKXTwoGrpServiceImp();
        VData tVData = new VData();
        TransferData mTransferData = new TransferData();
        /**
         * 00000614000002
			00000697000002
			00000905000003
			00001968000002
			00003325000002
         */                 
        mTransferData.setNameAndValue("DataInfoKey", "00106319000007"); //个单 000000001000004 团单 00105535000001
        tVData.add(mTransferData);
        tBusLogic.callJKXService(tVData, null);
    }
}
