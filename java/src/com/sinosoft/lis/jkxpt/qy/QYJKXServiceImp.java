/**
 * 2010-11-18
 */
package com.sinosoft.lis.jkxpt.qy;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.jkxpt.JKXService;
import com.sinosoft.lis.jkxpt.ReqJKXPreService;
import com.sinosoft.lis.yibaotong.JdomUtil;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class QYJKXServiceImp extends JKXService
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private TransferData mTransferData = null;

    /** 数据操作字符串 */
    private String mOperate = "";

    private String mDataInfoKey = null;

    private Element mRoot = null;

    public boolean callJKXService(VData cInputData, String cOperate)
    {
        // 获取入口参数 
        if (!getInputData(cInputData, cOperate))
        {
            return false;
        }
        // --------------------

        // 取数逻辑处理        
        //        mRoot = dealBusiXml(mPolicyNo);
        mRoot = reportBusiXml(mDataInfoKey);
        // --------------------

        // 调用前置机
        if (!calService())
        {
            return false;
        }
        // --------------------

        return true;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mDataInfoKey = (String) mTransferData.getValueByName("DataInfoKey");
        if (mDataInfoKey == null || mDataInfoKey.equals(""))
        {
            buildError("getInputData", "获取保单合同号码失败。");
            return false;
        }

        return true;
    }

    private Element reportBusiXml(String cDataInfoKey)
    {
        Element tRoot = new Element("PolicyDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.PolicyDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolicyInfo(cDataInfoKey);
        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String tContNo = null;
            String tContType = null;

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("Rejection");
            tTmpEle.setText(tDataInfo[0]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ApplicationFormNo");
            tTmpEle.setText(tDataInfo[1]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PolicyNo");
            tContNo = tDataInfo[2];
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ApplicationDate");
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ContractType");
            tContType = tDataInfo[4];
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("GroupMark");
            // tTmpEle.setText(tDataInfo[5]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PolicyStartDate");
            tTmpEle.setText(tDataInfo[6]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PolicyEndDate");
            tTmpEle.setText(tDataInfo[7]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("TerminationDate");
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SuspendDate");
            tTmpEle.setText(tDataInfo[9]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RecoverDate");
            tTmpEle.setText(tDataInfo[10]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ContractSource");
            tTmpEle.setText(tDataInfo[11]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ContractNo");
            tTmpEle.setText(tDataInfo[12]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RenewalTimes");
            tTmpEle.setText(tDataInfo[13]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RenewalMethod");
            tTmpEle.setText(tDataInfo[14]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("MainAttachedFlag");
            // tTmpEle.setText(tDataInfo[15]);
            // tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ContractStatus");
            tTmpEle.setText(tDataInfo[16]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("TerminationReason");
            tTmpEle.setText(tDataInfo[17]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SumInsured");
            tTmpEle.setText(tDataInfo[18]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("EffectiveSumInsured");
            tTmpEle.setText(tDataInfo[19]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Premium");
            tTmpEle.setText(tDataInfo[20]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CurrentPremium");
            tTmpEle.setText(tDataInfo[21]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PaymentMethod");
            // tTmpEle.setText(tDataInfo[22]);
            // tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PaymentYears");
            // tTmpEle.setText(tDataInfo[23]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PolicyHolderPro");
            tTmpEle.setText(tDataInfo[24]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PolicyHolderNum");
            tTmpEle.setText(tDataInfo[25]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("EffectiveInsuredNum");
            tTmpEle.setText(tDataInfo[26]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("FormerpolicyNo");
            tTmpEle.setText(tDataInfo[27]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SpecialRemark");
            tTmpEle.setText(tDataInfo[28]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RegularClearingMark");
            tTmpEle.setText(tDataInfo[29]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RegularClearing");
            tTmpEle.setText(tDataInfo[30]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RegularClearingDate");
            tTmpEle.setText(tDataInfo[31]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PremiumDueDate");
            tTmpEle.setText(tDataInfo[32]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RealTimeClaimFlag");
            tTmpEle.setText(tDataInfo[33]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            // 获取责任信息
            tTmpEle = reportPolUpCovInfo(tContNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

            if (tContType.equals("2"))
            {
                //获取团体投保人数组
                tTmpEle = reportPolGrpInfo(tContNo);
                if (tTmpEle != null)
                {
                    tItemEle.addContent(tTmpEle);
                }
                tTmpEle = null;
            }

            if (tContType.equals("1"))
            {
                //获取个人投保人数组
                tTmpEle = reportPolPerInfo(tContNo);
                if (tTmpEle != null)
                {
                    tItemEle.addContent(tTmpEle);
                }
                tTmpEle = null;
            }

            //被保险人数组
            tTmpEle = reportPolInsInfo(tContNo, tContType);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

            // --------------------

        }

        return tRoot;
    }

    private Element reportPolUpCovInfo(String cContNo)
    {
        Element tRoot = new Element("PolicyUpcoverageDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.PolicyUpcoverageDTO");

        Element tTmpEle = null;
        String tPolNo = "";

        SSRS tResPolicyInfo = loadPolUpCovInfo(cContNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("CoverageType"); //平台险种类型
            tTmpEle.setText(tDataInfo[0]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageCode"); //平台险种代码
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ComCoverageCode"); //公司险种代码
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageName"); //公司险种名称
            tTmpEle.setText(tDataInfo[3]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageEffectiveDate"); //险种起保日期
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageExpireDate"); //险种满期日期
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("MainAttachedFlag"); //主附险性质
            tTmpEle.setText(tDataInfo[6]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoveragePremium"); //险种保费
            tTmpEle.setText(tDataInfo[7]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageSuminsured"); //险种保额 
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageEffectiveSuminsured"); //险种有效保额
            tTmpEle.setText(tDataInfo[9]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PaymentMethod"); //缴费方式
            tTmpEle.setText(tDataInfo[10]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PaymentYears"); //缴费年限
            tTmpEle.setText(tDataInfo[11]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SpecificBusiness"); //特殊业务标识
            tTmpEle.setText(tDataInfo[12]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SpecificBusinessCode"); //特殊业务代码
            tTmpEle.setText(tDataInfo[13]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tPolNo = tDataInfo[14];

            //获取承保责任数组
            tTmpEle = reportPolCovInfo(tPolNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
        }

        return tRoot;
    }

    private Element reportPolCovInfo(String cPolNo)
    {
        Element tRoot = new Element("PolicyCoverageDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.PolicyCoverageDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolCovInfo(cPolNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("LiabilityClassification"); //平台责任分类    
            tTmpEle.setText(tDataInfo[0]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityCode"); //责任代码        
            tTmpEle.setText(tDataInfo[1]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityName"); //责任名称        
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityEffectiveDate"); //责任开始日期    
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityExpireDate"); //责任终止日期    
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityStatus"); //责任状态     
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityPremium"); //责任保费        
            tTmpEle.setText(tDataInfo[6]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LimitAmount"); //责任保额        
            tTmpEle.setText(tDataInfo[7]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("EffectivelyAmount"); //责任有效保额    
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("WaitingPeriod"); //免责期          
            tTmpEle.setText(tDataInfo[9]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("InsuredGroupCode"); //被保险人属组代码
            tTmpEle.setText(tDataInfo[10]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("GroupName"); //属组名称     
            tTmpEle.setText(tDataInfo[11]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            //获取核保结论数组
            tTmpEle = reportPolUWInfo(cPolNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

            //获取赔付比例分段数组
            tTmpEle = reportPolAmoInfo(cPolNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

        }

        return tRoot;
    }

    private Element reportPolAmoInfo(String cPolNo)
    {
        Element tRoot = new Element("PolicyAmountDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.PolicyAmountDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolAmoInfo();

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("SectionNo"); //段序
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Coststart"); //费用起线
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Costend"); //费用止线
            tTmpEle.setText(tDataInfo[2]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Deductible"); //免赔额
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ClaimRatio"); //赔付比例
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
        }

        return tRoot;
    }

    private Element reportPolUWInfo(String cPolNo)
    {
        Element tRoot = new Element("UnderWritingDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.UnderWritingDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolUWInfo(cPolNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("UnderwritingResult"); //核保结论
            tTmpEle.setText(tDataInfo[0]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("UnderwritingDate"); //核保结论日期
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
        }

        return tRoot;
    }

    private Element reportPolGrpInfo(String cContNo)
    {
        Element tRoot = new Element("PolicyGroupphDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.PolicyGroupphDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolGrpInfo(cContNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);
            tTmpEle = new Element("Name"); //单位名称   
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritType"); //团体客户证件类别
            tTmpEle.setText(tDataInfo[1]);
            //tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritCode"); //团体客户证件号码
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SocialSecurityNo"); //社保保险登记证号
            tTmpEle.setText(tDataInfo[3]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CompanyNature"); //单位性质       
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("IndustryClassification"); //行业分类     
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LegalRepresentative"); //法人代表        
            tTmpEle.setText(tDataInfo[6]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Contact"); //联系人      
            tTmpEle.setText(tDataInfo[7]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("NumberOfUnits"); //单位总人数     
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
        }

        return tRoot;
    }

    private Element reportPolPerInfo(String cContNo)
    {
        Element tRoot = new Element("PolicyPersonphDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.PolicyPersonphDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolPerInfo(cContNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("Name"); //姓名  
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Gender"); //性别       
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Birthday"); //出生日期      
            tTmpEle.setText(tDataInfo[2]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritType"); //证件类别      
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritCode"); //证件号码      
            tTmpEle.setText(tDataInfo[4]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RelationshipWithInsured"); //与被保险人关系
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
        }

        return tRoot;
    }

    private Element reportPolInsInfo(String cContNo, String cContType)
    {
        Element tRoot = new Element("PolicyInsuredDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.PolicyInsuredDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolInsInfo(cContNo, cContType);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("Name"); //姓名    
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Gender"); //性别       
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Birthday"); //出生日期        
            tTmpEle.setText(tDataInfo[2]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritType"); //被保险人证件类别    
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritCode"); //被保险人证件号码   
            tTmpEle.setText(tDataInfo[4]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RelationshipWithPh"); //与投保人关系        
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CustomerNo"); //客户编号          
            tTmpEle.setText(tDataInfo[6]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("AnomalyInform"); //异常告知          
            tTmpEle.setText(tDataInfo[7]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("OfferStatus"); //要约状态            
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SubStartDate"); //被保险人责任开始日期
            tTmpEle.setText(tDataInfo[9]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SubEndDate"); //被保险人责任终止日期
            tTmpEle.setText(tDataInfo[10]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("InsuredGroupCode"); //被保险人属组代码  
            tTmpEle.setText(tDataInfo[11]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("InsuredType"); //被保险人类型    
            tTmpEle.setText(tDataInfo[12]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SnOfMainInsured"); //主被保人序号  
            tTmpEle.setText(tDataInfo[13]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RelationshipWithMainInsured"); //与主被保人关系 
            tTmpEle.setText(tDataInfo[14]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ServiceMark"); //在职标志    
            tTmpEle.setText(tDataInfo[15]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("HealthFlag"); //医保标识          
            tTmpEle.setText(tDataInfo[16]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SocialcareNo"); //社保卡号  
            tTmpEle.setText(tDataInfo[17]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("OccupationalCode"); //职业代码    
            tTmpEle.setText(tDataInfo[18]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("WorkPlace"); //工作地点     
            tTmpEle.setText(tDataInfo[19]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("EmpoyeeNo"); //工号        
            tTmpEle.setText(tDataInfo[20]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            String tInsuredNo = tDataInfo[6];
            //获取特别承保险种数组
            tTmpEle = reportPolInsUpCovInfo(cContNo, tInsuredNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

            //获取受益人数组
            tTmpEle = reportPolInsBnfInfo(cContNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
        }

        return tRoot;
    }

    private Element reportPolInsUpCovInfo(String cContNo, String cInsuredNo)
    {
        Element tRoot = new Element("SpecialPolicyUpcoverageDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.SpecialPolicyUpcoverageDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolInsUpCovInfo(cContNo, cInsuredNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("CoverageType"); //平台险种类型
            tTmpEle.setText(tDataInfo[0]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageCode"); //平台险种代码
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ComCoverageCode"); //公司险种代码
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageName"); //公司险种名称
            tTmpEle.setText(tDataInfo[3]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageEffectiveDate"); //险种起保日期
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageExpireDate"); //险种满期日期
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("MainAttachedFlag"); //主附险性质
            tTmpEle.setText(tDataInfo[6]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoveragePremium"); //险种保费    
            tTmpEle.setText(tDataInfo[7]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageSuminsured"); //险种保额    
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageEffectiveSuminsured"); //险种有效保额
            tTmpEle.setText(tDataInfo[9]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PaymentMethod"); //缴费方式
            tTmpEle.setText(tDataInfo[10]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PaymentYears"); //缴费年限
            tTmpEle.setText(tDataInfo[11]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SpecificBusiness"); //特殊业务标识
            tTmpEle.setText(tDataInfo[12]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SpecificBusinessCode"); //特殊业务代码
            tTmpEle.setText(tDataInfo[13]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            String tPolNo = tDataInfo[14];
            //获取特别承保责任数组
            tTmpEle = reportPolInsCovInfo(tPolNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

            // 获取特别核保结论数组
            tTmpEle = reportPolSpeUWInfo(tPolNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

        }

        return tRoot;
    }

    private Element reportPolInsCovInfo(String cPolNo)
    {
        Element tRoot = new Element("SpecialPolicyCoverageDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.SpecialPolicyCoverageDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolInsCovInfo(cPolNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("LiabilityClassification"); //平台责任分类
            tTmpEle.setText(tDataInfo[0]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityCode"); //责任代码    
            tTmpEle.setText(tDataInfo[1]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityName"); //责任名称    
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityEffectiveDate"); //责任开始日期
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityExpireDate"); //责任终止日期
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityStatus"); //责任状态    
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityPremium"); //责任保费    
            tTmpEle.setText(tDataInfo[6]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LimitAmount"); //责任保额    
            tTmpEle.setText(tDataInfo[7]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("EffectivelyAmount"); //责任有效保额
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("WaitingPeriod"); //免责期      
            tTmpEle.setText(tDataInfo[9]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            //获取特别赔付比例分段数组
            tTmpEle = reportPolSpeAmInfo(cPolNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

        }

        return tRoot;
    }

    private Element reportPolSpeUWInfo(String cContNo)
    {
        Element tRoot = new Element("SpecialUnderWritingDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.SpecialUnderWritingDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolSpeUWInfo(cContNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("UnderwritingResult"); //核保结论
            tTmpEle.setText(tDataInfo[0]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("UnderwritingDate"); //核保结论日期
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
        }

        return tRoot;
    }

    private Element reportPolSpeAmInfo(String cContNo)
    {
        Element tRoot = new Element("SpecialPolicyAmountDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.SpecialPolicyAmountDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolSpeAmInfo(cContNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("SectionNo"); //段序    
            tTmpEle.setText(String.valueOf(idxPI));
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Coststart"); //费用起线
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Costend"); //费用止线
            tTmpEle.setText(tDataInfo[2]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Deductible"); //免赔额  
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ClaimRatio"); //赔付比例
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
        }

        return tRoot;
    }

    private Element reportPolInsBnfInfo(String cContNo)
    {
        Element tRoot = new Element("PolicyBenefitDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.PolicyBenefitDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolInsBnfInfo(cContNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("Name"); //名称   
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Gender"); //性别        
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Birthday"); //出生日期      
            tTmpEle.setText(tDataInfo[2]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritType"); //证件类别      
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritCode"); //证件号码     
            tTmpEle.setText(tDataInfo[4]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RelationshipWithInsured"); //与被保险人关系
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("BenefitDistribution"); //受益分配方式  
            tTmpEle.setText(tDataInfo[6]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("OrderNo"); //顺序号        
            tTmpEle.setText(tDataInfo[7]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Proportion"); //比例值
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
        }

        return tRoot;
    }

    private SSRS loadPolInsBnfInfo(String cContNo)
    {
        SSRS tResult = null;

        StringBuffer tStrBSql = new StringBuffer();

        tStrBSql.append(" select ");
        tStrBSql
                .append(" '' Name, '' Gender, '' Birthday, '' CritType, '' CritCode, '' RelationshipWithInsured, '' BenefitDistribution, '' OrderNo, '' Proportion ");
        tStrBSql.append(" from dual ");
        tStrBSql.append(" where 1 = 2 ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadPolSpeAmInfo(String cPolNo)
    {
        SSRS tResult = null;

        StringBuffer tStrBSql = new StringBuffer();

        tStrBSql.append(" select ");
        tStrBSql.append(" RowNumber() over() as SectionNo, ");
        tStrBSql
                .append(" '' Coststart, '' Costend, cast(lcd.GetLimit as decimal(12,2)) Deductible, cast(lcd.GetRate as decimal(12,2)) ClaimRatio ");
        tStrBSql.append(" from LCDuty lcd ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcd.PolNo = '" + cPolNo + "' ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadPolSpeUWInfo(String tPolNo)
    {
        SSRS tResult = null;

        StringBuffer tStrBSql = new StringBuffer();

        //        tStrBSql.append(" select ");
        //        tStrBSql.append(" '' UnderwritingResult, '' UnderwritingDate ");
        //        tStrBSql.append(" from dual ");
        //        tStrBSql.append(" where 1 = 2 ");

        tStrBSql.append(" select ");
        tStrBSql.append(" lcp.UwFlag UnderwritingResult, lcp.UwDate UnderwritingDate ");
        tStrBSql.append(" from LCPol lcp ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.PolNo = '" + tPolNo + "' ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadPolInsCovInfo(String cPolNo)
    {
        SSRS tResult = null;

        StringBuffer tStrBSql = new StringBuffer();

        //        tStrBSql.append(" select ");
        //        tStrBSql
        //                .append(" '' LiabilityClassification, '' LiabilityCode, '' LiabilityName, '' LiabilityEffectiveDate, '' LiabilityExpireDate, '' LiabilityStatus, '' LiabilityPremium, '' LimitAmount, '' EffectivelyAmount, '' WaitingPeriod ");
        //        tStrBSql.append(" from dual ");
        //        tStrBSql.append(" where 1 = 2 ");

        tStrBSql.append(" select ");
        tStrBSql.append(" lcd.DutyCode LiabilityClassification, ");
        tStrBSql.append(" lcd.DutyCode LiabilityCode, lmd.DutyName LiabilityName, ");
        tStrBSql.append(" lcd.GetStartDate LiabilityEffectiveDate, (lcd.EndDate - 1 day) LiabilityExpireDate, ");
        tStrBSql.append(" lcp.StateFlag LiabilityStatus, lcd.Prem LiabilityPremium, ");
        tStrBSql.append(" lcd.Amnt LimitAmount, lcd.Amnt EffectivelyAmount, ");
        tStrBSql.append(" '0' WaitingPeriod, ");
        // tStrBSql.append(" trim(lcp.ContNo) || '_' || trim(lcp.InsuredNo) InsuredGroupCode, ");
        tStrBSql.append(" '' InsuredGroupCode, ");
        // tStrBSql.append(" trim(lcp.ContNo) || '_' || trim(lcp.InsuredNo) GroupName ");
        tStrBSql.append(" '' GroupName ");
        tStrBSql.append(" from LCPol lcp ");
        tStrBSql.append(" inner join LCDuty lcd on lcd.PolNo = lcp.PolNo ");
        tStrBSql.append(" inner join LMDuty lmd on lmd.DutyCode = lcd.DutyCode ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.polno = '" + cPolNo + "' ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadPolInsUpCovInfo(String cContNo, String cInsuredNo)
    {
        SSRS tResult = null;

        StringBuffer tStrBSql = new StringBuffer();

        //        tStrBSql.append(" select ");
        //        tStrBSql
        //                .append(" '' CoverageType, '' CoverageCode, '' ComCoverageCode, '' CoverageName, '' CoverageEffectiveDate, '' CoverageExpireDate, '' CoveragePremium, '' CoverageSuminsured, '' CoverageEffectiveSuminsured, '' SpecificBusiness, '' SpecificBusinessCode ");
        //        tStrBSql.append(" from dual ");
        //        tStrBSql.append(" where 1 = 2 ");

        tStrBSql.append(" select ");
        tStrBSql.append(" 'M' CoverageType, lcp.RiskCode CoverageCode, ");
        tStrBSql.append(" lcp.RiskCode ComCoverageCode, lmra.RiskName CoverageName, ");
        tStrBSql.append(" lcp.GetStartDate CoverageEffectiveDate, lcp.EndDate CoverageExpireDate, ");
        tStrBSql.append(" lmra.SubRiskFlag MainAttachedFlag, ");
        tStrBSql.append(" lcp.Prem CoveragePremium, ");
        tStrBSql.append(" lcp.Amnt CoverageSuminsured, lcp.Amnt CoverageEffectiveSuminsured, ");
        tStrBSql
                .append(" lcp.PayIntv PaymentMethod, (Case when lcp.PayIntv is null then '' else char(lcp.PayYears) end) PaymentYears, ");
        tStrBSql.append(" '0' SpecificBusiness, '' SpecificBusinessCode, ");
        tStrBSql.append(" lcp.PolNo ");
        tStrBSql.append(" from LCPol lcp ");
        tStrBSql.append(" inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.ContNo = '" + cContNo + "' and lcp.InsuredNo = '" + cInsuredNo + "' ");
        tStrBSql.append(" union all ");
        tStrBSql.append(" select ");
        tStrBSql.append(" 'M' CoverageType, lcp.RiskCode CoverageCode, ");
        tStrBSql.append(" lcp.RiskCode ComCoverageCode, lmra.RiskName CoverageName, ");
        tStrBSql.append(" lcp.GetStartDate CoverageEffectiveDate, lcp.EndDate CoverageExpireDate, ");
        tStrBSql.append(" lmra.SubRiskFlag MainAttachedFlag, ");
        tStrBSql.append(" lcp.Prem CoveragePremium, ");
        tStrBSql.append(" lcp.Amnt CoverageSuminsured, lcp.Amnt CoverageEffectiveSuminsured, ");
        tStrBSql
                .append(" lcp.PayIntv PaymentMethod, (Case when lcp.PayIntv is null then '' else char(lcp.PayYears) end) PaymentYears, ");
        tStrBSql.append(" '0' SpecificBusiness, '' SpecificBusinessCode, ");
        tStrBSql.append(" lcp.PolNo ");
        tStrBSql.append(" from LCPol lcp ");
        tStrBSql.append(" inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.GrpContNo = '" + cContNo + "' and lcp.InsuredNo = '" + cInsuredNo + "' ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadPolInsInfo(String cContNo, String cContType)
    {
        SSRS tResult = null;

        String tContNo = cContNo;

        StringBuffer tStrBSql = new StringBuffer();

        if (cContType.equals("1"))
        {
            //            tStrBSql.append(" select * from ( ");

            tStrBSql.append(" select ");
            tStrBSql.append(" lci.Name Name, lci.Sex Gender, lci.Birthday Birthday, ");
            tStrBSql.append(" lci.IDType CritType, lci.IDNo CritCode, lci.RelationToAppnt RelationshipWithPh, ");
            tStrBSql.append(" lci.InsuredNo CustomerNo, '0' AnomalyInform, '待定' OfferStatus, ");
            tStrBSql.append(" '' SubStartDate, '' SubEndDate, ");
            //            tStrBSql.append(" trim('" + tContNo + "') || '_' || trim(lci.InsuredNo) InsuredGroupCode, ");
            tStrBSql.append(" '' InsuredGroupCode, ");
            tStrBSql.append(" lci.Relationtomaininsured InsuredType, ");
            tStrBSql
                    .append(" (case when lci.Relationtomaininsured = '00' then '' else (select slci.InsuredNo from LCInsured slci where slci.ContNo = lci.ContNo and slci.Relationtomaininsured = '00') end) SnOfMainInsured, ");
            tStrBSql.append(" lci.Relationtomaininsured RelationshipWithMainInsured, ");
            tStrBSql.append(" '' ServiceMark, '' HealthFlag, '' SocialcareNo, ");
            tStrBSql.append(" lci.OccupationCode OccupationalCode, '' WorkPlace, ");
            tStrBSql.append(" '' EmpoyeeNo ");
            tStrBSql.append(" from lcinsured lci ");
            tStrBSql.append(" where 1 = 1 ");
            tStrBSql.append(" and lci.ContNo = '" + tContNo + "' ");

            //            tStrBSql.append(" ) as tmp fetch first row only");
        }

        if (cContType.equals("2"))
        {
            //            tStrBSql.append(" select * from ( ");

            tStrBSql.append(" select ");
            tStrBSql.append(" lci.Name Name, lci.Sex Gender, lci.Birthday Birthday, ");
            tStrBSql.append(" lci.IDType CritType, lci.IDNo CritCode, '07' RelationshipWithPh, ");
            tStrBSql.append(" lci.InsuredNo CustomerNo, '0' AnomalyInform, '9' OfferStatus, ");
            tStrBSql.append(" '' SubStartDate, '' SubEndDate, ");
            //            tStrBSql.append(" trim(lci.ContNo) || '_' || trim(lci.InsuredNo) InsuredGroupCode, ");
            tStrBSql.append(" '' InsuredGroupCode, ");
            tStrBSql.append(" lci.Relationtomaininsured InsuredType, ");
            tStrBSql
                    .append(" (case when lci.Relationtomaininsured = '00' then '' else (select slci.InsuredNo from LCInsured slci where slci.ContNo = lci.ContNo and slci.Relationtomaininsured = '00') end) SnOfMainInsured, ");
            tStrBSql.append(" lci.Relationtomaininsured RelationshipWithMainInsured, ");
            tStrBSql.append(" '' ServiceMark, '' HealthFlag, '' SocialcareNo, ");
            tStrBSql.append(" lci.OccupationCode OccupationalCode, '' WorkPlace, ");
            tStrBSql.append(" '' EmpoyeeNo ");
            tStrBSql.append(" from lcinsured lci ");
            tStrBSql.append(" where 1 = 1 ");
            tStrBSql.append(" and lci.GrpContNo = '" + tContNo + "' ");

            //            tStrBSql.append(" ) as tmp fetch first row only");
        }

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadPolPerInfo(String cContNo)
    {
        SSRS tResult = null;

        String tContNo = cContNo;

        StringBuffer tStrBSql = new StringBuffer();

        tStrBSql.append(" select ");
        tStrBSql.append(" lcap.AppntName Name, lcap.AppntSex Gender, ");
        tStrBSql.append(" lcap.AppntBirthday Birthday, lcap.IDType CritType, ");
        tStrBSql.append(" lcap.IDNo CritCode, '' RelationshipWithInsured ");
        tStrBSql.append(" from lcappnt lcap ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcap.ContNo = '" + tContNo + "' ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadPolGrpInfo(String cContNo)
    {
        SSRS tResult = null;

        String tContNo = cContNo;

        StringBuffer tStrBSql = new StringBuffer();

        tStrBSql.append(" select ");
        tStrBSql.append(" lgap.Name Name, ");
        tStrBSql.append(" (case when lgap.OrganComCode is not null then '2' else '9' end) CritType, ");
        tStrBSql
                .append(" (case when lgap.OrganComCode is null then '999999999' else lgap.OrganComCode end) CritCode, ");
        tStrBSql.append(" '' SocialSecurityNo, '' CompanyNature, ");
        tStrBSql.append(" '' IndustryClassification, '' LegalRepresentative, ");
        tStrBSql.append(" '' Contact, '' NUMBEROFUNITS ");
        tStrBSql.append(" from LCGrpAppnt lgap ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lgap.GrpContNo = '" + tContNo + "' ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadPolAmoInfo()
    {
        SSRS tResult = null;

        StringBuffer tStrBSql = new StringBuffer();

        tStrBSql.append(" select ");
        tStrBSql.append(" '' SectionNo, '' Coststart, '' Costend, '' Deductible, '' ClaimRatio ");
        tStrBSql.append(" from dual ");
        tStrBSql.append(" where 1 = 2 ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadPolUWInfo(String cPolNo)
    {
        SSRS tResult = null;

        String tPolNo = cPolNo;

        StringBuffer tStrBSql = new StringBuffer();

        tStrBSql.append(" select ");
        tStrBSql.append(" lcp.UwFlag UnderwritingResult, lcp.UwDate UnderwritingDate ");
        tStrBSql.append(" from LCPol lcp ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.PolNo = '" + tPolNo + "' ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadPolCovInfo(String cPolNo)
    {
        SSRS tResult = null;

        String tPolNo = cPolNo;

        StringBuffer tStrBSql = new StringBuffer();

        tStrBSql.append(" select * from ( ");

        tStrBSql.append(" select ");
        tStrBSql.append(" lcd.DutyCode LiabilityClassification, ");
        tStrBSql.append(" lcd.DutyCode LiabilityCode, lmd.DutyName LiabilityName, ");
        tStrBSql.append(" lcd.GetStartDate LiabilityEffectiveDate, (lcd.EndDate - 1 day) LiabilityExpireDate, ");
        tStrBSql.append(" lcp.StateFlag LiabilityStatus, lcd.Prem LiabilityPremium, ");
        tStrBSql.append(" lcd.Amnt LimitAmount, lcd.Amnt EffectivelyAmount, ");
        tStrBSql.append(" '0' WaitingPeriod, ");
        tStrBSql.append(" trim(lcp.ContNo) || '_' || trim(lcp.InsuredNo) InsuredGroupCode, ");
        //        tStrBSql.append(" '' InsuredGroupCode, ");
        tStrBSql.append(" trim(lcp.ContNo) || '_' || trim(lcp.InsuredNo) GroupName ");
        //        tStrBSql.append(" '' GroupName ");
        tStrBSql.append(" from LCPol lcp ");
        tStrBSql.append(" inner join LCDuty lcd on lcd.PolNo = lcp.PolNo ");
        tStrBSql.append(" inner join LMDuty lmd on lmd.DutyCode = lcd.DutyCode ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.polno = '" + tPolNo + "' ");

        tStrBSql.append(" ) as tmp fetch first row only");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadPolUpCovInfo(String cContNo)
    {
        SSRS tResult = null;

        String tContNo = cContNo;

        StringBuffer tStrBSql = new StringBuffer();

        //        tStrBSql.append(" select * from ( ");

        tStrBSql.append(" select ");
        tStrBSql.append(" 'M' CoverageType, lcp.RiskCode CoverageCode, ");
        tStrBSql.append(" lcp.RiskCode ComCoverageCode, lmra.RiskName CoverageName, ");
        tStrBSql.append(" lcp.GetStartDate CoverageEffectiveDate, lcp.EndDate CoverageExpireDate, ");
        tStrBSql.append(" lmra.SubRiskFlag MainAttachedFlag, ");
        tStrBSql.append(" lcp.Prem CoveragePremium, ");
        tStrBSql.append(" lcp.Amnt CoverageSuminsured, lcp.Amnt CoverageEffectiveSuminsured, ");
        tStrBSql
                .append(" lcp.PayIntv PaymentMethod, (Case when lcp.PayIntv is null then '' else char(lcp.PayYears) end) PaymentYears, ");
        tStrBSql.append(" '0' SpecificBusiness, '' SpecificBusinessCode, ");
        tStrBSql.append(" lcp.PolNo ");
        tStrBSql.append(" from LCPol lcp ");
        tStrBSql.append(" inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode ");
        tStrBSql.append(" where 1 = 2 ");
        tStrBSql.append(" and lcp.ContNo = '" + tContNo + "' ");
        tStrBSql.append(" union all ");
        tStrBSql.append(" select ");
        tStrBSql.append(" 'M' CoverageType, lcp.RiskCode CoverageCode, ");
        tStrBSql.append(" lcp.RiskCode ComCoverageCode, lmra.RiskName CoverageName, ");
        tStrBSql.append(" lcp.GetStartDate CoverageEffectiveDate, lcp.EndDate CoverageExpireDate, ");
        tStrBSql.append(" lmra.SubRiskFlag MainAttachedFlag, ");
        tStrBSql.append(" lcp.Prem CoveragePremium, ");
        tStrBSql.append(" lcp.Amnt CoverageSuminsured, lcp.Amnt CoverageEffectiveSuminsured, ");
        tStrBSql
                .append(" lcp.PayIntv PaymentMethod, (Case when lcp.PayIntv is null then '' else char(lcp.PayYears) end) PaymentYears, ");
        tStrBSql.append(" '0' SpecificBusiness, '' SpecificBusinessCode, ");
        tStrBSql.append(" lcp.PolNo ");
        tStrBSql.append(" from LCPol lcp ");
        tStrBSql.append(" inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode ");
        tStrBSql.append(" where 1 = 2 ");
        tStrBSql.append(" and lcp.GrpContNo = '" + tContNo + "' ");

        //        tStrBSql.append(" ) as tmp fetch first row only");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadPolicyInfo(String cDataInfoKey)
    {
        SSRS tResult = null;

        String tContNo = cDataInfoKey;

        StringBuffer tStrBSql = new StringBuffer();
        tStrBSql.append(" select ");
        tStrBSql.append(" lcc.UWFlag Rejection, lcc.PrtNo ApplicationFormNo, lcc.ContNo PolicyNo, ");
        tStrBSql.append(" lcc.PolApplyDate ApplicationDate, lcc.ContType ContractType, ");
        tStrBSql.append(" '' GroupMark, lcc.CValidate PolicyStartDate, ");
        tStrBSql.append(" lcc.CInvalidate PolicyEndDate, ");
        tStrBSql.append(" '' TerminationDate, '' SuspendDate, '' RecoverDate, ");
        tStrBSql.append(" '01' ContractSource, ");
        tStrBSql.append(" lcc.ContNo ContractNo, ");
        tStrBSql.append(" '' RenewalTimes, '' RenewalMethod, 'M' MainAttachedFlag, ");
        tStrBSql.append(" lcc.StateFlag ContractStatus, '' TerminationReason, ");
        tStrBSql.append(" lcc.Amnt SumInsured, lcc.Amnt EffectiveSumInsured, ");
        tStrBSql.append(" lcc.Prem Premium, lcc.Prem CurrentPremium, ");
        tStrBSql.append(" lcc.PayIntv PaymentMethod, '1' PaymentYears, ");
        tStrBSql.append(" '2' PolicyHolderPro, ");
        tStrBSql.append(" (select count(1) from lcinsured where contno = '" + tContNo + "') PolicyHolderNum, ");
        tStrBSql.append(" (select count(1) from lcinsured where contno = '" + tContNo + "') EffectiveInsuredNum, ");
        tStrBSql.append(" '' FormerpolicyNo, '' SpecialRemark, ");
        tStrBSql.append(" '' RegularClearingMark, ");
        tStrBSql.append(" '待定' RegularClearing, ");
        tStrBSql.append(" '' RegularClearingDate, ");
        tStrBSql.append(" lcc.SignDate PremiumDueDate, ");
        tStrBSql.append(" '1' RealTimeClaimFlag, ");
        tStrBSql.append(" '' ");
        tStrBSql.append(" from LCCont lcc ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcc.ContNo = '" + tContNo + "' ");
        tStrBSql.append(" and lcc.ContType = '1' ");
        tStrBSql.append(" union all ");
        tStrBSql.append(" select ");
        tStrBSql.append(" lgc.UWFlag Rejection, lgc.PrtNo ApplicationFormNo, lgc.GrpContNo PolicyNo, ");
        tStrBSql.append(" lgc.PolApplyDate ApplicationDate, '2' ContractType, ");
        tStrBSql.append(" lgc.GrpContNo GroupMark, lgc.CValidate PolicyStartDate, ");
        tStrBSql.append(" lgc.CInvalidate PolicyEndDate, ");
        tStrBSql.append(" '' TerminationDate, '' SuspendDate, '' RecoverDate, ");
        tStrBSql.append(" '01' ContractSource, ");
        tStrBSql.append(" lgc.GrpContNo ContractNo, ");
        tStrBSql.append(" '' RenewalTimes, '' RenewalMethod, 'M' MainAttachedFlag, ");
        tStrBSql.append(" lgc.StateFlag ContractStatus, '' TerminationReason, ");
        tStrBSql.append(" lgc.Amnt SumInsured, lgc.Amnt EffectiveSumInsured, ");
        tStrBSql.append(" lgc.Prem Premium, lgc.Prem CurrentPremium, ");
        tStrBSql.append(" lgc.PayIntv PaymentMethod, '1' PaymentYears, ");
        tStrBSql.append(" '1' PolicyHolderPro, ");
        tStrBSql.append(" (select count(1) from lcinsured where grpcontno = '" + tContNo + "') PolicyHolderNum, ");
        tStrBSql.append(" (select count(1) from lcinsured where grpcontno = '" + tContNo + "') EffectiveInsuredNum, ");
        tStrBSql.append(" '' FormerpolicyNo, '' SpecialRemark, ");
        tStrBSql.append(" '' RegularClearingMark, ");
        tStrBSql.append(" '待定' RegularClearing, ");
        tStrBSql.append(" '' RegularClearingDate, ");
        tStrBSql.append(" lgc.SignDate PremiumDueDate, ");
        tStrBSql.append(" '1' RealTimeClaimFlag, ");
        tStrBSql.append(" '' ");
        tStrBSql.append(" from LCGrpCont lgc ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lgc.GrpContNo = '" + tContNo + "' ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    /**
     * 调用前置机服务
     * @return
     */
    private boolean calService()
    {
        Document tDocument = formJDocMessage(mRoot, "01", mDataInfoKey);
        JdomUtil.print(tDocument);

        Document tInStdXmlDoc = tDocument;

        ReqJKXPreService tJKXPreServlet = new ReqJKXPreService();
        Document tReceiveXmlDoc = tJKXPreServlet.callService(tInStdXmlDoc);
        JdomUtil.print(tReceiveXmlDoc);

        return true;
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        System.out.println(szFunc + ": " + szErrMsg);

        CError cError = new CError();
        cError.moduleName = "AddCertifyListBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public static void main(String args[])
    {
        QYJKXServiceImp tBusLogic = new QYJKXServiceImp();
        VData tVData = new VData();
        TransferData mTransferData = new TransferData();
        mTransferData.setNameAndValue("DataInfoKey", "00002045000002");
        tVData.add(mTransferData);
        tBusLogic.callJKXService(tVData, null);
    }
}
