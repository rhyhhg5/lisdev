/**
 * 2010-11-18
 */
package com.sinosoft.lis.jkxpt.qy;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.jkxpt.JKXService;
import com.sinosoft.lis.jkxpt.ReqJKXPreService;
import com.sinosoft.lis.taskservice.DataTrackBL;
import com.sinosoft.lis.yibaotong.JdomUtil;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class QYACCServiceImp extends JKXService
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private TransferData mTransferData = null;

    private String mDataInfoKey = null;

    private Element mRoot = null;

    public boolean callJKXService(VData cInputData, String cOperate)
    {
        // 获取入口参数 
        if (!getInputData(cInputData, cOperate))
        {
            return false;
        }
        // --------------------

        // 取数逻辑处理        
        //        mRoot = dealBusiXml(mPolicyNo);
        mRoot = reportBusiXml(mDataInfoKey);
        // --------------------

        // 调用前置机
        if (!calService())
        {
            return false;
        }
        // --------------------

        return true;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {

        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mDataInfoKey = (String) mTransferData.getValueByName("DataInfoKey");
        if (mDataInfoKey == null || mDataInfoKey.equals(""))
        {
            buildError("getInputData", "获取保单合同号码失败。");
            return false;
        }

        return true;
    }

    private Element reportBusiXml(String cDataInfoKey)
    {
        Element tRoot = new Element("AccidentPolicyDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.accident.insurance.dto.AccidentPolicyDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolicyInfo(cDataInfoKey);
        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String tContNo = null;

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("ApplicationFormNo");
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ApplicationDate");
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PolicyNo");
            tContNo = tDataInfo[2];
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PolicyStartDate");
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PolicyEndDate");
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RenewalMethod");
            tTmpEle.setText(tDataInfo[5]);
            //tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RenewalTimes");
            tTmpEle.setText(tDataInfo[6]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Sales");
            tTmpEle.setText(tDataInfo[7]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SalesChannelCode");
            tTmpEle.setText(tDataInfo[8]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SalesChannelName");
            tTmpEle.setText(tDataInfo[9]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("BusinessAddress");
            tTmpEle.setText(tDataInfo[10]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ProductName");
            tTmpEle.setText(tDataInfo[11]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Premium");
            tTmpEle.setText(tDataInfo[12]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SpecialRemark");
            tTmpEle.setText(tDataInfo[13]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("underwritngDate");
            tTmpEle.setText(tDataInfo[14]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("singleDate");
            tTmpEle.setText(tDataInfo[15]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("salesChannelNameComment");
            tTmpEle.setText(tDataInfo[16]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            //获取个人投保人数组
            tTmpEle = reportPolPerInfo(tContNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

            //被保险人数组
            tTmpEle = reportPolInsInfo(tContNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

            // --------------------

        }

        return tRoot;
    }

    private Element reportPolPerInfo(String cContNo)
    {
        Element tRoot = new Element("AccidentPolicyPersonphDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.accident.insurance.dto.AccidentPolicyPersonphDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolPerInfo(cContNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("Name"); //姓名  
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Gender"); //性别       
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Birthday"); //出生日期      
            tTmpEle.setText(tDataInfo[2]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritType"); //证件类别      
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritCode"); //证件号码      
            tTmpEle.setText(tDataInfo[4]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RelationshipWithInsured"); //与被保险人关系
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("Mobile"); //手机号码
            tTmpEle.setText(tDataInfo[6]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
        }

        return tRoot;
    }

    private Element reportPolInsInfo(String cContNo)
    {
        Element tRoot = new Element("AccidentPolicyInsuredDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.accident.insurance.dto.AccidentPolicyInsuredDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolInsInfo(cContNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);
            
            tTmpEle = new Element("CustomerNo"); //客户编号    
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Name"); //姓名    
            tTmpEle.setText(tDataInfo[1]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Gender"); //性别       
            tTmpEle.setText(tDataInfo[2]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Birthday"); //出生日期        
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritType"); //被保险人证件类别    
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritCode"); //被保险人证件号码   
            tTmpEle.setText(tDataInfo[5]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("Mobile"); //手机号码   
            tTmpEle.setText(tDataInfo[6]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("OfferStatus"); //要约状态   
            tTmpEle.setText(tDataInfo[7]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SubStartDate"); //被保险人责任开始日期
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SubEndDate"); //被保险人责任终止日期
            tTmpEle.setText(tDataInfo[9]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("AppointedBenefit"); //是否指定受益人
            tTmpEle.setText(tDataInfo[10]);
            // tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            String tInsuredNo = tDataInfo[0];
            String tWFInsuredNo = tDataInfo[11];
            String tPrem = tDataInfo[12];
            String tAmnt = tDataInfo[13];

            //获取受益人数组
            tTmpEle = reportPolInsBnfInfo(cContNo,tInsuredNo,tWFInsuredNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
            
//          获取特别承保险种数组
            tTmpEle = reportPolInsUpCovInfo(cContNo, tInsuredNo,tPrem,tAmnt);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
        }

        return tRoot;
    }

    private Element reportPolInsUpCovInfo(String cContNo, String cInsuredNo,String cPrem,String cAmnt)
    {
        Element tRoot = new Element("AccidentPolicyUpcoverageDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.accident.insurance.dto.AccidentPolicyUpcoverageDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolInsUpCovInfo(cContNo, cInsuredNo,cPrem,cAmnt);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("CoverageCode"); //平台险种代码
            tTmpEle.setText(tDataInfo[0]);
            // tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ComCoverageCode"); //公司险种代码
            tTmpEle.setText(tDataInfo[1]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageName"); //公司险种名称
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageEffectiveDate"); //险种起保日期
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageExpireDate"); //险种满期日期
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("MainAttachedFlag"); //主附险性质
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoveragePremium"); //险种保费    
            tTmpEle.setText(tDataInfo[6]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageSuminsured"); //险种保额    
            tTmpEle.setText(tDataInfo[7]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PaymentMethod"); //缴费方式
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PaymentYears"); //缴费年限
            tTmpEle.setText(tDataInfo[9]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SpecificBusiness"); //特殊业务标识
            tTmpEle.setText(tDataInfo[10]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SpecificBusinessCode"); //特殊业务代码
            tTmpEle.setText(tDataInfo[11]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            String tPolNo = tDataInfo[12];   //-------------???
            String tPrem = tDataInfo[6];
            String tAmnt = tDataInfo[7];
            //获取特别承保责任数组
            tTmpEle = reportPolInsCovInfo(cContNo,tPolNo,tPrem,tAmnt);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

            // 获取特别核保结论数组
//            tTmpEle = reportPolSpeUWInfo(tPolNo);
//            if (tTmpEle != null)
//            {
//                tItemEle.addContent(tTmpEle);
//            }
//            tTmpEle = null;

        }

        return tRoot;
    }

    private Element reportPolInsCovInfo(String cContNo,String cPolNo,String tPrem,String tAmnt)
    {
        Element tRoot = new Element("AccidentPolicyCoverageDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.accident.insurance.dto.AccidentPolicyCoverageDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolInsCovInfo(cContNo,cPolNo,tPrem,tAmnt);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("LiabilityCode"); //责任代码    
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityName"); //责任名称    
            tTmpEle.setText(tDataInfo[1]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityEffectiveDate"); //责任开始日期
            tTmpEle.setText(tDataInfo[2]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityExpireDate"); //责任终止日期
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityStatus"); //责任状态    
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityPremium"); //责任保费    
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LimitAmount"); //责任保额    
            tTmpEle.setText(tDataInfo[6]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("WaitingPeriod"); //免责期      
            tTmpEle.setText(tDataInfo[7]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("Remark"); //备注      
            tTmpEle.setText(tDataInfo[8]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            //获取特别赔付比例分段数组
//            tTmpEle = reportPolSpeAmInfo(cPolNo);
//            if (tTmpEle != null)
//            {
//                tItemEle.addContent(tTmpEle);
//            }
//            tTmpEle = null;

        }

        return tRoot;
    }


    private Element reportPolInsBnfInfo(String cContNo,String cInsuredNo,String cWFInsuredNo)
    {
        Element tRoot = new Element("AccidentPolicyBenefitDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.accident.insurance.dto.AccidentPolicyBenefitDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolInsBnfInfo(cContNo,cInsuredNo,cWFInsuredNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("Name"); //名称   
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Gender"); //性别        
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Birthday"); //出生日期      
            tTmpEle.setText(tDataInfo[2]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritType"); //证件类别      
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritCode"); //证件号码     
            tTmpEle.setText(tDataInfo[4]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RelationshipWithInsured"); //与被保险人关系
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("OrderNo"); //顺序号        
            tTmpEle.setText(tDataInfo[6]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Proportion"); //比例值
            tTmpEle.setText(tDataInfo[7]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
        }

        return tRoot;
    }

    private SSRS loadPolInsBnfInfo(String cContNo,String cInsuredNo,String cWFInsuredNo)
    {
        SSRS tResult = null;

        StringBuffer tStrBSql = new StringBuffer();

        tStrBSql.append(" select ");
        tStrBSql.append(" lcb.name Name, ");//姓名
        tStrBSql.append(" lcb.sex Gender, ");//性别
        tStrBSql.append(" lcb.birthday Birthday, ");//出生日期
        tStrBSql.append(" lcb.idtype CritType, ");//证件类别
        tStrBSql.append(" lcb.idno CritCode, ");//证件号码
        tStrBSql.append(" lcb.RelationToInsured RelationshipWithInsured, ");//与被保险人关系
        tStrBSql.append(" lcb.BnfGrade OrderNo, ");//顺序号
        tStrBSql.append(" decimal(round(lcb.BnfLot,2),10,2) Proportion ");//比例值
        tStrBSql.append(" from lcbnf lcb ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and contno = '"+cContNo+"' ");
        tStrBSql.append(" and insuredno = '"+cInsuredNo+"' ");
        tStrBSql.append(" union all ");
        tStrBSql.append(" select ");
        tStrBSql.append(" wfbnf.name Name, ");//姓名
        tStrBSql.append(" wfbnf.sex Gender, ");//性别
        tStrBSql.append(" wfbnf.birthday Birthday, ");//出生日期
        tStrBSql.append(" wfbnf.idtype CritType, ");//证件类别
        tStrBSql.append(" wfbnf.idno CritCode, ");//证件号码
        tStrBSql.append(" wfbnf.toinsurela RelationshipWithInsured, ");//与被保险人关系
        tStrBSql.append(" wfbnf.BnfGrade OrderNo, ");//顺序号
        tStrBSql.append(" wfbnf.Bnfrate Proportion ");//比例值
        tStrBSql.append(" from wfbnflist wfbnf ");
//        tStrBSql.append(" inner join licertify lic on lic.cardno = wfbnf.cardno ");
        tStrBSql.append(" inner join licardactiveinfolist lic on lic.cardno = wfbnf.cardno ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and wfbnf.cardno = '"+cContNo+"' ");
        tStrBSql.append(" and wfbnf.toinsuno = '"+cWFInsuredNo+"' ");
//        tStrBSql.append(" and lic.state = '04' ");//已签单
//        tStrBSql.append(" and double(lic.amnt) >0.00001 ");//保额>0

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }


    private SSRS loadPolInsCovInfo(String cContNo,String cPolNo,String tPrem,String tAmnt)
    {
        SSRS tResult = null;

        StringBuffer tStrBSql = new StringBuffer();

        tStrBSql.append(" select ");
        tStrBSql.append(" lcd.DutyCode LiabilityCode, ");//责任代码
        tStrBSql.append(" lmd.DutyName LiabilityName, ");//责任名称
        tStrBSql.append(" lcd.GetStartDate LiabilityEffectiveDate, ");//责任开始日期
        tStrBSql.append(" (lcd.EndDate -1 day) LiabilityExpireDate, ");//责任终止日期
        tStrBSql.append(" lcp.StateFlag LiabilityStatus, ");//责任状态
        tStrBSql.append(" lcd.Prem LiabilityPremium, ");//责任保费
        tStrBSql.append(" lcd.Amnt LimitAmount, ");//责任保额
        tStrBSql.append(" '0' WaitingPeriod, ");//免责期
        tStrBSql.append(" '' Remark ");//备注
        tStrBSql.append(" from LCPol lcp ");
        tStrBSql.append(" inner join LCDuty lcd on lcd.PolNo = lcp.PolNo ");
        tStrBSql.append(" inner join LMDuty lmd on lmd.DutyCode = lcd.DutyCode ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.polno = '" + cPolNo + "' ");
        tStrBSql.append(" union all ");
        tStrBSql.append(" select X.LiabilityCode LiabilityCode,X.LiabilityName LiabilityName,X.LiabilityEffectiveDate,X.LiabilityExpireDate LiabilityExpireDate, ");
        tStrBSql.append(" X.LiabilityStatus LiabilityStatus,");
        tStrBSql.append(" case when X.RowId = 1 then "+tPrem+" else 0 end LiabilityPremium,");
        tStrBSql.append(" case when X.RowId = 1 then "+tAmnt+" else 0 end LimitAmount,");
        tStrBSql.append(" X.WaitingPeriod WaitingPeriod,X.Remark Remark");
        tStrBSql.append(" from ( ");
        tStrBSql.append(" select ");
        tStrBSql.append(" row_number() over() as RowId, ");
        tStrBSql.append(" lrd.DutyCode LiabilityCode, ");//责任代码
        tStrBSql.append(" lmd.DutyName LiabilityName, ");//责任名称
//        tStrBSql.append(" (select cvalidate from licardactiveinfolist where cardno = lic.cardno fetch first 1 rows only) LiabilityEffectiveDate, ");//责任开始日期
//        tStrBSql.append(" (select inactivedate from licardactiveinfolist where cardno = lic.cardno fetch first 1 rows only) LiabilityExpireDate, ");//责任终止日期
        tStrBSql.append(" lic.cvalidate LiabilityEffectiveDate, ");//责任开始日期
        tStrBSql.append(" (lic.cvalidate + 1 year -1 day) LiabilityExpireDate, ");//责任终止日期
        tStrBSql.append(" '1' LiabilityStatus, ");//责任状态
        tStrBSql.append(" '' LiabilityPremium, ");//责任保费
        tStrBSql.append(" '' LimitAmount, ");//责任保额
        tStrBSql.append(" '0' WaitingPeriod, ");//免责期
        tStrBSql.append(" '' Remark ");//备注
        tStrBSql.append(" from licardactiveinfolist lic, ");
        tStrBSql.append(" LMRiskDuty lrd, ");
        tStrBSql.append(" LMDuty lmd  ");
        tStrBSql.append(" where 1 = 1 ");
//        tStrBSql.append(" and lic.state = '04' ");//已签单
//        tStrBSql.append(" and double(lic.amnt) >0.00001 ");//保额>0
        tStrBSql.append(" and lic.cardno = '" + cContNo + "' ");
        tStrBSql.append(" and lrd.riskcode = '" + cPolNo + "' ");
        tStrBSql.append(" and lmd.DutyCode = lrd.DutyCode ");
        tStrBSql.append(" ) as X ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadPolInsUpCovInfo(String cContNo, String cInsuredNo,String cPrem,String cAmnt)
    {
        SSRS tResult = null;

        StringBuffer tStrBSql = new StringBuffer();

        tStrBSql.append(" select ");
        tStrBSql.append(" Case when lcp.riskcode in ('5601','1607') then 'A02' else 'A01' end CoverageCode, ");//平台险种代码
        tStrBSql.append(" lcp.RiskCode ComCoverageCode, ");//公司险种代码
        tStrBSql.append(" lmra.RiskName CoverageName, ");//公司险种名称
        tStrBSql.append(" lcp.GetStartDate CoverageEffectiveDate, ");//险种起保日期
        tStrBSql.append(" (lcp.EndDate -1 day) CoverageExpireDate, ");//险种满期日期
        tStrBSql.append(" lmra.SubRiskFlag MainAttachedFlag, ");//主附险性质
        tStrBSql.append(" lcp.Prem CoveragePremium, ");//险种保费
        tStrBSql.append(" lcp.Amnt CoverageSuminsured, ");//险种保额
        tStrBSql.append(" lcp.PayIntv PaymentMethod, ");//缴费方式
        tStrBSql.append(" (Case when lcp.PayIntv is null then '' else char(lcp.PayYears) end) PaymentYears, ");//缴费年限
        tStrBSql.append(" '0' SpecificBusiness, ");//特殊业务标识
        tStrBSql.append(" '' SpecificBusinessCode, ");//特殊业务代码
        tStrBSql.append(" lcp.polno ");
        tStrBSql.append(" from LCPol lcp ");
        tStrBSql.append(" inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.ContNo = '" + cContNo + "' and lcp.InsuredNo = '" + cInsuredNo + "' ");
        tStrBSql.append(" union all ");
        tStrBSql.append(" select ");
        tStrBSql.append(" X.CoverageCode CoverageCode,X.ComCoverageCode ComCoverageCode,X.CoverageName CoverageName, ");
        tStrBSql.append(" X.CoverageEffectiveDate CoverageEffectiveDate,X.CoverageExpireDate CoverageExpireDate,X.MainAttachedFlag MainAttachedFlag, ");
        tStrBSql.append(" case when X.RowId = 1 then "+cPrem+" else 0 end CoveragePremium, ");
        tStrBSql.append(" case when X.RowId = 1 then "+cAmnt+" else 0 end CoverageSuminsured, ");
        tStrBSql.append(" X.PaymentMethod,X.PaymentYears,X.SpecificBusiness,X.SpecificBusinessCode,X.ComCoverageCode ");
        tStrBSql.append(" from ( ");
        tStrBSql.append(" select ");
        tStrBSql.append(" row_number() over() as RowId, ");
        tStrBSql.append(" Case when lrw.riskcode in ('5601','1607') then 'A02' else 'A01' end CoverageCode, ");//平台险种代码
        tStrBSql.append(" lrw.RiskCode ComCoverageCode, ");//公司险种代码
        tStrBSql.append(" (select riskname from lmrisk where riskcode = lrw.riskcode) CoverageName, ");//公司险种名称
//        tStrBSql.append(" (select cvalidate from licardactiveinfolist where cardno = lic.cardno fetch first 1 rows only) CoverageEffectiveDate, ");//险种起保日期
//        tStrBSql.append(" (select inactivedate from licardactiveinfolist where cardno = lic.cardno fetch first 1 rows only) CoverageExpireDate, ");//险种满期日期
        tStrBSql.append(" lic.cvalidate CoverageEffectiveDate, ");//险种起保日期
        tStrBSql.append(" (lic.cvalidate + 1 year - 1 day) CoverageExpireDate, ");//险种满期日期
        tStrBSql.append(" lmra.SubRiskFlag MainAttachedFlag, ");//主附险性质
        tStrBSql.append(" '' CoveragePremium, ");//险种保费
        tStrBSql.append(" '' CoverageSuminsured, ");//险种保额
//        tStrBSql.append(" (select payintv from lcgrpcont where grpcontno = lic.grpcontno)  PaymentMethod, ");//缴费方式 根据代码应为缴费频次
        tStrBSql.append(" 0 PaymentMethod, ");//缴费方式 根据代码应为缴费频次
        tStrBSql.append(" '1' PaymentYears, ");//缴费年限
        tStrBSql.append(" '0' SpecificBusiness, ");//特殊业务标识
        tStrBSql.append(" '' SpecificBusinessCode, ");//特殊业务代码
        tStrBSql.append(" '' ");
        tStrBSql.append(" from licardactiveinfolist lic ");
        tStrBSql.append(" inner join wfcontlist wfcont on lic.cardno = wfcont.cardno ");
        tStrBSql.append(" inner join ldriskwrap lrw on lrw.riskwrapcode = wfcont.riskcode ");
        tStrBSql.append(" inner join LMRiskApp lmra on lmra.RiskCode = lrw.RiskCode ");
        tStrBSql.append(" where 1=1 ");
        tStrBSql.append(" and lic.cardno = '"+cContNo+"' ");//保额>0
//        tStrBSql.append(" and lic.state = '04' ");//已签单
//        tStrBSql.append(" and double(lic.amnt) >0.00001 ");//保额>0
        tStrBSql.append(" ) as X ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadPolInsInfo(String cContNo)
    {
        SSRS tResult = null;
        String tContNo = cContNo;
        StringBuffer tStrBSql = new StringBuffer();

        tStrBSql.append(" select ");
        tStrBSql.append(" lci.insuredno CustomerNo, ");//客户编号
        tStrBSql.append(" lci.Name Name, ");//姓名
        tStrBSql.append(" lci.Sex Gender, ");//性别
        tStrBSql.append(" lci.Birthday Birthday, ");//出生日期
        tStrBSql.append(" lci.IDType CritType, ");//被保险人证件类别
        tStrBSql.append(" lci.IDNo CritCode, ");//被保险人证件号码
        tStrBSql.append(" (select case when (lcad.Mobile is not null and lcad.Mobile != '') then lcad.Mobile when (lcad.Phone is not null and lcad.Phone!='') then lcad.Phone else lci.grpinsuredphone end  from lcaddress lcad where lcad.customerno =lci.insuredno and lcad.addressno = lci.addressno ) Mobile, ");//手机号码
        tStrBSql.append(" '1' OfferStatus, ");//要约状态
        tStrBSql.append(" (select cvalidate from lccont where contno = '"+cContNo+"' ) SubStartDate, ");//被保险人责任开始日期
        tStrBSql.append(" ((select cinvalidate from lccont where contno = '"+cContNo+"' )-1 day) SubEndDate, ");//被保险人责任终止日期
        tStrBSql.append(" (case when exists(select 1 from lcbnf where contno = '"+cContNo+"' and insuredno = lci.insuredno ) then 1 else 0 end) AppointedBenefit, ");//是否指定受益人
        tStrBSql.append(" '',0,0 ");
        tStrBSql.append(" from lcinsured lci ");
//        tStrBSql.append(" inner join lcaddress lcad ");
//        tStrBSql.append(" on lci.insuredno = lcad.customerno ");
//        tStrBSql.append(" and lci.addressno = lcad.addressno ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lci.ContNo = '" + tContNo + "' ");
        tStrBSql.append(" union all ");
        tStrBSql.append(" select X.CustomerNo,X.Name,X.Gender,X.Birthday,X.CritType,X.CritCode,X.Mobile,X.OfferStatus, ");
        tStrBSql.append(" X.SubStartDate,X.SubEndDate,X.AppointedBenefit,X.Insuno, ");
        tStrBSql.append(" case when X.RowId = 1 then X.Prem else 0 end Prem,");
        tStrBSql.append(" case when X.RowId = 1 then X.Amnt else 0 end Amnt");
        tStrBSql.append(" from ( ");
        tStrBSql.append(" select ");
        tStrBSql.append(" row_number() over() as RowId, ");
        tStrBSql.append(" wfinsu.cardno || wfinsu.idno CustomerNo, ");//客户编号
        tStrBSql.append(" wfinsu.Name Name, ");//姓名
        tStrBSql.append(" wfinsu.Sex Gender, ");//性别
        tStrBSql.append(" wfinsu.Birthday Birthday, ");//出生日期
        tStrBSql.append(" wfinsu.IDType CritType, ");//被保险人证件类别
        tStrBSql.append(" wfinsu.IDNo CritCode, ");//被保险人证件号码
        tStrBSql.append(" case when wfinsu.mobile is not null and wfinsu.mobile != '' then wfinsu.mobile else wfinsu.phont end Mobile, ");//手机号码
        tStrBSql.append(" '1' OfferStatus, ");//要约状态
//        tStrBSql.append(" (select cvalidate from licardactiveinfolist where cardno = wfinsu.cardno fetch first 1 rows only) SubStartDate, ");//被保险人责任开始日期
//        tStrBSql.append(" (select inactivedate from licardactiveinfolist where cardno = wfinsu.cardno fetch first 1 rows only) SubEndDate, ");//被保险人责任开始日期
        tStrBSql.append(" lic.cvalidate SubStartDate, ");//被保险人责任开始日期
        tStrBSql.append(" (lic.cvalidate + 1 year - 1 day) SubEndDate, ");//被保险人责任开始日期
        tStrBSql.append(" (case when exists(select 1 from wfbnflist where cardno = wfinsu.cardno and toinsuno = wfinsu.insuno fetch first 1 rows only) then 1 else 0 end) AppointedBenefit, ");//是否指定受益人
        tStrBSql.append(" wfinsu.insuno Insuno,double(lic.prem) Prem,double(lic.amnt) Amnt ");
        tStrBSql.append(" from wfinsulist wfinsu ");
        tStrBSql.append(" inner join licardactiveinfolist lic on lic.cardno = wfinsu.cardno ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and wfinsu.cardno = '" + tContNo + "' ");
//        tStrBSql.append(" and lic.state = '04' ");//已签单
//        tStrBSql.append(" and double(lic.amnt) >0.00001 ");//保额>0
        tStrBSql.append(" ) as X ");
        
        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadPolPerInfo(String cContNo)
    {
        SSRS tResult = null;

        String tContNo = cContNo;

        StringBuffer tStrBSql = new StringBuffer();

        tStrBSql.append(" select ");
        tStrBSql.append(" lcap.AppntName Name, ");//姓名
        tStrBSql.append(" lcap.AppntSex Gender, ");//性别
        tStrBSql.append(" lcap.AppntBirthday Birthday, ");//出生日期
        tStrBSql.append(" lcap.IDType CritType, ");//证件类别
        tStrBSql.append(" lcap.IDNo CritCode, ");//证件号码
        tStrBSql.append(" case when (select count(1) from lcinsured where contno = lcap.contno) >1 then '99' else (select relationtoappnt from lcinsured where contno = lcap.contno fetch first 1 row only) end RelationshipWithInsured, ");//与被保险人关系
        tStrBSql.append(" case when lcad.mobile is not null and lcad.mobile != '' then lcad.mobile else lcad.phone end Mobile ");//手机号码
        tStrBSql.append(" from lcappnt lcap ");
        tStrBSql.append(" inner join lcaddress lcad");
        tStrBSql.append(" on lcap.appntno = lcad.customerno ");
        tStrBSql.append(" and lcap.addressno = lcad.addressno ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcap.ContNo = '" + tContNo + "' ");
        tStrBSql.append(" union all ");
        tStrBSql.append(" select ");
        tStrBSql.append(" wfapp.Name Name, ");//姓名
        tStrBSql.append(" wfapp.Sex Gender, ");//性别
        tStrBSql.append(" wfapp.Birthday Birthday, ");//出生日期
        tStrBSql.append(" wfapp.IDType CritType, ");//证件类别
        tStrBSql.append(" wfapp.IDNo CritCode, ");//证件号码
        tStrBSql.append(" case when (select count(1) from wfinsulist where cardno = wfapp.cardno) >1 then '99' else (select toappntrela from wfinsulist where cardno = wfapp.cardno fetch first 1 rows only) end RelationshipWithInsured, ");//与被保险人关系
        tStrBSql.append(" case when wfapp.mobile is not null and wfapp.mobile !='' then wfapp.mobile else wfapp.phont end Mobile ");//手机号码
        tStrBSql.append(" from WFAppntList wfapp ");
        tStrBSql.append(" inner join licardactiveinfolist lic on lic.cardno = wfapp.cardno ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and wfapp.cardno = '" + tContNo + "' ");
//        tStrBSql.append(" and lic.state = '04' ");//已签单
//        tStrBSql.append(" and double(lic.amnt) >0.00001 ");//保额>0
        

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }


    private SSRS loadPolicyInfo(String cDataInfoKey)
    {
        SSRS tResult = null;
        
        String tContNo = cDataInfoKey;
        //获取产品名称
        String tProductName = "";
        String tSql = "select riskname from lmrisk where riskcode in (select distinct riskcode from lcpol where contno = '"+tContNo+"' )";
        SSRS tSSRS = new ExeSQL().execSQL(tSql);
        if(tSSRS == null || tSSRS.getMaxRow()<=0){
        	//System.out.println("通过个单方式获取保单【"+tContNo+"】的产品名称时失败！");
        }else{
        	for(int i=1;i<=tSSRS.MaxRow;i++){
            	if("".equals(tProductName)){
            		tProductName += tSSRS.GetText(i,1);
            	}else{
            		tProductName += ","+tSSRS.GetText(i,1);
            	}
            }
        }
        if("".equals(tProductName)){
        	String tSQL = "select certifyname from lmcertifydes where certifycode in (select certifycode from lmcardrisk a,licardactiveinfolist b where a.riskcode = b.cardtype and b.cardno = '"+tContNo+"' )";
        	tProductName = new ExeSQL().getOneValue(tSQL);
        }
        if(tProductName == null || "".equals(tProductName)){
        	System.out.println("获取保单【"+tContNo+"】的产品名称时失败！");
        	return tResult;
        }
        
        StringBuffer tStrBSql = new StringBuffer();
        tStrBSql.append(" select ");
        tStrBSql.append(" lcc.Prtno ApplicationFormNo, ");//投保单号/卡号
        tStrBSql.append(" lcc.PolApplyDate ApplicationDate, ");//投保申请日期
        tStrBSql.append(" lcc.ContNo PolicyNo, ");//保单号码
        tStrBSql.append(" lcc.CValiDate PolicyStartDate, ");//保单起保时间
        tStrBSql.append(" (lcc.CInValiDate -1 day) PolicyEndDate, ");//保单满期时间
        tStrBSql.append(" '01' RenewalMethod, ");//投保方式
        tStrBSql.append(" '0' RenewalTimes, ");//续保次数
        tStrBSql.append(" lcc.SaleChnl Sales, ");//销售渠道
        tStrBSql.append(" case when (lcc.AgentCom is null or lcc.AgentCom = '') then lcc.managecom else lcc.AgentCom end SalesChannelCode, ");//销售机构代码
        tStrBSql.append(" case when (lcc.agentcom = '' or lcc.agentcom is null) then (select name from ldcom where comcode = lcc.managecom) else (select name from lacom where agentcom = lcc.agentcom) end SalesChannelName, ");//销售机构名称
        tStrBSql.append(" (case when lcc.SaleChnl in ('01', '02', '06', '07', '13') then '北京市西城区阜成门外大街7号' when lcc.SaleChnl in ('03', '04', '10') then '北京市' end) BusinessAddress, ");//营业场所地址
        tStrBSql.append(" '"+tProductName+"' ProductName, ");//产品名称
        tStrBSql.append(" lcc.prem Premium, ");//保费
        tStrBSql.append(" lcc.remark SpecialRemark, ");//特别约定
        tStrBSql.append(" (case when lcc.UWDate < lcc.CValiDate then lcc.UWDate else lcc.CValiDate - 1 day end) underwritngDate, ");//核保日期
        tStrBSql.append(" lcc.SignDate singleDate, ");//签单日期
        tStrBSql.append(" '' salesChannelNameComment ");//销售机构名称备注
        tStrBSql.append(" from LCCont lcc ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcc.ContNo = '" + tContNo + "' ");
        tStrBSql.append(" and lcc.ContType = '1' ");
        tStrBSql.append(" union all ");
        tStrBSql.append(" select ");//卡折数据
        tStrBSql.append(" lic.CardNo ApplicationFormNo, ");//投保单号/卡号
//        tStrBSql.append(" (select ActiveDate from licardactiveinfolist where cardno = lic.cardno fetch first 1 rows only) ApplicationDate, ");//投保申请日期
        tStrBSql.append(" lic.ActiveDate ApplicationDate, ");//投保申请日期
        tStrBSql.append(" lic.CardNo PolicyNo, ");//保单号码
//        tStrBSql.append(" (select CValiDate from licardactiveinfolist where cardno = lic.cardno fetch first 1 rows only ) PolicyStartDate, ");//保单起保时间
//        tStrBSql.append(" (select InActiveDate from licardactiveinfolist where cardno = lic.cardno fetch first 1 rows only) PolicyEndDate, ");//保单满期时间
        tStrBSql.append(" lic.CValiDate PolicyStartDate, ");//保单起保时间
        tStrBSql.append(" (lic.CValiDate + 1 year -1 day) PolicyEndDate, ");//保单满期时间
        tStrBSql.append(" '01' RenewalMethod, ");//投保方式
        tStrBSql.append(" '0' RenewalTimes, ");//续保次数
        tStrBSql.append(" '03' Sales, ");//销售渠道
//        tStrBSql.append(" case when (lic.AgentCom is null or lic.AgentCom = '') then lic.managecom else lic.AgentCom end SalesChannelCode, ");//销售机构代码
//        tStrBSql.append(" case when (lic.agentcom = '' or lic.agentcom is null) then (select name from ldcom where comcode = lic.managecom) else (select name from lacom where agentcom = lic.agentcom) end SalesChannelName, ");//销售机构名称
        tStrBSql.append(" '86110000' SalesChannelCode, ");//销售机构代码
        tStrBSql.append(" (select name from ldcom where comcode = '86110000') SalesChannelName, ");//销售机构名称
        tStrBSql.append(" ('北京市') BusinessAddress, ");//营业场所地址
        tStrBSql.append(" '"+tProductName+"' ProductName, ");//产品名称
        tStrBSql.append(" double(lic.prem) Premium, ");//保费
        tStrBSql.append(" lic.remark SpecialRemark, ");//特别约定
        tStrBSql.append(" lic.ActiveDate underwritngDate, ");//激活日期
        tStrBSql.append(" lic.ActiveDate singleDate, ");//激活日期
        tStrBSql.append(" '' salesChannelNameComment ");//销售机构名称备注
        tStrBSql.append(" from licardactiveinfolist lic ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lic.CardNo = '" + tContNo + "' ");
//        tStrBSql.append(" and lic.state = '04' ");
//        tStrBSql.append(" and double(lic.amnt) >0.00001 ");
        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    /**
     * 调用前置机服务
     * @return
     */
    private boolean calService()
    {
        Document tDocument = formJDocMessage(mRoot, "Acc01", mDataInfoKey);
        JdomUtil.print(tDocument);

        Document tInStdXmlDoc = tDocument;

        ReqJKXPreService tJKXPreServlet = new ReqJKXPreService();
        String re = tJKXPreServlet.callService(tInStdXmlDoc);
        
        try
	        {
    		 DataTrackBL mDataTrackBL=new DataTrackBL();
    		 if(!mDataTrackBL.submitData(mDataInfoKey, "Acc01", re)){
    			 System.out.println("契约个单报送插入报送轨迹表出错");
    		 }
	        }
	        catch (Exception ex)
	        {
	            // @@错误处理
	            CError.buildErr(this,"契约个单报送插入报送轨迹表出错。");
	            
	        }        
        return true;
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        System.out.println(szFunc + ": " + szErrMsg);

        CError cError = new CError();
        cError.moduleName = "AddCertifyListBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public static void main(String args[])
    {
        QYACCServiceImp tBusLogic = new QYACCServiceImp();
        VData tVData = new VData();
        TransferData mTransferData = new TransferData();
        mTransferData.setNameAndValue("DataInfoKey", "000008627000003");//000995918000001  TP000000406
        tVData.add(mTransferData);
        tBusLogic.callJKXService(tVData, null);
    }
}
