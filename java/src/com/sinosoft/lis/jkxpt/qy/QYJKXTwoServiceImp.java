/**
 * 2010-11-18
 */
package com.sinosoft.lis.jkxpt.qy;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.jkxpt.JKXService;
import com.sinosoft.lis.jkxpt.ReqJKXPreService;
import com.sinosoft.lis.yibaotong.JdomUtil;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class QYJKXTwoServiceImp extends JKXService
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private TransferData mTransferData = null;

    /** 数据操作字符串 */
    private String mOperate = "";

    private String mDataInfoKey = null;

    private Element mRoot = null;

    public boolean callJKXService(VData cInputData, String cOperate)
    {
        // 获取入口参数 
        if (!getInputData(cInputData, cOperate)) 
        {
            return false;
        }
        // --------------------

        // 取数逻辑处理        
        //        mRoot = dealBusiXml(mPolicyNo);
        mRoot = reportBusiXml(mDataInfoKey);
        // --------------------

        // 调用前置机
        if (!calService())
        {
            return false;
        }
        // --------------------

        return true;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mDataInfoKey = (String) mTransferData.getValueByName("DataInfoKey");
        if (mDataInfoKey == null || mDataInfoKey.equals(""))
        {
            buildError("getInputData", "获取保单合同号码失败。");
            return false;
        }

        return true;
    }

    private Element reportBusiXml(String cDataInfoKey) //保单详细信息
    {
        Element tRoot = new Element("PolicyDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.PolicyDTO");

        Element tTmpEle = null;
        //保单详细信息
        SSRS tResPolicyInfo = loadPolicyInfo(cDataInfoKey);
        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String tContNo = null;
            String tContType = null;

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);
            //start -------------------------
            tTmpEle = new Element("SplitPolicyNo");
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            //end --------------------------
            tTmpEle = new Element("Rejection");
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ApplicationFormNo");
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PolicyNo");
            tContNo = tDataInfo[3];
            tTmpEle.setText(tDataInfo[3]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ApplicationDate");
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            //start -------------------------
            
            tTmpEle = new Element("AcceptDate");
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("OrganId");
            tTmpEle.setText(tDataInfo[6]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("OrganName");
            tTmpEle.setText(tDataInfo[7]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("ChannelType");
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("SalesType");
            tTmpEle.setText(tDataInfo[9]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("SalesCode");
            tTmpEle.setText(tDataInfo[10]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("SalesName");
            tTmpEle.setText(tDataInfo[11]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("SalesChannelCode");
            tTmpEle.setText(tDataInfo[12]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("SalesChannelName");
            tTmpEle.setText(tDataInfo[13]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("BusinessAddress");
            tTmpEle.setText(tDataInfo[14]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            //end -------------------------
            tTmpEle = new Element("ContractType");
            tContType = tDataInfo[15];
            tTmpEle.setText(tDataInfo[15]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

//            tTmpEle = new Element("GroupMark");
//            // tTmpEle.setText(tDataInfo[5]);
//            tItemEle.addContent(tTmpEle);
//            tTmpEle = null;

            tTmpEle = new Element("PolicyStartDate");
            tTmpEle.setText(tDataInfo[16]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PolicyEndDate");
            tTmpEle.setText(tDataInfo[17]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("TerminationDate");
            tTmpEle.setText(tDataInfo[18]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SuspendDate");
            tTmpEle.setText(tDataInfo[19]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RecoverDate");
            tTmpEle.setText(tDataInfo[20]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ContractSource");
            tTmpEle.setText(tDataInfo[21]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ContractNo");
            tTmpEle.setText(tDataInfo[22]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RenewalTimes");
            tTmpEle.setText(tDataInfo[23]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RenewalMethod");
            tTmpEle.setText(tDataInfo[24]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

//            tTmpEle = new Element("MainAttachedFlag");
//            // tTmpEle.setText(tDataInfo[15]);
//            // tTmpEle.addAttribute("CData", "CBusCode");
//            tItemEle.addContent(tTmpEle);
//            tTmpEle = null;

            tTmpEle = new Element("ContractStatus");
            tTmpEle.setText(tDataInfo[26]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("TerminationReason");
            tTmpEle.setText(tDataInfo[27]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SumInsured");
            tTmpEle.setText(tDataInfo[28]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("EffectiveSumInsured");
            tTmpEle.setText(tDataInfo[29]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Premium");
            tTmpEle.setText(tDataInfo[30]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CurrentPremium");
            tTmpEle.setText(tDataInfo[31]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PaymentMethod");
            tTmpEle.setText(tDataInfo[32]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PaymentYears");
            tTmpEle.setText(tDataInfo[33]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            //start ----------------------------
            tTmpEle = new Element("PaymentNo");
            tTmpEle.setText(tDataInfo[34]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            //end ---------------------------------
            tTmpEle = new Element("PolicyHolderPro");
            tTmpEle.setText(tDataInfo[35]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PolicyHolderNum");
            tTmpEle.setText(tDataInfo[36]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("EffectiveInsuredNum");
            tTmpEle.setText(tDataInfo[37]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("FormerpolicyNo");
            tTmpEle.setText(tDataInfo[38]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SpecialRemark");
            tTmpEle.setText(tDataInfo[39]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RegularClearingMark");
            tTmpEle.setText(tDataInfo[40]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

          /*  tTmpEle = new Element("RegularClearing");   //modify by zhangyige 2012-12-28 不需上传
            tTmpEle.setText(tDataInfo[41]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            */

            tTmpEle = new Element("RegularClearingDate");
            tTmpEle.setText(tDataInfo[42]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            /*tTmpEle = new Element("PremiumDueDate");
            tTmpEle.setText(tDataInfo[43]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;*/

            tTmpEle = new Element("RealTimeClaimFlag");
            tTmpEle.setText(tDataInfo[44]);
//            tTmpEle.addAttribute("CData", "CBusCode");////zhangyige 2012-12-28 传固定值 否
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            //start --------------------------
            
            tTmpEle = new Element("PolicyLoan");
            tTmpEle.setText(tDataInfo[45]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("AutoPaidUp");
            tTmpEle.setText(tDataInfo[46]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("CoInsurance");
            tTmpEle.setText(tDataInfo[47]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("LeadCoInsurance");
            tTmpEle.setText(tDataInfo[48]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            //end --------------------------
            //   当前年化保费
            tTmpEle = new Element("YearPremium");   
            tTmpEle.setText(tDataInfo[49]);
            tTmpEle.addAttribute("CData", "CBigDecimal");  
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            
            if (tContType.equals("1"))
            {
                //个人投保人数组
                tTmpEle = reportPolPerInfo(tContNo);
                if (tTmpEle != null)
                {
                    tItemEle.addContent(tTmpEle);
                }
                tTmpEle = null;
            }

            //被保险人数组
            tTmpEle = reportPolInsInfo(tContNo, tContType);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

            // --------------------

        }

        return tRoot;
    }

    private Element reportPolUpCovInfo(String cContNo) //承保‘ 险种’  数组中详细信息 （已经注释）
    {
        Element tRoot = new Element("PolicyUpcoverageDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.PolicyUpcoverageDTO");

        Element tTmpEle = null;
        String tPolNo = "";

//        SSRS tResPolicyInfo = loadPolUpCovInfo(cContNo);
//
//        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
//        {
//            Element tItemEle = new Element("Item");
//            tRoot.addContent(tItemEle);
//
//            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);
//
//            tTmpEle = new Element("CoverageType"); //平台险种类型
//            tTmpEle.setText(tDataInfo[0]);
//            tTmpEle.addAttribute("CData", "CBusCode");
//            tItemEle.addContent(tTmpEle);
//            tTmpEle = null;
//
//            tTmpEle = new Element("CoverageCode"); //平台险种代码
//            tTmpEle.setText(tDataInfo[1]);
//            tTmpEle.addAttribute("CData", "CBusCode");
//            tItemEle.addContent(tTmpEle);
//            tTmpEle = null;
//
//            tTmpEle = new Element("ComCoverageCode"); //公司险种代码
//            tTmpEle.setText(tDataInfo[2]);
//            tItemEle.addContent(tTmpEle);
//            tTmpEle = null;
//
//            tTmpEle = new Element("CoverageName"); //公司险种名称
//            tTmpEle.setText(tDataInfo[3]);
//            tItemEle.addContent(tTmpEle);
//            tTmpEle = null;
//
//            tTmpEle = new Element("CoverageEffectiveDate"); //险种生效日期
//            tTmpEle.setText(tDataInfo[4]);
//            tTmpEle.addAttribute("CData", "CDate");
//            tItemEle.addContent(tTmpEle);
//            tTmpEle = null;
//
//            tTmpEle = new Element("CoverageExpireDate"); //险种满期日期
//            tTmpEle.setText(tDataInfo[5]);
//            tTmpEle.addAttribute("CData", "CDate");
//            tItemEle.addContent(tTmpEle);
//            tTmpEle = null;
//
//            tTmpEle = new Element("MainAttachedFlag"); //主附险性质
//            tTmpEle.setText(tDataInfo[6]);
//            tTmpEle.addAttribute("CData", "CBusCode");
//            tItemEle.addContent(tTmpEle);
//            tTmpEle = null;
//
//            tTmpEle = new Element("CoveragePremium"); //险种期初保费
//            tTmpEle.setText(tDataInfo[7]);
//            tTmpEle.addAttribute("CData", "CBigDecimal");
//            tItemEle.addContent(tTmpEle);
//            tTmpEle = null;
//            
//            //start ----------------------------
//            tTmpEle = new Element("CoverageCurrentPremium"); //险种当前保费
//            tTmpEle.setText(tDataInfo[8]);
//            tTmpEle.addAttribute("CData", "CBigDecimal");
//            tItemEle.addContent(tTmpEle);
//            tTmpEle = null;
//            //end ----------------------------
//            
//            tTmpEle = new Element("CoverageSuminsured"); //险种保额 
//            tTmpEle.setText(tDataInfo[9]);
//            tTmpEle.addAttribute("CData", "CBigDecimal");
//            tItemEle.addContent(tTmpEle);
//            tTmpEle = null;
//
//            tTmpEle = new Element("CoverageEffectiveSuminsured"); //险种有效保额
//            tTmpEle.setText(tDataInfo[10]);
//            tTmpEle.addAttribute("CData", "CBigDecimal");
//            tItemEle.addContent(tTmpEle);
//            tTmpEle = null;
//
//            tTmpEle = new Element("PaymentMethod"); //缴费方式
//            tTmpEle.setText(tDataInfo[11]);
//            tTmpEle.addAttribute("CData", "CBusCode");
//            tItemEle.addContent(tTmpEle);
//            tTmpEle = null;
//
//            tTmpEle = new Element("PaymentYears"); //缴费年限
//            tTmpEle.setText(tDataInfo[12]);
//            tTmpEle.addAttribute("CData", "CBigDecimal");
//            tItemEle.addContent(tTmpEle);
//            tTmpEle = null;
//            
//            //start ---------------------------
//            tTmpEle = new Element("PaymentNo"); //缴费次数
//            tTmpEle.setText(tDataInfo[13]);
//            tTmpEle.addAttribute("CData", "CBigDecimal");
//            tItemEle.addContent(tTmpEle);
//            tTmpEle = null;
//            //end ---------------------------
//                        
//            tTmpEle = new Element("SpecificBusiness"); //特殊业务标识
//            tTmpEle.setText(tDataInfo[14]);
//            tTmpEle.addAttribute("CData", "CBusCode");
//            tItemEle.addContent(tTmpEle);
//            tTmpEle = null;
//
//            tTmpEle = new Element("SpecificBusinessCode"); //特殊业务代码
//            tTmpEle.setText(tDataInfo[15]);
//            tTmpEle.addAttribute("CData", "CBusCode");
//            tItemEle.addContent(tTmpEle);
//            tTmpEle = null;
//
//            //start ----------------------
//            tTmpEle = new Element("CoverageStatus"); //险种状态
//            tTmpEle.setText(tDataInfo[16]);
//            tItemEle.addContent(tTmpEle);
//            tTmpEle = null;
//            //end ----------------------
//            
//            tPolNo = tDataInfo[17];
//            
//
//            //获取承保责任数组
//            tTmpEle = reportPolCovInfo(tPolNo);
//            if (tTmpEle != null)
//            {
//                tItemEle.addContent(tTmpEle);
//            }
//            tTmpEle = null;
//            //获取核保结论数组
//            tTmpEle = reportPolUWInfo(tPolNo);
//            if (tTmpEle != null)
//            {
//                tItemEle.addContent(tTmpEle);
//            }
//            tTmpEle = null;
//        }

        return tRoot;
    }

    private Element reportPolCovInfo(String cPolNo)  //承保 ‘ 责任 ’ 数组中的详细信息
    {
        Element tRoot = new Element("PolicyCoverageDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.PolicyCoverageDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolCovInfo(cPolNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("LiabilityClassification"); //平台责任分类    
            tTmpEle.setText(tDataInfo[0]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityCode"); //责任代码        
            tTmpEle.setText(tDataInfo[1]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityName"); //责任名称        
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityEffectiveDate"); //责任开始日期    
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityExpireDate"); //责任终止日期    
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityStatus"); //责任状态     
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityPremium"); //起初责任保费        
            tTmpEle.setText(tDataInfo[6]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            //start -----------------------
            tTmpEle = new Element("LiabilityCurrentPremium"); //当前责任保费        
            tTmpEle.setText(tDataInfo[7]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            //end -----------------------
            
            tTmpEle = new Element("LimitAmount"); //责任保额        
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("EffectivelyAmount"); //责任有效保额    
            tTmpEle.setText(tDataInfo[9]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("WaitingPeriod"); //免责期          
            tTmpEle.setText(tDataInfo[10]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("InsuredGroupCode"); //被保险人属组代码
            tTmpEle.setText(tDataInfo[11]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("GroupName"); //属组名称     
            tTmpEle.setText(tDataInfo[12]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityDayAmount"); //责任期初日额保险金    
            tTmpEle.setText(tDataInfo[13]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("LiabilitydayEffectiveAmount"); //责任有效日额保险金 
            tTmpEle.setText(tDataInfo[14]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            
            //获取赔付比例分段数组
            tTmpEle = reportPolAmoInfo(cPolNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

        }

        return tRoot;
    }

    private Element reportPolAmoInfo(String cPolNo)
    {
        Element tRoot = new Element("PolicyAmountDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.PolicyAmountDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolAmoInfo();

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("SectionNo"); //段序
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Coststart"); //费用起线
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Costend"); //费用止线
            tTmpEle.setText(tDataInfo[2]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Deductible"); //免赔额
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ClaimRatio"); //赔付比例
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
        }

        return tRoot;
    }

    private Element reportPolUWInfo(String cPolNo)
    {
        Element tRoot = new Element("UnderWritingDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.UnderWritingDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolUWInfo(cPolNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("UnderwritingResult"); //核保结论
            tTmpEle.setText(tDataInfo[0]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("UnderwritingDate"); //核保结论日期
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            //start ---------------------
            
            tTmpEle = new Element("DeclinedReason"); //拒保原因
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("State"); //业务流程状态
            tTmpEle.setText(tDataInfo[3]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("PhysicalExamination"); //是否体检
            tTmpEle.setText(tDataInfo[4]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            //end ---------------------
        }

        return tRoot;
    }

    private Element reportPolPerInfo(String cContNo)
    {
        Element tRoot = new Element("PolicyPersonphDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.PolicyPersonphDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolPerInfo(cContNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("Name"); //姓名  
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Gender"); //性别       
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Birthday"); //出生日期      
            tTmpEle.setText(tDataInfo[2]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritType"); //证件类别      
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritCode"); //证件号码      
            tTmpEle.setText(tDataInfo[4]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RelationshipWithInsured"); //与被保险人关系
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
        }

        return tRoot;
    }

    private Element reportPolInsInfo(String cContNo, String cContType)
    {
        Element tRoot = new Element("PolicyInsuredDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.PolicyInsuredDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolInsInfo(cContNo, cContType);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);
            
            tTmpEle = new Element("Name"); //姓名    
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Gender"); //性别       
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Birthday"); //出生日期        
            tTmpEle.setText(tDataInfo[2]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritType"); //被保险人证件类别    
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritCode"); //被保险人证件号码   
            tTmpEle.setText(tDataInfo[4]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RelationshipWithPh"); //与投保人关系        
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CustomerNo"); //客户编号          
            tTmpEle.setText(tDataInfo[6]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            //start --------------------
            tTmpEle = new Element("Address1"); //联系地址（省级）
            tTmpEle.setText(tDataInfo[7]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("Address2"); //联系地址（地级）
            tTmpEle.setText(tDataInfo[8]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("Address3"); //联系地址区（县级）
            tTmpEle.setText(getTransAddress(tDataInfo[9]));
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("Address4"); //具体联系地址
            if(tDataInfo[10].equals("")){
            	tTmpEle.setText("北京市西城区阜成门外大街7号国投大厦10层");
            }else{
            	tTmpEle.setText(tDataInfo[10]);
            }
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            //end --------------------
            
            tTmpEle = new Element("AnomalyInform"); //异常告知          
            tTmpEle.setText(tDataInfo[11]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("OfferStatus"); //要约状态            
            tTmpEle.setText(tDataInfo[12]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SubStartDate"); //被保险人责任开始日期
            tTmpEle.setText(tDataInfo[13]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SubEndDate"); //被保险人责任终止日期
            tTmpEle.setText(tDataInfo[14]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("InsuredGroupCode"); //被保险人属组代码  
            tTmpEle.setText(tDataInfo[15]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("InsuredType"); //被保险人类型
            if(tDataInfo[16].equals("")){
            	tTmpEle.setText("09"); //关系为其他
            }else{
            	tTmpEle.setText(tDataInfo[16]);
                tTmpEle.addAttribute("CData", "CBusCode");
            }
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SnOfMainInsured"); //主被保人序号  
            tTmpEle.setText(tDataInfo[17]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RelationshipWithMainInsured"); //与主被保人关系 
            tTmpEle.setText(tDataInfo[18]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ServiceMark"); //在职标志    
            tTmpEle.setText(tDataInfo[19]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("HealthFlag"); //医保标识          
            tTmpEle.setText(tDataInfo[20]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SocialcareNo"); //社保卡号  
            tTmpEle.setText(tDataInfo[21]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("OccupationalCode"); //职业代码    
            //zyg 职业代码采用固定值方式，采用平台代码“0001001”上传，内勤人员
            tTmpEle.setText("0001001");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("WorkPlace"); //工作地点     
            tTmpEle.setText(tDataInfo[23]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("EmpoyeeNo"); //工号        
            tTmpEle.setText(tDataInfo[24]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            //start -----------------------
            tTmpEle = new Element("AppointedBenefit"); //是否指定受益人 
            tTmpEle.setText(tDataInfo[25]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            //end -----------------------

            String tInsuredNo = tDataInfo[6];
            //特别承保险种数组
            tTmpEle = reportPolInsUpCovInfo(cContNo, tInsuredNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
            if(!tDataInfo[25].equals("0")){
	            //受益人数组
	            tTmpEle = reportPolInsBnfInfo(cContNo);
	            if (tTmpEle != null)
	            {
	                tItemEle.addContent(tTmpEle);
	            }
	            tTmpEle = null;
            }
        }

        return tRoot;
    }

    private Element reportPolInsUpCovInfo(String cContNo, String cInsuredNo) //特别承保 ‘险种 ’数组中详细信息
    {
        Element tRoot = new Element("SpecialPolicyUpcoverageDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.SpecialPolicyUpcoverageDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolInsUpCovInfo(cContNo, cInsuredNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("CoverageType"); //平台险种类型
            tTmpEle.setText(tDataInfo[0]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageCode"); //平台险种代码
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ComCoverageCode"); //公司险种代码
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageName"); //公司险种名称
            tTmpEle.setText(tDataInfo[3]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageEffectiveDate"); //险种起保日期
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageExpireDate"); //险种满期日期
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("MainAttachedFlag"); //主附险性质
            tTmpEle.setText(tDataInfo[6]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoveragePremium"); //险种保费    
            tTmpEle.setText(tDataInfo[7]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            //start ----------------------------
            tTmpEle = new Element("CoverageCurrentPremium"); //险种当前保费
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            //end ----------------------------
            
            tTmpEle = new Element("CoverageSuminsured"); //险种保额    
            tTmpEle.setText(tDataInfo[9]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageEffectiveSuminsured"); //险种有效保额
            tTmpEle.setText(tDataInfo[10]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PaymentMethod"); //缴费方式
            tTmpEle.setText(tDataInfo[11]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PaymentYears"); //缴费年限
            tTmpEle.setText(tDataInfo[12]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            //start ---------------------------
            tTmpEle = new Element("PaymentNo"); //缴费次数
            tTmpEle.setText(tDataInfo[13]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            //end ---------------------------
            
            tTmpEle = new Element("SpecificBusiness"); //特殊业务标识
            tTmpEle.setText(tDataInfo[14]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SpecificBusinessCode"); //特殊业务代码
            tTmpEle.setText(tDataInfo[15]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            //start ----------------------
            tTmpEle = new Element("CoverageStatus"); //险种状态
            tTmpEle.setText(tDataInfo[16]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            //end ----------------------
            
            String tPolNo = tDataInfo[17];
            
            //险种期初日额保险金
                     
            tTmpEle = new Element("CoveragedayAmount"); 
            tTmpEle.setText(tDataInfo[18]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            //险种有效日额保险金
            
            tTmpEle = new Element("CoveragedayEffectiveAmount"); 
            tTmpEle.setText(tDataInfo[19]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            //险种当前年化保费
            
            tTmpEle = new Element("CoverageyearPremium"); 
            tTmpEle.setText(tDataInfo[20]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            //保费来源
            tTmpEle = new Element("PremiumSource");
            tTmpEle.setText(tDataInfo[21]);
            //tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            
            //特别承保责任数组
            tTmpEle = reportPolInsCovInfo(tPolNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

            // 特别核保结论数组
            tTmpEle = reportPolSpeUWInfo(tPolNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

        }

        return tRoot;
    }

    private Element reportPolInsCovInfo(String cPolNo)  //特别承保‘责任’数组中的详细信息
    {
        Element tRoot = new Element("SpecialPolicyCoverageDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.SpecialPolicyCoverageDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolInsCovInfo(cPolNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("LiabilityClassification"); //平台责任分类
            tTmpEle.setText(tDataInfo[0]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityCode"); //责任代码    
            tTmpEle.setText(tDataInfo[1]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityName"); //责任名称    
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityEffectiveDate"); //责任开始日期
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityExpireDate"); //责任终止日期
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityStatus"); //责任状态    
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("LiabilityPremium"); //责任保费    
            tTmpEle.setText(tDataInfo[6]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityCurrentPremium"); //当前责任保费    
            tTmpEle.setText(tDataInfo[7]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LimitAmount"); //责任保额    
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("EffectivelyAmount"); //责任有效保额
            tTmpEle.setText(tDataInfo[9]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("WaitingPeriod"); //免责期      
            tTmpEle.setText(tDataInfo[10]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("LiabilityDayAmount"); //责任期初日额保险金    
            tTmpEle.setText(tDataInfo[11]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("LiabilitydayEffectiveAmount"); //责任有效日额保险金 
            tTmpEle.setText(tDataInfo[12]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            
            //特别赔付比例分段数组
            tTmpEle = reportPolSpeAmInfo(cPolNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

        }

        return tRoot;
    }

    private Element reportPolSpeUWInfo(String cContNo)
    {
        Element tRoot = new Element("SpecialUnderWritingDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.SpecialUnderWritingDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolSpeUWInfo(cContNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("UnderwritingResult"); //核保结论
            tTmpEle.setText(tDataInfo[0]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("UnderwritingDate"); //核保结论日期
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("DeclinedReason"); //拒保原因
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("State"); //业务流程状态
            tTmpEle.setText(tDataInfo[3]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("PhysicalExamination"); //是否体检
            tTmpEle.setText(tDataInfo[4]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
        }

        return tRoot;
    }

    private Element reportPolSpeAmInfo(String cContNo)
    {
        Element tRoot = new Element("SpecialPolicyAmountDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.SpecialPolicyAmountDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolSpeAmInfo(cContNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("SectionNo"); //段序    
            tTmpEle.setText(String.valueOf(idxPI));
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Coststart"); //费用起线
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Costend"); //费用止线
            tTmpEle.setText(tDataInfo[2]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Deductible"); //免赔额  
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ClaimRatio"); //赔付比例
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
        }

        return tRoot;
    }

    private Element reportPolInsBnfInfo(String cContNo)
    {
        Element tRoot = new Element("PolicyBenefitDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.PolicyBenefitDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolInsBnfInfo(cContNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("Name"); //名称   
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Gender"); //性别      
            if(tDataInfo[1].equals("")){
            	tTmpEle.setText("0");
            }else{
            	tTmpEle.setText(tDataInfo[1]);
            }
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Birthday"); //出生日期      
            if(tDataInfo[2].equals("")){
            	tTmpEle.setText("1987-01-01");
            }else{
            	tTmpEle.setText(tDataInfo[2]);
            }
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritType"); //证件类别      
            if(tDataInfo[3].equals("")){
            	tTmpEle.setText("4");
            }else{
            	tTmpEle.setText(tDataInfo[3]);
            }
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritCode"); //证件号码     
            if(tDataInfo[4].equals("")){
            	tTmpEle.setText("111111");
            }else{
            	tTmpEle.setText(tDataInfo[4]);
            }
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RelationshipWithInsured"); //与被保险人关系
            if(tDataInfo[5].equals("")){
            	tTmpEle.setText("99");  //zyg 置为默认 其他
            }else{
            	tTmpEle.setText(tDataInfo[5]);
            }
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("BenefitDistribution"); //受益分配方式  
            tTmpEle.setText(tDataInfo[6]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("OrderNo"); //顺序号        
            tTmpEle.setText(tDataInfo[7]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Proportion"); //比例值
            if(tDataInfo[8].length()>4){
            	tTmpEle.setText(tDataInfo[8].substring(0, 4));
            }else{
            	tTmpEle.setText(tDataInfo[8]);
            }
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
        }

        return tRoot;
    }

    private SSRS loadPolInsBnfInfo(String cContNo)
    {
        SSRS tResult = null;

        StringBuffer tStrBSql = new StringBuffer();
        
        tStrBSql.append(" select name Name, sex Gender,birthday Birthday,idtype CritType,idno CritCode, ");
        // '01' BenefitDistribution zyg 置为默认 未指定
        tStrBSql.append(" relationtoinsured RelationshipWithInsured, '01' BenefitDistribution, bnfgrade OrderNo,bnflot Proportion ");
        tStrBSql.append(" from lcbnf ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and contno = '"+cContNo+"'");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadPolSpeAmInfo(String cPolNo)
    {
        SSRS tResult = null;

        StringBuffer tStrBSql = new StringBuffer();

        tStrBSql.append(" select ");
        tStrBSql.append(" RowNumber() over() as SectionNo, ");
        // zyg 费用起线 费用止线 置为 0 
        tStrBSql
                .append(" '0' Coststart, '0' Costend, cast(lcd.GetLimit as decimal(12,2)) Deductible, cast(lcd.GetRate as decimal(12,2)) ClaimRatio ");
        tStrBSql.append(" from LCDuty lcd ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcd.PolNo = '" + cPolNo + "' ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadPolSpeUWInfo(String tPolNo)
    {
        SSRS tResult = null;

        StringBuffer tStrBSql = new StringBuffer();

        //        tStrBSql.append(" select ");
        //        tStrBSql.append(" '' UnderwritingResult, '' UnderwritingDate ");
        //        tStrBSql.append(" from dual ");
        //        tStrBSql.append(" where 1 = 1 ");

        tStrBSql.append(" select ");
        tStrBSql.append(" lcp.UwFlag UnderwritingResult, lcp.UwDate UnderwritingDate, ");
        tStrBSql.append(" (select uwidea from lcuwmaster where polno ='"+tPolNo+"') DeclinedReason, '1' State, ");
        tStrBSql.append(" case when exists (select 1 from LCPENotice where contno = lcp.contno) then '1' else '0' end PhysicalExamination ");
        tStrBSql.append(" from LCPol lcp ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.PolNo = '" + tPolNo + "' ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadPolInsCovInfo(String cPolNo)  //特别承保‘责任’数组中的详细信息
    {
        SSRS tResult = null;

        StringBuffer tStrBSql = new StringBuffer();

        //        tStrBSql.append(" select ");
        //        tStrBSql
        //                .append(" '' LiabilityClassification, '' LiabilityCode, '' LiabilityName, '' LiabilityEffectiveDate, '' LiabilityExpireDate, '' LiabilityStatus, '' LiabilityPremium, '' LimitAmount, '' EffectivelyAmount, '' WaitingPeriod ");
        //        tStrBSql.append(" from dual ");
        //        tStrBSql.append(" where 1 = 1 ");

        tStrBSql.append(" select ");
        tStrBSql.append(" lcd.DutyCode LiabilityClassification, ");
        tStrBSql.append(" lcd.DutyCode LiabilityCode, lmd.DutyName LiabilityName, ");
        tStrBSql.append(" lcd.GetStartDate LiabilityEffectiveDate, (lcd.EndDate - 1 day) LiabilityExpireDate, ");
        tStrBSql.append(" lcp.StateFlag LiabilityStatus, lcd.Prem LiabilityPremium, ");
        tStrBSql.append(" lcd.Prem LiabilityCurrentPremium, "); //当前责任保费
        tStrBSql.append(" lcd.Amnt LimitAmount, lcd.Amnt EffectivelyAmount, ");
        tStrBSql.append(" '0' WaitingPeriod, ");
        // tStrBSql.append(" trim(lcp.ContNo) || '_' || trim(lcp.InsuredNo) InsuredGroupCode, ");
        // tStrBSql.append(" trim(lcp.ContNo) || '_' || trim(lcp.InsuredNo) GroupName ");
        
       tStrBSql.append("(select case when lc.mult='1' then 50 when lc.mult='2' then 100 when lc.mult='3' then 150 else 0 end from lcpol lc where lc.riskcode='1201' and lc.polno= '" + cPolNo + "') LiabilityDayAmount,");//责任期初日额保险金
       tStrBSql.append("(select case when lc.mult='1' then 50 when lc.mult='2' then 100 when lc.mult='3' then 150 else 0 end from lcpol lc where lc.riskcode='1201' and lc.polno= '" + cPolNo + "') LiabilitydayEffectiveAmount,");//责任有效日额保险金
      
        
        tStrBSql.append(" '' InsuredGroupCode, ");
        tStrBSql.append(" '' GroupName ");
        tStrBSql.append(" from LCPol lcp ");
        tStrBSql.append(" inner join LCDuty lcd on lcd.PolNo = lcp.PolNo ");
        tStrBSql.append(" inner join LMDuty lmd on lmd.DutyCode = lcd.DutyCode ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.polno = '" + cPolNo + "' ");
        
        
        
        
        
        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadPolInsUpCovInfo(String cContNo, String cInsuredNo)  //特别承保 ‘险种’ 数组中详细信息
    {
        SSRS tResult = null;

        StringBuffer tStrBSql = new StringBuffer();

        //        tStrBSql.append(" select ");
        //        tStrBSql
        //                .append(" '' CoverageType, '' CoverageCode, '' ComCoverageCode, '' CoverageName, '' CoverageEffectiveDate, '' CoverageExpireDate, '' CoveragePremium, '' CoverageSuminsured, '' CoverageEffectiveSuminsured, '' SpecificBusiness, '' SpecificBusinessCode ");
        //        tStrBSql.append(" from dual ");
        //        tStrBSql.append(" where 1 = 1 ");

        tStrBSql.append(" select ");
        tStrBSql.append(" (select riskperiod from lmriskapp where riskcode = lcp.RiskCode) CoverageType, lcp.RiskCode CoverageCode, ");
        tStrBSql.append(" lcp.RiskCode ComCoverageCode, lmra.RiskName CoverageName, ");
        tStrBSql.append(" lcp.GetStartDate CoverageEffectiveDate, lcp.EndDate CoverageExpireDate, ");
        tStrBSql.append(" lmra.SubRiskFlag MainAttachedFlag, ");
        tStrBSql.append(" lcp.Prem CoveragePremium, ");
        tStrBSql.append(" lcp.Prem CoverageCurrentPremium, ");//险种当前保费
        tStrBSql.append(" lcp.Amnt CoverageSuminsured, lcp.Amnt CoverageEffectiveSuminsured, ");
        tStrBSql
                .append(" lcp.PayIntv PaymentMethod, (Case when lcp.PayIntv is null then '' else char(lcp.PayYears) end) PaymentYears, ");
        tStrBSql.append(" '1' PaymentNo, ");//缴费次数
        tStrBSql.append(" '0' SpecificBusiness, '' SpecificBusinessCode, ");
        tStrBSql.append(" '1' CoverageStatus, ");//险种状态
        tStrBSql.append(" lcp.PolNo ,");
        
        tStrBSql.append("(select case when lc.mult='1' then 50 when lc.mult='2' then 100 when lc.mult='3' then 150 else 0 end from lcpol lc where lc.riskcode='1201' and lc.ContNo= '" + cContNo + "') CoveragedayAmount,"); //险种期初日额保险金
        tStrBSql.append("(select case when lc.mult='1' then 50 when lc.mult='2' then 100 when lc.mult='3' then 150 else 0 end from lcpol lc where lc.riskcode='1201' and lc.ContNo= '" + cContNo + "') CoveragedayEffectiveAmount,");//险种有效日额保险金
        tStrBSql.append("( case when lcp.payintv='1'  then prem*12 when lcp.payintv='3'  then prem*4  when lcp.payintv='6'  then prem*2 when lcp.payintv='12' then prem   else 0 end ) CoverageyearPremium,");//险种当前年化保费
        tStrBSql.append("'01' PremiumSource");//保费来源
        
//  测试      tStrBSql.append("'444444' coveragedayAmount,"); //险种期初日额保险金
//        tStrBSql.append("'555555' coveragedayEffectiveAmount,");//险种有效日额保险金
//        tStrBSql.append("'666666' coverageyearPremium,");//险种当前年化保费
//       tStrBSql.append("'1' premiumSource");//保费来源
        
        
        tStrBSql.append(" from LCPol lcp ");
        tStrBSql.append(" inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.ContNo = '" + cContNo + "' and lcp.InsuredNo = '" + cInsuredNo + "' ");
        
        String tStrSql = tStrBSql.toString();
        System.out.println("#"+tStrSql);
        tResult = new ExeSQL().execSQL(tStrSql);
        if(tResult==null){
        	System.out.println("===liuijian承保险种数组为空（个单）===");
        	
        }else{
        	System.out.println("===liuijian承保险种数组不是空（个单）===");
        }
        
        return tResult;
    }

    private SSRS loadPolInsInfo(String cContNo, String cContType)
    {
    	System.out.println("cContType:"+cContType);
        SSRS tResult = null;

        String tContNo = cContNo;

        StringBuffer tStrBSql = new StringBuffer();

        if (cContType.equals("1"))
        {
            //            tStrBSql.append(" select * from ( ");

            tStrBSql.append(" select ");
            tStrBSql.append(" lci.Name Name, lci.Sex Gender, lci.Birthday Birthday, ");
            tStrBSql.append(" lci.IDType CritType, lci.IDNo CritCode, lci.RelationToAppnt RelationshipWithPh, ");
            tStrBSql.append(" lci.InsuredNo CustomerNo, ");
            //zyg 默认置为 北京（省）市辖（区） 西城（县）
            tStrBSql.append(" '110000' Address1, ");
            tStrBSql.append(" '110100' Address2, ");
            tStrBSql.append(" (select PostalAddress from LCAddress where customerno = lci.insuredno and addressno = lci.addressno ) Address3, ");
            tStrBSql.append(" (select PostalAddress from LCAddress where customerno = lci.insuredno and addressno = lci.addressno ) Address4, ");
            tStrBSql.append(" '0' AnomalyInform, (select stateflag from lccont where contno= '"+tContNo+"') OfferStatus, ");
            tStrBSql.append(" '' SubStartDate, '' SubEndDate, ");
            //            tStrBSql.append(" trim('" + tContNo + "') || '_' || trim(lci.InsuredNo) InsuredGroupCode, ");
            tStrBSql.append(" '' InsuredGroupCode, ");
            tStrBSql.append(" lci.Relationtomaininsured InsuredType, ");
            tStrBSql
                    .append(" (case when lci.Relationtomaininsured = '00' then '' else (select slci.InsuredNo from LCInsured slci where slci.ContNo = lci.ContNo and slci.Relationtomaininsured = '00') end) SnOfMainInsured, ");
            tStrBSql.append(" lci.Relationtomaininsured RelationshipWithMainInsured, ");
            tStrBSql.append(" '' ServiceMark, '' HealthFlag, '' SocialcareNo, ");
            tStrBSql.append(" lci.OccupationCode OccupationalCode, '' WorkPlace, ");
            tStrBSql.append(" '' EmpoyeeNo, ");
            tStrBSql.append(" case when exists (select 1 from LCBnf where contno = lci.contno and insuredno = lci.insuredno ) then '1' else '0' end AppointedBenefit ");
            tStrBSql.append(" from lcinsured lci ");
            tStrBSql.append(" where 1 = 1 ");
            tStrBSql.append(" and lci.ContNo = '" + tContNo + "' ");

            //            tStrBSql.append(" ) as tmp fetch first row only");
        }


        String tStrSql = tStrBSql.toString();
        System.out.println("======"+tStrSql);
        tResult = new ExeSQL().execSQL(tStrSql);
        if(tResult==null){
        	System.out.println("===liuijian被保人数组为空（个单）===");
        	
        }else{
        	System.out.println("===liuijian被保人数组不是空（个单）===");
        }
        return tResult;
    }

    private SSRS loadPolPerInfo(String cContNo)
    {
        SSRS tResult = null;

        String tContNo = cContNo;

        StringBuffer tStrBSql = new StringBuffer();

        tStrBSql.append(" select ");
        tStrBSql.append(" lcap.AppntName Name, lcap.AppntSex Gender, ");
        tStrBSql.append(" lcap.AppntBirthday Birthday, lcap.IDType CritType, ");
        tStrBSql.append(" lcap.IDNo CritCode, '' RelationshipWithInsured ");
        tStrBSql.append(" from lcappnt lcap ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcap.ContNo = '" + tContNo + "' ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadPolAmoInfo()
    {
        SSRS tResult = null;

        StringBuffer tStrBSql = new StringBuffer();

        tStrBSql.append(" select ");
        tStrBSql.append(" '' SectionNo, '0' Coststart, '0' Costend, '' Deductible, '' ClaimRatio ");
        tStrBSql.append(" from dual ");
        tStrBSql.append(" where 1 = 1 ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadPolUWInfo(String cPolNo)
    {
        SSRS tResult = null;

        String tPolNo = cPolNo;

        StringBuffer tStrBSql = new StringBuffer();

        tStrBSql.append(" select ");
        tStrBSql.append(" lcp.UwFlag UnderwritingResult, lcp.UwDate UnderwritingDate, ");
        tStrBSql.append(" (select uwidea from lcuwmaster where polno ='"+tPolNo+"') DeclinedReason, '1' State, ");
        tStrBSql.append(" case when exists (select 1 from LCPENotice where contno = lcp.contno) then '1' else '0' end PhysicalExamination ");
        tStrBSql.append(" from LCPol lcp ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.PolNo = '" + tPolNo + "' ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadPolCovInfo(String cPolNo)  //承保 ‘责任 ’数组中的详细信息
    {
        SSRS tResult = null;

        String tPolNo = cPolNo;

        StringBuffer tStrBSql = new StringBuffer();

        tStrBSql.append(" select * from ( ");

        tStrBSql.append(" select ");
        tStrBSql.append(" lcd.DutyCode LiabilityClassification, ");
        tStrBSql.append(" lcd.DutyCode LiabilityCode, lmd.DutyName LiabilityName, ");
        tStrBSql.append(" lcd.GetStartDate LiabilityEffectiveDate, (lcd.EndDate - 1 day) LiabilityExpireDate, ");
        tStrBSql.append(" lcp.StateFlag LiabilityStatus, lcd.Prem LiabilityPremium, ");
        tStrBSql.append(" lcd.Prem LiabilityCurrentPremium, "); //当前责任保费
        tStrBSql.append(" lcd.Amnt LimitAmount, lcd.Amnt EffectivelyAmount, ");
        tStrBSql.append(" '0' WaitingPeriod, ");
        tStrBSql.append(" trim(lcp.ContNo) || '_' || trim(lcp.InsuredNo) InsuredGroupCode, ");
        //        tStrBSql.append(" '' InsuredGroupCode, ");
        tStrBSql.append(" trim(lcp.ContNo) || '_' || trim(lcp.InsuredNo) GroupName ,");
        //        tStrBSql.append(" '' GroupName ");
        
        tStrBSql.append("(select case when lc.mult='1' then 50 when lc.mult='2' then 100 when lc.mult='3' then 150 else 0 end from lcpol lc where lc.riskcode='1201' and lc.polno= '" + cPolNo + "') LiabilityDayAmount,");//责任期初日额保险金
        tStrBSql.append("(select case when lc.mult='1' then 50 when lc.mult='2' then 100 when lc.mult='3' then 150 else 0 end from lcpol lc where lc.riskcode='1201' and lc.polno= '" + cPolNo + "') LiabilitydayEffectiveAmount");//责任有效日额保险金
//测试
        //        tStrBSql.append("'222222' liabilityDayAmount,");//责任期初日额保险金
//        tStrBSql.append("'333333' liabilitydayEffectiveAmount");//责任有效日额保险金
//        
        
        tStrBSql.append(" from LCPol lcp ");
        tStrBSql.append(" inner join LCDuty lcd on lcd.PolNo = lcp.PolNo ");
        tStrBSql.append(" inner join LMDuty lmd on lmd.DutyCode = lcd.DutyCode ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.polno = '" + tPolNo + "' ");

        tStrBSql.append(" ) as tmp fetch first row only");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadPolUpCovInfo(String cContNo)  //承保 ‘险种 ’数组中详细信息  （已经注释）
    {
        SSRS tResult = null;

        String tContNo = cContNo;

        StringBuffer tStrBSql = new StringBuffer();

        //        tStrBSql.append(" select * from ( ");

        tStrBSql.append(" select ");
        tStrBSql.append(" (select riskperiod from lmriskapp where riskcode = lcp.RiskCode) CoverageType, lcp.RiskCode CoverageCode, ");
        tStrBSql.append(" lcp.RiskCode ComCoverageCode, lmra.RiskName CoverageName, ");
        tStrBSql.append(" lcp.GetStartDate CoverageEffectiveDate, lcp.EndDate CoverageExpireDate, ");
        tStrBSql.append(" lmra.SubRiskFlag MainAttachedFlag, ");
        tStrBSql.append(" lcp.Prem CoveragePremium, ");
        tStrBSql.append(" lcp.Prem CoverageCurrentPremium, ");//险种当前保费
        tStrBSql.append(" lcp.Amnt CoverageSuminsured, lcp.Amnt CoverageEffectiveSuminsured, ");
        tStrBSql
                .append(" lcp.PayIntv PaymentMethod, (Case when lcp.PayIntv is null then '' else char(lcp.PayYears) end) PaymentYears, ");
        tStrBSql.append(" '1' PaymentNo, ");//缴费次数
        tStrBSql.append(" '0' SpecificBusiness, '' SpecificBusinessCode, ");
        tStrBSql.append(" '1' CoverageStatus, ");//险种状态
        tStrBSql.append(" lcp.PolNo ");
        tStrBSql.append(" from LCPol lcp ");
        tStrBSql.append(" inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.ContNo = '" + tContNo + "' ");
        

        //        tStrBSql.append(" ) as tmp fetch first row only");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadPolicyInfo(String cDataInfoKey)  //保单详细信息
    {
        SSRS tResult = null;

        String tContNo = cDataInfoKey;

        StringBuffer tStrBSql = new StringBuffer();
        tStrBSql.append(" select ");
        tStrBSql.append(" '0' SplitPolicyNo, ");//拆单标志
        tStrBSql.append(" lcc.UWFlag Rejection, lcc.PrtNo ApplicationFormNo, lcc.ContNo PolicyNo, ");
        tStrBSql.append(" (case when (select distinct 1 from lcrnewstatelog where contno=lcc.contno and state='6') is not null ");
        tStrBSql.append(" then (select distinct (nvl((select distinct enteraccdate from ljtempfee where tempfeeno = ljap.getnoticeno),ljap.confdate)) ");//续期保单为收费当天
        tStrBSql.append("  from ljapayperson ljap where ljap.curpaytodate=lcc.paytodate and contno=lcc.contno fetch first 1 rows only)");
        tStrBSql.append(" else lcc.PolApplyDate end ) ApplicationDate, ");//modify by lzy #2956续保保单的申请日期与承保日期改为续保收费日期
        tStrBSql.append(" (case when (select distinct 1 from lcrnewstatelog where contno=lcc.contno and state='6') is not null ");
        tStrBSql.append(" then (select distinct (nvl((select distinct enteraccdate from ljtempfee where tempfeeno = ljap.getnoticeno),ljap.confdate)) ");//续期保单为收费当天
        tStrBSql.append("  from ljapayperson ljap where ljap.curpaytodate=lcc.paytodate and contno=lcc.contno fetch first 1 rows only)");
        tStrBSql.append(" else lcc.SignDate end) AcceptDate, ");//保单出单日期(承保日期)
        tStrBSql.append(" '000085110000' OrganId, ");//保单所属机构代码 --需确认中介机构代码
        tStrBSql.append(" '中国人民健康保险股份有限公司北京分公司' OrganName, ");//保单所属机构名称
        tStrBSql.append(" lcc.Salechnl ChannelType, ");//销售渠道类型
        tStrBSql.append(" lcc.Salechnl SalesType, ");//销售人员类型
        tStrBSql.append(" (select qualifno 资格证号 from LAQUALIFICATION where agentcode = lcc.AgentCode) SalesCode, ");//销售人员代码
        tStrBSql.append(" (select name from laagent where agentcode = lcc.AgentCode) SalesName, ");//销售人员姓名
        tStrBSql.append(" lcc.AgentCom SalesChannelCode, ");//中介销售机构代码
        tStrBSql.append(" (select name from lacom where agentcom = lcc.AgentCom) SalesChannelName, ");//中介销售机构名称
        tStrBSql.append(" (select address from lacom where agentcom = lcc.AgentCom) BusinessAddress, ");//中介营业场所地址
        tStrBSql.append(" lcc.ContType ContractType, ");
//        tStrBSql.append(" '' GroupMark, "); //取消
        tStrBSql.append(" lcc.CValidate PolicyStartDate, ");
        tStrBSql.append(" lcc.CInvalidate PolicyEndDate, ");
        tStrBSql.append(" '' TerminationDate, '' SuspendDate, '' RecoverDate, ");
        tStrBSql.append(" '01' ContractSource, ");
        tStrBSql.append(" lcc.ContNo ContractNo, ");
        tStrBSql.append(" (select count(distinct NewContNo) from LCRNewStateLog where ContNo = lcc.ContNo) RenewalTimes, '' RenewalMethod, 'M' MainAttachedFlag, ");
        tStrBSql.append(" lcc.StateFlag ContractStatus, '' TerminationReason, ");
        tStrBSql.append(" lcc.Amnt SumInsured, lcc.Amnt EffectiveSumInsured, ");
        tStrBSql.append(" lcc.Prem Premium, lcc.Prem CurrentPremium, ");
        tStrBSql.append(" lcc.PayIntv PaymentMethod, ");
        tStrBSql.append(" (select payyears from lcpol where contno = '"+tContNo+"' and polno = mainpolno order by polno fetch first 1 rows only ) PaymentYears, ");//缴费年限
        tStrBSql.append(" '1' PaymentNo, ");//缴费次数
        tStrBSql.append(" '2' PolicyHolderPro, ");
        tStrBSql.append(" (select count(1) from lcinsured where contno = '" + tContNo + "') PolicyHolderNum, ");
        tStrBSql.append(" (select count(1) from lcinsured where contno = '" + tContNo + "') EffectiveInsuredNum, ");
        tStrBSql.append(" '' FormerpolicyNo, '' SpecialRemark, ");
        tStrBSql.append(" '' RegularClearingMark, ");
        tStrBSql.append(" '待定' RegularClearing, ");
        tStrBSql.append(" '' RegularClearingDate, ");
        tStrBSql.append(" lcc.SignDate PremiumDueDate, ");
        tStrBSql.append(" '0' RealTimeClaimFlag, ");//zhangyige 2012-12-28 传固定值 否
        tStrBSql.append(" '' PolicyLoan, ");//是否存在保单借款
        tStrBSql.append(" '' AutoPaidUp, ");//是否自动垫交
        tStrBSql.append(" '0' CoInsurance, ");//是否共保
        tStrBSql.append(" '' LeadCoInsurance, ");//是否主共保
        
        tStrBSql.append("( case when lcc.payintv='1'  then prem*12 when lcc.payintv='3'  then prem*4  when lcc.payintv='6'  then prem*2 when lcc.payintv='12' then prem   else 0 end )  YearPremium");//当年年化保费
        
       
        tStrBSql.append(" from LCCont lcc ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcc.ContNo = '" + tContNo + "' ");
        tStrBSql.append(" and lcc.ContType = '1' ");
      

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    /**
     * 调用前置机服务
     * @return
     */
    private boolean calService()
    {
        Document tDocument = formJDocMessage(mRoot, "01", mDataInfoKey);
        JdomUtil.print(tDocument);

        Document tInStdXmlDoc = tDocument;

        ReqJKXPreService tJKXPreServlet = new ReqJKXPreService();
        String tReceiveXmlDoc = tJKXPreServlet.callService(tInStdXmlDoc);
//        JdomUtil.print(tReceiveXmlDoc);
        System.out.println(tReceiveXmlDoc);

        return true;
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        System.out.println(szFunc + ": " + szErrMsg);

        CError cError = new CError();
        cError.moduleName = "AddCertifyListBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    /**
     * 地址转换
     * 只调整Address3 ，将地址包含北京行政区名字的，转换为对应代码，比如地址为 ：
	 * a、北京市朝阳区团结湖路15号 ，那么我们平台做如下显示 北京市 市辖区 朝阳区 北京市朝阳区团结湖路15号
	 * b、北京市朝阳团结湖路15号， 由于没有“朝阳区”字样，只能显示为默认值 北京市 市辖区 西城区 北京市朝阳区团结湖路15号
     * @param args
     */
    public static String getTransAddress(String address3){
    	String code = "";
    	ExeSQL exeSql = new ExeSQL();
    	String sql = "select code,codename from ldcode where codetype='bjArea' with ur";
    	SSRS result = exeSql.execSQL(sql);
    	for(int i=1;i<=result.getMaxRow();i++){
    		String area = result.GetText(i, 2);
    		if(address3.contains(area)){
    			code = result.GetText(i, 1);
    			break;
    		}
    	}
    	if(code.equals("")||code==null){
    		code = "110102"; //西城区
    	}
    	return code;
    }
    public static void main(String args[])
    {
        QYJKXTwoServiceImp tBusLogic = new QYJKXTwoServiceImp();
//        VData tVData = new VData();
//        /*
//         * 001016600000005
//		 *013936034000001
//		 *013936034000002
//         */
//        TransferData mTransferData = new TransferData(); //个单 有受益人 000000001000006
//        mTransferData.setNameAndValue("DataInfoKey", "000000001000004"); //个单 000000001000004 团单 000000001000004
//        tVData.add(mTransferData);apay
//        tBusLogic.callJKXService(tVData, null);
        tBusLogic.loadPolicyInfo("035867774000002");
    }
}
