/**
 * 2010-11-18
 */
package com.sinosoft.lis.jkxpt.qy;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.jkxpt.JKXService;
import com.sinosoft.lis.jkxpt.ReqJKXPreService;
import com.sinosoft.lis.taskservice.DataTrackBL;
import com.sinosoft.lis.yibaotong.JdomUtil;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class QYACCGrpServiceImp extends JKXService
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private TransferData mTransferData = null;

    private String mDataInfoKey = null;

    private Element mRoot = null;

    public boolean callJKXService(VData cInputData, String cOperate)
    {
        // 获取入口参数 
        if (!getInputData(cInputData, cOperate))
        {
            return false;
        }
        // --------------------

        // 取数逻辑处理        
        //        mRoot = dealBusiXml(mPolicyNo);
        mRoot = reportBusiXml(mDataInfoKey);
        // --------------------

        // 调用前置机
        if (!calService())
        {
            return false;
        }
        // --------------------

        return true;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {

        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mDataInfoKey = (String) mTransferData.getValueByName("DataInfoKey");
        if (mDataInfoKey == null || mDataInfoKey.equals(""))
        {
            buildError("getInputData", "获取保单合同号码失败。");
            return false;
        }

        return true;
    }

    private Element reportBusiXml(String cDataInfoKey)
    {
        Element tRoot = new Element("AccidentGroupPolicyDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.accident.insurance.dto.AccidentGroupPolicyDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolicyInfo(cDataInfoKey);
        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String tContNo = null;

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);
            
            tTmpEle = new Element("SplitPolicyNo");      //拆单标志
            //tTmpEle.addAttribute("CData", "CBusCode");
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ApplicationFormNo");   //投保单号码
            tTmpEle.setText(tDataInfo[1]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ApplicationDate");      //投保申请日期
            tTmpEle.setText(tDataInfo[2]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PolicyNo");     //保单号码
            tContNo = tDataInfo[3];
            tTmpEle.setText(tDataInfo[3]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PolicyStartDate");    //保单起保时间
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PolicyEndDate");    //保单满期时间
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RenewalMethod");    //投保方式
            tTmpEle.setText(tDataInfo[6]);
            //tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RenewalTimes");      //续保次数
            tTmpEle.setText(tDataInfo[7]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Sales");            //销售渠道
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SalesChannelCode");    //销售机构代码
            tTmpEle.setText(tDataInfo[9]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SalesChannelName");  //销售机构名称
            tTmpEle.setText(tDataInfo[10]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("BusinessAddress");
            tTmpEle.setText(tDataInfo[11]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ProductName");     //产品名称
            tTmpEle.setText(tDataInfo[12]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PolicyHolderNum");          //投保人数
            tTmpEle.setText(tDataInfo[13]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("EffectiveInsuredNum");          //有效被保险人数
            tTmpEle.setText(tDataInfo[14]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("Premium");          //期初保费
            tTmpEle.setText(tDataInfo[15]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SpecialRemark");     //特别约定
            tTmpEle.setText(tDataInfo[16]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("singleDate");      //签单日期
            tTmpEle.setText(tDataInfo[17]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("underwritngDate");      //核保日期
            tTmpEle.setText(tDataInfo[18]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("salesChannelNameComment");  //销售机构名称备注
            tTmpEle.setText(tDataInfo[19]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            //获取个人投保人数组
//          获取团体投保人数组
            tTmpEle = reportPolGrpInfo(tContNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

            //被保险人数组
            tTmpEle = reportPolInsInfo(tContNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

            // --------------------

        }

        return tRoot;
    }



    private Element reportPolGrpInfo(String cContNo)
    {
        Element tRoot = new Element("AccidentGroupPolicyPersonphDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.accident.insurance.dto.AccidentGroupPolicyPersonphDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolGrpInfo(cContNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);
            tTmpEle = new Element("Name"); //单位名称   
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritType"); //团体客户证件类别
            tTmpEle.setText(tDataInfo[1]);
            //tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritCode"); //团体客户证件号码
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("Contact"); //联系人姓名
            tTmpEle.setText(tDataInfo[3]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ContactPhone"); //联系人手机
            tTmpEle.setText(tDataInfo[4]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("ContactTel"); //联系电话
            tTmpEle.setText(tDataInfo[5]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

        }

        return tRoot;
    }

    private Element reportPolInsInfo(String cContNo)
    {
        Element tRoot = new Element("AccidentGroupPolicyInsuredDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.accident.insurance.dto.AccidentGroupPolicyInsuredDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolInsInfo(cContNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);
            
            tTmpEle = new Element("CustomerNo"); //客户编码   
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Name"); //姓名    
            tTmpEle.setText(tDataInfo[1]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Gender"); //性别       
            tTmpEle.setText(tDataInfo[2]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Birthday"); //出生日期        
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritType"); //被保险人证件类别    
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritCode"); //被保险人证件号码   
            tTmpEle.setText(tDataInfo[5]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("Mobile"); //手机号码        
            tTmpEle.setText(tDataInfo[6]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("OfferStatus"); //要约状态            
            tTmpEle.setText(tDataInfo[7]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SubStartDate"); //被保险人责任开始日期
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SubEndDate"); //被保险人责任终止日期
            tTmpEle.setText(tDataInfo[9]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("InsuredType"); //被保险人类型    
            tTmpEle.setText(tDataInfo[10]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SnOfMainInsured"); //主被保人序号  
            tTmpEle.setText(tDataInfo[11]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RelationshipWithMainInsured"); //与主被保人关系 
            tTmpEle.setText(tDataInfo[12]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("AppointedBenefit"); //是否指定收益人   
            tTmpEle.setText(tDataInfo[13]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("InsuredPremium"); //被保险人的期初保费   
            tTmpEle.setText(tDataInfo[14]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            String tInsuredNo = tDataInfo[0];

            //获取受益人数组
            String tContNo = tDataInfo[15];//用个单号码去查询受益人
            tTmpEle = reportPolInsBnfInfo(tContNo,tInsuredNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
            
//          获取特别承保险种数组
            tTmpEle = reportPolInsUpCovInfo(cContNo, tInsuredNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
        }

        return tRoot;
    }

    private Element reportPolInsUpCovInfo(String cContNo, String cInsuredNo)
    {
        Element tRoot = new Element("AccidentGroupPolicyUpcoverageDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.accident.insurance.dto.AccidentGroupPolicyUpcoverageDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolInsUpCovInfo(cContNo, cInsuredNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("CoverageCode"); //平台险种类型
            tTmpEle.setText(tDataInfo[0]);
            //tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ComCoverageCode"); //平台险种代码
            tTmpEle.setText(tDataInfo[1]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageName"); //公司险种名称
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageEffectiveDate"); //险种起保日期
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageExpireDate"); //险种满期日期
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("MainAttachedFlag"); //主附险性质
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoveragePremium"); //险种保费    
            tTmpEle.setText(tDataInfo[6]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageSuminsured"); //险种保额    
            tTmpEle.setText(tDataInfo[7]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PaymentMethod"); //缴费方式
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PaymentYears"); //缴费年限
            tTmpEle.setText(tDataInfo[9]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SpecificBusiness"); //特殊业务标识
            tTmpEle.setText(tDataInfo[10]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SpecificBusinessCode"); //特殊业务代码
            tTmpEle.setText(tDataInfo[11]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            String tPolNo = tDataInfo[12];  
            //获取特别承保责任数组
            tTmpEle = reportPolInsCovInfo(tPolNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

            // 获取特别核保结论数组
//            tTmpEle = reportPolSpeUWInfo(tPolNo);
//            if (tTmpEle != null)
//            {
//                tItemEle.addContent(tTmpEle);
//            }
//            tTmpEle = null;

        }

        return tRoot;
    }

    private Element reportPolInsCovInfo(String cPolNo)
    {
        Element tRoot = new Element("AccidentGroupPolicyCoverageDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.accident.insurance.dto.AccidentGroupPolicyCoverageDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolInsCovInfo(cPolNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("LiabilityCode"); //责任代码    
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityName"); //责任名称    
            tTmpEle.setText(tDataInfo[1]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityEffectiveDate"); //责任开始日期
            tTmpEle.setText(tDataInfo[2]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityExpireDate"); //责任终止日期
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityStatus"); //责任状态    
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityPremium"); //责任保费    
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LimitAmount"); //责任保额    
            tTmpEle.setText(tDataInfo[6]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("WaitingPeriod"); //免责期      
            tTmpEle.setText(tDataInfo[7]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("Remark"); //备注      
            tTmpEle.setText(tDataInfo[8]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            //获取特别赔付比例分段数组
//            tTmpEle = reportPolSpeAmInfo(cPolNo);
//            if (tTmpEle != null)
//            {
//                tItemEle.addContent(tTmpEle);
//            }
//            tTmpEle = null;

        }

        return tRoot;
    }


    private Element reportPolInsBnfInfo(String cContNo,String cInsuredNo)
    {
        Element tRoot = new Element("AccidentGroupPolicyBenefitDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.accident.insurance.dto.AccidentGroupPolicyBenefitDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolInsBnfInfo(cContNo,cInsuredNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("Name"); //名称   
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Gender"); //性别        
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Birthday"); //出生日期      
            tTmpEle.setText(tDataInfo[2]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritType"); //证件类别      
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritCode"); //证件号码     
            tTmpEle.setText(tDataInfo[4]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RelationshipWithInsured"); //与被保险人关系
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("OrderNo"); //顺序号        
            tTmpEle.setText(tDataInfo[6]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Proportion"); //比例值
            tTmpEle.setText(tDataInfo[7]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
        }

        return tRoot;
    }

    private SSRS loadPolInsBnfInfo(String cContNo,String cInsuredNo)
    {
        SSRS tResult = null;

        StringBuffer tStrBSql = new StringBuffer();

        tStrBSql.append(" select ");
        tStrBSql.append(" lcb.name Name, ");//姓名
        tStrBSql.append(" lcb.sex Gender, ");//性别
        tStrBSql.append(" lcb.birthday Birthday, ");//出生日期
        tStrBSql.append(" lcb.idtype CritType, ");//证件类别
        tStrBSql.append(" lcb.idno CritCode, ");//证件号码
        tStrBSql.append(" lcb.RelationToInsured RelationshipWithInsured, ");//与被保险人关系
        tStrBSql.append(" lcb.BnfGrade OrderNo, ");//顺序号
        tStrBSql.append(" lcb.BnfLot Proportion ");//比例值
        tStrBSql.append(" from lcbnf lcb ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and contno = '"+cContNo+"' ");
        tStrBSql.append(" and insuredno = '"+cInsuredNo+"' ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadPolInsCovInfo(String cPolNo)
    {
        SSRS tResult = null;

        StringBuffer tStrBSql = new StringBuffer();

        tStrBSql.append(" select ");
        tStrBSql.append(" lcd.DutyCode LiabilityCode, ");//责任代码
        tStrBSql.append(" lmd.DutyName LiabilityName, ");//责任名称
        tStrBSql.append(" lcd.GetStartDate LiabilityEffectiveDate, ");//责任开始日期
        tStrBSql.append(" (lcd.EndDate -1 day) LiabilityExpireDate, ");//责任终止日期
        tStrBSql.append(" lcp.StateFlag LiabilityStatus, ");//责任状态
        tStrBSql.append(" lcd.Prem LiabilityPremium, ");//责任保费
        tStrBSql.append(" lcd.Amnt LimitAmount, ");//责任保额
        tStrBSql.append(" '0' WaitingPeriod, ");//免责期
        tStrBSql.append(" '' Remark ");//备注
        tStrBSql.append(" from LCPol lcp ");
        tStrBSql.append(" inner join LCDuty lcd on lcd.PolNo = lcp.PolNo ");
        tStrBSql.append(" inner join LMDuty lmd on lmd.DutyCode = lcd.DutyCode ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.polno = '" + cPolNo + "' ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadPolInsUpCovInfo(String cContNo, String cInsuredNo)
    {
        SSRS tResult = null;

        StringBuffer tStrBSql = new StringBuffer();

        tStrBSql.append(" select ");
        tStrBSql.append(" Case when lcp.riskcode in ('5601','1607') then 'A02' else 'A01' end CoverageCode, ");//平台险种代码
        tStrBSql.append(" lcp.RiskCode ComCoverageCode, ");//公司险种代码
        tStrBSql.append(" lmra.RiskName CoverageName, ");//公司险种名称
        tStrBSql.append(" lcp.GetStartDate CoverageEffectiveDate, ");//险种起保日期
        tStrBSql.append(" (lcp.EndDate -1 day) CoverageExpireDate, ");//险种满期日期
        tStrBSql.append(" lmra.SubRiskFlag MainAttachedFlag, ");//主附险性质
        tStrBSql.append(" lcp.Prem CoveragePremium, ");//险种期初保费
        tStrBSql.append(" lcp.Amnt CoverageSuminsured, ");//险种期初保额
        tStrBSql.append(" lcp.PayIntv PaymentMethod, ");//缴费方式
        tStrBSql.append(" (Case when lcp.PayIntv is null then '' else char(lcp.PayYears) end) PaymentYears, ");//缴费年限
        tStrBSql.append(" '0' SpecificBusiness, ");//特殊业务标识
        tStrBSql.append(" '' SpecificBusinessCode, ");//特殊业务代码
        tStrBSql.append(" lcp.polno ");//特殊业务代码
        tStrBSql.append(" from LCPol lcp ");
        tStrBSql.append(" inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.GrpContNo = '" + cContNo + "' and lcp.InsuredNo = '" + cInsuredNo + "' ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadPolInsInfo(String cContNo)
    {
        SSRS tResult = null;
        String tContNo = cContNo;
        StringBuffer tStrBSql = new StringBuffer();

        tStrBSql.append(" select ");
        tStrBSql.append(" lci.insuredno CustomerNo, ");//客户编号
        tStrBSql.append(" lci.Name Name, ");//姓名
        tStrBSql.append(" lci.Sex Gender, ");//性别
        tStrBSql.append(" lci.Birthday Birthday, ");//出生日期
        tStrBSql.append(" (case when lci.IDType is not null and lci.IDType !='' then lci.IDType else lci.othidtype end)  CritType, ");//被保险人证件类别
        tStrBSql.append(" (case when lci.IDNo is not null and lci.IDNo != '' then lci.IDNo else lci.othidno end)  CritCode, ");//被保险人证件号码
//        tStrBSql.append(" lcad.mobile Mobile, ");//手机号码
        tStrBSql.append(" (select case when (lcad.Mobile is not null and lcad.Mobile != '') then lcad.Mobile when (lcad.Phone is not null and lcad.Phone!='') then lcad.Phone else lci.grpinsuredphone end from lcaddress lcad where lcad.customerno =lci.insuredno and lcad.addressno = lci.addressno ) Mobile,");//手机号码
        tStrBSql.append(" '1' OfferStatus, ");//要约状态
        tStrBSql.append(" (select cvalidate from lcgrpcont where grpcontno = lci.grpcontno ) SubStartDate, ");//被保险人责任开始日期
        tStrBSql.append(" (select cinvalidate from lcgrpcont where grpcontno =lci.grpcontno ) SubEndDate, ");//被保险人责任终止日期
        tStrBSql.append(" (case when lci.relationtomaininsured = '00' then '01' else '02' end) InsuredType, ");//被保险人类型
        tStrBSql.append(" '1' SnOfMainInsured, ");//主被保人序号
        tStrBSql.append(" lci.relationtomaininsured InsuredType, ");//与主被保人关系
        tStrBSql.append(" (case when exists(select 1 from lcbnf where contno = lci.contno and insuredno = lci.insuredno ) then 1 else 0 end) AppointedBenefit, ");//是否指定受益人
        tStrBSql.append(" (select sum(prem) from lcpol where grpcontno = lci.grpcontno and contno = lci.contno and insuredno = lci.insuredno) InsuredPremium, ");//被保险人的期初保费
        tStrBSql.append(" lci.contno ");//保单号码，用于查询受益人
        tStrBSql.append(" from lcinsured lci ");
//        tStrBSql.append(" inner join lcaddress lcad ");
//        tStrBSql.append(" on lci.insuredno = lcad.customerno ");
//        tStrBSql.append(" and lci.addressno = lcad.addressno ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lci.GrpContNo = '" + tContNo + "' ");
        

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);
        
        System.out.println("-------"+tResult);

        return tResult;
    }

    private SSRS loadPolGrpInfo(String cContNo)
    {
        SSRS tResult = null;

        String tContNo = cContNo;

        StringBuffer tStrBSql = new StringBuffer();

        tStrBSql.append(" select ");
        tStrBSql.append(" lgap.Name Name, ");
        tStrBSql.append(" (case when lgap.OrganComCode is not null then '2' else '9' end) CritType, ");
        tStrBSql.append(" (case when lgap.OrganComCode is null then '999999999' else lgap.OrganComCode end) CritCode, ");
        tStrBSql.append(" lgad.linkman1 Contact, ");
        tStrBSql.append(" lgad.mobile1 ContactPhone, ");
        tStrBSql.append(" lgad.phone1 ContactTel ");
        tStrBSql.append(" from LCGrpAppnt lgap ");
        tStrBSql.append(" inner join lcgrpaddress lgad ");
        tStrBSql.append(" on lgap.customerno = lgad.customerno ");
        tStrBSql.append(" and lgap.addressno = lgad.addressno ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lgap.GrpContNo = '" + tContNo + "' ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadPolicyInfo(String cDataInfoKey)
    {
        SSRS tResult = null;

        String tContNo = cDataInfoKey;
//      获取产品名称
        String tProductName = "";
        String tSql = "select riskname from lmrisk where riskcode in (select distinct riskcode from lcgrppol where grpcontno = '"+tContNo+"' )";
        SSRS tSSRS = new ExeSQL().execSQL(tSql);
        if(tSSRS == null || tSSRS.getMaxRow()<=0){
        	System.out.println("保单【"+tContNo+"】获取产品名称时失败！");
        	return tResult;
        }
        for(int i=1;i<=tSSRS.MaxRow;i++){
        	if("".equals(tProductName)){
        		tProductName += tSSRS.GetText(i,1);
        	}else{
        		tProductName += ","+tSSRS.GetText(i,1);
        	}
        }

        StringBuffer tStrBSql = new StringBuffer();
        tStrBSql.append(" select ");
        tStrBSql.append(" '0' SplitPolicyNo, ");//拆单标志
        tStrBSql.append(" lgc.Prtno ApplicationFormNo, ");//投保单号码
        tStrBSql.append(" lgc.PolApplyDate ApplicationDate, ");//投保申请日期
        tStrBSql.append(" lgc.GrpContNo PolicyNo, ");//保单号码
        tStrBSql.append(" lgc.CValiDate PolicystartDate, ");//保单起保时间
        tStrBSql.append(" (lgc.CInValiDate ) PolicyendDate, ");//保单满期时间
        tStrBSql.append(" '01' RenewalMethod, ");//投保方式
        tStrBSql.append(" '0' RenewalTimes, ");//续保次数
        tStrBSql.append(" lgc.SaleChnl Sales, ");//销售渠道
        tStrBSql.append(" case when (lgc.AgentCom is null or lgc.AgentCom = '') then lgc.managecom else lgc.AgentCom end SalesChannelCode, ");//销售机构代码
        tStrBSql.append(" case when (lgc.agentcom = '' or lgc.agentcom is null) then (select name from ldcom where comcode = lgc.managecom) else (select name from lacom where agentcom = lgc.agentcom) end SalesChannelName, ");//销售机构名称
        tStrBSql.append(" (case when lgc.SaleChnl in ('01', '02', '06', '07', '13') then '北京市西城区阜成门外大街7号' when lgc.SaleChnl in ('03', '04', '10') then '北京市' end) BusinessAddress, ");//营业场所地址
        tStrBSql.append(" '"+tProductName+"' ProductName, ");//产品名称
        tStrBSql.append(" (select count(1) from lcinsured where grpcontno = '" + tContNo + "') PolicyHolderNum, ");//投保人数
        tStrBSql.append(" (select count(1) from lcinsured where grpcontno = '" + tContNo + "') EffectiveInsuredNum, ");//有效被保险人数
        tStrBSql.append(" lgc.prem Premium, ");//期初保费
        tStrBSql.append(" lgc.remark SpecialRemark, ");//特别约定
        tStrBSql.append(" lgc.SignDate singleDate, ");//签单日期
        tStrBSql.append(" (case when lgc.UWDate < lgc.CValiDate then lgc.UWDate else lgc.CValiDate - 1 day end) underwritngDate, ");//核保日期
        tStrBSql.append(" '' salesChannelNameComment ");//销售机构名称备注
        tStrBSql.append(" from LCGrpCont lgc ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lgc.GrpContNo = '" + tContNo + "' ");
        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    /**
     * 调用前置机服务
     * @return
     */
    private boolean calService()
    {
        Document tDocument = formJDocMessage(mRoot, "Acc02", mDataInfoKey);
        JdomUtil.print(tDocument);

        Document tInStdXmlDoc = tDocument;

        ReqJKXPreService tJKXPreServlet = new ReqJKXPreService();
        String re = tJKXPreServlet.callService(tInStdXmlDoc); 
        
        try
	        {
    		 DataTrackBL mDataTrackBL=new DataTrackBL();
    		 if(!mDataTrackBL.submitData(mDataInfoKey, "Acc02", re)){
    			 System.out.println("契约团单报送插入报送轨迹表出错");
    		 }
	        }
	        catch (Exception ex)
	        {
	            // @@错误处理
	            CError.buildErr(this,"契约团单报送插入报送轨迹表出错。");
	            
	        }        

        return true;
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        System.out.println(szFunc + ": " + szErrMsg);

        CError cError = new CError();
        cError.moduleName = "AddCertifyListBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public static void main(String args[])
    {
        QYACCGrpServiceImp tBusLogic = new QYACCGrpServiceImp();
        VData tVData = new VData();
        TransferData mTransferData = new TransferData();
        mTransferData.setNameAndValue("DataInfoKey", "00105479000001");
        tVData.add(mTransferData);
        tBusLogic.callJKXService(tVData, null);
    }
}
