package com.sinosoft.lis.jkxpt;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import org.apache.log4j.Logger;
import org.jdom.Document;
//import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.yibaotong.ConfigFactory;
import com.sinosoft.lis.yibaotong.JdomUtil;

public class ReqJKXPreService
{
    private final static Logger cLogger = Logger.getLogger(ReqJKXPreService.class);

    public String callService(Document pInStdXmlDoc)
    {
        try
        {
            return callJKXPreServlet(pInStdXmlDoc);
        }
        catch (Exception ex)
        {
            System.err.println("与前置机通信出现异常");
            //			cLogger.error("与前置机通信出现异常");
            return null;
        }
    }

    /**
     * 调用健康险平台前置机servlet
     * @param pInStdXmlDoc
     * @return
     * @throws Exception
     */
    private String callJKXPreServlet(Document pInStdXmlDoc) throws Exception
    {
        //		cLogger.info("Into ReqJKXPreService.callJKXPreServlet()...");
        System.out.println("Into ReqJKXPreService.callJKXPreServlet()...");

        //		String mPreServletURL = ConfigFactory.getPreConfig().getString("PostServletURL");
        //		String mPreServletURL = "http://10.252.130.195:8088/jkxptpre/JKXPreServlet";

        //        String mPreServletURL = "http://10.252.130.197:8080/JKXPtPre/PubJKXPreServlet";
     //   String mPreServletURL = "http://10.252.56.35:9080/PubJKXPreServlet"; //测试地址   旧的
        String mPreServletURL = "http://10.136.4.9:9080/PubJKXPreServlet"; //测试地址   新的
     //   String mPreServletURL ="http://localhost:8080/ui/PubJKXPreServlet";  //本地
        //        String mPreServletURL = "http://10.252.56.31:9080/PubJKXPreServlet"; //正式地址
    
        //		cLogger.info("PreServletURL = " + mPreServletURL);
        System.out.println("PreServletURL = " + mPreServletURL);
        URL mURL = new URL(mPreServletURL);
        URLConnection mURLConnection = mURL.openConnection();
        mURLConnection.setDoOutput(true);
        mURLConnection.setDoInput(true);

        XMLOutputter mXMLOutputter = new XMLOutputter();
        mXMLOutputter.setEncoding("GBK");
        //		XMLOutputter mXMLOutputter = new XMLOutputter(Format.getCompactFormat()
        //				.setEncoding("GBK"));
        //		cLogger.info("开始健康险平台前置机发送报文...");
        System.out.println("开始健康险平台前置机发送报文...");
        OutputStream mURLOs = mURLConnection.getOutputStream();
        mXMLOutputter.output(pInStdXmlDoc, mURLOs);
        mURLOs.close();

        Document mOutStdXml = null;
        // 其他正常交易
        long tStartMillis = System.currentTimeMillis();
        InputStream tURLIs = mURLConnection.getInputStream();
        mOutStdXml = JdomUtil.build(tURLIs); // 统一使用GBK编码
        tURLIs.close();
        //		cLogger.info("健康险平台前置机处理耗时：" + (System.currentTimeMillis() - tStartMillis) / 1000.0 + "s");
        //		cLogger.info("接收到健康险平台前置机返回报文，并解析为Document！");
        System.out.println("健康险平台前置机处理耗时：" + (System.currentTimeMillis() - tStartMillis) / 1000.0 + "s");
        System.out.println("接收到健康险平台前置机返回报文，并解析为Document！");

        JdomUtil.print(mOutStdXml);

		String str = JdomUtil.outputToString(mOutStdXml);
		
		String re = str.substring(str.indexOf("<RESPONSE_CODE>") + 15, str.indexOf("</RESPONSE_CODE>"));
		
	
        return re;

    }
}
