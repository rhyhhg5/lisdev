package com.sinosoft.lis.jkxpt;

public final class LPXMLTag {

	// 案件信息表
	final String CaseNo = "CaseNo";

	final String RgtNo = "RgtNo";

	final String RgtType = "RgtType";

	final String RgtState = "RgtState";

	final String CustomerNo = "CustomerNo";

	final String CustomerName = "CustomerName";

	final String RgtDate = "RgtDate";

	final String AffixGetDate = "AffixGetDate";

	final String DeathDate = "DeathDate";

	final String CustState = "CustState";

	final String CustBirthday = "CustBirthday";

	final String CustomerSex = "CustomerSex";

	final String CustomerAge = "CustomerAge";

	final String IDType = "IDType";

	final String IDNo = "IDNo";

	final String Handler = "Handler";

	final String Dealer = "Dealer";

	final String EndCaseDate = "EndCaseDate";

	final String CancleReason = "CancleReason";

	final String CancleRemark = "CancleRemark";

	final String Cancler = "Cancler";

	final String CancleDate = "CancleDate";

	final String Rigister = "Rigister";

	final String OtherIDType = "OtherIDType";

	final String OtherIDNo = "OtherIDNo";

	final String[] LLCaseSet = { CaseNo, RgtNo, RgtType, RgtState, CustomerNo,
			CustomerName, RgtDate, AffixGetDate, DeathDate, CustState,
			CustBirthday, CustomerSex, CustomerAge, IDType, IDNo, Handler,
			Dealer, EndCaseDate, CancleReason, CancleRemark, Cancler,
			CancleDate, Rigister, OtherIDType, OtherIDNo };

	// 案件\事件
	final String CaseRelaNo = "CaseRelaNo";

	final String SubRptNo = "SubRptNo";

	final String AccidentType = "AccidentType";

	final String AccDate = "AccDate";

	final String AccDesc = "AccDesc";

	final String AccPlace = "AccPlace";

	final String[] LLSubReportSet = { CaseRelaNo, SubRptNo, AccidentType,
			AccDate, AccPlace, AccDesc };

	// 申请信息表

	final String RgtObj = "RgtObj";

	final String RgtObjNo = "RgtObjNo";

	final String RgtClass = "RgtClass";

	final String RgtantName = "RgtantName";

	final String Relation = "Relation";

	final String RgtantAddress = "RgtantAddress";

	final String RgtantPhone = "RgtantPhone";

	final String RgtantMobile = "RgtantMobile";

	final String PostCode = "PostCode";

	final String GrpName = "GrpName";

	final String GetMode = "GetMode";

	final String[] LLRegisterSet = { RgtNo, RgtState, RgtObj, RgtObjNo,
			RgtType, RgtClass, RgtantName, Relation, RgtantAddress,
			RgtantPhone, RgtantMobile, PostCode, CustomerNo, GrpName, RgtDate,
			GetMode };

	// 申请原因
	final String ReasonCode = "ReasonCode";

	final String LLAppClaimReasonSet[] = { ReasonCode };

	// 疾病

	final String DiseaseCode = "DiseaseCode";

	final String DiseaseName = "DiseaseName";

	final String[] LLCaseCureSet = { CaseRelaNo, DiseaseCode, DiseaseName };

	// 账单主表

	final String MainFeeNo = "MainFeeNo";

	final String HospitalCode = "HospitalCode";

	final String ReceiptNo = "ReceiptNo";

	final String FeeType = "FeeType";

	final String FeeAtti = "FeeAtti";

	final String FeeDate = "FeeDate";

	final String HospStartDate = "HospStartDate";

	final String HospEndDate = "HospEndDate";

	final String RealHospDate = "RealHospDate";

	final String InHosNo = "InHosNo";

	final String PreAmnt = "PreAmnt";

	final String SelfAmnt = "SelfAmnt";

	final String RefuseAmnt = "RefuseAmnt";

	final String SocialPlanAmnt = "SocialPlanAmnt";

	final String OtherOrganAmnt = "OtherOrganAmnt";

	final String SumFee = "SumFee";

	final String HosGrade = "HosGrade";

	final String SecurityNo = "SecurityNo";

	final String InsuredStat = "InsuredStat";

	final String Remnant = "Remnant";

	final String[] LLFeeMainSet = { MainFeeNo, CaseRelaNo, HospitalCode,
			ReceiptNo, FeeType, FeeAtti, FeeDate, HospStartDate, HospEndDate,
			RealHospDate, InHosNo, PreAmnt, SelfAmnt, RefuseAmnt,
			SocialPlanAmnt, OtherOrganAmnt, SumFee, HosGrade, SecurityNo,
			InsuredStat, Remnant };

	// 社保账单表

	final String ApplyAmnt = "ApplyAmnt";

	final String FeeInSecu = "FeeInSecu";

	final String FeeOutSecu = "FeeOutSecu";

	final String TotalSupDoorFee = "TotalSupDoorFee";

	final String SupDoorFee = "SupDoorFee";

	final String YearSupDoorFee = "YearSupDoorFee";

	final String PlanFee = "PlanFee";

	final String YearPlayFee = "YearPlayFee";

	final String SupInHosFee = "SupInHosFee";

	final String YearSupInHosFee = "YearSupInHosFee";

	final String SecurityFee = "SecurityFee";

	final String SmallDoorPay = "SmallDoorPay";

	final String EmergencyPay = "EmergencyPay";

	final String SelfPay1 = "SelfPay1";

	final String SelfPay2 = "SelfPay2";

	final String SecuRefuseFee = "SecuRefuseFee";

	final String GetLimit = "GetLimit";

	final String LowAmnt = "LowAmnt";

	final String MidAmnt = "MidAmnt";

	final String HighAmnt1 = "HighAmnt1";

	final String HighAmnt2 = "HighAmnt2";

	final String SuperAmnt = "SuperAmnt";

	final String AccountPay = "AccountPay";

	final String OfficialSubsidy = "OfficialSubsidy";

	final String HighDoorAmnt = "HighDoorAmnt";

	final String RetireAddFee = "RetireAddFee";

	final String YearRetireAddFee = "YearRetireAddFee";

	final String StandbyAmnt = "StandbyAmnt";

	final String[] LLSecurityReceiptSet = { MainFeeNo, ApplyAmnt, FeeInSecu,
			FeeOutSecu, TotalSupDoorFee, SupDoorFee, YearSupDoorFee, PlanFee,
			YearPlayFee, SupInHosFee, YearSupInHosFee, SecurityFee,
			SmallDoorPay, EmergencyPay, SelfPay1, SelfPay2, SecuRefuseFee,
			SelfAmnt, GetLimit, LowAmnt, MidAmnt, HighAmnt1, HighAmnt2,
			SuperAmnt, AccountPay, OfficialSubsidy, HighDoorAmnt, RetireAddFee,
			YearRetireAddFee, StandbyAmnt };

	// 费用清单

	final String FeeDetailNo = "FeeDetailNo";

	final String FeeItemType = "FeeItemType";

	final String FeeItemCode = "FeeItemCode";

	final String FeeItemName = "FeeItemName";

	final String Fee = "Fee";

	final String[] LLCaseReceiptSet = { MainFeeNo, FeeDetailNo, FeeItemType,
			FeeItemCode, FeeItemName, Fee, PreAmnt, SelfAmnt, RefuseAmnt };

	// 扣除明细
	final String DrugDetailNo = "DrugDetailNo";

	final String DrugType = "DrugType";

	final String DrugCode = "DrugCode";

	final String DrugName = "DrugName";

	final String SecuFee = "SecuFee";

	final String SelfFee = "SelfFee";

	final String PayRate = "PayRate";

	final String PayLimit = "PayLimit";

	final String Remark = "Remark";

	final String UnReasonableFee = "UnReasonableFee";

	final String PrescriptionNo = "PrescriptionNo";

	final String InputType = "InputType";

	final String[] LLCaseDrugSet = { DrugDetailNo, MainFeeNo, DrugType,
			DrugCode, DrugName, Fee, SecuFee, SelfPay2, SelfFee, PayRate,
			PayLimit, Remark, UnReasonableFee, PrescriptionNo, InputType };

	// 理赔明细
	final String GrpContNo = "GrpContNo";

	final String GrpPolNo = "GrpPolNo";

	final String ContNo = "ContNo";

	final String PolNo = "PolNo";

	final String RiskCode = "RiskCode";

	final String DutyCode = "DutyCode";

	final String GetDutyCode = "GetDutyCode";

	final String GetDutyKind = "GetDutyKind";

	final String TabFeeMoney = "TabFeeMoney";

	final String ClaimMoney = "ClaimMoney";

	final String DeclineAmnt = "DeclineAmnt";

	final String OverAmnt = "OverAmnt";

	final String RealPay = "RealPay";

	final String OutDutyAmnt = "OutDutyAmnt";

	final String OutDutyRate = "OutDutyRate";

	final String GiveType = "GiveType";

	final String GiveReason = "GiveReason";

	final String[] LLClaimDetailSet = { GrpContNo, GrpPolNo, ContNo, PolNo,
			RiskCode, DutyCode, GetDutyCode, GetDutyKind, CaseRelaNo,
			TabFeeMoney, ClaimMoney, DeclineAmnt, OverAmnt, RealPay,
			OutDutyAmnt, OutDutyRate, GiveType, GiveReason };

	// 理赔汇总
	final String[] LLClaimSet = { RealPay, GiveType };

}
