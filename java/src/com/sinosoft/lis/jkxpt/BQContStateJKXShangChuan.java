/**
 * 2010-11-18
 */
package com.sinosoft.lis.jkxpt;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.jkxpt.JKXService;
import com.sinosoft.lis.jkxpt.ReqJKXPreService;
import com.sinosoft.lis.yibaotong.JdomUtil;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
/**
 * @author dell
 *
 */
public class BQContStateJKXShangChuan extends JKXService
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private TransferData mTransferData = null;

    /** 数据操作字符串 */
    private String mOperate = "";

    private String mDataInfoKey = "";
    
    private String mEdorNo = ""; //保全受理号
    
    private String mContNo = ""; //保单号
    
    private String mStateFlag = "";//保单状态更新类型
    
    private String mEdorType = ""; //保全项目类型
    
    private String mEdorProp = ""; //个团标志 ：I-个单，G-团单

    private Element mRoot = null;

    private String mContType = null;

    public boolean callJKXService(VData cInputData, String cOperate)
    {
        // 获取入口参数 
        if (!getInputData(cInputData, cOperate))
        {
            return false;
        }
        // --------------------
        
        // 取数逻辑处理        
        mRoot = reportBusiXml(mDataInfoKey);
        // --------------------

        // 调用前置机
        if (!calService())
        {
            return false;
        }
        // --------------------

        return true;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }
        mDataInfoKey = (String) mTransferData.getValueByName("DataInfoKey");
        if (mDataInfoKey == null || mDataInfoKey.equals(""))
        {
            buildError("getInputData", "获取保全参数信息失败。");
            return false;
        }
        
    	String [] tEdorInfo = mDataInfoKey.split("&");
    	mEdorProp = tEdorInfo[0];
    	mContNo = tEdorInfo[1];
    	mStateFlag = tEdorInfo[2];
//		mEdorType = tEdorInfo[1];
//		mEdorProp = tEdorInfo[2];   
//        if (mContNo == null || mContNo.equals(""))
//        {
//            buildError("getInputData", "获取保单参数信息失败。");
//            return false;
//        }
//        if (mEdorProp == null || mEdorProp.equals(""))
//        {
//            buildError("getInputData", "获取保全个团标志EdorProp信息失败。");
//            return false;
//        }

        return true;
    }

    private Element reportBusiXml(String cDataInfoKey)
    {
        Element tRoot = new Element("UPStatusInfoDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.UPStatusInfoDTO");

        Element tTmpEle = null;

        SSRS tResPolicyLogInfo = loadPolicyLogInfo(cDataInfoKey);
        for (int idxPI = 1; idxPI <= tResPolicyLogInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyLogInfo.getRowData(idxPI);
            
            tTmpEle = new Element("PolicyNo");
            tTmpEle.setText(tDataInfo[0]);          
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PolicyStatusUpdateType");
            tTmpEle.setText(tDataInfo[1]);
//            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("TerminationDate");
            tTmpEle.setText(tDataInfo[2]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ContractStatus");
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("TerminationReason");
            tTmpEle.setText(tDataInfo[4]);
//            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            if(mStateFlag.equals("02")){
            	tTmpEle = new Element("RegularClearingMark");
                tTmpEle.setText(tDataInfo[5]);
                tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
                tItemEle.addContent(tTmpEle);
                tTmpEle = null;

                tTmpEle = new Element("RegularClearing");
                tTmpEle.setText(tDataInfo[6]);
                tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
                tItemEle.addContent(tTmpEle);
                tTmpEle = null;

                tTmpEle = new Element("RegularClearingDate");
                tTmpEle.setText(tDataInfo[7]);
                tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
                tItemEle.addContent(tTmpEle);
                tTmpEle = null;
            }
            
            tTmpEle = new Element("PauseDate");
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

           
            tTmpEle = reportPolicyUpcoverageLogInfo(mContNo);
	   	    if (tTmpEle != null)
	   	     {
	   	        tItemEle.addContent(tTmpEle);
	   	     }
 
            tTmpEle = null;
            

//          被保人数组          
            	tTmpEle = reportPolicyInsuredLogInfo(mContNo);
 	            if (tTmpEle != null)
 	            {
 	                tItemEle.addContent(tTmpEle);
 	            }
 	            tTmpEle = null;
 	            
            
        }

        return tRoot;
    }
	private String getItem(String s)
	{
		String[] t=null;
		t=s.split("&");
		return t[t.length-2];
	}

    private SSRS loadPolicyLogInfo(String cDataInfoKey)
    {
        SSRS tResult = null;
        StringBuffer tStrBSql = new StringBuffer();
        
//        用户名	USER
//        密码	PASSWORD
//        保单号	POLICYNO
//        保单状态修改类型	POLICYSTATUSUPDATETYPE
//        保单终止日期	TERMINATIONDATE
//        保单状态	CONTRACTSTATUS
//        保单终止原因	TERMINATIONREASON
//        定期结算标识	REGULARCLEARINGMARK
//        定期结算方式	REGULARCLEARING
//        定期结算日期	REGULARCLEARINGDATE
//        承保险种数组	UPCOVERAGE_ARRAY
//        被保险人数组	INSURED_ARRAY
        tStrBSql.append(" select  ");
        tStrBSql.append(" contNo PolicyNo ,");
        tStrBSql.append( "'"+mStateFlag +"' PolicyStatusUpdateType ,");
        tStrBSql.append(" Cvalidate TerminationDate ,");
        tStrBSql.append(" StateFlag ContractStatus ,");
        tStrBSql.append(" '2' TerminationReason ,"); //暂时设为2 理赔终止
        tStrBSql.append(" '1' RegularClearingMark ,");
        tStrBSql.append(" '1' RegularClearing ,");
        tStrBSql.append(" '' RegularClearingDate , ");
        tStrBSql.append(" (select startdate from lccontstate where contno='"+mContNo+"' and polno='000000' and statetype='Available'  and statereason='02') PauseDate ");
        tStrBSql.append(" from lccont where contno='"+mContNo+"'");   
        //个单B表
        tStrBSql.append(" union all ");
        tStrBSql.append(" select  ");
        tStrBSql.append(" contNo PolicyNo ,");
        tStrBSql.append( "'"+mStateFlag +"' PolicyStatusUpdateType ,");
        tStrBSql.append(" Cvalidate TerminationDate ,");
        tStrBSql.append(" StateFlag ContractStatus ,");
        tStrBSql.append(" '2' TerminationReason ,");//暂时设为2 理赔终止
        tStrBSql.append(" '1' RegularClearingMark ,");
        tStrBSql.append(" '1' RegularClearing ,");
        tStrBSql.append(" '' RegularClearingDate , ");
        tStrBSql.append(" (select startdate from lccontstate where contno='"+mContNo+"' and polno='000000' and statetype='Available'  and statereason='02') PauseDate ");
        tStrBSql.append(" from lbcont where contno='"+mContNo+"'");    
        //团单
        tStrBSql.append(" union all ");
        tStrBSql.append(" select  ");
        tStrBSql.append(" grpcontNo PolicyNo ,");
        tStrBSql.append( "'"+mStateFlag +"' PolicyStatusUpdateType ,");
        tStrBSql.append(" Cvalidate TerminationDate ,");
        tStrBSql.append(" StateFlag ContractStatus ,");
        tStrBSql.append(" '2' TerminationReason ,");//暂时设为2 理赔终止
        tStrBSql.append(" '1' RegularClearingMark ,");
        tStrBSql.append(" '1' RegularClearing ,");
        tStrBSql.append(" '' RegularClearingDate, ");
        tStrBSql.append(" (select startdate from lcgrpcontstate where grpcontno='"+mContNo+"' and statetype='Pause'  and statereason='01') PauseDate ");
        tStrBSql.append(" from lcgrpcont where grpcontno='"+mContNo+"'");  
        //团单B表
        tStrBSql.append(" union all ");
        tStrBSql.append(" select  ");
        tStrBSql.append(" grpcontNo PolicyNo ,");
        tStrBSql.append( "'"+mStateFlag +"' PolicyStatusUpdateType ,");
        tStrBSql.append(" Cvalidate TerminationDate ,");
        tStrBSql.append(" StateFlag ContractStatus ,");
        tStrBSql.append(" '2' TerminationReason ,");//暂时设为2 理赔终止
        tStrBSql.append(" '1' RegularClearingMark ,");
        tStrBSql.append(" '1' RegularClearing ,");
        tStrBSql.append(" '' RegularClearingDate, ");
        tStrBSql.append(" (select startdate from lcgrpcontstate where grpcontno='"+mContNo+"' and statetype='Pause'  and statereason='01') PauseDate ");
        tStrBSql.append(" from lbgrpcont where grpcontno='"+mContNo+"'");  
           
        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }
//  险种
    private Element reportPolicyUpcoverageLogInfo(String cContNo)
    {
        Element tRoot = new Element("UPStatusUpCoverageDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.UPStatusUpCoverageDTO");

        Element tTmpEle = null;

        SSRS tResPolicyUpcoverageLogInfo = loadPolicyUpcoverageLogInfo(cContNo);

        for (int idxPI = 1; idxPI <= tResPolicyUpcoverageLogInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyUpcoverageLogInfo.getRowData(idxPI);

            tTmpEle = new Element("CoverageType");
            tTmpEle.setText(tDataInfo[0]);
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageCode");
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ComCoverageCode");
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageSuminsured");
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为“NUMBER”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
//            tTmpEle = new Element("CoveragePremium");
//            tTmpEle.setText(tDataInfo[5]);
//            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为“NUMBER”的字段，请增加描述
//            tItemEle.addContent(tTmpEle);
//            tTmpEle = null;
            
            tTmpEle = new Element("CoverageStatus");
            tTmpEle.setText(tDataInfo[6]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            
            
//          获取险种下的责任信息
            String tPolNo = tDataInfo[4];            
            tTmpEle = reportPolicyCoverageLogInfo1(tPolNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
            

        }

        return tRoot;
    }
    
//责任
    private Element reportPolicyCoverageLogInfo(String cContNo)
    {
        Element tRoot = new Element("PolicyCoverageLogDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.PolicyCoverageLogDTO");

        Element tTmpEle = null;

        SSRS tResPolicyCoverageLogInfo = loadPolicyCoverageLogInfo(cContNo);

        for (int idxPI = 1; idxPI <= tResPolicyCoverageLogInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyCoverageLogInfo.getRowData(idxPI);

            tTmpEle = new Element("CoverageType");
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageCode");
            tTmpEle.setText(tDataInfo[1]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ComCoverageCode");
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageName");
            tTmpEle.setText(tDataInfo[3]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityClassification");
            tTmpEle.setText(tDataInfo[4]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityCode");
            tTmpEle.setText(tDataInfo[5]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityName");
            tTmpEle.setText(tDataInfo[6]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityEffectiveDate");
            tTmpEle.setText(tDataInfo[7]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityExpireDate");
            tTmpEle.setText(tDataInfo[8]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityStatus");
            tTmpEle.setText(tDataInfo[9]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("InsuredGroupCode");
            tTmpEle.setText(tDataInfo[10]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("GroupName");
            tTmpEle.setText(tDataInfo[11]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LimitAmount");
            tTmpEle.setText(tDataInfo[12]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("EffectivelyAmount");
            tTmpEle.setText(tDataInfo[13]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("WaitingPeriod");
            tTmpEle.setText(tDataInfo[14]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            String tPolNo = tDataInfo[14];
            
//          核保结论
            tTmpEle = reportUnderWritingLogInfo(tPolNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
            
//          赔付比例
            tTmpEle = reportPolicyAmountLogDTOInfo(tPolNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

        }

        return tRoot;
    }
    
//  责任
    private Element reportPolicyCoverageLogInfo1(String cPolNo)
    {
        Element tRoot = new Element("UPStatusCoverageDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.UPStatusCoverageDTO");

        Element tTmpEle = null;

        SSRS tResPolicyCoverageLogInfo1 = loadPolicyCoverageLogInfo1(cPolNo);

        for (int idxPI = 1; idxPI <= tResPolicyCoverageLogInfo1.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyCoverageLogInfo1.getRowData(idxPI);

            tTmpEle = new Element("LiabilityCode");
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;


            tTmpEle = new Element("LiabilityExpireDate");
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityStatus");
            tTmpEle.setText(tDataInfo[2]);
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;


            tTmpEle = new Element("InsuredGroupCode");
            tTmpEle.setText(tDataInfo[3]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

//
//            String tPolNo = tDataInfo[4];
//            
////          核保结论
//            tTmpEle = reportUnderWritingLogInfo(tPolNo);
//            if (tTmpEle != null)
//            {
//                tItemEle.addContent(tTmpEle);
//            }
//            tTmpEle = null;
//            
////          赔付比例
//            tTmpEle = reportPolicyAmountLogDTOInfo(tPolNo);
//            if (tTmpEle != null)
//            {
//                tItemEle.addContent(tTmpEle);
//            }
            tTmpEle = null;

        }

        return tRoot;
    }
       

    //得到险种信息
    private SSRS loadPolicyUpcoverageLogInfo(String cContNo)
    {
    	
    	String tContNo = cContNo;
    	
        SSRS tResult = null;
        StringBuffer tStrBSql = new StringBuffer();
        
        
        String gOrI=null;
        if(mDataInfoKey!=null)
        {
        	gOrI=getGOrI(mDataInfoKey);

        }
        
        
//        平台险种类型	COVERAGETYPE
//        平台险种代码	COVERAGECODE
//        公司险种代码	COMCOVERAGECODE
//        险种保额	COVERAGESUMINSURED


        if(mEdorProp!=null&&mEdorProp.equals("I"))
        {  
	        tStrBSql.append(" select ");
	        tStrBSql.append(" (select lmra.RiskPeriod from lmriskapp where riskcode=lcp.riskcode) CoverageType, lcp.RiskCode CoverageCode, ");
	        tStrBSql.append(" lcp.RiskCode ComCoverageCode,");
	        tStrBSql.append(" lcp.amnt CoverageSuminsured , "); 
	        tStrBSql.append(" lcp.polno polno,(select prem from lccont where contno = '" + tContNo + "' ), "); 
	        tStrBSql.append(" '3' CoverageStatus ");
	        tStrBSql.append(" from LCPol lcp ");
	        tStrBSql.append(" inner join LCDuty lcd on lcd.PolNo = lcp.PolNo ");
	        tStrBSql.append(" inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode ");
	        tStrBSql.append(" inner join LMDuty lmd on lmd.DutyCode = lcd.DutyCode ");
	        tStrBSql.append(" where 1 = 1 ");
	        tStrBSql.append(" and lcp.ContNo = '" + tContNo + "' ");
	        
	        tStrBSql.append(" union all  ");
	        
	        tStrBSql.append(" select ");
	        tStrBSql.append(" (select lmra.RiskPeriod from lmriskapp where riskcode=lcp.riskcode) CoverageType, lcp.RiskCode CoverageCode, ");
	        tStrBSql.append(" lcp.RiskCode ComCoverageCode, ");
	        tStrBSql.append(" lcp.amnt CoverageSuminsured , "); 
	        tStrBSql.append(" lcp.polno polno,(select prem from lccont where contno = '" + tContNo + "'), ");
	        tStrBSql.append(" '3' CoverageStatus ");
	        tStrBSql.append(" from LBPol lcp ");
	        tStrBSql.append(" inner join LBDuty lcd on lcd.PolNo = lcp.PolNo ");
	        tStrBSql.append(" inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode ");
	        tStrBSql.append(" inner join LMDuty lmd on lmd.DutyCode = lcd.DutyCode ");
	        tStrBSql.append(" where 1 = 1 ");
	        tStrBSql.append(" and lcp.ContNo = '" + tContNo + "' ");
        }
        else
        {
        	tStrBSql=new StringBuffer();
        	tStrBSql.append(" select ");
	        tStrBSql.append(" (select lmra.RiskPeriod from lmriskapp where riskcode=lcp.riskcode) CoverageType, lcp.RiskCode CoverageCode, ");
	        tStrBSql.append(" lcp.RiskCode ComCoverageCode,");
	        tStrBSql.append(" lcp.amnt CoverageSuminsured , "); 
	        tStrBSql.append(" lcp.grppolno polno,(select prem from lcgrpcont where grpContNo = '" + tContNo + "'), "); 
	        tStrBSql.append(" '3' CoverageStatus ");
	        tStrBSql.append(" from LCgrpPol lcp ");
	        tStrBSql.append(" inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode ");
	        tStrBSql.append(" where 1 = 1 ");
	        tStrBSql.append(" and lcp.grpContNo = '" + tContNo + "' ");	        
	        tStrBSql.append(" union all  ");	        
	        tStrBSql.append(" select ");
	        tStrBSql.append(" (select lmra.RiskPeriod from lmriskapp where riskcode=lcp.riskcode) CoverageType, lcp.RiskCode CoverageCode, ");
	        tStrBSql.append(" lcp.RiskCode ComCoverageCode,");
	        tStrBSql.append(" lcp.amnt CoverageSuminsured  ,");  
	        tStrBSql.append(" lcp.grppolno polno,(select prem from lcgrpcont where grpContNo = '" + tContNo + "'), ");
	        tStrBSql.append(" '3' CoverageStatus ");
	        tStrBSql.append(" from LBgrpPol lcp ");
	        tStrBSql.append(" inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode ");
	        tStrBSql.append(" where 1 = 1 ");
	        tStrBSql.append(" and lcp.grpContNo = '" + tContNo + "' ");
        }

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }
    
    private SSRS loadPolicyCoverageLogInfo(String cContNo)
    {
    	
    	String tContNo = cContNo;
    	
        SSRS tResult = null;
        StringBuffer tStrBSql = new StringBuffer();
        
//        平台险种类型	COVERAGETYPE	VARCHAR2(1)		Y	《公用代码》1.1.9
//        平台险种代码	COVERAGECODE	VARCHAR2(5)		Y	《公用代码》1.1.10
//        公司险种代码	COMCOVERAGECODE	VARCHAR2(60)			
//        公司险种名称	COVERAGENAME	VARCHAR2(200)			
//        平台责任分类	LIABILITYCLASSIFICATION	VARCHAR2(2)		Y	《公用代码》1.1.11
//        责任代码	LIABILITYCODE	VARCHAR2(60)			
//        责任名称	LIABILITYNAME	VARCHAR2(200)			
//        责任开始日期	LIABILITYEFFECTIVEDATE	DATE			
//        责任终止日期	LIABILITYEXPIREDATE	DATE			
//        责任状态	LIABILITYSTATUS	VARCHAR2(2)		Y	《公用代码》1.1.4
//        被保险人属组代码	INSUREDGROUPCODE	VARCHAR2(30)			若上传“承保责任数组”，则该字段不能为空；若上传“特别承保责任数组”，该字段必须为空
//        属组名称	GROUPNAME	VARCHAR2(100)			
//        责任保费	LIABILITYPREMIUM	NUMBER(20,2)	--	契约没有	--哪都没有
//        责任保额	LIMITAMOUNT	NUMBER(20,2)			
//        责任有效保额	EFFECTIVELYAMOUNT	NUMBER(20,2)			保额余额
//        免责期	WAITINGPERIOD	NUMBER(8)			以天为单位计算
//        核保结论数组	UNDERWRITING_ARRAY				详见下面描述
//        赔付比例分段数组	AMOUNT_ARRAY				详见下面描述

        tStrBSql.append(" select ");
        tStrBSql.append(" (select lmra.RiskPeriod from lmriskapp where riskcode=lcp.riskcode) CoverageType, lcp.RiskCode CoverageCode, ");
        tStrBSql.append(" lcp.RiskCode ComCoverageCode, lmra.RiskName CoverageName, ");
        tStrBSql.append(" '99' LiabilityClassification, ");
        tStrBSql.append(" lcd.DutyCode LiabilityCode, lmd.DutyName LiabilityName, ");
        tStrBSql.append(" lcd.GetStartDate LiabilityEffectiveDate, lcd.EndDate LiabilityExpireDate, ");
        tStrBSql.append(" '1' LiabilityStatus, ");
        tStrBSql.append(" lcp.PolNo InsuredGroupCode, '' GroupName, ");
        tStrBSql.append(" lcd.Amnt LimitAmount, '' EffectivelyAmount, ");
        tStrBSql.append(" '180' WaitingPeriod, ");
        tStrBSql.append(" '' PolNo ");
        tStrBSql.append(" from LCPol lcp ");
        tStrBSql.append(" inner join LCDuty lcd on lcd.PolNo = lcp.PolNo ");
        tStrBSql.append(" inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode ");
        tStrBSql.append(" inner join LMDuty lmd on lmd.DutyCode = lcd.DutyCode ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.ContNo = '" + tContNo + "' ");
        tStrBSql.append(" union all  ");
        tStrBSql.append(" select ");
        tStrBSql.append(" (select lmra.RiskPeriod from lmriskapp where riskcode=lcp.riskcode) CoverageType, lcp.RiskCode CoverageCode, ");
        tStrBSql.append(" lcp.RiskCode ComCoverageCode, lmra.RiskName CoverageName, ");
        tStrBSql.append(" '99' LiabilityClassification, ");
        tStrBSql.append(" lcd.DutyCode LiabilityCode, lmd.DutyName LiabilityName, ");
        tStrBSql.append(" lcd.GetStartDate LiabilityEffectiveDate, lcd.EndDate LiabilityExpireDate, ");
        tStrBSql.append(" '1' LiabilityStatus, ");
        tStrBSql.append(" lcp.PolNo InsuredGroupCode, '' GroupName, ");
        tStrBSql.append(" lcd.Amnt LimitAmount, '' EffectivelyAmount, ");
        tStrBSql.append(" '180' WaitingPeriod, ");
        tStrBSql.append(" '' PolNo ");
        tStrBSql.append(" from LBPol lcp ");
        tStrBSql.append(" inner join LBDuty lcd on lcd.PolNo = lcp.PolNo ");
        tStrBSql.append(" inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode ");
        tStrBSql.append(" inner join LMDuty lmd on lmd.DutyCode = lcd.DutyCode ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.ContNo = '" + tContNo + "' ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }
    
    
    //新责任接口
    private SSRS loadPolicyCoverageLogInfo1(String cPolNo)
    {
    	
    	String tPolNo = cPolNo;
    	
        SSRS tResult = null;
        StringBuffer tStrBSql = new StringBuffer();
//        责任代码	LIABILITYCODE	VARCHAR2(60)
//        责任终止日期	LIABILITYEXPIREDATE	DATE
//        责任状态	LIABILITYSTATUS	VARCHAR2(2)
//        被保险人属组代码	INSUREDGROUPCODE	VARCHAR2(30)


        
        tStrBSql.append(" select ");               
        tStrBSql.append(" lcd.DutyCode LiabilityCode,  ");
        tStrBSql.append(" lcd.EndDate LiabilityExpireDate, ");
        tStrBSql.append(" '1' LiabilityStatus, ");        
        tStrBSql.append(" '1' InsuredGroupCode, ");
        tStrBSql.append(" lcp.polno PolNo ");
        tStrBSql.append(" from LCPol lcp ");
        tStrBSql.append(" inner join LCDuty lcd on lcd.PolNo = lcp.PolNo ");
        tStrBSql.append(" inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode ");
        tStrBSql.append(" inner join LMDuty lmd on lmd.DutyCode = lcd.DutyCode ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.PolNo = '" + tPolNo + "' ");
        tStrBSql.append(" union all  ");
        tStrBSql.append(" select ");               
        tStrBSql.append(" lcd.DutyCode LiabilityCode,  ");
        tStrBSql.append(" lcd.EndDate LiabilityExpireDate, ");
        tStrBSql.append(" '1' LiabilityStatus, ");        
        tStrBSql.append(" '1' InsuredGroupCode, ");
        tStrBSql.append(" lcp.polno PolNo ");
        tStrBSql.append(" from LBPol lcp ");
        tStrBSql.append(" inner join LBDuty lcd on lcd.PolNo = lcp.PolNo ");
        tStrBSql.append(" inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode ");
        tStrBSql.append(" inner join LMDuty lmd on lmd.DutyCode = lcd.DutyCode ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.PolNo = '" + tPolNo + "' ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }
 
    //核保信息
    private Element reportUnderWritingLogInfo(String cPolNo)
    {
        Element tRoot = new Element("UnderWritingLogDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.UnderWritingLogDTO");

        Element tTmpEle = null;

        SSRS tResUnderWritingLogInfo = loadUnderWritingLogInfo(cPolNo);

        for (int idxPI = 1; idxPI <= tResUnderWritingLogInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResUnderWritingLogInfo.getRowData(idxPI);

            tTmpEle = new Element("UnderwritingResult");
            tTmpEle.setText(tDataInfo[0]);
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("UnderwritingDate");
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
        }

        return tRoot;
    }
    

    private SSRS loadUnderWritingLogInfo(String cPolNo)
    {
    	
    	String tPolNo = cPolNo;
    	
        SSRS tResult = null;
        StringBuffer tStrBSql = new StringBuffer();
        
//        核保结论	UNDERWRITINGRESULT	VARCHAR2(2)		Y	《公用代码》1.1.21
//        核保结论日期	UNDERWRITINGDATE	DATE			

        tStrBSql.append(" select ");
        tStrBSql.append(" UWFlag UnderwritingResult, UWDate UnderwritingDate ");
        tStrBSql.append(" from LCPol lcp ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.PolNo = '" + tPolNo + "' ");
        tStrBSql.append(" union all  ");
        tStrBSql.append(" select ");
        tStrBSql.append(" UWFlag UnderwritingResult, UWDate UnderwritingDate ");
        tStrBSql.append(" from LBPol lcp ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.PolNo = '" + tPolNo + "' ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }
    

    private Element reportPolicyAmountLogDTOInfo(String cPolNo)
    {
        Element tRoot = new Element("PolicyAmountLogDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.PolicyAmountLogDTO");

        Element tTmpEle = null;

        for (int idxPI = 1; idxPI <= 1; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            tTmpEle = new Element("ClaimRatio");
            tTmpEle.setText(null);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为“NUMBER”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Costend");
            tTmpEle.setText(null);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为“NUMBER”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("Coststart");
            tTmpEle.setText(null);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为“NUMBER”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Deductible");
            tTmpEle.setText(null);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为“NUMBER”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("SectionNo");
            tTmpEle.setText(null);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
        }

        return tRoot;
    }
    
//  团体投保人数组
    private Element reportPolicyGroupPHLogInfo(String cGrpContNo)
    {
        Element tRoot = new Element("PolicyGroupPHLogDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.PolicyGroupPHLogDTO");

        Element tTmpEle = null;

        SSRS tResPolicyGroupPHLogInfo = loadPolicyGroupPHLogInfo(cGrpContNo);

        for (int idxPI = 1; idxPI <= tResPolicyGroupPHLogInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyGroupPHLogInfo.getRowData(idxPI);
            
            tTmpEle = new Element("Name");
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritType");
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tTmpEle.setText(tDataInfo[1]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritCode");
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SocialSecurityNo");
            tTmpEle.setText(tDataInfo[3]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CompanyNature");
            tTmpEle.setText(tDataInfo[4]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("IndustryClassification");
            tTmpEle.setText(tDataInfo[5]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LegalRepresentative");
            tTmpEle.setText(tDataInfo[6]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Contact");
            tTmpEle.setText(tDataInfo[7]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("NumberOfUnits");
            tTmpEle.setText(tDataInfo[8]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
        }

        return tRoot;
    }
    
    private SSRS loadPolicyGroupPHLogInfo(String cGrpContNo)
    {
    	
        SSRS tResult = null;
        StringBuffer tStrBSql = new StringBuffer();
        
//        名称	代码	数据类型	非空	编码	备注
//        单位名称	NAME	VARCHAR2(100)			
//        团体客户证件类别	CRITTYPE	VARCHAR2(2)		Y	《公用代码》1.1.12
//        团体客户证件号码	CRITCODE	VARCHAR2(30)			
//        社保保险登记证号	SOCIAlSECURITYNO	VARCHAR2(50)			
//        单位性质	COMPANYNATURE	VARCHAR2(3)		Y	《公用代码》1.1.13
//        行业分类	INDUSTRYCLASSIFICATION	VARCHAR2(4)		Y	《公用代码》1.1.14
//        法人代表	LEGALREPRESENTATIVE	VARCHAR2(50)			
//        联系人	CONTACT	VARCHAR2(50)			
//        单位总人数	NUMBEROFUNITS	NUMBER(10)			


        tStrBSql.append(" select ");
        tStrBSql.append(" Name Name, '' CritType, ");
        tStrBSql.append(" '' CritCode, '' SocialSecurityNo, ");
        tStrBSql.append(" '' CompanyNature, '' IndustryClassification, '' LegalRepresentative,'' Contact, ");
        tStrBSql.append(" Peoples NumberOfUnits ");
        tStrBSql.append(" from LCGrpAppnt ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and GrpContNo = '" + cGrpContNo + "' ");
        tStrBSql.append(" union all  ");
        tStrBSql.append(" select ");
        tStrBSql.append(" Name Name, '' CritType, ");
        tStrBSql.append(" '' CritCode, '' SocialSecurityNo, ");
        tStrBSql.append(" '' CompanyNature, '' IndustryClassification, '' LegalRepresentative,'' Contact, ");
        tStrBSql.append(" Peoples NumberOfUnits ");
        tStrBSql.append(" from LBGrpAppnt ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and GrpContNo = '" + cGrpContNo + "' ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }
    
//  个人投保人数组
    private Element reportPolicyPersonPHLogInfo(String cContNo)
    {
        Element tRoot = new Element("PolicyPersonPHLogDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.PolicyPersonPHLogDTO");

        Element tTmpEle = null;

        SSRS tResPolicyGroupPHLogInfo = loadPolicyPersonPHLogInfo(cContNo);

        for (int idxPI = 1; idxPI <= tResPolicyGroupPHLogInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyGroupPHLogInfo.getRowData(idxPI);
            
            
            tTmpEle = new Element("Name");
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Gender");
            tTmpEle.setText(tDataInfo[1]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Birthday");
            tTmpEle.addAttribute("CData", "CDate");
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritType");
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tTmpEle.setText(tDataInfo[3]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritCode");
            tTmpEle.setText(tDataInfo[4]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RelationshipWithInsured");
            tTmpEle.setText(tDataInfo[5]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

        }

        return tRoot;
    }
    
    private SSRS loadPolicyPersonPHLogInfo(String cContNo)
    {
    	
        SSRS tResult = null;
        StringBuffer tStrBSql = new StringBuffer();
        
//        名称	代码	数据类型	非空	编码	备注
//        姓名	NAME	VARCHAR2(60)			
//        性别	GENDER	VARCHAR2(1)		Y	《公用代码》1.1.15
//        出生日期	BIRTHDAY	DATE			
//        证件类别	CRITTYPE	VARCHAR2(2)		Y	《公用代码》1.1.16
//        证件号码	CRITCODE	VARCHAR2(30)			
//        与被保险人关系	RELATIONSHIPWITHINSURED	VARCHAR2(2)		Y	《公用代码》1.1.17
			
        tStrBSql.append(" select ");
        tStrBSql.append(" AppntName Name, AppntSex Gender, ");
        tStrBSql.append(" AppntBirthday Birthday, IDType CritType, ");
        tStrBSql.append(" IDNo CritCode, '' RelationshipWithInsured ");
        tStrBSql.append(" from LCAppnt ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and ContNo = '" + cContNo + "' ");
        tStrBSql.append(" union all  ");
        tStrBSql.append(" select ");
        tStrBSql.append(" AppntName Name, AppntSex Gender, ");
        tStrBSql.append(" AppntBirthday Birthday, IDType CritType, ");
        tStrBSql.append(" IDNo CritCode, '' RelationshipWithInsured ");
        tStrBSql.append(" from LBAppnt ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and ContNo = '" + cContNo + "' ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }
    
//  被保人
    private Element reportPolicyInsuredLogInfo(String cContNo)
    {
        Element tRoot = new Element("UPStatusPolicyInsuredDTO");//	被保险人数组中的详细信息INSURED_ARRAY
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.UPStatusPolicyInsuredDTO");
        tRoot.addAttribute("ForceSyncFun", "setUPStatusPolicyInsuredDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInsuredLogInfo = loadPolicyInsuredLogInfo(cContNo);

        for (int idxPI = 1; idxPI <= tResPolicyInsuredLogInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInsuredLogInfo.getRowData(idxPI);
                       
            tTmpEle = new Element("CustomerNo");
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            
            tTmpEle = new Element("OfferStatus");
//            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tTmpEle.setText(tDataInfo[1]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SubEndDate");
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            
          
            String tInsuredNo = tDataInfo[0];
            //被保险人承保险种数组
            tTmpEle = reportPolInsUpCovInfo(cContNo, tInsuredNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

//            //获取受益人数组
//            tTmpEle = reportPolInsBnfInfo(cContNo);
//            if (tTmpEle != null)
//            {
//                tItemEle.addContent(tTmpEle);
//            }
//            tTmpEle = null;
           
            
        }

        return tRoot;
        
    }
  // 从这为粘过来  从契约
    private SSRS loadPolInsBnfInfo(String cContNo)
    {
        SSRS tResult = null;

        StringBuffer tStrBSql = new StringBuffer();

        tStrBSql.append(" select ");
        tStrBSql
                .append(" '' Name, '' Gender, '' Birthday, '' CritType, '' CritCode, '' RelationshipWithInsured, '' BenefitDistribution, '' OrderNo, '' Proportion ");
        tStrBSql.append(" from dual ");
        tStrBSql.append(" where 1 = 2 ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }
    
    private Element reportPolInsBnfInfo(String cContNo)
    {
        Element tRoot = new Element("PolicyBenefitDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.PolicyBenefitDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolInsBnfInfo(cContNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("Name"); //名称   
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Gender"); //性别        
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Birthday"); //出生日期      
            tTmpEle.setText(tDataInfo[2]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritType"); //证件类别      
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritCode"); //证件号码     
            tTmpEle.setText(tDataInfo[4]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RelationshipWithInsured"); //与被保险人关系
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("BenefitDistribution"); //受益分配方式  
            tTmpEle.setText(tDataInfo[6]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("OrderNo"); //顺序号        
            tTmpEle.setText(tDataInfo[7]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Proportion"); //比例值
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
        }

        return tRoot;
    }
    
    private SSRS loadPolInsUpCovInfo(String cContNo, String cInsuredNo)
    {
        SSRS tResult = null;

        StringBuffer tStrBSql = new StringBuffer();

//        平台险种类型	COVERAGETYPE	VARCHAR2(1)
//        平台险种代码	COVERAGECODE	VARCHAR2(5)
//        公司险种代码	COMCOVERAGECODE	VARCHAR2(60)
//        险种保额	COVERAGESUMINSURED	NUMBER(20,2)
//        被保险人承保责任数组	SPECIALCOVERAGE_ARRAY	


        tStrBSql.append(" select ");
        tStrBSql.append(" (select lmra.RiskPeriod from lmriskapp where riskcode=lcp.riskcode) CoverageType, lcp.RiskCode CoverageCode, ");
        tStrBSql.append(" lcp.RiskCode ComCoverageCode, ");
        tStrBSql.append(" lcp.Amnt CoverageSuminsured , ");
        tStrBSql.append(" lcp.PolNo, ");
        tStrBSql.append(" '3' CoverageStatus ");
        tStrBSql.append(" from LCPol lcp ");
        tStrBSql.append(" inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.ContNo = '" + cContNo + "' and lcp.InsuredNo = '" + cInsuredNo + "' ");
        tStrBSql.append(" union all ");
        tStrBSql.append(" select ");
        tStrBSql.append(" (select lmra.RiskPeriod from lmriskapp where riskcode=lcp.riskcode) CoverageType, lcp.RiskCode CoverageCode, ");
        tStrBSql.append(" lcp.RiskCode ComCoverageCode, ");
        tStrBSql.append(" lcp.Amnt CoverageSuminsured , ");
        tStrBSql.append(" lcp.PolNo, ");
        tStrBSql.append(" '3' CoverageStatus ");
        tStrBSql.append(" from LBPol lcp ");
        tStrBSql.append(" inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.ContNo = '" + cContNo + "' and lcp.InsuredNo = '" + cInsuredNo + "' ");
        /*tStrBSql.append(" union all ");
        tStrBSql.append(" select ");
        tStrBSql.append(" '2' CoverageType, lcp.RiskCode CoverageCode, ");
        tStrBSql.append(" lcp.RiskCode ComCoverageCode, ");
        tStrBSql.append(" lcp.Amnt CoverageSuminsured , ");
        tStrBSql.append(" lcp.PolNo, ");
        tStrBSql.append(" '3' CoverageStatus ");
        tStrBSql.append(" from LCGrpPol lcp ");
        tStrBSql.append(" inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.GrpContNo = '" + cContNo + "' and lcp.InsuredNo = '" + cInsuredNo + "' ");
        tStrBSql.append(" union all ");
        tStrBSql.append(" select ");
        tStrBSql.append(" '2' CoverageType, lcp.RiskCode CoverageCode, ");
        tStrBSql.append(" lcp.RiskCode ComCoverageCode, ");
        tStrBSql.append(" lcp.Amnt CoverageSuminsured , ");
        tStrBSql.append(" lcp.PolNo, ");
        tStrBSql.append(" '3' CoverageStatus ");
        tStrBSql.append(" from LCGrpPol lcp ");
        tStrBSql.append(" inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.GrpContNo = '" + cContNo + "' and lcp.InsuredNo = '" + cInsuredNo + "' ");*/

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }
    
    private Element reportPolInsUpCovInfo(String cContNo, String cInsuredNo)
    {
        Element tRoot = new Element("UPStatusSpecialUpCoverageDTO");
//        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.UPStatusSpecialUpCoverageDTO");
        //tRoot.addAttribute("ForceSyncFun", "setUPStatusSpecialUpCoverageDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolInsUpCovInfo(cContNo, cInsuredNo);
        if(tResPolicyInfo!=null)
        {
            tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.UPStatusSpecialUpCoverageDTO");
        }

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("CoverageType"); //平台险种类型
            tTmpEle.setText(tDataInfo[0]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageCode"); //平台险种代码
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ComCoverageCode"); //公司险种代码
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageSuminsured"); //险种保额    
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("CoverageStatus"); //险种状态    
            tTmpEle.setText(tDataInfo[5]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            

            String tPolNo = tDataInfo[4];
            //	被保险人承保责任数组中的详细信息
            tTmpEle = reportPolInsCovInfo(tPolNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
        }

        return tRoot;
    }
    
    private SSRS loadPolInsCovInfo(String cPolNo)
    {
        SSRS tResult = null;

        StringBuffer tStrBSql = new StringBuffer();
//        责任代码	LIABILITYCODE	VARCHAR2(60)
//        责任终止日期	LIABILITYEXPIREDATE	DATE
//        责任状态	LIABILITYSTATUS	VARCHAR2(2)


        tStrBSql.append(" select ");
        tStrBSql.append(" lcd.DutyCode LiabilityCode,  ");
        tStrBSql.append(" lcd.EndDate LiabilityExpireDate, ");
        tStrBSql.append(" lcp.StateFlag LiabilityStatus  ");
        tStrBSql.append(" from LCPol lcp ");
        tStrBSql.append(" inner join LCDuty lcd on lcd.PolNo = lcp.PolNo ");
        tStrBSql.append(" inner join LMDuty lmd on lmd.DutyCode = lcd.DutyCode ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.polno = '" + cPolNo + "' ");
 

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }
    
    private Element reportPolInsCovInfo(String cPolNo)
    {
        Element tRoot = new Element("UPStatusSpecialCoverageDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.UPStatusSpecialCoverageDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = loadPolInsCovInfo(cPolNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("LiabilityCode"); //责任代码    
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityExpireDate"); //责任终止日期
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityStatus"); //责任状态    
            tTmpEle.setText(tDataInfo[2]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;


        }

        return tRoot;
    }
    
   
    
    
    
   
     
    
    
    
    private SSRS loadPolicyInsuredLogInfo(String cContNo)
    {
    	
    	String tContNo = cContNo;
    	
        SSRS tResult = null;
        StringBuffer tStrBSql = new StringBuffer();
        
//        客户编号	CUSTOMERNO
//        要约状态	OFFERSTATUS
//        被保险人责任终止日期	SUBENDDATE
//        被保险人承保险种数组	SPECIALUPCOVERAGE_ARRAY



        tStrBSql.append(" select ");
        tStrBSql.append(" InsuredNo CustomerNo,  ");
        tStrBSql.append(" '3' OfferStatus,  '' SubEndDate ");   //被保险人要约状态暂时 设为终止   
        tStrBSql.append(" from LCInsured ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and ContNo = '" + tContNo + "' ");
        tStrBSql.append(" union all  ");
        tStrBSql.append(" select ");
        tStrBSql.append(" InsuredNo CustomerNo,  ");
        tStrBSql.append(" '3' OfferStatus,  '' SubEndDate "); //被保险人要约状态暂时 设为终止
        tStrBSql.append(" from LBInsured ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and ContNo = '" + tContNo + "' ");        
        tStrBSql.append(" union all  ");
        tStrBSql.append(" select ");
        tStrBSql.append(" InsuredNo CustomerNo,  ");
        tStrBSql.append(" '3' OfferStatus,  '' SubEndDate "); //被保险人要约状态暂时 设为终止
        tStrBSql.append(" from LCInsured ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and GRPContNo = '" + tContNo + "' ");
        tStrBSql.append(" union all  ");
        tStrBSql.append(" select ");
        tStrBSql.append(" InsuredNo CustomerNo,  ");
        tStrBSql.append(" '3' OfferStatus,  '' SubEndDate "); //被保险人要约状态暂时 设为终止
        tStrBSql.append(" from LbInsured ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and GRPContNo = '" + tContNo + "' ");


        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }
    //得到工单号
    private String getEdorNo(String s)
	{
		String[] t=null;
		t=s.split("&");
		return t[0];
	}
    //得到个或团的标志
    private String getGOrI(String s)
	{
		String[] t=null;
		t=s.split("&");
		return t[t.length-1];
	}
    /**
     * 调用前置机服务
     * @return
     */
    private boolean calService()
    {
        Document tDocument = formJDocMessage(mRoot, "10", mDataInfoKey);
        JdomUtil.print(tDocument);

        Document tInStdXmlDoc = tDocument;

        ReqJKXPreService tJKXPreServlet = new ReqJKXPreService();
        Document tReceiveXmlDoc = tJKXPreServlet.callService(tInStdXmlDoc);
        JdomUtil.print(tReceiveXmlDoc);

        return true;
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        System.out.println(szFunc + ": " + szErrMsg);

        CError cError = new CError();
        cError.moduleName = "BQJKXServiceImpBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    
    public static void main(String [] args)
    {
    	/*
    	 * 01	保单状态变更
			02	定期结算状态变更
			03	团单责任状态变更
			04	被保险人状态变更
			05	被保险人的承保责任状态变更
			06	保单险种状态变更
			07	被保险人的承保险种状态变更
    	 */
    	String tDataInfoKey = "I&013963143000001&06";//保单号&保单状态更改类型

    	TransferData tTransferData = new TransferData();
    	tTransferData.setNameAndValue("DataInfoKey", tDataInfoKey);
    	VData tVData = new VData();
    	tVData.add(tTransferData);

    	new BQContStateJKXShangChuan().callJKXService(tVData, "");
    }
}
