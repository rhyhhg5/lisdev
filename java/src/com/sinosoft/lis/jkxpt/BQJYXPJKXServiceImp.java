/**
 * 2010-11-18
 */
package com.sinosoft.lis.jkxpt;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.jkxpt.JKXService;
import com.sinosoft.lis.jkxpt.ReqJKXPreService;
import com.sinosoft.lis.taskservice.DataTrackBL;
import com.sinosoft.lis.yibaotong.JdomUtil;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class BQJYXPJKXServiceImp extends JKXService
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private TransferData mTransferData = null;

    /** 数据操作字符串 */
    private String mOperate = "";

    private String mDataInfoKey = "";
    
    private String mEdorNo = ""; //保全受理号
    
    private String mEdorType = ""; //保全项目类型
    
    private String mEdorProp = ""; //个团标志 ：I-个单，G-团单

    private Element mRoot = null;

    private String mContType = null;

    public boolean callJKXService(VData cInputData, String cOperate)
    {
        // 获取入口参数 
        if (!getInputData(cInputData, cOperate))
        {
            return false;
        }
        // --------------------
        
        // 取数逻辑处理        
        //        mRoot = dealBusiXml(mPolicyNo);
        mRoot = reportBusiXml(mDataInfoKey);
        // --------------------

        // 调用前置机
        if (!calService())
        {
            return false;
        }
        // --------------------

        return true;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mDataInfoKey = (String) mTransferData.getValueByName("DataInfoKey");
        if (mDataInfoKey == null || mDataInfoKey.equals(""))
        {
            buildError("getInputData", "获取保全参数信息失败。");
            return false;
        }
        
    	String [] tEdorInfo = mDataInfoKey.split("&");
		mEdorNo = tEdorInfo[0];
		mEdorType = tEdorInfo[1];
		mEdorProp = tEdorInfo[2];
		
		if (mEdorNo == null || mEdorNo.equals(""))
        {
            buildError("getInputData", "获取保全受理号EdorNo信息失败。");
            return false;
        }
		
		if (mEdorType == null || mEdorType.equals(""))
        {
            buildError("getInputData", "获取保全类型EdorType信息失败。");
            return false;
        }
		
		if (mEdorProp == null || mEdorProp.equals(""))
        {
            buildError("getInputData", "获取保全个团标志EdorProp信息失败。");
            return false;
        }

        return true;
    }

    private Element reportBusiXml(String cDataInfoKey)
    {
    	 Element tRoot = new Element("AccidentPolicyLogDTO");
         tRoot.addAttribute("DTOClass", "com.ebao.accident.insurance.dto.AccidentPolicyLogDTO");

         Element tTmpEle = null;
         
         //个单全单退保信息提取
         if(mEdorType.equals("CT")||mEdorType.equals("WT"))
         {
            //保单详细信息
             SSRS tResPolicyLogInfo = loadlccontJYInfo(cDataInfoKey);
             for (int idxPI = 1; idxPI <= tResPolicyLogInfo.MaxRow; idxPI++)
             {
                 Element tItemEle = new Element("Item");
                 tRoot.addContent(tItemEle);

                 String[] tDataInfo = tResPolicyLogInfo.getRowData(idxPI);
                 
                 tTmpEle = new Element("PolicyNo");
                 tTmpEle.setText(tDataInfo[0]);
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("RenewalTimes");
                 tTmpEle.setText(tDataInfo[1]);
                 tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为"NUMBER"的字段，请增加描述
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("EndorsementType");
                 tTmpEle.setText(tDataInfo[2]);
//                 tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为进行业务转换，即备注为《公用代码》”的字段
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("EndorsementNo");
                 tTmpEle.setText(tDataInfo[3]);
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("EndorsementApplicationDate");
                 tTmpEle.setText(tDataInfo[4]);
                 tTmpEle.addAttribute("CData", "CDate");//文档中类型需为"Date"的字段，请增加描述
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("EffectiveDate");
                 tTmpEle.setText(tDataInfo[5]);
                 tTmpEle.addAttribute("CData", "CDate");//文档中类型需为"Date"的字段，请增加描述
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;
             }
         }
        //3.1.1	保单资料变更信息提取 
         else if(mEdorType.equals("BC")||mEdorType.equals("CM")||mEdorType.equals("CC")||mEdorType.equals("TB")||mEdorType.equals("AD"))
         {
//         	保单详细信息
             SSRS tResPolicyLogInfo = loadlccontZLBGInfo(cDataInfoKey);
             for (int idxPI = 1; idxPI <= tResPolicyLogInfo.MaxRow; idxPI++)
             {
                 Element tItemEle = new Element("Item");
                 tRoot.addContent(tItemEle);

                 String[] tDataInfo = tResPolicyLogInfo.getRowData(idxPI);
                 
                 tTmpEle = new Element("PolicyNo");
                 tTmpEle.setText(tDataInfo[0]);
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("RenewalTimes");
                 tTmpEle.setText(tDataInfo[1]);
                 tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为"NUMBER"的字段，请增加描述
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("EndorsementType");
                 tTmpEle.setText(tDataInfo[2]);
//                 tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为进行业务转换，即备注为《公用代码》”的字段
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("EndorsementNo");
                 tTmpEle.setText(tDataInfo[3]);
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("EndorsementApplicationDate");
                 tTmpEle.setText(tDataInfo[4]);
                 tTmpEle.addAttribute("CData", "CDate");//文档中类型需为"Date"的字段，请增加描述
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("EffectiveDate");
                 tTmpEle.setText(tDataInfo[5]);
                 tTmpEle.addAttribute("CData", "CDate");//文档中类型需为"Date"的字段，请增加描述
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;
                 
                 tTmpEle = new Element("ApplicationFormNo");
                 tTmpEle.setText(tDataInfo[6]);
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("ApplicationDate");
                 tTmpEle.setText(tDataInfo[7]);
                 tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("PolicyStartDate");
                 tTmpEle.setText(tDataInfo[8]);
                 tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("PolicyEndDate");
                 tTmpEle.setText(tDataInfo[9]);
                 tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("RenewalMethod");
                 tTmpEle.setText(tDataInfo[10]);
//                 tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为进行业务转换，即备注为《公用代码》”的字段
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("Sales");
                 tTmpEle.setText(tDataInfo[11]);
//                 tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为进行业务转换，即备注为《公用代码》”的字段
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("SalesChannelCode");
                 tTmpEle.setText(tDataInfo[12]);
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("SalesChannelName");
                 tTmpEle.setText(tDataInfo[13]);
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("ProductName");
                 tTmpEle.setText(tDataInfo[14]);
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("Premium");
                 tTmpEle.setText(tDataInfo[15]);
                 tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为"NUMBER"的字段，请增加描述
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("SpecialRemark");
                 tTmpEle.setText(tDataInfo[16]);
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 String tContNo = tDataInfo[0];
                 
                 //个人投保人数组详细信息
                 tTmpEle = baoDanXiaTouBaoRenInfo(tContNo);
     	   	     if (tTmpEle != null)
     	   	     {
     	   	        tItemEle.addContent(tTmpEle);
     	   	     }
     	   	     tTmpEle = null;
     	   	     
     	   	     
                 //个人被保人数组详细信息
                 tTmpEle = baoDanXiaBeiBaoRenInfo(tContNo);
     	   	     if (tTmpEle != null)
     	   	     {
     	   	        tItemEle.addContent(tTmpEle);
     	   	     }
     	   	     tTmpEle = null;
     	   	     
            
             }
         }
         
    	
    	return tRoot;
    }
    //个人被保人数组详细信息
    private Element baoDanXiaBeiBaoRenInfo(String cContNo)
    {
        Element tRoot = new Element("AccidentPolicyInsuredLogDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.accident.insurance.dto.AccidentPolicyInsuredLogDTO");

        Element tTmpEle = null;

        SSRS tResPolicyUpcoverageLogInfo = baoDanXiaBeiBaoRenInfoSQL(cContNo);

        for (int idxPI = 1; idxPI <= tResPolicyUpcoverageLogInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyUpcoverageLogInfo.getRowData(idxPI);

            tTmpEle = new Element("CustomerNo");
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Name");
            tTmpEle.setText(tDataInfo[1]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Gender");
            tTmpEle.setText(tDataInfo[2]);
//            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Birthday");
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为"Date"的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritType");
            tTmpEle.setText(tDataInfo[4]);
//            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("CritCode");
            tTmpEle.setText(tDataInfo[5]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Mobile");
            tTmpEle.setText(tDataInfo[6]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("OfferStatus");
            tTmpEle.setText(tDataInfo[7]);
//            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SubStartDate");
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SubEndDate");
            tTmpEle.setText(tDataInfo[9]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("AppointedBenefit");
            tTmpEle.setText(tDataInfo[10]);
//            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            String insuredNo=tDataInfo[0];
            
            //被保人险种数组
            tTmpEle = beiBaoRenXiaoZhongShuZu(insuredNo);
 	        if (tTmpEle != null)
 	        {
 	              tItemEle.addContent(tTmpEle);
 	        }
 	        tTmpEle = null;
            
            
            

        }

        return tRoot;
    }
    
    private Element beiBaoRenXiaoZhongShuZu(String insuredNo)
    {
        Element tRoot = new Element("AccidentPolicyUpcoverageLogDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.accident.insurance.dto.AccidentPolicyUpcoverageLogDTO");

        Element tTmpEle = null;

        SSRS tResUnderWritingLogInfo = beiBaoRenXiaoZhongShuZuSQL(insuredNo);

        for (int idxPI = 1; idxPI <= tResUnderWritingLogInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResUnderWritingLogInfo.getRowData(idxPI);
            
            tTmpEle = new Element("CoverageCode");
            tTmpEle.setText(tDataInfo[0]);
//            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ComCoverageCode");
            tTmpEle.setText(tDataInfo[1]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageName");
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageEffectiveDate");
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageExpireDate");
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("MainAttachedFlag");
            tTmpEle.setText(tDataInfo[5]);
//            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoveragePremium");
            tTmpEle.setText(tDataInfo[6]);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为"NUMBER"的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("CoverageSuminsured");
            tTmpEle.setText(tDataInfo[7]);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为"NUMBER"的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PaymentMethod");
            tTmpEle.setText(tDataInfo[8]);
//            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PaymentYears");
            tTmpEle.setText(tDataInfo[9]);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为"NUMBER"的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("SpecificBusiness");
            tTmpEle.setText(tDataInfo[10]);
//            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SpecificBusinessCode");
            tTmpEle.setText(tDataInfo[11]);
//            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            //承保责任数组
            String polNo=tDataInfo[12];
            tTmpEle = chengBaoZeRenShuZu(polNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
           

            
        }

        return tRoot;
    }
    //承保责任数组
    
    private Element chengBaoZeRenShuZu(String cPolNo)
    {
        Element tRoot = new Element("AccidentPolicyCoverageLogDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.accident.insurance.dto.AccidentPolicyCoverageLogDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = chengBaoZeRenShuZuSQL(cPolNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("LiabilityCode"); //平台责任分类
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityName"); //责任代码    
            tTmpEle.setText(tDataInfo[1]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityEffectiveDate"); //责任名称    
            tTmpEle.setText(tDataInfo[2]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("LiabilityExpireDate");
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityStatus");
            tTmpEle.setText(tDataInfo[4]);
//            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityPremium");
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为"NUMBER"的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LimitAmount");
            tTmpEle.setText(tDataInfo[6]);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为"NUMBER"的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("WaitingPeriod");
            tTmpEle.setText(tDataInfo[7]);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为"NUMBER"的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Remark");
            tTmpEle.setText(tDataInfo[8]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;


        }

        return tRoot;
    }
    private SSRS chengBaoZeRenShuZuSQL(String cPolNo)
    {
        SSRS tResult = null;
//        责任代码	LIABILITYCODE	VARCHAR2(60)	Y		
//        责任名称	LIABILITYNAME	VARCHAR2(200)	Y		
//        责任开始日期	LIABILITYEFFECTIVEDATE	DATETIME	Y		
//        责任终止日期	LIABILITYEXPIREDATE	DATETIME	Y		
//        责任状态	LIABILITYSTATUS	VARCHAR2(2)	Y	Y	《公用代码》1.1.13
//        责任保费	LIABILITYPREMIUM	NUMBER(20,2)			
//        责任保额	LIMITAMOUNT	NUMBER(20,2)	S		与备注字段不能同时为空
//        免责期	WAITINGPERIOD	NUMBER(8)			
//        备注	REMARK	VARCHAR2(600)	S		与责任保额字段不能同时为空

        StringBuffer tStrBSql = new StringBuffer();
        tStrBSql.append(" select ");
        tStrBSql.append(" lcd.DutyCode liabilityCode,");//责任代码
        tStrBSql.append(" lmd.DutyName liabilityName,");//责任名称
        tStrBSql.append(" lcd.GetStartDate liabilityEffectiveDate,");//责任开始日期	
        tStrBSql.append(" lcd.EndDate liabilityExpireDate,");//责任终止日期
        tStrBSql.append(" lcp.StateFlag liabilityStatus,");//责任状态
        tStrBSql.append(" lcd.Prem  liabilityPremium,");//责任保费
        tStrBSql.append(" lcd.Amnt  limitAmount,");//责任保额
        tStrBSql.append(" '0' waitingPeriod,");//免责期
        tStrBSql.append(" '' remark");//备注
        tStrBSql.append(" from LCPol lcp ");
        tStrBSql.append(" inner join LCDuty lcd on lcd.PolNo = lcp.PolNo ");
        tStrBSql.append(" inner join LMDuty lmd on lmd.DutyCode = lcd.DutyCode ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.polno = '" + cPolNo + "' ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }
    
    
    private SSRS beiBaoRenXiaoZhongShuZuSQL(String insuredNo)
    {
        SSRS tResult = null;
        StringBuffer tStrBSql = new StringBuffer();
        
//        平台险种代码	COVERAGECODE	VARCHAR2(5)	Y	Y	《公用代码》1.1.15
//        公司险种代码	COMCOVERAGECODE	VARCHAR2(60)	Y		
//        公司险种名称	COVERAGENAME	VARCHAR2(200)	Y		
//        险种起保日期	COVERAGEEFFECTIVEDATE	DATETIME	Y		
//        险种满期日期	COVERAGEEXPIREDATE	DATETIME	Y		
//        主附险性质	MAINATTACHEDFLAG	VARCHAR2(1)	Y	Y	《公用代码》1.1.11
//        险种保费	COVERAGEPREMIUM	NUMBER(20,2)	Y		
//        险种保额	COVERAGESUMINSURED	NUMBER(20,2)			
//        缴费方式	PAYMENTMETHOD	VARCHAR2(2)		Y	《公用代码》1.1.14
//        缴费年限	PAYMENTYEARS	NUMBER(4)			一年期及一年期以下的业务缴费年限为1
//        特殊业务标识	SPECIFICBUSINESS	VARCHAR2(1)	Y	Y	《公用代码》1.1.7
//        特殊业务代码	SPECIFICBUSINESSCODE	VARCHAR2(5)	S	Y	《公用代码》1.1.6
//        如果特殊业务标识为1(是)，该字段必填
//        承保责任数组	COVERAGE_ARRAY		Y		详见下面描述
			

        tStrBSql.append(" select ");//
        tStrBSql.append(" 'A01' coverageCode,");//平台险种代码
        tStrBSql.append(" riskcode comCoverageCode,");//公司险种代码
        tStrBSql.append(" (select riskname from lmriskapp where riskcode=a.riskcode ) coverageName,");//公司险种名称
        tStrBSql.append("  cvalidate coverageEffectiveDate,");//险种起保日期
        tStrBSql.append("  (select cinvalidate from lccont where contno=a.contno) coverageExpireDate,");//险种满期日期
        tStrBSql.append("  (case when exists (select 1 from lmriskapp where subriskflag='S' and riskcode=a.riskcode ) then '2' else '1' end ) mainAttachedFlag,");//主附险性质
        tStrBSql.append("  prem coveragePremium,");//险种保费
        tStrBSql.append("  amnt coverageSuminsured,");//险种保额
        tStrBSql.append("  paymode paymentMethod,");//缴费方式
        tStrBSql.append("  payyears paymentYears,");//缴费年限
        tStrBSql.append("  '0' specificBusiness,");//特殊业务标识
        tStrBSql.append("  '' specificBusinessCode,");//特殊业务代码
        tStrBSql.append("  polNo polNo ");//险种号     后边用
        tStrBSql.append(" from lcpol a where 1=1 ");//
        tStrBSql.append(" and insuredNo='");//
        tStrBSql.append(insuredNo+"'");


        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }
    
    
    
    private SSRS baoDanXiaBeiBaoRenInfoSQL(String cContNo)
    {
    	
        SSRS tResult = null;
        StringBuffer tStrBSql = new StringBuffer();
//        客户编号	CUSTOMERNO	VARCHAR2(60)	Y		该保单中，此被保险人的唯一标识，保险公司自行约定
//        姓名	NAME	VARCHAR2(60)	Y		
//        性别	GENDER	VARCHAR2(1)		Y	《公用代码》1.1.3
//        出生日期	BIRTHDAY	DATE			
//        被保险人证件类别	CRITTYPE	VARCHAR2(2)	Y	Y	《公用代码》1.1.4
//        被保险人证件号码	CRITCODE	VARCHAR2(30)	Y		
//        手机号码	MOBILE	VARCHAR2 (20)			
//        要约状态	OFFERSTATUS	VARCHAR2(1)	Y	Y	《公用代码》1.1.13
//        被保险人责任开始日期	SUBSTARTDATE	DATETIME			
//        被保险人责任终止日期	SUBENDDATE	DATETIME			
//        是否指定受益人	APPOINTEDBENEFIT	VARCHAR2(1)	Y	Y	《公用代码》1.1.7
//        受益人数组	BENEFIT_ARRAY		S		详见下面描述
//        如果是否指定受益人为1(是)，则该数组必填
//        承保险种数组	UPCOVERAGE_ARRAY		Y		详见下面描述


        tStrBSql.append(" select ");
        tStrBSql.append(" insuredNo customerNo,");// 客户编号
        tStrBSql.append(" name name,");//姓名
        tStrBSql.append(" sex gender,");//性别
        tStrBSql.append(" birthday  birthday,");//出生日期
//        tStrBSql.append(" idtype critType,");//被保险人证件类别
        tStrBSql.append(" '01' critType,");//被保险人证件类别
        tStrBSql.append(" idno critCode,");//被保险人证件号码
        tStrBSql.append(" '' mobile,");//手机号码
        tStrBSql.append(" '1' offerStatus,");//要约状态
        tStrBSql.append(" (select cvalidate from lccont where contno=a.contno) subStartDate,");//被保险人责任开始日期
        tStrBSql.append(" (select cinvalidate from lccont where contno=a.contno) subEndDate,");//被保险人责任终止日期
        tStrBSql.append(" '0' appointedBenefit");//是否指定受益人

        tStrBSql.append("  from lcinsured a where contno='");
        tStrBSql.append(cContNo+"'");
       
        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }
    //个人投保人数组详细信息
    private Element baoDanXiaTouBaoRenInfo(String cContNo)
    {
        Element tRoot = new Element("AccidentPolicyPersonphLogDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.accident.insurance.dto.AccidentPolicyPersonphLogDTO");

        Element tTmpEle = null;

        SSRS tResPolicyUpcoverageLogInfo = baoDanXiaTouBaoRenInfoSQL(cContNo);

        for (int idxPI = 1; idxPI <= tResPolicyUpcoverageLogInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyUpcoverageLogInfo.getRowData(idxPI);

            tTmpEle = new Element("Name");
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Gender");
//            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tTmpEle.setText(tDataInfo[1]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Birthday");
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为"Date"的字段，请增加描述
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritType");
            tTmpEle.setText(tDataInfo[3]);
//            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritCode");
            tTmpEle.setText(tDataInfo[4]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RelationshipWithInsured");
            tTmpEle.setText(tDataInfo[5]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Mobile");
            tTmpEle.setText(tDataInfo[6]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

        }

        return tRoot;
    }
    private SSRS baoDanXiaTouBaoRenInfoSQL(String cContNo)
    {
    	
        SSRS tResult = null;
        StringBuffer tStrBSql = new StringBuffer();
//        姓名	NAME	VARCHAR2(120)	Y		
//        性别	GENDER	VARCHAR2(1)		Y	《公用代码》1.1.3
//        出生日期	BIRTHDAY	DATE			
//        证件类别	CRITTYPE	VARCHAR2(2)	Y	Y	《公用代码》1.1.4
//        证件号码	CRITCODE	VARCHAR2(30)	Y		证件类别为01(身份证时)，证件号码长度必须为15或18位。
//        与被保险人关系	RELATIONSHIPWITHINSURED	VARCHAR2(2)		Y	《公用代码》1.1.5
//        手机号码	MOBILE	VARCHAR2 (20)			供短信发送

        tStrBSql.append(" select ");//
        tStrBSql.append(" appntname name,");//姓名
        tStrBSql.append(" appntsex gender,");//性别
        tStrBSql.append(" appntbirthday birthday,");//出生日期
//        tStrBSql.append(" idtype critType,");//证件类别
        tStrBSql.append(" '01' critType,");//证件类别
        tStrBSql.append(" idno critCode,");//证件号码
        tStrBSql.append(" (SELECT relationtoappnt FROM LCINSURED WHERE  RELATIONTOMAININSURED='00' and contno=a.contno) relationshipWithInsured,");//与被保险人关系
        tStrBSql.append(" (select c.Mobile from lcaddress c where c.CustomerNo=a.AppntNo and c.AddressNo =a.AddressNo ) mobile ");//手机号码
        tStrBSql.append("  from lcappnt a where contno='");
        tStrBSql.append(cContNo+"'");

        
  


        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }
    
    
    //个单保单资料变更的保单详细信息
    private SSRS loadlccontZLBGInfo(String cDataInfoKey)
    {
        SSRS tResult = null;
        StringBuffer tStrBSql = new StringBuffer();
        String tStrSql = tStrBSql.toString();
//        保单号码	POLICYNO	VARCHAR2(30)	Y		
//        续保次数	RENEWALTIMES	NUMBER(3)	Y		
//        保全类别	ENDORSEMENTTYPE	VARCHAR2(2)	Y	Y	《公用代码》1.1.12
//        保全批单号	ENDORSEMENTNO	VARCHAR2(30)	Y		
//        保全申请日期	ENDORSEMENTAPPLICATIONDATE	DATETIME	Y		
//        保全生效日期	EFFECTIVEDATE	DATETIME	Y		
//        投保单号/卡号	APPLICATIONFORMNO	VARCHAR2(30)	Y		激活卡号
//        投保申请日期	APPLICATIONDATE	DATE	Y		年月日
//        保单起保时间	POLICYSTARTDATE	DATETIME	Y		
//        保单满期时间	POLICYENDDATE	DATETIME	Y		
//        投保方式	RENEWALMETHOD	VARCHAR2(2)	Y	Y	《公用代码》1.1.16
//        销售渠道	SALES	VARCHAR2(2)	Y	Y	《公用代码》1.1.8
//        销售机构代码	SALESCHANNELCODE	VARCHAR2(20)	Y		保险公司内部销售机构代码，原则上要求使用保监局提供的A码
//        销售机构名称	SALESCHANNELNAME	VARCHAR2(100)	Y		保险公司内部销售机构名称，直销业务填写保险公司名称
//        产品名称	PRODUCTNAME	VARCHAR2(500)	Y		
//        保费	PREMIUM	NUMBER(20,2)	Y		
//        特别约定	SPECIALREMARK	VARCHAR2(900)			
//        个人投保人数组	PERSONPH_ARRAY		Y		
//        被保险人数组	INSURED_ARRAY		Y		
		
		
        tStrBSql.append(" select ");
        tStrBSql.append(" lp.contno policyNo,");//保单号码
        tStrBSql.append(" (select (case when renewcount is null then 0 else renewcount end) from lppol where edorno=lp.edorno and contno=lp.contno fetch first 1 rows only ) renewalTimes,");//续保次数
        tStrBSql.append(" '01'  endorsementType,");//保全类别
//        tStrBSql.append(" LP.EdorType  endorsementType,");//保全类别
        tStrBSql.append(" lp.edorno endorsementNo,");//保全批单号
        tStrBSql.append(" (select edorappdate from lpedorapp where edoracceptno=lp.edorno) endorsementApplicationDate,");//保全申请日期
        tStrBSql.append(" lp.edorvalidate effectiveDate ,");//保全生效日期	
        tStrBSql.append(" lp.contno applicationFormNo,");//激活卡号
        tStrBSql.append(" (select polapplydate from lccont where contno=lp.contno union select polapplydate from lbcont where contno=lp.contno ) applicationDate,");//投保申请日期
        tStrBSql.append(" (select cvalidate from lccont where contno=lp.contno union select cvalidate from lbcont where contno=lp.contno) policyStartDate,");//保单起保时间
        tStrBSql.append(" (select cinvalidate from lccont where contno=lp.contno union select cinvalidate from lbcont where contno=lp.contno ) policyEndDate,");//保单满期时间	
        tStrBSql.append(" '01' renewalMethod,");//投保方式
//        tStrBSql.append(" (select salechnl from lccont where contno=lp.contno union select salechnl from lbcont where contno=lp.contno ) sales,");//销售渠道
        tStrBSql.append(" '01' sales,");//销售渠道
        tStrBSql.append(" (select managecom  from lccont where contno=lp.contno union select managecom from lbcont where contno=lp.contno ) salesChannelCode, ");//销售机构代码
        tStrBSql.append(" (select (select name from ldcom where comcode = lccont.managecom) from lccont where contno=lp.contno union select (select name from ldcom where comcode = lbcont.managecom) from lbcont where contno=lp.contno ) salesChannelName,");//销售机构名称
        tStrBSql.append(" '产品名称' productName,");//产品名称
        tStrBSql.append(" (select prem from lccont where contno=lp.contno  union select prem from lbcont where contno=lp.contno ) premium ,");//保费
        tStrBSql.append(" '' specialRemark ");//特别约定

        tStrBSql.append(" from lpedoritem lp where 1=1 and edortype in ('CM','AE','CC','BC','TB','AD') ");
        tStrBSql.append(" and edorNo='");
        tStrBSql.append(mEdorNo+"' with ur ");
      
        tResult = new ExeSQL().execSQL(tStrBSql.toString());

        return tResult;
    }
    //个单全单退保保单详细信息
    private SSRS loadlccontJYInfo(String cDataInfoKey)
    {
        SSRS tResult = null;
        StringBuffer tStrBSql = new StringBuffer();
        String tStrSql = tStrBSql.toString();
//        名称	代码	数据类型	非空	编码	备注
//        保单号码	POLICYNO	VARCHAR2(30)	Y		
//        续保次数	RENEWALTIMES	NUMBER(3)	Y		
//        保全类别	ENDORSEMENTTYPE	VARCHAR2(2)	Y	Y	《公用代码》1.1.12
//        保全批单号	ENDORSEMENTNO	VARCHAR2(30)	Y		
//        保全申请日期	ENDORSEMENTAPPLICATIONDATE	DATETIME	Y		
//        保全生效日期	EFFECTIVEDATE	DATETIME	Y		
		
        tStrBSql.append(" select ");
        tStrBSql.append(" lp.contno policyNo,");//保单号码
        tStrBSql.append(" (select renewcount from lbpol where edorno=lp.edorno and contno=lp.contno fetch first 1 rows only ) renewalTimes,");//续保次数
        tStrBSql.append(" '02'  endorsementType,");//保全类别
//        tStrBSql.append(" lp.edortype  endorsementType,");//保全类别
        tStrBSql.append(" lp.edorno endorsementNo,");//保全批单号
        tStrBSql.append(" (select edorappdate from lpedorapp where edoracceptno=lp.edorno) endorsementApplicationDate,");//保全申请日期
        tStrBSql.append(" lp.edorvalidate effectiveDate ");//保全生效日期	
        tStrBSql.append(" from lpedoritem lp where 1=1 and edortype in ('CT','WT') ");
        tStrBSql.append(" and edorNo='");
        tStrBSql.append(mEdorNo+"' with ur ");
      
        tResult = new ExeSQL().execSQL(tStrBSql.toString());

        return tResult;
    }

   
    /**
     * 调用前置机服务
     * @return
     */
    private boolean calService()
    {
        Document tDocument = formJDocMessage(mRoot, "Acc03", mDataInfoKey);
        JdomUtil.print(tDocument);

        Document tInStdXmlDoc = tDocument;

        ReqJKXPreService tJKXPreServlet = new ReqJKXPreService();
        String re = tJKXPreServlet.callService(tInStdXmlDoc);
//        JdomUtil.print(tReceiveXmlDoc);

        try
	        {
    		 DataTrackBL mDataTrackBL=new DataTrackBL();
    		 if(!mDataTrackBL.submitData(mDataInfoKey, "Acc03", re)){
    			 System.out.println("保全个单报送插入报送轨迹表出错");
    		 }
	        }
	        catch (Exception ex)
	        {
	            // @@错误处理
	            CError.buildErr(this,"保全个单报送插入报送轨迹表出错。");
	            
	        }               

        return true;
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        System.out.println(szFunc + ": " + szErrMsg);

        CError cError = new CError();
        cError.moduleName = "BQJYXPJKXServiceImp";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    
    public static void main(String [] args)
    {
//    	String tDataInfoKey = "20110805000007&CT&I";//个单CT--
//    	String tDataInfoKey = "20110106000003&CM&I";//个单CM--
//    	String tDataInfoKey = "20100126000141&CC&I";//个单CC--
//    	String tDataInfoKey = "20080403000007&BC&I";//个单BC--	  	
//    	String tDataInfoKey = "20110801000005&TB&I";//个单TB--   	        	
//    	String tDataInfoKey = "20110822000013&WT&I";//个单WT--
    	
        //000920923000001   	
//    	String tDataInfoKey = "20110831000008&CC&I";//个单CC-- 000920923000001 成功
//    	String tDataInfoKey = "20110831000009&CM&I";//个单CM-- 000920923000001 成功
//    	String tDataInfoKey = "20110831000010&BC&I";//个单BC-- 000920923000001 成功
//    	String tDataInfoKey = "20110831000011&CT&I";//个单CT-- 000920923000001 成功
       //004525843000001   
//    	String tDataInfoKey = "20110831000012&TB&I";//个单TB-- 004525843000001 成功
//    	String tDataInfoKey = "20110831000020&WT&I";//个单WT
    	
        //001016262000001
//    	String tDataInfoKey = "20110908000010&AD&I";//个单AD-001016262000001 成功
  
    	//001017078000001
    	String tDataInfoKey = "20110928000001&CT&I";//个单CT-001017078000001 成功
    	
    	TransferData tTransferData = new TransferData();
    	tTransferData.setNameAndValue("DataInfoKey", tDataInfoKey);
    	VData tVData = new VData();
    	tVData.add(tTransferData);

    	new BQJYXPJKXServiceImp().callJKXService(tVData, "");
    }
}
