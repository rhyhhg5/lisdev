package com.sinosoft.lis.jkxpt;

import org.jdom.Document;
import org.jdom.Element;

//import com.sinosoft.lis.yibaotong.JdomUtil;
//import com.sinosoft.utility.ExeSQL;
//import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * 健康险服务调用超类，子类需要继承此类
 * @author filon51
 *
 */
public class JKXService
{

    /**
     * 需要子类继承实现
     * @return
     */
    public boolean callJKXService(VData cInputData, String cOperate)
    {
        //        System.out.println("您调用的是父类方法!");
        //        //1 准备业务数据，将不同节点元素信息内容放入二维数组SSRS中。
        //
        //        //2 通过SSRS数组，构建XML JDOM
        //
        //        QYXmlTag tQYXmlTag = new QYXmlTag();
        //        //		for (int i = 0; i < tQYXmlTag.PolicyNode.length; i++) {
        //        //			System.out.println(tQYXmlTag.PolicyNode[i]);
        //        //		}
        //
        //        //3 重新组织XML数据结构，构建消息体
        //        //		QYXmlTag tQYXmlTag = new QYXmlTag();
        //        String[] tData = new String[tQYXmlTag.PolicyNode.length];
        //        for (int i = 1; i <= tQYXmlTag.PolicyNode.length; i++)
        //        {
        //            tData[i - 1] = "ElemContent" + i;
        //        }
        //
        //        JKXService tJKXService = new JKXService();
        //        Element out1 = tJKXService.formJElem(tData, tQYXmlTag.PolicyNode, tQYXmlTag.policy);
        //        //		JdomUtil.print(out1);
        //
        //        ExeSQL tExeSQL = new ExeSQL();
        //        String tSql = "select '01','02','03','04','05','06','07','08','09','010','011','012','013','014','015','016','017','018','019' from dual "
        //                + " union "
        //                + " select '11','12','13','14','15','16','17','18','19','110','111','112','113','114','115','116','117','118','119' from dual";
        //        SSRS tSSRS = tExeSQL.execSQL(tSql);
        //        System.out.println(tQYXmlTag.policyInsuredNode.length);
        //        Element out2 = tJKXService.formJElem(tSSRS, tQYXmlTag.PolicyInsuredList, tQYXmlTag.policyInsuredNode,
        //                tQYXmlTag.PolicyInsured);
        //        //		JdomUtil.print(out2);
        //
        //        Element root = out1;
        //        root.addContent(out2);
        //        //		root.addChild(out2);
        //
        //        Document tDocument = formJDocMessage(root, "01", "TransactionNo_123232XXX");
        //        JdomUtil.print(tDocument);
        //
        //        //4 调用前置机SERVLET
        //        Document tInStdXmlDoc = tDocument;
        //
        //        ReqJKXPreService tJKXPreServlet = new ReqJKXPreService();
        //        Document tReceiveXmlDoc = tJKXPreServlet.callService(tInStdXmlDoc);
        //        JdomUtil.print(tReceiveXmlDoc);

        return false;
    }

    /**
     * 从一维数组数据生成JDom Element
     * @param pData 原始数据
     * @param pEleNode 元素结点名数组
     * @param rootElement 根节点元素名
     * @return
     */
    //    public Element formJElem(String[] pData, String[] pEleNode, String rootElement)
    //    {
    //        if (rootElement == null || rootElement.equals("") || pData == null || pEleNode == null)
    //        {
    //            return null;
    //        }
    //        if (pData.length != pEleNode.length)
    //        {
    //            System.err.println("数据个数与元素结点个数不一致！");
    //            return null;
    //        }
    //
    //        Element root = new Element(rootElement);
    //        root.setText("");
    //
    //        for (int i = 0; i < pData.length; i++)
    //        {
    //            Element tElement = new Element(pEleNode[i]);
    //            tElement.setText(pData[i]);
    //            root.addContent(tElement);
    //        }
    //		Document tDocument = new Document(root);
    //        return root;
    //    }
    /**
     * 从二维数组数据生成JDom Element
     * @param tSSRS   二维数据集
     * @param parentElem  父节点名
     * @param pEleNode    子元素结点名数组
     * @param childElem   子节点名
     * @return
     */
    //    public Element formJElem(SSRS tSSRS, String parentElem, String[] pEleNode, String childElem)
    //    {
    //
    //        Element parent = new Element(parentElem);
    //        parent.setText("");
    //
    //        for (int i = 1; i <= tSSRS.getMaxRow(); i++)
    //        {
    //            Element child = this.formJElem(tSSRS.getRowData(i), pEleNode, childElem);
    //            parent.addContent(child);
    //        }
    //
    //        return parent;
    //    }
    /**
     * 构建消息PACKET XML数据
     * @param aElem  消息体
     * @param aRequestType 请求类型编码 01 承保 02 保全 03 理赔
     * @param aTransactionNum 交易号码
     * @return
     */
    public Document formJDocMessage(Element aElem, String aRequestType, String aTransactionNum)
    {

        CommonXmlTag tCommonXmlTag = new CommonXmlTag();

        Element pElement = new Element(tCommonXmlTag.PACKET);
        pElement.addAttribute(tCommonXmlTag.type, "REQUEST");
        pElement.addAttribute(tCommonXmlTag.version, "1.0");

        Element hElement = new Element(tCommonXmlTag.HEAD);
        hElement.setText("");

        Element rElement = new Element(tCommonXmlTag.REQUEST_TYPE);
        rElement.setText(aRequestType);
        hElement.addContent(rElement);

        Element tElement = new Element(tCommonXmlTag.TRANSACTION_NUM);
        tElement.setText(aTransactionNum);
        hElement.addContent(tElement);

        Element bElement = new Element(tCommonXmlTag.BODY);
        //		bElement.setText("");
        bElement.addContent(aElem);

        pElement.addContent(hElement);
        pElement.addContent(bElement);
        Document tDocument = new Document(pElement);

        return tDocument;
    }

    public static void main(String[] args)
    {

        //        QYXmlTag tQYXmlTag = new QYXmlTag();
        //        String[] tData = new String[tQYXmlTag.PolicyNode.length];
        //        for (int i = 1; i <= tQYXmlTag.PolicyNode.length; i++)
        //        {
        //            tData[i - 1] = "ElemContent" + i;
        //        }
        //
        //        JKXService tJKXService = new JKXService();
        //        Element out1 = tJKXService.formJElem(tData, tQYXmlTag.PolicyNode, tQYXmlTag.policy);
        //        JdomUtil.print(out1);
        //
        //        ExeSQL tExeSQL = new ExeSQL();
        //        String tSql = "select '01','02','03','04','05','06','07','08','09','010','011','012','013','014','015','016','017','018','019' from dual "
        //                + " union "
        //                + " select '11','12','13','14','15','16','17','18','19','110','111','112','113','114','115','116','117','118','119' from dual";
        //        SSRS tSSRS = tExeSQL.execSQL(tSql);
        //        System.out.println(tQYXmlTag.policyInsuredNode.length);
        //        Element out2 = tJKXService.formJElem(tSSRS, tQYXmlTag.PolicyInsuredList, tQYXmlTag.policyInsuredNode,
        //                tQYXmlTag.PolicyInsured);
        //        JdomUtil.print(out2);
        //
        //        Element root = out1;
        //        root.addContent(out2);
        //        //		root.addChild(out2);
        //
        //        //		Document tDocument = new Document(root);
        //        Document tDocument = tJKXService.formJDocMessage(root, "01", "ContNo");
        //        JdomUtil.print(tDocument);
        //        return;
    }
}
