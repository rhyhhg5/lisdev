/**
 * 2010-11-18
 */
package com.sinosoft.lis.jkxpt;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.jkxpt.JKXService;
import com.sinosoft.lis.jkxpt.ReqJKXPreService;
import com.sinosoft.lis.taskservice.DataTrackBL;
import com.sinosoft.lis.yibaotong.JdomUtil;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class BQJYXGRPJKXServiceImp extends JKXService
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private TransferData mTransferData = null;

    /** 数据操作字符串 */
    private String mOperate = "";

    private String mDataInfoKey = "";
    
    private String mEdorNo = ""; //保全受理号
    
    private String mEdorType = ""; //保全项目类型
    
    private String mEdorProp = ""; //个团标志 ：I-个单，G-团单

    private Element mRoot = null;

    private String mContType = null;

    public boolean callJKXService(VData cInputData, String cOperate)
    {
        // 获取入口参数 
        if (!getInputData(cInputData, cOperate))
        {
            return false;
        }
        // --------------------
        
        // 取数逻辑处理        
        //        mRoot = dealBusiXml(mPolicyNo);
        mRoot = reportBusiXml(mDataInfoKey);
        // --------------------

        // 调用前置机
        if (!calService())
        {
            return false;
        }
        // --------------------

        return true;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mDataInfoKey = (String) mTransferData.getValueByName("DataInfoKey");
        if (mDataInfoKey == null || mDataInfoKey.equals(""))
        {
            buildError("getInputData", "获取保全参数信息失败。");
            return false;
        }
        
    	String [] tEdorInfo = mDataInfoKey.split("&");
		mEdorNo = tEdorInfo[0];
		mEdorType = tEdorInfo[1];
		mEdorProp = tEdorInfo[2];
		
		if (mEdorNo == null || mEdorNo.equals(""))
        {
            buildError("getInputData", "获取保全受理号EdorNo信息失败。");
            return false;
        }
		
		if (mEdorType == null || mEdorType.equals(""))
        {
            buildError("getInputData", "获取保全类型EdorType信息失败。");
            return false;
        }
		
		if (mEdorProp == null || mEdorProp.equals(""))
        {
            buildError("getInputData", "获取保全个团标志EdorProp信息失败。");
            return false;
        }

        return true;
    }

    private Element reportBusiXml(String cDataInfoKey)
    {
    	 Element tRoot = new Element("AccidentGroupPolicyLogDTO");
         tRoot.addAttribute("DTOClass", "com.ebao.accident.insurance.dto.AccidentGroupPolicyLogDTO");
         Element tTmpEle = null;
         if(mEdorType.equals("CM"))//3.2.1	保单资料变更
         {
            //保单详细信息
             SSRS tResPolicyLogInfo = loadBaoDanZiLiaoBianGengInfoSQL(cDataInfoKey);
             for (int idxPI = 1; idxPI <= tResPolicyLogInfo.MaxRow; idxPI++)
             {
                 Element tItemEle = new Element("Item");
                 tRoot.addContent(tItemEle);

                 String[] tDataInfo = tResPolicyLogInfo.getRowData(idxPI);
                 
                 tTmpEle = new Element("PolicyNo");
                 tTmpEle.setText(tDataInfo[0]);
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("RenewalTimes");
                 tTmpEle.setText(tDataInfo[1]);
                 tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为"NUMBER"的字段，请增加描述
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("EndorsementType");
                 tTmpEle.setText(tDataInfo[2]);
//                 tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为进行业务转换，即备注为《公用代码》”的字段
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("EndorsementNo");
                 tTmpEle.setText(tDataInfo[3]);
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("EndorsementApplicationDate");
                 tTmpEle.setText(tDataInfo[4]);
                 tTmpEle.addAttribute("CData", "CDate");//文档中类型需为"Date"的字段，请增加描述
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("EffectiveDate");
                 tTmpEle.setText(tDataInfo[5]);
                 tTmpEle.addAttribute("CData", "CDate");//文档中类型需为"Date"的字段，请增加描述
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;
                 
                 tTmpEle = new Element("PolicyStartDate");
                 tTmpEle.setText(tDataInfo[6]);
                 tTmpEle.addAttribute("CData", "CDate");//文档中类型需为"Date"的字段，请增加描述
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("PolicyEndDate");
                 tTmpEle.setText(tDataInfo[7]);
                 tTmpEle.addAttribute("CData", "CDate");//文档中类型需为"Date"的字段，请增加描述
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("RenewalMethod");
                 tTmpEle.setText(tDataInfo[8]);
//                 tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为进行业务转换，即备注为《公用代码》”的字段
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("Sales");
                 tTmpEle.setText(tDataInfo[9]);
//                 tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为进行业务转换，即备注为《公用代码》”的字段
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("SalesChannelCode");
                 tTmpEle.setText(tDataInfo[10]);
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("SalesChannelName");
                 tTmpEle.setText(tDataInfo[11]);
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("ProductName");
                 tTmpEle.setText(tDataInfo[12]);
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;
                 
                 tTmpEle = new Element("EffectiveInsuredNum");
                 tTmpEle.setText(tDataInfo[13]);
                 tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为"NUMBER"的字段，请增加描述
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;
                 
                 tTmpEle = new Element("CurrentPremium");
                 tTmpEle.setText(tDataInfo[14]);
                 tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为"NUMBER"的字段，请增加描述
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;
                 
                 tTmpEle = new Element("SpecialRemark");
                 tTmpEle.setText(tDataInfo[15]);
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;
                 
              }
         }
         if(mEdorType.equals("CM"))//3.2.3	被保险人信息变更
         {
            //保单详细信息
             SSRS tResPolicyLogInfo = loadlccontCMInfoSQL(cDataInfoKey);
             for (int idxPI = 1; idxPI <= tResPolicyLogInfo.MaxRow; idxPI++)
             {
                 Element tItemEle = new Element("Item");
                 tRoot.addContent(tItemEle);

                 String[] tDataInfo = tResPolicyLogInfo.getRowData(idxPI);
                 
                 tTmpEle = new Element("PolicyNo");
                 tTmpEle.setText(tDataInfo[0]);
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("RenewalTimes");
                 tTmpEle.setText(tDataInfo[1]);
                 tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为"NUMBER"的字段，请增加描述
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("EndorsementType");
                 tTmpEle.setText(tDataInfo[2]);
//                 tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为进行业务转换，即备注为《公用代码》”的字段
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("EndorsementNo");
                 tTmpEle.setText(tDataInfo[3]);
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("EndorsementApplicationDate");
                 tTmpEle.setText(tDataInfo[4]);
                 tTmpEle.addAttribute("CData", "CDate");//文档中类型需为"Date"的字段，请增加描述
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("EffectiveDate");
                 tTmpEle.setText(tDataInfo[5]);
                 tTmpEle.addAttribute("CData", "CDate");//文档中类型需为"Date"的字段，请增加描述
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;
                 
                 tTmpEle = new Element("CurrentPremium");
                 tTmpEle.setText(tDataInfo[6]);
                 tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为"NUMBER"的字段，请增加描述
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 
                 String edorNo=tDataInfo[3];
                 //被保险人资料变更的详细信息
                 tTmpEle = ziLiaoBianGengBeiBaoRenInfo(edorNo);
     	   	     if (tTmpEle != null)
     	   	     {
     	   	        tItemEle.addContent(tTmpEle);
     	   	     }
     	   	     tTmpEle = null;

                 
                
             }
         }
         else if(mEdorType.equals("AC"))//3.2.2	团体投保人资料变更
         {
             //投保单位资料变更保单详细信息
             SSRS tResPolicyLogInfo = loadlccontACInfoSQL(cDataInfoKey);
             for (int idxPI = 1; idxPI <= tResPolicyLogInfo.MaxRow; idxPI++)
             {
                 Element tItemEle = new Element("Item");
                 tRoot.addContent(tItemEle);

                 String[] tDataInfo = tResPolicyLogInfo.getRowData(idxPI);
                 tTmpEle = new Element("PolicyNo");
                 tTmpEle.setText(tDataInfo[0]);
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("RenewalTimes");
                 tTmpEle.setText(tDataInfo[1]);
                 tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为"NUMBER"的字段，请增加描述
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("EndorsementType");
                 tTmpEle.setText(tDataInfo[2]);
//                 tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为进行业务转换，即备注为《公用代码》”的字段
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("EndorsementNo");
                 tTmpEle.setText(tDataInfo[3]);
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("EndorsementApplicationDate");
                 tTmpEle.setText(tDataInfo[4]);
                 tTmpEle.addAttribute("CData", "CDate");//文档中类型需为"Date"的字段，请增加描述
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("EffectiveDate");
                 tTmpEle.setText(tDataInfo[5]);
                 tTmpEle.addAttribute("CData", "CDate");//文档中类型需为"Date"的字段，请增加描述
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;
                 
                 //团体投保人数组
                 String grpcontNo=tDataInfo[0];
                 tTmpEle = tuanTiTouBaoRenhuZu(grpcontNo);
                 if (tTmpEle != null)
                 {
                     tItemEle.addContent(tTmpEle);
                 }
                 tTmpEle = null;
                 

                 
                
             }
         }
         else if(mEdorType.equals("NI"))//3.2.4	增加被保险人
         {
             //保单详细信息
             SSRS tResPolicyLogInfo = loadlccontNIInfoSQL(cDataInfoKey);
             for (int idxPI = 1; idxPI <= tResPolicyLogInfo.MaxRow; idxPI++)
             {
                 Element tItemEle = new Element("Item");
                 tRoot.addContent(tItemEle);

                 String[] tDataInfo = tResPolicyLogInfo.getRowData(idxPI);
                 
                 tTmpEle = new Element("PolicyNo");
                 tTmpEle.setText(tDataInfo[0]);
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("RenewalTimes");
                 tTmpEle.setText(tDataInfo[1]);
                 tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为"NUMBER"的字段，请增加描述
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("EndorsementType");
                 tTmpEle.setText(tDataInfo[2]);
//                 tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为进行业务转换，即备注为《公用代码》”的字段
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("EndorsementNo");
                 tTmpEle.setText(tDataInfo[3]);
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("EndorsementApplicationDate");
                 tTmpEle.setText(tDataInfo[4]);
                 tTmpEle.addAttribute("CData", "CDate");//文档中类型需为"Date"的字段，请增加描述
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("EffectiveDate");
                 tTmpEle.setText(tDataInfo[5]);
                 tTmpEle.addAttribute("CData", "CDate");//文档中类型需为"Date"的字段，请增加描述
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;
                 
                 tTmpEle = new Element("CurrentPremium");
                 tTmpEle.setText(tDataInfo[6]);
                 tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为"NUMBER"的字段，请增加描述
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("EffectiveInsuredNum");
                 tTmpEle.setText(tDataInfo[7]);
                 tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为"NUMBER"的字段，请增加描述
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;
                 
                 String edorNo=tDataInfo[3];
                 //新增的被保人的详细信息
                 tTmpEle = xinZengBaoRenInfo(edorNo);
     	   	     if (tTmpEle != null)
     	   	     {
     	   	        tItemEle.addContent(tTmpEle);
     	   	     }
     	   	     tTmpEle = null;

                 
                
             }
         }
         else if(mEdorType.equals("ZT"))//3.2.5	减少被保险人
         {
             //保单详细信息
             SSRS tResPolicyLogInfo = loadlccontZTInfoSQL(cDataInfoKey);
             for (int idxPI = 1; idxPI <= tResPolicyLogInfo.MaxRow; idxPI++)
             {
                 Element tItemEle = new Element("Item");
                 tRoot.addContent(tItemEle);

                 String[] tDataInfo = tResPolicyLogInfo.getRowData(idxPI);
                 
                 tTmpEle = new Element("PolicyNo");
                 tTmpEle.setText(tDataInfo[0]);
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("RenewalTimes");
                 tTmpEle.setText(tDataInfo[1]);
                 tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为"NUMBER"的字段，请增加描述
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("EndorsementType");
                 tTmpEle.setText(tDataInfo[2]);
//                 tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为进行业务转换，即备注为《公用代码》”的字段
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("EndorsementNo");
                 tTmpEle.setText(tDataInfo[3]);
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("EndorsementApplicationDate");
                 tTmpEle.setText(tDataInfo[4]);
                 tTmpEle.addAttribute("CData", "CDate");//文档中类型需为"Date"的字段，请增加描述
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("EffectiveDate");
                 tTmpEle.setText(tDataInfo[5]);
                 tTmpEle.addAttribute("CData", "CDate");//文档中类型需为"Date"的字段，请增加描述
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;
                 
                 tTmpEle = new Element("CurrentPremium");
                 tTmpEle.setText(tDataInfo[6]);
                 tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为"NUMBER"的字段，请增加描述
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("EffectiveInsuredNum");
                 tTmpEle.setText(tDataInfo[7]);
                 tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为"NUMBER"的字段，请增加描述
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;
                 
                 String edorNo=tDataInfo[3];
                 //减掉的被保人的详细信息
                 tTmpEle = jianDiaoDeBaoRenInfo(edorNo);
     	   	     if (tTmpEle != null)
     	   	     {
     	   	        tItemEle.addContent(tTmpEle);
     	   	     }
     	   	     tTmpEle = null;

                
             }
        	 
         }
         else if(mEdorType.equals("CT")||mEdorType.equals("WT"))//3.2.6	全单退保
         {
        	 //团单全单退保保单信息
             SSRS tResPolicyLogInfo = loadlcgrpcontCTInfoSQL(cDataInfoKey);
             for (int idxPI = 1; idxPI <= tResPolicyLogInfo.MaxRow; idxPI++)
             {
                 Element tItemEle = new Element("Item");
                 tRoot.addContent(tItemEle);

                 String[] tDataInfo = tResPolicyLogInfo.getRowData(idxPI);
                 
                 tTmpEle = new Element("PolicyNo");
                 tTmpEle.setText(tDataInfo[0]);
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("RenewalTimes");
                 tTmpEle.setText(tDataInfo[1]);
                 tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为"NUMBER"的字段，请增加描述
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("EndorsementType");
                 tTmpEle.setText(tDataInfo[2]);
//                 tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为进行业务转换，即备注为《公用代码》”的字段
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("EndorsementNo");
                 tTmpEle.setText(tDataInfo[3]);
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("EndorsementApplicationDate");
                 tTmpEle.setText(tDataInfo[4]);
                 tTmpEle.addAttribute("CData", "CDate");//文档中类型需为"Date"的字段，请增加描述
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;

                 tTmpEle = new Element("EffectiveDate");
                 tTmpEle.setText(tDataInfo[5]);
                 tTmpEle.addAttribute("CData", "CDate");//文档中类型需为"Date"的字段，请增加描述
                 tItemEle.addContent(tTmpEle);
                 tTmpEle = null;
                  
             }
         }
         
         
        
         
    	
    	return tRoot;
    }
    //团单被保险人信息变更保单信息数组
    private SSRS loadBaoDanZiLiaoBianGengInfoSQL(String cDataInfoKey)
    {
        SSRS tResult = null;
        StringBuffer tStrBSql = new StringBuffer();
        String tStrSql = tStrBSql.toString();
//        保单号码	POLICYNO	VARCHAR2(30)	Y		
//        续保次数	RENEWALTIMES	NUMBER(3)	Y		
//        保全类别	ENDORSEMENTTYPE	VARCHAR2(2)	Y	Y	《公用代码》1.1.12
//        保全批单号	ENDORSEMENTNO	VARCHAR2(30)	Y		
//        保全申请日期	ENDORSEMENTAPPLICATIONDATE	DATETIME	Y		
//        保全生效日期	EFFECTIVEDATE	DATETIME	Y		
//        保单起保时间	POLICYSTARTDATE	DATETIME			
//        保单满期时间	POLICYENDDATE	DATETIME			
//        投保方式	RENEWALMETHOD	VARCHAR2(2)		Y	《公用代码》1.1.17
//        销售渠道	SALES	VARCHAR2(2)		Y	《公用代码》1.1.8
//        销售机构代码	SALESCHANNELCODE	VARCHAR2(20)			
//        销售机构名称	SALESCHANNELNAME	VARCHAR2(100)			
//        产品名称	PRODUCTNAME	VARCHAR2(500)			
//        有效被保险人数	EFFECTIVEINSUREDNUM	NUMBER(8)			
//        当前保费	CURRENTPREMIUM	NUMBER(20,2)			
//        特别约定	SPECIALREMARK	VARCHAR2(900)			保险公司上传字段时如超过字数则需注明 内容详见保单字样。
	
	
		
		
		
        tStrBSql.append(" select ");
        tStrBSql.append(" lp.grpcontno policyNo,");//保单号码
        tStrBSql.append(" '0' renewalTimes,");//续保次数
        tStrBSql.append(" '11'  endorsementType,");//保全类别
//        tStrBSql.append(" lp.edortype  endorsementType,");//保全类别
        tStrBSql.append(" lp.edorno endorsementNo,");//保全批单号
        tStrBSql.append(" (select edorappdate from lpedorapp where edoracceptno=lp.edorno) endorsementApplicationDate,");//保全申请日期
        tStrBSql.append(" lp.edorvalidate effectiveDate, ");//保全生效日期	
        tStrBSql.append(" (select cvalidate from lcgrpcont where grpcontno=lp.grpcontno)  policyStartDate ,");//保单起保时间
        tStrBSql.append(" (select cinvalidate from lcgrpcont where grpcontno=lp.grpcontno)   policyEndDate,");//保单满期时间
        tStrBSql.append(" '01' renewalMethod,");//投保方式
        tStrBSql.append("  (select (case salechnl when '02' then '01' when '07' then '01'  else '11' end ) from lcgrpcont where grpcontno=lp.grpcontno)  sales,");//销售渠道
        tStrBSql.append("  (select agentcom from lcgrpcont where grpcontno=lp.grpcontno) salesChannelCode,");//销售机构代码
        tStrBSql.append("  (select (select name from lacom where agentcom = c.agentcom) from lcgrpcont c where grpcontno=lp.grpcontno ) salesChannelName,");//销售机构名称	
        tStrBSql.append("  (select  (select riskname from lmriskapp where riskcode=c.riskcode) from lcgrppol c where grpcontno=lp.grpcontno fetch first 1 rows only )  productName,");//产品名称
        tStrBSql.append(" (select count(1) from lcinsured where grpcontno=lp.grpcontno)  effectiveInsuredNum,");//有效被保险人数
        tStrBSql.append("  (select prem from lcgrpcont where grpcontno=lp.grpcontno) currentPremium,");//当前保费
        tStrBSql.append("  '' specialRemark ");//特别约定
        tStrBSql.append(" from lpGrpedoritem lp where 1=1 and edortype ='CM' ");
        tStrBSql.append(" and edorNo='");
        tStrBSql.append(mEdorNo+"' with ur ");
      
        tResult = new ExeSQL().execSQL(tStrBSql.toString());

        return tResult;
    }   
    
    
    
//  被保人信息变更被保人数组详细信息
    private SSRS ziLiaoBianGengBeiBaoRenInfoSQL(String edorNo)
    {
        SSRS tResult = null;
        StringBuffer tStrBSql = new StringBuffer();
        String tStrSql = tStrBSql.toString();
//        客户编码	CUSTOMERNO	VARCHAR2(60)	Y		公司内部识别被保险人的唯一标识，被保险人序号。
//        姓名	NAME	VARCHAR2(60)	Y		
//        性别	GENDER	VARCHAR2(1)		Y	《公用代码》1.1.3
//        出生日期	BIRTHDAY	DATE			
//        被保险人证件类别	CRITTYPE	VARCHAR2(2)	Y	Y	《公用代码》1.1.4
//        被保险人证件号码	CRITCODE	VARCHAR2(30)	Y		
//        手机号码	MOBILE	VARCHAR2(20)			供短信发送
//        要约状态	OFFERSTATUS	VARCHAR2(2)	Y	Y	《公用代码》1.1.13
//        被保险人责任开始日期	SUBSTARTDATE	DATETIME			
//        被保险人责任终止日期	SUBENDDATE	DATETIME			
//        被保险人类型	INSUREDTYPE	VARCHAR2(2)	Y	Y	《公用代码》1.1.9
//        主被保人序号	SNOFMAININSURED	VARCHAR2(30)	S		当被保险人类型为02(连带被保险人)时，该字段必填
//        与主被保人关系	RELATIONSHIPWITHMAININSURED	VARCHAR2(2)	S	Y	《公用代码》1.1.5
//        当被保险人类型为02(连带被保险人)时，该字段必填
//        是否指定受益人	APPOINTEDBENEFIT	VARCHAR2(1)	Y	Y	《公用代码》1.1.7
//        被保险人的当前保费	INSUREDCURRENTPREMIUM	NUMBER(20,2)	Y		
//        受益人数组	GROUPBENEFIT_ARRAY		S		详见下面描述
//        如果是否指定受益人为1(是)，则该数组必填
//        承保险种数组	GROUPCOVERAGE_ARRAY		Y		

		
				
        tStrBSql.append(" select ");//
        tStrBSql.append(" a.insuredNo customerNo,");//客户编码
        tStrBSql.append(" a.name  name,");//姓名
        tStrBSql.append(" (case when sex='0' then '1' when sex='1' then '2' else '9' end)   gender,");//性别
        tStrBSql.append(" a.birthday birthday,");//出生日期
        tStrBSql.append(" (case when idtype='0' then '01' when idtype='1' then '07' when idtype='2' then '04' else '99'    end) critType,");//被保险人证件类别
        tStrBSql.append(" idno critCode,");//被保险人证件号码
        tStrBSql.append(" '' mobile,");//手机号码
        tStrBSql.append(" '1' offerStatus,");//要约状态
        tStrBSql.append(" (select cvalidate from lccont where contno=a.contno) subStartDate,");//被保险人责任开始日期
        tStrBSql.append(" (select cinvalidate from lccont where contno=a.contno) subEndDate,");//被保险人责任终止日期
        tStrBSql.append(" ( case when relationtomaininsured='00' then '01' else '02' end ) insuredType,");//被保险人类型
        tStrBSql.append(" '2' snOfMainInsured,");//主被保人序号
        tStrBSql.append(" a.relationtomaininsured  relationshipWithMainInsured,");//与主被保人关系
        tStrBSql.append(" '0' appointedBenefit ,");//是否指定受益人
        tStrBSql.append(" nvl((select sum(prem) from lcpol where contno=a.contno and insuredno=a.insuredno),0)  insuredCurrentPremium ");//被保险人的当前保费

        tStrBSql.append(" from lcinsured a,lpedoritem b where 1=1 and a.contno=b.contno and  b.edortype='CM' ");
        tStrBSql.append(" and b.edorNo='");
        tStrBSql.append(mEdorNo+"' with ur ");
      
        tResult = new ExeSQL().execSQL(tStrBSql.toString());

        return tResult;
    }
    //被保人信息变更被保人数组详细信息
    private Element ziLiaoBianGengBeiBaoRenInfo(String edorNo)
    {
        Element tRoot = new Element("AccidentGroupPolicyInsuredLogDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.accident.insurance.dto.AccidentGroupPolicyInsuredLogDTO");

        Element tTmpEle = null;

        SSRS tResPolicyUpcoverageLogInfo = ziLiaoBianGengBeiBaoRenInfoSQL(edorNo);

        for (int idxPI = 1; idxPI <= tResPolicyUpcoverageLogInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyUpcoverageLogInfo.getRowData(idxPI);
            tTmpEle = new Element("CustomerNo");
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Name");
            tTmpEle.setText(tDataInfo[1]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Gender");
            tTmpEle.setText(tDataInfo[2]);
//            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Birthday");
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritType");
            tTmpEle.setText(tDataInfo[4]);
//            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritCode");
            tTmpEle.setText(tDataInfo[5]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Mobile");
            tTmpEle.setText(tDataInfo[6]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("OfferStatus");
            tTmpEle.setText(tDataInfo[7]);
//            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SubStartDate");
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SubEndDate");
            tTmpEle.setText(tDataInfo[9]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("InsuredType");
            tTmpEle.setText(tDataInfo[10]);
//            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SnOfMainInsured");
            tTmpEle.setText(tDataInfo[11]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RelationshipWithMainInsured");
            tTmpEle.setText(tDataInfo[12]);
//            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("AppointedBenefit");
            tTmpEle.setText(tDataInfo[13]);
//            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("InsuredCurrentPremium");
            tTmpEle.setText(tDataInfo[14]);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为"NUMBER"的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
           //受益人数组没有
            
            String insuredNo=tDataInfo[0];
            
            //被保人信息变更险种数组
            tTmpEle = xinXiBianGengBeiBaoRenXianZhongShuZu(insuredNo);
 	        if (tTmpEle != null)
 	        {
 	              tItemEle.addContent(tTmpEle);
 	        }
 	        tTmpEle = null;
            

        }

        return tRoot;
    }
    private SSRS xinXiBianGengBeiBaoRenXianZhongShuZuSQL(String insuredNo)
    {
        SSRS tResult = null;
        StringBuffer tStrBSql = new StringBuffer();
        
//        平台险种代码	COVERAGECODE	VARCHAR2(5)	Y	Y	《公用代码》1.1.15
//        公司险种代码	COMCOVERAGECODE	VARCHAR2(60)	Y		
//        公司险种名称	COVERAGENAME	VARCHAR2(200)	Y		
//        险种起保日期	COVERAGEEFFECTIVEDATE	DATETIME	Y		
//        险种满期日期	COVERAGEEXPIREDATE	DATETIME	Y		
//        主附险性质	MAINATTACHEDFLAG	VARCHAR2(1)	Y	Y	《公用代码》1.1.11
//        险种当前保费	COVERAGECURRENTPREMIUM	NUMBER(20,2)	Y		
//        险种当前保额	COVERAGECURRENTSUMINSURED	NUMBER(20,2)			
//        缴费方式	PAYMENTMETHOD	VARCHAR2(2)		Y	《公用代码》1.1.14
//        缴费年限	PAYMENTYEARS	NUMBER(4)			一年期及一年期以下的业务缴费年限为1；
//        特殊业务标识	SPECIFICBUSINESS	VARCHAR2(1)	Y	Y	《公用代码》1.1.7
//        特殊业务代码	SPECIFICBUSINESSCODE	VARCHAR2(5)	S	Y	《公用代码》1.1.6
//        如果特殊业务标识为1(是)，该字段必填
//        承保责任数组	GROUPCOVERAGE_ARRAY		Y		详见下面描述


        tStrBSql.append(" select ");//
        tStrBSql.append(" 'A01' coverageCode,");//平台险种代码
        tStrBSql.append(" riskcode comCoverageCode,");//公司险种代码
        tStrBSql.append(" (select riskname from lmriskapp where riskcode=a.riskcode ) coverageName,");//公司险种名称
        tStrBSql.append("  cvalidate coverageEffectiveDate,");//险种起保日期
        tStrBSql.append("  (select cinvalidate from lccont where contno=a.contno) coverageExpireDate,");//险种满期日期
        tStrBSql.append("  (case when exists (select 1 from lmriskapp where subriskflag='S' and riskcode=a.riskcode ) then '2' else '1' end ) mainAttachedFlag,");//主附险性质
        tStrBSql.append("  prem coveragePremium,");//险种期初保费
        tStrBSql.append("  amnt coverageSuminsured,");//险种期初保额
        tStrBSql.append("  paymode paymentMethod,");//缴费方式
        tStrBSql.append("  payyears paymentYears,");//缴费年限
        tStrBSql.append("  '0' specificBusiness,");//特殊业务标识
        tStrBSql.append("  'CK' specificBusinessCode,");//特殊业务代码
        tStrBSql.append("  polNo polNo ");//险种号     后边用
        tStrBSql.append(" from lcpol a where 1=1 ");//
        tStrBSql.append(" and insuredNo='");//
        tStrBSql.append(insuredNo+"'");


        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }
    //被保人信息变更险种数组
    private Element xinXiBianGengBeiBaoRenXianZhongShuZu(String insuredNo)
    {
        Element tRoot = new Element("AccidentGroupPolicyUpcoverageLogDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.accident.insurance.dto.AccidentGroupPolicyUpcoverageLogDTO");

        Element tTmpEle = null;

        SSRS tResUnderWritingLogInfo = xinXiBianGengBeiBaoRenXianZhongShuZuSQL(insuredNo);

        for (int idxPI = 1; idxPI <= tResUnderWritingLogInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResUnderWritingLogInfo.getRowData(idxPI);
            
            tTmpEle = new Element("CoverageCode");
            tTmpEle.setText(tDataInfo[0]);
//            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ComCoverageCode");
            tTmpEle.setText(tDataInfo[1]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageName");
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageEffectiveDate");
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageExpireDate");
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("MainAttachedFlag");
            tTmpEle.setText(tDataInfo[5]);
//            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoveragePremium");
            tTmpEle.setText(tDataInfo[6]);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为"NUMBER"的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("CoverageSuminsured");
            tTmpEle.setText(tDataInfo[7]);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为"NUMBER"的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PaymentMethod");
            tTmpEle.setText(tDataInfo[8]);
//            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PaymentYears");
            tTmpEle.setText(tDataInfo[9]);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为"NUMBER"的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("SpecificBusiness");
            tTmpEle.setText(tDataInfo[10]);
//            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SpecificBusinessCode");
            tTmpEle.setText(tDataInfo[11]);
//            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            //被保险人变更的险种下承保责任数组
            String polNo=tDataInfo[12];
            tTmpEle = beiBaoXianRenXianZhongXiaZeRenShuZu(polNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
           

            
        }

        return tRoot;
    }
    private SSRS beiBaoXianRenXianZhongXiaZeRenShuZuSQL(String cPolNo)
    {
        SSRS tResult = null;
//        责任代码	LIABILITYCODE	VARCHAR2(60)	Y		
//        责任名称	LIABILITYNAME	VARCHAR2(200)	Y		
//        责任开始日期	LIABILITYEFFECTIVEDATE	DATETIME	Y		
//        责任终止日期	LIABILITYEXPIREDATE	DATETIME	Y		
//        责任状态	LIABILITYSTATUS	VARCHAR2(2)	Y	Y	《公用代码》1.1.13
//        责任当前保费	LIABILITYCURRENTPREMIUM	NUMBER(20,2)			
//        责任当前保额	CURRENTLIMITAMOUNT	NUMBER(20,2)	S		与备注字段不能同时为空
//        免责期	WAITINGPERIOD	NUMBER(8)			
//        备注	REMARK	VARCHAR2(600)	S		与责任当前保额字段不能同时为空




        StringBuffer tStrBSql = new StringBuffer();
        tStrBSql.append(" select ");
        tStrBSql.append(" lcd.DutyCode liabilityCode,");//责任代码
        tStrBSql.append(" lmd.DutyName liabilityName,");//责任名称
        tStrBSql.append(" lcd.GetStartDate liabilityEffectiveDate,");//责任开始日期	
        tStrBSql.append(" lcd.EndDate liabilityExpireDate,");//责任终止日期
        tStrBSql.append(" lcp.StateFlag liabilityStatus,");//责任状态
        tStrBSql.append(" lcd.Prem  liabilityPremium,");//责任当前保费	
        tStrBSql.append(" lcd.Amnt  limitAmount,");//责任当前保额
        tStrBSql.append(" '0' waitingPeriod,");//免责期
        tStrBSql.append(" '' remark");//备注
        tStrBSql.append(" from LCPol lcp ");
        tStrBSql.append(" inner join LCDuty lcd on lcd.PolNo = lcp.PolNo ");
        tStrBSql.append(" inner join LMDuty lmd on lmd.DutyCode = lcd.DutyCode ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.polno = '" + cPolNo + "' ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }
    private Element beiBaoXianRenXianZhongXiaZeRenShuZu(String cPolNo)
    {
        Element tRoot = new Element("AccidentGroupPolicyCoverageLogDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.accident.insurance.dto.AccidentGroupPolicyCoverageLogDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = beiBaoXianRenXianZhongXiaZeRenShuZuSQL(cPolNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("LiabilityCode"); //平台责任分类
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityName"); //责任代码    
            tTmpEle.setText(tDataInfo[1]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityEffectiveDate"); //责任名称    
            tTmpEle.setText(tDataInfo[2]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("LiabilityExpireDate");
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityStatus");
            tTmpEle.setText(tDataInfo[4]);
//            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityPremium");
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为"NUMBER"的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LimitAmount");
            tTmpEle.setText(tDataInfo[6]);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为"NUMBER"的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("WaitingPeriod");
            tTmpEle.setText(tDataInfo[7]);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为"NUMBER"的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Remark");
            tTmpEle.setText(tDataInfo[8]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;


        }

        return tRoot;
    } 
    //团单被保险人信息变更保单信息数组
    private SSRS loadlccontCMInfoSQL(String cDataInfoKey)
    {
        SSRS tResult = null;
        StringBuffer tStrBSql = new StringBuffer();
        String tStrSql = tStrBSql.toString();
//        保单号码	POLICYNO	VARCHAR2(30)	Y		
//        续保次数	RENEWALTIMES	NUMBER(3)	Y		
//        保全类别	ENDORSEMENTTYPE	VARCHAR2(2)	Y	Y	《公用代码》1.1.12
//        保全批单号	ENDORSEMENTNO	VARCHAR2(30)	Y		
//        保全申请日期	ENDORSEMENTAPPLICATIONDATE	DATETIME	Y		
//        保全生效日期	EFFECTIVEDATE	DATETIME	Y		
//        当前保费	CURRENTPREMIUM	NUMBER(20,2)			
//        被保险人数组	GROUPINSURED_ARRAY		Y		
	
		
		
		
        tStrBSql.append(" select ");
        tStrBSql.append(" lp.grpcontno policyNo,");//保单号码
        tStrBSql.append(" '0' renewalTimes,");//续保次数
        tStrBSql.append(" '13'  endorsementType,");//保全类别
//        tStrBSql.append(" lp.edorType  endorsementType,");//保全类别
        tStrBSql.append(" lp.edorno endorsementNo,");//保全批单号
        tStrBSql.append(" (select edorappdate from lpedorapp where edoracceptno=lp.edorno) endorsementApplicationDate,");//保全申请日期
        tStrBSql.append(" lp.edorvalidate effectiveDate, ");//保全生效日期	
        tStrBSql.append(" (select sum(prem) from lcgrppol where grpcontno=lp.grpcontno ) currentPremium ");//当前保费
        tStrBSql.append(" from lpGrpedoritem lp where 1=1 and edortype ='CM' ");
        tStrBSql.append(" and edorNo='");
        tStrBSql.append(mEdorNo+"' with ur ");
      
        tResult = new ExeSQL().execSQL(tStrBSql.toString());

        return tResult;
    }
   //团体投保人数组
    private Element tuanTiTouBaoRenhuZu(String grpContNo)
    {
        Element tRoot = new Element("AccidentGroupPolicyPersonphLogDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.accident.insurance.dto.AccidentGroupPolicyPersonphLogDTO");

        Element tTmpEle = null;

        SSRS tResUnderWritingLogInfo = tuanTiTouBaoRenhuZuSQL(grpContNo);

        for (int idxPI = 1; idxPI <= tResUnderWritingLogInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResUnderWritingLogInfo.getRowData(idxPI);
            
            tTmpEle = new Element("Name");
            tTmpEle.setText(tDataInfo[0]);      
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritType");
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritCode");
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Contact");
            tTmpEle.setText(tDataInfo[3]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ContactPhone");
            tTmpEle.setText(tDataInfo[4]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ContactTel");
            tTmpEle.setText(tDataInfo[5]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
        }

        return tRoot;
    }
    //团体投保人数组
    private SSRS tuanTiTouBaoRenhuZuSQL(String grpContNo)
    {
        SSRS tResult = null;
//        单位名称	NAME	VARCHAR2(120)			
//        团体客户证件类别	CRITTYPE	VARCHAR2(2)		Y	《公用代码》1.1.17
//        团体客户证件号码	CRITCODE	VARCHAR2(30)			
//        联系人姓名	CONTACT	VARCHAR2(120)			
//        联系人手机	CONTACTPHONE	VARCHAR2(20)			
//        联系人电话	CONTACTTEL	VARCHAR2(30)			


        StringBuffer tStrBSql = new StringBuffer();
        tStrBSql.append(" select ");
        tStrBSql.append(" name name,");//单位名称
        tStrBSql.append(" '' critType,");//团体客户证件类别
        tStrBSql.append(" '' critCode,");//团体客户证件号码
        tStrBSql.append(" (select linkman1 From lcgrpaddress where customerno=a.customerno  and addressno =a.addressno) contact,");//联系人姓名
        tStrBSql.append(" (select mobile1 From lcgrpaddress where customerno=a.customerno  and addressno =a.addressno ) contactPhone,");//联系人手机
        tStrBSql.append(" (select phone1 From lcgrpaddress where customerno=a.customerno  and addressno =a.addressno ) contactTel ");//联系人电话
        tStrBSql.append(" from LCGRPAppnt a ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and grpcontno = '" + grpContNo + "' ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }
    //团单投保单位保单信息数组
    private SSRS loadlccontACInfoSQL(String cDataInfoKey)
    {
        SSRS tResult = null;
        StringBuffer tStrBSql = new StringBuffer();
        String tStrSql = tStrBSql.toString();
//        保单号码	POLICYNO	VARCHAR2(30)	Y		
//        续保次数	RENEWALTIMES	NUMBER(3)	Y		
//        保全类别	ENDORSEMENTTYPE	VARCHAR2(2)	Y	Y	《公用代码》1.1.12
//        保全批单号	ENDORSEMENTNO	VARCHAR2(30)	Y		
//        保全申请日期	ENDORSEMENTAPPLICATIONDATE	DATETIME	Y		
//        保全生效日期	EFFECTIVEDATE	DATETIME	Y				
//        被保险人数组	GROUPINSURED_ARRAY		Y		

		
        tStrBSql.append(" select ");
        tStrBSql.append(" lp.grpcontno policyNo,");//保单号码
        tStrBSql.append(" '0' renewalTimes,");//续保次数
        tStrBSql.append(" '12'  endorsementType,");//保全类别
//        tStrBSql.append(" lp.edorType  endorsementType,");//保全类别
        tStrBSql.append(" lp.edorno endorsementNo,");//保全批单号
        tStrBSql.append(" (select edorappdate from lpedorapp where edoracceptno=lp.edorno) endorsementApplicationDate,");//保全申请日期
        tStrBSql.append(" lp.edorvalidate effectiveDate ");//保全生效日期	

        tStrBSql.append(" from lpGrpedoritem lp where 1=1 and edortype ='AC' ");
        tStrBSql.append(" and edorNo='");
        tStrBSql.append(mEdorNo+"' with ur ");
      
        tResult = new ExeSQL().execSQL(tStrBSql.toString());

        return tResult;
    }
    // 团单全单退保保单信息
    private SSRS loadlcgrpcontCTInfoSQL(String cDataInfoKey)
    {
        SSRS tResult = null;
        StringBuffer tStrBSql = new StringBuffer();
        String tStrSql = tStrBSql.toString();
//        保单号码	POLICYNO	VARCHAR2(30)	Y		
//        续保次数	RENEWALTIMES	NUMBER(3)	Y		
//        保全类别	ENDORSEMENTTYPE	VARCHAR2(2)	Y	Y	《公用代码》1.1.12
//        保全批单号	ENDORSEMENTAPPLICATIONDATE	VARCHAR2(30)	Y		
//        保全申请日期	APPLICATIONDATE	DATETIME	Y		
//        保全生效日期	EFFECTIVEDATE	DATETIME	Y		
		
		
		
        tStrBSql.append(" select ");
        tStrBSql.append(" lp.grpcontno policyNo,");//保单号码
        tStrBSql.append(" '0' renewalTimes,");//续保次数
        tStrBSql.append(" '16'  endorsementType,");//保全类别
        tStrBSql.append(" lp.edorno endorsementNo,");//保全批单号
        tStrBSql.append(" (select edorappdate from lpedorapp where edoracceptno=lp.edorno) endorsementApplicationDate,");//保全申请日期
        tStrBSql.append(" lp.edorvalidate effectiveDate ");//保全生效日期	
        tStrBSql.append(" from lpGrpedoritem lp where 1=1 and edortype in ('CT','WT') ");
        tStrBSql.append(" and edorNo='");
        tStrBSql.append(mEdorNo+"' with ur ");
      
        tResult = new ExeSQL().execSQL(tStrBSql.toString());

        return tResult;
    }
    //减掉的被保人的详细信息
    private Element jianDiaoDeBaoRenInfo(String edorNo)
    {
        Element tRoot = new Element("AccidentGroupPolicyInsuredLogDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.accident.insurance.dto.AccidentGroupPolicyInsuredLogDTO");

        Element tTmpEle = null;

        SSRS tResPolicyUpcoverageLogInfo = jianDiaoDeBaoRenInfoSQL(edorNo);

        for (int idxPI = 1; idxPI <= tResPolicyUpcoverageLogInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyUpcoverageLogInfo.getRowData(idxPI);
            
            tTmpEle = new Element("CustomerNo");
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
          
            tTmpEle = new Element("SubEndDate");
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;           
            
            
        }

        return tRoot;
    }
    //减掉的被保人的详细信息
    private SSRS jianDiaoDeBaoRenInfoSQL(String edorNo)
    {
        SSRS tResult = null;
        StringBuffer tStrBSql = new StringBuffer();
        String tStrSql = tStrBSql.toString();
//        客户编码	CUSTOMERNO	VARCHAR2(60)	Y		公司内部识别被保险人的唯一标识，被保险人序号。
//        被保险人责任终止日期	SUBENDDATE	DATETIME			
					
        tStrBSql.append(" select ");//
        tStrBSql.append(" insuredNo customerNo,");//客户编码 
        tStrBSql.append(" current date subEndDate ");//被保险人责任终止日期     
        tStrBSql.append(" from ljagetendorse a where 1=1 and  feeoperationtype='ZT' ");
        tStrBSql.append(" and endorsementno='");
        tStrBSql.append(mEdorNo+"' group by insuredNo with ur ");
      
        tResult = new ExeSQL().execSQL(tStrBSql.toString());

        return tResult;
    }
    //团单增人保单信息数组
    private SSRS loadlccontZTInfoSQL(String cDataInfoKey)
    {
        SSRS tResult = null;
        StringBuffer tStrBSql = new StringBuffer();
        String tStrSql = tStrBSql.toString();
		//        保单号码	POLICYNO	VARCHAR2(30)	Y		
		//        续保次数	RENEWALTIMES	NUMBER(3)	Y		
		//        保全类别	ENDORSEMENTTYPE	VARCHAR2(2)	Y	Y	《公用代码》1.1.12
		//        保全批单号	ENDORSEMENTNO	VARCHAR2(30)	Y		
		//        保全申请日期	ENDORSEMENTAPPLICATIONDATE	DATETIME	Y		
		//        保全生效日期	EFFECTIVEDATE	DATETIME	Y		
		//        当前保费	CURRENTPREMIUM	NUMBER(20,2)			
		//        有效被保险人数	EFFECTIVEINSUREDNUM	NUMBER(8)	Y		
		//        被保险人数组	GROUPINSURED_ARRAY		Y		

		
		
        tStrBSql.append(" select ");
        tStrBSql.append(" lp.grpcontno policyNo,");//保单号码
        tStrBSql.append(" '0' renewalTimes,");//续保次数
        tStrBSql.append(" '15'  endorsementType,");//保全类别
//        tStrBSql.append(" lp.edorType  endorsementType,");//保全类别
        tStrBSql.append(" lp.edorno endorsementNo,");//保全批单号
        tStrBSql.append(" (select edorappdate from lpedorapp where edoracceptno=lp.edorno) endorsementApplicationDate,");//保全申请日期
        tStrBSql.append(" lp.edorvalidate effectiveDate ,");//保全生效日期	
        tStrBSql.append(" (select sum(prem) from lcgrppol where grpcontno=lp.grpcontno ) currentPremium,");//当前保费
        tStrBSql.append(" (select count(1) from lcinsured where grpcontno=lp.grpcontno) effectiveInsuredNum");//有效被保险人数

        tStrBSql.append(" from lpGrpedoritem lp where 1=1 and edortype ='ZT' ");
        tStrBSql.append(" and edorNo='");
        tStrBSql.append(mEdorNo+"' with ur ");
      
        tResult = new ExeSQL().execSQL(tStrBSql.toString());

        return tResult;
    }
    
    
    
    
    //新增被保人数组详细信息
    private Element xinZengBaoRenInfo(String edorNo)
    {
        Element tRoot = new Element("AccidentGroupPolicyInsuredLogDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.accident.insurance.dto.AccidentGroupPolicyInsuredLogDTO");

        Element tTmpEle = null;

        SSRS tResPolicyUpcoverageLogInfo = xinZengBaoRenInfoSQL(edorNo);

        for (int idxPI = 1; idxPI <= tResPolicyUpcoverageLogInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyUpcoverageLogInfo.getRowData(idxPI);
            tTmpEle = new Element("CustomerNo");
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Name");
            tTmpEle.setText(tDataInfo[1]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Gender");
            tTmpEle.setText(tDataInfo[2]);
//            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Birthday");
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritType");
            tTmpEle.setText(tDataInfo[4]);
//            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritCode");
            tTmpEle.setText(tDataInfo[5]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Mobile");
            tTmpEle.setText(tDataInfo[6]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("OfferStatus");
            tTmpEle.setText(tDataInfo[7]);
//            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SubStartDate");
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SubEndDate");
            tTmpEle.setText(tDataInfo[9]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("InsuredType");
            tTmpEle.setText(tDataInfo[10]);
//            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SnOfMainInsured");
            tTmpEle.setText(tDataInfo[11]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RelationshipWithMainInsured");
            tTmpEle.setText(tDataInfo[12]);
//            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("AppointedBenefit");
            tTmpEle.setText(tDataInfo[13]);
//            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("InsuredPremium");
            tTmpEle.setText(tDataInfo[14]);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为"NUMBER"的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;            
            
            tTmpEle = new Element("InsuredCurrentPremium");
            tTmpEle.setText(tDataInfo[15]);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为"NUMBER"的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;  
            
            
           //受益人数组没有
            
            String insuredNo=tDataInfo[0];
            
            //新增被保人险种数组
            tTmpEle = xinZengBeiBaoRenXianZhongShuZu(insuredNo);
 	        if (tTmpEle != null)
 	        {
 	              tItemEle.addContent(tTmpEle);
 	        }
 	        tTmpEle = null;
            

        }

        return tRoot;
    }
    
    //新增被保人险种数组
    private Element xinZengBeiBaoRenXianZhongShuZu(String insuredNo)
    {
        Element tRoot = new Element("AccidentGroupPolicyUpcoverageLogDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.accident.insurance.dto.AccidentGroupPolicyUpcoverageLogDTO");

        Element tTmpEle = null;

        SSRS tResUnderWritingLogInfo = xinZengBeiBaoRenXianZhongShuZuSQL(insuredNo);

        for (int idxPI = 1; idxPI <= tResUnderWritingLogInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResUnderWritingLogInfo.getRowData(idxPI);
            
            tTmpEle = new Element("CoverageCode");
            tTmpEle.setText(tDataInfo[0]);
//            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ComCoverageCode");
            tTmpEle.setText(tDataInfo[1]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageName");
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageEffectiveDate");
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageExpireDate");
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("MainAttachedFlag");
            tTmpEle.setText(tDataInfo[5]);
//            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoveragePremium");
            tTmpEle.setText(tDataInfo[6]);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为"NUMBER"的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("CoverageSuminsured");
            tTmpEle.setText(tDataInfo[7]);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为"NUMBER"的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PaymentMethod");
            tTmpEle.setText(tDataInfo[8]);
//            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PaymentYears");
            tTmpEle.setText(tDataInfo[9]);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为"NUMBER"的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("SpecificBusiness");
            tTmpEle.setText(tDataInfo[10]);
//            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SpecificBusinessCode");
            tTmpEle.setText(tDataInfo[11]);
//            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为“进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            //新增人险种下承保责任数组
            String polNo=tDataInfo[12];
            tTmpEle = xinZenRenXianZhongXiaZeRenShuZu(polNo);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
           

            
        }

        return tRoot;
    }
    
    private Element xinZenRenXianZhongXiaZeRenShuZu(String cPolNo)
    {
        Element tRoot = new Element("AccidentGroupPolicyCoverageLogDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.accident.insurance.dto.AccidentGroupPolicyCoverageLogDTO");

        Element tTmpEle = null;

        SSRS tResPolicyInfo = xinZenRenXianZhongXiaZeRenShuZuSQL(cPolNo);

        for (int idxPI = 1; idxPI <= tResPolicyInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tResPolicyInfo.getRowData(idxPI);

            tTmpEle = new Element("LiabilityCode"); //平台责任分类
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityName"); //责任代码    
            tTmpEle.setText(tDataInfo[1]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityEffectiveDate"); //责任名称    
            tTmpEle.setText(tDataInfo[2]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("LiabilityExpireDate");
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CDate");//文档中类型需为“Date”的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityStatus");
            tTmpEle.setText(tDataInfo[4]);
//            tTmpEle.addAttribute("CData", "CBusCode");//文档中类型需为进行业务转换，即备注为《公用代码》”的字段
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityPremium");
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为"NUMBER"的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LimitAmount");
            tTmpEle.setText(tDataInfo[6]);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为"NUMBER"的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("WaitingPeriod");
            tTmpEle.setText(tDataInfo[7]);
            tTmpEle.addAttribute("CData", "CBigDecimal");// 文档中类型需为"NUMBER"的字段，请增加描述
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Remark");
            tTmpEle.setText(tDataInfo[8]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;


        }

        return tRoot;
    } 
    private SSRS xinZenRenXianZhongXiaZeRenShuZuSQL(String cPolNo)
    {
        SSRS tResult = null;
//        责任代码	LIABILITYCODE	VARCHAR2(60)	Y		
//        责任名称	LIABILITYNAME	VARCHAR2(200)	Y		
//        责任开始日期	LIABILITYEFFECTIVEDATE	DATETIME	Y		
//        责任终止日期	LIABILITYEXPIREDATE	DATETIME	Y		
//        责任状态	LIABILITYSTATUS	VARCHAR2(2)	Y	Y	《公用代码》1.1.13
//        责任期初保费	LIABILITYPREMIUM	NUMBER(20,2)			
//        责任期初保额	LIMITAMOUNT	NUMBER(20,2)	S		与备注字段不能同时为空
//        免责期	WAITINGPERIOD	NUMBER(8)			
//        备注	REMARK	VARCHAR2(600)	S		与责任期初保额字段不能同时为空


        StringBuffer tStrBSql = new StringBuffer();
        tStrBSql.append(" select ");
        tStrBSql.append(" lcd.DutyCode liabilityCode,");//责任代码
        tStrBSql.append(" lmd.DutyName liabilityName,");//责任名称
        tStrBSql.append(" lcd.GetStartDate liabilityEffectiveDate,");//责任开始日期	
        tStrBSql.append(" lcd.EndDate liabilityExpireDate,");//责任终止日期
        tStrBSql.append(" lcp.StateFlag liabilityStatus,");//责任状态
        tStrBSql.append(" lcd.Prem  liabilityPremium,");//责任保费
        tStrBSql.append(" lcd.Amnt  limitAmount,");//责任保额
        tStrBSql.append(" '0' waitingPeriod,");//免责期
        tStrBSql.append(" '' remark");//备注
        tStrBSql.append(" from LCPol lcp ");
        tStrBSql.append(" inner join LCDuty lcd on lcd.PolNo = lcp.PolNo ");
        tStrBSql.append(" inner join LMDuty lmd on lmd.DutyCode = lcd.DutyCode ");
        tStrBSql.append(" where 1 = 1 ");
        tStrBSql.append(" and lcp.polno = '" + cPolNo + "' ");

        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }
    
    
    private SSRS xinZengBeiBaoRenXianZhongShuZuSQL(String insuredNo)
    {
        SSRS tResult = null;
        StringBuffer tStrBSql = new StringBuffer();
        
//        平台险种代码	COVERAGECODE	VARCHAR2(5)	Y	Y	《公用代码》1.1.15
//        公司险种代码	COMCOVERAGECODE	VARCHAR2(60)	Y		
//        公司险种名称	COVERAGENAME	VARCHAR2(200)	Y		
//        险种起保日期	COVERAGEEFFECTIVEDATE	DATETIME	Y		
//        险种满期日期	COVERAGEEXPIREDATE	DATETIME	Y		
//        主附险性质	MAINATTACHEDFLAG	VARCHAR2(1)	Y	Y	《公用代码》1.1.11
//        险种期初保费	COVERAGEPREMIUM	NUMBER(20,2)	Y		
//        险种期初保额	COVERAGESUMINSURED	NUMBER(20,2)			
//        缴费方式	PAYMENTMETHOD	VARCHAR2(2)		Y	《公用代码》1.1.14
//        缴费年限	PAYMENTYEARS	NUMBER(4)			一年期及一年期以下的业务缴费年限为1；
//        特殊业务标识	SPECIFICBUSINESS	VARCHAR2(1)	Y	Y	《公用代码》1.1.7
//        特殊业务代码	SPECIFICBUSINESSCODE	VARCHAR2(5)	S	Y	《公用代码》1.1.6
//        如果特殊业务标识为1(是)，该字段必填
//        承保责任数组	GROUPCOVERAGE_ARRAY		Y		详见下面描述

			

        tStrBSql.append(" select ");//
        tStrBSql.append(" 'A01' coverageCode,");//平台险种代码
        tStrBSql.append(" riskcode comCoverageCode,");//公司险种代码
        tStrBSql.append(" (select riskname from lmriskapp where riskcode=a.riskcode ) coverageName,");//公司险种名称
        tStrBSql.append("  cvalidate coverageEffectiveDate,");//险种起保日期
        tStrBSql.append("  (select cinvalidate from lccont where contno=a.contno) coverageExpireDate,");//险种满期日期
        tStrBSql.append("  (case when exists (select 1 from lmriskapp where subriskflag='S' and riskcode=a.riskcode ) then '2' else '1' end ) mainAttachedFlag,");//主附险性质
        tStrBSql.append("  prem coveragePremium,");//险种期初保费
        tStrBSql.append("  amnt coverageSuminsured,");//险种期初保额
        tStrBSql.append("  paymode paymentMethod,");//缴费方式
        tStrBSql.append("  payyears paymentYears,");//缴费年限
        tStrBSql.append("  '0' specificBusiness,");//特殊业务标识
        tStrBSql.append("  'CK' specificBusinessCode,");//特殊业务代码
        tStrBSql.append("  polNo polNo ");//险种号     后边用
        tStrBSql.append(" from lcpol a where 1=1 ");//
        tStrBSql.append(" and insuredNo='");//
        tStrBSql.append(insuredNo+"'");


        String tStrSql = tStrBSql.toString();
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }
    //新增被保人数组详细信息
    private SSRS xinZengBaoRenInfoSQL(String edorNo)
    {
        SSRS tResult = null;
        StringBuffer tStrBSql = new StringBuffer();
        String tStrSql = tStrBSql.toString();
//        客户编码	CUSTOMERNO	VARCHAR2(60)	Y		公司内部识别被保险人的唯一标识，被保险人序号。
//        姓名	NAME	VARCHAR2(60)	Y		
//        性别	GENDER	VARCHAR2(1)		Y	《公用代码》1.1.3
//        出生日期	BIRTHDAY	DATE			
//        被保险人证件类别	CRITTYPE	VARCHAR2(2)	Y	Y	《公用代码》1.1.4
//        被保险人证件号码	CRITCODE	VARCHAR2(30)	Y		
//        手机号码	MOBILE	VARCHAR2(20)			供短信发送
//        要约状态	OFFERSTATUS	VARCHAR2(2)	Y	Y	《公用代码》1.1.13
//        被保险人责任开始日期	SUBSTARTDATE	DATETIME			
//        被保险人责任终止日期	SUBENDDATE	DATETIME			
//        被保险人类型	INSUREDTYPE	VARCHAR2(2)	Y	Y	《公用代码》1.1.9
//        主被保人序号	SNOFMAININSURED	VARCHAR2(30)	S		当被保险人类型为02(连带被保险人)时，该字段必填
//        与主被保人关系	RELATIONSHIPWITHMAININSURED	VARCHAR2(2)	S	Y	《公用代码》1.1.5
//        当被保险人类型为02(连带被保险人)时，该字段必填
//        是否指定受益人	APPOINTEDBENEFIT	VARCHAR2(1)	Y	Y	《公用代码》1.1.7
//        被保险人的期初保费	INSUREDPREMIUM	NUMBER(20,2)	Y		
//        被保险人的当前保费	INSUREDCURRENTPREMIUM	NUMBER(20,2)	Y		

//        受益人数组	GROUPBENEFIT_ARRAY		S		详见下面描述
//        如果是否指定受益人为1(是)，则该数组必填
//        承保险种数组	GROUPCOVERAGE_ARRAY		Y		
		
				
        tStrBSql.append(" select ");//
        tStrBSql.append(" insuredNo customerNo,");//客户编码
        tStrBSql.append(" (select name from lcinsured where insuredno=a.insuredno and contno=a.contno)  name,");//姓名
        tStrBSql.append(" (select (case when sex='0' then '1' when sex='1' then '2' else '9' end) from lcinsured where insuredno=a.insuredno and contno=a.contno)   gender,");//性别
        tStrBSql.append(" (select birthday from lcinsured where insuredno=a.insuredno and contno=a.contno) birthday,");//出生日期
        tStrBSql.append(" (select (case when idtype='0' then '01' when idtype='1' then '07' when idtype='2' then '04' else '99'    end)  from lcinsured where insuredno=a.insuredno and contno=a.contno) critType,");//被保险人证件类别
        tStrBSql.append(" (select idno from lcinsured where insuredno=a.insuredno and contno=a.contno) critCode,");//被保险人证件号码
        tStrBSql.append("  '' mobile,");//手机号码
        tStrBSql.append("  '1' offerStatus,");//要约状态
        tStrBSql.append("  (select cvalidate from lccont where contno=a.contno) subStartDate,");//被保险人责任开始日期
        tStrBSql.append("  (select cinvalidate from lccont where contno=a.contno) subEndDate,");//被保险人责任终止日期
        tStrBSql.append("  (select ( case when relationtomaininsured='00' then '01' else '02' end )from lcinsured where insuredno=a.insuredno and contno=a.contno)  insuredType,");//被保险人类型
        tStrBSql.append("  '2' snOfMainInsured,");//主被保人序号
        tStrBSql.append("  (select  relationtomaininsured from lcinsured where insuredno=a.insuredno and contno=a.contno) relationshipWithMainInsured,");//与主被保人关系
        tStrBSql.append("  '0' appointedBenefit ,");//是否指定受益人
        tStrBSql.append("  nvl((select sum(prem) from lcpol where contno=a.contno and insuredno=a.insuredNo),0) insuredPremium ,");//被保险人的期初保费
        tStrBSql.append("  nvl((select sum(prem) from lcpol where contno=a.contno and insuredno=a.insuredNo),0) insuredCurrentPremium ");//被保险人的当前保费

        tStrBSql.append(" from ljagetendorse a where 1=1 and  feeoperationtype='NI' ");
        tStrBSql.append(" and endorsementno='");
        tStrBSql.append(mEdorNo+"'  group by insuredNo,contno with ur ");
      
        tResult = new ExeSQL().execSQL(tStrBSql.toString());

        return tResult;
    }
    
    //团单增人保单信息数组
    private SSRS loadlccontNIInfoSQL(String cDataInfoKey)
    {
        SSRS tResult = null;
        StringBuffer tStrBSql = new StringBuffer();
        String tStrSql = tStrBSql.toString();
//        保单号码	POLICYNO	VARCHAR2(30)	Y		
//        续保次数	RENEWALTIMES	NUMBER(3)	Y		
//        保全类别	ENDORSEMENTTYPE	VARCHAR2(2)	Y	Y	《公用代码》1.1.12
//        保全批单号	ENDORSEMENTNO	VARCHAR2(30)	Y		
//        保全申请日期	ENDORSEMENTAPPLICATIONDATE	DATETIME	Y		
//        保全生效日期	EFFECTIVEDATE	DATETIME	Y		
//        当前保费	CURRENTPREMIUM	NUMBER(20,2)			
//        有效被保险人数	EFFECTIVEINSUREDNUM	NUMBER(8)	Y		
//        被保险人数组	GROUPINSURED_ARRAY		Y		
		
		
        tStrBSql.append(" select ");
        tStrBSql.append(" lp.grpcontno policyNo,");//保单号码
        tStrBSql.append(" '0' renewalTimes,");//续保次数
        tStrBSql.append(" '14'  endorsementType,");//保全类别
//        tStrBSql.append(" LP.EdorTpe  endorsementType,");//保全类别
        tStrBSql.append(" lp.edorno endorsementNo,");//保全批单号
        tStrBSql.append(" (select edorappdate from lpedorapp where edoracceptno=lp.edorno) endorsementApplicationDate,");//保全申请日期
        tStrBSql.append(" lp.edorvalidate effectiveDate ,");//保全生效日期	
        tStrBSql.append(" (select sum(prem) from lcgrppol where grpcontno=lp.grpcontno ) currentPremium,");//当前保费
        tStrBSql.append(" (select count(1) from lcinsured where grpcontno=lp.grpcontno) effectiveInsuredNum");//有效被保险人数

        tStrBSql.append(" from lpGrpedoritem lp where 1=1 and edortype ='NI' ");
        tStrBSql.append(" and edorNo='");
        tStrBSql.append(mEdorNo+"' with ur ");
      
        tResult = new ExeSQL().execSQL(tStrBSql.toString());

        return tResult;
    }
   
    /**
     * 调用前置机服务
     * @return
     */
    private boolean calService()
    {
        Document tDocument = formJDocMessage(mRoot, "Acc04", mDataInfoKey);
        JdomUtil.print(tDocument);

        Document tInStdXmlDoc = tDocument;

        ReqJKXPreService tJKXPreServlet = new ReqJKXPreService();
        String re = tJKXPreServlet.callService(tInStdXmlDoc);
//        JdomUtil.print(tReceiveXmlDoc);

        try
	        {
    		 DataTrackBL mDataTrackBL=new DataTrackBL();
    		 if(!mDataTrackBL.submitData(mDataInfoKey, "Acc04", re)){
    			 System.out.println("保全团单报送插入报送轨迹表出错");
    		 }
	        }
	        catch (Exception ex)
	        {
	            // @@错误处理
	            CError.buildErr(this,"保全团单报送插入报送轨迹表出错。");
	            
	        }                      
        return true;
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        System.out.println(szFunc + ": " + szErrMsg);

        CError cError = new CError();
        cError.moduleName = "BQJYXGRPJKXServiceImp";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    
    public static void main(String [] args)
    {
//    	String tDataInfoKey = "20110816000018&NI&G";//团单NI--
//    	String tDataInfoKey = "20110301000791&ZT&G";//团单ZT--
//    	String tDataInfoKey = "20110511000111&CT&G";//团单CT--
//    	String tDataInfoKey = "20110126000001&WT&G";//团单WT--   	
//    	String tDataInfoKey = "20110401000006&AC&G";//团单AC--    	
//    	String tDataInfoKey = "20110106000005&CM&G";//团单CM --人多
    	
    	
//    	String tDataInfoKey = "20110831000019&CT&G";//团单CT-- 00003031000002 成功
//    	String tDataInfoKey = "20110831000014&AC&G";//团单AC-- 00003031000002 成功
//    	String tDataInfoKey = "20110831000015&ZT&G";//团单ZT-- 00003550000001 成功
//    	String tDataInfoKey = "20110831000013&NI&G";//团单NI-- 00003550000001 成功
//    	String tDataInfoKey = "20110518000015&CM&G";//团单CM-- 00003550000001 成功
//    	String tDataInfoKey = "20110901000005&NI&G";//团单NI-- 00003550000001 成功
//    	String tDataInfoKey = "20110902000010&NI&G";//团单NI-- 00002703000003 成功
//    	String tDataInfoKey = "20110908000012&NI&G";//团单NI-- 00002703000003 成功
    	
    //二次改后	
//    	String tDataInfoKey = "20110922000002&CM&G";//团单CM-- 00004219000002 成功
//    	String tDataInfoKey = "20110922000005&NI&G";//团单NI-- 00004226000001 成功
    	
    	//00004231000001  
    	String tDataInfoKey = "20110928000003&CM&G";//团单CM-- 00004231000001 成功
//    	String tDataInfoKey = "20110928000004&NI&G";//团单NI-- 00004231000001 成功
    	
    	TransferData tTransferData = new TransferData();
    	tTransferData.setNameAndValue("DataInfoKey", tDataInfoKey);
    	VData tVData = new VData();
    	tVData.add(tTransferData);

    	new BQJYXGRPJKXServiceImp().callJKXService(tVData, "");
    }
}
