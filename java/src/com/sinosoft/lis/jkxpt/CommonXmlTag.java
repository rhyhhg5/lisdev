package com.sinosoft.lis.jkxpt;

public final class CommonXmlTag {
	final String PACKET = "PACKET";
	final String HEAD = "HEAD";
	final String BODY = "BODY";
	final String REQUEST_TYPE = "REQUEST_TYPE";
	final String RESPONSE_CODE = "RESPONSE_CODE";
	final String ERROR_MESSAGE = "ERROR_MESSAGE";
	final String TRANSACTION_NUM = "TRANSACTION_NUM";
	final String type = "type";
	final String version = "version";
}
