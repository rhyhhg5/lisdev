/**
 * 2010-11-18
 */
package com.sinosoft.lis.jkxpt;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.jkxpt.JKXService;
import com.sinosoft.lis.jkxpt.ReqJKXPreService;
import com.sinosoft.lis.yibaotong.JdomUtil;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class LPJKXServiceImp extends JKXService
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private TransferData mTransferData = null;

    /** 数据操作字符串 */
    private String mOperate = "";

    private String mDataInfoKey = "";

    private Element mRoot = null;

    private String mContType = null;

    public boolean callJKXService(VData cInputData, String cOperate)
    {
        // 获取入口参数 
        if (!getInputData(cInputData, cOperate))
        {
            return false;
        }
        // --------------------

        // 取数逻辑处理        
        //        mRoot = dealBusiXml(mPolicyNo);
        mRoot = reportBusiXml(mDataInfoKey);
        // --------------------

        // 调用前置机
        if (!calService())
        {
            return false;
        }
        // --------------------

        return true;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mDataInfoKey = (String) mTransferData.getValueByName("DataInfoKey");
        if (mDataInfoKey == null || mDataInfoKey.equals(""))
        {
            buildError("getInputData", "获取案件号失败。");
            return false;
        }

        return true;
    }

    private Element reportBusiXml(String cDataInfoKey)
    {
        Element tRoot = new Element("ClaimInfoDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.ClaimInfoDTO");

        Element tTmpEle = null;

        SSRS tResClaimInfo = loadClaimInfo(cDataInfoKey);
        for (int idxPI = 1; idxPI <= tResClaimInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            tTmpEle = new Element("UserName");
            tTmpEle.setText("picch_test");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Password");
            tTmpEle.setText("111111");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            //            Element tClaimRoot = new Element("ClaimDTO");
            //            tClaimRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.ClaimDTO");
            //            String tCaseNo = null;
            //            Element tClaimItemEle = new Element("Item");
            //            tClaimRoot.addContent(tClaimItemEle);

            //            tItemEle.addContent(tClaimRoot);

            String[] tDataInfo = tResClaimInfo.getRowData(idxPI);

            tTmpEle = new Element("ClaimNo");
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ReportNo");
            tTmpEle.setText(tDataInfo[1]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RegistrationNo");
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("BatchNo");
            tTmpEle.setText(tDataInfo[3]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("AccidentTime");
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("AccidentReason");
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("AccidentPlace");
            tTmpEle.setText(tDataInfo[6]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Accidentdes");
            tTmpEle.setText(tDataInfo[7]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("TreatmentAdvice");
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("HospitalCode");
            tTmpEle.setText(tDataInfo[22]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ClinicTimes");
            tTmpEle.setText(tDataInfo[9]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("HospitalizationTimes");
            tTmpEle.setText(tDataInfo[10]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ClinicAmount");
            tTmpEle.setText(tDataInfo[11]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ClinicDeductions");
            tTmpEle.setText(tDataInfo[12]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ClinicSocialsecurity");
            tTmpEle.setText(tDataInfo[13]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ClinicReimbursement");
            tTmpEle.setText(tDataInfo[14]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("HospitalizationAmount");
            tTmpEle.setText(tDataInfo[15]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("HospitalizationDeductions");
            tTmpEle.setText(tDataInfo[16]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("HospitalizationSocialsecurity");
            tTmpEle.setText(tDataInfo[17]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("HospitalizationReimbursement");
            tTmpEle.setText(tDataInfo[18]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ClaimAmount");
            tTmpEle.setText(tDataInfo[19]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("EndcaseDate");
            tTmpEle.setText(tDataInfo[20]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            //          获取报案申请人信息
            tTmpEle = getClmCaller(tDataInfo[3]);//根据rgtno进行查询
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

            //获取出险人信息
            tTmpEle = getClmClaimant(tDataInfo[21]);//根据客户号进行查询
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

            // --------------------
            //出险结果
            tTmpEle = getAccidentResult(tDataInfo[4], cDataInfoKey);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

            //          非医保实时收据收据
            tTmpEle = getClmNonmedicReceipt(cDataInfoKey);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

            //门诊医疗收据数组
            tTmpEle = getClinicReceipt(cDataInfoKey);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

            //          门诊特殊病收据数组
            tTmpEle = getSpecialReceipt(cDataInfoKey);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

            //          住院医疗收据收据
            tTmpEle = getHospitalReceipt(cDataInfoKey);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

            //          获取//保单赔付信息
            tTmpEle = getPolicyPayment(cDataInfoKey);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
        }

        return tRoot;
    }

    private Element getClmNonmedicReceipt(String cDataInfoKey)//非医保实时收据
    {
        Element tRoot = new Element("ClmNonmedicReceiptDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.ClmNonmedicReceiptDTO");

        Element tTmpEle = null;

        SSRS tHospitalReceipt = loadClmNonmedicReceipt(cDataInfoKey);

        for (int idxPI = 1; idxPI <= tHospitalReceipt.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tHospitalReceipt.getRowData(idxPI);

            tTmpEle = new Element("NonmedicareReceiptNo");
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("HospitalCode");
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("FeeHappenDate");
            tTmpEle.setText(tDataInfo[2]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("FeeEndDate");
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityClassification");
            tTmpEle.setText(tDataInfo[4]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("MedicareType");
            tTmpEle.setText(tDataInfo[5]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("FeeAmount");
            tTmpEle.setText(tDataInfo[6]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("FeeDeductibleAmount");
            tTmpEle.setText(tDataInfo[7]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ReimbusementAmount");
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            //医疗费用项目
            tTmpEle = getCostItem(cDataInfoKey);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

            //          疾病代码
            tTmpEle = getDiseaseCode(cDataInfoKey, "");
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
        }

        return tRoot;
    }

    private Element getHospitalReceipt(String cDataInfoKey)//住院收据
    {
        Element tRoot = new Element("HospitalReceiptDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.HospitalReceiptDTO");

        Element tTmpEle = null;

        SSRS tHospitalReceipt = loadHospitalReceipt(cDataInfoKey);

        for (int idxPI = 1; idxPI <= tHospitalReceipt.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tHospitalReceipt.getRowData(idxPI);

            tTmpEle = new Element("HospitalReceiptNo");
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("HospitalCode");
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityClassification");
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("HospitalDate");
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("DischardeDate");
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("HospitalStay");
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("HospitalMoney");
            tTmpEle.setText(tDataInfo[6]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("InsurancePayment");
            tTmpEle.setText(tDataInfo[7]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoOrdinationFund");
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Paymenta");
            tTmpEle.setText(tDataInfo[9]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PaymentFrom");
            tTmpEle.setText(tDataInfo[10]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("MutualPayment");
            tTmpEle.setText(tDataInfo[11]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CivilServants");
            tTmpEle.setText(tDataInfo[12]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("BesideInsurancePayment");
            tTmpEle.setText(tDataInfo[13]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("OwnExpense");
            tTmpEle.setText(tDataInfo[14]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Paymentb");
            tTmpEle.setText(tDataInfo[15]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("TotalOwnExpense");
            tTmpEle.setText(tDataInfo[16]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Cash");
            tTmpEle.setText(tDataInfo[17]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("AccountPayment");
            tTmpEle.setText(tDataInfo[18]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            //          医疗费用项目
            tTmpEle = getCostItem(cDataInfoKey);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

            //          疾病代码
            tTmpEle = getDiseaseCode(cDataInfoKey, "");
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
        }

        return tRoot;
    }

    private Element getClinicReceipt(String cDataInfoKey)//门诊收据
    {
        Element tRoot = new Element("ClinicReceiptDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.ClinicReceiptDTO");

        Element tTmpEle = null;

        SSRS tHospitalReceipt = loadClinicReceipt(cDataInfoKey);

        for (int idxPI = 1; idxPI <= tHospitalReceipt.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tHospitalReceipt.getRowData(idxPI);

            tTmpEle = new Element("ClinicReceiptNo");
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("HospitalCode");
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityClassification");
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ClinicDate");
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ClinicMoney");
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Cash");
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("AccountPayment");
            tTmpEle.setText(tDataInfo[6]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("AccountBalance");
            tTmpEle.setText(tDataInfo[7]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("MedicalPayment");
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("TotalMedicalPayment");
            tTmpEle.setText(tDataInfo[9]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("TotalAnnualPayment");
            tTmpEle.setText(tDataInfo[10]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("InsurancePayment");
            tTmpEle.setText(tDataInfo[11]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CliniclargePayment");
            tTmpEle.setText(tDataInfo[12]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RetirementPayment");
            tTmpEle.setText(tDataInfo[13]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("DisabilitySoldier");
            tTmpEle.setText(tDataInfo[14]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CivilServants");
            tTmpEle.setText(tDataInfo[15]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("BalanceAnnualPayment");
            tTmpEle.setText(tDataInfo[16]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("IndividualPayment");
            tTmpEle.setText(tDataInfo[17]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Paymenta");
            tTmpEle.setText(tDataInfo[18]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PaymentFrom");
            tTmpEle.setText(tDataInfo[19]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PaymentCap");
            tTmpEle.setText(tDataInfo[20]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Paymentb");
            tTmpEle.setText(tDataInfo[21]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("OwnExpense");
            tTmpEle.setText(tDataInfo[22]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            //          医疗费用项目
            tTmpEle = getCostItem(cDataInfoKey);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

            //          疾病代码
            tTmpEle = getDiseaseCode(cDataInfoKey, "");
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
        }

        return tRoot;
    }

    private Element getSpecialReceipt(String cDataInfoKey)//门诊特殊病收据
    {
        Element tRoot = new Element("SpecialReceiptDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.SpecialReceiptDTO");

        Element tTmpEle = null;

        SSRS tHospitalReceipt = loadSpecialReceipt(cDataInfoKey);

        for (int idxPI = 1; idxPI <= tHospitalReceipt.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tHospitalReceipt.getRowData(idxPI);

            tTmpEle = new Element("SpecialReceiptNo");
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("HospitalCode");
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityClassification");
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ClinicDate");
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SpecialMoney");
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("MedicalPayment");
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("InsurancePayment");
            tTmpEle.setText(tDataInfo[6]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoOrdinationFund");
            tTmpEle.setText(tDataInfo[7]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("MutualPayment");
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RetirementPayment");
            tTmpEle.setText(tDataInfo[9]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CivilServants");
            tTmpEle.setText(tDataInfo[10]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("DisabilitySoldier");
            tTmpEle.setText(tDataInfo[11]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Cash");
            tTmpEle.setText(tDataInfo[12]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("IndividualPayment");
            tTmpEle.setText(tDataInfo[13]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Paymenta");
            tTmpEle.setText(tDataInfo[14]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PaymentFrom");
            tTmpEle.setText(tDataInfo[15]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PaymentCap");
            tTmpEle.setText(tDataInfo[16]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Paymentb");
            tTmpEle.setText(tDataInfo[17]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("OwnExpense");
            tTmpEle.setText(tDataInfo[18]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("AccountPayment");
            tTmpEle.setText(tDataInfo[19]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("YearCoOrdinationFund");
            tTmpEle.setText(tDataInfo[20]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("YearMutualPayment");
            tTmpEle.setText(tDataInfo[21]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("AccountBalance");
            tTmpEle.setText(tDataInfo[22]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            //          医疗费用项目
            tTmpEle = getCostItem(cDataInfoKey);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

            //          疾病代码
            tTmpEle = getDiseaseCode(cDataInfoKey, "");
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
        }

        return tRoot;
    }

    private Element getDiseaseCode(String aCaseNo, String aCaseRelaNo)//疾病代码
    {
        Element tRoot = new Element("DiseaseCodeDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.DiseaseCodeDTO");

        Element tTmpEle = null;

        SSRS tDiseaseCode = loadDiseaseCode(aCaseNo, aCaseRelaNo);

        for (int idxPI = 1; idxPI <= tDiseaseCode.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tDiseaseCode.getRowData(idxPI);

            tTmpEle = new Element("DiseaseCode");
            tTmpEle.setText(tDataInfo[0]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

        }

        return tRoot;
    }

    private Element getCostItem(String mainfeeno)//医疗费用项目
    {
        Element tRoot = new Element("CostItemDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.CostItemDTO");

        Element tTmpEle = null;

        SSRS tDiseaseCode = loadCostItem(mainfeeno);

        for (int idxPI = 1; idxPI <= tDiseaseCode.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tDiseaseCode.getRowData(idxPI);

            tTmpEle = new Element("DiseaseCode");
            tTmpEle.setText(tDataInfo[0]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

        }

        return tRoot;
    }

    private Element getAccidentResult(String accdate, String caseno)//出险结果
    {
        Element tRoot = new Element("AccidentResultDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.AccidentResultDTO");

        Element tTmpEle = null;

        SSRS tAccidentResult = loadAccidentResult(accdate, caseno);

        for (int idxPI = 1; idxPI <= tAccidentResult.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tAccidentResult.getRowData(idxPI);

            tTmpEle = new Element("AccidentResult");//出险结果
            tTmpEle.setText(tDataInfo[0]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("AccidentResultDate");//出险结果时间
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

        }

        return tRoot;
    }

    private Element getClmCaller(String tRgtNo)//报案申请人信息
    {
        Element tRoot = new Element("ClmCallerDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.ClmCallerDTO");

        Element tTmpEle = null;

        SSRS tClmClaimant = loadClmCaller(tRgtNo);

        for (int idxPI = 1; idxPI <= tClmClaimant.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tClmClaimant.getRowData(idxPI);

            tTmpEle = new Element("Name");//名字
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Gender");//性别
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Birthday");
            tTmpEle.setText(tDataInfo[2]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritType");//证件类型
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritCode");//证件代码
            tTmpEle.setText(tDataInfo[4]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RelationshipWithClaimant");//与出险人关系
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ContactNo");//联系电话
            tTmpEle.setText(tDataInfo[6]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ContactAddress");//联系地址
            tTmpEle.setText(tDataInfo[7]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

        }

        return tRoot;
    }

    private Element getClmClaimant(String tCustomerNo)//出险人信息
    {
        Element tRoot = new Element("ClmClaimantDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.ClmClaimantDTO");

        Element tTmpEle = null;

        SSRS tClmClaimant = loadClmClaimant(tCustomerNo);

        for (int idxPI = 1; idxPI <= tClmClaimant.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tClmClaimant.getRowData(idxPI);

            tTmpEle = new Element("Name");//名字
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Gender");//性别
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Birthday");
            tTmpEle.setText(tDataInfo[2]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritType");//证件类型
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritCode");//证件代码
            tTmpEle.setText(tDataInfo[4]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ClaimantType");//出险人保单身份
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            //            tTmpEle = getCoveragePayment(cDataInfoKey);
            //            if (tTmpEle != null)
            //            {
            //                tItemEle.addContent(tTmpEle);
            //            }
            //            tTmpEle = null;
        }

        return tRoot;
    }

    private SSRS loadClmCaller(String tRgtNo)//报案申请人信息
    {
        SSRS tResult = null;
        //        String tStrSql="select rgtantname Name,RgtantSex Gender,'' Birthday,"+
        //        "case idtype when '0' then '01' when '1' then '51' when '2' then '04' else '99' end CritType,"+
        //        "idno CritCode,case relation when '00' then '00' when '01' then '01' when '02' then '01' when '04' then '02' when '05' then '02' when '03' then '03' " +
        //        "when '12' then '15' when '28' then '06' when '29' then '07' when '19' then '08' when '20' then '08' when '22' then '08'when '23' then '08'" +
        //        "when '21' then '09' when '24' then '09' when '27' then '12' else '99' end RelationshipWithClaimant,RgtantPhone ContactNo,RgtantAddress ContactAddress " +
        //        "from llregister where rgtno='"+tRgtNo+"' with ur";//
        String tStrSql = "select rgtantname Name,RgtantSex Gender,'' Birthday," + "idtype CritType,"
                + "idno CritCode,relation RelationshipWithClaimant,RgtantPhone ContactNo,RgtantAddress ContactAddress "
                + "from llregister where rgtno='" + tRgtNo + "' and 1=2 with ur";//暂时不取数
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadClmClaimant(String tCustomerNo)//出险人信息
    {
        SSRS tResult = null;
        //        String tStrSql="select name Name,case sex when '0' then '1' when '1' then '2' else '9' end Gender,birthday Birthday,"+
        //        "case idtype when '0' then '01' when '1' then '51' when '2' then '04' else '99' end CritType,"+
        //        "idno CritCode,'2' ClaimantType from lcinsured where insuredno='"+tCustomerNo+"' fetch first 1 rows only with ur";//出险人保单身份ClaimantType暂时全部默认设置为2被保人
        String tStrSql = "select name Name,sex Gender,birthday Birthday,idtype CritType,idno CritCode,'2' ClaimantType from lcinsured where insuredno='"
                + tCustomerNo
                + "' union select name Name,sex Gender,birthday Birthday,idtype CritType,idno CritCode,'2' ClaimantType from lbinsured where insuredno='"
                + tCustomerNo + "' fetch first 1 rows only with ur";//出险人保单身份ClaimantType暂时全部默认设置为2被保人
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private Element getPolicyPayment(String cDataInfoKey)//保单赔付信息
    {
        Element tRoot = new Element("PolicyPaymentDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.PolicyPaymentDTO");

        Element tTmpEle = null;

        SSRS tPolicyPayment = loadPolicyPayment(cDataInfoKey);

        for (int idxPI = 1; idxPI <= tPolicyPayment.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tPolicyPayment.getRowData(idxPI);

            tTmpEle = new Element("PolicySequenceNo");
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PolicyNo");
            tTmpEle.setText(tDataInfo[1]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CustomerNo");
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ClaimOpinion");
            tTmpEle.setText(tDataInfo[3]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ClaimConclusionCode");
            tTmpEle.setText(tDataInfo[4]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("TotalPaymentAmount");
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = getCoveragePayment(cDataInfoKey, tDataInfo[1]);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
        }

        return tRoot;
    }

    private Element getCoveragePayment(String cDataInfoKey, String tContNo)//保单责任赔付信息
    {
        Element tRoot = new Element("CoveragePaymentDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.CoveragePaymentDTO");

        Element tTmpEle = null;

        SSRS tPolicyPayment = loadCoveragePayment(cDataInfoKey, tContNo);

        for (int idxPI = 1; idxPI <= tPolicyPayment.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tPolicyPayment.getRowData(idxPI);

            tTmpEle = new Element("CoverageType");
            tTmpEle.setText(tDataInfo[0]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageCode");
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ComcoverageCode");
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageName");
            tTmpEle.setText(tDataInfo[3]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityCode");
            tTmpEle.setText(tDataInfo[4]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ClaimOpinion");
            tTmpEle.setText(tDataInfo[5]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ClaimConclusionCode");
            tTmpEle.setText(tDataInfo[6]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PaymentAmount");
            tTmpEle.setText(tDataInfo[7]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = getDiseaseCode(cDataInfoKey, tDataInfo[8]);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
        }
        return tRoot;
    }

    private SSRS loadCoveragePayment(String cDataInfoKey, String cContNo)//保单责任赔付信息,平台险种代码CoverageCode暂时先设置为20医疗保险,平台责任代码LiabilityCode先设置为99其他
    {
        SSRS tResult = null;
        //        String tStrSql="select case b.riskperiod when 'L' then '1' when 'M' then '2' when 'S' then '2' else '' end CoverageType," +
        //        		"'20' CoverageCode,a.riskcode ComcoverageCode,b.riskname CoverageName,'99' LiabilityCode," +
        //        		"a.givetypedesc ClaimOpinion,'' ClaimConclusionCode,a.realpay PaymentAmount from llclaimdetail a,lmriskapp b " +
        //        		"where a.caseno='"+cDataInfoKey+"' and (a.grpcontno='"+cContNo+"' or a.contno='"+cContNo+"') and a.riskcode=b.riskcode and b.risktype1 = '1'";
        String tStrSql = "select b.riskperiod CoverageType,"
                + "a.riskcode CoverageCode,a.riskcode ComcoverageCode,b.riskname CoverageName,a.dutycode LiabilityCode,"
                + "a.givetypedesc ClaimOpinion,'' ClaimConclusionCode,a.realpay PaymentAmount,a.caserelano from llclaimdetail a,lmriskapp b "
                + "where a.caseno='" + cDataInfoKey + "' and (a.grpcontno='" + cContNo + "' or a.contno='" + cContNo
                + "') and a.riskcode=b.riskcode and b.risktype1 = '1'";
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadClmNonmedicReceipt(String cDataInfoKey)//非医保实时收据
    {
        SSRS tResult = null;
        String tStrSql = "select mainfeeno NonmedicareReceiptNo,hospitalcode HospitalCode,'' FeeHappenDate,'' FeeEndDate,'' LiabilityClassification,'' MedicareType,"
                + "'' FeeAmount,'' FeeDeductibleAmount,'' ReimbusementAmount from llfeemain where caseno='"
                + cDataInfoKey + "' and feeatti in ('0','3') with ur";

        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadHospitalReceipt(String cDataInfoKey)//住院收据
    {
        SSRS tResult = null;
        String tStrSql = "select mainfeeno HospitalReceiptNo,hospitalcode HospitalCode,'99' LiabilityClassification,hospstartdate HospitalDate,hospenddate DischardeDate,realhospdate HospitalStay,"
                + "'' HospitalMoney,'' InsurancePayment,'' CoOrdinationFund,'' Paymenta,'' PaymentFrom,'' MutualPayment,'' CivilServants,"
                + "'' BesideInsurancePayment,'' OwnExpense,'' Paymentb,'' TotalOwnExpense,'' Cash,'' AccountPayment from llfeemain where caseno='"
                + cDataInfoKey + "' and feetype='2' and feeatti in ('1','2','4') with ur";

        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadClinicReceipt(String cDataInfoKey)//门诊收据
    {
        SSRS tResult = null;
        String tStrSql = "select mainfeeno ClinicReceiptNo,hospitalcode HospitalCode,'' LiabilityClassification,'' ClinicDate,'' ClinicMoney,'' Cash,'' AccountPayment,"
                + "'' AccountBalance,'' MedicalPayment,'' TotalMedicalPayment,'' TotalAnnualPayment,'' InsurancePayment,"
                + "'' CliniclargePayment,'' RetirementPayment,'' DisabilitySoldier,'' CivilServants,'' BalanceAnnualPayment,"
                + "'' IndividualPayment,'' Paymenta,'' PaymentFrom,'' PaymentCap,'' Paymentb,'' OwnExpense "
                + "from llfeemain where caseno='"
                + cDataInfoKey
                + "' and feetype='1' and feeatti in ('1','2','4') with ur";

        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadSpecialReceipt(String cDataInfoKey)//门诊特殊病收据
    {
        SSRS tResult = null;
        String tStrSql = "select mainfeeno SpecialReceiptNo,hospitalcode HospitalCode,'' LiabilityClassification,'' ClinicDate,'' SpecialMoney,'' MedicalPayment,"
                + "'' InsurancePayment,'' CoOrdinationFund,'' MutualPayment,'' RetirementPayment,'' CivilServants,'' DisabilitySoldier,'' Cash,"
                + "'' IndividualPayment,'' Paymenta,'' PaymentCap,'' Paymentb,'' OwnExpense,'' AccountPayment,"
                + "'' YearCoOrdinationFund,'' YearMutualPayment,'' AccountBalance from llfeemain where caseno='"
                + cDataInfoKey + "' and feetype='3' and feeatti in ('1','2','4') with ur";

        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadDiseaseCode(String aCaseNo, String aCaseRelaNo)//疾病代码
    {
        SSRS tResult = null;

        String tCaseRelaSQL = "";

        if (aCaseRelaNo != null && !"".equals(aCaseRelaNo))
        {
            tCaseRelaSQL = " and caserelano = '" + aCaseRelaNo + "'";
        }

        String tStrSql = "select DiseaseCode DiseaseCode from llcasecure where caseno='" + aCaseNo + "' "
                + tCaseRelaSQL + " with ur";//

        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadCostItem(String mainfeeno)// 医疗费用项目
    {
        SSRS tResult = null;
        String tStrSql = "select 1 from llcasecure where 1=2 fetch first 1 rows only with ur";//暂时不取数据

        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadAccidentResult(String accdate, String caseno)//出险结果
    {
        SSRS tResult = null;
        //        String tStrSql="select case reasoncode when '05' then '01' when '08' then '03' when '04' then '04' when '07' then '05' when '06' then '06'" +
        //        		" else '99' end AccidentResult,'"+accdate+"' from llappclaimreason where caseno='"+caseno+"'";//出险结果时间按事件发生日期算
        String tStrSql="select reasoncode AccidentResult,'"+accdate+"' AccidentResultDate from llappclaimreason where caseno='"+caseno+"' fetch first 1 rows only with ur";//出险结果时间按事件发生日期算
        tResult = new ExeSQL().execSQL(tStrSql);
        
        if (tResult == null ||tResult.getMaxRow()==0) {
        String tStrNewSql = "select '' AccidentResult,'" + accdate
                + "' AccidentResultDate from dual where 1=1 ";//出险结果时间按事件发生日期算
        tResult = new ExeSQL().execSQL(tStrNewSql);
        }

        return tResult;
    }

    private SSRS loadPolicyPayment(String cDataInfoKey)
    {
        SSRS tResult = null;
        //        String tCaseNo="C0000060822000003";
        String tStrSql = "select distinct '' PolicySequenceNo,case grpcontno when '00000000000000000000' then contno else grpcontno end PolicyNo,"
                + "(select customerno from llcase where caseno=llclaimdetail.caseno fetch first 1 rows only) CustomerNo,'' ClaimOpinion,'' ClaimConclusionCode,'' TotalPaymentAmount FROM "
                + "llclaimdetail where caseno='"
                + cDataInfoKey
                + "' and exists(select 1 from lmriskapp where riskcode=llclaimdetail.riskcode and risktype1 = '1') with ur";
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadClaimInfo(String cDataInfoKey)
    {
        SSRS tResult = null;
        //        String tCaseNo="C1100080116000009";
        StringBuffer tStrBuSql = new StringBuffer();
        //        tStrBuSql.append("select a.caseno ClaimNo,'' ReportNo,a.caseno RegistrationNo,a.rgtno BatchNo,c.accdate AccidentTime,");
        //        tStrBuSql.append("coalesce(c.accidenttype,'9') AccidentReason,'' AccidentPlace,'' Accidentdes,");
        //        tStrBuSql.append("(select case givetype when '1' then '01' when '3' then '04' when '4' then '02' when '5' then '05' else '99' end");
        //        tStrBuSql.append(" from llclaim where caseno=a.caseno) TreatmentAdvice,'' ClinicTimes,'' HospitalizationTimes,'' ClinicAmount,");
        //        tStrBuSql.append("'' ClinicDeductions,'' ClinicSocialsecurity,'' ClinicReimbursement,'' HospitalizationAmount,'' HospitalizationDeductions,");
        //        tStrBuSql.append("'' HospitalizationSocialsecurity,'' HospitalizationReimbursement,");
        //        tStrBuSql.append("(select coalesce(sum(realpay),0) from llclaimdetail where caseno=a.caseno) ClaimAmount,");
        //        tStrBuSql.append("a.endcasedate  EndcaseDate,a.customerno ");
        //        tStrBuSql.append("from llcase a,llcaserela b,llsubreport c where a.caseno=b.caseno and b.subrptno=c.subrptno and a.caseno='"+cDataInfoKey+"'" +
        //        		" fetch first 1 rows only");
        tStrBuSql
                .append("select a.caseno ClaimNo,'' ReportNo,a.caseno RegistrationNo,a.rgtno BatchNo,c.accdate AccidentTime,");
        tStrBuSql.append("c.accidenttype AccidentReason,'' AccidentPlace,'' Accidentdes,");
//        tStrBuSql.append("'01' TreatmentAdvice,'' ClinicTimes,'' HospitalizationTimes,'' ClinicAmount,");
        tStrBuSql.append("(select givetype  from llclaim where caseno=a.caseno fetch first 1 rows only) TreatmentAdvice,'' ClinicTimes,'' HospitalizationTimes,'' ClinicAmount,");
        //        tStrBuSql.append(" from llclaim where caseno=a.caseno) TreatmentAdvice,'' ClinicTimes,'' HospitalizationTimes,'' ClinicAmount,");
        tStrBuSql
                .append("'' ClinicDeductions,'' ClinicSocialsecurity,'' ClinicReimbursement,'' HospitalizationAmount,'' HospitalizationDeductions,");
        tStrBuSql.append("'' HospitalizationSocialsecurity,'' HospitalizationReimbursement,");
        tStrBuSql.append("(select coalesce(sum(realpay),0) from llclaimdetail where caseno=a.caseno) ClaimAmount,");
        tStrBuSql.append("a.endcasedate  EndcaseDate,a.customerno, ");
        tStrBuSql
                .append("(select hospitalcode from llfeemain where caseno=a.caseno and caserelano=b.caserelano fetch first 1 rows only) HospitalCode ");
        tStrBuSql
                .append("from llcase a,llcaserela b,llsubreport c where a.caseno=b.caseno and b.subrptno=c.subrptno and a.caseno='"
                        + cDataInfoKey + "'" + " fetch first 1 rows only");

        tResult = new ExeSQL().execSQL(tStrBuSql.toString());
        return tResult;
    }

    /**
     * 调用前置机服务
     * @return
     */
    private boolean calService()
    {
        Document tDocument = formJDocMessage(mRoot, "03", mDataInfoKey);
        JdomUtil.print(tDocument);

        Document tInStdXmlDoc = tDocument;

        ReqJKXPreService tJKXPreServlet = new ReqJKXPreService();
        Document tReceiveXmlDoc = tJKXPreServlet.callService(tInStdXmlDoc);
        JdomUtil.print(tReceiveXmlDoc);

        return true;
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        System.out.println(szFunc + ": " + szErrMsg);

        CError cError = new CError();
        cError.moduleName = "AddCertifyListBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 测试主程序
     * 
     * @param args
     */
    public static void main(String[] args)
    {
        LPJKXServiceImp tLPJKXServiceImp = new LPJKXServiceImp();
        VData tVData = new VData();
        TransferData mTransferData = new TransferData();
        mTransferData.setNameAndValue("DataInfoKey", "C1100101220000001");
        tVData.add(mTransferData);
        tLPJKXServiceImp.callJKXService(tVData, null);
    }
}
