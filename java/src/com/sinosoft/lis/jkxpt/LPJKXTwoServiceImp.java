/**
 * 2010-11-18
 */
package com.sinosoft.lis.jkxpt;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.jkxpt.JKXService;
import com.sinosoft.lis.jkxpt.ReqJKXPreService;
import com.sinosoft.lis.jkxpt.qy.QYJKXTwoServiceImp;
import com.sinosoft.lis.yibaotong.JdomUtil;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class LPJKXTwoServiceImp extends JKXService
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private TransferData mTransferData = null;

    /** 数据操作字符串 */
    private String mOperate = "";

    private String mDataInfoKey = "";

    private Element mRoot = null;

    private String mContType = null;

    public boolean callJKXService(VData cInputData, String cOperate)
    {
        // 获取入口参数 
        if (!getInputData(cInputData, cOperate))
        {
            return false;
        }
        // --------------------

        // 取数逻辑处理        
        //        mRoot = dealBusiXml(mPolicyNo);
        mRoot = reportBusiXml(mDataInfoKey);
        // --------------------

        // 调用前置机
        if (!calService())
        {
            return false;
        }
        // --------------------

        return true;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mDataInfoKey = (String) mTransferData.getValueByName("DataInfoKey");
        if (mDataInfoKey == null || mDataInfoKey.equals(""))
        {
            buildError("getInputData", "获取案件号失败。");
            return false;
        }

        return true;
    }

    private Element reportBusiXml(String cDataInfoKey)
    {
        Element tRoot = new Element("ClaimInfoDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.ClaimInfoDTO");

        Element tTmpEle = null;

        SSRS tResClaimInfo = loadClaimInfo(cDataInfoKey);
        
        for (int idxPI = 1; idxPI <= tResClaimInfo.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);
            
            tTmpEle = new Element("UserName");
            tTmpEle.setText("picch_test");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Password");
            tTmpEle.setText("111111");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            String[] tDataInfo = tResClaimInfo.getRowData(idxPI);

            tTmpEle = new Element("ClaimNo");
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            //---------------------- start ----------------------
            
            tTmpEle = new Element("ReportDate");
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            //---------------------- end ------------------------
            
            tTmpEle = new Element("ReportNo");
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            //---------------------- start ----------------------
            
            tTmpEle = new Element("RegistrationDate");
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            //---------------------- end ------------------------
            
            tTmpEle = new Element("RegistrationNo");
            tTmpEle.setText(tDataInfo[4]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("BatchNo");
            tTmpEle.setText(tDataInfo[5]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("AccidentTime");
            tTmpEle.setText(tDataInfo[6]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("AccidentReason");
            if(tDataInfo[7].equals("")){
            	tTmpEle.setText("9"); //其他
            }else{
            	tTmpEle.setText(tDataInfo[7]);
                tTmpEle.addAttribute("CData", "CBusCode");
            }
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            //---------------------- start ----------------------
            
            tTmpEle = new Element("Address1");
            tTmpEle.setText(tDataInfo[8]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            
            tTmpEle = new Element("Address2");
            tTmpEle.setText(tDataInfo[9]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            
            tTmpEle = new Element("Address3");
            tTmpEle.setText(QYJKXTwoServiceImp.getTransAddress(tDataInfo[10]));
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            if(tDataInfo[11].equals("")){
            	String sql = "select address from ldcom where comcode='86110000' with ur";
            	tDataInfo[11] = new ExeSQL().getOneValue(sql);
            }
            tTmpEle = new Element("Address4");
            tTmpEle.setText(tDataInfo[11]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            //---------------------- end ------------------------

            tTmpEle = new Element("Accidentdes");
            tTmpEle.setText(tDataInfo[12]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            //---------------------- start ----------------------
            
            tTmpEle = new Element("ClaimInvestigation");
            tTmpEle.setText(tDataInfo[13]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            
            tTmpEle = new Element("RemoteClaimInvestigation");
            if(tDataInfo[14].equals("1")){
            	tTmpEle.setText("04");
            }else{
            	tTmpEle.setText("01");
            }
            if(tDataInfo[13].equals("0")){
            	tTmpEle.setText("");//是否经过理赔调查为“否”； “异地理赔”字段应为空项
            }
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            
            tTmpEle = new Element("InvestigationStartDate");
            tTmpEle.setText(tDataInfo[15]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            
            tTmpEle = new Element("InvestigationEndDate");
            tTmpEle.setText(tDataInfo[16]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            //---------------------- end ------------------------
            tTmpEle = new Element("TreatmentAdvice");
            tTmpEle.setText(tDataInfo[17]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            //---------------------- start ----------------------
            
            tTmpEle = new Element("CriticalIllnessNo");
            tTmpEle.setText(tDataInfo[18]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("InjuryReasonCode");
            tTmpEle.setText(tDataInfo[19]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            //---------------------- end ------------------------
            
            tTmpEle = new Element("ClaimAmount");
            tTmpEle.setText(tDataInfo[20]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("EndcaseDate");
            tTmpEle.setText(tDataInfo[21]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
//           #2994 增加索赔日期 
            tTmpEle = new Element("ClaimDate");
            tTmpEle.setText(tDataInfo[24]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            //---------------------- start ----------------------
            
            tTmpEle = new Element("Status");
            tTmpEle.setText(tDataInfo[22]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            //---------------------- end ------------------------
            
            //获取报案申请人信息
            tTmpEle = getClmCaller(tDataInfo[5]);//根据rgtno进行查询
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
            
            //获取出险人信息
            tTmpEle = getClmClaimant(tDataInfo[23]);//根据客户号进行查询
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

                   // --------------------
            //出险结果
            tTmpEle = getAccidentResult(tDataInfo[6], cDataInfoKey);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

            //------------- 新数组开始zyg --------------//
//            //医保门诊收据数组
            tTmpEle = getClinicReceipt(cDataInfoKey); 
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
//
//            //门诊特殊病收据数组
            tTmpEle = getSpecialReceipt(cDataInfoKey); 
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

            //医保住院收据数组
            tTmpEle = getHospitalReceipt(cDataInfoKey); 
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
            //医保审批表收据数组
            tTmpEle = getApprovalReceipt(cDataInfoKey);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;	
            
//            //生育门诊收据数组
//            tTmpEle = getBirthClinicReceipt(cDataInfoKey);
//            if (tTmpEle != null)
//            {
//                tItemEle.addContent(tTmpEle);
//            }
//            tTmpEle = null;	
//            
//            //新农合-住院收据数组
//            tTmpEle = getCooperationHospitalReceipt(cDataInfoKey);
//            if (tTmpEle != null)
//            {
//                tItemEle.addContent(tTmpEle);
//            }
//            tTmpEle = null;	
//         //分割单收据数组  
//            tTmpEle = getSegmentationReceipt(cDataInfoKey);
//            if (tTmpEle != null)
//            {
//                tItemEle.addContent(tTmpEle);
//            }
//            tTmpEle = null;	
            
            
            //普通门诊收据数组
            tTmpEle = getOrdinaryClinicReceipt(cDataInfoKey);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;	
            //普通住院收据数组
            tTmpEle = getOrdinaryHospitalReceipt(cDataInfoKey);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;	
            
            
            //------------- 新数组结束zyg --------------//
            //保单赔付信息
            tTmpEle = getPolicyPayment(cDataInfoKey);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
            
            
            
            
        }

        return tRoot;
    }

    private Element getHospitalReceipt(String cDataInfoKey)//住院收据
    {
        Element tRoot = new Element("HospitalReceiptDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.HospitalReceiptDTO");

        Element tTmpEle = null;

        SSRS tHospitalReceipt = loadHospitalReceipt(cDataInfoKey);

        for (int idxPI = 1; idxPI <= tHospitalReceipt.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tHospitalReceipt.getRowData(idxPI);

            tTmpEle = new Element("ReceiptNo");
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("HospitalCode");
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("HospitalDate");
            tTmpEle.setText(tDataInfo[2]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("DischardeDate");
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("HospitalStay");
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("HospitalMoney");
            if(tDataInfo[5].equals("")||tDataInfo[5].equals("null")){
            	tTmpEle.setText("0.0");
            }else{
            	tTmpEle.setText(tDataInfo[5]);
            }
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("MedicalPayment");
            tTmpEle.setText(tDataInfo[6]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("InsurancePayment");
            tTmpEle.setText(tDataInfo[7]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoOrdinationFund");
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PaymentA");
            tTmpEle.setText(tDataInfo[9]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PaymentFrom");
            tTmpEle.setText(tDataInfo[10]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("MutualPayment");
            tTmpEle.setText(tDataInfo[11]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("CivilServants");
            tTmpEle.setText(tDataInfo[12]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("BesideInsurancePayment");
            tTmpEle.setText(tDataInfo[13]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("OwnExpense");
            tTmpEle.setText(tDataInfo[14]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("PaymentB");
            tTmpEle.setText(tDataInfo[15]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            
            tTmpEle = new Element("TotalOwnExpense");
            tTmpEle.setText(tDataInfo[16]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Cash");
            tTmpEle.setText(tDataInfo[17]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;


            tTmpEle = new Element("AccountPayment");
            tTmpEle.setText(tDataInfo[18]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ReceiptClaimAmount");
            tTmpEle.setText(tDataInfo[19]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Operation");
            tTmpEle.setText(tDataInfo[20]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            String mainfeeno = tDataInfo[21];
            //医疗费用项目
            tTmpEle = getCostItem(cDataInfoKey,mainfeeno);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
            //西医代码数组 
            tTmpEle = getWesternMedicone(cDataInfoKey);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
            
            //手术代码数组
            tTmpEle = getOperationCode(cDataInfoKey);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
        }

        return tRoot;
    }

    private Element getClinicReceipt(String cDataInfoKey)//门诊收据
    {
        Element tRoot = new Element("ClinicReceiptDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.ClinicReceiptDTO");

        Element tTmpEle = null;

        SSRS tHospitalReceipt = loadClinicReceipt(cDataInfoKey);

        for (int idxPI = 1; idxPI <= tHospitalReceipt.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tHospitalReceipt.getRowData(idxPI);

            tTmpEle = new Element("ReceiptNo");
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("HospitalCode");
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ClinicDate");
            tTmpEle.setText(tDataInfo[2]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ClinicMoney");
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Cash");
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("AccountPayment");
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("AccountBalance");
            tTmpEle.setText(tDataInfo[6]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("MedicalPayment");
            tTmpEle.setText(tDataInfo[7]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("TotalMedicalPayment");
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("TotalAnnualPayment");
            tTmpEle.setText(tDataInfo[9]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("InsurancePayment");
            tTmpEle.setText(tDataInfo[10]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CliniclargePayment");
            tTmpEle.setText(tDataInfo[11]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RetirementPayment");
            tTmpEle.setText(tDataInfo[12]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("DisabilitySoldier");
            tTmpEle.setText(tDataInfo[13]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CivilServants");
            tTmpEle.setText(tDataInfo[14]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("AnnualPayment");
            tTmpEle.setText(tDataInfo[15]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("IndividualPayment");
            tTmpEle.setText(tDataInfo[16]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PaymentA");
            tTmpEle.setText(tDataInfo[17]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PaymentFrom");
            tTmpEle.setText(tDataInfo[18]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PaymentCap");
            tTmpEle.setText(tDataInfo[19]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PaymentB");
            tTmpEle.setText(tDataInfo[20]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("OwnExpense");
            tTmpEle.setText(tDataInfo[21]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ReceiptClaimAmount");
            tTmpEle.setText(tDataInfo[22]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Operation");
            tTmpEle.setText(tDataInfo[23]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            String mainfeeno = tDataInfo[24];
            //医疗费用项目
            tTmpEle = getCostItem(cDataInfoKey,mainfeeno);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
            //西医代码数组 
            tTmpEle = getWesternMedicone(cDataInfoKey);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
            
            //手术代码数组
            tTmpEle = getOperationCode(cDataInfoKey);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
            
        }

        return tRoot;
    }

    private Element getSpecialReceipt(String cDataInfoKey)//门诊特殊病收据
    {
        Element tRoot = new Element("SpecialReceiptDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.SpecialReceiptDTO");

        Element tTmpEle = null;

        SSRS tHospitalReceipt = loadSpecialReceipt(cDataInfoKey);

        for (int idxPI = 1; idxPI <= tHospitalReceipt.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tHospitalReceipt.getRowData(idxPI);

            tTmpEle = new Element("ReceiptNo");
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("HospitalCode");
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ClinicDate");
            tTmpEle.setText(tDataInfo[2]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("SpecialMoney");
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("MedicalPayment");
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("InsurancePayment");
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoOrdinationFund");
            tTmpEle.setText(tDataInfo[6]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("MutualPayment");
            tTmpEle.setText(tDataInfo[7]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RetirementPayment");
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CivilServants");
            tTmpEle.setText(tDataInfo[9]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("DisabilitySoldier");
            tTmpEle.setText(tDataInfo[10]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Cash");
            tTmpEle.setText(tDataInfo[11]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("IndividualPayment");
            tTmpEle.setText(tDataInfo[12]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PaymentA");
            tTmpEle.setText(tDataInfo[13]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PaymentFrom");
            tTmpEle.setText(tDataInfo[14]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PaymentCap");
            tTmpEle.setText(tDataInfo[15]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PaymentB");
            tTmpEle.setText(tDataInfo[16]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("OwnExpense");
            tTmpEle.setText(tDataInfo[17]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("AccountPayment");
            tTmpEle.setText(tDataInfo[18]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("YearCoOrdinationFund");
            tTmpEle.setText(tDataInfo[19]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("YearMutualPayment");
            tTmpEle.setText(tDataInfo[20]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("AccountBalance");
            tTmpEle.setText(tDataInfo[21]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Operation");
            tTmpEle.setText(tDataInfo[22]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            String mainfeeno = tDataInfo[23];
            //医疗费用项目
            tTmpEle = getCostItem(cDataInfoKey,mainfeeno);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
            //西医代码数组 
            tTmpEle = getWesternMedicone(cDataInfoKey);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
            
            //手术代码数组
            tTmpEle = getOperationCode(cDataInfoKey);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
        }

        return tRoot;
    }

    private Element getDiseaseCode(String aCaseNo, String aCaseRelaNo)//疾病代码
    {
        Element tRoot = new Element("DiseaseCodeDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.DiseaseCodeDTO");

        Element tTmpEle = null;

        SSRS tDiseaseCode = loadDiseaseCode(aCaseNo, aCaseRelaNo);

        for (int idxPI = 1; idxPI <= tDiseaseCode.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tDiseaseCode.getRowData(idxPI);

            tTmpEle = new Element("DiseaseCode");
            tTmpEle.setText(tDataInfo[0]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

        }

        return tRoot;
    }

    private Element getCostItem(String CaseNo,String mainfeeno)//医疗费用项目
    {
        Element tRoot = new Element("CostItemDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.CostItemDTO");

        Element tTmpEle = null;

        SSRS tDiseaseCode = loadCostItem(CaseNo,mainfeeno);

        for (int idxPI = 1; idxPI <= tDiseaseCode.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tDiseaseCode.getRowData(idxPI);
            
            tTmpEle = new Element("CostCategory");
            tTmpEle.setText(tDataInfo[0]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("Amount");
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Feedeductibleamount");
            tTmpEle.setText(tDataInfo[2]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

        }

        return tRoot;
    }

    private Element getAccidentResult(String accdate, String caseno)//出险结果
    {
        Element tRoot = new Element("AccidentResultDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.AccidentResultDTO");

        Element tTmpEle = null;

        SSRS tAccidentResult = loadAccidentResult(accdate, caseno);

        for (int idxPI = 1; idxPI <= tAccidentResult.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tAccidentResult.getRowData(idxPI);

            tTmpEle = new Element("AccidentResult");//出险结果
            if(tDataInfo[0].equals("")){
            	tTmpEle.setText("99");
                tItemEle.addContent(tTmpEle);
            }else{
            	tTmpEle.setText(tDataInfo[0]);
                tTmpEle.addAttribute("CData", "CBusCode");
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

            tTmpEle = new Element("AccidentResultDate");//出险结果时间
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

        }

        return tRoot;
    }

    private Element getClmCaller(String tRgtNo)//报案申请人信息
    {
        Element tRoot = new Element("ClmCallerDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.ClmCallerDTO");

        Element tTmpEle = null;

        SSRS tClmClaimant = loadClmCaller(tRgtNo);

        for (int idxPI = 1; idxPI <= tClmClaimant.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tClmClaimant.getRowData(idxPI);

            tTmpEle = new Element("Name");//名字
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Gender");//性别
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Birthday");
            tTmpEle.setText(tDataInfo[2]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritType");//证件类型
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritCode");//证件代码
            tTmpEle.setText(tDataInfo[4]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("RelationshipWithClaimant");//与出险人关系 //zhangyige
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ContactNo");//联系电话
            tTmpEle.setText(tDataInfo[6]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ContactAddress");//联系地址
            tTmpEle.setText(tDataInfo[7]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

        }

        return tRoot;
    }

    private Element getClmClaimant(String tCustomerNo)//出险人信息
    {
        Element tRoot = new Element("ClmClaimantDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.ClmClaimantDTO");

        Element tTmpEle = null;

        SSRS tClmClaimant = loadClmClaimant(tCustomerNo);

        for (int idxPI = 1; idxPI <= tClmClaimant.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tClmClaimant.getRowData(idxPI);

            tTmpEle = new Element("Name");//名字
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Gender");//性别
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Birthday");
            tTmpEle.setText(tDataInfo[2]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritType");//证件类型
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CritCode");//证件代码
            tTmpEle.setText(tDataInfo[4]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ClaimantType");//出险人保单身份
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
        }

        return tRoot;
    }

    private SSRS loadClmCaller(String tRgtNo)//报案申请人信息
    {
        SSRS tResult = null;
        //        String tStrSql="select rgtantname Name,RgtantSex Gender,'' Birthday,"+
        //        "case idtype when '0' then '01' when '1' then '51' when '2' then '04' else '99' end CritType,"+
        //        "idno CritCode,case relation when '00' then '00' when '01' then '01' when '02' then '01' when '04' then '02' when '05' then '02' when '03' then '03' " +
        //        "when '12' then '15' when '28' then '06' when '29' then '07' when '19' then '08' when '20' then '08' when '22' then '08'when '23' then '08'" +
        //        "when '21' then '09' when '24' then '09' when '27' then '12' else '99' end RelationshipWithClaimant,RgtantPhone ContactNo,RgtantAddress ContactAddress " +
        //        "from llregister where rgtno='"+tRgtNo+"' with ur";//
        String tStrSql = "select rgtantname Name,RgtantSex Gender,'' Birthday," + "idtype CritType,"
                + "idno CritCode,relation RelationshipWithClaimant,RgtantPhone ContactNo,RgtantAddress ContactAddress "
                + " from llregister where rgtno='" + tRgtNo + "' and 1=2 with ur";//暂时不取数
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadClmClaimant(String tCustomerNo)//出险人信息
    {
        SSRS tResult = null;
        //        String tStrSql="select name Name,case sex when '0' then '1' when '1' then '2' else '9' end Gender,birthday Birthday,"+
        //        "case idtype when '0' then '01' when '1' then '51' when '2' then '04' else '99' end CritType,"+
        //        "idno CritCode,'2' ClaimantType from lcinsured where insuredno='"+tCustomerNo+"' fetch first 1 rows only with ur";//出险人保单身份ClaimantType暂时全部默认设置为2被保人
        String tStrSql = "select name Name,sex Gender,birthday Birthday,idtype CritType,idno CritCode,'2' ClaimantType from lcinsured where insuredno='"
                + tCustomerNo
                + "' union select name Name,sex Gender,birthday Birthday,idtype CritType,idno CritCode,'2' ClaimantType from lbinsured where insuredno='"
                + tCustomerNo + "' fetch first 1 rows only with ur";//出险人保单身份ClaimantType暂时全部默认设置为2被保人
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private Element getPolicyPayment(String cDataInfoKey)//保单赔付信息
    {
        Element tRoot = new Element("PolicyPaymentDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.PolicyPaymentDTO");

        Element tTmpEle = null;

        SSRS tPolicyPayment = loadPolicyPayment(cDataInfoKey);

        for (int idxPI = 1; idxPI <= tPolicyPayment.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tPolicyPayment.getRowData(idxPI);

            tTmpEle = new Element("PolicySequenceNo");
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PolicyNo");
            tTmpEle.setText(tDataInfo[1]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CustomerNo");
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ClaimOpinion");
            tTmpEle.setText(tDataInfo[3]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ClaimConclusionCode");
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("TotalPaymentAmount");
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("RenewalTimes");
            tTmpEle.setText(tDataInfo[6]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = getCoveragePayment(cDataInfoKey, tDataInfo[1]);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
        }

        return tRoot;
    }

    private Element getCoveragePayment(String cDataInfoKey, String tContNo)//保单责任赔付信息
    {
        Element tRoot = new Element("CoveragePaymentDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.CoveragePaymentDTO");

        Element tTmpEle = null;

        SSRS tPolicyPayment = loadCoveragePayment(cDataInfoKey, tContNo);

        for (int idxPI = 1; idxPI <= tPolicyPayment.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tPolicyPayment.getRowData(idxPI);

            tTmpEle = new Element("CoverageType");
            tTmpEle.setText(tDataInfo[0]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageCode");
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ComcoverageCode");
            tTmpEle.setText(tDataInfo[2]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("CoverageName");
            tTmpEle.setText(tDataInfo[3]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("LiabilityCode");
            tTmpEle.setText(tDataInfo[4]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("LiabilityClassification");
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ClaimOpinion");
            tTmpEle.setText(tDataInfo[6]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ClaimConclusionCode");
            tTmpEle.setText(tDataInfo[7]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PaymentAmount");
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("Operation");
            tTmpEle.setText(tDataInfo[9]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            tTmpEle = new Element("HospitalCode");
            tTmpEle.setText(tDataInfo[10]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            //手术代码数组
            tTmpEle = getOperationCode(cDataInfoKey);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
            
            //西医代码数组 
            tTmpEle = getWesternMedicone(cDataInfoKey);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
            
        }
        return tRoot;
    }

    private SSRS loadCoveragePayment(String cDataInfoKey, String cContNo)//保单责任赔付信息,平台险种代码CoverageCode暂时先设置为20医疗保险,平台责任代码LiabilityCode先设置为99其他
    {
        SSRS tResult = null;
        //        String tStrSql="select case b.riskperiod when 'L' then '1' when 'M' then '2' when 'S' then '2' else '' end CoverageType," +
        //        		"'20' CoverageCode,a.riskcode ComcoverageCode,b.riskname CoverageName,'99' LiabilityCode," +
        //        		"a.givetypedesc ClaimOpinion,'' ClaimConclusionCode,a.realpay PaymentAmount from llclaimdetail a,lmriskapp b " +
        //        		"where a.caseno='"+cDataInfoKey+"' and (a.grpcontno='"+cContNo+"' or a.contno='"+cContNo+"') and a.riskcode=b.riskcode and b.risktype1 = '1'";
        String tStrSql = "select b.riskperiod CoverageType,"
                + "a.riskcode CoverageCode," +
                		"a.riskcode ComcoverageCode," +
                		"b.riskname CoverageName," +
                		"a.dutycode LiabilityCode,"
                		+ "a.dutycode LiabilityClassification,"
                + "a.givetypedesc ClaimOpinion," +
                		"(select givetype from llclaim  where caseno=a.caseno) ClaimConclusionCode," +
                		"a.realpay PaymentAmount,"
                + "(case (select '1' from LLOperation where caseno=a.caseno  and operationcode <> '空' fetch first 1 rows only) when '1' then '1' else '0' end ) Operation,"  // #3104
                + "(select HospitalCode from llfeemain where caseno=a.caseno fetch first 1 rows only) HospitalCode "
                +		" from llclaimdetail a,lmriskapp b "
                + "where a.caseno='" + cDataInfoKey + "' and (a.grpcontno='" + cContNo + "' or a.contno='" + cContNo
                + "') and a.riskcode=b.riskcode ";
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }


    private SSRS loadDiseaseCode(String aCaseNo, String aCaseRelaNo)//疾病代码
    {
        SSRS tResult = null;

        String tCaseRelaSQL = "";

        if (aCaseRelaNo != null && !"".equals(aCaseRelaNo))
        {
            tCaseRelaSQL = " and caserelano = '" + aCaseRelaNo + "'";
        }

        String tStrSql = "select DiseaseCode DiseaseCode from llcasecure where caseno='" + aCaseNo + "' "
                + tCaseRelaSQL + " with ur";//

        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    private SSRS loadCostItem(String CaseNo,String mainfeeno)// 医疗费用项目
    {
    	
        SSRS tResult = null;//以下sql modify by zhangyige 2013-1-5
        String tStrSql = "select FeeItemCode as CostCategory,fee as Amount,(preamnt+selfamnt+refuseamnt) as Feedeductibleamount "
        +" from llcasereceipt where mainfeeno = '"+mainfeeno+"' with ur";

        tResult = new ExeSQL().execSQL(tStrSql);
        String tempSql = "select '404' CostCategory,'0' Amount,'' Feedeductibleamount from LLCaseReceipt fetch first 1 rows only";
        if(tResult==null||tResult.MaxRow==0){
        	tResult = new ExeSQL().execSQL(tempSql);
        }else{
        	String CostCategory = tResult.GetText(1, 1);
        	if(CostCategory==null||CostCategory.equals("")||CostCategory.equals("null")){	
        		tResult = new ExeSQL().execSQL(tempSql);
        	}
        }
        return tResult;
    }

    private SSRS loadAccidentResult(String accdate, String caseno)//出险结果
    {
        SSRS tResult = null;
        //        String tStrSql="select case reasoncode when '05' then '01' when '08' then '03' when '04' then '04' when '07' then '05' when '06' then '06'" +
        //        		" else '99' end AccidentResult,'"+accdate+"' from llappclaimreason where caseno='"+caseno+"'";//出险结果时间按事件发生日期算
        String tStrSql="select reasoncode AccidentResult,'"+accdate+"' AccidentResultDate from llappclaimreason where caseno='"+caseno+"' fetch first 1 rows only with ur";//出险结果时间按事件发生日期算
        tResult = new ExeSQL().execSQL(tStrSql);
        
        if (tResult == null ||tResult.getMaxRow()==0) {
        String tStrNewSql = "select '' AccidentResult,'" + accdate
                + "' AccidentResultDate from dual where 1=1 ";//出险结果时间按事件发生日期算
        tResult = new ExeSQL().execSQL(tStrNewSql);
        }

        return tResult;
    }

    private SSRS loadPolicyPayment(String cDataInfoKey)
    {
        SSRS tResult = null;
        //        String tCaseNo="C0000060822000003";
        String tStrSql = "select distinct '' PolicySequenceNo,case grpcontno when '00000000000000000000' then contno else grpcontno end PolicyNo,"
                + "(select customerno from llcase where caseno=ll.caseno fetch first 1 rows only) CustomerNo,'' ClaimOpinion,(select givetype from llclaim  where caseno=ll.caseno) ClaimConclusionCode,"
                +"case grpcontno when '00000000000000000000' then (select sum(pay) from ljagetclaim where contno=ll.contno and otherno = ll.caseno) "
                +"else (select sum(pay) from ljagetclaim where grpcontno=ll.grpcontno and otherno = ll.caseno) end TotalPaymentAmount " 
                +",(select count(distinct NewContNo) from LCRNewStateLog where ContNo = ll.ContNo) RenewalTimes "
                + "FROM  llclaimdetail ll where caseno='"
                + cDataInfoKey
//                + "' and exists(select 1 from lmriskapp where riskcode=ll.riskcode and risktype1 = '1') " 
                +"' with ur";
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }
    
    private Element getSegmentationReceipt(String cDataInfoKey)//分割单收据数组
    {
    	Element tRoot = new Element("SegmentationReceiptDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.SegmentationReceiptDTO");
        Element tTmpEle = null;
        SSRS tSegmentationReceipt = loadSegmentationReceipt(cDataInfoKey);

        for (int idxPI = 1; idxPI <= tSegmentationReceipt.MaxRow; idxPI++)
        {
        	 Element tItemEle = new Element("Item");
             tRoot.addContent(tItemEle);

             String[] tDataInfo = tSegmentationReceipt.getRowData(idxPI);

             tTmpEle = new Element("receiptNo");//收据编号
             tTmpEle.setText(tDataInfo[0]);
             tItemEle.addContent(tTmpEle);
             tTmpEle = null;
        	
             tTmpEle = new Element("issuedUnit");//出具单位
             tTmpEle.setText(tDataInfo[1]);
             tItemEle.addContent(tTmpEle);
             tTmpEle = null;
             
             tTmpEle = new Element("feeAmount");//收据发生金额
             tTmpEle.setText(tDataInfo[2]);
             tItemEle.addContent(tTmpEle);
             tTmpEle = null;
             
             tTmpEle = new Element("feeDeductibleAmount");//第三方支付金额
             tTmpEle.setText(tDataInfo[3]);
             tItemEle.addContent(tTmpEle);
             tTmpEle = null;
             
             tTmpEle = new Element("receiptClaimAmount");//剩余发生金额
             tTmpEle.setText(tDataInfo[4]);
             tItemEle.addContent(tTmpEle);
             tTmpEle = null;
             
             if (tTmpEle != null)
             {
                 tItemEle.addContent(tTmpEle);
             }
             tTmpEle = null;
             
        }
        return tRoot;
    }
    
    private SSRS loadSegmentationReceipt(String cDataInfoKey)//分割单收据数组的信息
    {
    	SSRS tResult =null;
         StringBuffer tStrBSql = new StringBuffer();
         tStrBSql.append(" select ");
    	
         String tStrSql = tStrBSql.toString();
         tResult = new ExeSQL().execSQL(tStrSql);
         return tResult;
    }
    
    private SSRS loadClaimInfo(String cDataInfoKey) // 个人理赔信息
    {
        SSRS tResult = null;
        //        String tCaseNo="C1100080116000009";
        StringBuffer tStrBuSql = new StringBuffer();
        tStrBuSql
                .append("select a.caseno ClaimNo," +
                		"a.rgtdate ReportDate," +
                		"'' ReportNo," +
                		"a.rgtdate RegistrationDate," +
                		"a.caseno RegistrationNo," +
                		"a.rgtno BatchNo," +
                		"c.accdate AccidentTime,");
        tStrBuSql.append("c.accidenttype AccidentReason," +
        		"'110000' Address1, " +
        		"'110100' Address2, " +
        		"(select accplace from LLSubReport lsr,llcaserela llr  where lsr.subrptno=llr.subrptno  and llr.caseno = a.caseno fetch first 1 rows only) Address3,");
        tStrBuSql.append("(select accplace from LLSubReport lsr,llcaserela llr  where lsr.subrptno=llr.subrptno  and llr.caseno = a.caseno fetch first 1 rows only) Address4," +
        		"'' Accidentdes,");
        tStrBuSql.append("(case when a.SurveyFlag  is null or a.SurveyFlag='0' then '0' else '1' end) ClaimInvestigation,");
        tStrBuSql.append("(select 1 from llinqapply lli where  otherno=a.caseno and substr(inqdept,1,4)=substr(a.mngcom,1,4) fetch first 1 rows only)  RemoteClaimInvestigation,"); //zyg 添加 需要理赔模块确认sql
        tStrBuSql.append("(select startdate from llcaseoptime where caseno=a.caseno and rgtstate='07' fetch first 1 rows only) InvestigationStartDate,");
        tStrBuSql.append("(select enddate from llcaseoptime where caseno=a.caseno and rgtstate='08' fetch first 1 rows only) InvestigationEndDate,");
        tStrBuSql.append("(select givetype  from llclaim where caseno=a.caseno fetch first 1 rows only) TreatmentAdvice," +
        		"(select  diseasecode from LLCaseCure where SeriousFlag='1' and caseno=a.caseno) CriticalIllnessNo,");
        tStrBuSql.append("(select code from LLAccident where caseno = a.caseno) InjuryReasonCode," );
        tStrBuSql.append("(select coalesce(sum(realpay),0) from llclaimdetail where caseno=a.caseno) ClaimAmount,");
        tStrBuSql.append("a.endcasedate  EndcaseDate, " +
        		"(case when a.rgtstate='14' then '2' else '1' end) Status,a.customerno CustomerNo,(select appdate from llregister where rgtno=a.rgtno) ");
        tStrBuSql
                .append(" from llcase a,llcaserela b,llsubreport c where a.caseno=b.caseno and b.subrptno=c.subrptno and a.caseno='"
                        + cDataInfoKey + "'" + " fetch first 1 rows only");
        System.out.println("tStrBuSql.toString() zyg:"+tStrBuSql.toString());
        tResult = new ExeSQL().execSQL(tStrBuSql.toString());
        return tResult;
    }
    
    private SSRS loadClinicReceipt(String caseno) //?	医保门诊医疗收据数组数组中的详细信息
    {
    	SSRS tResult = null;
    	StringBuffer tStrBuSql = new StringBuffer();
    	tStrBuSql.append("select a.ReceiptNo ReceiptNo," +
    			"a.HospitalCode HospitalCode," +
    			"a.FeeDate ClinicDate," +
    			"a.SumFee ClinicMoney," +
    			"'' Cash,");
    	tStrBuSql.append("'' AccountPayment," +
    			"'' AccountBalance," +
    			"(select sum(b.FeeInSecu) from LLSecurityReceipt b where b.mainfeeno=a.mainfeeno) Medicalpayment," +
    			"'' TotalMedicalPayment," +
    			"(select sum(b.YearSupDoorFee) from LLSecurityReceipt b where b.mainfeeno=a.mainfeeno) TotalAnnualpayment,");
        tStrBuSql.append("(select sum(b.SecurityFee) from LLSecurityReceipt b where b.mainfeeno=a.mainfeeno) Insurancepayment," +
        		"(select sum(b.SupDoorFee) from LLSecurityReceipt b where b.mainfeeno=a.mainfeeno) Cliniclargepayment," +
        		"(select sum(b.RetireAddFee) from LLSecurityReceipt b where b.mainfeeno=a.mainfeeno) Retirementpayment,");
        tStrBuSql.append("'' DisabilitySoldier," +
        		"(select sum(b.OfficialSubsidy) from LLSecurityReceipt b where b.mainfeeno=a.mainfeeno) Civilservants," +
        		"'' BalanceAnnualPayment," +
        		"(select sum(b.SelfAmnt) from LLSecurityReceipt b where b.mainfeeno=a.mainfeeno) Individualpayment," +
        		"(select sum(b.SelfPay1) from LLSecurityReceipt b where b.mainfeeno=a.mainfeeno) PaymentA," +
        		"(select sum(b.GetLimit) from LLSecurityReceipt b where b.mainfeeno=a.mainfeeno) PaymentFrom," +
        		"'' PaymentCap,");
        tStrBuSql.append("(select sum(b.SelfPay2) from LLSecurityReceipt b where b.mainfeeno=a.mainfeeno) PaymentB," +
        		"(select sum(b.SelfAmnt) from LLSecurityReceipt b where b.mainfeeno=a.mainfeeno) Ownexpense," +
        		"'' ReceiptClaimAmount,");
        tStrBuSql.append("(case (select '1' from LLOperation where caseno=a.caseno and CaseRelaNo=a.CaseRelaNo and operationcode <>'空') when '1' then '1' else '0' end ) Operation,a.mainfeeno ");  //#3104
        tStrBuSql.append(" from llfeemain a  where a.caseno='" 
        		         + caseno 
        		         + "' and  a.feetype='1' and  (a.feeatti='1' or a.feeatti='4') with ur");
        
        tResult = new ExeSQL().execSQL(tStrBuSql.toString());

        return tResult;
    }
    
    private SSRS loadSpecialReceipt(String caseno) //?	?	医保特殊病医疗收据数组中的详细信息
    {
    	SSRS tResult = null;
    	StringBuffer tStrBuSql = new StringBuffer();
    	tStrBuSql.append("select a.ReceiptNo ReceiptNo," +
    			"a.HospitalCode HospitalCode," +
    			"a.FeeDate ClinicDate," +
    			"a.SumFee SpecialMoney," +
    			"(select sum(b.FeeInSecu) from LLSecurityReceipt b where b.mainfeeno=a.mainfeeno) Medicalpayment," +
    			"(select sum(b.SecurityFee) from LLSecurityReceipt b where b.mainfeeno=a.mainfeeno) Insurancepayment,");
        tStrBuSql.append("(select sum(b.PlanFee) from LLSecurityReceipt b where b.mainfeeno=a.mainfeeno) CoOrdinationfund," +
        		"(select sum(b.SupInHosFee) from LLSecurityReceipt b where b.mainfeeno=a.mainfeeno) Mutualpayment," +
        		"(select sum(b.RetireAddFee) from LLSecurityReceipt b where b.mainfeeno=a.mainfeeno) Retirementpayment,");
        tStrBuSql.append("(select sum(b.OfficialSubsidy) from LLSecurityReceipt b where b.mainfeeno=a.mainfeeno) Civilservants," +
        		"'' DisabilitySoldier," +
        		"'' Cash," +
        		"(select sum(b.SelfAmnt) from LLSecurityReceipt b where b.mainfeeno=a.mainfeeno) Individualpayment," +
        		"(select sum(b.SelfPay1) from LLSecurityReceipt b where b.mainfeeno=a.mainfeeno) PaymentA," +
        		"(select sum(b.GetLimit) from LLSecurityReceipt b where b.mainfeeno=a.mainfeeno) PaymentFrom," +
        		"'' PaymentCap,");
        tStrBuSql.append("(select sum(b.SelfPay2) from LLSecurityReceipt b where b.mainfeeno=a.mainfeeno) PaymentB," +
        		"(select sum(b.SelfAmnt) from LLSecurityReceipt b where b.mainfeeno=a.mainfeeno) Ownexpense," +
        		"'' AccountPayment," +
        		"(select sum(b.YearPlayFee) from LLSecurityReceipt b where b.mainfeeno=a.mainfeeno) YearannualCoOrdinationfund," +
        		"'' YearAnnualMutualPayment," +
        		"'' AccountBalance,");
        tStrBuSql.append("(case (select '1' from LLOperation where caseno=a.caseno and CaseRelaNo=a.CaseRelaNo and operationcode <>'空') when '1' then '1' else '0' end ) Operation,a.mainfeeno ");  // #3104
        tStrBuSql.append(" from llfeemain a  where a.caseno='" 
        		         + caseno
        		         + "'  and  a.feetype='3' and a.feeatti='1' with ur");
        
        tResult = new ExeSQL().execSQL(tStrBuSql.toString());

        return tResult;
    }
    
    private SSRS loadHospitalReceipt(String caseno) //?	?	医保住院医疗收据数组中的详细信息
    {
    	SSRS tResult = null;
    	StringBuffer tStrBuSql = new StringBuffer();
    	tStrBuSql.append("select a.ReceiptNo ReceiptNo," +
    			"a.HospitalCode HospitalCode," +
    			"a.HospStartDate HospitalDate," +
    			"a.HospEndDate DischardeDate," +
    			"a.RealHospDate HospitalStay," +
    			"(select sum(b.ApplyAmnt) from LLSecurityReceipt b where b.mainfeeno=a.mainfeeno) HospitalMoney,");
        tStrBuSql.append("(select sum(b.FeeInSecu) from LLSecurityReceipt b where b.mainfeeno=a.mainfeeno) MedicalPayment," +
        		"(select sum(b.SecurityFee) from LLSecurityReceipt b where b.mainfeeno=a.mainfeeno) InsurancePayment," +
        		"(select sum(b.PlanFee) from LLSecurityReceipt b where b.mainfeeno=a.mainfeeno) CoOrdinationFund," +
        		"(select sum(b.SelfPay1) from LLSecurityReceipt b where b.mainfeeno=a.mainfeeno) PaymentA," +
        		"(select sum(b.GetLimit) from LLSecurityReceipt b where b.mainfeeno=a.mainfeeno) PaymentFrom," +
        		"(select sum(b.SupInHosFee) from LLSecurityReceipt b where b.mainfeeno=a.mainfeeno) MutualPayment," +
        		"(select sum(b.OfficialSubsidy) from LLSecurityReceipt b where b.mainfeeno=a.mainfeeno) CivilServants,");
        tStrBuSql.append("(select sum(b.FeeOutSecu) from LLSecurityReceipt b where b.mainfeeno=a.mainfeeno) BesideInsurancePayment," +
        		"(select sum(b.SelfAmnt) from LLSecurityReceipt b where b.mainfeeno=a.mainfeeno) OwnexPense," +
        		"(select sum(b.SelfPay2) from LLSecurityReceipt b where b.mainfeeno=a.mainfeeno) PaymentB," +
        		"(select sum(b.SelfAmnt +b.SelfPay1 +b.SelfPay2) from LLSecurityReceipt b where b.mainfeeno=a.mainfeeno) TotalOwnExpense," +
        		"'' Cash," +
        		"''AccountPayment," +
        		"'' ReceiptClaimAmount,");
        tStrBuSql.append("(case (select '1' from LLOperation where caseno=a.caseno and CaseRelaNo=a.CaseRelaNo and operationcode <> '空') when '1' then '1' else '0' end ) Operation,a.mainfeeno ");  // #3104
        tStrBuSql.append(" from llfeemain a  where a.caseno='" 
        		         + caseno
        		         + "' and  a.feetype='2' and a.feeatti='1' with ur");
        
        tResult = new ExeSQL().execSQL(tStrBuSql.toString());

        return tResult;
    }
    
    private SSRS loadApprovalReceipt(String caseno) //?	?	医保审批表收据数组中的详细信息
    {
    	SSRS tResult = null;
    	StringBuffer tStrBuSql = new StringBuffer();
    	tStrBuSql.append("select a.FeeDate ClinicStartDate," +
    			"a.SumFee ClinicMoney," +
    			"(select sum(b.HighDoorAmnt-b.SupDoorFee) from LLSecurityReceipt b where b.mainfeeno=a.mainfeeno) MutualFundPayA," +
    			"(select sum(b.HighDoorAmnt-b.TotalSupDoorFee) from LLSecurityReceipt b where b.mainfeeno=a.mainfeeno) MutualFundPayB," +
    			"(select sum(b.SelfPay1) from LLSecurityReceipt b where b.mainfeeno=a.mainfeeno) PaymentA," +
    			"(select sum(b.PlanFee) from LLSecurityReceipt b where b.mainfeeno=a.mainfeeno) OverallFinancingPayment," +
    			"(select sum(b.YearPlayFee) from LLSecurityReceipt b where b.mainfeeno=a.mainfeeno) OverallFinancingYearPayment," +
    			"(select sum(b.SelfPay2) from LLSecurityReceipt b where b.mainfeeno=a.mainfeeno) PaymentB," +
    			"(select sum(b.SelfAmnt) from LLSecurityReceipt b where b.mainfeeno=a.mainfeeno) OwnExpense");
        tStrBuSql.append(" from llfeemain a  where a.caseno='" 
        		         + caseno
        		         + "' and  ((a.feetype='1' and a.feeatti='2') or (a.feetype='2' and a.feeatti='2')) with ur");
        
        tResult = new ExeSQL().execSQL(tStrBuSql.toString());
        return tResult;
    }
    
    private SSRS loadOrdinaryClinicReceipt(String caseno) //?	 普通门诊收据数组中的详细信息
    {
    	SSRS tResult = null;
    	StringBuffer tStrBuSql = new StringBuffer();
    	tStrBuSql.append("select a.ReceiptNo ReceiptNo," +
    			"a.HospitalCode HospitalCode," +
    			"a.FeeDate ClinicDate," +
    			"a.SumFee FeeAmount,");
    	tStrBuSql.append("(select sum(preamnt+selfamnt+refuseamnt) from LLCaseReceipt where MainFeeNo=a.mainfeeno) FeeDeductibleAmount," +
    			"'' ReceiptClaimAmount,");
        tStrBuSql.append("(case (select '1' from LLOperation where caseno=a.caseno and CaseRelaNo=a.CaseRelaNo  and operationcode <> '空' fetch first 1 rows only) when '1' then '1' else '0' end ) Operation,a.mainfeeno "); // #3104  
    	tStrBuSql.append(" from llfeemain a where a.caseno='" 
    			         + caseno
    			         + "' and  a.feetype='1' and a.feeatti='0' fetch first 1 rows only with ur");
        
        tResult = new ExeSQL().execSQL(tStrBuSql.toString());

        return tResult;
    }
    
    private SSRS loadOrdinaryHospitalReceipt(String caseno) //?	 普通住院收据数组中的详细信息
    {
    	SSRS tResult = null;
    	StringBuffer tStrBuSql = new StringBuffer();
    	tStrBuSql.append("select a.ReceiptNo ReceiptNo," +
    			"a.HospitalCode HospitalCode," +
    			"a.HospStartDate HospitalDate," +
    			"a.HospEndDate DischardeDate," +
    			"a.RealHospDate HospitalStay," +
    			"sum(a.sumfee) HospitalMoney,");
    	tStrBuSql.append("(select sum(preamnt+selfamnt+refuseamnt) from LLCaseReceipt where MainFeeNo=a.mainfeeno) Feedeductibleamount," +
    			"'' ReceiptClaimAmount,");
        tStrBuSql.append("(case (select '1' from LLOperation where caseno=a.caseno and CaseRelaNo=a.CaseRelaNo and operationcode <> '空') when '1' then '1' else '0' end ) Operation,a.mainfeeno ");   // #3104
    	tStrBuSql.append(" from llfeemain a  where a.caseno='" 
    			         + caseno
    			         + "'  and  a.feetype='2' and a.feeatti='0' group by a.RealHospDate,a.caseno,a.CaseRelaNo,a.mainfeeno,a.HospEndDate,a.HospStartDate,a.HospitalCode,a.ReceiptNo with ur");
        
        tResult = new ExeSQL().execSQL(tStrBuSql.toString());

        return tResult;
    }
    
    private SSRS loadWesternMedicone(String caseno) //?	 西医疾病代码数组中的详细信息
    {
    	SSRS tResult = null;
         	String tStrSql = "select (case when diseasecode = 'M51.1' then 'M51.1+' else diseasecode end ) westernMedicineCode from LLCaseCure where caseno='"
        	             + caseno
        		         + "' and SeriousFlag<>'1' with ur";
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }
    
    private SSRS loadOperationCode(String caseno) //?	手术代码数组中的详细信息
    {
    	SSRS tResult = null;
        String tStrSql = "select OperationCode OperationCode from LLOperation where caseno='" 
        	             + caseno
        		         + "' with ur";
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }
    
    private SSRS loadDaignosts(String caseno) //?	诊断类型数组中的详细信息
    {
    	SSRS tResult = null;
        String tStrSql = "select (case when (select  1 from LLCaseCure where SeriousFlag='1' and caseno=a.caseno fetch first 1 rows only) is not null then '02' "
        	             + "when (select 2 from LLOperation where caseno=a.caseno fetch first 1 rows only ) is not null then '06' else '01' end ) DiagnosisType "
        	             + " from llclaimdetail a where caseno='" 
        	             + caseno
//        		         + "' and riskcode='"
//        		         + riskcode
        		         +"' with ur";
        tResult = new ExeSQL().execSQL(tStrSql);

        return tResult;
    }

    /**
     * 调用前置机服务
     * @return
     */
    private boolean calService()
    {
        Document tDocument = formJDocMessage(mRoot, "03", mDataInfoKey);
        JdomUtil.print(tDocument);

        Document tInStdXmlDoc = tDocument;

        ReqJKXPreService tJKXPreServlet = new ReqJKXPreService();
        String tReceiveXmlDoc = tJKXPreServlet.callService(tInStdXmlDoc);
//        JdomUtil.print(tReceiveXmlDoc);
        System.out.println(tReceiveXmlDoc);

        return true;
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        System.out.println(szFunc + ": " + szErrMsg);

        CError cError = new CError();
        cError.moduleName = "AddCertifyListBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    //  医保审批表收据数组
    private Element getApprovalReceipt(String cDataInfoKey)
    {
        Element tRoot = new Element("ApprovalReceiptDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.ApprovalReceiptDTO");

        Element tTmpEle = null;

        SSRS tApprovalReceipt = loadApprovalReceipt(cDataInfoKey);

        for (int idxPI = 1; idxPI <= tApprovalReceipt.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tApprovalReceipt.getRowData(idxPI);

            tTmpEle = new Element("ClinicStartDate");
            tTmpEle.setText(tDataInfo[0]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ClinicMoney");
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("MutualFundPayA");
            tTmpEle.setText(tDataInfo[2]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("MutualFundPayB");
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PaymentA");
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("OverallFinancingPayment");
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("OverallFinancingYearPayment");
            tTmpEle.setText(tDataInfo[6]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("PaymentB");
            tTmpEle.setText(tDataInfo[7]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("OwnExpense");
            tTmpEle.setText(tDataInfo[8]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

        }

        return tRoot;
    }
    
    //西医疾病代码数组
    private Element getWesternMedicone(String cDataInfoKey)
    {
        Element tRoot = new Element("WesternMedicineDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.WesternMedicineDTO");

        Element tTmpEle = null;

        SSRS tWesternMedicone = loadWesternMedicone(cDataInfoKey);

        for (int idxPI = 1; idxPI <= tWesternMedicone.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tWesternMedicone.getRowData(idxPI);

            tTmpEle = new Element("WesternMedicineCode");
            tTmpEle.setText(tDataInfo[0]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            //诊断类型数组
            tTmpEle = getDiagnosis(cDataInfoKey);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

        }
        
        return tRoot;
    }
    //诊断类型数组
    private Element getDiagnosis(String cDataInfoKey)
    {
        Element tRoot = new Element("DiagnosisDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.DiagnosisDTO");

        Element tTmpEle = null;

        SSRS tloadDaignosts = loadDaignosts(cDataInfoKey);

        for (int idxPI = 1; idxPI <= tloadDaignosts.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tloadDaignosts.getRowData(idxPI);

            tTmpEle = new Element("DiagnosisType");
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

        }
        
        return tRoot;
    }
    //手术代码数组
    private Element getOperationCode(String cDataInfoKey)
    {
        Element tRoot = new Element("OperationDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.OperationDTO");

        Element tTmpEle = null;

        SSRS tloadOperationCode = loadOperationCode(cDataInfoKey);

        for (int idxPI = 1; idxPI <= tloadOperationCode.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tloadOperationCode.getRowData(idxPI);

            tTmpEle = new Element("OperationCode");
            tTmpEle.setText(tDataInfo[0]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            //诊断类型数组
            tTmpEle = getDiagnosis(cDataInfoKey);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;

        }
        
        return tRoot;
    }
    //普通门诊收据数组
    private Element getOrdinaryClinicReceipt(String cDataInfoKey)
    {
        Element tRoot = new Element("OrdinaryClinicReceiptDTO");
        tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.OrdinaryClinicReceiptDTO");

        Element tTmpEle = null;

        SSRS tOrdinaryClinicReceipt = loadOrdinaryClinicReceipt(cDataInfoKey);

        for (int idxPI = 1; idxPI <= tOrdinaryClinicReceipt.MaxRow; idxPI++)
        {
            Element tItemEle = new Element("Item");
            tRoot.addContent(tItemEle);

            String[] tDataInfo = tOrdinaryClinicReceipt.getRowData(idxPI);

            tTmpEle = new Element("ReceiptNo");
            tTmpEle.setText(tDataInfo[0]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("HospitalCode");
            tTmpEle.setText(tDataInfo[1]);
            tTmpEle.addAttribute("CData", "CBusCode");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ClinicDate");
            tTmpEle.setText(tDataInfo[2]);
            tTmpEle.addAttribute("CData", "CDate");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("FeeAmount");
            tTmpEle.setText(tDataInfo[3]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("FeeDeductibleAmount");
            tTmpEle.setText(tDataInfo[4]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("ReceiptClaimAmount");
            tTmpEle.setText(tDataInfo[5]);
            tTmpEle.addAttribute("CData", "CBigDecimal");
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;

            tTmpEle = new Element("Operation");
            tTmpEle.setText(tDataInfo[6]);
            tItemEle.addContent(tTmpEle);
            tTmpEle = null;
            
            String mainfeeno = tDataInfo[7];
            //医疗费用项目
            tTmpEle = getCostItem(cDataInfoKey,mainfeeno);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
            //西医代码数组 
            tTmpEle = getWesternMedicone(cDataInfoKey);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
            
            //手术代码数组
            tTmpEle = getOperationCode(cDataInfoKey);
            if (tTmpEle != null)
            {
                tItemEle.addContent(tTmpEle);
            }
            tTmpEle = null;
            
        }

        return tRoot;
    }
    //普通住院收据数组
    private Element getOrdinaryHospitalReceipt(String cDataInfoKey)
    {
    	Element tRoot = new Element("OrdinaryHospitalReceiptDTO");
    	tRoot.addAttribute("DTOClass", "com.ebao.health.insurance.dto.OrdinaryHospitalReceiptDTO");
    	
    	Element tTmpEle = null;
    	
    	SSRS tOrdinaryHospitalReceipt = loadOrdinaryHospitalReceipt(cDataInfoKey);
    	
    	for (int idxPI = 1; idxPI <= tOrdinaryHospitalReceipt.MaxRow; idxPI++)
    	{
    		Element tItemEle = new Element("Item");
    		tRoot.addContent(tItemEle);
    		
    		String[] tDataInfo = tOrdinaryHospitalReceipt.getRowData(idxPI);
    		
    		tTmpEle = new Element("ReceiptNo");
    		tTmpEle.setText(tDataInfo[0]);
    		tItemEle.addContent(tTmpEle);
    		tTmpEle = null;
    		
    		tTmpEle = new Element("HospitalCode");
    		tTmpEle.setText(tDataInfo[1]);
    		tTmpEle.addAttribute("CData", "CBusCode");
    		tItemEle.addContent(tTmpEle);
    		tTmpEle = null;
    		
    		tTmpEle = new Element("HospitalDate");
    		tTmpEle.setText(tDataInfo[2]);
    		tTmpEle.addAttribute("CData", "CDate");
    		tItemEle.addContent(tTmpEle);
    		tTmpEle = null;
    		
    		tTmpEle = new Element("DischardeDate");
    		tTmpEle.setText(tDataInfo[3]);
    		tTmpEle.addAttribute("CData", "CDate");
    		tItemEle.addContent(tTmpEle);
    		tTmpEle = null;
    		
    		tTmpEle = new Element("HospitalStay");
    		tTmpEle.setText(tDataInfo[4]);
    		tTmpEle.addAttribute("CData", "CBigDecimal");
    		tItemEle.addContent(tTmpEle);
    		tTmpEle = null;
    		
    		tTmpEle = new Element("HospitalMoney");
    		if(tDataInfo[5].equals("")||tDataInfo[5].equals("null")){
    			tTmpEle.setText("0.0");
    		}else{
    			tTmpEle.setText(tDataInfo[5]);
    		}
    		tTmpEle.addAttribute("CData", "CBigDecimal");
    		tItemEle.addContent(tTmpEle);
    		tTmpEle = null;
    		
    		tTmpEle = new Element("Feedeductibleamount");
    		tTmpEle.setText(tDataInfo[6]);
    		tTmpEle.addAttribute("CData", "CBigDecimal");
    		tItemEle.addContent(tTmpEle);
    		tTmpEle = null;
    		
    		tTmpEle = new Element("ReceiptClaimAmount");
    		tTmpEle.setText(tDataInfo[7]);
    		tTmpEle.addAttribute("CData", "CBigDecimal");
    		tItemEle.addContent(tTmpEle);
    		tTmpEle = null;
    		
    		tTmpEle = new Element("Operation");
    		tTmpEle.setText(tDataInfo[8]);
    		tItemEle.addContent(tTmpEle);
    		tTmpEle = null;
    		String mainfeeno = tDataInfo[9];
    		//医疗费用项目
    		tTmpEle = getCostItem(cDataInfoKey,mainfeeno);
    		if (tTmpEle != null)
    		{
    			tItemEle.addContent(tTmpEle);
    		}
    		tTmpEle = null;
    		//西医代码数组 
    		tTmpEle = getWesternMedicone(cDataInfoKey);
    		if (tTmpEle != null)
    		{
    			tItemEle.addContent(tTmpEle);
    		}
    		tTmpEle = null;
    		
    		//手术代码数组
    		tTmpEle = getOperationCode(cDataInfoKey);
    		if (tTmpEle != null)
    		{
    			tItemEle.addContent(tTmpEle);
    		}
    		tTmpEle = null;
    		
    	}
    	
    	return tRoot;
    }
    /**
     * 测试主程序
     * 
     * @param args
     */
    public static void main(String[] args)
    {
        LPJKXTwoServiceImp tLPJKXServiceImp = new LPJKXTwoServiceImp();
        VData tVData = new VData();
        TransferData mTransferData = new TransferData();
        mTransferData.setNameAndValue("DataInfoKey", "C1100140126000003"); // 测试号  C0000120802000002
        tVData.add(mTransferData);
        tLPJKXServiceImp.callJKXService(tVData, null);
    }
}
