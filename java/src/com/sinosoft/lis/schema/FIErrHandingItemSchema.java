/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.FIErrHandingItemDB;

/*
 * <p>ClassName: FIErrHandingItemSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 财务接口最新版
 * @CreateDate：2011-12-13
 */
public class FIErrHandingItemSchema implements Schema, Cloneable
{
	// @Field
	/** 错误申请号码 */
	private String ErrAppNo;
	/** 批次号 */
	private String BatchNo;
	/** 业务交易编号 */
	private String BusTypeID;
	/** 费用编号 */
	private String CostID;
	/** 明细索引类型 */
	private String IndexCode;
	/** 明细索引号码 */
	private String IndexNo;
	/** 关联cq号码 */
	private String CQNo;
	/** 数据来源 */
	private String DataSource;
	/** 处理方式 */
	private String DealType;
	/** 回滚节点 */
	private String CallPointID;
	/** 日志错误流水号码 */
	private String ErrSerialNo;
	/** 错误类型 */
	private String ErrType;
	/** 错误类型明细 */
	private String SubErrType;
	/** 错误描述 */
	private String ErrReMark;
	/** 申请日期 */
	private Date AppDate;
	/** 申请人 */
	private String Applicant;
	/** 申请状态 */
	private String AppState;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;

	public static final int FIELDNUM = 19;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public FIErrHandingItemSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "ErrAppNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		FIErrHandingItemSchema cloned = (FIErrHandingItemSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getErrAppNo()
	{
		return ErrAppNo;
	}
	public void setErrAppNo(String aErrAppNo)
	{
		ErrAppNo = aErrAppNo;
	}
	public String getBatchNo()
	{
		return BatchNo;
	}
	public void setBatchNo(String aBatchNo)
	{
		BatchNo = aBatchNo;
	}
	public String getBusTypeID()
	{
		return BusTypeID;
	}
	public void setBusTypeID(String aBusTypeID)
	{
		BusTypeID = aBusTypeID;
	}
	public String getCostID()
	{
		return CostID;
	}
	public void setCostID(String aCostID)
	{
		CostID = aCostID;
	}
	public String getIndexCode()
	{
		return IndexCode;
	}
	public void setIndexCode(String aIndexCode)
	{
		IndexCode = aIndexCode;
	}
	public String getIndexNo()
	{
		return IndexNo;
	}
	public void setIndexNo(String aIndexNo)
	{
		IndexNo = aIndexNo;
	}
	public String getCQNo()
	{
		return CQNo;
	}
	public void setCQNo(String aCQNo)
	{
		CQNo = aCQNo;
	}
	public String getDataSource()
	{
		return DataSource;
	}
	public void setDataSource(String aDataSource)
	{
		DataSource = aDataSource;
	}
	public String getDealType()
	{
		return DealType;
	}
	public void setDealType(String aDealType)
	{
		DealType = aDealType;
	}
	public String getCallPointID()
	{
		return CallPointID;
	}
	public void setCallPointID(String aCallPointID)
	{
		CallPointID = aCallPointID;
	}
	public String getErrSerialNo()
	{
		return ErrSerialNo;
	}
	public void setErrSerialNo(String aErrSerialNo)
	{
		ErrSerialNo = aErrSerialNo;
	}
	public String getErrType()
	{
		return ErrType;
	}
	public void setErrType(String aErrType)
	{
		ErrType = aErrType;
	}
	public String getSubErrType()
	{
		return SubErrType;
	}
	public void setSubErrType(String aSubErrType)
	{
		SubErrType = aSubErrType;
	}
	public String getErrReMark()
	{
		return ErrReMark;
	}
	public void setErrReMark(String aErrReMark)
	{
		ErrReMark = aErrReMark;
	}
	public String getAppDate()
	{
		if( AppDate != null )
			return fDate.getString(AppDate);
		else
			return null;
	}
	public void setAppDate(Date aAppDate)
	{
		AppDate = aAppDate;
	}
	public void setAppDate(String aAppDate)
	{
		if (aAppDate != null && !aAppDate.equals("") )
		{
			AppDate = fDate.getDate( aAppDate );
		}
		else
			AppDate = null;
	}

	public String getApplicant()
	{
		return Applicant;
	}
	public void setApplicant(String aApplicant)
	{
		Applicant = aApplicant;
	}
	public String getAppState()
	{
		return AppState;
	}
	public void setAppState(String aAppState)
	{
		AppState = aAppState;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}

	/**
	* 使用另外一个 FIErrHandingItemSchema 对象给 Schema 赋值
	* @param: aFIErrHandingItemSchema FIErrHandingItemSchema
	**/
	public void setSchema(FIErrHandingItemSchema aFIErrHandingItemSchema)
	{
		this.ErrAppNo = aFIErrHandingItemSchema.getErrAppNo();
		this.BatchNo = aFIErrHandingItemSchema.getBatchNo();
		this.BusTypeID = aFIErrHandingItemSchema.getBusTypeID();
		this.CostID = aFIErrHandingItemSchema.getCostID();
		this.IndexCode = aFIErrHandingItemSchema.getIndexCode();
		this.IndexNo = aFIErrHandingItemSchema.getIndexNo();
		this.CQNo = aFIErrHandingItemSchema.getCQNo();
		this.DataSource = aFIErrHandingItemSchema.getDataSource();
		this.DealType = aFIErrHandingItemSchema.getDealType();
		this.CallPointID = aFIErrHandingItemSchema.getCallPointID();
		this.ErrSerialNo = aFIErrHandingItemSchema.getErrSerialNo();
		this.ErrType = aFIErrHandingItemSchema.getErrType();
		this.SubErrType = aFIErrHandingItemSchema.getSubErrType();
		this.ErrReMark = aFIErrHandingItemSchema.getErrReMark();
		this.AppDate = fDate.getDate( aFIErrHandingItemSchema.getAppDate());
		this.Applicant = aFIErrHandingItemSchema.getApplicant();
		this.AppState = aFIErrHandingItemSchema.getAppState();
		this.MakeDate = fDate.getDate( aFIErrHandingItemSchema.getMakeDate());
		this.MakeTime = aFIErrHandingItemSchema.getMakeTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("ErrAppNo") == null )
				this.ErrAppNo = null;
			else
				this.ErrAppNo = rs.getString("ErrAppNo").trim();

			if( rs.getString("BatchNo") == null )
				this.BatchNo = null;
			else
				this.BatchNo = rs.getString("BatchNo").trim();

			if( rs.getString("BusTypeID") == null )
				this.BusTypeID = null;
			else
				this.BusTypeID = rs.getString("BusTypeID").trim();

			if( rs.getString("CostID") == null )
				this.CostID = null;
			else
				this.CostID = rs.getString("CostID").trim();

			if( rs.getString("IndexCode") == null )
				this.IndexCode = null;
			else
				this.IndexCode = rs.getString("IndexCode").trim();

			if( rs.getString("IndexNo") == null )
				this.IndexNo = null;
			else
				this.IndexNo = rs.getString("IndexNo").trim();

			if( rs.getString("CQNo") == null )
				this.CQNo = null;
			else
				this.CQNo = rs.getString("CQNo").trim();

			if( rs.getString("DataSource") == null )
				this.DataSource = null;
			else
				this.DataSource = rs.getString("DataSource").trim();

			if( rs.getString("DealType") == null )
				this.DealType = null;
			else
				this.DealType = rs.getString("DealType").trim();

			if( rs.getString("CallPointID") == null )
				this.CallPointID = null;
			else
				this.CallPointID = rs.getString("CallPointID").trim();

			if( rs.getString("ErrSerialNo") == null )
				this.ErrSerialNo = null;
			else
				this.ErrSerialNo = rs.getString("ErrSerialNo").trim();

			if( rs.getString("ErrType") == null )
				this.ErrType = null;
			else
				this.ErrType = rs.getString("ErrType").trim();

			if( rs.getString("SubErrType") == null )
				this.SubErrType = null;
			else
				this.SubErrType = rs.getString("SubErrType").trim();

			if( rs.getString("ErrReMark") == null )
				this.ErrReMark = null;
			else
				this.ErrReMark = rs.getString("ErrReMark").trim();

			this.AppDate = rs.getDate("AppDate");
			if( rs.getString("Applicant") == null )
				this.Applicant = null;
			else
				this.Applicant = rs.getString("Applicant").trim();

			if( rs.getString("AppState") == null )
				this.AppState = null;
			else
				this.AppState = rs.getString("AppState").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的FIErrHandingItem表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIErrHandingItemSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public FIErrHandingItemSchema getSchema()
	{
		FIErrHandingItemSchema aFIErrHandingItemSchema = new FIErrHandingItemSchema();
		aFIErrHandingItemSchema.setSchema(this);
		return aFIErrHandingItemSchema;
	}

	public FIErrHandingItemDB getDB()
	{
		FIErrHandingItemDB aDBOper = new FIErrHandingItemDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpFIErrHandingItem描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(ErrAppNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BatchNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusTypeID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CostID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IndexCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IndexNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CQNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DataSource)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DealType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CallPointID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ErrSerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ErrType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SubErrType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ErrReMark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( AppDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Applicant)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppState)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpFIErrHandingItem>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			ErrAppNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			BatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			BusTypeID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			CostID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			IndexCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			IndexNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			CQNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			DataSource = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			DealType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			CallPointID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			ErrSerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			ErrType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			SubErrType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			ErrReMark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			AppDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,SysConst.PACKAGESPILTER));
			Applicant = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			AppState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIErrHandingItemSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("ErrAppNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ErrAppNo));
		}
		if (FCode.equals("BatchNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BatchNo));
		}
		if (FCode.equals("BusTypeID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusTypeID));
		}
		if (FCode.equals("CostID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CostID));
		}
		if (FCode.equals("IndexCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IndexCode));
		}
		if (FCode.equals("IndexNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IndexNo));
		}
		if (FCode.equals("CQNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CQNo));
		}
		if (FCode.equals("DataSource"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DataSource));
		}
		if (FCode.equals("DealType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DealType));
		}
		if (FCode.equals("CallPointID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CallPointID));
		}
		if (FCode.equals("ErrSerialNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ErrSerialNo));
		}
		if (FCode.equals("ErrType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ErrType));
		}
		if (FCode.equals("SubErrType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SubErrType));
		}
		if (FCode.equals("ErrReMark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ErrReMark));
		}
		if (FCode.equals("AppDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getAppDate()));
		}
		if (FCode.equals("Applicant"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Applicant));
		}
		if (FCode.equals("AppState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppState));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(ErrAppNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(BatchNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(BusTypeID);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(CostID);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(IndexCode);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(IndexNo);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(CQNo);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(DataSource);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(DealType);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(CallPointID);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(ErrSerialNo);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(ErrType);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(SubErrType);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(ErrReMark);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getAppDate()));
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(Applicant);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(AppState);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("ErrAppNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ErrAppNo = FValue.trim();
			}
			else
				ErrAppNo = null;
		}
		if (FCode.equalsIgnoreCase("BatchNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BatchNo = FValue.trim();
			}
			else
				BatchNo = null;
		}
		if (FCode.equalsIgnoreCase("BusTypeID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusTypeID = FValue.trim();
			}
			else
				BusTypeID = null;
		}
		if (FCode.equalsIgnoreCase("CostID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CostID = FValue.trim();
			}
			else
				CostID = null;
		}
		if (FCode.equalsIgnoreCase("IndexCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IndexCode = FValue.trim();
			}
			else
				IndexCode = null;
		}
		if (FCode.equalsIgnoreCase("IndexNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IndexNo = FValue.trim();
			}
			else
				IndexNo = null;
		}
		if (FCode.equalsIgnoreCase("CQNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CQNo = FValue.trim();
			}
			else
				CQNo = null;
		}
		if (FCode.equalsIgnoreCase("DataSource"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DataSource = FValue.trim();
			}
			else
				DataSource = null;
		}
		if (FCode.equalsIgnoreCase("DealType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DealType = FValue.trim();
			}
			else
				DealType = null;
		}
		if (FCode.equalsIgnoreCase("CallPointID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CallPointID = FValue.trim();
			}
			else
				CallPointID = null;
		}
		if (FCode.equalsIgnoreCase("ErrSerialNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ErrSerialNo = FValue.trim();
			}
			else
				ErrSerialNo = null;
		}
		if (FCode.equalsIgnoreCase("ErrType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ErrType = FValue.trim();
			}
			else
				ErrType = null;
		}
		if (FCode.equalsIgnoreCase("SubErrType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SubErrType = FValue.trim();
			}
			else
				SubErrType = null;
		}
		if (FCode.equalsIgnoreCase("ErrReMark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ErrReMark = FValue.trim();
			}
			else
				ErrReMark = null;
		}
		if (FCode.equalsIgnoreCase("AppDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				AppDate = fDate.getDate( FValue );
			}
			else
				AppDate = null;
		}
		if (FCode.equalsIgnoreCase("Applicant"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Applicant = FValue.trim();
			}
			else
				Applicant = null;
		}
		if (FCode.equalsIgnoreCase("AppState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppState = FValue.trim();
			}
			else
				AppState = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		FIErrHandingItemSchema other = (FIErrHandingItemSchema)otherObject;
		return
			(ErrAppNo == null ? other.getErrAppNo() == null : ErrAppNo.equals(other.getErrAppNo()))
			&& (BatchNo == null ? other.getBatchNo() == null : BatchNo.equals(other.getBatchNo()))
			&& (BusTypeID == null ? other.getBusTypeID() == null : BusTypeID.equals(other.getBusTypeID()))
			&& (CostID == null ? other.getCostID() == null : CostID.equals(other.getCostID()))
			&& (IndexCode == null ? other.getIndexCode() == null : IndexCode.equals(other.getIndexCode()))
			&& (IndexNo == null ? other.getIndexNo() == null : IndexNo.equals(other.getIndexNo()))
			&& (CQNo == null ? other.getCQNo() == null : CQNo.equals(other.getCQNo()))
			&& (DataSource == null ? other.getDataSource() == null : DataSource.equals(other.getDataSource()))
			&& (DealType == null ? other.getDealType() == null : DealType.equals(other.getDealType()))
			&& (CallPointID == null ? other.getCallPointID() == null : CallPointID.equals(other.getCallPointID()))
			&& (ErrSerialNo == null ? other.getErrSerialNo() == null : ErrSerialNo.equals(other.getErrSerialNo()))
			&& (ErrType == null ? other.getErrType() == null : ErrType.equals(other.getErrType()))
			&& (SubErrType == null ? other.getSubErrType() == null : SubErrType.equals(other.getSubErrType()))
			&& (ErrReMark == null ? other.getErrReMark() == null : ErrReMark.equals(other.getErrReMark()))
			&& (AppDate == null ? other.getAppDate() == null : fDate.getString(AppDate).equals(other.getAppDate()))
			&& (Applicant == null ? other.getApplicant() == null : Applicant.equals(other.getApplicant()))
			&& (AppState == null ? other.getAppState() == null : AppState.equals(other.getAppState()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("ErrAppNo") ) {
			return 0;
		}
		if( strFieldName.equals("BatchNo") ) {
			return 1;
		}
		if( strFieldName.equals("BusTypeID") ) {
			return 2;
		}
		if( strFieldName.equals("CostID") ) {
			return 3;
		}
		if( strFieldName.equals("IndexCode") ) {
			return 4;
		}
		if( strFieldName.equals("IndexNo") ) {
			return 5;
		}
		if( strFieldName.equals("CQNo") ) {
			return 6;
		}
		if( strFieldName.equals("DataSource") ) {
			return 7;
		}
		if( strFieldName.equals("DealType") ) {
			return 8;
		}
		if( strFieldName.equals("CallPointID") ) {
			return 9;
		}
		if( strFieldName.equals("ErrSerialNo") ) {
			return 10;
		}
		if( strFieldName.equals("ErrType") ) {
			return 11;
		}
		if( strFieldName.equals("SubErrType") ) {
			return 12;
		}
		if( strFieldName.equals("ErrReMark") ) {
			return 13;
		}
		if( strFieldName.equals("AppDate") ) {
			return 14;
		}
		if( strFieldName.equals("Applicant") ) {
			return 15;
		}
		if( strFieldName.equals("AppState") ) {
			return 16;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 17;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 18;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "ErrAppNo";
				break;
			case 1:
				strFieldName = "BatchNo";
				break;
			case 2:
				strFieldName = "BusTypeID";
				break;
			case 3:
				strFieldName = "CostID";
				break;
			case 4:
				strFieldName = "IndexCode";
				break;
			case 5:
				strFieldName = "IndexNo";
				break;
			case 6:
				strFieldName = "CQNo";
				break;
			case 7:
				strFieldName = "DataSource";
				break;
			case 8:
				strFieldName = "DealType";
				break;
			case 9:
				strFieldName = "CallPointID";
				break;
			case 10:
				strFieldName = "ErrSerialNo";
				break;
			case 11:
				strFieldName = "ErrType";
				break;
			case 12:
				strFieldName = "SubErrType";
				break;
			case 13:
				strFieldName = "ErrReMark";
				break;
			case 14:
				strFieldName = "AppDate";
				break;
			case 15:
				strFieldName = "Applicant";
				break;
			case 16:
				strFieldName = "AppState";
				break;
			case 17:
				strFieldName = "MakeDate";
				break;
			case 18:
				strFieldName = "MakeTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("ErrAppNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BatchNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BusTypeID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CostID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IndexCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IndexNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CQNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DataSource") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DealType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CallPointID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ErrSerialNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ErrType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SubErrType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ErrReMark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Applicant") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
