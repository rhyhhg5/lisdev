/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.FIBnTypeDefDB;

/*
 * <p>ClassName: FIBnTypeDefSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 财务接口最新版
 * @CreateDate：2011-12-13
 */
public class FIBnTypeDefSchema implements Schema, Cloneable
{
	// @Field
	/** 版本编号 */
	private String VersionNo;
	/** 业务交易编号 */
	private String BusinessID;
	/** 业务交易名称 */
	private String BusinessName;
	/** 业务大类 */
	private String BusType;
	/** 业务类别 */
	private String DetailType;
	/** 业务对象 */
	private String Object;
	/** 对象编号 */
	private String ObjectID;
	/** 索引编码 */
	private String IndexCode;
	/** 索引名称 */
	private String IndexName;
	/** 状态 */
	private String State;
	/** 说明 */
	private String Remark;

	public static final int FIELDNUM = 11;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public FIBnTypeDefSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "VersionNo";
		pk[1] = "BusinessID";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		FIBnTypeDefSchema cloned = (FIBnTypeDefSchema) super.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getVersionNo()
	{
		return VersionNo;
	}
	public void setVersionNo(String aVersionNo)
	{
		VersionNo = aVersionNo;
	}
	public String getBusinessID()
	{
		return BusinessID;
	}
	public void setBusinessID(String aBusinessID)
	{
		BusinessID = aBusinessID;
	}
	public String getBusinessName()
	{
		return BusinessName;
	}
	public void setBusinessName(String aBusinessName)
	{
		BusinessName = aBusinessName;
	}
	public String getBusType()
	{
		return BusType;
	}
	public void setBusType(String aBusType)
	{
		BusType = aBusType;
	}
	public String getDetailType()
	{
		return DetailType;
	}
	public void setDetailType(String aDetailType)
	{
		DetailType = aDetailType;
	}
	public String getObject()
	{
		return Object;
	}
	public void setObject(String aObject)
	{
		Object = aObject;
	}
	public String getObjectID()
	{
		return ObjectID;
	}
	public void setObjectID(String aObjectID)
	{
		ObjectID = aObjectID;
	}
	public String getIndexCode()
	{
		return IndexCode;
	}
	public void setIndexCode(String aIndexCode)
	{
		IndexCode = aIndexCode;
	}
	public String getIndexName()
	{
		return IndexName;
	}
	public void setIndexName(String aIndexName)
	{
		IndexName = aIndexName;
	}
	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}
	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
		Remark = aRemark;
	}

	/**
	* 使用另外一个 FIBnTypeDefSchema 对象给 Schema 赋值
	* @param: aFIBnTypeDefSchema FIBnTypeDefSchema
	**/
	public void setSchema(FIBnTypeDefSchema aFIBnTypeDefSchema)
	{
		this.VersionNo = aFIBnTypeDefSchema.getVersionNo();
		this.BusinessID = aFIBnTypeDefSchema.getBusinessID();
		this.BusinessName = aFIBnTypeDefSchema.getBusinessName();
		this.BusType = aFIBnTypeDefSchema.getBusType();
		this.DetailType = aFIBnTypeDefSchema.getDetailType();
		this.Object = aFIBnTypeDefSchema.getObject();
		this.ObjectID = aFIBnTypeDefSchema.getObjectID();
		this.IndexCode = aFIBnTypeDefSchema.getIndexCode();
		this.IndexName = aFIBnTypeDefSchema.getIndexName();
		this.State = aFIBnTypeDefSchema.getState();
		this.Remark = aFIBnTypeDefSchema.getRemark();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("VersionNo") == null )
				this.VersionNo = null;
			else
				this.VersionNo = rs.getString("VersionNo").trim();

			if( rs.getString("BusinessID") == null )
				this.BusinessID = null;
			else
				this.BusinessID = rs.getString("BusinessID").trim();

			if( rs.getString("BusinessName") == null )
				this.BusinessName = null;
			else
				this.BusinessName = rs.getString("BusinessName").trim();

			if( rs.getString("BusType") == null )
				this.BusType = null;
			else
				this.BusType = rs.getString("BusType").trim();

			if( rs.getString("DetailType") == null )
				this.DetailType = null;
			else
				this.DetailType = rs.getString("DetailType").trim();

			if( rs.getString("Object") == null )
				this.Object = null;
			else
				this.Object = rs.getString("Object").trim();

			if( rs.getString("ObjectID") == null )
				this.ObjectID = null;
			else
				this.ObjectID = rs.getString("ObjectID").trim();

			if( rs.getString("IndexCode") == null )
				this.IndexCode = null;
			else
				this.IndexCode = rs.getString("IndexCode").trim();

			if( rs.getString("IndexName") == null )
				this.IndexName = null;
			else
				this.IndexName = rs.getString("IndexName").trim();

			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的FIBnTypeDef表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIBnTypeDefSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public FIBnTypeDefSchema getSchema()
	{
		FIBnTypeDefSchema aFIBnTypeDefSchema = new FIBnTypeDefSchema();
		aFIBnTypeDefSchema.setSchema(this);
		return aFIBnTypeDefSchema;
	}

	public FIBnTypeDefDB getDB()
	{
		FIBnTypeDefDB aDBOper = new FIBnTypeDefDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpFIBnTypeDef描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(VersionNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusinessID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusinessName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DetailType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Object)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ObjectID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IndexCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IndexName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpFIBnTypeDef>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			VersionNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			BusinessID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			BusinessName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			BusType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			DetailType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			Object = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			ObjectID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			IndexCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			IndexName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIBnTypeDefSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("VersionNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(VersionNo));
		}
		if (FCode.equals("BusinessID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusinessID));
		}
		if (FCode.equals("BusinessName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusinessName));
		}
		if (FCode.equals("BusType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusType));
		}
		if (FCode.equals("DetailType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DetailType));
		}
		if (FCode.equals("Object"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Object));
		}
		if (FCode.equals("ObjectID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ObjectID));
		}
		if (FCode.equals("IndexCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IndexCode));
		}
		if (FCode.equals("IndexName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IndexName));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(VersionNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(BusinessID);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(BusinessName);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(BusType);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(DetailType);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(Object);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(ObjectID);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(IndexCode);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(IndexName);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("VersionNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				VersionNo = FValue.trim();
			}
			else
				VersionNo = null;
		}
		if (FCode.equalsIgnoreCase("BusinessID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusinessID = FValue.trim();
			}
			else
				BusinessID = null;
		}
		if (FCode.equalsIgnoreCase("BusinessName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusinessName = FValue.trim();
			}
			else
				BusinessName = null;
		}
		if (FCode.equalsIgnoreCase("BusType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusType = FValue.trim();
			}
			else
				BusType = null;
		}
		if (FCode.equalsIgnoreCase("DetailType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DetailType = FValue.trim();
			}
			else
				DetailType = null;
		}
		if (FCode.equalsIgnoreCase("Object"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Object = FValue.trim();
			}
			else
				Object = null;
		}
		if (FCode.equalsIgnoreCase("ObjectID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ObjectID = FValue.trim();
			}
			else
				ObjectID = null;
		}
		if (FCode.equalsIgnoreCase("IndexCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IndexCode = FValue.trim();
			}
			else
				IndexCode = null;
		}
		if (FCode.equalsIgnoreCase("IndexName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IndexName = FValue.trim();
			}
			else
				IndexName = null;
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		FIBnTypeDefSchema other = (FIBnTypeDefSchema)otherObject;
		return
			(VersionNo == null ? other.getVersionNo() == null : VersionNo.equals(other.getVersionNo()))
			&& (BusinessID == null ? other.getBusinessID() == null : BusinessID.equals(other.getBusinessID()))
			&& (BusinessName == null ? other.getBusinessName() == null : BusinessName.equals(other.getBusinessName()))
			&& (BusType == null ? other.getBusType() == null : BusType.equals(other.getBusType()))
			&& (DetailType == null ? other.getDetailType() == null : DetailType.equals(other.getDetailType()))
			&& (Object == null ? other.getObject() == null : Object.equals(other.getObject()))
			&& (ObjectID == null ? other.getObjectID() == null : ObjectID.equals(other.getObjectID()))
			&& (IndexCode == null ? other.getIndexCode() == null : IndexCode.equals(other.getIndexCode()))
			&& (IndexName == null ? other.getIndexName() == null : IndexName.equals(other.getIndexName()))
			&& (State == null ? other.getState() == null : State.equals(other.getState()))
			&& (Remark == null ? other.getRemark() == null : Remark.equals(other.getRemark()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("VersionNo") ) {
			return 0;
		}
		if( strFieldName.equals("BusinessID") ) {
			return 1;
		}
		if( strFieldName.equals("BusinessName") ) {
			return 2;
		}
		if( strFieldName.equals("BusType") ) {
			return 3;
		}
		if( strFieldName.equals("DetailType") ) {
			return 4;
		}
		if( strFieldName.equals("Object") ) {
			return 5;
		}
		if( strFieldName.equals("ObjectID") ) {
			return 6;
		}
		if( strFieldName.equals("IndexCode") ) {
			return 7;
		}
		if( strFieldName.equals("IndexName") ) {
			return 8;
		}
		if( strFieldName.equals("State") ) {
			return 9;
		}
		if( strFieldName.equals("Remark") ) {
			return 10;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "VersionNo";
				break;
			case 1:
				strFieldName = "BusinessID";
				break;
			case 2:
				strFieldName = "BusinessName";
				break;
			case 3:
				strFieldName = "BusType";
				break;
			case 4:
				strFieldName = "DetailType";
				break;
			case 5:
				strFieldName = "Object";
				break;
			case 6:
				strFieldName = "ObjectID";
				break;
			case 7:
				strFieldName = "IndexCode";
				break;
			case 8:
				strFieldName = "IndexName";
				break;
			case 9:
				strFieldName = "State";
				break;
			case 10:
				strFieldName = "Remark";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("VersionNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BusinessID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BusinessName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BusType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DetailType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Object") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ObjectID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IndexCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IndexName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
