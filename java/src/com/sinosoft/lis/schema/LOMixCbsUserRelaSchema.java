/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LOMixCbsUserRelaDB;

/*
 * <p>ClassName: LOMixCbsUserRelaSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2010-07-28
 */
public class LOMixCbsUserRelaSchema implements Schema, Cloneable
{
	// @Field
	/** 代理方出单人员工号 */
	private String operatorcode;
	/** 子公司代码 */
	private String comp_cod;
	/** 管理机构编号 */
	private String man_org_cod;
	/** 用户姓名 */
	private String UserName;
	/** 口令 */
	private String Password;
	/** 是否上报 */
	private String Crs_Check_Status;

	public static final int FIELDNUM = 6;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LOMixCbsUserRelaSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[3];
		pk[0] = "operatorcode";
		pk[1] = "comp_cod";
		pk[2] = "man_org_cod";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LOMixCbsUserRelaSchema cloned = (LOMixCbsUserRelaSchema) super.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getoperatorcode()
	{
		return operatorcode;
	}
	public void setoperatorcode(String aoperatorcode)
	{
		operatorcode = aoperatorcode;
	}
	public String getcomp_cod()
	{
		return comp_cod;
	}
	public void setcomp_cod(String acomp_cod)
	{
		comp_cod = acomp_cod;
	}
	public String getman_org_cod()
	{
		return man_org_cod;
	}
	public void setman_org_cod(String aman_org_cod)
	{
		man_org_cod = aman_org_cod;
	}
	public String getUserName()
	{
		return UserName;
	}
	public void setUserName(String aUserName)
	{
		UserName = aUserName;
	}
	public String getPassword()
	{
		return Password;
	}
	public void setPassword(String aPassword)
	{
		Password = aPassword;
	}
	public String getCrs_Check_Status()
	{
		return Crs_Check_Status;
	}
	public void setCrs_Check_Status(String aCrs_Check_Status)
	{
		Crs_Check_Status = aCrs_Check_Status;
	}

	/**
	* 使用另外一个 LOMixCbsUserRelaSchema 对象给 Schema 赋值
	* @param: aLOMixCbsUserRelaSchema LOMixCbsUserRelaSchema
	**/
	public void setSchema(LOMixCbsUserRelaSchema aLOMixCbsUserRelaSchema)
	{
		this.operatorcode = aLOMixCbsUserRelaSchema.getoperatorcode();
		this.comp_cod = aLOMixCbsUserRelaSchema.getcomp_cod();
		this.man_org_cod = aLOMixCbsUserRelaSchema.getman_org_cod();
		this.UserName = aLOMixCbsUserRelaSchema.getUserName();
		this.Password = aLOMixCbsUserRelaSchema.getPassword();
		this.Crs_Check_Status = aLOMixCbsUserRelaSchema.getCrs_Check_Status();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("operatorcode") == null )
				this.operatorcode = null;
			else
				this.operatorcode = rs.getString("operatorcode").trim();

			if( rs.getString("comp_cod") == null )
				this.comp_cod = null;
			else
				this.comp_cod = rs.getString("comp_cod").trim();

			if( rs.getString("man_org_cod") == null )
				this.man_org_cod = null;
			else
				this.man_org_cod = rs.getString("man_org_cod").trim();

			if( rs.getString("UserName") == null )
				this.UserName = null;
			else
				this.UserName = rs.getString("UserName").trim();

			if( rs.getString("Password") == null )
				this.Password = null;
			else
				this.Password = rs.getString("Password").trim();

			if( rs.getString("Crs_Check_Status") == null )
				this.Crs_Check_Status = null;
			else
				this.Crs_Check_Status = rs.getString("Crs_Check_Status").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LOMixCbsUserRela表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LOMixCbsUserRelaSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LOMixCbsUserRelaSchema getSchema()
	{
		LOMixCbsUserRelaSchema aLOMixCbsUserRelaSchema = new LOMixCbsUserRelaSchema();
		aLOMixCbsUserRelaSchema.setSchema(this);
		return aLOMixCbsUserRelaSchema;
	}

	public LOMixCbsUserRelaDB getDB()
	{
		LOMixCbsUserRelaDB aDBOper = new LOMixCbsUserRelaDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOMixCbsUserRela描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(operatorcode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(comp_cod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(man_org_cod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UserName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Password)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Crs_Check_Status));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOMixCbsUserRela>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			operatorcode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			comp_cod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			man_org_cod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			UserName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			Password = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			Crs_Check_Status = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LOMixCbsUserRelaSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("operatorcode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(operatorcode));
		}
		if (FCode.equals("comp_cod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(comp_cod));
		}
		if (FCode.equals("man_org_cod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(man_org_cod));
		}
		if (FCode.equals("UserName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UserName));
		}
		if (FCode.equals("Password"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Password));
		}
		if (FCode.equals("Crs_Check_Status"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Crs_Check_Status));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(operatorcode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(comp_cod);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(man_org_cod);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(UserName);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(Password);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(Crs_Check_Status);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("operatorcode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				operatorcode = FValue.trim();
			}
			else
				operatorcode = null;
		}
		if (FCode.equalsIgnoreCase("comp_cod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				comp_cod = FValue.trim();
			}
			else
				comp_cod = null;
		}
		if (FCode.equalsIgnoreCase("man_org_cod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				man_org_cod = FValue.trim();
			}
			else
				man_org_cod = null;
		}
		if (FCode.equalsIgnoreCase("UserName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UserName = FValue.trim();
			}
			else
				UserName = null;
		}
		if (FCode.equalsIgnoreCase("Password"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Password = FValue.trim();
			}
			else
				Password = null;
		}
		if (FCode.equalsIgnoreCase("Crs_Check_Status"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Crs_Check_Status = FValue.trim();
			}
			else
				Crs_Check_Status = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LOMixCbsUserRelaSchema other = (LOMixCbsUserRelaSchema)otherObject;
		return
			(operatorcode == null ? other.getoperatorcode() == null : operatorcode.equals(other.getoperatorcode()))
			&& (comp_cod == null ? other.getcomp_cod() == null : comp_cod.equals(other.getcomp_cod()))
			&& (man_org_cod == null ? other.getman_org_cod() == null : man_org_cod.equals(other.getman_org_cod()))
			&& (UserName == null ? other.getUserName() == null : UserName.equals(other.getUserName()))
			&& (Password == null ? other.getPassword() == null : Password.equals(other.getPassword()))
			&& (Crs_Check_Status == null ? other.getCrs_Check_Status() == null : Crs_Check_Status.equals(other.getCrs_Check_Status()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("operatorcode") ) {
			return 0;
		}
		if( strFieldName.equals("comp_cod") ) {
			return 1;
		}
		if( strFieldName.equals("man_org_cod") ) {
			return 2;
		}
		if( strFieldName.equals("UserName") ) {
			return 3;
		}
		if( strFieldName.equals("Password") ) {
			return 4;
		}
		if( strFieldName.equals("Crs_Check_Status") ) {
			return 5;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "operatorcode";
				break;
			case 1:
				strFieldName = "comp_cod";
				break;
			case 2:
				strFieldName = "man_org_cod";
				break;
			case 3:
				strFieldName = "UserName";
				break;
			case 4:
				strFieldName = "Password";
				break;
			case 5:
				strFieldName = "Crs_Check_Status";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("operatorcode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("comp_cod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("man_org_cod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UserName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Password") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Crs_Check_Status") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
