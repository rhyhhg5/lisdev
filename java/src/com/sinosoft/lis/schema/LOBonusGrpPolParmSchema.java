/*
 * <p>ClassName: LOBonusGrpPolParmSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LOBonusGrpPolParmDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LOBonusGrpPolParmSchema implements Schema
{
    // @Field
    /** 红利分配会计年度 */
    private int FiscalYear;
    /** 集体保单号码 */
    private String GrpPolNo;
    /** 状态 */
    private String ComputeState;
    /** 实际投资收益率 */
    private double ActuRate;
    /** 保证年收益率 */
    private double EnsuRate;
    /** 默认保证年收益率 */
    private double EnsuRateDefault;
    /** 分红比率 */
    private double AssignRate;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 红利应计算日期 */
    private Date SGetDate;
    /** 险种编码 */
    private String RiskCode;
    /** 红利金额 */
    private double SumBonus;

    public static final int FIELDNUM = 15; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LOBonusGrpPolParmSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "FiscalYear";
        pk[1] = "GrpPolNo";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public int getFiscalYear()
    {
        return FiscalYear;
    }

    public void setFiscalYear(int aFiscalYear)
    {
        FiscalYear = aFiscalYear;
    }

    public void setFiscalYear(String aFiscalYear)
    {
        if (aFiscalYear != null && !aFiscalYear.equals(""))
        {
            Integer tInteger = new Integer(aFiscalYear);
            int i = tInteger.intValue();
            FiscalYear = i;
        }
    }

    public String getGrpPolNo()
    {
        if (GrpPolNo != null && !GrpPolNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            GrpPolNo = StrTool.unicodeToGBK(GrpPolNo);
        }
        return GrpPolNo;
    }

    public void setGrpPolNo(String aGrpPolNo)
    {
        GrpPolNo = aGrpPolNo;
    }

    public String getComputeState()
    {
        if (ComputeState != null && !ComputeState.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ComputeState = StrTool.unicodeToGBK(ComputeState);
        }
        return ComputeState;
    }

    public void setComputeState(String aComputeState)
    {
        ComputeState = aComputeState;
    }

    public double getActuRate()
    {
        return ActuRate;
    }

    public void setActuRate(double aActuRate)
    {
        ActuRate = aActuRate;
    }

    public void setActuRate(String aActuRate)
    {
        if (aActuRate != null && !aActuRate.equals(""))
        {
            Double tDouble = new Double(aActuRate);
            double d = tDouble.doubleValue();
            ActuRate = d;
        }
    }

    public double getEnsuRate()
    {
        return EnsuRate;
    }

    public void setEnsuRate(double aEnsuRate)
    {
        EnsuRate = aEnsuRate;
    }

    public void setEnsuRate(String aEnsuRate)
    {
        if (aEnsuRate != null && !aEnsuRate.equals(""))
        {
            Double tDouble = new Double(aEnsuRate);
            double d = tDouble.doubleValue();
            EnsuRate = d;
        }
    }

    public double getEnsuRateDefault()
    {
        return EnsuRateDefault;
    }

    public void setEnsuRateDefault(double aEnsuRateDefault)
    {
        EnsuRateDefault = aEnsuRateDefault;
    }

    public void setEnsuRateDefault(String aEnsuRateDefault)
    {
        if (aEnsuRateDefault != null && !aEnsuRateDefault.equals(""))
        {
            Double tDouble = new Double(aEnsuRateDefault);
            double d = tDouble.doubleValue();
            EnsuRateDefault = d;
        }
    }

    public double getAssignRate()
    {
        return AssignRate;
    }

    public void setAssignRate(double aAssignRate)
    {
        AssignRate = aAssignRate;
    }

    public void setAssignRate(String aAssignRate)
    {
        if (aAssignRate != null && !aAssignRate.equals(""))
        {
            Double tDouble = new Double(aAssignRate);
            double d = tDouble.doubleValue();
            AssignRate = d;
        }
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    public String getSGetDate()
    {
        if (SGetDate != null)
        {
            return fDate.getString(SGetDate);
        }
        else
        {
            return null;
        }
    }

    public void setSGetDate(Date aSGetDate)
    {
        SGetDate = aSGetDate;
    }

    public void setSGetDate(String aSGetDate)
    {
        if (aSGetDate != null && !aSGetDate.equals(""))
        {
            SGetDate = fDate.getDate(aSGetDate);
        }
        else
        {
            SGetDate = null;
        }
    }

    public String getRiskCode()
    {
        if (RiskCode != null && !RiskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public double getSumBonus()
    {
        return SumBonus;
    }

    public void setSumBonus(double aSumBonus)
    {
        SumBonus = aSumBonus;
    }

    public void setSumBonus(String aSumBonus)
    {
        if (aSumBonus != null && !aSumBonus.equals(""))
        {
            Double tDouble = new Double(aSumBonus);
            double d = tDouble.doubleValue();
            SumBonus = d;
        }
    }


    /**
     * 使用另外一个 LOBonusGrpPolParmSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LOBonusGrpPolParmSchema aLOBonusGrpPolParmSchema)
    {
        this.FiscalYear = aLOBonusGrpPolParmSchema.getFiscalYear();
        this.GrpPolNo = aLOBonusGrpPolParmSchema.getGrpPolNo();
        this.ComputeState = aLOBonusGrpPolParmSchema.getComputeState();
        this.ActuRate = aLOBonusGrpPolParmSchema.getActuRate();
        this.EnsuRate = aLOBonusGrpPolParmSchema.getEnsuRate();
        this.EnsuRateDefault = aLOBonusGrpPolParmSchema.getEnsuRateDefault();
        this.AssignRate = aLOBonusGrpPolParmSchema.getAssignRate();
        this.Operator = aLOBonusGrpPolParmSchema.getOperator();
        this.MakeDate = fDate.getDate(aLOBonusGrpPolParmSchema.getMakeDate());
        this.MakeTime = aLOBonusGrpPolParmSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLOBonusGrpPolParmSchema.getModifyDate());
        this.ModifyTime = aLOBonusGrpPolParmSchema.getModifyTime();
        this.SGetDate = fDate.getDate(aLOBonusGrpPolParmSchema.getSGetDate());
        this.RiskCode = aLOBonusGrpPolParmSchema.getRiskCode();
        this.SumBonus = aLOBonusGrpPolParmSchema.getSumBonus();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            this.FiscalYear = rs.getInt("FiscalYear");
            if (rs.getString("GrpPolNo") == null)
            {
                this.GrpPolNo = null;
            }
            else
            {
                this.GrpPolNo = rs.getString("GrpPolNo").trim();
            }

            if (rs.getString("ComputeState") == null)
            {
                this.ComputeState = null;
            }
            else
            {
                this.ComputeState = rs.getString("ComputeState").trim();
            }

            this.ActuRate = rs.getDouble("ActuRate");
            this.EnsuRate = rs.getDouble("EnsuRate");
            this.EnsuRateDefault = rs.getDouble("EnsuRateDefault");
            this.AssignRate = rs.getDouble("AssignRate");
            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            this.SGetDate = rs.getDate("SGetDate");
            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            this.SumBonus = rs.getDouble("SumBonus");
        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LOBonusGrpPolParmSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LOBonusGrpPolParmSchema getSchema()
    {
        LOBonusGrpPolParmSchema aLOBonusGrpPolParmSchema = new
                LOBonusGrpPolParmSchema();
        aLOBonusGrpPolParmSchema.setSchema(this);
        return aLOBonusGrpPolParmSchema;
    }

    public LOBonusGrpPolParmDB getDB()
    {
        LOBonusGrpPolParmDB aDBOper = new LOBonusGrpPolParmDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOBonusGrpPolParm描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = ChgData.chgData(FiscalYear) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GrpPolNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ComputeState)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(ActuRate) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(EnsuRate) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(EnsuRateDefault) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(AssignRate) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(SGetDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(SumBonus);
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOBonusGrpPolParm>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            FiscalYear = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 1, SysConst.PACKAGESPILTER))).intValue();
            GrpPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            ComputeState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                          SysConst.PACKAGESPILTER);
            ActuRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 4, SysConst.PACKAGESPILTER))).doubleValue();
            EnsuRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 5, SysConst.PACKAGESPILTER))).doubleValue();
            EnsuRateDefault = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 6, SysConst.PACKAGESPILTER))).doubleValue();
            AssignRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 7, SysConst.PACKAGESPILTER))).doubleValue();
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 9, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 11, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                        SysConst.PACKAGESPILTER);
            SGetDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 13, SysConst.PACKAGESPILTER));
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                      SysConst.PACKAGESPILTER);
            SumBonus = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 15, SysConst.PACKAGESPILTER))).doubleValue();
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LOBonusGrpPolParmSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("FiscalYear"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(FiscalYear));
        }
        if (FCode.equals("GrpPolNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GrpPolNo));
        }
        if (FCode.equals("ComputeState"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ComputeState));
        }
        if (FCode.equals("ActuRate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ActuRate));
        }
        if (FCode.equals("EnsuRate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(EnsuRate));
        }
        if (FCode.equals("EnsuRateDefault"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(EnsuRateDefault));
        }
        if (FCode.equals("AssignRate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AssignRate));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (FCode.equals("SGetDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getSGetDate()));
        }
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskCode));
        }
        if (FCode.equals("SumBonus"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SumBonus));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = String.valueOf(FiscalYear);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(GrpPolNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ComputeState);
                break;
            case 3:
                strFieldValue = String.valueOf(ActuRate);
                break;
            case 4:
                strFieldValue = String.valueOf(EnsuRate);
                break;
            case 5:
                strFieldValue = String.valueOf(EnsuRateDefault);
                break;
            case 6:
                strFieldValue = String.valueOf(AssignRate);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getSGetDate()));
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 14:
                strFieldValue = String.valueOf(SumBonus);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("FiscalYear"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                FiscalYear = i;
            }
        }
        if (FCode.equals("GrpPolNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpPolNo = FValue.trim();
            }
            else
            {
                GrpPolNo = null;
            }
        }
        if (FCode.equals("ComputeState"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ComputeState = FValue.trim();
            }
            else
            {
                ComputeState = null;
            }
        }
        if (FCode.equals("ActuRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                ActuRate = d;
            }
        }
        if (FCode.equals("EnsuRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                EnsuRate = d;
            }
        }
        if (FCode.equals("EnsuRateDefault"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                EnsuRateDefault = d;
            }
        }
        if (FCode.equals("AssignRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                AssignRate = d;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        if (FCode.equals("SGetDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SGetDate = fDate.getDate(FValue);
            }
            else
            {
                SGetDate = null;
            }
        }
        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("SumBonus"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                SumBonus = d;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LOBonusGrpPolParmSchema other = (LOBonusGrpPolParmSchema) otherObject;
        return
                FiscalYear == other.getFiscalYear()
                && GrpPolNo.equals(other.getGrpPolNo())
                && ComputeState.equals(other.getComputeState())
                && ActuRate == other.getActuRate()
                && EnsuRate == other.getEnsuRate()
                && EnsuRateDefault == other.getEnsuRateDefault()
                && AssignRate == other.getAssignRate()
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && fDate.getString(SGetDate).equals(other.getSGetDate())
                && RiskCode.equals(other.getRiskCode())
                && SumBonus == other.getSumBonus();
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("FiscalYear"))
        {
            return 0;
        }
        if (strFieldName.equals("GrpPolNo"))
        {
            return 1;
        }
        if (strFieldName.equals("ComputeState"))
        {
            return 2;
        }
        if (strFieldName.equals("ActuRate"))
        {
            return 3;
        }
        if (strFieldName.equals("EnsuRate"))
        {
            return 4;
        }
        if (strFieldName.equals("EnsuRateDefault"))
        {
            return 5;
        }
        if (strFieldName.equals("AssignRate"))
        {
            return 6;
        }
        if (strFieldName.equals("Operator"))
        {
            return 7;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 8;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 9;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 10;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 11;
        }
        if (strFieldName.equals("SGetDate"))
        {
            return 12;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return 13;
        }
        if (strFieldName.equals("SumBonus"))
        {
            return 14;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "FiscalYear";
                break;
            case 1:
                strFieldName = "GrpPolNo";
                break;
            case 2:
                strFieldName = "ComputeState";
                break;
            case 3:
                strFieldName = "ActuRate";
                break;
            case 4:
                strFieldName = "EnsuRate";
                break;
            case 5:
                strFieldName = "EnsuRateDefault";
                break;
            case 6:
                strFieldName = "AssignRate";
                break;
            case 7:
                strFieldName = "Operator";
                break;
            case 8:
                strFieldName = "MakeDate";
                break;
            case 9:
                strFieldName = "MakeTime";
                break;
            case 10:
                strFieldName = "ModifyDate";
                break;
            case 11:
                strFieldName = "ModifyTime";
                break;
            case 12:
                strFieldName = "SGetDate";
                break;
            case 13:
                strFieldName = "RiskCode";
                break;
            case 14:
                strFieldName = "SumBonus";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("FiscalYear"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("GrpPolNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ComputeState"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ActuRate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("EnsuRate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("EnsuRateDefault"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("AssignRate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SGetDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SumBonus"))
        {
            return Schema.TYPE_DOUBLE;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_INT;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 4:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 5:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 6:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
