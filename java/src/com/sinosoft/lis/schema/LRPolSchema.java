/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LRPolDB;

/*
 * <p>ClassName: LRPolSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 再保新系统模型
 * @CreateDate：2017-02-21
 */
public class LRPolSchema implements Schema, Cloneable
{
	// @Field
	/** 团体合同号 */
	private String GrpContNo;
	/** 集体保单号码 */
	private String GrpPolNo;
	/** 总单/合同号码 */
	private String ContNo;
	/** 保单号码 */
	private String PolNo;
	/** 投保单号码 */
	private String ProposalNo;
	/** 印刷号码 */
	private String PrtNo;
	/** 再保合同号 */
	private String ReContCode;
	/** 再保项目 */
	private String ReinsureItem;
	/** 临分标志 */
	private String TempCessFlag;
	/** 续保次数 */
	private int ReNewCount;
	/** 保单类型 */
	private String ContType;
	/** 管理机构 */
	private String ManageCom;
	/** 险种计算分类 */
	private String RiskCalSort;
	/** 主险保单号码 */
	private String MainPolNo;
	/** 主被保人保单号码 */
	private String MasterPolNo;
	/** 险种编码 */
	private String RiskCode;
	/** 险种版本 */
	private String RiskVersion;
	/** 被保人客户号码 */
	private String InsuredNo;
	/** 被保人名称 */
	private String InsuredName;
	/** 被保人性别 */
	private String InsuredSex;
	/** 被保人生日 */
	private Date InsuredBirthday;
	/** 被保人投保年龄 */
	private int InsuredAppAge;
	/** 保险年期 */
	private int Years;
	/** 保单生效日期 */
	private Date CValiDate;
	/** 保险责任终止日期 */
	private Date EndDate;
	/** 标准保费 */
	private double StandPrem;
	/** 保费 */
	private double Prem;
	/** 总累计保费 */
	private double SumPrem;
	/** 总基本保额 */
	private double Amnt;
	/** 保险期间 */
	private String InsurePeriod;
	/** 职业类别 */
	private String OccupationType;
	/** 总风险保额 */
	private double RiskAmnt;
	/** 交费间隔 */
	private int PayIntv;
	/** 交费方式 */
	private String PayMode;
	/** 交费年期 */
	private int PayYears;
	/** 终交年龄年期标志 */
	private String PayEndYearFlag;
	/** 终交年龄年期 */
	private int PayEndYear;
	/** 保险年龄年期标志 */
	private String InsuYearFlag;
	/** 保险年龄年期 */
	private int InsuYear;
	/** 协议类型 */
	private String ProtItem;
	/** 分保开始日期 */
	private Date CessStart;
	/** 分保结束日期 */
	private Date CessEnd;
	/** 保单状态 */
	private String PolStat;
	/** 签单日期 */
	private Date SignDate;
	/** 累计风险保额 */
	private double SumRiskAmount;
	/** 当前风险保额 */
	private double NowRiskAmount;
	/** 投保人客户号码 */
	private String AppntNo;
	/** 投保人名称 */
	private String AppntName;
	/** 投保人类型 */
	private String AppntType;
	/** 销售渠道 */
	private String SaleChnl;
	/** 死亡加费率 */
	private double DeadAddFeeRate;
	/** 重疾加费率 */
	private double DiseaseAddFeeRate;
	/** 数据提取日期 */
	private Date GetDataDate;
	/** 操作人 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 成本中心编码 */
	private String CostCenter;
	/** 分保规则 */
	private String ReType;
	/** 结算标记 */
	private String ActuGetState;
	/** 结算号码 */
	private String ActuGetNo;
	/** 本次分保保额 */
	private double CessAmnt;
	/** 交费号码 */
	private String PayNo;
	/** 保险计划编码 */
	private String ContPlanCode;
	/** 保单类型标记 */
	private String PolTypeFlag;

	public static final int FIELDNUM = 66;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LRPolSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[6];
		pk[0] = "PolNo";
		pk[1] = "ReContCode";
		pk[2] = "ReinsureItem";
		pk[3] = "ReNewCount";
		pk[4] = "GetDataDate";
		pk[5] = "PayNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LRPolSchema cloned = (LRPolSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getGrpContNo()
	{
		return GrpContNo;
	}
	public void setGrpContNo(String aGrpContNo)
	{
		GrpContNo = aGrpContNo;
	}
	public String getGrpPolNo()
	{
		return GrpPolNo;
	}
	public void setGrpPolNo(String aGrpPolNo)
	{
		GrpPolNo = aGrpPolNo;
	}
	public String getContNo()
	{
		return ContNo;
	}
	public void setContNo(String aContNo)
	{
		ContNo = aContNo;
	}
	public String getPolNo()
	{
		return PolNo;
	}
	public void setPolNo(String aPolNo)
	{
		PolNo = aPolNo;
	}
	public String getProposalNo()
	{
		return ProposalNo;
	}
	public void setProposalNo(String aProposalNo)
	{
		ProposalNo = aProposalNo;
	}
	public String getPrtNo()
	{
		return PrtNo;
	}
	public void setPrtNo(String aPrtNo)
	{
		PrtNo = aPrtNo;
	}
	public String getReContCode()
	{
		return ReContCode;
	}
	public void setReContCode(String aReContCode)
	{
		ReContCode = aReContCode;
	}
	public String getReinsureItem()
	{
		return ReinsureItem;
	}
	public void setReinsureItem(String aReinsureItem)
	{
		ReinsureItem = aReinsureItem;
	}
	public String getTempCessFlag()
	{
		return TempCessFlag;
	}
	public void setTempCessFlag(String aTempCessFlag)
	{
		TempCessFlag = aTempCessFlag;
	}
	public int getReNewCount()
	{
		return ReNewCount;
	}
	public void setReNewCount(int aReNewCount)
	{
		ReNewCount = aReNewCount;
	}
	public void setReNewCount(String aReNewCount)
	{
		if (aReNewCount != null && !aReNewCount.equals(""))
		{
			Integer tInteger = new Integer(aReNewCount);
			int i = tInteger.intValue();
			ReNewCount = i;
		}
	}

	public String getContType()
	{
		return ContType;
	}
	public void setContType(String aContType)
	{
		ContType = aContType;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getRiskCalSort()
	{
		return RiskCalSort;
	}
	public void setRiskCalSort(String aRiskCalSort)
	{
		RiskCalSort = aRiskCalSort;
	}
	public String getMainPolNo()
	{
		return MainPolNo;
	}
	public void setMainPolNo(String aMainPolNo)
	{
		MainPolNo = aMainPolNo;
	}
	public String getMasterPolNo()
	{
		return MasterPolNo;
	}
	public void setMasterPolNo(String aMasterPolNo)
	{
		MasterPolNo = aMasterPolNo;
	}
	public String getRiskCode()
	{
		return RiskCode;
	}
	public void setRiskCode(String aRiskCode)
	{
		RiskCode = aRiskCode;
	}
	public String getRiskVersion()
	{
		return RiskVersion;
	}
	public void setRiskVersion(String aRiskVersion)
	{
		RiskVersion = aRiskVersion;
	}
	public String getInsuredNo()
	{
		return InsuredNo;
	}
	public void setInsuredNo(String aInsuredNo)
	{
		InsuredNo = aInsuredNo;
	}
	public String getInsuredName()
	{
		return InsuredName;
	}
	public void setInsuredName(String aInsuredName)
	{
		InsuredName = aInsuredName;
	}
	public String getInsuredSex()
	{
		return InsuredSex;
	}
	public void setInsuredSex(String aInsuredSex)
	{
		InsuredSex = aInsuredSex;
	}
	public String getInsuredBirthday()
	{
		if( InsuredBirthday != null )
			return fDate.getString(InsuredBirthday);
		else
			return null;
	}
	public void setInsuredBirthday(Date aInsuredBirthday)
	{
		InsuredBirthday = aInsuredBirthday;
	}
	public void setInsuredBirthday(String aInsuredBirthday)
	{
		if (aInsuredBirthday != null && !aInsuredBirthday.equals("") )
		{
			InsuredBirthday = fDate.getDate( aInsuredBirthday );
		}
		else
			InsuredBirthday = null;
	}

	public int getInsuredAppAge()
	{
		return InsuredAppAge;
	}
	public void setInsuredAppAge(int aInsuredAppAge)
	{
		InsuredAppAge = aInsuredAppAge;
	}
	public void setInsuredAppAge(String aInsuredAppAge)
	{
		if (aInsuredAppAge != null && !aInsuredAppAge.equals(""))
		{
			Integer tInteger = new Integer(aInsuredAppAge);
			int i = tInteger.intValue();
			InsuredAppAge = i;
		}
	}

	public int getYears()
	{
		return Years;
	}
	public void setYears(int aYears)
	{
		Years = aYears;
	}
	public void setYears(String aYears)
	{
		if (aYears != null && !aYears.equals(""))
		{
			Integer tInteger = new Integer(aYears);
			int i = tInteger.intValue();
			Years = i;
		}
	}

	public String getCValiDate()
	{
		if( CValiDate != null )
			return fDate.getString(CValiDate);
		else
			return null;
	}
	public void setCValiDate(Date aCValiDate)
	{
		CValiDate = aCValiDate;
	}
	public void setCValiDate(String aCValiDate)
	{
		if (aCValiDate != null && !aCValiDate.equals("") )
		{
			CValiDate = fDate.getDate( aCValiDate );
		}
		else
			CValiDate = null;
	}

	public String getEndDate()
	{
		if( EndDate != null )
			return fDate.getString(EndDate);
		else
			return null;
	}
	public void setEndDate(Date aEndDate)
	{
		EndDate = aEndDate;
	}
	public void setEndDate(String aEndDate)
	{
		if (aEndDate != null && !aEndDate.equals("") )
		{
			EndDate = fDate.getDate( aEndDate );
		}
		else
			EndDate = null;
	}

	public double getStandPrem()
	{
		return StandPrem;
	}
	public void setStandPrem(double aStandPrem)
	{
		StandPrem = Arith.round(aStandPrem,2);
	}
	public void setStandPrem(String aStandPrem)
	{
		if (aStandPrem != null && !aStandPrem.equals(""))
		{
			Double tDouble = new Double(aStandPrem);
			double d = tDouble.doubleValue();
                StandPrem = Arith.round(d,2);
		}
	}

	public double getPrem()
	{
		return Prem;
	}
	public void setPrem(double aPrem)
	{
		Prem = Arith.round(aPrem,2);
	}
	public void setPrem(String aPrem)
	{
		if (aPrem != null && !aPrem.equals(""))
		{
			Double tDouble = new Double(aPrem);
			double d = tDouble.doubleValue();
                Prem = Arith.round(d,2);
		}
	}

	public double getSumPrem()
	{
		return SumPrem;
	}
	public void setSumPrem(double aSumPrem)
	{
		SumPrem = Arith.round(aSumPrem,2);
	}
	public void setSumPrem(String aSumPrem)
	{
		if (aSumPrem != null && !aSumPrem.equals(""))
		{
			Double tDouble = new Double(aSumPrem);
			double d = tDouble.doubleValue();
                SumPrem = Arith.round(d,2);
		}
	}

	public double getAmnt()
	{
		return Amnt;
	}
	public void setAmnt(double aAmnt)
	{
		Amnt = Arith.round(aAmnt,2);
	}
	public void setAmnt(String aAmnt)
	{
		if (aAmnt != null && !aAmnt.equals(""))
		{
			Double tDouble = new Double(aAmnt);
			double d = tDouble.doubleValue();
                Amnt = Arith.round(d,2);
		}
	}

	public String getInsurePeriod()
	{
		return InsurePeriod;
	}
	public void setInsurePeriod(String aInsurePeriod)
	{
		InsurePeriod = aInsurePeriod;
	}
	public String getOccupationType()
	{
		return OccupationType;
	}
	public void setOccupationType(String aOccupationType)
	{
		OccupationType = aOccupationType;
	}
	public double getRiskAmnt()
	{
		return RiskAmnt;
	}
	public void setRiskAmnt(double aRiskAmnt)
	{
		RiskAmnt = Arith.round(aRiskAmnt,2);
	}
	public void setRiskAmnt(String aRiskAmnt)
	{
		if (aRiskAmnt != null && !aRiskAmnt.equals(""))
		{
			Double tDouble = new Double(aRiskAmnt);
			double d = tDouble.doubleValue();
                RiskAmnt = Arith.round(d,2);
		}
	}

	public int getPayIntv()
	{
		return PayIntv;
	}
	public void setPayIntv(int aPayIntv)
	{
		PayIntv = aPayIntv;
	}
	public void setPayIntv(String aPayIntv)
	{
		if (aPayIntv != null && !aPayIntv.equals(""))
		{
			Integer tInteger = new Integer(aPayIntv);
			int i = tInteger.intValue();
			PayIntv = i;
		}
	}

	public String getPayMode()
	{
		return PayMode;
	}
	public void setPayMode(String aPayMode)
	{
		PayMode = aPayMode;
	}
	public int getPayYears()
	{
		return PayYears;
	}
	public void setPayYears(int aPayYears)
	{
		PayYears = aPayYears;
	}
	public void setPayYears(String aPayYears)
	{
		if (aPayYears != null && !aPayYears.equals(""))
		{
			Integer tInteger = new Integer(aPayYears);
			int i = tInteger.intValue();
			PayYears = i;
		}
	}

	public String getPayEndYearFlag()
	{
		return PayEndYearFlag;
	}
	public void setPayEndYearFlag(String aPayEndYearFlag)
	{
		PayEndYearFlag = aPayEndYearFlag;
	}
	public int getPayEndYear()
	{
		return PayEndYear;
	}
	public void setPayEndYear(int aPayEndYear)
	{
		PayEndYear = aPayEndYear;
	}
	public void setPayEndYear(String aPayEndYear)
	{
		if (aPayEndYear != null && !aPayEndYear.equals(""))
		{
			Integer tInteger = new Integer(aPayEndYear);
			int i = tInteger.intValue();
			PayEndYear = i;
		}
	}

	public String getInsuYearFlag()
	{
		return InsuYearFlag;
	}
	public void setInsuYearFlag(String aInsuYearFlag)
	{
		InsuYearFlag = aInsuYearFlag;
	}
	public int getInsuYear()
	{
		return InsuYear;
	}
	public void setInsuYear(int aInsuYear)
	{
		InsuYear = aInsuYear;
	}
	public void setInsuYear(String aInsuYear)
	{
		if (aInsuYear != null && !aInsuYear.equals(""))
		{
			Integer tInteger = new Integer(aInsuYear);
			int i = tInteger.intValue();
			InsuYear = i;
		}
	}

	public String getProtItem()
	{
		return ProtItem;
	}
	public void setProtItem(String aProtItem)
	{
		ProtItem = aProtItem;
	}
	public String getCessStart()
	{
		if( CessStart != null )
			return fDate.getString(CessStart);
		else
			return null;
	}
	public void setCessStart(Date aCessStart)
	{
		CessStart = aCessStart;
	}
	public void setCessStart(String aCessStart)
	{
		if (aCessStart != null && !aCessStart.equals("") )
		{
			CessStart = fDate.getDate( aCessStart );
		}
		else
			CessStart = null;
	}

	public String getCessEnd()
	{
		if( CessEnd != null )
			return fDate.getString(CessEnd);
		else
			return null;
	}
	public void setCessEnd(Date aCessEnd)
	{
		CessEnd = aCessEnd;
	}
	public void setCessEnd(String aCessEnd)
	{
		if (aCessEnd != null && !aCessEnd.equals("") )
		{
			CessEnd = fDate.getDate( aCessEnd );
		}
		else
			CessEnd = null;
	}

	public String getPolStat()
	{
		return PolStat;
	}
	public void setPolStat(String aPolStat)
	{
		PolStat = aPolStat;
	}
	public String getSignDate()
	{
		if( SignDate != null )
			return fDate.getString(SignDate);
		else
			return null;
	}
	public void setSignDate(Date aSignDate)
	{
		SignDate = aSignDate;
	}
	public void setSignDate(String aSignDate)
	{
		if (aSignDate != null && !aSignDate.equals("") )
		{
			SignDate = fDate.getDate( aSignDate );
		}
		else
			SignDate = null;
	}

	public double getSumRiskAmount()
	{
		return SumRiskAmount;
	}
	public void setSumRiskAmount(double aSumRiskAmount)
	{
		SumRiskAmount = Arith.round(aSumRiskAmount,2);
	}
	public void setSumRiskAmount(String aSumRiskAmount)
	{
		if (aSumRiskAmount != null && !aSumRiskAmount.equals(""))
		{
			Double tDouble = new Double(aSumRiskAmount);
			double d = tDouble.doubleValue();
                SumRiskAmount = Arith.round(d,2);
		}
	}

	public double getNowRiskAmount()
	{
		return NowRiskAmount;
	}
	public void setNowRiskAmount(double aNowRiskAmount)
	{
		NowRiskAmount = Arith.round(aNowRiskAmount,2);
	}
	public void setNowRiskAmount(String aNowRiskAmount)
	{
		if (aNowRiskAmount != null && !aNowRiskAmount.equals(""))
		{
			Double tDouble = new Double(aNowRiskAmount);
			double d = tDouble.doubleValue();
                NowRiskAmount = Arith.round(d,2);
		}
	}

	public String getAppntNo()
	{
		return AppntNo;
	}
	public void setAppntNo(String aAppntNo)
	{
		AppntNo = aAppntNo;
	}
	public String getAppntName()
	{
		return AppntName;
	}
	public void setAppntName(String aAppntName)
	{
		AppntName = aAppntName;
	}
	public String getAppntType()
	{
		return AppntType;
	}
	public void setAppntType(String aAppntType)
	{
		AppntType = aAppntType;
	}
	public String getSaleChnl()
	{
		return SaleChnl;
	}
	public void setSaleChnl(String aSaleChnl)
	{
		SaleChnl = aSaleChnl;
	}
	public double getDeadAddFeeRate()
	{
		return DeadAddFeeRate;
	}
	public void setDeadAddFeeRate(double aDeadAddFeeRate)
	{
		DeadAddFeeRate = Arith.round(aDeadAddFeeRate,6);
	}
	public void setDeadAddFeeRate(String aDeadAddFeeRate)
	{
		if (aDeadAddFeeRate != null && !aDeadAddFeeRate.equals(""))
		{
			Double tDouble = new Double(aDeadAddFeeRate);
			double d = tDouble.doubleValue();
                DeadAddFeeRate = Arith.round(d,6);
		}
	}

	public double getDiseaseAddFeeRate()
	{
		return DiseaseAddFeeRate;
	}
	public void setDiseaseAddFeeRate(double aDiseaseAddFeeRate)
	{
		DiseaseAddFeeRate = Arith.round(aDiseaseAddFeeRate,6);
	}
	public void setDiseaseAddFeeRate(String aDiseaseAddFeeRate)
	{
		if (aDiseaseAddFeeRate != null && !aDiseaseAddFeeRate.equals(""))
		{
			Double tDouble = new Double(aDiseaseAddFeeRate);
			double d = tDouble.doubleValue();
                DiseaseAddFeeRate = Arith.round(d,6);
		}
	}

	public String getGetDataDate()
	{
		if( GetDataDate != null )
			return fDate.getString(GetDataDate);
		else
			return null;
	}
	public void setGetDataDate(Date aGetDataDate)
	{
		GetDataDate = aGetDataDate;
	}
	public void setGetDataDate(String aGetDataDate)
	{
		if (aGetDataDate != null && !aGetDataDate.equals("") )
		{
			GetDataDate = fDate.getDate( aGetDataDate );
		}
		else
			GetDataDate = null;
	}

	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getCostCenter()
	{
		return CostCenter;
	}
	public void setCostCenter(String aCostCenter)
	{
		CostCenter = aCostCenter;
	}
	public String getReType()
	{
		return ReType;
	}
	public void setReType(String aReType)
	{
		ReType = aReType;
	}
	public String getActuGetState()
	{
		return ActuGetState;
	}
	public void setActuGetState(String aActuGetState)
	{
		ActuGetState = aActuGetState;
	}
	public String getActuGetNo()
	{
		return ActuGetNo;
	}
	public void setActuGetNo(String aActuGetNo)
	{
		ActuGetNo = aActuGetNo;
	}
	public double getCessAmnt()
	{
		return CessAmnt;
	}
	public void setCessAmnt(double aCessAmnt)
	{
		CessAmnt = Arith.round(aCessAmnt,2);
	}
	public void setCessAmnt(String aCessAmnt)
	{
		if (aCessAmnt != null && !aCessAmnt.equals(""))
		{
			Double tDouble = new Double(aCessAmnt);
			double d = tDouble.doubleValue();
                CessAmnt = Arith.round(d,2);
		}
	}

	public String getPayNo()
	{
		return PayNo;
	}
	public void setPayNo(String aPayNo)
	{
		PayNo = aPayNo;
	}
	public String getContPlanCode()
	{
		return ContPlanCode;
	}
	public void setContPlanCode(String aContPlanCode)
	{
		ContPlanCode = aContPlanCode;
	}
	public String getPolTypeFlag()
	{
		return PolTypeFlag;
	}
	public void setPolTypeFlag(String aPolTypeFlag)
	{
		PolTypeFlag = aPolTypeFlag;
	}

	/**
	* 使用另外一个 LRPolSchema 对象给 Schema 赋值
	* @param: aLRPolSchema LRPolSchema
	**/
	public void setSchema(LRPolSchema aLRPolSchema)
	{
		this.GrpContNo = aLRPolSchema.getGrpContNo();
		this.GrpPolNo = aLRPolSchema.getGrpPolNo();
		this.ContNo = aLRPolSchema.getContNo();
		this.PolNo = aLRPolSchema.getPolNo();
		this.ProposalNo = aLRPolSchema.getProposalNo();
		this.PrtNo = aLRPolSchema.getPrtNo();
		this.ReContCode = aLRPolSchema.getReContCode();
		this.ReinsureItem = aLRPolSchema.getReinsureItem();
		this.TempCessFlag = aLRPolSchema.getTempCessFlag();
		this.ReNewCount = aLRPolSchema.getReNewCount();
		this.ContType = aLRPolSchema.getContType();
		this.ManageCom = aLRPolSchema.getManageCom();
		this.RiskCalSort = aLRPolSchema.getRiskCalSort();
		this.MainPolNo = aLRPolSchema.getMainPolNo();
		this.MasterPolNo = aLRPolSchema.getMasterPolNo();
		this.RiskCode = aLRPolSchema.getRiskCode();
		this.RiskVersion = aLRPolSchema.getRiskVersion();
		this.InsuredNo = aLRPolSchema.getInsuredNo();
		this.InsuredName = aLRPolSchema.getInsuredName();
		this.InsuredSex = aLRPolSchema.getInsuredSex();
		this.InsuredBirthday = fDate.getDate( aLRPolSchema.getInsuredBirthday());
		this.InsuredAppAge = aLRPolSchema.getInsuredAppAge();
		this.Years = aLRPolSchema.getYears();
		this.CValiDate = fDate.getDate( aLRPolSchema.getCValiDate());
		this.EndDate = fDate.getDate( aLRPolSchema.getEndDate());
		this.StandPrem = aLRPolSchema.getStandPrem();
		this.Prem = aLRPolSchema.getPrem();
		this.SumPrem = aLRPolSchema.getSumPrem();
		this.Amnt = aLRPolSchema.getAmnt();
		this.InsurePeriod = aLRPolSchema.getInsurePeriod();
		this.OccupationType = aLRPolSchema.getOccupationType();
		this.RiskAmnt = aLRPolSchema.getRiskAmnt();
		this.PayIntv = aLRPolSchema.getPayIntv();
		this.PayMode = aLRPolSchema.getPayMode();
		this.PayYears = aLRPolSchema.getPayYears();
		this.PayEndYearFlag = aLRPolSchema.getPayEndYearFlag();
		this.PayEndYear = aLRPolSchema.getPayEndYear();
		this.InsuYearFlag = aLRPolSchema.getInsuYearFlag();
		this.InsuYear = aLRPolSchema.getInsuYear();
		this.ProtItem = aLRPolSchema.getProtItem();
		this.CessStart = fDate.getDate( aLRPolSchema.getCessStart());
		this.CessEnd = fDate.getDate( aLRPolSchema.getCessEnd());
		this.PolStat = aLRPolSchema.getPolStat();
		this.SignDate = fDate.getDate( aLRPolSchema.getSignDate());
		this.SumRiskAmount = aLRPolSchema.getSumRiskAmount();
		this.NowRiskAmount = aLRPolSchema.getNowRiskAmount();
		this.AppntNo = aLRPolSchema.getAppntNo();
		this.AppntName = aLRPolSchema.getAppntName();
		this.AppntType = aLRPolSchema.getAppntType();
		this.SaleChnl = aLRPolSchema.getSaleChnl();
		this.DeadAddFeeRate = aLRPolSchema.getDeadAddFeeRate();
		this.DiseaseAddFeeRate = aLRPolSchema.getDiseaseAddFeeRate();
		this.GetDataDate = fDate.getDate( aLRPolSchema.getGetDataDate());
		this.Operator = aLRPolSchema.getOperator();
		this.MakeDate = fDate.getDate( aLRPolSchema.getMakeDate());
		this.MakeTime = aLRPolSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLRPolSchema.getModifyDate());
		this.ModifyTime = aLRPolSchema.getModifyTime();
		this.CostCenter = aLRPolSchema.getCostCenter();
		this.ReType = aLRPolSchema.getReType();
		this.ActuGetState = aLRPolSchema.getActuGetState();
		this.ActuGetNo = aLRPolSchema.getActuGetNo();
		this.CessAmnt = aLRPolSchema.getCessAmnt();
		this.PayNo = aLRPolSchema.getPayNo();
		this.ContPlanCode = aLRPolSchema.getContPlanCode();
		this.PolTypeFlag = aLRPolSchema.getPolTypeFlag();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("GrpContNo") == null )
				this.GrpContNo = null;
			else
				this.GrpContNo = rs.getString("GrpContNo").trim();

			if( rs.getString("GrpPolNo") == null )
				this.GrpPolNo = null;
			else
				this.GrpPolNo = rs.getString("GrpPolNo").trim();

			if( rs.getString("ContNo") == null )
				this.ContNo = null;
			else
				this.ContNo = rs.getString("ContNo").trim();

			if( rs.getString("PolNo") == null )
				this.PolNo = null;
			else
				this.PolNo = rs.getString("PolNo").trim();

			if( rs.getString("ProposalNo") == null )
				this.ProposalNo = null;
			else
				this.ProposalNo = rs.getString("ProposalNo").trim();

			if( rs.getString("PrtNo") == null )
				this.PrtNo = null;
			else
				this.PrtNo = rs.getString("PrtNo").trim();

			if( rs.getString("ReContCode") == null )
				this.ReContCode = null;
			else
				this.ReContCode = rs.getString("ReContCode").trim();

			if( rs.getString("ReinsureItem") == null )
				this.ReinsureItem = null;
			else
				this.ReinsureItem = rs.getString("ReinsureItem").trim();

			if( rs.getString("TempCessFlag") == null )
				this.TempCessFlag = null;
			else
				this.TempCessFlag = rs.getString("TempCessFlag").trim();

			this.ReNewCount = rs.getInt("ReNewCount");
			if( rs.getString("ContType") == null )
				this.ContType = null;
			else
				this.ContType = rs.getString("ContType").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("RiskCalSort") == null )
				this.RiskCalSort = null;
			else
				this.RiskCalSort = rs.getString("RiskCalSort").trim();

			if( rs.getString("MainPolNo") == null )
				this.MainPolNo = null;
			else
				this.MainPolNo = rs.getString("MainPolNo").trim();

			if( rs.getString("MasterPolNo") == null )
				this.MasterPolNo = null;
			else
				this.MasterPolNo = rs.getString("MasterPolNo").trim();

			if( rs.getString("RiskCode") == null )
				this.RiskCode = null;
			else
				this.RiskCode = rs.getString("RiskCode").trim();

			if( rs.getString("RiskVersion") == null )
				this.RiskVersion = null;
			else
				this.RiskVersion = rs.getString("RiskVersion").trim();

			if( rs.getString("InsuredNo") == null )
				this.InsuredNo = null;
			else
				this.InsuredNo = rs.getString("InsuredNo").trim();

			if( rs.getString("InsuredName") == null )
				this.InsuredName = null;
			else
				this.InsuredName = rs.getString("InsuredName").trim();

			if( rs.getString("InsuredSex") == null )
				this.InsuredSex = null;
			else
				this.InsuredSex = rs.getString("InsuredSex").trim();

			this.InsuredBirthday = rs.getDate("InsuredBirthday");
			this.InsuredAppAge = rs.getInt("InsuredAppAge");
			this.Years = rs.getInt("Years");
			this.CValiDate = rs.getDate("CValiDate");
			this.EndDate = rs.getDate("EndDate");
			this.StandPrem = rs.getDouble("StandPrem");
			this.Prem = rs.getDouble("Prem");
			this.SumPrem = rs.getDouble("SumPrem");
			this.Amnt = rs.getDouble("Amnt");
			if( rs.getString("InsurePeriod") == null )
				this.InsurePeriod = null;
			else
				this.InsurePeriod = rs.getString("InsurePeriod").trim();

			if( rs.getString("OccupationType") == null )
				this.OccupationType = null;
			else
				this.OccupationType = rs.getString("OccupationType").trim();

			this.RiskAmnt = rs.getDouble("RiskAmnt");
			this.PayIntv = rs.getInt("PayIntv");
			if( rs.getString("PayMode") == null )
				this.PayMode = null;
			else
				this.PayMode = rs.getString("PayMode").trim();

			this.PayYears = rs.getInt("PayYears");
			if( rs.getString("PayEndYearFlag") == null )
				this.PayEndYearFlag = null;
			else
				this.PayEndYearFlag = rs.getString("PayEndYearFlag").trim();

			this.PayEndYear = rs.getInt("PayEndYear");
			if( rs.getString("InsuYearFlag") == null )
				this.InsuYearFlag = null;
			else
				this.InsuYearFlag = rs.getString("InsuYearFlag").trim();

			this.InsuYear = rs.getInt("InsuYear");
			if( rs.getString("ProtItem") == null )
				this.ProtItem = null;
			else
				this.ProtItem = rs.getString("ProtItem").trim();

			this.CessStart = rs.getDate("CessStart");
			this.CessEnd = rs.getDate("CessEnd");
			if( rs.getString("PolStat") == null )
				this.PolStat = null;
			else
				this.PolStat = rs.getString("PolStat").trim();

			this.SignDate = rs.getDate("SignDate");
			this.SumRiskAmount = rs.getDouble("SumRiskAmount");
			this.NowRiskAmount = rs.getDouble("NowRiskAmount");
			if( rs.getString("AppntNo") == null )
				this.AppntNo = null;
			else
				this.AppntNo = rs.getString("AppntNo").trim();

			if( rs.getString("AppntName") == null )
				this.AppntName = null;
			else
				this.AppntName = rs.getString("AppntName").trim();

			if( rs.getString("AppntType") == null )
				this.AppntType = null;
			else
				this.AppntType = rs.getString("AppntType").trim();

			if( rs.getString("SaleChnl") == null )
				this.SaleChnl = null;
			else
				this.SaleChnl = rs.getString("SaleChnl").trim();

			this.DeadAddFeeRate = rs.getDouble("DeadAddFeeRate");
			this.DiseaseAddFeeRate = rs.getDouble("DiseaseAddFeeRate");
			this.GetDataDate = rs.getDate("GetDataDate");
			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("CostCenter") == null )
				this.CostCenter = null;
			else
				this.CostCenter = rs.getString("CostCenter").trim();

			if( rs.getString("ReType") == null )
				this.ReType = null;
			else
				this.ReType = rs.getString("ReType").trim();

			if( rs.getString("ActuGetState") == null )
				this.ActuGetState = null;
			else
				this.ActuGetState = rs.getString("ActuGetState").trim();

			if( rs.getString("ActuGetNo") == null )
				this.ActuGetNo = null;
			else
				this.ActuGetNo = rs.getString("ActuGetNo").trim();

			this.CessAmnt = rs.getDouble("CessAmnt");
			if( rs.getString("PayNo") == null )
				this.PayNo = null;
			else
				this.PayNo = rs.getString("PayNo").trim();

			if( rs.getString("ContPlanCode") == null )
				this.ContPlanCode = null;
			else
				this.ContPlanCode = rs.getString("ContPlanCode").trim();

			if( rs.getString("PolTypeFlag") == null )
				this.PolTypeFlag = null;
			else
				this.PolTypeFlag = rs.getString("PolTypeFlag").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LRPol表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LRPolSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LRPolSchema getSchema()
	{
		LRPolSchema aLRPolSchema = new LRPolSchema();
		aLRPolSchema.setSchema(this);
		return aLRPolSchema;
	}

	public LRPolDB getDB()
	{
		LRPolDB aDBOper = new LRPolDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLRPol描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpPolNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PolNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProposalNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PrtNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReContCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReinsureItem)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TempCessFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ReNewCount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskCalSort)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MainPolNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MasterPolNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskVersion)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuredNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuredName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuredSex)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( InsuredBirthday ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(InsuredAppAge));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Years));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CValiDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( EndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(StandPrem));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Prem));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SumPrem));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Amnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsurePeriod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OccupationType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(RiskAmnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PayIntv));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayMode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PayYears));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayEndYearFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PayEndYear));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuYearFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(InsuYear));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProtItem)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CessStart ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CessEnd ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PolStat)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( SignDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SumRiskAmount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(NowRiskAmount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppntNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppntName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppntType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SaleChnl)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(DeadAddFeeRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(DiseaseAddFeeRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( GetDataDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CostCenter)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ActuGetState)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ActuGetNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(CessAmnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContPlanCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PolTypeFlag));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLRPol>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			GrpPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			ProposalNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			ReContCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			ReinsureItem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			TempCessFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			ReNewCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,10,SysConst.PACKAGESPILTER))).intValue();
			ContType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			RiskCalSort = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			MainPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			MasterPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			RiskVersion = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			InsuredNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			InsuredName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			InsuredSex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			InsuredBirthday = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,SysConst.PACKAGESPILTER));
			InsuredAppAge= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,22,SysConst.PACKAGESPILTER))).intValue();
			Years= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,23,SysConst.PACKAGESPILTER))).intValue();
			CValiDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,SysConst.PACKAGESPILTER));
			EndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,SysConst.PACKAGESPILTER));
			StandPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,26,SysConst.PACKAGESPILTER))).doubleValue();
			Prem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,27,SysConst.PACKAGESPILTER))).doubleValue();
			SumPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,28,SysConst.PACKAGESPILTER))).doubleValue();
			Amnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,29,SysConst.PACKAGESPILTER))).doubleValue();
			InsurePeriod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			OccupationType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			RiskAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,32,SysConst.PACKAGESPILTER))).doubleValue();
			PayIntv= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,33,SysConst.PACKAGESPILTER))).intValue();
			PayMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			PayYears= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,35,SysConst.PACKAGESPILTER))).intValue();
			PayEndYearFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
			PayEndYear= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,37,SysConst.PACKAGESPILTER))).intValue();
			InsuYearFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
			InsuYear= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,39,SysConst.PACKAGESPILTER))).intValue();
			ProtItem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
			CessStart = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41,SysConst.PACKAGESPILTER));
			CessEnd = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42,SysConst.PACKAGESPILTER));
			PolStat = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43, SysConst.PACKAGESPILTER );
			SignDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44,SysConst.PACKAGESPILTER));
			SumRiskAmount = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,45,SysConst.PACKAGESPILTER))).doubleValue();
			NowRiskAmount = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,46,SysConst.PACKAGESPILTER))).doubleValue();
			AppntNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 47, SysConst.PACKAGESPILTER );
			AppntName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 48, SysConst.PACKAGESPILTER );
			AppntType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 49, SysConst.PACKAGESPILTER );
			SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 50, SysConst.PACKAGESPILTER );
			DeadAddFeeRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,51,SysConst.PACKAGESPILTER))).doubleValue();
			DiseaseAddFeeRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,52,SysConst.PACKAGESPILTER))).doubleValue();
			GetDataDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 53,SysConst.PACKAGESPILTER));
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 54, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 55,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 56, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 57,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 58, SysConst.PACKAGESPILTER );
			CostCenter = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 59, SysConst.PACKAGESPILTER );
			ReType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 60, SysConst.PACKAGESPILTER );
			ActuGetState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 61, SysConst.PACKAGESPILTER );
			ActuGetNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 62, SysConst.PACKAGESPILTER );
			CessAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,63,SysConst.PACKAGESPILTER))).doubleValue();
			PayNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 64, SysConst.PACKAGESPILTER );
			ContPlanCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 65, SysConst.PACKAGESPILTER );
			PolTypeFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 66, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LRPolSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("GrpContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
		}
		if (FCode.equals("GrpPolNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpPolNo));
		}
		if (FCode.equals("ContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
		}
		if (FCode.equals("PolNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
		}
		if (FCode.equals("ProposalNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProposalNo));
		}
		if (FCode.equals("PrtNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
		}
		if (FCode.equals("ReContCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReContCode));
		}
		if (FCode.equals("ReinsureItem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReinsureItem));
		}
		if (FCode.equals("TempCessFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TempCessFlag));
		}
		if (FCode.equals("ReNewCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReNewCount));
		}
		if (FCode.equals("ContType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContType));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("RiskCalSort"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCalSort));
		}
		if (FCode.equals("MainPolNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MainPolNo));
		}
		if (FCode.equals("MasterPolNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MasterPolNo));
		}
		if (FCode.equals("RiskCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
		}
		if (FCode.equals("RiskVersion"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskVersion));
		}
		if (FCode.equals("InsuredNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredNo));
		}
		if (FCode.equals("InsuredName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredName));
		}
		if (FCode.equals("InsuredSex"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredSex));
		}
		if (FCode.equals("InsuredBirthday"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getInsuredBirthday()));
		}
		if (FCode.equals("InsuredAppAge"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredAppAge));
		}
		if (FCode.equals("Years"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Years));
		}
		if (FCode.equals("CValiDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCValiDate()));
		}
		if (FCode.equals("EndDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEndDate()));
		}
		if (FCode.equals("StandPrem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandPrem));
		}
		if (FCode.equals("Prem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Prem));
		}
		if (FCode.equals("SumPrem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SumPrem));
		}
		if (FCode.equals("Amnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Amnt));
		}
		if (FCode.equals("InsurePeriod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsurePeriod));
		}
		if (FCode.equals("OccupationType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationType));
		}
		if (FCode.equals("RiskAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskAmnt));
		}
		if (FCode.equals("PayIntv"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayIntv));
		}
		if (FCode.equals("PayMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayMode));
		}
		if (FCode.equals("PayYears"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayYears));
		}
		if (FCode.equals("PayEndYearFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayEndYearFlag));
		}
		if (FCode.equals("PayEndYear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayEndYear));
		}
		if (FCode.equals("InsuYearFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuYearFlag));
		}
		if (FCode.equals("InsuYear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuYear));
		}
		if (FCode.equals("ProtItem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProtItem));
		}
		if (FCode.equals("CessStart"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCessStart()));
		}
		if (FCode.equals("CessEnd"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCessEnd()));
		}
		if (FCode.equals("PolStat"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolStat));
		}
		if (FCode.equals("SignDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getSignDate()));
		}
		if (FCode.equals("SumRiskAmount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SumRiskAmount));
		}
		if (FCode.equals("NowRiskAmount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(NowRiskAmount));
		}
		if (FCode.equals("AppntNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
		}
		if (FCode.equals("AppntName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppntName));
		}
		if (FCode.equals("AppntType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppntType));
		}
		if (FCode.equals("SaleChnl"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
		}
		if (FCode.equals("DeadAddFeeRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DeadAddFeeRate));
		}
		if (FCode.equals("DiseaseAddFeeRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DiseaseAddFeeRate));
		}
		if (FCode.equals("GetDataDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getGetDataDate()));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("CostCenter"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CostCenter));
		}
		if (FCode.equals("ReType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReType));
		}
		if (FCode.equals("ActuGetState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ActuGetState));
		}
		if (FCode.equals("ActuGetNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ActuGetNo));
		}
		if (FCode.equals("CessAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CessAmnt));
		}
		if (FCode.equals("PayNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayNo));
		}
		if (FCode.equals("ContPlanCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContPlanCode));
		}
		if (FCode.equals("PolTypeFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolTypeFlag));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(GrpContNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(GrpPolNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(ContNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(PolNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(ProposalNo);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(PrtNo);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(ReContCode);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(ReinsureItem);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(TempCessFlag);
				break;
			case 9:
				strFieldValue = String.valueOf(ReNewCount);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(ContType);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(RiskCalSort);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(MainPolNo);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(MasterPolNo);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(RiskCode);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(RiskVersion);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(InsuredNo);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(InsuredName);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(InsuredSex);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getInsuredBirthday()));
				break;
			case 21:
				strFieldValue = String.valueOf(InsuredAppAge);
				break;
			case 22:
				strFieldValue = String.valueOf(Years);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCValiDate()));
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEndDate()));
				break;
			case 25:
				strFieldValue = String.valueOf(StandPrem);
				break;
			case 26:
				strFieldValue = String.valueOf(Prem);
				break;
			case 27:
				strFieldValue = String.valueOf(SumPrem);
				break;
			case 28:
				strFieldValue = String.valueOf(Amnt);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(InsurePeriod);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(OccupationType);
				break;
			case 31:
				strFieldValue = String.valueOf(RiskAmnt);
				break;
			case 32:
				strFieldValue = String.valueOf(PayIntv);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(PayMode);
				break;
			case 34:
				strFieldValue = String.valueOf(PayYears);
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(PayEndYearFlag);
				break;
			case 36:
				strFieldValue = String.valueOf(PayEndYear);
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(InsuYearFlag);
				break;
			case 38:
				strFieldValue = String.valueOf(InsuYear);
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(ProtItem);
				break;
			case 40:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCessStart()));
				break;
			case 41:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCessEnd()));
				break;
			case 42:
				strFieldValue = StrTool.GBKToUnicode(PolStat);
				break;
			case 43:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getSignDate()));
				break;
			case 44:
				strFieldValue = String.valueOf(SumRiskAmount);
				break;
			case 45:
				strFieldValue = String.valueOf(NowRiskAmount);
				break;
			case 46:
				strFieldValue = StrTool.GBKToUnicode(AppntNo);
				break;
			case 47:
				strFieldValue = StrTool.GBKToUnicode(AppntName);
				break;
			case 48:
				strFieldValue = StrTool.GBKToUnicode(AppntType);
				break;
			case 49:
				strFieldValue = StrTool.GBKToUnicode(SaleChnl);
				break;
			case 50:
				strFieldValue = String.valueOf(DeadAddFeeRate);
				break;
			case 51:
				strFieldValue = String.valueOf(DiseaseAddFeeRate);
				break;
			case 52:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getGetDataDate()));
				break;
			case 53:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 54:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 55:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 56:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 57:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 58:
				strFieldValue = StrTool.GBKToUnicode(CostCenter);
				break;
			case 59:
				strFieldValue = StrTool.GBKToUnicode(ReType);
				break;
			case 60:
				strFieldValue = StrTool.GBKToUnicode(ActuGetState);
				break;
			case 61:
				strFieldValue = StrTool.GBKToUnicode(ActuGetNo);
				break;
			case 62:
				strFieldValue = String.valueOf(CessAmnt);
				break;
			case 63:
				strFieldValue = StrTool.GBKToUnicode(PayNo);
				break;
			case 64:
				strFieldValue = StrTool.GBKToUnicode(ContPlanCode);
				break;
			case 65:
				strFieldValue = StrTool.GBKToUnicode(PolTypeFlag);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("GrpContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpContNo = FValue.trim();
			}
			else
				GrpContNo = null;
		}
		if (FCode.equalsIgnoreCase("GrpPolNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpPolNo = FValue.trim();
			}
			else
				GrpPolNo = null;
		}
		if (FCode.equalsIgnoreCase("ContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContNo = FValue.trim();
			}
			else
				ContNo = null;
		}
		if (FCode.equalsIgnoreCase("PolNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PolNo = FValue.trim();
			}
			else
				PolNo = null;
		}
		if (FCode.equalsIgnoreCase("ProposalNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProposalNo = FValue.trim();
			}
			else
				ProposalNo = null;
		}
		if (FCode.equalsIgnoreCase("PrtNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PrtNo = FValue.trim();
			}
			else
				PrtNo = null;
		}
		if (FCode.equalsIgnoreCase("ReContCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReContCode = FValue.trim();
			}
			else
				ReContCode = null;
		}
		if (FCode.equalsIgnoreCase("ReinsureItem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReinsureItem = FValue.trim();
			}
			else
				ReinsureItem = null;
		}
		if (FCode.equalsIgnoreCase("TempCessFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TempCessFlag = FValue.trim();
			}
			else
				TempCessFlag = null;
		}
		if (FCode.equalsIgnoreCase("ReNewCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				ReNewCount = i;
			}
		}
		if (FCode.equalsIgnoreCase("ContType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContType = FValue.trim();
			}
			else
				ContType = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("RiskCalSort"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskCalSort = FValue.trim();
			}
			else
				RiskCalSort = null;
		}
		if (FCode.equalsIgnoreCase("MainPolNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MainPolNo = FValue.trim();
			}
			else
				MainPolNo = null;
		}
		if (FCode.equalsIgnoreCase("MasterPolNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MasterPolNo = FValue.trim();
			}
			else
				MasterPolNo = null;
		}
		if (FCode.equalsIgnoreCase("RiskCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskCode = FValue.trim();
			}
			else
				RiskCode = null;
		}
		if (FCode.equalsIgnoreCase("RiskVersion"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskVersion = FValue.trim();
			}
			else
				RiskVersion = null;
		}
		if (FCode.equalsIgnoreCase("InsuredNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuredNo = FValue.trim();
			}
			else
				InsuredNo = null;
		}
		if (FCode.equalsIgnoreCase("InsuredName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuredName = FValue.trim();
			}
			else
				InsuredName = null;
		}
		if (FCode.equalsIgnoreCase("InsuredSex"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuredSex = FValue.trim();
			}
			else
				InsuredSex = null;
		}
		if (FCode.equalsIgnoreCase("InsuredBirthday"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				InsuredBirthday = fDate.getDate( FValue );
			}
			else
				InsuredBirthday = null;
		}
		if (FCode.equalsIgnoreCase("InsuredAppAge"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				InsuredAppAge = i;
			}
		}
		if (FCode.equalsIgnoreCase("Years"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				Years = i;
			}
		}
		if (FCode.equalsIgnoreCase("CValiDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CValiDate = fDate.getDate( FValue );
			}
			else
				CValiDate = null;
		}
		if (FCode.equalsIgnoreCase("EndDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				EndDate = fDate.getDate( FValue );
			}
			else
				EndDate = null;
		}
		if (FCode.equalsIgnoreCase("StandPrem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				StandPrem = d;
			}
		}
		if (FCode.equalsIgnoreCase("Prem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Prem = d;
			}
		}
		if (FCode.equalsIgnoreCase("SumPrem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SumPrem = d;
			}
		}
		if (FCode.equalsIgnoreCase("Amnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Amnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("InsurePeriod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsurePeriod = FValue.trim();
			}
			else
				InsurePeriod = null;
		}
		if (FCode.equalsIgnoreCase("OccupationType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OccupationType = FValue.trim();
			}
			else
				OccupationType = null;
		}
		if (FCode.equalsIgnoreCase("RiskAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				RiskAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("PayIntv"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PayIntv = i;
			}
		}
		if (FCode.equalsIgnoreCase("PayMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayMode = FValue.trim();
			}
			else
				PayMode = null;
		}
		if (FCode.equalsIgnoreCase("PayYears"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PayYears = i;
			}
		}
		if (FCode.equalsIgnoreCase("PayEndYearFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayEndYearFlag = FValue.trim();
			}
			else
				PayEndYearFlag = null;
		}
		if (FCode.equalsIgnoreCase("PayEndYear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PayEndYear = i;
			}
		}
		if (FCode.equalsIgnoreCase("InsuYearFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuYearFlag = FValue.trim();
			}
			else
				InsuYearFlag = null;
		}
		if (FCode.equalsIgnoreCase("InsuYear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				InsuYear = i;
			}
		}
		if (FCode.equalsIgnoreCase("ProtItem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProtItem = FValue.trim();
			}
			else
				ProtItem = null;
		}
		if (FCode.equalsIgnoreCase("CessStart"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CessStart = fDate.getDate( FValue );
			}
			else
				CessStart = null;
		}
		if (FCode.equalsIgnoreCase("CessEnd"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CessEnd = fDate.getDate( FValue );
			}
			else
				CessEnd = null;
		}
		if (FCode.equalsIgnoreCase("PolStat"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PolStat = FValue.trim();
			}
			else
				PolStat = null;
		}
		if (FCode.equalsIgnoreCase("SignDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				SignDate = fDate.getDate( FValue );
			}
			else
				SignDate = null;
		}
		if (FCode.equalsIgnoreCase("SumRiskAmount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SumRiskAmount = d;
			}
		}
		if (FCode.equalsIgnoreCase("NowRiskAmount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				NowRiskAmount = d;
			}
		}
		if (FCode.equalsIgnoreCase("AppntNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppntNo = FValue.trim();
			}
			else
				AppntNo = null;
		}
		if (FCode.equalsIgnoreCase("AppntName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppntName = FValue.trim();
			}
			else
				AppntName = null;
		}
		if (FCode.equalsIgnoreCase("AppntType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppntType = FValue.trim();
			}
			else
				AppntType = null;
		}
		if (FCode.equalsIgnoreCase("SaleChnl"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SaleChnl = FValue.trim();
			}
			else
				SaleChnl = null;
		}
		if (FCode.equalsIgnoreCase("DeadAddFeeRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				DeadAddFeeRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("DiseaseAddFeeRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				DiseaseAddFeeRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("GetDataDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				GetDataDate = fDate.getDate( FValue );
			}
			else
				GetDataDate = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("CostCenter"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CostCenter = FValue.trim();
			}
			else
				CostCenter = null;
		}
		if (FCode.equalsIgnoreCase("ReType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReType = FValue.trim();
			}
			else
				ReType = null;
		}
		if (FCode.equalsIgnoreCase("ActuGetState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ActuGetState = FValue.trim();
			}
			else
				ActuGetState = null;
		}
		if (FCode.equalsIgnoreCase("ActuGetNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ActuGetNo = FValue.trim();
			}
			else
				ActuGetNo = null;
		}
		if (FCode.equalsIgnoreCase("CessAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				CessAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("PayNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayNo = FValue.trim();
			}
			else
				PayNo = null;
		}
		if (FCode.equalsIgnoreCase("ContPlanCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContPlanCode = FValue.trim();
			}
			else
				ContPlanCode = null;
		}
		if (FCode.equalsIgnoreCase("PolTypeFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PolTypeFlag = FValue.trim();
			}
			else
				PolTypeFlag = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LRPolSchema other = (LRPolSchema)otherObject;
		return
			(GrpContNo == null ? other.getGrpContNo() == null : GrpContNo.equals(other.getGrpContNo()))
			&& (GrpPolNo == null ? other.getGrpPolNo() == null : GrpPolNo.equals(other.getGrpPolNo()))
			&& (ContNo == null ? other.getContNo() == null : ContNo.equals(other.getContNo()))
			&& (PolNo == null ? other.getPolNo() == null : PolNo.equals(other.getPolNo()))
			&& (ProposalNo == null ? other.getProposalNo() == null : ProposalNo.equals(other.getProposalNo()))
			&& (PrtNo == null ? other.getPrtNo() == null : PrtNo.equals(other.getPrtNo()))
			&& (ReContCode == null ? other.getReContCode() == null : ReContCode.equals(other.getReContCode()))
			&& (ReinsureItem == null ? other.getReinsureItem() == null : ReinsureItem.equals(other.getReinsureItem()))
			&& (TempCessFlag == null ? other.getTempCessFlag() == null : TempCessFlag.equals(other.getTempCessFlag()))
			&& ReNewCount == other.getReNewCount()
			&& (ContType == null ? other.getContType() == null : ContType.equals(other.getContType()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (RiskCalSort == null ? other.getRiskCalSort() == null : RiskCalSort.equals(other.getRiskCalSort()))
			&& (MainPolNo == null ? other.getMainPolNo() == null : MainPolNo.equals(other.getMainPolNo()))
			&& (MasterPolNo == null ? other.getMasterPolNo() == null : MasterPolNo.equals(other.getMasterPolNo()))
			&& (RiskCode == null ? other.getRiskCode() == null : RiskCode.equals(other.getRiskCode()))
			&& (RiskVersion == null ? other.getRiskVersion() == null : RiskVersion.equals(other.getRiskVersion()))
			&& (InsuredNo == null ? other.getInsuredNo() == null : InsuredNo.equals(other.getInsuredNo()))
			&& (InsuredName == null ? other.getInsuredName() == null : InsuredName.equals(other.getInsuredName()))
			&& (InsuredSex == null ? other.getInsuredSex() == null : InsuredSex.equals(other.getInsuredSex()))
			&& (InsuredBirthday == null ? other.getInsuredBirthday() == null : fDate.getString(InsuredBirthday).equals(other.getInsuredBirthday()))
			&& InsuredAppAge == other.getInsuredAppAge()
			&& Years == other.getYears()
			&& (CValiDate == null ? other.getCValiDate() == null : fDate.getString(CValiDate).equals(other.getCValiDate()))
			&& (EndDate == null ? other.getEndDate() == null : fDate.getString(EndDate).equals(other.getEndDate()))
			&& StandPrem == other.getStandPrem()
			&& Prem == other.getPrem()
			&& SumPrem == other.getSumPrem()
			&& Amnt == other.getAmnt()
			&& (InsurePeriod == null ? other.getInsurePeriod() == null : InsurePeriod.equals(other.getInsurePeriod()))
			&& (OccupationType == null ? other.getOccupationType() == null : OccupationType.equals(other.getOccupationType()))
			&& RiskAmnt == other.getRiskAmnt()
			&& PayIntv == other.getPayIntv()
			&& (PayMode == null ? other.getPayMode() == null : PayMode.equals(other.getPayMode()))
			&& PayYears == other.getPayYears()
			&& (PayEndYearFlag == null ? other.getPayEndYearFlag() == null : PayEndYearFlag.equals(other.getPayEndYearFlag()))
			&& PayEndYear == other.getPayEndYear()
			&& (InsuYearFlag == null ? other.getInsuYearFlag() == null : InsuYearFlag.equals(other.getInsuYearFlag()))
			&& InsuYear == other.getInsuYear()
			&& (ProtItem == null ? other.getProtItem() == null : ProtItem.equals(other.getProtItem()))
			&& (CessStart == null ? other.getCessStart() == null : fDate.getString(CessStart).equals(other.getCessStart()))
			&& (CessEnd == null ? other.getCessEnd() == null : fDate.getString(CessEnd).equals(other.getCessEnd()))
			&& (PolStat == null ? other.getPolStat() == null : PolStat.equals(other.getPolStat()))
			&& (SignDate == null ? other.getSignDate() == null : fDate.getString(SignDate).equals(other.getSignDate()))
			&& SumRiskAmount == other.getSumRiskAmount()
			&& NowRiskAmount == other.getNowRiskAmount()
			&& (AppntNo == null ? other.getAppntNo() == null : AppntNo.equals(other.getAppntNo()))
			&& (AppntName == null ? other.getAppntName() == null : AppntName.equals(other.getAppntName()))
			&& (AppntType == null ? other.getAppntType() == null : AppntType.equals(other.getAppntType()))
			&& (SaleChnl == null ? other.getSaleChnl() == null : SaleChnl.equals(other.getSaleChnl()))
			&& DeadAddFeeRate == other.getDeadAddFeeRate()
			&& DiseaseAddFeeRate == other.getDiseaseAddFeeRate()
			&& (GetDataDate == null ? other.getGetDataDate() == null : fDate.getString(GetDataDate).equals(other.getGetDataDate()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (CostCenter == null ? other.getCostCenter() == null : CostCenter.equals(other.getCostCenter()))
			&& (ReType == null ? other.getReType() == null : ReType.equals(other.getReType()))
			&& (ActuGetState == null ? other.getActuGetState() == null : ActuGetState.equals(other.getActuGetState()))
			&& (ActuGetNo == null ? other.getActuGetNo() == null : ActuGetNo.equals(other.getActuGetNo()))
			&& CessAmnt == other.getCessAmnt()
			&& (PayNo == null ? other.getPayNo() == null : PayNo.equals(other.getPayNo()))
			&& (ContPlanCode == null ? other.getContPlanCode() == null : ContPlanCode.equals(other.getContPlanCode()))
			&& (PolTypeFlag == null ? other.getPolTypeFlag() == null : PolTypeFlag.equals(other.getPolTypeFlag()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("GrpContNo") ) {
			return 0;
		}
		if( strFieldName.equals("GrpPolNo") ) {
			return 1;
		}
		if( strFieldName.equals("ContNo") ) {
			return 2;
		}
		if( strFieldName.equals("PolNo") ) {
			return 3;
		}
		if( strFieldName.equals("ProposalNo") ) {
			return 4;
		}
		if( strFieldName.equals("PrtNo") ) {
			return 5;
		}
		if( strFieldName.equals("ReContCode") ) {
			return 6;
		}
		if( strFieldName.equals("ReinsureItem") ) {
			return 7;
		}
		if( strFieldName.equals("TempCessFlag") ) {
			return 8;
		}
		if( strFieldName.equals("ReNewCount") ) {
			return 9;
		}
		if( strFieldName.equals("ContType") ) {
			return 10;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 11;
		}
		if( strFieldName.equals("RiskCalSort") ) {
			return 12;
		}
		if( strFieldName.equals("MainPolNo") ) {
			return 13;
		}
		if( strFieldName.equals("MasterPolNo") ) {
			return 14;
		}
		if( strFieldName.equals("RiskCode") ) {
			return 15;
		}
		if( strFieldName.equals("RiskVersion") ) {
			return 16;
		}
		if( strFieldName.equals("InsuredNo") ) {
			return 17;
		}
		if( strFieldName.equals("InsuredName") ) {
			return 18;
		}
		if( strFieldName.equals("InsuredSex") ) {
			return 19;
		}
		if( strFieldName.equals("InsuredBirthday") ) {
			return 20;
		}
		if( strFieldName.equals("InsuredAppAge") ) {
			return 21;
		}
		if( strFieldName.equals("Years") ) {
			return 22;
		}
		if( strFieldName.equals("CValiDate") ) {
			return 23;
		}
		if( strFieldName.equals("EndDate") ) {
			return 24;
		}
		if( strFieldName.equals("StandPrem") ) {
			return 25;
		}
		if( strFieldName.equals("Prem") ) {
			return 26;
		}
		if( strFieldName.equals("SumPrem") ) {
			return 27;
		}
		if( strFieldName.equals("Amnt") ) {
			return 28;
		}
		if( strFieldName.equals("InsurePeriod") ) {
			return 29;
		}
		if( strFieldName.equals("OccupationType") ) {
			return 30;
		}
		if( strFieldName.equals("RiskAmnt") ) {
			return 31;
		}
		if( strFieldName.equals("PayIntv") ) {
			return 32;
		}
		if( strFieldName.equals("PayMode") ) {
			return 33;
		}
		if( strFieldName.equals("PayYears") ) {
			return 34;
		}
		if( strFieldName.equals("PayEndYearFlag") ) {
			return 35;
		}
		if( strFieldName.equals("PayEndYear") ) {
			return 36;
		}
		if( strFieldName.equals("InsuYearFlag") ) {
			return 37;
		}
		if( strFieldName.equals("InsuYear") ) {
			return 38;
		}
		if( strFieldName.equals("ProtItem") ) {
			return 39;
		}
		if( strFieldName.equals("CessStart") ) {
			return 40;
		}
		if( strFieldName.equals("CessEnd") ) {
			return 41;
		}
		if( strFieldName.equals("PolStat") ) {
			return 42;
		}
		if( strFieldName.equals("SignDate") ) {
			return 43;
		}
		if( strFieldName.equals("SumRiskAmount") ) {
			return 44;
		}
		if( strFieldName.equals("NowRiskAmount") ) {
			return 45;
		}
		if( strFieldName.equals("AppntNo") ) {
			return 46;
		}
		if( strFieldName.equals("AppntName") ) {
			return 47;
		}
		if( strFieldName.equals("AppntType") ) {
			return 48;
		}
		if( strFieldName.equals("SaleChnl") ) {
			return 49;
		}
		if( strFieldName.equals("DeadAddFeeRate") ) {
			return 50;
		}
		if( strFieldName.equals("DiseaseAddFeeRate") ) {
			return 51;
		}
		if( strFieldName.equals("GetDataDate") ) {
			return 52;
		}
		if( strFieldName.equals("Operator") ) {
			return 53;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 54;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 55;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 56;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 57;
		}
		if( strFieldName.equals("CostCenter") ) {
			return 58;
		}
		if( strFieldName.equals("ReType") ) {
			return 59;
		}
		if( strFieldName.equals("ActuGetState") ) {
			return 60;
		}
		if( strFieldName.equals("ActuGetNo") ) {
			return 61;
		}
		if( strFieldName.equals("CessAmnt") ) {
			return 62;
		}
		if( strFieldName.equals("PayNo") ) {
			return 63;
		}
		if( strFieldName.equals("ContPlanCode") ) {
			return 64;
		}
		if( strFieldName.equals("PolTypeFlag") ) {
			return 65;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "GrpContNo";
				break;
			case 1:
				strFieldName = "GrpPolNo";
				break;
			case 2:
				strFieldName = "ContNo";
				break;
			case 3:
				strFieldName = "PolNo";
				break;
			case 4:
				strFieldName = "ProposalNo";
				break;
			case 5:
				strFieldName = "PrtNo";
				break;
			case 6:
				strFieldName = "ReContCode";
				break;
			case 7:
				strFieldName = "ReinsureItem";
				break;
			case 8:
				strFieldName = "TempCessFlag";
				break;
			case 9:
				strFieldName = "ReNewCount";
				break;
			case 10:
				strFieldName = "ContType";
				break;
			case 11:
				strFieldName = "ManageCom";
				break;
			case 12:
				strFieldName = "RiskCalSort";
				break;
			case 13:
				strFieldName = "MainPolNo";
				break;
			case 14:
				strFieldName = "MasterPolNo";
				break;
			case 15:
				strFieldName = "RiskCode";
				break;
			case 16:
				strFieldName = "RiskVersion";
				break;
			case 17:
				strFieldName = "InsuredNo";
				break;
			case 18:
				strFieldName = "InsuredName";
				break;
			case 19:
				strFieldName = "InsuredSex";
				break;
			case 20:
				strFieldName = "InsuredBirthday";
				break;
			case 21:
				strFieldName = "InsuredAppAge";
				break;
			case 22:
				strFieldName = "Years";
				break;
			case 23:
				strFieldName = "CValiDate";
				break;
			case 24:
				strFieldName = "EndDate";
				break;
			case 25:
				strFieldName = "StandPrem";
				break;
			case 26:
				strFieldName = "Prem";
				break;
			case 27:
				strFieldName = "SumPrem";
				break;
			case 28:
				strFieldName = "Amnt";
				break;
			case 29:
				strFieldName = "InsurePeriod";
				break;
			case 30:
				strFieldName = "OccupationType";
				break;
			case 31:
				strFieldName = "RiskAmnt";
				break;
			case 32:
				strFieldName = "PayIntv";
				break;
			case 33:
				strFieldName = "PayMode";
				break;
			case 34:
				strFieldName = "PayYears";
				break;
			case 35:
				strFieldName = "PayEndYearFlag";
				break;
			case 36:
				strFieldName = "PayEndYear";
				break;
			case 37:
				strFieldName = "InsuYearFlag";
				break;
			case 38:
				strFieldName = "InsuYear";
				break;
			case 39:
				strFieldName = "ProtItem";
				break;
			case 40:
				strFieldName = "CessStart";
				break;
			case 41:
				strFieldName = "CessEnd";
				break;
			case 42:
				strFieldName = "PolStat";
				break;
			case 43:
				strFieldName = "SignDate";
				break;
			case 44:
				strFieldName = "SumRiskAmount";
				break;
			case 45:
				strFieldName = "NowRiskAmount";
				break;
			case 46:
				strFieldName = "AppntNo";
				break;
			case 47:
				strFieldName = "AppntName";
				break;
			case 48:
				strFieldName = "AppntType";
				break;
			case 49:
				strFieldName = "SaleChnl";
				break;
			case 50:
				strFieldName = "DeadAddFeeRate";
				break;
			case 51:
				strFieldName = "DiseaseAddFeeRate";
				break;
			case 52:
				strFieldName = "GetDataDate";
				break;
			case 53:
				strFieldName = "Operator";
				break;
			case 54:
				strFieldName = "MakeDate";
				break;
			case 55:
				strFieldName = "MakeTime";
				break;
			case 56:
				strFieldName = "ModifyDate";
				break;
			case 57:
				strFieldName = "ModifyTime";
				break;
			case 58:
				strFieldName = "CostCenter";
				break;
			case 59:
				strFieldName = "ReType";
				break;
			case 60:
				strFieldName = "ActuGetState";
				break;
			case 61:
				strFieldName = "ActuGetNo";
				break;
			case 62:
				strFieldName = "CessAmnt";
				break;
			case 63:
				strFieldName = "PayNo";
				break;
			case 64:
				strFieldName = "ContPlanCode";
				break;
			case 65:
				strFieldName = "PolTypeFlag";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("GrpContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpPolNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ProposalNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PrtNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReContCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReinsureItem") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TempCessFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReNewCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("ContType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskCalSort") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MainPolNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MasterPolNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskVersion") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuredNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuredName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuredSex") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuredBirthday") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("InsuredAppAge") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("Years") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("CValiDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("EndDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("StandPrem") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Prem") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SumPrem") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Amnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("InsurePeriod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OccupationType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("PayIntv") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("PayMode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayYears") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("PayEndYearFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayEndYear") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("InsuYearFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuYear") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("ProtItem") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CessStart") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("CessEnd") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("PolStat") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SignDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("SumRiskAmount") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("NowRiskAmount") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("AppntNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppntName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppntType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SaleChnl") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DeadAddFeeRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("DiseaseAddFeeRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("GetDataDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CostCenter") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ActuGetState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ActuGetNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CessAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("PayNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContPlanCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolTypeFlag") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_INT;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 21:
				nFieldType = Schema.TYPE_INT;
				break;
			case 22:
				nFieldType = Schema.TYPE_INT;
				break;
			case 23:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 24:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 25:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 26:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 27:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 28:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 32:
				nFieldType = Schema.TYPE_INT;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_INT;
				break;
			case 35:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 36:
				nFieldType = Schema.TYPE_INT;
				break;
			case 37:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 38:
				nFieldType = Schema.TYPE_INT;
				break;
			case 39:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 40:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 41:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 42:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 43:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 44:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 45:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 46:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 47:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 48:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 49:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 50:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 51:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 52:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 53:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 54:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 55:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 56:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 57:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 58:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 59:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 60:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 61:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 62:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 63:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 64:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 65:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
