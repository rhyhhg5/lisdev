/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LCAddDecInsuredDB;

/*
 * <p>ClassName: LCAddDecInsuredSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 被保人增减人清单
 * @CreateDate：2006-04-28
 */
public class LCAddDecInsuredSchema implements Schema, Cloneable
{
	// @Field
	/** 集体合同号码 */
	private String GrpContNo;
	/** 被保人序号 */
	private String InsuredID;
	/** 状态 */
	private String State;
	/** 合同号码 */
	private String ContNo;
	/** 续期期数 */
	private String BatchNo;
	/** 被保人客户号 */
	private String InsuredNo;
	/** 在职/退休 */
	private String Retire;
	/** 员工姓名 */
	private String EmployeeName;
	/** 被保人姓名 */
	private String InsuredName;
	/** 与员工关系 */
	private String Relation;
	/** 性别 */
	private String Sex;
	/** 出生日期 */
	private Date Birthday;
	/** 证件类型 */
	private String IDType;
	/** 证件号码 */
	private String IDNo;
	/** 保险计划 */
	private String ContPlanCode;
	/** 职业类别 */
	private String OccupationType;
	/** 理赔金转帐银行 */
	private String BankCode;
	/** 帐号 */
	private String BankAccNo;
	/** 户名 */
	private String AccName;
	/** 其它证件类型 */
	private String OthIDType;
	/** 其它证件号码 */
	private String OthIDNo;
	/** 保全生效日期 */
	private Date EdorValiDate;
	/** 期交保费 */
	private String Prem;
	/** 单位编码 */
	private String GrpNo;
	/** 单位名称 */
	private String GrpName;
	/** 社保分中心代码 */
	private String SocialCenterNo;
	/** 社保中心名称 */
	private String SocialCenterName;
	/** 增减人标志 */
	private String AddDecFlag;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 比对完成标记 */
	private String FinishFlag;

	public static final int FIELDNUM = 34;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LCAddDecInsuredSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[3];
		pk[0] = "GrpContNo";
		pk[1] = "InsuredID";
		pk[2] = "BatchNo";

		PK = pk;
	}

            /**
             * Schema克隆
             * @return Object
             * @throws CloneNotSupportedException
             */
            public Object clone()
                    throws CloneNotSupportedException
            {
                LCAddDecInsuredSchema cloned = (LCAddDecInsuredSchema)super.clone();
                cloned.fDate = (FDate) fDate.clone();
                cloned.mErrors = (CErrors) mErrors.clone();
                return cloned;
            }

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getGrpContNo()
	{
		return GrpContNo;
	}
	public void setGrpContNo(String aGrpContNo)
	{
            GrpContNo = aGrpContNo;
	}
	public String getInsuredID()
	{
		return InsuredID;
	}
	public void setInsuredID(String aInsuredID)
	{
            InsuredID = aInsuredID;
	}
	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
            State = aState;
	}
	public String getContNo()
	{
		return ContNo;
	}
	public void setContNo(String aContNo)
	{
            ContNo = aContNo;
	}
	public String getBatchNo()
	{
		return BatchNo;
	}
	public void setBatchNo(String aBatchNo)
	{
            BatchNo = aBatchNo;
	}
	public String getInsuredNo()
	{
		return InsuredNo;
	}
	public void setInsuredNo(String aInsuredNo)
	{
            InsuredNo = aInsuredNo;
	}
	public String getRetire()
	{
		return Retire;
	}
	public void setRetire(String aRetire)
	{
            Retire = aRetire;
	}
	public String getEmployeeName()
	{
		return EmployeeName;
	}
	public void setEmployeeName(String aEmployeeName)
	{
            EmployeeName = aEmployeeName;
	}
	public String getInsuredName()
	{
		return InsuredName;
	}
	public void setInsuredName(String aInsuredName)
	{
            InsuredName = aInsuredName;
	}
	public String getRelation()
	{
		return Relation;
	}
	public void setRelation(String aRelation)
	{
            Relation = aRelation;
	}
	public String getSex()
	{
		return Sex;
	}
	public void setSex(String aSex)
	{
            Sex = aSex;
	}
	public String getBirthday()
	{
		if( Birthday != null )
			return fDate.getString(Birthday);
		else
			return null;
	}
	public void setBirthday(Date aBirthday)
	{
            Birthday = aBirthday;
	}
	public void setBirthday(String aBirthday)
	{
		if (aBirthday != null && !aBirthday.equals("") )
		{
			Birthday = fDate.getDate( aBirthday );
		}
		else
			Birthday = null;
	}

	public String getIDType()
	{
		return IDType;
	}
	public void setIDType(String aIDType)
	{
            IDType = aIDType;
	}
	public String getIDNo()
	{
		return IDNo;
	}
	public void setIDNo(String aIDNo)
	{
            IDNo = aIDNo;
	}
	public String getContPlanCode()
	{
		return ContPlanCode;
	}
	public void setContPlanCode(String aContPlanCode)
	{
            ContPlanCode = aContPlanCode;
	}
	public String getOccupationType()
	{
		return OccupationType;
	}
	public void setOccupationType(String aOccupationType)
	{
            OccupationType = aOccupationType;
	}
	public String getBankCode()
	{
		return BankCode;
	}
	public void setBankCode(String aBankCode)
	{
            BankCode = aBankCode;
	}
	public String getBankAccNo()
	{
		return BankAccNo;
	}
	public void setBankAccNo(String aBankAccNo)
	{
            BankAccNo = aBankAccNo;
	}
	public String getAccName()
	{
		return AccName;
	}
	public void setAccName(String aAccName)
	{
            AccName = aAccName;
	}
	public String getOthIDType()
	{
		return OthIDType;
	}
	public void setOthIDType(String aOthIDType)
	{
            OthIDType = aOthIDType;
	}
	public String getOthIDNo()
	{
		return OthIDNo;
	}
	public void setOthIDNo(String aOthIDNo)
	{
            OthIDNo = aOthIDNo;
	}
	public String getEdorValiDate()
	{
		if( EdorValiDate != null )
			return fDate.getString(EdorValiDate);
		else
			return null;
	}
	public void setEdorValiDate(Date aEdorValiDate)
	{
            EdorValiDate = aEdorValiDate;
	}
	public void setEdorValiDate(String aEdorValiDate)
	{
		if (aEdorValiDate != null && !aEdorValiDate.equals("") )
		{
			EdorValiDate = fDate.getDate( aEdorValiDate );
		}
		else
			EdorValiDate = null;
	}

	public String getPrem()
	{
		return Prem;
	}
	public void setPrem(String aPrem)
	{
            Prem = aPrem;
	}
	public String getGrpNo()
	{
		return GrpNo;
	}
	public void setGrpNo(String aGrpNo)
	{
            GrpNo = aGrpNo;
	}
	public String getGrpName()
	{
		return GrpName;
	}
	public void setGrpName(String aGrpName)
	{
            GrpName = aGrpName;
	}
	public String getSocialCenterNo()
	{
		return SocialCenterNo;
	}
	public void setSocialCenterNo(String aSocialCenterNo)
	{
            SocialCenterNo = aSocialCenterNo;
	}
	public String getSocialCenterName()
	{
		return SocialCenterName;
	}
	public void setSocialCenterName(String aSocialCenterName)
	{
            SocialCenterName = aSocialCenterName;
	}
	public String getAddDecFlag()
	{
		return AddDecFlag;
	}
	public void setAddDecFlag(String aAddDecFlag)
	{
            AddDecFlag = aAddDecFlag;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
            Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
            MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
            MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
            ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
            ModifyTime = aModifyTime;
	}
	public String getFinishFlag()
	{
		return FinishFlag;
	}
	public void setFinishFlag(String aFinishFlag)
	{
            FinishFlag = aFinishFlag;
	}

	/**
	* 使用另外一个 LCAddDecInsuredSchema 对象给 Schema 赋值
	* @param: aLCAddDecInsuredSchema LCAddDecInsuredSchema
	**/
	public void setSchema(LCAddDecInsuredSchema aLCAddDecInsuredSchema)
	{
		this.GrpContNo = aLCAddDecInsuredSchema.getGrpContNo();
		this.InsuredID = aLCAddDecInsuredSchema.getInsuredID();
		this.State = aLCAddDecInsuredSchema.getState();
		this.ContNo = aLCAddDecInsuredSchema.getContNo();
		this.BatchNo = aLCAddDecInsuredSchema.getBatchNo();
		this.InsuredNo = aLCAddDecInsuredSchema.getInsuredNo();
		this.Retire = aLCAddDecInsuredSchema.getRetire();
		this.EmployeeName = aLCAddDecInsuredSchema.getEmployeeName();
		this.InsuredName = aLCAddDecInsuredSchema.getInsuredName();
		this.Relation = aLCAddDecInsuredSchema.getRelation();
		this.Sex = aLCAddDecInsuredSchema.getSex();
		this.Birthday = fDate.getDate( aLCAddDecInsuredSchema.getBirthday());
		this.IDType = aLCAddDecInsuredSchema.getIDType();
		this.IDNo = aLCAddDecInsuredSchema.getIDNo();
		this.ContPlanCode = aLCAddDecInsuredSchema.getContPlanCode();
		this.OccupationType = aLCAddDecInsuredSchema.getOccupationType();
		this.BankCode = aLCAddDecInsuredSchema.getBankCode();
		this.BankAccNo = aLCAddDecInsuredSchema.getBankAccNo();
		this.AccName = aLCAddDecInsuredSchema.getAccName();
		this.OthIDType = aLCAddDecInsuredSchema.getOthIDType();
		this.OthIDNo = aLCAddDecInsuredSchema.getOthIDNo();
		this.EdorValiDate = fDate.getDate( aLCAddDecInsuredSchema.getEdorValiDate());
		this.Prem = aLCAddDecInsuredSchema.getPrem();
		this.GrpNo = aLCAddDecInsuredSchema.getGrpNo();
		this.GrpName = aLCAddDecInsuredSchema.getGrpName();
		this.SocialCenterNo = aLCAddDecInsuredSchema.getSocialCenterNo();
		this.SocialCenterName = aLCAddDecInsuredSchema.getSocialCenterName();
		this.AddDecFlag = aLCAddDecInsuredSchema.getAddDecFlag();
		this.Operator = aLCAddDecInsuredSchema.getOperator();
		this.MakeDate = fDate.getDate( aLCAddDecInsuredSchema.getMakeDate());
		this.MakeTime = aLCAddDecInsuredSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLCAddDecInsuredSchema.getModifyDate());
		this.ModifyTime = aLCAddDecInsuredSchema.getModifyTime();
		this.FinishFlag = aLCAddDecInsuredSchema.getFinishFlag();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("GrpContNo") == null )
				this.GrpContNo = null;
			else
				this.GrpContNo = rs.getString("GrpContNo").trim();

			if( rs.getString("InsuredID") == null )
				this.InsuredID = null;
			else
				this.InsuredID = rs.getString("InsuredID").trim();

			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			if( rs.getString("ContNo") == null )
				this.ContNo = null;
			else
				this.ContNo = rs.getString("ContNo").trim();

			if( rs.getString("BatchNo") == null )
				this.BatchNo = null;
			else
				this.BatchNo = rs.getString("BatchNo").trim();

			if( rs.getString("InsuredNo") == null )
				this.InsuredNo = null;
			else
				this.InsuredNo = rs.getString("InsuredNo").trim();

			if( rs.getString("Retire") == null )
				this.Retire = null;
			else
				this.Retire = rs.getString("Retire").trim();

			if( rs.getString("EmployeeName") == null )
				this.EmployeeName = null;
			else
				this.EmployeeName = rs.getString("EmployeeName").trim();

			if( rs.getString("InsuredName") == null )
				this.InsuredName = null;
			else
				this.InsuredName = rs.getString("InsuredName").trim();

			if( rs.getString("Relation") == null )
				this.Relation = null;
			else
				this.Relation = rs.getString("Relation").trim();

			if( rs.getString("Sex") == null )
				this.Sex = null;
			else
				this.Sex = rs.getString("Sex").trim();

			this.Birthday = rs.getDate("Birthday");
			if( rs.getString("IDType") == null )
				this.IDType = null;
			else
				this.IDType = rs.getString("IDType").trim();

			if( rs.getString("IDNo") == null )
				this.IDNo = null;
			else
				this.IDNo = rs.getString("IDNo").trim();

			if( rs.getString("ContPlanCode") == null )
				this.ContPlanCode = null;
			else
				this.ContPlanCode = rs.getString("ContPlanCode").trim();

			if( rs.getString("OccupationType") == null )
				this.OccupationType = null;
			else
				this.OccupationType = rs.getString("OccupationType").trim();

			if( rs.getString("BankCode") == null )
				this.BankCode = null;
			else
				this.BankCode = rs.getString("BankCode").trim();

			if( rs.getString("BankAccNo") == null )
				this.BankAccNo = null;
			else
				this.BankAccNo = rs.getString("BankAccNo").trim();

			if( rs.getString("AccName") == null )
				this.AccName = null;
			else
				this.AccName = rs.getString("AccName").trim();

			if( rs.getString("OthIDType") == null )
				this.OthIDType = null;
			else
				this.OthIDType = rs.getString("OthIDType").trim();

			if( rs.getString("OthIDNo") == null )
				this.OthIDNo = null;
			else
				this.OthIDNo = rs.getString("OthIDNo").trim();

			this.EdorValiDate = rs.getDate("EdorValiDate");
			if( rs.getString("Prem") == null )
				this.Prem = null;
			else
				this.Prem = rs.getString("Prem").trim();

			if( rs.getString("GrpNo") == null )
				this.GrpNo = null;
			else
				this.GrpNo = rs.getString("GrpNo").trim();

			if( rs.getString("GrpName") == null )
				this.GrpName = null;
			else
				this.GrpName = rs.getString("GrpName").trim();

			if( rs.getString("SocialCenterNo") == null )
				this.SocialCenterNo = null;
			else
				this.SocialCenterNo = rs.getString("SocialCenterNo").trim();

			if( rs.getString("SocialCenterName") == null )
				this.SocialCenterName = null;
			else
				this.SocialCenterName = rs.getString("SocialCenterName").trim();

			if( rs.getString("AddDecFlag") == null )
				this.AddDecFlag = null;
			else
				this.AddDecFlag = rs.getString("AddDecFlag").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("FinishFlag") == null )
				this.FinishFlag = null;
			else
				this.FinishFlag = rs.getString("FinishFlag").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LCAddDecInsured表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCAddDecInsuredSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LCAddDecInsuredSchema getSchema()
	{
		LCAddDecInsuredSchema aLCAddDecInsuredSchema = new LCAddDecInsuredSchema();
		aLCAddDecInsuredSchema.setSchema(this);
		return aLCAddDecInsuredSchema;
	}

	public LCAddDecInsuredDB getDB()
	{
		LCAddDecInsuredDB aDBOper = new LCAddDecInsuredDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCAddDecInsured描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(InsuredID)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(BatchNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(InsuredNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Retire)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(EmployeeName)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(InsuredName)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Relation)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Sex)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( Birthday ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(IDType)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(IDNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ContPlanCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(OccupationType)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(BankCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(BankAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(AccName)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(OthIDType)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(OthIDNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( EdorValiDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Prem)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(GrpNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(GrpName)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(SocialCenterNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(SocialCenterName)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(AddDecFlag)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(FinishFlag));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCAddDecInsured>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			InsuredID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			BatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			InsuredNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			Retire = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			EmployeeName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			InsuredName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			Relation = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			Sex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			Birthday = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
			IDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			IDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			ContPlanCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			OccupationType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			BankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			AccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			OthIDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			OthIDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			EdorValiDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,SysConst.PACKAGESPILTER));
			Prem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			GrpNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			GrpName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			SocialCenterNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			SocialCenterName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			AddDecFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			FinishFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCAddDecInsuredSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("GrpContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
		}
		if (FCode.equals("InsuredID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredID));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("ContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
		}
		if (FCode.equals("BatchNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BatchNo));
		}
		if (FCode.equals("InsuredNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredNo));
		}
		if (FCode.equals("Retire"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Retire));
		}
		if (FCode.equals("EmployeeName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EmployeeName));
		}
		if (FCode.equals("InsuredName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredName));
		}
		if (FCode.equals("Relation"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Relation));
		}
		if (FCode.equals("Sex"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Sex));
		}
		if (FCode.equals("Birthday"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getBirthday()));
		}
		if (FCode.equals("IDType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IDType));
		}
		if (FCode.equals("IDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
		}
		if (FCode.equals("ContPlanCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContPlanCode));
		}
		if (FCode.equals("OccupationType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationType));
		}
		if (FCode.equals("BankCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
		}
		if (FCode.equals("BankAccNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccNo));
		}
		if (FCode.equals("AccName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccName));
		}
		if (FCode.equals("OthIDType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OthIDType));
		}
		if (FCode.equals("OthIDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OthIDNo));
		}
		if (FCode.equals("EdorValiDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEdorValiDate()));
		}
		if (FCode.equals("Prem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Prem));
		}
		if (FCode.equals("GrpNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpNo));
		}
		if (FCode.equals("GrpName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpName));
		}
		if (FCode.equals("SocialCenterNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SocialCenterNo));
		}
		if (FCode.equals("SocialCenterName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SocialCenterName));
		}
		if (FCode.equals("AddDecFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AddDecFlag));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("FinishFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FinishFlag));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(GrpContNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(InsuredID);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(ContNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(BatchNo);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(InsuredNo);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(Retire);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(EmployeeName);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(InsuredName);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(Relation);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(Sex);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getBirthday()));
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(IDType);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(IDNo);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(ContPlanCode);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(OccupationType);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(BankCode);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(BankAccNo);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(AccName);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(OthIDType);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(OthIDNo);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEdorValiDate()));
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(Prem);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(GrpNo);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(GrpName);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(SocialCenterNo);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(SocialCenterName);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(AddDecFlag);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(FinishFlag);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("GrpContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpContNo = FValue.trim();
			}
			else
				GrpContNo = null;
		}
		if (FCode.equalsIgnoreCase("InsuredID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuredID = FValue.trim();
			}
			else
				InsuredID = null;
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("ContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContNo = FValue.trim();
			}
			else
				ContNo = null;
		}
		if (FCode.equalsIgnoreCase("BatchNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BatchNo = FValue.trim();
			}
			else
				BatchNo = null;
		}
		if (FCode.equalsIgnoreCase("InsuredNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuredNo = FValue.trim();
			}
			else
				InsuredNo = null;
		}
		if (FCode.equalsIgnoreCase("Retire"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Retire = FValue.trim();
			}
			else
				Retire = null;
		}
		if (FCode.equalsIgnoreCase("EmployeeName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EmployeeName = FValue.trim();
			}
			else
				EmployeeName = null;
		}
		if (FCode.equalsIgnoreCase("InsuredName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuredName = FValue.trim();
			}
			else
				InsuredName = null;
		}
		if (FCode.equalsIgnoreCase("Relation"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Relation = FValue.trim();
			}
			else
				Relation = null;
		}
		if (FCode.equalsIgnoreCase("Sex"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Sex = FValue.trim();
			}
			else
				Sex = null;
		}
		if (FCode.equalsIgnoreCase("Birthday"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Birthday = fDate.getDate( FValue );
			}
			else
				Birthday = null;
		}
		if (FCode.equalsIgnoreCase("IDType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IDType = FValue.trim();
			}
			else
				IDType = null;
		}
		if (FCode.equalsIgnoreCase("IDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IDNo = FValue.trim();
			}
			else
				IDNo = null;
		}
		if (FCode.equalsIgnoreCase("ContPlanCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContPlanCode = FValue.trim();
			}
			else
				ContPlanCode = null;
		}
		if (FCode.equalsIgnoreCase("OccupationType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OccupationType = FValue.trim();
			}
			else
				OccupationType = null;
		}
		if (FCode.equalsIgnoreCase("BankCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankCode = FValue.trim();
			}
			else
				BankCode = null;
		}
		if (FCode.equalsIgnoreCase("BankAccNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankAccNo = FValue.trim();
			}
			else
				BankAccNo = null;
		}
		if (FCode.equalsIgnoreCase("AccName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccName = FValue.trim();
			}
			else
				AccName = null;
		}
		if (FCode.equalsIgnoreCase("OthIDType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OthIDType = FValue.trim();
			}
			else
				OthIDType = null;
		}
		if (FCode.equalsIgnoreCase("OthIDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OthIDNo = FValue.trim();
			}
			else
				OthIDNo = null;
		}
		if (FCode.equalsIgnoreCase("EdorValiDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				EdorValiDate = fDate.getDate( FValue );
			}
			else
				EdorValiDate = null;
		}
		if (FCode.equalsIgnoreCase("Prem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Prem = FValue.trim();
			}
			else
				Prem = null;
		}
		if (FCode.equalsIgnoreCase("GrpNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpNo = FValue.trim();
			}
			else
				GrpNo = null;
		}
		if (FCode.equalsIgnoreCase("GrpName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpName = FValue.trim();
			}
			else
				GrpName = null;
		}
		if (FCode.equalsIgnoreCase("SocialCenterNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SocialCenterNo = FValue.trim();
			}
			else
				SocialCenterNo = null;
		}
		if (FCode.equalsIgnoreCase("SocialCenterName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SocialCenterName = FValue.trim();
			}
			else
				SocialCenterName = null;
		}
		if (FCode.equalsIgnoreCase("AddDecFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AddDecFlag = FValue.trim();
			}
			else
				AddDecFlag = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("FinishFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FinishFlag = FValue.trim();
			}
			else
				FinishFlag = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LCAddDecInsuredSchema other = (LCAddDecInsuredSchema)otherObject;
		return
			GrpContNo.equals(other.getGrpContNo())
			&& InsuredID.equals(other.getInsuredID())
			&& State.equals(other.getState())
			&& ContNo.equals(other.getContNo())
			&& BatchNo.equals(other.getBatchNo())
			&& InsuredNo.equals(other.getInsuredNo())
			&& Retire.equals(other.getRetire())
			&& EmployeeName.equals(other.getEmployeeName())
			&& InsuredName.equals(other.getInsuredName())
			&& Relation.equals(other.getRelation())
			&& Sex.equals(other.getSex())
			&& fDate.getString(Birthday).equals(other.getBirthday())
			&& IDType.equals(other.getIDType())
			&& IDNo.equals(other.getIDNo())
			&& ContPlanCode.equals(other.getContPlanCode())
			&& OccupationType.equals(other.getOccupationType())
			&& BankCode.equals(other.getBankCode())
			&& BankAccNo.equals(other.getBankAccNo())
			&& AccName.equals(other.getAccName())
			&& OthIDType.equals(other.getOthIDType())
			&& OthIDNo.equals(other.getOthIDNo())
			&& fDate.getString(EdorValiDate).equals(other.getEdorValiDate())
			&& Prem.equals(other.getPrem())
			&& GrpNo.equals(other.getGrpNo())
			&& GrpName.equals(other.getGrpName())
			&& SocialCenterNo.equals(other.getSocialCenterNo())
			&& SocialCenterName.equals(other.getSocialCenterName())
			&& AddDecFlag.equals(other.getAddDecFlag())
			&& Operator.equals(other.getOperator())
			&& fDate.getString(MakeDate).equals(other.getMakeDate())
			&& MakeTime.equals(other.getMakeTime())
			&& fDate.getString(ModifyDate).equals(other.getModifyDate())
			&& ModifyTime.equals(other.getModifyTime())
			&& FinishFlag.equals(other.getFinishFlag());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("GrpContNo") ) {
			return 0;
		}
		if( strFieldName.equals("InsuredID") ) {
			return 1;
		}
		if( strFieldName.equals("State") ) {
			return 2;
		}
		if( strFieldName.equals("ContNo") ) {
			return 3;
		}
		if( strFieldName.equals("BatchNo") ) {
			return 4;
		}
		if( strFieldName.equals("InsuredNo") ) {
			return 5;
		}
		if( strFieldName.equals("Retire") ) {
			return 6;
		}
		if( strFieldName.equals("EmployeeName") ) {
			return 7;
		}
		if( strFieldName.equals("InsuredName") ) {
			return 8;
		}
		if( strFieldName.equals("Relation") ) {
			return 9;
		}
		if( strFieldName.equals("Sex") ) {
			return 10;
		}
		if( strFieldName.equals("Birthday") ) {
			return 11;
		}
		if( strFieldName.equals("IDType") ) {
			return 12;
		}
		if( strFieldName.equals("IDNo") ) {
			return 13;
		}
		if( strFieldName.equals("ContPlanCode") ) {
			return 14;
		}
		if( strFieldName.equals("OccupationType") ) {
			return 15;
		}
		if( strFieldName.equals("BankCode") ) {
			return 16;
		}
		if( strFieldName.equals("BankAccNo") ) {
			return 17;
		}
		if( strFieldName.equals("AccName") ) {
			return 18;
		}
		if( strFieldName.equals("OthIDType") ) {
			return 19;
		}
		if( strFieldName.equals("OthIDNo") ) {
			return 20;
		}
		if( strFieldName.equals("EdorValiDate") ) {
			return 21;
		}
		if( strFieldName.equals("Prem") ) {
			return 22;
		}
		if( strFieldName.equals("GrpNo") ) {
			return 23;
		}
		if( strFieldName.equals("GrpName") ) {
			return 24;
		}
		if( strFieldName.equals("SocialCenterNo") ) {
			return 25;
		}
		if( strFieldName.equals("SocialCenterName") ) {
			return 26;
		}
		if( strFieldName.equals("AddDecFlag") ) {
			return 27;
		}
		if( strFieldName.equals("Operator") ) {
			return 28;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 29;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 30;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 31;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 32;
		}
		if( strFieldName.equals("FinishFlag") ) {
			return 33;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "GrpContNo";
				break;
			case 1:
				strFieldName = "InsuredID";
				break;
			case 2:
				strFieldName = "State";
				break;
			case 3:
				strFieldName = "ContNo";
				break;
			case 4:
				strFieldName = "BatchNo";
				break;
			case 5:
				strFieldName = "InsuredNo";
				break;
			case 6:
				strFieldName = "Retire";
				break;
			case 7:
				strFieldName = "EmployeeName";
				break;
			case 8:
				strFieldName = "InsuredName";
				break;
			case 9:
				strFieldName = "Relation";
				break;
			case 10:
				strFieldName = "Sex";
				break;
			case 11:
				strFieldName = "Birthday";
				break;
			case 12:
				strFieldName = "IDType";
				break;
			case 13:
				strFieldName = "IDNo";
				break;
			case 14:
				strFieldName = "ContPlanCode";
				break;
			case 15:
				strFieldName = "OccupationType";
				break;
			case 16:
				strFieldName = "BankCode";
				break;
			case 17:
				strFieldName = "BankAccNo";
				break;
			case 18:
				strFieldName = "AccName";
				break;
			case 19:
				strFieldName = "OthIDType";
				break;
			case 20:
				strFieldName = "OthIDNo";
				break;
			case 21:
				strFieldName = "EdorValiDate";
				break;
			case 22:
				strFieldName = "Prem";
				break;
			case 23:
				strFieldName = "GrpNo";
				break;
			case 24:
				strFieldName = "GrpName";
				break;
			case 25:
				strFieldName = "SocialCenterNo";
				break;
			case 26:
				strFieldName = "SocialCenterName";
				break;
			case 27:
				strFieldName = "AddDecFlag";
				break;
			case 28:
				strFieldName = "Operator";
				break;
			case 29:
				strFieldName = "MakeDate";
				break;
			case 30:
				strFieldName = "MakeTime";
				break;
			case 31:
				strFieldName = "ModifyDate";
				break;
			case 32:
				strFieldName = "ModifyTime";
				break;
			case 33:
				strFieldName = "FinishFlag";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("GrpContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuredID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BatchNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuredNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Retire") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EmployeeName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuredName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Relation") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Sex") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Birthday") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("IDType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IDNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContPlanCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OccupationType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankAccNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OthIDType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OthIDNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EdorValiDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Prem") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SocialCenterNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SocialCenterName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AddDecFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FinishFlag") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
