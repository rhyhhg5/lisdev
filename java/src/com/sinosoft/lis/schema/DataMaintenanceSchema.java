/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.DataMaintenanceDB;

/*
 * <p>ClassName: DataMaintenanceSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 数据运维
 * @CreateDate：2011-03-01
 */
public class DataMaintenanceSchema implements Schema, Cloneable
{
	// @Field
	/** 流水号 */
	private String SerialNo;
	/** 表名 */
	private String TableName;
	/** 字段名 */
	private String FieldName;
	/** 条件 */
	private String Condition;
	/** 数据量 */
	private String DataSize;
	/** 维护原因 */
	private String Reason;
	/** 上传路径 */
	private String Route;
	/** 提出日期 */
	private Date MakeDate;
	/** 提出维护人 */
	private String MaintenanceMan;
	/** 提取状态 */
	private String State;
	/** 回复日期 */
	private Date ReversionDate;
	/** Cq */
	private String cq;

	public static final int FIELDNUM = 12;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public DataMaintenanceSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "SerialNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		DataMaintenanceSchema cloned = (DataMaintenanceSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getSerialNo()
	{
		return SerialNo;
	}
	public void setSerialNo(String aSerialNo)
	{
		SerialNo = aSerialNo;
	}
	public String getTableName()
	{
		return TableName;
	}
	public void setTableName(String aTableName)
	{
		TableName = aTableName;
	}
	public String getFieldName()
	{
		return FieldName;
	}
	public void setFieldName(String aFieldName)
	{
		FieldName = aFieldName;
	}
	public String getCondition()
	{
		return Condition;
	}
	public void setCondition(String aCondition)
	{
		Condition = aCondition;
	}
	public String getDataSize()
	{
		return DataSize;
	}
	public void setDataSize(String aDataSize)
	{
		DataSize = aDataSize;
	}
	public String getReason()
	{
		return Reason;
	}
	public void setReason(String aReason)
	{
		Reason = aReason;
	}
	public String getRoute()
	{
		return Route;
	}
	public void setRoute(String aRoute)
	{
		Route = aRoute;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMaintenanceMan()
	{
		return MaintenanceMan;
	}
	public void setMaintenanceMan(String aMaintenanceMan)
	{
		MaintenanceMan = aMaintenanceMan;
	}
	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}
	public String getReversionDate()
	{
		if( ReversionDate != null )
			return fDate.getString(ReversionDate);
		else
			return null;
	}
	public void setReversionDate(Date aReversionDate)
	{
		ReversionDate = aReversionDate;
	}
	public void setReversionDate(String aReversionDate)
	{
		if (aReversionDate != null && !aReversionDate.equals("") )
		{
			ReversionDate = fDate.getDate( aReversionDate );
		}
		else
			ReversionDate = null;
	}

	public String getcq()
	{
		return cq;
	}
	public void setcq(String acq)
	{
		cq = acq;
	}

	/**
	* 使用另外一个 DataMaintenanceSchema 对象给 Schema 赋值
	* @param: aDataMaintenanceSchema DataMaintenanceSchema
	**/
	public void setSchema(DataMaintenanceSchema aDataMaintenanceSchema)
	{
		this.SerialNo = aDataMaintenanceSchema.getSerialNo();
		this.TableName = aDataMaintenanceSchema.getTableName();
		this.FieldName = aDataMaintenanceSchema.getFieldName();
		this.Condition = aDataMaintenanceSchema.getCondition();
		this.DataSize = aDataMaintenanceSchema.getDataSize();
		this.Reason = aDataMaintenanceSchema.getReason();
		this.Route = aDataMaintenanceSchema.getRoute();
		this.MakeDate = fDate.getDate( aDataMaintenanceSchema.getMakeDate());
		this.MaintenanceMan = aDataMaintenanceSchema.getMaintenanceMan();
		this.State = aDataMaintenanceSchema.getState();
		this.ReversionDate = fDate.getDate( aDataMaintenanceSchema.getReversionDate());
		this.cq = aDataMaintenanceSchema.getcq();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("SerialNo") == null )
				this.SerialNo = null;
			else
				this.SerialNo = rs.getString("SerialNo").trim();

			if( rs.getString("TableName") == null )
				this.TableName = null;
			else
				this.TableName = rs.getString("TableName").trim();

			if( rs.getString("FieldName") == null )
				this.FieldName = null;
			else
				this.FieldName = rs.getString("FieldName").trim();

			if( rs.getString("Condition") == null )
				this.Condition = null;
			else
				this.Condition = rs.getString("Condition").trim();

			if( rs.getString("DataSize") == null )
				this.DataSize = null;
			else
				this.DataSize = rs.getString("DataSize").trim();

			if( rs.getString("Reason") == null )
				this.Reason = null;
			else
				this.Reason = rs.getString("Reason").trim();

			if( rs.getString("Route") == null )
				this.Route = null;
			else
				this.Route = rs.getString("Route").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MaintenanceMan") == null )
				this.MaintenanceMan = null;
			else
				this.MaintenanceMan = rs.getString("MaintenanceMan").trim();

			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			this.ReversionDate = rs.getDate("ReversionDate");
			if( rs.getString("cq") == null )
				this.cq = null;
			else
				this.cq = rs.getString("cq").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的DataMaintenance表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "DataMaintenanceSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public DataMaintenanceSchema getSchema()
	{
		DataMaintenanceSchema aDataMaintenanceSchema = new DataMaintenanceSchema();
		aDataMaintenanceSchema.setSchema(this);
		return aDataMaintenanceSchema;
	}

	public DataMaintenanceDB getDB()
	{
		DataMaintenanceDB aDBOper = new DataMaintenanceDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpDataMaintenance描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(SerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TableName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FieldName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Condition)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DataSize)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Reason)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Route)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MaintenanceMan)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ReversionDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(cq));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpDataMaintenance>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			TableName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			FieldName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			Condition = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			DataSize = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			Reason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			Route = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,SysConst.PACKAGESPILTER));
			MaintenanceMan = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			ReversionDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
			cq = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "DataMaintenanceSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("SerialNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
		}
		if (FCode.equals("TableName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TableName));
		}
		if (FCode.equals("FieldName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FieldName));
		}
		if (FCode.equals("Condition"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Condition));
		}
		if (FCode.equals("DataSize"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DataSize));
		}
		if (FCode.equals("Reason"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Reason));
		}
		if (FCode.equals("Route"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Route));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MaintenanceMan"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MaintenanceMan));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("ReversionDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getReversionDate()));
		}
		if (FCode.equals("cq"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(cq));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(SerialNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(TableName);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(FieldName);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(Condition);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(DataSize);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(Reason);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(Route);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(MaintenanceMan);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getReversionDate()));
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(cq);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("SerialNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SerialNo = FValue.trim();
			}
			else
				SerialNo = null;
		}
		if (FCode.equalsIgnoreCase("TableName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TableName = FValue.trim();
			}
			else
				TableName = null;
		}
		if (FCode.equalsIgnoreCase("FieldName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FieldName = FValue.trim();
			}
			else
				FieldName = null;
		}
		if (FCode.equalsIgnoreCase("Condition"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Condition = FValue.trim();
			}
			else
				Condition = null;
		}
		if (FCode.equalsIgnoreCase("DataSize"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DataSize = FValue.trim();
			}
			else
				DataSize = null;
		}
		if (FCode.equalsIgnoreCase("Reason"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Reason = FValue.trim();
			}
			else
				Reason = null;
		}
		if (FCode.equalsIgnoreCase("Route"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Route = FValue.trim();
			}
			else
				Route = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MaintenanceMan"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MaintenanceMan = FValue.trim();
			}
			else
				MaintenanceMan = null;
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("ReversionDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ReversionDate = fDate.getDate( FValue );
			}
			else
				ReversionDate = null;
		}
		if (FCode.equalsIgnoreCase("cq"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				cq = FValue.trim();
			}
			else
				cq = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		DataMaintenanceSchema other = (DataMaintenanceSchema)otherObject;
		return
			(SerialNo == null ? other.getSerialNo() == null : SerialNo.equals(other.getSerialNo()))
			&& (TableName == null ? other.getTableName() == null : TableName.equals(other.getTableName()))
			&& (FieldName == null ? other.getFieldName() == null : FieldName.equals(other.getFieldName()))
			&& (Condition == null ? other.getCondition() == null : Condition.equals(other.getCondition()))
			&& (DataSize == null ? other.getDataSize() == null : DataSize.equals(other.getDataSize()))
			&& (Reason == null ? other.getReason() == null : Reason.equals(other.getReason()))
			&& (Route == null ? other.getRoute() == null : Route.equals(other.getRoute()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MaintenanceMan == null ? other.getMaintenanceMan() == null : MaintenanceMan.equals(other.getMaintenanceMan()))
			&& (State == null ? other.getState() == null : State.equals(other.getState()))
			&& (ReversionDate == null ? other.getReversionDate() == null : fDate.getString(ReversionDate).equals(other.getReversionDate()))
			&& (cq == null ? other.getcq() == null : cq.equals(other.getcq()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("SerialNo") ) {
			return 0;
		}
		if( strFieldName.equals("TableName") ) {
			return 1;
		}
		if( strFieldName.equals("FieldName") ) {
			return 2;
		}
		if( strFieldName.equals("Condition") ) {
			return 3;
		}
		if( strFieldName.equals("DataSize") ) {
			return 4;
		}
		if( strFieldName.equals("Reason") ) {
			return 5;
		}
		if( strFieldName.equals("Route") ) {
			return 6;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 7;
		}
		if( strFieldName.equals("MaintenanceMan") ) {
			return 8;
		}
		if( strFieldName.equals("State") ) {
			return 9;
		}
		if( strFieldName.equals("ReversionDate") ) {
			return 10;
		}
		if( strFieldName.equals("cq") ) {
			return 11;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "SerialNo";
				break;
			case 1:
				strFieldName = "TableName";
				break;
			case 2:
				strFieldName = "FieldName";
				break;
			case 3:
				strFieldName = "Condition";
				break;
			case 4:
				strFieldName = "DataSize";
				break;
			case 5:
				strFieldName = "Reason";
				break;
			case 6:
				strFieldName = "Route";
				break;
			case 7:
				strFieldName = "MakeDate";
				break;
			case 8:
				strFieldName = "MaintenanceMan";
				break;
			case 9:
				strFieldName = "State";
				break;
			case 10:
				strFieldName = "ReversionDate";
				break;
			case 11:
				strFieldName = "cq";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("SerialNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TableName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FieldName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Condition") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DataSize") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Reason") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Route") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MaintenanceMan") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReversionDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("cq") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
