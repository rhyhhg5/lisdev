/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LAAnnuityDB;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LAAnnuitySchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 新表结构
 * @CreateDate：2005-07-06
 */
public class LAAnnuitySchema implements Schema, Cloneable
{
    // @Field
    /** 养老金序号 */
    private String SeriesNo;
    /** 代理人编码 */
    private String AgentCode;
    /** 代理人展业机构代码 */
    private String AgentGroup;
    /** 管理机构 */
    private String ManageCom;
    /** 离司时间 */
    private Date OutWorkDate;
    /** 实付时间 */
    private Date PayDate;
    /** 领取标记 */
    private String DrawMarker;
    /** 养老金合计 */
    private double SumAnnutity;
    /** 应付养老金金额 */
    private double DealAnnuity;
    /** 领取养老金金额 */
    private double DrawAnnuity;
    /** 当月养老金提取基数 */
    private double BaseAnnuity;
    /** 养老金提取比例 */
    private double ScaleAnnuity;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 17; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LAAnnuitySchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "SeriesNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException
    {
        LAAnnuitySchema cloned = (LAAnnuitySchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getSeriesNo()
    {
        return SeriesNo;
    }

    public void setSeriesNo(String aSeriesNo)
    {
        SeriesNo = aSeriesNo;
    }

    public String getAgentCode()
    {
        return AgentCode;
    }

    public void setAgentCode(String aAgentCode)
    {
        AgentCode = aAgentCode;
    }

    public String getAgentGroup()
    {
        return AgentGroup;
    }

    public void setAgentGroup(String aAgentGroup)
    {
        AgentGroup = aAgentGroup;
    }

    public String getManageCom()
    {
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public String getOutWorkDate()
    {
        if (OutWorkDate != null)
        {
            return fDate.getString(OutWorkDate);
        }
        else
        {
            return null;
        }
    }

    public void setOutWorkDate(Date aOutWorkDate)
    {
        OutWorkDate = aOutWorkDate;
    }

    public void setOutWorkDate(String aOutWorkDate)
    {
        if (aOutWorkDate != null && !aOutWorkDate.equals(""))
        {
            OutWorkDate = fDate.getDate(aOutWorkDate);
        }
        else
        {
            OutWorkDate = null;
        }
    }

    public String getPayDate()
    {
        if (PayDate != null)
        {
            return fDate.getString(PayDate);
        }
        else
        {
            return null;
        }
    }

    public void setPayDate(Date aPayDate)
    {
        PayDate = aPayDate;
    }

    public void setPayDate(String aPayDate)
    {
        if (aPayDate != null && !aPayDate.equals(""))
        {
            PayDate = fDate.getDate(aPayDate);
        }
        else
        {
            PayDate = null;
        }
    }

    public String getDrawMarker()
    {
        return DrawMarker;
    }

    public void setDrawMarker(String aDrawMarker)
    {
        DrawMarker = aDrawMarker;
    }

    public double getSumAnnutity()
    {
        return SumAnnutity;
    }

    public void setSumAnnutity(double aSumAnnutity)
    {
        SumAnnutity = Arith.round(aSumAnnutity, 2);
    }

    public void setSumAnnutity(String aSumAnnutity)
    {
        if (aSumAnnutity != null && !aSumAnnutity.equals(""))
        {
            Double tDouble = new Double(aSumAnnutity);
            double d = tDouble.doubleValue();
            SumAnnutity = Arith.round(d, 2);
        }
    }

    public double getDealAnnuity()
    {
        return DealAnnuity;
    }

    public void setDealAnnuity(double aDealAnnuity)
    {
        DealAnnuity = Arith.round(aDealAnnuity, 2);
    }

    public void setDealAnnuity(String aDealAnnuity)
    {
        if (aDealAnnuity != null && !aDealAnnuity.equals(""))
        {
            Double tDouble = new Double(aDealAnnuity);
            double d = tDouble.doubleValue();
            DealAnnuity = Arith.round(d, 2);
        }
    }

    public double getDrawAnnuity()
    {
        return DrawAnnuity;
    }

    public void setDrawAnnuity(double aDrawAnnuity)
    {
        DrawAnnuity = Arith.round(aDrawAnnuity, 2);
    }

    public void setDrawAnnuity(String aDrawAnnuity)
    {
        if (aDrawAnnuity != null && !aDrawAnnuity.equals(""))
        {
            Double tDouble = new Double(aDrawAnnuity);
            double d = tDouble.doubleValue();
            DrawAnnuity = Arith.round(d, 2);
        }
    }

    public double getBaseAnnuity()
    {
        return BaseAnnuity;
    }

    public void setBaseAnnuity(double aBaseAnnuity)
    {
        BaseAnnuity = Arith.round(aBaseAnnuity, 2);
    }

    public void setBaseAnnuity(String aBaseAnnuity)
    {
        if (aBaseAnnuity != null && !aBaseAnnuity.equals(""))
        {
            Double tDouble = new Double(aBaseAnnuity);
            double d = tDouble.doubleValue();
            BaseAnnuity = Arith.round(d, 2);
        }
    }

    public double getScaleAnnuity()
    {
        return ScaleAnnuity;
    }

    public void setScaleAnnuity(double aScaleAnnuity)
    {
        ScaleAnnuity = Arith.round(aScaleAnnuity, 2);
    }

    public void setScaleAnnuity(String aScaleAnnuity)
    {
        if (aScaleAnnuity != null && !aScaleAnnuity.equals(""))
        {
            Double tDouble = new Double(aScaleAnnuity);
            double d = tDouble.doubleValue();
            ScaleAnnuity = Arith.round(d, 2);
        }
    }

    public String getOperator()
    {
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LAAnnuitySchema 对象给 Schema 赋值
     * @param: aLAAnnuitySchema LAAnnuitySchema
     **/
    public void setSchema(LAAnnuitySchema aLAAnnuitySchema)
    {
        this.SeriesNo = aLAAnnuitySchema.getSeriesNo();
        this.AgentCode = aLAAnnuitySchema.getAgentCode();
        this.AgentGroup = aLAAnnuitySchema.getAgentGroup();
        this.ManageCom = aLAAnnuitySchema.getManageCom();
        this.OutWorkDate = fDate.getDate(aLAAnnuitySchema.getOutWorkDate());
        this.PayDate = fDate.getDate(aLAAnnuitySchema.getPayDate());
        this.DrawMarker = aLAAnnuitySchema.getDrawMarker();
        this.SumAnnutity = aLAAnnuitySchema.getSumAnnutity();
        this.DealAnnuity = aLAAnnuitySchema.getDealAnnuity();
        this.DrawAnnuity = aLAAnnuitySchema.getDrawAnnuity();
        this.BaseAnnuity = aLAAnnuitySchema.getBaseAnnuity();
        this.ScaleAnnuity = aLAAnnuitySchema.getScaleAnnuity();
        this.Operator = aLAAnnuitySchema.getOperator();
        this.MakeDate = fDate.getDate(aLAAnnuitySchema.getMakeDate());
        this.MakeTime = aLAAnnuitySchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLAAnnuitySchema.getModifyDate());
        this.ModifyTime = aLAAnnuitySchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("SeriesNo") == null)
            {
                this.SeriesNo = null;
            }
            else
            {
                this.SeriesNo = rs.getString("SeriesNo").trim();
            }

            if (rs.getString("AgentCode") == null)
            {
                this.AgentCode = null;
            }
            else
            {
                this.AgentCode = rs.getString("AgentCode").trim();
            }

            if (rs.getString("AgentGroup") == null)
            {
                this.AgentGroup = null;
            }
            else
            {
                this.AgentGroup = rs.getString("AgentGroup").trim();
            }

            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            this.OutWorkDate = rs.getDate("OutWorkDate");
            this.PayDate = rs.getDate("PayDate");
            if (rs.getString("DrawMarker") == null)
            {
                this.DrawMarker = null;
            }
            else
            {
                this.DrawMarker = rs.getString("DrawMarker").trim();
            }

            this.SumAnnutity = rs.getDouble("SumAnnutity");
            this.DealAnnuity = rs.getDouble("DealAnnuity");
            this.DrawAnnuity = rs.getDouble("DrawAnnuity");
            this.BaseAnnuity = rs.getDouble("BaseAnnuity");
            this.ScaleAnnuity = rs.getDouble("ScaleAnnuity");
            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            System.out.println("数据库中的LAAnnuity表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAnnuitySchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LAAnnuitySchema getSchema()
    {
        LAAnnuitySchema aLAAnnuitySchema = new LAAnnuitySchema();
        aLAAnnuitySchema.setSchema(this);
        return aLAAnnuitySchema;
    }

    public LAAnnuityDB getDB()
    {
        LAAnnuityDB aDBOper = new LAAnnuityDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAAnnuity描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(SeriesNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentGroup));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(OutWorkDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(PayDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DrawMarker));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SumAnnutity));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DealAnnuity));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DrawAnnuity));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(BaseAnnuity));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(ScaleAnnuity));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAAnnuity>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            SeriesNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                       SysConst.PACKAGESPILTER);
            AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                        SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                       SysConst.PACKAGESPILTER);
            OutWorkDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 5, SysConst.PACKAGESPILTER));
            PayDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 6, SysConst.PACKAGESPILTER));
            DrawMarker = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                        SysConst.PACKAGESPILTER);
            SumAnnutity = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 8, SysConst.PACKAGESPILTER))).doubleValue();
            DealAnnuity = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 9, SysConst.PACKAGESPILTER))).doubleValue();
            DrawAnnuity = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 10, SysConst.PACKAGESPILTER))).doubleValue();
            BaseAnnuity = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 11, SysConst.PACKAGESPILTER))).doubleValue();
            ScaleAnnuity = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 12, SysConst.PACKAGESPILTER))).doubleValue();
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 14, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 16, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAnnuitySchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("SeriesNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SeriesNo));
        }
        if (FCode.equals("AgentCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equals("AgentGroup"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
        }
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("OutWorkDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getOutWorkDate()));
        }
        if (FCode.equals("PayDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getPayDate()));
        }
        if (FCode.equals("DrawMarker"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DrawMarker));
        }
        if (FCode.equals("SumAnnutity"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumAnnutity));
        }
        if (FCode.equals("DealAnnuity"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DealAnnuity));
        }
        if (FCode.equals("DrawAnnuity"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DrawAnnuity));
        }
        if (FCode.equals("BaseAnnuity"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BaseAnnuity));
        }
        if (FCode.equals("ScaleAnnuity"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ScaleAnnuity));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(SeriesNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(AgentCode);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(AgentGroup);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getOutWorkDate()));
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getPayDate()));
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(DrawMarker);
                break;
            case 7:
                strFieldValue = String.valueOf(SumAnnutity);
                break;
            case 8:
                strFieldValue = String.valueOf(DealAnnuity);
                break;
            case 9:
                strFieldValue = String.valueOf(DrawAnnuity);
                break;
            case 10:
                strFieldValue = String.valueOf(BaseAnnuity);
                break;
            case 11:
                strFieldValue = String.valueOf(ScaleAnnuity);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("SeriesNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SeriesNo = FValue.trim();
            }
            else
            {
                SeriesNo = null;
            }
        }
        if (FCode.equals("AgentCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
            {
                AgentCode = null;
            }
        }
        if (FCode.equals("AgentGroup"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
            {
                AgentGroup = null;
            }
        }
        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("OutWorkDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OutWorkDate = fDate.getDate(FValue);
            }
            else
            {
                OutWorkDate = null;
            }
        }
        if (FCode.equals("PayDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PayDate = fDate.getDate(FValue);
            }
            else
            {
                PayDate = null;
            }
        }
        if (FCode.equals("DrawMarker"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DrawMarker = FValue.trim();
            }
            else
            {
                DrawMarker = null;
            }
        }
        if (FCode.equals("SumAnnutity"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                SumAnnutity = d;
            }
        }
        if (FCode.equals("DealAnnuity"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                DealAnnuity = d;
            }
        }
        if (FCode.equals("DrawAnnuity"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                DrawAnnuity = d;
            }
        }
        if (FCode.equals("BaseAnnuity"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                BaseAnnuity = d;
            }
        }
        if (FCode.equals("ScaleAnnuity"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                ScaleAnnuity = d;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LAAnnuitySchema other = (LAAnnuitySchema) otherObject;
        return
                SeriesNo.equals(other.getSeriesNo())
                && AgentCode.equals(other.getAgentCode())
                && AgentGroup.equals(other.getAgentGroup())
                && ManageCom.equals(other.getManageCom())
                && fDate.getString(OutWorkDate).equals(other.getOutWorkDate())
                && fDate.getString(PayDate).equals(other.getPayDate())
                && DrawMarker.equals(other.getDrawMarker())
                && SumAnnutity == other.getSumAnnutity()
                && DealAnnuity == other.getDealAnnuity()
                && DrawAnnuity == other.getDrawAnnuity()
                && BaseAnnuity == other.getBaseAnnuity()
                && ScaleAnnuity == other.getScaleAnnuity()
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("SeriesNo"))
        {
            return 0;
        }
        if (strFieldName.equals("AgentCode"))
        {
            return 1;
        }
        if (strFieldName.equals("AgentGroup"))
        {
            return 2;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return 3;
        }
        if (strFieldName.equals("OutWorkDate"))
        {
            return 4;
        }
        if (strFieldName.equals("PayDate"))
        {
            return 5;
        }
        if (strFieldName.equals("DrawMarker"))
        {
            return 6;
        }
        if (strFieldName.equals("SumAnnutity"))
        {
            return 7;
        }
        if (strFieldName.equals("DealAnnuity"))
        {
            return 8;
        }
        if (strFieldName.equals("DrawAnnuity"))
        {
            return 9;
        }
        if (strFieldName.equals("BaseAnnuity"))
        {
            return 10;
        }
        if (strFieldName.equals("ScaleAnnuity"))
        {
            return 11;
        }
        if (strFieldName.equals("Operator"))
        {
            return 12;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 13;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 14;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 15;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 16;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "SeriesNo";
                break;
            case 1:
                strFieldName = "AgentCode";
                break;
            case 2:
                strFieldName = "AgentGroup";
                break;
            case 3:
                strFieldName = "ManageCom";
                break;
            case 4:
                strFieldName = "OutWorkDate";
                break;
            case 5:
                strFieldName = "PayDate";
                break;
            case 6:
                strFieldName = "DrawMarker";
                break;
            case 7:
                strFieldName = "SumAnnutity";
                break;
            case 8:
                strFieldName = "DealAnnuity";
                break;
            case 9:
                strFieldName = "DrawAnnuity";
                break;
            case 10:
                strFieldName = "BaseAnnuity";
                break;
            case 11:
                strFieldName = "ScaleAnnuity";
                break;
            case 12:
                strFieldName = "Operator";
                break;
            case 13:
                strFieldName = "MakeDate";
                break;
            case 14:
                strFieldName = "MakeTime";
                break;
            case 15:
                strFieldName = "ModifyDate";
                break;
            case 16:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("SeriesNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentGroup"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OutWorkDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("PayDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("DrawMarker"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SumAnnutity"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("DealAnnuity"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("DrawAnnuity"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("BaseAnnuity"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("ScaleAnnuity"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 5:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 8:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 9:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 10:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 11:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
