/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LAInterErrLogDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LAInterErrLogSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-02-28
 */
public class LAInterErrLogSchema implements Schema
{
    // @Field
    /** 错误流水号 */
    private String ErrSN;
    /** 管理机构 */
    private String MgCom;
    /** 展业类型 */
    private String BrchType;
    /** 集体保单号码 */
    private String GrpPolNo;
    /** 错误信息 */
    private String ErrMsg;
    /** 计算日期 */
    private int StartDate;
    /** 操作人员 */
    private String Operator;
    /** 入机日期 */
    private int MakeDate;
    /** 入机时间 */
    private String MakeTime;

    public static final int FIELDNUM = 9; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LAInterErrLogSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "ErrSN";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getErrSN()
    {
        if (ErrSN != null && !ErrSN.equals("") && SysConst.CHANGECHARSET)
        {
            ErrSN = StrTool.unicodeToGBK(ErrSN);
        }
        return ErrSN;
    }

    public void setErrSN(String aErrSN)
    {
        ErrSN = aErrSN;
    }

    public String getMgCom()
    {
        if (MgCom != null && !MgCom.equals("") && SysConst.CHANGECHARSET)
        {
            MgCom = StrTool.unicodeToGBK(MgCom);
        }
        return MgCom;
    }

    public void setMgCom(String aMgCom)
    {
        MgCom = aMgCom;
    }

    public String getBrchType()
    {
        if (BrchType != null && !BrchType.equals("") && SysConst.CHANGECHARSET)
        {
            BrchType = StrTool.unicodeToGBK(BrchType);
        }
        return BrchType;
    }

    public void setBrchType(String aBrchType)
    {
        BrchType = aBrchType;
    }

    public String getGrpPolNo()
    {
        if (GrpPolNo != null && !GrpPolNo.equals("") && SysConst.CHANGECHARSET)
        {
            GrpPolNo = StrTool.unicodeToGBK(GrpPolNo);
        }
        return GrpPolNo;
    }

    public void setGrpPolNo(String aGrpPolNo)
    {
        GrpPolNo = aGrpPolNo;
    }

    public String getErrMsg()
    {
        if (ErrMsg != null && !ErrMsg.equals("") && SysConst.CHANGECHARSET)
        {
            ErrMsg = StrTool.unicodeToGBK(ErrMsg);
        }
        return ErrMsg;
    }

    public void setErrMsg(String aErrMsg)
    {
        ErrMsg = aErrMsg;
    }

    public int getStartDate()
    {
        return StartDate;
    }

    public void setStartDate(int aStartDate)
    {
        StartDate = aStartDate;
    }

    public void setStartDate(String aStartDate)
    {
        if (aStartDate != null && !aStartDate.equals(""))
        {
            Integer tInteger = new Integer(aStartDate);
            int i = tInteger.intValue();
            StartDate = i;
        }
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public int getMakeDate()
    {
        return MakeDate;
    }

    public void setMakeDate(int aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            Integer tInteger = new Integer(aMakeDate);
            int i = tInteger.intValue();
            MakeDate = i;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    /**
     * 使用另外一个 LAInterErrLogSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LAInterErrLogSchema aLAInterErrLogSchema)
    {
        this.ErrSN = aLAInterErrLogSchema.getErrSN();
        this.MgCom = aLAInterErrLogSchema.getMgCom();
        this.BrchType = aLAInterErrLogSchema.getBrchType();
        this.GrpPolNo = aLAInterErrLogSchema.getGrpPolNo();
        this.ErrMsg = aLAInterErrLogSchema.getErrMsg();
        this.StartDate = aLAInterErrLogSchema.getStartDate();
        this.Operator = aLAInterErrLogSchema.getOperator();
        this.MakeDate = aLAInterErrLogSchema.getMakeDate();
        this.MakeTime = aLAInterErrLogSchema.getMakeTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ErrSN") == null)
            {
                this.ErrSN = null;
            }
            else
            {
                this.ErrSN = rs.getString("ErrSN").trim();
            }

            if (rs.getString("MgCom") == null)
            {
                this.MgCom = null;
            }
            else
            {
                this.MgCom = rs.getString("MgCom").trim();
            }

            if (rs.getString("BrchType") == null)
            {
                this.BrchType = null;
            }
            else
            {
                this.BrchType = rs.getString("BrchType").trim();
            }

            if (rs.getString("GrpPolNo") == null)
            {
                this.GrpPolNo = null;
            }
            else
            {
                this.GrpPolNo = rs.getString("GrpPolNo").trim();
            }

            if (rs.getString("ErrMsg") == null)
            {
                this.ErrMsg = null;
            }
            else
            {
                this.ErrMsg = rs.getString("ErrMsg").trim();
            }

            this.StartDate = rs.getInt("StartDate");
            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getInt("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAInterErrLogSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LAInterErrLogSchema getSchema()
    {
        LAInterErrLogSchema aLAInterErrLogSchema = new LAInterErrLogSchema();
        aLAInterErrLogSchema.setSchema(this);
        return aLAInterErrLogSchema;
    }

    public LAInterErrLogDB getDB()
    {
        LAInterErrLogDB aDBOper = new LAInterErrLogDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAInterErrLog描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(ErrSN)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MgCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(BrchType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GrpPolNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ErrMsg)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(StartDate) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(MakeDate) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAInterErrLog>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            ErrSN = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                   SysConst.PACKAGESPILTER);
            MgCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                   SysConst.PACKAGESPILTER);
            BrchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            GrpPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                      SysConst.PACKAGESPILTER);
            ErrMsg = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                    SysConst.PACKAGESPILTER);
            StartDate = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 6, SysConst.PACKAGESPILTER))).intValue();
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                      SysConst.PACKAGESPILTER);
            MakeDate = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 8, SysConst.PACKAGESPILTER))).intValue();
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                      SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAInterErrLogSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("ErrSN"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ErrSN));
        }
        if (FCode.equals("MgCom"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MgCom));
        }
        if (FCode.equals("BrchType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BrchType));
        }
        if (FCode.equals("GrpPolNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpPolNo));
        }
        if (FCode.equals("ErrMsg"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ErrMsg));
        }
        if (FCode.equals("StartDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StartDate));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ErrSN);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(MgCom);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(BrchType);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(GrpPolNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(ErrMsg);
                break;
            case 5:
                strFieldValue = String.valueOf(StartDate);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 7:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("ErrSN"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ErrSN = FValue.trim();
            }
            else
            {
                ErrSN = null;
            }
        }
        if (FCode.equals("MgCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MgCom = FValue.trim();
            }
            else
            {
                MgCom = null;
            }
        }
        if (FCode.equals("BrchType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BrchType = FValue.trim();
            }
            else
            {
                BrchType = null;
            }
        }
        if (FCode.equals("GrpPolNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpPolNo = FValue.trim();
            }
            else
            {
                GrpPolNo = null;
            }
        }
        if (FCode.equals("ErrMsg"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ErrMsg = FValue.trim();
            }
            else
            {
                ErrMsg = null;
            }
        }
        if (FCode.equals("StartDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                StartDate = i;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                MakeDate = i;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LAInterErrLogSchema other = (LAInterErrLogSchema) otherObject;
        return
                ErrSN.equals(other.getErrSN())
                && MgCom.equals(other.getMgCom())
                && BrchType.equals(other.getBrchType())
                && GrpPolNo.equals(other.getGrpPolNo())
                && ErrMsg.equals(other.getErrMsg())
                && StartDate == other.getStartDate()
                && Operator.equals(other.getOperator())
                && MakeDate == other.getMakeDate()
                && MakeTime.equals(other.getMakeTime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("ErrSN"))
        {
            return 0;
        }
        if (strFieldName.equals("MgCom"))
        {
            return 1;
        }
        if (strFieldName.equals("BrchType"))
        {
            return 2;
        }
        if (strFieldName.equals("GrpPolNo"))
        {
            return 3;
        }
        if (strFieldName.equals("ErrMsg"))
        {
            return 4;
        }
        if (strFieldName.equals("StartDate"))
        {
            return 5;
        }
        if (strFieldName.equals("Operator"))
        {
            return 6;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 7;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 8;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "ErrSN";
                break;
            case 1:
                strFieldName = "MgCom";
                break;
            case 2:
                strFieldName = "BrchType";
                break;
            case 3:
                strFieldName = "GrpPolNo";
                break;
            case 4:
                strFieldName = "ErrMsg";
                break;
            case 5:
                strFieldName = "StartDate";
                break;
            case 6:
                strFieldName = "Operator";
                break;
            case 7:
                strFieldName = "MakeDate";
                break;
            case 8:
                strFieldName = "MakeTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("ErrSN"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MgCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BrchType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpPolNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ErrMsg"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StartDate"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_INT;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_INT;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
