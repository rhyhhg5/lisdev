/*
 * <p>ClassName: LWProcessInstanceSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 工作流模型
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LWProcessInstanceDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LWProcessInstanceSchema implements Schema
{
    // @Field
    /** 转移id */
    private String TransitionID;
    /** 过程id */
    private String ProcessID;
    /** 转移起点 */
    private String TransitionStart;
    /** 转移终点 */
    private String TransitionEnd;
    /** 转移条件 */
    private String TransitionCond;
    /** 转移条件类型 */
    private String TransitionCondT;
    /** 转移时方式 */
    private String TransitionModel;
    /** 起点类型 */
    private String StartType;

    public static final int FIELDNUM = 8; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LWProcessInstanceSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "TransitionID";
        pk[1] = "ProcessID";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getTransitionID()
    {
        if (TransitionID != null && !TransitionID.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            TransitionID = StrTool.unicodeToGBK(TransitionID);
        }
        return TransitionID;
    }

    public void setTransitionID(String aTransitionID)
    {
        TransitionID = aTransitionID;
    }

    public String getProcessID()
    {
        if (ProcessID != null && !ProcessID.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ProcessID = StrTool.unicodeToGBK(ProcessID);
        }
        return ProcessID;
    }

    public void setProcessID(String aProcessID)
    {
        ProcessID = aProcessID;
    }

    public String getTransitionStart()
    {
        if (TransitionStart != null && !TransitionStart.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            TransitionStart = StrTool.unicodeToGBK(TransitionStart);
        }
        return TransitionStart;
    }

    public void setTransitionStart(String aTransitionStart)
    {
        TransitionStart = aTransitionStart;
    }

    public String getTransitionEnd()
    {
        if (TransitionEnd != null && !TransitionEnd.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            TransitionEnd = StrTool.unicodeToGBK(TransitionEnd);
        }
        return TransitionEnd;
    }

    public void setTransitionEnd(String aTransitionEnd)
    {
        TransitionEnd = aTransitionEnd;
    }

    public String getTransitionCond()
    {
        if (TransitionCond != null && !TransitionCond.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            TransitionCond = StrTool.unicodeToGBK(TransitionCond);
        }
        return TransitionCond;
    }

    public void setTransitionCond(String aTransitionCond)
    {
        TransitionCond = aTransitionCond;
    }

    public String getTransitionCondT()
    {
        if (TransitionCondT != null && !TransitionCondT.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            TransitionCondT = StrTool.unicodeToGBK(TransitionCondT);
        }
        return TransitionCondT;
    }

    public void setTransitionCondT(String aTransitionCondT)
    {
        TransitionCondT = aTransitionCondT;
    }

    public String getTransitionModel()
    {
        if (TransitionModel != null && !TransitionModel.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            TransitionModel = StrTool.unicodeToGBK(TransitionModel);
        }
        return TransitionModel;
    }

    public void setTransitionModel(String aTransitionModel)
    {
        TransitionModel = aTransitionModel;
    }

    public String getStartType()
    {
        if (StartType != null && !StartType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            StartType = StrTool.unicodeToGBK(StartType);
        }
        return StartType;
    }

    public void setStartType(String aStartType)
    {
        StartType = aStartType;
    }

    /**
     * 使用另外一个 LWProcessInstanceSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LWProcessInstanceSchema aLWProcessInstanceSchema)
    {
        this.TransitionID = aLWProcessInstanceSchema.getTransitionID();
        this.ProcessID = aLWProcessInstanceSchema.getProcessID();
        this.TransitionStart = aLWProcessInstanceSchema.getTransitionStart();
        this.TransitionEnd = aLWProcessInstanceSchema.getTransitionEnd();
        this.TransitionCond = aLWProcessInstanceSchema.getTransitionCond();
        this.TransitionCondT = aLWProcessInstanceSchema.getTransitionCondT();
        this.TransitionModel = aLWProcessInstanceSchema.getTransitionModel();
        this.StartType = aLWProcessInstanceSchema.getStartType();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("TransitionID") == null)
            {
                this.TransitionID = null;
            }
            else
            {
                this.TransitionID = rs.getString("TransitionID").trim();
            }

            if (rs.getString("ProcessID") == null)
            {
                this.ProcessID = null;
            }
            else
            {
                this.ProcessID = rs.getString("ProcessID").trim();
            }

            if (rs.getString("TransitionStart") == null)
            {
                this.TransitionStart = null;
            }
            else
            {
                this.TransitionStart = rs.getString("TransitionStart").trim();
            }

            if (rs.getString("TransitionEnd") == null)
            {
                this.TransitionEnd = null;
            }
            else
            {
                this.TransitionEnd = rs.getString("TransitionEnd").trim();
            }

            if (rs.getString("TransitionCond") == null)
            {
                this.TransitionCond = null;
            }
            else
            {
                this.TransitionCond = rs.getString("TransitionCond").trim();
            }

            if (rs.getString("TransitionCondT") == null)
            {
                this.TransitionCondT = null;
            }
            else
            {
                this.TransitionCondT = rs.getString("TransitionCondT").trim();
            }

            if (rs.getString("TransitionModel") == null)
            {
                this.TransitionModel = null;
            }
            else
            {
                this.TransitionModel = rs.getString("TransitionModel").trim();
            }

            if (rs.getString("StartType") == null)
            {
                this.StartType = null;
            }
            else
            {
                this.StartType = rs.getString("StartType").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LWProcessInstanceSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LWProcessInstanceSchema getSchema()
    {
        LWProcessInstanceSchema aLWProcessInstanceSchema = new
                LWProcessInstanceSchema();
        aLWProcessInstanceSchema.setSchema(this);
        return aLWProcessInstanceSchema;
    }

    public LWProcessInstanceDB getDB()
    {
        LWProcessInstanceDB aDBOper = new LWProcessInstanceDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLWProcessInstance描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(TransitionID)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ProcessID)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(TransitionStart)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(TransitionEnd)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(TransitionCond)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(TransitionCondT)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(TransitionModel)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(StartType));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLWProcessInstance>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            TransitionID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                          SysConst.PACKAGESPILTER);
            ProcessID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                       SysConst.PACKAGESPILTER);
            TransitionStart = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             3, SysConst.PACKAGESPILTER);
            TransitionEnd = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                           SysConst.PACKAGESPILTER);
            TransitionCond = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                            SysConst.PACKAGESPILTER);
            TransitionCondT = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             6, SysConst.PACKAGESPILTER);
            TransitionModel = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             7, SysConst.PACKAGESPILTER);
            StartType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                       SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LWProcessInstanceSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("TransitionID"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(TransitionID));
        }
        if (FCode.equals("ProcessID"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ProcessID));
        }
        if (FCode.equals("TransitionStart"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(TransitionStart));
        }
        if (FCode.equals("TransitionEnd"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(TransitionEnd));
        }
        if (FCode.equals("TransitionCond"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(TransitionCond));
        }
        if (FCode.equals("TransitionCondT"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(TransitionCondT));
        }
        if (FCode.equals("TransitionModel"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(TransitionModel));
        }
        if (FCode.equals("StartType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(StartType));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(TransitionID);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ProcessID);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(TransitionStart);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(TransitionEnd);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(TransitionCond);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(TransitionCondT);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(TransitionModel);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(StartType);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("TransitionID"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                TransitionID = FValue.trim();
            }
            else
            {
                TransitionID = null;
            }
        }
        if (FCode.equals("ProcessID"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ProcessID = FValue.trim();
            }
            else
            {
                ProcessID = null;
            }
        }
        if (FCode.equals("TransitionStart"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                TransitionStart = FValue.trim();
            }
            else
            {
                TransitionStart = null;
            }
        }
        if (FCode.equals("TransitionEnd"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                TransitionEnd = FValue.trim();
            }
            else
            {
                TransitionEnd = null;
            }
        }
        if (FCode.equals("TransitionCond"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                TransitionCond = FValue.trim();
            }
            else
            {
                TransitionCond = null;
            }
        }
        if (FCode.equals("TransitionCondT"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                TransitionCondT = FValue.trim();
            }
            else
            {
                TransitionCondT = null;
            }
        }
        if (FCode.equals("TransitionModel"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                TransitionModel = FValue.trim();
            }
            else
            {
                TransitionModel = null;
            }
        }
        if (FCode.equals("StartType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                StartType = FValue.trim();
            }
            else
            {
                StartType = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LWProcessInstanceSchema other = (LWProcessInstanceSchema) otherObject;
        return
                TransitionID.equals(other.getTransitionID())
                && ProcessID.equals(other.getProcessID())
                && TransitionStart.equals(other.getTransitionStart())
                && TransitionEnd.equals(other.getTransitionEnd())
                && TransitionCond.equals(other.getTransitionCond())
                && TransitionCondT.equals(other.getTransitionCondT())
                && TransitionModel.equals(other.getTransitionModel())
                && StartType.equals(other.getStartType());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("TransitionID"))
        {
            return 0;
        }
        if (strFieldName.equals("ProcessID"))
        {
            return 1;
        }
        if (strFieldName.equals("TransitionStart"))
        {
            return 2;
        }
        if (strFieldName.equals("TransitionEnd"))
        {
            return 3;
        }
        if (strFieldName.equals("TransitionCond"))
        {
            return 4;
        }
        if (strFieldName.equals("TransitionCondT"))
        {
            return 5;
        }
        if (strFieldName.equals("TransitionModel"))
        {
            return 6;
        }
        if (strFieldName.equals("StartType"))
        {
            return 7;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "TransitionID";
                break;
            case 1:
                strFieldName = "ProcessID";
                break;
            case 2:
                strFieldName = "TransitionStart";
                break;
            case 3:
                strFieldName = "TransitionEnd";
                break;
            case 4:
                strFieldName = "TransitionCond";
                break;
            case 5:
                strFieldName = "TransitionCondT";
                break;
            case 6:
                strFieldName = "TransitionModel";
                break;
            case 7:
                strFieldName = "StartType";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("TransitionID"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ProcessID"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("TransitionStart"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("TransitionEnd"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("TransitionCond"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("TransitionCondT"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("TransitionModel"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StartType"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
