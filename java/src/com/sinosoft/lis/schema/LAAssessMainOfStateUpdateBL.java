/*
 * <p>ClassName: LABranchGroupBuildBL </p>
 * <p>Description: LABranchGroupBuildBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2013-05-17
 */
package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LAAssessMainDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.db.LAWageHistoryDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAgentBSchema;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LABranchGroupBSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vdb.LAAssessMainDBSet;
import com.sinosoft.lis.vschema.LAAgentBSet;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LAAssessMainSet;
import com.sinosoft.lis.vschema.LABranchGroupBSet;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LATreeBSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.lis.vschema.LAWageHistorySet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;

public class LAAssessMainOfStateUpdateBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	private MMap map = new MMap();

	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	/** 数据操作字符串 */
	private String mOperate;

	private String currentDate = PubFun.getCurrentDate();

	private String currentTime = PubFun.getCurrentTime();

	/** 全局变量 */
	private LAAssessMainSchema mLAAssessMainSchema = new LAAssessMainSchema();

	private LAAssessMainSet mLAAssessMainSet = new LAAssessMainSet();

	public LAAssessMainOfStateUpdateBL() {
	}

	public static void main(String[] args) {
	}

	/**
	 * 传输数据的公共方法
	 * 
	 * @param: cInputData 输入的数据 cOperate 数据操作
	 * @return:
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		/*
		 * if (!check()) { return false; }
		 */

		// 进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAH";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败LAH-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}

		// 准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "LAH";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	/*
	 * private boolean check() { String tManageCom
	 * =this.mLAWageHistorySchema.getManageCom(); String tWageNo
	 * =this.mLAWageHistorySchema.getWageNo(); String tBranchType
	 * =this.mLAWageHistorySchema.getBranchType(); String tBranchType2
	 * =this.mLAWageHistorySchema.getBranchType2();
	 * 
	 * String tsql = "select * from lawagehistory where
	 * ManageCom='"+tManageCom+"' and WageNo='"+tWageNo+"' and
	 * BranchType='"+tBranchType+"' BranchType2='"+tBranchType2+"'"; ExeSQL
	 * tExeSQL = new ExeSQL(); String tResult = tExeSQL.getOneValue(tsql);
	 * if(tResult==null||tResult.equals("")) { CError tError = new CError();
	 * tError.moduleName = "LAWageHistoryOfStateUpdateBL"; tError.functionName =
	 * "check"; tError.errorMessage = "该数据不存在！";
	 * this.mErrors.addOneError(tError); return false; } return true; }
	 */

	/**
	 * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData() {
		LAAssessMainDB tLAAssessMainDB = new LAAssessMainDB();
		LAAssessMainSet tLAAssessMainSet = new LAAssessMainSet();

		/**
		 * 插入处理
		 */
		if (this.mOperate.equals("Save")) {
			mLAAssessMainSchema.setOperator(this.mGlobalInput.Operator);
			mLAAssessMainSchema.setMakeDate(this.currentDate);
			mLAAssessMainSchema.setMakeTime(this.currentTime);
			mLAAssessMainSchema.setModifyDate(this.currentDate);
			mLAAssessMainSchema.setModifyTime(this.currentTime);
			mLAAssessMainSchema.setAssessType("00");
			this.mLAAssessMainSet.add(mLAAssessMainSchema);
			map.put(mLAAssessMainSet, "INSERT");
			System.out.println(mOperate);

		} else {

			/**
			 * 修改、删除数据处理
			 */
			// 团队信息修改
			String sql = "select * from laassessmain where 1=1 and branchtype ='"
					+ this.mLAAssessMainSchema.getBranchType()
					+ "'"
					+ " and branchtype2 = '"
					+ this.mLAAssessMainSchema.getBranchType2()
					+ "' and ManageCom='"
					+ this.mLAAssessMainSchema.getManageCom()
					+ "' and IndexCalNo= '"
					+ this.mLAAssessMainSchema.getIndexCalNo()
					+ "'and AgentGrade='"
					+ this.mLAAssessMainSchema.getAgentGrade() + "'";
			System.out.println(sql);
			tLAAssessMainSet = tLAAssessMainDB.executeQuery(sql);
			for (int i = 1; i <= tLAAssessMainSet.size(); i++) {
				LAAssessMainSchema tLLAAssessMainSchema = new LAAssessMainSchema();
				tLLAAssessMainSchema = tLAAssessMainSet.get(i).getSchema();

				/*
				 * tLAWageHistorySchema.setAClass(this.mLAWageHistorySchema.getAClass());
				 * tLAWageHistorySchema.setMakeDate(this.mLAWageHistorySchema.getMakeDate());
				 */

				tLLAAssessMainSchema.setState(this.mLAAssessMainSchema.getState());
				tLLAAssessMainSchema.setOperator(this.mGlobalInput.Operator);
				tLLAAssessMainSchema.setModifyDate(this.currentDate);
				tLLAAssessMainSchema.setModifyTime(this.currentTime);

				this.mLAAssessMainSet.add(tLLAAssessMainSchema);

			}
			// map.put(mLAAssessMainSet, "UPDATE");
			if (this.mOperate.equals("Modify")) {

				map.put(mLAAssessMainSet, "UPDATE");
				System.out.println(mOperate);
			}

			if (this.mOperate.equals("Delete")) {

				map.put(mLAAssessMainSet, "DELETE");
				System.out.println(mOperate);
			}

		}
		return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) {
		this.mLAAssessMainSchema.setSchema((LAAssessMainSchema) cInputData
				.getObjectByObjectName("LAAssessMainSchema", 0));
		this.mGlobalInput.setSchema((GlobalInput) cInputData
				.getObjectByObjectName("GlobalInput", 0));

		if (this.mGlobalInput == null) {
			CError tError = new CError();
			tError.moduleName = "LAAssessMainOfStateUpdateBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "mGlobalInput为空！";
			this.mErrors.addOneError(tError);
			return false;
		}
		if (this.mLAAssessMainSchema == null) {
			CError tError = new CError();
			tError.moduleName = "LAAssessMainOfStateUpdateBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "LAAssessMainSchema为空！";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	private boolean prepareOutputData() {
		try {
			mInputData.clear();
			mInputData.add(map);

		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAAssessMainOfStateUpdateBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	public VData getResult() {
		return this.mResult;
	}

	public void insert() {

	}

}
