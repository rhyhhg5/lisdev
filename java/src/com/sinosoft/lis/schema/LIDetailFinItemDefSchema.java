/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LIDetailFinItemDefDB;

/*
 * <p>ClassName: LIDetailFinItemDefSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 财务接口最新版
 * @CreateDate：2008-05-20
 */
public class LIDetailFinItemDefSchema implements Schema, Cloneable
{
	// @Field
	/** 科目类型编码 */
	private String FinItemID;
	/** 科目层级 */
	private int FinItemlevel;
	/** 判断条件编号 */
	private String JudgementNo;
	/** 层级条件组合 */
	private String LevelCondition;
	/** 描述 */
	private String ReMark;

	public static final int FIELDNUM = 5;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LIDetailFinItemDefSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "FinItemID";
		pk[1] = "JudgementNo";

		PK = pk;
	}

            /**
             * Schema克隆
             * @return Object
             * @throws CloneNotSupportedException
             */
            public Object clone()
                    throws CloneNotSupportedException
            {
                LIDetailFinItemDefSchema cloned = (LIDetailFinItemDefSchema)super.clone();
                cloned.mErrors = (CErrors) mErrors.clone();
                return cloned;
            }

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getFinItemID()
	{
		return FinItemID;
	}
	public void setFinItemID(String aFinItemID)
	{
            FinItemID = aFinItemID;
	}
	public int getFinItemlevel()
	{
		return FinItemlevel;
	}
	public void setFinItemlevel(int aFinItemlevel)
	{
            FinItemlevel = aFinItemlevel;
	}
	public void setFinItemlevel(String aFinItemlevel)
	{
		if (aFinItemlevel != null && !aFinItemlevel.equals(""))
		{
			Integer tInteger = new Integer(aFinItemlevel);
			int i = tInteger.intValue();
			FinItemlevel = i;
		}
	}

	public String getJudgementNo()
	{
		return JudgementNo;
	}
	public void setJudgementNo(String aJudgementNo)
	{
            JudgementNo = aJudgementNo;
	}
	public String getLevelCondition()
	{
		return LevelCondition;
	}
	public void setLevelCondition(String aLevelCondition)
	{
            LevelCondition = aLevelCondition;
	}
	public String getReMark()
	{
		return ReMark;
	}
	public void setReMark(String aReMark)
	{
            ReMark = aReMark;
	}

	/**
	* 使用另外一个 LIDetailFinItemDefSchema 对象给 Schema 赋值
	* @param: aLIDetailFinItemDefSchema LIDetailFinItemDefSchema
	**/
	public void setSchema(LIDetailFinItemDefSchema aLIDetailFinItemDefSchema)
	{
		this.FinItemID = aLIDetailFinItemDefSchema.getFinItemID();
		this.FinItemlevel = aLIDetailFinItemDefSchema.getFinItemlevel();
		this.JudgementNo = aLIDetailFinItemDefSchema.getJudgementNo();
		this.LevelCondition = aLIDetailFinItemDefSchema.getLevelCondition();
		this.ReMark = aLIDetailFinItemDefSchema.getReMark();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("FinItemID") == null )
				this.FinItemID = null;
			else
				this.FinItemID = rs.getString("FinItemID").trim();

			this.FinItemlevel = rs.getInt("FinItemlevel");
			if( rs.getString("JudgementNo") == null )
				this.JudgementNo = null;
			else
				this.JudgementNo = rs.getString("JudgementNo").trim();

			if( rs.getString("LevelCondition") == null )
				this.LevelCondition = null;
			else
				this.LevelCondition = rs.getString("LevelCondition").trim();

			if( rs.getString("ReMark") == null )
				this.ReMark = null;
			else
				this.ReMark = rs.getString("ReMark").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LIDetailFinItemDef表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LIDetailFinItemDefSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LIDetailFinItemDefSchema getSchema()
	{
		LIDetailFinItemDefSchema aLIDetailFinItemDefSchema = new LIDetailFinItemDefSchema();
		aLIDetailFinItemDefSchema.setSchema(this);
		return aLIDetailFinItemDefSchema;
	}

	public LIDetailFinItemDefDB getDB()
	{
		LIDetailFinItemDefDB aDBOper = new LIDetailFinItemDefDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLIDetailFinItemDef描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append(StrTool.cTrim(FinItemID)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(FinItemlevel));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(JudgementNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(LevelCondition)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ReMark));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLIDetailFinItemDef>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			FinItemID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			FinItemlevel= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,2,SysConst.PACKAGESPILTER))).intValue();
			JudgementNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			LevelCondition = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			ReMark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LIDetailFinItemDefSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("FinItemID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FinItemID));
		}
		if (FCode.equals("FinItemlevel"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FinItemlevel));
		}
		if (FCode.equals("JudgementNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(JudgementNo));
		}
		if (FCode.equals("LevelCondition"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LevelCondition));
		}
		if (FCode.equals("ReMark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReMark));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(FinItemID);
				break;
			case 1:
				strFieldValue = String.valueOf(FinItemlevel);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(JudgementNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(LevelCondition);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(ReMark);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("FinItemID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FinItemID = FValue.trim();
			}
			else
				FinItemID = null;
		}
		if (FCode.equalsIgnoreCase("FinItemlevel"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				FinItemlevel = i;
			}
		}
		if (FCode.equalsIgnoreCase("JudgementNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				JudgementNo = FValue.trim();
			}
			else
				JudgementNo = null;
		}
		if (FCode.equalsIgnoreCase("LevelCondition"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LevelCondition = FValue.trim();
			}
			else
				LevelCondition = null;
		}
		if (FCode.equalsIgnoreCase("ReMark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReMark = FValue.trim();
			}
			else
				ReMark = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LIDetailFinItemDefSchema other = (LIDetailFinItemDefSchema)otherObject;
		return
			FinItemID.equals(other.getFinItemID())
			&& FinItemlevel == other.getFinItemlevel()
			&& JudgementNo.equals(other.getJudgementNo())
			&& LevelCondition.equals(other.getLevelCondition())
			&& ReMark.equals(other.getReMark());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("FinItemID") ) {
			return 0;
		}
		if( strFieldName.equals("FinItemlevel") ) {
			return 1;
		}
		if( strFieldName.equals("JudgementNo") ) {
			return 2;
		}
		if( strFieldName.equals("LevelCondition") ) {
			return 3;
		}
		if( strFieldName.equals("ReMark") ) {
			return 4;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "FinItemID";
				break;
			case 1:
				strFieldName = "FinItemlevel";
				break;
			case 2:
				strFieldName = "JudgementNo";
				break;
			case 3:
				strFieldName = "LevelCondition";
				break;
			case 4:
				strFieldName = "ReMark";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("FinItemID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FinItemlevel") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("JudgementNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LevelCondition") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReMark") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_INT;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
