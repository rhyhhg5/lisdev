/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LCBriGrpImportBatchInfoDB;

/*
 * <p>ClassName: LCBriGrpImportBatchInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 大连补充工伤险承保导入接口业务
 * @CreateDate：2011-08-10
 */
public class LCBriGrpImportBatchInfoSchema implements Schema, Cloneable
{
	// @Field
	/** 批次号 */
	private String BatchNo;
	/** 报送日期 */
	private Date SendDate;
	/** 报送时间 */
	private String SendTime;
	/** 报送单位 */
	private String BranchCode;
	/** 报送人员 */
	private String SendOperator;
	/** 任务类型 */
	private String MsgType;
	/** 任务数量 */
	private int TaskCount;
	/** 导入状态 */
	private String ImportState;
	/** 导入开始日期 */
	private Date ImportStartDate;
	/** 导入完成日期 */
	private Date ImportEndDate;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 批次确认状态 */
	private String BatchConfState;
	/** 批次确认时间 */
	private Date BatchConfDate;

	public static final int FIELDNUM = 17;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LCBriGrpImportBatchInfoSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "BatchNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LCBriGrpImportBatchInfoSchema cloned = (LCBriGrpImportBatchInfoSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getBatchNo()
	{
		return BatchNo;
	}
	public void setBatchNo(String aBatchNo)
	{
		BatchNo = aBatchNo;
	}
	public String getSendDate()
	{
		if( SendDate != null )
			return fDate.getString(SendDate);
		else
			return null;
	}
	public void setSendDate(Date aSendDate)
	{
		SendDate = aSendDate;
	}
	public void setSendDate(String aSendDate)
	{
		if (aSendDate != null && !aSendDate.equals("") )
		{
			SendDate = fDate.getDate( aSendDate );
		}
		else
			SendDate = null;
	}

	public String getSendTime()
	{
		return SendTime;
	}
	public void setSendTime(String aSendTime)
	{
		SendTime = aSendTime;
	}
	public String getBranchCode()
	{
		return BranchCode;
	}
	public void setBranchCode(String aBranchCode)
	{
		BranchCode = aBranchCode;
	}
	public String getSendOperator()
	{
		return SendOperator;
	}
	public void setSendOperator(String aSendOperator)
	{
		SendOperator = aSendOperator;
	}
	public String getMsgType()
	{
		return MsgType;
	}
	public void setMsgType(String aMsgType)
	{
		MsgType = aMsgType;
	}
	public int getTaskCount()
	{
		return TaskCount;
	}
	public void setTaskCount(int aTaskCount)
	{
		TaskCount = aTaskCount;
	}
	public void setTaskCount(String aTaskCount)
	{
		if (aTaskCount != null && !aTaskCount.equals(""))
		{
			Integer tInteger = new Integer(aTaskCount);
			int i = tInteger.intValue();
			TaskCount = i;
		}
	}

	public String getImportState()
	{
		return ImportState;
	}
	public void setImportState(String aImportState)
	{
		ImportState = aImportState;
	}
	public String getImportStartDate()
	{
		if( ImportStartDate != null )
			return fDate.getString(ImportStartDate);
		else
			return null;
	}
	public void setImportStartDate(Date aImportStartDate)
	{
		ImportStartDate = aImportStartDate;
	}
	public void setImportStartDate(String aImportStartDate)
	{
		if (aImportStartDate != null && !aImportStartDate.equals("") )
		{
			ImportStartDate = fDate.getDate( aImportStartDate );
		}
		else
			ImportStartDate = null;
	}

	public String getImportEndDate()
	{
		if( ImportEndDate != null )
			return fDate.getString(ImportEndDate);
		else
			return null;
	}
	public void setImportEndDate(Date aImportEndDate)
	{
		ImportEndDate = aImportEndDate;
	}
	public void setImportEndDate(String aImportEndDate)
	{
		if (aImportEndDate != null && !aImportEndDate.equals("") )
		{
			ImportEndDate = fDate.getDate( aImportEndDate );
		}
		else
			ImportEndDate = null;
	}

	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getBatchConfState()
	{
		return BatchConfState;
	}
	public void setBatchConfState(String aBatchConfState)
	{
		BatchConfState = aBatchConfState;
	}
	public String getBatchConfDate()
	{
		if( BatchConfDate != null )
			return fDate.getString(BatchConfDate);
		else
			return null;
	}
	public void setBatchConfDate(Date aBatchConfDate)
	{
		BatchConfDate = aBatchConfDate;
	}
	public void setBatchConfDate(String aBatchConfDate)
	{
		if (aBatchConfDate != null && !aBatchConfDate.equals("") )
		{
			BatchConfDate = fDate.getDate( aBatchConfDate );
		}
		else
			BatchConfDate = null;
	}


	/**
	* 使用另外一个 LCBriGrpImportBatchInfoSchema 对象给 Schema 赋值
	* @param: aLCBriGrpImportBatchInfoSchema LCBriGrpImportBatchInfoSchema
	**/
	public void setSchema(LCBriGrpImportBatchInfoSchema aLCBriGrpImportBatchInfoSchema)
	{
		this.BatchNo = aLCBriGrpImportBatchInfoSchema.getBatchNo();
		this.SendDate = fDate.getDate( aLCBriGrpImportBatchInfoSchema.getSendDate());
		this.SendTime = aLCBriGrpImportBatchInfoSchema.getSendTime();
		this.BranchCode = aLCBriGrpImportBatchInfoSchema.getBranchCode();
		this.SendOperator = aLCBriGrpImportBatchInfoSchema.getSendOperator();
		this.MsgType = aLCBriGrpImportBatchInfoSchema.getMsgType();
		this.TaskCount = aLCBriGrpImportBatchInfoSchema.getTaskCount();
		this.ImportState = aLCBriGrpImportBatchInfoSchema.getImportState();
		this.ImportStartDate = fDate.getDate( aLCBriGrpImportBatchInfoSchema.getImportStartDate());
		this.ImportEndDate = fDate.getDate( aLCBriGrpImportBatchInfoSchema.getImportEndDate());
		this.Operator = aLCBriGrpImportBatchInfoSchema.getOperator();
		this.MakeDate = fDate.getDate( aLCBriGrpImportBatchInfoSchema.getMakeDate());
		this.MakeTime = aLCBriGrpImportBatchInfoSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLCBriGrpImportBatchInfoSchema.getModifyDate());
		this.ModifyTime = aLCBriGrpImportBatchInfoSchema.getModifyTime();
		this.BatchConfState = aLCBriGrpImportBatchInfoSchema.getBatchConfState();
		this.BatchConfDate = fDate.getDate( aLCBriGrpImportBatchInfoSchema.getBatchConfDate());
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("BatchNo") == null )
				this.BatchNo = null;
			else
				this.BatchNo = rs.getString("BatchNo").trim();

			this.SendDate = rs.getDate("SendDate");
			if( rs.getString("SendTime") == null )
				this.SendTime = null;
			else
				this.SendTime = rs.getString("SendTime").trim();

			if( rs.getString("BranchCode") == null )
				this.BranchCode = null;
			else
				this.BranchCode = rs.getString("BranchCode").trim();

			if( rs.getString("SendOperator") == null )
				this.SendOperator = null;
			else
				this.SendOperator = rs.getString("SendOperator").trim();

			if( rs.getString("MsgType") == null )
				this.MsgType = null;
			else
				this.MsgType = rs.getString("MsgType").trim();

			this.TaskCount = rs.getInt("TaskCount");
			if( rs.getString("ImportState") == null )
				this.ImportState = null;
			else
				this.ImportState = rs.getString("ImportState").trim();

			this.ImportStartDate = rs.getDate("ImportStartDate");
			this.ImportEndDate = rs.getDate("ImportEndDate");
			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("BatchConfState") == null )
				this.BatchConfState = null;
			else
				this.BatchConfState = rs.getString("BatchConfState").trim();

			this.BatchConfDate = rs.getDate("BatchConfDate");
		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LCBriGrpImportBatchInfo表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCBriGrpImportBatchInfoSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LCBriGrpImportBatchInfoSchema getSchema()
	{
		LCBriGrpImportBatchInfoSchema aLCBriGrpImportBatchInfoSchema = new LCBriGrpImportBatchInfoSchema();
		aLCBriGrpImportBatchInfoSchema.setSchema(this);
		return aLCBriGrpImportBatchInfoSchema;
	}

	public LCBriGrpImportBatchInfoDB getDB()
	{
		LCBriGrpImportBatchInfoDB aDBOper = new LCBriGrpImportBatchInfoDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCBriGrpImportBatchInfo描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(BatchNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( SendDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SendTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SendOperator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MsgType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(TaskCount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ImportState)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ImportStartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ImportEndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BatchConfState)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( BatchConfDate )));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCBriGrpImportBatchInfo>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			BatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			SendDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,SysConst.PACKAGESPILTER));
			SendTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			BranchCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			SendOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			MsgType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			TaskCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,7,SysConst.PACKAGESPILTER))).intValue();
			ImportState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			ImportStartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,SysConst.PACKAGESPILTER));
			ImportEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,SysConst.PACKAGESPILTER));
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			BatchConfState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			BatchConfDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,SysConst.PACKAGESPILTER));
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCBriGrpImportBatchInfoSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("BatchNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BatchNo));
		}
		if (FCode.equals("SendDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getSendDate()));
		}
		if (FCode.equals("SendTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SendTime));
		}
		if (FCode.equals("BranchCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchCode));
		}
		if (FCode.equals("SendOperator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SendOperator));
		}
		if (FCode.equals("MsgType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MsgType));
		}
		if (FCode.equals("TaskCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TaskCount));
		}
		if (FCode.equals("ImportState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ImportState));
		}
		if (FCode.equals("ImportStartDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getImportStartDate()));
		}
		if (FCode.equals("ImportEndDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getImportEndDate()));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("BatchConfState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BatchConfState));
		}
		if (FCode.equals("BatchConfDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getBatchConfDate()));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(BatchNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getSendDate()));
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(SendTime);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(BranchCode);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(SendOperator);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(MsgType);
				break;
			case 6:
				strFieldValue = String.valueOf(TaskCount);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(ImportState);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getImportStartDate()));
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getImportEndDate()));
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(BatchConfState);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getBatchConfDate()));
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("BatchNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BatchNo = FValue.trim();
			}
			else
				BatchNo = null;
		}
		if (FCode.equalsIgnoreCase("SendDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				SendDate = fDate.getDate( FValue );
			}
			else
				SendDate = null;
		}
		if (FCode.equalsIgnoreCase("SendTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SendTime = FValue.trim();
			}
			else
				SendTime = null;
		}
		if (FCode.equalsIgnoreCase("BranchCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchCode = FValue.trim();
			}
			else
				BranchCode = null;
		}
		if (FCode.equalsIgnoreCase("SendOperator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SendOperator = FValue.trim();
			}
			else
				SendOperator = null;
		}
		if (FCode.equalsIgnoreCase("MsgType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MsgType = FValue.trim();
			}
			else
				MsgType = null;
		}
		if (FCode.equalsIgnoreCase("TaskCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				TaskCount = i;
			}
		}
		if (FCode.equalsIgnoreCase("ImportState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ImportState = FValue.trim();
			}
			else
				ImportState = null;
		}
		if (FCode.equalsIgnoreCase("ImportStartDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ImportStartDate = fDate.getDate( FValue );
			}
			else
				ImportStartDate = null;
		}
		if (FCode.equalsIgnoreCase("ImportEndDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ImportEndDate = fDate.getDate( FValue );
			}
			else
				ImportEndDate = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("BatchConfState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BatchConfState = FValue.trim();
			}
			else
				BatchConfState = null;
		}
		if (FCode.equalsIgnoreCase("BatchConfDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				BatchConfDate = fDate.getDate( FValue );
			}
			else
				BatchConfDate = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LCBriGrpImportBatchInfoSchema other = (LCBriGrpImportBatchInfoSchema)otherObject;
		return
			(BatchNo == null ? other.getBatchNo() == null : BatchNo.equals(other.getBatchNo()))
			&& (SendDate == null ? other.getSendDate() == null : fDate.getString(SendDate).equals(other.getSendDate()))
			&& (SendTime == null ? other.getSendTime() == null : SendTime.equals(other.getSendTime()))
			&& (BranchCode == null ? other.getBranchCode() == null : BranchCode.equals(other.getBranchCode()))
			&& (SendOperator == null ? other.getSendOperator() == null : SendOperator.equals(other.getSendOperator()))
			&& (MsgType == null ? other.getMsgType() == null : MsgType.equals(other.getMsgType()))
			&& TaskCount == other.getTaskCount()
			&& (ImportState == null ? other.getImportState() == null : ImportState.equals(other.getImportState()))
			&& (ImportStartDate == null ? other.getImportStartDate() == null : fDate.getString(ImportStartDate).equals(other.getImportStartDate()))
			&& (ImportEndDate == null ? other.getImportEndDate() == null : fDate.getString(ImportEndDate).equals(other.getImportEndDate()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (BatchConfState == null ? other.getBatchConfState() == null : BatchConfState.equals(other.getBatchConfState()))
			&& (BatchConfDate == null ? other.getBatchConfDate() == null : fDate.getString(BatchConfDate).equals(other.getBatchConfDate()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("BatchNo") ) {
			return 0;
		}
		if( strFieldName.equals("SendDate") ) {
			return 1;
		}
		if( strFieldName.equals("SendTime") ) {
			return 2;
		}
		if( strFieldName.equals("BranchCode") ) {
			return 3;
		}
		if( strFieldName.equals("SendOperator") ) {
			return 4;
		}
		if( strFieldName.equals("MsgType") ) {
			return 5;
		}
		if( strFieldName.equals("TaskCount") ) {
			return 6;
		}
		if( strFieldName.equals("ImportState") ) {
			return 7;
		}
		if( strFieldName.equals("ImportStartDate") ) {
			return 8;
		}
		if( strFieldName.equals("ImportEndDate") ) {
			return 9;
		}
		if( strFieldName.equals("Operator") ) {
			return 10;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 11;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 12;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 13;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 14;
		}
		if( strFieldName.equals("BatchConfState") ) {
			return 15;
		}
		if( strFieldName.equals("BatchConfDate") ) {
			return 16;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "BatchNo";
				break;
			case 1:
				strFieldName = "SendDate";
				break;
			case 2:
				strFieldName = "SendTime";
				break;
			case 3:
				strFieldName = "BranchCode";
				break;
			case 4:
				strFieldName = "SendOperator";
				break;
			case 5:
				strFieldName = "MsgType";
				break;
			case 6:
				strFieldName = "TaskCount";
				break;
			case 7:
				strFieldName = "ImportState";
				break;
			case 8:
				strFieldName = "ImportStartDate";
				break;
			case 9:
				strFieldName = "ImportEndDate";
				break;
			case 10:
				strFieldName = "Operator";
				break;
			case 11:
				strFieldName = "MakeDate";
				break;
			case 12:
				strFieldName = "MakeTime";
				break;
			case 13:
				strFieldName = "ModifyDate";
				break;
			case 14:
				strFieldName = "ModifyTime";
				break;
			case 15:
				strFieldName = "BatchConfState";
				break;
			case 16:
				strFieldName = "BatchConfDate";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("BatchNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SendDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("SendTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SendOperator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MsgType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TaskCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("ImportState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ImportStartDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ImportEndDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BatchConfState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BatchConfDate") ) {
			return Schema.TYPE_DATE;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_INT;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 9:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_DATE;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
