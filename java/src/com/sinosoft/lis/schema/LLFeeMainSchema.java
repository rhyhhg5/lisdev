/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LLFeeMainDB;

/*
 * <p>ClassName: LLFeeMainSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2017-08-15
 */
public class LLFeeMainSchema implements Schema, Cloneable
{
	// @Field
	/** 账单号 */
	private String MainFeeNo;
	/** 分案号(个人理赔号) */
	private String CaseNo;
	/** 受理事故号 */
	private String CaseRelaNo;
	/** 立案号(申请登记号) */
	private String RgtNo;
	/** 附件代码 */
	private String AffixNo;
	/** (附件)流水号 */
	private int SerialNo;
	/** 出险人客户号 */
	private String CustomerNo;
	/** 出险人名称 */
	private String CustomerName;
	/** 出险人性别 */
	private String CustomerSex;
	/** 医院代码 */
	private String HospitalCode;
	/** 医院名称 */
	private String HospitalName;
	/** 责任类型 */
	private String DutyKind;
	/** 账单号码 */
	private String ReceiptNo;
	/** 账单种类 */
	private String FeeType;
	/** 账单属性 */
	private String FeeAtti;
	/** 帐单类型 */
	private String FeeAffixType;
	/** 账单日期 */
	private Date FeeDate;
	/** 住院起始日期 */
	private Date HospStartDate;
	/** 住院结束日期 */
	private Date HospEndDate;
	/** 实际住院天数 */
	private int RealHospDate;
	/** 重症监护天数 */
	private int SeriousWard;
	/** 管理机构 */
	private String MngCom;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 住院号 */
	private String InHosNo;
	/** 先期给付 */
	private double PreAmnt;
	/** 自费金额 */
	private double SelfAmnt;
	/** 不合理费用 */
	private double RefuseAmnt;
	/** 社会统筹 */
	private double SocialPlanAmnt;
	/** 附加支付 */
	private double AppendAmnt;
	/** 其他机构报销 */
	private double OtherOrganAmnt;
	/** 其它 */
	private double OtherAmnt;
	/** 个人支付 */
	private double PersonGiveAmnt;
	/** 费用总额 */
	private double SumFee;
	/** 医院所在地 */
	private String HosAddress;
	/** 医院级别 */
	private String HosGrade;
	/** 医院联系电话 */
	private String HosPhone;
	/** 医院属性 */
	private String HosAtti;
	/** 单位社保登记号 */
	private String CompSecuNo;
	/** 参保人员社保号 */
	private String SecurityNo;
	/** 年龄 */
	private int Age;
	/** 参保人员类别 */
	private String InsuredStat;
	/** 第一次住院标志 */
	private String FirstInHos;
	/** 原始账单标记 */
	private String OriginFlag;
	/** 再次赔付标记 */
	private String RepayFlag;
	/** 赔付次数 */
	private int PayTimes;
	/** 赔付剩余金额 */
	private double Remnant;
	/** 既往账单号 */
	private String OldMainFeeNo;
	/** 既往理赔号 */
	private String OldCaseNo;
	/** 医院城区属性 */
	private String HosDistrict;
	/** 医保结算时间 */
	private Date SecuPayDate;
	/** 医疗形式 */
	private String MedicalType;
	/** 医保结算方式 */
	private String SecuPayType;
	/** 可报销范围内金额 */
	private double Reimbursement;
	/** 收据类型 */
	private String ReceiptType;
	/** 分割单出具单位 */
	private String IssueUnit;
	/** 医保类型 */
	private String MedicareType;

	public static final int FIELDNUM = 60;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LLFeeMainSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "MainFeeNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LLFeeMainSchema cloned = (LLFeeMainSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getMainFeeNo()
	{
		return MainFeeNo;
	}
	public void setMainFeeNo(String aMainFeeNo)
	{
		MainFeeNo = aMainFeeNo;
	}
	public String getCaseNo()
	{
		return CaseNo;
	}
	public void setCaseNo(String aCaseNo)
	{
		CaseNo = aCaseNo;
	}
	public String getCaseRelaNo()
	{
		return CaseRelaNo;
	}
	public void setCaseRelaNo(String aCaseRelaNo)
	{
		CaseRelaNo = aCaseRelaNo;
	}
	public String getRgtNo()
	{
		return RgtNo;
	}
	public void setRgtNo(String aRgtNo)
	{
		RgtNo = aRgtNo;
	}
	public String getAffixNo()
	{
		return AffixNo;
	}
	public void setAffixNo(String aAffixNo)
	{
		AffixNo = aAffixNo;
	}
	public int getSerialNo()
	{
		return SerialNo;
	}
	public void setSerialNo(int aSerialNo)
	{
		SerialNo = aSerialNo;
	}
	public void setSerialNo(String aSerialNo)
	{
		if (aSerialNo != null && !aSerialNo.equals(""))
		{
			Integer tInteger = new Integer(aSerialNo);
			int i = tInteger.intValue();
			SerialNo = i;
		}
	}

	public String getCustomerNo()
	{
		return CustomerNo;
	}
	public void setCustomerNo(String aCustomerNo)
	{
		CustomerNo = aCustomerNo;
	}
	public String getCustomerName()
	{
		return CustomerName;
	}
	public void setCustomerName(String aCustomerName)
	{
		CustomerName = aCustomerName;
	}
	public String getCustomerSex()
	{
		return CustomerSex;
	}
	public void setCustomerSex(String aCustomerSex)
	{
		CustomerSex = aCustomerSex;
	}
	public String getHospitalCode()
	{
		return HospitalCode;
	}
	public void setHospitalCode(String aHospitalCode)
	{
		HospitalCode = aHospitalCode;
	}
	public String getHospitalName()
	{
		return HospitalName;
	}
	public void setHospitalName(String aHospitalName)
	{
		HospitalName = aHospitalName;
	}
	public String getDutyKind()
	{
		return DutyKind;
	}
	public void setDutyKind(String aDutyKind)
	{
		DutyKind = aDutyKind;
	}
	public String getReceiptNo()
	{
		return ReceiptNo;
	}
	public void setReceiptNo(String aReceiptNo)
	{
		ReceiptNo = aReceiptNo;
	}
	public String getFeeType()
	{
		return FeeType;
	}
	public void setFeeType(String aFeeType)
	{
		FeeType = aFeeType;
	}
	public String getFeeAtti()
	{
		return FeeAtti;
	}
	public void setFeeAtti(String aFeeAtti)
	{
		FeeAtti = aFeeAtti;
	}
	public String getFeeAffixType()
	{
		return FeeAffixType;
	}
	public void setFeeAffixType(String aFeeAffixType)
	{
		FeeAffixType = aFeeAffixType;
	}
	public String getFeeDate()
	{
		if( FeeDate != null )
			return fDate.getString(FeeDate);
		else
			return null;
	}
	public void setFeeDate(Date aFeeDate)
	{
		FeeDate = aFeeDate;
	}
	public void setFeeDate(String aFeeDate)
	{
		if (aFeeDate != null && !aFeeDate.equals("") )
		{
			FeeDate = fDate.getDate( aFeeDate );
		}
		else
			FeeDate = null;
	}

	public String getHospStartDate()
	{
		if( HospStartDate != null )
			return fDate.getString(HospStartDate);
		else
			return null;
	}
	public void setHospStartDate(Date aHospStartDate)
	{
		HospStartDate = aHospStartDate;
	}
	public void setHospStartDate(String aHospStartDate)
	{
		if (aHospStartDate != null && !aHospStartDate.equals("") )
		{
			HospStartDate = fDate.getDate( aHospStartDate );
		}
		else
			HospStartDate = null;
	}

	public String getHospEndDate()
	{
		if( HospEndDate != null )
			return fDate.getString(HospEndDate);
		else
			return null;
	}
	public void setHospEndDate(Date aHospEndDate)
	{
		HospEndDate = aHospEndDate;
	}
	public void setHospEndDate(String aHospEndDate)
	{
		if (aHospEndDate != null && !aHospEndDate.equals("") )
		{
			HospEndDate = fDate.getDate( aHospEndDate );
		}
		else
			HospEndDate = null;
	}

	public int getRealHospDate()
	{
		return RealHospDate;
	}
	public void setRealHospDate(int aRealHospDate)
	{
		RealHospDate = aRealHospDate;
	}
	public void setRealHospDate(String aRealHospDate)
	{
		if (aRealHospDate != null && !aRealHospDate.equals(""))
		{
			Integer tInteger = new Integer(aRealHospDate);
			int i = tInteger.intValue();
			RealHospDate = i;
		}
	}

	public int getSeriousWard()
	{
		return SeriousWard;
	}
	public void setSeriousWard(int aSeriousWard)
	{
		SeriousWard = aSeriousWard;
	}
	public void setSeriousWard(String aSeriousWard)
	{
		if (aSeriousWard != null && !aSeriousWard.equals(""))
		{
			Integer tInteger = new Integer(aSeriousWard);
			int i = tInteger.intValue();
			SeriousWard = i;
		}
	}

	public String getMngCom()
	{
		return MngCom;
	}
	public void setMngCom(String aMngCom)
	{
		MngCom = aMngCom;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getInHosNo()
	{
		return InHosNo;
	}
	public void setInHosNo(String aInHosNo)
	{
		InHosNo = aInHosNo;
	}
	public double getPreAmnt()
	{
		return PreAmnt;
	}
	public void setPreAmnt(double aPreAmnt)
	{
		PreAmnt = Arith.round(aPreAmnt,2);
	}
	public void setPreAmnt(String aPreAmnt)
	{
		if (aPreAmnt != null && !aPreAmnt.equals(""))
		{
			Double tDouble = new Double(aPreAmnt);
			double d = tDouble.doubleValue();
                PreAmnt = Arith.round(d,2);
		}
	}

	public double getSelfAmnt()
	{
		return SelfAmnt;
	}
	public void setSelfAmnt(double aSelfAmnt)
	{
		SelfAmnt = Arith.round(aSelfAmnt,2);
	}
	public void setSelfAmnt(String aSelfAmnt)
	{
		if (aSelfAmnt != null && !aSelfAmnt.equals(""))
		{
			Double tDouble = new Double(aSelfAmnt);
			double d = tDouble.doubleValue();
                SelfAmnt = Arith.round(d,2);
		}
	}

	public double getRefuseAmnt()
	{
		return RefuseAmnt;
	}
	public void setRefuseAmnt(double aRefuseAmnt)
	{
		RefuseAmnt = Arith.round(aRefuseAmnt,2);
	}
	public void setRefuseAmnt(String aRefuseAmnt)
	{
		if (aRefuseAmnt != null && !aRefuseAmnt.equals(""))
		{
			Double tDouble = new Double(aRefuseAmnt);
			double d = tDouble.doubleValue();
                RefuseAmnt = Arith.round(d,2);
		}
	}

	public double getSocialPlanAmnt()
	{
		return SocialPlanAmnt;
	}
	public void setSocialPlanAmnt(double aSocialPlanAmnt)
	{
		SocialPlanAmnt = Arith.round(aSocialPlanAmnt,2);
	}
	public void setSocialPlanAmnt(String aSocialPlanAmnt)
	{
		if (aSocialPlanAmnt != null && !aSocialPlanAmnt.equals(""))
		{
			Double tDouble = new Double(aSocialPlanAmnt);
			double d = tDouble.doubleValue();
                SocialPlanAmnt = Arith.round(d,2);
		}
	}

	public double getAppendAmnt()
	{
		return AppendAmnt;
	}
	public void setAppendAmnt(double aAppendAmnt)
	{
		AppendAmnt = Arith.round(aAppendAmnt,2);
	}
	public void setAppendAmnt(String aAppendAmnt)
	{
		if (aAppendAmnt != null && !aAppendAmnt.equals(""))
		{
			Double tDouble = new Double(aAppendAmnt);
			double d = tDouble.doubleValue();
                AppendAmnt = Arith.round(d,2);
		}
	}

	public double getOtherOrganAmnt()
	{
		return OtherOrganAmnt;
	}
	public void setOtherOrganAmnt(double aOtherOrganAmnt)
	{
		OtherOrganAmnt = Arith.round(aOtherOrganAmnt,2);
	}
	public void setOtherOrganAmnt(String aOtherOrganAmnt)
	{
		if (aOtherOrganAmnt != null && !aOtherOrganAmnt.equals(""))
		{
			Double tDouble = new Double(aOtherOrganAmnt);
			double d = tDouble.doubleValue();
                OtherOrganAmnt = Arith.round(d,2);
		}
	}

	public double getOtherAmnt()
	{
		return OtherAmnt;
	}
	public void setOtherAmnt(double aOtherAmnt)
	{
		OtherAmnt = Arith.round(aOtherAmnt,2);
	}
	public void setOtherAmnt(String aOtherAmnt)
	{
		if (aOtherAmnt != null && !aOtherAmnt.equals(""))
		{
			Double tDouble = new Double(aOtherAmnt);
			double d = tDouble.doubleValue();
                OtherAmnt = Arith.round(d,2);
		}
	}

	public double getPersonGiveAmnt()
	{
		return PersonGiveAmnt;
	}
	public void setPersonGiveAmnt(double aPersonGiveAmnt)
	{
		PersonGiveAmnt = Arith.round(aPersonGiveAmnt,2);
	}
	public void setPersonGiveAmnt(String aPersonGiveAmnt)
	{
		if (aPersonGiveAmnt != null && !aPersonGiveAmnt.equals(""))
		{
			Double tDouble = new Double(aPersonGiveAmnt);
			double d = tDouble.doubleValue();
                PersonGiveAmnt = Arith.round(d,2);
		}
	}

	public double getSumFee()
	{
		return SumFee;
	}
	public void setSumFee(double aSumFee)
	{
		SumFee = Arith.round(aSumFee,2);
	}
	public void setSumFee(String aSumFee)
	{
		if (aSumFee != null && !aSumFee.equals(""))
		{
			Double tDouble = new Double(aSumFee);
			double d = tDouble.doubleValue();
                SumFee = Arith.round(d,2);
		}
	}

	public String getHosAddress()
	{
		return HosAddress;
	}
	public void setHosAddress(String aHosAddress)
	{
		HosAddress = aHosAddress;
	}
	public String getHosGrade()
	{
		return HosGrade;
	}
	public void setHosGrade(String aHosGrade)
	{
		HosGrade = aHosGrade;
	}
	public String getHosPhone()
	{
		return HosPhone;
	}
	public void setHosPhone(String aHosPhone)
	{
		HosPhone = aHosPhone;
	}
	public String getHosAtti()
	{
		return HosAtti;
	}
	public void setHosAtti(String aHosAtti)
	{
		HosAtti = aHosAtti;
	}
	public String getCompSecuNo()
	{
		return CompSecuNo;
	}
	public void setCompSecuNo(String aCompSecuNo)
	{
		CompSecuNo = aCompSecuNo;
	}
	public String getSecurityNo()
	{
		return SecurityNo;
	}
	public void setSecurityNo(String aSecurityNo)
	{
		SecurityNo = aSecurityNo;
	}
	public int getAge()
	{
		return Age;
	}
	public void setAge(int aAge)
	{
		Age = aAge;
	}
	public void setAge(String aAge)
	{
		if (aAge != null && !aAge.equals(""))
		{
			Integer tInteger = new Integer(aAge);
			int i = tInteger.intValue();
			Age = i;
		}
	}

	public String getInsuredStat()
	{
		return InsuredStat;
	}
	public void setInsuredStat(String aInsuredStat)
	{
		InsuredStat = aInsuredStat;
	}
	public String getFirstInHos()
	{
		return FirstInHos;
	}
	public void setFirstInHos(String aFirstInHos)
	{
		FirstInHos = aFirstInHos;
	}
	public String getOriginFlag()
	{
		return OriginFlag;
	}
	public void setOriginFlag(String aOriginFlag)
	{
		OriginFlag = aOriginFlag;
	}
	public String getRepayFlag()
	{
		return RepayFlag;
	}
	public void setRepayFlag(String aRepayFlag)
	{
		RepayFlag = aRepayFlag;
	}
	public int getPayTimes()
	{
		return PayTimes;
	}
	public void setPayTimes(int aPayTimes)
	{
		PayTimes = aPayTimes;
	}
	public void setPayTimes(String aPayTimes)
	{
		if (aPayTimes != null && !aPayTimes.equals(""))
		{
			Integer tInteger = new Integer(aPayTimes);
			int i = tInteger.intValue();
			PayTimes = i;
		}
	}

	public double getRemnant()
	{
		return Remnant;
	}
	public void setRemnant(double aRemnant)
	{
		Remnant = Arith.round(aRemnant,2);
	}
	public void setRemnant(String aRemnant)
	{
		if (aRemnant != null && !aRemnant.equals(""))
		{
			Double tDouble = new Double(aRemnant);
			double d = tDouble.doubleValue();
                Remnant = Arith.round(d,2);
		}
	}

	public String getOldMainFeeNo()
	{
		return OldMainFeeNo;
	}
	public void setOldMainFeeNo(String aOldMainFeeNo)
	{
		OldMainFeeNo = aOldMainFeeNo;
	}
	public String getOldCaseNo()
	{
		return OldCaseNo;
	}
	public void setOldCaseNo(String aOldCaseNo)
	{
		OldCaseNo = aOldCaseNo;
	}
	public String getHosDistrict()
	{
		return HosDistrict;
	}
	public void setHosDistrict(String aHosDistrict)
	{
		HosDistrict = aHosDistrict;
	}
	public String getSecuPayDate()
	{
		if( SecuPayDate != null )
			return fDate.getString(SecuPayDate);
		else
			return null;
	}
	public void setSecuPayDate(Date aSecuPayDate)
	{
		SecuPayDate = aSecuPayDate;
	}
	public void setSecuPayDate(String aSecuPayDate)
	{
		if (aSecuPayDate != null && !aSecuPayDate.equals("") )
		{
			SecuPayDate = fDate.getDate( aSecuPayDate );
		}
		else
			SecuPayDate = null;
	}

	public String getMedicalType()
	{
		return MedicalType;
	}
	public void setMedicalType(String aMedicalType)
	{
		MedicalType = aMedicalType;
	}
	public String getSecuPayType()
	{
		return SecuPayType;
	}
	public void setSecuPayType(String aSecuPayType)
	{
		SecuPayType = aSecuPayType;
	}
	public double getReimbursement()
	{
		return Reimbursement;
	}
	public void setReimbursement(double aReimbursement)
	{
		Reimbursement = Arith.round(aReimbursement,2);
	}
	public void setReimbursement(String aReimbursement)
	{
		if (aReimbursement != null && !aReimbursement.equals(""))
		{
			Double tDouble = new Double(aReimbursement);
			double d = tDouble.doubleValue();
                Reimbursement = Arith.round(d,2);
		}
	}

	public String getReceiptType()
	{
		return ReceiptType;
	}
	public void setReceiptType(String aReceiptType)
	{
		ReceiptType = aReceiptType;
	}
	public String getIssueUnit()
	{
		return IssueUnit;
	}
	public void setIssueUnit(String aIssueUnit)
	{
		IssueUnit = aIssueUnit;
	}
	public String getMedicareType()
	{
		return MedicareType;
	}
	public void setMedicareType(String aMedicareType)
	{
		MedicareType = aMedicareType;
	}

	/**
	* 使用另外一个 LLFeeMainSchema 对象给 Schema 赋值
	* @param: aLLFeeMainSchema LLFeeMainSchema
	**/
	public void setSchema(LLFeeMainSchema aLLFeeMainSchema)
	{
		this.MainFeeNo = aLLFeeMainSchema.getMainFeeNo();
		this.CaseNo = aLLFeeMainSchema.getCaseNo();
		this.CaseRelaNo = aLLFeeMainSchema.getCaseRelaNo();
		this.RgtNo = aLLFeeMainSchema.getRgtNo();
		this.AffixNo = aLLFeeMainSchema.getAffixNo();
		this.SerialNo = aLLFeeMainSchema.getSerialNo();
		this.CustomerNo = aLLFeeMainSchema.getCustomerNo();
		this.CustomerName = aLLFeeMainSchema.getCustomerName();
		this.CustomerSex = aLLFeeMainSchema.getCustomerSex();
		this.HospitalCode = aLLFeeMainSchema.getHospitalCode();
		this.HospitalName = aLLFeeMainSchema.getHospitalName();
		this.DutyKind = aLLFeeMainSchema.getDutyKind();
		this.ReceiptNo = aLLFeeMainSchema.getReceiptNo();
		this.FeeType = aLLFeeMainSchema.getFeeType();
		this.FeeAtti = aLLFeeMainSchema.getFeeAtti();
		this.FeeAffixType = aLLFeeMainSchema.getFeeAffixType();
		this.FeeDate = fDate.getDate( aLLFeeMainSchema.getFeeDate());
		this.HospStartDate = fDate.getDate( aLLFeeMainSchema.getHospStartDate());
		this.HospEndDate = fDate.getDate( aLLFeeMainSchema.getHospEndDate());
		this.RealHospDate = aLLFeeMainSchema.getRealHospDate();
		this.SeriousWard = aLLFeeMainSchema.getSeriousWard();
		this.MngCom = aLLFeeMainSchema.getMngCom();
		this.Operator = aLLFeeMainSchema.getOperator();
		this.MakeDate = fDate.getDate( aLLFeeMainSchema.getMakeDate());
		this.MakeTime = aLLFeeMainSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLLFeeMainSchema.getModifyDate());
		this.ModifyTime = aLLFeeMainSchema.getModifyTime();
		this.InHosNo = aLLFeeMainSchema.getInHosNo();
		this.PreAmnt = aLLFeeMainSchema.getPreAmnt();
		this.SelfAmnt = aLLFeeMainSchema.getSelfAmnt();
		this.RefuseAmnt = aLLFeeMainSchema.getRefuseAmnt();
		this.SocialPlanAmnt = aLLFeeMainSchema.getSocialPlanAmnt();
		this.AppendAmnt = aLLFeeMainSchema.getAppendAmnt();
		this.OtherOrganAmnt = aLLFeeMainSchema.getOtherOrganAmnt();
		this.OtherAmnt = aLLFeeMainSchema.getOtherAmnt();
		this.PersonGiveAmnt = aLLFeeMainSchema.getPersonGiveAmnt();
		this.SumFee = aLLFeeMainSchema.getSumFee();
		this.HosAddress = aLLFeeMainSchema.getHosAddress();
		this.HosGrade = aLLFeeMainSchema.getHosGrade();
		this.HosPhone = aLLFeeMainSchema.getHosPhone();
		this.HosAtti = aLLFeeMainSchema.getHosAtti();
		this.CompSecuNo = aLLFeeMainSchema.getCompSecuNo();
		this.SecurityNo = aLLFeeMainSchema.getSecurityNo();
		this.Age = aLLFeeMainSchema.getAge();
		this.InsuredStat = aLLFeeMainSchema.getInsuredStat();
		this.FirstInHos = aLLFeeMainSchema.getFirstInHos();
		this.OriginFlag = aLLFeeMainSchema.getOriginFlag();
		this.RepayFlag = aLLFeeMainSchema.getRepayFlag();
		this.PayTimes = aLLFeeMainSchema.getPayTimes();
		this.Remnant = aLLFeeMainSchema.getRemnant();
		this.OldMainFeeNo = aLLFeeMainSchema.getOldMainFeeNo();
		this.OldCaseNo = aLLFeeMainSchema.getOldCaseNo();
		this.HosDistrict = aLLFeeMainSchema.getHosDistrict();
		this.SecuPayDate = fDate.getDate( aLLFeeMainSchema.getSecuPayDate());
		this.MedicalType = aLLFeeMainSchema.getMedicalType();
		this.SecuPayType = aLLFeeMainSchema.getSecuPayType();
		this.Reimbursement = aLLFeeMainSchema.getReimbursement();
		this.ReceiptType = aLLFeeMainSchema.getReceiptType();
		this.IssueUnit = aLLFeeMainSchema.getIssueUnit();
		this.MedicareType = aLLFeeMainSchema.getMedicareType();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("MainFeeNo") == null )
				this.MainFeeNo = null;
			else
				this.MainFeeNo = rs.getString("MainFeeNo").trim();

			if( rs.getString("CaseNo") == null )
				this.CaseNo = null;
			else
				this.CaseNo = rs.getString("CaseNo").trim();

			if( rs.getString("CaseRelaNo") == null )
				this.CaseRelaNo = null;
			else
				this.CaseRelaNo = rs.getString("CaseRelaNo").trim();

			if( rs.getString("RgtNo") == null )
				this.RgtNo = null;
			else
				this.RgtNo = rs.getString("RgtNo").trim();

			if( rs.getString("AffixNo") == null )
				this.AffixNo = null;
			else
				this.AffixNo = rs.getString("AffixNo").trim();

			this.SerialNo = rs.getInt("SerialNo");
			if( rs.getString("CustomerNo") == null )
				this.CustomerNo = null;
			else
				this.CustomerNo = rs.getString("CustomerNo").trim();

			if( rs.getString("CustomerName") == null )
				this.CustomerName = null;
			else
				this.CustomerName = rs.getString("CustomerName").trim();

			if( rs.getString("CustomerSex") == null )
				this.CustomerSex = null;
			else
				this.CustomerSex = rs.getString("CustomerSex").trim();

			if( rs.getString("HospitalCode") == null )
				this.HospitalCode = null;
			else
				this.HospitalCode = rs.getString("HospitalCode").trim();

			if( rs.getString("HospitalName") == null )
				this.HospitalName = null;
			else
				this.HospitalName = rs.getString("HospitalName").trim();

			if( rs.getString("DutyKind") == null )
				this.DutyKind = null;
			else
				this.DutyKind = rs.getString("DutyKind").trim();

			if( rs.getString("ReceiptNo") == null )
				this.ReceiptNo = null;
			else
				this.ReceiptNo = rs.getString("ReceiptNo").trim();

			if( rs.getString("FeeType") == null )
				this.FeeType = null;
			else
				this.FeeType = rs.getString("FeeType").trim();

			if( rs.getString("FeeAtti") == null )
				this.FeeAtti = null;
			else
				this.FeeAtti = rs.getString("FeeAtti").trim();

			if( rs.getString("FeeAffixType") == null )
				this.FeeAffixType = null;
			else
				this.FeeAffixType = rs.getString("FeeAffixType").trim();

			this.FeeDate = rs.getDate("FeeDate");
			this.HospStartDate = rs.getDate("HospStartDate");
			this.HospEndDate = rs.getDate("HospEndDate");
			this.RealHospDate = rs.getInt("RealHospDate");
			this.SeriousWard = rs.getInt("SeriousWard");
			if( rs.getString("MngCom") == null )
				this.MngCom = null;
			else
				this.MngCom = rs.getString("MngCom").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("InHosNo") == null )
				this.InHosNo = null;
			else
				this.InHosNo = rs.getString("InHosNo").trim();

			this.PreAmnt = rs.getDouble("PreAmnt");
			this.SelfAmnt = rs.getDouble("SelfAmnt");
			this.RefuseAmnt = rs.getDouble("RefuseAmnt");
			this.SocialPlanAmnt = rs.getDouble("SocialPlanAmnt");
			this.AppendAmnt = rs.getDouble("AppendAmnt");
			this.OtherOrganAmnt = rs.getDouble("OtherOrganAmnt");
			this.OtherAmnt = rs.getDouble("OtherAmnt");
			this.PersonGiveAmnt = rs.getDouble("PersonGiveAmnt");
			this.SumFee = rs.getDouble("SumFee");
			if( rs.getString("HosAddress") == null )
				this.HosAddress = null;
			else
				this.HosAddress = rs.getString("HosAddress").trim();

			if( rs.getString("HosGrade") == null )
				this.HosGrade = null;
			else
				this.HosGrade = rs.getString("HosGrade").trim();

			if( rs.getString("HosPhone") == null )
				this.HosPhone = null;
			else
				this.HosPhone = rs.getString("HosPhone").trim();

			if( rs.getString("HosAtti") == null )
				this.HosAtti = null;
			else
				this.HosAtti = rs.getString("HosAtti").trim();

			if( rs.getString("CompSecuNo") == null )
				this.CompSecuNo = null;
			else
				this.CompSecuNo = rs.getString("CompSecuNo").trim();

			if( rs.getString("SecurityNo") == null )
				this.SecurityNo = null;
			else
				this.SecurityNo = rs.getString("SecurityNo").trim();

			this.Age = rs.getInt("Age");
			if( rs.getString("InsuredStat") == null )
				this.InsuredStat = null;
			else
				this.InsuredStat = rs.getString("InsuredStat").trim();

			if( rs.getString("FirstInHos") == null )
				this.FirstInHos = null;
			else
				this.FirstInHos = rs.getString("FirstInHos").trim();

			if( rs.getString("OriginFlag") == null )
				this.OriginFlag = null;
			else
				this.OriginFlag = rs.getString("OriginFlag").trim();

			if( rs.getString("RepayFlag") == null )
				this.RepayFlag = null;
			else
				this.RepayFlag = rs.getString("RepayFlag").trim();

			this.PayTimes = rs.getInt("PayTimes");
			this.Remnant = rs.getDouble("Remnant");
			if( rs.getString("OldMainFeeNo") == null )
				this.OldMainFeeNo = null;
			else
				this.OldMainFeeNo = rs.getString("OldMainFeeNo").trim();

			if( rs.getString("OldCaseNo") == null )
				this.OldCaseNo = null;
			else
				this.OldCaseNo = rs.getString("OldCaseNo").trim();

			if( rs.getString("HosDistrict") == null )
				this.HosDistrict = null;
			else
				this.HosDistrict = rs.getString("HosDistrict").trim();

			this.SecuPayDate = rs.getDate("SecuPayDate");
			if( rs.getString("MedicalType") == null )
				this.MedicalType = null;
			else
				this.MedicalType = rs.getString("MedicalType").trim();

			if( rs.getString("SecuPayType") == null )
				this.SecuPayType = null;
			else
				this.SecuPayType = rs.getString("SecuPayType").trim();

			this.Reimbursement = rs.getDouble("Reimbursement");
			if( rs.getString("ReceiptType") == null )
				this.ReceiptType = null;
			else
				this.ReceiptType = rs.getString("ReceiptType").trim();

			if( rs.getString("IssueUnit") == null )
				this.IssueUnit = null;
			else
				this.IssueUnit = rs.getString("IssueUnit").trim();

			if( rs.getString("MedicareType") == null )
				this.MedicareType = null;
			else
				this.MedicareType = rs.getString("MedicareType").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LLFeeMain表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLFeeMainSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LLFeeMainSchema getSchema()
	{
		LLFeeMainSchema aLLFeeMainSchema = new LLFeeMainSchema();
		aLLFeeMainSchema.setSchema(this);
		return aLLFeeMainSchema;
	}

	public LLFeeMainDB getDB()
	{
		LLFeeMainDB aDBOper = new LLFeeMainDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLFeeMain描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(MainFeeNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CaseNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CaseRelaNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RgtNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AffixNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SerialNo));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerSex)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HospitalCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HospitalName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DutyKind)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReceiptNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FeeType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FeeAtti)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FeeAffixType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( FeeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( HospStartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( HospEndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(RealHospDate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SeriousWard));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MngCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InHosNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PreAmnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SelfAmnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(RefuseAmnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SocialPlanAmnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(AppendAmnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(OtherOrganAmnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(OtherAmnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PersonGiveAmnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SumFee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HosAddress)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HosGrade)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HosPhone)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HosAtti)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CompSecuNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SecurityNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Age));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuredStat)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FirstInHos)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OriginFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RepayFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PayTimes));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Remnant));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OldMainFeeNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OldCaseNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HosDistrict)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( SecuPayDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MedicalType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SecuPayType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Reimbursement));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReceiptType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IssueUnit)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MedicareType));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLFeeMain>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			MainFeeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			CaseNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			CaseRelaNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			RgtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			AffixNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			SerialNo= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,6,SysConst.PACKAGESPILTER))).intValue();
			CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			CustomerName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			CustomerSex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			HospitalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			HospitalName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			DutyKind = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			ReceiptNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			FeeType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			FeeAtti = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			FeeAffixType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			FeeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,SysConst.PACKAGESPILTER));
			HospStartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,SysConst.PACKAGESPILTER));
			HospEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,SysConst.PACKAGESPILTER));
			RealHospDate= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,20,SysConst.PACKAGESPILTER))).intValue();
			SeriousWard= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,21,SysConst.PACKAGESPILTER))).intValue();
			MngCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			InHosNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			PreAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,29,SysConst.PACKAGESPILTER))).doubleValue();
			SelfAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,30,SysConst.PACKAGESPILTER))).doubleValue();
			RefuseAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,31,SysConst.PACKAGESPILTER))).doubleValue();
			SocialPlanAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,32,SysConst.PACKAGESPILTER))).doubleValue();
			AppendAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,33,SysConst.PACKAGESPILTER))).doubleValue();
			OtherOrganAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,34,SysConst.PACKAGESPILTER))).doubleValue();
			OtherAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,35,SysConst.PACKAGESPILTER))).doubleValue();
			PersonGiveAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,36,SysConst.PACKAGESPILTER))).doubleValue();
			SumFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,37,SysConst.PACKAGESPILTER))).doubleValue();
			HosAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
			HosGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
			HosPhone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
			HosAtti = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41, SysConst.PACKAGESPILTER );
			CompSecuNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
			SecurityNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43, SysConst.PACKAGESPILTER );
			Age= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,44,SysConst.PACKAGESPILTER))).intValue();
			InsuredStat = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 45, SysConst.PACKAGESPILTER );
			FirstInHos = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 46, SysConst.PACKAGESPILTER );
			OriginFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 47, SysConst.PACKAGESPILTER );
			RepayFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 48, SysConst.PACKAGESPILTER );
			PayTimes= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,49,SysConst.PACKAGESPILTER))).intValue();
			Remnant = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,50,SysConst.PACKAGESPILTER))).doubleValue();
			OldMainFeeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 51, SysConst.PACKAGESPILTER );
			OldCaseNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 52, SysConst.PACKAGESPILTER );
			HosDistrict = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 53, SysConst.PACKAGESPILTER );
			SecuPayDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 54,SysConst.PACKAGESPILTER));
			MedicalType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 55, SysConst.PACKAGESPILTER );
			SecuPayType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 56, SysConst.PACKAGESPILTER );
			Reimbursement = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,57,SysConst.PACKAGESPILTER))).doubleValue();
			ReceiptType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 58, SysConst.PACKAGESPILTER );
			IssueUnit = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 59, SysConst.PACKAGESPILTER );
			MedicareType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 60, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLFeeMainSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("MainFeeNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MainFeeNo));
		}
		if (FCode.equals("CaseNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CaseNo));
		}
		if (FCode.equals("CaseRelaNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CaseRelaNo));
		}
		if (FCode.equals("RgtNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RgtNo));
		}
		if (FCode.equals("AffixNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AffixNo));
		}
		if (FCode.equals("SerialNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
		}
		if (FCode.equals("CustomerNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
		}
		if (FCode.equals("CustomerName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerName));
		}
		if (FCode.equals("CustomerSex"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerSex));
		}
		if (FCode.equals("HospitalCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HospitalCode));
		}
		if (FCode.equals("HospitalName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HospitalName));
		}
		if (FCode.equals("DutyKind"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DutyKind));
		}
		if (FCode.equals("ReceiptNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiptNo));
		}
		if (FCode.equals("FeeType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FeeType));
		}
		if (FCode.equals("FeeAtti"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FeeAtti));
		}
		if (FCode.equals("FeeAffixType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FeeAffixType));
		}
		if (FCode.equals("FeeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getFeeDate()));
		}
		if (FCode.equals("HospStartDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getHospStartDate()));
		}
		if (FCode.equals("HospEndDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getHospEndDate()));
		}
		if (FCode.equals("RealHospDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RealHospDate));
		}
		if (FCode.equals("SeriousWard"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SeriousWard));
		}
		if (FCode.equals("MngCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MngCom));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("InHosNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InHosNo));
		}
		if (FCode.equals("PreAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PreAmnt));
		}
		if (FCode.equals("SelfAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SelfAmnt));
		}
		if (FCode.equals("RefuseAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RefuseAmnt));
		}
		if (FCode.equals("SocialPlanAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SocialPlanAmnt));
		}
		if (FCode.equals("AppendAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppendAmnt));
		}
		if (FCode.equals("OtherOrganAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OtherOrganAmnt));
		}
		if (FCode.equals("OtherAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OtherAmnt));
		}
		if (FCode.equals("PersonGiveAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PersonGiveAmnt));
		}
		if (FCode.equals("SumFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SumFee));
		}
		if (FCode.equals("HosAddress"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HosAddress));
		}
		if (FCode.equals("HosGrade"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HosGrade));
		}
		if (FCode.equals("HosPhone"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HosPhone));
		}
		if (FCode.equals("HosAtti"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HosAtti));
		}
		if (FCode.equals("CompSecuNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CompSecuNo));
		}
		if (FCode.equals("SecurityNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SecurityNo));
		}
		if (FCode.equals("Age"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Age));
		}
		if (FCode.equals("InsuredStat"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredStat));
		}
		if (FCode.equals("FirstInHos"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FirstInHos));
		}
		if (FCode.equals("OriginFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OriginFlag));
		}
		if (FCode.equals("RepayFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RepayFlag));
		}
		if (FCode.equals("PayTimes"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayTimes));
		}
		if (FCode.equals("Remnant"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remnant));
		}
		if (FCode.equals("OldMainFeeNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OldMainFeeNo));
		}
		if (FCode.equals("OldCaseNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OldCaseNo));
		}
		if (FCode.equals("HosDistrict"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HosDistrict));
		}
		if (FCode.equals("SecuPayDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getSecuPayDate()));
		}
		if (FCode.equals("MedicalType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MedicalType));
		}
		if (FCode.equals("SecuPayType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SecuPayType));
		}
		if (FCode.equals("Reimbursement"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Reimbursement));
		}
		if (FCode.equals("ReceiptType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiptType));
		}
		if (FCode.equals("IssueUnit"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IssueUnit));
		}
		if (FCode.equals("MedicareType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MedicareType));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(MainFeeNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(CaseNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(CaseRelaNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(RgtNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(AffixNo);
				break;
			case 5:
				strFieldValue = String.valueOf(SerialNo);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(CustomerNo);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(CustomerName);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(CustomerSex);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(HospitalCode);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(HospitalName);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(DutyKind);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(ReceiptNo);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(FeeType);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(FeeAtti);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(FeeAffixType);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getFeeDate()));
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getHospStartDate()));
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getHospEndDate()));
				break;
			case 19:
				strFieldValue = String.valueOf(RealHospDate);
				break;
			case 20:
				strFieldValue = String.valueOf(SeriousWard);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(MngCom);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(InHosNo);
				break;
			case 28:
				strFieldValue = String.valueOf(PreAmnt);
				break;
			case 29:
				strFieldValue = String.valueOf(SelfAmnt);
				break;
			case 30:
				strFieldValue = String.valueOf(RefuseAmnt);
				break;
			case 31:
				strFieldValue = String.valueOf(SocialPlanAmnt);
				break;
			case 32:
				strFieldValue = String.valueOf(AppendAmnt);
				break;
			case 33:
				strFieldValue = String.valueOf(OtherOrganAmnt);
				break;
			case 34:
				strFieldValue = String.valueOf(OtherAmnt);
				break;
			case 35:
				strFieldValue = String.valueOf(PersonGiveAmnt);
				break;
			case 36:
				strFieldValue = String.valueOf(SumFee);
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(HosAddress);
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(HosGrade);
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(HosPhone);
				break;
			case 40:
				strFieldValue = StrTool.GBKToUnicode(HosAtti);
				break;
			case 41:
				strFieldValue = StrTool.GBKToUnicode(CompSecuNo);
				break;
			case 42:
				strFieldValue = StrTool.GBKToUnicode(SecurityNo);
				break;
			case 43:
				strFieldValue = String.valueOf(Age);
				break;
			case 44:
				strFieldValue = StrTool.GBKToUnicode(InsuredStat);
				break;
			case 45:
				strFieldValue = StrTool.GBKToUnicode(FirstInHos);
				break;
			case 46:
				strFieldValue = StrTool.GBKToUnicode(OriginFlag);
				break;
			case 47:
				strFieldValue = StrTool.GBKToUnicode(RepayFlag);
				break;
			case 48:
				strFieldValue = String.valueOf(PayTimes);
				break;
			case 49:
				strFieldValue = String.valueOf(Remnant);
				break;
			case 50:
				strFieldValue = StrTool.GBKToUnicode(OldMainFeeNo);
				break;
			case 51:
				strFieldValue = StrTool.GBKToUnicode(OldCaseNo);
				break;
			case 52:
				strFieldValue = StrTool.GBKToUnicode(HosDistrict);
				break;
			case 53:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getSecuPayDate()));
				break;
			case 54:
				strFieldValue = StrTool.GBKToUnicode(MedicalType);
				break;
			case 55:
				strFieldValue = StrTool.GBKToUnicode(SecuPayType);
				break;
			case 56:
				strFieldValue = String.valueOf(Reimbursement);
				break;
			case 57:
				strFieldValue = StrTool.GBKToUnicode(ReceiptType);
				break;
			case 58:
				strFieldValue = StrTool.GBKToUnicode(IssueUnit);
				break;
			case 59:
				strFieldValue = StrTool.GBKToUnicode(MedicareType);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("MainFeeNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MainFeeNo = FValue.trim();
			}
			else
				MainFeeNo = null;
		}
		if (FCode.equalsIgnoreCase("CaseNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CaseNo = FValue.trim();
			}
			else
				CaseNo = null;
		}
		if (FCode.equalsIgnoreCase("CaseRelaNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CaseRelaNo = FValue.trim();
			}
			else
				CaseRelaNo = null;
		}
		if (FCode.equalsIgnoreCase("RgtNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RgtNo = FValue.trim();
			}
			else
				RgtNo = null;
		}
		if (FCode.equalsIgnoreCase("AffixNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AffixNo = FValue.trim();
			}
			else
				AffixNo = null;
		}
		if (FCode.equalsIgnoreCase("SerialNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				SerialNo = i;
			}
		}
		if (FCode.equalsIgnoreCase("CustomerNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerNo = FValue.trim();
			}
			else
				CustomerNo = null;
		}
		if (FCode.equalsIgnoreCase("CustomerName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerName = FValue.trim();
			}
			else
				CustomerName = null;
		}
		if (FCode.equalsIgnoreCase("CustomerSex"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerSex = FValue.trim();
			}
			else
				CustomerSex = null;
		}
		if (FCode.equalsIgnoreCase("HospitalCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HospitalCode = FValue.trim();
			}
			else
				HospitalCode = null;
		}
		if (FCode.equalsIgnoreCase("HospitalName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HospitalName = FValue.trim();
			}
			else
				HospitalName = null;
		}
		if (FCode.equalsIgnoreCase("DutyKind"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DutyKind = FValue.trim();
			}
			else
				DutyKind = null;
		}
		if (FCode.equalsIgnoreCase("ReceiptNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReceiptNo = FValue.trim();
			}
			else
				ReceiptNo = null;
		}
		if (FCode.equalsIgnoreCase("FeeType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FeeType = FValue.trim();
			}
			else
				FeeType = null;
		}
		if (FCode.equalsIgnoreCase("FeeAtti"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FeeAtti = FValue.trim();
			}
			else
				FeeAtti = null;
		}
		if (FCode.equalsIgnoreCase("FeeAffixType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FeeAffixType = FValue.trim();
			}
			else
				FeeAffixType = null;
		}
		if (FCode.equalsIgnoreCase("FeeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				FeeDate = fDate.getDate( FValue );
			}
			else
				FeeDate = null;
		}
		if (FCode.equalsIgnoreCase("HospStartDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				HospStartDate = fDate.getDate( FValue );
			}
			else
				HospStartDate = null;
		}
		if (FCode.equalsIgnoreCase("HospEndDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				HospEndDate = fDate.getDate( FValue );
			}
			else
				HospEndDate = null;
		}
		if (FCode.equalsIgnoreCase("RealHospDate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				RealHospDate = i;
			}
		}
		if (FCode.equalsIgnoreCase("SeriousWard"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				SeriousWard = i;
			}
		}
		if (FCode.equalsIgnoreCase("MngCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MngCom = FValue.trim();
			}
			else
				MngCom = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("InHosNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InHosNo = FValue.trim();
			}
			else
				InHosNo = null;
		}
		if (FCode.equalsIgnoreCase("PreAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				PreAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("SelfAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SelfAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("RefuseAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				RefuseAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("SocialPlanAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SocialPlanAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("AppendAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				AppendAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("OtherOrganAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				OtherOrganAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("OtherAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				OtherAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("PersonGiveAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				PersonGiveAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("SumFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SumFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("HosAddress"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HosAddress = FValue.trim();
			}
			else
				HosAddress = null;
		}
		if (FCode.equalsIgnoreCase("HosGrade"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HosGrade = FValue.trim();
			}
			else
				HosGrade = null;
		}
		if (FCode.equalsIgnoreCase("HosPhone"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HosPhone = FValue.trim();
			}
			else
				HosPhone = null;
		}
		if (FCode.equalsIgnoreCase("HosAtti"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HosAtti = FValue.trim();
			}
			else
				HosAtti = null;
		}
		if (FCode.equalsIgnoreCase("CompSecuNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CompSecuNo = FValue.trim();
			}
			else
				CompSecuNo = null;
		}
		if (FCode.equalsIgnoreCase("SecurityNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SecurityNo = FValue.trim();
			}
			else
				SecurityNo = null;
		}
		if (FCode.equalsIgnoreCase("Age"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				Age = i;
			}
		}
		if (FCode.equalsIgnoreCase("InsuredStat"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuredStat = FValue.trim();
			}
			else
				InsuredStat = null;
		}
		if (FCode.equalsIgnoreCase("FirstInHos"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FirstInHos = FValue.trim();
			}
			else
				FirstInHos = null;
		}
		if (FCode.equalsIgnoreCase("OriginFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OriginFlag = FValue.trim();
			}
			else
				OriginFlag = null;
		}
		if (FCode.equalsIgnoreCase("RepayFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RepayFlag = FValue.trim();
			}
			else
				RepayFlag = null;
		}
		if (FCode.equalsIgnoreCase("PayTimes"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PayTimes = i;
			}
		}
		if (FCode.equalsIgnoreCase("Remnant"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Remnant = d;
			}
		}
		if (FCode.equalsIgnoreCase("OldMainFeeNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OldMainFeeNo = FValue.trim();
			}
			else
				OldMainFeeNo = null;
		}
		if (FCode.equalsIgnoreCase("OldCaseNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OldCaseNo = FValue.trim();
			}
			else
				OldCaseNo = null;
		}
		if (FCode.equalsIgnoreCase("HosDistrict"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HosDistrict = FValue.trim();
			}
			else
				HosDistrict = null;
		}
		if (FCode.equalsIgnoreCase("SecuPayDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				SecuPayDate = fDate.getDate( FValue );
			}
			else
				SecuPayDate = null;
		}
		if (FCode.equalsIgnoreCase("MedicalType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MedicalType = FValue.trim();
			}
			else
				MedicalType = null;
		}
		if (FCode.equalsIgnoreCase("SecuPayType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SecuPayType = FValue.trim();
			}
			else
				SecuPayType = null;
		}
		if (FCode.equalsIgnoreCase("Reimbursement"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Reimbursement = d;
			}
		}
		if (FCode.equalsIgnoreCase("ReceiptType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReceiptType = FValue.trim();
			}
			else
				ReceiptType = null;
		}
		if (FCode.equalsIgnoreCase("IssueUnit"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IssueUnit = FValue.trim();
			}
			else
				IssueUnit = null;
		}
		if (FCode.equalsIgnoreCase("MedicareType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MedicareType = FValue.trim();
			}
			else
				MedicareType = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LLFeeMainSchema other = (LLFeeMainSchema)otherObject;
		return
			(MainFeeNo == null ? other.getMainFeeNo() == null : MainFeeNo.equals(other.getMainFeeNo()))
			&& (CaseNo == null ? other.getCaseNo() == null : CaseNo.equals(other.getCaseNo()))
			&& (CaseRelaNo == null ? other.getCaseRelaNo() == null : CaseRelaNo.equals(other.getCaseRelaNo()))
			&& (RgtNo == null ? other.getRgtNo() == null : RgtNo.equals(other.getRgtNo()))
			&& (AffixNo == null ? other.getAffixNo() == null : AffixNo.equals(other.getAffixNo()))
			&& SerialNo == other.getSerialNo()
			&& (CustomerNo == null ? other.getCustomerNo() == null : CustomerNo.equals(other.getCustomerNo()))
			&& (CustomerName == null ? other.getCustomerName() == null : CustomerName.equals(other.getCustomerName()))
			&& (CustomerSex == null ? other.getCustomerSex() == null : CustomerSex.equals(other.getCustomerSex()))
			&& (HospitalCode == null ? other.getHospitalCode() == null : HospitalCode.equals(other.getHospitalCode()))
			&& (HospitalName == null ? other.getHospitalName() == null : HospitalName.equals(other.getHospitalName()))
			&& (DutyKind == null ? other.getDutyKind() == null : DutyKind.equals(other.getDutyKind()))
			&& (ReceiptNo == null ? other.getReceiptNo() == null : ReceiptNo.equals(other.getReceiptNo()))
			&& (FeeType == null ? other.getFeeType() == null : FeeType.equals(other.getFeeType()))
			&& (FeeAtti == null ? other.getFeeAtti() == null : FeeAtti.equals(other.getFeeAtti()))
			&& (FeeAffixType == null ? other.getFeeAffixType() == null : FeeAffixType.equals(other.getFeeAffixType()))
			&& (FeeDate == null ? other.getFeeDate() == null : fDate.getString(FeeDate).equals(other.getFeeDate()))
			&& (HospStartDate == null ? other.getHospStartDate() == null : fDate.getString(HospStartDate).equals(other.getHospStartDate()))
			&& (HospEndDate == null ? other.getHospEndDate() == null : fDate.getString(HospEndDate).equals(other.getHospEndDate()))
			&& RealHospDate == other.getRealHospDate()
			&& SeriousWard == other.getSeriousWard()
			&& (MngCom == null ? other.getMngCom() == null : MngCom.equals(other.getMngCom()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (InHosNo == null ? other.getInHosNo() == null : InHosNo.equals(other.getInHosNo()))
			&& PreAmnt == other.getPreAmnt()
			&& SelfAmnt == other.getSelfAmnt()
			&& RefuseAmnt == other.getRefuseAmnt()
			&& SocialPlanAmnt == other.getSocialPlanAmnt()
			&& AppendAmnt == other.getAppendAmnt()
			&& OtherOrganAmnt == other.getOtherOrganAmnt()
			&& OtherAmnt == other.getOtherAmnt()
			&& PersonGiveAmnt == other.getPersonGiveAmnt()
			&& SumFee == other.getSumFee()
			&& (HosAddress == null ? other.getHosAddress() == null : HosAddress.equals(other.getHosAddress()))
			&& (HosGrade == null ? other.getHosGrade() == null : HosGrade.equals(other.getHosGrade()))
			&& (HosPhone == null ? other.getHosPhone() == null : HosPhone.equals(other.getHosPhone()))
			&& (HosAtti == null ? other.getHosAtti() == null : HosAtti.equals(other.getHosAtti()))
			&& (CompSecuNo == null ? other.getCompSecuNo() == null : CompSecuNo.equals(other.getCompSecuNo()))
			&& (SecurityNo == null ? other.getSecurityNo() == null : SecurityNo.equals(other.getSecurityNo()))
			&& Age == other.getAge()
			&& (InsuredStat == null ? other.getInsuredStat() == null : InsuredStat.equals(other.getInsuredStat()))
			&& (FirstInHos == null ? other.getFirstInHos() == null : FirstInHos.equals(other.getFirstInHos()))
			&& (OriginFlag == null ? other.getOriginFlag() == null : OriginFlag.equals(other.getOriginFlag()))
			&& (RepayFlag == null ? other.getRepayFlag() == null : RepayFlag.equals(other.getRepayFlag()))
			&& PayTimes == other.getPayTimes()
			&& Remnant == other.getRemnant()
			&& (OldMainFeeNo == null ? other.getOldMainFeeNo() == null : OldMainFeeNo.equals(other.getOldMainFeeNo()))
			&& (OldCaseNo == null ? other.getOldCaseNo() == null : OldCaseNo.equals(other.getOldCaseNo()))
			&& (HosDistrict == null ? other.getHosDistrict() == null : HosDistrict.equals(other.getHosDistrict()))
			&& (SecuPayDate == null ? other.getSecuPayDate() == null : fDate.getString(SecuPayDate).equals(other.getSecuPayDate()))
			&& (MedicalType == null ? other.getMedicalType() == null : MedicalType.equals(other.getMedicalType()))
			&& (SecuPayType == null ? other.getSecuPayType() == null : SecuPayType.equals(other.getSecuPayType()))
			&& Reimbursement == other.getReimbursement()
			&& (ReceiptType == null ? other.getReceiptType() == null : ReceiptType.equals(other.getReceiptType()))
			&& (IssueUnit == null ? other.getIssueUnit() == null : IssueUnit.equals(other.getIssueUnit()))
			&& (MedicareType == null ? other.getMedicareType() == null : MedicareType.equals(other.getMedicareType()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("MainFeeNo") ) {
			return 0;
		}
		if( strFieldName.equals("CaseNo") ) {
			return 1;
		}
		if( strFieldName.equals("CaseRelaNo") ) {
			return 2;
		}
		if( strFieldName.equals("RgtNo") ) {
			return 3;
		}
		if( strFieldName.equals("AffixNo") ) {
			return 4;
		}
		if( strFieldName.equals("SerialNo") ) {
			return 5;
		}
		if( strFieldName.equals("CustomerNo") ) {
			return 6;
		}
		if( strFieldName.equals("CustomerName") ) {
			return 7;
		}
		if( strFieldName.equals("CustomerSex") ) {
			return 8;
		}
		if( strFieldName.equals("HospitalCode") ) {
			return 9;
		}
		if( strFieldName.equals("HospitalName") ) {
			return 10;
		}
		if( strFieldName.equals("DutyKind") ) {
			return 11;
		}
		if( strFieldName.equals("ReceiptNo") ) {
			return 12;
		}
		if( strFieldName.equals("FeeType") ) {
			return 13;
		}
		if( strFieldName.equals("FeeAtti") ) {
			return 14;
		}
		if( strFieldName.equals("FeeAffixType") ) {
			return 15;
		}
		if( strFieldName.equals("FeeDate") ) {
			return 16;
		}
		if( strFieldName.equals("HospStartDate") ) {
			return 17;
		}
		if( strFieldName.equals("HospEndDate") ) {
			return 18;
		}
		if( strFieldName.equals("RealHospDate") ) {
			return 19;
		}
		if( strFieldName.equals("SeriousWard") ) {
			return 20;
		}
		if( strFieldName.equals("MngCom") ) {
			return 21;
		}
		if( strFieldName.equals("Operator") ) {
			return 22;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 23;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 24;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 25;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 26;
		}
		if( strFieldName.equals("InHosNo") ) {
			return 27;
		}
		if( strFieldName.equals("PreAmnt") ) {
			return 28;
		}
		if( strFieldName.equals("SelfAmnt") ) {
			return 29;
		}
		if( strFieldName.equals("RefuseAmnt") ) {
			return 30;
		}
		if( strFieldName.equals("SocialPlanAmnt") ) {
			return 31;
		}
		if( strFieldName.equals("AppendAmnt") ) {
			return 32;
		}
		if( strFieldName.equals("OtherOrganAmnt") ) {
			return 33;
		}
		if( strFieldName.equals("OtherAmnt") ) {
			return 34;
		}
		if( strFieldName.equals("PersonGiveAmnt") ) {
			return 35;
		}
		if( strFieldName.equals("SumFee") ) {
			return 36;
		}
		if( strFieldName.equals("HosAddress") ) {
			return 37;
		}
		if( strFieldName.equals("HosGrade") ) {
			return 38;
		}
		if( strFieldName.equals("HosPhone") ) {
			return 39;
		}
		if( strFieldName.equals("HosAtti") ) {
			return 40;
		}
		if( strFieldName.equals("CompSecuNo") ) {
			return 41;
		}
		if( strFieldName.equals("SecurityNo") ) {
			return 42;
		}
		if( strFieldName.equals("Age") ) {
			return 43;
		}
		if( strFieldName.equals("InsuredStat") ) {
			return 44;
		}
		if( strFieldName.equals("FirstInHos") ) {
			return 45;
		}
		if( strFieldName.equals("OriginFlag") ) {
			return 46;
		}
		if( strFieldName.equals("RepayFlag") ) {
			return 47;
		}
		if( strFieldName.equals("PayTimes") ) {
			return 48;
		}
		if( strFieldName.equals("Remnant") ) {
			return 49;
		}
		if( strFieldName.equals("OldMainFeeNo") ) {
			return 50;
		}
		if( strFieldName.equals("OldCaseNo") ) {
			return 51;
		}
		if( strFieldName.equals("HosDistrict") ) {
			return 52;
		}
		if( strFieldName.equals("SecuPayDate") ) {
			return 53;
		}
		if( strFieldName.equals("MedicalType") ) {
			return 54;
		}
		if( strFieldName.equals("SecuPayType") ) {
			return 55;
		}
		if( strFieldName.equals("Reimbursement") ) {
			return 56;
		}
		if( strFieldName.equals("ReceiptType") ) {
			return 57;
		}
		if( strFieldName.equals("IssueUnit") ) {
			return 58;
		}
		if( strFieldName.equals("MedicareType") ) {
			return 59;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "MainFeeNo";
				break;
			case 1:
				strFieldName = "CaseNo";
				break;
			case 2:
				strFieldName = "CaseRelaNo";
				break;
			case 3:
				strFieldName = "RgtNo";
				break;
			case 4:
				strFieldName = "AffixNo";
				break;
			case 5:
				strFieldName = "SerialNo";
				break;
			case 6:
				strFieldName = "CustomerNo";
				break;
			case 7:
				strFieldName = "CustomerName";
				break;
			case 8:
				strFieldName = "CustomerSex";
				break;
			case 9:
				strFieldName = "HospitalCode";
				break;
			case 10:
				strFieldName = "HospitalName";
				break;
			case 11:
				strFieldName = "DutyKind";
				break;
			case 12:
				strFieldName = "ReceiptNo";
				break;
			case 13:
				strFieldName = "FeeType";
				break;
			case 14:
				strFieldName = "FeeAtti";
				break;
			case 15:
				strFieldName = "FeeAffixType";
				break;
			case 16:
				strFieldName = "FeeDate";
				break;
			case 17:
				strFieldName = "HospStartDate";
				break;
			case 18:
				strFieldName = "HospEndDate";
				break;
			case 19:
				strFieldName = "RealHospDate";
				break;
			case 20:
				strFieldName = "SeriousWard";
				break;
			case 21:
				strFieldName = "MngCom";
				break;
			case 22:
				strFieldName = "Operator";
				break;
			case 23:
				strFieldName = "MakeDate";
				break;
			case 24:
				strFieldName = "MakeTime";
				break;
			case 25:
				strFieldName = "ModifyDate";
				break;
			case 26:
				strFieldName = "ModifyTime";
				break;
			case 27:
				strFieldName = "InHosNo";
				break;
			case 28:
				strFieldName = "PreAmnt";
				break;
			case 29:
				strFieldName = "SelfAmnt";
				break;
			case 30:
				strFieldName = "RefuseAmnt";
				break;
			case 31:
				strFieldName = "SocialPlanAmnt";
				break;
			case 32:
				strFieldName = "AppendAmnt";
				break;
			case 33:
				strFieldName = "OtherOrganAmnt";
				break;
			case 34:
				strFieldName = "OtherAmnt";
				break;
			case 35:
				strFieldName = "PersonGiveAmnt";
				break;
			case 36:
				strFieldName = "SumFee";
				break;
			case 37:
				strFieldName = "HosAddress";
				break;
			case 38:
				strFieldName = "HosGrade";
				break;
			case 39:
				strFieldName = "HosPhone";
				break;
			case 40:
				strFieldName = "HosAtti";
				break;
			case 41:
				strFieldName = "CompSecuNo";
				break;
			case 42:
				strFieldName = "SecurityNo";
				break;
			case 43:
				strFieldName = "Age";
				break;
			case 44:
				strFieldName = "InsuredStat";
				break;
			case 45:
				strFieldName = "FirstInHos";
				break;
			case 46:
				strFieldName = "OriginFlag";
				break;
			case 47:
				strFieldName = "RepayFlag";
				break;
			case 48:
				strFieldName = "PayTimes";
				break;
			case 49:
				strFieldName = "Remnant";
				break;
			case 50:
				strFieldName = "OldMainFeeNo";
				break;
			case 51:
				strFieldName = "OldCaseNo";
				break;
			case 52:
				strFieldName = "HosDistrict";
				break;
			case 53:
				strFieldName = "SecuPayDate";
				break;
			case 54:
				strFieldName = "MedicalType";
				break;
			case 55:
				strFieldName = "SecuPayType";
				break;
			case 56:
				strFieldName = "Reimbursement";
				break;
			case 57:
				strFieldName = "ReceiptType";
				break;
			case 58:
				strFieldName = "IssueUnit";
				break;
			case 59:
				strFieldName = "MedicareType";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("MainFeeNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CaseNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CaseRelaNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RgtNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AffixNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SerialNo") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("CustomerNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerSex") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HospitalCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HospitalName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DutyKind") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReceiptNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FeeType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FeeAtti") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FeeAffixType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FeeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("HospStartDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("HospEndDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("RealHospDate") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("SeriousWard") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("MngCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InHosNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PreAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SelfAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("RefuseAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SocialPlanAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("AppendAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("OtherOrganAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("OtherAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("PersonGiveAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SumFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("HosAddress") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HosGrade") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HosPhone") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HosAtti") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CompSecuNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SecurityNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Age") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("InsuredStat") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FirstInHos") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OriginFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RepayFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayTimes") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("Remnant") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("OldMainFeeNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OldCaseNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HosDistrict") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SecuPayDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MedicalType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SecuPayType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Reimbursement") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ReceiptType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IssueUnit") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MedicareType") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_INT;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 17:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 18:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 19:
				nFieldType = Schema.TYPE_INT;
				break;
			case 20:
				nFieldType = Schema.TYPE_INT;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 29:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 30:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 31:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 32:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 33:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 34:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 35:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 36:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 37:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 38:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 39:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 40:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 41:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 42:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 43:
				nFieldType = Schema.TYPE_INT;
				break;
			case 44:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 45:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 46:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 47:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 48:
				nFieldType = Schema.TYPE_INT;
				break;
			case 49:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 50:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 51:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 52:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 53:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 54:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 55:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 56:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 57:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 58:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 59:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
