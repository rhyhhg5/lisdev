/*
 * <p>ClassName: LFComputeReportDataLogSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: MSREPORT
 * @CreateDate：2005-01-10
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LFComputeReportDataLogDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LFComputeReportDataLogSchema implements Schema
{
    // @Field
    /** 流水号 */
    private String ReportSN;
    /** 保监会机构编码 */
    private String ComCodeISC;
    /** 科目类型 */
    private String ItemType;
    /** 统计年 */
    private int StatYear;
    /** 统计月 */
    private int StatMon;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;

    public static final int FIELDNUM = 7; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LFComputeReportDataLogSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[0];

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getReportSN()
    {
        if (ReportSN != null && !ReportSN.equals("") && SysConst.CHANGECHARSET == true)
        {
            ReportSN = StrTool.unicodeToGBK(ReportSN);
        }
        return ReportSN;
    }

    public void setReportSN(String aReportSN)
    {
        ReportSN = aReportSN;
    }

    public String getComCodeISC()
    {
        if (ComCodeISC != null && !ComCodeISC.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ComCodeISC = StrTool.unicodeToGBK(ComCodeISC);
        }
        return ComCodeISC;
    }

    public void setComCodeISC(String aComCodeISC)
    {
        ComCodeISC = aComCodeISC;
    }

    public String getItemType()
    {
        if (ItemType != null && !ItemType.equals("") && SysConst.CHANGECHARSET == true)
        {
            ItemType = StrTool.unicodeToGBK(ItemType);
        }
        return ItemType;
    }

    public void setItemType(String aItemType)
    {
        ItemType = aItemType;
    }

    public int getStatYear()
    {
        return StatYear;
    }

    public void setStatYear(int aStatYear)
    {
        StatYear = aStatYear;
    }

    public void setStatYear(String aStatYear)
    {
        if (aStatYear != null && !aStatYear.equals(""))
        {
            Integer tInteger = new Integer(aStatYear);
            int i = tInteger.intValue();
            StatYear = i;
        }
    }

    public int getStatMon()
    {
        return StatMon;
    }

    public void setStatMon(int aStatMon)
    {
        StatMon = aStatMon;
    }

    public void setStatMon(String aStatMon)
    {
        if (aStatMon != null && !aStatMon.equals(""))
        {
            Integer tInteger = new Integer(aStatMon);
            int i = tInteger.intValue();
            StatMon = i;
        }
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    /**
     * 使用另外一个 LFComputeReportDataLogSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LFComputeReportDataLogSchema
                          aLFComputeReportDataLogSchema)
    {
        this.ReportSN = aLFComputeReportDataLogSchema.getReportSN();
        this.ComCodeISC = aLFComputeReportDataLogSchema.getComCodeISC();
        this.ItemType = aLFComputeReportDataLogSchema.getItemType();
        this.StatYear = aLFComputeReportDataLogSchema.getStatYear();
        this.StatMon = aLFComputeReportDataLogSchema.getStatMon();
        this.MakeDate = fDate.getDate(aLFComputeReportDataLogSchema.getMakeDate());
        this.MakeTime = aLFComputeReportDataLogSchema.getMakeTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ReportSN") == null)
            {
                this.ReportSN = null;
            }
            else
            {
                this.ReportSN = rs.getString("ReportSN").trim();
            }

            if (rs.getString("ComCodeISC") == null)
            {
                this.ComCodeISC = null;
            }
            else
            {
                this.ComCodeISC = rs.getString("ComCodeISC").trim();
            }

            if (rs.getString("ItemType") == null)
            {
                this.ItemType = null;
            }
            else
            {
                this.ItemType = rs.getString("ItemType").trim();
            }

            this.StatYear = rs.getInt("StatYear");
            this.StatMon = rs.getInt("StatMon");
            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LFComputeReportDataLogSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LFComputeReportDataLogSchema getSchema()
    {
        LFComputeReportDataLogSchema aLFComputeReportDataLogSchema = new
                LFComputeReportDataLogSchema();
        aLFComputeReportDataLogSchema.setSchema(this);
        return aLFComputeReportDataLogSchema;
    }

    public LFComputeReportDataLogDB getDB()
    {
        LFComputeReportDataLogDB aDBOper = new LFComputeReportDataLogDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLFComputeReportDataLog描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(ReportSN)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ComCodeISC)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ItemType)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(StatYear) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(StatMon) + SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLFComputeReportDataLog>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            ReportSN = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            ComCodeISC = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
            ItemType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            StatYear = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 4, SysConst.PACKAGESPILTER))).intValue();
            StatMon = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 5, SysConst.PACKAGESPILTER))).intValue();
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 6, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                      SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LFComputeReportDataLogSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("ReportSN"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ReportSN));
        }
        if (FCode.equals("ComCodeISC"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ComCodeISC));
        }
        if (FCode.equals("ItemType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ItemType));
        }
        if (FCode.equals("StatYear"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(StatYear));
        }
        if (FCode.equals("StatMon"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(StatMon));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ReportSN);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ComCodeISC);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ItemType);
                break;
            case 3:
                strFieldValue = String.valueOf(StatYear);
                break;
            case 4:
                strFieldValue = String.valueOf(StatMon);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("ReportSN"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ReportSN = FValue.trim();
            }
            else
            {
                ReportSN = null;
            }
        }
        if (FCode.equals("ComCodeISC"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ComCodeISC = FValue.trim();
            }
            else
            {
                ComCodeISC = null;
            }
        }
        if (FCode.equals("ItemType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ItemType = FValue.trim();
            }
            else
            {
                ItemType = null;
            }
        }
        if (FCode.equals("StatYear"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                StatYear = i;
            }
        }
        if (FCode.equals("StatMon"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                StatMon = i;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LFComputeReportDataLogSchema other = (LFComputeReportDataLogSchema)
                                             otherObject;
        return
                ReportSN.equals(other.getReportSN())
                && ComCodeISC.equals(other.getComCodeISC())
                && ItemType.equals(other.getItemType())
                && StatYear == other.getStatYear()
                && StatMon == other.getStatMon()
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("ReportSN"))
        {
            return 0;
        }
        if (strFieldName.equals("ComCodeISC"))
        {
            return 1;
        }
        if (strFieldName.equals("ItemType"))
        {
            return 2;
        }
        if (strFieldName.equals("StatYear"))
        {
            return 3;
        }
        if (strFieldName.equals("StatMon"))
        {
            return 4;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 5;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 6;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "ReportSN";
                break;
            case 1:
                strFieldName = "ComCodeISC";
                break;
            case 2:
                strFieldName = "ItemType";
                break;
            case 3:
                strFieldName = "StatYear";
                break;
            case 4:
                strFieldName = "StatMon";
                break;
            case 5:
                strFieldName = "MakeDate";
                break;
            case 6:
                strFieldName = "MakeTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("ReportSN"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ComCodeISC"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ItemType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StatYear"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("StatMon"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_INT;
                break;
            case 4:
                nFieldType = Schema.TYPE_INT;
                break;
            case 5:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
