/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LCPaySerialnoHZDB;

/*
 * <p>ClassName: LCPaySerialnoHZSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 手续费付费
 * @CreateDate：2017-06-08
 */
public class LCPaySerialnoHZSchema implements Schema, Cloneable
{
	// @Field
	/** 交易流水号 */
	private String TranSerial;
	/** 所属组织 */
	private String Pk_Org;
	/** 结算日期 */
	private String VDate;
	/** 来源系统 */
	private String SrcSysItem;
	/** 业务日期 */
	private String BusiDate;
	/** 收款方名称 */
	private String CustName;
	/** 含税金额 */
	private double LocalTotalAmt;
	/** 交易批次号 */
	private String TranBatch;
	/** 产品代码 */
	private String ProCode;
	/** 收支项目 */
	private String BusiNo;
	/** 被冲销标识 */
	private String CzType;
	/** 冲销人 */
	private String AccPsn;
	/** 冲销日期 */
	private String AccDate;
	/** 创建人 */
	private String Operator;
	/** 创建日期 */
	private String MakeDate;
	/** 创建时间 */
	private String MakeTime;
	/** 修改人 */
	private String Modifier;
	/** 修改日期 */
	private String ModifyDate;
	/** 修改时间 */
	private String ModifyTime;
	/** 核心是否已读取 */
	private String State;
	/** 是否应税险种 */
	private String TaxStr;
	/** 匹配发票状态 */
	private int RelatBillStatus;
	/** 关联日期 */
	private String ConnectDate;
	/** 票据号码 */
	private String BillNum;
	/** 票据代码 */
	private String BillCode;
	/** 税款日期 */
	private String TaxPeriod;
	/** 税票类型 */
	private int TaxBillType;
	/** 发票不含税金额 */
	private double LocalAmt;
	/** 税率(%) */
	private String TaxRate;
	/** 发票税额 */
	private double LocalTax;
	/** 手续费不含税金额 */
	private double BillTaxAmt;
	/** 手续费税额 */
	private double BillTaxRate;
	/** 转出税额 */
	private double ReturnRate;
	/** 进项交易流水接收日期 */
	private String Receive_Date;
	/** 进项交易流水接收状态 */
	private String Receive_State;
	/** 进项交易流水接收失败原因 */
	private String Fail_Reason;
	/** 进项转出接收日期 */
	private String Receive_Date_Z;
	/** 进项转出接收状态 */
	private String Receive_State_Z;
	/** 进项转出接收失败原因 */
	private String Fail_Reason_Z;
	/** 时间戳 */
	private String TS;

	public static final int FIELDNUM = 40;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LCPaySerialnoHZSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "TranSerial";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LCPaySerialnoHZSchema cloned = (LCPaySerialnoHZSchema) super.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getTranSerial()
	{
		return TranSerial;
	}
	public void setTranSerial(String aTranSerial)
	{
		TranSerial = aTranSerial;
	}
	public String getPk_Org()
	{
		return Pk_Org;
	}
	public void setPk_Org(String aPk_Org)
	{
		Pk_Org = aPk_Org;
	}
	public String getVDate()
	{
		return VDate;
	}
	public void setVDate(String aVDate)
	{
		VDate = aVDate;
	}
	public String getSrcSysItem()
	{
		return SrcSysItem;
	}
	public void setSrcSysItem(String aSrcSysItem)
	{
		SrcSysItem = aSrcSysItem;
	}
	public String getBusiDate()
	{
		return BusiDate;
	}
	public void setBusiDate(String aBusiDate)
	{
		BusiDate = aBusiDate;
	}
	public String getCustName()
	{
		return CustName;
	}
	public void setCustName(String aCustName)
	{
		CustName = aCustName;
	}
	public double getLocalTotalAmt()
	{
		return LocalTotalAmt;
	}
	public void setLocalTotalAmt(double aLocalTotalAmt)
	{
		LocalTotalAmt = Arith.round(aLocalTotalAmt,8);
	}
	public void setLocalTotalAmt(String aLocalTotalAmt)
	{
		if (aLocalTotalAmt != null && !aLocalTotalAmt.equals(""))
		{
			Double tDouble = new Double(aLocalTotalAmt);
			double d = tDouble.doubleValue();
                LocalTotalAmt = Arith.round(d,8);
		}
	}

	public String getTranBatch()
	{
		return TranBatch;
	}
	public void setTranBatch(String aTranBatch)
	{
		TranBatch = aTranBatch;
	}
	public String getProCode()
	{
		return ProCode;
	}
	public void setProCode(String aProCode)
	{
		ProCode = aProCode;
	}
	public String getBusiNo()
	{
		return BusiNo;
	}
	public void setBusiNo(String aBusiNo)
	{
		BusiNo = aBusiNo;
	}
	public String getCzType()
	{
		return CzType;
	}
	public void setCzType(String aCzType)
	{
		CzType = aCzType;
	}
	public String getAccPsn()
	{
		return AccPsn;
	}
	public void setAccPsn(String aAccPsn)
	{
		AccPsn = aAccPsn;
	}
	public String getAccDate()
	{
		return AccDate;
	}
	public void setAccDate(String aAccDate)
	{
		AccDate = aAccDate;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		return MakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifier()
	{
		return Modifier;
	}
	public void setModifier(String aModifier)
	{
		Modifier = aModifier;
	}
	public String getModifyDate()
	{
		return ModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}
	public String getTaxStr()
	{
		return TaxStr;
	}
	public void setTaxStr(String aTaxStr)
	{
		TaxStr = aTaxStr;
	}
	public int getRelatBillStatus()
	{
		return RelatBillStatus;
	}
	public void setRelatBillStatus(int aRelatBillStatus)
	{
		RelatBillStatus = aRelatBillStatus;
	}
	public void setRelatBillStatus(String aRelatBillStatus)
	{
		if (aRelatBillStatus != null && !aRelatBillStatus.equals(""))
		{
			Integer tInteger = new Integer(aRelatBillStatus);
			int i = tInteger.intValue();
			RelatBillStatus = i;
		}
	}

	public String getConnectDate()
	{
		return ConnectDate;
	}
	public void setConnectDate(String aConnectDate)
	{
		ConnectDate = aConnectDate;
	}
	public String getBillNum()
	{
		return BillNum;
	}
	public void setBillNum(String aBillNum)
	{
		BillNum = aBillNum;
	}
	public String getBillCode()
	{
		return BillCode;
	}
	public void setBillCode(String aBillCode)
	{
		BillCode = aBillCode;
	}
	public String getTaxPeriod()
	{
		return TaxPeriod;
	}
	public void setTaxPeriod(String aTaxPeriod)
	{
		TaxPeriod = aTaxPeriod;
	}
	public int getTaxBillType()
	{
		return TaxBillType;
	}
	public void setTaxBillType(int aTaxBillType)
	{
		TaxBillType = aTaxBillType;
	}
	public void setTaxBillType(String aTaxBillType)
	{
		if (aTaxBillType != null && !aTaxBillType.equals(""))
		{
			Integer tInteger = new Integer(aTaxBillType);
			int i = tInteger.intValue();
			TaxBillType = i;
		}
	}

	public double getLocalAmt()
	{
		return LocalAmt;
	}
	public void setLocalAmt(double aLocalAmt)
	{
		LocalAmt = Arith.round(aLocalAmt,8);
	}
	public void setLocalAmt(String aLocalAmt)
	{
		if (aLocalAmt != null && !aLocalAmt.equals(""))
		{
			Double tDouble = new Double(aLocalAmt);
			double d = tDouble.doubleValue();
                LocalAmt = Arith.round(d,8);
		}
	}

	public String getTaxRate()
	{
		return TaxRate;
	}
	public void setTaxRate(String aTaxRate)
	{
		TaxRate = aTaxRate;
	}
	public double getLocalTax()
	{
		return LocalTax;
	}
	public void setLocalTax(double aLocalTax)
	{
		LocalTax = Arith.round(aLocalTax,8);
	}
	public void setLocalTax(String aLocalTax)
	{
		if (aLocalTax != null && !aLocalTax.equals(""))
		{
			Double tDouble = new Double(aLocalTax);
			double d = tDouble.doubleValue();
                LocalTax = Arith.round(d,8);
		}
	}

	public double getBillTaxAmt()
	{
		return BillTaxAmt;
	}
	public void setBillTaxAmt(double aBillTaxAmt)
	{
		BillTaxAmt = Arith.round(aBillTaxAmt,8);
	}
	public void setBillTaxAmt(String aBillTaxAmt)
	{
		if (aBillTaxAmt != null && !aBillTaxAmt.equals(""))
		{
			Double tDouble = new Double(aBillTaxAmt);
			double d = tDouble.doubleValue();
                BillTaxAmt = Arith.round(d,8);
		}
	}

	public double getBillTaxRate()
	{
		return BillTaxRate;
	}
	public void setBillTaxRate(double aBillTaxRate)
	{
		BillTaxRate = Arith.round(aBillTaxRate,8);
	}
	public void setBillTaxRate(String aBillTaxRate)
	{
		if (aBillTaxRate != null && !aBillTaxRate.equals(""))
		{
			Double tDouble = new Double(aBillTaxRate);
			double d = tDouble.doubleValue();
                BillTaxRate = Arith.round(d,8);
		}
	}

	public double getReturnRate()
	{
		return ReturnRate;
	}
	public void setReturnRate(double aReturnRate)
	{
		ReturnRate = Arith.round(aReturnRate,8);
	}
	public void setReturnRate(String aReturnRate)
	{
		if (aReturnRate != null && !aReturnRate.equals(""))
		{
			Double tDouble = new Double(aReturnRate);
			double d = tDouble.doubleValue();
                ReturnRate = Arith.round(d,8);
		}
	}

	public String getReceive_Date()
	{
		return Receive_Date;
	}
	public void setReceive_Date(String aReceive_Date)
	{
		Receive_Date = aReceive_Date;
	}
	public String getReceive_State()
	{
		return Receive_State;
	}
	public void setReceive_State(String aReceive_State)
	{
		Receive_State = aReceive_State;
	}
	public String getFail_Reason()
	{
		return Fail_Reason;
	}
	public void setFail_Reason(String aFail_Reason)
	{
		Fail_Reason = aFail_Reason;
	}
	public String getReceive_Date_Z()
	{
		return Receive_Date_Z;
	}
	public void setReceive_Date_Z(String aReceive_Date_Z)
	{
		Receive_Date_Z = aReceive_Date_Z;
	}
	public String getReceive_State_Z()
	{
		return Receive_State_Z;
	}
	public void setReceive_State_Z(String aReceive_State_Z)
	{
		Receive_State_Z = aReceive_State_Z;
	}
	public String getFail_Reason_Z()
	{
		return Fail_Reason_Z;
	}
	public void setFail_Reason_Z(String aFail_Reason_Z)
	{
		Fail_Reason_Z = aFail_Reason_Z;
	}
	public String getTS()
	{
		return TS;
	}
	public void setTS(String aTS)
	{
		TS = aTS;
	}

	/**
	* 使用另外一个 LCPaySerialnoHZSchema 对象给 Schema 赋值
	* @param: aLCPaySerialnoHZSchema LCPaySerialnoHZSchema
	**/
	public void setSchema(LCPaySerialnoHZSchema aLCPaySerialnoHZSchema)
	{
		this.TranSerial = aLCPaySerialnoHZSchema.getTranSerial();
		this.Pk_Org = aLCPaySerialnoHZSchema.getPk_Org();
		this.VDate = aLCPaySerialnoHZSchema.getVDate();
		this.SrcSysItem = aLCPaySerialnoHZSchema.getSrcSysItem();
		this.BusiDate = aLCPaySerialnoHZSchema.getBusiDate();
		this.CustName = aLCPaySerialnoHZSchema.getCustName();
		this.LocalTotalAmt = aLCPaySerialnoHZSchema.getLocalTotalAmt();
		this.TranBatch = aLCPaySerialnoHZSchema.getTranBatch();
		this.ProCode = aLCPaySerialnoHZSchema.getProCode();
		this.BusiNo = aLCPaySerialnoHZSchema.getBusiNo();
		this.CzType = aLCPaySerialnoHZSchema.getCzType();
		this.AccPsn = aLCPaySerialnoHZSchema.getAccPsn();
		this.AccDate = aLCPaySerialnoHZSchema.getAccDate();
		this.Operator = aLCPaySerialnoHZSchema.getOperator();
		this.MakeDate = aLCPaySerialnoHZSchema.getMakeDate();
		this.MakeTime = aLCPaySerialnoHZSchema.getMakeTime();
		this.Modifier = aLCPaySerialnoHZSchema.getModifier();
		this.ModifyDate = aLCPaySerialnoHZSchema.getModifyDate();
		this.ModifyTime = aLCPaySerialnoHZSchema.getModifyTime();
		this.State = aLCPaySerialnoHZSchema.getState();
		this.TaxStr = aLCPaySerialnoHZSchema.getTaxStr();
		this.RelatBillStatus = aLCPaySerialnoHZSchema.getRelatBillStatus();
		this.ConnectDate = aLCPaySerialnoHZSchema.getConnectDate();
		this.BillNum = aLCPaySerialnoHZSchema.getBillNum();
		this.BillCode = aLCPaySerialnoHZSchema.getBillCode();
		this.TaxPeriod = aLCPaySerialnoHZSchema.getTaxPeriod();
		this.TaxBillType = aLCPaySerialnoHZSchema.getTaxBillType();
		this.LocalAmt = aLCPaySerialnoHZSchema.getLocalAmt();
		this.TaxRate = aLCPaySerialnoHZSchema.getTaxRate();
		this.LocalTax = aLCPaySerialnoHZSchema.getLocalTax();
		this.BillTaxAmt = aLCPaySerialnoHZSchema.getBillTaxAmt();
		this.BillTaxRate = aLCPaySerialnoHZSchema.getBillTaxRate();
		this.ReturnRate = aLCPaySerialnoHZSchema.getReturnRate();
		this.Receive_Date = aLCPaySerialnoHZSchema.getReceive_Date();
		this.Receive_State = aLCPaySerialnoHZSchema.getReceive_State();
		this.Fail_Reason = aLCPaySerialnoHZSchema.getFail_Reason();
		this.Receive_Date_Z = aLCPaySerialnoHZSchema.getReceive_Date_Z();
		this.Receive_State_Z = aLCPaySerialnoHZSchema.getReceive_State_Z();
		this.Fail_Reason_Z = aLCPaySerialnoHZSchema.getFail_Reason_Z();
		this.TS = aLCPaySerialnoHZSchema.getTS();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("TranSerial") == null )
				this.TranSerial = null;
			else
				this.TranSerial = rs.getString("TranSerial").trim();

			if( rs.getString("Pk_Org") == null )
				this.Pk_Org = null;
			else
				this.Pk_Org = rs.getString("Pk_Org").trim();

			if( rs.getString("VDate") == null )
				this.VDate = null;
			else
				this.VDate = rs.getString("VDate").trim();

			if( rs.getString("SrcSysItem") == null )
				this.SrcSysItem = null;
			else
				this.SrcSysItem = rs.getString("SrcSysItem").trim();

			if( rs.getString("BusiDate") == null )
				this.BusiDate = null;
			else
				this.BusiDate = rs.getString("BusiDate").trim();

			if( rs.getString("CustName") == null )
				this.CustName = null;
			else
				this.CustName = rs.getString("CustName").trim();

			this.LocalTotalAmt = rs.getDouble("LocalTotalAmt");
			if( rs.getString("TranBatch") == null )
				this.TranBatch = null;
			else
				this.TranBatch = rs.getString("TranBatch").trim();

			if( rs.getString("ProCode") == null )
				this.ProCode = null;
			else
				this.ProCode = rs.getString("ProCode").trim();

			if( rs.getString("BusiNo") == null )
				this.BusiNo = null;
			else
				this.BusiNo = rs.getString("BusiNo").trim();

			if( rs.getString("CzType") == null )
				this.CzType = null;
			else
				this.CzType = rs.getString("CzType").trim();

			if( rs.getString("AccPsn") == null )
				this.AccPsn = null;
			else
				this.AccPsn = rs.getString("AccPsn").trim();

			if( rs.getString("AccDate") == null )
				this.AccDate = null;
			else
				this.AccDate = rs.getString("AccDate").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			if( rs.getString("MakeDate") == null )
				this.MakeDate = null;
			else
				this.MakeDate = rs.getString("MakeDate").trim();

			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			if( rs.getString("Modifier") == null )
				this.Modifier = null;
			else
				this.Modifier = rs.getString("Modifier").trim();

			if( rs.getString("ModifyDate") == null )
				this.ModifyDate = null;
			else
				this.ModifyDate = rs.getString("ModifyDate").trim();

			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			if( rs.getString("TaxStr") == null )
				this.TaxStr = null;
			else
				this.TaxStr = rs.getString("TaxStr").trim();

			this.RelatBillStatus = rs.getInt("RelatBillStatus");
			if( rs.getString("ConnectDate") == null )
				this.ConnectDate = null;
			else
				this.ConnectDate = rs.getString("ConnectDate").trim();

			if( rs.getString("BillNum") == null )
				this.BillNum = null;
			else
				this.BillNum = rs.getString("BillNum").trim();

			if( rs.getString("BillCode") == null )
				this.BillCode = null;
			else
				this.BillCode = rs.getString("BillCode").trim();

			if( rs.getString("TaxPeriod") == null )
				this.TaxPeriod = null;
			else
				this.TaxPeriod = rs.getString("TaxPeriod").trim();

			this.TaxBillType = rs.getInt("TaxBillType");
			this.LocalAmt = rs.getDouble("LocalAmt");
			if( rs.getString("TaxRate") == null )
				this.TaxRate = null;
			else
				this.TaxRate = rs.getString("TaxRate").trim();

			this.LocalTax = rs.getDouble("LocalTax");
			this.BillTaxAmt = rs.getDouble("BillTaxAmt");
			this.BillTaxRate = rs.getDouble("BillTaxRate");
			this.ReturnRate = rs.getDouble("ReturnRate");
			if( rs.getString("Receive_Date") == null )
				this.Receive_Date = null;
			else
				this.Receive_Date = rs.getString("Receive_Date").trim();

			if( rs.getString("Receive_State") == null )
				this.Receive_State = null;
			else
				this.Receive_State = rs.getString("Receive_State").trim();

			if( rs.getString("Fail_Reason") == null )
				this.Fail_Reason = null;
			else
				this.Fail_Reason = rs.getString("Fail_Reason").trim();

			if( rs.getString("Receive_Date_Z") == null )
				this.Receive_Date_Z = null;
			else
				this.Receive_Date_Z = rs.getString("Receive_Date_Z").trim();

			if( rs.getString("Receive_State_Z") == null )
				this.Receive_State_Z = null;
			else
				this.Receive_State_Z = rs.getString("Receive_State_Z").trim();

			if( rs.getString("Fail_Reason_Z") == null )
				this.Fail_Reason_Z = null;
			else
				this.Fail_Reason_Z = rs.getString("Fail_Reason_Z").trim();

			if( rs.getString("TS") == null )
				this.TS = null;
			else
				this.TS = rs.getString("TS").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LCPaySerialnoHZ表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCPaySerialnoHZSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LCPaySerialnoHZSchema getSchema()
	{
		LCPaySerialnoHZSchema aLCPaySerialnoHZSchema = new LCPaySerialnoHZSchema();
		aLCPaySerialnoHZSchema.setSchema(this);
		return aLCPaySerialnoHZSchema;
	}

	public LCPaySerialnoHZDB getDB()
	{
		LCPaySerialnoHZDB aDBOper = new LCPaySerialnoHZDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCPaySerialnoHZ描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(TranSerial)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Pk_Org)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(VDate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SrcSysItem)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusiDate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(LocalTotalAmt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TranBatch)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusiNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CzType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccPsn)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccDate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeDate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Modifier)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyDate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TaxStr)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(RelatBillStatus));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ConnectDate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BillNum)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BillCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TaxPeriod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(TaxBillType));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(LocalAmt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TaxRate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(LocalTax));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(BillTaxAmt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(BillTaxRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ReturnRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Receive_Date)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Receive_State)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Fail_Reason)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Receive_Date_Z)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Receive_State_Z)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Fail_Reason_Z)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TS));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCPaySerialnoHZ>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			TranSerial = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			Pk_Org = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			VDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			SrcSysItem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			BusiDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			CustName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			LocalTotalAmt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,7,SysConst.PACKAGESPILTER))).doubleValue();
			TranBatch = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			ProCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			BusiNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			CzType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			AccPsn = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			AccDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			MakeDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			Modifier = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			ModifyDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			TaxStr = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			RelatBillStatus= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,22,SysConst.PACKAGESPILTER))).intValue();
			ConnectDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			BillNum = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			BillCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			TaxPeriod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			TaxBillType= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,27,SysConst.PACKAGESPILTER))).intValue();
			LocalAmt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,28,SysConst.PACKAGESPILTER))).doubleValue();
			TaxRate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			LocalTax = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,30,SysConst.PACKAGESPILTER))).doubleValue();
			BillTaxAmt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,31,SysConst.PACKAGESPILTER))).doubleValue();
			BillTaxRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,32,SysConst.PACKAGESPILTER))).doubleValue();
			ReturnRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,33,SysConst.PACKAGESPILTER))).doubleValue();
			Receive_Date = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			Receive_State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			Fail_Reason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
			Receive_Date_Z = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
			Receive_State_Z = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
			Fail_Reason_Z = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
			TS = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCPaySerialnoHZSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("TranSerial"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TranSerial));
		}
		if (FCode.equals("Pk_Org"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Pk_Org));
		}
		if (FCode.equals("VDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(VDate));
		}
		if (FCode.equals("SrcSysItem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SrcSysItem));
		}
		if (FCode.equals("BusiDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusiDate));
		}
		if (FCode.equals("CustName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustName));
		}
		if (FCode.equals("LocalTotalAmt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LocalTotalAmt));
		}
		if (FCode.equals("TranBatch"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TranBatch));
		}
		if (FCode.equals("ProCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProCode));
		}
		if (FCode.equals("BusiNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusiNo));
		}
		if (FCode.equals("CzType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CzType));
		}
		if (FCode.equals("AccPsn"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccPsn));
		}
		if (FCode.equals("AccDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccDate));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("Modifier"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Modifier));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("TaxStr"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TaxStr));
		}
		if (FCode.equals("RelatBillStatus"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RelatBillStatus));
		}
		if (FCode.equals("ConnectDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ConnectDate));
		}
		if (FCode.equals("BillNum"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BillNum));
		}
		if (FCode.equals("BillCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BillCode));
		}
		if (FCode.equals("TaxPeriod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TaxPeriod));
		}
		if (FCode.equals("TaxBillType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TaxBillType));
		}
		if (FCode.equals("LocalAmt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LocalAmt));
		}
		if (FCode.equals("TaxRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TaxRate));
		}
		if (FCode.equals("LocalTax"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LocalTax));
		}
		if (FCode.equals("BillTaxAmt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BillTaxAmt));
		}
		if (FCode.equals("BillTaxRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BillTaxRate));
		}
		if (FCode.equals("ReturnRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReturnRate));
		}
		if (FCode.equals("Receive_Date"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Receive_Date));
		}
		if (FCode.equals("Receive_State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Receive_State));
		}
		if (FCode.equals("Fail_Reason"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Fail_Reason));
		}
		if (FCode.equals("Receive_Date_Z"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Receive_Date_Z));
		}
		if (FCode.equals("Receive_State_Z"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Receive_State_Z));
		}
		if (FCode.equals("Fail_Reason_Z"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Fail_Reason_Z));
		}
		if (FCode.equals("TS"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TS));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(TranSerial);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(Pk_Org);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(VDate);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(SrcSysItem);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(BusiDate);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(CustName);
				break;
			case 6:
				strFieldValue = String.valueOf(LocalTotalAmt);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(TranBatch);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(ProCode);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(BusiNo);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(CzType);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(AccPsn);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(AccDate);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(MakeDate);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(Modifier);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(ModifyDate);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(TaxStr);
				break;
			case 21:
				strFieldValue = String.valueOf(RelatBillStatus);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(ConnectDate);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(BillNum);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(BillCode);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(TaxPeriod);
				break;
			case 26:
				strFieldValue = String.valueOf(TaxBillType);
				break;
			case 27:
				strFieldValue = String.valueOf(LocalAmt);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(TaxRate);
				break;
			case 29:
				strFieldValue = String.valueOf(LocalTax);
				break;
			case 30:
				strFieldValue = String.valueOf(BillTaxAmt);
				break;
			case 31:
				strFieldValue = String.valueOf(BillTaxRate);
				break;
			case 32:
				strFieldValue = String.valueOf(ReturnRate);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(Receive_Date);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(Receive_State);
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(Fail_Reason);
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(Receive_Date_Z);
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(Receive_State_Z);
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(Fail_Reason_Z);
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(TS);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("TranSerial"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TranSerial = FValue.trim();
			}
			else
				TranSerial = null;
		}
		if (FCode.equalsIgnoreCase("Pk_Org"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Pk_Org = FValue.trim();
			}
			else
				Pk_Org = null;
		}
		if (FCode.equalsIgnoreCase("VDate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				VDate = FValue.trim();
			}
			else
				VDate = null;
		}
		if (FCode.equalsIgnoreCase("SrcSysItem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SrcSysItem = FValue.trim();
			}
			else
				SrcSysItem = null;
		}
		if (FCode.equalsIgnoreCase("BusiDate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusiDate = FValue.trim();
			}
			else
				BusiDate = null;
		}
		if (FCode.equalsIgnoreCase("CustName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustName = FValue.trim();
			}
			else
				CustName = null;
		}
		if (FCode.equalsIgnoreCase("LocalTotalAmt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				LocalTotalAmt = d;
			}
		}
		if (FCode.equalsIgnoreCase("TranBatch"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TranBatch = FValue.trim();
			}
			else
				TranBatch = null;
		}
		if (FCode.equalsIgnoreCase("ProCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProCode = FValue.trim();
			}
			else
				ProCode = null;
		}
		if (FCode.equalsIgnoreCase("BusiNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusiNo = FValue.trim();
			}
			else
				BusiNo = null;
		}
		if (FCode.equalsIgnoreCase("CzType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CzType = FValue.trim();
			}
			else
				CzType = null;
		}
		if (FCode.equalsIgnoreCase("AccPsn"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccPsn = FValue.trim();
			}
			else
				AccPsn = null;
		}
		if (FCode.equalsIgnoreCase("AccDate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccDate = FValue.trim();
			}
			else
				AccDate = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeDate = FValue.trim();
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("Modifier"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Modifier = FValue.trim();
			}
			else
				Modifier = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyDate = FValue.trim();
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("TaxStr"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TaxStr = FValue.trim();
			}
			else
				TaxStr = null;
		}
		if (FCode.equalsIgnoreCase("RelatBillStatus"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				RelatBillStatus = i;
			}
		}
		if (FCode.equalsIgnoreCase("ConnectDate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ConnectDate = FValue.trim();
			}
			else
				ConnectDate = null;
		}
		if (FCode.equalsIgnoreCase("BillNum"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BillNum = FValue.trim();
			}
			else
				BillNum = null;
		}
		if (FCode.equalsIgnoreCase("BillCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BillCode = FValue.trim();
			}
			else
				BillCode = null;
		}
		if (FCode.equalsIgnoreCase("TaxPeriod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TaxPeriod = FValue.trim();
			}
			else
				TaxPeriod = null;
		}
		if (FCode.equalsIgnoreCase("TaxBillType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				TaxBillType = i;
			}
		}
		if (FCode.equalsIgnoreCase("LocalAmt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				LocalAmt = d;
			}
		}
		if (FCode.equalsIgnoreCase("TaxRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TaxRate = FValue.trim();
			}
			else
				TaxRate = null;
		}
		if (FCode.equalsIgnoreCase("LocalTax"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				LocalTax = d;
			}
		}
		if (FCode.equalsIgnoreCase("BillTaxAmt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				BillTaxAmt = d;
			}
		}
		if (FCode.equalsIgnoreCase("BillTaxRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				BillTaxRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("ReturnRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ReturnRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("Receive_Date"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Receive_Date = FValue.trim();
			}
			else
				Receive_Date = null;
		}
		if (FCode.equalsIgnoreCase("Receive_State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Receive_State = FValue.trim();
			}
			else
				Receive_State = null;
		}
		if (FCode.equalsIgnoreCase("Fail_Reason"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Fail_Reason = FValue.trim();
			}
			else
				Fail_Reason = null;
		}
		if (FCode.equalsIgnoreCase("Receive_Date_Z"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Receive_Date_Z = FValue.trim();
			}
			else
				Receive_Date_Z = null;
		}
		if (FCode.equalsIgnoreCase("Receive_State_Z"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Receive_State_Z = FValue.trim();
			}
			else
				Receive_State_Z = null;
		}
		if (FCode.equalsIgnoreCase("Fail_Reason_Z"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Fail_Reason_Z = FValue.trim();
			}
			else
				Fail_Reason_Z = null;
		}
		if (FCode.equalsIgnoreCase("TS"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TS = FValue.trim();
			}
			else
				TS = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LCPaySerialnoHZSchema other = (LCPaySerialnoHZSchema)otherObject;
		return
			(TranSerial == null ? other.getTranSerial() == null : TranSerial.equals(other.getTranSerial()))
			&& (Pk_Org == null ? other.getPk_Org() == null : Pk_Org.equals(other.getPk_Org()))
			&& (VDate == null ? other.getVDate() == null : VDate.equals(other.getVDate()))
			&& (SrcSysItem == null ? other.getSrcSysItem() == null : SrcSysItem.equals(other.getSrcSysItem()))
			&& (BusiDate == null ? other.getBusiDate() == null : BusiDate.equals(other.getBusiDate()))
			&& (CustName == null ? other.getCustName() == null : CustName.equals(other.getCustName()))
			&& LocalTotalAmt == other.getLocalTotalAmt()
			&& (TranBatch == null ? other.getTranBatch() == null : TranBatch.equals(other.getTranBatch()))
			&& (ProCode == null ? other.getProCode() == null : ProCode.equals(other.getProCode()))
			&& (BusiNo == null ? other.getBusiNo() == null : BusiNo.equals(other.getBusiNo()))
			&& (CzType == null ? other.getCzType() == null : CzType.equals(other.getCzType()))
			&& (AccPsn == null ? other.getAccPsn() == null : AccPsn.equals(other.getAccPsn()))
			&& (AccDate == null ? other.getAccDate() == null : AccDate.equals(other.getAccDate()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : MakeDate.equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (Modifier == null ? other.getModifier() == null : Modifier.equals(other.getModifier()))
			&& (ModifyDate == null ? other.getModifyDate() == null : ModifyDate.equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (State == null ? other.getState() == null : State.equals(other.getState()))
			&& (TaxStr == null ? other.getTaxStr() == null : TaxStr.equals(other.getTaxStr()))
			&& RelatBillStatus == other.getRelatBillStatus()
			&& (ConnectDate == null ? other.getConnectDate() == null : ConnectDate.equals(other.getConnectDate()))
			&& (BillNum == null ? other.getBillNum() == null : BillNum.equals(other.getBillNum()))
			&& (BillCode == null ? other.getBillCode() == null : BillCode.equals(other.getBillCode()))
			&& (TaxPeriod == null ? other.getTaxPeriod() == null : TaxPeriod.equals(other.getTaxPeriod()))
			&& TaxBillType == other.getTaxBillType()
			&& LocalAmt == other.getLocalAmt()
			&& (TaxRate == null ? other.getTaxRate() == null : TaxRate.equals(other.getTaxRate()))
			&& LocalTax == other.getLocalTax()
			&& BillTaxAmt == other.getBillTaxAmt()
			&& BillTaxRate == other.getBillTaxRate()
			&& ReturnRate == other.getReturnRate()
			&& (Receive_Date == null ? other.getReceive_Date() == null : Receive_Date.equals(other.getReceive_Date()))
			&& (Receive_State == null ? other.getReceive_State() == null : Receive_State.equals(other.getReceive_State()))
			&& (Fail_Reason == null ? other.getFail_Reason() == null : Fail_Reason.equals(other.getFail_Reason()))
			&& (Receive_Date_Z == null ? other.getReceive_Date_Z() == null : Receive_Date_Z.equals(other.getReceive_Date_Z()))
			&& (Receive_State_Z == null ? other.getReceive_State_Z() == null : Receive_State_Z.equals(other.getReceive_State_Z()))
			&& (Fail_Reason_Z == null ? other.getFail_Reason_Z() == null : Fail_Reason_Z.equals(other.getFail_Reason_Z()))
			&& (TS == null ? other.getTS() == null : TS.equals(other.getTS()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("TranSerial") ) {
			return 0;
		}
		if( strFieldName.equals("Pk_Org") ) {
			return 1;
		}
		if( strFieldName.equals("VDate") ) {
			return 2;
		}
		if( strFieldName.equals("SrcSysItem") ) {
			return 3;
		}
		if( strFieldName.equals("BusiDate") ) {
			return 4;
		}
		if( strFieldName.equals("CustName") ) {
			return 5;
		}
		if( strFieldName.equals("LocalTotalAmt") ) {
			return 6;
		}
		if( strFieldName.equals("TranBatch") ) {
			return 7;
		}
		if( strFieldName.equals("ProCode") ) {
			return 8;
		}
		if( strFieldName.equals("BusiNo") ) {
			return 9;
		}
		if( strFieldName.equals("CzType") ) {
			return 10;
		}
		if( strFieldName.equals("AccPsn") ) {
			return 11;
		}
		if( strFieldName.equals("AccDate") ) {
			return 12;
		}
		if( strFieldName.equals("Operator") ) {
			return 13;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 14;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 15;
		}
		if( strFieldName.equals("Modifier") ) {
			return 16;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 17;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 18;
		}
		if( strFieldName.equals("State") ) {
			return 19;
		}
		if( strFieldName.equals("TaxStr") ) {
			return 20;
		}
		if( strFieldName.equals("RelatBillStatus") ) {
			return 21;
		}
		if( strFieldName.equals("ConnectDate") ) {
			return 22;
		}
		if( strFieldName.equals("BillNum") ) {
			return 23;
		}
		if( strFieldName.equals("BillCode") ) {
			return 24;
		}
		if( strFieldName.equals("TaxPeriod") ) {
			return 25;
		}
		if( strFieldName.equals("TaxBillType") ) {
			return 26;
		}
		if( strFieldName.equals("LocalAmt") ) {
			return 27;
		}
		if( strFieldName.equals("TaxRate") ) {
			return 28;
		}
		if( strFieldName.equals("LocalTax") ) {
			return 29;
		}
		if( strFieldName.equals("BillTaxAmt") ) {
			return 30;
		}
		if( strFieldName.equals("BillTaxRate") ) {
			return 31;
		}
		if( strFieldName.equals("ReturnRate") ) {
			return 32;
		}
		if( strFieldName.equals("Receive_Date") ) {
			return 33;
		}
		if( strFieldName.equals("Receive_State") ) {
			return 34;
		}
		if( strFieldName.equals("Fail_Reason") ) {
			return 35;
		}
		if( strFieldName.equals("Receive_Date_Z") ) {
			return 36;
		}
		if( strFieldName.equals("Receive_State_Z") ) {
			return 37;
		}
		if( strFieldName.equals("Fail_Reason_Z") ) {
			return 38;
		}
		if( strFieldName.equals("TS") ) {
			return 39;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "TranSerial";
				break;
			case 1:
				strFieldName = "Pk_Org";
				break;
			case 2:
				strFieldName = "VDate";
				break;
			case 3:
				strFieldName = "SrcSysItem";
				break;
			case 4:
				strFieldName = "BusiDate";
				break;
			case 5:
				strFieldName = "CustName";
				break;
			case 6:
				strFieldName = "LocalTotalAmt";
				break;
			case 7:
				strFieldName = "TranBatch";
				break;
			case 8:
				strFieldName = "ProCode";
				break;
			case 9:
				strFieldName = "BusiNo";
				break;
			case 10:
				strFieldName = "CzType";
				break;
			case 11:
				strFieldName = "AccPsn";
				break;
			case 12:
				strFieldName = "AccDate";
				break;
			case 13:
				strFieldName = "Operator";
				break;
			case 14:
				strFieldName = "MakeDate";
				break;
			case 15:
				strFieldName = "MakeTime";
				break;
			case 16:
				strFieldName = "Modifier";
				break;
			case 17:
				strFieldName = "ModifyDate";
				break;
			case 18:
				strFieldName = "ModifyTime";
				break;
			case 19:
				strFieldName = "State";
				break;
			case 20:
				strFieldName = "TaxStr";
				break;
			case 21:
				strFieldName = "RelatBillStatus";
				break;
			case 22:
				strFieldName = "ConnectDate";
				break;
			case 23:
				strFieldName = "BillNum";
				break;
			case 24:
				strFieldName = "BillCode";
				break;
			case 25:
				strFieldName = "TaxPeriod";
				break;
			case 26:
				strFieldName = "TaxBillType";
				break;
			case 27:
				strFieldName = "LocalAmt";
				break;
			case 28:
				strFieldName = "TaxRate";
				break;
			case 29:
				strFieldName = "LocalTax";
				break;
			case 30:
				strFieldName = "BillTaxAmt";
				break;
			case 31:
				strFieldName = "BillTaxRate";
				break;
			case 32:
				strFieldName = "ReturnRate";
				break;
			case 33:
				strFieldName = "Receive_Date";
				break;
			case 34:
				strFieldName = "Receive_State";
				break;
			case 35:
				strFieldName = "Fail_Reason";
				break;
			case 36:
				strFieldName = "Receive_Date_Z";
				break;
			case 37:
				strFieldName = "Receive_State_Z";
				break;
			case 38:
				strFieldName = "Fail_Reason_Z";
				break;
			case 39:
				strFieldName = "TS";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("TranSerial") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Pk_Org") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("VDate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SrcSysItem") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BusiDate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LocalTotalAmt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("TranBatch") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ProCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BusiNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CzType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccPsn") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccDate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Modifier") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TaxStr") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RelatBillStatus") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("ConnectDate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BillNum") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BillCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TaxPeriod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TaxBillType") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("LocalAmt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("TaxRate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LocalTax") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("BillTaxAmt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("BillTaxRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ReturnRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Receive_Date") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Receive_State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Fail_Reason") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Receive_Date_Z") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Receive_State_Z") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Fail_Reason_Z") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TS") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_INT;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_INT;
				break;
			case 27:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 30:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 31:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 32:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 36:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 37:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 38:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 39:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
