/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LHEvalueExpMainDB;

/*
 * <p>ClassName: LHEvalueExpMainSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PICCH-郭丽颖-表结构评估管理-20061013.pdm
 * @CreateDate：2006-10-16
 */
public class LHEvalueExpMainSchema implements Schema, Cloneable {
    // @Field
    /** 客户评估号码 */
    private String CusEvalCode;
    /** 任务实施号码 */
    private String TaskExecNo;
    /** 服务任务号码 */
    private String ServTaskNo;
    /** 服务任务标准代码 */
    private String ServTaskCode;
    /** 服务事件号码 */
    private String ServCaseCode;
    /** 服务项目号码 */
    private String ServItemNo;
    /** 服务计划号码 */
    private String ServPlanNo;
    /** 客户号码 */
    private String CustomerNo;
    /** 团体客户号 */
    private String GrpCustomerNo;
    /** 个人保单号 */
    private String ContNo;
    /** 团体保单号 */
    private String GrpContNo;
    /** 问卷类型 */
    private String EvalType;
    /** 评估日期 */
    private Date EvalDate;
    /** 评估时间 */
    private String EvalTime;
    /** 导出标志 */
    private String ExportFlag;
    /** 失败原因 */
    private String FailReason;
    /** 附加信息1 */
    private String AppendInfo;
    /** 附加信息2 */
    private String AppendInfo2;
    /** 附加信息3 */
    private String AppendInfo3;
    /** 附加信息4 */
    private String AppendInfo4;
    /** 管理机构 */
    private String ManageCom;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后修改日期 */
    private Date ModifyDate;
    /** 最后修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 26; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LHEvalueExpMainSchema() {
        mErrors = new CErrors();

        String[] pk = new String[0];

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LHEvalueExpMainSchema cloned = (LHEvalueExpMainSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getCusEvalCode() {
        return CusEvalCode;
    }

    public void setCusEvalCode(String aCusEvalCode) {
        CusEvalCode = aCusEvalCode;
    }

    public String getTaskExecNo() {
        return TaskExecNo;
    }

    public void setTaskExecNo(String aTaskExecNo) {
        TaskExecNo = aTaskExecNo;
    }

    public String getServTaskNo() {
        return ServTaskNo;
    }

    public void setServTaskNo(String aServTaskNo) {
        ServTaskNo = aServTaskNo;
    }

    public String getServTaskCode() {
        return ServTaskCode;
    }

    public void setServTaskCode(String aServTaskCode) {
        ServTaskCode = aServTaskCode;
    }

    public String getServCaseCode() {
        return ServCaseCode;
    }

    public void setServCaseCode(String aServCaseCode) {
        ServCaseCode = aServCaseCode;
    }

    public String getServItemNo() {
        return ServItemNo;
    }

    public void setServItemNo(String aServItemNo) {
        ServItemNo = aServItemNo;
    }

    public String getServPlanNo() {
        return ServPlanNo;
    }

    public void setServPlanNo(String aServPlanNo) {
        ServPlanNo = aServPlanNo;
    }

    public String getCustomerNo() {
        return CustomerNo;
    }

    public void setCustomerNo(String aCustomerNo) {
        CustomerNo = aCustomerNo;
    }

    public String getGrpCustomerNo() {
        return GrpCustomerNo;
    }

    public void setGrpCustomerNo(String aGrpCustomerNo) {
        GrpCustomerNo = aGrpCustomerNo;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }

    public String getGrpContNo() {
        return GrpContNo;
    }

    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }

    public String getEvalType() {
        return EvalType;
    }

    public void setEvalType(String aEvalType) {
        EvalType = aEvalType;
    }

    public String getEvalDate() {
        if (EvalDate != null) {
            return fDate.getString(EvalDate);
        } else {
            return null;
        }
    }

    public void setEvalDate(Date aEvalDate) {
        EvalDate = aEvalDate;
    }

    public void setEvalDate(String aEvalDate) {
        if (aEvalDate != null && !aEvalDate.equals("")) {
            EvalDate = fDate.getDate(aEvalDate);
        } else {
            EvalDate = null;
        }
    }

    public String getEvalTime() {
        return EvalTime;
    }

    public void setEvalTime(String aEvalTime) {
        EvalTime = aEvalTime;
    }

    public String getExportFlag() {
        return ExportFlag;
    }

    public void setExportFlag(String aExportFlag) {
        ExportFlag = aExportFlag;
    }

    public String getFailReason() {
        return FailReason;
    }

    public void setFailReason(String aFailReason) {
        FailReason = aFailReason;
    }

    public String getAppendInfo() {
        return AppendInfo;
    }

    public void setAppendInfo(String aAppendInfo) {
        AppendInfo = aAppendInfo;
    }

    public String getAppendInfo2() {
        return AppendInfo2;
    }

    public void setAppendInfo2(String aAppendInfo2) {
        AppendInfo2 = aAppendInfo2;
    }

    public String getAppendInfo3() {
        return AppendInfo3;
    }

    public void setAppendInfo3(String aAppendInfo3) {
        AppendInfo3 = aAppendInfo3;
    }

    public String getAppendInfo4() {
        return AppendInfo4;
    }

    public void setAppendInfo4(String aAppendInfo4) {
        AppendInfo4 = aAppendInfo4;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LHEvalueExpMainSchema 对象给 Schema 赋值
     * @param: aLHEvalueExpMainSchema LHEvalueExpMainSchema
     **/
    public void setSchema(LHEvalueExpMainSchema aLHEvalueExpMainSchema) {
        this.CusEvalCode = aLHEvalueExpMainSchema.getCusEvalCode();
        this.TaskExecNo = aLHEvalueExpMainSchema.getTaskExecNo();
        this.ServTaskNo = aLHEvalueExpMainSchema.getServTaskNo();
        this.ServTaskCode = aLHEvalueExpMainSchema.getServTaskCode();
        this.ServCaseCode = aLHEvalueExpMainSchema.getServCaseCode();
        this.ServItemNo = aLHEvalueExpMainSchema.getServItemNo();
        this.ServPlanNo = aLHEvalueExpMainSchema.getServPlanNo();
        this.CustomerNo = aLHEvalueExpMainSchema.getCustomerNo();
        this.GrpCustomerNo = aLHEvalueExpMainSchema.getGrpCustomerNo();
        this.ContNo = aLHEvalueExpMainSchema.getContNo();
        this.GrpContNo = aLHEvalueExpMainSchema.getGrpContNo();
        this.EvalType = aLHEvalueExpMainSchema.getEvalType();
        this.EvalDate = fDate.getDate(aLHEvalueExpMainSchema.getEvalDate());
        this.EvalTime = aLHEvalueExpMainSchema.getEvalTime();
        this.ExportFlag = aLHEvalueExpMainSchema.getExportFlag();
        this.FailReason = aLHEvalueExpMainSchema.getFailReason();
        this.AppendInfo = aLHEvalueExpMainSchema.getAppendInfo();
        this.AppendInfo2 = aLHEvalueExpMainSchema.getAppendInfo2();
        this.AppendInfo3 = aLHEvalueExpMainSchema.getAppendInfo3();
        this.AppendInfo4 = aLHEvalueExpMainSchema.getAppendInfo4();
        this.ManageCom = aLHEvalueExpMainSchema.getManageCom();
        this.Operator = aLHEvalueExpMainSchema.getOperator();
        this.MakeDate = fDate.getDate(aLHEvalueExpMainSchema.getMakeDate());
        this.MakeTime = aLHEvalueExpMainSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLHEvalueExpMainSchema.getModifyDate());
        this.ModifyTime = aLHEvalueExpMainSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("CusEvalCode") == null) {
                this.CusEvalCode = null;
            } else {
                this.CusEvalCode = rs.getString("CusEvalCode").trim();
            }

            if (rs.getString("TaskExecNo") == null) {
                this.TaskExecNo = null;
            } else {
                this.TaskExecNo = rs.getString("TaskExecNo").trim();
            }

            if (rs.getString("ServTaskNo") == null) {
                this.ServTaskNo = null;
            } else {
                this.ServTaskNo = rs.getString("ServTaskNo").trim();
            }

            if (rs.getString("ServTaskCode") == null) {
                this.ServTaskCode = null;
            } else {
                this.ServTaskCode = rs.getString("ServTaskCode").trim();
            }

            if (rs.getString("ServCaseCode") == null) {
                this.ServCaseCode = null;
            } else {
                this.ServCaseCode = rs.getString("ServCaseCode").trim();
            }

            if (rs.getString("ServItemNo") == null) {
                this.ServItemNo = null;
            } else {
                this.ServItemNo = rs.getString("ServItemNo").trim();
            }

            if (rs.getString("ServPlanNo") == null) {
                this.ServPlanNo = null;
            } else {
                this.ServPlanNo = rs.getString("ServPlanNo").trim();
            }

            if (rs.getString("CustomerNo") == null) {
                this.CustomerNo = null;
            } else {
                this.CustomerNo = rs.getString("CustomerNo").trim();
            }

            if (rs.getString("GrpCustomerNo") == null) {
                this.GrpCustomerNo = null;
            } else {
                this.GrpCustomerNo = rs.getString("GrpCustomerNo").trim();
            }

            if (rs.getString("ContNo") == null) {
                this.ContNo = null;
            } else {
                this.ContNo = rs.getString("ContNo").trim();
            }

            if (rs.getString("GrpContNo") == null) {
                this.GrpContNo = null;
            } else {
                this.GrpContNo = rs.getString("GrpContNo").trim();
            }

            if (rs.getString("EvalType") == null) {
                this.EvalType = null;
            } else {
                this.EvalType = rs.getString("EvalType").trim();
            }

            this.EvalDate = rs.getDate("EvalDate");
            if (rs.getString("EvalTime") == null) {
                this.EvalTime = null;
            } else {
                this.EvalTime = rs.getString("EvalTime").trim();
            }

            if (rs.getString("ExportFlag") == null) {
                this.ExportFlag = null;
            } else {
                this.ExportFlag = rs.getString("ExportFlag").trim();
            }

            if (rs.getString("FailReason") == null) {
                this.FailReason = null;
            } else {
                this.FailReason = rs.getString("FailReason").trim();
            }

            if (rs.getString("AppendInfo") == null) {
                this.AppendInfo = null;
            } else {
                this.AppendInfo = rs.getString("AppendInfo").trim();
            }

            if (rs.getString("AppendInfo2") == null) {
                this.AppendInfo2 = null;
            } else {
                this.AppendInfo2 = rs.getString("AppendInfo2").trim();
            }

            if (rs.getString("AppendInfo3") == null) {
                this.AppendInfo3 = null;
            } else {
                this.AppendInfo3 = rs.getString("AppendInfo3").trim();
            }

            if (rs.getString("AppendInfo4") == null) {
                this.AppendInfo4 = null;
            } else {
                this.AppendInfo4 = rs.getString("AppendInfo4").trim();
            }

            if (rs.getString("ManageCom") == null) {
                this.ManageCom = null;
            } else {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LHEvalueExpMain表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHEvalueExpMainSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LHEvalueExpMainSchema getSchema() {
        LHEvalueExpMainSchema aLHEvalueExpMainSchema = new
                LHEvalueExpMainSchema();
        aLHEvalueExpMainSchema.setSchema(this);
        return aLHEvalueExpMainSchema;
    }

    public LHEvalueExpMainDB getDB() {
        LHEvalueExpMainDB aDBOper = new LHEvalueExpMainDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHEvalueExpMain描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(CusEvalCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TaskExecNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServTaskNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServTaskCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServCaseCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServItemNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServPlanNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CustomerNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpCustomerNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpContNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EvalType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(EvalDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EvalTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ExportFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FailReason));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppendInfo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppendInfo2));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppendInfo3));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppendInfo4));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHEvalueExpMain>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            CusEvalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                         SysConst.PACKAGESPILTER);
            TaskExecNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
            ServTaskNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                        SysConst.PACKAGESPILTER);
            ServTaskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                          SysConst.PACKAGESPILTER);
            ServCaseCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                          SysConst.PACKAGESPILTER);
            ServItemNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                        SysConst.PACKAGESPILTER);
            ServPlanNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                        SysConst.PACKAGESPILTER);
            CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                        SysConst.PACKAGESPILTER);
            GrpCustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                           SysConst.PACKAGESPILTER);
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                    SysConst.PACKAGESPILTER);
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                       SysConst.PACKAGESPILTER);
            EvalType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                      SysConst.PACKAGESPILTER);
            EvalDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 13, SysConst.PACKAGESPILTER));
            EvalTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                      SysConst.PACKAGESPILTER);
            ExportFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                        SysConst.PACKAGESPILTER);
            FailReason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                        SysConst.PACKAGESPILTER);
            AppendInfo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                        SysConst.PACKAGESPILTER);
            AppendInfo2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                         SysConst.PACKAGESPILTER);
            AppendInfo3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                         SysConst.PACKAGESPILTER);
            AppendInfo4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                         SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,
                                       SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 23, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 25, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26,
                                        SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHEvalueExpMainSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("CusEvalCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CusEvalCode));
        }
        if (FCode.equals("TaskExecNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TaskExecNo));
        }
        if (FCode.equals("ServTaskNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServTaskNo));
        }
        if (FCode.equals("ServTaskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServTaskCode));
        }
        if (FCode.equals("ServCaseCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServCaseCode));
        }
        if (FCode.equals("ServItemNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServItemNo));
        }
        if (FCode.equals("ServPlanNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServPlanNo));
        }
        if (FCode.equals("CustomerNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
        }
        if (FCode.equals("GrpCustomerNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpCustomerNo));
        }
        if (FCode.equals("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equals("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equals("EvalType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EvalType));
        }
        if (FCode.equals("EvalDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getEvalDate()));
        }
        if (FCode.equals("EvalTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EvalTime));
        }
        if (FCode.equals("ExportFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExportFlag));
        }
        if (FCode.equals("FailReason")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FailReason));
        }
        if (FCode.equals("AppendInfo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppendInfo));
        }
        if (FCode.equals("AppendInfo2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppendInfo2));
        }
        if (FCode.equals("AppendInfo3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppendInfo3));
        }
        if (FCode.equals("AppendInfo4")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppendInfo4));
        }
        if (FCode.equals("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(CusEvalCode);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(TaskExecNo);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(ServTaskNo);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(ServTaskCode);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(ServCaseCode);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(ServItemNo);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(ServPlanNo);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(CustomerNo);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(GrpCustomerNo);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(ContNo);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(GrpContNo);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(EvalType);
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getEvalDate()));
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(EvalTime);
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(ExportFlag);
            break;
        case 15:
            strFieldValue = StrTool.GBKToUnicode(FailReason);
            break;
        case 16:
            strFieldValue = StrTool.GBKToUnicode(AppendInfo);
            break;
        case 17:
            strFieldValue = StrTool.GBKToUnicode(AppendInfo2);
            break;
        case 18:
            strFieldValue = StrTool.GBKToUnicode(AppendInfo3);
            break;
        case 19:
            strFieldValue = StrTool.GBKToUnicode(AppendInfo4);
            break;
        case 20:
            strFieldValue = StrTool.GBKToUnicode(ManageCom);
            break;
        case 21:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 22:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 23:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 24:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 25:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("CusEvalCode")) {
            if (FValue != null && !FValue.equals("")) {
                CusEvalCode = FValue.trim();
            } else {
                CusEvalCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("TaskExecNo")) {
            if (FValue != null && !FValue.equals("")) {
                TaskExecNo = FValue.trim();
            } else {
                TaskExecNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServTaskNo")) {
            if (FValue != null && !FValue.equals("")) {
                ServTaskNo = FValue.trim();
            } else {
                ServTaskNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServTaskCode")) {
            if (FValue != null && !FValue.equals("")) {
                ServTaskCode = FValue.trim();
            } else {
                ServTaskCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServCaseCode")) {
            if (FValue != null && !FValue.equals("")) {
                ServCaseCode = FValue.trim();
            } else {
                ServCaseCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServItemNo")) {
            if (FValue != null && !FValue.equals("")) {
                ServItemNo = FValue.trim();
            } else {
                ServItemNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServPlanNo")) {
            if (FValue != null && !FValue.equals("")) {
                ServPlanNo = FValue.trim();
            } else {
                ServPlanNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            if (FValue != null && !FValue.equals("")) {
                CustomerNo = FValue.trim();
            } else {
                CustomerNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("GrpCustomerNo")) {
            if (FValue != null && !FValue.equals("")) {
                GrpCustomerNo = FValue.trim();
            } else {
                GrpCustomerNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if (FValue != null && !FValue.equals("")) {
                ContNo = FValue.trim();
            } else {
                ContNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if (FValue != null && !FValue.equals("")) {
                GrpContNo = FValue.trim();
            } else {
                GrpContNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("EvalType")) {
            if (FValue != null && !FValue.equals("")) {
                EvalType = FValue.trim();
            } else {
                EvalType = null;
            }
        }
        if (FCode.equalsIgnoreCase("EvalDate")) {
            if (FValue != null && !FValue.equals("")) {
                EvalDate = fDate.getDate(FValue);
            } else {
                EvalDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("EvalTime")) {
            if (FValue != null && !FValue.equals("")) {
                EvalTime = FValue.trim();
            } else {
                EvalTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ExportFlag")) {
            if (FValue != null && !FValue.equals("")) {
                ExportFlag = FValue.trim();
            } else {
                ExportFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("FailReason")) {
            if (FValue != null && !FValue.equals("")) {
                FailReason = FValue.trim();
            } else {
                FailReason = null;
            }
        }
        if (FCode.equalsIgnoreCase("AppendInfo")) {
            if (FValue != null && !FValue.equals("")) {
                AppendInfo = FValue.trim();
            } else {
                AppendInfo = null;
            }
        }
        if (FCode.equalsIgnoreCase("AppendInfo2")) {
            if (FValue != null && !FValue.equals("")) {
                AppendInfo2 = FValue.trim();
            } else {
                AppendInfo2 = null;
            }
        }
        if (FCode.equalsIgnoreCase("AppendInfo3")) {
            if (FValue != null && !FValue.equals("")) {
                AppendInfo3 = FValue.trim();
            } else {
                AppendInfo3 = null;
            }
        }
        if (FCode.equalsIgnoreCase("AppendInfo4")) {
            if (FValue != null && !FValue.equals("")) {
                AppendInfo4 = FValue.trim();
            } else {
                AppendInfo4 = null;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ManageCom = FValue.trim();
            } else {
                ManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LHEvalueExpMainSchema other = (LHEvalueExpMainSchema) otherObject;
        return
                CusEvalCode.equals(other.getCusEvalCode())
                && TaskExecNo.equals(other.getTaskExecNo())
                && ServTaskNo.equals(other.getServTaskNo())
                && ServTaskCode.equals(other.getServTaskCode())
                && ServCaseCode.equals(other.getServCaseCode())
                && ServItemNo.equals(other.getServItemNo())
                && ServPlanNo.equals(other.getServPlanNo())
                && CustomerNo.equals(other.getCustomerNo())
                && GrpCustomerNo.equals(other.getGrpCustomerNo())
                && ContNo.equals(other.getContNo())
                && GrpContNo.equals(other.getGrpContNo())
                && EvalType.equals(other.getEvalType())
                && fDate.getString(EvalDate).equals(other.getEvalDate())
                && EvalTime.equals(other.getEvalTime())
                && ExportFlag.equals(other.getExportFlag())
                && FailReason.equals(other.getFailReason())
                && AppendInfo.equals(other.getAppendInfo())
                && AppendInfo2.equals(other.getAppendInfo2())
                && AppendInfo3.equals(other.getAppendInfo3())
                && AppendInfo4.equals(other.getAppendInfo4())
                && ManageCom.equals(other.getManageCom())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("CusEvalCode")) {
            return 0;
        }
        if (strFieldName.equals("TaskExecNo")) {
            return 1;
        }
        if (strFieldName.equals("ServTaskNo")) {
            return 2;
        }
        if (strFieldName.equals("ServTaskCode")) {
            return 3;
        }
        if (strFieldName.equals("ServCaseCode")) {
            return 4;
        }
        if (strFieldName.equals("ServItemNo")) {
            return 5;
        }
        if (strFieldName.equals("ServPlanNo")) {
            return 6;
        }
        if (strFieldName.equals("CustomerNo")) {
            return 7;
        }
        if (strFieldName.equals("GrpCustomerNo")) {
            return 8;
        }
        if (strFieldName.equals("ContNo")) {
            return 9;
        }
        if (strFieldName.equals("GrpContNo")) {
            return 10;
        }
        if (strFieldName.equals("EvalType")) {
            return 11;
        }
        if (strFieldName.equals("EvalDate")) {
            return 12;
        }
        if (strFieldName.equals("EvalTime")) {
            return 13;
        }
        if (strFieldName.equals("ExportFlag")) {
            return 14;
        }
        if (strFieldName.equals("FailReason")) {
            return 15;
        }
        if (strFieldName.equals("AppendInfo")) {
            return 16;
        }
        if (strFieldName.equals("AppendInfo2")) {
            return 17;
        }
        if (strFieldName.equals("AppendInfo3")) {
            return 18;
        }
        if (strFieldName.equals("AppendInfo4")) {
            return 19;
        }
        if (strFieldName.equals("ManageCom")) {
            return 20;
        }
        if (strFieldName.equals("Operator")) {
            return 21;
        }
        if (strFieldName.equals("MakeDate")) {
            return 22;
        }
        if (strFieldName.equals("MakeTime")) {
            return 23;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 24;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 25;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "CusEvalCode";
            break;
        case 1:
            strFieldName = "TaskExecNo";
            break;
        case 2:
            strFieldName = "ServTaskNo";
            break;
        case 3:
            strFieldName = "ServTaskCode";
            break;
        case 4:
            strFieldName = "ServCaseCode";
            break;
        case 5:
            strFieldName = "ServItemNo";
            break;
        case 6:
            strFieldName = "ServPlanNo";
            break;
        case 7:
            strFieldName = "CustomerNo";
            break;
        case 8:
            strFieldName = "GrpCustomerNo";
            break;
        case 9:
            strFieldName = "ContNo";
            break;
        case 10:
            strFieldName = "GrpContNo";
            break;
        case 11:
            strFieldName = "EvalType";
            break;
        case 12:
            strFieldName = "EvalDate";
            break;
        case 13:
            strFieldName = "EvalTime";
            break;
        case 14:
            strFieldName = "ExportFlag";
            break;
        case 15:
            strFieldName = "FailReason";
            break;
        case 16:
            strFieldName = "AppendInfo";
            break;
        case 17:
            strFieldName = "AppendInfo2";
            break;
        case 18:
            strFieldName = "AppendInfo3";
            break;
        case 19:
            strFieldName = "AppendInfo4";
            break;
        case 20:
            strFieldName = "ManageCom";
            break;
        case 21:
            strFieldName = "Operator";
            break;
        case 22:
            strFieldName = "MakeDate";
            break;
        case 23:
            strFieldName = "MakeTime";
            break;
        case 24:
            strFieldName = "ModifyDate";
            break;
        case 25:
            strFieldName = "ModifyTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("CusEvalCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("TaskExecNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServTaskNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServTaskCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServCaseCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServItemNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServPlanNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CustomerNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpCustomerNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpContNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EvalType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EvalDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("EvalTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ExportFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FailReason")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppendInfo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppendInfo2")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppendInfo3")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppendInfo4")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 12:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 13:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 14:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 15:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 16:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 17:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 18:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 19:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 20:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 21:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 22:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 23:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 24:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 25:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
