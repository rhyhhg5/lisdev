/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LCPersonTraceDB;

/*
 * <p>ClassName: LCPersonTraceSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 授权标识接口
 * @CreateDate：2019-05-06
 */
public class LCPersonTraceSchema implements Schema, Cloneable
{
	// @Field
	/** 客户id */
	private String CustomerNo;
	/** 客户授权轨迹id */
	private String TraceNo;
	/** 合约号码 */
	private String ContractNo;
	/** 共享标识 */
	private String SharedMark;
	/** 授权标识 */
	private String Authorization;
	/** 特殊限定标识 */
	private String SpecialLimitMark;
	/** 来源公司 */
	private String CompanySource;
	/** 来源机构 */
	private String InstitutionSource;
	/** 业务环节 */
	private String BusinessLink;
	/** 客户触面 */
	private String CustomerContact;
	/** 授权合约类型 */
	private String AuthType;
	/** 授权合约版本 */
	private String AuthVersion;
	/** 报送日期 */
	private Date SendDate;
	/** 报送时间 */
	private String SendTime;
	/** 修改日期 */
	private Date ModifyDate;
	/** 修改时间 */
	private String ModifyTime;
	/** 备用字段1 */
	private String Spare1;
	/** 备用字段2 */
	private String Spare2;
	/** 备用字段3 */
	private Date Spare3;

	public static final int FIELDNUM = 19;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LCPersonTraceSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[3];
		pk[0] = "CustomerNo";
		pk[1] = "TraceNo";
		pk[2] = "ContractNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LCPersonTraceSchema cloned = (LCPersonTraceSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getCustomerNo()
	{
		return CustomerNo;
	}
	public void setCustomerNo(String aCustomerNo)
	{
		CustomerNo = aCustomerNo;
	}
	public String getTraceNo()
	{
		return TraceNo;
	}
	public void setTraceNo(String aTraceNo)
	{
		TraceNo = aTraceNo;
	}
	public String getContractNo()
	{
		return ContractNo;
	}
	public void setContractNo(String aContractNo)
	{
		ContractNo = aContractNo;
	}
	public String getSharedMark()
	{
		return SharedMark;
	}
	public void setSharedMark(String aSharedMark)
	{
		SharedMark = aSharedMark;
	}
	public String getAuthorization()
	{
		return Authorization;
	}
	public void setAuthorization(String aAuthorization)
	{
		Authorization = aAuthorization;
	}
	public String getSpecialLimitMark()
	{
		return SpecialLimitMark;
	}
	public void setSpecialLimitMark(String aSpecialLimitMark)
	{
		SpecialLimitMark = aSpecialLimitMark;
	}
	public String getCompanySource()
	{
		return CompanySource;
	}
	public void setCompanySource(String aCompanySource)
	{
		CompanySource = aCompanySource;
	}
	public String getInstitutionSource()
	{
		return InstitutionSource;
	}
	public void setInstitutionSource(String aInstitutionSource)
	{
		InstitutionSource = aInstitutionSource;
	}
	public String getBusinessLink()
	{
		return BusinessLink;
	}
	public void setBusinessLink(String aBusinessLink)
	{
		BusinessLink = aBusinessLink;
	}
	public String getCustomerContact()
	{
		return CustomerContact;
	}
	public void setCustomerContact(String aCustomerContact)
	{
		CustomerContact = aCustomerContact;
	}
	public String getAuthType()
	{
		return AuthType;
	}
	public void setAuthType(String aAuthType)
	{
		AuthType = aAuthType;
	}
	public String getAuthVersion()
	{
		return AuthVersion;
	}
	public void setAuthVersion(String aAuthVersion)
	{
		AuthVersion = aAuthVersion;
	}
	public String getSendDate()
	{
		if( SendDate != null )
			return fDate.getString(SendDate);
		else
			return null;
	}
	public void setSendDate(Date aSendDate)
	{
		SendDate = aSendDate;
	}
	public void setSendDate(String aSendDate)
	{
		if (aSendDate != null && !aSendDate.equals("") )
		{
			SendDate = fDate.getDate( aSendDate );
		}
		else
			SendDate = null;
	}

	public String getSendTime()
	{
		return SendTime;
	}
	public void setSendTime(String aSendTime)
	{
		SendTime = aSendTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getSpare1()
	{
		return Spare1;
	}
	public void setSpare1(String aSpare1)
	{
		Spare1 = aSpare1;
	}
	public String getSpare2()
	{
		return Spare2;
	}
	public void setSpare2(String aSpare2)
	{
		Spare2 = aSpare2;
	}
	public String getSpare3()
	{
		if( Spare3 != null )
			return fDate.getString(Spare3);
		else
			return null;
	}
	public void setSpare3(Date aSpare3)
	{
		Spare3 = aSpare3;
	}
	public void setSpare3(String aSpare3)
	{
		if (aSpare3 != null && !aSpare3.equals("") )
		{
			Spare3 = fDate.getDate( aSpare3 );
		}
		else
			Spare3 = null;
	}


	/**
	* 使用另外一个 LCPersonTraceSchema 对象给 Schema 赋值
	* @param: aLCPersonTraceSchema LCPersonTraceSchema
	**/
	public void setSchema(LCPersonTraceSchema aLCPersonTraceSchema)
	{
		this.CustomerNo = aLCPersonTraceSchema.getCustomerNo();
		this.TraceNo = aLCPersonTraceSchema.getTraceNo();
		this.ContractNo = aLCPersonTraceSchema.getContractNo();
		this.SharedMark = aLCPersonTraceSchema.getSharedMark();
		this.Authorization = aLCPersonTraceSchema.getAuthorization();
		this.SpecialLimitMark = aLCPersonTraceSchema.getSpecialLimitMark();
		this.CompanySource = aLCPersonTraceSchema.getCompanySource();
		this.InstitutionSource = aLCPersonTraceSchema.getInstitutionSource();
		this.BusinessLink = aLCPersonTraceSchema.getBusinessLink();
		this.CustomerContact = aLCPersonTraceSchema.getCustomerContact();
		this.AuthType = aLCPersonTraceSchema.getAuthType();
		this.AuthVersion = aLCPersonTraceSchema.getAuthVersion();
		this.SendDate = fDate.getDate( aLCPersonTraceSchema.getSendDate());
		this.SendTime = aLCPersonTraceSchema.getSendTime();
		this.ModifyDate = fDate.getDate( aLCPersonTraceSchema.getModifyDate());
		this.ModifyTime = aLCPersonTraceSchema.getModifyTime();
		this.Spare1 = aLCPersonTraceSchema.getSpare1();
		this.Spare2 = aLCPersonTraceSchema.getSpare2();
		this.Spare3 = fDate.getDate( aLCPersonTraceSchema.getSpare3());
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("CustomerNo") == null )
				this.CustomerNo = null;
			else
				this.CustomerNo = rs.getString("CustomerNo").trim();

			if( rs.getString("TraceNo") == null )
				this.TraceNo = null;
			else
				this.TraceNo = rs.getString("TraceNo").trim();

			if( rs.getString("ContractNo") == null )
				this.ContractNo = null;
			else
				this.ContractNo = rs.getString("ContractNo").trim();

			if( rs.getString("SharedMark") == null )
				this.SharedMark = null;
			else
				this.SharedMark = rs.getString("SharedMark").trim();

			if( rs.getString("Authorization") == null )
				this.Authorization = null;
			else
				this.Authorization = rs.getString("Authorization").trim();

			if( rs.getString("SpecialLimitMark") == null )
				this.SpecialLimitMark = null;
			else
				this.SpecialLimitMark = rs.getString("SpecialLimitMark").trim();

			if( rs.getString("CompanySource") == null )
				this.CompanySource = null;
			else
				this.CompanySource = rs.getString("CompanySource").trim();

			if( rs.getString("InstitutionSource") == null )
				this.InstitutionSource = null;
			else
				this.InstitutionSource = rs.getString("InstitutionSource").trim();

			if( rs.getString("BusinessLink") == null )
				this.BusinessLink = null;
			else
				this.BusinessLink = rs.getString("BusinessLink").trim();

			if( rs.getString("CustomerContact") == null )
				this.CustomerContact = null;
			else
				this.CustomerContact = rs.getString("CustomerContact").trim();

			if( rs.getString("AuthType") == null )
				this.AuthType = null;
			else
				this.AuthType = rs.getString("AuthType").trim();

			if( rs.getString("AuthVersion") == null )
				this.AuthVersion = null;
			else
				this.AuthVersion = rs.getString("AuthVersion").trim();

			this.SendDate = rs.getDate("SendDate");
			if( rs.getString("SendTime") == null )
				this.SendTime = null;
			else
				this.SendTime = rs.getString("SendTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("Spare1") == null )
				this.Spare1 = null;
			else
				this.Spare1 = rs.getString("Spare1").trim();

			if( rs.getString("Spare2") == null )
				this.Spare2 = null;
			else
				this.Spare2 = rs.getString("Spare2").trim();

			this.Spare3 = rs.getDate("Spare3");
		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LCPersonTrace表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCPersonTraceSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LCPersonTraceSchema getSchema()
	{
		LCPersonTraceSchema aLCPersonTraceSchema = new LCPersonTraceSchema();
		aLCPersonTraceSchema.setSchema(this);
		return aLCPersonTraceSchema;
	}

	public LCPersonTraceDB getDB()
	{
		LCPersonTraceDB aDBOper = new LCPersonTraceDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCPersonTrace描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(CustomerNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TraceNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContractNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SharedMark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Authorization)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SpecialLimitMark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CompanySource)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InstitutionSource)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusinessLink)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerContact)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AuthType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AuthVersion)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( SendDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SendTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Spare1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Spare2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Spare3 )));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCPersonTrace>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			TraceNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			ContractNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			SharedMark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			Authorization = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			SpecialLimitMark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			CompanySource = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			InstitutionSource = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			BusinessLink = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			CustomerContact = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			AuthType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			AuthVersion = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			SendDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,SysConst.PACKAGESPILTER));
			SendTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			Spare1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			Spare2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			Spare3 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,SysConst.PACKAGESPILTER));
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCPersonTraceSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("CustomerNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
		}
		if (FCode.equals("TraceNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TraceNo));
		}
		if (FCode.equals("ContractNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContractNo));
		}
		if (FCode.equals("SharedMark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SharedMark));
		}
		if (FCode.equals("Authorization"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Authorization));
		}
		if (FCode.equals("SpecialLimitMark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SpecialLimitMark));
		}
		if (FCode.equals("CompanySource"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CompanySource));
		}
		if (FCode.equals("InstitutionSource"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InstitutionSource));
		}
		if (FCode.equals("BusinessLink"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusinessLink));
		}
		if (FCode.equals("CustomerContact"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerContact));
		}
		if (FCode.equals("AuthType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AuthType));
		}
		if (FCode.equals("AuthVersion"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AuthVersion));
		}
		if (FCode.equals("SendDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getSendDate()));
		}
		if (FCode.equals("SendTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SendTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("Spare1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Spare1));
		}
		if (FCode.equals("Spare2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Spare2));
		}
		if (FCode.equals("Spare3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getSpare3()));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(CustomerNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(TraceNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(ContractNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(SharedMark);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(Authorization);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(SpecialLimitMark);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(CompanySource);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(InstitutionSource);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(BusinessLink);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(CustomerContact);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(AuthType);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(AuthVersion);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getSendDate()));
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(SendTime);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(Spare1);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(Spare2);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getSpare3()));
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("CustomerNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerNo = FValue.trim();
			}
			else
				CustomerNo = null;
		}
		if (FCode.equalsIgnoreCase("TraceNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TraceNo = FValue.trim();
			}
			else
				TraceNo = null;
		}
		if (FCode.equalsIgnoreCase("ContractNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContractNo = FValue.trim();
			}
			else
				ContractNo = null;
		}
		if (FCode.equalsIgnoreCase("SharedMark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SharedMark = FValue.trim();
			}
			else
				SharedMark = null;
		}
		if (FCode.equalsIgnoreCase("Authorization"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Authorization = FValue.trim();
			}
			else
				Authorization = null;
		}
		if (FCode.equalsIgnoreCase("SpecialLimitMark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SpecialLimitMark = FValue.trim();
			}
			else
				SpecialLimitMark = null;
		}
		if (FCode.equalsIgnoreCase("CompanySource"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CompanySource = FValue.trim();
			}
			else
				CompanySource = null;
		}
		if (FCode.equalsIgnoreCase("InstitutionSource"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InstitutionSource = FValue.trim();
			}
			else
				InstitutionSource = null;
		}
		if (FCode.equalsIgnoreCase("BusinessLink"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusinessLink = FValue.trim();
			}
			else
				BusinessLink = null;
		}
		if (FCode.equalsIgnoreCase("CustomerContact"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerContact = FValue.trim();
			}
			else
				CustomerContact = null;
		}
		if (FCode.equalsIgnoreCase("AuthType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AuthType = FValue.trim();
			}
			else
				AuthType = null;
		}
		if (FCode.equalsIgnoreCase("AuthVersion"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AuthVersion = FValue.trim();
			}
			else
				AuthVersion = null;
		}
		if (FCode.equalsIgnoreCase("SendDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				SendDate = fDate.getDate( FValue );
			}
			else
				SendDate = null;
		}
		if (FCode.equalsIgnoreCase("SendTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SendTime = FValue.trim();
			}
			else
				SendTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("Spare1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Spare1 = FValue.trim();
			}
			else
				Spare1 = null;
		}
		if (FCode.equalsIgnoreCase("Spare2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Spare2 = FValue.trim();
			}
			else
				Spare2 = null;
		}
		if (FCode.equalsIgnoreCase("Spare3"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Spare3 = fDate.getDate( FValue );
			}
			else
				Spare3 = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LCPersonTraceSchema other = (LCPersonTraceSchema)otherObject;
		return
			(CustomerNo == null ? other.getCustomerNo() == null : CustomerNo.equals(other.getCustomerNo()))
			&& (TraceNo == null ? other.getTraceNo() == null : TraceNo.equals(other.getTraceNo()))
			&& (ContractNo == null ? other.getContractNo() == null : ContractNo.equals(other.getContractNo()))
			&& (SharedMark == null ? other.getSharedMark() == null : SharedMark.equals(other.getSharedMark()))
			&& (Authorization == null ? other.getAuthorization() == null : Authorization.equals(other.getAuthorization()))
			&& (SpecialLimitMark == null ? other.getSpecialLimitMark() == null : SpecialLimitMark.equals(other.getSpecialLimitMark()))
			&& (CompanySource == null ? other.getCompanySource() == null : CompanySource.equals(other.getCompanySource()))
			&& (InstitutionSource == null ? other.getInstitutionSource() == null : InstitutionSource.equals(other.getInstitutionSource()))
			&& (BusinessLink == null ? other.getBusinessLink() == null : BusinessLink.equals(other.getBusinessLink()))
			&& (CustomerContact == null ? other.getCustomerContact() == null : CustomerContact.equals(other.getCustomerContact()))
			&& (AuthType == null ? other.getAuthType() == null : AuthType.equals(other.getAuthType()))
			&& (AuthVersion == null ? other.getAuthVersion() == null : AuthVersion.equals(other.getAuthVersion()))
			&& (SendDate == null ? other.getSendDate() == null : fDate.getString(SendDate).equals(other.getSendDate()))
			&& (SendTime == null ? other.getSendTime() == null : SendTime.equals(other.getSendTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (Spare1 == null ? other.getSpare1() == null : Spare1.equals(other.getSpare1()))
			&& (Spare2 == null ? other.getSpare2() == null : Spare2.equals(other.getSpare2()))
			&& (Spare3 == null ? other.getSpare3() == null : fDate.getString(Spare3).equals(other.getSpare3()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("CustomerNo") ) {
			return 0;
		}
		if( strFieldName.equals("TraceNo") ) {
			return 1;
		}
		if( strFieldName.equals("ContractNo") ) {
			return 2;
		}
		if( strFieldName.equals("SharedMark") ) {
			return 3;
		}
		if( strFieldName.equals("Authorization") ) {
			return 4;
		}
		if( strFieldName.equals("SpecialLimitMark") ) {
			return 5;
		}
		if( strFieldName.equals("CompanySource") ) {
			return 6;
		}
		if( strFieldName.equals("InstitutionSource") ) {
			return 7;
		}
		if( strFieldName.equals("BusinessLink") ) {
			return 8;
		}
		if( strFieldName.equals("CustomerContact") ) {
			return 9;
		}
		if( strFieldName.equals("AuthType") ) {
			return 10;
		}
		if( strFieldName.equals("AuthVersion") ) {
			return 11;
		}
		if( strFieldName.equals("SendDate") ) {
			return 12;
		}
		if( strFieldName.equals("SendTime") ) {
			return 13;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 14;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 15;
		}
		if( strFieldName.equals("Spare1") ) {
			return 16;
		}
		if( strFieldName.equals("Spare2") ) {
			return 17;
		}
		if( strFieldName.equals("Spare3") ) {
			return 18;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "CustomerNo";
				break;
			case 1:
				strFieldName = "TraceNo";
				break;
			case 2:
				strFieldName = "ContractNo";
				break;
			case 3:
				strFieldName = "SharedMark";
				break;
			case 4:
				strFieldName = "Authorization";
				break;
			case 5:
				strFieldName = "SpecialLimitMark";
				break;
			case 6:
				strFieldName = "CompanySource";
				break;
			case 7:
				strFieldName = "InstitutionSource";
				break;
			case 8:
				strFieldName = "BusinessLink";
				break;
			case 9:
				strFieldName = "CustomerContact";
				break;
			case 10:
				strFieldName = "AuthType";
				break;
			case 11:
				strFieldName = "AuthVersion";
				break;
			case 12:
				strFieldName = "SendDate";
				break;
			case 13:
				strFieldName = "SendTime";
				break;
			case 14:
				strFieldName = "ModifyDate";
				break;
			case 15:
				strFieldName = "ModifyTime";
				break;
			case 16:
				strFieldName = "Spare1";
				break;
			case 17:
				strFieldName = "Spare2";
				break;
			case 18:
				strFieldName = "Spare3";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("CustomerNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TraceNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContractNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SharedMark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Authorization") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SpecialLimitMark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CompanySource") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InstitutionSource") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BusinessLink") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerContact") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AuthType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AuthVersion") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SendDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("SendTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Spare1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Spare2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Spare3") ) {
			return Schema.TYPE_DATE;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_DATE;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
