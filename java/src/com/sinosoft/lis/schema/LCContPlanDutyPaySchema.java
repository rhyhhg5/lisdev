/*
 * <p>ClassName: LCContPlanDutyPaySchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 新系统模型
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LCContPlanDutyPayDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LCContPlanDutyPaySchema implements Schema
{
    // @Field
    /** 集体合同号码 */
    private String GrpContNo;
    /** 集体保单险种号码 */
    private String GrpPolNo;
    /** 险种编码 */
    private String RiskCode;
    /** 险种版本 */
    private String RiskVersion;
    /** 保险计划编码 */
    private String ContPlanCode;
    /** 责任编码 */
    private String DutyCode;
    /** 交费计划编码 */
    private String PayPlanCode;
    /** 交费计划类型 */
    private String PayPlanType;
    /** 保费分配比率 */
    private double Rate;
    /** 实际保费 */
    private double Prem;
    /** 加费评点 */
    private double SuppRiskScore;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 16; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LCContPlanDutyPaySchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[6];
        pk[0] = "GrpContNo";
        pk[1] = "GrpPolNo";
        pk[2] = "RiskCode";
        pk[3] = "ContPlanCode";
        pk[4] = "DutyCode";
        pk[5] = "PayPlanCode";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getGrpContNo()
    {
        if (GrpContNo != null && !GrpContNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GrpContNo = StrTool.unicodeToGBK(GrpContNo);
        }
        return GrpContNo;
    }

    public void setGrpContNo(String aGrpContNo)
    {
        GrpContNo = aGrpContNo;
    }

    public String getGrpPolNo()
    {
        if (GrpPolNo != null && !GrpPolNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            GrpPolNo = StrTool.unicodeToGBK(GrpPolNo);
        }
        return GrpPolNo;
    }

    public void setGrpPolNo(String aGrpPolNo)
    {
        GrpPolNo = aGrpPolNo;
    }

    public String getRiskCode()
    {
        if (RiskCode != null && !RiskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getRiskVersion()
    {
        if (RiskVersion != null && !RiskVersion.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            RiskVersion = StrTool.unicodeToGBK(RiskVersion);
        }
        return RiskVersion;
    }

    public void setRiskVersion(String aRiskVersion)
    {
        RiskVersion = aRiskVersion;
    }

    public String getContPlanCode()
    {
        if (ContPlanCode != null && !ContPlanCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ContPlanCode = StrTool.unicodeToGBK(ContPlanCode);
        }
        return ContPlanCode;
    }

    public void setContPlanCode(String aContPlanCode)
    {
        ContPlanCode = aContPlanCode;
    }

    public String getDutyCode()
    {
        if (DutyCode != null && !DutyCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            DutyCode = StrTool.unicodeToGBK(DutyCode);
        }
        return DutyCode;
    }

    public void setDutyCode(String aDutyCode)
    {
        DutyCode = aDutyCode;
    }

    public String getPayPlanCode()
    {
        if (PayPlanCode != null && !PayPlanCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            PayPlanCode = StrTool.unicodeToGBK(PayPlanCode);
        }
        return PayPlanCode;
    }

    public void setPayPlanCode(String aPayPlanCode)
    {
        PayPlanCode = aPayPlanCode;
    }

    public String getPayPlanType()
    {
        if (PayPlanType != null && !PayPlanType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            PayPlanType = StrTool.unicodeToGBK(PayPlanType);
        }
        return PayPlanType;
    }

    public void setPayPlanType(String aPayPlanType)
    {
        PayPlanType = aPayPlanType;
    }

    public double getRate()
    {
        return Rate;
    }

    public void setRate(double aRate)
    {
        Rate = aRate;
    }

    public void setRate(String aRate)
    {
        if (aRate != null && !aRate.equals(""))
        {
            Double tDouble = new Double(aRate);
            double d = tDouble.doubleValue();
            Rate = d;
        }
    }

    public double getPrem()
    {
        return Prem;
    }

    public void setPrem(double aPrem)
    {
        Prem = aPrem;
    }

    public void setPrem(String aPrem)
    {
        if (aPrem != null && !aPrem.equals(""))
        {
            Double tDouble = new Double(aPrem);
            double d = tDouble.doubleValue();
            Prem = d;
        }
    }

    public double getSuppRiskScore()
    {
        return SuppRiskScore;
    }

    public void setSuppRiskScore(double aSuppRiskScore)
    {
        SuppRiskScore = aSuppRiskScore;
    }

    public void setSuppRiskScore(String aSuppRiskScore)
    {
        if (aSuppRiskScore != null && !aSuppRiskScore.equals(""))
        {
            Double tDouble = new Double(aSuppRiskScore);
            double d = tDouble.doubleValue();
            SuppRiskScore = d;
        }
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LCContPlanDutyPaySchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LCContPlanDutyPaySchema aLCContPlanDutyPaySchema)
    {
        this.GrpContNo = aLCContPlanDutyPaySchema.getGrpContNo();
        this.GrpPolNo = aLCContPlanDutyPaySchema.getGrpPolNo();
        this.RiskCode = aLCContPlanDutyPaySchema.getRiskCode();
        this.RiskVersion = aLCContPlanDutyPaySchema.getRiskVersion();
        this.ContPlanCode = aLCContPlanDutyPaySchema.getContPlanCode();
        this.DutyCode = aLCContPlanDutyPaySchema.getDutyCode();
        this.PayPlanCode = aLCContPlanDutyPaySchema.getPayPlanCode();
        this.PayPlanType = aLCContPlanDutyPaySchema.getPayPlanType();
        this.Rate = aLCContPlanDutyPaySchema.getRate();
        this.Prem = aLCContPlanDutyPaySchema.getPrem();
        this.SuppRiskScore = aLCContPlanDutyPaySchema.getSuppRiskScore();
        this.Operator = aLCContPlanDutyPaySchema.getOperator();
        this.MakeDate = fDate.getDate(aLCContPlanDutyPaySchema.getMakeDate());
        this.MakeTime = aLCContPlanDutyPaySchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLCContPlanDutyPaySchema.getModifyDate());
        this.ModifyTime = aLCContPlanDutyPaySchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("GrpContNo") == null)
            {
                this.GrpContNo = null;
            }
            else
            {
                this.GrpContNo = rs.getString("GrpContNo").trim();
            }

            if (rs.getString("GrpPolNo") == null)
            {
                this.GrpPolNo = null;
            }
            else
            {
                this.GrpPolNo = rs.getString("GrpPolNo").trim();
            }

            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("RiskVersion") == null)
            {
                this.RiskVersion = null;
            }
            else
            {
                this.RiskVersion = rs.getString("RiskVersion").trim();
            }

            if (rs.getString("ContPlanCode") == null)
            {
                this.ContPlanCode = null;
            }
            else
            {
                this.ContPlanCode = rs.getString("ContPlanCode").trim();
            }

            if (rs.getString("DutyCode") == null)
            {
                this.DutyCode = null;
            }
            else
            {
                this.DutyCode = rs.getString("DutyCode").trim();
            }

            if (rs.getString("PayPlanCode") == null)
            {
                this.PayPlanCode = null;
            }
            else
            {
                this.PayPlanCode = rs.getString("PayPlanCode").trim();
            }

            if (rs.getString("PayPlanType") == null)
            {
                this.PayPlanType = null;
            }
            else
            {
                this.PayPlanType = rs.getString("PayPlanType").trim();
            }

            this.Rate = rs.getDouble("Rate");
            this.Prem = rs.getDouble("Prem");
            this.SuppRiskScore = rs.getDouble("SuppRiskScore");
            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCContPlanDutyPaySchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LCContPlanDutyPaySchema getSchema()
    {
        LCContPlanDutyPaySchema aLCContPlanDutyPaySchema = new
                LCContPlanDutyPaySchema();
        aLCContPlanDutyPaySchema.setSchema(this);
        return aLCContPlanDutyPaySchema;
    }

    public LCContPlanDutyPayDB getDB()
    {
        LCContPlanDutyPayDB aDBOper = new LCContPlanDutyPayDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCContPlanDutyPay描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(GrpContNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GrpPolNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskVersion)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ContPlanCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(DutyCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PayPlanCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PayPlanType)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(Rate) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(Prem) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(SuppRiskScore) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCContPlanDutyPay>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            GrpPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            RiskVersion = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                         SysConst.PACKAGESPILTER);
            ContPlanCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                          SysConst.PACKAGESPILTER);
            DutyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                      SysConst.PACKAGESPILTER);
            PayPlanCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                         SysConst.PACKAGESPILTER);
            PayPlanType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                         SysConst.PACKAGESPILTER);
            Rate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    9, SysConst.PACKAGESPILTER))).doubleValue();
            Prem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    10, SysConst.PACKAGESPILTER))).doubleValue();
            SuppRiskScore = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 11, SysConst.PACKAGESPILTER))).doubleValue();
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 13, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 15, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCContPlanDutyPaySchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("GrpContNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GrpContNo));
        }
        if (FCode.equals("GrpPolNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GrpPolNo));
        }
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskCode));
        }
        if (FCode.equals("RiskVersion"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskVersion));
        }
        if (FCode.equals("ContPlanCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ContPlanCode));
        }
        if (FCode.equals("DutyCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DutyCode));
        }
        if (FCode.equals("PayPlanCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PayPlanCode));
        }
        if (FCode.equals("PayPlanType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PayPlanType));
        }
        if (FCode.equals("Rate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Rate));
        }
        if (FCode.equals("Prem"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Prem));
        }
        if (FCode.equals("SuppRiskScore"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SuppRiskScore));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(GrpContNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(GrpPolNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(RiskVersion);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(ContPlanCode);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(DutyCode);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(PayPlanCode);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(PayPlanType);
                break;
            case 8:
                strFieldValue = String.valueOf(Rate);
                break;
            case 9:
                strFieldValue = String.valueOf(Prem);
                break;
            case 10:
                strFieldValue = String.valueOf(SuppRiskScore);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("GrpContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
            {
                GrpContNo = null;
            }
        }
        if (FCode.equals("GrpPolNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpPolNo = FValue.trim();
            }
            else
            {
                GrpPolNo = null;
            }
        }
        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("RiskVersion"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskVersion = FValue.trim();
            }
            else
            {
                RiskVersion = null;
            }
        }
        if (FCode.equals("ContPlanCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ContPlanCode = FValue.trim();
            }
            else
            {
                ContPlanCode = null;
            }
        }
        if (FCode.equals("DutyCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DutyCode = FValue.trim();
            }
            else
            {
                DutyCode = null;
            }
        }
        if (FCode.equals("PayPlanCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PayPlanCode = FValue.trim();
            }
            else
            {
                PayPlanCode = null;
            }
        }
        if (FCode.equals("PayPlanType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PayPlanType = FValue.trim();
            }
            else
            {
                PayPlanType = null;
            }
        }
        if (FCode.equals("Rate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Rate = d;
            }
        }
        if (FCode.equals("Prem"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Prem = d;
            }
        }
        if (FCode.equals("SuppRiskScore"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                SuppRiskScore = d;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LCContPlanDutyPaySchema other = (LCContPlanDutyPaySchema) otherObject;
        return
                GrpContNo.equals(other.getGrpContNo())
                && GrpPolNo.equals(other.getGrpPolNo())
                && RiskCode.equals(other.getRiskCode())
                && RiskVersion.equals(other.getRiskVersion())
                && ContPlanCode.equals(other.getContPlanCode())
                && DutyCode.equals(other.getDutyCode())
                && PayPlanCode.equals(other.getPayPlanCode())
                && PayPlanType.equals(other.getPayPlanType())
                && Rate == other.getRate()
                && Prem == other.getPrem()
                && SuppRiskScore == other.getSuppRiskScore()
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("GrpContNo"))
        {
            return 0;
        }
        if (strFieldName.equals("GrpPolNo"))
        {
            return 1;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return 2;
        }
        if (strFieldName.equals("RiskVersion"))
        {
            return 3;
        }
        if (strFieldName.equals("ContPlanCode"))
        {
            return 4;
        }
        if (strFieldName.equals("DutyCode"))
        {
            return 5;
        }
        if (strFieldName.equals("PayPlanCode"))
        {
            return 6;
        }
        if (strFieldName.equals("PayPlanType"))
        {
            return 7;
        }
        if (strFieldName.equals("Rate"))
        {
            return 8;
        }
        if (strFieldName.equals("Prem"))
        {
            return 9;
        }
        if (strFieldName.equals("SuppRiskScore"))
        {
            return 10;
        }
        if (strFieldName.equals("Operator"))
        {
            return 11;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 12;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 13;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 14;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 15;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "GrpContNo";
                break;
            case 1:
                strFieldName = "GrpPolNo";
                break;
            case 2:
                strFieldName = "RiskCode";
                break;
            case 3:
                strFieldName = "RiskVersion";
                break;
            case 4:
                strFieldName = "ContPlanCode";
                break;
            case 5:
                strFieldName = "DutyCode";
                break;
            case 6:
                strFieldName = "PayPlanCode";
                break;
            case 7:
                strFieldName = "PayPlanType";
                break;
            case 8:
                strFieldName = "Rate";
                break;
            case 9:
                strFieldName = "Prem";
                break;
            case 10:
                strFieldName = "SuppRiskScore";
                break;
            case 11:
                strFieldName = "Operator";
                break;
            case 12:
                strFieldName = "MakeDate";
                break;
            case 13:
                strFieldName = "MakeTime";
                break;
            case 14:
                strFieldName = "ModifyDate";
                break;
            case 15:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("GrpContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpPolNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskVersion"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContPlanCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DutyCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PayPlanCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PayPlanType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Rate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Prem"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("SuppRiskScore"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 9:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 10:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
