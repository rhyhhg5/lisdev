/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LOBInsureAccFeeDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LOBInsureAccFeeSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-03-23
 */
public class LOBInsureAccFeeSchema implements Schema
{
    // @Field
    /** 集体合同号码 */
    private String GrpContNo;
    /** 集体保单险种号码 */
    private String GrpPolNo;
    /** 保单险种号码 */
    private String PolNo;
    /** 保险帐户号码 */
    private String InsuAccNo;
    /** 合同号码 */
    private String ContNo;
    /** 管理机构 */
    private String ManageCom;
    /** 印刷号码 */
    private String PrtNo;
    /** 险种编码 */
    private String RiskCode;
    /** 账户类型 */
    private String AccType;
    /** 投资类型 */
    private String InvestType;
    /** 基金公司代码 */
    private String FundCompanyCode;
    /** 被保人客户号码 */
    private String InsuredNo;
    /** 投保人客户号码 */
    private String AppntNo;
    /** 账户所有者 */
    private String Owner;
    /** 账户结算方式 */
    private String AccComputeFlag;
    /** 账户成立日期 */
    private Date AccFoundDate;
    /** 账户成立时间 */
    private String AccFoundTime;
    /** 管理费比例 */
    private double FeeRate;
    /** 本次管理费金额 */
    private double Fee;
    /** 本次管理费单位数 */
    private double FeeUnit;
    /** 结算日期 */
    private Date BalaDate;
    /** 结算时间 */
    private String BalaTime;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 27; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LOBInsureAccFeeSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "PolNo";
        pk[1] = "InsuAccNo";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getGrpContNo()
    {
        if (GrpContNo != null && !GrpContNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GrpContNo = StrTool.unicodeToGBK(GrpContNo);
        }
        return GrpContNo;
    }

    public void setGrpContNo(String aGrpContNo)
    {
        GrpContNo = aGrpContNo;
    }

    public String getGrpPolNo()
    {
        if (GrpPolNo != null && !GrpPolNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            GrpPolNo = StrTool.unicodeToGBK(GrpPolNo);
        }
        return GrpPolNo;
    }

    public void setGrpPolNo(String aGrpPolNo)
    {
        GrpPolNo = aGrpPolNo;
    }

    public String getPolNo()
    {
        if (PolNo != null && !PolNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            PolNo = StrTool.unicodeToGBK(PolNo);
        }
        return PolNo;
    }

    public void setPolNo(String aPolNo)
    {
        PolNo = aPolNo;
    }

    public String getInsuAccNo()
    {
        if (InsuAccNo != null && !InsuAccNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            InsuAccNo = StrTool.unicodeToGBK(InsuAccNo);
        }
        return InsuAccNo;
    }

    public void setInsuAccNo(String aInsuAccNo)
    {
        InsuAccNo = aInsuAccNo;
    }

    public String getContNo()
    {
        if (ContNo != null && !ContNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            ContNo = StrTool.unicodeToGBK(ContNo);
        }
        return ContNo;
    }

    public void setContNo(String aContNo)
    {
        ContNo = aContNo;
    }

    public String getManageCom()
    {
        if (ManageCom != null && !ManageCom.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ManageCom = StrTool.unicodeToGBK(ManageCom);
        }
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public String getPrtNo()
    {
        if (PrtNo != null && !PrtNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            PrtNo = StrTool.unicodeToGBK(PrtNo);
        }
        return PrtNo;
    }

    public void setPrtNo(String aPrtNo)
    {
        PrtNo = aPrtNo;
    }

    public String getRiskCode()
    {
        if (RiskCode != null && !RiskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getAccType()
    {
        if (AccType != null && !AccType.equals("") && SysConst.CHANGECHARSET == true)
        {
            AccType = StrTool.unicodeToGBK(AccType);
        }
        return AccType;
    }

    public void setAccType(String aAccType)
    {
        AccType = aAccType;
    }

    public String getInvestType()
    {
        if (InvestType != null && !InvestType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            InvestType = StrTool.unicodeToGBK(InvestType);
        }
        return InvestType;
    }

    public void setInvestType(String aInvestType)
    {
        InvestType = aInvestType;
    }

    public String getFundCompanyCode()
    {
        if (FundCompanyCode != null && !FundCompanyCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            FundCompanyCode = StrTool.unicodeToGBK(FundCompanyCode);
        }
        return FundCompanyCode;
    }

    public void setFundCompanyCode(String aFundCompanyCode)
    {
        FundCompanyCode = aFundCompanyCode;
    }

    public String getInsuredNo()
    {
        if (InsuredNo != null && !InsuredNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            InsuredNo = StrTool.unicodeToGBK(InsuredNo);
        }
        return InsuredNo;
    }

    public void setInsuredNo(String aInsuredNo)
    {
        InsuredNo = aInsuredNo;
    }

    public String getAppntNo()
    {
        if (AppntNo != null && !AppntNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            AppntNo = StrTool.unicodeToGBK(AppntNo);
        }
        return AppntNo;
    }

    public void setAppntNo(String aAppntNo)
    {
        AppntNo = aAppntNo;
    }

    public String getOwner()
    {
        if (Owner != null && !Owner.equals("") && SysConst.CHANGECHARSET == true)
        {
            Owner = StrTool.unicodeToGBK(Owner);
        }
        return Owner;
    }

    public void setOwner(String aOwner)
    {
        Owner = aOwner;
    }

    public String getAccComputeFlag()
    {
        if (AccComputeFlag != null && !AccComputeFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AccComputeFlag = StrTool.unicodeToGBK(AccComputeFlag);
        }
        return AccComputeFlag;
    }

    public void setAccComputeFlag(String aAccComputeFlag)
    {
        AccComputeFlag = aAccComputeFlag;
    }

    public String getAccFoundDate()
    {
        if (AccFoundDate != null)
        {
            return fDate.getString(AccFoundDate);
        }
        else
        {
            return null;
        }
    }

    public void setAccFoundDate(Date aAccFoundDate)
    {
        AccFoundDate = aAccFoundDate;
    }

    public void setAccFoundDate(String aAccFoundDate)
    {
        if (aAccFoundDate != null && !aAccFoundDate.equals(""))
        {
            AccFoundDate = fDate.getDate(aAccFoundDate);
        }
        else
        {
            AccFoundDate = null;
        }
    }

    public String getAccFoundTime()
    {
        if (AccFoundTime != null && !AccFoundTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AccFoundTime = StrTool.unicodeToGBK(AccFoundTime);
        }
        return AccFoundTime;
    }

    public void setAccFoundTime(String aAccFoundTime)
    {
        AccFoundTime = aAccFoundTime;
    }

    public double getFeeRate()
    {
        return FeeRate;
    }

    public void setFeeRate(double aFeeRate)
    {
        FeeRate = aFeeRate;
    }

    public void setFeeRate(String aFeeRate)
    {
        if (aFeeRate != null && !aFeeRate.equals(""))
        {
            Double tDouble = new Double(aFeeRate);
            double d = tDouble.doubleValue();
            FeeRate = d;
        }
    }

    public double getFee()
    {
        return Fee;
    }

    public void setFee(double aFee)
    {
        Fee = aFee;
    }

    public void setFee(String aFee)
    {
        if (aFee != null && !aFee.equals(""))
        {
            Double tDouble = new Double(aFee);
            double d = tDouble.doubleValue();
            Fee = d;
        }
    }

    public double getFeeUnit()
    {
        return FeeUnit;
    }

    public void setFeeUnit(double aFeeUnit)
    {
        FeeUnit = aFeeUnit;
    }

    public void setFeeUnit(String aFeeUnit)
    {
        if (aFeeUnit != null && !aFeeUnit.equals(""))
        {
            Double tDouble = new Double(aFeeUnit);
            double d = tDouble.doubleValue();
            FeeUnit = d;
        }
    }

    public String getBalaDate()
    {
        if (BalaDate != null)
        {
            return fDate.getString(BalaDate);
        }
        else
        {
            return null;
        }
    }

    public void setBalaDate(Date aBalaDate)
    {
        BalaDate = aBalaDate;
    }

    public void setBalaDate(String aBalaDate)
    {
        if (aBalaDate != null && !aBalaDate.equals(""))
        {
            BalaDate = fDate.getDate(aBalaDate);
        }
        else
        {
            BalaDate = null;
        }
    }

    public String getBalaTime()
    {
        if (BalaTime != null && !BalaTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            BalaTime = StrTool.unicodeToGBK(BalaTime);
        }
        return BalaTime;
    }

    public void setBalaTime(String aBalaTime)
    {
        BalaTime = aBalaTime;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LOBInsureAccFeeSchema 对象给 Schema 赋值
     * @param: aLOBInsureAccFeeSchema LOBInsureAccFeeSchema
     **/
    public void setSchema(LOBInsureAccFeeSchema aLOBInsureAccFeeSchema)
    {
        this.GrpContNo = aLOBInsureAccFeeSchema.getGrpContNo();
        this.GrpPolNo = aLOBInsureAccFeeSchema.getGrpPolNo();
        this.PolNo = aLOBInsureAccFeeSchema.getPolNo();
        this.InsuAccNo = aLOBInsureAccFeeSchema.getInsuAccNo();
        this.ContNo = aLOBInsureAccFeeSchema.getContNo();
        this.ManageCom = aLOBInsureAccFeeSchema.getManageCom();
        this.PrtNo = aLOBInsureAccFeeSchema.getPrtNo();
        this.RiskCode = aLOBInsureAccFeeSchema.getRiskCode();
        this.AccType = aLOBInsureAccFeeSchema.getAccType();
        this.InvestType = aLOBInsureAccFeeSchema.getInvestType();
        this.FundCompanyCode = aLOBInsureAccFeeSchema.getFundCompanyCode();
        this.InsuredNo = aLOBInsureAccFeeSchema.getInsuredNo();
        this.AppntNo = aLOBInsureAccFeeSchema.getAppntNo();
        this.Owner = aLOBInsureAccFeeSchema.getOwner();
        this.AccComputeFlag = aLOBInsureAccFeeSchema.getAccComputeFlag();
        this.AccFoundDate = fDate.getDate(aLOBInsureAccFeeSchema.
                                          getAccFoundDate());
        this.AccFoundTime = aLOBInsureAccFeeSchema.getAccFoundTime();
        this.FeeRate = aLOBInsureAccFeeSchema.getFeeRate();
        this.Fee = aLOBInsureAccFeeSchema.getFee();
        this.FeeUnit = aLOBInsureAccFeeSchema.getFeeUnit();
        this.BalaDate = fDate.getDate(aLOBInsureAccFeeSchema.getBalaDate());
        this.BalaTime = aLOBInsureAccFeeSchema.getBalaTime();
        this.Operator = aLOBInsureAccFeeSchema.getOperator();
        this.MakeDate = fDate.getDate(aLOBInsureAccFeeSchema.getMakeDate());
        this.MakeTime = aLOBInsureAccFeeSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLOBInsureAccFeeSchema.getModifyDate());
        this.ModifyTime = aLOBInsureAccFeeSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("GrpContNo") == null)
            {
                this.GrpContNo = null;
            }
            else
            {
                this.GrpContNo = rs.getString("GrpContNo").trim();
            }

            if (rs.getString("GrpPolNo") == null)
            {
                this.GrpPolNo = null;
            }
            else
            {
                this.GrpPolNo = rs.getString("GrpPolNo").trim();
            }

            if (rs.getString("PolNo") == null)
            {
                this.PolNo = null;
            }
            else
            {
                this.PolNo = rs.getString("PolNo").trim();
            }

            if (rs.getString("InsuAccNo") == null)
            {
                this.InsuAccNo = null;
            }
            else
            {
                this.InsuAccNo = rs.getString("InsuAccNo").trim();
            }

            if (rs.getString("ContNo") == null)
            {
                this.ContNo = null;
            }
            else
            {
                this.ContNo = rs.getString("ContNo").trim();
            }

            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("PrtNo") == null)
            {
                this.PrtNo = null;
            }
            else
            {
                this.PrtNo = rs.getString("PrtNo").trim();
            }

            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("AccType") == null)
            {
                this.AccType = null;
            }
            else
            {
                this.AccType = rs.getString("AccType").trim();
            }

            if (rs.getString("InvestType") == null)
            {
                this.InvestType = null;
            }
            else
            {
                this.InvestType = rs.getString("InvestType").trim();
            }

            if (rs.getString("FundCompanyCode") == null)
            {
                this.FundCompanyCode = null;
            }
            else
            {
                this.FundCompanyCode = rs.getString("FundCompanyCode").trim();
            }

            if (rs.getString("InsuredNo") == null)
            {
                this.InsuredNo = null;
            }
            else
            {
                this.InsuredNo = rs.getString("InsuredNo").trim();
            }

            if (rs.getString("AppntNo") == null)
            {
                this.AppntNo = null;
            }
            else
            {
                this.AppntNo = rs.getString("AppntNo").trim();
            }

            if (rs.getString("Owner") == null)
            {
                this.Owner = null;
            }
            else
            {
                this.Owner = rs.getString("Owner").trim();
            }

            if (rs.getString("AccComputeFlag") == null)
            {
                this.AccComputeFlag = null;
            }
            else
            {
                this.AccComputeFlag = rs.getString("AccComputeFlag").trim();
            }

            this.AccFoundDate = rs.getDate("AccFoundDate");
            if (rs.getString("AccFoundTime") == null)
            {
                this.AccFoundTime = null;
            }
            else
            {
                this.AccFoundTime = rs.getString("AccFoundTime").trim();
            }

            this.FeeRate = rs.getDouble("FeeRate");
            this.Fee = rs.getDouble("Fee");
            this.FeeUnit = rs.getDouble("FeeUnit");
            this.BalaDate = rs.getDate("BalaDate");
            if (rs.getString("BalaTime") == null)
            {
                this.BalaTime = null;
            }
            else
            {
                this.BalaTime = rs.getString("BalaTime").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LOBInsureAccFeeSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LOBInsureAccFeeSchema getSchema()
    {
        LOBInsureAccFeeSchema aLOBInsureAccFeeSchema = new
                LOBInsureAccFeeSchema();
        aLOBInsureAccFeeSchema.setSchema(this);
        return aLOBInsureAccFeeSchema;
    }

    public LOBInsureAccFeeDB getDB()
    {
        LOBInsureAccFeeDB aDBOper = new LOBInsureAccFeeDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOBInsureAccFee描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(GrpContNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GrpPolNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PolNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InsuAccNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ContNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ManageCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PrtNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AccType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InvestType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(FundCompanyCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InsuredNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AppntNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Owner)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AccComputeFlag)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(AccFoundDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AccFoundTime)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(FeeRate) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(Fee) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(FeeUnit) + SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(BalaDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(BalaTime)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOBInsureAccFee>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            GrpPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                   SysConst.PACKAGESPILTER);
            InsuAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                       SysConst.PACKAGESPILTER);
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                    SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                       SysConst.PACKAGESPILTER);
            PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                   SysConst.PACKAGESPILTER);
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                      SysConst.PACKAGESPILTER);
            AccType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                     SysConst.PACKAGESPILTER);
            InvestType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                        SysConst.PACKAGESPILTER);
            FundCompanyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             11, SysConst.PACKAGESPILTER);
            InsuredNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                       SysConst.PACKAGESPILTER);
            AppntNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                     SysConst.PACKAGESPILTER);
            Owner = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                   SysConst.PACKAGESPILTER);
            AccComputeFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                            15, SysConst.PACKAGESPILTER);
            AccFoundDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 16, SysConst.PACKAGESPILTER));
            AccFoundTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                          SysConst.PACKAGESPILTER);
            FeeRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 18, SysConst.PACKAGESPILTER))).doubleValue();
            Fee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    19, SysConst.PACKAGESPILTER))).doubleValue();
            FeeUnit = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 20, SysConst.PACKAGESPILTER))).doubleValue();
            BalaDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 21, SysConst.PACKAGESPILTER));
            BalaTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,
                                      SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 24, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 26, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LOBInsureAccFeeSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("GrpContNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equals("GrpPolNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpPolNo));
        }
        if (FCode.equals("PolNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
        }
        if (FCode.equals("InsuAccNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuAccNo));
        }
        if (FCode.equals("ContNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("PrtNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equals("AccType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccType));
        }
        if (FCode.equals("InvestType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InvestType));
        }
        if (FCode.equals("FundCompanyCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FundCompanyCode));
        }
        if (FCode.equals("InsuredNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredNo));
        }
        if (FCode.equals("AppntNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
        }
        if (FCode.equals("Owner"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Owner));
        }
        if (FCode.equals("AccComputeFlag"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccComputeFlag));
        }
        if (FCode.equals("AccFoundDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getAccFoundDate()));
        }
        if (FCode.equals("AccFoundTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccFoundTime));
        }
        if (FCode.equals("FeeRate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeRate));
        }
        if (FCode.equals("Fee"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Fee));
        }
        if (FCode.equals("FeeUnit"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeUnit));
        }
        if (FCode.equals("BalaDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getBalaDate()));
        }
        if (FCode.equals("BalaTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BalaTime));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(GrpContNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(GrpPolNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(PolNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(InsuAccNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(PrtNo);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(AccType);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(InvestType);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(FundCompanyCode);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(InsuredNo);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(AppntNo);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(Owner);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(AccComputeFlag);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getAccFoundDate()));
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(AccFoundTime);
                break;
            case 17:
                strFieldValue = String.valueOf(FeeRate);
                break;
            case 18:
                strFieldValue = String.valueOf(Fee);
                break;
            case 19:
                strFieldValue = String.valueOf(FeeUnit);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getBalaDate()));
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(BalaTime);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("GrpContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
            {
                GrpContNo = null;
            }
        }
        if (FCode.equals("GrpPolNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpPolNo = FValue.trim();
            }
            else
            {
                GrpPolNo = null;
            }
        }
        if (FCode.equals("PolNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
            {
                PolNo = null;
            }
        }
        if (FCode.equals("InsuAccNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InsuAccNo = FValue.trim();
            }
            else
            {
                InsuAccNo = null;
            }
        }
        if (FCode.equals("ContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
            {
                ContNo = null;
            }
        }
        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("PrtNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PrtNo = FValue.trim();
            }
            else
            {
                PrtNo = null;
            }
        }
        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("AccType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AccType = FValue.trim();
            }
            else
            {
                AccType = null;
            }
        }
        if (FCode.equals("InvestType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InvestType = FValue.trim();
            }
            else
            {
                InvestType = null;
            }
        }
        if (FCode.equals("FundCompanyCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                FundCompanyCode = FValue.trim();
            }
            else
            {
                FundCompanyCode = null;
            }
        }
        if (FCode.equals("InsuredNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InsuredNo = FValue.trim();
            }
            else
            {
                InsuredNo = null;
            }
        }
        if (FCode.equals("AppntNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppntNo = FValue.trim();
            }
            else
            {
                AppntNo = null;
            }
        }
        if (FCode.equals("Owner"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Owner = FValue.trim();
            }
            else
            {
                Owner = null;
            }
        }
        if (FCode.equals("AccComputeFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AccComputeFlag = FValue.trim();
            }
            else
            {
                AccComputeFlag = null;
            }
        }
        if (FCode.equals("AccFoundDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AccFoundDate = fDate.getDate(FValue);
            }
            else
            {
                AccFoundDate = null;
            }
        }
        if (FCode.equals("AccFoundTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AccFoundTime = FValue.trim();
            }
            else
            {
                AccFoundTime = null;
            }
        }
        if (FCode.equals("FeeRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                FeeRate = d;
            }
        }
        if (FCode.equals("Fee"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Fee = d;
            }
        }
        if (FCode.equals("FeeUnit"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                FeeUnit = d;
            }
        }
        if (FCode.equals("BalaDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BalaDate = fDate.getDate(FValue);
            }
            else
            {
                BalaDate = null;
            }
        }
        if (FCode.equals("BalaTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BalaTime = FValue.trim();
            }
            else
            {
                BalaTime = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LOBInsureAccFeeSchema other = (LOBInsureAccFeeSchema) otherObject;
        return
                GrpContNo.equals(other.getGrpContNo())
                && GrpPolNo.equals(other.getGrpPolNo())
                && PolNo.equals(other.getPolNo())
                && InsuAccNo.equals(other.getInsuAccNo())
                && ContNo.equals(other.getContNo())
                && ManageCom.equals(other.getManageCom())
                && PrtNo.equals(other.getPrtNo())
                && RiskCode.equals(other.getRiskCode())
                && AccType.equals(other.getAccType())
                && InvestType.equals(other.getInvestType())
                && FundCompanyCode.equals(other.getFundCompanyCode())
                && InsuredNo.equals(other.getInsuredNo())
                && AppntNo.equals(other.getAppntNo())
                && Owner.equals(other.getOwner())
                && AccComputeFlag.equals(other.getAccComputeFlag())
                && fDate.getString(AccFoundDate).equals(other.getAccFoundDate())
                && AccFoundTime.equals(other.getAccFoundTime())
                && FeeRate == other.getFeeRate()
                && Fee == other.getFee()
                && FeeUnit == other.getFeeUnit()
                && fDate.getString(BalaDate).equals(other.getBalaDate())
                && BalaTime.equals(other.getBalaTime())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("GrpContNo"))
        {
            return 0;
        }
        if (strFieldName.equals("GrpPolNo"))
        {
            return 1;
        }
        if (strFieldName.equals("PolNo"))
        {
            return 2;
        }
        if (strFieldName.equals("InsuAccNo"))
        {
            return 3;
        }
        if (strFieldName.equals("ContNo"))
        {
            return 4;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return 5;
        }
        if (strFieldName.equals("PrtNo"))
        {
            return 6;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return 7;
        }
        if (strFieldName.equals("AccType"))
        {
            return 8;
        }
        if (strFieldName.equals("InvestType"))
        {
            return 9;
        }
        if (strFieldName.equals("FundCompanyCode"))
        {
            return 10;
        }
        if (strFieldName.equals("InsuredNo"))
        {
            return 11;
        }
        if (strFieldName.equals("AppntNo"))
        {
            return 12;
        }
        if (strFieldName.equals("Owner"))
        {
            return 13;
        }
        if (strFieldName.equals("AccComputeFlag"))
        {
            return 14;
        }
        if (strFieldName.equals("AccFoundDate"))
        {
            return 15;
        }
        if (strFieldName.equals("AccFoundTime"))
        {
            return 16;
        }
        if (strFieldName.equals("FeeRate"))
        {
            return 17;
        }
        if (strFieldName.equals("Fee"))
        {
            return 18;
        }
        if (strFieldName.equals("FeeUnit"))
        {
            return 19;
        }
        if (strFieldName.equals("BalaDate"))
        {
            return 20;
        }
        if (strFieldName.equals("BalaTime"))
        {
            return 21;
        }
        if (strFieldName.equals("Operator"))
        {
            return 22;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 23;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 24;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 25;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 26;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "GrpContNo";
                break;
            case 1:
                strFieldName = "GrpPolNo";
                break;
            case 2:
                strFieldName = "PolNo";
                break;
            case 3:
                strFieldName = "InsuAccNo";
                break;
            case 4:
                strFieldName = "ContNo";
                break;
            case 5:
                strFieldName = "ManageCom";
                break;
            case 6:
                strFieldName = "PrtNo";
                break;
            case 7:
                strFieldName = "RiskCode";
                break;
            case 8:
                strFieldName = "AccType";
                break;
            case 9:
                strFieldName = "InvestType";
                break;
            case 10:
                strFieldName = "FundCompanyCode";
                break;
            case 11:
                strFieldName = "InsuredNo";
                break;
            case 12:
                strFieldName = "AppntNo";
                break;
            case 13:
                strFieldName = "Owner";
                break;
            case 14:
                strFieldName = "AccComputeFlag";
                break;
            case 15:
                strFieldName = "AccFoundDate";
                break;
            case 16:
                strFieldName = "AccFoundTime";
                break;
            case 17:
                strFieldName = "FeeRate";
                break;
            case 18:
                strFieldName = "Fee";
                break;
            case 19:
                strFieldName = "FeeUnit";
                break;
            case 20:
                strFieldName = "BalaDate";
                break;
            case 21:
                strFieldName = "BalaTime";
                break;
            case 22:
                strFieldName = "Operator";
                break;
            case 23:
                strFieldName = "MakeDate";
                break;
            case 24:
                strFieldName = "MakeTime";
                break;
            case 25:
                strFieldName = "ModifyDate";
                break;
            case 26:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("GrpContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpPolNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PolNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsuAccNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PrtNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AccType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InvestType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FundCompanyCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsuredNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppntNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Owner"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AccComputeFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AccFoundDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("AccFoundTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FeeRate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Fee"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("FeeUnit"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("BalaDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("BalaTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 17:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 18:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 19:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 20:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 21:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 22:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 23:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 24:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 25:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 26:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
