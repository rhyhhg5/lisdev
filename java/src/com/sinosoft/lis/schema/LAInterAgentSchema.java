/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LAInterAgentDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

/*
 * <p>ClassName: LAInterAgentSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-02-28
 */
public class LAInterAgentSchema implements Schema
{
    // @Field
    /** 原代理人编码 */
    private String AgentCode1;
    /** 原代理人类别 */
    private String AgentType;
    /** 团险代理人编码 */
    private String AgentCode2;
    /** 团险代理人流水号 */
    private String AgentSN2;

    public static final int FIELDNUM = 4; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LAInterAgentSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "AgentCode2";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getAgentCode1()
    {
        if (AgentCode1 != null && !AgentCode1.equals("") &&
            SysConst.CHANGECHARSET)
        {
            AgentCode1 = StrTool.unicodeToGBK(AgentCode1);
        }
        return AgentCode1;
    }

    public void setAgentCode1(String aAgentCode1)
    {
        AgentCode1 = aAgentCode1;
    }

    public String getAgentType()
    {
        if (AgentType != null && !AgentType.equals("") &&
            SysConst.CHANGECHARSET)
        {
            AgentType = StrTool.unicodeToGBK(AgentType);
        }
        return AgentType;
    }

    public void setAgentType(String aAgentType)
    {
        AgentType = aAgentType;
    }

    public String getAgentCode2()
    {
        if (AgentCode2 != null && !AgentCode2.equals("") &&
            SysConst.CHANGECHARSET)
        {
            AgentCode2 = StrTool.unicodeToGBK(AgentCode2);
        }
        return AgentCode2;
    }

    public void setAgentCode2(String aAgentCode2)
    {
        AgentCode2 = aAgentCode2;
    }

    public String getAgentSN2()
    {
        if (AgentSN2 != null && !AgentSN2.equals("") && SysConst.CHANGECHARSET)
        {
            AgentSN2 = StrTool.unicodeToGBK(AgentSN2);
        }
        return AgentSN2;
    }

    public void setAgentSN2(String aAgentSN2)
    {
        AgentSN2 = aAgentSN2;
    }

    /**
     * 使用另外一个 LAInterAgentSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LAInterAgentSchema aLAInterAgentSchema)
    {
        this.AgentCode1 = aLAInterAgentSchema.getAgentCode1();
        this.AgentType = aLAInterAgentSchema.getAgentType();
        this.AgentCode2 = aLAInterAgentSchema.getAgentCode2();
        this.AgentSN2 = aLAInterAgentSchema.getAgentSN2();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("AgentCode1") == null)
            {
                this.AgentCode1 = null;
            }
            else
            {
                this.AgentCode1 = rs.getString("AgentCode1").trim();
            }

            if (rs.getString("AgentType") == null)
            {
                this.AgentType = null;
            }
            else
            {
                this.AgentType = rs.getString("AgentType").trim();
            }

            if (rs.getString("AgentCode2") == null)
            {
                this.AgentCode2 = null;
            }
            else
            {
                this.AgentCode2 = rs.getString("AgentCode2").trim();
            }

            if (rs.getString("AgentSN2") == null)
            {
                this.AgentSN2 = null;
            }
            else
            {
                this.AgentSN2 = rs.getString("AgentSN2").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAInterAgentSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LAInterAgentSchema getSchema()
    {
        LAInterAgentSchema aLAInterAgentSchema = new LAInterAgentSchema();
        aLAInterAgentSchema.setSchema(this);
        return aLAInterAgentSchema;
    }

    public LAInterAgentDB getDB()
    {
        LAInterAgentDB aDBOper = new LAInterAgentDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAInterAgent描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(AgentCode1)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgentType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgentCode2)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgentSN2));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAInterAgent>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            AgentCode1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                        SysConst.PACKAGESPILTER);
            AgentType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                       SysConst.PACKAGESPILTER);
            AgentCode2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                        SysConst.PACKAGESPILTER);
            AgentSN2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                      SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAInterAgentSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("AgentCode1"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode1));
        }
        if (FCode.equals("AgentType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentType));
        }
        if (FCode.equals("AgentCode2"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode2));
        }
        if (FCode.equals("AgentSN2"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentSN2));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(AgentCode1);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(AgentType);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(AgentCode2);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(AgentSN2);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("AgentCode1"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentCode1 = FValue.trim();
            }
            else
            {
                AgentCode1 = null;
            }
        }
        if (FCode.equals("AgentType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentType = FValue.trim();
            }
            else
            {
                AgentType = null;
            }
        }
        if (FCode.equals("AgentCode2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentCode2 = FValue.trim();
            }
            else
            {
                AgentCode2 = null;
            }
        }
        if (FCode.equals("AgentSN2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentSN2 = FValue.trim();
            }
            else
            {
                AgentSN2 = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LAInterAgentSchema other = (LAInterAgentSchema) otherObject;
        return
                AgentCode1.equals(other.getAgentCode1())
                && AgentType.equals(other.getAgentType())
                && AgentCode2.equals(other.getAgentCode2())
                && AgentSN2.equals(other.getAgentSN2());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("AgentCode1"))
        {
            return 0;
        }
        if (strFieldName.equals("AgentType"))
        {
            return 1;
        }
        if (strFieldName.equals("AgentCode2"))
        {
            return 2;
        }
        if (strFieldName.equals("AgentSN2"))
        {
            return 3;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "AgentCode1";
                break;
            case 1:
                strFieldName = "AgentType";
                break;
            case 2:
                strFieldName = "AgentCode2";
                break;
            case 3:
                strFieldName = "AgentSN2";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("AgentCode1"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentCode2"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentSN2"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
