/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LDAreaHospitalDB;

/*
 * <p>ClassName: LDAreaHospitalSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2010-03-17
 */
public class LDAreaHospitalSchema implements Schema, Cloneable
{
	// @Field
	/** 地区编码 */
	private String AreaCode;
	/** 年度 */
	private String Year;
	/** 管理机构 */
	private String ManageCom;
	/** 医院级别1 */
	private String LevelCode1;
	/** 医院级别2 */
	private String LevelCode2;
	/** 地区名称 */
	private String AreaName;
	/** 地区年均住院费用 */
	private double AverageCharge;
	/** 地区年度平均住院日 */
	private String AverageDate;
	/** 数据来源 */
	private String DataSource;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 备用字段1 */
	private String StandbyFlag1;
	/** 备用字段2 */
	private String StandbyFlag2;
	/** 地区平均药品费占比 */
	private String FeeScale;
	/** 地区住院总人次 */
	private String HospitalCount;
	/** 地区门诊总人次 */
	private String ClinicCount;

	public static final int FIELDNUM = 19;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LDAreaHospitalSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "AreaCode";
		pk[1] = "Year";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LDAreaHospitalSchema cloned = (LDAreaHospitalSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getAreaCode()
	{
		return AreaCode;
	}
	public void setAreaCode(String aAreaCode)
	{
		AreaCode = aAreaCode;
	}
	public String getYear()
	{
		return Year;
	}
	public void setYear(String aYear)
	{
		Year = aYear;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getLevelCode1()
	{
		return LevelCode1;
	}
	public void setLevelCode1(String aLevelCode1)
	{
		LevelCode1 = aLevelCode1;
	}
	public String getLevelCode2()
	{
		return LevelCode2;
	}
	public void setLevelCode2(String aLevelCode2)
	{
		LevelCode2 = aLevelCode2;
	}
	public String getAreaName()
	{
		return AreaName;
	}
	public void setAreaName(String aAreaName)
	{
		AreaName = aAreaName;
	}
	public double getAverageCharge()
	{
		return AverageCharge;
	}
	public void setAverageCharge(double aAverageCharge)
	{
		AverageCharge = Arith.round(aAverageCharge,2);
	}
	public void setAverageCharge(String aAverageCharge)
	{
		if (aAverageCharge != null && !aAverageCharge.equals(""))
		{
			Double tDouble = new Double(aAverageCharge);
			double d = tDouble.doubleValue();
                AverageCharge = Arith.round(d,2);
		}
	}

	public String getAverageDate()
	{
		return AverageDate;
	}
	public void setAverageDate(String aAverageDate)
	{
		AverageDate = aAverageDate;
	}
	public String getDataSource()
	{
		return DataSource;
	}
	public void setDataSource(String aDataSource)
	{
		DataSource = aDataSource;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getStandbyFlag1()
	{
		return StandbyFlag1;
	}
	public void setStandbyFlag1(String aStandbyFlag1)
	{
		StandbyFlag1 = aStandbyFlag1;
	}
	public String getStandbyFlag2()
	{
		return StandbyFlag2;
	}
	public void setStandbyFlag2(String aStandbyFlag2)
	{
		StandbyFlag2 = aStandbyFlag2;
	}
	public String getFeeScale()
	{
		return FeeScale;
	}
	public void setFeeScale(String aFeeScale)
	{
		FeeScale = aFeeScale;
	}
	public String getHospitalCount()
	{
		return HospitalCount;
	}
	public void setHospitalCount(String aHospitalCount)
	{
		HospitalCount = aHospitalCount;
	}
	public String getClinicCount()
	{
		return ClinicCount;
	}
	public void setClinicCount(String aClinicCount)
	{
		ClinicCount = aClinicCount;
	}

	/**
	* 使用另外一个 LDAreaHospitalSchema 对象给 Schema 赋值
	* @param: aLDAreaHospitalSchema LDAreaHospitalSchema
	**/
	public void setSchema(LDAreaHospitalSchema aLDAreaHospitalSchema)
	{
		this.AreaCode = aLDAreaHospitalSchema.getAreaCode();
		this.Year = aLDAreaHospitalSchema.getYear();
		this.ManageCom = aLDAreaHospitalSchema.getManageCom();
		this.LevelCode1 = aLDAreaHospitalSchema.getLevelCode1();
		this.LevelCode2 = aLDAreaHospitalSchema.getLevelCode2();
		this.AreaName = aLDAreaHospitalSchema.getAreaName();
		this.AverageCharge = aLDAreaHospitalSchema.getAverageCharge();
		this.AverageDate = aLDAreaHospitalSchema.getAverageDate();
		this.DataSource = aLDAreaHospitalSchema.getDataSource();
		this.Operator = aLDAreaHospitalSchema.getOperator();
		this.MakeDate = fDate.getDate( aLDAreaHospitalSchema.getMakeDate());
		this.MakeTime = aLDAreaHospitalSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLDAreaHospitalSchema.getModifyDate());
		this.ModifyTime = aLDAreaHospitalSchema.getModifyTime();
		this.StandbyFlag1 = aLDAreaHospitalSchema.getStandbyFlag1();
		this.StandbyFlag2 = aLDAreaHospitalSchema.getStandbyFlag2();
		this.FeeScale = aLDAreaHospitalSchema.getFeeScale();
		this.HospitalCount = aLDAreaHospitalSchema.getHospitalCount();
		this.ClinicCount = aLDAreaHospitalSchema.getClinicCount();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("AreaCode") == null )
				this.AreaCode = null;
			else
				this.AreaCode = rs.getString("AreaCode").trim();

			if( rs.getString("Year") == null )
				this.Year = null;
			else
				this.Year = rs.getString("Year").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("LevelCode1") == null )
				this.LevelCode1 = null;
			else
				this.LevelCode1 = rs.getString("LevelCode1").trim();

			if( rs.getString("LevelCode2") == null )
				this.LevelCode2 = null;
			else
				this.LevelCode2 = rs.getString("LevelCode2").trim();

			if( rs.getString("AreaName") == null )
				this.AreaName = null;
			else
				this.AreaName = rs.getString("AreaName").trim();

			this.AverageCharge = rs.getDouble("AverageCharge");
			if( rs.getString("AverageDate") == null )
				this.AverageDate = null;
			else
				this.AverageDate = rs.getString("AverageDate").trim();

			if( rs.getString("DataSource") == null )
				this.DataSource = null;
			else
				this.DataSource = rs.getString("DataSource").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("StandbyFlag1") == null )
				this.StandbyFlag1 = null;
			else
				this.StandbyFlag1 = rs.getString("StandbyFlag1").trim();

			if( rs.getString("StandbyFlag2") == null )
				this.StandbyFlag2 = null;
			else
				this.StandbyFlag2 = rs.getString("StandbyFlag2").trim();

			if( rs.getString("FeeScale") == null )
				this.FeeScale = null;
			else
				this.FeeScale = rs.getString("FeeScale").trim();

			if( rs.getString("HospitalCount") == null )
				this.HospitalCount = null;
			else
				this.HospitalCount = rs.getString("HospitalCount").trim();

			if( rs.getString("ClinicCount") == null )
				this.ClinicCount = null;
			else
				this.ClinicCount = rs.getString("ClinicCount").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LDAreaHospital表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LDAreaHospitalSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LDAreaHospitalSchema getSchema()
	{
		LDAreaHospitalSchema aLDAreaHospitalSchema = new LDAreaHospitalSchema();
		aLDAreaHospitalSchema.setSchema(this);
		return aLDAreaHospitalSchema;
	}

	public LDAreaHospitalDB getDB()
	{
		LDAreaHospitalDB aDBOper = new LDAreaHospitalDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDAreaHospital描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(AreaCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Year)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LevelCode1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LevelCode2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AreaName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(AverageCharge));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AverageDate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DataSource)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandbyFlag1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandbyFlag2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FeeScale)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HospitalCount)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ClinicCount));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDAreaHospital>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			AreaCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			Year = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			LevelCode1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			LevelCode2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			AreaName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			AverageCharge = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,7,SysConst.PACKAGESPILTER))).doubleValue();
			AverageDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			DataSource = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			StandbyFlag1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			StandbyFlag2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			FeeScale = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			HospitalCount = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			ClinicCount = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LDAreaHospitalSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("AreaCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AreaCode));
		}
		if (FCode.equals("Year"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Year));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("LevelCode1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LevelCode1));
		}
		if (FCode.equals("LevelCode2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LevelCode2));
		}
		if (FCode.equals("AreaName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AreaName));
		}
		if (FCode.equals("AverageCharge"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AverageCharge));
		}
		if (FCode.equals("AverageDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AverageDate));
		}
		if (FCode.equals("DataSource"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DataSource));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("StandbyFlag1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag1));
		}
		if (FCode.equals("StandbyFlag2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag2));
		}
		if (FCode.equals("FeeScale"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FeeScale));
		}
		if (FCode.equals("HospitalCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HospitalCount));
		}
		if (FCode.equals("ClinicCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClinicCount));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(AreaCode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(Year);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(LevelCode1);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(LevelCode2);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(AreaName);
				break;
			case 6:
				strFieldValue = String.valueOf(AverageCharge);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(AverageDate);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(DataSource);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(StandbyFlag1);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(StandbyFlag2);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(FeeScale);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(HospitalCount);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(ClinicCount);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("AreaCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AreaCode = FValue.trim();
			}
			else
				AreaCode = null;
		}
		if (FCode.equalsIgnoreCase("Year"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Year = FValue.trim();
			}
			else
				Year = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("LevelCode1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LevelCode1 = FValue.trim();
			}
			else
				LevelCode1 = null;
		}
		if (FCode.equalsIgnoreCase("LevelCode2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LevelCode2 = FValue.trim();
			}
			else
				LevelCode2 = null;
		}
		if (FCode.equalsIgnoreCase("AreaName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AreaName = FValue.trim();
			}
			else
				AreaName = null;
		}
		if (FCode.equalsIgnoreCase("AverageCharge"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				AverageCharge = d;
			}
		}
		if (FCode.equalsIgnoreCase("AverageDate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AverageDate = FValue.trim();
			}
			else
				AverageDate = null;
		}
		if (FCode.equalsIgnoreCase("DataSource"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DataSource = FValue.trim();
			}
			else
				DataSource = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("StandbyFlag1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandbyFlag1 = FValue.trim();
			}
			else
				StandbyFlag1 = null;
		}
		if (FCode.equalsIgnoreCase("StandbyFlag2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandbyFlag2 = FValue.trim();
			}
			else
				StandbyFlag2 = null;
		}
		if (FCode.equalsIgnoreCase("FeeScale"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FeeScale = FValue.trim();
			}
			else
				FeeScale = null;
		}
		if (FCode.equalsIgnoreCase("HospitalCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HospitalCount = FValue.trim();
			}
			else
				HospitalCount = null;
		}
		if (FCode.equalsIgnoreCase("ClinicCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ClinicCount = FValue.trim();
			}
			else
				ClinicCount = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LDAreaHospitalSchema other = (LDAreaHospitalSchema)otherObject;
		return
			(AreaCode == null ? other.getAreaCode() == null : AreaCode.equals(other.getAreaCode()))
			&& (Year == null ? other.getYear() == null : Year.equals(other.getYear()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (LevelCode1 == null ? other.getLevelCode1() == null : LevelCode1.equals(other.getLevelCode1()))
			&& (LevelCode2 == null ? other.getLevelCode2() == null : LevelCode2.equals(other.getLevelCode2()))
			&& (AreaName == null ? other.getAreaName() == null : AreaName.equals(other.getAreaName()))
			&& AverageCharge == other.getAverageCharge()
			&& (AverageDate == null ? other.getAverageDate() == null : AverageDate.equals(other.getAverageDate()))
			&& (DataSource == null ? other.getDataSource() == null : DataSource.equals(other.getDataSource()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (StandbyFlag1 == null ? other.getStandbyFlag1() == null : StandbyFlag1.equals(other.getStandbyFlag1()))
			&& (StandbyFlag2 == null ? other.getStandbyFlag2() == null : StandbyFlag2.equals(other.getStandbyFlag2()))
			&& (FeeScale == null ? other.getFeeScale() == null : FeeScale.equals(other.getFeeScale()))
			&& (HospitalCount == null ? other.getHospitalCount() == null : HospitalCount.equals(other.getHospitalCount()))
			&& (ClinicCount == null ? other.getClinicCount() == null : ClinicCount.equals(other.getClinicCount()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("AreaCode") ) {
			return 0;
		}
		if( strFieldName.equals("Year") ) {
			return 1;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 2;
		}
		if( strFieldName.equals("LevelCode1") ) {
			return 3;
		}
		if( strFieldName.equals("LevelCode2") ) {
			return 4;
		}
		if( strFieldName.equals("AreaName") ) {
			return 5;
		}
		if( strFieldName.equals("AverageCharge") ) {
			return 6;
		}
		if( strFieldName.equals("AverageDate") ) {
			return 7;
		}
		if( strFieldName.equals("DataSource") ) {
			return 8;
		}
		if( strFieldName.equals("Operator") ) {
			return 9;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 10;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 11;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 12;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 13;
		}
		if( strFieldName.equals("StandbyFlag1") ) {
			return 14;
		}
		if( strFieldName.equals("StandbyFlag2") ) {
			return 15;
		}
		if( strFieldName.equals("FeeScale") ) {
			return 16;
		}
		if( strFieldName.equals("HospitalCount") ) {
			return 17;
		}
		if( strFieldName.equals("ClinicCount") ) {
			return 18;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "AreaCode";
				break;
			case 1:
				strFieldName = "Year";
				break;
			case 2:
				strFieldName = "ManageCom";
				break;
			case 3:
				strFieldName = "LevelCode1";
				break;
			case 4:
				strFieldName = "LevelCode2";
				break;
			case 5:
				strFieldName = "AreaName";
				break;
			case 6:
				strFieldName = "AverageCharge";
				break;
			case 7:
				strFieldName = "AverageDate";
				break;
			case 8:
				strFieldName = "DataSource";
				break;
			case 9:
				strFieldName = "Operator";
				break;
			case 10:
				strFieldName = "MakeDate";
				break;
			case 11:
				strFieldName = "MakeTime";
				break;
			case 12:
				strFieldName = "ModifyDate";
				break;
			case 13:
				strFieldName = "ModifyTime";
				break;
			case 14:
				strFieldName = "StandbyFlag1";
				break;
			case 15:
				strFieldName = "StandbyFlag2";
				break;
			case 16:
				strFieldName = "FeeScale";
				break;
			case 17:
				strFieldName = "HospitalCount";
				break;
			case 18:
				strFieldName = "ClinicCount";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("AreaCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Year") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LevelCode1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LevelCode2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AreaName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AverageCharge") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("AverageDate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DataSource") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandbyFlag1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandbyFlag2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FeeScale") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HospitalCount") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ClinicCount") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
