/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LRAccountDB;

/*
 * <p>ClassName: LRAccountSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 再保账单表
 * @CreateDate：2015-06-18
 */
public class LRAccountSchema implements Schema, Cloneable
{
	// @Field
	/** 再保序列号 */
	private String LRSerialNo;
	/** 再保合同号 */
	private String ReContCode;
	/** 产品代码 */
	private String RiskCode;
	/** 团单号 */
	private String GrpContNo;
	/** 保单号 */
	private String ContNo;
	/** 管理机构 */
	private String ManageCom;
	/** 成本中心编码 */
	private String CostCenter;
	/** 市场类型 */
	private String MarketType;
	/** 数据类型 */
	private String DataType;
	/** 导入次数 */
	private int ImportCount;
	/** 数据所属年份 */
	private String BelongYear;
	/** 数据所属季度 */
	private String BelongQuarter;
	/** 数据所属月份 */
	private String BelongMonth;
	/** 财务流转日期 */
	private Date FinAccountDate;
	/** 分出保费(退保金额) */
	private double CessPrem;
	/** 分出保费状态 */
	private String CessPremState;
	/** 分保(退回)手续费 */
	private double ReProcFee;
	/** 手续费状态 */
	private String ReProcFeeState;
	/** 摊回赔款 */
	private double ClaimBackFee;
	/** 摊回赔款状态 */
	private String ClaimBackFeeState;
	/** 分保类型 */
	private String TempCessFlag;
	/** 结算单号 */
	private String BranchNo;
	/** 结算金额 */
	private double AccountMoney;
	/** 结算日期 */
	private Date AccountDate;
	/** 结算人 */
	private String AccountOprater;
	/** 操作用户 */
	private String Oprater;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 修改日期 */
	private Date ModifyDate;
	/** 修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 30;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LRAccountSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "LRSerialNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LRAccountSchema cloned = (LRAccountSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getLRSerialNo()
	{
		return LRSerialNo;
	}
	public void setLRSerialNo(String aLRSerialNo)
	{
		LRSerialNo = aLRSerialNo;
	}
	public String getReContCode()
	{
		return ReContCode;
	}
	public void setReContCode(String aReContCode)
	{
		ReContCode = aReContCode;
	}
	public String getRiskCode()
	{
		return RiskCode;
	}
	public void setRiskCode(String aRiskCode)
	{
		RiskCode = aRiskCode;
	}
	public String getGrpContNo()
	{
		return GrpContNo;
	}
	public void setGrpContNo(String aGrpContNo)
	{
		GrpContNo = aGrpContNo;
	}
	public String getContNo()
	{
		return ContNo;
	}
	public void setContNo(String aContNo)
	{
		ContNo = aContNo;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getCostCenter()
	{
		return CostCenter;
	}
	public void setCostCenter(String aCostCenter)
	{
		CostCenter = aCostCenter;
	}
	public String getMarketType()
	{
		return MarketType;
	}
	public void setMarketType(String aMarketType)
	{
		MarketType = aMarketType;
	}
	public String getDataType()
	{
		return DataType;
	}
	public void setDataType(String aDataType)
	{
		DataType = aDataType;
	}
	public int getImportCount()
	{
		return ImportCount;
	}
	public void setImportCount(int aImportCount)
	{
		ImportCount = aImportCount;
	}
	public void setImportCount(String aImportCount)
	{
		if (aImportCount != null && !aImportCount.equals(""))
		{
			Integer tInteger = new Integer(aImportCount);
			int i = tInteger.intValue();
			ImportCount = i;
		}
	}

	public String getBelongYear()
	{
		return BelongYear;
	}
	public void setBelongYear(String aBelongYear)
	{
		BelongYear = aBelongYear;
	}
	public String getBelongQuarter()
	{
		return BelongQuarter;
	}
	public void setBelongQuarter(String aBelongQuarter)
	{
		BelongQuarter = aBelongQuarter;
	}
	public String getBelongMonth()
	{
		return BelongMonth;
	}
	public void setBelongMonth(String aBelongMonth)
	{
		BelongMonth = aBelongMonth;
	}
	public String getFinAccountDate()
	{
		if( FinAccountDate != null )
			return fDate.getString(FinAccountDate);
		else
			return null;
	}
	public void setFinAccountDate(Date aFinAccountDate)
	{
		FinAccountDate = aFinAccountDate;
	}
	public void setFinAccountDate(String aFinAccountDate)
	{
		if (aFinAccountDate != null && !aFinAccountDate.equals("") )
		{
			FinAccountDate = fDate.getDate( aFinAccountDate );
		}
		else
			FinAccountDate = null;
	}

	public double getCessPrem()
	{
		return CessPrem;
	}
	public void setCessPrem(double aCessPrem)
	{
		CessPrem = Arith.round(aCessPrem,2);
	}
	public void setCessPrem(String aCessPrem)
	{
		if (aCessPrem != null && !aCessPrem.equals(""))
		{
			Double tDouble = new Double(aCessPrem);
			double d = tDouble.doubleValue();
                CessPrem = Arith.round(d,2);
		}
	}

	public String getCessPremState()
	{
		return CessPremState;
	}
	public void setCessPremState(String aCessPremState)
	{
		CessPremState = aCessPremState;
	}
	public double getReProcFee()
	{
		return ReProcFee;
	}
	public void setReProcFee(double aReProcFee)
	{
		ReProcFee = Arith.round(aReProcFee,2);
	}
	public void setReProcFee(String aReProcFee)
	{
		if (aReProcFee != null && !aReProcFee.equals(""))
		{
			Double tDouble = new Double(aReProcFee);
			double d = tDouble.doubleValue();
                ReProcFee = Arith.round(d,2);
		}
	}

	public String getReProcFeeState()
	{
		return ReProcFeeState;
	}
	public void setReProcFeeState(String aReProcFeeState)
	{
		ReProcFeeState = aReProcFeeState;
	}
	public double getClaimBackFee()
	{
		return ClaimBackFee;
	}
	public void setClaimBackFee(double aClaimBackFee)
	{
		ClaimBackFee = Arith.round(aClaimBackFee,2);
	}
	public void setClaimBackFee(String aClaimBackFee)
	{
		if (aClaimBackFee != null && !aClaimBackFee.equals(""))
		{
			Double tDouble = new Double(aClaimBackFee);
			double d = tDouble.doubleValue();
                ClaimBackFee = Arith.round(d,2);
		}
	}

	public String getClaimBackFeeState()
	{
		return ClaimBackFeeState;
	}
	public void setClaimBackFeeState(String aClaimBackFeeState)
	{
		ClaimBackFeeState = aClaimBackFeeState;
	}
	public String getTempCessFlag()
	{
		return TempCessFlag;
	}
	public void setTempCessFlag(String aTempCessFlag)
	{
		TempCessFlag = aTempCessFlag;
	}
	public String getBranchNo()
	{
		return BranchNo;
	}
	public void setBranchNo(String aBranchNo)
	{
		BranchNo = aBranchNo;
	}
	public double getAccountMoney()
	{
		return AccountMoney;
	}
	public void setAccountMoney(double aAccountMoney)
	{
		AccountMoney = Arith.round(aAccountMoney,2);
	}
	public void setAccountMoney(String aAccountMoney)
	{
		if (aAccountMoney != null && !aAccountMoney.equals(""))
		{
			Double tDouble = new Double(aAccountMoney);
			double d = tDouble.doubleValue();
                AccountMoney = Arith.round(d,2);
		}
	}

	public String getAccountDate()
	{
		if( AccountDate != null )
			return fDate.getString(AccountDate);
		else
			return null;
	}
	public void setAccountDate(Date aAccountDate)
	{
		AccountDate = aAccountDate;
	}
	public void setAccountDate(String aAccountDate)
	{
		if (aAccountDate != null && !aAccountDate.equals("") )
		{
			AccountDate = fDate.getDate( aAccountDate );
		}
		else
			AccountDate = null;
	}

	public String getAccountOprater()
	{
		return AccountOprater;
	}
	public void setAccountOprater(String aAccountOprater)
	{
		AccountOprater = aAccountOprater;
	}
	public String getOprater()
	{
		return Oprater;
	}
	public void setOprater(String aOprater)
	{
		Oprater = aOprater;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LRAccountSchema 对象给 Schema 赋值
	* @param: aLRAccountSchema LRAccountSchema
	**/
	public void setSchema(LRAccountSchema aLRAccountSchema)
	{
		this.LRSerialNo = aLRAccountSchema.getLRSerialNo();
		this.ReContCode = aLRAccountSchema.getReContCode();
		this.RiskCode = aLRAccountSchema.getRiskCode();
		this.GrpContNo = aLRAccountSchema.getGrpContNo();
		this.ContNo = aLRAccountSchema.getContNo();
		this.ManageCom = aLRAccountSchema.getManageCom();
		this.CostCenter = aLRAccountSchema.getCostCenter();
		this.MarketType = aLRAccountSchema.getMarketType();
		this.DataType = aLRAccountSchema.getDataType();
		this.ImportCount = aLRAccountSchema.getImportCount();
		this.BelongYear = aLRAccountSchema.getBelongYear();
		this.BelongQuarter = aLRAccountSchema.getBelongQuarter();
		this.BelongMonth = aLRAccountSchema.getBelongMonth();
		this.FinAccountDate = fDate.getDate( aLRAccountSchema.getFinAccountDate());
		this.CessPrem = aLRAccountSchema.getCessPrem();
		this.CessPremState = aLRAccountSchema.getCessPremState();
		this.ReProcFee = aLRAccountSchema.getReProcFee();
		this.ReProcFeeState = aLRAccountSchema.getReProcFeeState();
		this.ClaimBackFee = aLRAccountSchema.getClaimBackFee();
		this.ClaimBackFeeState = aLRAccountSchema.getClaimBackFeeState();
		this.TempCessFlag = aLRAccountSchema.getTempCessFlag();
		this.BranchNo = aLRAccountSchema.getBranchNo();
		this.AccountMoney = aLRAccountSchema.getAccountMoney();
		this.AccountDate = fDate.getDate( aLRAccountSchema.getAccountDate());
		this.AccountOprater = aLRAccountSchema.getAccountOprater();
		this.Oprater = aLRAccountSchema.getOprater();
		this.MakeDate = fDate.getDate( aLRAccountSchema.getMakeDate());
		this.MakeTime = aLRAccountSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLRAccountSchema.getModifyDate());
		this.ModifyTime = aLRAccountSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("LRSerialNo") == null )
				this.LRSerialNo = null;
			else
				this.LRSerialNo = rs.getString("LRSerialNo").trim();

			if( rs.getString("ReContCode") == null )
				this.ReContCode = null;
			else
				this.ReContCode = rs.getString("ReContCode").trim();

			if( rs.getString("RiskCode") == null )
				this.RiskCode = null;
			else
				this.RiskCode = rs.getString("RiskCode").trim();

			if( rs.getString("GrpContNo") == null )
				this.GrpContNo = null;
			else
				this.GrpContNo = rs.getString("GrpContNo").trim();

			if( rs.getString("ContNo") == null )
				this.ContNo = null;
			else
				this.ContNo = rs.getString("ContNo").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("CostCenter") == null )
				this.CostCenter = null;
			else
				this.CostCenter = rs.getString("CostCenter").trim();

			if( rs.getString("MarketType") == null )
				this.MarketType = null;
			else
				this.MarketType = rs.getString("MarketType").trim();

			if( rs.getString("DataType") == null )
				this.DataType = null;
			else
				this.DataType = rs.getString("DataType").trim();

			this.ImportCount = rs.getInt("ImportCount");
			if( rs.getString("BelongYear") == null )
				this.BelongYear = null;
			else
				this.BelongYear = rs.getString("BelongYear").trim();

			if( rs.getString("BelongQuarter") == null )
				this.BelongQuarter = null;
			else
				this.BelongQuarter = rs.getString("BelongQuarter").trim();

			if( rs.getString("BelongMonth") == null )
				this.BelongMonth = null;
			else
				this.BelongMonth = rs.getString("BelongMonth").trim();

			this.FinAccountDate = rs.getDate("FinAccountDate");
			this.CessPrem = rs.getDouble("CessPrem");
			if( rs.getString("CessPremState") == null )
				this.CessPremState = null;
			else
				this.CessPremState = rs.getString("CessPremState").trim();

			this.ReProcFee = rs.getDouble("ReProcFee");
			if( rs.getString("ReProcFeeState") == null )
				this.ReProcFeeState = null;
			else
				this.ReProcFeeState = rs.getString("ReProcFeeState").trim();

			this.ClaimBackFee = rs.getDouble("ClaimBackFee");
			if( rs.getString("ClaimBackFeeState") == null )
				this.ClaimBackFeeState = null;
			else
				this.ClaimBackFeeState = rs.getString("ClaimBackFeeState").trim();

			if( rs.getString("TempCessFlag") == null )
				this.TempCessFlag = null;
			else
				this.TempCessFlag = rs.getString("TempCessFlag").trim();

			if( rs.getString("BranchNo") == null )
				this.BranchNo = null;
			else
				this.BranchNo = rs.getString("BranchNo").trim();

			this.AccountMoney = rs.getDouble("AccountMoney");
			this.AccountDate = rs.getDate("AccountDate");
			if( rs.getString("AccountOprater") == null )
				this.AccountOprater = null;
			else
				this.AccountOprater = rs.getString("AccountOprater").trim();

			if( rs.getString("Oprater") == null )
				this.Oprater = null;
			else
				this.Oprater = rs.getString("Oprater").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LRAccount表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LRAccountSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LRAccountSchema getSchema()
	{
		LRAccountSchema aLRAccountSchema = new LRAccountSchema();
		aLRAccountSchema.setSchema(this);
		return aLRAccountSchema;
	}

	public LRAccountDB getDB()
	{
		LRAccountDB aDBOper = new LRAccountDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLRAccount描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(LRSerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReContCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CostCenter)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MarketType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DataType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ImportCount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BelongYear)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BelongQuarter)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BelongMonth)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( FinAccountDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(CessPrem));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CessPremState)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ReProcFee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReProcFeeState)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ClaimBackFee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ClaimBackFeeState)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TempCessFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(AccountMoney));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( AccountDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccountOprater)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Oprater)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLRAccount>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			LRSerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			ReContCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			CostCenter = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			MarketType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			DataType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			ImportCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,10,SysConst.PACKAGESPILTER))).intValue();
			BelongYear = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			BelongQuarter = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			BelongMonth = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			FinAccountDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,SysConst.PACKAGESPILTER));
			CessPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,15,SysConst.PACKAGESPILTER))).doubleValue();
			CessPremState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			ReProcFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,17,SysConst.PACKAGESPILTER))).doubleValue();
			ReProcFeeState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			ClaimBackFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,19,SysConst.PACKAGESPILTER))).doubleValue();
			ClaimBackFeeState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			TempCessFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			BranchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			AccountMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,23,SysConst.PACKAGESPILTER))).doubleValue();
			AccountDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,SysConst.PACKAGESPILTER));
			AccountOprater = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			Oprater = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LRAccountSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("LRSerialNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LRSerialNo));
		}
		if (FCode.equals("ReContCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReContCode));
		}
		if (FCode.equals("RiskCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
		}
		if (FCode.equals("GrpContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
		}
		if (FCode.equals("ContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("CostCenter"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CostCenter));
		}
		if (FCode.equals("MarketType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MarketType));
		}
		if (FCode.equals("DataType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DataType));
		}
		if (FCode.equals("ImportCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ImportCount));
		}
		if (FCode.equals("BelongYear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BelongYear));
		}
		if (FCode.equals("BelongQuarter"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BelongQuarter));
		}
		if (FCode.equals("BelongMonth"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BelongMonth));
		}
		if (FCode.equals("FinAccountDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getFinAccountDate()));
		}
		if (FCode.equals("CessPrem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CessPrem));
		}
		if (FCode.equals("CessPremState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CessPremState));
		}
		if (FCode.equals("ReProcFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReProcFee));
		}
		if (FCode.equals("ReProcFeeState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReProcFeeState));
		}
		if (FCode.equals("ClaimBackFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClaimBackFee));
		}
		if (FCode.equals("ClaimBackFeeState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClaimBackFeeState));
		}
		if (FCode.equals("TempCessFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TempCessFlag));
		}
		if (FCode.equals("BranchNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchNo));
		}
		if (FCode.equals("AccountMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccountMoney));
		}
		if (FCode.equals("AccountDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getAccountDate()));
		}
		if (FCode.equals("AccountOprater"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccountOprater));
		}
		if (FCode.equals("Oprater"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Oprater));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(LRSerialNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(ReContCode);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(RiskCode);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(GrpContNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(ContNo);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(CostCenter);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(MarketType);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(DataType);
				break;
			case 9:
				strFieldValue = String.valueOf(ImportCount);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(BelongYear);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(BelongQuarter);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(BelongMonth);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getFinAccountDate()));
				break;
			case 14:
				strFieldValue = String.valueOf(CessPrem);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(CessPremState);
				break;
			case 16:
				strFieldValue = String.valueOf(ReProcFee);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(ReProcFeeState);
				break;
			case 18:
				strFieldValue = String.valueOf(ClaimBackFee);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(ClaimBackFeeState);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(TempCessFlag);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(BranchNo);
				break;
			case 22:
				strFieldValue = String.valueOf(AccountMoney);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getAccountDate()));
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(AccountOprater);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(Oprater);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("LRSerialNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LRSerialNo = FValue.trim();
			}
			else
				LRSerialNo = null;
		}
		if (FCode.equalsIgnoreCase("ReContCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReContCode = FValue.trim();
			}
			else
				ReContCode = null;
		}
		if (FCode.equalsIgnoreCase("RiskCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskCode = FValue.trim();
			}
			else
				RiskCode = null;
		}
		if (FCode.equalsIgnoreCase("GrpContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpContNo = FValue.trim();
			}
			else
				GrpContNo = null;
		}
		if (FCode.equalsIgnoreCase("ContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContNo = FValue.trim();
			}
			else
				ContNo = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("CostCenter"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CostCenter = FValue.trim();
			}
			else
				CostCenter = null;
		}
		if (FCode.equalsIgnoreCase("MarketType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MarketType = FValue.trim();
			}
			else
				MarketType = null;
		}
		if (FCode.equalsIgnoreCase("DataType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DataType = FValue.trim();
			}
			else
				DataType = null;
		}
		if (FCode.equalsIgnoreCase("ImportCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				ImportCount = i;
			}
		}
		if (FCode.equalsIgnoreCase("BelongYear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BelongYear = FValue.trim();
			}
			else
				BelongYear = null;
		}
		if (FCode.equalsIgnoreCase("BelongQuarter"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BelongQuarter = FValue.trim();
			}
			else
				BelongQuarter = null;
		}
		if (FCode.equalsIgnoreCase("BelongMonth"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BelongMonth = FValue.trim();
			}
			else
				BelongMonth = null;
		}
		if (FCode.equalsIgnoreCase("FinAccountDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				FinAccountDate = fDate.getDate( FValue );
			}
			else
				FinAccountDate = null;
		}
		if (FCode.equalsIgnoreCase("CessPrem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				CessPrem = d;
			}
		}
		if (FCode.equalsIgnoreCase("CessPremState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CessPremState = FValue.trim();
			}
			else
				CessPremState = null;
		}
		if (FCode.equalsIgnoreCase("ReProcFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ReProcFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("ReProcFeeState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReProcFeeState = FValue.trim();
			}
			else
				ReProcFeeState = null;
		}
		if (FCode.equalsIgnoreCase("ClaimBackFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ClaimBackFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("ClaimBackFeeState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ClaimBackFeeState = FValue.trim();
			}
			else
				ClaimBackFeeState = null;
		}
		if (FCode.equalsIgnoreCase("TempCessFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TempCessFlag = FValue.trim();
			}
			else
				TempCessFlag = null;
		}
		if (FCode.equalsIgnoreCase("BranchNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchNo = FValue.trim();
			}
			else
				BranchNo = null;
		}
		if (FCode.equalsIgnoreCase("AccountMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				AccountMoney = d;
			}
		}
		if (FCode.equalsIgnoreCase("AccountDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				AccountDate = fDate.getDate( FValue );
			}
			else
				AccountDate = null;
		}
		if (FCode.equalsIgnoreCase("AccountOprater"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccountOprater = FValue.trim();
			}
			else
				AccountOprater = null;
		}
		if (FCode.equalsIgnoreCase("Oprater"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Oprater = FValue.trim();
			}
			else
				Oprater = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LRAccountSchema other = (LRAccountSchema)otherObject;
		return
			(LRSerialNo == null ? other.getLRSerialNo() == null : LRSerialNo.equals(other.getLRSerialNo()))
			&& (ReContCode == null ? other.getReContCode() == null : ReContCode.equals(other.getReContCode()))
			&& (RiskCode == null ? other.getRiskCode() == null : RiskCode.equals(other.getRiskCode()))
			&& (GrpContNo == null ? other.getGrpContNo() == null : GrpContNo.equals(other.getGrpContNo()))
			&& (ContNo == null ? other.getContNo() == null : ContNo.equals(other.getContNo()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (CostCenter == null ? other.getCostCenter() == null : CostCenter.equals(other.getCostCenter()))
			&& (MarketType == null ? other.getMarketType() == null : MarketType.equals(other.getMarketType()))
			&& (DataType == null ? other.getDataType() == null : DataType.equals(other.getDataType()))
			&& ImportCount == other.getImportCount()
			&& (BelongYear == null ? other.getBelongYear() == null : BelongYear.equals(other.getBelongYear()))
			&& (BelongQuarter == null ? other.getBelongQuarter() == null : BelongQuarter.equals(other.getBelongQuarter()))
			&& (BelongMonth == null ? other.getBelongMonth() == null : BelongMonth.equals(other.getBelongMonth()))
			&& (FinAccountDate == null ? other.getFinAccountDate() == null : fDate.getString(FinAccountDate).equals(other.getFinAccountDate()))
			&& CessPrem == other.getCessPrem()
			&& (CessPremState == null ? other.getCessPremState() == null : CessPremState.equals(other.getCessPremState()))
			&& ReProcFee == other.getReProcFee()
			&& (ReProcFeeState == null ? other.getReProcFeeState() == null : ReProcFeeState.equals(other.getReProcFeeState()))
			&& ClaimBackFee == other.getClaimBackFee()
			&& (ClaimBackFeeState == null ? other.getClaimBackFeeState() == null : ClaimBackFeeState.equals(other.getClaimBackFeeState()))
			&& (TempCessFlag == null ? other.getTempCessFlag() == null : TempCessFlag.equals(other.getTempCessFlag()))
			&& (BranchNo == null ? other.getBranchNo() == null : BranchNo.equals(other.getBranchNo()))
			&& AccountMoney == other.getAccountMoney()
			&& (AccountDate == null ? other.getAccountDate() == null : fDate.getString(AccountDate).equals(other.getAccountDate()))
			&& (AccountOprater == null ? other.getAccountOprater() == null : AccountOprater.equals(other.getAccountOprater()))
			&& (Oprater == null ? other.getOprater() == null : Oprater.equals(other.getOprater()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("LRSerialNo") ) {
			return 0;
		}
		if( strFieldName.equals("ReContCode") ) {
			return 1;
		}
		if( strFieldName.equals("RiskCode") ) {
			return 2;
		}
		if( strFieldName.equals("GrpContNo") ) {
			return 3;
		}
		if( strFieldName.equals("ContNo") ) {
			return 4;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 5;
		}
		if( strFieldName.equals("CostCenter") ) {
			return 6;
		}
		if( strFieldName.equals("MarketType") ) {
			return 7;
		}
		if( strFieldName.equals("DataType") ) {
			return 8;
		}
		if( strFieldName.equals("ImportCount") ) {
			return 9;
		}
		if( strFieldName.equals("BelongYear") ) {
			return 10;
		}
		if( strFieldName.equals("BelongQuarter") ) {
			return 11;
		}
		if( strFieldName.equals("BelongMonth") ) {
			return 12;
		}
		if( strFieldName.equals("FinAccountDate") ) {
			return 13;
		}
		if( strFieldName.equals("CessPrem") ) {
			return 14;
		}
		if( strFieldName.equals("CessPremState") ) {
			return 15;
		}
		if( strFieldName.equals("ReProcFee") ) {
			return 16;
		}
		if( strFieldName.equals("ReProcFeeState") ) {
			return 17;
		}
		if( strFieldName.equals("ClaimBackFee") ) {
			return 18;
		}
		if( strFieldName.equals("ClaimBackFeeState") ) {
			return 19;
		}
		if( strFieldName.equals("TempCessFlag") ) {
			return 20;
		}
		if( strFieldName.equals("BranchNo") ) {
			return 21;
		}
		if( strFieldName.equals("AccountMoney") ) {
			return 22;
		}
		if( strFieldName.equals("AccountDate") ) {
			return 23;
		}
		if( strFieldName.equals("AccountOprater") ) {
			return 24;
		}
		if( strFieldName.equals("Oprater") ) {
			return 25;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 26;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 27;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 28;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 29;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "LRSerialNo";
				break;
			case 1:
				strFieldName = "ReContCode";
				break;
			case 2:
				strFieldName = "RiskCode";
				break;
			case 3:
				strFieldName = "GrpContNo";
				break;
			case 4:
				strFieldName = "ContNo";
				break;
			case 5:
				strFieldName = "ManageCom";
				break;
			case 6:
				strFieldName = "CostCenter";
				break;
			case 7:
				strFieldName = "MarketType";
				break;
			case 8:
				strFieldName = "DataType";
				break;
			case 9:
				strFieldName = "ImportCount";
				break;
			case 10:
				strFieldName = "BelongYear";
				break;
			case 11:
				strFieldName = "BelongQuarter";
				break;
			case 12:
				strFieldName = "BelongMonth";
				break;
			case 13:
				strFieldName = "FinAccountDate";
				break;
			case 14:
				strFieldName = "CessPrem";
				break;
			case 15:
				strFieldName = "CessPremState";
				break;
			case 16:
				strFieldName = "ReProcFee";
				break;
			case 17:
				strFieldName = "ReProcFeeState";
				break;
			case 18:
				strFieldName = "ClaimBackFee";
				break;
			case 19:
				strFieldName = "ClaimBackFeeState";
				break;
			case 20:
				strFieldName = "TempCessFlag";
				break;
			case 21:
				strFieldName = "BranchNo";
				break;
			case 22:
				strFieldName = "AccountMoney";
				break;
			case 23:
				strFieldName = "AccountDate";
				break;
			case 24:
				strFieldName = "AccountOprater";
				break;
			case 25:
				strFieldName = "Oprater";
				break;
			case 26:
				strFieldName = "MakeDate";
				break;
			case 27:
				strFieldName = "MakeTime";
				break;
			case 28:
				strFieldName = "ModifyDate";
				break;
			case 29:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("LRSerialNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReContCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CostCenter") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MarketType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DataType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ImportCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("BelongYear") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BelongQuarter") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BelongMonth") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FinAccountDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("CessPrem") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("CessPremState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReProcFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ReProcFeeState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ClaimBackFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ClaimBackFeeState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TempCessFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccountMoney") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("AccountDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("AccountOprater") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Oprater") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_INT;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 14:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 23:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
