/**
 * Copyright (c) 20171111 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.io.*;
import java.util.Date;

import com.sinosoft.lis.db.CiitcTaskDB;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: TransTaskZBXSLSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2015-08-07
 */
public class TransTaskZBXSLSchema implements Schema, Cloneable {

	/** 流水号 */
	private String CiitcTaskNo;
	/** 自增序列 */
	private int Id;
	/** 批次号 */
	private String BatchNo;
	/** 投保单号 */
	private String ProposalNo;
	/** 文件名 */
	private String FileName;
	/** 服务ip */
	private String ServerIp;
	/** 服务器登录用户 */
	private String ServerUser;
	/** 服务登陆密码 */
	private String ServerPassWord;
	/** 服务端口 */
	private String ServerPort;
	/** 本地文件路径 */
	private String LocalFilePaths;
	/** 远程文件路径 */
	private String RemoteFilePaths;
	/** 来源渠道 */
	private String SourceChannel;
	/** 是否需要后置机下载标识 */
	private String Priority;
	/** 文件MD5值 */
	private String FileMD5;
	/** 请求登陆Cookie */
	private String Cookie;
	/** 请求中保信返回的本次任务唯一ID */
	private String TaskId;
	/** 交易类型 */
	private String TransType;
	/** 请求报文 */
	private String ReqMessage;
	/** 分段上传还是直接上传标识 */
	private String Flag;
	/** 交易状态 */
	private String Status;
	/** 交易结果描述 */
	private String TransMSG;
	/** 文件操作日期 */
	private Date FileMakeDate;
	/** 文件操作时间 */
	private Date MakeDate;
	/** 交易最后修改时间 */
	private Date ModifyDate;
	/** 预留字段1 */
	private String Bak1;
	/** 预留字段2 */
	private String Bak2;
	/** 预留字段3 */
	private String Bak3;

	public static final int FIELDNUM = 27; // 数据库表的字段个数

	private FDate fDate = new FDate();// 处理日期

	public CErrors mErrors; // 错误信息

	public String getCiitcTaskNo() {
		return CiitcTaskNo;
	}

	public void setCiitcTaskNo(String ciitcTaskNo) {
		CiitcTaskNo = ciitcTaskNo;
	}

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getBatchNo() {
		return BatchNo;
	}

	public void setBatchNo(String batchNo) {
		BatchNo = batchNo;
	}

	public String getProposalNo() {
		return ProposalNo;
	}

	public void setProposalNo(String proposalNo) {
		ProposalNo = proposalNo;
	}

	public String getFileName() {
		return FileName;
	}

	public void setFileName(String fileName) {
		FileName = fileName;
	}

	public String getServerIp() {
		return ServerIp;
	}

	public void setServerIp(String serverIp) {
		ServerIp = serverIp;
	}

	public String getServerUser() {
		return ServerUser;
	}

	public void setServerUser(String serverUser) {
		ServerUser = serverUser;
	}

	public String getServerPassWord() {
		return ServerPassWord;
	}

	public void setServerPassWord(String serverPassWord) {
		ServerPassWord = serverPassWord;
	}

	public String getServerPort() {
		return ServerPort;
	}

	public void setServerPort(String serverPort) {
		ServerPort = serverPort;
	}

	public String getLocalFilePaths() {
		return LocalFilePaths;
	}

	public void setLocalFilePaths(String localFilePaths) {
		LocalFilePaths = localFilePaths;
	}

	public String getRemoteFilePaths() {
		return RemoteFilePaths;
	}

	public void setRemoteFilePaths(String remoteFilePaths) {
		RemoteFilePaths = remoteFilePaths;
	}

	public String getSourceChannel() {
		return SourceChannel;
	}

	public void setSourceChannel(String sourceChannel) {
		SourceChannel = sourceChannel;
	}

	public String getPriority() {
		return Priority;
	}

	public void setPriority(String priority) {
		Priority = priority;
	}

	public String getFileMD5() {
		return FileMD5;
	}

	public void setFileMD5(String fileMD5) {
		FileMD5 = fileMD5;
	}

	public String getCookie() {
		return Cookie;
	}

	public void setCookie(String cookie) {
		Cookie = cookie;
	}

	public String getTaskId() {
		return TaskId;
	}

	public void setTaskId(String taskId) {
		TaskId = taskId;
	}

	public String getTransType() {
		return TransType;
	}

	public void setTransType(String transType) {
		TransType = transType;
	}

	public String getReqMessage() {
		return ReqMessage;
	}

	public void setReqMessage(String reqMessage) {
		ReqMessage = reqMessage;
	}

	public String getFlag() {
		return Flag;
	}

	public void setFlag(String flag) {
		Flag = flag;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public String getTransMSG() {
		return TransMSG;
	}

	public void setTransMSG(String transMSG) {
		TransMSG = transMSG;
	}

	public String getFileMakeDate() {
		if (FileMakeDate != null)
			return fDate.getString(FileMakeDate);
		else
			return null;
	}

	public void setFileMakeDate(Date fileMakeDate) {
		FileMakeDate = fileMakeDate;
	}

	public void setFileMakeDate(String fileMakeDate) {
		if (fileMakeDate != null && !fileMakeDate.equals("")) {
			FileMakeDate = fDate.getDate(fileMakeDate);
		} else
			FileMakeDate = null;
	}

	public String getMakeDate() {
		if (MakeDate != null)
			return fDate.getString(MakeDate);
		else
			return null;
	}

	public void setMakeDate(Date makeDate) {
		MakeDate = makeDate;
	}

	public void setMakeDate(String makeDate) {
		if (makeDate != null && !makeDate.equals("")) {
			MakeDate = fDate.getDate(makeDate);
		} else
			MakeDate = null;
	}

	public String getModifyDate() {
		if (ModifyDate != null)
			return fDate.getString(ModifyDate);
		else
			return null;
	}

	public void setModifyDate(Date modifyDate) {
		ModifyDate = modifyDate;
	}

	public void setModifyDate(String modifyDate) {
		if (modifyDate != null && !modifyDate.equals("")) {
			ModifyDate = fDate.getDate(modifyDate);
		} else
			ModifyDate = null;
	}

	public String getBak1() {
		return Bak1;
	}

	public void setBak1(String bak1) {
		Bak1 = bak1;
	}

	public String getBak2() {
		return Bak2;
	}

	public void setBak2(String bak2) {
		Bak2 = bak2;
	}

	public String getBak3() {
		return Bak3;
	}

	public void setBak3(String bak3) {
		Bak3 = bak3;
	}

	public CErrors getmErrors() {
		return mErrors;
	}

	public void setmErrors(CErrors mErrors) {
		this.mErrors = mErrors;
	}

	public static int getFieldnum() {
		return FIELDNUM;
	}

	public int getFieldCount() {
		// TODO Auto-generated method stub
		return FIELDNUM;
	}

	public int getFieldIndex(String strFieldName) {
		// TODO Auto-generated method stub
		return 0;
	}

	public String getFieldName(int nFieldIndex) {

		String strFieldName = "";
		switch (nFieldIndex) {
		case 0:
			strFieldName = "CIITC_TASK_NO";
			break;
		case 1:
			strFieldName = "ID";
			break;
		case 2:
			strFieldName = "BATCHNO";
			break;
		case 3:
			strFieldName = "PROPOSALNO";
			break;
		case 4:
			strFieldName = "FILENAME";
			break;
		case 5:
			strFieldName = "SERVER_IP";
			break;
		case 6:
			strFieldName = "SERVER_USER";
			break;
		case 7:
			strFieldName = "SERVER_PASSWORD";
			break;
		case 8:
			strFieldName = "SERVER_PORT";
			break;
		case 9:
			strFieldName = "LOCALFILEPATHS";
			break;
		case 10:
			strFieldName = "REMOTEFILEPATHS";
			break;
		case 11:
			strFieldName = "SOURCECHANNEL";
			break;
		case 12:
			strFieldName = "PRIORITY";
			break;
		case 13:
			strFieldName = "FILEMD5";
			break;
		case 14:
			strFieldName = "COOKIE";
			break;
		case 15:
			strFieldName = "TASK_ID";
			break;
		case 16:
			strFieldName = "TRANSTYPE";
			break;
		case 17:
			strFieldName = "REQMESSAGE";
			break;
		case 18:
			strFieldName = "FLAG";
			break;
		case 19:
			strFieldName = "STATUS";
			break;
		case 20:
			strFieldName = "TRANSMSG";
			break;
		case 21:
			strFieldName = "FILEMAKEDATE";
			break;
		case 22:
			strFieldName = "MAKEDATE";
			break;
		case 23:
			strFieldName = "MODIFYDATE";
			break;
		case 24:
			strFieldName = "Bak1";
			break;
		case 25:
			strFieldName = "Bak2";
			break;
		case 26:
			strFieldName = "Bak3";
			break;
		default:
			strFieldName = "";
		}
		;
		return strFieldName;

	}

	public int getFieldType(String strFieldName) {

		if (strFieldName.equals("CiitcTaskNo")) {
			return Schema.TYPE_STRING;
		}
		if (strFieldName.equals("Id")) {
			return Schema.TYPE_INT;
		}
		if (strFieldName.equals("BatchNo")) {
			return Schema.TYPE_STRING;
		}
		if (strFieldName.equals("ProposalNo")) {
			return Schema.TYPE_STRING;
		}
		if (strFieldName.equals("FileName")) {
			return Schema.TYPE_STRING;
		}
		if (strFieldName.equals("ServerIp")) {
			return Schema.TYPE_STRING;
		}
		if (strFieldName.equals("ServerUser")) {
			return Schema.TYPE_STRING;
		}
		if (strFieldName.equals("ServerPassWord")) {
			return Schema.TYPE_STRING;
		}
		if (strFieldName.equals("ServerPort")) {
			return Schema.TYPE_STRING;
		}
		if (strFieldName.equals("LocalFilePaths")) {
			return Schema.TYPE_STRING;
		}
		if (strFieldName.equals("RemoteFilePaths")) {
			return Schema.TYPE_STRING;
		}
		if (strFieldName.equals("SourceChannel")) {
			return Schema.TYPE_STRING;
		}
		if (strFieldName.equals("Priority")) {
			return Schema.TYPE_STRING;
		}
		if (strFieldName.equals("FileMD5")) {
			return Schema.TYPE_STRING;
		}
		if (strFieldName.equals("Cookie")) {
			return Schema.TYPE_STRING;
		}
		if (strFieldName.equals("TaskId")) {
			return Schema.TYPE_STRING;
		}
		if (strFieldName.equals("TransType")) {
			return Schema.TYPE_STRING;
		}
		if (strFieldName.equals("ReqMessage")) {
			return Schema.TYPE_STRING;
		}
		if (strFieldName.equals("Flag")) {
			return Schema.TYPE_STRING;
		}
		if (strFieldName.equals("Status")) {
			return Schema.TYPE_STRING;
		}
		if (strFieldName.equals("TransMSG")) {
			return Schema.TYPE_STRING;
		}
		if (strFieldName.equals("FileMakeDate")) {
			return Schema.TYPE_DATE;
		}
		if (strFieldName.equals("MakeDate")) {
			return Schema.TYPE_DATE;
		}
		if (strFieldName.equals("ModifyDate")) {
			return Schema.TYPE_DATE;
		}
		if (strFieldName.equals("Bak1")) {
			return Schema.TYPE_STRING;
		}
		if (strFieldName.equals("Bak2")) {
			return Schema.TYPE_STRING;
		}
		if (strFieldName.equals("Bak3")) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	public int getFieldType(int nFieldIndex) {
		// TODO Auto-generated method stub
		return 0;
	}

	public String[] getPK() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getV(String FCode) {

		String strReturn = "";
		if (FCode.equals("CiitcTaskNo")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(CiitcTaskNo));
		}
		if (FCode.equals("Id")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(Id));
		}
		if (FCode.equals("BatchNo")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(BatchNo));
		}
		if (FCode.equals("ProposalNo")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProposalNo));
		}
		if (FCode.equals("FileName")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(FileName));
		}
		if (FCode.equals("ServerIp")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(ServerIp));
		}
		if (FCode.equals("ServerUser")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(ServerUser));
		}
		if (FCode.equals("ServerPassWord")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(ServerPassWord));
		}
		if (FCode.equals("ServerPort")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(ServerPort));
		}
		if (FCode.equals("LocalFilePaths")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(LocalFilePaths));
		}
		if (FCode.equals("RemoteFilePaths")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(RemoteFilePaths));
		}
		if (FCode.equals("SourceChannel")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(SourceChannel));
		}
		if (FCode.equals("Priority")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(Priority));
		}
		if (FCode.equals("FileMD5")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(FileMD5));
		}
		if (FCode.equals("Cookie")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(Cookie));
		}
		if (FCode.equals("TaskId")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(TaskId));
		}
		if (FCode.equals("TransType")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransType));
		}
		if (FCode.equals("ReqMessage")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReqMessage));
		}
		if (FCode.equals("Flag")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(Flag));
		}
		if (FCode.equals("Status")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(Status));
		}
		if (FCode.equals("TransMSG")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransMSG));
		}
		if (FCode.equals("FileMakeDate")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(this
					.getFileMakeDate()));
		}
		if (FCode.equals("MakeDate")) {
			strReturn = StrTool
					.GBKToUnicode(String.valueOf(this.getMakeDate()));
		}
		if (FCode.equals("ModifyDate")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(this
					.getModifyDate()));
		}
		if (FCode.equals("Bak1")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(Bak1));
		}
		if (FCode.equals("Bak2")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(Bak2));
		}
		if (FCode.equals("Bak3")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(Bak3));
		}

		return strReturn;

	}

	public String getV(int nFieldIndex) {

		String strFieldValue = "";
		switch (nFieldIndex) {
		case 0:
			strFieldValue = StrTool.GBKToUnicode(CiitcTaskNo);
			break;
		case 1:
			strFieldValue = StrTool.GBKToUnicode(String.valueOf(Id));
			break;
		case 2:
			strFieldValue = StrTool.GBKToUnicode(BatchNo);
			break;
		case 3:
			strFieldValue = StrTool.GBKToUnicode(ProposalNo);
			break;
		case 4:
			strFieldValue = StrTool.GBKToUnicode(FileName);
			break;
		case 5:
			strFieldValue = StrTool.GBKToUnicode(ServerIp);
			break;
		case 6:
			strFieldValue = StrTool.GBKToUnicode(ServerUser);
			break;
		case 7:
			strFieldValue = StrTool.GBKToUnicode(ServerPassWord);
			break;
		case 8:
			strFieldValue = StrTool.GBKToUnicode(ServerPort);
			break;
		case 9:
			strFieldValue = StrTool.GBKToUnicode(LocalFilePaths);
			break;
		case 10:
			strFieldValue = StrTool.GBKToUnicode(RemoteFilePaths);
			break;
		case 11:
			strFieldValue = StrTool.GBKToUnicode(SourceChannel);
			break;
		case 12:
			strFieldValue = StrTool.GBKToUnicode(Priority);
			break;
		case 13:
			strFieldValue = StrTool.GBKToUnicode(FileMD5);
			break;
		case 14:
			strFieldValue = StrTool.GBKToUnicode(Cookie);
			break;
		case 15:
			strFieldValue = StrTool.GBKToUnicode(TaskId);
			break;
		case 16:
			strFieldValue = StrTool.GBKToUnicode(TransType);
			break;
		case 17:
			strFieldValue = StrTool.GBKToUnicode(ReqMessage);
			break;
		case 18:
			strFieldValue = StrTool.GBKToUnicode(Flag);
			break;
		case 19:
			strFieldValue = StrTool.GBKToUnicode(Status);
			break;
		case 20:
			strFieldValue = StrTool.GBKToUnicode(TransMSG);
			break;
		case 21:
			strFieldValue = StrTool.GBKToUnicode(String.valueOf(this
					.getFileMakeDate()));
			break;
		case 22:
			strFieldValue = StrTool.GBKToUnicode(String.valueOf(this
					.getMakeDate()));
			break;
		case 23:
			strFieldValue = StrTool.GBKToUnicode(String.valueOf(this
					.getModifyDate()));
			break;
		case 24:
			strFieldValue = StrTool.GBKToUnicode(Bak1);
			break;
		case 25:
			strFieldValue = StrTool.GBKToUnicode(Bak2);
			break;
		case 26:
			strFieldValue = StrTool.GBKToUnicode(Bak3);
			break;
		default:
			strFieldValue = "";
		}
		;
		if (strFieldValue.equals("")) {
			strFieldValue = "null";
		}
		return strFieldValue;

	}

	public boolean setV(String strFieldName, String strFieldValue) {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * 使用另外一个 CiitcTaskSchema 对象给 Schema 赋值
	 * 
	 * @param: aCiitcTaskSchema CiitcTaskSchema
	 **/
	public void setSchema(TransTaskZBXSLSchema aTransTaskZBXSLSchema) {
		this.CiitcTaskNo = aTransTaskZBXSLSchema.getCiitcTaskNo();
		this.Id = aTransTaskZBXSLSchema.getId();
		this.BatchNo = aTransTaskZBXSLSchema.getBatchNo();
		this.ProposalNo = aTransTaskZBXSLSchema.getProposalNo();
		this.FileName = aTransTaskZBXSLSchema.getFileName();
		this.ServerIp = aTransTaskZBXSLSchema.getServerIp();
		this.ServerUser = aTransTaskZBXSLSchema.getServerUser();
		this.ServerPassWord = aTransTaskZBXSLSchema.getServerPassWord();
		this.ServerPort = aTransTaskZBXSLSchema.getServerPort();
		this.LocalFilePaths = aTransTaskZBXSLSchema.getLocalFilePaths();
		this.RemoteFilePaths = aTransTaskZBXSLSchema.getRemoteFilePaths();
		this.SourceChannel = aTransTaskZBXSLSchema.getSourceChannel();
		this.Priority = aTransTaskZBXSLSchema.getPriority();
		this.FileMD5 = aTransTaskZBXSLSchema.getFileMD5();
		this.Cookie = aTransTaskZBXSLSchema.getCookie();
		this.TaskId = aTransTaskZBXSLSchema.getTaskId();
		this.TransType = aTransTaskZBXSLSchema.getTransType();
		this.ReqMessage = aTransTaskZBXSLSchema.getReqMessage();
		this.Flag = aTransTaskZBXSLSchema.getFlag();
		this.Status = aTransTaskZBXSLSchema.getStatus();
		this.TransMSG = aTransTaskZBXSLSchema.getTransMSG();
		this.FileMakeDate = fDate.getDate(aTransTaskZBXSLSchema.getFileMakeDate());
		this.MakeDate = fDate.getDate(aTransTaskZBXSLSchema.getMakeDate());
		this.ModifyDate = fDate.getDate(aTransTaskZBXSLSchema.getModifyDate());
		this.Bak1 = aTransTaskZBXSLSchema.getBak1();
		this.Bak2 = aTransTaskZBXSLSchema.getBak2();
		this.Bak3 = aTransTaskZBXSLSchema.getBak3();
	}

	public TransTaskZBXSLSchema getSchema() {
		TransTaskZBXSLSchema aCiitcTaskSchema = new TransTaskZBXSLSchema();
		aCiitcTaskSchema.setSchema(this);
		return aCiitcTaskSchema;
	}

	public boolean setSchema(ResultSet rs, int i) {
		try {
			// rs.absolute(i); // 非滚动游标
			if (rs.getString("CiitcTaskNo") == null)
				this.CiitcTaskNo = null;
			else
				this.CiitcTaskNo = rs.getString("CiitcTaskNo").trim();

			if (rs.getString("Id") == null)
				this.Id = -1;
			else
				this.ProposalNo = rs.getString("Id").trim();

			if (rs.getString("BatchNo") == null)
				this.BatchNo = null;
			else
				this.BatchNo = rs.getString("BatchNo").trim();

			if (rs.getString("ProposalNo") == null)
				this.ProposalNo = null;
			else
				this.ProposalNo = rs.getString("ProposalNo").trim();

			if (rs.getString("FileName") == null)
				this.FileName = null;
			else
				this.FileName = rs.getString("FileName").trim();

			if (rs.getString("ServerIp") == null)
				this.ServerIp = null;
			else
				this.ServerIp = rs.getString("ServerIp").trim();

			if (rs.getString("ServerUser") == null)
				this.ServerUser = null;
			else
				this.ServerUser = rs.getString("ServerUser").trim();

			if (rs.getString("ServerPassWord") == null)
				this.ServerPassWord = null;
			else
				this.ServerPassWord = rs.getString("ServerPassWord").trim();

			if (rs.getString("ServerPort") == null)
				this.ServerPort = null;
			else
				this.ServerPort = rs.getString("ServerPort").trim();

			if (rs.getString("LocalFilePaths") == null)
				this.LocalFilePaths = null;
			else
				this.LocalFilePaths = rs.getString("LocalFilePaths").trim();

			if (rs.getString("RemoteFilePaths") == null)
				this.RemoteFilePaths = null;
			else
				this.RemoteFilePaths = rs.getString("RemoteFilePaths").trim();

			if (rs.getString("SourceChannel") == null)
				this.SourceChannel = null;
			else
				this.SourceChannel = rs.getString("SourceChannel").trim();

			if (rs.getString("Priority") == null)
				this.Priority = null;
			else
				this.Priority = rs.getString("Priority").trim();

			if (rs.getString("FileMD5") == null)
				this.FileMD5 = null;
			else
				this.FileMD5 = rs.getString("FileMD5").trim();

			if (rs.getString("Cookie") == null)
				this.Cookie = null;
			else
				this.Cookie = rs.getString("Cookie").trim();

			if (rs.getString("TaskId") == null)
				this.TaskId = null;
			else
				this.TaskId = rs.getString("TaskId").trim();

			if (rs.getString("TransType") == null)
				this.TransType = null;
			else
				this.TransType = rs.getString("TransType").trim();

			if (rs.getString("ReqMessage") == null)
				this.ReqMessage = null;
			else
				this.ReqMessage = rs.getString("ReqMessage").trim();

			if (rs.getString("Flag") == null)
				this.Flag = null;
			else
				this.Flag = rs.getString("Flag").trim();

			if (rs.getString("Status") == null)
				this.Status = null;
			else
				this.Status = rs.getString("Status").trim();

			if (rs.getString("TransMSG") == null)
				this.TransMSG = null;
			else
				this.TransMSG = rs.getString("TransMSG").trim();

			this.FileMakeDate = rs.getDate("FileMakeDate");

			this.MakeDate = rs.getDate("MakeDate");

			this.ModifyDate = rs.getDate("ModifyDate");

			if (rs.getString("Bak1") == null)
				this.Bak1 = null;
			else
				this.Bak1 = rs.getString("Bak1").trim();

			if (rs.getString("Bak2") == null)
				this.Bak2 = null;
			else
				this.Bak2 = rs.getString("Bak2").trim();

			if (rs.getString("Bak3") == null)
				this.Bak3 = null;
			else
				this.Bak3 = rs.getString("Bak3").trim();

		} catch (SQLException sqle) {
			System.out
					.println("数据库中的LKTransStatus表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LKTransStatusSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	/**
	 * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}
	 * /dataStructure/tb.html#PrpCiitcTask描述/A>表字段
	 * 
	 * @return: String 返回打包后字符串
	 **/
	public String encode() {
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(CiitcTaskNo));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(String.valueOf(Id)));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BatchNo));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProposalNo));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FileName));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ServerIp));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ServerUser));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ServerPassWord));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ServerPort));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LocalFilePaths));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RemoteFilePaths));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SourceChannel));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Priority));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FileMD5));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Cookie));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TaskId));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TransType));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReqMessage));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Flag));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Status));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TransMSG));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BatchNo));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString(FileMakeDate)));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Bak1));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Bak2));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Bak3));
		strReturn.append(SysConst.PACKAGESPILTER);
		return strReturn.toString();
	}

	/**
	 * 数据解包，解包顺序参见<A href ={@docRoot}
	 * /dataStructure/tb.html#PrpCiitcTask>历史记账凭证主表信息</A>表字段
	 * 
	 * @param: strMessage String 包含一条纪录数据的字符串
	 * @return: boolean
	 **/
	public boolean decode(String strMessage) {
		try {
			CiitcTaskNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
					SysConst.PACKAGESPILTER);
			Id = Integer.parseInt(StrTool.getStr(StrTool
					.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER));
			BatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
					SysConst.PACKAGESPILTER);
			ProposalNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
					SysConst.PACKAGESPILTER);
			FileName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
					SysConst.PACKAGESPILTER);
			ServerIp = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
					SysConst.PACKAGESPILTER);
			ServerUser = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
					SysConst.PACKAGESPILTER);
			ServerPassWord = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
					8, SysConst.PACKAGESPILTER);
			ServerPort = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
					SysConst.PACKAGESPILTER);
			LocalFilePaths = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
					10, SysConst.PACKAGESPILTER);
			RemoteFilePaths = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
					11, SysConst.PACKAGESPILTER);
			SourceChannel = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
					12, SysConst.PACKAGESPILTER);
			Priority = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
					SysConst.PACKAGESPILTER);
			FileMD5 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
					SysConst.PACKAGESPILTER);
			Cookie = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
					SysConst.PACKAGESPILTER);
			TaskId = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
					SysConst.PACKAGESPILTER);
			TransType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
					SysConst.PACKAGESPILTER);
			ReqMessage = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
					SysConst.PACKAGESPILTER);
			Flag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
					SysConst.PACKAGESPILTER);
			Status = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
					SysConst.PACKAGESPILTER);
			TransMSG = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,
					SysConst.PACKAGESPILTER);
			FileMakeDate = fDate.getDate(StrTool.getStr(StrTool
					.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER));
			MakeDate = fDate.getDate(StrTool.getStr(StrTool
					.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER));
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool
					.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER));
			Bak1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,
					SysConst.PACKAGESPILTER);
			Bak2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26,
					SysConst.PACKAGESPILTER);
			Bak3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27,
					SysConst.PACKAGESPILTER);
		} catch (NumberFormatException ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "CiitcTaskSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors.addOneError(tError);

			return false;
		}
		return true;
	}

}
