/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LGTraceNodeOpDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LGTraceNodeOpSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: TaskTraceNodeOp
 * @CreateDate：2005-06-17
 */
public class LGTraceNodeOpSchema implements Schema, Cloneable
{
    // @Field
    /** 作业编号 */
    private String WorkNo;
    /** 结点编号 */
    private String NodeNo;
    /** 操作编号 */
    private String OperatorNo;
    /** 操作类型 */
    private String OperatorType;
    /** 批注 */
    private String Remark;
    /** 完结日期 */
    private Date FinishDate;
    /** 完结时间 */
    private String FinishTime;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 12; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LGTraceNodeOpSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "WorkNo";
        pk[1] = "NodeNo";
        pk[2] = "OperatorNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException
    {
        LGTraceNodeOpSchema cloned = (LGTraceNodeOpSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getWorkNo()
    {
        return WorkNo;
    }

    public void setWorkNo(String aWorkNo)
    {
        WorkNo = aWorkNo;
    }

    public String getNodeNo()
    {
        return NodeNo;
    }

    public void setNodeNo(String aNodeNo)
    {
        NodeNo = aNodeNo;
    }

    public String getOperatorNo()
    {
        return OperatorNo;
    }

    public void setOperatorNo(String aOperatorNo)
    {
        OperatorNo = aOperatorNo;
    }

    public String getOperatorType()
    {
        return OperatorType;
    }

    public void setOperatorType(String aOperatorType)
    {
        OperatorType = aOperatorType;
    }

    public String getRemark()
    {
        return Remark;
    }

    public void setRemark(String aRemark)
    {
        Remark = aRemark;
    }

    public String getFinishDate()
    {
        if (FinishDate != null)
        {
            return fDate.getString(FinishDate);
        }
        else
        {
            return null;
        }
    }

    public void setFinishDate(Date aFinishDate)
    {
        FinishDate = aFinishDate;
    }

    public void setFinishDate(String aFinishDate)
    {
        if (aFinishDate != null && !aFinishDate.equals(""))
        {
            FinishDate = fDate.getDate(aFinishDate);
        }
        else
        {
            FinishDate = null;
        }
    }

    public String getFinishTime()
    {
        return FinishTime;
    }

    public void setFinishTime(String aFinishTime)
    {
        FinishTime = aFinishTime;
    }

    public String getOperator()
    {
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LGTraceNodeOpSchema 对象给 Schema 赋值
     * @param: aLGTraceNodeOpSchema LGTraceNodeOpSchema
     **/
    public void setSchema(LGTraceNodeOpSchema aLGTraceNodeOpSchema)
    {
        this.WorkNo = aLGTraceNodeOpSchema.getWorkNo();
        this.NodeNo = aLGTraceNodeOpSchema.getNodeNo();
        this.OperatorNo = aLGTraceNodeOpSchema.getOperatorNo();
        this.OperatorType = aLGTraceNodeOpSchema.getOperatorType();
        this.Remark = aLGTraceNodeOpSchema.getRemark();
        this.FinishDate = fDate.getDate(aLGTraceNodeOpSchema.getFinishDate());
        this.FinishTime = aLGTraceNodeOpSchema.getFinishTime();
        this.Operator = aLGTraceNodeOpSchema.getOperator();
        this.MakeDate = fDate.getDate(aLGTraceNodeOpSchema.getMakeDate());
        this.MakeTime = aLGTraceNodeOpSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLGTraceNodeOpSchema.getModifyDate());
        this.ModifyTime = aLGTraceNodeOpSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("WorkNo") == null)
            {
                this.WorkNo = null;
            }
            else
            {
                this.WorkNo = rs.getString("WorkNo").trim();
            }

            if (rs.getString("NodeNo") == null)
            {
                this.NodeNo = null;
            }
            else
            {
                this.NodeNo = rs.getString("NodeNo").trim();
            }

            if (rs.getString("OperatorNo") == null)
            {
                this.OperatorNo = null;
            }
            else
            {
                this.OperatorNo = rs.getString("OperatorNo").trim();
            }

            if (rs.getString("OperatorType") == null)
            {
                this.OperatorType = null;
            }
            else
            {
                this.OperatorType = rs.getString("OperatorType").trim();
            }

            if (rs.getString("Remark") == null)
            {
                this.Remark = null;
            }
            else
            {
                this.Remark = rs.getString("Remark").trim();
            }

            this.FinishDate = rs.getDate("FinishDate");
            if (rs.getString("FinishTime") == null)
            {
                this.FinishTime = null;
            }
            else
            {
                this.FinishTime = rs.getString("FinishTime").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            System.out.println("数据库中的LGTraceNodeOp表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LGTraceNodeOpSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LGTraceNodeOpSchema getSchema()
    {
        LGTraceNodeOpSchema aLGTraceNodeOpSchema = new LGTraceNodeOpSchema();
        aLGTraceNodeOpSchema.setSchema(this);
        return aLGTraceNodeOpSchema;
    }

    public LGTraceNodeOpDB getDB()
    {
        LGTraceNodeOpDB aDBOper = new LGTraceNodeOpDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLGTraceNodeOp描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(WorkNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(NodeNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OperatorNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OperatorType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Remark));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(FinishDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FinishTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLGTraceNodeOp>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            WorkNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                    SysConst.PACKAGESPILTER);
            NodeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                    SysConst.PACKAGESPILTER);
            OperatorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                        SysConst.PACKAGESPILTER);
            OperatorType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                          SysConst.PACKAGESPILTER);
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                    SysConst.PACKAGESPILTER);
            FinishDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 6, SysConst.PACKAGESPILTER));
            FinishTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                        SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 9, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 11, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LGTraceNodeOpSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("WorkNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(WorkNo));
        }
        if (FCode.equals("NodeNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NodeNo));
        }
        if (FCode.equals("OperatorNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OperatorNo));
        }
        if (FCode.equals("OperatorType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OperatorType));
        }
        if (FCode.equals("Remark"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equals("FinishDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getFinishDate()));
        }
        if (FCode.equals("FinishTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FinishTime));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(WorkNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(NodeNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(OperatorNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(OperatorType);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getFinishDate()));
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(FinishTime);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("WorkNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                WorkNo = FValue.trim();
            }
            else
            {
                WorkNo = null;
            }
        }
        if (FCode.equals("NodeNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                NodeNo = FValue.trim();
            }
            else
            {
                NodeNo = null;
            }
        }
        if (FCode.equals("OperatorNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OperatorNo = FValue.trim();
            }
            else
            {
                OperatorNo = null;
            }
        }
        if (FCode.equals("OperatorType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OperatorType = FValue.trim();
            }
            else
            {
                OperatorType = null;
            }
        }
        if (FCode.equals("Remark"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
            {
                Remark = null;
            }
        }
        if (FCode.equals("FinishDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                FinishDate = fDate.getDate(FValue);
            }
            else
            {
                FinishDate = null;
            }
        }
        if (FCode.equals("FinishTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                FinishTime = FValue.trim();
            }
            else
            {
                FinishTime = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LGTraceNodeOpSchema other = (LGTraceNodeOpSchema) otherObject;
        return
                WorkNo.equals(other.getWorkNo())
                && NodeNo.equals(other.getNodeNo())
                && OperatorNo.equals(other.getOperatorNo())
                && OperatorType.equals(other.getOperatorType())
                && Remark.equals(other.getRemark())
                && fDate.getString(FinishDate).equals(other.getFinishDate())
                && FinishTime.equals(other.getFinishTime())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("WorkNo"))
        {
            return 0;
        }
        if (strFieldName.equals("NodeNo"))
        {
            return 1;
        }
        if (strFieldName.equals("OperatorNo"))
        {
            return 2;
        }
        if (strFieldName.equals("OperatorType"))
        {
            return 3;
        }
        if (strFieldName.equals("Remark"))
        {
            return 4;
        }
        if (strFieldName.equals("FinishDate"))
        {
            return 5;
        }
        if (strFieldName.equals("FinishTime"))
        {
            return 6;
        }
        if (strFieldName.equals("Operator"))
        {
            return 7;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 8;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 9;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 10;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 11;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "WorkNo";
                break;
            case 1:
                strFieldName = "NodeNo";
                break;
            case 2:
                strFieldName = "OperatorNo";
                break;
            case 3:
                strFieldName = "OperatorType";
                break;
            case 4:
                strFieldName = "Remark";
                break;
            case 5:
                strFieldName = "FinishDate";
                break;
            case 6:
                strFieldName = "FinishTime";
                break;
            case 7:
                strFieldName = "Operator";
                break;
            case 8:
                strFieldName = "MakeDate";
                break;
            case 9:
                strFieldName = "MakeTime";
                break;
            case 10:
                strFieldName = "ModifyDate";
                break;
            case 11:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("WorkNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("NodeNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OperatorNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OperatorType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Remark"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FinishDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("FinishTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
