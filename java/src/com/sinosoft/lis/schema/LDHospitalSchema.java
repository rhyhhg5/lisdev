/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LDHospitalDB;

/*
 * <p>ClassName: LDHospitalSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2010-03-09
 */
public class LDHospitalSchema implements Schema, Cloneable
{
	// @Field
	/** 医疗机构代码 */
	private String HospitCode;
	/** 医疗机构名称 */
	private String HospitName;
	/** 签约标识 */
	private String FixFlag;
	/** 医疗机构简称 */
	private String HospitShortName;
	/** 社保定点机构标志 */
	private String CommunFixFlag;
	/** 所在地区代码 */
	private String AreaCode;
	/** 医院等级代码 */
	private String LevelCode;
	/** 机构分类管理代码 */
	private String AdminiSortCode;
	/** 业务类型代码 */
	private String BusiTypeCode;
	/** 经济成分代码 */
	private String EconomElemenCode;
	/** 地址 */
	private String Address;
	/** 邮编 */
	private String ZipCode;
	/** 联系电话 */
	private String Phone;
	/** 网址 */
	private String WebAddress;
	/** 传真 */
	private String Fax;
	/** 执业许可证号 */
	private String HospitLicencNo;
	/** 开户银行 */
	private String bankCode;
	/** 户名 */
	private String AccName;
	/** 银行帐号 */
	private String bankAccNo;
	/** 负责人 */
	private String SatrapName;
	/** 联系人 */
	private String Linkman;
	/** 最近修改日期 */
	private Date LastModiDate;
	/** 地区名称 */
	private String AreaName;
	/** 医疗机构类型 */
	private String HospitalType;
	/** 合作级别 */
	private String AssociateClass;
	/** 流水号 */
	private String FlowNo;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 医院完全码 */
	private String HospitalStandardCode;
	/** 日均门诊量 */
	private String PatientPerDay;
	/** 年均出院人数 */
	private String OutHospital;
	/** 高级职称人数 */
	private String SuperiorNo;
	/** 中级职称人数 */
	private String InterNo;
	/** 初级职称人数 */
	private String ElementaryNo;
	/** 职工总人数 */
	private String TotalNo;
	/** 床位数 */
	private String BedAmount;
	/** 教授人数 */
	private String ProfAmount;
	/** 主任医师人数 */
	private String DirectAmount;
	/** 副主任医师人数 */
	private String AssistDirectAmount;
	/** 主治医师人数 */
	private String MainDoctorAmount;
	/** 管理机构 */
	private String ManageCom;
	/** 城乡属性 */
	private String UrbanOption;
	/** 乘车路线 */
	private String Route;
	/** 社保号码 */
	private String SecurityNo;
	/** 核保体检医院 */
	private String Examination;

	public static final int FIELDNUM = 48;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LDHospitalSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "HospitCode";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LDHospitalSchema cloned = (LDHospitalSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getHospitCode()
	{
		return HospitCode;
	}
	public void setHospitCode(String aHospitCode)
	{
		HospitCode = aHospitCode;
	}
	public String getHospitName()
	{
		return HospitName;
	}
	public void setHospitName(String aHospitName)
	{
		HospitName = aHospitName;
	}
	public String getFixFlag()
	{
		return FixFlag;
	}
	public void setFixFlag(String aFixFlag)
	{
		FixFlag = aFixFlag;
	}
	public String getHospitShortName()
	{
		return HospitShortName;
	}
	public void setHospitShortName(String aHospitShortName)
	{
		HospitShortName = aHospitShortName;
	}
	public String getCommunFixFlag()
	{
		return CommunFixFlag;
	}
	public void setCommunFixFlag(String aCommunFixFlag)
	{
		CommunFixFlag = aCommunFixFlag;
	}
	public String getAreaCode()
	{
		return AreaCode;
	}
	public void setAreaCode(String aAreaCode)
	{
		AreaCode = aAreaCode;
	}
	public String getLevelCode()
	{
		return LevelCode;
	}
	public void setLevelCode(String aLevelCode)
	{
		LevelCode = aLevelCode;
	}
	public String getAdminiSortCode()
	{
		return AdminiSortCode;
	}
	public void setAdminiSortCode(String aAdminiSortCode)
	{
		AdminiSortCode = aAdminiSortCode;
	}
	public String getBusiTypeCode()
	{
		return BusiTypeCode;
	}
	public void setBusiTypeCode(String aBusiTypeCode)
	{
		BusiTypeCode = aBusiTypeCode;
	}
	public String getEconomElemenCode()
	{
		return EconomElemenCode;
	}
	public void setEconomElemenCode(String aEconomElemenCode)
	{
		EconomElemenCode = aEconomElemenCode;
	}
	public String getAddress()
	{
		return Address;
	}
	public void setAddress(String aAddress)
	{
		Address = aAddress;
	}
	public String getZipCode()
	{
		return ZipCode;
	}
	public void setZipCode(String aZipCode)
	{
		ZipCode = aZipCode;
	}
	public String getPhone()
	{
		return Phone;
	}
	public void setPhone(String aPhone)
	{
		Phone = aPhone;
	}
	public String getWebAddress()
	{
		return WebAddress;
	}
	public void setWebAddress(String aWebAddress)
	{
		WebAddress = aWebAddress;
	}
	public String getFax()
	{
		return Fax;
	}
	public void setFax(String aFax)
	{
		Fax = aFax;
	}
	public String getHospitLicencNo()
	{
		return HospitLicencNo;
	}
	public void setHospitLicencNo(String aHospitLicencNo)
	{
		HospitLicencNo = aHospitLicencNo;
	}
	public String getbankCode()
	{
		return bankCode;
	}
	public void setbankCode(String abankCode)
	{
		bankCode = abankCode;
	}
	public String getAccName()
	{
		return AccName;
	}
	public void setAccName(String aAccName)
	{
		AccName = aAccName;
	}
	public String getbankAccNo()
	{
		return bankAccNo;
	}
	public void setbankAccNo(String abankAccNo)
	{
		bankAccNo = abankAccNo;
	}
	public String getSatrapName()
	{
		return SatrapName;
	}
	public void setSatrapName(String aSatrapName)
	{
		SatrapName = aSatrapName;
	}
	public String getLinkman()
	{
		return Linkman;
	}
	public void setLinkman(String aLinkman)
	{
		Linkman = aLinkman;
	}
	public String getLastModiDate()
	{
		if( LastModiDate != null )
			return fDate.getString(LastModiDate);
		else
			return null;
	}
	public void setLastModiDate(Date aLastModiDate)
	{
		LastModiDate = aLastModiDate;
	}
	public void setLastModiDate(String aLastModiDate)
	{
		if (aLastModiDate != null && !aLastModiDate.equals("") )
		{
			LastModiDate = fDate.getDate( aLastModiDate );
		}
		else
			LastModiDate = null;
	}

	public String getAreaName()
	{
		return AreaName;
	}
	public void setAreaName(String aAreaName)
	{
		AreaName = aAreaName;
	}
	public String getHospitalType()
	{
		return HospitalType;
	}
	public void setHospitalType(String aHospitalType)
	{
		HospitalType = aHospitalType;
	}
	public String getAssociateClass()
	{
		return AssociateClass;
	}
	public void setAssociateClass(String aAssociateClass)
	{
		AssociateClass = aAssociateClass;
	}
	public String getFlowNo()
	{
		return FlowNo;
	}
	public void setFlowNo(String aFlowNo)
	{
		FlowNo = aFlowNo;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getHospitalStandardCode()
	{
		return HospitalStandardCode;
	}
	public void setHospitalStandardCode(String aHospitalStandardCode)
	{
		HospitalStandardCode = aHospitalStandardCode;
	}
	public String getPatientPerDay()
	{
		return PatientPerDay;
	}
	public void setPatientPerDay(String aPatientPerDay)
	{
		PatientPerDay = aPatientPerDay;
	}
	public String getOutHospital()
	{
		return OutHospital;
	}
	public void setOutHospital(String aOutHospital)
	{
		OutHospital = aOutHospital;
	}
	public String getSuperiorNo()
	{
		return SuperiorNo;
	}
	public void setSuperiorNo(String aSuperiorNo)
	{
		SuperiorNo = aSuperiorNo;
	}
	public String getInterNo()
	{
		return InterNo;
	}
	public void setInterNo(String aInterNo)
	{
		InterNo = aInterNo;
	}
	public String getElementaryNo()
	{
		return ElementaryNo;
	}
	public void setElementaryNo(String aElementaryNo)
	{
		ElementaryNo = aElementaryNo;
	}
	public String getTotalNo()
	{
		return TotalNo;
	}
	public void setTotalNo(String aTotalNo)
	{
		TotalNo = aTotalNo;
	}
	public String getBedAmount()
	{
		return BedAmount;
	}
	public void setBedAmount(String aBedAmount)
	{
		BedAmount = aBedAmount;
	}
	public String getProfAmount()
	{
		return ProfAmount;
	}
	public void setProfAmount(String aProfAmount)
	{
		ProfAmount = aProfAmount;
	}
	public String getDirectAmount()
	{
		return DirectAmount;
	}
	public void setDirectAmount(String aDirectAmount)
	{
		DirectAmount = aDirectAmount;
	}
	public String getAssistDirectAmount()
	{
		return AssistDirectAmount;
	}
	public void setAssistDirectAmount(String aAssistDirectAmount)
	{
		AssistDirectAmount = aAssistDirectAmount;
	}
	public String getMainDoctorAmount()
	{
		return MainDoctorAmount;
	}
	public void setMainDoctorAmount(String aMainDoctorAmount)
	{
		MainDoctorAmount = aMainDoctorAmount;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getUrbanOption()
	{
		return UrbanOption;
	}
	public void setUrbanOption(String aUrbanOption)
	{
		UrbanOption = aUrbanOption;
	}
	public String getRoute()
	{
		return Route;
	}
	public void setRoute(String aRoute)
	{
		Route = aRoute;
	}
	public String getSecurityNo()
	{
		return SecurityNo;
	}
	public void setSecurityNo(String aSecurityNo)
	{
		SecurityNo = aSecurityNo;
	}
	public String getExamination()
	{
		return Examination;
	}
	public void setExamination(String aExamination)
	{
		Examination = aExamination;
	}

	/**
	* 使用另外一个 LDHospitalSchema 对象给 Schema 赋值
	* @param: aLDHospitalSchema LDHospitalSchema
	**/
	public void setSchema(LDHospitalSchema aLDHospitalSchema)
	{
		this.HospitCode = aLDHospitalSchema.getHospitCode();
		this.HospitName = aLDHospitalSchema.getHospitName();
		this.FixFlag = aLDHospitalSchema.getFixFlag();
		this.HospitShortName = aLDHospitalSchema.getHospitShortName();
		this.CommunFixFlag = aLDHospitalSchema.getCommunFixFlag();
		this.AreaCode = aLDHospitalSchema.getAreaCode();
		this.LevelCode = aLDHospitalSchema.getLevelCode();
		this.AdminiSortCode = aLDHospitalSchema.getAdminiSortCode();
		this.BusiTypeCode = aLDHospitalSchema.getBusiTypeCode();
		this.EconomElemenCode = aLDHospitalSchema.getEconomElemenCode();
		this.Address = aLDHospitalSchema.getAddress();
		this.ZipCode = aLDHospitalSchema.getZipCode();
		this.Phone = aLDHospitalSchema.getPhone();
		this.WebAddress = aLDHospitalSchema.getWebAddress();
		this.Fax = aLDHospitalSchema.getFax();
		this.HospitLicencNo = aLDHospitalSchema.getHospitLicencNo();
		this.bankCode = aLDHospitalSchema.getbankCode();
		this.AccName = aLDHospitalSchema.getAccName();
		this.bankAccNo = aLDHospitalSchema.getbankAccNo();
		this.SatrapName = aLDHospitalSchema.getSatrapName();
		this.Linkman = aLDHospitalSchema.getLinkman();
		this.LastModiDate = fDate.getDate( aLDHospitalSchema.getLastModiDate());
		this.AreaName = aLDHospitalSchema.getAreaName();
		this.HospitalType = aLDHospitalSchema.getHospitalType();
		this.AssociateClass = aLDHospitalSchema.getAssociateClass();
		this.FlowNo = aLDHospitalSchema.getFlowNo();
		this.Operator = aLDHospitalSchema.getOperator();
		this.MakeDate = fDate.getDate( aLDHospitalSchema.getMakeDate());
		this.MakeTime = aLDHospitalSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLDHospitalSchema.getModifyDate());
		this.ModifyTime = aLDHospitalSchema.getModifyTime();
		this.HospitalStandardCode = aLDHospitalSchema.getHospitalStandardCode();
		this.PatientPerDay = aLDHospitalSchema.getPatientPerDay();
		this.OutHospital = aLDHospitalSchema.getOutHospital();
		this.SuperiorNo = aLDHospitalSchema.getSuperiorNo();
		this.InterNo = aLDHospitalSchema.getInterNo();
		this.ElementaryNo = aLDHospitalSchema.getElementaryNo();
		this.TotalNo = aLDHospitalSchema.getTotalNo();
		this.BedAmount = aLDHospitalSchema.getBedAmount();
		this.ProfAmount = aLDHospitalSchema.getProfAmount();
		this.DirectAmount = aLDHospitalSchema.getDirectAmount();
		this.AssistDirectAmount = aLDHospitalSchema.getAssistDirectAmount();
		this.MainDoctorAmount = aLDHospitalSchema.getMainDoctorAmount();
		this.ManageCom = aLDHospitalSchema.getManageCom();
		this.UrbanOption = aLDHospitalSchema.getUrbanOption();
		this.Route = aLDHospitalSchema.getRoute();
		this.SecurityNo = aLDHospitalSchema.getSecurityNo();
		this.Examination = aLDHospitalSchema.getExamination();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("HospitCode") == null )
				this.HospitCode = null;
			else
				this.HospitCode = rs.getString("HospitCode").trim();

			if( rs.getString("HospitName") == null )
				this.HospitName = null;
			else
				this.HospitName = rs.getString("HospitName").trim();

			if( rs.getString("FixFlag") == null )
				this.FixFlag = null;
			else
				this.FixFlag = rs.getString("FixFlag").trim();

			if( rs.getString("HospitShortName") == null )
				this.HospitShortName = null;
			else
				this.HospitShortName = rs.getString("HospitShortName").trim();

			if( rs.getString("CommunFixFlag") == null )
				this.CommunFixFlag = null;
			else
				this.CommunFixFlag = rs.getString("CommunFixFlag").trim();

			if( rs.getString("AreaCode") == null )
				this.AreaCode = null;
			else
				this.AreaCode = rs.getString("AreaCode").trim();

			if( rs.getString("LevelCode") == null )
				this.LevelCode = null;
			else
				this.LevelCode = rs.getString("LevelCode").trim();

			if( rs.getString("AdminiSortCode") == null )
				this.AdminiSortCode = null;
			else
				this.AdminiSortCode = rs.getString("AdminiSortCode").trim();

			if( rs.getString("BusiTypeCode") == null )
				this.BusiTypeCode = null;
			else
				this.BusiTypeCode = rs.getString("BusiTypeCode").trim();

			if( rs.getString("EconomElemenCode") == null )
				this.EconomElemenCode = null;
			else
				this.EconomElemenCode = rs.getString("EconomElemenCode").trim();

			if( rs.getString("Address") == null )
				this.Address = null;
			else
				this.Address = rs.getString("Address").trim();

			if( rs.getString("ZipCode") == null )
				this.ZipCode = null;
			else
				this.ZipCode = rs.getString("ZipCode").trim();

			if( rs.getString("Phone") == null )
				this.Phone = null;
			else
				this.Phone = rs.getString("Phone").trim();

			if( rs.getString("WebAddress") == null )
				this.WebAddress = null;
			else
				this.WebAddress = rs.getString("WebAddress").trim();

			if( rs.getString("Fax") == null )
				this.Fax = null;
			else
				this.Fax = rs.getString("Fax").trim();

			if( rs.getString("HospitLicencNo") == null )
				this.HospitLicencNo = null;
			else
				this.HospitLicencNo = rs.getString("HospitLicencNo").trim();

			if( rs.getString("bankCode") == null )
				this.bankCode = null;
			else
				this.bankCode = rs.getString("bankCode").trim();

			if( rs.getString("AccName") == null )
				this.AccName = null;
			else
				this.AccName = rs.getString("AccName").trim();

			if( rs.getString("bankAccNo") == null )
				this.bankAccNo = null;
			else
				this.bankAccNo = rs.getString("bankAccNo").trim();

			if( rs.getString("SatrapName") == null )
				this.SatrapName = null;
			else
				this.SatrapName = rs.getString("SatrapName").trim();

			if( rs.getString("Linkman") == null )
				this.Linkman = null;
			else
				this.Linkman = rs.getString("Linkman").trim();

			this.LastModiDate = rs.getDate("LastModiDate");
			if( rs.getString("AreaName") == null )
				this.AreaName = null;
			else
				this.AreaName = rs.getString("AreaName").trim();

			if( rs.getString("HospitalType") == null )
				this.HospitalType = null;
			else
				this.HospitalType = rs.getString("HospitalType").trim();

			if( rs.getString("AssociateClass") == null )
				this.AssociateClass = null;
			else
				this.AssociateClass = rs.getString("AssociateClass").trim();

			if( rs.getString("FlowNo") == null )
				this.FlowNo = null;
			else
				this.FlowNo = rs.getString("FlowNo").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("HospitalStandardCode") == null )
				this.HospitalStandardCode = null;
			else
				this.HospitalStandardCode = rs.getString("HospitalStandardCode").trim();

			if( rs.getString("PatientPerDay") == null )
				this.PatientPerDay = null;
			else
				this.PatientPerDay = rs.getString("PatientPerDay").trim();

			if( rs.getString("OutHospital") == null )
				this.OutHospital = null;
			else
				this.OutHospital = rs.getString("OutHospital").trim();

			if( rs.getString("SuperiorNo") == null )
				this.SuperiorNo = null;
			else
				this.SuperiorNo = rs.getString("SuperiorNo").trim();

			if( rs.getString("InterNo") == null )
				this.InterNo = null;
			else
				this.InterNo = rs.getString("InterNo").trim();

			if( rs.getString("ElementaryNo") == null )
				this.ElementaryNo = null;
			else
				this.ElementaryNo = rs.getString("ElementaryNo").trim();

			if( rs.getString("TotalNo") == null )
				this.TotalNo = null;
			else
				this.TotalNo = rs.getString("TotalNo").trim();

			if( rs.getString("BedAmount") == null )
				this.BedAmount = null;
			else
				this.BedAmount = rs.getString("BedAmount").trim();

			if( rs.getString("ProfAmount") == null )
				this.ProfAmount = null;
			else
				this.ProfAmount = rs.getString("ProfAmount").trim();

			if( rs.getString("DirectAmount") == null )
				this.DirectAmount = null;
			else
				this.DirectAmount = rs.getString("DirectAmount").trim();

			if( rs.getString("AssistDirectAmount") == null )
				this.AssistDirectAmount = null;
			else
				this.AssistDirectAmount = rs.getString("AssistDirectAmount").trim();

			if( rs.getString("MainDoctorAmount") == null )
				this.MainDoctorAmount = null;
			else
				this.MainDoctorAmount = rs.getString("MainDoctorAmount").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("UrbanOption") == null )
				this.UrbanOption = null;
			else
				this.UrbanOption = rs.getString("UrbanOption").trim();

			if( rs.getString("Route") == null )
				this.Route = null;
			else
				this.Route = rs.getString("Route").trim();

			if( rs.getString("SecurityNo") == null )
				this.SecurityNo = null;
			else
				this.SecurityNo = rs.getString("SecurityNo").trim();

			if( rs.getString("Examination") == null )
				this.Examination = null;
			else
				this.Examination = rs.getString("Examination").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LDHospital表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LDHospitalSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LDHospitalSchema getSchema()
	{
		LDHospitalSchema aLDHospitalSchema = new LDHospitalSchema();
		aLDHospitalSchema.setSchema(this);
		return aLDHospitalSchema;
	}

	public LDHospitalDB getDB()
	{
		LDHospitalDB aDBOper = new LDHospitalDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDHospital描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(HospitCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HospitName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FixFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HospitShortName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CommunFixFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AreaCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LevelCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AdminiSortCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusiTypeCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EconomElemenCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Address)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ZipCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Phone)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(WebAddress)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Fax)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HospitLicencNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bankCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bankAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SatrapName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Linkman)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( LastModiDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AreaName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HospitalType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AssociateClass)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FlowNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HospitalStandardCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PatientPerDay)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OutHospital)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SuperiorNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InterNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ElementaryNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TotalNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BedAmount)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProfAmount)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DirectAmount)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AssistDirectAmount)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MainDoctorAmount)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UrbanOption)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Route)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SecurityNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Examination));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDHospital>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			HospitCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			HospitName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			FixFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			HospitShortName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			CommunFixFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			AreaCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			LevelCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			AdminiSortCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			BusiTypeCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			EconomElemenCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			Address = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			ZipCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			Phone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			WebAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			Fax = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			HospitLicencNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			bankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			AccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			bankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			SatrapName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			Linkman = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			LastModiDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,SysConst.PACKAGESPILTER));
			AreaName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			HospitalType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			AssociateClass = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			FlowNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			HospitalStandardCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
			PatientPerDay = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			OutHospital = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			SuperiorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			InterNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
			ElementaryNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
			TotalNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
			BedAmount = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
			ProfAmount = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
			DirectAmount = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41, SysConst.PACKAGESPILTER );
			AssistDirectAmount = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
			MainDoctorAmount = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44, SysConst.PACKAGESPILTER );
			UrbanOption = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 45, SysConst.PACKAGESPILTER );
			Route = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 46, SysConst.PACKAGESPILTER );
			SecurityNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 47, SysConst.PACKAGESPILTER );
			Examination = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 48, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LDHospitalSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("HospitCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HospitCode));
		}
		if (FCode.equals("HospitName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HospitName));
		}
		if (FCode.equals("FixFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FixFlag));
		}
		if (FCode.equals("HospitShortName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HospitShortName));
		}
		if (FCode.equals("CommunFixFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CommunFixFlag));
		}
		if (FCode.equals("AreaCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AreaCode));
		}
		if (FCode.equals("LevelCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LevelCode));
		}
		if (FCode.equals("AdminiSortCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AdminiSortCode));
		}
		if (FCode.equals("BusiTypeCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusiTypeCode));
		}
		if (FCode.equals("EconomElemenCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EconomElemenCode));
		}
		if (FCode.equals("Address"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Address));
		}
		if (FCode.equals("ZipCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ZipCode));
		}
		if (FCode.equals("Phone"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Phone));
		}
		if (FCode.equals("WebAddress"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WebAddress));
		}
		if (FCode.equals("Fax"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Fax));
		}
		if (FCode.equals("HospitLicencNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HospitLicencNo));
		}
		if (FCode.equals("bankCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bankCode));
		}
		if (FCode.equals("AccName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccName));
		}
		if (FCode.equals("bankAccNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bankAccNo));
		}
		if (FCode.equals("SatrapName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SatrapName));
		}
		if (FCode.equals("Linkman"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Linkman));
		}
		if (FCode.equals("LastModiDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getLastModiDate()));
		}
		if (FCode.equals("AreaName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AreaName));
		}
		if (FCode.equals("HospitalType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HospitalType));
		}
		if (FCode.equals("AssociateClass"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AssociateClass));
		}
		if (FCode.equals("FlowNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FlowNo));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("HospitalStandardCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HospitalStandardCode));
		}
		if (FCode.equals("PatientPerDay"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PatientPerDay));
		}
		if (FCode.equals("OutHospital"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OutHospital));
		}
		if (FCode.equals("SuperiorNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SuperiorNo));
		}
		if (FCode.equals("InterNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InterNo));
		}
		if (FCode.equals("ElementaryNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ElementaryNo));
		}
		if (FCode.equals("TotalNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TotalNo));
		}
		if (FCode.equals("BedAmount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BedAmount));
		}
		if (FCode.equals("ProfAmount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProfAmount));
		}
		if (FCode.equals("DirectAmount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DirectAmount));
		}
		if (FCode.equals("AssistDirectAmount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AssistDirectAmount));
		}
		if (FCode.equals("MainDoctorAmount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MainDoctorAmount));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("UrbanOption"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UrbanOption));
		}
		if (FCode.equals("Route"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Route));
		}
		if (FCode.equals("SecurityNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SecurityNo));
		}
		if (FCode.equals("Examination"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Examination));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(HospitCode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(HospitName);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(FixFlag);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(HospitShortName);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(CommunFixFlag);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(AreaCode);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(LevelCode);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(AdminiSortCode);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(BusiTypeCode);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(EconomElemenCode);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(Address);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(ZipCode);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(Phone);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(WebAddress);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(Fax);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(HospitLicencNo);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(bankCode);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(AccName);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(bankAccNo);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(SatrapName);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(Linkman);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getLastModiDate()));
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(AreaName);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(HospitalType);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(AssociateClass);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(FlowNo);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(HospitalStandardCode);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(PatientPerDay);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(OutHospital);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(SuperiorNo);
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(InterNo);
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(ElementaryNo);
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(TotalNo);
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(BedAmount);
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(ProfAmount);
				break;
			case 40:
				strFieldValue = StrTool.GBKToUnicode(DirectAmount);
				break;
			case 41:
				strFieldValue = StrTool.GBKToUnicode(AssistDirectAmount);
				break;
			case 42:
				strFieldValue = StrTool.GBKToUnicode(MainDoctorAmount);
				break;
			case 43:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 44:
				strFieldValue = StrTool.GBKToUnicode(UrbanOption);
				break;
			case 45:
				strFieldValue = StrTool.GBKToUnicode(Route);
				break;
			case 46:
				strFieldValue = StrTool.GBKToUnicode(SecurityNo);
				break;
			case 47:
				strFieldValue = StrTool.GBKToUnicode(Examination);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("HospitCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HospitCode = FValue.trim();
			}
			else
				HospitCode = null;
		}
		if (FCode.equalsIgnoreCase("HospitName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HospitName = FValue.trim();
			}
			else
				HospitName = null;
		}
		if (FCode.equalsIgnoreCase("FixFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FixFlag = FValue.trim();
			}
			else
				FixFlag = null;
		}
		if (FCode.equalsIgnoreCase("HospitShortName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HospitShortName = FValue.trim();
			}
			else
				HospitShortName = null;
		}
		if (FCode.equalsIgnoreCase("CommunFixFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CommunFixFlag = FValue.trim();
			}
			else
				CommunFixFlag = null;
		}
		if (FCode.equalsIgnoreCase("AreaCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AreaCode = FValue.trim();
			}
			else
				AreaCode = null;
		}
		if (FCode.equalsIgnoreCase("LevelCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LevelCode = FValue.trim();
			}
			else
				LevelCode = null;
		}
		if (FCode.equalsIgnoreCase("AdminiSortCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AdminiSortCode = FValue.trim();
			}
			else
				AdminiSortCode = null;
		}
		if (FCode.equalsIgnoreCase("BusiTypeCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusiTypeCode = FValue.trim();
			}
			else
				BusiTypeCode = null;
		}
		if (FCode.equalsIgnoreCase("EconomElemenCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EconomElemenCode = FValue.trim();
			}
			else
				EconomElemenCode = null;
		}
		if (FCode.equalsIgnoreCase("Address"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Address = FValue.trim();
			}
			else
				Address = null;
		}
		if (FCode.equalsIgnoreCase("ZipCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ZipCode = FValue.trim();
			}
			else
				ZipCode = null;
		}
		if (FCode.equalsIgnoreCase("Phone"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Phone = FValue.trim();
			}
			else
				Phone = null;
		}
		if (FCode.equalsIgnoreCase("WebAddress"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WebAddress = FValue.trim();
			}
			else
				WebAddress = null;
		}
		if (FCode.equalsIgnoreCase("Fax"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Fax = FValue.trim();
			}
			else
				Fax = null;
		}
		if (FCode.equalsIgnoreCase("HospitLicencNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HospitLicencNo = FValue.trim();
			}
			else
				HospitLicencNo = null;
		}
		if (FCode.equalsIgnoreCase("bankCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bankCode = FValue.trim();
			}
			else
				bankCode = null;
		}
		if (FCode.equalsIgnoreCase("AccName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccName = FValue.trim();
			}
			else
				AccName = null;
		}
		if (FCode.equalsIgnoreCase("bankAccNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bankAccNo = FValue.trim();
			}
			else
				bankAccNo = null;
		}
		if (FCode.equalsIgnoreCase("SatrapName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SatrapName = FValue.trim();
			}
			else
				SatrapName = null;
		}
		if (FCode.equalsIgnoreCase("Linkman"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Linkman = FValue.trim();
			}
			else
				Linkman = null;
		}
		if (FCode.equalsIgnoreCase("LastModiDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				LastModiDate = fDate.getDate( FValue );
			}
			else
				LastModiDate = null;
		}
		if (FCode.equalsIgnoreCase("AreaName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AreaName = FValue.trim();
			}
			else
				AreaName = null;
		}
		if (FCode.equalsIgnoreCase("HospitalType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HospitalType = FValue.trim();
			}
			else
				HospitalType = null;
		}
		if (FCode.equalsIgnoreCase("AssociateClass"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AssociateClass = FValue.trim();
			}
			else
				AssociateClass = null;
		}
		if (FCode.equalsIgnoreCase("FlowNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FlowNo = FValue.trim();
			}
			else
				FlowNo = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("HospitalStandardCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HospitalStandardCode = FValue.trim();
			}
			else
				HospitalStandardCode = null;
		}
		if (FCode.equalsIgnoreCase("PatientPerDay"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PatientPerDay = FValue.trim();
			}
			else
				PatientPerDay = null;
		}
		if (FCode.equalsIgnoreCase("OutHospital"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OutHospital = FValue.trim();
			}
			else
				OutHospital = null;
		}
		if (FCode.equalsIgnoreCase("SuperiorNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SuperiorNo = FValue.trim();
			}
			else
				SuperiorNo = null;
		}
		if (FCode.equalsIgnoreCase("InterNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InterNo = FValue.trim();
			}
			else
				InterNo = null;
		}
		if (FCode.equalsIgnoreCase("ElementaryNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ElementaryNo = FValue.trim();
			}
			else
				ElementaryNo = null;
		}
		if (FCode.equalsIgnoreCase("TotalNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TotalNo = FValue.trim();
			}
			else
				TotalNo = null;
		}
		if (FCode.equalsIgnoreCase("BedAmount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BedAmount = FValue.trim();
			}
			else
				BedAmount = null;
		}
		if (FCode.equalsIgnoreCase("ProfAmount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProfAmount = FValue.trim();
			}
			else
				ProfAmount = null;
		}
		if (FCode.equalsIgnoreCase("DirectAmount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DirectAmount = FValue.trim();
			}
			else
				DirectAmount = null;
		}
		if (FCode.equalsIgnoreCase("AssistDirectAmount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AssistDirectAmount = FValue.trim();
			}
			else
				AssistDirectAmount = null;
		}
		if (FCode.equalsIgnoreCase("MainDoctorAmount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MainDoctorAmount = FValue.trim();
			}
			else
				MainDoctorAmount = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("UrbanOption"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UrbanOption = FValue.trim();
			}
			else
				UrbanOption = null;
		}
		if (FCode.equalsIgnoreCase("Route"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Route = FValue.trim();
			}
			else
				Route = null;
		}
		if (FCode.equalsIgnoreCase("SecurityNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SecurityNo = FValue.trim();
			}
			else
				SecurityNo = null;
		}
		if (FCode.equalsIgnoreCase("Examination"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Examination = FValue.trim();
			}
			else
				Examination = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LDHospitalSchema other = (LDHospitalSchema)otherObject;
		return
			(HospitCode == null ? other.getHospitCode() == null : HospitCode.equals(other.getHospitCode()))
			&& (HospitName == null ? other.getHospitName() == null : HospitName.equals(other.getHospitName()))
			&& (FixFlag == null ? other.getFixFlag() == null : FixFlag.equals(other.getFixFlag()))
			&& (HospitShortName == null ? other.getHospitShortName() == null : HospitShortName.equals(other.getHospitShortName()))
			&& (CommunFixFlag == null ? other.getCommunFixFlag() == null : CommunFixFlag.equals(other.getCommunFixFlag()))
			&& (AreaCode == null ? other.getAreaCode() == null : AreaCode.equals(other.getAreaCode()))
			&& (LevelCode == null ? other.getLevelCode() == null : LevelCode.equals(other.getLevelCode()))
			&& (AdminiSortCode == null ? other.getAdminiSortCode() == null : AdminiSortCode.equals(other.getAdminiSortCode()))
			&& (BusiTypeCode == null ? other.getBusiTypeCode() == null : BusiTypeCode.equals(other.getBusiTypeCode()))
			&& (EconomElemenCode == null ? other.getEconomElemenCode() == null : EconomElemenCode.equals(other.getEconomElemenCode()))
			&& (Address == null ? other.getAddress() == null : Address.equals(other.getAddress()))
			&& (ZipCode == null ? other.getZipCode() == null : ZipCode.equals(other.getZipCode()))
			&& (Phone == null ? other.getPhone() == null : Phone.equals(other.getPhone()))
			&& (WebAddress == null ? other.getWebAddress() == null : WebAddress.equals(other.getWebAddress()))
			&& (Fax == null ? other.getFax() == null : Fax.equals(other.getFax()))
			&& (HospitLicencNo == null ? other.getHospitLicencNo() == null : HospitLicencNo.equals(other.getHospitLicencNo()))
			&& (bankCode == null ? other.getbankCode() == null : bankCode.equals(other.getbankCode()))
			&& (AccName == null ? other.getAccName() == null : AccName.equals(other.getAccName()))
			&& (bankAccNo == null ? other.getbankAccNo() == null : bankAccNo.equals(other.getbankAccNo()))
			&& (SatrapName == null ? other.getSatrapName() == null : SatrapName.equals(other.getSatrapName()))
			&& (Linkman == null ? other.getLinkman() == null : Linkman.equals(other.getLinkman()))
			&& (LastModiDate == null ? other.getLastModiDate() == null : fDate.getString(LastModiDate).equals(other.getLastModiDate()))
			&& (AreaName == null ? other.getAreaName() == null : AreaName.equals(other.getAreaName()))
			&& (HospitalType == null ? other.getHospitalType() == null : HospitalType.equals(other.getHospitalType()))
			&& (AssociateClass == null ? other.getAssociateClass() == null : AssociateClass.equals(other.getAssociateClass()))
			&& (FlowNo == null ? other.getFlowNo() == null : FlowNo.equals(other.getFlowNo()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (HospitalStandardCode == null ? other.getHospitalStandardCode() == null : HospitalStandardCode.equals(other.getHospitalStandardCode()))
			&& (PatientPerDay == null ? other.getPatientPerDay() == null : PatientPerDay.equals(other.getPatientPerDay()))
			&& (OutHospital == null ? other.getOutHospital() == null : OutHospital.equals(other.getOutHospital()))
			&& (SuperiorNo == null ? other.getSuperiorNo() == null : SuperiorNo.equals(other.getSuperiorNo()))
			&& (InterNo == null ? other.getInterNo() == null : InterNo.equals(other.getInterNo()))
			&& (ElementaryNo == null ? other.getElementaryNo() == null : ElementaryNo.equals(other.getElementaryNo()))
			&& (TotalNo == null ? other.getTotalNo() == null : TotalNo.equals(other.getTotalNo()))
			&& (BedAmount == null ? other.getBedAmount() == null : BedAmount.equals(other.getBedAmount()))
			&& (ProfAmount == null ? other.getProfAmount() == null : ProfAmount.equals(other.getProfAmount()))
			&& (DirectAmount == null ? other.getDirectAmount() == null : DirectAmount.equals(other.getDirectAmount()))
			&& (AssistDirectAmount == null ? other.getAssistDirectAmount() == null : AssistDirectAmount.equals(other.getAssistDirectAmount()))
			&& (MainDoctorAmount == null ? other.getMainDoctorAmount() == null : MainDoctorAmount.equals(other.getMainDoctorAmount()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (UrbanOption == null ? other.getUrbanOption() == null : UrbanOption.equals(other.getUrbanOption()))
			&& (Route == null ? other.getRoute() == null : Route.equals(other.getRoute()))
			&& (SecurityNo == null ? other.getSecurityNo() == null : SecurityNo.equals(other.getSecurityNo()))
			&& (Examination == null ? other.getExamination() == null : Examination.equals(other.getExamination()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("HospitCode") ) {
			return 0;
		}
		if( strFieldName.equals("HospitName") ) {
			return 1;
		}
		if( strFieldName.equals("FixFlag") ) {
			return 2;
		}
		if( strFieldName.equals("HospitShortName") ) {
			return 3;
		}
		if( strFieldName.equals("CommunFixFlag") ) {
			return 4;
		}
		if( strFieldName.equals("AreaCode") ) {
			return 5;
		}
		if( strFieldName.equals("LevelCode") ) {
			return 6;
		}
		if( strFieldName.equals("AdminiSortCode") ) {
			return 7;
		}
		if( strFieldName.equals("BusiTypeCode") ) {
			return 8;
		}
		if( strFieldName.equals("EconomElemenCode") ) {
			return 9;
		}
		if( strFieldName.equals("Address") ) {
			return 10;
		}
		if( strFieldName.equals("ZipCode") ) {
			return 11;
		}
		if( strFieldName.equals("Phone") ) {
			return 12;
		}
		if( strFieldName.equals("WebAddress") ) {
			return 13;
		}
		if( strFieldName.equals("Fax") ) {
			return 14;
		}
		if( strFieldName.equals("HospitLicencNo") ) {
			return 15;
		}
		if( strFieldName.equals("bankCode") ) {
			return 16;
		}
		if( strFieldName.equals("AccName") ) {
			return 17;
		}
		if( strFieldName.equals("bankAccNo") ) {
			return 18;
		}
		if( strFieldName.equals("SatrapName") ) {
			return 19;
		}
		if( strFieldName.equals("Linkman") ) {
			return 20;
		}
		if( strFieldName.equals("LastModiDate") ) {
			return 21;
		}
		if( strFieldName.equals("AreaName") ) {
			return 22;
		}
		if( strFieldName.equals("HospitalType") ) {
			return 23;
		}
		if( strFieldName.equals("AssociateClass") ) {
			return 24;
		}
		if( strFieldName.equals("FlowNo") ) {
			return 25;
		}
		if( strFieldName.equals("Operator") ) {
			return 26;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 27;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 28;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 29;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 30;
		}
		if( strFieldName.equals("HospitalStandardCode") ) {
			return 31;
		}
		if( strFieldName.equals("PatientPerDay") ) {
			return 32;
		}
		if( strFieldName.equals("OutHospital") ) {
			return 33;
		}
		if( strFieldName.equals("SuperiorNo") ) {
			return 34;
		}
		if( strFieldName.equals("InterNo") ) {
			return 35;
		}
		if( strFieldName.equals("ElementaryNo") ) {
			return 36;
		}
		if( strFieldName.equals("TotalNo") ) {
			return 37;
		}
		if( strFieldName.equals("BedAmount") ) {
			return 38;
		}
		if( strFieldName.equals("ProfAmount") ) {
			return 39;
		}
		if( strFieldName.equals("DirectAmount") ) {
			return 40;
		}
		if( strFieldName.equals("AssistDirectAmount") ) {
			return 41;
		}
		if( strFieldName.equals("MainDoctorAmount") ) {
			return 42;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 43;
		}
		if( strFieldName.equals("UrbanOption") ) {
			return 44;
		}
		if( strFieldName.equals("Route") ) {
			return 45;
		}
		if( strFieldName.equals("SecurityNo") ) {
			return 46;
		}
		if( strFieldName.equals("Examination") ) {
			return 47;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "HospitCode";
				break;
			case 1:
				strFieldName = "HospitName";
				break;
			case 2:
				strFieldName = "FixFlag";
				break;
			case 3:
				strFieldName = "HospitShortName";
				break;
			case 4:
				strFieldName = "CommunFixFlag";
				break;
			case 5:
				strFieldName = "AreaCode";
				break;
			case 6:
				strFieldName = "LevelCode";
				break;
			case 7:
				strFieldName = "AdminiSortCode";
				break;
			case 8:
				strFieldName = "BusiTypeCode";
				break;
			case 9:
				strFieldName = "EconomElemenCode";
				break;
			case 10:
				strFieldName = "Address";
				break;
			case 11:
				strFieldName = "ZipCode";
				break;
			case 12:
				strFieldName = "Phone";
				break;
			case 13:
				strFieldName = "WebAddress";
				break;
			case 14:
				strFieldName = "Fax";
				break;
			case 15:
				strFieldName = "HospitLicencNo";
				break;
			case 16:
				strFieldName = "bankCode";
				break;
			case 17:
				strFieldName = "AccName";
				break;
			case 18:
				strFieldName = "bankAccNo";
				break;
			case 19:
				strFieldName = "SatrapName";
				break;
			case 20:
				strFieldName = "Linkman";
				break;
			case 21:
				strFieldName = "LastModiDate";
				break;
			case 22:
				strFieldName = "AreaName";
				break;
			case 23:
				strFieldName = "HospitalType";
				break;
			case 24:
				strFieldName = "AssociateClass";
				break;
			case 25:
				strFieldName = "FlowNo";
				break;
			case 26:
				strFieldName = "Operator";
				break;
			case 27:
				strFieldName = "MakeDate";
				break;
			case 28:
				strFieldName = "MakeTime";
				break;
			case 29:
				strFieldName = "ModifyDate";
				break;
			case 30:
				strFieldName = "ModifyTime";
				break;
			case 31:
				strFieldName = "HospitalStandardCode";
				break;
			case 32:
				strFieldName = "PatientPerDay";
				break;
			case 33:
				strFieldName = "OutHospital";
				break;
			case 34:
				strFieldName = "SuperiorNo";
				break;
			case 35:
				strFieldName = "InterNo";
				break;
			case 36:
				strFieldName = "ElementaryNo";
				break;
			case 37:
				strFieldName = "TotalNo";
				break;
			case 38:
				strFieldName = "BedAmount";
				break;
			case 39:
				strFieldName = "ProfAmount";
				break;
			case 40:
				strFieldName = "DirectAmount";
				break;
			case 41:
				strFieldName = "AssistDirectAmount";
				break;
			case 42:
				strFieldName = "MainDoctorAmount";
				break;
			case 43:
				strFieldName = "ManageCom";
				break;
			case 44:
				strFieldName = "UrbanOption";
				break;
			case 45:
				strFieldName = "Route";
				break;
			case 46:
				strFieldName = "SecurityNo";
				break;
			case 47:
				strFieldName = "Examination";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("HospitCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HospitName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FixFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HospitShortName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CommunFixFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AreaCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LevelCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AdminiSortCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BusiTypeCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EconomElemenCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Address") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ZipCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Phone") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("WebAddress") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Fax") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HospitLicencNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bankCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bankAccNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SatrapName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Linkman") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LastModiDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("AreaName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HospitalType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AssociateClass") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FlowNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HospitalStandardCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PatientPerDay") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OutHospital") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SuperiorNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InterNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ElementaryNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TotalNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BedAmount") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ProfAmount") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DirectAmount") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AssistDirectAmount") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MainDoctorAmount") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UrbanOption") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Route") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SecurityNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Examination") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 36:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 37:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 38:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 39:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 40:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 41:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 42:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 43:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 44:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 45:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 46:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 47:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
