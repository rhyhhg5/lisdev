/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.ES_DOC_QC_TRACEDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: ES_DOC_QC_TRACESchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-01-04
 */
public class ES_DOC_QC_TRACESchema implements Schema, Cloneable {
    // @Field
    /** 轨迹编号 */
    private double TraceID;
    /** 单证编号 */
    private double DocID;
    /** 单证号码 */
    private String DocCode;
    /** 单证类型 */
    private String BussType;
    /** 单证细类 */
    private String SubType;
    /** 扫描操作员 */
    private String ScanOperator;
    /** 扫描员管理机构 */
    private String ScanManageCom;
    /** 扫描员最后回复日期 */
    private Date ScanLastApplyDate;
    /** 扫描员最后回复时间 */
    private String ScanLastApplyTime;
    /** 质检操作员 */
    private String QCOperator;
    /** 质检员管理机构 */
    private String QCManageCom;
    /** 质检员最后回复日期 */
    private Date QCLastApplyDate;
    /** 质检员最后回复时间 */
    private String QCLastApplyTime;
    /** 质检标记 */
    private String QCFlag;
    /** 质检员建议操作 */
    private String QCSuggest;
    /** 质检未通过原因 */
    private String UnpassReason;
    /** 扫描员操作标记 */
    private String ScanOpeState;
    /** 生成日期 */
    private Date MakeDate;
    /** 生成时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 21; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public ES_DOC_QC_TRACESchema() {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "TraceID";
        pk[1] = "DocID";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        ES_DOC_QC_TRACESchema cloned = (ES_DOC_QC_TRACESchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public double getTraceID() {
        return TraceID;
    }

    public void setTraceID(double aTraceID) {
        TraceID = aTraceID;
    }

    public void setTraceID(String aTraceID) {
        if (aTraceID != null && !aTraceID.equals("")) {
            Double tDouble = new Double(aTraceID);
            double d = tDouble.doubleValue();
            TraceID = d;
        }
    }

    public double getDocID() {
        return DocID;
    }

    public void setDocID(double aDocID) {
        DocID = aDocID;
    }

    public void setDocID(String aDocID) {
        if (aDocID != null && !aDocID.equals("")) {
            Double tDouble = new Double(aDocID);
            double d = tDouble.doubleValue();
            DocID = d;
        }
    }

    public String getDocCode() {
        return DocCode;
    }

    public void setDocCode(String aDocCode) {
        DocCode = aDocCode;
    }

    public String getBussType() {
        return BussType;
    }

    public void setBussType(String aBussType) {
        BussType = aBussType;
    }

    public String getSubType() {
        return SubType;
    }

    public void setSubType(String aSubType) {
        SubType = aSubType;
    }

    public String getScanOperator() {
        return ScanOperator;
    }

    public void setScanOperator(String aScanOperator) {
        ScanOperator = aScanOperator;
    }

    public String getScanManageCom() {
        return ScanManageCom;
    }

    public void setScanManageCom(String aScanManageCom) {
        ScanManageCom = aScanManageCom;
    }

    public String getScanLastApplyDate() {
        if (ScanLastApplyDate != null) {
            return fDate.getString(ScanLastApplyDate);
        } else {
            return null;
        }
    }

    public void setScanLastApplyDate(Date aScanLastApplyDate) {
        ScanLastApplyDate = aScanLastApplyDate;
    }

    public void setScanLastApplyDate(String aScanLastApplyDate) {
        if (aScanLastApplyDate != null && !aScanLastApplyDate.equals("")) {
            ScanLastApplyDate = fDate.getDate(aScanLastApplyDate);
        } else {
            ScanLastApplyDate = null;
        }
    }

    public String getScanLastApplyTime() {
        return ScanLastApplyTime;
    }

    public void setScanLastApplyTime(String aScanLastApplyTime) {
        ScanLastApplyTime = aScanLastApplyTime;
    }

    public String getQCOperator() {
        return QCOperator;
    }

    public void setQCOperator(String aQCOperator) {
        QCOperator = aQCOperator;
    }

    public String getQCManageCom() {
        return QCManageCom;
    }

    public void setQCManageCom(String aQCManageCom) {
        QCManageCom = aQCManageCom;
    }

    public String getQCLastApplyDate() {
        if (QCLastApplyDate != null) {
            return fDate.getString(QCLastApplyDate);
        } else {
            return null;
        }
    }

    public void setQCLastApplyDate(Date aQCLastApplyDate) {
        QCLastApplyDate = aQCLastApplyDate;
    }

    public void setQCLastApplyDate(String aQCLastApplyDate) {
        if (aQCLastApplyDate != null && !aQCLastApplyDate.equals("")) {
            QCLastApplyDate = fDate.getDate(aQCLastApplyDate);
        } else {
            QCLastApplyDate = null;
        }
    }

    public String getQCLastApplyTime() {
        return QCLastApplyTime;
    }

    public void setQCLastApplyTime(String aQCLastApplyTime) {
        QCLastApplyTime = aQCLastApplyTime;
    }

    public String getQCFlag() {
        return QCFlag;
    }

    public void setQCFlag(String aQCFlag) {
        QCFlag = aQCFlag;
    }

    public String getQCSuggest() {
        return QCSuggest;
    }

    public void setQCSuggest(String aQCSuggest) {
        QCSuggest = aQCSuggest;
    }

    public String getUnpassReason() {
        return UnpassReason;
    }

    public void setUnpassReason(String aUnpassReason) {
        UnpassReason = aUnpassReason;
    }

    public String getScanOpeState() {
        return ScanOpeState;
    }

    public void setScanOpeState(String aScanOpeState) {
        ScanOpeState = aScanOpeState;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 ES_DOC_QC_TRACESchema 对象给 Schema 赋值
     * @param: aES_DOC_QC_TRACESchema ES_DOC_QC_TRACESchema
     **/
    public void setSchema(ES_DOC_QC_TRACESchema aES_DOC_QC_TRACESchema) {
        this.TraceID = aES_DOC_QC_TRACESchema.getTraceID();
        this.DocID = aES_DOC_QC_TRACESchema.getDocID();
        this.DocCode = aES_DOC_QC_TRACESchema.getDocCode();
        this.BussType = aES_DOC_QC_TRACESchema.getBussType();
        this.SubType = aES_DOC_QC_TRACESchema.getSubType();
        this.ScanOperator = aES_DOC_QC_TRACESchema.getScanOperator();
        this.ScanManageCom = aES_DOC_QC_TRACESchema.getScanManageCom();
        this.ScanLastApplyDate = fDate.getDate(aES_DOC_QC_TRACESchema.
                                               getScanLastApplyDate());
        this.ScanLastApplyTime = aES_DOC_QC_TRACESchema.getScanLastApplyTime();
        this.QCOperator = aES_DOC_QC_TRACESchema.getQCOperator();
        this.QCManageCom = aES_DOC_QC_TRACESchema.getQCManageCom();
        this.QCLastApplyDate = fDate.getDate(aES_DOC_QC_TRACESchema.
                                             getQCLastApplyDate());
        this.QCLastApplyTime = aES_DOC_QC_TRACESchema.getQCLastApplyTime();
        this.QCFlag = aES_DOC_QC_TRACESchema.getQCFlag();
        this.QCSuggest = aES_DOC_QC_TRACESchema.getQCSuggest();
        this.UnpassReason = aES_DOC_QC_TRACESchema.getUnpassReason();
        this.ScanOpeState = aES_DOC_QC_TRACESchema.getScanOpeState();
        this.MakeDate = fDate.getDate(aES_DOC_QC_TRACESchema.getMakeDate());
        this.MakeTime = aES_DOC_QC_TRACESchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aES_DOC_QC_TRACESchema.getModifyDate());
        this.ModifyTime = aES_DOC_QC_TRACESchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            this.TraceID = rs.getDouble("TraceID");
            this.DocID = rs.getDouble("DocID");
            if (rs.getString("DocCode") == null) {
                this.DocCode = null;
            } else {
                this.DocCode = rs.getString("DocCode").trim();
            }

            if (rs.getString("BussType") == null) {
                this.BussType = null;
            } else {
                this.BussType = rs.getString("BussType").trim();
            }

            if (rs.getString("SubType") == null) {
                this.SubType = null;
            } else {
                this.SubType = rs.getString("SubType").trim();
            }

            if (rs.getString("ScanOperator") == null) {
                this.ScanOperator = null;
            } else {
                this.ScanOperator = rs.getString("ScanOperator").trim();
            }

            if (rs.getString("ScanManageCom") == null) {
                this.ScanManageCom = null;
            } else {
                this.ScanManageCom = rs.getString("ScanManageCom").trim();
            }

            this.ScanLastApplyDate = rs.getDate("ScanLastApplyDate");
            if (rs.getString("ScanLastApplyTime") == null) {
                this.ScanLastApplyTime = null;
            } else {
                this.ScanLastApplyTime = rs.getString("ScanLastApplyTime").trim();
            }

            if (rs.getString("QCOperator") == null) {
                this.QCOperator = null;
            } else {
                this.QCOperator = rs.getString("QCOperator").trim();
            }

            if (rs.getString("QCManageCom") == null) {
                this.QCManageCom = null;
            } else {
                this.QCManageCom = rs.getString("QCManageCom").trim();
            }

            this.QCLastApplyDate = rs.getDate("QCLastApplyDate");
            if (rs.getString("QCLastApplyTime") == null) {
                this.QCLastApplyTime = null;
            } else {
                this.QCLastApplyTime = rs.getString("QCLastApplyTime").trim();
            }

            if (rs.getString("QCFlag") == null) {
                this.QCFlag = null;
            } else {
                this.QCFlag = rs.getString("QCFlag").trim();
            }

            if (rs.getString("QCSuggest") == null) {
                this.QCSuggest = null;
            } else {
                this.QCSuggest = rs.getString("QCSuggest").trim();
            }

            if (rs.getString("UnpassReason") == null) {
                this.UnpassReason = null;
            } else {
                this.UnpassReason = rs.getString("UnpassReason").trim();
            }

            if (rs.getString("ScanOpeState") == null) {
                this.ScanOpeState = null;
            } else {
                this.ScanOpeState = rs.getString("ScanOpeState").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的ES_DOC_QC_TRACE表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ES_DOC_QC_TRACESchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public ES_DOC_QC_TRACESchema getSchema() {
        ES_DOC_QC_TRACESchema aES_DOC_QC_TRACESchema = new
                ES_DOC_QC_TRACESchema();
        aES_DOC_QC_TRACESchema.setSchema(this);
        return aES_DOC_QC_TRACESchema;
    }

    public ES_DOC_QC_TRACEDB getDB() {
        ES_DOC_QC_TRACEDB aDBOper = new ES_DOC_QC_TRACEDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpES_DOC_QC_TRACE描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(ChgData.chgData(TraceID));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DocID));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DocCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BussType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SubType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ScanOperator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ScanManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ScanLastApplyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ScanLastApplyTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(QCOperator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(QCManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(QCLastApplyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(QCLastApplyTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(QCFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(QCSuggest));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UnpassReason));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ScanOpeState));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpES_DOC_QC_TRACE>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            TraceID = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 1, SysConst.PACKAGESPILTER))).doubleValue();
            DocID = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    2, SysConst.PACKAGESPILTER))).doubleValue();
            DocCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                     SysConst.PACKAGESPILTER);
            BussType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                      SysConst.PACKAGESPILTER);
            SubType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                     SysConst.PACKAGESPILTER);
            ScanOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                          SysConst.PACKAGESPILTER);
            ScanManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                           SysConst.PACKAGESPILTER);
            ScanLastApplyDate = fDate.getDate(StrTool.getStr(StrTool.
                    GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER));
            ScanLastApplyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                               9, SysConst.PACKAGESPILTER);
            QCOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                        SysConst.PACKAGESPILTER);
            QCManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                         SysConst.PACKAGESPILTER);
            QCLastApplyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 12, SysConst.PACKAGESPILTER));
            QCLastApplyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             13, SysConst.PACKAGESPILTER);
            QCFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                    SysConst.PACKAGESPILTER);
            QCSuggest = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                       SysConst.PACKAGESPILTER);
            UnpassReason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                          SysConst.PACKAGESPILTER);
            ScanOpeState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                          SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 18, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 20, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,
                                        SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ES_DOC_QC_TRACESchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("TraceID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TraceID));
        }
        if (FCode.equalsIgnoreCase("DocID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DocID));
        }
        if (FCode.equalsIgnoreCase("DocCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DocCode));
        }
        if (FCode.equalsIgnoreCase("BussType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BussType));
        }
        if (FCode.equalsIgnoreCase("SubType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SubType));
        }
        if (FCode.equalsIgnoreCase("ScanOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ScanOperator));
        }
        if (FCode.equalsIgnoreCase("ScanManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ScanManageCom));
        }
        if (FCode.equalsIgnoreCase("ScanLastApplyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getScanLastApplyDate()));
        }
        if (FCode.equalsIgnoreCase("ScanLastApplyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ScanLastApplyTime));
        }
        if (FCode.equalsIgnoreCase("QCOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(QCOperator));
        }
        if (FCode.equalsIgnoreCase("QCManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(QCManageCom));
        }
        if (FCode.equalsIgnoreCase("QCLastApplyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getQCLastApplyDate()));
        }
        if (FCode.equalsIgnoreCase("QCLastApplyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(QCLastApplyTime));
        }
        if (FCode.equalsIgnoreCase("QCFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(QCFlag));
        }
        if (FCode.equalsIgnoreCase("QCSuggest")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(QCSuggest));
        }
        if (FCode.equalsIgnoreCase("UnpassReason")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UnpassReason));
        }
        if (FCode.equalsIgnoreCase("ScanOpeState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ScanOpeState));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = String.valueOf(TraceID);
            break;
        case 1:
            strFieldValue = String.valueOf(DocID);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(DocCode);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(BussType);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(SubType);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(ScanOperator);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(ScanManageCom);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getScanLastApplyDate()));
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(ScanLastApplyTime);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(QCOperator);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(QCManageCom);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getQCLastApplyDate()));
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(QCLastApplyTime);
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(QCFlag);
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(QCSuggest);
            break;
        case 15:
            strFieldValue = StrTool.GBKToUnicode(UnpassReason);
            break;
        case 16:
            strFieldValue = StrTool.GBKToUnicode(ScanOpeState);
            break;
        case 17:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 18:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 19:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 20:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("TraceID")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                TraceID = d;
            }
        }
        if (FCode.equalsIgnoreCase("DocID")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                DocID = d;
            }
        }
        if (FCode.equalsIgnoreCase("DocCode")) {
            if (FValue != null && !FValue.equals("")) {
                DocCode = FValue.trim();
            } else {
                DocCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("BussType")) {
            if (FValue != null && !FValue.equals("")) {
                BussType = FValue.trim();
            } else {
                BussType = null;
            }
        }
        if (FCode.equalsIgnoreCase("SubType")) {
            if (FValue != null && !FValue.equals("")) {
                SubType = FValue.trim();
            } else {
                SubType = null;
            }
        }
        if (FCode.equalsIgnoreCase("ScanOperator")) {
            if (FValue != null && !FValue.equals("")) {
                ScanOperator = FValue.trim();
            } else {
                ScanOperator = null;
            }
        }
        if (FCode.equalsIgnoreCase("ScanManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ScanManageCom = FValue.trim();
            } else {
                ScanManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("ScanLastApplyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ScanLastApplyDate = fDate.getDate(FValue);
            } else {
                ScanLastApplyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ScanLastApplyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ScanLastApplyTime = FValue.trim();
            } else {
                ScanLastApplyTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("QCOperator")) {
            if (FValue != null && !FValue.equals("")) {
                QCOperator = FValue.trim();
            } else {
                QCOperator = null;
            }
        }
        if (FCode.equalsIgnoreCase("QCManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                QCManageCom = FValue.trim();
            } else {
                QCManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("QCLastApplyDate")) {
            if (FValue != null && !FValue.equals("")) {
                QCLastApplyDate = fDate.getDate(FValue);
            } else {
                QCLastApplyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("QCLastApplyTime")) {
            if (FValue != null && !FValue.equals("")) {
                QCLastApplyTime = FValue.trim();
            } else {
                QCLastApplyTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("QCFlag")) {
            if (FValue != null && !FValue.equals("")) {
                QCFlag = FValue.trim();
            } else {
                QCFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("QCSuggest")) {
            if (FValue != null && !FValue.equals("")) {
                QCSuggest = FValue.trim();
            } else {
                QCSuggest = null;
            }
        }
        if (FCode.equalsIgnoreCase("UnpassReason")) {
            if (FValue != null && !FValue.equals("")) {
                UnpassReason = FValue.trim();
            } else {
                UnpassReason = null;
            }
        }
        if (FCode.equalsIgnoreCase("ScanOpeState")) {
            if (FValue != null && !FValue.equals("")) {
                ScanOpeState = FValue.trim();
            } else {
                ScanOpeState = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        ES_DOC_QC_TRACESchema other = (ES_DOC_QC_TRACESchema) otherObject;
        return
                TraceID == other.getTraceID()
                && DocID == other.getDocID()
                && DocCode.equals(other.getDocCode())
                && BussType.equals(other.getBussType())
                && SubType.equals(other.getSubType())
                && ScanOperator.equals(other.getScanOperator())
                && ScanManageCom.equals(other.getScanManageCom())
                &&
                fDate.getString(ScanLastApplyDate).equals(other.
                getScanLastApplyDate())
                && ScanLastApplyTime.equals(other.getScanLastApplyTime())
                && QCOperator.equals(other.getQCOperator())
                && QCManageCom.equals(other.getQCManageCom())
                &&
                fDate.getString(QCLastApplyDate).equals(other.
                getQCLastApplyDate())
                && QCLastApplyTime.equals(other.getQCLastApplyTime())
                && QCFlag.equals(other.getQCFlag())
                && QCSuggest.equals(other.getQCSuggest())
                && UnpassReason.equals(other.getUnpassReason())
                && ScanOpeState.equals(other.getScanOpeState())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("TraceID")) {
            return 0;
        }
        if (strFieldName.equals("DocID")) {
            return 1;
        }
        if (strFieldName.equals("DocCode")) {
            return 2;
        }
        if (strFieldName.equals("BussType")) {
            return 3;
        }
        if (strFieldName.equals("SubType")) {
            return 4;
        }
        if (strFieldName.equals("ScanOperator")) {
            return 5;
        }
        if (strFieldName.equals("ScanManageCom")) {
            return 6;
        }
        if (strFieldName.equals("ScanLastApplyDate")) {
            return 7;
        }
        if (strFieldName.equals("ScanLastApplyTime")) {
            return 8;
        }
        if (strFieldName.equals("QCOperator")) {
            return 9;
        }
        if (strFieldName.equals("QCManageCom")) {
            return 10;
        }
        if (strFieldName.equals("QCLastApplyDate")) {
            return 11;
        }
        if (strFieldName.equals("QCLastApplyTime")) {
            return 12;
        }
        if (strFieldName.equals("QCFlag")) {
            return 13;
        }
        if (strFieldName.equals("QCSuggest")) {
            return 14;
        }
        if (strFieldName.equals("UnpassReason")) {
            return 15;
        }
        if (strFieldName.equals("ScanOpeState")) {
            return 16;
        }
        if (strFieldName.equals("MakeDate")) {
            return 17;
        }
        if (strFieldName.equals("MakeTime")) {
            return 18;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 19;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 20;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "TraceID";
            break;
        case 1:
            strFieldName = "DocID";
            break;
        case 2:
            strFieldName = "DocCode";
            break;
        case 3:
            strFieldName = "BussType";
            break;
        case 4:
            strFieldName = "SubType";
            break;
        case 5:
            strFieldName = "ScanOperator";
            break;
        case 6:
            strFieldName = "ScanManageCom";
            break;
        case 7:
            strFieldName = "ScanLastApplyDate";
            break;
        case 8:
            strFieldName = "ScanLastApplyTime";
            break;
        case 9:
            strFieldName = "QCOperator";
            break;
        case 10:
            strFieldName = "QCManageCom";
            break;
        case 11:
            strFieldName = "QCLastApplyDate";
            break;
        case 12:
            strFieldName = "QCLastApplyTime";
            break;
        case 13:
            strFieldName = "QCFlag";
            break;
        case 14:
            strFieldName = "QCSuggest";
            break;
        case 15:
            strFieldName = "UnpassReason";
            break;
        case 16:
            strFieldName = "ScanOpeState";
            break;
        case 17:
            strFieldName = "MakeDate";
            break;
        case 18:
            strFieldName = "MakeTime";
            break;
        case 19:
            strFieldName = "ModifyDate";
            break;
        case 20:
            strFieldName = "ModifyTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("TraceID")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("DocID")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("DocCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BussType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SubType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ScanOperator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ScanManageCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ScanLastApplyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ScanLastApplyTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("QCOperator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("QCManageCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("QCLastApplyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("QCLastApplyTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("QCFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("QCSuggest")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UnpassReason")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ScanOpeState")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 1:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 12:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 13:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 14:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 15:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 16:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 17:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 18:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 19:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 20:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
