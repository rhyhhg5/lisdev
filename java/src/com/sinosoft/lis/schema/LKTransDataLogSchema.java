/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LKTransDataLogDB;

/*
 * <p>ClassName: LKTransDataLogSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 实时交易系统
 * @CreateDate：2005-08-28
 */
public class LKTransDataLogSchema implements Schema, Cloneable
{
	// @Field
	/** 银行代码 */
	private String BankCode;
	/** 地区代码 */
	private String ZoneNo;
	/** 银行网点 */
	private String BankNode;
	/** 交易流水号 */
	private String TransNo;
	/** 交易代码 */
	private String TransCode;
	/** 序号 */
	private double SerNo;
	/** 数据类型 */
	private String ClassType;
	/** 数据对象名称 */
	private String ClassName;
	/** 对象数据 */
	private InputStream ClassData;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后修改日期 */
	private Date ModifyDate;
	/** 最后修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 13;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LKTransDataLogSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[5];
		pk[0] = "BankCode";
		pk[1] = "ZoneNo";
		pk[2] = "BankNode";
		pk[3] = "TransNo";
		pk[4] = "SerNo";

		PK = pk;
	}

            /**
             * Schema克隆
             * @return Object
             * @throws CloneNotSupportedException
             */
            public Object clone()
                    throws CloneNotSupportedException
            {
                LKTransDataLogSchema cloned = (LKTransDataLogSchema)super.clone();
                cloned.fDate = (FDate) fDate.clone();
                cloned.mErrors = (CErrors) mErrors.clone();
                return cloned;
            }

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getBankCode()
	{
		return BankCode;
	}
	public void setBankCode(String aBankCode)
	{
		BankCode = aBankCode;
	}
	public String getZoneNo()
	{
		return ZoneNo;
	}
	public void setZoneNo(String aZoneNo)
	{
		ZoneNo = aZoneNo;
	}
	public String getBankNode()
	{
		return BankNode;
	}
	public void setBankNode(String aBankNode)
	{
		BankNode = aBankNode;
	}
	public String getTransNo()
	{
		return TransNo;
	}
	public void setTransNo(String aTransNo)
	{
		TransNo = aTransNo;
	}
	public String getTransCode()
	{
		return TransCode;
	}
	public void setTransCode(String aTransCode)
	{
		TransCode = aTransCode;
	}
	public double getSerNo()
	{
		return SerNo;
	}
	public void setSerNo(double aSerNo)
	{
		SerNo = aSerNo;
	}
	public void setSerNo(String aSerNo)
	{
		if (aSerNo != null && !aSerNo.equals(""))
		{
			Double tDouble = new Double(aSerNo);
			double d = tDouble.doubleValue();
			SerNo = d;
		}
	}

	public String getClassType()
	{
		return ClassType;
	}
	public void setClassType(String aClassType)
	{
		ClassType = aClassType;
	}
	public String getClassName()
	{
		return ClassName;
	}
	public void setClassName(String aClassName)
	{
		ClassName = aClassName;
	}
	public InputStream getClassData()
	{
		return ClassData;
	}
	public void setClassData(InputStream aClassData)
	{
		ClassData = aClassData;
	}
	public void setClassData(String aClassData)
	{
	}

	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LKTransDataLogSchema 对象给 Schema 赋值
	* @param: aLKTransDataLogSchema LKTransDataLogSchema
	**/
	public void setSchema(LKTransDataLogSchema aLKTransDataLogSchema)
	{
		this.BankCode = aLKTransDataLogSchema.getBankCode();
		this.ZoneNo = aLKTransDataLogSchema.getZoneNo();
		this.BankNode = aLKTransDataLogSchema.getBankNode();
		this.TransNo = aLKTransDataLogSchema.getTransNo();
		this.TransCode = aLKTransDataLogSchema.getTransCode();
		this.SerNo = aLKTransDataLogSchema.getSerNo();
		this.ClassType = aLKTransDataLogSchema.getClassType();
		this.ClassName = aLKTransDataLogSchema.getClassName();
		this.ClassData = aLKTransDataLogSchema.getClassData();
		this.MakeDate = fDate.getDate( aLKTransDataLogSchema.getMakeDate());
		this.MakeTime = aLKTransDataLogSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLKTransDataLogSchema.getModifyDate());
		this.ModifyTime = aLKTransDataLogSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("BankCode") == null )
				this.BankCode = null;
			else
				this.BankCode = rs.getString("BankCode").trim();

			if( rs.getString("ZoneNo") == null )
				this.ZoneNo = null;
			else
				this.ZoneNo = rs.getString("ZoneNo").trim();

			if( rs.getString("BankNode") == null )
				this.BankNode = null;
			else
				this.BankNode = rs.getString("BankNode").trim();

			if( rs.getString("TransNo") == null )
				this.TransNo = null;
			else
				this.TransNo = rs.getString("TransNo").trim();

			if( rs.getString("TransCode") == null )
				this.TransCode = null;
			else
				this.TransCode = rs.getString("TransCode").trim();

			this.SerNo = rs.getDouble("SerNo");
			if( rs.getString("ClassType") == null )
				this.ClassType = null;
			else
				this.ClassType = rs.getString("ClassType").trim();

			if( rs.getString("ClassName") == null )
				this.ClassName = null;
			else
				this.ClassName = rs.getString("ClassName").trim();

			this.ClassData = rs.getBinaryStream("ClassData");
			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LKTransDataLog表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LKTransDataLogSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LKTransDataLogSchema getSchema()
	{
		LKTransDataLogSchema aLKTransDataLogSchema = new LKTransDataLogSchema();
		aLKTransDataLogSchema.setSchema(this);
		return aLKTransDataLogSchema;
	}

	public LKTransDataLogDB getDB()
	{
		LKTransDataLogDB aDBOper = new LKTransDataLogDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLKTransDataLog描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append(StrTool.cTrim(BankCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ZoneNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(BankNode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(TransNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(TransCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(SerNo));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ClassType)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ClassName)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( 1 );strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLKTransDataLog>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			ZoneNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			BankNode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			TransNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			TransCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			SerNo = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,6,SysConst.PACKAGESPILTER))).doubleValue();
			ClassType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			ClassName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LKTransDataLogSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equalsIgnoreCase("BankCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
		}
		if (FCode.equalsIgnoreCase("ZoneNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ZoneNo));
		}
		if (FCode.equalsIgnoreCase("BankNode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankNode));
		}
		if (FCode.equalsIgnoreCase("TransNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransNo));
		}
		if (FCode.equalsIgnoreCase("TransCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransCode));
		}
		if (FCode.equalsIgnoreCase("SerNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SerNo));
		}
		if (FCode.equalsIgnoreCase("ClassType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClassType));
		}
		if (FCode.equalsIgnoreCase("ClassName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClassName));
		}
		if (FCode.equalsIgnoreCase("ClassData"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClassData));
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(BankCode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(ZoneNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(BankNode);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(TransNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(TransCode);
				break;
			case 5:
				strFieldValue = String.valueOf(SerNo);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(ClassType);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(ClassName);
				break;
			case 8:
				strFieldValue = String.valueOf(ClassData);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("BankCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankCode = FValue.trim();
			}
			else
				BankCode = null;
		}
		if (FCode.equalsIgnoreCase("ZoneNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ZoneNo = FValue.trim();
			}
			else
				ZoneNo = null;
		}
		if (FCode.equalsIgnoreCase("BankNode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankNode = FValue.trim();
			}
			else
				BankNode = null;
		}
		if (FCode.equalsIgnoreCase("TransNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TransNo = FValue.trim();
			}
			else
				TransNo = null;
		}
		if (FCode.equalsIgnoreCase("TransCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TransCode = FValue.trim();
			}
			else
				TransCode = null;
		}
		if (FCode.equalsIgnoreCase("SerNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SerNo = d;
			}
		}
		if (FCode.equalsIgnoreCase("ClassType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ClassType = FValue.trim();
			}
			else
				ClassType = null;
		}
		if (FCode.equalsIgnoreCase("ClassName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ClassName = FValue.trim();
			}
			else
				ClassName = null;
		}
		if (FCode.equalsIgnoreCase("ClassData"))
		{
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LKTransDataLogSchema other = (LKTransDataLogSchema)otherObject;
		return
			BankCode.equals(other.getBankCode())
			&& ZoneNo.equals(other.getZoneNo())
			&& BankNode.equals(other.getBankNode())
			&& TransNo.equals(other.getTransNo())
			&& TransCode.equals(other.getTransCode())
			&& SerNo == other.getSerNo()
			&& ClassType.equals(other.getClassType())
			&& ClassName.equals(other.getClassName())
			
			&& fDate.getString(MakeDate).equals(other.getMakeDate())
			&& MakeTime.equals(other.getMakeTime())
			&& fDate.getString(ModifyDate).equals(other.getModifyDate())
			&& ModifyTime.equals(other.getModifyTime());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("BankCode") ) {
			return 0;
		}
		if( strFieldName.equals("ZoneNo") ) {
			return 1;
		}
		if( strFieldName.equals("BankNode") ) {
			return 2;
		}
		if( strFieldName.equals("TransNo") ) {
			return 3;
		}
		if( strFieldName.equals("TransCode") ) {
			return 4;
		}
		if( strFieldName.equals("SerNo") ) {
			return 5;
		}
		if( strFieldName.equals("ClassType") ) {
			return 6;
		}
		if( strFieldName.equals("ClassName") ) {
			return 7;
		}
		if( strFieldName.equals("ClassData") ) {
			return 8;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 9;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 10;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 11;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 12;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "BankCode";
				break;
			case 1:
				strFieldName = "ZoneNo";
				break;
			case 2:
				strFieldName = "BankNode";
				break;
			case 3:
				strFieldName = "TransNo";
				break;
			case 4:
				strFieldName = "TransCode";
				break;
			case 5:
				strFieldName = "SerNo";
				break;
			case 6:
				strFieldName = "ClassType";
				break;
			case 7:
				strFieldName = "ClassName";
				break;
			case 8:
				strFieldName = "ClassData";
				break;
			case 9:
				strFieldName = "MakeDate";
				break;
			case 10:
				strFieldName = "MakeTime";
				break;
			case 11:
				strFieldName = "ModifyDate";
				break;
			case 12:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("BankCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ZoneNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankNode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TransNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TransCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SerNo") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ClassType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ClassName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ClassData") ) {
			return Schema.TYPE_NOFOUND;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_NOFOUND;
				break;
			case 9:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
