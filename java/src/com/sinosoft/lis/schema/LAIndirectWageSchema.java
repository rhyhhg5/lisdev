/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LAIndirectWageDB;

/*
 * <p>ClassName: LAIndirectWageSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 银代新建表
 * @CreateDate：2016-05-06
 */
public class LAIndirectWageSchema implements Schema, Cloneable
{
	// @Field
	/** 扎帐系列号 */
	private String CommisionSN;
	/** 银代团队主管首期 */
	private double BankGrpMoney;
	/** J银代奖励方案 */
	private double RewardMoney;
	/** 团队首期绩效比例 */
	private double GrpRate;
	/** J奖励方案比例 */
	private double RewardRate;
	/** 薪资项1 */
	private double F1;
	/** 薪资项2 */
	private double F2;
	/** 薪资项3 */
	private double F3;
	/** 薪资项4 */
	private double F4;
	/** 薪资项5 */
	private double F5;
	/** 间接提奖比例1 */
	private double P1;
	/** 间接提奖比例2 */
	private double P2;
	/** 间接提奖比例3 */
	private double P3;
	/** 间接提奖比例4 */
	private double P4;
	/** 间接提奖比例5 */
	private double P5;
	/** 提数日期 */
	private Date TMakeDate;
	/** 展业类型 */
	private String BranchType;
	/** 销售渠道 */
	private String BranchType2;
	/** 操作人 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 修改日期 */
	private Date ModifyDate;
	/** 修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 23;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LAIndirectWageSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "CommisionSN";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LAIndirectWageSchema cloned = (LAIndirectWageSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getCommisionSN()
	{
		return CommisionSN;
	}
	public void setCommisionSN(String aCommisionSN)
	{
		CommisionSN = aCommisionSN;
	}
	public double getBankGrpMoney()
	{
		return BankGrpMoney;
	}
	public void setBankGrpMoney(double aBankGrpMoney)
	{
		BankGrpMoney = Arith.round(aBankGrpMoney,2);
	}
	public void setBankGrpMoney(String aBankGrpMoney)
	{
		if (aBankGrpMoney != null && !aBankGrpMoney.equals(""))
		{
			Double tDouble = new Double(aBankGrpMoney);
			double d = tDouble.doubleValue();
                BankGrpMoney = Arith.round(d,2);
		}
	}

	public double getRewardMoney()
	{
		return RewardMoney;
	}
	public void setRewardMoney(double aRewardMoney)
	{
		RewardMoney = Arith.round(aRewardMoney,2);
	}
	public void setRewardMoney(String aRewardMoney)
	{
		if (aRewardMoney != null && !aRewardMoney.equals(""))
		{
			Double tDouble = new Double(aRewardMoney);
			double d = tDouble.doubleValue();
                RewardMoney = Arith.round(d,2);
		}
	}

	public double getGrpRate()
	{
		return GrpRate;
	}
	public void setGrpRate(double aGrpRate)
	{
		GrpRate = Arith.round(aGrpRate,6);
	}
	public void setGrpRate(String aGrpRate)
	{
		if (aGrpRate != null && !aGrpRate.equals(""))
		{
			Double tDouble = new Double(aGrpRate);
			double d = tDouble.doubleValue();
                GrpRate = Arith.round(d,6);
		}
	}

	public double getRewardRate()
	{
		return RewardRate;
	}
	public void setRewardRate(double aRewardRate)
	{
		RewardRate = Arith.round(aRewardRate,6);
	}
	public void setRewardRate(String aRewardRate)
	{
		if (aRewardRate != null && !aRewardRate.equals(""))
		{
			Double tDouble = new Double(aRewardRate);
			double d = tDouble.doubleValue();
                RewardRate = Arith.round(d,6);
		}
	}

	public double getF1()
	{
		return F1;
	}
	public void setF1(double aF1)
	{
		F1 = Arith.round(aF1,2);
	}
	public void setF1(String aF1)
	{
		if (aF1 != null && !aF1.equals(""))
		{
			Double tDouble = new Double(aF1);
			double d = tDouble.doubleValue();
                F1 = Arith.round(d,2);
		}
	}

	public double getF2()
	{
		return F2;
	}
	public void setF2(double aF2)
	{
		F2 = Arith.round(aF2,2);
	}
	public void setF2(String aF2)
	{
		if (aF2 != null && !aF2.equals(""))
		{
			Double tDouble = new Double(aF2);
			double d = tDouble.doubleValue();
                F2 = Arith.round(d,2);
		}
	}

	public double getF3()
	{
		return F3;
	}
	public void setF3(double aF3)
	{
		F3 = Arith.round(aF3,2);
	}
	public void setF3(String aF3)
	{
		if (aF3 != null && !aF3.equals(""))
		{
			Double tDouble = new Double(aF3);
			double d = tDouble.doubleValue();
                F3 = Arith.round(d,2);
		}
	}

	public double getF4()
	{
		return F4;
	}
	public void setF4(double aF4)
	{
		F4 = Arith.round(aF4,2);
	}
	public void setF4(String aF4)
	{
		if (aF4 != null && !aF4.equals(""))
		{
			Double tDouble = new Double(aF4);
			double d = tDouble.doubleValue();
                F4 = Arith.round(d,2);
		}
	}

	public double getF5()
	{
		return F5;
	}
	public void setF5(double aF5)
	{
		F5 = Arith.round(aF5,2);
	}
	public void setF5(String aF5)
	{
		if (aF5 != null && !aF5.equals(""))
		{
			Double tDouble = new Double(aF5);
			double d = tDouble.doubleValue();
                F5 = Arith.round(d,2);
		}
	}

	public double getP1()
	{
		return P1;
	}
	public void setP1(double aP1)
	{
		P1 = Arith.round(aP1,6);
	}
	public void setP1(String aP1)
	{
		if (aP1 != null && !aP1.equals(""))
		{
			Double tDouble = new Double(aP1);
			double d = tDouble.doubleValue();
                P1 = Arith.round(d,6);
		}
	}

	public double getP2()
	{
		return P2;
	}
	public void setP2(double aP2)
	{
		P2 = Arith.round(aP2,6);
	}
	public void setP2(String aP2)
	{
		if (aP2 != null && !aP2.equals(""))
		{
			Double tDouble = new Double(aP2);
			double d = tDouble.doubleValue();
                P2 = Arith.round(d,6);
		}
	}

	public double getP3()
	{
		return P3;
	}
	public void setP3(double aP3)
	{
		P3 = Arith.round(aP3,6);
	}
	public void setP3(String aP3)
	{
		if (aP3 != null && !aP3.equals(""))
		{
			Double tDouble = new Double(aP3);
			double d = tDouble.doubleValue();
                P3 = Arith.round(d,6);
		}
	}

	public double getP4()
	{
		return P4;
	}
	public void setP4(double aP4)
	{
		P4 = Arith.round(aP4,6);
	}
	public void setP4(String aP4)
	{
		if (aP4 != null && !aP4.equals(""))
		{
			Double tDouble = new Double(aP4);
			double d = tDouble.doubleValue();
                P4 = Arith.round(d,6);
		}
	}

	public double getP5()
	{
		return P5;
	}
	public void setP5(double aP5)
	{
		P5 = Arith.round(aP5,6);
	}
	public void setP5(String aP5)
	{
		if (aP5 != null && !aP5.equals(""))
		{
			Double tDouble = new Double(aP5);
			double d = tDouble.doubleValue();
                P5 = Arith.round(d,6);
		}
	}

	public String getTMakeDate()
	{
		if( TMakeDate != null )
			return fDate.getString(TMakeDate);
		else
			return null;
	}
	public void setTMakeDate(Date aTMakeDate)
	{
		TMakeDate = aTMakeDate;
	}
	public void setTMakeDate(String aTMakeDate)
	{
		if (aTMakeDate != null && !aTMakeDate.equals("") )
		{
			TMakeDate = fDate.getDate( aTMakeDate );
		}
		else
			TMakeDate = null;
	}

	public String getBranchType()
	{
		return BranchType;
	}
	public void setBranchType(String aBranchType)
	{
		BranchType = aBranchType;
	}
	public String getBranchType2()
	{
		return BranchType2;
	}
	public void setBranchType2(String aBranchType2)
	{
		BranchType2 = aBranchType2;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LAIndirectWageSchema 对象给 Schema 赋值
	* @param: aLAIndirectWageSchema LAIndirectWageSchema
	**/
	public void setSchema(LAIndirectWageSchema aLAIndirectWageSchema)
	{
		this.CommisionSN = aLAIndirectWageSchema.getCommisionSN();
		this.BankGrpMoney = aLAIndirectWageSchema.getBankGrpMoney();
		this.RewardMoney = aLAIndirectWageSchema.getRewardMoney();
		this.GrpRate = aLAIndirectWageSchema.getGrpRate();
		this.RewardRate = aLAIndirectWageSchema.getRewardRate();
		this.F1 = aLAIndirectWageSchema.getF1();
		this.F2 = aLAIndirectWageSchema.getF2();
		this.F3 = aLAIndirectWageSchema.getF3();
		this.F4 = aLAIndirectWageSchema.getF4();
		this.F5 = aLAIndirectWageSchema.getF5();
		this.P1 = aLAIndirectWageSchema.getP1();
		this.P2 = aLAIndirectWageSchema.getP2();
		this.P3 = aLAIndirectWageSchema.getP3();
		this.P4 = aLAIndirectWageSchema.getP4();
		this.P5 = aLAIndirectWageSchema.getP5();
		this.TMakeDate = fDate.getDate( aLAIndirectWageSchema.getTMakeDate());
		this.BranchType = aLAIndirectWageSchema.getBranchType();
		this.BranchType2 = aLAIndirectWageSchema.getBranchType2();
		this.Operator = aLAIndirectWageSchema.getOperator();
		this.MakeDate = fDate.getDate( aLAIndirectWageSchema.getMakeDate());
		this.MakeTime = aLAIndirectWageSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLAIndirectWageSchema.getModifyDate());
		this.ModifyTime = aLAIndirectWageSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("CommisionSN") == null )
				this.CommisionSN = null;
			else
				this.CommisionSN = rs.getString("CommisionSN").trim();

			this.BankGrpMoney = rs.getDouble("BankGrpMoney");
			this.RewardMoney = rs.getDouble("RewardMoney");
			this.GrpRate = rs.getDouble("GrpRate");
			this.RewardRate = rs.getDouble("RewardRate");
			this.F1 = rs.getDouble("F1");
			this.F2 = rs.getDouble("F2");
			this.F3 = rs.getDouble("F3");
			this.F4 = rs.getDouble("F4");
			this.F5 = rs.getDouble("F5");
			this.P1 = rs.getDouble("P1");
			this.P2 = rs.getDouble("P2");
			this.P3 = rs.getDouble("P3");
			this.P4 = rs.getDouble("P4");
			this.P5 = rs.getDouble("P5");
			this.TMakeDate = rs.getDate("TMakeDate");
			if( rs.getString("BranchType") == null )
				this.BranchType = null;
			else
				this.BranchType = rs.getString("BranchType").trim();

			if( rs.getString("BranchType2") == null )
				this.BranchType2 = null;
			else
				this.BranchType2 = rs.getString("BranchType2").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LAIndirectWage表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAIndirectWageSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LAIndirectWageSchema getSchema()
	{
		LAIndirectWageSchema aLAIndirectWageSchema = new LAIndirectWageSchema();
		aLAIndirectWageSchema.setSchema(this);
		return aLAIndirectWageSchema;
	}

	public LAIndirectWageDB getDB()
	{
		LAIndirectWageDB aDBOper = new LAIndirectWageDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAIndirectWage描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(CommisionSN)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(BankGrpMoney));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(RewardMoney));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(GrpRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(RewardRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(F1));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(F2));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(F3));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(F4));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(F5));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(P1));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(P2));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(P3));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(P4));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(P5));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( TMakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchType2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAIndirectWage>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			CommisionSN = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			BankGrpMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,2,SysConst.PACKAGESPILTER))).doubleValue();
			RewardMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,3,SysConst.PACKAGESPILTER))).doubleValue();
			GrpRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,4,SysConst.PACKAGESPILTER))).doubleValue();
			RewardRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,5,SysConst.PACKAGESPILTER))).doubleValue();
			F1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,6,SysConst.PACKAGESPILTER))).doubleValue();
			F2 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,7,SysConst.PACKAGESPILTER))).doubleValue();
			F3 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,8,SysConst.PACKAGESPILTER))).doubleValue();
			F4 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,9,SysConst.PACKAGESPILTER))).doubleValue();
			F5 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,10,SysConst.PACKAGESPILTER))).doubleValue();
			P1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,11,SysConst.PACKAGESPILTER))).doubleValue();
			P2 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,12,SysConst.PACKAGESPILTER))).doubleValue();
			P3 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,13,SysConst.PACKAGESPILTER))).doubleValue();
			P4 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,14,SysConst.PACKAGESPILTER))).doubleValue();
			P5 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,15,SysConst.PACKAGESPILTER))).doubleValue();
			TMakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,SysConst.PACKAGESPILTER));
			BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAIndirectWageSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("CommisionSN"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CommisionSN));
		}
		if (FCode.equals("BankGrpMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankGrpMoney));
		}
		if (FCode.equals("RewardMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RewardMoney));
		}
		if (FCode.equals("GrpRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpRate));
		}
		if (FCode.equals("RewardRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RewardRate));
		}
		if (FCode.equals("F1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F1));
		}
		if (FCode.equals("F2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F2));
		}
		if (FCode.equals("F3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F3));
		}
		if (FCode.equals("F4"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F4));
		}
		if (FCode.equals("F5"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F5));
		}
		if (FCode.equals("P1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(P1));
		}
		if (FCode.equals("P2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(P2));
		}
		if (FCode.equals("P3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(P3));
		}
		if (FCode.equals("P4"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(P4));
		}
		if (FCode.equals("P5"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(P5));
		}
		if (FCode.equals("TMakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getTMakeDate()));
		}
		if (FCode.equals("BranchType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
		}
		if (FCode.equals("BranchType2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(CommisionSN);
				break;
			case 1:
				strFieldValue = String.valueOf(BankGrpMoney);
				break;
			case 2:
				strFieldValue = String.valueOf(RewardMoney);
				break;
			case 3:
				strFieldValue = String.valueOf(GrpRate);
				break;
			case 4:
				strFieldValue = String.valueOf(RewardRate);
				break;
			case 5:
				strFieldValue = String.valueOf(F1);
				break;
			case 6:
				strFieldValue = String.valueOf(F2);
				break;
			case 7:
				strFieldValue = String.valueOf(F3);
				break;
			case 8:
				strFieldValue = String.valueOf(F4);
				break;
			case 9:
				strFieldValue = String.valueOf(F5);
				break;
			case 10:
				strFieldValue = String.valueOf(P1);
				break;
			case 11:
				strFieldValue = String.valueOf(P2);
				break;
			case 12:
				strFieldValue = String.valueOf(P3);
				break;
			case 13:
				strFieldValue = String.valueOf(P4);
				break;
			case 14:
				strFieldValue = String.valueOf(P5);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getTMakeDate()));
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(BranchType);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(BranchType2);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("CommisionSN"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CommisionSN = FValue.trim();
			}
			else
				CommisionSN = null;
		}
		if (FCode.equalsIgnoreCase("BankGrpMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				BankGrpMoney = d;
			}
		}
		if (FCode.equalsIgnoreCase("RewardMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				RewardMoney = d;
			}
		}
		if (FCode.equalsIgnoreCase("GrpRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				GrpRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("RewardRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				RewardRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("F1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				F1 = d;
			}
		}
		if (FCode.equalsIgnoreCase("F2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				F2 = d;
			}
		}
		if (FCode.equalsIgnoreCase("F3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				F3 = d;
			}
		}
		if (FCode.equalsIgnoreCase("F4"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				F4 = d;
			}
		}
		if (FCode.equalsIgnoreCase("F5"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				F5 = d;
			}
		}
		if (FCode.equalsIgnoreCase("P1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				P1 = d;
			}
		}
		if (FCode.equalsIgnoreCase("P2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				P2 = d;
			}
		}
		if (FCode.equalsIgnoreCase("P3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				P3 = d;
			}
		}
		if (FCode.equalsIgnoreCase("P4"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				P4 = d;
			}
		}
		if (FCode.equalsIgnoreCase("P5"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				P5 = d;
			}
		}
		if (FCode.equalsIgnoreCase("TMakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				TMakeDate = fDate.getDate( FValue );
			}
			else
				TMakeDate = null;
		}
		if (FCode.equalsIgnoreCase("BranchType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType = FValue.trim();
			}
			else
				BranchType = null;
		}
		if (FCode.equalsIgnoreCase("BranchType2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType2 = FValue.trim();
			}
			else
				BranchType2 = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LAIndirectWageSchema other = (LAIndirectWageSchema)otherObject;
		return
			(CommisionSN == null ? other.getCommisionSN() == null : CommisionSN.equals(other.getCommisionSN()))
			&& BankGrpMoney == other.getBankGrpMoney()
			&& RewardMoney == other.getRewardMoney()
			&& GrpRate == other.getGrpRate()
			&& RewardRate == other.getRewardRate()
			&& F1 == other.getF1()
			&& F2 == other.getF2()
			&& F3 == other.getF3()
			&& F4 == other.getF4()
			&& F5 == other.getF5()
			&& P1 == other.getP1()
			&& P2 == other.getP2()
			&& P3 == other.getP3()
			&& P4 == other.getP4()
			&& P5 == other.getP5()
			&& (TMakeDate == null ? other.getTMakeDate() == null : fDate.getString(TMakeDate).equals(other.getTMakeDate()))
			&& (BranchType == null ? other.getBranchType() == null : BranchType.equals(other.getBranchType()))
			&& (BranchType2 == null ? other.getBranchType2() == null : BranchType2.equals(other.getBranchType2()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("CommisionSN") ) {
			return 0;
		}
		if( strFieldName.equals("BankGrpMoney") ) {
			return 1;
		}
		if( strFieldName.equals("RewardMoney") ) {
			return 2;
		}
		if( strFieldName.equals("GrpRate") ) {
			return 3;
		}
		if( strFieldName.equals("RewardRate") ) {
			return 4;
		}
		if( strFieldName.equals("F1") ) {
			return 5;
		}
		if( strFieldName.equals("F2") ) {
			return 6;
		}
		if( strFieldName.equals("F3") ) {
			return 7;
		}
		if( strFieldName.equals("F4") ) {
			return 8;
		}
		if( strFieldName.equals("F5") ) {
			return 9;
		}
		if( strFieldName.equals("P1") ) {
			return 10;
		}
		if( strFieldName.equals("P2") ) {
			return 11;
		}
		if( strFieldName.equals("P3") ) {
			return 12;
		}
		if( strFieldName.equals("P4") ) {
			return 13;
		}
		if( strFieldName.equals("P5") ) {
			return 14;
		}
		if( strFieldName.equals("TMakeDate") ) {
			return 15;
		}
		if( strFieldName.equals("BranchType") ) {
			return 16;
		}
		if( strFieldName.equals("BranchType2") ) {
			return 17;
		}
		if( strFieldName.equals("Operator") ) {
			return 18;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 19;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 20;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 21;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 22;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "CommisionSN";
				break;
			case 1:
				strFieldName = "BankGrpMoney";
				break;
			case 2:
				strFieldName = "RewardMoney";
				break;
			case 3:
				strFieldName = "GrpRate";
				break;
			case 4:
				strFieldName = "RewardRate";
				break;
			case 5:
				strFieldName = "F1";
				break;
			case 6:
				strFieldName = "F2";
				break;
			case 7:
				strFieldName = "F3";
				break;
			case 8:
				strFieldName = "F4";
				break;
			case 9:
				strFieldName = "F5";
				break;
			case 10:
				strFieldName = "P1";
				break;
			case 11:
				strFieldName = "P2";
				break;
			case 12:
				strFieldName = "P3";
				break;
			case 13:
				strFieldName = "P4";
				break;
			case 14:
				strFieldName = "P5";
				break;
			case 15:
				strFieldName = "TMakeDate";
				break;
			case 16:
				strFieldName = "BranchType";
				break;
			case 17:
				strFieldName = "BranchType2";
				break;
			case 18:
				strFieldName = "Operator";
				break;
			case 19:
				strFieldName = "MakeDate";
				break;
			case 20:
				strFieldName = "MakeTime";
				break;
			case 21:
				strFieldName = "ModifyDate";
				break;
			case 22:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("CommisionSN") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankGrpMoney") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("RewardMoney") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("GrpRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("RewardRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("F1") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("F2") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("F3") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("F4") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("F5") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("P1") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("P2") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("P3") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("P4") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("P5") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("TMakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("BranchType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchType2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 2:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 3:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 4:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 5:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 6:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 7:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 8:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 9:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 10:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 11:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 12:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 13:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 14:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 15:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
