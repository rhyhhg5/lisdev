/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.Ciitc_TaskDB;

/*
 * <p>ClassName: Ciitc_TaskSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2017-10-19
 */
public class Ciitc_TaskSchema implements Schema, Cloneable
{
	// @Field
	/** Tranno */
	private String TranNo;
	/** Proposalno */
	private String ProposalNo;
	/** Counterid */
	private String CounterId;
	/** Bankcode */
	private String BankCode;
	/** Subbankcode */
	private String SubBankCode;
	/** Nodebankcode */
	private String NodeBankCode;
	/** Insurercode */
	private String InsurerCode;
	/** Subinsurercode */
	private String SubInsurerCode;
	/** Product_name */
	private String Product_Name;
	/** Product_code */
	private String Product_Code;
	/** Businessno */
	private String BusinessNo;
	/** Customername */
	private String CustomerName;
	/** Customercardtype */
	private int CustomerCardType;
	/** Customercardno */
	private String CustomerCardNo;
	/** Customerbirthday */
	private String CustomerBirthday;
	/** Videoname */
	private String VideoName;
	/** Videotype */
	private String VideoType;
	/** Videosize */
	private String VideoSize;
	/** Businessseriano */
	private String BusinessSeriaNo;
	/** Batchno */
	private String BatchNo;
	/** Content_md5 */
	private String Content_MD5;
	/** Bak1 */
	private String bak1;
	/** Bak2 */
	private String bak2;
	/** Bak3 */
	private String bak3;

	public static final int FIELDNUM = 24;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public Ciitc_TaskSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "TranNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		Ciitc_TaskSchema cloned = (Ciitc_TaskSchema) super.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getTranNo()
	{
		return TranNo;
	}
	public void setTranNo(String aTranNo)
	{
		TranNo = aTranNo;
	}
	public String getProposalNo()
	{
		return ProposalNo;
	}
	public void setProposalNo(String aProposalNo)
	{
		ProposalNo = aProposalNo;
	}
	public String getCounterId()
	{
		return CounterId;
	}
	public void setCounterId(String aCounterId)
	{
		CounterId = aCounterId;
	}
	public String getBankCode()
	{
		return BankCode;
	}
	public void setBankCode(String aBankCode)
	{
		BankCode = aBankCode;
	}
	public String getSubBankCode()
	{
		return SubBankCode;
	}
	public void setSubBankCode(String aSubBankCode)
	{
		SubBankCode = aSubBankCode;
	}
	public String getNodeBankCode()
	{
		return NodeBankCode;
	}
	public void setNodeBankCode(String aNodeBankCode)
	{
		NodeBankCode = aNodeBankCode;
	}
	public String getInsurerCode()
	{
		return InsurerCode;
	}
	public void setInsurerCode(String aInsurerCode)
	{
		InsurerCode = aInsurerCode;
	}
	public String getSubInsurerCode()
	{
		return SubInsurerCode;
	}
	public void setSubInsurerCode(String aSubInsurerCode)
	{
		SubInsurerCode = aSubInsurerCode;
	}
	public String getProduct_Name()
	{
		return Product_Name;
	}
	public void setProduct_Name(String aProduct_Name)
	{
		Product_Name = aProduct_Name;
	}
	public String getProduct_Code()
	{
		return Product_Code;
	}
	public void setProduct_Code(String aProduct_Code)
	{
		Product_Code = aProduct_Code;
	}
	public String getBusinessNo()
	{
		return BusinessNo;
	}
	public void setBusinessNo(String aBusinessNo)
	{
		BusinessNo = aBusinessNo;
	}
	public String getCustomerName()
	{
		return CustomerName;
	}
	public void setCustomerName(String aCustomerName)
	{
		CustomerName = aCustomerName;
	}
	public int getCustomerCardType()
	{
		return CustomerCardType;
	}
	public void setCustomerCardType(int aCustomerCardType)
	{
		CustomerCardType = aCustomerCardType;
	}
	public void setCustomerCardType(String aCustomerCardType)
	{
		if (aCustomerCardType != null && !aCustomerCardType.equals(""))
		{
			Integer tInteger = new Integer(aCustomerCardType);
			int i = tInteger.intValue();
			CustomerCardType = i;
		}
	}

	public String getCustomerCardNo()
	{
		return CustomerCardNo;
	}
	public void setCustomerCardNo(String aCustomerCardNo)
	{
		CustomerCardNo = aCustomerCardNo;
	}
	public String getCustomerBirthday()
	{
		return CustomerBirthday;
	}
	public void setCustomerBirthday(String aCustomerBirthday)
	{
		CustomerBirthday = aCustomerBirthday;
	}
	public String getVideoName()
	{
		return VideoName;
	}
	public void setVideoName(String aVideoName)
	{
		VideoName = aVideoName;
	}
	public String getVideoType()
	{
		return VideoType;
	}
	public void setVideoType(String aVideoType)
	{
		VideoType = aVideoType;
	}
	public String getVideoSize()
	{
		return VideoSize;
	}
	public void setVideoSize(String aVideoSize)
	{
		VideoSize = aVideoSize;
	}
	public String getBusinessSeriaNo()
	{
		return BusinessSeriaNo;
	}
	public void setBusinessSeriaNo(String aBusinessSeriaNo)
	{
		BusinessSeriaNo = aBusinessSeriaNo;
	}
	public String getBatchNo()
	{
		return BatchNo;
	}
	public void setBatchNo(String aBatchNo)
	{
		BatchNo = aBatchNo;
	}
	public String getContent_MD5()
	{
		return Content_MD5;
	}
	public void setContent_MD5(String aContent_MD5)
	{
		Content_MD5 = aContent_MD5;
	}
	public String getbak1()
	{
		return bak1;
	}
	public void setbak1(String abak1)
	{
		bak1 = abak1;
	}
	public String getbak2()
	{
		return bak2;
	}
	public void setbak2(String abak2)
	{
		bak2 = abak2;
	}
	public String getbak3()
	{
		return bak3;
	}
	public void setbak3(String abak3)
	{
		bak3 = abak3;
	}

	/**
	* 使用另外一个 Ciitc_TaskSchema 对象给 Schema 赋值
	* @param: aCiitc_TaskSchema Ciitc_TaskSchema
	**/
	public void setSchema(Ciitc_TaskSchema aCiitc_TaskSchema)
	{
		this.TranNo = aCiitc_TaskSchema.getTranNo();
		this.ProposalNo = aCiitc_TaskSchema.getProposalNo();
		this.CounterId = aCiitc_TaskSchema.getCounterId();
		this.BankCode = aCiitc_TaskSchema.getBankCode();
		this.SubBankCode = aCiitc_TaskSchema.getSubBankCode();
		this.NodeBankCode = aCiitc_TaskSchema.getNodeBankCode();
		this.InsurerCode = aCiitc_TaskSchema.getInsurerCode();
		this.SubInsurerCode = aCiitc_TaskSchema.getSubInsurerCode();
		this.Product_Name = aCiitc_TaskSchema.getProduct_Name();
		this.Product_Code = aCiitc_TaskSchema.getProduct_Code();
		this.BusinessNo = aCiitc_TaskSchema.getBusinessNo();
		this.CustomerName = aCiitc_TaskSchema.getCustomerName();
		this.CustomerCardType = aCiitc_TaskSchema.getCustomerCardType();
		this.CustomerCardNo = aCiitc_TaskSchema.getCustomerCardNo();
		this.CustomerBirthday = aCiitc_TaskSchema.getCustomerBirthday();
		this.VideoName = aCiitc_TaskSchema.getVideoName();
		this.VideoType = aCiitc_TaskSchema.getVideoType();
		this.VideoSize = aCiitc_TaskSchema.getVideoSize();
		this.BusinessSeriaNo = aCiitc_TaskSchema.getBusinessSeriaNo();
		this.BatchNo = aCiitc_TaskSchema.getBatchNo();
		this.Content_MD5 = aCiitc_TaskSchema.getContent_MD5();
		this.bak1 = aCiitc_TaskSchema.getbak1();
		this.bak2 = aCiitc_TaskSchema.getbak2();
		this.bak3 = aCiitc_TaskSchema.getbak3();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("TranNo") == null )
				this.TranNo = null;
			else
				this.TranNo = rs.getString("TranNo").trim();

			if( rs.getString("ProposalNo") == null )
				this.ProposalNo = null;
			else
				this.ProposalNo = rs.getString("ProposalNo").trim();

			if( rs.getString("CounterId") == null )
				this.CounterId = null;
			else
				this.CounterId = rs.getString("CounterId").trim();

			if( rs.getString("BankCode") == null )
				this.BankCode = null;
			else
				this.BankCode = rs.getString("BankCode").trim();

			if( rs.getString("SubBankCode") == null )
				this.SubBankCode = null;
			else
				this.SubBankCode = rs.getString("SubBankCode").trim();

			if( rs.getString("NodeBankCode") == null )
				this.NodeBankCode = null;
			else
				this.NodeBankCode = rs.getString("NodeBankCode").trim();

			if( rs.getString("InsurerCode") == null )
				this.InsurerCode = null;
			else
				this.InsurerCode = rs.getString("InsurerCode").trim();

			if( rs.getString("SubInsurerCode") == null )
				this.SubInsurerCode = null;
			else
				this.SubInsurerCode = rs.getString("SubInsurerCode").trim();

			if( rs.getString("Product_Name") == null )
				this.Product_Name = null;
			else
				this.Product_Name = rs.getString("Product_Name").trim();

			if( rs.getString("Product_Code") == null )
				this.Product_Code = null;
			else
				this.Product_Code = rs.getString("Product_Code").trim();

			if( rs.getString("BusinessNo") == null )
				this.BusinessNo = null;
			else
				this.BusinessNo = rs.getString("BusinessNo").trim();

			if( rs.getString("CustomerName") == null )
				this.CustomerName = null;
			else
				this.CustomerName = rs.getString("CustomerName").trim();

			this.CustomerCardType = rs.getInt("CustomerCardType");
			if( rs.getString("CustomerCardNo") == null )
				this.CustomerCardNo = null;
			else
				this.CustomerCardNo = rs.getString("CustomerCardNo").trim();

			if( rs.getString("CustomerBirthday") == null )
				this.CustomerBirthday = null;
			else
				this.CustomerBirthday = rs.getString("CustomerBirthday").trim();

			if( rs.getString("VideoName") == null )
				this.VideoName = null;
			else
				this.VideoName = rs.getString("VideoName").trim();

			if( rs.getString("VideoType") == null )
				this.VideoType = null;
			else
				this.VideoType = rs.getString("VideoType").trim();

			if( rs.getString("VideoSize") == null )
				this.VideoSize = null;
			else
				this.VideoSize = rs.getString("VideoSize").trim();

			if( rs.getString("BusinessSeriaNo") == null )
				this.BusinessSeriaNo = null;
			else
				this.BusinessSeriaNo = rs.getString("BusinessSeriaNo").trim();

			if( rs.getString("BatchNo") == null )
				this.BatchNo = null;
			else
				this.BatchNo = rs.getString("BatchNo").trim();

			if( rs.getString("Content_MD5") == null )
				this.Content_MD5 = null;
			else
				this.Content_MD5 = rs.getString("Content_MD5").trim();

			if( rs.getString("bak1") == null )
				this.bak1 = null;
			else
				this.bak1 = rs.getString("bak1").trim();

			if( rs.getString("bak2") == null )
				this.bak2 = null;
			else
				this.bak2 = rs.getString("bak2").trim();

			if( rs.getString("bak3") == null )
				this.bak3 = null;
			else
				this.bak3 = rs.getString("bak3").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的Ciitc_Task表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "Ciitc_TaskSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public Ciitc_TaskSchema getSchema()
	{
		Ciitc_TaskSchema aCiitc_TaskSchema = new Ciitc_TaskSchema();
		aCiitc_TaskSchema.setSchema(this);
		return aCiitc_TaskSchema;
	}

	public Ciitc_TaskDB getDB()
	{
		Ciitc_TaskDB aDBOper = new Ciitc_TaskDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpCiitc_Task描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(TranNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProposalNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CounterId)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SubBankCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(NodeBankCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsurerCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SubInsurerCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Product_Name)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Product_Code)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusinessNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(CustomerCardType));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerCardNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerBirthday)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(VideoName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(VideoType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(VideoSize)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusinessSeriaNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BatchNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Content_MD5)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak3));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpCiitc_Task>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			TranNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			ProposalNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			CounterId = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			SubBankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			NodeBankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			InsurerCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			SubInsurerCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			Product_Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			Product_Code = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			BusinessNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			CustomerName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			CustomerCardType= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,13,SysConst.PACKAGESPILTER))).intValue();
			CustomerCardNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			CustomerBirthday = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			VideoName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			VideoType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			VideoSize = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			BusinessSeriaNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			BatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			Content_MD5 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			bak1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			bak2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			bak3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "Ciitc_TaskSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("TranNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TranNo));
		}
		if (FCode.equals("ProposalNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProposalNo));
		}
		if (FCode.equals("CounterId"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CounterId));
		}
		if (FCode.equals("BankCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
		}
		if (FCode.equals("SubBankCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SubBankCode));
		}
		if (FCode.equals("NodeBankCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(NodeBankCode));
		}
		if (FCode.equals("InsurerCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsurerCode));
		}
		if (FCode.equals("SubInsurerCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SubInsurerCode));
		}
		if (FCode.equals("Product_Name"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Product_Name));
		}
		if (FCode.equals("Product_Code"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Product_Code));
		}
		if (FCode.equals("BusinessNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusinessNo));
		}
		if (FCode.equals("CustomerName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerName));
		}
		if (FCode.equals("CustomerCardType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerCardType));
		}
		if (FCode.equals("CustomerCardNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerCardNo));
		}
		if (FCode.equals("CustomerBirthday"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerBirthday));
		}
		if (FCode.equals("VideoName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(VideoName));
		}
		if (FCode.equals("VideoType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(VideoType));
		}
		if (FCode.equals("VideoSize"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(VideoSize));
		}
		if (FCode.equals("BusinessSeriaNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusinessSeriaNo));
		}
		if (FCode.equals("BatchNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BatchNo));
		}
		if (FCode.equals("Content_MD5"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Content_MD5));
		}
		if (FCode.equals("bak1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak1));
		}
		if (FCode.equals("bak2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak2));
		}
		if (FCode.equals("bak3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak3));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(TranNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(ProposalNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(CounterId);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(BankCode);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(SubBankCode);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(NodeBankCode);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(InsurerCode);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(SubInsurerCode);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(Product_Name);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(Product_Code);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(BusinessNo);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(CustomerName);
				break;
			case 12:
				strFieldValue = String.valueOf(CustomerCardType);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(CustomerCardNo);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(CustomerBirthday);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(VideoName);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(VideoType);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(VideoSize);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(BusinessSeriaNo);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(BatchNo);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(Content_MD5);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(bak1);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(bak2);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(bak3);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("TranNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TranNo = FValue.trim();
			}
			else
				TranNo = null;
		}
		if (FCode.equalsIgnoreCase("ProposalNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProposalNo = FValue.trim();
			}
			else
				ProposalNo = null;
		}
		if (FCode.equalsIgnoreCase("CounterId"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CounterId = FValue.trim();
			}
			else
				CounterId = null;
		}
		if (FCode.equalsIgnoreCase("BankCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankCode = FValue.trim();
			}
			else
				BankCode = null;
		}
		if (FCode.equalsIgnoreCase("SubBankCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SubBankCode = FValue.trim();
			}
			else
				SubBankCode = null;
		}
		if (FCode.equalsIgnoreCase("NodeBankCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				NodeBankCode = FValue.trim();
			}
			else
				NodeBankCode = null;
		}
		if (FCode.equalsIgnoreCase("InsurerCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsurerCode = FValue.trim();
			}
			else
				InsurerCode = null;
		}
		if (FCode.equalsIgnoreCase("SubInsurerCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SubInsurerCode = FValue.trim();
			}
			else
				SubInsurerCode = null;
		}
		if (FCode.equalsIgnoreCase("Product_Name"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Product_Name = FValue.trim();
			}
			else
				Product_Name = null;
		}
		if (FCode.equalsIgnoreCase("Product_Code"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Product_Code = FValue.trim();
			}
			else
				Product_Code = null;
		}
		if (FCode.equalsIgnoreCase("BusinessNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusinessNo = FValue.trim();
			}
			else
				BusinessNo = null;
		}
		if (FCode.equalsIgnoreCase("CustomerName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerName = FValue.trim();
			}
			else
				CustomerName = null;
		}
		if (FCode.equalsIgnoreCase("CustomerCardType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				CustomerCardType = i;
			}
		}
		if (FCode.equalsIgnoreCase("CustomerCardNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerCardNo = FValue.trim();
			}
			else
				CustomerCardNo = null;
		}
		if (FCode.equalsIgnoreCase("CustomerBirthday"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerBirthday = FValue.trim();
			}
			else
				CustomerBirthday = null;
		}
		if (FCode.equalsIgnoreCase("VideoName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				VideoName = FValue.trim();
			}
			else
				VideoName = null;
		}
		if (FCode.equalsIgnoreCase("VideoType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				VideoType = FValue.trim();
			}
			else
				VideoType = null;
		}
		if (FCode.equalsIgnoreCase("VideoSize"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				VideoSize = FValue.trim();
			}
			else
				VideoSize = null;
		}
		if (FCode.equalsIgnoreCase("BusinessSeriaNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusinessSeriaNo = FValue.trim();
			}
			else
				BusinessSeriaNo = null;
		}
		if (FCode.equalsIgnoreCase("BatchNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BatchNo = FValue.trim();
			}
			else
				BatchNo = null;
		}
		if (FCode.equalsIgnoreCase("Content_MD5"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Content_MD5 = FValue.trim();
			}
			else
				Content_MD5 = null;
		}
		if (FCode.equalsIgnoreCase("bak1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak1 = FValue.trim();
			}
			else
				bak1 = null;
		}
		if (FCode.equalsIgnoreCase("bak2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak2 = FValue.trim();
			}
			else
				bak2 = null;
		}
		if (FCode.equalsIgnoreCase("bak3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak3 = FValue.trim();
			}
			else
				bak3 = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		Ciitc_TaskSchema other = (Ciitc_TaskSchema)otherObject;
		return
			(TranNo == null ? other.getTranNo() == null : TranNo.equals(other.getTranNo()))
			&& (ProposalNo == null ? other.getProposalNo() == null : ProposalNo.equals(other.getProposalNo()))
			&& (CounterId == null ? other.getCounterId() == null : CounterId.equals(other.getCounterId()))
			&& (BankCode == null ? other.getBankCode() == null : BankCode.equals(other.getBankCode()))
			&& (SubBankCode == null ? other.getSubBankCode() == null : SubBankCode.equals(other.getSubBankCode()))
			&& (NodeBankCode == null ? other.getNodeBankCode() == null : NodeBankCode.equals(other.getNodeBankCode()))
			&& (InsurerCode == null ? other.getInsurerCode() == null : InsurerCode.equals(other.getInsurerCode()))
			&& (SubInsurerCode == null ? other.getSubInsurerCode() == null : SubInsurerCode.equals(other.getSubInsurerCode()))
			&& (Product_Name == null ? other.getProduct_Name() == null : Product_Name.equals(other.getProduct_Name()))
			&& (Product_Code == null ? other.getProduct_Code() == null : Product_Code.equals(other.getProduct_Code()))
			&& (BusinessNo == null ? other.getBusinessNo() == null : BusinessNo.equals(other.getBusinessNo()))
			&& (CustomerName == null ? other.getCustomerName() == null : CustomerName.equals(other.getCustomerName()))
			&& CustomerCardType == other.getCustomerCardType()
			&& (CustomerCardNo == null ? other.getCustomerCardNo() == null : CustomerCardNo.equals(other.getCustomerCardNo()))
			&& (CustomerBirthday == null ? other.getCustomerBirthday() == null : CustomerBirthday.equals(other.getCustomerBirthday()))
			&& (VideoName == null ? other.getVideoName() == null : VideoName.equals(other.getVideoName()))
			&& (VideoType == null ? other.getVideoType() == null : VideoType.equals(other.getVideoType()))
			&& (VideoSize == null ? other.getVideoSize() == null : VideoSize.equals(other.getVideoSize()))
			&& (BusinessSeriaNo == null ? other.getBusinessSeriaNo() == null : BusinessSeriaNo.equals(other.getBusinessSeriaNo()))
			&& (BatchNo == null ? other.getBatchNo() == null : BatchNo.equals(other.getBatchNo()))
			&& (Content_MD5 == null ? other.getContent_MD5() == null : Content_MD5.equals(other.getContent_MD5()))
			&& (bak1 == null ? other.getbak1() == null : bak1.equals(other.getbak1()))
			&& (bak2 == null ? other.getbak2() == null : bak2.equals(other.getbak2()))
			&& (bak3 == null ? other.getbak3() == null : bak3.equals(other.getbak3()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("TranNo") ) {
			return 0;
		}
		if( strFieldName.equals("ProposalNo") ) {
			return 1;
		}
		if( strFieldName.equals("CounterId") ) {
			return 2;
		}
		if( strFieldName.equals("BankCode") ) {
			return 3;
		}
		if( strFieldName.equals("SubBankCode") ) {
			return 4;
		}
		if( strFieldName.equals("NodeBankCode") ) {
			return 5;
		}
		if( strFieldName.equals("InsurerCode") ) {
			return 6;
		}
		if( strFieldName.equals("SubInsurerCode") ) {
			return 7;
		}
		if( strFieldName.equals("Product_Name") ) {
			return 8;
		}
		if( strFieldName.equals("Product_Code") ) {
			return 9;
		}
		if( strFieldName.equals("BusinessNo") ) {
			return 10;
		}
		if( strFieldName.equals("CustomerName") ) {
			return 11;
		}
		if( strFieldName.equals("CustomerCardType") ) {
			return 12;
		}
		if( strFieldName.equals("CustomerCardNo") ) {
			return 13;
		}
		if( strFieldName.equals("CustomerBirthday") ) {
			return 14;
		}
		if( strFieldName.equals("VideoName") ) {
			return 15;
		}
		if( strFieldName.equals("VideoType") ) {
			return 16;
		}
		if( strFieldName.equals("VideoSize") ) {
			return 17;
		}
		if( strFieldName.equals("BusinessSeriaNo") ) {
			return 18;
		}
		if( strFieldName.equals("BatchNo") ) {
			return 19;
		}
		if( strFieldName.equals("Content_MD5") ) {
			return 20;
		}
		if( strFieldName.equals("bak1") ) {
			return 21;
		}
		if( strFieldName.equals("bak2") ) {
			return 22;
		}
		if( strFieldName.equals("bak3") ) {
			return 23;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "TranNo";
				break;
			case 1:
				strFieldName = "ProposalNo";
				break;
			case 2:
				strFieldName = "CounterId";
				break;
			case 3:
				strFieldName = "BankCode";
				break;
			case 4:
				strFieldName = "SubBankCode";
				break;
			case 5:
				strFieldName = "NodeBankCode";
				break;
			case 6:
				strFieldName = "InsurerCode";
				break;
			case 7:
				strFieldName = "SubInsurerCode";
				break;
			case 8:
				strFieldName = "Product_Name";
				break;
			case 9:
				strFieldName = "Product_Code";
				break;
			case 10:
				strFieldName = "BusinessNo";
				break;
			case 11:
				strFieldName = "CustomerName";
				break;
			case 12:
				strFieldName = "CustomerCardType";
				break;
			case 13:
				strFieldName = "CustomerCardNo";
				break;
			case 14:
				strFieldName = "CustomerBirthday";
				break;
			case 15:
				strFieldName = "VideoName";
				break;
			case 16:
				strFieldName = "VideoType";
				break;
			case 17:
				strFieldName = "VideoSize";
				break;
			case 18:
				strFieldName = "BusinessSeriaNo";
				break;
			case 19:
				strFieldName = "BatchNo";
				break;
			case 20:
				strFieldName = "Content_MD5";
				break;
			case 21:
				strFieldName = "bak1";
				break;
			case 22:
				strFieldName = "bak2";
				break;
			case 23:
				strFieldName = "bak3";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("TranNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ProposalNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CounterId") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SubBankCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("NodeBankCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsurerCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SubInsurerCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Product_Name") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Product_Code") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BusinessNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerCardType") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("CustomerCardNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerBirthday") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("VideoName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("VideoType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("VideoSize") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BusinessSeriaNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BatchNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Content_MD5") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak3") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_INT;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
