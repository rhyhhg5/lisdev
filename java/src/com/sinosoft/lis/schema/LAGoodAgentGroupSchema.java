/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LAGoodAgentGroupDB;

/*
 * <p>ClassName: LAGoodAgentGroupSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 在职人力
 * @CreateDate：2017-05-22
 */
public class LAGoodAgentGroupSchema implements Schema, Cloneable
{
	// @Field
	/** 考核年月 */
	private String YearMonth;
	/** 销售团队 */
	private String AgentGroup;
	/** 管理机构 */
	private String Managecom;
	/** 基础人力 */
	private int BaseMan;
	/** 基础离职人力 */
	private int DepartMan;
	/** 新增有效人力 */
	private int NewAddMan;
	/** 净增有效人力 */
	private int PrueAddMan;
	/** 奖励金额 */
	private double AwardMoney;
	/** 备用字段1 */
	private String F1;
	/** 备用字段2 */
	private double F2;
	/** 操作人 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 修改日期 */
	private Date ModifyDate;
	/** 修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 15;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LAGoodAgentGroupSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "YearMonth";
		pk[1] = "AgentGroup";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LAGoodAgentGroupSchema cloned = (LAGoodAgentGroupSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getYearMonth()
	{
		return YearMonth;
	}
	public void setYearMonth(String aYearMonth)
	{
		YearMonth = aYearMonth;
	}
	public String getAgentGroup()
	{
		return AgentGroup;
	}
	public void setAgentGroup(String aAgentGroup)
	{
		AgentGroup = aAgentGroup;
	}
	public String getManagecom()
	{
		return Managecom;
	}
	public void setManagecom(String aManagecom)
	{
		Managecom = aManagecom;
	}
	public int getBaseMan()
	{
		return BaseMan;
	}
	public void setBaseMan(int aBaseMan)
	{
		BaseMan = aBaseMan;
	}
	public void setBaseMan(String aBaseMan)
	{
		if (aBaseMan != null && !aBaseMan.equals(""))
		{
			Integer tInteger = new Integer(aBaseMan);
			int i = tInteger.intValue();
			BaseMan = i;
		}
	}

	public int getDepartMan()
	{
		return DepartMan;
	}
	public void setDepartMan(int aDepartMan)
	{
		DepartMan = aDepartMan;
	}
	public void setDepartMan(String aDepartMan)
	{
		if (aDepartMan != null && !aDepartMan.equals(""))
		{
			Integer tInteger = new Integer(aDepartMan);
			int i = tInteger.intValue();
			DepartMan = i;
		}
	}

	public int getNewAddMan()
	{
		return NewAddMan;
	}
	public void setNewAddMan(int aNewAddMan)
	{
		NewAddMan = aNewAddMan;
	}
	public void setNewAddMan(String aNewAddMan)
	{
		if (aNewAddMan != null && !aNewAddMan.equals(""))
		{
			Integer tInteger = new Integer(aNewAddMan);
			int i = tInteger.intValue();
			NewAddMan = i;
		}
	}

	public int getPrueAddMan()
	{
		return PrueAddMan;
	}
	public void setPrueAddMan(int aPrueAddMan)
	{
		PrueAddMan = aPrueAddMan;
	}
	public void setPrueAddMan(String aPrueAddMan)
	{
		if (aPrueAddMan != null && !aPrueAddMan.equals(""))
		{
			Integer tInteger = new Integer(aPrueAddMan);
			int i = tInteger.intValue();
			PrueAddMan = i;
		}
	}

	public double getAwardMoney()
	{
		return AwardMoney;
	}
	public void setAwardMoney(double aAwardMoney)
	{
		AwardMoney = Arith.round(aAwardMoney,2);
	}
	public void setAwardMoney(String aAwardMoney)
	{
		if (aAwardMoney != null && !aAwardMoney.equals(""))
		{
			Double tDouble = new Double(aAwardMoney);
			double d = tDouble.doubleValue();
                AwardMoney = Arith.round(d,2);
		}
	}

	public String getF1()
	{
		return F1;
	}
	public void setF1(String aF1)
	{
		F1 = aF1;
	}
	public double getF2()
	{
		return F2;
	}
	public void setF2(double aF2)
	{
		F2 = Arith.round(aF2,2);
	}
	public void setF2(String aF2)
	{
		if (aF2 != null && !aF2.equals(""))
		{
			Double tDouble = new Double(aF2);
			double d = tDouble.doubleValue();
                F2 = Arith.round(d,2);
		}
	}

	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LAGoodAgentGroupSchema 对象给 Schema 赋值
	* @param: aLAGoodAgentGroupSchema LAGoodAgentGroupSchema
	**/
	public void setSchema(LAGoodAgentGroupSchema aLAGoodAgentGroupSchema)
	{
		this.YearMonth = aLAGoodAgentGroupSchema.getYearMonth();
		this.AgentGroup = aLAGoodAgentGroupSchema.getAgentGroup();
		this.Managecom = aLAGoodAgentGroupSchema.getManagecom();
		this.BaseMan = aLAGoodAgentGroupSchema.getBaseMan();
		this.DepartMan = aLAGoodAgentGroupSchema.getDepartMan();
		this.NewAddMan = aLAGoodAgentGroupSchema.getNewAddMan();
		this.PrueAddMan = aLAGoodAgentGroupSchema.getPrueAddMan();
		this.AwardMoney = aLAGoodAgentGroupSchema.getAwardMoney();
		this.F1 = aLAGoodAgentGroupSchema.getF1();
		this.F2 = aLAGoodAgentGroupSchema.getF2();
		this.Operator = aLAGoodAgentGroupSchema.getOperator();
		this.MakeDate = fDate.getDate( aLAGoodAgentGroupSchema.getMakeDate());
		this.MakeTime = aLAGoodAgentGroupSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLAGoodAgentGroupSchema.getModifyDate());
		this.ModifyTime = aLAGoodAgentGroupSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("YearMonth") == null )
				this.YearMonth = null;
			else
				this.YearMonth = rs.getString("YearMonth").trim();

			if( rs.getString("AgentGroup") == null )
				this.AgentGroup = null;
			else
				this.AgentGroup = rs.getString("AgentGroup").trim();

			if( rs.getString("Managecom") == null )
				this.Managecom = null;
			else
				this.Managecom = rs.getString("Managecom").trim();

			this.BaseMan = rs.getInt("BaseMan");
			this.DepartMan = rs.getInt("DepartMan");
			this.NewAddMan = rs.getInt("NewAddMan");
			this.PrueAddMan = rs.getInt("PrueAddMan");
			this.AwardMoney = rs.getDouble("AwardMoney");
			if( rs.getString("F1") == null )
				this.F1 = null;
			else
				this.F1 = rs.getString("F1").trim();

			this.F2 = rs.getDouble("F2");
			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LAGoodAgentGroup表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAGoodAgentGroupSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LAGoodAgentGroupSchema getSchema()
	{
		LAGoodAgentGroupSchema aLAGoodAgentGroupSchema = new LAGoodAgentGroupSchema();
		aLAGoodAgentGroupSchema.setSchema(this);
		return aLAGoodAgentGroupSchema;
	}

	public LAGoodAgentGroupDB getDB()
	{
		LAGoodAgentGroupDB aDBOper = new LAGoodAgentGroupDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAGoodAgentGroup描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(YearMonth)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentGroup)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Managecom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(BaseMan));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(DepartMan));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(NewAddMan));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PrueAddMan));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(AwardMoney));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(F1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(F2));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAGoodAgentGroup>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			YearMonth = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			Managecom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			BaseMan= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,4,SysConst.PACKAGESPILTER))).intValue();
			DepartMan= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,5,SysConst.PACKAGESPILTER))).intValue();
			NewAddMan= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,6,SysConst.PACKAGESPILTER))).intValue();
			PrueAddMan= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,7,SysConst.PACKAGESPILTER))).intValue();
			AwardMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,8,SysConst.PACKAGESPILTER))).doubleValue();
			F1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			F2 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,10,SysConst.PACKAGESPILTER))).doubleValue();
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAGoodAgentGroupSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("YearMonth"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(YearMonth));
		}
		if (FCode.equals("AgentGroup"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
		}
		if (FCode.equals("Managecom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Managecom));
		}
		if (FCode.equals("BaseMan"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BaseMan));
		}
		if (FCode.equals("DepartMan"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DepartMan));
		}
		if (FCode.equals("NewAddMan"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(NewAddMan));
		}
		if (FCode.equals("PrueAddMan"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrueAddMan));
		}
		if (FCode.equals("AwardMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AwardMoney));
		}
		if (FCode.equals("F1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F1));
		}
		if (FCode.equals("F2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F2));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(YearMonth);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(AgentGroup);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(Managecom);
				break;
			case 3:
				strFieldValue = String.valueOf(BaseMan);
				break;
			case 4:
				strFieldValue = String.valueOf(DepartMan);
				break;
			case 5:
				strFieldValue = String.valueOf(NewAddMan);
				break;
			case 6:
				strFieldValue = String.valueOf(PrueAddMan);
				break;
			case 7:
				strFieldValue = String.valueOf(AwardMoney);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(F1);
				break;
			case 9:
				strFieldValue = String.valueOf(F2);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("YearMonth"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				YearMonth = FValue.trim();
			}
			else
				YearMonth = null;
		}
		if (FCode.equalsIgnoreCase("AgentGroup"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentGroup = FValue.trim();
			}
			else
				AgentGroup = null;
		}
		if (FCode.equalsIgnoreCase("Managecom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Managecom = FValue.trim();
			}
			else
				Managecom = null;
		}
		if (FCode.equalsIgnoreCase("BaseMan"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				BaseMan = i;
			}
		}
		if (FCode.equalsIgnoreCase("DepartMan"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				DepartMan = i;
			}
		}
		if (FCode.equalsIgnoreCase("NewAddMan"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				NewAddMan = i;
			}
		}
		if (FCode.equalsIgnoreCase("PrueAddMan"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PrueAddMan = i;
			}
		}
		if (FCode.equalsIgnoreCase("AwardMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				AwardMoney = d;
			}
		}
		if (FCode.equalsIgnoreCase("F1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				F1 = FValue.trim();
			}
			else
				F1 = null;
		}
		if (FCode.equalsIgnoreCase("F2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				F2 = d;
			}
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LAGoodAgentGroupSchema other = (LAGoodAgentGroupSchema)otherObject;
		return
			(YearMonth == null ? other.getYearMonth() == null : YearMonth.equals(other.getYearMonth()))
			&& (AgentGroup == null ? other.getAgentGroup() == null : AgentGroup.equals(other.getAgentGroup()))
			&& (Managecom == null ? other.getManagecom() == null : Managecom.equals(other.getManagecom()))
			&& BaseMan == other.getBaseMan()
			&& DepartMan == other.getDepartMan()
			&& NewAddMan == other.getNewAddMan()
			&& PrueAddMan == other.getPrueAddMan()
			&& AwardMoney == other.getAwardMoney()
			&& (F1 == null ? other.getF1() == null : F1.equals(other.getF1()))
			&& F2 == other.getF2()
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("YearMonth") ) {
			return 0;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return 1;
		}
		if( strFieldName.equals("Managecom") ) {
			return 2;
		}
		if( strFieldName.equals("BaseMan") ) {
			return 3;
		}
		if( strFieldName.equals("DepartMan") ) {
			return 4;
		}
		if( strFieldName.equals("NewAddMan") ) {
			return 5;
		}
		if( strFieldName.equals("PrueAddMan") ) {
			return 6;
		}
		if( strFieldName.equals("AwardMoney") ) {
			return 7;
		}
		if( strFieldName.equals("F1") ) {
			return 8;
		}
		if( strFieldName.equals("F2") ) {
			return 9;
		}
		if( strFieldName.equals("Operator") ) {
			return 10;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 11;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 12;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 13;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 14;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "YearMonth";
				break;
			case 1:
				strFieldName = "AgentGroup";
				break;
			case 2:
				strFieldName = "Managecom";
				break;
			case 3:
				strFieldName = "BaseMan";
				break;
			case 4:
				strFieldName = "DepartMan";
				break;
			case 5:
				strFieldName = "NewAddMan";
				break;
			case 6:
				strFieldName = "PrueAddMan";
				break;
			case 7:
				strFieldName = "AwardMoney";
				break;
			case 8:
				strFieldName = "F1";
				break;
			case 9:
				strFieldName = "F2";
				break;
			case 10:
				strFieldName = "Operator";
				break;
			case 11:
				strFieldName = "MakeDate";
				break;
			case 12:
				strFieldName = "MakeTime";
				break;
			case 13:
				strFieldName = "ModifyDate";
				break;
			case 14:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("YearMonth") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Managecom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BaseMan") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("DepartMan") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("NewAddMan") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("PrueAddMan") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("AwardMoney") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("F1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("F2") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_INT;
				break;
			case 4:
				nFieldType = Schema.TYPE_INT;
				break;
			case 5:
				nFieldType = Schema.TYPE_INT;
				break;
			case 6:
				nFieldType = Schema.TYPE_INT;
				break;
			case 7:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
