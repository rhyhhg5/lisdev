/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LDOrganiSubjecDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

/*
 * <p>ClassName: LDOrganiSubjecSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_3
 * @CreateDate：2005-03-03
 */
public class LDOrganiSubjecSchema implements Schema
{
    // @Field
    /** 单位隶属关系代码 */
    private String SubjecCode;
    /** 单位隶属关系 */
    private String SubjecName;

    public static final int FIELDNUM = 2; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LDOrganiSubjecSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "SubjecCode";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getSubjecCode()
    {
        if (SubjecCode != null && !SubjecCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            SubjecCode = StrTool.unicodeToGBK(SubjecCode);
        }
        return SubjecCode;
    }

    public void setSubjecCode(String aSubjecCode)
    {
        SubjecCode = aSubjecCode;
    }

    public String getSubjecName()
    {
        if (SubjecName != null && !SubjecName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            SubjecName = StrTool.unicodeToGBK(SubjecName);
        }
        return SubjecName;
    }

    public void setSubjecName(String aSubjecName)
    {
        SubjecName = aSubjecName;
    }

    /**
     * 使用另外一个 LDOrganiSubjecSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LDOrganiSubjecSchema aLDOrganiSubjecSchema)
    {
        this.SubjecCode = aLDOrganiSubjecSchema.getSubjecCode();
        this.SubjecName = aLDOrganiSubjecSchema.getSubjecName();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("SubjecCode") == null)
            {
                this.SubjecCode = null;
            }
            else
            {
                this.SubjecCode = rs.getString("SubjecCode").trim();
            }

            if (rs.getString("SubjecName") == null)
            {
                this.SubjecName = null;
            }
            else
            {
                this.SubjecName = rs.getString("SubjecName").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDOrganiSubjecSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LDOrganiSubjecSchema getSchema()
    {
        LDOrganiSubjecSchema aLDOrganiSubjecSchema = new LDOrganiSubjecSchema();
        aLDOrganiSubjecSchema.setSchema(this);
        return aLDOrganiSubjecSchema;
    }

    public LDOrganiSubjecDB getDB()
    {
        LDOrganiSubjecDB aDBOper = new LDOrganiSubjecDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDOrganiSubjec描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(SubjecCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SubjecName));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDOrganiSubjec>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            SubjecCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                        SysConst.PACKAGESPILTER);
            SubjecName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDOrganiSubjecSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("SubjecCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SubjecCode));
        }
        if (FCode.equals("SubjecName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SubjecName));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(SubjecCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(SubjecName);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("SubjecCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SubjecCode = FValue.trim();
            }
            else
            {
                SubjecCode = null;
            }
        }
        if (FCode.equals("SubjecName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SubjecName = FValue.trim();
            }
            else
            {
                SubjecName = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LDOrganiSubjecSchema other = (LDOrganiSubjecSchema) otherObject;
        return
                SubjecCode.equals(other.getSubjecCode())
                && SubjecName.equals(other.getSubjecName());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("SubjecCode"))
        {
            return 0;
        }
        if (strFieldName.equals("SubjecName"))
        {
            return 1;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "SubjecCode";
                break;
            case 1:
                strFieldName = "SubjecName";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("SubjecCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SubjecName"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
