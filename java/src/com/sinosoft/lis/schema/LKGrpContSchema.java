/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LKGrpContDB;

/*
 * <p>ClassName: LKGrpContSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 在线报价系统模型
 * @CreateDate：2019-09-02
 */
public class LKGrpContSchema implements Schema, Cloneable
{
	// @Field
	/** 报价单号 */
	private String PrtNo;
	/** 创建时间 */
	private Date PolApplyDate;
	/** 管理机构 */
	private String ManageCom;
	/** 销售渠道 */
	private String SaleChnl;
	/** 业务员代码 */
	private String AgentCode;
	/** 代理人组别 */
	private String AgentGroup;
	/** 中介公司代码 */
	private String AgentCom;
	/** 合同生效日期 */
	private Date CValiDate;
	/** 合同终止日期 */
	private Date CInValiDate;
	/** 共保标志 */
	private String CoInsuranceFlag;
	/** 业务类型 */
	private String BusType;
	/** 业务提奖 */
	private String BusineAwards;
	/** 中介手续费率 */
	private String IntermediaryFeerate;
	/** 投保类型 */
	private String InsuType;
	/** 参保形式 */
	private String InsuForm;
	/** 单位名称 */
	private String GrpName;
	/** 客户号码 */
	private String AppntNo;
	/** 行业 */
	private String BusinessType;
	/** 总人数 */
	private int Peoples;
	/** 被保险人数 */
	private int Peoples3;
	/** 在职人数 */
	private int OnWorkPeoples;
	/** 退休人数 */
	private int OffWorkPeoples;
	/** 其它人员人数 */
	private int OtherPeoples;
	/** 连带被保险人数 */
	private int RelaPeoples;
	/** 配偶人数 */
	private int RelaMatePeoples;
	/** 子女人数 */
	private int RelaYoungPeoples;
	/** 其他人员人数 */
	private int RelaOtherPeoples;
	/** 单位电话 */
	private String Phone;
	/** 总保费 */
	private double Prem;
	/** 总保额 */
	private double Amnt;
	/** 录单人 */
	private String InputOperator;
	/** 录单完成日期 */
	private Date InputDate;
	/** 录单完成时间 */
	private String InputTime;
	/** 核保人 */
	private String UWOperator;
	/** 核保状态 */
	private String UWFlag;
	/** 核保完成日期 */
	private Date UWDate;
	/** 核保完成时间 */
	private String UWTime;
	/** 保单状态 */
	private String StateFlag;
	/** 单位性质 */
	private String GrpNature;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 44;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LKGrpContSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "PrtNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LKGrpContSchema cloned = (LKGrpContSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getPrtNo()
	{
		return PrtNo;
	}
	public void setPrtNo(String aPrtNo)
	{
		PrtNo = aPrtNo;
	}
	public String getPolApplyDate()
	{
		if( PolApplyDate != null )
			return fDate.getString(PolApplyDate);
		else
			return null;
	}
	public void setPolApplyDate(Date aPolApplyDate)
	{
		PolApplyDate = aPolApplyDate;
	}
	public void setPolApplyDate(String aPolApplyDate)
	{
		if (aPolApplyDate != null && !aPolApplyDate.equals("") )
		{
			PolApplyDate = fDate.getDate( aPolApplyDate );
		}
		else
			PolApplyDate = null;
	}

	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getSaleChnl()
	{
		return SaleChnl;
	}
	public void setSaleChnl(String aSaleChnl)
	{
		SaleChnl = aSaleChnl;
	}
	public String getAgentCode()
	{
		return AgentCode;
	}
	public void setAgentCode(String aAgentCode)
	{
		AgentCode = aAgentCode;
	}
	public String getAgentGroup()
	{
		return AgentGroup;
	}
	public void setAgentGroup(String aAgentGroup)
	{
		AgentGroup = aAgentGroup;
	}
	public String getAgentCom()
	{
		return AgentCom;
	}
	public void setAgentCom(String aAgentCom)
	{
		AgentCom = aAgentCom;
	}
	public String getCValiDate()
	{
		if( CValiDate != null )
			return fDate.getString(CValiDate);
		else
			return null;
	}
	public void setCValiDate(Date aCValiDate)
	{
		CValiDate = aCValiDate;
	}
	public void setCValiDate(String aCValiDate)
	{
		if (aCValiDate != null && !aCValiDate.equals("") )
		{
			CValiDate = fDate.getDate( aCValiDate );
		}
		else
			CValiDate = null;
	}

	public String getCInValiDate()
	{
		if( CInValiDate != null )
			return fDate.getString(CInValiDate);
		else
			return null;
	}
	public void setCInValiDate(Date aCInValiDate)
	{
		CInValiDate = aCInValiDate;
	}
	public void setCInValiDate(String aCInValiDate)
	{
		if (aCInValiDate != null && !aCInValiDate.equals("") )
		{
			CInValiDate = fDate.getDate( aCInValiDate );
		}
		else
			CInValiDate = null;
	}

	public String getCoInsuranceFlag()
	{
		return CoInsuranceFlag;
	}
	public void setCoInsuranceFlag(String aCoInsuranceFlag)
	{
		CoInsuranceFlag = aCoInsuranceFlag;
	}
	public String getBusType()
	{
		return BusType;
	}
	public void setBusType(String aBusType)
	{
		BusType = aBusType;
	}
	public String getBusineAwards()
	{
		return BusineAwards;
	}
	public void setBusineAwards(String aBusineAwards)
	{
		BusineAwards = aBusineAwards;
	}
	public String getIntermediaryFeerate()
	{
		return IntermediaryFeerate;
	}
	public void setIntermediaryFeerate(String aIntermediaryFeerate)
	{
		IntermediaryFeerate = aIntermediaryFeerate;
	}
	public String getInsuType()
	{
		return InsuType;
	}
	public void setInsuType(String aInsuType)
	{
		InsuType = aInsuType;
	}
	public String getInsuForm()
	{
		return InsuForm;
	}
	public void setInsuForm(String aInsuForm)
	{
		InsuForm = aInsuForm;
	}
	public String getGrpName()
	{
		return GrpName;
	}
	public void setGrpName(String aGrpName)
	{
		GrpName = aGrpName;
	}
	public String getAppntNo()
	{
		return AppntNo;
	}
	public void setAppntNo(String aAppntNo)
	{
		AppntNo = aAppntNo;
	}
	public String getBusinessType()
	{
		return BusinessType;
	}
	public void setBusinessType(String aBusinessType)
	{
		BusinessType = aBusinessType;
	}
	public int getPeoples()
	{
		return Peoples;
	}
	public void setPeoples(int aPeoples)
	{
		Peoples = aPeoples;
	}
	public void setPeoples(String aPeoples)
	{
		if (aPeoples != null && !aPeoples.equals(""))
		{
			Integer tInteger = new Integer(aPeoples);
			int i = tInteger.intValue();
			Peoples = i;
		}
	}

	public int getPeoples3()
	{
		return Peoples3;
	}
	public void setPeoples3(int aPeoples3)
	{
		Peoples3 = aPeoples3;
	}
	public void setPeoples3(String aPeoples3)
	{
		if (aPeoples3 != null && !aPeoples3.equals(""))
		{
			Integer tInteger = new Integer(aPeoples3);
			int i = tInteger.intValue();
			Peoples3 = i;
		}
	}

	public int getOnWorkPeoples()
	{
		return OnWorkPeoples;
	}
	public void setOnWorkPeoples(int aOnWorkPeoples)
	{
		OnWorkPeoples = aOnWorkPeoples;
	}
	public void setOnWorkPeoples(String aOnWorkPeoples)
	{
		if (aOnWorkPeoples != null && !aOnWorkPeoples.equals(""))
		{
			Integer tInteger = new Integer(aOnWorkPeoples);
			int i = tInteger.intValue();
			OnWorkPeoples = i;
		}
	}

	public int getOffWorkPeoples()
	{
		return OffWorkPeoples;
	}
	public void setOffWorkPeoples(int aOffWorkPeoples)
	{
		OffWorkPeoples = aOffWorkPeoples;
	}
	public void setOffWorkPeoples(String aOffWorkPeoples)
	{
		if (aOffWorkPeoples != null && !aOffWorkPeoples.equals(""))
		{
			Integer tInteger = new Integer(aOffWorkPeoples);
			int i = tInteger.intValue();
			OffWorkPeoples = i;
		}
	}

	public int getOtherPeoples()
	{
		return OtherPeoples;
	}
	public void setOtherPeoples(int aOtherPeoples)
	{
		OtherPeoples = aOtherPeoples;
	}
	public void setOtherPeoples(String aOtherPeoples)
	{
		if (aOtherPeoples != null && !aOtherPeoples.equals(""))
		{
			Integer tInteger = new Integer(aOtherPeoples);
			int i = tInteger.intValue();
			OtherPeoples = i;
		}
	}

	public int getRelaPeoples()
	{
		return RelaPeoples;
	}
	public void setRelaPeoples(int aRelaPeoples)
	{
		RelaPeoples = aRelaPeoples;
	}
	public void setRelaPeoples(String aRelaPeoples)
	{
		if (aRelaPeoples != null && !aRelaPeoples.equals(""))
		{
			Integer tInteger = new Integer(aRelaPeoples);
			int i = tInteger.intValue();
			RelaPeoples = i;
		}
	}

	public int getRelaMatePeoples()
	{
		return RelaMatePeoples;
	}
	public void setRelaMatePeoples(int aRelaMatePeoples)
	{
		RelaMatePeoples = aRelaMatePeoples;
	}
	public void setRelaMatePeoples(String aRelaMatePeoples)
	{
		if (aRelaMatePeoples != null && !aRelaMatePeoples.equals(""))
		{
			Integer tInteger = new Integer(aRelaMatePeoples);
			int i = tInteger.intValue();
			RelaMatePeoples = i;
		}
	}

	public int getRelaYoungPeoples()
	{
		return RelaYoungPeoples;
	}
	public void setRelaYoungPeoples(int aRelaYoungPeoples)
	{
		RelaYoungPeoples = aRelaYoungPeoples;
	}
	public void setRelaYoungPeoples(String aRelaYoungPeoples)
	{
		if (aRelaYoungPeoples != null && !aRelaYoungPeoples.equals(""))
		{
			Integer tInteger = new Integer(aRelaYoungPeoples);
			int i = tInteger.intValue();
			RelaYoungPeoples = i;
		}
	}

	public int getRelaOtherPeoples()
	{
		return RelaOtherPeoples;
	}
	public void setRelaOtherPeoples(int aRelaOtherPeoples)
	{
		RelaOtherPeoples = aRelaOtherPeoples;
	}
	public void setRelaOtherPeoples(String aRelaOtherPeoples)
	{
		if (aRelaOtherPeoples != null && !aRelaOtherPeoples.equals(""))
		{
			Integer tInteger = new Integer(aRelaOtherPeoples);
			int i = tInteger.intValue();
			RelaOtherPeoples = i;
		}
	}

	public String getPhone()
	{
		return Phone;
	}
	public void setPhone(String aPhone)
	{
		Phone = aPhone;
	}
	public double getPrem()
	{
		return Prem;
	}
	public void setPrem(double aPrem)
	{
		Prem = Arith.round(aPrem,2);
	}
	public void setPrem(String aPrem)
	{
		if (aPrem != null && !aPrem.equals(""))
		{
			Double tDouble = new Double(aPrem);
			double d = tDouble.doubleValue();
                Prem = Arith.round(d,2);
		}
	}

	public double getAmnt()
	{
		return Amnt;
	}
	public void setAmnt(double aAmnt)
	{
		Amnt = Arith.round(aAmnt,2);
	}
	public void setAmnt(String aAmnt)
	{
		if (aAmnt != null && !aAmnt.equals(""))
		{
			Double tDouble = new Double(aAmnt);
			double d = tDouble.doubleValue();
                Amnt = Arith.round(d,2);
		}
	}

	public String getInputOperator()
	{
		return InputOperator;
	}
	public void setInputOperator(String aInputOperator)
	{
		InputOperator = aInputOperator;
	}
	public String getInputDate()
	{
		if( InputDate != null )
			return fDate.getString(InputDate);
		else
			return null;
	}
	public void setInputDate(Date aInputDate)
	{
		InputDate = aInputDate;
	}
	public void setInputDate(String aInputDate)
	{
		if (aInputDate != null && !aInputDate.equals("") )
		{
			InputDate = fDate.getDate( aInputDate );
		}
		else
			InputDate = null;
	}

	public String getInputTime()
	{
		return InputTime;
	}
	public void setInputTime(String aInputTime)
	{
		InputTime = aInputTime;
	}
	public String getUWOperator()
	{
		return UWOperator;
	}
	public void setUWOperator(String aUWOperator)
	{
		UWOperator = aUWOperator;
	}
	public String getUWFlag()
	{
		return UWFlag;
	}
	public void setUWFlag(String aUWFlag)
	{
		UWFlag = aUWFlag;
	}
	public String getUWDate()
	{
		if( UWDate != null )
			return fDate.getString(UWDate);
		else
			return null;
	}
	public void setUWDate(Date aUWDate)
	{
		UWDate = aUWDate;
	}
	public void setUWDate(String aUWDate)
	{
		if (aUWDate != null && !aUWDate.equals("") )
		{
			UWDate = fDate.getDate( aUWDate );
		}
		else
			UWDate = null;
	}

	public String getUWTime()
	{
		return UWTime;
	}
	public void setUWTime(String aUWTime)
	{
		UWTime = aUWTime;
	}
	public String getStateFlag()
	{
		return StateFlag;
	}
	public void setStateFlag(String aStateFlag)
	{
		StateFlag = aStateFlag;
	}
	public String getGrpNature()
	{
		return GrpNature;
	}
	public void setGrpNature(String aGrpNature)
	{
		GrpNature = aGrpNature;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LKGrpContSchema 对象给 Schema 赋值
	* @param: aLKGrpContSchema LKGrpContSchema
	**/
	public void setSchema(LKGrpContSchema aLKGrpContSchema)
	{
		this.PrtNo = aLKGrpContSchema.getPrtNo();
		this.PolApplyDate = fDate.getDate( aLKGrpContSchema.getPolApplyDate());
		this.ManageCom = aLKGrpContSchema.getManageCom();
		this.SaleChnl = aLKGrpContSchema.getSaleChnl();
		this.AgentCode = aLKGrpContSchema.getAgentCode();
		this.AgentGroup = aLKGrpContSchema.getAgentGroup();
		this.AgentCom = aLKGrpContSchema.getAgentCom();
		this.CValiDate = fDate.getDate( aLKGrpContSchema.getCValiDate());
		this.CInValiDate = fDate.getDate( aLKGrpContSchema.getCInValiDate());
		this.CoInsuranceFlag = aLKGrpContSchema.getCoInsuranceFlag();
		this.BusType = aLKGrpContSchema.getBusType();
		this.BusineAwards = aLKGrpContSchema.getBusineAwards();
		this.IntermediaryFeerate = aLKGrpContSchema.getIntermediaryFeerate();
		this.InsuType = aLKGrpContSchema.getInsuType();
		this.InsuForm = aLKGrpContSchema.getInsuForm();
		this.GrpName = aLKGrpContSchema.getGrpName();
		this.AppntNo = aLKGrpContSchema.getAppntNo();
		this.BusinessType = aLKGrpContSchema.getBusinessType();
		this.Peoples = aLKGrpContSchema.getPeoples();
		this.Peoples3 = aLKGrpContSchema.getPeoples3();
		this.OnWorkPeoples = aLKGrpContSchema.getOnWorkPeoples();
		this.OffWorkPeoples = aLKGrpContSchema.getOffWorkPeoples();
		this.OtherPeoples = aLKGrpContSchema.getOtherPeoples();
		this.RelaPeoples = aLKGrpContSchema.getRelaPeoples();
		this.RelaMatePeoples = aLKGrpContSchema.getRelaMatePeoples();
		this.RelaYoungPeoples = aLKGrpContSchema.getRelaYoungPeoples();
		this.RelaOtherPeoples = aLKGrpContSchema.getRelaOtherPeoples();
		this.Phone = aLKGrpContSchema.getPhone();
		this.Prem = aLKGrpContSchema.getPrem();
		this.Amnt = aLKGrpContSchema.getAmnt();
		this.InputOperator = aLKGrpContSchema.getInputOperator();
		this.InputDate = fDate.getDate( aLKGrpContSchema.getInputDate());
		this.InputTime = aLKGrpContSchema.getInputTime();
		this.UWOperator = aLKGrpContSchema.getUWOperator();
		this.UWFlag = aLKGrpContSchema.getUWFlag();
		this.UWDate = fDate.getDate( aLKGrpContSchema.getUWDate());
		this.UWTime = aLKGrpContSchema.getUWTime();
		this.StateFlag = aLKGrpContSchema.getStateFlag();
		this.GrpNature = aLKGrpContSchema.getGrpNature();
		this.Operator = aLKGrpContSchema.getOperator();
		this.MakeDate = fDate.getDate( aLKGrpContSchema.getMakeDate());
		this.MakeTime = aLKGrpContSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLKGrpContSchema.getModifyDate());
		this.ModifyTime = aLKGrpContSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("PrtNo") == null )
				this.PrtNo = null;
			else
				this.PrtNo = rs.getString("PrtNo").trim();

			this.PolApplyDate = rs.getDate("PolApplyDate");
			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("SaleChnl") == null )
				this.SaleChnl = null;
			else
				this.SaleChnl = rs.getString("SaleChnl").trim();

			if( rs.getString("AgentCode") == null )
				this.AgentCode = null;
			else
				this.AgentCode = rs.getString("AgentCode").trim();

			if( rs.getString("AgentGroup") == null )
				this.AgentGroup = null;
			else
				this.AgentGroup = rs.getString("AgentGroup").trim();

			if( rs.getString("AgentCom") == null )
				this.AgentCom = null;
			else
				this.AgentCom = rs.getString("AgentCom").trim();

			this.CValiDate = rs.getDate("CValiDate");
			this.CInValiDate = rs.getDate("CInValiDate");
			if( rs.getString("CoInsuranceFlag") == null )
				this.CoInsuranceFlag = null;
			else
				this.CoInsuranceFlag = rs.getString("CoInsuranceFlag").trim();

			if( rs.getString("BusType") == null )
				this.BusType = null;
			else
				this.BusType = rs.getString("BusType").trim();

			if( rs.getString("BusineAwards") == null )
				this.BusineAwards = null;
			else
				this.BusineAwards = rs.getString("BusineAwards").trim();

			if( rs.getString("IntermediaryFeerate") == null )
				this.IntermediaryFeerate = null;
			else
				this.IntermediaryFeerate = rs.getString("IntermediaryFeerate").trim();

			if( rs.getString("InsuType") == null )
				this.InsuType = null;
			else
				this.InsuType = rs.getString("InsuType").trim();

			if( rs.getString("InsuForm") == null )
				this.InsuForm = null;
			else
				this.InsuForm = rs.getString("InsuForm").trim();

			if( rs.getString("GrpName") == null )
				this.GrpName = null;
			else
				this.GrpName = rs.getString("GrpName").trim();

			if( rs.getString("AppntNo") == null )
				this.AppntNo = null;
			else
				this.AppntNo = rs.getString("AppntNo").trim();

			if( rs.getString("BusinessType") == null )
				this.BusinessType = null;
			else
				this.BusinessType = rs.getString("BusinessType").trim();

			this.Peoples = rs.getInt("Peoples");
			this.Peoples3 = rs.getInt("Peoples3");
			this.OnWorkPeoples = rs.getInt("OnWorkPeoples");
			this.OffWorkPeoples = rs.getInt("OffWorkPeoples");
			this.OtherPeoples = rs.getInt("OtherPeoples");
			this.RelaPeoples = rs.getInt("RelaPeoples");
			this.RelaMatePeoples = rs.getInt("RelaMatePeoples");
			this.RelaYoungPeoples = rs.getInt("RelaYoungPeoples");
			this.RelaOtherPeoples = rs.getInt("RelaOtherPeoples");
			if( rs.getString("Phone") == null )
				this.Phone = null;
			else
				this.Phone = rs.getString("Phone").trim();

			this.Prem = rs.getDouble("Prem");
			this.Amnt = rs.getDouble("Amnt");
			if( rs.getString("InputOperator") == null )
				this.InputOperator = null;
			else
				this.InputOperator = rs.getString("InputOperator").trim();

			this.InputDate = rs.getDate("InputDate");
			if( rs.getString("InputTime") == null )
				this.InputTime = null;
			else
				this.InputTime = rs.getString("InputTime").trim();

			if( rs.getString("UWOperator") == null )
				this.UWOperator = null;
			else
				this.UWOperator = rs.getString("UWOperator").trim();

			if( rs.getString("UWFlag") == null )
				this.UWFlag = null;
			else
				this.UWFlag = rs.getString("UWFlag").trim();

			this.UWDate = rs.getDate("UWDate");
			if( rs.getString("UWTime") == null )
				this.UWTime = null;
			else
				this.UWTime = rs.getString("UWTime").trim();

			if( rs.getString("StateFlag") == null )
				this.StateFlag = null;
			else
				this.StateFlag = rs.getString("StateFlag").trim();

			if( rs.getString("GrpNature") == null )
				this.GrpNature = null;
			else
				this.GrpNature = rs.getString("GrpNature").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LKGrpCont表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LKGrpContSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LKGrpContSchema getSchema()
	{
		LKGrpContSchema aLKGrpContSchema = new LKGrpContSchema();
		aLKGrpContSchema.setSchema(this);
		return aLKGrpContSchema;
	}

	public LKGrpContDB getDB()
	{
		LKGrpContDB aDBOper = new LKGrpContDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLKGrpCont描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(PrtNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( PolApplyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SaleChnl)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentGroup)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CValiDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CInValiDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CoInsuranceFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusineAwards)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IntermediaryFeerate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuForm)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppntNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusinessType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Peoples));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Peoples3));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(OnWorkPeoples));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(OffWorkPeoples));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(OtherPeoples));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(RelaPeoples));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(RelaMatePeoples));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(RelaYoungPeoples));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(RelaOtherPeoples));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Phone)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Prem));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Amnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InputOperator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( InputDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InputTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UWOperator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UWFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( UWDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UWTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StateFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpNature)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLKGrpCont>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			PolApplyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,SysConst.PACKAGESPILTER));
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			AgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			CValiDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,SysConst.PACKAGESPILTER));
			CInValiDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,SysConst.PACKAGESPILTER));
			CoInsuranceFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			BusType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			BusineAwards = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			IntermediaryFeerate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			InsuType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			InsuForm = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			GrpName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			AppntNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			BusinessType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			Peoples= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,19,SysConst.PACKAGESPILTER))).intValue();
			Peoples3= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,20,SysConst.PACKAGESPILTER))).intValue();
			OnWorkPeoples= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,21,SysConst.PACKAGESPILTER))).intValue();
			OffWorkPeoples= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,22,SysConst.PACKAGESPILTER))).intValue();
			OtherPeoples= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,23,SysConst.PACKAGESPILTER))).intValue();
			RelaPeoples= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,24,SysConst.PACKAGESPILTER))).intValue();
			RelaMatePeoples= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,25,SysConst.PACKAGESPILTER))).intValue();
			RelaYoungPeoples= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,26,SysConst.PACKAGESPILTER))).intValue();
			RelaOtherPeoples= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,27,SysConst.PACKAGESPILTER))).intValue();
			Phone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			Prem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,29,SysConst.PACKAGESPILTER))).doubleValue();
			Amnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,30,SysConst.PACKAGESPILTER))).doubleValue();
			InputOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			InputDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32,SysConst.PACKAGESPILTER));
			InputTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			UWOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			UWFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			UWDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36,SysConst.PACKAGESPILTER));
			UWTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
			StateFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
			GrpNature = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LKGrpContSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("PrtNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
		}
		if (FCode.equals("PolApplyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getPolApplyDate()));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("SaleChnl"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
		}
		if (FCode.equals("AgentCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
		}
		if (FCode.equals("AgentGroup"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
		}
		if (FCode.equals("AgentCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
		}
		if (FCode.equals("CValiDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCValiDate()));
		}
		if (FCode.equals("CInValiDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCInValiDate()));
		}
		if (FCode.equals("CoInsuranceFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CoInsuranceFlag));
		}
		if (FCode.equals("BusType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusType));
		}
		if (FCode.equals("BusineAwards"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusineAwards));
		}
		if (FCode.equals("IntermediaryFeerate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IntermediaryFeerate));
		}
		if (FCode.equals("InsuType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuType));
		}
		if (FCode.equals("InsuForm"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuForm));
		}
		if (FCode.equals("GrpName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpName));
		}
		if (FCode.equals("AppntNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
		}
		if (FCode.equals("BusinessType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusinessType));
		}
		if (FCode.equals("Peoples"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Peoples));
		}
		if (FCode.equals("Peoples3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Peoples3));
		}
		if (FCode.equals("OnWorkPeoples"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OnWorkPeoples));
		}
		if (FCode.equals("OffWorkPeoples"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OffWorkPeoples));
		}
		if (FCode.equals("OtherPeoples"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OtherPeoples));
		}
		if (FCode.equals("RelaPeoples"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RelaPeoples));
		}
		if (FCode.equals("RelaMatePeoples"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RelaMatePeoples));
		}
		if (FCode.equals("RelaYoungPeoples"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RelaYoungPeoples));
		}
		if (FCode.equals("RelaOtherPeoples"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RelaOtherPeoples));
		}
		if (FCode.equals("Phone"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Phone));
		}
		if (FCode.equals("Prem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Prem));
		}
		if (FCode.equals("Amnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Amnt));
		}
		if (FCode.equals("InputOperator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InputOperator));
		}
		if (FCode.equals("InputDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getInputDate()));
		}
		if (FCode.equals("InputTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InputTime));
		}
		if (FCode.equals("UWOperator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UWOperator));
		}
		if (FCode.equals("UWFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UWFlag));
		}
		if (FCode.equals("UWDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getUWDate()));
		}
		if (FCode.equals("UWTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UWTime));
		}
		if (FCode.equals("StateFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StateFlag));
		}
		if (FCode.equals("GrpNature"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpNature));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(PrtNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getPolApplyDate()));
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(SaleChnl);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(AgentCode);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(AgentGroup);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(AgentCom);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCValiDate()));
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCInValiDate()));
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(CoInsuranceFlag);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(BusType);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(BusineAwards);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(IntermediaryFeerate);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(InsuType);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(InsuForm);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(GrpName);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(AppntNo);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(BusinessType);
				break;
			case 18:
				strFieldValue = String.valueOf(Peoples);
				break;
			case 19:
				strFieldValue = String.valueOf(Peoples3);
				break;
			case 20:
				strFieldValue = String.valueOf(OnWorkPeoples);
				break;
			case 21:
				strFieldValue = String.valueOf(OffWorkPeoples);
				break;
			case 22:
				strFieldValue = String.valueOf(OtherPeoples);
				break;
			case 23:
				strFieldValue = String.valueOf(RelaPeoples);
				break;
			case 24:
				strFieldValue = String.valueOf(RelaMatePeoples);
				break;
			case 25:
				strFieldValue = String.valueOf(RelaYoungPeoples);
				break;
			case 26:
				strFieldValue = String.valueOf(RelaOtherPeoples);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(Phone);
				break;
			case 28:
				strFieldValue = String.valueOf(Prem);
				break;
			case 29:
				strFieldValue = String.valueOf(Amnt);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(InputOperator);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getInputDate()));
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(InputTime);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(UWOperator);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(UWFlag);
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getUWDate()));
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(UWTime);
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(StateFlag);
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(GrpNature);
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 40:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 41:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 42:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 43:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("PrtNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PrtNo = FValue.trim();
			}
			else
				PrtNo = null;
		}
		if (FCode.equalsIgnoreCase("PolApplyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				PolApplyDate = fDate.getDate( FValue );
			}
			else
				PolApplyDate = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("SaleChnl"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SaleChnl = FValue.trim();
			}
			else
				SaleChnl = null;
		}
		if (FCode.equalsIgnoreCase("AgentCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCode = FValue.trim();
			}
			else
				AgentCode = null;
		}
		if (FCode.equalsIgnoreCase("AgentGroup"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentGroup = FValue.trim();
			}
			else
				AgentGroup = null;
		}
		if (FCode.equalsIgnoreCase("AgentCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCom = FValue.trim();
			}
			else
				AgentCom = null;
		}
		if (FCode.equalsIgnoreCase("CValiDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CValiDate = fDate.getDate( FValue );
			}
			else
				CValiDate = null;
		}
		if (FCode.equalsIgnoreCase("CInValiDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CInValiDate = fDate.getDate( FValue );
			}
			else
				CInValiDate = null;
		}
		if (FCode.equalsIgnoreCase("CoInsuranceFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CoInsuranceFlag = FValue.trim();
			}
			else
				CoInsuranceFlag = null;
		}
		if (FCode.equalsIgnoreCase("BusType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusType = FValue.trim();
			}
			else
				BusType = null;
		}
		if (FCode.equalsIgnoreCase("BusineAwards"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusineAwards = FValue.trim();
			}
			else
				BusineAwards = null;
		}
		if (FCode.equalsIgnoreCase("IntermediaryFeerate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IntermediaryFeerate = FValue.trim();
			}
			else
				IntermediaryFeerate = null;
		}
		if (FCode.equalsIgnoreCase("InsuType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuType = FValue.trim();
			}
			else
				InsuType = null;
		}
		if (FCode.equalsIgnoreCase("InsuForm"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuForm = FValue.trim();
			}
			else
				InsuForm = null;
		}
		if (FCode.equalsIgnoreCase("GrpName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpName = FValue.trim();
			}
			else
				GrpName = null;
		}
		if (FCode.equalsIgnoreCase("AppntNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppntNo = FValue.trim();
			}
			else
				AppntNo = null;
		}
		if (FCode.equalsIgnoreCase("BusinessType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusinessType = FValue.trim();
			}
			else
				BusinessType = null;
		}
		if (FCode.equalsIgnoreCase("Peoples"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				Peoples = i;
			}
		}
		if (FCode.equalsIgnoreCase("Peoples3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				Peoples3 = i;
			}
		}
		if (FCode.equalsIgnoreCase("OnWorkPeoples"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				OnWorkPeoples = i;
			}
		}
		if (FCode.equalsIgnoreCase("OffWorkPeoples"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				OffWorkPeoples = i;
			}
		}
		if (FCode.equalsIgnoreCase("OtherPeoples"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				OtherPeoples = i;
			}
		}
		if (FCode.equalsIgnoreCase("RelaPeoples"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				RelaPeoples = i;
			}
		}
		if (FCode.equalsIgnoreCase("RelaMatePeoples"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				RelaMatePeoples = i;
			}
		}
		if (FCode.equalsIgnoreCase("RelaYoungPeoples"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				RelaYoungPeoples = i;
			}
		}
		if (FCode.equalsIgnoreCase("RelaOtherPeoples"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				RelaOtherPeoples = i;
			}
		}
		if (FCode.equalsIgnoreCase("Phone"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Phone = FValue.trim();
			}
			else
				Phone = null;
		}
		if (FCode.equalsIgnoreCase("Prem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Prem = d;
			}
		}
		if (FCode.equalsIgnoreCase("Amnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Amnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("InputOperator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InputOperator = FValue.trim();
			}
			else
				InputOperator = null;
		}
		if (FCode.equalsIgnoreCase("InputDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				InputDate = fDate.getDate( FValue );
			}
			else
				InputDate = null;
		}
		if (FCode.equalsIgnoreCase("InputTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InputTime = FValue.trim();
			}
			else
				InputTime = null;
		}
		if (FCode.equalsIgnoreCase("UWOperator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UWOperator = FValue.trim();
			}
			else
				UWOperator = null;
		}
		if (FCode.equalsIgnoreCase("UWFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UWFlag = FValue.trim();
			}
			else
				UWFlag = null;
		}
		if (FCode.equalsIgnoreCase("UWDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				UWDate = fDate.getDate( FValue );
			}
			else
				UWDate = null;
		}
		if (FCode.equalsIgnoreCase("UWTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UWTime = FValue.trim();
			}
			else
				UWTime = null;
		}
		if (FCode.equalsIgnoreCase("StateFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StateFlag = FValue.trim();
			}
			else
				StateFlag = null;
		}
		if (FCode.equalsIgnoreCase("GrpNature"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpNature = FValue.trim();
			}
			else
				GrpNature = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LKGrpContSchema other = (LKGrpContSchema)otherObject;
		return
			(PrtNo == null ? other.getPrtNo() == null : PrtNo.equals(other.getPrtNo()))
			&& (PolApplyDate == null ? other.getPolApplyDate() == null : fDate.getString(PolApplyDate).equals(other.getPolApplyDate()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (SaleChnl == null ? other.getSaleChnl() == null : SaleChnl.equals(other.getSaleChnl()))
			&& (AgentCode == null ? other.getAgentCode() == null : AgentCode.equals(other.getAgentCode()))
			&& (AgentGroup == null ? other.getAgentGroup() == null : AgentGroup.equals(other.getAgentGroup()))
			&& (AgentCom == null ? other.getAgentCom() == null : AgentCom.equals(other.getAgentCom()))
			&& (CValiDate == null ? other.getCValiDate() == null : fDate.getString(CValiDate).equals(other.getCValiDate()))
			&& (CInValiDate == null ? other.getCInValiDate() == null : fDate.getString(CInValiDate).equals(other.getCInValiDate()))
			&& (CoInsuranceFlag == null ? other.getCoInsuranceFlag() == null : CoInsuranceFlag.equals(other.getCoInsuranceFlag()))
			&& (BusType == null ? other.getBusType() == null : BusType.equals(other.getBusType()))
			&& (BusineAwards == null ? other.getBusineAwards() == null : BusineAwards.equals(other.getBusineAwards()))
			&& (IntermediaryFeerate == null ? other.getIntermediaryFeerate() == null : IntermediaryFeerate.equals(other.getIntermediaryFeerate()))
			&& (InsuType == null ? other.getInsuType() == null : InsuType.equals(other.getInsuType()))
			&& (InsuForm == null ? other.getInsuForm() == null : InsuForm.equals(other.getInsuForm()))
			&& (GrpName == null ? other.getGrpName() == null : GrpName.equals(other.getGrpName()))
			&& (AppntNo == null ? other.getAppntNo() == null : AppntNo.equals(other.getAppntNo()))
			&& (BusinessType == null ? other.getBusinessType() == null : BusinessType.equals(other.getBusinessType()))
			&& Peoples == other.getPeoples()
			&& Peoples3 == other.getPeoples3()
			&& OnWorkPeoples == other.getOnWorkPeoples()
			&& OffWorkPeoples == other.getOffWorkPeoples()
			&& OtherPeoples == other.getOtherPeoples()
			&& RelaPeoples == other.getRelaPeoples()
			&& RelaMatePeoples == other.getRelaMatePeoples()
			&& RelaYoungPeoples == other.getRelaYoungPeoples()
			&& RelaOtherPeoples == other.getRelaOtherPeoples()
			&& (Phone == null ? other.getPhone() == null : Phone.equals(other.getPhone()))
			&& Prem == other.getPrem()
			&& Amnt == other.getAmnt()
			&& (InputOperator == null ? other.getInputOperator() == null : InputOperator.equals(other.getInputOperator()))
			&& (InputDate == null ? other.getInputDate() == null : fDate.getString(InputDate).equals(other.getInputDate()))
			&& (InputTime == null ? other.getInputTime() == null : InputTime.equals(other.getInputTime()))
			&& (UWOperator == null ? other.getUWOperator() == null : UWOperator.equals(other.getUWOperator()))
			&& (UWFlag == null ? other.getUWFlag() == null : UWFlag.equals(other.getUWFlag()))
			&& (UWDate == null ? other.getUWDate() == null : fDate.getString(UWDate).equals(other.getUWDate()))
			&& (UWTime == null ? other.getUWTime() == null : UWTime.equals(other.getUWTime()))
			&& (StateFlag == null ? other.getStateFlag() == null : StateFlag.equals(other.getStateFlag()))
			&& (GrpNature == null ? other.getGrpNature() == null : GrpNature.equals(other.getGrpNature()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("PrtNo") ) {
			return 0;
		}
		if( strFieldName.equals("PolApplyDate") ) {
			return 1;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 2;
		}
		if( strFieldName.equals("SaleChnl") ) {
			return 3;
		}
		if( strFieldName.equals("AgentCode") ) {
			return 4;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return 5;
		}
		if( strFieldName.equals("AgentCom") ) {
			return 6;
		}
		if( strFieldName.equals("CValiDate") ) {
			return 7;
		}
		if( strFieldName.equals("CInValiDate") ) {
			return 8;
		}
		if( strFieldName.equals("CoInsuranceFlag") ) {
			return 9;
		}
		if( strFieldName.equals("BusType") ) {
			return 10;
		}
		if( strFieldName.equals("BusineAwards") ) {
			return 11;
		}
		if( strFieldName.equals("IntermediaryFeerate") ) {
			return 12;
		}
		if( strFieldName.equals("InsuType") ) {
			return 13;
		}
		if( strFieldName.equals("InsuForm") ) {
			return 14;
		}
		if( strFieldName.equals("GrpName") ) {
			return 15;
		}
		if( strFieldName.equals("AppntNo") ) {
			return 16;
		}
		if( strFieldName.equals("BusinessType") ) {
			return 17;
		}
		if( strFieldName.equals("Peoples") ) {
			return 18;
		}
		if( strFieldName.equals("Peoples3") ) {
			return 19;
		}
		if( strFieldName.equals("OnWorkPeoples") ) {
			return 20;
		}
		if( strFieldName.equals("OffWorkPeoples") ) {
			return 21;
		}
		if( strFieldName.equals("OtherPeoples") ) {
			return 22;
		}
		if( strFieldName.equals("RelaPeoples") ) {
			return 23;
		}
		if( strFieldName.equals("RelaMatePeoples") ) {
			return 24;
		}
		if( strFieldName.equals("RelaYoungPeoples") ) {
			return 25;
		}
		if( strFieldName.equals("RelaOtherPeoples") ) {
			return 26;
		}
		if( strFieldName.equals("Phone") ) {
			return 27;
		}
		if( strFieldName.equals("Prem") ) {
			return 28;
		}
		if( strFieldName.equals("Amnt") ) {
			return 29;
		}
		if( strFieldName.equals("InputOperator") ) {
			return 30;
		}
		if( strFieldName.equals("InputDate") ) {
			return 31;
		}
		if( strFieldName.equals("InputTime") ) {
			return 32;
		}
		if( strFieldName.equals("UWOperator") ) {
			return 33;
		}
		if( strFieldName.equals("UWFlag") ) {
			return 34;
		}
		if( strFieldName.equals("UWDate") ) {
			return 35;
		}
		if( strFieldName.equals("UWTime") ) {
			return 36;
		}
		if( strFieldName.equals("StateFlag") ) {
			return 37;
		}
		if( strFieldName.equals("GrpNature") ) {
			return 38;
		}
		if( strFieldName.equals("Operator") ) {
			return 39;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 40;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 41;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 42;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 43;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "PrtNo";
				break;
			case 1:
				strFieldName = "PolApplyDate";
				break;
			case 2:
				strFieldName = "ManageCom";
				break;
			case 3:
				strFieldName = "SaleChnl";
				break;
			case 4:
				strFieldName = "AgentCode";
				break;
			case 5:
				strFieldName = "AgentGroup";
				break;
			case 6:
				strFieldName = "AgentCom";
				break;
			case 7:
				strFieldName = "CValiDate";
				break;
			case 8:
				strFieldName = "CInValiDate";
				break;
			case 9:
				strFieldName = "CoInsuranceFlag";
				break;
			case 10:
				strFieldName = "BusType";
				break;
			case 11:
				strFieldName = "BusineAwards";
				break;
			case 12:
				strFieldName = "IntermediaryFeerate";
				break;
			case 13:
				strFieldName = "InsuType";
				break;
			case 14:
				strFieldName = "InsuForm";
				break;
			case 15:
				strFieldName = "GrpName";
				break;
			case 16:
				strFieldName = "AppntNo";
				break;
			case 17:
				strFieldName = "BusinessType";
				break;
			case 18:
				strFieldName = "Peoples";
				break;
			case 19:
				strFieldName = "Peoples3";
				break;
			case 20:
				strFieldName = "OnWorkPeoples";
				break;
			case 21:
				strFieldName = "OffWorkPeoples";
				break;
			case 22:
				strFieldName = "OtherPeoples";
				break;
			case 23:
				strFieldName = "RelaPeoples";
				break;
			case 24:
				strFieldName = "RelaMatePeoples";
				break;
			case 25:
				strFieldName = "RelaYoungPeoples";
				break;
			case 26:
				strFieldName = "RelaOtherPeoples";
				break;
			case 27:
				strFieldName = "Phone";
				break;
			case 28:
				strFieldName = "Prem";
				break;
			case 29:
				strFieldName = "Amnt";
				break;
			case 30:
				strFieldName = "InputOperator";
				break;
			case 31:
				strFieldName = "InputDate";
				break;
			case 32:
				strFieldName = "InputTime";
				break;
			case 33:
				strFieldName = "UWOperator";
				break;
			case 34:
				strFieldName = "UWFlag";
				break;
			case 35:
				strFieldName = "UWDate";
				break;
			case 36:
				strFieldName = "UWTime";
				break;
			case 37:
				strFieldName = "StateFlag";
				break;
			case 38:
				strFieldName = "GrpNature";
				break;
			case 39:
				strFieldName = "Operator";
				break;
			case 40:
				strFieldName = "MakeDate";
				break;
			case 41:
				strFieldName = "MakeTime";
				break;
			case 42:
				strFieldName = "ModifyDate";
				break;
			case 43:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("PrtNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolApplyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SaleChnl") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CValiDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("CInValiDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("CoInsuranceFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BusType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BusineAwards") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IntermediaryFeerate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuForm") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppntNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BusinessType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Peoples") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("Peoples3") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("OnWorkPeoples") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("OffWorkPeoples") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("OtherPeoples") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("RelaPeoples") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("RelaMatePeoples") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("RelaYoungPeoples") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("RelaOtherPeoples") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("Phone") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Prem") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Amnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("InputOperator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InputDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("InputTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UWOperator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UWFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UWDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("UWTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StateFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpNature") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 8:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_INT;
				break;
			case 19:
				nFieldType = Schema.TYPE_INT;
				break;
			case 20:
				nFieldType = Schema.TYPE_INT;
				break;
			case 21:
				nFieldType = Schema.TYPE_INT;
				break;
			case 22:
				nFieldType = Schema.TYPE_INT;
				break;
			case 23:
				nFieldType = Schema.TYPE_INT;
				break;
			case 24:
				nFieldType = Schema.TYPE_INT;
				break;
			case 25:
				nFieldType = Schema.TYPE_INT;
				break;
			case 26:
				nFieldType = Schema.TYPE_INT;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 29:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 36:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 37:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 38:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 39:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 40:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 41:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 42:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 43:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
