/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LLClaimUWDutyDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LLClaimUWDutySchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-03-21
 */
public class LLClaimUWDutySchema implements Schema
{
    // @Field
    /** 立案号 */
    private String RgtNo;
    /** 分案号 */
    private String CaseNo;
    /** 总单/合同号 */
    private String ContNo;
    /** 集体保单号 */
    private String GrpPolNo;
    /** 保单号 */
    private String PolNo;
    /** 险类代码 */
    private String KindCode;
    /** 险种代码 */
    private String RiskCode;
    /** 险种版本号 */
    private String RiskVer;
    /** 给付责任代码 */
    private String GetDutyCode;
    /** 给付责任类型 */
    private String GetDutyKind;
    /** 赔案号 */
    private String ClmNo;
    /** 受理事故号 */
    private String CaseRelaNo;
    /** 保单管理机构 */
    private String PolMngCom;
    /** 销售渠道 */
    private String SaleChnl;
    /** 事故者客户号码 */
    private String CustomerNo;
    /** 事故者名称 */
    private String InsuredName;
    /** 保单生效日期 */
    private Date CValiDate;
    /** 保单状态 */
    private String PolState;
    /** 核算赔付金额 */
    private double StandPay;
    /** 核赔赔付金额 */
    private double RealPay;
    /** 审核人员 */
    private String ClmCheckOperator;
    /** 审核人级别 */
    private String ClmCheckGrade;
    /** 审核类型 */
    private String CheckType;
    /** 审核结论 */
    private String ClmCheckDecision;
    /** 备注 */
    private String Remark;
    /** 操作员 */
    private String Operator;
    /** 管理机构 */
    private String MngCom;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 31; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LLClaimUWDutySchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "PolNo";
        pk[1] = "GetDutyCode";
        pk[2] = "GetDutyKind";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getRgtNo()
    {
        if (RgtNo != null && !RgtNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            RgtNo = StrTool.unicodeToGBK(RgtNo);
        }
        return RgtNo;
    }

    public void setRgtNo(String aRgtNo)
    {
        RgtNo = aRgtNo;
    }

    public String getCaseNo()
    {
        if (CaseNo != null && !CaseNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            CaseNo = StrTool.unicodeToGBK(CaseNo);
        }
        return CaseNo;
    }

    public void setCaseNo(String aCaseNo)
    {
        CaseNo = aCaseNo;
    }

    public String getContNo()
    {
        if (ContNo != null && !ContNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            ContNo = StrTool.unicodeToGBK(ContNo);
        }
        return ContNo;
    }

    public void setContNo(String aContNo)
    {
        ContNo = aContNo;
    }

    public String getGrpPolNo()
    {
        if (GrpPolNo != null && !GrpPolNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            GrpPolNo = StrTool.unicodeToGBK(GrpPolNo);
        }
        return GrpPolNo;
    }

    public void setGrpPolNo(String aGrpPolNo)
    {
        GrpPolNo = aGrpPolNo;
    }

    public String getPolNo()
    {
        if (PolNo != null && !PolNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            PolNo = StrTool.unicodeToGBK(PolNo);
        }
        return PolNo;
    }

    public void setPolNo(String aPolNo)
    {
        PolNo = aPolNo;
    }

    public String getKindCode()
    {
        if (KindCode != null && !KindCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            KindCode = StrTool.unicodeToGBK(KindCode);
        }
        return KindCode;
    }

    public void setKindCode(String aKindCode)
    {
        KindCode = aKindCode;
    }

    public String getRiskCode()
    {
        if (RiskCode != null && !RiskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getRiskVer()
    {
        if (RiskVer != null && !RiskVer.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskVer = StrTool.unicodeToGBK(RiskVer);
        }
        return RiskVer;
    }

    public void setRiskVer(String aRiskVer)
    {
        RiskVer = aRiskVer;
    }

    public String getGetDutyCode()
    {
        if (GetDutyCode != null && !GetDutyCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GetDutyCode = StrTool.unicodeToGBK(GetDutyCode);
        }
        return GetDutyCode;
    }

    public void setGetDutyCode(String aGetDutyCode)
    {
        GetDutyCode = aGetDutyCode;
    }

    public String getGetDutyKind()
    {
        if (GetDutyKind != null && !GetDutyKind.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GetDutyKind = StrTool.unicodeToGBK(GetDutyKind);
        }
        return GetDutyKind;
    }

    public void setGetDutyKind(String aGetDutyKind)
    {
        GetDutyKind = aGetDutyKind;
    }

    public String getClmNo()
    {
        if (ClmNo != null && !ClmNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            ClmNo = StrTool.unicodeToGBK(ClmNo);
        }
        return ClmNo;
    }

    public void setClmNo(String aClmNo)
    {
        ClmNo = aClmNo;
    }

    public String getCaseRelaNo()
    {
        if (CaseRelaNo != null && !CaseRelaNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CaseRelaNo = StrTool.unicodeToGBK(CaseRelaNo);
        }
        return CaseRelaNo;
    }

    public void setCaseRelaNo(String aCaseRelaNo)
    {
        CaseRelaNo = aCaseRelaNo;
    }

    public String getPolMngCom()
    {
        if (PolMngCom != null && !PolMngCom.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            PolMngCom = StrTool.unicodeToGBK(PolMngCom);
        }
        return PolMngCom;
    }

    public void setPolMngCom(String aPolMngCom)
    {
        PolMngCom = aPolMngCom;
    }

    public String getSaleChnl()
    {
        if (SaleChnl != null && !SaleChnl.equals("") && SysConst.CHANGECHARSET == true)
        {
            SaleChnl = StrTool.unicodeToGBK(SaleChnl);
        }
        return SaleChnl;
    }

    public void setSaleChnl(String aSaleChnl)
    {
        SaleChnl = aSaleChnl;
    }

    public String getCustomerNo()
    {
        if (CustomerNo != null && !CustomerNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CustomerNo = StrTool.unicodeToGBK(CustomerNo);
        }
        return CustomerNo;
    }

    public void setCustomerNo(String aCustomerNo)
    {
        CustomerNo = aCustomerNo;
    }

    public String getInsuredName()
    {
        if (InsuredName != null && !InsuredName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            InsuredName = StrTool.unicodeToGBK(InsuredName);
        }
        return InsuredName;
    }

    public void setInsuredName(String aInsuredName)
    {
        InsuredName = aInsuredName;
    }

    public String getCValiDate()
    {
        if (CValiDate != null)
        {
            return fDate.getString(CValiDate);
        }
        else
        {
            return null;
        }
    }

    public void setCValiDate(Date aCValiDate)
    {
        CValiDate = aCValiDate;
    }

    public void setCValiDate(String aCValiDate)
    {
        if (aCValiDate != null && !aCValiDate.equals(""))
        {
            CValiDate = fDate.getDate(aCValiDate);
        }
        else
        {
            CValiDate = null;
        }
    }

    public String getPolState()
    {
        if (PolState != null && !PolState.equals("") && SysConst.CHANGECHARSET == true)
        {
            PolState = StrTool.unicodeToGBK(PolState);
        }
        return PolState;
    }

    public void setPolState(String aPolState)
    {
        PolState = aPolState;
    }

    public double getStandPay()
    {
        return StandPay;
    }

    public void setStandPay(double aStandPay)
    {
        StandPay = aStandPay;
    }

    public void setStandPay(String aStandPay)
    {
        if (aStandPay != null && !aStandPay.equals(""))
        {
            Double tDouble = new Double(aStandPay);
            double d = tDouble.doubleValue();
            StandPay = d;
        }
    }

    public double getRealPay()
    {
        return RealPay;
    }

    public void setRealPay(double aRealPay)
    {
        RealPay = aRealPay;
    }

    public void setRealPay(String aRealPay)
    {
        if (aRealPay != null && !aRealPay.equals(""))
        {
            Double tDouble = new Double(aRealPay);
            double d = tDouble.doubleValue();
            RealPay = d;
        }
    }

    public String getClmCheckOperator()
    {
        if (ClmCheckOperator != null && !ClmCheckOperator.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ClmCheckOperator = StrTool.unicodeToGBK(ClmCheckOperator);
        }
        return ClmCheckOperator;
    }

    public void setClmCheckOperator(String aClmCheckOperator)
    {
        ClmCheckOperator = aClmCheckOperator;
    }

    public String getClmCheckGrade()
    {
        if (ClmCheckGrade != null && !ClmCheckGrade.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ClmCheckGrade = StrTool.unicodeToGBK(ClmCheckGrade);
        }
        return ClmCheckGrade;
    }

    public void setClmCheckGrade(String aClmCheckGrade)
    {
        ClmCheckGrade = aClmCheckGrade;
    }

    public String getCheckType()
    {
        if (CheckType != null && !CheckType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CheckType = StrTool.unicodeToGBK(CheckType);
        }
        return CheckType;
    }

    public void setCheckType(String aCheckType)
    {
        CheckType = aCheckType;
    }

    public String getClmCheckDecision()
    {
        if (ClmCheckDecision != null && !ClmCheckDecision.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ClmCheckDecision = StrTool.unicodeToGBK(ClmCheckDecision);
        }
        return ClmCheckDecision;
    }

    public void setClmCheckDecision(String aClmCheckDecision)
    {
        ClmCheckDecision = aClmCheckDecision;
    }

    public String getRemark()
    {
        if (Remark != null && !Remark.equals("") && SysConst.CHANGECHARSET == true)
        {
            Remark = StrTool.unicodeToGBK(Remark);
        }
        return Remark;
    }

    public void setRemark(String aRemark)
    {
        Remark = aRemark;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMngCom()
    {
        if (MngCom != null && !MngCom.equals("") && SysConst.CHANGECHARSET == true)
        {
            MngCom = StrTool.unicodeToGBK(MngCom);
        }
        return MngCom;
    }

    public void setMngCom(String aMngCom)
    {
        MngCom = aMngCom;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LLClaimUWDutySchema 对象给 Schema 赋值
     * @param: aLLClaimUWDutySchema LLClaimUWDutySchema
     **/
    public void setSchema(LLClaimUWDutySchema aLLClaimUWDutySchema)
    {
        this.RgtNo = aLLClaimUWDutySchema.getRgtNo();
        this.CaseNo = aLLClaimUWDutySchema.getCaseNo();
        this.ContNo = aLLClaimUWDutySchema.getContNo();
        this.GrpPolNo = aLLClaimUWDutySchema.getGrpPolNo();
        this.PolNo = aLLClaimUWDutySchema.getPolNo();
        this.KindCode = aLLClaimUWDutySchema.getKindCode();
        this.RiskCode = aLLClaimUWDutySchema.getRiskCode();
        this.RiskVer = aLLClaimUWDutySchema.getRiskVer();
        this.GetDutyCode = aLLClaimUWDutySchema.getGetDutyCode();
        this.GetDutyKind = aLLClaimUWDutySchema.getGetDutyKind();
        this.ClmNo = aLLClaimUWDutySchema.getClmNo();
        this.CaseRelaNo = aLLClaimUWDutySchema.getCaseRelaNo();
        this.PolMngCom = aLLClaimUWDutySchema.getPolMngCom();
        this.SaleChnl = aLLClaimUWDutySchema.getSaleChnl();
        this.CustomerNo = aLLClaimUWDutySchema.getCustomerNo();
        this.InsuredName = aLLClaimUWDutySchema.getInsuredName();
        this.CValiDate = fDate.getDate(aLLClaimUWDutySchema.getCValiDate());
        this.PolState = aLLClaimUWDutySchema.getPolState();
        this.StandPay = aLLClaimUWDutySchema.getStandPay();
        this.RealPay = aLLClaimUWDutySchema.getRealPay();
        this.ClmCheckOperator = aLLClaimUWDutySchema.getClmCheckOperator();
        this.ClmCheckGrade = aLLClaimUWDutySchema.getClmCheckGrade();
        this.CheckType = aLLClaimUWDutySchema.getCheckType();
        this.ClmCheckDecision = aLLClaimUWDutySchema.getClmCheckDecision();
        this.Remark = aLLClaimUWDutySchema.getRemark();
        this.Operator = aLLClaimUWDutySchema.getOperator();
        this.MngCom = aLLClaimUWDutySchema.getMngCom();
        this.MakeDate = fDate.getDate(aLLClaimUWDutySchema.getMakeDate());
        this.MakeTime = aLLClaimUWDutySchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLLClaimUWDutySchema.getModifyDate());
        this.ModifyTime = aLLClaimUWDutySchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("RgtNo") == null)
            {
                this.RgtNo = null;
            }
            else
            {
                this.RgtNo = rs.getString("RgtNo").trim();
            }

            if (rs.getString("CaseNo") == null)
            {
                this.CaseNo = null;
            }
            else
            {
                this.CaseNo = rs.getString("CaseNo").trim();
            }

            if (rs.getString("ContNo") == null)
            {
                this.ContNo = null;
            }
            else
            {
                this.ContNo = rs.getString("ContNo").trim();
            }

            if (rs.getString("GrpPolNo") == null)
            {
                this.GrpPolNo = null;
            }
            else
            {
                this.GrpPolNo = rs.getString("GrpPolNo").trim();
            }

            if (rs.getString("PolNo") == null)
            {
                this.PolNo = null;
            }
            else
            {
                this.PolNo = rs.getString("PolNo").trim();
            }

            if (rs.getString("KindCode") == null)
            {
                this.KindCode = null;
            }
            else
            {
                this.KindCode = rs.getString("KindCode").trim();
            }

            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("RiskVer") == null)
            {
                this.RiskVer = null;
            }
            else
            {
                this.RiskVer = rs.getString("RiskVer").trim();
            }

            if (rs.getString("GetDutyCode") == null)
            {
                this.GetDutyCode = null;
            }
            else
            {
                this.GetDutyCode = rs.getString("GetDutyCode").trim();
            }

            if (rs.getString("GetDutyKind") == null)
            {
                this.GetDutyKind = null;
            }
            else
            {
                this.GetDutyKind = rs.getString("GetDutyKind").trim();
            }

            if (rs.getString("ClmNo") == null)
            {
                this.ClmNo = null;
            }
            else
            {
                this.ClmNo = rs.getString("ClmNo").trim();
            }

            if (rs.getString("CaseRelaNo") == null)
            {
                this.CaseRelaNo = null;
            }
            else
            {
                this.CaseRelaNo = rs.getString("CaseRelaNo").trim();
            }

            if (rs.getString("PolMngCom") == null)
            {
                this.PolMngCom = null;
            }
            else
            {
                this.PolMngCom = rs.getString("PolMngCom").trim();
            }

            if (rs.getString("SaleChnl") == null)
            {
                this.SaleChnl = null;
            }
            else
            {
                this.SaleChnl = rs.getString("SaleChnl").trim();
            }

            if (rs.getString("CustomerNo") == null)
            {
                this.CustomerNo = null;
            }
            else
            {
                this.CustomerNo = rs.getString("CustomerNo").trim();
            }

            if (rs.getString("InsuredName") == null)
            {
                this.InsuredName = null;
            }
            else
            {
                this.InsuredName = rs.getString("InsuredName").trim();
            }

            this.CValiDate = rs.getDate("CValiDate");
            if (rs.getString("PolState") == null)
            {
                this.PolState = null;
            }
            else
            {
                this.PolState = rs.getString("PolState").trim();
            }

            this.StandPay = rs.getDouble("StandPay");
            this.RealPay = rs.getDouble("RealPay");
            if (rs.getString("ClmCheckOperator") == null)
            {
                this.ClmCheckOperator = null;
            }
            else
            {
                this.ClmCheckOperator = rs.getString("ClmCheckOperator").trim();
            }

            if (rs.getString("ClmCheckGrade") == null)
            {
                this.ClmCheckGrade = null;
            }
            else
            {
                this.ClmCheckGrade = rs.getString("ClmCheckGrade").trim();
            }

            if (rs.getString("CheckType") == null)
            {
                this.CheckType = null;
            }
            else
            {
                this.CheckType = rs.getString("CheckType").trim();
            }

            if (rs.getString("ClmCheckDecision") == null)
            {
                this.ClmCheckDecision = null;
            }
            else
            {
                this.ClmCheckDecision = rs.getString("ClmCheckDecision").trim();
            }

            if (rs.getString("Remark") == null)
            {
                this.Remark = null;
            }
            else
            {
                this.Remark = rs.getString("Remark").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            if (rs.getString("MngCom") == null)
            {
                this.MngCom = null;
            }
            else
            {
                this.MngCom = rs.getString("MngCom").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLClaimUWDutySchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LLClaimUWDutySchema getSchema()
    {
        LLClaimUWDutySchema aLLClaimUWDutySchema = new LLClaimUWDutySchema();
        aLLClaimUWDutySchema.setSchema(this);
        return aLLClaimUWDutySchema;
    }

    public LLClaimUWDutyDB getDB()
    {
        LLClaimUWDutyDB aDBOper = new LLClaimUWDutyDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLClaimUWDuty描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(RgtNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CaseNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ContNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GrpPolNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PolNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(KindCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskVer)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GetDutyCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GetDutyKind)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ClmNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CaseRelaNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PolMngCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SaleChnl)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CustomerNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InsuredName)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(CValiDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PolState)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(StandPay) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(RealPay) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ClmCheckOperator)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ClmCheckGrade)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CheckType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ClmCheckDecision)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Remark)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MngCom)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLClaimUWDuty>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            RgtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                   SysConst.PACKAGESPILTER);
            CaseNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                    SysConst.PACKAGESPILTER);
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                    SysConst.PACKAGESPILTER);
            GrpPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                      SysConst.PACKAGESPILTER);
            PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                   SysConst.PACKAGESPILTER);
            KindCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                      SysConst.PACKAGESPILTER);
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                      SysConst.PACKAGESPILTER);
            RiskVer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                     SysConst.PACKAGESPILTER);
            GetDutyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                         SysConst.PACKAGESPILTER);
            GetDutyKind = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                         SysConst.PACKAGESPILTER);
            ClmNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                   SysConst.PACKAGESPILTER);
            CaseRelaNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                        SysConst.PACKAGESPILTER);
            PolMngCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                       SysConst.PACKAGESPILTER);
            SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                      SysConst.PACKAGESPILTER);
            CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                        SysConst.PACKAGESPILTER);
            InsuredName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                         SysConst.PACKAGESPILTER);
            CValiDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 17, SysConst.PACKAGESPILTER));
            PolState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                      SysConst.PACKAGESPILTER);
            StandPay = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 19, SysConst.PACKAGESPILTER))).doubleValue();
            RealPay = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 20, SysConst.PACKAGESPILTER))).doubleValue();
            ClmCheckOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                              21, SysConst.PACKAGESPILTER);
            ClmCheckGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,
                                           SysConst.PACKAGESPILTER);
            CheckType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,
                                       SysConst.PACKAGESPILTER);
            ClmCheckDecision = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                              24, SysConst.PACKAGESPILTER);
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,
                                    SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26,
                                      SysConst.PACKAGESPILTER);
            MngCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27,
                                    SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 28, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 30, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLClaimUWDutySchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("RgtNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RgtNo));
        }
        if (FCode.equals("CaseNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CaseNo));
        }
        if (FCode.equals("ContNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equals("GrpPolNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpPolNo));
        }
        if (FCode.equals("PolNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
        }
        if (FCode.equals("KindCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(KindCode));
        }
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equals("RiskVer"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskVer));
        }
        if (FCode.equals("GetDutyCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetDutyCode));
        }
        if (FCode.equals("GetDutyKind"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetDutyKind));
        }
        if (FCode.equals("ClmNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClmNo));
        }
        if (FCode.equals("CaseRelaNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CaseRelaNo));
        }
        if (FCode.equals("PolMngCom"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolMngCom));
        }
        if (FCode.equals("SaleChnl"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
        }
        if (FCode.equals("CustomerNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
        }
        if (FCode.equals("InsuredName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredName));
        }
        if (FCode.equals("CValiDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getCValiDate()));
        }
        if (FCode.equals("PolState"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolState));
        }
        if (FCode.equals("StandPay"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandPay));
        }
        if (FCode.equals("RealPay"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RealPay));
        }
        if (FCode.equals("ClmCheckOperator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClmCheckOperator));
        }
        if (FCode.equals("ClmCheckGrade"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClmCheckGrade));
        }
        if (FCode.equals("CheckType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CheckType));
        }
        if (FCode.equals("ClmCheckDecision"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClmCheckDecision));
        }
        if (FCode.equals("Remark"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MngCom"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MngCom));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(RgtNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(CaseNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(GrpPolNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(PolNo);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(KindCode);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(RiskVer);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(GetDutyCode);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(GetDutyKind);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(ClmNo);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(CaseRelaNo);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(PolMngCom);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(SaleChnl);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(CustomerNo);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(InsuredName);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getCValiDate()));
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(PolState);
                break;
            case 18:
                strFieldValue = String.valueOf(StandPay);
                break;
            case 19:
                strFieldValue = String.valueOf(RealPay);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(ClmCheckOperator);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(ClmCheckGrade);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(CheckType);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(ClmCheckDecision);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(MngCom);
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 29:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 30:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("RgtNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RgtNo = FValue.trim();
            }
            else
            {
                RgtNo = null;
            }
        }
        if (FCode.equals("CaseNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CaseNo = FValue.trim();
            }
            else
            {
                CaseNo = null;
            }
        }
        if (FCode.equals("ContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
            {
                ContNo = null;
            }
        }
        if (FCode.equals("GrpPolNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpPolNo = FValue.trim();
            }
            else
            {
                GrpPolNo = null;
            }
        }
        if (FCode.equals("PolNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
            {
                PolNo = null;
            }
        }
        if (FCode.equals("KindCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                KindCode = FValue.trim();
            }
            else
            {
                KindCode = null;
            }
        }
        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("RiskVer"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskVer = FValue.trim();
            }
            else
            {
                RiskVer = null;
            }
        }
        if (FCode.equals("GetDutyCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetDutyCode = FValue.trim();
            }
            else
            {
                GetDutyCode = null;
            }
        }
        if (FCode.equals("GetDutyKind"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetDutyKind = FValue.trim();
            }
            else
            {
                GetDutyKind = null;
            }
        }
        if (FCode.equals("ClmNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ClmNo = FValue.trim();
            }
            else
            {
                ClmNo = null;
            }
        }
        if (FCode.equals("CaseRelaNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CaseRelaNo = FValue.trim();
            }
            else
            {
                CaseRelaNo = null;
            }
        }
        if (FCode.equals("PolMngCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PolMngCom = FValue.trim();
            }
            else
            {
                PolMngCom = null;
            }
        }
        if (FCode.equals("SaleChnl"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SaleChnl = FValue.trim();
            }
            else
            {
                SaleChnl = null;
            }
        }
        if (FCode.equals("CustomerNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CustomerNo = FValue.trim();
            }
            else
            {
                CustomerNo = null;
            }
        }
        if (FCode.equals("InsuredName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InsuredName = FValue.trim();
            }
            else
            {
                InsuredName = null;
            }
        }
        if (FCode.equals("CValiDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CValiDate = fDate.getDate(FValue);
            }
            else
            {
                CValiDate = null;
            }
        }
        if (FCode.equals("PolState"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PolState = FValue.trim();
            }
            else
            {
                PolState = null;
            }
        }
        if (FCode.equals("StandPay"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                StandPay = d;
            }
        }
        if (FCode.equals("RealPay"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                RealPay = d;
            }
        }
        if (FCode.equals("ClmCheckOperator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ClmCheckOperator = FValue.trim();
            }
            else
            {
                ClmCheckOperator = null;
            }
        }
        if (FCode.equals("ClmCheckGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ClmCheckGrade = FValue.trim();
            }
            else
            {
                ClmCheckGrade = null;
            }
        }
        if (FCode.equals("CheckType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CheckType = FValue.trim();
            }
            else
            {
                CheckType = null;
            }
        }
        if (FCode.equals("ClmCheckDecision"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ClmCheckDecision = FValue.trim();
            }
            else
            {
                ClmCheckDecision = null;
            }
        }
        if (FCode.equals("Remark"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
            {
                Remark = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MngCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MngCom = FValue.trim();
            }
            else
            {
                MngCom = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LLClaimUWDutySchema other = (LLClaimUWDutySchema) otherObject;
        return
                RgtNo.equals(other.getRgtNo())
                && CaseNo.equals(other.getCaseNo())
                && ContNo.equals(other.getContNo())
                && GrpPolNo.equals(other.getGrpPolNo())
                && PolNo.equals(other.getPolNo())
                && KindCode.equals(other.getKindCode())
                && RiskCode.equals(other.getRiskCode())
                && RiskVer.equals(other.getRiskVer())
                && GetDutyCode.equals(other.getGetDutyCode())
                && GetDutyKind.equals(other.getGetDutyKind())
                && ClmNo.equals(other.getClmNo())
                && CaseRelaNo.equals(other.getCaseRelaNo())
                && PolMngCom.equals(other.getPolMngCom())
                && SaleChnl.equals(other.getSaleChnl())
                && CustomerNo.equals(other.getCustomerNo())
                && InsuredName.equals(other.getInsuredName())
                && fDate.getString(CValiDate).equals(other.getCValiDate())
                && PolState.equals(other.getPolState())
                && StandPay == other.getStandPay()
                && RealPay == other.getRealPay()
                && ClmCheckOperator.equals(other.getClmCheckOperator())
                && ClmCheckGrade.equals(other.getClmCheckGrade())
                && CheckType.equals(other.getCheckType())
                && ClmCheckDecision.equals(other.getClmCheckDecision())
                && Remark.equals(other.getRemark())
                && Operator.equals(other.getOperator())
                && MngCom.equals(other.getMngCom())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("RgtNo"))
        {
            return 0;
        }
        if (strFieldName.equals("CaseNo"))
        {
            return 1;
        }
        if (strFieldName.equals("ContNo"))
        {
            return 2;
        }
        if (strFieldName.equals("GrpPolNo"))
        {
            return 3;
        }
        if (strFieldName.equals("PolNo"))
        {
            return 4;
        }
        if (strFieldName.equals("KindCode"))
        {
            return 5;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return 6;
        }
        if (strFieldName.equals("RiskVer"))
        {
            return 7;
        }
        if (strFieldName.equals("GetDutyCode"))
        {
            return 8;
        }
        if (strFieldName.equals("GetDutyKind"))
        {
            return 9;
        }
        if (strFieldName.equals("ClmNo"))
        {
            return 10;
        }
        if (strFieldName.equals("CaseRelaNo"))
        {
            return 11;
        }
        if (strFieldName.equals("PolMngCom"))
        {
            return 12;
        }
        if (strFieldName.equals("SaleChnl"))
        {
            return 13;
        }
        if (strFieldName.equals("CustomerNo"))
        {
            return 14;
        }
        if (strFieldName.equals("InsuredName"))
        {
            return 15;
        }
        if (strFieldName.equals("CValiDate"))
        {
            return 16;
        }
        if (strFieldName.equals("PolState"))
        {
            return 17;
        }
        if (strFieldName.equals("StandPay"))
        {
            return 18;
        }
        if (strFieldName.equals("RealPay"))
        {
            return 19;
        }
        if (strFieldName.equals("ClmCheckOperator"))
        {
            return 20;
        }
        if (strFieldName.equals("ClmCheckGrade"))
        {
            return 21;
        }
        if (strFieldName.equals("CheckType"))
        {
            return 22;
        }
        if (strFieldName.equals("ClmCheckDecision"))
        {
            return 23;
        }
        if (strFieldName.equals("Remark"))
        {
            return 24;
        }
        if (strFieldName.equals("Operator"))
        {
            return 25;
        }
        if (strFieldName.equals("MngCom"))
        {
            return 26;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 27;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 28;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 29;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 30;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "RgtNo";
                break;
            case 1:
                strFieldName = "CaseNo";
                break;
            case 2:
                strFieldName = "ContNo";
                break;
            case 3:
                strFieldName = "GrpPolNo";
                break;
            case 4:
                strFieldName = "PolNo";
                break;
            case 5:
                strFieldName = "KindCode";
                break;
            case 6:
                strFieldName = "RiskCode";
                break;
            case 7:
                strFieldName = "RiskVer";
                break;
            case 8:
                strFieldName = "GetDutyCode";
                break;
            case 9:
                strFieldName = "GetDutyKind";
                break;
            case 10:
                strFieldName = "ClmNo";
                break;
            case 11:
                strFieldName = "CaseRelaNo";
                break;
            case 12:
                strFieldName = "PolMngCom";
                break;
            case 13:
                strFieldName = "SaleChnl";
                break;
            case 14:
                strFieldName = "CustomerNo";
                break;
            case 15:
                strFieldName = "InsuredName";
                break;
            case 16:
                strFieldName = "CValiDate";
                break;
            case 17:
                strFieldName = "PolState";
                break;
            case 18:
                strFieldName = "StandPay";
                break;
            case 19:
                strFieldName = "RealPay";
                break;
            case 20:
                strFieldName = "ClmCheckOperator";
                break;
            case 21:
                strFieldName = "ClmCheckGrade";
                break;
            case 22:
                strFieldName = "CheckType";
                break;
            case 23:
                strFieldName = "ClmCheckDecision";
                break;
            case 24:
                strFieldName = "Remark";
                break;
            case 25:
                strFieldName = "Operator";
                break;
            case 26:
                strFieldName = "MngCom";
                break;
            case 27:
                strFieldName = "MakeDate";
                break;
            case 28:
                strFieldName = "MakeTime";
                break;
            case 29:
                strFieldName = "ModifyDate";
                break;
            case 30:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("RgtNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CaseNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpPolNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PolNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("KindCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskVer"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetDutyCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetDutyKind"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ClmNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CaseRelaNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PolMngCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SaleChnl"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CustomerNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsuredName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CValiDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("PolState"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StandPay"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("RealPay"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("ClmCheckOperator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ClmCheckGrade"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CheckType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ClmCheckDecision"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Remark"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MngCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 17:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 18:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 19:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 20:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 21:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 22:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 23:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 24:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 25:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 26:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 27:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 28:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 29:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 30:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
