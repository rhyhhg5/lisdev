/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LAWelfareRadixBDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LAWelfareRadixBSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-03-30
 */
public class LAWelfareRadixBSchema implements Schema
{
    // @Field
    /** 转储号码 */
    private String EdorNo;
    /** 转储类型 */
    private String EdorType;
    /** 地区编码 */
    private String ManageCom;
    /** 代理人职级 */
    private String AgentGrade;
    /** 福利类别 */
    private String AClass;
    /** 起始日期 */
    private Date StartDate;
    /** 终止日期 */
    private Date EndDate;
    /** 展业类型 */
    private String BranchType;
    /** 发放起始年度 */
    private int StartPayYear;
    /** 服务年限 */
    private int ServeYear;
    /** 提取基数 */
    private double DrawBase;
    /** 提取比例 */
    private double DrawRate;
    /** 提取金额 */
    private double DrawMoney;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 其他提取比例 */
    private double OthDrawRate;
    /** 预留基数 */
    private double DrawBase1;
    /** 预留基数2 */
    private double DrawBase2;
    /** 预留基数3 */
    private double DrawBase3;
    /** 预留基数4 */
    private double DrawBase4;
    /** 渠道 */
    private String BranchType2;

    public static final int FIELDNUM = 24; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LAWelfareRadixBSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[7];
        pk[0] = "EdorNo";
        pk[1] = "EdorType";
        pk[2] = "ManageCom";
        pk[3] = "AgentGrade";
        pk[4] = "AClass";
        pk[5] = "StartDate";
        pk[6] = "EndDate";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getEdorNo()
    {
        if (SysConst.CHANGECHARSET && EdorNo != null && !EdorNo.equals(""))
        {
            EdorNo = StrTool.unicodeToGBK(EdorNo);
        }
        return EdorNo;
    }

    public void setEdorNo(String aEdorNo)
    {
        EdorNo = aEdorNo;
    }

    public String getEdorType()
    {
        if (SysConst.CHANGECHARSET && EdorType != null && !EdorType.equals(""))
        {
            EdorType = StrTool.unicodeToGBK(EdorType);
        }
        return EdorType;
    }

    public void setEdorType(String aEdorType)
    {
        EdorType = aEdorType;
    }

    public String getManageCom()
    {
        if (SysConst.CHANGECHARSET && ManageCom != null && !ManageCom.equals(""))
        {
            ManageCom = StrTool.unicodeToGBK(ManageCom);
        }
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public String getAgentGrade()
    {
        if (SysConst.CHANGECHARSET && AgentGrade != null &&
            !AgentGrade.equals(""))
        {
            AgentGrade = StrTool.unicodeToGBK(AgentGrade);
        }
        return AgentGrade;
    }

    public void setAgentGrade(String aAgentGrade)
    {
        AgentGrade = aAgentGrade;
    }

    public String getAClass()
    {
        if (SysConst.CHANGECHARSET && AClass != null && !AClass.equals(""))
        {
            AClass = StrTool.unicodeToGBK(AClass);
        }
        return AClass;
    }

    public void setAClass(String aAClass)
    {
        AClass = aAClass;
    }

    public String getStartDate()
    {
        if (StartDate != null)
        {
            return fDate.getString(StartDate);
        }
        else
        {
            return null;
        }
    }

    public void setStartDate(Date aStartDate)
    {
        StartDate = aStartDate;
    }

    public void setStartDate(String aStartDate)
    {
        if (aStartDate != null && !aStartDate.equals(""))
        {
            StartDate = fDate.getDate(aStartDate);
        }
        else
        {
            StartDate = null;
        }
    }

    public String getEndDate()
    {
        if (EndDate != null)
        {
            return fDate.getString(EndDate);
        }
        else
        {
            return null;
        }
    }

    public void setEndDate(Date aEndDate)
    {
        EndDate = aEndDate;
    }

    public void setEndDate(String aEndDate)
    {
        if (aEndDate != null && !aEndDate.equals(""))
        {
            EndDate = fDate.getDate(aEndDate);
        }
        else
        {
            EndDate = null;
        }
    }

    public String getBranchType()
    {
        if (SysConst.CHANGECHARSET && BranchType != null &&
            !BranchType.equals(""))
        {
            BranchType = StrTool.unicodeToGBK(BranchType);
        }
        return BranchType;
    }

    public void setBranchType(String aBranchType)
    {
        BranchType = aBranchType;
    }

    public int getStartPayYear()
    {
        return StartPayYear;
    }

    public void setStartPayYear(int aStartPayYear)
    {
        StartPayYear = aStartPayYear;
    }

    public void setStartPayYear(String aStartPayYear)
    {
        if (aStartPayYear != null && !aStartPayYear.equals(""))
        {
            Integer tInteger = new Integer(aStartPayYear);
            int i = tInteger.intValue();
            StartPayYear = i;
        }
    }

    public int getServeYear()
    {
        return ServeYear;
    }

    public void setServeYear(int aServeYear)
    {
        ServeYear = aServeYear;
    }

    public void setServeYear(String aServeYear)
    {
        if (aServeYear != null && !aServeYear.equals(""))
        {
            Integer tInteger = new Integer(aServeYear);
            int i = tInteger.intValue();
            ServeYear = i;
        }
    }

    public double getDrawBase()
    {
        return DrawBase;
    }

    public void setDrawBase(double aDrawBase)
    {
        DrawBase = aDrawBase;
    }

    public void setDrawBase(String aDrawBase)
    {
        if (aDrawBase != null && !aDrawBase.equals(""))
        {
            Double tDouble = new Double(aDrawBase);
            double d = tDouble.doubleValue();
            DrawBase = d;
        }
    }

    public double getDrawRate()
    {
        return DrawRate;
    }

    public void setDrawRate(double aDrawRate)
    {
        DrawRate = aDrawRate;
    }

    public void setDrawRate(String aDrawRate)
    {
        if (aDrawRate != null && !aDrawRate.equals(""))
        {
            Double tDouble = new Double(aDrawRate);
            double d = tDouble.doubleValue();
            DrawRate = d;
        }
    }

    public double getDrawMoney()
    {
        return DrawMoney;
    }

    public void setDrawMoney(double aDrawMoney)
    {
        DrawMoney = aDrawMoney;
    }

    public void setDrawMoney(String aDrawMoney)
    {
        if (aDrawMoney != null && !aDrawMoney.equals(""))
        {
            Double tDouble = new Double(aDrawMoney);
            double d = tDouble.doubleValue();
            DrawMoney = d;
        }
    }

    public String getOperator()
    {
        if (SysConst.CHANGECHARSET && Operator != null && !Operator.equals(""))
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (SysConst.CHANGECHARSET && MakeTime != null && !MakeTime.equals(""))
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (SysConst.CHANGECHARSET && ModifyTime != null &&
            !ModifyTime.equals(""))
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    public double getOthDrawRate()
    {
        return OthDrawRate;
    }

    public void setOthDrawRate(double aOthDrawRate)
    {
        OthDrawRate = aOthDrawRate;
    }

    public void setOthDrawRate(String aOthDrawRate)
    {
        if (aOthDrawRate != null && !aOthDrawRate.equals(""))
        {
            Double tDouble = new Double(aOthDrawRate);
            double d = tDouble.doubleValue();
            OthDrawRate = d;
        }
    }

    public double getDrawBase1()
    {
        return DrawBase1;
    }

    public void setDrawBase1(double aDrawBase1)
    {
        DrawBase1 = aDrawBase1;
    }

    public void setDrawBase1(String aDrawBase1)
    {
        if (aDrawBase1 != null && !aDrawBase1.equals(""))
        {
            Double tDouble = new Double(aDrawBase1);
            double d = tDouble.doubleValue();
            DrawBase1 = d;
        }
    }

    public double getDrawBase2()
    {
        return DrawBase2;
    }

    public void setDrawBase2(double aDrawBase2)
    {
        DrawBase2 = aDrawBase2;
    }

    public void setDrawBase2(String aDrawBase2)
    {
        if (aDrawBase2 != null && !aDrawBase2.equals(""))
        {
            Double tDouble = new Double(aDrawBase2);
            double d = tDouble.doubleValue();
            DrawBase2 = d;
        }
    }

    public double getDrawBase3()
    {
        return DrawBase3;
    }

    public void setDrawBase3(double aDrawBase3)
    {
        DrawBase3 = aDrawBase3;
    }

    public void setDrawBase3(String aDrawBase3)
    {
        if (aDrawBase3 != null && !aDrawBase3.equals(""))
        {
            Double tDouble = new Double(aDrawBase3);
            double d = tDouble.doubleValue();
            DrawBase3 = d;
        }
    }

    public double getDrawBase4()
    {
        return DrawBase4;
    }

    public void setDrawBase4(double aDrawBase4)
    {
        DrawBase4 = aDrawBase4;
    }

    public void setDrawBase4(String aDrawBase4)
    {
        if (aDrawBase4 != null && !aDrawBase4.equals(""))
        {
            Double tDouble = new Double(aDrawBase4);
            double d = tDouble.doubleValue();
            DrawBase4 = d;
        }
    }

    public String getBranchType2()
    {
        if (SysConst.CHANGECHARSET && BranchType2 != null &&
            !BranchType2.equals(""))
        {
            BranchType2 = StrTool.unicodeToGBK(BranchType2);
        }
        return BranchType2;
    }

    public void setBranchType2(String aBranchType2)
    {
        BranchType2 = aBranchType2;
    }

    /**
     * 使用另外一个 LAWelfareRadixBSchema 对象给 Schema 赋值
     * @param: aLAWelfareRadixBSchema LAWelfareRadixBSchema
     **/
    public void setSchema(LAWelfareRadixBSchema aLAWelfareRadixBSchema)
    {
        this.EdorNo = aLAWelfareRadixBSchema.getEdorNo();
        this.EdorType = aLAWelfareRadixBSchema.getEdorType();
        this.ManageCom = aLAWelfareRadixBSchema.getManageCom();
        this.AgentGrade = aLAWelfareRadixBSchema.getAgentGrade();
        this.AClass = aLAWelfareRadixBSchema.getAClass();
        this.StartDate = fDate.getDate(aLAWelfareRadixBSchema.getStartDate());
        this.EndDate = fDate.getDate(aLAWelfareRadixBSchema.getEndDate());
        this.BranchType = aLAWelfareRadixBSchema.getBranchType();
        this.StartPayYear = aLAWelfareRadixBSchema.getStartPayYear();
        this.ServeYear = aLAWelfareRadixBSchema.getServeYear();
        this.DrawBase = aLAWelfareRadixBSchema.getDrawBase();
        this.DrawRate = aLAWelfareRadixBSchema.getDrawRate();
        this.DrawMoney = aLAWelfareRadixBSchema.getDrawMoney();
        this.Operator = aLAWelfareRadixBSchema.getOperator();
        this.MakeDate = fDate.getDate(aLAWelfareRadixBSchema.getMakeDate());
        this.MakeTime = aLAWelfareRadixBSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLAWelfareRadixBSchema.getModifyDate());
        this.ModifyTime = aLAWelfareRadixBSchema.getModifyTime();
        this.OthDrawRate = aLAWelfareRadixBSchema.getOthDrawRate();
        this.DrawBase1 = aLAWelfareRadixBSchema.getDrawBase1();
        this.DrawBase2 = aLAWelfareRadixBSchema.getDrawBase2();
        this.DrawBase3 = aLAWelfareRadixBSchema.getDrawBase3();
        this.DrawBase4 = aLAWelfareRadixBSchema.getDrawBase4();
        this.BranchType2 = aLAWelfareRadixBSchema.getBranchType2();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("EdorNo") == null)
            {
                this.EdorNo = null;
            }
            else
            {
                this.EdorNo = rs.getString("EdorNo").trim();
            }

            if (rs.getString("EdorType") == null)
            {
                this.EdorType = null;
            }
            else
            {
                this.EdorType = rs.getString("EdorType").trim();
            }

            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("AgentGrade") == null)
            {
                this.AgentGrade = null;
            }
            else
            {
                this.AgentGrade = rs.getString("AgentGrade").trim();
            }

            if (rs.getString("AClass") == null)
            {
                this.AClass = null;
            }
            else
            {
                this.AClass = rs.getString("AClass").trim();
            }

            this.StartDate = rs.getDate("StartDate");
            this.EndDate = rs.getDate("EndDate");
            if (rs.getString("BranchType") == null)
            {
                this.BranchType = null;
            }
            else
            {
                this.BranchType = rs.getString("BranchType").trim();
            }

            this.StartPayYear = rs.getInt("StartPayYear");
            this.ServeYear = rs.getInt("ServeYear");
            this.DrawBase = rs.getDouble("DrawBase");
            this.DrawRate = rs.getDouble("DrawRate");
            this.DrawMoney = rs.getDouble("DrawMoney");
            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            this.OthDrawRate = rs.getDouble("OthDrawRate");
            this.DrawBase1 = rs.getDouble("DrawBase1");
            this.DrawBase2 = rs.getDouble("DrawBase2");
            this.DrawBase3 = rs.getDouble("DrawBase3");
            this.DrawBase4 = rs.getDouble("DrawBase4");
            if (rs.getString("BranchType2") == null)
            {
                this.BranchType2 = null;
            }
            else
            {
                this.BranchType2 = rs.getString("BranchType2").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAWelfareRadixBSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LAWelfareRadixBSchema getSchema()
    {
        LAWelfareRadixBSchema aLAWelfareRadixBSchema = new
                LAWelfareRadixBSchema();
        aLAWelfareRadixBSchema.setSchema(this);
        return aLAWelfareRadixBSchema;
    }

    public LAWelfareRadixBDB getDB()
    {
        LAWelfareRadixBDB aDBOper = new LAWelfareRadixBDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAWelfareRadixB描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(EdorNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(EdorType)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ManageCom)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AgentGrade)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AClass)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                StartDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                EndDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BranchType)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(StartPayYear));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(ServeYear));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DrawBase));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DrawRate));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DrawMoney));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Operator)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                MakeDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                ModifyDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(OthDrawRate));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DrawBase1));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DrawBase2));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DrawBase3));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DrawBase4));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BranchType2)));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAWelfareRadixB>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            EdorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                    SysConst.PACKAGESPILTER);
            EdorType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                       SysConst.PACKAGESPILTER);
            AgentGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                        SysConst.PACKAGESPILTER);
            AClass = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                    SysConst.PACKAGESPILTER);
            StartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 6, SysConst.PACKAGESPILTER));
            EndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 7, SysConst.PACKAGESPILTER));
            BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                        SysConst.PACKAGESPILTER);
            StartPayYear = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 9, SysConst.PACKAGESPILTER))).intValue();
            ServeYear = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 10, SysConst.PACKAGESPILTER))).intValue();
            DrawBase = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 11, SysConst.PACKAGESPILTER))).doubleValue();
            DrawRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 12, SysConst.PACKAGESPILTER))).doubleValue();
            DrawMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 13, SysConst.PACKAGESPILTER))).doubleValue();
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 15, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 17, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                        SysConst.PACKAGESPILTER);
            OthDrawRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 19, SysConst.PACKAGESPILTER))).doubleValue();
            DrawBase1 = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 20, SysConst.PACKAGESPILTER))).doubleValue();
            DrawBase2 = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 21, SysConst.PACKAGESPILTER))).doubleValue();
            DrawBase3 = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 22, SysConst.PACKAGESPILTER))).doubleValue();
            DrawBase4 = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 23, SysConst.PACKAGESPILTER))).doubleValue();
            BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,
                                         SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAWelfareRadixBSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("EdorNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorNo));
        }
        if (FCode.equals("EdorType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorType));
        }
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("AgentGrade"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGrade));
        }
        if (FCode.equals("AClass"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AClass));
        }
        if (FCode.equals("StartDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getStartDate()));
        }
        if (FCode.equals("EndDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getEndDate()));
        }
        if (FCode.equals("BranchType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
        }
        if (FCode.equals("StartPayYear"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StartPayYear));
        }
        if (FCode.equals("ServeYear"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServeYear));
        }
        if (FCode.equals("DrawBase"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DrawBase));
        }
        if (FCode.equals("DrawRate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DrawRate));
        }
        if (FCode.equals("DrawMoney"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DrawMoney));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("OthDrawRate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OthDrawRate));
        }
        if (FCode.equals("DrawBase1"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DrawBase1));
        }
        if (FCode.equals("DrawBase2"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DrawBase2));
        }
        if (FCode.equals("DrawBase3"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DrawBase3));
        }
        if (FCode.equals("DrawBase4"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DrawBase4));
        }
        if (FCode.equals("BranchType2"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(EdorNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(EdorType);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(AgentGrade);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(AClass);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getStartDate()));
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getEndDate()));
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(BranchType);
                break;
            case 8:
                strFieldValue = String.valueOf(StartPayYear);
                break;
            case 9:
                strFieldValue = String.valueOf(ServeYear);
                break;
            case 10:
                strFieldValue = String.valueOf(DrawBase);
                break;
            case 11:
                strFieldValue = String.valueOf(DrawRate);
                break;
            case 12:
                strFieldValue = String.valueOf(DrawMoney);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 18:
                strFieldValue = String.valueOf(OthDrawRate);
                break;
            case 19:
                strFieldValue = String.valueOf(DrawBase1);
                break;
            case 20:
                strFieldValue = String.valueOf(DrawBase2);
                break;
            case 21:
                strFieldValue = String.valueOf(DrawBase3);
                break;
            case 22:
                strFieldValue = String.valueOf(DrawBase4);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(BranchType2);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("EdorNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorNo = FValue.trim();
            }
            else
            {
                EdorNo = null;
            }
        }
        if (FCode.equals("EdorType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorType = FValue.trim();
            }
            else
            {
                EdorType = null;
            }
        }
        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("AgentGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentGrade = FValue.trim();
            }
            else
            {
                AgentGrade = null;
            }
        }
        if (FCode.equals("AClass"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AClass = FValue.trim();
            }
            else
            {
                AClass = null;
            }
        }
        if (FCode.equals("StartDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                StartDate = fDate.getDate(FValue);
            }
            else
            {
                StartDate = null;
            }
        }
        if (FCode.equals("EndDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EndDate = fDate.getDate(FValue);
            }
            else
            {
                EndDate = null;
            }
        }
        if (FCode.equals("BranchType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchType = FValue.trim();
            }
            else
            {
                BranchType = null;
            }
        }
        if (FCode.equals("StartPayYear"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                StartPayYear = i;
            }
        }
        if (FCode.equals("ServeYear"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                ServeYear = i;
            }
        }
        if (FCode.equals("DrawBase"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                DrawBase = d;
            }
        }
        if (FCode.equals("DrawRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                DrawRate = d;
            }
        }
        if (FCode.equals("DrawMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                DrawMoney = d;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        if (FCode.equals("OthDrawRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                OthDrawRate = d;
            }
        }
        if (FCode.equals("DrawBase1"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                DrawBase1 = d;
            }
        }
        if (FCode.equals("DrawBase2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                DrawBase2 = d;
            }
        }
        if (FCode.equals("DrawBase3"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                DrawBase3 = d;
            }
        }
        if (FCode.equals("DrawBase4"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                DrawBase4 = d;
            }
        }
        if (FCode.equals("BranchType2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchType2 = FValue.trim();
            }
            else
            {
                BranchType2 = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LAWelfareRadixBSchema other = (LAWelfareRadixBSchema) otherObject;
        return
                EdorNo.equals(other.getEdorNo())
                && EdorType.equals(other.getEdorType())
                && ManageCom.equals(other.getManageCom())
                && AgentGrade.equals(other.getAgentGrade())
                && AClass.equals(other.getAClass())
                && fDate.getString(StartDate).equals(other.getStartDate())
                && fDate.getString(EndDate).equals(other.getEndDate())
                && BranchType.equals(other.getBranchType())
                && StartPayYear == other.getStartPayYear()
                && ServeYear == other.getServeYear()
                && DrawBase == other.getDrawBase()
                && DrawRate == other.getDrawRate()
                && DrawMoney == other.getDrawMoney()
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && OthDrawRate == other.getOthDrawRate()
                && DrawBase1 == other.getDrawBase1()
                && DrawBase2 == other.getDrawBase2()
                && DrawBase3 == other.getDrawBase3()
                && DrawBase4 == other.getDrawBase4()
                && BranchType2.equals(other.getBranchType2());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("EdorNo"))
        {
            return 0;
        }
        if (strFieldName.equals("EdorType"))
        {
            return 1;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return 2;
        }
        if (strFieldName.equals("AgentGrade"))
        {
            return 3;
        }
        if (strFieldName.equals("AClass"))
        {
            return 4;
        }
        if (strFieldName.equals("StartDate"))
        {
            return 5;
        }
        if (strFieldName.equals("EndDate"))
        {
            return 6;
        }
        if (strFieldName.equals("BranchType"))
        {
            return 7;
        }
        if (strFieldName.equals("StartPayYear"))
        {
            return 8;
        }
        if (strFieldName.equals("ServeYear"))
        {
            return 9;
        }
        if (strFieldName.equals("DrawBase"))
        {
            return 10;
        }
        if (strFieldName.equals("DrawRate"))
        {
            return 11;
        }
        if (strFieldName.equals("DrawMoney"))
        {
            return 12;
        }
        if (strFieldName.equals("Operator"))
        {
            return 13;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 14;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 15;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 16;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 17;
        }
        if (strFieldName.equals("OthDrawRate"))
        {
            return 18;
        }
        if (strFieldName.equals("DrawBase1"))
        {
            return 19;
        }
        if (strFieldName.equals("DrawBase2"))
        {
            return 20;
        }
        if (strFieldName.equals("DrawBase3"))
        {
            return 21;
        }
        if (strFieldName.equals("DrawBase4"))
        {
            return 22;
        }
        if (strFieldName.equals("BranchType2"))
        {
            return 23;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "EdorNo";
                break;
            case 1:
                strFieldName = "EdorType";
                break;
            case 2:
                strFieldName = "ManageCom";
                break;
            case 3:
                strFieldName = "AgentGrade";
                break;
            case 4:
                strFieldName = "AClass";
                break;
            case 5:
                strFieldName = "StartDate";
                break;
            case 6:
                strFieldName = "EndDate";
                break;
            case 7:
                strFieldName = "BranchType";
                break;
            case 8:
                strFieldName = "StartPayYear";
                break;
            case 9:
                strFieldName = "ServeYear";
                break;
            case 10:
                strFieldName = "DrawBase";
                break;
            case 11:
                strFieldName = "DrawRate";
                break;
            case 12:
                strFieldName = "DrawMoney";
                break;
            case 13:
                strFieldName = "Operator";
                break;
            case 14:
                strFieldName = "MakeDate";
                break;
            case 15:
                strFieldName = "MakeTime";
                break;
            case 16:
                strFieldName = "ModifyDate";
                break;
            case 17:
                strFieldName = "ModifyTime";
                break;
            case 18:
                strFieldName = "OthDrawRate";
                break;
            case 19:
                strFieldName = "DrawBase1";
                break;
            case 20:
                strFieldName = "DrawBase2";
                break;
            case 21:
                strFieldName = "DrawBase3";
                break;
            case 22:
                strFieldName = "DrawBase4";
                break;
            case 23:
                strFieldName = "BranchType2";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("EdorNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EdorType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentGrade"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AClass"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StartDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("EndDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("BranchType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StartPayYear"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("ServeYear"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("DrawBase"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("DrawRate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("DrawMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OthDrawRate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("DrawBase1"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("DrawBase2"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("DrawBase3"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("DrawBase4"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("BranchType2"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 6:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_INT;
                break;
            case 9:
                nFieldType = Schema.TYPE_INT;
                break;
            case 10:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 11:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 12:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 17:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 18:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 19:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 20:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 21:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 22:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 23:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
