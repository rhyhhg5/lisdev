/*
 * <p>ClassName: LDHealthSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LDHealthDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LDHealthSchema implements Schema
{
    // @Field
    /** 流水号 */
    private String SerialNo;
    /** 体检项目代码 */
    private String HealthCode;
    /** 体检项目名称 */
    private String HealthName;
    /** 体检额度起始值 */
    private double StartMoney;
    /** 体检额度终止值 */
    private double EndMoney;
    /** 体检年龄起始值 */
    private int StartAge;
    /** 体检年龄终止值 */
    private int EndAge;
    /** 体检人性别 */
    private String Sex;
    /** 备注1 */
    private String Note1;
    /** 备注2 */
    private String Note2;

    public static final int FIELDNUM = 10; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LDHealthSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "SerialNo";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getSerialNo()
    {
        if (SerialNo != null && !SerialNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            SerialNo = StrTool.unicodeToGBK(SerialNo);
        }
        return SerialNo;
    }

    public void setSerialNo(String aSerialNo)
    {
        SerialNo = aSerialNo;
    }

    public String getHealthCode()
    {
        if (HealthCode != null && !HealthCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            HealthCode = StrTool.unicodeToGBK(HealthCode);
        }
        return HealthCode;
    }

    public void setHealthCode(String aHealthCode)
    {
        HealthCode = aHealthCode;
    }

    public String getHealthName()
    {
        if (HealthName != null && !HealthName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            HealthName = StrTool.unicodeToGBK(HealthName);
        }
        return HealthName;
    }

    public void setHealthName(String aHealthName)
    {
        HealthName = aHealthName;
    }

    public double getStartMoney()
    {
        return StartMoney;
    }

    public void setStartMoney(double aStartMoney)
    {
        StartMoney = aStartMoney;
    }

    public void setStartMoney(String aStartMoney)
    {
        if (aStartMoney != null && !aStartMoney.equals(""))
        {
            Double tDouble = new Double(aStartMoney);
            double d = tDouble.doubleValue();
            StartMoney = d;
        }
    }

    public double getEndMoney()
    {
        return EndMoney;
    }

    public void setEndMoney(double aEndMoney)
    {
        EndMoney = aEndMoney;
    }

    public void setEndMoney(String aEndMoney)
    {
        if (aEndMoney != null && !aEndMoney.equals(""))
        {
            Double tDouble = new Double(aEndMoney);
            double d = tDouble.doubleValue();
            EndMoney = d;
        }
    }

    public int getStartAge()
    {
        return StartAge;
    }

    public void setStartAge(int aStartAge)
    {
        StartAge = aStartAge;
    }

    public void setStartAge(String aStartAge)
    {
        if (aStartAge != null && !aStartAge.equals(""))
        {
            Integer tInteger = new Integer(aStartAge);
            int i = tInteger.intValue();
            StartAge = i;
        }
    }

    public int getEndAge()
    {
        return EndAge;
    }

    public void setEndAge(int aEndAge)
    {
        EndAge = aEndAge;
    }

    public void setEndAge(String aEndAge)
    {
        if (aEndAge != null && !aEndAge.equals(""))
        {
            Integer tInteger = new Integer(aEndAge);
            int i = tInteger.intValue();
            EndAge = i;
        }
    }

    public String getSex()
    {
        if (Sex != null && !Sex.equals("") && SysConst.CHANGECHARSET == true)
        {
            Sex = StrTool.unicodeToGBK(Sex);
        }
        return Sex;
    }

    public void setSex(String aSex)
    {
        Sex = aSex;
    }

    public String getNote1()
    {
        if (Note1 != null && !Note1.equals("") && SysConst.CHANGECHARSET == true)
        {
            Note1 = StrTool.unicodeToGBK(Note1);
        }
        return Note1;
    }

    public void setNote1(String aNote1)
    {
        Note1 = aNote1;
    }

    public String getNote2()
    {
        if (Note2 != null && !Note2.equals("") && SysConst.CHANGECHARSET == true)
        {
            Note2 = StrTool.unicodeToGBK(Note2);
        }
        return Note2;
    }

    public void setNote2(String aNote2)
    {
        Note2 = aNote2;
    }

    /**
     * 使用另外一个 LDHealthSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LDHealthSchema aLDHealthSchema)
    {
        this.SerialNo = aLDHealthSchema.getSerialNo();
        this.HealthCode = aLDHealthSchema.getHealthCode();
        this.HealthName = aLDHealthSchema.getHealthName();
        this.StartMoney = aLDHealthSchema.getStartMoney();
        this.EndMoney = aLDHealthSchema.getEndMoney();
        this.StartAge = aLDHealthSchema.getStartAge();
        this.EndAge = aLDHealthSchema.getEndAge();
        this.Sex = aLDHealthSchema.getSex();
        this.Note1 = aLDHealthSchema.getNote1();
        this.Note2 = aLDHealthSchema.getNote2();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("SerialNo") == null)
            {
                this.SerialNo = null;
            }
            else
            {
                this.SerialNo = rs.getString("SerialNo").trim();
            }

            if (rs.getString("HealthCode") == null)
            {
                this.HealthCode = null;
            }
            else
            {
                this.HealthCode = rs.getString("HealthCode").trim();
            }

            if (rs.getString("HealthName") == null)
            {
                this.HealthName = null;
            }
            else
            {
                this.HealthName = rs.getString("HealthName").trim();
            }

            this.StartMoney = rs.getDouble("StartMoney");
            this.EndMoney = rs.getDouble("EndMoney");
            this.StartAge = rs.getInt("StartAge");
            this.EndAge = rs.getInt("EndAge");
            if (rs.getString("Sex") == null)
            {
                this.Sex = null;
            }
            else
            {
                this.Sex = rs.getString("Sex").trim();
            }

            if (rs.getString("Note1") == null)
            {
                this.Note1 = null;
            }
            else
            {
                this.Note1 = rs.getString("Note1").trim();
            }

            if (rs.getString("Note2") == null)
            {
                this.Note2 = null;
            }
            else
            {
                this.Note2 = rs.getString("Note2").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDHealthSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LDHealthSchema getSchema()
    {
        LDHealthSchema aLDHealthSchema = new LDHealthSchema();
        aLDHealthSchema.setSchema(this);
        return aLDHealthSchema;
    }

    public LDHealthDB getDB()
    {
        LDHealthDB aDBOper = new LDHealthDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDHealth描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(SerialNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(HealthCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(HealthName)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(StartMoney) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(EndMoney) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(StartAge) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(EndAge) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Sex)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Note1)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Note2));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDHealth>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            HealthCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
            HealthName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                        SysConst.PACKAGESPILTER);
            StartMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 4, SysConst.PACKAGESPILTER))).doubleValue();
            EndMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 5, SysConst.PACKAGESPILTER))).doubleValue();
            StartAge = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 6, SysConst.PACKAGESPILTER))).intValue();
            EndAge = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 7, SysConst.PACKAGESPILTER))).intValue();
            Sex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                 SysConst.PACKAGESPILTER);
            Note1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                   SysConst.PACKAGESPILTER);
            Note2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                   SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDHealthSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("SerialNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SerialNo));
        }
        if (FCode.equals("HealthCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(HealthCode));
        }
        if (FCode.equals("HealthName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(HealthName));
        }
        if (FCode.equals("StartMoney"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(StartMoney));
        }
        if (FCode.equals("EndMoney"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(EndMoney));
        }
        if (FCode.equals("StartAge"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(StartAge));
        }
        if (FCode.equals("EndAge"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(EndAge));
        }
        if (FCode.equals("Sex"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Sex));
        }
        if (FCode.equals("Note1"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Note1));
        }
        if (FCode.equals("Note2"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Note2));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(SerialNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(HealthCode);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(HealthName);
                break;
            case 3:
                strFieldValue = String.valueOf(StartMoney);
                break;
            case 4:
                strFieldValue = String.valueOf(EndMoney);
                break;
            case 5:
                strFieldValue = String.valueOf(StartAge);
                break;
            case 6:
                strFieldValue = String.valueOf(EndAge);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(Sex);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(Note1);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(Note2);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("SerialNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
            {
                SerialNo = null;
            }
        }
        if (FCode.equals("HealthCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                HealthCode = FValue.trim();
            }
            else
            {
                HealthCode = null;
            }
        }
        if (FCode.equals("HealthName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                HealthName = FValue.trim();
            }
            else
            {
                HealthName = null;
            }
        }
        if (FCode.equals("StartMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                StartMoney = d;
            }
        }
        if (FCode.equals("EndMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                EndMoney = d;
            }
        }
        if (FCode.equals("StartAge"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                StartAge = i;
            }
        }
        if (FCode.equals("EndAge"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                EndAge = i;
            }
        }
        if (FCode.equals("Sex"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Sex = FValue.trim();
            }
            else
            {
                Sex = null;
            }
        }
        if (FCode.equals("Note1"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Note1 = FValue.trim();
            }
            else
            {
                Note1 = null;
            }
        }
        if (FCode.equals("Note2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Note2 = FValue.trim();
            }
            else
            {
                Note2 = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LDHealthSchema other = (LDHealthSchema) otherObject;
        return
                SerialNo.equals(other.getSerialNo())
                && HealthCode.equals(other.getHealthCode())
                && HealthName.equals(other.getHealthName())
                && StartMoney == other.getStartMoney()
                && EndMoney == other.getEndMoney()
                && StartAge == other.getStartAge()
                && EndAge == other.getEndAge()
                && Sex.equals(other.getSex())
                && Note1.equals(other.getNote1())
                && Note2.equals(other.getNote2());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("SerialNo"))
        {
            return 0;
        }
        if (strFieldName.equals("HealthCode"))
        {
            return 1;
        }
        if (strFieldName.equals("HealthName"))
        {
            return 2;
        }
        if (strFieldName.equals("StartMoney"))
        {
            return 3;
        }
        if (strFieldName.equals("EndMoney"))
        {
            return 4;
        }
        if (strFieldName.equals("StartAge"))
        {
            return 5;
        }
        if (strFieldName.equals("EndAge"))
        {
            return 6;
        }
        if (strFieldName.equals("Sex"))
        {
            return 7;
        }
        if (strFieldName.equals("Note1"))
        {
            return 8;
        }
        if (strFieldName.equals("Note2"))
        {
            return 9;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "SerialNo";
                break;
            case 1:
                strFieldName = "HealthCode";
                break;
            case 2:
                strFieldName = "HealthName";
                break;
            case 3:
                strFieldName = "StartMoney";
                break;
            case 4:
                strFieldName = "EndMoney";
                break;
            case 5:
                strFieldName = "StartAge";
                break;
            case 6:
                strFieldName = "EndAge";
                break;
            case 7:
                strFieldName = "Sex";
                break;
            case 8:
                strFieldName = "Note1";
                break;
            case 9:
                strFieldName = "Note2";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("SerialNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("HealthCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("HealthName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StartMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("EndMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("StartAge"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("EndAge"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("Sex"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Note1"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Note2"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 4:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 5:
                nFieldType = Schema.TYPE_INT;
                break;
            case 6:
                nFieldType = Schema.TYPE_INT;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
