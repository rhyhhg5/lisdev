/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.CIITC_CONFIGDB;

/*
 * <p>ClassName: CIITC_CONFIGSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2017-10-19
 */
public class CIITC_CONFIGSchema implements Schema, Cloneable
{
	// @Field
	/** Lp */
	private String lp;
	/** Username */
	private String UserName;
	/** Password */
	private String Password;
	/** Backetname */
	private String BacketName;
	/** Paths */
	private String Paths;
	/** Getmax */
	private String GetMax;

	public static final int FIELDNUM = 6;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public CIITC_CONFIGSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "lp";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		CIITC_CONFIGSchema cloned = (CIITC_CONFIGSchema) super.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getlp()
	{
		return lp;
	}
	public void setlp(String alp)
	{
		lp = alp;
	}
	public String getUserName()
	{
		return UserName;
	}
	public void setUserName(String aUserName)
	{
		UserName = aUserName;
	}
	public String getPassword()
	{
		return Password;
	}
	public void setPassword(String aPassword)
	{
		Password = aPassword;
	}
	public String getBacketName()
	{
		return BacketName;
	}
	public void setBacketName(String aBacketName)
	{
		BacketName = aBacketName;
	}
	public String getPaths()
	{
		return Paths;
	}
	public void setPaths(String aPaths)
	{
		Paths = aPaths;
	}
	public String getGetMax()
	{
		return GetMax;
	}
	public void setGetMax(String aGetMax)
	{
		GetMax = aGetMax;
	}

	/**
	* 使用另外一个 CIITC_CONFIGSchema 对象给 Schema 赋值
	* @param: aCIITC_CONFIGSchema CIITC_CONFIGSchema
	**/
	public void setSchema(CIITC_CONFIGSchema aCIITC_CONFIGSchema)
	{
		this.lp = aCIITC_CONFIGSchema.getlp();
		this.UserName = aCIITC_CONFIGSchema.getUserName();
		this.Password = aCIITC_CONFIGSchema.getPassword();
		this.BacketName = aCIITC_CONFIGSchema.getBacketName();
		this.Paths = aCIITC_CONFIGSchema.getPaths();
		this.GetMax = aCIITC_CONFIGSchema.getGetMax();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("lp") == null )
				this.lp = null;
			else
				this.lp = rs.getString("lp").trim();

			if( rs.getString("UserName") == null )
				this.UserName = null;
			else
				this.UserName = rs.getString("UserName").trim();

			if( rs.getString("Password") == null )
				this.Password = null;
			else
				this.Password = rs.getString("Password").trim();

			if( rs.getString("BacketName") == null )
				this.BacketName = null;
			else
				this.BacketName = rs.getString("BacketName").trim();

			if( rs.getString("Paths") == null )
				this.Paths = null;
			else
				this.Paths = rs.getString("Paths").trim();

			if( rs.getString("GetMax") == null )
				this.GetMax = null;
			else
				this.GetMax = rs.getString("GetMax").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的CIITC_CONFIG表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "CIITC_CONFIGSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public CIITC_CONFIGSchema getSchema()
	{
		CIITC_CONFIGSchema aCIITC_CONFIGSchema = new CIITC_CONFIGSchema();
		aCIITC_CONFIGSchema.setSchema(this);
		return aCIITC_CONFIGSchema;
	}

	public CIITC_CONFIGDB getDB()
	{
		CIITC_CONFIGDB aDBOper = new CIITC_CONFIGDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpCIITC_CONFIG描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(lp)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UserName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Password)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BacketName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Paths)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GetMax));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpCIITC_CONFIG>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			lp = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			UserName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			Password = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			BacketName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			Paths = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			GetMax = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "CIITC_CONFIGSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("lp"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(lp));
		}
		if (FCode.equals("UserName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UserName));
		}
		if (FCode.equals("Password"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Password));
		}
		if (FCode.equals("BacketName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BacketName));
		}
		if (FCode.equals("Paths"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Paths));
		}
		if (FCode.equals("GetMax"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GetMax));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(lp);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(UserName);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(Password);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(BacketName);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(Paths);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(GetMax);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("lp"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				lp = FValue.trim();
			}
			else
				lp = null;
		}
		if (FCode.equalsIgnoreCase("UserName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UserName = FValue.trim();
			}
			else
				UserName = null;
		}
		if (FCode.equalsIgnoreCase("Password"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Password = FValue.trim();
			}
			else
				Password = null;
		}
		if (FCode.equalsIgnoreCase("BacketName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BacketName = FValue.trim();
			}
			else
				BacketName = null;
		}
		if (FCode.equalsIgnoreCase("Paths"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Paths = FValue.trim();
			}
			else
				Paths = null;
		}
		if (FCode.equalsIgnoreCase("GetMax"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GetMax = FValue.trim();
			}
			else
				GetMax = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		CIITC_CONFIGSchema other = (CIITC_CONFIGSchema)otherObject;
		return
			(lp == null ? other.getlp() == null : lp.equals(other.getlp()))
			&& (UserName == null ? other.getUserName() == null : UserName.equals(other.getUserName()))
			&& (Password == null ? other.getPassword() == null : Password.equals(other.getPassword()))
			&& (BacketName == null ? other.getBacketName() == null : BacketName.equals(other.getBacketName()))
			&& (Paths == null ? other.getPaths() == null : Paths.equals(other.getPaths()))
			&& (GetMax == null ? other.getGetMax() == null : GetMax.equals(other.getGetMax()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("lp") ) {
			return 0;
		}
		if( strFieldName.equals("UserName") ) {
			return 1;
		}
		if( strFieldName.equals("Password") ) {
			return 2;
		}
		if( strFieldName.equals("BacketName") ) {
			return 3;
		}
		if( strFieldName.equals("Paths") ) {
			return 4;
		}
		if( strFieldName.equals("GetMax") ) {
			return 5;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "lp";
				break;
			case 1:
				strFieldName = "UserName";
				break;
			case 2:
				strFieldName = "Password";
				break;
			case 3:
				strFieldName = "BacketName";
				break;
			case 4:
				strFieldName = "Paths";
				break;
			case 5:
				strFieldName = "GetMax";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("lp") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UserName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Password") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BacketName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Paths") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GetMax") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
