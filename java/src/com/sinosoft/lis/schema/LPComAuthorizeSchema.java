/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LPComAuthorizeDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

/*
 * <p>ClassName: LPComAuthorizeSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-03-29
 */
public class LPComAuthorizeSchema implements Schema
{
    // @Field
    /** 上报申请号 */
    private String AppID;
    /** 上报项目 */
    private String AppItem;
    /** 险种编码 */
    private String RiskCode;
    /** 目的兼业机构编码 */
    private String AgentCom;
    /** 管理机构 */
    private String ManageCom;
    /** 保险公司id */
    private String InsureID;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 填表日期 */
    private Date FillDate;
    /** 表号 */
    private String TableNo;

    public static final int FIELDNUM = 13; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LPComAuthorizeSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[4];
        pk[0] = "AppID";
        pk[1] = "AppItem";
        pk[2] = "RiskCode";
        pk[3] = "AgentCom";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getAppID()
    {
        if (SysConst.CHANGECHARSET && AppID != null && !AppID.equals(""))
        {
            AppID = StrTool.unicodeToGBK(AppID);
        }
        return AppID;
    }

    public void setAppID(String aAppID)
    {
        AppID = aAppID;
    }

    public String getAppItem()
    {
        if (SysConst.CHANGECHARSET && AppItem != null && !AppItem.equals(""))
        {
            AppItem = StrTool.unicodeToGBK(AppItem);
        }
        return AppItem;
    }

    public void setAppItem(String aAppItem)
    {
        AppItem = aAppItem;
    }

    public String getRiskCode()
    {
        if (SysConst.CHANGECHARSET && RiskCode != null && !RiskCode.equals(""))
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getAgentCom()
    {
        if (SysConst.CHANGECHARSET && AgentCom != null && !AgentCom.equals(""))
        {
            AgentCom = StrTool.unicodeToGBK(AgentCom);
        }
        return AgentCom;
    }

    public void setAgentCom(String aAgentCom)
    {
        AgentCom = aAgentCom;
    }

    public String getManageCom()
    {
        if (SysConst.CHANGECHARSET && ManageCom != null && !ManageCom.equals(""))
        {
            ManageCom = StrTool.unicodeToGBK(ManageCom);
        }
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public String getInsureID()
    {
        if (SysConst.CHANGECHARSET && InsureID != null && !InsureID.equals(""))
        {
            InsureID = StrTool.unicodeToGBK(InsureID);
        }
        return InsureID;
    }

    public void setInsureID(String aInsureID)
    {
        InsureID = aInsureID;
    }

    public String getOperator()
    {
        if (SysConst.CHANGECHARSET && Operator != null && !Operator.equals(""))
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (SysConst.CHANGECHARSET && MakeTime != null && !MakeTime.equals(""))
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (SysConst.CHANGECHARSET && ModifyTime != null &&
            !ModifyTime.equals(""))
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    public String getFillDate()
    {
        if (FillDate != null)
        {
            return fDate.getString(FillDate);
        }
        else
        {
            return null;
        }
    }

    public void setFillDate(Date aFillDate)
    {
        FillDate = aFillDate;
    }

    public void setFillDate(String aFillDate)
    {
        if (aFillDate != null && !aFillDate.equals(""))
        {
            FillDate = fDate.getDate(aFillDate);
        }
        else
        {
            FillDate = null;
        }
    }

    public String getTableNo()
    {
        if (SysConst.CHANGECHARSET && TableNo != null && !TableNo.equals(""))
        {
            TableNo = StrTool.unicodeToGBK(TableNo);
        }
        return TableNo;
    }

    public void setTableNo(String aTableNo)
    {
        TableNo = aTableNo;
    }

    /**
     * 使用另外一个 LPComAuthorizeSchema 对象给 Schema 赋值
     * @param: aLPComAuthorizeSchema LPComAuthorizeSchema
     **/
    public void setSchema(LPComAuthorizeSchema aLPComAuthorizeSchema)
    {
        this.AppID = aLPComAuthorizeSchema.getAppID();
        this.AppItem = aLPComAuthorizeSchema.getAppItem();
        this.RiskCode = aLPComAuthorizeSchema.getRiskCode();
        this.AgentCom = aLPComAuthorizeSchema.getAgentCom();
        this.ManageCom = aLPComAuthorizeSchema.getManageCom();
        this.InsureID = aLPComAuthorizeSchema.getInsureID();
        this.Operator = aLPComAuthorizeSchema.getOperator();
        this.MakeDate = fDate.getDate(aLPComAuthorizeSchema.getMakeDate());
        this.MakeTime = aLPComAuthorizeSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLPComAuthorizeSchema.getModifyDate());
        this.ModifyTime = aLPComAuthorizeSchema.getModifyTime();
        this.FillDate = fDate.getDate(aLPComAuthorizeSchema.getFillDate());
        this.TableNo = aLPComAuthorizeSchema.getTableNo();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("AppID") == null)
            {
                this.AppID = null;
            }
            else
            {
                this.AppID = rs.getString("AppID").trim();
            }

            if (rs.getString("AppItem") == null)
            {
                this.AppItem = null;
            }
            else
            {
                this.AppItem = rs.getString("AppItem").trim();
            }

            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("AgentCom") == null)
            {
                this.AgentCom = null;
            }
            else
            {
                this.AgentCom = rs.getString("AgentCom").trim();
            }

            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("InsureID") == null)
            {
                this.InsureID = null;
            }
            else
            {
                this.InsureID = rs.getString("InsureID").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            this.FillDate = rs.getDate("FillDate");
            if (rs.getString("TableNo") == null)
            {
                this.TableNo = null;
            }
            else
            {
                this.TableNo = rs.getString("TableNo").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LPComAuthorizeSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LPComAuthorizeSchema getSchema()
    {
        LPComAuthorizeSchema aLPComAuthorizeSchema = new LPComAuthorizeSchema();
        aLPComAuthorizeSchema.setSchema(this);
        return aLPComAuthorizeSchema;
    }

    public LPComAuthorizeDB getDB()
    {
        LPComAuthorizeDB aDBOper = new LPComAuthorizeDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLPComAuthorize描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(AppID)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AppItem)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgentCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ManageCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InsureID)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(FillDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(TableNo));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLPComAuthorize>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            AppID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                   SysConst.PACKAGESPILTER);
            AppItem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                     SysConst.PACKAGESPILTER);
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            AgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                      SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                       SysConst.PACKAGESPILTER);
            InsureID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                      SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 8, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 10, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                        SysConst.PACKAGESPILTER);
            FillDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 12, SysConst.PACKAGESPILTER));
            TableNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                     SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LPComAuthorizeSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("AppID"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppID));
        }
        if (FCode.equals("AppItem"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppItem));
        }
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equals("AgentCom"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
        }
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("InsureID"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsureID));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("FillDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getFillDate()));
        }
        if (FCode.equals("TableNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TableNo));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(AppID);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(AppItem);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(AgentCom);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(InsureID);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getFillDate()));
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(TableNo);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("AppID"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppID = FValue.trim();
            }
            else
            {
                AppID = null;
            }
        }
        if (FCode.equals("AppItem"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppItem = FValue.trim();
            }
            else
            {
                AppItem = null;
            }
        }
        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("AgentCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentCom = FValue.trim();
            }
            else
            {
                AgentCom = null;
            }
        }
        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("InsureID"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InsureID = FValue.trim();
            }
            else
            {
                InsureID = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        if (FCode.equals("FillDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                FillDate = fDate.getDate(FValue);
            }
            else
            {
                FillDate = null;
            }
        }
        if (FCode.equals("TableNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                TableNo = FValue.trim();
            }
            else
            {
                TableNo = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LPComAuthorizeSchema other = (LPComAuthorizeSchema) otherObject;
        return
                AppID.equals(other.getAppID())
                && AppItem.equals(other.getAppItem())
                && RiskCode.equals(other.getRiskCode())
                && AgentCom.equals(other.getAgentCom())
                && ManageCom.equals(other.getManageCom())
                && InsureID.equals(other.getInsureID())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && fDate.getString(FillDate).equals(other.getFillDate())
                && TableNo.equals(other.getTableNo());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("AppID"))
        {
            return 0;
        }
        if (strFieldName.equals("AppItem"))
        {
            return 1;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return 2;
        }
        if (strFieldName.equals("AgentCom"))
        {
            return 3;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return 4;
        }
        if (strFieldName.equals("InsureID"))
        {
            return 5;
        }
        if (strFieldName.equals("Operator"))
        {
            return 6;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 7;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 8;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 9;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 10;
        }
        if (strFieldName.equals("FillDate"))
        {
            return 11;
        }
        if (strFieldName.equals("TableNo"))
        {
            return 12;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "AppID";
                break;
            case 1:
                strFieldName = "AppItem";
                break;
            case 2:
                strFieldName = "RiskCode";
                break;
            case 3:
                strFieldName = "AgentCom";
                break;
            case 4:
                strFieldName = "ManageCom";
                break;
            case 5:
                strFieldName = "InsureID";
                break;
            case 6:
                strFieldName = "Operator";
                break;
            case 7:
                strFieldName = "MakeDate";
                break;
            case 8:
                strFieldName = "MakeTime";
                break;
            case 9:
                strFieldName = "ModifyDate";
                break;
            case 10:
                strFieldName = "ModifyTime";
                break;
            case 11:
                strFieldName = "FillDate";
                break;
            case 12:
                strFieldName = "TableNo";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("AppID"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppItem"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsureID"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FillDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("TableNo"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
