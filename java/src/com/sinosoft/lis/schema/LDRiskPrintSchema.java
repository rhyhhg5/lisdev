/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LDRiskPrintDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

/*
 * <p>ClassName: LDRiskPrintSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-03-08
 */
public class LDRiskPrintSchema implements Schema
{
    // @Field
    /** 险种编码 */
    private String RiskCode;
    /** 条款名称 */
    private String ItemName;
    /** 条款类型 */
    private String ItemType;
    /** 文档名称 */
    private String FileName;

    public static final int FIELDNUM = 4; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LDRiskPrintSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "RiskCode";
        pk[1] = "ItemType";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getRiskCode()
    {
        if (RiskCode != null && !RiskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getItemName()
    {
        if (ItemName != null && !ItemName.equals("") && SysConst.CHANGECHARSET == true)
        {
            ItemName = StrTool.unicodeToGBK(ItemName);
        }
        return ItemName;
    }

    public void setItemName(String aItemName)
    {
        ItemName = aItemName;
    }

    public String getItemType()
    {
        if (ItemType != null && !ItemType.equals("") && SysConst.CHANGECHARSET == true)
        {
            ItemType = StrTool.unicodeToGBK(ItemType);
        }
        return ItemType;
    }

    public void setItemType(String aItemType)
    {
        ItemType = aItemType;
    }

    public String getFileName()
    {
        if (FileName != null && !FileName.equals("") && SysConst.CHANGECHARSET == true)
        {
            FileName = StrTool.unicodeToGBK(FileName);
        }
        return FileName;
    }

    public void setFileName(String aFileName)
    {
        FileName = aFileName;
    }

    /**
     * 使用另外一个 LDRiskPrintSchema 对象给 Schema 赋值
     * @param: aLDRiskPrintSchema LDRiskPrintSchema
     **/
    public void setSchema(LDRiskPrintSchema aLDRiskPrintSchema)
    {
        this.RiskCode = aLDRiskPrintSchema.getRiskCode();
        this.ItemName = aLDRiskPrintSchema.getItemName();
        this.ItemType = aLDRiskPrintSchema.getItemType();
        this.FileName = aLDRiskPrintSchema.getFileName();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("ItemName") == null)
            {
                this.ItemName = null;
            }
            else
            {
                this.ItemName = rs.getString("ItemName").trim();
            }

            if (rs.getString("ItemType") == null)
            {
                this.ItemType = null;
            }
            else
            {
                this.ItemType = rs.getString("ItemType").trim();
            }

            if (rs.getString("FileName") == null)
            {
                this.FileName = null;
            }
            else
            {
                this.FileName = rs.getString("FileName").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDRiskPrintSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LDRiskPrintSchema getSchema()
    {
        LDRiskPrintSchema aLDRiskPrintSchema = new LDRiskPrintSchema();
        aLDRiskPrintSchema.setSchema(this);
        return aLDRiskPrintSchema;
    }

    public LDRiskPrintDB getDB()
    {
        LDRiskPrintDB aDBOper = new LDRiskPrintDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDRiskPrint描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ItemName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ItemType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(FileName));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDRiskPrint>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            ItemName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            ItemType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            FileName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                      SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDRiskPrintSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskCode));
        }
        if (FCode.equals("ItemName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ItemName));
        }
        if (FCode.equals("ItemType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ItemType));
        }
        if (FCode.equals("FileName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(FileName));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ItemName);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ItemType);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(FileName);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("ItemName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ItemName = FValue.trim();
            }
            else
            {
                ItemName = null;
            }
        }
        if (FCode.equals("ItemType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ItemType = FValue.trim();
            }
            else
            {
                ItemType = null;
            }
        }
        if (FCode.equals("FileName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                FileName = FValue.trim();
            }
            else
            {
                FileName = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LDRiskPrintSchema other = (LDRiskPrintSchema) otherObject;
        return
                RiskCode.equals(other.getRiskCode())
                && ItemName.equals(other.getItemName())
                && ItemType.equals(other.getItemType())
                && FileName.equals(other.getFileName());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("RiskCode"))
        {
            return 0;
        }
        if (strFieldName.equals("ItemName"))
        {
            return 1;
        }
        if (strFieldName.equals("ItemType"))
        {
            return 2;
        }
        if (strFieldName.equals("FileName"))
        {
            return 3;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "ItemName";
                break;
            case 2:
                strFieldName = "ItemType";
                break;
            case 3:
                strFieldName = "FileName";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ItemName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ItemType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FileName"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
