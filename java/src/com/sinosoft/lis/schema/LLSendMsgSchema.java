/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LLSendMsgDB;

/*
 * <p>ClassName: LLSendMsgSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2016-11-30
 */
public class LLSendMsgSchema implements Schema, Cloneable
{
	// @Field
	/** 批次号 */
	private String BatchNo;
	/** 发送日期 */
	private Date SendDate;
	/** 发送时间 */
	private String SendTime;
	/** 交易编码 */
	private String BranchCode;
	/** 交易人员 */
	private String SendOperator;
	/** 固定值 */
	private String MsgType;
	/** 给付人数 */
	private int Rownum;
	/** 申请号 */
	private String Applno;
	/** 文档存放路径 */
	private String ExcelAddress;
	/** 受理方式 */
	private String AcceptMethod;
	/** 给付方式 */
	private String PayMethod;
	/** 联系手机 */
	private String PhoneNumber;
	/** 电子邮箱 */
	private String EMail;
	/** 赔款领取方式 */
	private String ReceiveMethod;
	/** 银行编码 */
	private String BankCode;
	/** 签约银行 */
	private String SignBank;
	/** 银行账户 */
	private String Account;
	/** 账户名 */
	private String AccountName;
	/** 申报金额 */
	private String DeclareAmount;
	/** 是否回销预付赔款标志 */
	private String AdvanceFlag;
	/** 申请操作结构 */
	private String ManageCom;
	/** 申请操作人员 */
	private String Operate;
	/** 申请人数 */
	private int AppPeoples;
	/** 邮政编码 */
	private String PostCode;
	/** 证件类型 */
	private String IDType;
	/** 证件号码 */
	private String IDNo;
	/** 备注 */
	private String Remark;
	/** 团体保单特约信息 */
	private String GrpRemark;
	/** 给付操作结构 */
	private String GEManageCom;
	/** 给付操作人员 */
	private String GEOperate;

	public static final int FIELDNUM = 30;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LLSendMsgSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[0];

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LLSendMsgSchema cloned = (LLSendMsgSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getBatchNo()
	{
		return BatchNo;
	}
	public void setBatchNo(String aBatchNo)
	{
		BatchNo = aBatchNo;
	}
	public String getSendDate()
	{
		if( SendDate != null )
			return fDate.getString(SendDate);
		else
			return null;
	}
	public void setSendDate(Date aSendDate)
	{
		SendDate = aSendDate;
	}
	public void setSendDate(String aSendDate)
	{
		if (aSendDate != null && !aSendDate.equals("") )
		{
			SendDate = fDate.getDate( aSendDate );
		}
		else
			SendDate = null;
	}

	public String getSendTime()
	{
		return SendTime;
	}
	public void setSendTime(String aSendTime)
	{
		SendTime = aSendTime;
	}
	public String getBranchCode()
	{
		return BranchCode;
	}
	public void setBranchCode(String aBranchCode)
	{
		BranchCode = aBranchCode;
	}
	public String getSendOperator()
	{
		return SendOperator;
	}
	public void setSendOperator(String aSendOperator)
	{
		SendOperator = aSendOperator;
	}
	public String getMsgType()
	{
		return MsgType;
	}
	public void setMsgType(String aMsgType)
	{
		MsgType = aMsgType;
	}
	public int getRownum()
	{
		return Rownum;
	}
	public void setRownum(int aRownum)
	{
		Rownum = aRownum;
	}
	public void setRownum(String aRownum)
	{
		if (aRownum != null && !aRownum.equals(""))
		{
			Integer tInteger = new Integer(aRownum);
			int i = tInteger.intValue();
			Rownum = i;
		}
	}

	public String getApplno()
	{
		return Applno;
	}
	public void setApplno(String aApplno)
	{
		Applno = aApplno;
	}
	public String getExcelAddress()
	{
		return ExcelAddress;
	}
	public void setExcelAddress(String aExcelAddress)
	{
		ExcelAddress = aExcelAddress;
	}
	public String getAcceptMethod()
	{
		return AcceptMethod;
	}
	public void setAcceptMethod(String aAcceptMethod)
	{
		AcceptMethod = aAcceptMethod;
	}
	public String getPayMethod()
	{
		return PayMethod;
	}
	public void setPayMethod(String aPayMethod)
	{
		PayMethod = aPayMethod;
	}
	public String getPhoneNumber()
	{
		return PhoneNumber;
	}
	public void setPhoneNumber(String aPhoneNumber)
	{
		PhoneNumber = aPhoneNumber;
	}
	public String getEMail()
	{
		return EMail;
	}
	public void setEMail(String aEMail)
	{
		EMail = aEMail;
	}
	public String getReceiveMethod()
	{
		return ReceiveMethod;
	}
	public void setReceiveMethod(String aReceiveMethod)
	{
		ReceiveMethod = aReceiveMethod;
	}
	public String getBankCode()
	{
		return BankCode;
	}
	public void setBankCode(String aBankCode)
	{
		BankCode = aBankCode;
	}
	public String getSignBank()
	{
		return SignBank;
	}
	public void setSignBank(String aSignBank)
	{
		SignBank = aSignBank;
	}
	public String getAccount()
	{
		return Account;
	}
	public void setAccount(String aAccount)
	{
		Account = aAccount;
	}
	public String getAccountName()
	{
		return AccountName;
	}
	public void setAccountName(String aAccountName)
	{
		AccountName = aAccountName;
	}
	public String getDeclareAmount()
	{
		return DeclareAmount;
	}
	public void setDeclareAmount(String aDeclareAmount)
	{
		DeclareAmount = aDeclareAmount;
	}
	public String getAdvanceFlag()
	{
		return AdvanceFlag;
	}
	public void setAdvanceFlag(String aAdvanceFlag)
	{
		AdvanceFlag = aAdvanceFlag;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getOperate()
	{
		return Operate;
	}
	public void setOperate(String aOperate)
	{
		Operate = aOperate;
	}
	public int getAppPeoples()
	{
		return AppPeoples;
	}
	public void setAppPeoples(int aAppPeoples)
	{
		AppPeoples = aAppPeoples;
	}
	public void setAppPeoples(String aAppPeoples)
	{
		if (aAppPeoples != null && !aAppPeoples.equals(""))
		{
			Integer tInteger = new Integer(aAppPeoples);
			int i = tInteger.intValue();
			AppPeoples = i;
		}
	}

	public String getPostCode()
	{
		return PostCode;
	}
	public void setPostCode(String aPostCode)
	{
		PostCode = aPostCode;
	}
	public String getIDType()
	{
		return IDType;
	}
	public void setIDType(String aIDType)
	{
		IDType = aIDType;
	}
	public String getIDNo()
	{
		return IDNo;
	}
	public void setIDNo(String aIDNo)
	{
		IDNo = aIDNo;
	}
	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
		Remark = aRemark;
	}
	public String getGrpRemark()
	{
		return GrpRemark;
	}
	public void setGrpRemark(String aGrpRemark)
	{
		GrpRemark = aGrpRemark;
	}
	public String getGEManageCom()
	{
		return GEManageCom;
	}
	public void setGEManageCom(String aGEManageCom)
	{
		GEManageCom = aGEManageCom;
	}
	public String getGEOperate()
	{
		return GEOperate;
	}
	public void setGEOperate(String aGEOperate)
	{
		GEOperate = aGEOperate;
	}

	/**
	* 使用另外一个 LLSendMsgSchema 对象给 Schema 赋值
	* @param: aLLSendMsgSchema LLSendMsgSchema
	**/
	public void setSchema(LLSendMsgSchema aLLSendMsgSchema)
	{
		this.BatchNo = aLLSendMsgSchema.getBatchNo();
		this.SendDate = fDate.getDate( aLLSendMsgSchema.getSendDate());
		this.SendTime = aLLSendMsgSchema.getSendTime();
		this.BranchCode = aLLSendMsgSchema.getBranchCode();
		this.SendOperator = aLLSendMsgSchema.getSendOperator();
		this.MsgType = aLLSendMsgSchema.getMsgType();
		this.Rownum = aLLSendMsgSchema.getRownum();
		this.Applno = aLLSendMsgSchema.getApplno();
		this.ExcelAddress = aLLSendMsgSchema.getExcelAddress();
		this.AcceptMethod = aLLSendMsgSchema.getAcceptMethod();
		this.PayMethod = aLLSendMsgSchema.getPayMethod();
		this.PhoneNumber = aLLSendMsgSchema.getPhoneNumber();
		this.EMail = aLLSendMsgSchema.getEMail();
		this.ReceiveMethod = aLLSendMsgSchema.getReceiveMethod();
		this.BankCode = aLLSendMsgSchema.getBankCode();
		this.SignBank = aLLSendMsgSchema.getSignBank();
		this.Account = aLLSendMsgSchema.getAccount();
		this.AccountName = aLLSendMsgSchema.getAccountName();
		this.DeclareAmount = aLLSendMsgSchema.getDeclareAmount();
		this.AdvanceFlag = aLLSendMsgSchema.getAdvanceFlag();
		this.ManageCom = aLLSendMsgSchema.getManageCom();
		this.Operate = aLLSendMsgSchema.getOperate();
		this.AppPeoples = aLLSendMsgSchema.getAppPeoples();
		this.PostCode = aLLSendMsgSchema.getPostCode();
		this.IDType = aLLSendMsgSchema.getIDType();
		this.IDNo = aLLSendMsgSchema.getIDNo();
		this.Remark = aLLSendMsgSchema.getRemark();
		this.GrpRemark = aLLSendMsgSchema.getGrpRemark();
		this.GEManageCom = aLLSendMsgSchema.getGEManageCom();
		this.GEOperate = aLLSendMsgSchema.getGEOperate();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("BatchNo") == null )
				this.BatchNo = null;
			else
				this.BatchNo = rs.getString("BatchNo").trim();

			this.SendDate = rs.getDate("SendDate");
			if( rs.getString("SendTime") == null )
				this.SendTime = null;
			else
				this.SendTime = rs.getString("SendTime").trim();

			if( rs.getString("BranchCode") == null )
				this.BranchCode = null;
			else
				this.BranchCode = rs.getString("BranchCode").trim();

			if( rs.getString("SendOperator") == null )
				this.SendOperator = null;
			else
				this.SendOperator = rs.getString("SendOperator").trim();

			if( rs.getString("MsgType") == null )
				this.MsgType = null;
			else
				this.MsgType = rs.getString("MsgType").trim();

			this.Rownum = rs.getInt("Rownum");
			if( rs.getString("Applno") == null )
				this.Applno = null;
			else
				this.Applno = rs.getString("Applno").trim();

			if( rs.getString("ExcelAddress") == null )
				this.ExcelAddress = null;
			else
				this.ExcelAddress = rs.getString("ExcelAddress").trim();

			if( rs.getString("AcceptMethod") == null )
				this.AcceptMethod = null;
			else
				this.AcceptMethod = rs.getString("AcceptMethod").trim();

			if( rs.getString("PayMethod") == null )
				this.PayMethod = null;
			else
				this.PayMethod = rs.getString("PayMethod").trim();

			if( rs.getString("PhoneNumber") == null )
				this.PhoneNumber = null;
			else
				this.PhoneNumber = rs.getString("PhoneNumber").trim();

			if( rs.getString("EMail") == null )
				this.EMail = null;
			else
				this.EMail = rs.getString("EMail").trim();

			if( rs.getString("ReceiveMethod") == null )
				this.ReceiveMethod = null;
			else
				this.ReceiveMethod = rs.getString("ReceiveMethod").trim();

			if( rs.getString("BankCode") == null )
				this.BankCode = null;
			else
				this.BankCode = rs.getString("BankCode").trim();

			if( rs.getString("SignBank") == null )
				this.SignBank = null;
			else
				this.SignBank = rs.getString("SignBank").trim();

			if( rs.getString("Account") == null )
				this.Account = null;
			else
				this.Account = rs.getString("Account").trim();

			if( rs.getString("AccountName") == null )
				this.AccountName = null;
			else
				this.AccountName = rs.getString("AccountName").trim();

			if( rs.getString("DeclareAmount") == null )
				this.DeclareAmount = null;
			else
				this.DeclareAmount = rs.getString("DeclareAmount").trim();

			if( rs.getString("AdvanceFlag") == null )
				this.AdvanceFlag = null;
			else
				this.AdvanceFlag = rs.getString("AdvanceFlag").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("Operate") == null )
				this.Operate = null;
			else
				this.Operate = rs.getString("Operate").trim();

			this.AppPeoples = rs.getInt("AppPeoples");
			if( rs.getString("PostCode") == null )
				this.PostCode = null;
			else
				this.PostCode = rs.getString("PostCode").trim();

			if( rs.getString("IDType") == null )
				this.IDType = null;
			else
				this.IDType = rs.getString("IDType").trim();

			if( rs.getString("IDNo") == null )
				this.IDNo = null;
			else
				this.IDNo = rs.getString("IDNo").trim();

			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

			if( rs.getString("GrpRemark") == null )
				this.GrpRemark = null;
			else
				this.GrpRemark = rs.getString("GrpRemark").trim();

			if( rs.getString("GEManageCom") == null )
				this.GEManageCom = null;
			else
				this.GEManageCom = rs.getString("GEManageCom").trim();

			if( rs.getString("GEOperate") == null )
				this.GEOperate = null;
			else
				this.GEOperate = rs.getString("GEOperate").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LLSendMsg表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLSendMsgSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LLSendMsgSchema getSchema()
	{
		LLSendMsgSchema aLLSendMsgSchema = new LLSendMsgSchema();
		aLLSendMsgSchema.setSchema(this);
		return aLLSendMsgSchema;
	}

	public LLSendMsgDB getDB()
	{
		LLSendMsgDB aDBOper = new LLSendMsgDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLSendMsg描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(BatchNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( SendDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SendTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SendOperator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MsgType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Rownum));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Applno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ExcelAddress)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AcceptMethod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayMethod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PhoneNumber)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EMail)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReceiveMethod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SignBank)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Account)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccountName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DeclareAmount)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AdvanceFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(AppPeoples));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PostCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IDType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IDNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpRemark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GEManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GEOperate));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLSendMsg>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			BatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			SendDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,SysConst.PACKAGESPILTER));
			SendTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			BranchCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			SendOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			MsgType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			Rownum= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,7,SysConst.PACKAGESPILTER))).intValue();
			Applno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			ExcelAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			AcceptMethod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			PayMethod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			PhoneNumber = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			EMail = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			ReceiveMethod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			SignBank = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			Account = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			AccountName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			DeclareAmount = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			AdvanceFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			Operate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			AppPeoples= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,23,SysConst.PACKAGESPILTER))).intValue();
			PostCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			IDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			IDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			GrpRemark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			GEManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			GEOperate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLSendMsgSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("BatchNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BatchNo));
		}
		if (FCode.equals("SendDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getSendDate()));
		}
		if (FCode.equals("SendTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SendTime));
		}
		if (FCode.equals("BranchCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchCode));
		}
		if (FCode.equals("SendOperator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SendOperator));
		}
		if (FCode.equals("MsgType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MsgType));
		}
		if (FCode.equals("Rownum"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Rownum));
		}
		if (FCode.equals("Applno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Applno));
		}
		if (FCode.equals("ExcelAddress"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ExcelAddress));
		}
		if (FCode.equals("AcceptMethod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AcceptMethod));
		}
		if (FCode.equals("PayMethod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayMethod));
		}
		if (FCode.equals("PhoneNumber"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PhoneNumber));
		}
		if (FCode.equals("EMail"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EMail));
		}
		if (FCode.equals("ReceiveMethod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiveMethod));
		}
		if (FCode.equals("BankCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
		}
		if (FCode.equals("SignBank"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SignBank));
		}
		if (FCode.equals("Account"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Account));
		}
		if (FCode.equals("AccountName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccountName));
		}
		if (FCode.equals("DeclareAmount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DeclareAmount));
		}
		if (FCode.equals("AdvanceFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AdvanceFlag));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("Operate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operate));
		}
		if (FCode.equals("AppPeoples"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppPeoples));
		}
		if (FCode.equals("PostCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PostCode));
		}
		if (FCode.equals("IDType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IDType));
		}
		if (FCode.equals("IDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (FCode.equals("GrpRemark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpRemark));
		}
		if (FCode.equals("GEManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GEManageCom));
		}
		if (FCode.equals("GEOperate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GEOperate));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(BatchNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getSendDate()));
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(SendTime);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(BranchCode);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(SendOperator);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(MsgType);
				break;
			case 6:
				strFieldValue = String.valueOf(Rownum);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(Applno);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(ExcelAddress);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(AcceptMethod);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(PayMethod);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(PhoneNumber);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(EMail);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(ReceiveMethod);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(BankCode);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(SignBank);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(Account);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(AccountName);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(DeclareAmount);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(AdvanceFlag);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(Operate);
				break;
			case 22:
				strFieldValue = String.valueOf(AppPeoples);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(PostCode);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(IDType);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(IDNo);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(GrpRemark);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(GEManageCom);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(GEOperate);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("BatchNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BatchNo = FValue.trim();
			}
			else
				BatchNo = null;
		}
		if (FCode.equalsIgnoreCase("SendDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				SendDate = fDate.getDate( FValue );
			}
			else
				SendDate = null;
		}
		if (FCode.equalsIgnoreCase("SendTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SendTime = FValue.trim();
			}
			else
				SendTime = null;
		}
		if (FCode.equalsIgnoreCase("BranchCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchCode = FValue.trim();
			}
			else
				BranchCode = null;
		}
		if (FCode.equalsIgnoreCase("SendOperator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SendOperator = FValue.trim();
			}
			else
				SendOperator = null;
		}
		if (FCode.equalsIgnoreCase("MsgType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MsgType = FValue.trim();
			}
			else
				MsgType = null;
		}
		if (FCode.equalsIgnoreCase("Rownum"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				Rownum = i;
			}
		}
		if (FCode.equalsIgnoreCase("Applno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Applno = FValue.trim();
			}
			else
				Applno = null;
		}
		if (FCode.equalsIgnoreCase("ExcelAddress"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ExcelAddress = FValue.trim();
			}
			else
				ExcelAddress = null;
		}
		if (FCode.equalsIgnoreCase("AcceptMethod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AcceptMethod = FValue.trim();
			}
			else
				AcceptMethod = null;
		}
		if (FCode.equalsIgnoreCase("PayMethod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayMethod = FValue.trim();
			}
			else
				PayMethod = null;
		}
		if (FCode.equalsIgnoreCase("PhoneNumber"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PhoneNumber = FValue.trim();
			}
			else
				PhoneNumber = null;
		}
		if (FCode.equalsIgnoreCase("EMail"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EMail = FValue.trim();
			}
			else
				EMail = null;
		}
		if (FCode.equalsIgnoreCase("ReceiveMethod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReceiveMethod = FValue.trim();
			}
			else
				ReceiveMethod = null;
		}
		if (FCode.equalsIgnoreCase("BankCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankCode = FValue.trim();
			}
			else
				BankCode = null;
		}
		if (FCode.equalsIgnoreCase("SignBank"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SignBank = FValue.trim();
			}
			else
				SignBank = null;
		}
		if (FCode.equalsIgnoreCase("Account"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Account = FValue.trim();
			}
			else
				Account = null;
		}
		if (FCode.equalsIgnoreCase("AccountName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccountName = FValue.trim();
			}
			else
				AccountName = null;
		}
		if (FCode.equalsIgnoreCase("DeclareAmount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DeclareAmount = FValue.trim();
			}
			else
				DeclareAmount = null;
		}
		if (FCode.equalsIgnoreCase("AdvanceFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AdvanceFlag = FValue.trim();
			}
			else
				AdvanceFlag = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("Operate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operate = FValue.trim();
			}
			else
				Operate = null;
		}
		if (FCode.equalsIgnoreCase("AppPeoples"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				AppPeoples = i;
			}
		}
		if (FCode.equalsIgnoreCase("PostCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PostCode = FValue.trim();
			}
			else
				PostCode = null;
		}
		if (FCode.equalsIgnoreCase("IDType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IDType = FValue.trim();
			}
			else
				IDType = null;
		}
		if (FCode.equalsIgnoreCase("IDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IDNo = FValue.trim();
			}
			else
				IDNo = null;
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		if (FCode.equalsIgnoreCase("GrpRemark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpRemark = FValue.trim();
			}
			else
				GrpRemark = null;
		}
		if (FCode.equalsIgnoreCase("GEManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GEManageCom = FValue.trim();
			}
			else
				GEManageCom = null;
		}
		if (FCode.equalsIgnoreCase("GEOperate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GEOperate = FValue.trim();
			}
			else
				GEOperate = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LLSendMsgSchema other = (LLSendMsgSchema)otherObject;
		return
			(BatchNo == null ? other.getBatchNo() == null : BatchNo.equals(other.getBatchNo()))
			&& (SendDate == null ? other.getSendDate() == null : fDate.getString(SendDate).equals(other.getSendDate()))
			&& (SendTime == null ? other.getSendTime() == null : SendTime.equals(other.getSendTime()))
			&& (BranchCode == null ? other.getBranchCode() == null : BranchCode.equals(other.getBranchCode()))
			&& (SendOperator == null ? other.getSendOperator() == null : SendOperator.equals(other.getSendOperator()))
			&& (MsgType == null ? other.getMsgType() == null : MsgType.equals(other.getMsgType()))
			&& Rownum == other.getRownum()
			&& (Applno == null ? other.getApplno() == null : Applno.equals(other.getApplno()))
			&& (ExcelAddress == null ? other.getExcelAddress() == null : ExcelAddress.equals(other.getExcelAddress()))
			&& (AcceptMethod == null ? other.getAcceptMethod() == null : AcceptMethod.equals(other.getAcceptMethod()))
			&& (PayMethod == null ? other.getPayMethod() == null : PayMethod.equals(other.getPayMethod()))
			&& (PhoneNumber == null ? other.getPhoneNumber() == null : PhoneNumber.equals(other.getPhoneNumber()))
			&& (EMail == null ? other.getEMail() == null : EMail.equals(other.getEMail()))
			&& (ReceiveMethod == null ? other.getReceiveMethod() == null : ReceiveMethod.equals(other.getReceiveMethod()))
			&& (BankCode == null ? other.getBankCode() == null : BankCode.equals(other.getBankCode()))
			&& (SignBank == null ? other.getSignBank() == null : SignBank.equals(other.getSignBank()))
			&& (Account == null ? other.getAccount() == null : Account.equals(other.getAccount()))
			&& (AccountName == null ? other.getAccountName() == null : AccountName.equals(other.getAccountName()))
			&& (DeclareAmount == null ? other.getDeclareAmount() == null : DeclareAmount.equals(other.getDeclareAmount()))
			&& (AdvanceFlag == null ? other.getAdvanceFlag() == null : AdvanceFlag.equals(other.getAdvanceFlag()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (Operate == null ? other.getOperate() == null : Operate.equals(other.getOperate()))
			&& AppPeoples == other.getAppPeoples()
			&& (PostCode == null ? other.getPostCode() == null : PostCode.equals(other.getPostCode()))
			&& (IDType == null ? other.getIDType() == null : IDType.equals(other.getIDType()))
			&& (IDNo == null ? other.getIDNo() == null : IDNo.equals(other.getIDNo()))
			&& (Remark == null ? other.getRemark() == null : Remark.equals(other.getRemark()))
			&& (GrpRemark == null ? other.getGrpRemark() == null : GrpRemark.equals(other.getGrpRemark()))
			&& (GEManageCom == null ? other.getGEManageCom() == null : GEManageCom.equals(other.getGEManageCom()))
			&& (GEOperate == null ? other.getGEOperate() == null : GEOperate.equals(other.getGEOperate()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("BatchNo") ) {
			return 0;
		}
		if( strFieldName.equals("SendDate") ) {
			return 1;
		}
		if( strFieldName.equals("SendTime") ) {
			return 2;
		}
		if( strFieldName.equals("BranchCode") ) {
			return 3;
		}
		if( strFieldName.equals("SendOperator") ) {
			return 4;
		}
		if( strFieldName.equals("MsgType") ) {
			return 5;
		}
		if( strFieldName.equals("Rownum") ) {
			return 6;
		}
		if( strFieldName.equals("Applno") ) {
			return 7;
		}
		if( strFieldName.equals("ExcelAddress") ) {
			return 8;
		}
		if( strFieldName.equals("AcceptMethod") ) {
			return 9;
		}
		if( strFieldName.equals("PayMethod") ) {
			return 10;
		}
		if( strFieldName.equals("PhoneNumber") ) {
			return 11;
		}
		if( strFieldName.equals("EMail") ) {
			return 12;
		}
		if( strFieldName.equals("ReceiveMethod") ) {
			return 13;
		}
		if( strFieldName.equals("BankCode") ) {
			return 14;
		}
		if( strFieldName.equals("SignBank") ) {
			return 15;
		}
		if( strFieldName.equals("Account") ) {
			return 16;
		}
		if( strFieldName.equals("AccountName") ) {
			return 17;
		}
		if( strFieldName.equals("DeclareAmount") ) {
			return 18;
		}
		if( strFieldName.equals("AdvanceFlag") ) {
			return 19;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 20;
		}
		if( strFieldName.equals("Operate") ) {
			return 21;
		}
		if( strFieldName.equals("AppPeoples") ) {
			return 22;
		}
		if( strFieldName.equals("PostCode") ) {
			return 23;
		}
		if( strFieldName.equals("IDType") ) {
			return 24;
		}
		if( strFieldName.equals("IDNo") ) {
			return 25;
		}
		if( strFieldName.equals("Remark") ) {
			return 26;
		}
		if( strFieldName.equals("GrpRemark") ) {
			return 27;
		}
		if( strFieldName.equals("GEManageCom") ) {
			return 28;
		}
		if( strFieldName.equals("GEOperate") ) {
			return 29;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "BatchNo";
				break;
			case 1:
				strFieldName = "SendDate";
				break;
			case 2:
				strFieldName = "SendTime";
				break;
			case 3:
				strFieldName = "BranchCode";
				break;
			case 4:
				strFieldName = "SendOperator";
				break;
			case 5:
				strFieldName = "MsgType";
				break;
			case 6:
				strFieldName = "Rownum";
				break;
			case 7:
				strFieldName = "Applno";
				break;
			case 8:
				strFieldName = "ExcelAddress";
				break;
			case 9:
				strFieldName = "AcceptMethod";
				break;
			case 10:
				strFieldName = "PayMethod";
				break;
			case 11:
				strFieldName = "PhoneNumber";
				break;
			case 12:
				strFieldName = "EMail";
				break;
			case 13:
				strFieldName = "ReceiveMethod";
				break;
			case 14:
				strFieldName = "BankCode";
				break;
			case 15:
				strFieldName = "SignBank";
				break;
			case 16:
				strFieldName = "Account";
				break;
			case 17:
				strFieldName = "AccountName";
				break;
			case 18:
				strFieldName = "DeclareAmount";
				break;
			case 19:
				strFieldName = "AdvanceFlag";
				break;
			case 20:
				strFieldName = "ManageCom";
				break;
			case 21:
				strFieldName = "Operate";
				break;
			case 22:
				strFieldName = "AppPeoples";
				break;
			case 23:
				strFieldName = "PostCode";
				break;
			case 24:
				strFieldName = "IDType";
				break;
			case 25:
				strFieldName = "IDNo";
				break;
			case 26:
				strFieldName = "Remark";
				break;
			case 27:
				strFieldName = "GrpRemark";
				break;
			case 28:
				strFieldName = "GEManageCom";
				break;
			case 29:
				strFieldName = "GEOperate";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("BatchNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SendDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("SendTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SendOperator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MsgType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Rownum") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("Applno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ExcelAddress") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AcceptMethod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayMethod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PhoneNumber") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EMail") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReceiveMethod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SignBank") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Account") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccountName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DeclareAmount") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AdvanceFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppPeoples") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("PostCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IDType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IDNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpRemark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GEManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GEOperate") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_INT;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_INT;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
