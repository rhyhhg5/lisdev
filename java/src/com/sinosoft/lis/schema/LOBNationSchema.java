/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LOBNationDB;

/*
 * <p>ClassName: LOBNationSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-09-07
 */
public class LOBNationSchema implements Schema, Cloneable {
    // @Field
    /** 国家编码 */
    private String NationNo;
    /** 合同号码 */
    private String ContNo;
    /** 集体合同号码 */
    private String GrpContNo;
    /** 地址号码 */
    private String AddressNo;
    /** 国家中文名称 */
    private String ChineseName;
    /** 国家英文名称 */
    private String EnglishName;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 11; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LOBNationSchema() {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "NationNo";
        pk[1] = "ContNo";
        pk[2] = "GrpContNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LOBNationSchema cloned = (LOBNationSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getNationNo() {
        return NationNo;
    }

    public void setNationNo(String aNationNo) {
        NationNo = aNationNo;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }

    public String getGrpContNo() {
        return GrpContNo;
    }

    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }

    public String getAddressNo() {
        return AddressNo;
    }

    public void setAddressNo(String aAddressNo) {
        AddressNo = aAddressNo;
    }

    public String getChineseName() {
        return ChineseName;
    }

    public void setChineseName(String aChineseName) {
        ChineseName = aChineseName;
    }

    public String getEnglishName() {
        return EnglishName;
    }

    public void setEnglishName(String aEnglishName) {
        EnglishName = aEnglishName;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LOBNationSchema 对象给 Schema 赋值
     * @param: aLOBNationSchema LOBNationSchema
     **/
    public void setSchema(LOBNationSchema aLOBNationSchema) {
        this.NationNo = aLOBNationSchema.getNationNo();
        this.ContNo = aLOBNationSchema.getContNo();
        this.GrpContNo = aLOBNationSchema.getGrpContNo();
        this.AddressNo = aLOBNationSchema.getAddressNo();
        this.ChineseName = aLOBNationSchema.getChineseName();
        this.EnglishName = aLOBNationSchema.getEnglishName();
        this.Operator = aLOBNationSchema.getOperator();
        this.MakeDate = fDate.getDate(aLOBNationSchema.getMakeDate());
        this.MakeTime = aLOBNationSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLOBNationSchema.getModifyDate());
        this.ModifyTime = aLOBNationSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("NationNo") == null) {
                this.NationNo = null;
            } else {
                this.NationNo = rs.getString("NationNo").trim();
            }

            if (rs.getString("ContNo") == null) {
                this.ContNo = null;
            } else {
                this.ContNo = rs.getString("ContNo").trim();
            }

            if (rs.getString("GrpContNo") == null) {
                this.GrpContNo = null;
            } else {
                this.GrpContNo = rs.getString("GrpContNo").trim();
            }

            if (rs.getString("AddressNo") == null) {
                this.AddressNo = null;
            } else {
                this.AddressNo = rs.getString("AddressNo").trim();
            }

            if (rs.getString("ChineseName") == null) {
                this.ChineseName = null;
            } else {
                this.ChineseName = rs.getString("ChineseName").trim();
            }

            if (rs.getString("EnglishName") == null) {
                this.EnglishName = null;
            } else {
                this.EnglishName = rs.getString("EnglishName").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LOBNation表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LOBNationSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LOBNationSchema getSchema() {
        LOBNationSchema aLOBNationSchema = new LOBNationSchema();
        aLOBNationSchema.setSchema(this);
        return aLOBNationSchema;
    }

    public LOBNationDB getDB() {
        LOBNationDB aDBOper = new LOBNationDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOBNation描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(NationNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpContNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AddressNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ChineseName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EnglishName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOBNation>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            NationNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                    SysConst.PACKAGESPILTER);
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                       SysConst.PACKAGESPILTER);
            AddressNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                       SysConst.PACKAGESPILTER);
            ChineseName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                         SysConst.PACKAGESPILTER);
            EnglishName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                         SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 8, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 10, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                        SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LOBNationSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("NationNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NationNo));
        }
        if (FCode.equals("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equals("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equals("AddressNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AddressNo));
        }
        if (FCode.equals("ChineseName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChineseName));
        }
        if (FCode.equals("EnglishName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EnglishName));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(NationNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(ContNo);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(GrpContNo);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(AddressNo);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(ChineseName);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(EnglishName);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("NationNo")) {
            if (FValue != null && !FValue.equals("")) {
                NationNo = FValue.trim();
            } else {
                NationNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if (FValue != null && !FValue.equals("")) {
                ContNo = FValue.trim();
            } else {
                ContNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if (FValue != null && !FValue.equals("")) {
                GrpContNo = FValue.trim();
            } else {
                GrpContNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("AddressNo")) {
            if (FValue != null && !FValue.equals("")) {
                AddressNo = FValue.trim();
            } else {
                AddressNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ChineseName")) {
            if (FValue != null && !FValue.equals("")) {
                ChineseName = FValue.trim();
            } else {
                ChineseName = null;
            }
        }
        if (FCode.equalsIgnoreCase("EnglishName")) {
            if (FValue != null && !FValue.equals("")) {
                EnglishName = FValue.trim();
            } else {
                EnglishName = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LOBNationSchema other = (LOBNationSchema) otherObject;
        return
                NationNo.equals(other.getNationNo())
                && ContNo.equals(other.getContNo())
                && GrpContNo.equals(other.getGrpContNo())
                && AddressNo.equals(other.getAddressNo())
                && ChineseName.equals(other.getChineseName())
                && EnglishName.equals(other.getEnglishName())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("NationNo")) {
            return 0;
        }
        if (strFieldName.equals("ContNo")) {
            return 1;
        }
        if (strFieldName.equals("GrpContNo")) {
            return 2;
        }
        if (strFieldName.equals("AddressNo")) {
            return 3;
        }
        if (strFieldName.equals("ChineseName")) {
            return 4;
        }
        if (strFieldName.equals("EnglishName")) {
            return 5;
        }
        if (strFieldName.equals("Operator")) {
            return 6;
        }
        if (strFieldName.equals("MakeDate")) {
            return 7;
        }
        if (strFieldName.equals("MakeTime")) {
            return 8;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 9;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 10;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "NationNo";
            break;
        case 1:
            strFieldName = "ContNo";
            break;
        case 2:
            strFieldName = "GrpContNo";
            break;
        case 3:
            strFieldName = "AddressNo";
            break;
        case 4:
            strFieldName = "ChineseName";
            break;
        case 5:
            strFieldName = "EnglishName";
            break;
        case 6:
            strFieldName = "Operator";
            break;
        case 7:
            strFieldName = "MakeDate";
            break;
        case 8:
            strFieldName = "MakeTime";
            break;
        case 9:
            strFieldName = "ModifyDate";
            break;
        case 10:
            strFieldName = "ModifyTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("NationNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpContNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AddressNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ChineseName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EnglishName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
