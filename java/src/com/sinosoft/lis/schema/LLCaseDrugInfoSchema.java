/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LLCaseDrugInfoDB;

/*
 * <p>ClassName: LLCaseDrugInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 受益人
 * @CreateDate：2015-01-30
 */
public class LLCaseDrugInfoSchema implements Schema, Cloneable
{
	// @Field
	/** 流水号 */
	private String SerialNo;
	/** 药品编码 */
	private String DrugCode;
	/** 药品类别 */
	private String DrugCategory;
	/** 药品通用名称 */
	private String DrugGenericName;
	/** 商品名 */
	private String DrugTradeName;
	/** 英文名 */
	private String DrugEnglishName;
	/** 药品分类 */
	private String DrugClassification;
	/** 次类别 */
	private String SecondCategory;
	/** 医保剂型 */
	private String Formulations;
	/** 药品规格 */
	private String Specifications;
	/** 拼音简码 */
	private String Phonetic;
	/** 生产企业名称 */
	private String Manufacturer;
	/** 包装信息 */
	private String PackageMessage;
	/** 给药途径 */
	private String Administration;
	/** 医保序号 */
	private String HealthNo;
	/** 费用等级 */
	private String CostLevel;
	/** 限定支付范围 */
	private String PaymentScope;
	/** 限门诊使用 */
	private String OutpatientLimit;
	/** 限住院使用 */
	private String HospitalLimit;
	/** 工伤标识 */
	private String InjuriesMark;
	/** 生育标识 */
	private String FertilityMark;
	/** 最高限价 */
	private double PriceCeiling;
	/** 备注 */
	private String Remark;
	/** 管理机构 */
	private String MngCom;
	/** 地区编码 */
	private String AreaCode;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 30;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LLCaseDrugInfoSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "SerialNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LLCaseDrugInfoSchema cloned = (LLCaseDrugInfoSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getSerialNo()
	{
		return SerialNo;
	}
	public void setSerialNo(String aSerialNo)
	{
		SerialNo = aSerialNo;
	}
	public String getDrugCode()
	{
		return DrugCode;
	}
	public void setDrugCode(String aDrugCode)
	{
		DrugCode = aDrugCode;
	}
	public String getDrugCategory()
	{
		return DrugCategory;
	}
	public void setDrugCategory(String aDrugCategory)
	{
		DrugCategory = aDrugCategory;
	}
	public String getDrugGenericName()
	{
		return DrugGenericName;
	}
	public void setDrugGenericName(String aDrugGenericName)
	{
		DrugGenericName = aDrugGenericName;
	}
	public String getDrugTradeName()
	{
		return DrugTradeName;
	}
	public void setDrugTradeName(String aDrugTradeName)
	{
		DrugTradeName = aDrugTradeName;
	}
	public String getDrugEnglishName()
	{
		return DrugEnglishName;
	}
	public void setDrugEnglishName(String aDrugEnglishName)
	{
		DrugEnglishName = aDrugEnglishName;
	}
	public String getDrugClassification()
	{
		return DrugClassification;
	}
	public void setDrugClassification(String aDrugClassification)
	{
		DrugClassification = aDrugClassification;
	}
	public String getSecondCategory()
	{
		return SecondCategory;
	}
	public void setSecondCategory(String aSecondCategory)
	{
		SecondCategory = aSecondCategory;
	}
	public String getFormulations()
	{
		return Formulations;
	}
	public void setFormulations(String aFormulations)
	{
		Formulations = aFormulations;
	}
	public String getSpecifications()
	{
		return Specifications;
	}
	public void setSpecifications(String aSpecifications)
	{
		Specifications = aSpecifications;
	}
	public String getPhonetic()
	{
		return Phonetic;
	}
	public void setPhonetic(String aPhonetic)
	{
		Phonetic = aPhonetic;
	}
	public String getManufacturer()
	{
		return Manufacturer;
	}
	public void setManufacturer(String aManufacturer)
	{
		Manufacturer = aManufacturer;
	}
	public String getPackageMessage()
	{
		return PackageMessage;
	}
	public void setPackageMessage(String aPackageMessage)
	{
		PackageMessage = aPackageMessage;
	}
	public String getAdministration()
	{
		return Administration;
	}
	public void setAdministration(String aAdministration)
	{
		Administration = aAdministration;
	}
	public String getHealthNo()
	{
		return HealthNo;
	}
	public void setHealthNo(String aHealthNo)
	{
		HealthNo = aHealthNo;
	}
	public String getCostLevel()
	{
		return CostLevel;
	}
	public void setCostLevel(String aCostLevel)
	{
		CostLevel = aCostLevel;
	}
	public String getPaymentScope()
	{
		return PaymentScope;
	}
	public void setPaymentScope(String aPaymentScope)
	{
		PaymentScope = aPaymentScope;
	}
	public String getOutpatientLimit()
	{
		return OutpatientLimit;
	}
	public void setOutpatientLimit(String aOutpatientLimit)
	{
		OutpatientLimit = aOutpatientLimit;
	}
	public String getHospitalLimit()
	{
		return HospitalLimit;
	}
	public void setHospitalLimit(String aHospitalLimit)
	{
		HospitalLimit = aHospitalLimit;
	}
	public String getInjuriesMark()
	{
		return InjuriesMark;
	}
	public void setInjuriesMark(String aInjuriesMark)
	{
		InjuriesMark = aInjuriesMark;
	}
	public String getFertilityMark()
	{
		return FertilityMark;
	}
	public void setFertilityMark(String aFertilityMark)
	{
		FertilityMark = aFertilityMark;
	}
	public double getPriceCeiling()
	{
		return PriceCeiling;
	}
	public void setPriceCeiling(double aPriceCeiling)
	{
		PriceCeiling = Arith.round(aPriceCeiling,2);
	}
	public void setPriceCeiling(String aPriceCeiling)
	{
		if (aPriceCeiling != null && !aPriceCeiling.equals(""))
		{
			Double tDouble = new Double(aPriceCeiling);
			double d = tDouble.doubleValue();
                PriceCeiling = Arith.round(d,2);
		}
	}

	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
		Remark = aRemark;
	}
	public String getMngCom()
	{
		return MngCom;
	}
	public void setMngCom(String aMngCom)
	{
		MngCom = aMngCom;
	}
	public String getAreaCode()
	{
		return AreaCode;
	}
	public void setAreaCode(String aAreaCode)
	{
		AreaCode = aAreaCode;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LLCaseDrugInfoSchema 对象给 Schema 赋值
	* @param: aLLCaseDrugInfoSchema LLCaseDrugInfoSchema
	**/
	public void setSchema(LLCaseDrugInfoSchema aLLCaseDrugInfoSchema)
	{
		this.SerialNo = aLLCaseDrugInfoSchema.getSerialNo();
		this.DrugCode = aLLCaseDrugInfoSchema.getDrugCode();
		this.DrugCategory = aLLCaseDrugInfoSchema.getDrugCategory();
		this.DrugGenericName = aLLCaseDrugInfoSchema.getDrugGenericName();
		this.DrugTradeName = aLLCaseDrugInfoSchema.getDrugTradeName();
		this.DrugEnglishName = aLLCaseDrugInfoSchema.getDrugEnglishName();
		this.DrugClassification = aLLCaseDrugInfoSchema.getDrugClassification();
		this.SecondCategory = aLLCaseDrugInfoSchema.getSecondCategory();
		this.Formulations = aLLCaseDrugInfoSchema.getFormulations();
		this.Specifications = aLLCaseDrugInfoSchema.getSpecifications();
		this.Phonetic = aLLCaseDrugInfoSchema.getPhonetic();
		this.Manufacturer = aLLCaseDrugInfoSchema.getManufacturer();
		this.PackageMessage = aLLCaseDrugInfoSchema.getPackageMessage();
		this.Administration = aLLCaseDrugInfoSchema.getAdministration();
		this.HealthNo = aLLCaseDrugInfoSchema.getHealthNo();
		this.CostLevel = aLLCaseDrugInfoSchema.getCostLevel();
		this.PaymentScope = aLLCaseDrugInfoSchema.getPaymentScope();
		this.OutpatientLimit = aLLCaseDrugInfoSchema.getOutpatientLimit();
		this.HospitalLimit = aLLCaseDrugInfoSchema.getHospitalLimit();
		this.InjuriesMark = aLLCaseDrugInfoSchema.getInjuriesMark();
		this.FertilityMark = aLLCaseDrugInfoSchema.getFertilityMark();
		this.PriceCeiling = aLLCaseDrugInfoSchema.getPriceCeiling();
		this.Remark = aLLCaseDrugInfoSchema.getRemark();
		this.MngCom = aLLCaseDrugInfoSchema.getMngCom();
		this.AreaCode = aLLCaseDrugInfoSchema.getAreaCode();
		this.Operator = aLLCaseDrugInfoSchema.getOperator();
		this.MakeDate = fDate.getDate( aLLCaseDrugInfoSchema.getMakeDate());
		this.MakeTime = aLLCaseDrugInfoSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLLCaseDrugInfoSchema.getModifyDate());
		this.ModifyTime = aLLCaseDrugInfoSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("SerialNo") == null )
				this.SerialNo = null;
			else
				this.SerialNo = rs.getString("SerialNo").trim();

			if( rs.getString("DrugCode") == null )
				this.DrugCode = null;
			else
				this.DrugCode = rs.getString("DrugCode").trim();

			if( rs.getString("DrugCategory") == null )
				this.DrugCategory = null;
			else
				this.DrugCategory = rs.getString("DrugCategory").trim();

			if( rs.getString("DrugGenericName") == null )
				this.DrugGenericName = null;
			else
				this.DrugGenericName = rs.getString("DrugGenericName").trim();

			if( rs.getString("DrugTradeName") == null )
				this.DrugTradeName = null;
			else
				this.DrugTradeName = rs.getString("DrugTradeName").trim();

			if( rs.getString("DrugEnglishName") == null )
				this.DrugEnglishName = null;
			else
				this.DrugEnglishName = rs.getString("DrugEnglishName").trim();

			if( rs.getString("DrugClassification") == null )
				this.DrugClassification = null;
			else
				this.DrugClassification = rs.getString("DrugClassification").trim();

			if( rs.getString("SecondCategory") == null )
				this.SecondCategory = null;
			else
				this.SecondCategory = rs.getString("SecondCategory").trim();

			if( rs.getString("Formulations") == null )
				this.Formulations = null;
			else
				this.Formulations = rs.getString("Formulations").trim();

			if( rs.getString("Specifications") == null )
				this.Specifications = null;
			else
				this.Specifications = rs.getString("Specifications").trim();

			if( rs.getString("Phonetic") == null )
				this.Phonetic = null;
			else
				this.Phonetic = rs.getString("Phonetic").trim();

			if( rs.getString("Manufacturer") == null )
				this.Manufacturer = null;
			else
				this.Manufacturer = rs.getString("Manufacturer").trim();

			if( rs.getString("PackageMessage") == null )
				this.PackageMessage = null;
			else
				this.PackageMessage = rs.getString("PackageMessage").trim();

			if( rs.getString("Administration") == null )
				this.Administration = null;
			else
				this.Administration = rs.getString("Administration").trim();

			if( rs.getString("HealthNo") == null )
				this.HealthNo = null;
			else
				this.HealthNo = rs.getString("HealthNo").trim();

			if( rs.getString("CostLevel") == null )
				this.CostLevel = null;
			else
				this.CostLevel = rs.getString("CostLevel").trim();

			if( rs.getString("PaymentScope") == null )
				this.PaymentScope = null;
			else
				this.PaymentScope = rs.getString("PaymentScope").trim();

			if( rs.getString("OutpatientLimit") == null )
				this.OutpatientLimit = null;
			else
				this.OutpatientLimit = rs.getString("OutpatientLimit").trim();

			if( rs.getString("HospitalLimit") == null )
				this.HospitalLimit = null;
			else
				this.HospitalLimit = rs.getString("HospitalLimit").trim();

			if( rs.getString("InjuriesMark") == null )
				this.InjuriesMark = null;
			else
				this.InjuriesMark = rs.getString("InjuriesMark").trim();

			if( rs.getString("FertilityMark") == null )
				this.FertilityMark = null;
			else
				this.FertilityMark = rs.getString("FertilityMark").trim();

			this.PriceCeiling = rs.getDouble("PriceCeiling");
			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

			if( rs.getString("MngCom") == null )
				this.MngCom = null;
			else
				this.MngCom = rs.getString("MngCom").trim();

			if( rs.getString("AreaCode") == null )
				this.AreaCode = null;
			else
				this.AreaCode = rs.getString("AreaCode").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LLCaseDrugInfo表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLCaseDrugInfoSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LLCaseDrugInfoSchema getSchema()
	{
		LLCaseDrugInfoSchema aLLCaseDrugInfoSchema = new LLCaseDrugInfoSchema();
		aLLCaseDrugInfoSchema.setSchema(this);
		return aLLCaseDrugInfoSchema;
	}

	public LLCaseDrugInfoDB getDB()
	{
		LLCaseDrugInfoDB aDBOper = new LLCaseDrugInfoDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLCaseDrugInfo描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(SerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DrugCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DrugCategory)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DrugGenericName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DrugTradeName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DrugEnglishName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DrugClassification)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SecondCategory)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Formulations)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Specifications)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Phonetic)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Manufacturer)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PackageMessage)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Administration)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HealthNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CostLevel)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PaymentScope)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OutpatientLimit)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HospitalLimit)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InjuriesMark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FertilityMark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PriceCeiling));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MngCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AreaCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLCaseDrugInfo>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			DrugCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			DrugCategory = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			DrugGenericName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			DrugTradeName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			DrugEnglishName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			DrugClassification = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			SecondCategory = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			Formulations = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			Specifications = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			Phonetic = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			Manufacturer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			PackageMessage = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			Administration = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			HealthNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			CostLevel = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			PaymentScope = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			OutpatientLimit = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			HospitalLimit = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			InjuriesMark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			FertilityMark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			PriceCeiling = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,22,SysConst.PACKAGESPILTER))).doubleValue();
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			MngCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			AreaCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLCaseDrugInfoSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("SerialNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
		}
		if (FCode.equals("DrugCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DrugCode));
		}
		if (FCode.equals("DrugCategory"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DrugCategory));
		}
		if (FCode.equals("DrugGenericName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DrugGenericName));
		}
		if (FCode.equals("DrugTradeName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DrugTradeName));
		}
		if (FCode.equals("DrugEnglishName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DrugEnglishName));
		}
		if (FCode.equals("DrugClassification"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DrugClassification));
		}
		if (FCode.equals("SecondCategory"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SecondCategory));
		}
		if (FCode.equals("Formulations"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Formulations));
		}
		if (FCode.equals("Specifications"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Specifications));
		}
		if (FCode.equals("Phonetic"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Phonetic));
		}
		if (FCode.equals("Manufacturer"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Manufacturer));
		}
		if (FCode.equals("PackageMessage"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PackageMessage));
		}
		if (FCode.equals("Administration"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Administration));
		}
		if (FCode.equals("HealthNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HealthNo));
		}
		if (FCode.equals("CostLevel"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CostLevel));
		}
		if (FCode.equals("PaymentScope"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PaymentScope));
		}
		if (FCode.equals("OutpatientLimit"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OutpatientLimit));
		}
		if (FCode.equals("HospitalLimit"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HospitalLimit));
		}
		if (FCode.equals("InjuriesMark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InjuriesMark));
		}
		if (FCode.equals("FertilityMark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FertilityMark));
		}
		if (FCode.equals("PriceCeiling"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PriceCeiling));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (FCode.equals("MngCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MngCom));
		}
		if (FCode.equals("AreaCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AreaCode));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(SerialNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(DrugCode);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(DrugCategory);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(DrugGenericName);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(DrugTradeName);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(DrugEnglishName);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(DrugClassification);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(SecondCategory);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(Formulations);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(Specifications);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(Phonetic);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(Manufacturer);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(PackageMessage);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(Administration);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(HealthNo);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(CostLevel);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(PaymentScope);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(OutpatientLimit);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(HospitalLimit);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(InjuriesMark);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(FertilityMark);
				break;
			case 21:
				strFieldValue = String.valueOf(PriceCeiling);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(MngCom);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(AreaCode);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("SerialNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SerialNo = FValue.trim();
			}
			else
				SerialNo = null;
		}
		if (FCode.equalsIgnoreCase("DrugCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DrugCode = FValue.trim();
			}
			else
				DrugCode = null;
		}
		if (FCode.equalsIgnoreCase("DrugCategory"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DrugCategory = FValue.trim();
			}
			else
				DrugCategory = null;
		}
		if (FCode.equalsIgnoreCase("DrugGenericName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DrugGenericName = FValue.trim();
			}
			else
				DrugGenericName = null;
		}
		if (FCode.equalsIgnoreCase("DrugTradeName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DrugTradeName = FValue.trim();
			}
			else
				DrugTradeName = null;
		}
		if (FCode.equalsIgnoreCase("DrugEnglishName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DrugEnglishName = FValue.trim();
			}
			else
				DrugEnglishName = null;
		}
		if (FCode.equalsIgnoreCase("DrugClassification"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DrugClassification = FValue.trim();
			}
			else
				DrugClassification = null;
		}
		if (FCode.equalsIgnoreCase("SecondCategory"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SecondCategory = FValue.trim();
			}
			else
				SecondCategory = null;
		}
		if (FCode.equalsIgnoreCase("Formulations"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Formulations = FValue.trim();
			}
			else
				Formulations = null;
		}
		if (FCode.equalsIgnoreCase("Specifications"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Specifications = FValue.trim();
			}
			else
				Specifications = null;
		}
		if (FCode.equalsIgnoreCase("Phonetic"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Phonetic = FValue.trim();
			}
			else
				Phonetic = null;
		}
		if (FCode.equalsIgnoreCase("Manufacturer"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Manufacturer = FValue.trim();
			}
			else
				Manufacturer = null;
		}
		if (FCode.equalsIgnoreCase("PackageMessage"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PackageMessage = FValue.trim();
			}
			else
				PackageMessage = null;
		}
		if (FCode.equalsIgnoreCase("Administration"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Administration = FValue.trim();
			}
			else
				Administration = null;
		}
		if (FCode.equalsIgnoreCase("HealthNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HealthNo = FValue.trim();
			}
			else
				HealthNo = null;
		}
		if (FCode.equalsIgnoreCase("CostLevel"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CostLevel = FValue.trim();
			}
			else
				CostLevel = null;
		}
		if (FCode.equalsIgnoreCase("PaymentScope"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PaymentScope = FValue.trim();
			}
			else
				PaymentScope = null;
		}
		if (FCode.equalsIgnoreCase("OutpatientLimit"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OutpatientLimit = FValue.trim();
			}
			else
				OutpatientLimit = null;
		}
		if (FCode.equalsIgnoreCase("HospitalLimit"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HospitalLimit = FValue.trim();
			}
			else
				HospitalLimit = null;
		}
		if (FCode.equalsIgnoreCase("InjuriesMark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InjuriesMark = FValue.trim();
			}
			else
				InjuriesMark = null;
		}
		if (FCode.equalsIgnoreCase("FertilityMark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FertilityMark = FValue.trim();
			}
			else
				FertilityMark = null;
		}
		if (FCode.equalsIgnoreCase("PriceCeiling"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				PriceCeiling = d;
			}
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		if (FCode.equalsIgnoreCase("MngCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MngCom = FValue.trim();
			}
			else
				MngCom = null;
		}
		if (FCode.equalsIgnoreCase("AreaCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AreaCode = FValue.trim();
			}
			else
				AreaCode = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LLCaseDrugInfoSchema other = (LLCaseDrugInfoSchema)otherObject;
		return
			(SerialNo == null ? other.getSerialNo() == null : SerialNo.equals(other.getSerialNo()))
			&& (DrugCode == null ? other.getDrugCode() == null : DrugCode.equals(other.getDrugCode()))
			&& (DrugCategory == null ? other.getDrugCategory() == null : DrugCategory.equals(other.getDrugCategory()))
			&& (DrugGenericName == null ? other.getDrugGenericName() == null : DrugGenericName.equals(other.getDrugGenericName()))
			&& (DrugTradeName == null ? other.getDrugTradeName() == null : DrugTradeName.equals(other.getDrugTradeName()))
			&& (DrugEnglishName == null ? other.getDrugEnglishName() == null : DrugEnglishName.equals(other.getDrugEnglishName()))
			&& (DrugClassification == null ? other.getDrugClassification() == null : DrugClassification.equals(other.getDrugClassification()))
			&& (SecondCategory == null ? other.getSecondCategory() == null : SecondCategory.equals(other.getSecondCategory()))
			&& (Formulations == null ? other.getFormulations() == null : Formulations.equals(other.getFormulations()))
			&& (Specifications == null ? other.getSpecifications() == null : Specifications.equals(other.getSpecifications()))
			&& (Phonetic == null ? other.getPhonetic() == null : Phonetic.equals(other.getPhonetic()))
			&& (Manufacturer == null ? other.getManufacturer() == null : Manufacturer.equals(other.getManufacturer()))
			&& (PackageMessage == null ? other.getPackageMessage() == null : PackageMessage.equals(other.getPackageMessage()))
			&& (Administration == null ? other.getAdministration() == null : Administration.equals(other.getAdministration()))
			&& (HealthNo == null ? other.getHealthNo() == null : HealthNo.equals(other.getHealthNo()))
			&& (CostLevel == null ? other.getCostLevel() == null : CostLevel.equals(other.getCostLevel()))
			&& (PaymentScope == null ? other.getPaymentScope() == null : PaymentScope.equals(other.getPaymentScope()))
			&& (OutpatientLimit == null ? other.getOutpatientLimit() == null : OutpatientLimit.equals(other.getOutpatientLimit()))
			&& (HospitalLimit == null ? other.getHospitalLimit() == null : HospitalLimit.equals(other.getHospitalLimit()))
			&& (InjuriesMark == null ? other.getInjuriesMark() == null : InjuriesMark.equals(other.getInjuriesMark()))
			&& (FertilityMark == null ? other.getFertilityMark() == null : FertilityMark.equals(other.getFertilityMark()))
			&& PriceCeiling == other.getPriceCeiling()
			&& (Remark == null ? other.getRemark() == null : Remark.equals(other.getRemark()))
			&& (MngCom == null ? other.getMngCom() == null : MngCom.equals(other.getMngCom()))
			&& (AreaCode == null ? other.getAreaCode() == null : AreaCode.equals(other.getAreaCode()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("SerialNo") ) {
			return 0;
		}
		if( strFieldName.equals("DrugCode") ) {
			return 1;
		}
		if( strFieldName.equals("DrugCategory") ) {
			return 2;
		}
		if( strFieldName.equals("DrugGenericName") ) {
			return 3;
		}
		if( strFieldName.equals("DrugTradeName") ) {
			return 4;
		}
		if( strFieldName.equals("DrugEnglishName") ) {
			return 5;
		}
		if( strFieldName.equals("DrugClassification") ) {
			return 6;
		}
		if( strFieldName.equals("SecondCategory") ) {
			return 7;
		}
		if( strFieldName.equals("Formulations") ) {
			return 8;
		}
		if( strFieldName.equals("Specifications") ) {
			return 9;
		}
		if( strFieldName.equals("Phonetic") ) {
			return 10;
		}
		if( strFieldName.equals("Manufacturer") ) {
			return 11;
		}
		if( strFieldName.equals("PackageMessage") ) {
			return 12;
		}
		if( strFieldName.equals("Administration") ) {
			return 13;
		}
		if( strFieldName.equals("HealthNo") ) {
			return 14;
		}
		if( strFieldName.equals("CostLevel") ) {
			return 15;
		}
		if( strFieldName.equals("PaymentScope") ) {
			return 16;
		}
		if( strFieldName.equals("OutpatientLimit") ) {
			return 17;
		}
		if( strFieldName.equals("HospitalLimit") ) {
			return 18;
		}
		if( strFieldName.equals("InjuriesMark") ) {
			return 19;
		}
		if( strFieldName.equals("FertilityMark") ) {
			return 20;
		}
		if( strFieldName.equals("PriceCeiling") ) {
			return 21;
		}
		if( strFieldName.equals("Remark") ) {
			return 22;
		}
		if( strFieldName.equals("MngCom") ) {
			return 23;
		}
		if( strFieldName.equals("AreaCode") ) {
			return 24;
		}
		if( strFieldName.equals("Operator") ) {
			return 25;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 26;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 27;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 28;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 29;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "SerialNo";
				break;
			case 1:
				strFieldName = "DrugCode";
				break;
			case 2:
				strFieldName = "DrugCategory";
				break;
			case 3:
				strFieldName = "DrugGenericName";
				break;
			case 4:
				strFieldName = "DrugTradeName";
				break;
			case 5:
				strFieldName = "DrugEnglishName";
				break;
			case 6:
				strFieldName = "DrugClassification";
				break;
			case 7:
				strFieldName = "SecondCategory";
				break;
			case 8:
				strFieldName = "Formulations";
				break;
			case 9:
				strFieldName = "Specifications";
				break;
			case 10:
				strFieldName = "Phonetic";
				break;
			case 11:
				strFieldName = "Manufacturer";
				break;
			case 12:
				strFieldName = "PackageMessage";
				break;
			case 13:
				strFieldName = "Administration";
				break;
			case 14:
				strFieldName = "HealthNo";
				break;
			case 15:
				strFieldName = "CostLevel";
				break;
			case 16:
				strFieldName = "PaymentScope";
				break;
			case 17:
				strFieldName = "OutpatientLimit";
				break;
			case 18:
				strFieldName = "HospitalLimit";
				break;
			case 19:
				strFieldName = "InjuriesMark";
				break;
			case 20:
				strFieldName = "FertilityMark";
				break;
			case 21:
				strFieldName = "PriceCeiling";
				break;
			case 22:
				strFieldName = "Remark";
				break;
			case 23:
				strFieldName = "MngCom";
				break;
			case 24:
				strFieldName = "AreaCode";
				break;
			case 25:
				strFieldName = "Operator";
				break;
			case 26:
				strFieldName = "MakeDate";
				break;
			case 27:
				strFieldName = "MakeTime";
				break;
			case 28:
				strFieldName = "ModifyDate";
				break;
			case 29:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("SerialNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DrugCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DrugCategory") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DrugGenericName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DrugTradeName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DrugEnglishName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DrugClassification") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SecondCategory") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Formulations") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Specifications") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Phonetic") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Manufacturer") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PackageMessage") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Administration") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HealthNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CostLevel") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PaymentScope") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OutpatientLimit") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HospitalLimit") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InjuriesMark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FertilityMark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PriceCeiling") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MngCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AreaCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
