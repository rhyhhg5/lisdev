/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LLInqCertificateDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LLInqCertificateSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 理赔调查相关表
 * @CreateDate：2005-06-02
 */
public class LLInqCertificateSchema implements Schema, Cloneable
{
    // @Field
    /** 对应号码 */
    private String OtherNo;
    /** 其他对应号码类型 */
    private String OtherNoType;
    /** 调查号 */
    private String SurveyNo;
    /** 调查序号 */
    private String InqNo;
    /** 过程序号 */
    private String CouNo;
    /** 单证类型 */
    private String CerType;
    /** 原件标志 */
    private String OriFlag;
    /** 张数 */
    private int CerCount;
    /** 备注 */
    private String Remark;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 14; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LLInqCertificateSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[4];
        pk[0] = "SurveyNo";
        pk[1] = "InqNo";
        pk[2] = "CouNo";
        pk[3] = "CerType";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException
    {
        LLInqCertificateSchema cloned = (LLInqCertificateSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getOtherNo()
    {
        return OtherNo;
    }

    public void setOtherNo(String aOtherNo)
    {
        OtherNo = aOtherNo;
    }

    public String getOtherNoType()
    {
        return OtherNoType;
    }

    public void setOtherNoType(String aOtherNoType)
    {
        OtherNoType = aOtherNoType;
    }

    public String getSurveyNo()
    {
        return SurveyNo;
    }

    public void setSurveyNo(String aSurveyNo)
    {
        SurveyNo = aSurveyNo;
    }

    public String getInqNo()
    {
        return InqNo;
    }

    public void setInqNo(String aInqNo)
    {
        InqNo = aInqNo;
    }

    public String getCouNo()
    {
        return CouNo;
    }

    public void setCouNo(String aCouNo)
    {
        CouNo = aCouNo;
    }

    public String getCerType()
    {
        return CerType;
    }

    public void setCerType(String aCerType)
    {
        CerType = aCerType;
    }

    public String getOriFlag()
    {
        return OriFlag;
    }

    public void setOriFlag(String aOriFlag)
    {
        OriFlag = aOriFlag;
    }

    public int getCerCount()
    {
        return CerCount;
    }

    public void setCerCount(int aCerCount)
    {
        CerCount = aCerCount;
    }

    public void setCerCount(String aCerCount)
    {
        if (aCerCount != null && !aCerCount.equals(""))
        {
            Integer tInteger = new Integer(aCerCount);
            int i = tInteger.intValue();
            CerCount = i;
        }
    }

    public String getRemark()
    {
        return Remark;
    }

    public void setRemark(String aRemark)
    {
        Remark = aRemark;
    }

    public String getOperator()
    {
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LLInqCertificateSchema 对象给 Schema 赋值
     * @param: aLLInqCertificateSchema LLInqCertificateSchema
     **/
    public void setSchema(LLInqCertificateSchema aLLInqCertificateSchema)
    {
        this.OtherNo = aLLInqCertificateSchema.getOtherNo();
        this.OtherNoType = aLLInqCertificateSchema.getOtherNoType();
        this.SurveyNo = aLLInqCertificateSchema.getSurveyNo();
        this.InqNo = aLLInqCertificateSchema.getInqNo();
        this.CouNo = aLLInqCertificateSchema.getCouNo();
        this.CerType = aLLInqCertificateSchema.getCerType();
        this.OriFlag = aLLInqCertificateSchema.getOriFlag();
        this.CerCount = aLLInqCertificateSchema.getCerCount();
        this.Remark = aLLInqCertificateSchema.getRemark();
        this.Operator = aLLInqCertificateSchema.getOperator();
        this.MakeDate = fDate.getDate(aLLInqCertificateSchema.getMakeDate());
        this.MakeTime = aLLInqCertificateSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLLInqCertificateSchema.getModifyDate());
        this.ModifyTime = aLLInqCertificateSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("OtherNo") == null)
            {
                this.OtherNo = null;
            }
            else
            {
                this.OtherNo = rs.getString("OtherNo").trim();
            }

            if (rs.getString("OtherNoType") == null)
            {
                this.OtherNoType = null;
            }
            else
            {
                this.OtherNoType = rs.getString("OtherNoType").trim();
            }

            if (rs.getString("SurveyNo") == null)
            {
                this.SurveyNo = null;
            }
            else
            {
                this.SurveyNo = rs.getString("SurveyNo").trim();
            }

            if (rs.getString("InqNo") == null)
            {
                this.InqNo = null;
            }
            else
            {
                this.InqNo = rs.getString("InqNo").trim();
            }

            if (rs.getString("CouNo") == null)
            {
                this.CouNo = null;
            }
            else
            {
                this.CouNo = rs.getString("CouNo").trim();
            }

            if (rs.getString("CerType") == null)
            {
                this.CerType = null;
            }
            else
            {
                this.CerType = rs.getString("CerType").trim();
            }

            if (rs.getString("OriFlag") == null)
            {
                this.OriFlag = null;
            }
            else
            {
                this.OriFlag = rs.getString("OriFlag").trim();
            }

            this.CerCount = rs.getInt("CerCount");
            if (rs.getString("Remark") == null)
            {
                this.Remark = null;
            }
            else
            {
                this.Remark = rs.getString("Remark").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            System.out.println("数据库中的LLInqCertificate表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLInqCertificateSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LLInqCertificateSchema getSchema()
    {
        LLInqCertificateSchema aLLInqCertificateSchema = new
                LLInqCertificateSchema();
        aLLInqCertificateSchema.setSchema(this);
        return aLLInqCertificateSchema;
    }

    public LLInqCertificateDB getDB()
    {
        LLInqCertificateDB aDBOper = new LLInqCertificateDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLInqCertificate描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(OtherNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OtherNoType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SurveyNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InqNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CouNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CerType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OriFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(CerCount));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Remark));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLInqCertificate>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            OtherNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                     SysConst.PACKAGESPILTER);
            OtherNoType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                         SysConst.PACKAGESPILTER);
            SurveyNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            InqNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                   SysConst.PACKAGESPILTER);
            CouNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                   SysConst.PACKAGESPILTER);
            CerType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                     SysConst.PACKAGESPILTER);
            OriFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                     SysConst.PACKAGESPILTER);
            CerCount = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 8, SysConst.PACKAGESPILTER))).intValue();
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                    SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 11, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 13, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLInqCertificateSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("OtherNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNo));
        }
        if (FCode.equals("OtherNoType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNoType));
        }
        if (FCode.equals("SurveyNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SurveyNo));
        }
        if (FCode.equals("InqNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InqNo));
        }
        if (FCode.equals("CouNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CouNo));
        }
        if (FCode.equals("CerType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CerType));
        }
        if (FCode.equals("OriFlag"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OriFlag));
        }
        if (FCode.equals("CerCount"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CerCount));
        }
        if (FCode.equals("Remark"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(OtherNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(OtherNoType);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(SurveyNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(InqNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(CouNo);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(CerType);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(OriFlag);
                break;
            case 7:
                strFieldValue = String.valueOf(CerCount);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("OtherNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OtherNo = FValue.trim();
            }
            else
            {
                OtherNo = null;
            }
        }
        if (FCode.equals("OtherNoType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OtherNoType = FValue.trim();
            }
            else
            {
                OtherNoType = null;
            }
        }
        if (FCode.equals("SurveyNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SurveyNo = FValue.trim();
            }
            else
            {
                SurveyNo = null;
            }
        }
        if (FCode.equals("InqNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InqNo = FValue.trim();
            }
            else
            {
                InqNo = null;
            }
        }
        if (FCode.equals("CouNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CouNo = FValue.trim();
            }
            else
            {
                CouNo = null;
            }
        }
        if (FCode.equals("CerType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CerType = FValue.trim();
            }
            else
            {
                CerType = null;
            }
        }
        if (FCode.equals("OriFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OriFlag = FValue.trim();
            }
            else
            {
                OriFlag = null;
            }
        }
        if (FCode.equals("CerCount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                CerCount = i;
            }
        }
        if (FCode.equals("Remark"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
            {
                Remark = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LLInqCertificateSchema other = (LLInqCertificateSchema) otherObject;
        return
                OtherNo.equals(other.getOtherNo())
                && OtherNoType.equals(other.getOtherNoType())
                && SurveyNo.equals(other.getSurveyNo())
                && InqNo.equals(other.getInqNo())
                && CouNo.equals(other.getCouNo())
                && CerType.equals(other.getCerType())
                && OriFlag.equals(other.getOriFlag())
                && CerCount == other.getCerCount()
                && Remark.equals(other.getRemark())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("OtherNo"))
        {
            return 0;
        }
        if (strFieldName.equals("OtherNoType"))
        {
            return 1;
        }
        if (strFieldName.equals("SurveyNo"))
        {
            return 2;
        }
        if (strFieldName.equals("InqNo"))
        {
            return 3;
        }
        if (strFieldName.equals("CouNo"))
        {
            return 4;
        }
        if (strFieldName.equals("CerType"))
        {
            return 5;
        }
        if (strFieldName.equals("OriFlag"))
        {
            return 6;
        }
        if (strFieldName.equals("CerCount"))
        {
            return 7;
        }
        if (strFieldName.equals("Remark"))
        {
            return 8;
        }
        if (strFieldName.equals("Operator"))
        {
            return 9;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 10;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 11;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 12;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 13;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "OtherNo";
                break;
            case 1:
                strFieldName = "OtherNoType";
                break;
            case 2:
                strFieldName = "SurveyNo";
                break;
            case 3:
                strFieldName = "InqNo";
                break;
            case 4:
                strFieldName = "CouNo";
                break;
            case 5:
                strFieldName = "CerType";
                break;
            case 6:
                strFieldName = "OriFlag";
                break;
            case 7:
                strFieldName = "CerCount";
                break;
            case 8:
                strFieldName = "Remark";
                break;
            case 9:
                strFieldName = "Operator";
                break;
            case 10:
                strFieldName = "MakeDate";
                break;
            case 11:
                strFieldName = "MakeTime";
                break;
            case 12:
                strFieldName = "ModifyDate";
                break;
            case 13:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("OtherNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OtherNoType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SurveyNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InqNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CouNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CerType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OriFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CerCount"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("Remark"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_INT;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
