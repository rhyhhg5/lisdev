/*
 * <p>ClassName: LMDutyCalFactorSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 健康险责任计算
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LMDutyCalFactorDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LMDutyCalFactorSchema implements Schema
{
    // @Field
    /** 责任代码 */
    private String DutyCode;
    /** 责任名称 */
    private String DutyName;
    /** 计算要素代码 */
    private String CalFactorCode;
    /** 计算要素名称 */
    private String CalFactorName;
    /** 计算公式代码 */
    private String CalCode;
    /** 计算公式说明 */
    private String Remark;

    public static final int FIELDNUM = 6; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LMDutyCalFactorSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "DutyCode";
        pk[1] = "CalFactorCode";
        pk[2] = "CalCode";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getDutyCode()
    {
        if (DutyCode != null && !DutyCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            DutyCode = StrTool.unicodeToGBK(DutyCode);
        }
        return DutyCode;
    }

    public void setDutyCode(String aDutyCode)
    {
        DutyCode = aDutyCode;
    }

    public String getDutyName()
    {
        if (DutyName != null && !DutyName.equals("") && SysConst.CHANGECHARSET == true)
        {
            DutyName = StrTool.unicodeToGBK(DutyName);
        }
        return DutyName;
    }

    public void setDutyName(String aDutyName)
    {
        DutyName = aDutyName;
    }

    public String getCalFactorCode()
    {
        if (CalFactorCode != null && !CalFactorCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CalFactorCode = StrTool.unicodeToGBK(CalFactorCode);
        }
        return CalFactorCode;
    }

    public void setCalFactorCode(String aCalFactorCode)
    {
        CalFactorCode = aCalFactorCode;
    }

    public String getCalFactorName()
    {
        if (CalFactorName != null && !CalFactorName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CalFactorName = StrTool.unicodeToGBK(CalFactorName);
        }
        return CalFactorName;
    }

    public void setCalFactorName(String aCalFactorName)
    {
        CalFactorName = aCalFactorName;
    }

    public String getCalCode()
    {
        if (CalCode != null && !CalCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            CalCode = StrTool.unicodeToGBK(CalCode);
        }
        return CalCode;
    }

    public void setCalCode(String aCalCode)
    {
        CalCode = aCalCode;
    }

    public String getRemark()
    {
        if (Remark != null && !Remark.equals("") && SysConst.CHANGECHARSET == true)
        {
            Remark = StrTool.unicodeToGBK(Remark);
        }
        return Remark;
    }

    public void setRemark(String aRemark)
    {
        Remark = aRemark;
    }

    /**
     * 使用另外一个 LMDutyCalFactorSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LMDutyCalFactorSchema aLMDutyCalFactorSchema)
    {
        this.DutyCode = aLMDutyCalFactorSchema.getDutyCode();
        this.DutyName = aLMDutyCalFactorSchema.getDutyName();
        this.CalFactorCode = aLMDutyCalFactorSchema.getCalFactorCode();
        this.CalFactorName = aLMDutyCalFactorSchema.getCalFactorName();
        this.CalCode = aLMDutyCalFactorSchema.getCalCode();
        this.Remark = aLMDutyCalFactorSchema.getRemark();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("DutyCode") == null)
            {
                this.DutyCode = null;
            }
            else
            {
                this.DutyCode = rs.getString("DutyCode").trim();
            }

            if (rs.getString("DutyName") == null)
            {
                this.DutyName = null;
            }
            else
            {
                this.DutyName = rs.getString("DutyName").trim();
            }

            if (rs.getString("CalFactorCode") == null)
            {
                this.CalFactorCode = null;
            }
            else
            {
                this.CalFactorCode = rs.getString("CalFactorCode").trim();
            }

            if (rs.getString("CalFactorName") == null)
            {
                this.CalFactorName = null;
            }
            else
            {
                this.CalFactorName = rs.getString("CalFactorName").trim();
            }

            if (rs.getString("CalCode") == null)
            {
                this.CalCode = null;
            }
            else
            {
                this.CalCode = rs.getString("CalCode").trim();
            }

            if (rs.getString("Remark") == null)
            {
                this.Remark = null;
            }
            else
            {
                this.Remark = rs.getString("Remark").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMDutyCalFactorSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LMDutyCalFactorSchema getSchema()
    {
        LMDutyCalFactorSchema aLMDutyCalFactorSchema = new
                LMDutyCalFactorSchema();
        aLMDutyCalFactorSchema.setSchema(this);
        return aLMDutyCalFactorSchema;
    }

    public LMDutyCalFactorDB getDB()
    {
        LMDutyCalFactorDB aDBOper = new LMDutyCalFactorDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMDutyCalFactor描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(DutyCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(DutyName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CalFactorCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CalFactorName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CalCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Remark));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMDutyCalFactor>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            DutyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            DutyName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            CalFactorCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                           SysConst.PACKAGESPILTER);
            CalFactorName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                           SysConst.PACKAGESPILTER);
            CalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                     SysConst.PACKAGESPILTER);
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                    SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMDutyCalFactorSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("DutyCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DutyCode));
        }
        if (FCode.equals("DutyName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DutyName));
        }
        if (FCode.equals("CalFactorCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CalFactorCode));
        }
        if (FCode.equals("CalFactorName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CalFactorName));
        }
        if (FCode.equals("CalCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CalCode));
        }
        if (FCode.equals("Remark"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Remark));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(DutyCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(DutyName);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(CalFactorCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(CalFactorName);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(CalCode);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("DutyCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DutyCode = FValue.trim();
            }
            else
            {
                DutyCode = null;
            }
        }
        if (FCode.equals("DutyName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DutyName = FValue.trim();
            }
            else
            {
                DutyName = null;
            }
        }
        if (FCode.equals("CalFactorCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CalFactorCode = FValue.trim();
            }
            else
            {
                CalFactorCode = null;
            }
        }
        if (FCode.equals("CalFactorName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CalFactorName = FValue.trim();
            }
            else
            {
                CalFactorName = null;
            }
        }
        if (FCode.equals("CalCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CalCode = FValue.trim();
            }
            else
            {
                CalCode = null;
            }
        }
        if (FCode.equals("Remark"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
            {
                Remark = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LMDutyCalFactorSchema other = (LMDutyCalFactorSchema) otherObject;
        return
                DutyCode.equals(other.getDutyCode())
                && DutyName.equals(other.getDutyName())
                && CalFactorCode.equals(other.getCalFactorCode())
                && CalFactorName.equals(other.getCalFactorName())
                && CalCode.equals(other.getCalCode())
                && Remark.equals(other.getRemark());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("DutyCode"))
        {
            return 0;
        }
        if (strFieldName.equals("DutyName"))
        {
            return 1;
        }
        if (strFieldName.equals("CalFactorCode"))
        {
            return 2;
        }
        if (strFieldName.equals("CalFactorName"))
        {
            return 3;
        }
        if (strFieldName.equals("CalCode"))
        {
            return 4;
        }
        if (strFieldName.equals("Remark"))
        {
            return 5;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "DutyCode";
                break;
            case 1:
                strFieldName = "DutyName";
                break;
            case 2:
                strFieldName = "CalFactorCode";
                break;
            case 3:
                strFieldName = "CalFactorName";
                break;
            case 4:
                strFieldName = "CalCode";
                break;
            case 5:
                strFieldName = "Remark";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("DutyCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DutyName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CalFactorCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CalFactorName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CalCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Remark"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
