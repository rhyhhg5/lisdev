/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LAAssessMarkDB;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LAAssessMarkSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-12-14
 */
public class LAAssessMarkSchema implements Schema, Cloneable {
    // @Field
    /** 代理人编码 */
    private String AgentCode;
    /** 展业类型 */
    private String BranchType;
    /** 纪录顺序号 */
    private int Idx;
    /** 考评类型 */
    private String AssessMarkType;
    /** 考评分数 */
    private double AssessMark;
    /** 执行日期 */
    private Date DoneDate;
    /** 批注 */
    private String Noti;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 考评年月 */
    private String AssessYM;
    /** 展业机构外部编码 */
    private String BranchAttr;
    /** 渠道 */
    private String BranchType2;
    /** 活动工具管理能力(活动工具使用) */
    private double ToolMark;
    /** 费用使用公平、公开(培训管理) */
    private double ChargeMark;
    /** 会议经营能力(会议纪律) */
    private double MeetMark;
    /** 遵守规章制度(考勤) */
    private double RuleMark;
    /** 备用1 */
    private double StandbyMark1;
    /** 备用2 */
    private double StandbyMark2;
    /** 备用3 */
    private double StandbyMark3;

    public static final int FIELDNUM = 22; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LAAssessMarkSchema() {
        mErrors = new CErrors();

        String[] pk = new String[4];
        pk[0] = "AgentCode";
        pk[1] = "BranchType";
        pk[2] = "AssessYM";
        pk[3] = "BranchType2";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LAAssessMarkSchema cloned = (LAAssessMarkSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getAgentCode() {
        return AgentCode;
    }

    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }

    public String getBranchType() {
        return BranchType;
    }

    public void setBranchType(String aBranchType) {
        BranchType = aBranchType;
    }

    public int getIdx() {
        return Idx;
    }

    public void setIdx(int aIdx) {
        Idx = aIdx;
    }

    public void setIdx(String aIdx) {
        if (aIdx != null && !aIdx.equals("")) {
            Integer tInteger = new Integer(aIdx);
            int i = tInteger.intValue();
            Idx = i;
        }
    }

    public String getAssessMarkType() {
        return AssessMarkType;
    }

    public void setAssessMarkType(String aAssessMarkType) {
        AssessMarkType = aAssessMarkType;
    }

    public double getAssessMark() {
        return AssessMark;
    }

    public void setAssessMark(double aAssessMark) {
        AssessMark = Arith.round(aAssessMark, 2);
    }

    public void setAssessMark(String aAssessMark) {
        if (aAssessMark != null && !aAssessMark.equals("")) {
            Double tDouble = new Double(aAssessMark);
            double d = tDouble.doubleValue();
            AssessMark = Arith.round(d, 2);
        }
    }

    public String getDoneDate() {
        if (DoneDate != null) {
            return fDate.getString(DoneDate);
        } else {
            return null;
        }
    }

    public void setDoneDate(Date aDoneDate) {
        DoneDate = aDoneDate;
    }

    public void setDoneDate(String aDoneDate) {
        if (aDoneDate != null && !aDoneDate.equals("")) {
            DoneDate = fDate.getDate(aDoneDate);
        } else {
            DoneDate = null;
        }
    }

    public String getNoti() {
        return Noti;
    }

    public void setNoti(String aNoti) {
        Noti = aNoti;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    public String getAssessYM() {
        return AssessYM;
    }

    public void setAssessYM(String aAssessYM) {
        AssessYM = aAssessYM;
    }

    public String getBranchAttr() {
        return BranchAttr;
    }

    public void setBranchAttr(String aBranchAttr) {
        BranchAttr = aBranchAttr;
    }

    public String getBranchType2() {
        return BranchType2;
    }

    public void setBranchType2(String aBranchType2) {
        BranchType2 = aBranchType2;
    }

    public double getToolMark() {
        return ToolMark;
    }

    public void setToolMark(double aToolMark) {
        ToolMark = Arith.round(aToolMark, 2);
    }

    public void setToolMark(String aToolMark) {
        if (aToolMark != null && !aToolMark.equals("")) {
            Double tDouble = new Double(aToolMark);
            double d = tDouble.doubleValue();
            ToolMark = Arith.round(d, 2);
        }
    }

    public double getChargeMark() {
        return ChargeMark;
    }

    public void setChargeMark(double aChargeMark) {
        ChargeMark = Arith.round(aChargeMark, 2);
    }

    public void setChargeMark(String aChargeMark) {
        if (aChargeMark != null && !aChargeMark.equals("")) {
            Double tDouble = new Double(aChargeMark);
            double d = tDouble.doubleValue();
            ChargeMark = Arith.round(d, 2);
        }
    }

    public double getMeetMark() {
        return MeetMark;
    }

    public void setMeetMark(double aMeetMark) {
        MeetMark = Arith.round(aMeetMark, 2);
    }

    public void setMeetMark(String aMeetMark) {
        if (aMeetMark != null && !aMeetMark.equals("")) {
            Double tDouble = new Double(aMeetMark);
            double d = tDouble.doubleValue();
            MeetMark = Arith.round(d, 2);
        }
    }

    public double getRuleMark() {
        return RuleMark;
    }

    public void setRuleMark(double aRuleMark) {
        RuleMark = Arith.round(aRuleMark, 2);
    }

    public void setRuleMark(String aRuleMark) {
        if (aRuleMark != null && !aRuleMark.equals("")) {
            Double tDouble = new Double(aRuleMark);
            double d = tDouble.doubleValue();
            RuleMark = Arith.round(d, 2);
        }
    }

    public double getStandbyMark1() {
        return StandbyMark1;
    }

    public void setStandbyMark1(double aStandbyMark1) {
        StandbyMark1 = Arith.round(aStandbyMark1, 2);
    }

    public void setStandbyMark1(String aStandbyMark1) {
        if (aStandbyMark1 != null && !aStandbyMark1.equals("")) {
            Double tDouble = new Double(aStandbyMark1);
            double d = tDouble.doubleValue();
            StandbyMark1 = Arith.round(d, 2);
        }
    }

    public double getStandbyMark2() {
        return StandbyMark2;
    }

    public void setStandbyMark2(double aStandbyMark2) {
        StandbyMark2 = Arith.round(aStandbyMark2, 2);
    }

    public void setStandbyMark2(String aStandbyMark2) {
        if (aStandbyMark2 != null && !aStandbyMark2.equals("")) {
            Double tDouble = new Double(aStandbyMark2);
            double d = tDouble.doubleValue();
            StandbyMark2 = Arith.round(d, 2);
        }
    }

    public double getStandbyMark3() {
        return StandbyMark3;
    }

    public void setStandbyMark3(double aStandbyMark3) {
        StandbyMark3 = Arith.round(aStandbyMark3, 2);
    }

    public void setStandbyMark3(String aStandbyMark3) {
        if (aStandbyMark3 != null && !aStandbyMark3.equals("")) {
            Double tDouble = new Double(aStandbyMark3);
            double d = tDouble.doubleValue();
            StandbyMark3 = Arith.round(d, 2);
        }
    }


    /**
     * 使用另外一个 LAAssessMarkSchema 对象给 Schema 赋值
     * @param: aLAAssessMarkSchema LAAssessMarkSchema
     **/
    public void setSchema(LAAssessMarkSchema aLAAssessMarkSchema) {
        this.AgentCode = aLAAssessMarkSchema.getAgentCode();
        this.BranchType = aLAAssessMarkSchema.getBranchType();
        this.Idx = aLAAssessMarkSchema.getIdx();
        this.AssessMarkType = aLAAssessMarkSchema.getAssessMarkType();
        this.AssessMark = aLAAssessMarkSchema.getAssessMark();
        this.DoneDate = fDate.getDate(aLAAssessMarkSchema.getDoneDate());
        this.Noti = aLAAssessMarkSchema.getNoti();
        this.Operator = aLAAssessMarkSchema.getOperator();
        this.MakeDate = fDate.getDate(aLAAssessMarkSchema.getMakeDate());
        this.MakeTime = aLAAssessMarkSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLAAssessMarkSchema.getModifyDate());
        this.ModifyTime = aLAAssessMarkSchema.getModifyTime();
        this.AssessYM = aLAAssessMarkSchema.getAssessYM();
        this.BranchAttr = aLAAssessMarkSchema.getBranchAttr();
        this.BranchType2 = aLAAssessMarkSchema.getBranchType2();
        this.ToolMark = aLAAssessMarkSchema.getToolMark();
        this.ChargeMark = aLAAssessMarkSchema.getChargeMark();
        this.MeetMark = aLAAssessMarkSchema.getMeetMark();
        this.RuleMark = aLAAssessMarkSchema.getRuleMark();
        this.StandbyMark1 = aLAAssessMarkSchema.getStandbyMark1();
        this.StandbyMark2 = aLAAssessMarkSchema.getStandbyMark2();
        this.StandbyMark3 = aLAAssessMarkSchema.getStandbyMark3();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("AgentCode") == null) {
                this.AgentCode = null;
            } else {
                this.AgentCode = rs.getString("AgentCode").trim();
            }

            if (rs.getString("BranchType") == null) {
                this.BranchType = null;
            } else {
                this.BranchType = rs.getString("BranchType").trim();
            }

            this.Idx = rs.getInt("Idx");
            if (rs.getString("AssessMarkType") == null) {
                this.AssessMarkType = null;
            } else {
                this.AssessMarkType = rs.getString("AssessMarkType").trim();
            }

            this.AssessMark = rs.getDouble("AssessMark");
            this.DoneDate = rs.getDate("DoneDate");
            if (rs.getString("Noti") == null) {
                this.Noti = null;
            } else {
                this.Noti = rs.getString("Noti").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("AssessYM") == null) {
                this.AssessYM = null;
            } else {
                this.AssessYM = rs.getString("AssessYM").trim();
            }

            if (rs.getString("BranchAttr") == null) {
                this.BranchAttr = null;
            } else {
                this.BranchAttr = rs.getString("BranchAttr").trim();
            }

            if (rs.getString("BranchType2") == null) {
                this.BranchType2 = null;
            } else {
                this.BranchType2 = rs.getString("BranchType2").trim();
            }

            this.ToolMark = rs.getDouble("ToolMark");
            this.ChargeMark = rs.getDouble("ChargeMark");
            this.MeetMark = rs.getDouble("MeetMark");
            this.RuleMark = rs.getDouble("RuleMark");
            this.StandbyMark1 = rs.getDouble("StandbyMark1");
            this.StandbyMark2 = rs.getDouble("StandbyMark2");
            this.StandbyMark3 = rs.getDouble("StandbyMark3");
        } catch (SQLException sqle) {
            System.out.println("数据库中的LAAssessMark表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAssessMarkSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LAAssessMarkSchema getSchema() {
        LAAssessMarkSchema aLAAssessMarkSchema = new LAAssessMarkSchema();
        aLAAssessMarkSchema.setSchema(this);
        return aLAAssessMarkSchema;
    }

    public LAAssessMarkDB getDB() {
        LAAssessMarkDB aDBOper = new LAAssessMarkDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAAssessMark描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(AgentCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BranchType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Idx));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AssessMarkType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(AssessMark));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(DoneDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Noti));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AssessYM));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BranchAttr));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BranchType2));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(ToolMark));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(ChargeMark));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(MeetMark));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(RuleMark));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(StandbyMark1));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(StandbyMark2));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(StandbyMark3));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAAssessMark>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
            Idx = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    3, SysConst.PACKAGESPILTER))).intValue();
            AssessMarkType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                            SysConst.PACKAGESPILTER);
            AssessMark = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 5, SysConst.PACKAGESPILTER))).doubleValue();
            DoneDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 6, SysConst.PACKAGESPILTER));
            Noti = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                  SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 9, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 11, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                        SysConst.PACKAGESPILTER);
            AssessYM = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                      SysConst.PACKAGESPILTER);
            BranchAttr = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                        SysConst.PACKAGESPILTER);
            BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                         SysConst.PACKAGESPILTER);
            ToolMark = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 16, SysConst.PACKAGESPILTER))).doubleValue();
            ChargeMark = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 17, SysConst.PACKAGESPILTER))).doubleValue();
            MeetMark = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 18, SysConst.PACKAGESPILTER))).doubleValue();
            RuleMark = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 19, SysConst.PACKAGESPILTER))).doubleValue();
            StandbyMark1 = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 20, SysConst.PACKAGESPILTER))).doubleValue();
            StandbyMark2 = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 21, SysConst.PACKAGESPILTER))).doubleValue();
            StandbyMark3 = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 22, SysConst.PACKAGESPILTER))).doubleValue();
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAssessMarkSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equals("BranchType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
        }
        if (FCode.equals("Idx")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Idx));
        }
        if (FCode.equals("AssessMarkType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AssessMarkType));
        }
        if (FCode.equals("AssessMark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AssessMark));
        }
        if (FCode.equals("DoneDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getDoneDate()));
        }
        if (FCode.equals("Noti")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Noti));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("AssessYM")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AssessYM));
        }
        if (FCode.equals("BranchAttr")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchAttr));
        }
        if (FCode.equals("BranchType2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
        }
        if (FCode.equals("ToolMark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ToolMark));
        }
        if (FCode.equals("ChargeMark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChargeMark));
        }
        if (FCode.equals("MeetMark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MeetMark));
        }
        if (FCode.equals("RuleMark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RuleMark));
        }
        if (FCode.equals("StandbyMark1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyMark1));
        }
        if (FCode.equals("StandbyMark2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyMark2));
        }
        if (FCode.equals("StandbyMark3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyMark3));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(AgentCode);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(BranchType);
            break;
        case 2:
            strFieldValue = String.valueOf(Idx);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(AssessMarkType);
            break;
        case 4:
            strFieldValue = String.valueOf(AssessMark);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getDoneDate()));
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(Noti);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(AssessYM);
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(BranchAttr);
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(BranchType2);
            break;
        case 15:
            strFieldValue = String.valueOf(ToolMark);
            break;
        case 16:
            strFieldValue = String.valueOf(ChargeMark);
            break;
        case 17:
            strFieldValue = String.valueOf(MeetMark);
            break;
        case 18:
            strFieldValue = String.valueOf(RuleMark);
            break;
        case 19:
            strFieldValue = String.valueOf(StandbyMark1);
            break;
        case 20:
            strFieldValue = String.valueOf(StandbyMark2);
            break;
        case 21:
            strFieldValue = String.valueOf(StandbyMark3);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("AgentCode")) {
            if (FValue != null && !FValue.equals("")) {
                AgentCode = FValue.trim();
            } else {
                AgentCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("BranchType")) {
            if (FValue != null && !FValue.equals("")) {
                BranchType = FValue.trim();
            } else {
                BranchType = null;
            }
        }
        if (FCode.equalsIgnoreCase("Idx")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                Idx = i;
            }
        }
        if (FCode.equalsIgnoreCase("AssessMarkType")) {
            if (FValue != null && !FValue.equals("")) {
                AssessMarkType = FValue.trim();
            } else {
                AssessMarkType = null;
            }
        }
        if (FCode.equalsIgnoreCase("AssessMark")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                AssessMark = d;
            }
        }
        if (FCode.equalsIgnoreCase("DoneDate")) {
            if (FValue != null && !FValue.equals("")) {
                DoneDate = fDate.getDate(FValue);
            } else {
                DoneDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("Noti")) {
            if (FValue != null && !FValue.equals("")) {
                Noti = FValue.trim();
            } else {
                Noti = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("AssessYM")) {
            if (FValue != null && !FValue.equals("")) {
                AssessYM = FValue.trim();
            } else {
                AssessYM = null;
            }
        }
        if (FCode.equalsIgnoreCase("BranchAttr")) {
            if (FValue != null && !FValue.equals("")) {
                BranchAttr = FValue.trim();
            } else {
                BranchAttr = null;
            }
        }
        if (FCode.equalsIgnoreCase("BranchType2")) {
            if (FValue != null && !FValue.equals("")) {
                BranchType2 = FValue.trim();
            } else {
                BranchType2 = null;
            }
        }
        if (FCode.equalsIgnoreCase("ToolMark")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                ToolMark = d;
            }
        }
        if (FCode.equalsIgnoreCase("ChargeMark")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                ChargeMark = d;
            }
        }
        if (FCode.equalsIgnoreCase("MeetMark")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                MeetMark = d;
            }
        }
        if (FCode.equalsIgnoreCase("RuleMark")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                RuleMark = d;
            }
        }
        if (FCode.equalsIgnoreCase("StandbyMark1")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                StandbyMark1 = d;
            }
        }
        if (FCode.equalsIgnoreCase("StandbyMark2")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                StandbyMark2 = d;
            }
        }
        if (FCode.equalsIgnoreCase("StandbyMark3")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                StandbyMark3 = d;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LAAssessMarkSchema other = (LAAssessMarkSchema) otherObject;
        return
                AgentCode.equals(other.getAgentCode())
                && BranchType.equals(other.getBranchType())
                && Idx == other.getIdx()
                && AssessMarkType.equals(other.getAssessMarkType())
                && AssessMark == other.getAssessMark()
                && fDate.getString(DoneDate).equals(other.getDoneDate())
                && Noti.equals(other.getNoti())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && AssessYM.equals(other.getAssessYM())
                && BranchAttr.equals(other.getBranchAttr())
                && BranchType2.equals(other.getBranchType2())
                && ToolMark == other.getToolMark()
                && ChargeMark == other.getChargeMark()
                && MeetMark == other.getMeetMark()
                && RuleMark == other.getRuleMark()
                && StandbyMark1 == other.getStandbyMark1()
                && StandbyMark2 == other.getStandbyMark2()
                && StandbyMark3 == other.getStandbyMark3();
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("AgentCode")) {
            return 0;
        }
        if (strFieldName.equals("BranchType")) {
            return 1;
        }
        if (strFieldName.equals("Idx")) {
            return 2;
        }
        if (strFieldName.equals("AssessMarkType")) {
            return 3;
        }
        if (strFieldName.equals("AssessMark")) {
            return 4;
        }
        if (strFieldName.equals("DoneDate")) {
            return 5;
        }
        if (strFieldName.equals("Noti")) {
            return 6;
        }
        if (strFieldName.equals("Operator")) {
            return 7;
        }
        if (strFieldName.equals("MakeDate")) {
            return 8;
        }
        if (strFieldName.equals("MakeTime")) {
            return 9;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 10;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 11;
        }
        if (strFieldName.equals("AssessYM")) {
            return 12;
        }
        if (strFieldName.equals("BranchAttr")) {
            return 13;
        }
        if (strFieldName.equals("BranchType2")) {
            return 14;
        }
        if (strFieldName.equals("ToolMark")) {
            return 15;
        }
        if (strFieldName.equals("ChargeMark")) {
            return 16;
        }
        if (strFieldName.equals("MeetMark")) {
            return 17;
        }
        if (strFieldName.equals("RuleMark")) {
            return 18;
        }
        if (strFieldName.equals("StandbyMark1")) {
            return 19;
        }
        if (strFieldName.equals("StandbyMark2")) {
            return 20;
        }
        if (strFieldName.equals("StandbyMark3")) {
            return 21;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "AgentCode";
            break;
        case 1:
            strFieldName = "BranchType";
            break;
        case 2:
            strFieldName = "Idx";
            break;
        case 3:
            strFieldName = "AssessMarkType";
            break;
        case 4:
            strFieldName = "AssessMark";
            break;
        case 5:
            strFieldName = "DoneDate";
            break;
        case 6:
            strFieldName = "Noti";
            break;
        case 7:
            strFieldName = "Operator";
            break;
        case 8:
            strFieldName = "MakeDate";
            break;
        case 9:
            strFieldName = "MakeTime";
            break;
        case 10:
            strFieldName = "ModifyDate";
            break;
        case 11:
            strFieldName = "ModifyTime";
            break;
        case 12:
            strFieldName = "AssessYM";
            break;
        case 13:
            strFieldName = "BranchAttr";
            break;
        case 14:
            strFieldName = "BranchType2";
            break;
        case 15:
            strFieldName = "ToolMark";
            break;
        case 16:
            strFieldName = "ChargeMark";
            break;
        case 17:
            strFieldName = "MeetMark";
            break;
        case 18:
            strFieldName = "RuleMark";
            break;
        case 19:
            strFieldName = "StandbyMark1";
            break;
        case 20:
            strFieldName = "StandbyMark2";
            break;
        case 21:
            strFieldName = "StandbyMark3";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("AgentCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Idx")) {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("AssessMarkType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AssessMark")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("DoneDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("Noti")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AssessYM")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchAttr")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchType2")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ToolMark")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("ChargeMark")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("MeetMark")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("RuleMark")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("StandbyMark1")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("StandbyMark2")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("StandbyMark3")) {
            return Schema.TYPE_DOUBLE;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_INT;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 5:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 12:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 13:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 14:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 15:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 16:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 17:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 18:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 19:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 20:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 21:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
