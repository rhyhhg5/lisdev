/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.tab_midplat_serviceDB;

/*
 * <p>ClassName: tab_midplat_serviceSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 产品化中间业务平台
 * @CreateDate：2007-04-17
 */
public class tab_midplat_serviceSchema implements Schema, Cloneable
{
	// @Field
	/** 服务id */
	private String service_id;
	/** 子服务id */
	private String sub_service_id;
	/** 调用子服务的顺序 */
	private String sub_service_order;
	/** 服务类型 */
	private String request_type;
	/** 服务程序实现类型 */
	private String impl_type;
	/** 服务程序 */
	private String service_provider;
	/** 服务描述 */
	private String service_info;
	/** 备用字段 */
	private String bak1;
	/** 备用字段2 */
	private String bak2;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后修改日期 */
	private Date ModifyDate;
	/** 最后修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 13;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public tab_midplat_serviceSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[3];
		pk[0] = "service_id";
		pk[1] = "sub_service_id";
		pk[2] = "sub_service_order";

		PK = pk;
	}

            /**
             * Schema克隆
             * @return Object
             * @throws CloneNotSupportedException
             */
            public Object clone()
                    throws CloneNotSupportedException
            {
                tab_midplat_serviceSchema cloned = (tab_midplat_serviceSchema)super.clone();
                cloned.fDate = (FDate) fDate.clone();
                cloned.mErrors = (CErrors) mErrors.clone();
                return cloned;
            }

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getservice_id()
	{
		return service_id;
	}
	public void setservice_id(String aservice_id)
	{
		service_id = aservice_id;
	}
	public String getsub_service_id()
	{
		return sub_service_id;
	}
	public void setsub_service_id(String asub_service_id)
	{
		sub_service_id = asub_service_id;
	}
	public String getsub_service_order()
	{
		return sub_service_order;
	}
	public void setsub_service_order(String asub_service_order)
	{
		sub_service_order = asub_service_order;
	}
	public String getrequest_type()
	{
		return request_type;
	}
	public void setrequest_type(String arequest_type)
	{
		request_type = arequest_type;
	}
	public String getimpl_type()
	{
		return impl_type;
	}
	public void setimpl_type(String aimpl_type)
	{
		impl_type = aimpl_type;
	}
	public String getservice_provider()
	{
		return service_provider;
	}
	public void setservice_provider(String aservice_provider)
	{
		service_provider = aservice_provider;
	}
	public String getservice_info()
	{
		return service_info;
	}
	public void setservice_info(String aservice_info)
	{
		service_info = aservice_info;
	}
	public String getbak1()
	{
		return bak1;
	}
	public void setbak1(String abak1)
	{
		bak1 = abak1;
	}
	public String getbak2()
	{
		return bak2;
	}
	public void setbak2(String abak2)
	{
		bak2 = abak2;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 tab_midplat_serviceSchema 对象给 Schema 赋值
	* @param: atab_midplat_serviceSchema tab_midplat_serviceSchema
	**/
	public void setSchema(tab_midplat_serviceSchema atab_midplat_serviceSchema)
	{
		this.service_id = atab_midplat_serviceSchema.getservice_id();
		this.sub_service_id = atab_midplat_serviceSchema.getsub_service_id();
		this.sub_service_order = atab_midplat_serviceSchema.getsub_service_order();
		this.request_type = atab_midplat_serviceSchema.getrequest_type();
		this.impl_type = atab_midplat_serviceSchema.getimpl_type();
		this.service_provider = atab_midplat_serviceSchema.getservice_provider();
		this.service_info = atab_midplat_serviceSchema.getservice_info();
		this.bak1 = atab_midplat_serviceSchema.getbak1();
		this.bak2 = atab_midplat_serviceSchema.getbak2();
		this.MakeDate = fDate.getDate( atab_midplat_serviceSchema.getMakeDate());
		this.MakeTime = atab_midplat_serviceSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( atab_midplat_serviceSchema.getModifyDate());
		this.ModifyTime = atab_midplat_serviceSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("service_id") == null )
				this.service_id = null;
			else
				this.service_id = rs.getString("service_id").trim();

			if( rs.getString("sub_service_id") == null )
				this.sub_service_id = null;
			else
				this.sub_service_id = rs.getString("sub_service_id").trim();

			if( rs.getString("sub_service_order") == null )
				this.sub_service_order = null;
			else
				this.sub_service_order = rs.getString("sub_service_order").trim();

			if( rs.getString("request_type") == null )
				this.request_type = null;
			else
				this.request_type = rs.getString("request_type").trim();

			if( rs.getString("impl_type") == null )
				this.impl_type = null;
			else
				this.impl_type = rs.getString("impl_type").trim();

			if( rs.getString("service_provider") == null )
				this.service_provider = null;
			else
				this.service_provider = rs.getString("service_provider").trim();

			if( rs.getString("service_info") == null )
				this.service_info = null;
			else
				this.service_info = rs.getString("service_info").trim();

			if( rs.getString("bak1") == null )
				this.bak1 = null;
			else
				this.bak1 = rs.getString("bak1").trim();

			if( rs.getString("bak2") == null )
				this.bak2 = null;
			else
				this.bak2 = rs.getString("bak2").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的tab_midplat_service表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "tab_midplat_serviceSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public tab_midplat_serviceSchema getSchema()
	{
		tab_midplat_serviceSchema atab_midplat_serviceSchema = new tab_midplat_serviceSchema();
		atab_midplat_serviceSchema.setSchema(this);
		return atab_midplat_serviceSchema;
	}

	public tab_midplat_serviceDB getDB()
	{
		tab_midplat_serviceDB aDBOper = new tab_midplat_serviceDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#Prptab_midplat_service描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append(StrTool.cTrim(service_id)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(sub_service_id)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(sub_service_order)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(request_type)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(impl_type)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(service_provider)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(service_info)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(bak1)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(bak2)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#Prptab_midplat_service>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			service_id = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			sub_service_id = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			sub_service_order = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			request_type = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			impl_type = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			service_provider = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			service_info = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			bak1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			bak2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "tab_midplat_serviceSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equalsIgnoreCase("service_id"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(service_id));
		}
		if (FCode.equalsIgnoreCase("sub_service_id"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(sub_service_id));
		}
		if (FCode.equalsIgnoreCase("sub_service_order"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(sub_service_order));
		}
		if (FCode.equalsIgnoreCase("request_type"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(request_type));
		}
		if (FCode.equalsIgnoreCase("impl_type"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(impl_type));
		}
		if (FCode.equalsIgnoreCase("service_provider"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(service_provider));
		}
		if (FCode.equalsIgnoreCase("service_info"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(service_info));
		}
		if (FCode.equalsIgnoreCase("bak1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak1));
		}
		if (FCode.equalsIgnoreCase("bak2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak2));
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(service_id);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(sub_service_id);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(sub_service_order);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(request_type);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(impl_type);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(service_provider);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(service_info);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(bak1);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(bak2);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("service_id"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				service_id = FValue.trim();
			}
		}
		if (FCode.equalsIgnoreCase("sub_service_id"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				sub_service_id = FValue.trim();
			}
		}
		if (FCode.equalsIgnoreCase("sub_service_order"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				sub_service_order = FValue.trim();
			}
		}
		if (FCode.equalsIgnoreCase("request_type"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				request_type = FValue.trim();
			}
		}
		if (FCode.equalsIgnoreCase("impl_type"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				impl_type = FValue.trim();
			}
		}
		if (FCode.equalsIgnoreCase("service_provider"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				service_provider = FValue.trim();
			}
		}
		if (FCode.equalsIgnoreCase("service_info"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				service_info = FValue.trim();
			}
		}
		if (FCode.equalsIgnoreCase("bak1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak1 = FValue.trim();
			}
		}
		if (FCode.equalsIgnoreCase("bak2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak2 = FValue.trim();
			}
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		tab_midplat_serviceSchema other = (tab_midplat_serviceSchema)otherObject;
		return
			service_id.equals(other.getservice_id())
			&& sub_service_id.equals(other.getsub_service_id())
			&& sub_service_order.equals(other.getsub_service_order())
			&& request_type.equals(other.getrequest_type())
			&& impl_type.equals(other.getimpl_type())
			&& service_provider.equals(other.getservice_provider())
			&& service_info.equals(other.getservice_info())
			&& bak1.equals(other.getbak1())
			&& bak2.equals(other.getbak2())
			&& fDate.getString(MakeDate).equals(other.getMakeDate())
			&& MakeTime.equals(other.getMakeTime())
			&& fDate.getString(ModifyDate).equals(other.getModifyDate())
			&& ModifyTime.equals(other.getModifyTime());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("service_id") ) {
			return 0;
		}
		if( strFieldName.equals("sub_service_id") ) {
			return 1;
		}
		if( strFieldName.equals("sub_service_order") ) {
			return 2;
		}
		if( strFieldName.equals("request_type") ) {
			return 3;
		}
		if( strFieldName.equals("impl_type") ) {
			return 4;
		}
		if( strFieldName.equals("service_provider") ) {
			return 5;
		}
		if( strFieldName.equals("service_info") ) {
			return 6;
		}
		if( strFieldName.equals("bak1") ) {
			return 7;
		}
		if( strFieldName.equals("bak2") ) {
			return 8;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 9;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 10;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 11;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 12;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "service_id";
				break;
			case 1:
				strFieldName = "sub_service_id";
				break;
			case 2:
				strFieldName = "sub_service_order";
				break;
			case 3:
				strFieldName = "request_type";
				break;
			case 4:
				strFieldName = "impl_type";
				break;
			case 5:
				strFieldName = "service_provider";
				break;
			case 6:
				strFieldName = "service_info";
				break;
			case 7:
				strFieldName = "bak1";
				break;
			case 8:
				strFieldName = "bak2";
				break;
			case 9:
				strFieldName = "MakeDate";
				break;
			case 10:
				strFieldName = "MakeTime";
				break;
			case 11:
				strFieldName = "ModifyDate";
				break;
			case 12:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("service_id") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("sub_service_id") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("sub_service_order") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("request_type") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("impl_type") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("service_provider") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("service_info") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
