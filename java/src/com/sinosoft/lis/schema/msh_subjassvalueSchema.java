/*
 * <p>ClassName: msh_subjassvalueSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: MSREPORT
 * @CreateDate：2005-01-10
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.msh_subjassvalueDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class msh_subjassvalueSchema implements Schema
{
    // @Field
    /** 公司编码 */
    private String unitcode;
    /** 公司名称 */
    private String unitname;
    /** 年度 */
    private String year;
    /** 会计月份 */
    private String month;
    /** 科目编码 */
    private String subjcode;
    /** 科目名称 */
    private String subjname;
    /** 余额方向 */
    private int balanorient;
    /** 辅助1编码 */
    private String assitem1code;
    /** 辅助1名称 */
    private String assitem1name;
    /** 辅助2编码 */
    private String assitem2code;
    /** 辅助2名称 */
    private String assitem2name;
    /** 辅助3编码 */
    private String assitem3code;
    /** 辅助3名称 */
    private String assitem3name;
    /** 辅助4编码 */
    private String assitem4code;
    /** 辅助4名称 */
    private String assitem4name;
    /** 辅助5编码 */
    private String assitem5code;
    /** 辅助5名称 */
    private String assitem5name;
    /** 借方金额 */
    private double debitamount;
    /** 贷方金额 */
    private double creditamount;
    /** 期末金额 */
    private double endamount;
    /** 借方累计金额 */
    private double totaldebitamount;
    /** 贷方累计金额 */
    private double totalcreditamount;

    public static final int FIELDNUM = 22; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public msh_subjassvalueSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[0];

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getunitcode()
    {
        if (unitcode != null && !unitcode.equals("") && SysConst.CHANGECHARSET == true)
        {
            unitcode = StrTool.unicodeToGBK(unitcode);
        }
        return unitcode;
    }

    public void setunitcode(String aunitcode)
    {
        unitcode = aunitcode;
    }

    public String getunitname()
    {
        if (unitname != null && !unitname.equals("") && SysConst.CHANGECHARSET == true)
        {
            unitname = StrTool.unicodeToGBK(unitname);
        }
        return unitname;
    }

    public void setunitname(String aunitname)
    {
        unitname = aunitname;
    }

    public String getyear()
    {
        if (year != null && !year.equals("") && SysConst.CHANGECHARSET == true)
        {
            year = StrTool.unicodeToGBK(year);
        }
        return year;
    }

    public void setyear(String ayear)
    {
        year = ayear;
    }

    public String getmonth()
    {
        if (month != null && !month.equals("") && SysConst.CHANGECHARSET == true)
        {
            month = StrTool.unicodeToGBK(month);
        }
        return month;
    }

    public void setmonth(String amonth)
    {
        month = amonth;
    }

    public String getsubjcode()
    {
        if (subjcode != null && !subjcode.equals("") && SysConst.CHANGECHARSET == true)
        {
            subjcode = StrTool.unicodeToGBK(subjcode);
        }
        return subjcode;
    }

    public void setsubjcode(String asubjcode)
    {
        subjcode = asubjcode;
    }

    public String getsubjname()
    {
        if (subjname != null && !subjname.equals("") && SysConst.CHANGECHARSET == true)
        {
            subjname = StrTool.unicodeToGBK(subjname);
        }
        return subjname;
    }

    public void setsubjname(String asubjname)
    {
        subjname = asubjname;
    }

    public int getbalanorient()
    {
        return balanorient;
    }

    public void setbalanorient(int abalanorient)
    {
        balanorient = abalanorient;
    }

    public void setbalanorient(String abalanorient)
    {
        if (abalanorient != null && !abalanorient.equals(""))
        {
            Integer tInteger = new Integer(abalanorient);
            int i = tInteger.intValue();
            balanorient = i;
        }
    }

    public String getassitem1code()
    {
        if (assitem1code != null && !assitem1code.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            assitem1code = StrTool.unicodeToGBK(assitem1code);
        }
        return assitem1code;
    }

    public void setassitem1code(String aassitem1code)
    {
        assitem1code = aassitem1code;
    }

    public String getassitem1name()
    {
        if (assitem1name != null && !assitem1name.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            assitem1name = StrTool.unicodeToGBK(assitem1name);
        }
        return assitem1name;
    }

    public void setassitem1name(String aassitem1name)
    {
        assitem1name = aassitem1name;
    }

    public String getassitem2code()
    {
        if (assitem2code != null && !assitem2code.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            assitem2code = StrTool.unicodeToGBK(assitem2code);
        }
        return assitem2code;
    }

    public void setassitem2code(String aassitem2code)
    {
        assitem2code = aassitem2code;
    }

    public String getassitem2name()
    {
        if (assitem2name != null && !assitem2name.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            assitem2name = StrTool.unicodeToGBK(assitem2name);
        }
        return assitem2name;
    }

    public void setassitem2name(String aassitem2name)
    {
        assitem2name = aassitem2name;
    }

    public String getassitem3code()
    {
        if (assitem3code != null && !assitem3code.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            assitem3code = StrTool.unicodeToGBK(assitem3code);
        }
        return assitem3code;
    }

    public void setassitem3code(String aassitem3code)
    {
        assitem3code = aassitem3code;
    }

    public String getassitem3name()
    {
        if (assitem3name != null && !assitem3name.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            assitem3name = StrTool.unicodeToGBK(assitem3name);
        }
        return assitem3name;
    }

    public void setassitem3name(String aassitem3name)
    {
        assitem3name = aassitem3name;
    }

    public String getassitem4code()
    {
        if (assitem4code != null && !assitem4code.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            assitem4code = StrTool.unicodeToGBK(assitem4code);
        }
        return assitem4code;
    }

    public void setassitem4code(String aassitem4code)
    {
        assitem4code = aassitem4code;
    }

    public String getassitem4name()
    {
        if (assitem4name != null && !assitem4name.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            assitem4name = StrTool.unicodeToGBK(assitem4name);
        }
        return assitem4name;
    }

    public void setassitem4name(String aassitem4name)
    {
        assitem4name = aassitem4name;
    }

    public String getassitem5code()
    {
        if (assitem5code != null && !assitem5code.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            assitem5code = StrTool.unicodeToGBK(assitem5code);
        }
        return assitem5code;
    }

    public void setassitem5code(String aassitem5code)
    {
        assitem5code = aassitem5code;
    }

    public String getassitem5name()
    {
        if (assitem5name != null && !assitem5name.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            assitem5name = StrTool.unicodeToGBK(assitem5name);
        }
        return assitem5name;
    }

    public void setassitem5name(String aassitem5name)
    {
        assitem5name = aassitem5name;
    }

    public double getdebitamount()
    {
        return debitamount;
    }

    public void setdebitamount(double adebitamount)
    {
        debitamount = adebitamount;
    }

    public void setdebitamount(String adebitamount)
    {
        if (adebitamount != null && !adebitamount.equals(""))
        {
            Double tDouble = new Double(adebitamount);
            double d = tDouble.doubleValue();
            debitamount = d;
        }
    }

    public double getcreditamount()
    {
        return creditamount;
    }

    public void setcreditamount(double acreditamount)
    {
        creditamount = acreditamount;
    }

    public void setcreditamount(String acreditamount)
    {
        if (acreditamount != null && !acreditamount.equals(""))
        {
            Double tDouble = new Double(acreditamount);
            double d = tDouble.doubleValue();
            creditamount = d;
        }
    }

    public double getendamount()
    {
        return endamount;
    }

    public void setendamount(double aendamount)
    {
        endamount = aendamount;
    }

    public void setendamount(String aendamount)
    {
        if (aendamount != null && !aendamount.equals(""))
        {
            Double tDouble = new Double(aendamount);
            double d = tDouble.doubleValue();
            endamount = d;
        }
    }

    public double gettotaldebitamount()
    {
        return totaldebitamount;
    }

    public void settotaldebitamount(double atotaldebitamount)
    {
        totaldebitamount = atotaldebitamount;
    }

    public void settotaldebitamount(String atotaldebitamount)
    {
        if (atotaldebitamount != null && !atotaldebitamount.equals(""))
        {
            Double tDouble = new Double(atotaldebitamount);
            double d = tDouble.doubleValue();
            totaldebitamount = d;
        }
    }

    public double gettotalcreditamount()
    {
        return totalcreditamount;
    }

    public void settotalcreditamount(double atotalcreditamount)
    {
        totalcreditamount = atotalcreditamount;
    }

    public void settotalcreditamount(String atotalcreditamount)
    {
        if (atotalcreditamount != null && !atotalcreditamount.equals(""))
        {
            Double tDouble = new Double(atotalcreditamount);
            double d = tDouble.doubleValue();
            totalcreditamount = d;
        }
    }


    /**
     * 使用另外一个 msh_subjassvalueSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(msh_subjassvalueSchema amsh_subjassvalueSchema)
    {
        this.unitcode = amsh_subjassvalueSchema.getunitcode();
        this.unitname = amsh_subjassvalueSchema.getunitname();
        this.year = amsh_subjassvalueSchema.getyear();
        this.month = amsh_subjassvalueSchema.getmonth();
        this.subjcode = amsh_subjassvalueSchema.getsubjcode();
        this.subjname = amsh_subjassvalueSchema.getsubjname();
        this.balanorient = amsh_subjassvalueSchema.getbalanorient();
        this.assitem1code = amsh_subjassvalueSchema.getassitem1code();
        this.assitem1name = amsh_subjassvalueSchema.getassitem1name();
        this.assitem2code = amsh_subjassvalueSchema.getassitem2code();
        this.assitem2name = amsh_subjassvalueSchema.getassitem2name();
        this.assitem3code = amsh_subjassvalueSchema.getassitem3code();
        this.assitem3name = amsh_subjassvalueSchema.getassitem3name();
        this.assitem4code = amsh_subjassvalueSchema.getassitem4code();
        this.assitem4name = amsh_subjassvalueSchema.getassitem4name();
        this.assitem5code = amsh_subjassvalueSchema.getassitem5code();
        this.assitem5name = amsh_subjassvalueSchema.getassitem5name();
        this.debitamount = amsh_subjassvalueSchema.getdebitamount();
        this.creditamount = amsh_subjassvalueSchema.getcreditamount();
        this.endamount = amsh_subjassvalueSchema.getendamount();
        this.totaldebitamount = amsh_subjassvalueSchema.gettotaldebitamount();
        this.totalcreditamount = amsh_subjassvalueSchema.gettotalcreditamount();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("unitcode") == null)
            {
                this.unitcode = null;
            }
            else
            {
                this.unitcode = rs.getString("unitcode").trim();
            }

            if (rs.getString("unitname") == null)
            {
                this.unitname = null;
            }
            else
            {
                this.unitname = rs.getString("unitname").trim();
            }

            if (rs.getString("year") == null)
            {
                this.year = null;
            }
            else
            {
                this.year = rs.getString("year").trim();
            }

            if (rs.getString("month") == null)
            {
                this.month = null;
            }
            else
            {
                this.month = rs.getString("month").trim();
            }

            if (rs.getString("subjcode") == null)
            {
                this.subjcode = null;
            }
            else
            {
                this.subjcode = rs.getString("subjcode").trim();
            }

            if (rs.getString("subjname") == null)
            {
                this.subjname = null;
            }
            else
            {
                this.subjname = rs.getString("subjname").trim();
            }

            this.balanorient = rs.getInt("balanorient");
            if (rs.getString("assitem1code") == null)
            {
                this.assitem1code = null;
            }
            else
            {
                this.assitem1code = rs.getString("assitem1code").trim();
            }

            if (rs.getString("assitem1name") == null)
            {
                this.assitem1name = null;
            }
            else
            {
                this.assitem1name = rs.getString("assitem1name").trim();
            }

            if (rs.getString("assitem2code") == null)
            {
                this.assitem2code = null;
            }
            else
            {
                this.assitem2code = rs.getString("assitem2code").trim();
            }

            if (rs.getString("assitem2name") == null)
            {
                this.assitem2name = null;
            }
            else
            {
                this.assitem2name = rs.getString("assitem2name").trim();
            }

            if (rs.getString("assitem3code") == null)
            {
                this.assitem3code = null;
            }
            else
            {
                this.assitem3code = rs.getString("assitem3code").trim();
            }

            if (rs.getString("assitem3name") == null)
            {
                this.assitem3name = null;
            }
            else
            {
                this.assitem3name = rs.getString("assitem3name").trim();
            }

            if (rs.getString("assitem4code") == null)
            {
                this.assitem4code = null;
            }
            else
            {
                this.assitem4code = rs.getString("assitem4code").trim();
            }

            if (rs.getString("assitem4name") == null)
            {
                this.assitem4name = null;
            }
            else
            {
                this.assitem4name = rs.getString("assitem4name").trim();
            }

            if (rs.getString("assitem5code") == null)
            {
                this.assitem5code = null;
            }
            else
            {
                this.assitem5code = rs.getString("assitem5code").trim();
            }

            if (rs.getString("assitem5name") == null)
            {
                this.assitem5name = null;
            }
            else
            {
                this.assitem5name = rs.getString("assitem5name").trim();
            }

            this.debitamount = rs.getDouble("debitamount");
            this.creditamount = rs.getDouble("creditamount");
            this.endamount = rs.getDouble("endamount");
            this.totaldebitamount = rs.getDouble("totaldebitamount");
            this.totalcreditamount = rs.getDouble("totalcreditamount");
        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "msh_subjassvalueSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public msh_subjassvalueSchema getSchema()
    {
        msh_subjassvalueSchema amsh_subjassvalueSchema = new
                msh_subjassvalueSchema();
        amsh_subjassvalueSchema.setSchema(this);
        return amsh_subjassvalueSchema;
    }

    public msh_subjassvalueDB getDB()
    {
        msh_subjassvalueDB aDBOper = new msh_subjassvalueDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#Prpmsh_subjassvalue描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(unitcode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(unitname)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(year)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(month)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(subjcode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(subjname)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(balanorient) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(assitem1code)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(assitem1name)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(assitem2code)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(assitem2name)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(assitem3code)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(assitem3name)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(assitem4code)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(assitem4name)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(assitem5code)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(assitem5name)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(debitamount) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(creditamount) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(endamount) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(totaldebitamount) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(totalcreditamount);
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#Prpmsh_subjassvalue>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            unitcode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            unitname = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            year = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                  SysConst.PACKAGESPILTER);
            month = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                   SysConst.PACKAGESPILTER);
            subjcode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                      SysConst.PACKAGESPILTER);
            subjname = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                      SysConst.PACKAGESPILTER);
            balanorient = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 7, SysConst.PACKAGESPILTER))).intValue();
            assitem1code = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                          SysConst.PACKAGESPILTER);
            assitem1name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                          SysConst.PACKAGESPILTER);
            assitem2code = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                          SysConst.PACKAGESPILTER);
            assitem2name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                          SysConst.PACKAGESPILTER);
            assitem3code = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                          SysConst.PACKAGESPILTER);
            assitem3name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                          SysConst.PACKAGESPILTER);
            assitem4code = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                          SysConst.PACKAGESPILTER);
            assitem4name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                          SysConst.PACKAGESPILTER);
            assitem5code = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                          SysConst.PACKAGESPILTER);
            assitem5name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                          SysConst.PACKAGESPILTER);
            debitamount = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 18, SysConst.PACKAGESPILTER))).doubleValue();
            creditamount = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 19, SysConst.PACKAGESPILTER))).doubleValue();
            endamount = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 20, SysConst.PACKAGESPILTER))).doubleValue();
            totaldebitamount = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 21, SysConst.PACKAGESPILTER))).doubleValue();
            totalcreditamount = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 22, SysConst.PACKAGESPILTER))).doubleValue();
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "msh_subjassvalueSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("unitcode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(unitcode));
        }
        if (FCode.equals("unitname"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(unitname));
        }
        if (FCode.equals("year"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(year));
        }
        if (FCode.equals("month"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(month));
        }
        if (FCode.equals("subjcode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(subjcode));
        }
        if (FCode.equals("subjname"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(subjname));
        }
        if (FCode.equals("balanorient"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(balanorient));
        }
        if (FCode.equals("assitem1code"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(assitem1code));
        }
        if (FCode.equals("assitem1name"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(assitem1name));
        }
        if (FCode.equals("assitem2code"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(assitem2code));
        }
        if (FCode.equals("assitem2name"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(assitem2name));
        }
        if (FCode.equals("assitem3code"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(assitem3code));
        }
        if (FCode.equals("assitem3name"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(assitem3name));
        }
        if (FCode.equals("assitem4code"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(assitem4code));
        }
        if (FCode.equals("assitem4name"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(assitem4name));
        }
        if (FCode.equals("assitem5code"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(assitem5code));
        }
        if (FCode.equals("assitem5name"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(assitem5name));
        }
        if (FCode.equals("debitamount"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(debitamount));
        }
        if (FCode.equals("creditamount"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(creditamount));
        }
        if (FCode.equals("endamount"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(endamount));
        }
        if (FCode.equals("totaldebitamount"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(totaldebitamount));
        }
        if (FCode.equals("totalcreditamount"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(
                    totalcreditamount));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(unitcode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(unitname);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(year);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(month);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(subjcode);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(subjname);
                break;
            case 6:
                strFieldValue = String.valueOf(balanorient);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(assitem1code);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(assitem1name);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(assitem2code);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(assitem2name);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(assitem3code);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(assitem3name);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(assitem4code);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(assitem4name);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(assitem5code);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(assitem5name);
                break;
            case 17:
                strFieldValue = String.valueOf(debitamount);
                break;
            case 18:
                strFieldValue = String.valueOf(creditamount);
                break;
            case 19:
                strFieldValue = String.valueOf(endamount);
                break;
            case 20:
                strFieldValue = String.valueOf(totaldebitamount);
                break;
            case 21:
                strFieldValue = String.valueOf(totalcreditamount);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("unitcode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                unitcode = FValue.trim();
            }
            else
            {
                unitcode = null;
            }
        }
        if (FCode.equals("unitname"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                unitname = FValue.trim();
            }
            else
            {
                unitname = null;
            }
        }
        if (FCode.equals("year"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                year = FValue.trim();
            }
            else
            {
                year = null;
            }
        }
        if (FCode.equals("month"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                month = FValue.trim();
            }
            else
            {
                month = null;
            }
        }
        if (FCode.equals("subjcode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                subjcode = FValue.trim();
            }
            else
            {
                subjcode = null;
            }
        }
        if (FCode.equals("subjname"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                subjname = FValue.trim();
            }
            else
            {
                subjname = null;
            }
        }
        if (FCode.equals("balanorient"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                balanorient = i;
            }
        }
        if (FCode.equals("assitem1code"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                assitem1code = FValue.trim();
            }
            else
            {
                assitem1code = null;
            }
        }
        if (FCode.equals("assitem1name"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                assitem1name = FValue.trim();
            }
            else
            {
                assitem1name = null;
            }
        }
        if (FCode.equals("assitem2code"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                assitem2code = FValue.trim();
            }
            else
            {
                assitem2code = null;
            }
        }
        if (FCode.equals("assitem2name"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                assitem2name = FValue.trim();
            }
            else
            {
                assitem2name = null;
            }
        }
        if (FCode.equals("assitem3code"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                assitem3code = FValue.trim();
            }
            else
            {
                assitem3code = null;
            }
        }
        if (FCode.equals("assitem3name"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                assitem3name = FValue.trim();
            }
            else
            {
                assitem3name = null;
            }
        }
        if (FCode.equals("assitem4code"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                assitem4code = FValue.trim();
            }
            else
            {
                assitem4code = null;
            }
        }
        if (FCode.equals("assitem4name"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                assitem4name = FValue.trim();
            }
            else
            {
                assitem4name = null;
            }
        }
        if (FCode.equals("assitem5code"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                assitem5code = FValue.trim();
            }
            else
            {
                assitem5code = null;
            }
        }
        if (FCode.equals("assitem5name"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                assitem5name = FValue.trim();
            }
            else
            {
                assitem5name = null;
            }
        }
        if (FCode.equals("debitamount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                debitamount = d;
            }
        }
        if (FCode.equals("creditamount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                creditamount = d;
            }
        }
        if (FCode.equals("endamount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                endamount = d;
            }
        }
        if (FCode.equals("totaldebitamount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                totaldebitamount = d;
            }
        }
        if (FCode.equals("totalcreditamount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                totalcreditamount = d;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        msh_subjassvalueSchema other = (msh_subjassvalueSchema) otherObject;
        return
                unitcode.equals(other.getunitcode())
                && unitname.equals(other.getunitname())
                && year.equals(other.getyear())
                && month.equals(other.getmonth())
                && subjcode.equals(other.getsubjcode())
                && subjname.equals(other.getsubjname())
                && balanorient == other.getbalanorient()
                && assitem1code.equals(other.getassitem1code())
                && assitem1name.equals(other.getassitem1name())
                && assitem2code.equals(other.getassitem2code())
                && assitem2name.equals(other.getassitem2name())
                && assitem3code.equals(other.getassitem3code())
                && assitem3name.equals(other.getassitem3name())
                && assitem4code.equals(other.getassitem4code())
                && assitem4name.equals(other.getassitem4name())
                && assitem5code.equals(other.getassitem5code())
                && assitem5name.equals(other.getassitem5name())
                && debitamount == other.getdebitamount()
                && creditamount == other.getcreditamount()
                && endamount == other.getendamount()
                && totaldebitamount == other.gettotaldebitamount()
                && totalcreditamount == other.gettotalcreditamount();
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("unitcode"))
        {
            return 0;
        }
        if (strFieldName.equals("unitname"))
        {
            return 1;
        }
        if (strFieldName.equals("year"))
        {
            return 2;
        }
        if (strFieldName.equals("month"))
        {
            return 3;
        }
        if (strFieldName.equals("subjcode"))
        {
            return 4;
        }
        if (strFieldName.equals("subjname"))
        {
            return 5;
        }
        if (strFieldName.equals("balanorient"))
        {
            return 6;
        }
        if (strFieldName.equals("assitem1code"))
        {
            return 7;
        }
        if (strFieldName.equals("assitem1name"))
        {
            return 8;
        }
        if (strFieldName.equals("assitem2code"))
        {
            return 9;
        }
        if (strFieldName.equals("assitem2name"))
        {
            return 10;
        }
        if (strFieldName.equals("assitem3code"))
        {
            return 11;
        }
        if (strFieldName.equals("assitem3name"))
        {
            return 12;
        }
        if (strFieldName.equals("assitem4code"))
        {
            return 13;
        }
        if (strFieldName.equals("assitem4name"))
        {
            return 14;
        }
        if (strFieldName.equals("assitem5code"))
        {
            return 15;
        }
        if (strFieldName.equals("assitem5name"))
        {
            return 16;
        }
        if (strFieldName.equals("debitamount"))
        {
            return 17;
        }
        if (strFieldName.equals("creditamount"))
        {
            return 18;
        }
        if (strFieldName.equals("endamount"))
        {
            return 19;
        }
        if (strFieldName.equals("totaldebitamount"))
        {
            return 20;
        }
        if (strFieldName.equals("totalcreditamount"))
        {
            return 21;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "unitcode";
                break;
            case 1:
                strFieldName = "unitname";
                break;
            case 2:
                strFieldName = "year";
                break;
            case 3:
                strFieldName = "month";
                break;
            case 4:
                strFieldName = "subjcode";
                break;
            case 5:
                strFieldName = "subjname";
                break;
            case 6:
                strFieldName = "balanorient";
                break;
            case 7:
                strFieldName = "assitem1code";
                break;
            case 8:
                strFieldName = "assitem1name";
                break;
            case 9:
                strFieldName = "assitem2code";
                break;
            case 10:
                strFieldName = "assitem2name";
                break;
            case 11:
                strFieldName = "assitem3code";
                break;
            case 12:
                strFieldName = "assitem3name";
                break;
            case 13:
                strFieldName = "assitem4code";
                break;
            case 14:
                strFieldName = "assitem4name";
                break;
            case 15:
                strFieldName = "assitem5code";
                break;
            case 16:
                strFieldName = "assitem5name";
                break;
            case 17:
                strFieldName = "debitamount";
                break;
            case 18:
                strFieldName = "creditamount";
                break;
            case 19:
                strFieldName = "endamount";
                break;
            case 20:
                strFieldName = "totaldebitamount";
                break;
            case 21:
                strFieldName = "totalcreditamount";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("unitcode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("unitname"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("year"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("month"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("subjcode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("subjname"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("balanorient"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("assitem1code"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("assitem1name"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("assitem2code"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("assitem2name"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("assitem3code"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("assitem3name"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("assitem4code"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("assitem4name"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("assitem5code"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("assitem5name"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("debitamount"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("creditamount"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("endamount"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("totaldebitamount"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("totalcreditamount"))
        {
            return Schema.TYPE_DOUBLE;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_INT;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 17:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 18:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 19:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 20:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 21:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
