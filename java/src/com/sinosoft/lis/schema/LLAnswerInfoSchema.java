/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LLAnswerInfoDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LLAnswerInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-04-20
 */
public class LLAnswerInfoSchema implements Schema, Cloneable
{
    // @Field
    /** 咨询号码 */
    private String ConsultNo;
    /** 登记号码 */
    private String LogNo;
    /** 回复序号 */
    private int SerialNo;
    /** 询问状态 */
    private String AskState;
    /** 咨询回复人 */
    private String AnswerOperator;
    /** 咨询回复 */
    private String Answer;
    /** 回复管理机构 */
    private String ManageCom;
    /** 咨询回复日期 */
    private Date AnswerDate;
    /** 咨询回复时间 */
    private String AnswerTime;
    /** 发送标记 */
    private String SendFlag;
    /** 备注 */
    private String Remark;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 解答结束日期 */
    private Date AnswerEndDate;
    /** 解答结束时间 */
    private String AnswerEndTime;
    /** 解答花费时间 */
    private String AnswerCostTime;

    public static final int FIELDNUM = 18; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LLAnswerInfoSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "ConsultNo";
        pk[1] = "LogNo";
        pk[2] = "SerialNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException
    {
        LLAnswerInfoSchema cloned = (LLAnswerInfoSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getConsultNo()
    {
        if (SysConst.CHANGECHARSET && ConsultNo != null && !ConsultNo.equals(""))
        {
            ConsultNo = StrTool.unicodeToGBK(ConsultNo);
        }
        return ConsultNo;
    }

    public void setConsultNo(String aConsultNo)
    {
        ConsultNo = aConsultNo;
    }

    public String getLogNo()
    {
        if (SysConst.CHANGECHARSET && LogNo != null && !LogNo.equals(""))
        {
            LogNo = StrTool.unicodeToGBK(LogNo);
        }
        return LogNo;
    }

    public void setLogNo(String aLogNo)
    {
        LogNo = aLogNo;
    }

    public int getSerialNo()
    {
        return SerialNo;
    }

    public void setSerialNo(int aSerialNo)
    {
        SerialNo = aSerialNo;
    }

    public void setSerialNo(String aSerialNo)
    {
        if (aSerialNo != null && !aSerialNo.equals(""))
        {
            Integer tInteger = new Integer(aSerialNo);
            int i = tInteger.intValue();
            SerialNo = i;
        }
    }

    public String getAskState()
    {
        if (SysConst.CHANGECHARSET && AskState != null && !AskState.equals(""))
        {
            AskState = StrTool.unicodeToGBK(AskState);
        }
        return AskState;
    }

    public void setAskState(String aAskState)
    {
        AskState = aAskState;
    }

    public String getAnswerOperator()
    {
        if (SysConst.CHANGECHARSET && AnswerOperator != null &&
            !AnswerOperator.equals(""))
        {
            AnswerOperator = StrTool.unicodeToGBK(AnswerOperator);
        }
        return AnswerOperator;
    }

    public void setAnswerOperator(String aAnswerOperator)
    {
        AnswerOperator = aAnswerOperator;
    }

    public String getAnswer()
    {
        if (SysConst.CHANGECHARSET && Answer != null && !Answer.equals(""))
        {
            Answer = StrTool.unicodeToGBK(Answer);
        }
        return Answer;
    }

    public void setAnswer(String aAnswer)
    {
        Answer = aAnswer;
    }

    public String getManageCom()
    {
        if (SysConst.CHANGECHARSET && ManageCom != null && !ManageCom.equals(""))
        {
            ManageCom = StrTool.unicodeToGBK(ManageCom);
        }
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public String getAnswerDate()
    {
        if (AnswerDate != null)
        {
            return fDate.getString(AnswerDate);
        }
        else
        {
            return null;
        }
    }

    public void setAnswerDate(Date aAnswerDate)
    {
        AnswerDate = aAnswerDate;
    }

    public void setAnswerDate(String aAnswerDate)
    {
        if (aAnswerDate != null && !aAnswerDate.equals(""))
        {
            AnswerDate = fDate.getDate(aAnswerDate);
        }
        else
        {
            AnswerDate = null;
        }
    }

    public String getAnswerTime()
    {
        if (SysConst.CHANGECHARSET && AnswerTime != null &&
            !AnswerTime.equals(""))
        {
            AnswerTime = StrTool.unicodeToGBK(AnswerTime);
        }
        return AnswerTime;
    }

    public void setAnswerTime(String aAnswerTime)
    {
        AnswerTime = aAnswerTime;
    }

    public String getSendFlag()
    {
        if (SysConst.CHANGECHARSET && SendFlag != null && !SendFlag.equals(""))
        {
            SendFlag = StrTool.unicodeToGBK(SendFlag);
        }
        return SendFlag;
    }

    public void setSendFlag(String aSendFlag)
    {
        SendFlag = aSendFlag;
    }

    public String getRemark()
    {
        if (SysConst.CHANGECHARSET && Remark != null && !Remark.equals(""))
        {
            Remark = StrTool.unicodeToGBK(Remark);
        }
        return Remark;
    }

    public void setRemark(String aRemark)
    {
        Remark = aRemark;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (SysConst.CHANGECHARSET && MakeTime != null && !MakeTime.equals(""))
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (SysConst.CHANGECHARSET && ModifyTime != null &&
            !ModifyTime.equals(""))
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    public String getAnswerEndDate()
    {
        if (AnswerEndDate != null)
        {
            return fDate.getString(AnswerEndDate);
        }
        else
        {
            return null;
        }
    }

    public void setAnswerEndDate(Date aAnswerEndDate)
    {
        AnswerEndDate = aAnswerEndDate;
    }

    public void setAnswerEndDate(String aAnswerEndDate)
    {
        if (aAnswerEndDate != null && !aAnswerEndDate.equals(""))
        {
            AnswerEndDate = fDate.getDate(aAnswerEndDate);
        }
        else
        {
            AnswerEndDate = null;
        }
    }

    public String getAnswerEndTime()
    {
        if (SysConst.CHANGECHARSET && AnswerEndTime != null &&
            !AnswerEndTime.equals(""))
        {
            AnswerEndTime = StrTool.unicodeToGBK(AnswerEndTime);
        }
        return AnswerEndTime;
    }

    public void setAnswerEndTime(String aAnswerEndTime)
    {
        AnswerEndTime = aAnswerEndTime;
    }

    public String getAnswerCostTime()
    {
        if (SysConst.CHANGECHARSET && AnswerCostTime != null &&
            !AnswerCostTime.equals(""))
        {
            AnswerCostTime = StrTool.unicodeToGBK(AnswerCostTime);
        }
        return AnswerCostTime;
    }

    public void setAnswerCostTime(String aAnswerCostTime)
    {
        AnswerCostTime = aAnswerCostTime;
    }

    /**
     * 使用另外一个 LLAnswerInfoSchema 对象给 Schema 赋值
     * @param: aLLAnswerInfoSchema LLAnswerInfoSchema
     **/
    public void setSchema(LLAnswerInfoSchema aLLAnswerInfoSchema)
    {
        this.ConsultNo = aLLAnswerInfoSchema.getConsultNo();
        this.LogNo = aLLAnswerInfoSchema.getLogNo();
        this.SerialNo = aLLAnswerInfoSchema.getSerialNo();
        this.AskState = aLLAnswerInfoSchema.getAskState();
        this.AnswerOperator = aLLAnswerInfoSchema.getAnswerOperator();
        this.Answer = aLLAnswerInfoSchema.getAnswer();
        this.ManageCom = aLLAnswerInfoSchema.getManageCom();
        this.AnswerDate = fDate.getDate(aLLAnswerInfoSchema.getAnswerDate());
        this.AnswerTime = aLLAnswerInfoSchema.getAnswerTime();
        this.SendFlag = aLLAnswerInfoSchema.getSendFlag();
        this.Remark = aLLAnswerInfoSchema.getRemark();
        this.MakeDate = fDate.getDate(aLLAnswerInfoSchema.getMakeDate());
        this.MakeTime = aLLAnswerInfoSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLLAnswerInfoSchema.getModifyDate());
        this.ModifyTime = aLLAnswerInfoSchema.getModifyTime();
        this.AnswerEndDate = fDate.getDate(aLLAnswerInfoSchema.getAnswerEndDate());
        this.AnswerEndTime = aLLAnswerInfoSchema.getAnswerEndTime();
        this.AnswerCostTime = aLLAnswerInfoSchema.getAnswerCostTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString(1) == null)
            {
                this.ConsultNo = null;
            }
            else
            {
                this.ConsultNo = rs.getString(1).trim();
            }

            if (rs.getString(2) == null)
            {
                this.LogNo = null;
            }
            else
            {
                this.LogNo = rs.getString(2).trim();
            }

            this.SerialNo = rs.getInt(3);
            if (rs.getString(4) == null)
            {
                this.AskState = null;
            }
            else
            {
                this.AskState = rs.getString(4).trim();
            }

            if (rs.getString(5) == null)
            {
                this.AnswerOperator = null;
            }
            else
            {
                this.AnswerOperator = rs.getString(5).trim();
            }

            if (rs.getString(6) == null)
            {
                this.Answer = null;
            }
            else
            {
                this.Answer = rs.getString(6).trim();
            }

            if (rs.getString(7) == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString(7).trim();
            }

            this.AnswerDate = rs.getDate(8);
            if (rs.getString(9) == null)
            {
                this.AnswerTime = null;
            }
            else
            {
                this.AnswerTime = rs.getString(9).trim();
            }

            if (rs.getString(10) == null)
            {
                this.SendFlag = null;
            }
            else
            {
                this.SendFlag = rs.getString(10).trim();
            }

            if (rs.getString(11) == null)
            {
                this.Remark = null;
            }
            else
            {
                this.Remark = rs.getString(11).trim();
            }

            this.MakeDate = rs.getDate(12);
            if (rs.getString(13) == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString(13).trim();
            }

            this.ModifyDate = rs.getDate(14);
            if (rs.getString(15) == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString(15).trim();
            }

            this.AnswerEndDate = rs.getDate(16);
            if (rs.getString(17) == null)
            {
                this.AnswerEndTime = null;
            }
            else
            {
                this.AnswerEndTime = rs.getString(17).trim();
            }

            if (rs.getString(18) == null)
            {
                this.AnswerCostTime = null;
            }
            else
            {
                this.AnswerCostTime = rs.getString(18).trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLAnswerInfoSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LLAnswerInfoSchema getSchema()
    {
        LLAnswerInfoSchema aLLAnswerInfoSchema = new LLAnswerInfoSchema();
        aLLAnswerInfoSchema.setSchema(this);
        return aLLAnswerInfoSchema;
    }

    public LLAnswerInfoDB getDB()
    {
        LLAnswerInfoDB aDBOper = new LLAnswerInfoDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLAnswerInfo描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ConsultNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(LogNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SerialNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AskState)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AnswerOperator)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Answer)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ManageCom)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                AnswerDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AnswerTime)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(SendFlag)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Remark)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                MakeDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                ModifyDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                AnswerEndDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AnswerEndTime)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AnswerCostTime)));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLAnswerInfo>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            ConsultNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            LogNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                   SysConst.PACKAGESPILTER);
            SerialNo = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 3, SysConst.PACKAGESPILTER))).intValue();
            AskState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                      SysConst.PACKAGESPILTER);
            AnswerOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                            SysConst.PACKAGESPILTER);
            Answer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                    SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                       SysConst.PACKAGESPILTER);
            AnswerDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 8, SysConst.PACKAGESPILTER));
            AnswerTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                        SysConst.PACKAGESPILTER);
            SendFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                      SysConst.PACKAGESPILTER);
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                    SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 12, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 14, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                        SysConst.PACKAGESPILTER);
            AnswerEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 16, SysConst.PACKAGESPILTER));
            AnswerEndTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                           SysConst.PACKAGESPILTER);
            AnswerCostTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                            18, SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLAnswerInfoSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("ConsultNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ConsultNo));
        }
        if (FCode.equals("LogNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LogNo));
        }
        if (FCode.equals("SerialNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equals("AskState"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AskState));
        }
        if (FCode.equals("AnswerOperator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AnswerOperator));
        }
        if (FCode.equals("Answer"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Answer));
        }
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("AnswerDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getAnswerDate()));
        }
        if (FCode.equals("AnswerTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AnswerTime));
        }
        if (FCode.equals("SendFlag"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SendFlag));
        }
        if (FCode.equals("Remark"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("AnswerEndDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getAnswerEndDate()));
        }
        if (FCode.equals("AnswerEndTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AnswerEndTime));
        }
        if (FCode.equals("AnswerCostTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AnswerCostTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ConsultNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(LogNo);
                break;
            case 2:
                strFieldValue = String.valueOf(SerialNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(AskState);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(AnswerOperator);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(Answer);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getAnswerDate()));
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(AnswerTime);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(SendFlag);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getAnswerEndDate()));
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(AnswerEndTime);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(AnswerCostTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("ConsultNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ConsultNo = FValue.trim();
            }
            else
            {
                ConsultNo = null;
            }
        }
        if (FCode.equals("LogNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                LogNo = FValue.trim();
            }
            else
            {
                LogNo = null;
            }
        }
        if (FCode.equals("SerialNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                SerialNo = i;
            }
        }
        if (FCode.equals("AskState"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AskState = FValue.trim();
            }
            else
            {
                AskState = null;
            }
        }
        if (FCode.equals("AnswerOperator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AnswerOperator = FValue.trim();
            }
            else
            {
                AnswerOperator = null;
            }
        }
        if (FCode.equals("Answer"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Answer = FValue.trim();
            }
            else
            {
                Answer = null;
            }
        }
        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("AnswerDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AnswerDate = fDate.getDate(FValue);
            }
            else
            {
                AnswerDate = null;
            }
        }
        if (FCode.equals("AnswerTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AnswerTime = FValue.trim();
            }
            else
            {
                AnswerTime = null;
            }
        }
        if (FCode.equals("SendFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SendFlag = FValue.trim();
            }
            else
            {
                SendFlag = null;
            }
        }
        if (FCode.equals("Remark"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
            {
                Remark = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        if (FCode.equals("AnswerEndDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AnswerEndDate = fDate.getDate(FValue);
            }
            else
            {
                AnswerEndDate = null;
            }
        }
        if (FCode.equals("AnswerEndTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AnswerEndTime = FValue.trim();
            }
            else
            {
                AnswerEndTime = null;
            }
        }
        if (FCode.equals("AnswerCostTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AnswerCostTime = FValue.trim();
            }
            else
            {
                AnswerCostTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LLAnswerInfoSchema other = (LLAnswerInfoSchema) otherObject;
        return
                ConsultNo.equals(other.getConsultNo())
                && LogNo.equals(other.getLogNo())
                && SerialNo == other.getSerialNo()
                && AskState.equals(other.getAskState())
                && AnswerOperator.equals(other.getAnswerOperator())
                && Answer.equals(other.getAnswer())
                && ManageCom.equals(other.getManageCom())
                && fDate.getString(AnswerDate).equals(other.getAnswerDate())
                && AnswerTime.equals(other.getAnswerTime())
                && SendFlag.equals(other.getSendFlag())
                && Remark.equals(other.getRemark())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && fDate.getString(AnswerEndDate).equals(other.getAnswerEndDate())
                && AnswerEndTime.equals(other.getAnswerEndTime())
                && AnswerCostTime.equals(other.getAnswerCostTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("ConsultNo"))
        {
            return 0;
        }
        if (strFieldName.equals("LogNo"))
        {
            return 1;
        }
        if (strFieldName.equals("SerialNo"))
        {
            return 2;
        }
        if (strFieldName.equals("AskState"))
        {
            return 3;
        }
        if (strFieldName.equals("AnswerOperator"))
        {
            return 4;
        }
        if (strFieldName.equals("Answer"))
        {
            return 5;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return 6;
        }
        if (strFieldName.equals("AnswerDate"))
        {
            return 7;
        }
        if (strFieldName.equals("AnswerTime"))
        {
            return 8;
        }
        if (strFieldName.equals("SendFlag"))
        {
            return 9;
        }
        if (strFieldName.equals("Remark"))
        {
            return 10;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 11;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 12;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 13;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 14;
        }
        if (strFieldName.equals("AnswerEndDate"))
        {
            return 15;
        }
        if (strFieldName.equals("AnswerEndTime"))
        {
            return 16;
        }
        if (strFieldName.equals("AnswerCostTime"))
        {
            return 17;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "ConsultNo";
                break;
            case 1:
                strFieldName = "LogNo";
                break;
            case 2:
                strFieldName = "SerialNo";
                break;
            case 3:
                strFieldName = "AskState";
                break;
            case 4:
                strFieldName = "AnswerOperator";
                break;
            case 5:
                strFieldName = "Answer";
                break;
            case 6:
                strFieldName = "ManageCom";
                break;
            case 7:
                strFieldName = "AnswerDate";
                break;
            case 8:
                strFieldName = "AnswerTime";
                break;
            case 9:
                strFieldName = "SendFlag";
                break;
            case 10:
                strFieldName = "Remark";
                break;
            case 11:
                strFieldName = "MakeDate";
                break;
            case 12:
                strFieldName = "MakeTime";
                break;
            case 13:
                strFieldName = "ModifyDate";
                break;
            case 14:
                strFieldName = "ModifyTime";
                break;
            case 15:
                strFieldName = "AnswerEndDate";
                break;
            case 16:
                strFieldName = "AnswerEndTime";
                break;
            case 17:
                strFieldName = "AnswerCostTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("ConsultNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("LogNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SerialNo"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("AskState"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AnswerOperator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Answer"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AnswerDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("AnswerTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SendFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Remark"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AnswerEndDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("AnswerEndTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AnswerCostTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_INT;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 17:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
