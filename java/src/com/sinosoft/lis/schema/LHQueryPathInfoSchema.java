/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LHQueryPathInfoDB;

/*
 * <p>ClassName: LHQueryPathInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PICCH-郭楠-表结构修改-20060207-查询
 * @CreateDate：2006-02-14
 */
public class LHQueryPathInfoSchema implements Schema, Cloneable {
    // @Field
    /** 服务器名称 */
    private String HostName;
    /** 查询项目类型 */
    private String ItemType;
    /** 服务器ip */
    private String ServerIP;
    /** Ftp路径 */
    private String PicPathFTP;
    /** Http访问ip与端口 */
    private String ServerPort;
    /** Http路径 */
    private String PicPath;

    public static final int FIELDNUM = 6; // 数据库表的字段个数

    private static String[] PK; // 主键

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LHQueryPathInfoSchema() {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "HostName";
        pk[1] = "ItemType";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LHQueryPathInfoSchema cloned = (LHQueryPathInfoSchema)super.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getHostName() {
        return HostName;
    }

    public void setHostName(String aHostName) {
        HostName = aHostName;
    }

    public String getItemType() {
        return ItemType;
    }

    public void setItemType(String aItemType) {
        ItemType = aItemType;
    }

    public String getServerIP() {
        return ServerIP;
    }

    public void setServerIP(String aServerIP) {
        ServerIP = aServerIP;
    }

    public String getPicPathFTP() {
        return PicPathFTP;
    }

    public void setPicPathFTP(String aPicPathFTP) {
        PicPathFTP = aPicPathFTP;
    }

    public String getServerPort() {
        return ServerPort;
    }

    public void setServerPort(String aServerPort) {
        ServerPort = aServerPort;
    }

    public String getPicPath() {
        return PicPath;
    }

    public void setPicPath(String aPicPath) {
        PicPath = aPicPath;
    }

    /**
     * 使用另外一个 LHQueryPathInfoSchema 对象给 Schema 赋值
     * @param: aLHQueryPathInfoSchema LHQueryPathInfoSchema
     **/
    public void setSchema(LHQueryPathInfoSchema aLHQueryPathInfoSchema) {
        this.HostName = aLHQueryPathInfoSchema.getHostName();
        this.ItemType = aLHQueryPathInfoSchema.getItemType();
        this.ServerIP = aLHQueryPathInfoSchema.getServerIP();
        this.PicPathFTP = aLHQueryPathInfoSchema.getPicPathFTP();
        this.ServerPort = aLHQueryPathInfoSchema.getServerPort();
        this.PicPath = aLHQueryPathInfoSchema.getPicPath();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("HostName") == null) {
                this.HostName = null;
            } else {
                this.HostName = rs.getString("HostName").trim();
            }

            if (rs.getString("ItemType") == null) {
                this.ItemType = null;
            } else {
                this.ItemType = rs.getString("ItemType").trim();
            }

            if (rs.getString("ServerIP") == null) {
                this.ServerIP = null;
            } else {
                this.ServerIP = rs.getString("ServerIP").trim();
            }

            if (rs.getString("PicPathFTP") == null) {
                this.PicPathFTP = null;
            } else {
                this.PicPathFTP = rs.getString("PicPathFTP").trim();
            }

            if (rs.getString("ServerPort") == null) {
                this.ServerPort = null;
            } else {
                this.ServerPort = rs.getString("ServerPort").trim();
            }

            if (rs.getString("PicPath") == null) {
                this.PicPath = null;
            } else {
                this.PicPath = rs.getString("PicPath").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LHQueryPathInfo表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHQueryPathInfoSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LHQueryPathInfoSchema getSchema() {
        LHQueryPathInfoSchema aLHQueryPathInfoSchema = new
                LHQueryPathInfoSchema();
        aLHQueryPathInfoSchema.setSchema(this);
        return aLHQueryPathInfoSchema;
    }

    public LHQueryPathInfoDB getDB() {
        LHQueryPathInfoDB aDBOper = new LHQueryPathInfoDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHQueryPathInfo描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(HostName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ItemType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServerIP));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PicPathFTP));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServerPort));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PicPath));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHQueryPathInfo>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            HostName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            ItemType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            ServerIP = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            PicPathFTP = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                        SysConst.PACKAGESPILTER);
            ServerPort = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                        SysConst.PACKAGESPILTER);
            PicPath = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                     SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHQueryPathInfoSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("HostName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HostName));
        }
        if (FCode.equals("ItemType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ItemType));
        }
        if (FCode.equals("ServerIP")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServerIP));
        }
        if (FCode.equals("PicPathFTP")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PicPathFTP));
        }
        if (FCode.equals("ServerPort")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServerPort));
        }
        if (FCode.equals("PicPath")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PicPath));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(HostName);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(ItemType);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(ServerIP);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(PicPathFTP);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(ServerPort);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(PicPath);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("HostName")) {
            if (FValue != null && !FValue.equals("")) {
                HostName = FValue.trim();
            } else {
                HostName = null;
            }
        }
        if (FCode.equalsIgnoreCase("ItemType")) {
            if (FValue != null && !FValue.equals("")) {
                ItemType = FValue.trim();
            } else {
                ItemType = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServerIP")) {
            if (FValue != null && !FValue.equals("")) {
                ServerIP = FValue.trim();
            } else {
                ServerIP = null;
            }
        }
        if (FCode.equalsIgnoreCase("PicPathFTP")) {
            if (FValue != null && !FValue.equals("")) {
                PicPathFTP = FValue.trim();
            } else {
                PicPathFTP = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServerPort")) {
            if (FValue != null && !FValue.equals("")) {
                ServerPort = FValue.trim();
            } else {
                ServerPort = null;
            }
        }
        if (FCode.equalsIgnoreCase("PicPath")) {
            if (FValue != null && !FValue.equals("")) {
                PicPath = FValue.trim();
            } else {
                PicPath = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LHQueryPathInfoSchema other = (LHQueryPathInfoSchema) otherObject;
        return
                HostName.equals(other.getHostName())
                && ItemType.equals(other.getItemType())
                && ServerIP.equals(other.getServerIP())
                && PicPathFTP.equals(other.getPicPathFTP())
                && ServerPort.equals(other.getServerPort())
                && PicPath.equals(other.getPicPath());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("HostName")) {
            return 0;
        }
        if (strFieldName.equals("ItemType")) {
            return 1;
        }
        if (strFieldName.equals("ServerIP")) {
            return 2;
        }
        if (strFieldName.equals("PicPathFTP")) {
            return 3;
        }
        if (strFieldName.equals("ServerPort")) {
            return 4;
        }
        if (strFieldName.equals("PicPath")) {
            return 5;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "HostName";
            break;
        case 1:
            strFieldName = "ItemType";
            break;
        case 2:
            strFieldName = "ServerIP";
            break;
        case 3:
            strFieldName = "PicPathFTP";
            break;
        case 4:
            strFieldName = "ServerPort";
            break;
        case 5:
            strFieldName = "PicPath";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("HostName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ItemType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServerIP")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PicPathFTP")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServerPort")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PicPath")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
