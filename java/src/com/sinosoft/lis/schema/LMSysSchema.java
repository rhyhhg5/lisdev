/*
 * <p>ClassName: LMSysSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 险种定义
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LMSysDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LMSysSchema implements Schema
{
    // @Field
    /** 公司名称 */
    private String CompanyName;
    /** 主机类型 */
    private String HostType;
    /** 通用密码 */
    private String GlobPassword;
    /** 预定利率 */
    private double PrepInterest;
    /** 实际利率 */
    private double RealInterest;
    /** 数据集中标记 */
    private String DataCentFlag;
    /** 错误处理 */
    private String DealError;
    /** 双岗录入 */
    private String MultInpFlag;
    /** 重新读入单证格式 */
    private String ReReadCardForm;
    /** 到账确认标记 */
    private String EnterAccConfFlag;
    /** 投保时缴费校验标记 */
    private String AppPayCheckFlag;
    /** 签单时缴费校验标记 */
    private String SignPayCheckFlag;
    /** 给付通知书号 */
    private String GetNoticeNoFlag;
    /** 客户批改标记 */
    private String CustEdorFlag;
    /** 催收控制 */
    private String UrgePayCtl;

    public static final int FIELDNUM = 15; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LMSysSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[0];

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getCompanyName()
    {
        if (CompanyName != null && !CompanyName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CompanyName = StrTool.unicodeToGBK(CompanyName);
        }
        return CompanyName;
    }

    public void setCompanyName(String aCompanyName)
    {
        CompanyName = aCompanyName;
    }

    public String getHostType()
    {
        if (HostType != null && !HostType.equals("") && SysConst.CHANGECHARSET == true)
        {
            HostType = StrTool.unicodeToGBK(HostType);
        }
        return HostType;
    }

    public void setHostType(String aHostType)
    {
        HostType = aHostType;
    }

    public String getGlobPassword()
    {
        if (GlobPassword != null && !GlobPassword.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GlobPassword = StrTool.unicodeToGBK(GlobPassword);
        }
        return GlobPassword;
    }

    public void setGlobPassword(String aGlobPassword)
    {
        GlobPassword = aGlobPassword;
    }

    public double getPrepInterest()
    {
        return PrepInterest;
    }

    public void setPrepInterest(double aPrepInterest)
    {
        PrepInterest = aPrepInterest;
    }

    public void setPrepInterest(String aPrepInterest)
    {
        if (aPrepInterest != null && !aPrepInterest.equals(""))
        {
            Double tDouble = new Double(aPrepInterest);
            double d = tDouble.doubleValue();
            PrepInterest = d;
        }
    }

    public double getRealInterest()
    {
        return RealInterest;
    }

    public void setRealInterest(double aRealInterest)
    {
        RealInterest = aRealInterest;
    }

    public void setRealInterest(String aRealInterest)
    {
        if (aRealInterest != null && !aRealInterest.equals(""))
        {
            Double tDouble = new Double(aRealInterest);
            double d = tDouble.doubleValue();
            RealInterest = d;
        }
    }

    public String getDataCentFlag()
    {
        if (DataCentFlag != null && !DataCentFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            DataCentFlag = StrTool.unicodeToGBK(DataCentFlag);
        }
        return DataCentFlag;
    }

    public void setDataCentFlag(String aDataCentFlag)
    {
        DataCentFlag = aDataCentFlag;
    }

    public String getDealError()
    {
        if (DealError != null && !DealError.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            DealError = StrTool.unicodeToGBK(DealError);
        }
        return DealError;
    }

    public void setDealError(String aDealError)
    {
        DealError = aDealError;
    }

    public String getMultInpFlag()
    {
        if (MultInpFlag != null && !MultInpFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            MultInpFlag = StrTool.unicodeToGBK(MultInpFlag);
        }
        return MultInpFlag;
    }

    public void setMultInpFlag(String aMultInpFlag)
    {
        MultInpFlag = aMultInpFlag;
    }

    public String getReReadCardForm()
    {
        if (ReReadCardForm != null && !ReReadCardForm.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ReReadCardForm = StrTool.unicodeToGBK(ReReadCardForm);
        }
        return ReReadCardForm;
    }

    public void setReReadCardForm(String aReReadCardForm)
    {
        ReReadCardForm = aReReadCardForm;
    }

    public String getEnterAccConfFlag()
    {
        if (EnterAccConfFlag != null && !EnterAccConfFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            EnterAccConfFlag = StrTool.unicodeToGBK(EnterAccConfFlag);
        }
        return EnterAccConfFlag;
    }

    public void setEnterAccConfFlag(String aEnterAccConfFlag)
    {
        EnterAccConfFlag = aEnterAccConfFlag;
    }

    public String getAppPayCheckFlag()
    {
        if (AppPayCheckFlag != null && !AppPayCheckFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AppPayCheckFlag = StrTool.unicodeToGBK(AppPayCheckFlag);
        }
        return AppPayCheckFlag;
    }

    public void setAppPayCheckFlag(String aAppPayCheckFlag)
    {
        AppPayCheckFlag = aAppPayCheckFlag;
    }

    public String getSignPayCheckFlag()
    {
        if (SignPayCheckFlag != null && !SignPayCheckFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            SignPayCheckFlag = StrTool.unicodeToGBK(SignPayCheckFlag);
        }
        return SignPayCheckFlag;
    }

    public void setSignPayCheckFlag(String aSignPayCheckFlag)
    {
        SignPayCheckFlag = aSignPayCheckFlag;
    }

    public String getGetNoticeNoFlag()
    {
        if (GetNoticeNoFlag != null && !GetNoticeNoFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GetNoticeNoFlag = StrTool.unicodeToGBK(GetNoticeNoFlag);
        }
        return GetNoticeNoFlag;
    }

    public void setGetNoticeNoFlag(String aGetNoticeNoFlag)
    {
        GetNoticeNoFlag = aGetNoticeNoFlag;
    }

    public String getCustEdorFlag()
    {
        if (CustEdorFlag != null && !CustEdorFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CustEdorFlag = StrTool.unicodeToGBK(CustEdorFlag);
        }
        return CustEdorFlag;
    }

    public void setCustEdorFlag(String aCustEdorFlag)
    {
        CustEdorFlag = aCustEdorFlag;
    }

    public String getUrgePayCtl()
    {
        if (UrgePayCtl != null && !UrgePayCtl.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            UrgePayCtl = StrTool.unicodeToGBK(UrgePayCtl);
        }
        return UrgePayCtl;
    }

    public void setUrgePayCtl(String aUrgePayCtl)
    {
        UrgePayCtl = aUrgePayCtl;
    }

    /**
     * 使用另外一个 LMSysSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LMSysSchema aLMSysSchema)
    {
        this.CompanyName = aLMSysSchema.getCompanyName();
        this.HostType = aLMSysSchema.getHostType();
        this.GlobPassword = aLMSysSchema.getGlobPassword();
        this.PrepInterest = aLMSysSchema.getPrepInterest();
        this.RealInterest = aLMSysSchema.getRealInterest();
        this.DataCentFlag = aLMSysSchema.getDataCentFlag();
        this.DealError = aLMSysSchema.getDealError();
        this.MultInpFlag = aLMSysSchema.getMultInpFlag();
        this.ReReadCardForm = aLMSysSchema.getReReadCardForm();
        this.EnterAccConfFlag = aLMSysSchema.getEnterAccConfFlag();
        this.AppPayCheckFlag = aLMSysSchema.getAppPayCheckFlag();
        this.SignPayCheckFlag = aLMSysSchema.getSignPayCheckFlag();
        this.GetNoticeNoFlag = aLMSysSchema.getGetNoticeNoFlag();
        this.CustEdorFlag = aLMSysSchema.getCustEdorFlag();
        this.UrgePayCtl = aLMSysSchema.getUrgePayCtl();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("CompanyName") == null)
            {
                this.CompanyName = null;
            }
            else
            {
                this.CompanyName = rs.getString("CompanyName").trim();
            }

            if (rs.getString("HostType") == null)
            {
                this.HostType = null;
            }
            else
            {
                this.HostType = rs.getString("HostType").trim();
            }

            if (rs.getString("GlobPassword") == null)
            {
                this.GlobPassword = null;
            }
            else
            {
                this.GlobPassword = rs.getString("GlobPassword").trim();
            }

            this.PrepInterest = rs.getDouble("PrepInterest");
            this.RealInterest = rs.getDouble("RealInterest");
            if (rs.getString("DataCentFlag") == null)
            {
                this.DataCentFlag = null;
            }
            else
            {
                this.DataCentFlag = rs.getString("DataCentFlag").trim();
            }

            if (rs.getString("DealError") == null)
            {
                this.DealError = null;
            }
            else
            {
                this.DealError = rs.getString("DealError").trim();
            }

            if (rs.getString("MultInpFlag") == null)
            {
                this.MultInpFlag = null;
            }
            else
            {
                this.MultInpFlag = rs.getString("MultInpFlag").trim();
            }

            if (rs.getString("ReReadCardForm") == null)
            {
                this.ReReadCardForm = null;
            }
            else
            {
                this.ReReadCardForm = rs.getString("ReReadCardForm").trim();
            }

            if (rs.getString("EnterAccConfFlag") == null)
            {
                this.EnterAccConfFlag = null;
            }
            else
            {
                this.EnterAccConfFlag = rs.getString("EnterAccConfFlag").trim();
            }

            if (rs.getString("AppPayCheckFlag") == null)
            {
                this.AppPayCheckFlag = null;
            }
            else
            {
                this.AppPayCheckFlag = rs.getString("AppPayCheckFlag").trim();
            }

            if (rs.getString("SignPayCheckFlag") == null)
            {
                this.SignPayCheckFlag = null;
            }
            else
            {
                this.SignPayCheckFlag = rs.getString("SignPayCheckFlag").trim();
            }

            if (rs.getString("GetNoticeNoFlag") == null)
            {
                this.GetNoticeNoFlag = null;
            }
            else
            {
                this.GetNoticeNoFlag = rs.getString("GetNoticeNoFlag").trim();
            }

            if (rs.getString("CustEdorFlag") == null)
            {
                this.CustEdorFlag = null;
            }
            else
            {
                this.CustEdorFlag = rs.getString("CustEdorFlag").trim();
            }

            if (rs.getString("UrgePayCtl") == null)
            {
                this.UrgePayCtl = null;
            }
            else
            {
                this.UrgePayCtl = rs.getString("UrgePayCtl").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMSysSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LMSysSchema getSchema()
    {
        LMSysSchema aLMSysSchema = new LMSysSchema();
        aLMSysSchema.setSchema(this);
        return aLMSysSchema;
    }

    public LMSysDB getDB()
    {
        LMSysDB aDBOper = new LMSysDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMSys描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(CompanyName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(HostType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GlobPassword)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(PrepInterest) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(RealInterest) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(DataCentFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(DealError)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MultInpFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ReReadCardForm)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(EnterAccConfFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AppPayCheckFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SignPayCheckFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GetNoticeNoFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CustEdorFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(UrgePayCtl));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMSys>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            CompanyName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                         SysConst.PACKAGESPILTER);
            HostType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            GlobPassword = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                          SysConst.PACKAGESPILTER);
            PrepInterest = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 4, SysConst.PACKAGESPILTER))).doubleValue();
            RealInterest = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 5, SysConst.PACKAGESPILTER))).doubleValue();
            DataCentFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                          SysConst.PACKAGESPILTER);
            DealError = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                       SysConst.PACKAGESPILTER);
            MultInpFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                         SysConst.PACKAGESPILTER);
            ReReadCardForm = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                            SysConst.PACKAGESPILTER);
            EnterAccConfFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                              10, SysConst.PACKAGESPILTER);
            AppPayCheckFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             11, SysConst.PACKAGESPILTER);
            SignPayCheckFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                              12, SysConst.PACKAGESPILTER);
            GetNoticeNoFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             13, SysConst.PACKAGESPILTER);
            CustEdorFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                          SysConst.PACKAGESPILTER);
            UrgePayCtl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMSysSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("CompanyName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CompanyName));
        }
        if (FCode.equals("HostType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(HostType));
        }
        if (FCode.equals("GlobPassword"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GlobPassword));
        }
        if (FCode.equals("PrepInterest"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PrepInterest));
        }
        if (FCode.equals("RealInterest"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RealInterest));
        }
        if (FCode.equals("DataCentFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DataCentFlag));
        }
        if (FCode.equals("DealError"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DealError));
        }
        if (FCode.equals("MultInpFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MultInpFlag));
        }
        if (FCode.equals("ReReadCardForm"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ReReadCardForm));
        }
        if (FCode.equals("EnterAccConfFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(EnterAccConfFlag));
        }
        if (FCode.equals("AppPayCheckFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AppPayCheckFlag));
        }
        if (FCode.equals("SignPayCheckFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SignPayCheckFlag));
        }
        if (FCode.equals("GetNoticeNoFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetNoticeNoFlag));
        }
        if (FCode.equals("CustEdorFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CustEdorFlag));
        }
        if (FCode.equals("UrgePayCtl"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(UrgePayCtl));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(CompanyName);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(HostType);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(GlobPassword);
                break;
            case 3:
                strFieldValue = String.valueOf(PrepInterest);
                break;
            case 4:
                strFieldValue = String.valueOf(RealInterest);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(DataCentFlag);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(DealError);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(MultInpFlag);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(ReReadCardForm);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(EnterAccConfFlag);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(AppPayCheckFlag);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(SignPayCheckFlag);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(GetNoticeNoFlag);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(CustEdorFlag);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(UrgePayCtl);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("CompanyName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CompanyName = FValue.trim();
            }
            else
            {
                CompanyName = null;
            }
        }
        if (FCode.equals("HostType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                HostType = FValue.trim();
            }
            else
            {
                HostType = null;
            }
        }
        if (FCode.equals("GlobPassword"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GlobPassword = FValue.trim();
            }
            else
            {
                GlobPassword = null;
            }
        }
        if (FCode.equals("PrepInterest"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                PrepInterest = d;
            }
        }
        if (FCode.equals("RealInterest"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                RealInterest = d;
            }
        }
        if (FCode.equals("DataCentFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DataCentFlag = FValue.trim();
            }
            else
            {
                DataCentFlag = null;
            }
        }
        if (FCode.equals("DealError"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DealError = FValue.trim();
            }
            else
            {
                DealError = null;
            }
        }
        if (FCode.equals("MultInpFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MultInpFlag = FValue.trim();
            }
            else
            {
                MultInpFlag = null;
            }
        }
        if (FCode.equals("ReReadCardForm"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ReReadCardForm = FValue.trim();
            }
            else
            {
                ReReadCardForm = null;
            }
        }
        if (FCode.equals("EnterAccConfFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EnterAccConfFlag = FValue.trim();
            }
            else
            {
                EnterAccConfFlag = null;
            }
        }
        if (FCode.equals("AppPayCheckFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppPayCheckFlag = FValue.trim();
            }
            else
            {
                AppPayCheckFlag = null;
            }
        }
        if (FCode.equals("SignPayCheckFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SignPayCheckFlag = FValue.trim();
            }
            else
            {
                SignPayCheckFlag = null;
            }
        }
        if (FCode.equals("GetNoticeNoFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetNoticeNoFlag = FValue.trim();
            }
            else
            {
                GetNoticeNoFlag = null;
            }
        }
        if (FCode.equals("CustEdorFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CustEdorFlag = FValue.trim();
            }
            else
            {
                CustEdorFlag = null;
            }
        }
        if (FCode.equals("UrgePayCtl"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UrgePayCtl = FValue.trim();
            }
            else
            {
                UrgePayCtl = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LMSysSchema other = (LMSysSchema) otherObject;
        return
                CompanyName.equals(other.getCompanyName())
                && HostType.equals(other.getHostType())
                && GlobPassword.equals(other.getGlobPassword())
                && PrepInterest == other.getPrepInterest()
                && RealInterest == other.getRealInterest()
                && DataCentFlag.equals(other.getDataCentFlag())
                && DealError.equals(other.getDealError())
                && MultInpFlag.equals(other.getMultInpFlag())
                && ReReadCardForm.equals(other.getReReadCardForm())
                && EnterAccConfFlag.equals(other.getEnterAccConfFlag())
                && AppPayCheckFlag.equals(other.getAppPayCheckFlag())
                && SignPayCheckFlag.equals(other.getSignPayCheckFlag())
                && GetNoticeNoFlag.equals(other.getGetNoticeNoFlag())
                && CustEdorFlag.equals(other.getCustEdorFlag())
                && UrgePayCtl.equals(other.getUrgePayCtl());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("CompanyName"))
        {
            return 0;
        }
        if (strFieldName.equals("HostType"))
        {
            return 1;
        }
        if (strFieldName.equals("GlobPassword"))
        {
            return 2;
        }
        if (strFieldName.equals("PrepInterest"))
        {
            return 3;
        }
        if (strFieldName.equals("RealInterest"))
        {
            return 4;
        }
        if (strFieldName.equals("DataCentFlag"))
        {
            return 5;
        }
        if (strFieldName.equals("DealError"))
        {
            return 6;
        }
        if (strFieldName.equals("MultInpFlag"))
        {
            return 7;
        }
        if (strFieldName.equals("ReReadCardForm"))
        {
            return 8;
        }
        if (strFieldName.equals("EnterAccConfFlag"))
        {
            return 9;
        }
        if (strFieldName.equals("AppPayCheckFlag"))
        {
            return 10;
        }
        if (strFieldName.equals("SignPayCheckFlag"))
        {
            return 11;
        }
        if (strFieldName.equals("GetNoticeNoFlag"))
        {
            return 12;
        }
        if (strFieldName.equals("CustEdorFlag"))
        {
            return 13;
        }
        if (strFieldName.equals("UrgePayCtl"))
        {
            return 14;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "CompanyName";
                break;
            case 1:
                strFieldName = "HostType";
                break;
            case 2:
                strFieldName = "GlobPassword";
                break;
            case 3:
                strFieldName = "PrepInterest";
                break;
            case 4:
                strFieldName = "RealInterest";
                break;
            case 5:
                strFieldName = "DataCentFlag";
                break;
            case 6:
                strFieldName = "DealError";
                break;
            case 7:
                strFieldName = "MultInpFlag";
                break;
            case 8:
                strFieldName = "ReReadCardForm";
                break;
            case 9:
                strFieldName = "EnterAccConfFlag";
                break;
            case 10:
                strFieldName = "AppPayCheckFlag";
                break;
            case 11:
                strFieldName = "SignPayCheckFlag";
                break;
            case 12:
                strFieldName = "GetNoticeNoFlag";
                break;
            case 13:
                strFieldName = "CustEdorFlag";
                break;
            case 14:
                strFieldName = "UrgePayCtl";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("CompanyName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("HostType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GlobPassword"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PrepInterest"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("RealInterest"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("DataCentFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DealError"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MultInpFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ReReadCardForm"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EnterAccConfFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppPayCheckFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SignPayCheckFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetNoticeNoFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CustEdorFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UrgePayCtl"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 4:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
