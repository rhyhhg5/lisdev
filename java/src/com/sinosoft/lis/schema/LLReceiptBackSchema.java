/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LLReceiptBackDB;

/*
 * <p>ClassName: LLReceiptBackSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2007-03-15
 */
public class LLReceiptBackSchema implements Schema, Cloneable {
    // @Field
    /** 回退序号 */
    private String CaseBackNo;
    /** 账单号 */
    private String MainFeeNo;
    /** 分案号(个人理赔号) */
    private String CaseNo;
    /** 立案号(申请登记号) */
    private String RgtNo;
    /** 回退前状态 */
    private String BeforState;
    /** 回退前录入人员 */
    private String OInputer;
    /** 修改人 */
    private String NInputer;
    /** 回退原因 */
    private String Reason;
    /** 备注 */
    private String Remark;
    /** 管理机构 */
    private String MngCom;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 15; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LLReceiptBackSchema() {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "CaseBackNo";
        pk[1] = "MainFeeNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LLReceiptBackSchema cloned = (LLReceiptBackSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getCaseBackNo() {
        return CaseBackNo;
    }

    public void setCaseBackNo(String aCaseBackNo) {
        CaseBackNo = aCaseBackNo;
    }

    public String getMainFeeNo() {
        return MainFeeNo;
    }

    public void setMainFeeNo(String aMainFeeNo) {
        MainFeeNo = aMainFeeNo;
    }

    public String getCaseNo() {
        return CaseNo;
    }

    public void setCaseNo(String aCaseNo) {
        CaseNo = aCaseNo;
    }

    public String getRgtNo() {
        return RgtNo;
    }

    public void setRgtNo(String aRgtNo) {
        RgtNo = aRgtNo;
    }

    public String getBeforState() {
        return BeforState;
    }

    public void setBeforState(String aBeforState) {
        BeforState = aBeforState;
    }

    public String getOInputer() {
        return OInputer;
    }

    public void setOInputer(String aOInputer) {
        OInputer = aOInputer;
    }

    public String getNInputer() {
        return NInputer;
    }

    public void setNInputer(String aNInputer) {
        NInputer = aNInputer;
    }

    public String getReason() {
        return Reason;
    }

    public void setReason(String aReason) {
        Reason = aReason;
    }

    public String getRemark() {
        return Remark;
    }

    public void setRemark(String aRemark) {
        Remark = aRemark;
    }

    public String getMngCom() {
        return MngCom;
    }

    public void setMngCom(String aMngCom) {
        MngCom = aMngCom;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LLReceiptBackSchema 对象给 Schema 赋值
     * @param: aLLReceiptBackSchema LLReceiptBackSchema
     **/
    public void setSchema(LLReceiptBackSchema aLLReceiptBackSchema) {
        this.CaseBackNo = aLLReceiptBackSchema.getCaseBackNo();
        this.MainFeeNo = aLLReceiptBackSchema.getMainFeeNo();
        this.CaseNo = aLLReceiptBackSchema.getCaseNo();
        this.RgtNo = aLLReceiptBackSchema.getRgtNo();
        this.BeforState = aLLReceiptBackSchema.getBeforState();
        this.OInputer = aLLReceiptBackSchema.getOInputer();
        this.NInputer = aLLReceiptBackSchema.getNInputer();
        this.Reason = aLLReceiptBackSchema.getReason();
        this.Remark = aLLReceiptBackSchema.getRemark();
        this.MngCom = aLLReceiptBackSchema.getMngCom();
        this.Operator = aLLReceiptBackSchema.getOperator();
        this.MakeDate = fDate.getDate(aLLReceiptBackSchema.getMakeDate());
        this.MakeTime = aLLReceiptBackSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLLReceiptBackSchema.getModifyDate());
        this.ModifyTime = aLLReceiptBackSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("CaseBackNo") == null) {
                this.CaseBackNo = null;
            } else {
                this.CaseBackNo = rs.getString("CaseBackNo").trim();
            }

            if (rs.getString("MainFeeNo") == null) {
                this.MainFeeNo = null;
            } else {
                this.MainFeeNo = rs.getString("MainFeeNo").trim();
            }

            if (rs.getString("CaseNo") == null) {
                this.CaseNo = null;
            } else {
                this.CaseNo = rs.getString("CaseNo").trim();
            }

            if (rs.getString("RgtNo") == null) {
                this.RgtNo = null;
            } else {
                this.RgtNo = rs.getString("RgtNo").trim();
            }

            if (rs.getString("BeforState") == null) {
                this.BeforState = null;
            } else {
                this.BeforState = rs.getString("BeforState").trim();
            }

            if (rs.getString("OInputer") == null) {
                this.OInputer = null;
            } else {
                this.OInputer = rs.getString("OInputer").trim();
            }

            if (rs.getString("NInputer") == null) {
                this.NInputer = null;
            } else {
                this.NInputer = rs.getString("NInputer").trim();
            }

            if (rs.getString("Reason") == null) {
                this.Reason = null;
            } else {
                this.Reason = rs.getString("Reason").trim();
            }

            if (rs.getString("Remark") == null) {
                this.Remark = null;
            } else {
                this.Remark = rs.getString("Remark").trim();
            }

            if (rs.getString("MngCom") == null) {
                this.MngCom = null;
            } else {
                this.MngCom = rs.getString("MngCom").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LLReceiptBack表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLReceiptBackSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LLReceiptBackSchema getSchema() {
        LLReceiptBackSchema aLLReceiptBackSchema = new LLReceiptBackSchema();
        aLLReceiptBackSchema.setSchema(this);
        return aLLReceiptBackSchema;
    }

    public LLReceiptBackDB getDB() {
        LLReceiptBackDB aDBOper = new LLReceiptBackDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLReceiptBack描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(CaseBackNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MainFeeNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CaseNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RgtNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BeforState));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OInputer));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(NInputer));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Reason));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Remark));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MngCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLReceiptBack>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            CaseBackNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                        SysConst.PACKAGESPILTER);
            MainFeeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                       SysConst.PACKAGESPILTER);
            CaseNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                    SysConst.PACKAGESPILTER);
            RgtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                   SysConst.PACKAGESPILTER);
            BeforState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                        SysConst.PACKAGESPILTER);
            OInputer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                      SysConst.PACKAGESPILTER);
            NInputer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                      SysConst.PACKAGESPILTER);
            Reason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                    SysConst.PACKAGESPILTER);
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                    SysConst.PACKAGESPILTER);
            MngCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                    SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 12, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 14, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                        SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLReceiptBackSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("CaseBackNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CaseBackNo));
        }
        if (FCode.equals("MainFeeNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MainFeeNo));
        }
        if (FCode.equals("CaseNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CaseNo));
        }
        if (FCode.equals("RgtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RgtNo));
        }
        if (FCode.equals("BeforState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BeforState));
        }
        if (FCode.equals("OInputer")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OInputer));
        }
        if (FCode.equals("NInputer")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NInputer));
        }
        if (FCode.equals("Reason")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Reason));
        }
        if (FCode.equals("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equals("MngCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MngCom));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(CaseBackNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(MainFeeNo);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(CaseNo);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(RgtNo);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(BeforState);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(OInputer);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(NInputer);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(Reason);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(Remark);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(MngCom);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("CaseBackNo")) {
            if (FValue != null && !FValue.equals("")) {
                CaseBackNo = FValue.trim();
            } else {
                CaseBackNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("MainFeeNo")) {
            if (FValue != null && !FValue.equals("")) {
                MainFeeNo = FValue.trim();
            } else {
                MainFeeNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("CaseNo")) {
            if (FValue != null && !FValue.equals("")) {
                CaseNo = FValue.trim();
            } else {
                CaseNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("RgtNo")) {
            if (FValue != null && !FValue.equals("")) {
                RgtNo = FValue.trim();
            } else {
                RgtNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("BeforState")) {
            if (FValue != null && !FValue.equals("")) {
                BeforState = FValue.trim();
            } else {
                BeforState = null;
            }
        }
        if (FCode.equalsIgnoreCase("OInputer")) {
            if (FValue != null && !FValue.equals("")) {
                OInputer = FValue.trim();
            } else {
                OInputer = null;
            }
        }
        if (FCode.equalsIgnoreCase("NInputer")) {
            if (FValue != null && !FValue.equals("")) {
                NInputer = FValue.trim();
            } else {
                NInputer = null;
            }
        }
        if (FCode.equalsIgnoreCase("Reason")) {
            if (FValue != null && !FValue.equals("")) {
                Reason = FValue.trim();
            } else {
                Reason = null;
            }
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if (FValue != null && !FValue.equals("")) {
                Remark = FValue.trim();
            } else {
                Remark = null;
            }
        }
        if (FCode.equalsIgnoreCase("MngCom")) {
            if (FValue != null && !FValue.equals("")) {
                MngCom = FValue.trim();
            } else {
                MngCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LLReceiptBackSchema other = (LLReceiptBackSchema) otherObject;
        return
                CaseBackNo.equals(other.getCaseBackNo())
                && MainFeeNo.equals(other.getMainFeeNo())
                && CaseNo.equals(other.getCaseNo())
                && RgtNo.equals(other.getRgtNo())
                && BeforState.equals(other.getBeforState())
                && OInputer.equals(other.getOInputer())
                && NInputer.equals(other.getNInputer())
                && Reason.equals(other.getReason())
                && Remark.equals(other.getRemark())
                && MngCom.equals(other.getMngCom())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("CaseBackNo")) {
            return 0;
        }
        if (strFieldName.equals("MainFeeNo")) {
            return 1;
        }
        if (strFieldName.equals("CaseNo")) {
            return 2;
        }
        if (strFieldName.equals("RgtNo")) {
            return 3;
        }
        if (strFieldName.equals("BeforState")) {
            return 4;
        }
        if (strFieldName.equals("OInputer")) {
            return 5;
        }
        if (strFieldName.equals("NInputer")) {
            return 6;
        }
        if (strFieldName.equals("Reason")) {
            return 7;
        }
        if (strFieldName.equals("Remark")) {
            return 8;
        }
        if (strFieldName.equals("MngCom")) {
            return 9;
        }
        if (strFieldName.equals("Operator")) {
            return 10;
        }
        if (strFieldName.equals("MakeDate")) {
            return 11;
        }
        if (strFieldName.equals("MakeTime")) {
            return 12;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 13;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 14;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "CaseBackNo";
            break;
        case 1:
            strFieldName = "MainFeeNo";
            break;
        case 2:
            strFieldName = "CaseNo";
            break;
        case 3:
            strFieldName = "RgtNo";
            break;
        case 4:
            strFieldName = "BeforState";
            break;
        case 5:
            strFieldName = "OInputer";
            break;
        case 6:
            strFieldName = "NInputer";
            break;
        case 7:
            strFieldName = "Reason";
            break;
        case 8:
            strFieldName = "Remark";
            break;
        case 9:
            strFieldName = "MngCom";
            break;
        case 10:
            strFieldName = "Operator";
            break;
        case 11:
            strFieldName = "MakeDate";
            break;
        case 12:
            strFieldName = "MakeTime";
            break;
        case 13:
            strFieldName = "ModifyDate";
            break;
        case 14:
            strFieldName = "ModifyTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("CaseBackNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MainFeeNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CaseNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RgtNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BeforState")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OInputer")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("NInputer")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Reason")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Remark")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MngCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 12:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 13:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 14:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
