/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LAContFYCRateBDB;

/*
 * <p>ClassName: LAContFYCRateBSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 新表结构
 * @CreateDate：2006-03-13
 */
public class LAContFYCRateBSchema implements Schema, Cloneable
{
	// @Field
	/** 批改号 */
	private String EdorNo;
	/** 团体保单号 */
	private String GrpContNo;
	/** 险种代码 */
	private String RiskCode;
	/** 提奖比例 */
	private double Rate;
	/** 提奖金额 */
	private double Money;
	/** 提奖计算标志 */
	private String Flag;
	/** 原操作员 */
	private String OriOperator;
	/** 原入机日期 */
	private Date OriMakeDate;
	/** 原入机时间 */
	private String OriMakeTime;
	/** 原最后一次修改日期 */
	private Date OriModifyDate;
	/** 原最后一次修改时间 */
	private String OriModifyTime;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最近一次修改日期 */
	private Date ModifyDate;
	/** 最近一次修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 16;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LAContFYCRateBSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[3];
		pk[0] = "EdorNo";
		pk[1] = "GrpContNo";
		pk[2] = "RiskCode";

		PK = pk;
	}

            /**
             * Schema克隆
             * @return Object
             * @throws CloneNotSupportedException
             */
            public Object clone()
                    throws CloneNotSupportedException
            {
                LAContFYCRateBSchema cloned = (LAContFYCRateBSchema)super.clone();
                cloned.fDate = (FDate) fDate.clone();
                cloned.mErrors = (CErrors) mErrors.clone();
                return cloned;
            }

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getEdorNo()
	{
		return EdorNo;
	}
	public void setEdorNo(String aEdorNo)
	{
            EdorNo = aEdorNo;
	}
	public String getGrpContNo()
	{
		return GrpContNo;
	}
	public void setGrpContNo(String aGrpContNo)
	{
            GrpContNo = aGrpContNo;
	}
	public String getRiskCode()
	{
		return RiskCode;
	}
	public void setRiskCode(String aRiskCode)
	{
            RiskCode = aRiskCode;
	}
	public double getRate()
	{
		return Rate;
	}
	public void setRate(double aRate)
	{
            Rate = Arith.round(aRate,6);
	}
	public void setRate(String aRate)
	{
		if (aRate != null && !aRate.equals(""))
		{
			Double tDouble = new Double(aRate);
			double d = tDouble.doubleValue();
                Rate = Arith.round(d,6);
		}
	}

	public double getMoney()
	{
		return Money;
	}
	public void setMoney(double aMoney)
	{
            Money = Arith.round(aMoney,2);
	}
	public void setMoney(String aMoney)
	{
		if (aMoney != null && !aMoney.equals(""))
		{
			Double tDouble = new Double(aMoney);
			double d = tDouble.doubleValue();
                Money = Arith.round(d,2);
		}
	}

	public String getFlag()
	{
		return Flag;
	}
	public void setFlag(String aFlag)
	{
            Flag = aFlag;
	}
	public String getOriOperator()
	{
		return OriOperator;
	}
	public void setOriOperator(String aOriOperator)
	{
            OriOperator = aOriOperator;
	}
	public String getOriMakeDate()
	{
		if( OriMakeDate != null )
			return fDate.getString(OriMakeDate);
		else
			return null;
	}
	public void setOriMakeDate(Date aOriMakeDate)
	{
            OriMakeDate = aOriMakeDate;
	}
	public void setOriMakeDate(String aOriMakeDate)
	{
		if (aOriMakeDate != null && !aOriMakeDate.equals("") )
		{
			OriMakeDate = fDate.getDate( aOriMakeDate );
		}
		else
			OriMakeDate = null;
	}

	public String getOriMakeTime()
	{
		return OriMakeTime;
	}
	public void setOriMakeTime(String aOriMakeTime)
	{
            OriMakeTime = aOriMakeTime;
	}
	public String getOriModifyDate()
	{
		if( OriModifyDate != null )
			return fDate.getString(OriModifyDate);
		else
			return null;
	}
	public void setOriModifyDate(Date aOriModifyDate)
	{
            OriModifyDate = aOriModifyDate;
	}
	public void setOriModifyDate(String aOriModifyDate)
	{
		if (aOriModifyDate != null && !aOriModifyDate.equals("") )
		{
			OriModifyDate = fDate.getDate( aOriModifyDate );
		}
		else
			OriModifyDate = null;
	}

	public String getOriModifyTime()
	{
		return OriModifyTime;
	}
	public void setOriModifyTime(String aOriModifyTime)
	{
            OriModifyTime = aOriModifyTime;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
            Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
            MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
            MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
            ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
            ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LAContFYCRateBSchema 对象给 Schema 赋值
	* @param: aLAContFYCRateBSchema LAContFYCRateBSchema
	**/
	public void setSchema(LAContFYCRateBSchema aLAContFYCRateBSchema)
	{
		this.EdorNo = aLAContFYCRateBSchema.getEdorNo();
		this.GrpContNo = aLAContFYCRateBSchema.getGrpContNo();
		this.RiskCode = aLAContFYCRateBSchema.getRiskCode();
		this.Rate = aLAContFYCRateBSchema.getRate();
		this.Money = aLAContFYCRateBSchema.getMoney();
		this.Flag = aLAContFYCRateBSchema.getFlag();
		this.OriOperator = aLAContFYCRateBSchema.getOriOperator();
		this.OriMakeDate = fDate.getDate( aLAContFYCRateBSchema.getOriMakeDate());
		this.OriMakeTime = aLAContFYCRateBSchema.getOriMakeTime();
		this.OriModifyDate = fDate.getDate( aLAContFYCRateBSchema.getOriModifyDate());
		this.OriModifyTime = aLAContFYCRateBSchema.getOriModifyTime();
		this.Operator = aLAContFYCRateBSchema.getOperator();
		this.MakeDate = fDate.getDate( aLAContFYCRateBSchema.getMakeDate());
		this.MakeTime = aLAContFYCRateBSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLAContFYCRateBSchema.getModifyDate());
		this.ModifyTime = aLAContFYCRateBSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("EdorNo") == null )
				this.EdorNo = null;
			else
				this.EdorNo = rs.getString("EdorNo").trim();

			if( rs.getString("GrpContNo") == null )
				this.GrpContNo = null;
			else
				this.GrpContNo = rs.getString("GrpContNo").trim();

			if( rs.getString("RiskCode") == null )
				this.RiskCode = null;
			else
				this.RiskCode = rs.getString("RiskCode").trim();

			this.Rate = rs.getDouble("Rate");
			this.Money = rs.getDouble("Money");
			if( rs.getString("Flag") == null )
				this.Flag = null;
			else
				this.Flag = rs.getString("Flag").trim();

			if( rs.getString("OriOperator") == null )
				this.OriOperator = null;
			else
				this.OriOperator = rs.getString("OriOperator").trim();

			this.OriMakeDate = rs.getDate("OriMakeDate");
			if( rs.getString("OriMakeTime") == null )
				this.OriMakeTime = null;
			else
				this.OriMakeTime = rs.getString("OriMakeTime").trim();

			this.OriModifyDate = rs.getDate("OriModifyDate");
			if( rs.getString("OriModifyTime") == null )
				this.OriModifyTime = null;
			else
				this.OriModifyTime = rs.getString("OriModifyTime").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LAContFYCRateB表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAContFYCRateBSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LAContFYCRateBSchema getSchema()
	{
		LAContFYCRateBSchema aLAContFYCRateBSchema = new LAContFYCRateBSchema();
		aLAContFYCRateBSchema.setSchema(this);
		return aLAContFYCRateBSchema;
	}

	public LAContFYCRateBDB getDB()
	{
		LAContFYCRateBDB aDBOper = new LAContFYCRateBDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAContFYCRateB描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append(StrTool.cTrim(EdorNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(Rate));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(Money));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Flag)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(OriOperator)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( OriMakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(OriMakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( OriModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(OriModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAContFYCRateB>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			EdorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			Rate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,4,SysConst.PACKAGESPILTER))).doubleValue();
			Money = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,5,SysConst.PACKAGESPILTER))).doubleValue();
			Flag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			OriOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			OriMakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,SysConst.PACKAGESPILTER));
			OriMakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			OriModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,SysConst.PACKAGESPILTER));
			OriModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAContFYCRateBSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("EdorNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EdorNo));
		}
		if (FCode.equals("GrpContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
		}
		if (FCode.equals("RiskCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
		}
		if (FCode.equals("Rate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Rate));
		}
		if (FCode.equals("Money"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Money));
		}
		if (FCode.equals("Flag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Flag));
		}
		if (FCode.equals("OriOperator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OriOperator));
		}
		if (FCode.equals("OriMakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getOriMakeDate()));
		}
		if (FCode.equals("OriMakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OriMakeTime));
		}
		if (FCode.equals("OriModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getOriModifyDate()));
		}
		if (FCode.equals("OriModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OriModifyTime));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(EdorNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(GrpContNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(RiskCode);
				break;
			case 3:
				strFieldValue = String.valueOf(Rate);
				break;
			case 4:
				strFieldValue = String.valueOf(Money);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(Flag);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(OriOperator);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getOriMakeDate()));
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(OriMakeTime);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getOriModifyDate()));
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(OriModifyTime);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("EdorNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EdorNo = FValue.trim();
			}
			else
				EdorNo = null;
		}
		if (FCode.equalsIgnoreCase("GrpContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpContNo = FValue.trim();
			}
			else
				GrpContNo = null;
		}
		if (FCode.equalsIgnoreCase("RiskCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskCode = FValue.trim();
			}
			else
				RiskCode = null;
		}
		if (FCode.equalsIgnoreCase("Rate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Rate = d;
			}
		}
		if (FCode.equalsIgnoreCase("Money"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Money = d;
			}
		}
		if (FCode.equalsIgnoreCase("Flag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Flag = FValue.trim();
			}
			else
				Flag = null;
		}
		if (FCode.equalsIgnoreCase("OriOperator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OriOperator = FValue.trim();
			}
			else
				OriOperator = null;
		}
		if (FCode.equalsIgnoreCase("OriMakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				OriMakeDate = fDate.getDate( FValue );
			}
			else
				OriMakeDate = null;
		}
		if (FCode.equalsIgnoreCase("OriMakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OriMakeTime = FValue.trim();
			}
			else
				OriMakeTime = null;
		}
		if (FCode.equalsIgnoreCase("OriModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				OriModifyDate = fDate.getDate( FValue );
			}
			else
				OriModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("OriModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OriModifyTime = FValue.trim();
			}
			else
				OriModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LAContFYCRateBSchema other = (LAContFYCRateBSchema)otherObject;
		return
			EdorNo.equals(other.getEdorNo())
			&& GrpContNo.equals(other.getGrpContNo())
			&& RiskCode.equals(other.getRiskCode())
			&& Rate == other.getRate()
			&& Money == other.getMoney()
			&& Flag.equals(other.getFlag())
			&& OriOperator.equals(other.getOriOperator())
			&& fDate.getString(OriMakeDate).equals(other.getOriMakeDate())
			&& OriMakeTime.equals(other.getOriMakeTime())
			&& fDate.getString(OriModifyDate).equals(other.getOriModifyDate())
			&& OriModifyTime.equals(other.getOriModifyTime())
			&& Operator.equals(other.getOperator())
			&& fDate.getString(MakeDate).equals(other.getMakeDate())
			&& MakeTime.equals(other.getMakeTime())
			&& fDate.getString(ModifyDate).equals(other.getModifyDate())
			&& ModifyTime.equals(other.getModifyTime());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("EdorNo") ) {
			return 0;
		}
		if( strFieldName.equals("GrpContNo") ) {
			return 1;
		}
		if( strFieldName.equals("RiskCode") ) {
			return 2;
		}
		if( strFieldName.equals("Rate") ) {
			return 3;
		}
		if( strFieldName.equals("Money") ) {
			return 4;
		}
		if( strFieldName.equals("Flag") ) {
			return 5;
		}
		if( strFieldName.equals("OriOperator") ) {
			return 6;
		}
		if( strFieldName.equals("OriMakeDate") ) {
			return 7;
		}
		if( strFieldName.equals("OriMakeTime") ) {
			return 8;
		}
		if( strFieldName.equals("OriModifyDate") ) {
			return 9;
		}
		if( strFieldName.equals("OriModifyTime") ) {
			return 10;
		}
		if( strFieldName.equals("Operator") ) {
			return 11;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 12;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 13;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 14;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 15;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "EdorNo";
				break;
			case 1:
				strFieldName = "GrpContNo";
				break;
			case 2:
				strFieldName = "RiskCode";
				break;
			case 3:
				strFieldName = "Rate";
				break;
			case 4:
				strFieldName = "Money";
				break;
			case 5:
				strFieldName = "Flag";
				break;
			case 6:
				strFieldName = "OriOperator";
				break;
			case 7:
				strFieldName = "OriMakeDate";
				break;
			case 8:
				strFieldName = "OriMakeTime";
				break;
			case 9:
				strFieldName = "OriModifyDate";
				break;
			case 10:
				strFieldName = "OriModifyTime";
				break;
			case 11:
				strFieldName = "Operator";
				break;
			case 12:
				strFieldName = "MakeDate";
				break;
			case 13:
				strFieldName = "MakeTime";
				break;
			case 14:
				strFieldName = "ModifyDate";
				break;
			case 15:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("EdorNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Rate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Money") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Flag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OriOperator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OriMakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("OriMakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OriModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("OriModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 4:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
