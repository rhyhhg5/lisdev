/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.BPOServerInfoDB;

/*
 * <p>ClassName: BPOServerInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 外包录入
 * @CreateDate：2007-09-24
 */
public class BPOServerInfoSchema implements Schema, Cloneable
{
	// @Field
	/** 外包商标识 */
	private String BPOID;
	/** 服务器地址 */
	private String ServerIP;
	/** 服务器端口 */
	private String ServerPort;
	/** 目录绝对路径 */
	private String BasePath;
	/** 目录相对路径 */
	private String FilePath;
	/** 登录用户名 */
	private String LogInUser;
	/** 登录密码 */
	private String LogInPwd;
	/** 核心系统扫描件备份相对路径 */
	private String BackupBasePath;
	/** 前置机外包回传xml存放相对路径 */
	private String BackBasePath;
	/** 核心系统外包回传xml备份相对路径 */
	private String BackBackupBasePath;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 15;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public BPOServerInfoSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "BPOID";

		PK = pk;
	}

            /**
             * Schema克隆
             * @return Object
             * @throws CloneNotSupportedException
             */
            public Object clone()
                    throws CloneNotSupportedException
            {
                BPOServerInfoSchema cloned = (BPOServerInfoSchema)super.clone();
                cloned.fDate = (FDate) fDate.clone();
                cloned.mErrors = (CErrors) mErrors.clone();
                return cloned;
            }

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getBPOID()
	{
		return BPOID;
	}
	public void setBPOID(String aBPOID)
	{
            BPOID = aBPOID;
	}
	public String getServerIP()
	{
		return ServerIP;
	}
	public void setServerIP(String aServerIP)
	{
            ServerIP = aServerIP;
	}
	public String getServerPort()
	{
		return ServerPort;
	}
	public void setServerPort(String aServerPort)
	{
            ServerPort = aServerPort;
	}
	public String getBasePath()
	{
		return BasePath;
	}
	public void setBasePath(String aBasePath)
	{
            BasePath = aBasePath;
	}
	public String getFilePath()
	{
		return FilePath;
	}
	public void setFilePath(String aFilePath)
	{
            FilePath = aFilePath;
	}
	public String getLogInUser()
	{
		return LogInUser;
	}
	public void setLogInUser(String aLogInUser)
	{
            LogInUser = aLogInUser;
	}
	public String getLogInPwd()
	{
		return LogInPwd;
	}
	public void setLogInPwd(String aLogInPwd)
	{
            LogInPwd = aLogInPwd;
	}
	public String getBackupBasePath()
	{
		return BackupBasePath;
	}
	public void setBackupBasePath(String aBackupBasePath)
	{
            BackupBasePath = aBackupBasePath;
	}
	public String getBackBasePath()
	{
		return BackBasePath;
	}
	public void setBackBasePath(String aBackBasePath)
	{
            BackBasePath = aBackBasePath;
	}
	public String getBackBackupBasePath()
	{
		return BackBackupBasePath;
	}
	public void setBackBackupBasePath(String aBackBackupBasePath)
	{
            BackBackupBasePath = aBackBackupBasePath;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
            Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
            MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
            MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
            ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
            ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 BPOServerInfoSchema 对象给 Schema 赋值
	* @param: aBPOServerInfoSchema BPOServerInfoSchema
	**/
	public void setSchema(BPOServerInfoSchema aBPOServerInfoSchema)
	{
		this.BPOID = aBPOServerInfoSchema.getBPOID();
		this.ServerIP = aBPOServerInfoSchema.getServerIP();
		this.ServerPort = aBPOServerInfoSchema.getServerPort();
		this.BasePath = aBPOServerInfoSchema.getBasePath();
		this.FilePath = aBPOServerInfoSchema.getFilePath();
		this.LogInUser = aBPOServerInfoSchema.getLogInUser();
		this.LogInPwd = aBPOServerInfoSchema.getLogInPwd();
		this.BackupBasePath = aBPOServerInfoSchema.getBackupBasePath();
		this.BackBasePath = aBPOServerInfoSchema.getBackBasePath();
		this.BackBackupBasePath = aBPOServerInfoSchema.getBackBackupBasePath();
		this.Operator = aBPOServerInfoSchema.getOperator();
		this.MakeDate = fDate.getDate( aBPOServerInfoSchema.getMakeDate());
		this.MakeTime = aBPOServerInfoSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aBPOServerInfoSchema.getModifyDate());
		this.ModifyTime = aBPOServerInfoSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("BPOID") == null )
				this.BPOID = null;
			else
				this.BPOID = rs.getString("BPOID").trim();

			if( rs.getString("ServerIP") == null )
				this.ServerIP = null;
			else
				this.ServerIP = rs.getString("ServerIP").trim();

			if( rs.getString("ServerPort") == null )
				this.ServerPort = null;
			else
				this.ServerPort = rs.getString("ServerPort").trim();

			if( rs.getString("BasePath") == null )
				this.BasePath = null;
			else
				this.BasePath = rs.getString("BasePath").trim();

			if( rs.getString("FilePath") == null )
				this.FilePath = null;
			else
				this.FilePath = rs.getString("FilePath").trim();

			if( rs.getString("LogInUser") == null )
				this.LogInUser = null;
			else
				this.LogInUser = rs.getString("LogInUser").trim();

			if( rs.getString("LogInPwd") == null )
				this.LogInPwd = null;
			else
				this.LogInPwd = rs.getString("LogInPwd").trim();

			if( rs.getString("BackupBasePath") == null )
				this.BackupBasePath = null;
			else
				this.BackupBasePath = rs.getString("BackupBasePath").trim();

			if( rs.getString("BackBasePath") == null )
				this.BackBasePath = null;
			else
				this.BackBasePath = rs.getString("BackBasePath").trim();

			if( rs.getString("BackBackupBasePath") == null )
				this.BackBackupBasePath = null;
			else
				this.BackBackupBasePath = rs.getString("BackBackupBasePath").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的BPOServerInfo表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "BPOServerInfoSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public BPOServerInfoSchema getSchema()
	{
		BPOServerInfoSchema aBPOServerInfoSchema = new BPOServerInfoSchema();
		aBPOServerInfoSchema.setSchema(this);
		return aBPOServerInfoSchema;
	}

	public BPOServerInfoDB getDB()
	{
		BPOServerInfoDB aDBOper = new BPOServerInfoDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpBPOServerInfo描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append(StrTool.cTrim(BPOID)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ServerIP)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ServerPort)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(BasePath)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(FilePath)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(LogInUser)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(LogInPwd)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(BackupBasePath)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(BackBasePath)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(BackBackupBasePath)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpBPOServerInfo>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			BPOID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			ServerIP = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			ServerPort = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			BasePath = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			FilePath = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			LogInUser = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			LogInPwd = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			BackupBasePath = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			BackBasePath = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			BackBackupBasePath = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "BPOServerInfoSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("BPOID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BPOID));
		}
		if (FCode.equals("ServerIP"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ServerIP));
		}
		if (FCode.equals("ServerPort"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ServerPort));
		}
		if (FCode.equals("BasePath"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BasePath));
		}
		if (FCode.equals("FilePath"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FilePath));
		}
		if (FCode.equals("LogInUser"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LogInUser));
		}
		if (FCode.equals("LogInPwd"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LogInPwd));
		}
		if (FCode.equals("BackupBasePath"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BackupBasePath));
		}
		if (FCode.equals("BackBasePath"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BackBasePath));
		}
		if (FCode.equals("BackBackupBasePath"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BackBackupBasePath));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(BPOID);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(ServerIP);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(ServerPort);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(BasePath);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(FilePath);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(LogInUser);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(LogInPwd);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(BackupBasePath);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(BackBasePath);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(BackBackupBasePath);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("BPOID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BPOID = FValue.trim();
			}
			else
				BPOID = null;
		}
		if (FCode.equalsIgnoreCase("ServerIP"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ServerIP = FValue.trim();
			}
			else
				ServerIP = null;
		}
		if (FCode.equalsIgnoreCase("ServerPort"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ServerPort = FValue.trim();
			}
			else
				ServerPort = null;
		}
		if (FCode.equalsIgnoreCase("BasePath"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BasePath = FValue.trim();
			}
			else
				BasePath = null;
		}
		if (FCode.equalsIgnoreCase("FilePath"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FilePath = FValue.trim();
			}
			else
				FilePath = null;
		}
		if (FCode.equalsIgnoreCase("LogInUser"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LogInUser = FValue.trim();
			}
			else
				LogInUser = null;
		}
		if (FCode.equalsIgnoreCase("LogInPwd"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LogInPwd = FValue.trim();
			}
			else
				LogInPwd = null;
		}
		if (FCode.equalsIgnoreCase("BackupBasePath"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BackupBasePath = FValue.trim();
			}
			else
				BackupBasePath = null;
		}
		if (FCode.equalsIgnoreCase("BackBasePath"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BackBasePath = FValue.trim();
			}
			else
				BackBasePath = null;
		}
		if (FCode.equalsIgnoreCase("BackBackupBasePath"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BackBackupBasePath = FValue.trim();
			}
			else
				BackBackupBasePath = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		BPOServerInfoSchema other = (BPOServerInfoSchema)otherObject;
		return
			BPOID.equals(other.getBPOID())
			&& ServerIP.equals(other.getServerIP())
			&& ServerPort.equals(other.getServerPort())
			&& BasePath.equals(other.getBasePath())
			&& FilePath.equals(other.getFilePath())
			&& LogInUser.equals(other.getLogInUser())
			&& LogInPwd.equals(other.getLogInPwd())
			&& BackupBasePath.equals(other.getBackupBasePath())
			&& BackBasePath.equals(other.getBackBasePath())
			&& BackBackupBasePath.equals(other.getBackBackupBasePath())
			&& Operator.equals(other.getOperator())
			&& fDate.getString(MakeDate).equals(other.getMakeDate())
			&& MakeTime.equals(other.getMakeTime())
			&& fDate.getString(ModifyDate).equals(other.getModifyDate())
			&& ModifyTime.equals(other.getModifyTime());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("BPOID") ) {
			return 0;
		}
		if( strFieldName.equals("ServerIP") ) {
			return 1;
		}
		if( strFieldName.equals("ServerPort") ) {
			return 2;
		}
		if( strFieldName.equals("BasePath") ) {
			return 3;
		}
		if( strFieldName.equals("FilePath") ) {
			return 4;
		}
		if( strFieldName.equals("LogInUser") ) {
			return 5;
		}
		if( strFieldName.equals("LogInPwd") ) {
			return 6;
		}
		if( strFieldName.equals("BackupBasePath") ) {
			return 7;
		}
		if( strFieldName.equals("BackBasePath") ) {
			return 8;
		}
		if( strFieldName.equals("BackBackupBasePath") ) {
			return 9;
		}
		if( strFieldName.equals("Operator") ) {
			return 10;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 11;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 12;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 13;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 14;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "BPOID";
				break;
			case 1:
				strFieldName = "ServerIP";
				break;
			case 2:
				strFieldName = "ServerPort";
				break;
			case 3:
				strFieldName = "BasePath";
				break;
			case 4:
				strFieldName = "FilePath";
				break;
			case 5:
				strFieldName = "LogInUser";
				break;
			case 6:
				strFieldName = "LogInPwd";
				break;
			case 7:
				strFieldName = "BackupBasePath";
				break;
			case 8:
				strFieldName = "BackBasePath";
				break;
			case 9:
				strFieldName = "BackBackupBasePath";
				break;
			case 10:
				strFieldName = "Operator";
				break;
			case 11:
				strFieldName = "MakeDate";
				break;
			case 12:
				strFieldName = "MakeTime";
				break;
			case 13:
				strFieldName = "ModifyDate";
				break;
			case 14:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("BPOID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ServerIP") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ServerPort") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BasePath") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FilePath") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LogInUser") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LogInPwd") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BackupBasePath") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BackBasePath") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BackBackupBasePath") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
