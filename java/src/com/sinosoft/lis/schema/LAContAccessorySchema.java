/*
 * <p>ClassName: LAContAccessorySchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2004-12-20
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LAContAccessoryDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

public class LAContAccessorySchema implements Schema
{
    // @Field
    /** 合同书号码 */
    private String ProtocolNo;
    /** 代理机构 */
    private String AgentCom;
    /** 中介分配类型 */
    private String AssignType;
    /** 分配比例 */
    private double AssignRate;
    /** 分配金额 */
    private double AssignMoney;
    /** 分配算法 */
    private String AssignCode;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 11; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LAContAccessorySchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "ProtocolNo";
        pk[1] = "AgentCom";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getProtocolNo()
    {
        if (ProtocolNo != null && !ProtocolNo.equals("") &&
            SysConst.CHANGECHARSET)
        {
            ProtocolNo = StrTool.unicodeToGBK(ProtocolNo);
        }
        return ProtocolNo;
    }

    public void setProtocolNo(String aProtocolNo)
    {
        ProtocolNo = aProtocolNo;
    }

    public String getAgentCom()
    {
        if (AgentCom != null && !AgentCom.equals("") && SysConst.CHANGECHARSET)
        {
            AgentCom = StrTool.unicodeToGBK(AgentCom);
        }
        return AgentCom;
    }

    public void setAgentCom(String aAgentCom)
    {
        AgentCom = aAgentCom;
    }

    public String getAssignType()
    {
        if (AssignType != null && !AssignType.equals("") &&
            SysConst.CHANGECHARSET)
        {
            AssignType = StrTool.unicodeToGBK(AssignType);
        }
        return AssignType;
    }

    public void setAssignType(String aAssignType)
    {
        AssignType = aAssignType;
    }

    public double getAssignRate()
    {
        return AssignRate;
    }

    public void setAssignRate(double aAssignRate)
    {
        AssignRate = aAssignRate;
    }

    public void setAssignRate(String aAssignRate)
    {
        if (aAssignRate != null && !aAssignRate.equals(""))
        {
            Double tDouble = new Double(aAssignRate);
            double d = tDouble.doubleValue();
            AssignRate = d;
        }
    }

    public double getAssignMoney()
    {
        return AssignMoney;
    }

    public void setAssignMoney(double aAssignMoney)
    {
        AssignMoney = aAssignMoney;
    }

    public void setAssignMoney(String aAssignMoney)
    {
        if (aAssignMoney != null && !aAssignMoney.equals(""))
        {
            Double tDouble = new Double(aAssignMoney);
            double d = tDouble.doubleValue();
            AssignMoney = d;
        }
    }

    public String getAssignCode()
    {
        if (AssignCode != null && !AssignCode.equals("") &&
            SysConst.CHANGECHARSET)
        {
            AssignCode = StrTool.unicodeToGBK(AssignCode);
        }
        return AssignCode;
    }

    public void setAssignCode(String aAssignCode)
    {
        AssignCode = aAssignCode;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LAContAccessorySchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LAContAccessorySchema aLAContAccessorySchema)
    {
        this.ProtocolNo = aLAContAccessorySchema.getProtocolNo();
        this.AgentCom = aLAContAccessorySchema.getAgentCom();
        this.AssignType = aLAContAccessorySchema.getAssignType();
        this.AssignRate = aLAContAccessorySchema.getAssignRate();
        this.AssignMoney = aLAContAccessorySchema.getAssignMoney();
        this.AssignCode = aLAContAccessorySchema.getAssignCode();
        this.Operator = aLAContAccessorySchema.getOperator();
        this.MakeDate = fDate.getDate(aLAContAccessorySchema.getMakeDate());
        this.MakeTime = aLAContAccessorySchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLAContAccessorySchema.getModifyDate());
        this.ModifyTime = aLAContAccessorySchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ProtocolNo") == null)
            {
                this.ProtocolNo = null;
            }
            else
            {
                this.ProtocolNo = rs.getString("ProtocolNo").trim();
            }

            if (rs.getString("AgentCom") == null)
            {
                this.AgentCom = null;
            }
            else
            {
                this.AgentCom = rs.getString("AgentCom").trim();
            }

            if (rs.getString("AssignType") == null)
            {
                this.AssignType = null;
            }
            else
            {
                this.AssignType = rs.getString("AssignType").trim();
            }

            this.AssignRate = rs.getDouble("AssignRate");
            this.AssignMoney = rs.getDouble("AssignMoney");
            if (rs.getString("AssignCode") == null)
            {
                this.AssignCode = null;
            }
            else
            {
                this.AssignCode = rs.getString("AssignCode").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAContAccessorySchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LAContAccessorySchema getSchema()
    {
        LAContAccessorySchema aLAContAccessorySchema = new
                LAContAccessorySchema();
        aLAContAccessorySchema.setSchema(this);
        return aLAContAccessorySchema;
    }

    public LAContAccessoryDB getDB()
    {
        LAContAccessoryDB aDBOper = new LAContAccessoryDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAContAccessory描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(ProtocolNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgentCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AssignType)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(AssignRate) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(AssignMoney) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AssignCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAContAccessory>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            ProtocolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                        SysConst.PACKAGESPILTER);
            AgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            AssignType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                        SysConst.PACKAGESPILTER);
            AssignRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 4, SysConst.PACKAGESPILTER))).doubleValue();
            AssignMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 5, SysConst.PACKAGESPILTER))).doubleValue();
            AssignCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                        SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 8, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 10, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAContAccessorySchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("ProtocolNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProtocolNo));
        }
        if (FCode.equals("AgentCom"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
        }
        if (FCode.equals("AssignType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AssignType));
        }
        if (FCode.equals("AssignRate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AssignRate));
        }
        if (FCode.equals("AssignMoney"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AssignMoney));
        }
        if (FCode.equals("AssignCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AssignCode));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ProtocolNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(AgentCom);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(AssignType);
                break;
            case 3:
                strFieldValue = String.valueOf(AssignRate);
                break;
            case 4:
                strFieldValue = String.valueOf(AssignMoney);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(AssignCode);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("ProtocolNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ProtocolNo = FValue.trim();
            }
            else
            {
                ProtocolNo = null;
            }
        }
        if (FCode.equals("AgentCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentCom = FValue.trim();
            }
            else
            {
                AgentCom = null;
            }
        }
        if (FCode.equals("AssignType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AssignType = FValue.trim();
            }
            else
            {
                AssignType = null;
            }
        }
        if (FCode.equals("AssignRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                AssignRate = d;
            }
        }
        if (FCode.equals("AssignMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                AssignMoney = d;
            }
        }
        if (FCode.equals("AssignCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AssignCode = FValue.trim();
            }
            else
            {
                AssignCode = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LAContAccessorySchema other = (LAContAccessorySchema) otherObject;
        return
                ProtocolNo.equals(other.getProtocolNo())
                && AgentCom.equals(other.getAgentCom())
                && AssignType.equals(other.getAssignType())
                && AssignRate == other.getAssignRate()
                && AssignMoney == other.getAssignMoney()
                && AssignCode.equals(other.getAssignCode())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("ProtocolNo"))
        {
            return 0;
        }
        if (strFieldName.equals("AgentCom"))
        {
            return 1;
        }
        if (strFieldName.equals("AssignType"))
        {
            return 2;
        }
        if (strFieldName.equals("AssignRate"))
        {
            return 3;
        }
        if (strFieldName.equals("AssignMoney"))
        {
            return 4;
        }
        if (strFieldName.equals("AssignCode"))
        {
            return 5;
        }
        if (strFieldName.equals("Operator"))
        {
            return 6;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 7;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 8;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 9;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 10;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "ProtocolNo";
                break;
            case 1:
                strFieldName = "AgentCom";
                break;
            case 2:
                strFieldName = "AssignType";
                break;
            case 3:
                strFieldName = "AssignRate";
                break;
            case 4:
                strFieldName = "AssignMoney";
                break;
            case 5:
                strFieldName = "AssignCode";
                break;
            case 6:
                strFieldName = "Operator";
                break;
            case 7:
                strFieldName = "MakeDate";
                break;
            case 8:
                strFieldName = "MakeTime";
                break;
            case 9:
                strFieldName = "ModifyDate";
                break;
            case 10:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("ProtocolNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AssignType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AssignRate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("AssignMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("AssignCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 4:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
