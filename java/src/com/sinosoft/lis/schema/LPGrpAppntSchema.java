/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LPGrpAppntDB;

/*
 * <p>ClassName: LPGrpAppntSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2019-07-22
 */
public class LPGrpAppntSchema implements Schema, Cloneable
{
	// @Field
	/** 批单号 */
	private String EdorNo;
	/** 批改类型 */
	private String EdorType;
	/** 集体合同号码 */
	private String GrpContNo;
	/** 客户号码 */
	private String CustomerNo;
	/** 印刷号码 */
	private String PrtNo;
	/** 客户地址号码 */
	private String AddressNo;
	/** 投保人级别 */
	private String AppntGrade;
	/** 单位名称 */
	private String Name;
	/** 通讯地址 */
	private String PostalAddress;
	/** 单位邮编 */
	private String ZipCode;
	/** 单位电话 */
	private String Phone;
	/** 密码 */
	private String Password;
	/** 状态 */
	private String State;
	/** 投保人类型 */
	private String AppntType;
	/** 被保人与投保人关系 */
	private String RelationToInsured;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 理赔金银行编码 */
	private String ClaimBankCode;
	/** 理赔金银行帐号 */
	private String ClaimBankAccNo;
	/** 理赔金银行帐户名 */
	private String ClaimAccName;
	/** 组织机构代码 */
	private String OrganComCode;
	/** 总人数 */
	private int Peoples;
	/** 在职人数 */
	private int OnWorkPeoples;
	/** 退休人数 */
	private int OffWorkPeoples;
	/** 其它人员人数 */
	private int OtherPeoples;
	/** 税务登记证 */
	private String TaxRegistration;
	/** 营业执照 */
	private String BusinessLicence;
	/** 其他证件 */
	private String OtherCertificates;
	/** 单位级别 */
	private String GrpLevel;
	/** 税务登记证号码 */
	private String TaxNo;
	/** 法人姓名 */
	private String LegalPersonName;
	/** 法人身份证号 */
	private String LegalPersonIDNo;
	/** 经营范围 */
	private String BusinessScope;
	/** 证件生效日期 */
	private Date IDStartDate;
	/** 证件失效日期 */
	private Date IDEndDate;
	/** 投保人/联系人证件类型 */
	private String IDType;
	/** 投保人/联系人证件号码 */
	private String IDNo;
	/** 投保人/联系人证件长期标识 */
	private String IDLongEffFlag;
	/** 统一社会信用代码 */
	private String UnifiedSocialCreditNo;
	/** 投保人属性 */
	private String InsuredProperty;
	/** 税务人类型 */
	private String TaxpayerType;
	/** 客户开户银行 */
	private String CustomerBankCode;
	/** 客户银行账户 */
	private String CustomerBankAccNo;
	/** 控股股东/实际控制人姓名 */
	private String ShareholderName;
	/** 控股股东/实际控制人证件类型 */
	private String ShareholderIDType;
	/** 控股股东/实际控制人证件号码 */
	private String ShareholderIDNo;
	/** 控股股东/实际控制人证件生效日 */
	private Date ShareholderIDStart;
	/** 控股股东/实际控制人证件失效日 */
	private Date ShareholderIDEnd;
	/** 控股股东/实际控制人证件长期有效 */
	private String ShareholderIDLongFlag;
	/** 法人1姓名 */
	private String LegalPersonName1;
	/** 法人1证件类型 */
	private String LegalPersonIDType1;
	/** 法人1证件号码 */
	private String LegalPersonIDNo1;
	/** 法人1证件生效日 */
	private Date LegalPersonIDStart1;
	/** 法人1证件失效日 */
	private Date LegalPersonIDEnd1;
	/** 法人1证件长期有效 */
	private String LegalPersonIDLongFlag1;
	/** 负责人姓名 */
	private String ResponsibleName;
	/** 负责人证件类型 */
	private String ResponsibleIDType;
	/** 负责人证件号码 */
	private String ResponsibleIDNo;
	/** 负责人证件生效日 */
	private Date ResponsibleIDStart;
	/** 负责人证件失效日 */
	private Date ResponsibleIDEnd;
	/** 负责人证件长期有效 */
	private String ResponsibleIDLongFlag;
	/** 客户授权标识 */
	private String Authorization;
	/** 关联交???标识 */
	private String RelatedTags;
	/** 生僻字标识 */
	private String UnCommonChar;

	public static final int FIELDNUM = 67;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LPGrpAppntSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[3];
		pk[0] = "EdorNo";
		pk[1] = "EdorType";
		pk[2] = "GrpContNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LPGrpAppntSchema cloned = (LPGrpAppntSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getEdorNo()
	{
		return EdorNo;
	}
	public void setEdorNo(String aEdorNo)
	{
		EdorNo = aEdorNo;
	}
	public String getEdorType()
	{
		return EdorType;
	}
	public void setEdorType(String aEdorType)
	{
		EdorType = aEdorType;
	}
	public String getGrpContNo()
	{
		return GrpContNo;
	}
	public void setGrpContNo(String aGrpContNo)
	{
		GrpContNo = aGrpContNo;
	}
	public String getCustomerNo()
	{
		return CustomerNo;
	}
	public void setCustomerNo(String aCustomerNo)
	{
		CustomerNo = aCustomerNo;
	}
	public String getPrtNo()
	{
		return PrtNo;
	}
	public void setPrtNo(String aPrtNo)
	{
		PrtNo = aPrtNo;
	}
	public String getAddressNo()
	{
		return AddressNo;
	}
	public void setAddressNo(String aAddressNo)
	{
		AddressNo = aAddressNo;
	}
	public String getAppntGrade()
	{
		return AppntGrade;
	}
	public void setAppntGrade(String aAppntGrade)
	{
		AppntGrade = aAppntGrade;
	}
	public String getName()
	{
		return Name;
	}
	public void setName(String aName)
	{
		Name = aName;
	}
	public String getPostalAddress()
	{
		return PostalAddress;
	}
	public void setPostalAddress(String aPostalAddress)
	{
		PostalAddress = aPostalAddress;
	}
	public String getZipCode()
	{
		return ZipCode;
	}
	public void setZipCode(String aZipCode)
	{
		ZipCode = aZipCode;
	}
	public String getPhone()
	{
		return Phone;
	}
	public void setPhone(String aPhone)
	{
		Phone = aPhone;
	}
	public String getPassword()
	{
		return Password;
	}
	public void setPassword(String aPassword)
	{
		Password = aPassword;
	}
	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}
	public String getAppntType()
	{
		return AppntType;
	}
	public void setAppntType(String aAppntType)
	{
		AppntType = aAppntType;
	}
	public String getRelationToInsured()
	{
		return RelationToInsured;
	}
	public void setRelationToInsured(String aRelationToInsured)
	{
		RelationToInsured = aRelationToInsured;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getClaimBankCode()
	{
		return ClaimBankCode;
	}
	public void setClaimBankCode(String aClaimBankCode)
	{
		ClaimBankCode = aClaimBankCode;
	}
	public String getClaimBankAccNo()
	{
		return ClaimBankAccNo;
	}
	public void setClaimBankAccNo(String aClaimBankAccNo)
	{
		ClaimBankAccNo = aClaimBankAccNo;
	}
	public String getClaimAccName()
	{
		return ClaimAccName;
	}
	public void setClaimAccName(String aClaimAccName)
	{
		ClaimAccName = aClaimAccName;
	}
	public String getOrganComCode()
	{
		return OrganComCode;
	}
	public void setOrganComCode(String aOrganComCode)
	{
		OrganComCode = aOrganComCode;
	}
	public int getPeoples()
	{
		return Peoples;
	}
	public void setPeoples(int aPeoples)
	{
		Peoples = aPeoples;
	}
	public void setPeoples(String aPeoples)
	{
		if (aPeoples != null && !aPeoples.equals(""))
		{
			Integer tInteger = new Integer(aPeoples);
			int i = tInteger.intValue();
			Peoples = i;
		}
	}

	public int getOnWorkPeoples()
	{
		return OnWorkPeoples;
	}
	public void setOnWorkPeoples(int aOnWorkPeoples)
	{
		OnWorkPeoples = aOnWorkPeoples;
	}
	public void setOnWorkPeoples(String aOnWorkPeoples)
	{
		if (aOnWorkPeoples != null && !aOnWorkPeoples.equals(""))
		{
			Integer tInteger = new Integer(aOnWorkPeoples);
			int i = tInteger.intValue();
			OnWorkPeoples = i;
		}
	}

	public int getOffWorkPeoples()
	{
		return OffWorkPeoples;
	}
	public void setOffWorkPeoples(int aOffWorkPeoples)
	{
		OffWorkPeoples = aOffWorkPeoples;
	}
	public void setOffWorkPeoples(String aOffWorkPeoples)
	{
		if (aOffWorkPeoples != null && !aOffWorkPeoples.equals(""))
		{
			Integer tInteger = new Integer(aOffWorkPeoples);
			int i = tInteger.intValue();
			OffWorkPeoples = i;
		}
	}

	public int getOtherPeoples()
	{
		return OtherPeoples;
	}
	public void setOtherPeoples(int aOtherPeoples)
	{
		OtherPeoples = aOtherPeoples;
	}
	public void setOtherPeoples(String aOtherPeoples)
	{
		if (aOtherPeoples != null && !aOtherPeoples.equals(""))
		{
			Integer tInteger = new Integer(aOtherPeoples);
			int i = tInteger.intValue();
			OtherPeoples = i;
		}
	}

	public String getTaxRegistration()
	{
		return TaxRegistration;
	}
	public void setTaxRegistration(String aTaxRegistration)
	{
		TaxRegistration = aTaxRegistration;
	}
	public String getBusinessLicence()
	{
		return BusinessLicence;
	}
	public void setBusinessLicence(String aBusinessLicence)
	{
		BusinessLicence = aBusinessLicence;
	}
	public String getOtherCertificates()
	{
		return OtherCertificates;
	}
	public void setOtherCertificates(String aOtherCertificates)
	{
		OtherCertificates = aOtherCertificates;
	}
	public String getGrpLevel()
	{
		return GrpLevel;
	}
	public void setGrpLevel(String aGrpLevel)
	{
		GrpLevel = aGrpLevel;
	}
	public String getTaxNo()
	{
		return TaxNo;
	}
	public void setTaxNo(String aTaxNo)
	{
		TaxNo = aTaxNo;
	}
	public String getLegalPersonName()
	{
		return LegalPersonName;
	}
	public void setLegalPersonName(String aLegalPersonName)
	{
		LegalPersonName = aLegalPersonName;
	}
	public String getLegalPersonIDNo()
	{
		return LegalPersonIDNo;
	}
	public void setLegalPersonIDNo(String aLegalPersonIDNo)
	{
		LegalPersonIDNo = aLegalPersonIDNo;
	}
	public String getBusinessScope()
	{
		return BusinessScope;
	}
	public void setBusinessScope(String aBusinessScope)
	{
		BusinessScope = aBusinessScope;
	}
	public String getIDStartDate()
	{
		if( IDStartDate != null )
			return fDate.getString(IDStartDate);
		else
			return null;
	}
	public void setIDStartDate(Date aIDStartDate)
	{
		IDStartDate = aIDStartDate;
	}
	public void setIDStartDate(String aIDStartDate)
	{
		if (aIDStartDate != null && !aIDStartDate.equals("") )
		{
			IDStartDate = fDate.getDate( aIDStartDate );
		}
		else
			IDStartDate = null;
	}

	public String getIDEndDate()
	{
		if( IDEndDate != null )
			return fDate.getString(IDEndDate);
		else
			return null;
	}
	public void setIDEndDate(Date aIDEndDate)
	{
		IDEndDate = aIDEndDate;
	}
	public void setIDEndDate(String aIDEndDate)
	{
		if (aIDEndDate != null && !aIDEndDate.equals("") )
		{
			IDEndDate = fDate.getDate( aIDEndDate );
		}
		else
			IDEndDate = null;
	}

	public String getIDType()
	{
		return IDType;
	}
	public void setIDType(String aIDType)
	{
		IDType = aIDType;
	}
	public String getIDNo()
	{
		return IDNo;
	}
	public void setIDNo(String aIDNo)
	{
		IDNo = aIDNo;
	}
	public String getIDLongEffFlag()
	{
		return IDLongEffFlag;
	}
	public void setIDLongEffFlag(String aIDLongEffFlag)
	{
		IDLongEffFlag = aIDLongEffFlag;
	}
	public String getUnifiedSocialCreditNo()
	{
		return UnifiedSocialCreditNo;
	}
	public void setUnifiedSocialCreditNo(String aUnifiedSocialCreditNo)
	{
		UnifiedSocialCreditNo = aUnifiedSocialCreditNo;
	}
	public String getInsuredProperty()
	{
		return InsuredProperty;
	}
	public void setInsuredProperty(String aInsuredProperty)
	{
		InsuredProperty = aInsuredProperty;
	}
	public String getTaxpayerType()
	{
		return TaxpayerType;
	}
	public void setTaxpayerType(String aTaxpayerType)
	{
		TaxpayerType = aTaxpayerType;
	}
	public String getCustomerBankCode()
	{
		return CustomerBankCode;
	}
	public void setCustomerBankCode(String aCustomerBankCode)
	{
		CustomerBankCode = aCustomerBankCode;
	}
	public String getCustomerBankAccNo()
	{
		return CustomerBankAccNo;
	}
	public void setCustomerBankAccNo(String aCustomerBankAccNo)
	{
		CustomerBankAccNo = aCustomerBankAccNo;
	}
	public String getShareholderName()
	{
		return ShareholderName;
	}
	public void setShareholderName(String aShareholderName)
	{
		ShareholderName = aShareholderName;
	}
	public String getShareholderIDType()
	{
		return ShareholderIDType;
	}
	public void setShareholderIDType(String aShareholderIDType)
	{
		ShareholderIDType = aShareholderIDType;
	}
	public String getShareholderIDNo()
	{
		return ShareholderIDNo;
	}
	public void setShareholderIDNo(String aShareholderIDNo)
	{
		ShareholderIDNo = aShareholderIDNo;
	}
	public String getShareholderIDStart()
	{
		if( ShareholderIDStart != null )
			return fDate.getString(ShareholderIDStart);
		else
			return null;
	}
	public void setShareholderIDStart(Date aShareholderIDStart)
	{
		ShareholderIDStart = aShareholderIDStart;
	}
	public void setShareholderIDStart(String aShareholderIDStart)
	{
		if (aShareholderIDStart != null && !aShareholderIDStart.equals("") )
		{
			ShareholderIDStart = fDate.getDate( aShareholderIDStart );
		}
		else
			ShareholderIDStart = null;
	}

	public String getShareholderIDEnd()
	{
		if( ShareholderIDEnd != null )
			return fDate.getString(ShareholderIDEnd);
		else
			return null;
	}
	public void setShareholderIDEnd(Date aShareholderIDEnd)
	{
		ShareholderIDEnd = aShareholderIDEnd;
	}
	public void setShareholderIDEnd(String aShareholderIDEnd)
	{
		if (aShareholderIDEnd != null && !aShareholderIDEnd.equals("") )
		{
			ShareholderIDEnd = fDate.getDate( aShareholderIDEnd );
		}
		else
			ShareholderIDEnd = null;
	}

	public String getShareholderIDLongFlag()
	{
		return ShareholderIDLongFlag;
	}
	public void setShareholderIDLongFlag(String aShareholderIDLongFlag)
	{
		ShareholderIDLongFlag = aShareholderIDLongFlag;
	}
	public String getLegalPersonName1()
	{
		return LegalPersonName1;
	}
	public void setLegalPersonName1(String aLegalPersonName1)
	{
		LegalPersonName1 = aLegalPersonName1;
	}
	public String getLegalPersonIDType1()
	{
		return LegalPersonIDType1;
	}
	public void setLegalPersonIDType1(String aLegalPersonIDType1)
	{
		LegalPersonIDType1 = aLegalPersonIDType1;
	}
	public String getLegalPersonIDNo1()
	{
		return LegalPersonIDNo1;
	}
	public void setLegalPersonIDNo1(String aLegalPersonIDNo1)
	{
		LegalPersonIDNo1 = aLegalPersonIDNo1;
	}
	public String getLegalPersonIDStart1()
	{
		if( LegalPersonIDStart1 != null )
			return fDate.getString(LegalPersonIDStart1);
		else
			return null;
	}
	public void setLegalPersonIDStart1(Date aLegalPersonIDStart1)
	{
		LegalPersonIDStart1 = aLegalPersonIDStart1;
	}
	public void setLegalPersonIDStart1(String aLegalPersonIDStart1)
	{
		if (aLegalPersonIDStart1 != null && !aLegalPersonIDStart1.equals("") )
		{
			LegalPersonIDStart1 = fDate.getDate( aLegalPersonIDStart1 );
		}
		else
			LegalPersonIDStart1 = null;
	}

	public String getLegalPersonIDEnd1()
	{
		if( LegalPersonIDEnd1 != null )
			return fDate.getString(LegalPersonIDEnd1);
		else
			return null;
	}
	public void setLegalPersonIDEnd1(Date aLegalPersonIDEnd1)
	{
		LegalPersonIDEnd1 = aLegalPersonIDEnd1;
	}
	public void setLegalPersonIDEnd1(String aLegalPersonIDEnd1)
	{
		if (aLegalPersonIDEnd1 != null && !aLegalPersonIDEnd1.equals("") )
		{
			LegalPersonIDEnd1 = fDate.getDate( aLegalPersonIDEnd1 );
		}
		else
			LegalPersonIDEnd1 = null;
	}

	public String getLegalPersonIDLongFlag1()
	{
		return LegalPersonIDLongFlag1;
	}
	public void setLegalPersonIDLongFlag1(String aLegalPersonIDLongFlag1)
	{
		LegalPersonIDLongFlag1 = aLegalPersonIDLongFlag1;
	}
	public String getResponsibleName()
	{
		return ResponsibleName;
	}
	public void setResponsibleName(String aResponsibleName)
	{
		ResponsibleName = aResponsibleName;
	}
	public String getResponsibleIDType()
	{
		return ResponsibleIDType;
	}
	public void setResponsibleIDType(String aResponsibleIDType)
	{
		ResponsibleIDType = aResponsibleIDType;
	}
	public String getResponsibleIDNo()
	{
		return ResponsibleIDNo;
	}
	public void setResponsibleIDNo(String aResponsibleIDNo)
	{
		ResponsibleIDNo = aResponsibleIDNo;
	}
	public String getResponsibleIDStart()
	{
		if( ResponsibleIDStart != null )
			return fDate.getString(ResponsibleIDStart);
		else
			return null;
	}
	public void setResponsibleIDStart(Date aResponsibleIDStart)
	{
		ResponsibleIDStart = aResponsibleIDStart;
	}
	public void setResponsibleIDStart(String aResponsibleIDStart)
	{
		if (aResponsibleIDStart != null && !aResponsibleIDStart.equals("") )
		{
			ResponsibleIDStart = fDate.getDate( aResponsibleIDStart );
		}
		else
			ResponsibleIDStart = null;
	}

	public String getResponsibleIDEnd()
	{
		if( ResponsibleIDEnd != null )
			return fDate.getString(ResponsibleIDEnd);
		else
			return null;
	}
	public void setResponsibleIDEnd(Date aResponsibleIDEnd)
	{
		ResponsibleIDEnd = aResponsibleIDEnd;
	}
	public void setResponsibleIDEnd(String aResponsibleIDEnd)
	{
		if (aResponsibleIDEnd != null && !aResponsibleIDEnd.equals("") )
		{
			ResponsibleIDEnd = fDate.getDate( aResponsibleIDEnd );
		}
		else
			ResponsibleIDEnd = null;
	}

	public String getResponsibleIDLongFlag()
	{
		return ResponsibleIDLongFlag;
	}
	public void setResponsibleIDLongFlag(String aResponsibleIDLongFlag)
	{
		ResponsibleIDLongFlag = aResponsibleIDLongFlag;
	}
	public String getAuthorization()
	{
		return Authorization;
	}
	public void setAuthorization(String aAuthorization)
	{
		Authorization = aAuthorization;
	}
	public String getRelatedTags()
	{
		return RelatedTags;
	}
	public void setRelatedTags(String aRelatedTags)
	{
		RelatedTags = aRelatedTags;
	}
	public String getUnCommonChar()
	{
		return UnCommonChar;
	}
	public void setUnCommonChar(String aUnCommonChar)
	{
		UnCommonChar = aUnCommonChar;
	}

	/**
	* 使用另外一个 LPGrpAppntSchema 对象给 Schema 赋值
	* @param: aLPGrpAppntSchema LPGrpAppntSchema
	**/
	public void setSchema(LPGrpAppntSchema aLPGrpAppntSchema)
	{
		this.EdorNo = aLPGrpAppntSchema.getEdorNo();
		this.EdorType = aLPGrpAppntSchema.getEdorType();
		this.GrpContNo = aLPGrpAppntSchema.getGrpContNo();
		this.CustomerNo = aLPGrpAppntSchema.getCustomerNo();
		this.PrtNo = aLPGrpAppntSchema.getPrtNo();
		this.AddressNo = aLPGrpAppntSchema.getAddressNo();
		this.AppntGrade = aLPGrpAppntSchema.getAppntGrade();
		this.Name = aLPGrpAppntSchema.getName();
		this.PostalAddress = aLPGrpAppntSchema.getPostalAddress();
		this.ZipCode = aLPGrpAppntSchema.getZipCode();
		this.Phone = aLPGrpAppntSchema.getPhone();
		this.Password = aLPGrpAppntSchema.getPassword();
		this.State = aLPGrpAppntSchema.getState();
		this.AppntType = aLPGrpAppntSchema.getAppntType();
		this.RelationToInsured = aLPGrpAppntSchema.getRelationToInsured();
		this.Operator = aLPGrpAppntSchema.getOperator();
		this.MakeDate = fDate.getDate( aLPGrpAppntSchema.getMakeDate());
		this.MakeTime = aLPGrpAppntSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLPGrpAppntSchema.getModifyDate());
		this.ModifyTime = aLPGrpAppntSchema.getModifyTime();
		this.ClaimBankCode = aLPGrpAppntSchema.getClaimBankCode();
		this.ClaimBankAccNo = aLPGrpAppntSchema.getClaimBankAccNo();
		this.ClaimAccName = aLPGrpAppntSchema.getClaimAccName();
		this.OrganComCode = aLPGrpAppntSchema.getOrganComCode();
		this.Peoples = aLPGrpAppntSchema.getPeoples();
		this.OnWorkPeoples = aLPGrpAppntSchema.getOnWorkPeoples();
		this.OffWorkPeoples = aLPGrpAppntSchema.getOffWorkPeoples();
		this.OtherPeoples = aLPGrpAppntSchema.getOtherPeoples();
		this.TaxRegistration = aLPGrpAppntSchema.getTaxRegistration();
		this.BusinessLicence = aLPGrpAppntSchema.getBusinessLicence();
		this.OtherCertificates = aLPGrpAppntSchema.getOtherCertificates();
		this.GrpLevel = aLPGrpAppntSchema.getGrpLevel();
		this.TaxNo = aLPGrpAppntSchema.getTaxNo();
		this.LegalPersonName = aLPGrpAppntSchema.getLegalPersonName();
		this.LegalPersonIDNo = aLPGrpAppntSchema.getLegalPersonIDNo();
		this.BusinessScope = aLPGrpAppntSchema.getBusinessScope();
		this.IDStartDate = fDate.getDate( aLPGrpAppntSchema.getIDStartDate());
		this.IDEndDate = fDate.getDate( aLPGrpAppntSchema.getIDEndDate());
		this.IDType = aLPGrpAppntSchema.getIDType();
		this.IDNo = aLPGrpAppntSchema.getIDNo();
		this.IDLongEffFlag = aLPGrpAppntSchema.getIDLongEffFlag();
		this.UnifiedSocialCreditNo = aLPGrpAppntSchema.getUnifiedSocialCreditNo();
		this.InsuredProperty = aLPGrpAppntSchema.getInsuredProperty();
		this.TaxpayerType = aLPGrpAppntSchema.getTaxpayerType();
		this.CustomerBankCode = aLPGrpAppntSchema.getCustomerBankCode();
		this.CustomerBankAccNo = aLPGrpAppntSchema.getCustomerBankAccNo();
		this.ShareholderName = aLPGrpAppntSchema.getShareholderName();
		this.ShareholderIDType = aLPGrpAppntSchema.getShareholderIDType();
		this.ShareholderIDNo = aLPGrpAppntSchema.getShareholderIDNo();
		this.ShareholderIDStart = fDate.getDate( aLPGrpAppntSchema.getShareholderIDStart());
		this.ShareholderIDEnd = fDate.getDate( aLPGrpAppntSchema.getShareholderIDEnd());
		this.ShareholderIDLongFlag = aLPGrpAppntSchema.getShareholderIDLongFlag();
		this.LegalPersonName1 = aLPGrpAppntSchema.getLegalPersonName1();
		this.LegalPersonIDType1 = aLPGrpAppntSchema.getLegalPersonIDType1();
		this.LegalPersonIDNo1 = aLPGrpAppntSchema.getLegalPersonIDNo1();
		this.LegalPersonIDStart1 = fDate.getDate( aLPGrpAppntSchema.getLegalPersonIDStart1());
		this.LegalPersonIDEnd1 = fDate.getDate( aLPGrpAppntSchema.getLegalPersonIDEnd1());
		this.LegalPersonIDLongFlag1 = aLPGrpAppntSchema.getLegalPersonIDLongFlag1();
		this.ResponsibleName = aLPGrpAppntSchema.getResponsibleName();
		this.ResponsibleIDType = aLPGrpAppntSchema.getResponsibleIDType();
		this.ResponsibleIDNo = aLPGrpAppntSchema.getResponsibleIDNo();
		this.ResponsibleIDStart = fDate.getDate( aLPGrpAppntSchema.getResponsibleIDStart());
		this.ResponsibleIDEnd = fDate.getDate( aLPGrpAppntSchema.getResponsibleIDEnd());
		this.ResponsibleIDLongFlag = aLPGrpAppntSchema.getResponsibleIDLongFlag();
		this.Authorization = aLPGrpAppntSchema.getAuthorization();
		this.RelatedTags = aLPGrpAppntSchema.getRelatedTags();
		this.UnCommonChar = aLPGrpAppntSchema.getUnCommonChar();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("EdorNo") == null )
				this.EdorNo = null;
			else
				this.EdorNo = rs.getString("EdorNo").trim();

			if( rs.getString("EdorType") == null )
				this.EdorType = null;
			else
				this.EdorType = rs.getString("EdorType").trim();

			if( rs.getString("GrpContNo") == null )
				this.GrpContNo = null;
			else
				this.GrpContNo = rs.getString("GrpContNo").trim();

			if( rs.getString("CustomerNo") == null )
				this.CustomerNo = null;
			else
				this.CustomerNo = rs.getString("CustomerNo").trim();

			if( rs.getString("PrtNo") == null )
				this.PrtNo = null;
			else
				this.PrtNo = rs.getString("PrtNo").trim();

			if( rs.getString("AddressNo") == null )
				this.AddressNo = null;
			else
				this.AddressNo = rs.getString("AddressNo").trim();

			if( rs.getString("AppntGrade") == null )
				this.AppntGrade = null;
			else
				this.AppntGrade = rs.getString("AppntGrade").trim();

			if( rs.getString("Name") == null )
				this.Name = null;
			else
				this.Name = rs.getString("Name").trim();

			if( rs.getString("PostalAddress") == null )
				this.PostalAddress = null;
			else
				this.PostalAddress = rs.getString("PostalAddress").trim();

			if( rs.getString("ZipCode") == null )
				this.ZipCode = null;
			else
				this.ZipCode = rs.getString("ZipCode").trim();

			if( rs.getString("Phone") == null )
				this.Phone = null;
			else
				this.Phone = rs.getString("Phone").trim();

			if( rs.getString("Password") == null )
				this.Password = null;
			else
				this.Password = rs.getString("Password").trim();

			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			if( rs.getString("AppntType") == null )
				this.AppntType = null;
			else
				this.AppntType = rs.getString("AppntType").trim();

			if( rs.getString("RelationToInsured") == null )
				this.RelationToInsured = null;
			else
				this.RelationToInsured = rs.getString("RelationToInsured").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("ClaimBankCode") == null )
				this.ClaimBankCode = null;
			else
				this.ClaimBankCode = rs.getString("ClaimBankCode").trim();

			if( rs.getString("ClaimBankAccNo") == null )
				this.ClaimBankAccNo = null;
			else
				this.ClaimBankAccNo = rs.getString("ClaimBankAccNo").trim();

			if( rs.getString("ClaimAccName") == null )
				this.ClaimAccName = null;
			else
				this.ClaimAccName = rs.getString("ClaimAccName").trim();

			if( rs.getString("OrganComCode") == null )
				this.OrganComCode = null;
			else
				this.OrganComCode = rs.getString("OrganComCode").trim();

			this.Peoples = rs.getInt("Peoples");
			this.OnWorkPeoples = rs.getInt("OnWorkPeoples");
			this.OffWorkPeoples = rs.getInt("OffWorkPeoples");
			this.OtherPeoples = rs.getInt("OtherPeoples");
			if( rs.getString("TaxRegistration") == null )
				this.TaxRegistration = null;
			else
				this.TaxRegistration = rs.getString("TaxRegistration").trim();

			if( rs.getString("BusinessLicence") == null )
				this.BusinessLicence = null;
			else
				this.BusinessLicence = rs.getString("BusinessLicence").trim();

			if( rs.getString("OtherCertificates") == null )
				this.OtherCertificates = null;
			else
				this.OtherCertificates = rs.getString("OtherCertificates").trim();

			if( rs.getString("GrpLevel") == null )
				this.GrpLevel = null;
			else
				this.GrpLevel = rs.getString("GrpLevel").trim();

			if( rs.getString("TaxNo") == null )
				this.TaxNo = null;
			else
				this.TaxNo = rs.getString("TaxNo").trim();

			if( rs.getString("LegalPersonName") == null )
				this.LegalPersonName = null;
			else
				this.LegalPersonName = rs.getString("LegalPersonName").trim();

			if( rs.getString("LegalPersonIDNo") == null )
				this.LegalPersonIDNo = null;
			else
				this.LegalPersonIDNo = rs.getString("LegalPersonIDNo").trim();

			if( rs.getString("BusinessScope") == null )
				this.BusinessScope = null;
			else
				this.BusinessScope = rs.getString("BusinessScope").trim();

			this.IDStartDate = rs.getDate("IDStartDate");
			this.IDEndDate = rs.getDate("IDEndDate");
			if( rs.getString("IDType") == null )
				this.IDType = null;
			else
				this.IDType = rs.getString("IDType").trim();

			if( rs.getString("IDNo") == null )
				this.IDNo = null;
			else
				this.IDNo = rs.getString("IDNo").trim();

			if( rs.getString("IDLongEffFlag") == null )
				this.IDLongEffFlag = null;
			else
				this.IDLongEffFlag = rs.getString("IDLongEffFlag").trim();

			if( rs.getString("UnifiedSocialCreditNo") == null )
				this.UnifiedSocialCreditNo = null;
			else
				this.UnifiedSocialCreditNo = rs.getString("UnifiedSocialCreditNo").trim();

			if( rs.getString("InsuredProperty") == null )
				this.InsuredProperty = null;
			else
				this.InsuredProperty = rs.getString("InsuredProperty").trim();

			if( rs.getString("TaxpayerType") == null )
				this.TaxpayerType = null;
			else
				this.TaxpayerType = rs.getString("TaxpayerType").trim();

			if( rs.getString("CustomerBankCode") == null )
				this.CustomerBankCode = null;
			else
				this.CustomerBankCode = rs.getString("CustomerBankCode").trim();

			if( rs.getString("CustomerBankAccNo") == null )
				this.CustomerBankAccNo = null;
			else
				this.CustomerBankAccNo = rs.getString("CustomerBankAccNo").trim();

			if( rs.getString("ShareholderName") == null )
				this.ShareholderName = null;
			else
				this.ShareholderName = rs.getString("ShareholderName").trim();

			if( rs.getString("ShareholderIDType") == null )
				this.ShareholderIDType = null;
			else
				this.ShareholderIDType = rs.getString("ShareholderIDType").trim();

			if( rs.getString("ShareholderIDNo") == null )
				this.ShareholderIDNo = null;
			else
				this.ShareholderIDNo = rs.getString("ShareholderIDNo").trim();

			this.ShareholderIDStart = rs.getDate("ShareholderIDStart");
			this.ShareholderIDEnd = rs.getDate("ShareholderIDEnd");
			if( rs.getString("ShareholderIDLongFlag") == null )
				this.ShareholderIDLongFlag = null;
			else
				this.ShareholderIDLongFlag = rs.getString("ShareholderIDLongFlag").trim();

			if( rs.getString("LegalPersonName1") == null )
				this.LegalPersonName1 = null;
			else
				this.LegalPersonName1 = rs.getString("LegalPersonName1").trim();

			if( rs.getString("LegalPersonIDType1") == null )
				this.LegalPersonIDType1 = null;
			else
				this.LegalPersonIDType1 = rs.getString("LegalPersonIDType1").trim();

			if( rs.getString("LegalPersonIDNo1") == null )
				this.LegalPersonIDNo1 = null;
			else
				this.LegalPersonIDNo1 = rs.getString("LegalPersonIDNo1").trim();

			this.LegalPersonIDStart1 = rs.getDate("LegalPersonIDStart1");
			this.LegalPersonIDEnd1 = rs.getDate("LegalPersonIDEnd1");
			if( rs.getString("LegalPersonIDLongFlag1") == null )
				this.LegalPersonIDLongFlag1 = null;
			else
				this.LegalPersonIDLongFlag1 = rs.getString("LegalPersonIDLongFlag1").trim();

			if( rs.getString("ResponsibleName") == null )
				this.ResponsibleName = null;
			else
				this.ResponsibleName = rs.getString("ResponsibleName").trim();

			if( rs.getString("ResponsibleIDType") == null )
				this.ResponsibleIDType = null;
			else
				this.ResponsibleIDType = rs.getString("ResponsibleIDType").trim();

			if( rs.getString("ResponsibleIDNo") == null )
				this.ResponsibleIDNo = null;
			else
				this.ResponsibleIDNo = rs.getString("ResponsibleIDNo").trim();

			this.ResponsibleIDStart = rs.getDate("ResponsibleIDStart");
			this.ResponsibleIDEnd = rs.getDate("ResponsibleIDEnd");
			if( rs.getString("ResponsibleIDLongFlag") == null )
				this.ResponsibleIDLongFlag = null;
			else
				this.ResponsibleIDLongFlag = rs.getString("ResponsibleIDLongFlag").trim();

			if( rs.getString("Authorization") == null )
				this.Authorization = null;
			else
				this.Authorization = rs.getString("Authorization").trim();

			if( rs.getString("RelatedTags") == null )
				this.RelatedTags = null;
			else
				this.RelatedTags = rs.getString("RelatedTags").trim();

			if( rs.getString("UnCommonChar") == null )
				this.UnCommonChar = null;
			else
				this.UnCommonChar = rs.getString("UnCommonChar").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LPGrpAppnt表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LPGrpAppntSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LPGrpAppntSchema getSchema()
	{
		LPGrpAppntSchema aLPGrpAppntSchema = new LPGrpAppntSchema();
		aLPGrpAppntSchema.setSchema(this);
		return aLPGrpAppntSchema;
	}

	public LPGrpAppntDB getDB()
	{
		LPGrpAppntDB aDBOper = new LPGrpAppntDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLPGrpAppnt描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(EdorNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EdorType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PrtNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AddressNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppntGrade)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Name)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PostalAddress)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ZipCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Phone)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Password)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppntType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RelationToInsured)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ClaimBankCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ClaimBankAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ClaimAccName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OrganComCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Peoples));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(OnWorkPeoples));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(OffWorkPeoples));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(OtherPeoples));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TaxRegistration)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusinessLicence)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OtherCertificates)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpLevel)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TaxNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LegalPersonName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LegalPersonIDNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusinessScope)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( IDStartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( IDEndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IDType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IDNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IDLongEffFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UnifiedSocialCreditNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuredProperty)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TaxpayerType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerBankCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerBankAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ShareholderName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ShareholderIDType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ShareholderIDNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ShareholderIDStart ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ShareholderIDEnd ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ShareholderIDLongFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LegalPersonName1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LegalPersonIDType1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LegalPersonIDNo1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( LegalPersonIDStart1 ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( LegalPersonIDEnd1 ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LegalPersonIDLongFlag1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ResponsibleName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ResponsibleIDType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ResponsibleIDNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ResponsibleIDStart ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ResponsibleIDEnd ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ResponsibleIDLongFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Authorization)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RelatedTags)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UnCommonChar));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLPGrpAppnt>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			EdorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			EdorType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			AddressNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			AppntGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			PostalAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			ZipCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			Phone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			Password = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			AppntType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			RelationToInsured = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			ClaimBankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			ClaimBankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			ClaimAccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			OrganComCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			Peoples= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,25,SysConst.PACKAGESPILTER))).intValue();
			OnWorkPeoples= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,26,SysConst.PACKAGESPILTER))).intValue();
			OffWorkPeoples= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,27,SysConst.PACKAGESPILTER))).intValue();
			OtherPeoples= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,28,SysConst.PACKAGESPILTER))).intValue();
			TaxRegistration = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			BusinessLicence = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			OtherCertificates = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			GrpLevel = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
			TaxNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			LegalPersonName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			LegalPersonIDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			BusinessScope = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
			IDStartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37,SysConst.PACKAGESPILTER));
			IDEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38,SysConst.PACKAGESPILTER));
			IDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
			IDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
			IDLongEffFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41, SysConst.PACKAGESPILTER );
			UnifiedSocialCreditNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
			InsuredProperty = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43, SysConst.PACKAGESPILTER );
			TaxpayerType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44, SysConst.PACKAGESPILTER );
			CustomerBankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 45, SysConst.PACKAGESPILTER );
			CustomerBankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 46, SysConst.PACKAGESPILTER );
			ShareholderName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 47, SysConst.PACKAGESPILTER );
			ShareholderIDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 48, SysConst.PACKAGESPILTER );
			ShareholderIDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 49, SysConst.PACKAGESPILTER );
			ShareholderIDStart = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 50,SysConst.PACKAGESPILTER));
			ShareholderIDEnd = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 51,SysConst.PACKAGESPILTER));
			ShareholderIDLongFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 52, SysConst.PACKAGESPILTER );
			LegalPersonName1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 53, SysConst.PACKAGESPILTER );
			LegalPersonIDType1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 54, SysConst.PACKAGESPILTER );
			LegalPersonIDNo1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 55, SysConst.PACKAGESPILTER );
			LegalPersonIDStart1 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 56,SysConst.PACKAGESPILTER));
			LegalPersonIDEnd1 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 57,SysConst.PACKAGESPILTER));
			LegalPersonIDLongFlag1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 58, SysConst.PACKAGESPILTER );
			ResponsibleName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 59, SysConst.PACKAGESPILTER );
			ResponsibleIDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 60, SysConst.PACKAGESPILTER );
			ResponsibleIDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 61, SysConst.PACKAGESPILTER );
			ResponsibleIDStart = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 62,SysConst.PACKAGESPILTER));
			ResponsibleIDEnd = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 63,SysConst.PACKAGESPILTER));
			ResponsibleIDLongFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 64, SysConst.PACKAGESPILTER );
			Authorization = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 65, SysConst.PACKAGESPILTER );
			RelatedTags = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 66, SysConst.PACKAGESPILTER );
			UnCommonChar = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 67, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LPGrpAppntSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("EdorNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EdorNo));
		}
		if (FCode.equals("EdorType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EdorType));
		}
		if (FCode.equals("GrpContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
		}
		if (FCode.equals("CustomerNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
		}
		if (FCode.equals("PrtNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
		}
		if (FCode.equals("AddressNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AddressNo));
		}
		if (FCode.equals("AppntGrade"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppntGrade));
		}
		if (FCode.equals("Name"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
		}
		if (FCode.equals("PostalAddress"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PostalAddress));
		}
		if (FCode.equals("ZipCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ZipCode));
		}
		if (FCode.equals("Phone"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Phone));
		}
		if (FCode.equals("Password"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Password));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("AppntType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppntType));
		}
		if (FCode.equals("RelationToInsured"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RelationToInsured));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("ClaimBankCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClaimBankCode));
		}
		if (FCode.equals("ClaimBankAccNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClaimBankAccNo));
		}
		if (FCode.equals("ClaimAccName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClaimAccName));
		}
		if (FCode.equals("OrganComCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OrganComCode));
		}
		if (FCode.equals("Peoples"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Peoples));
		}
		if (FCode.equals("OnWorkPeoples"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OnWorkPeoples));
		}
		if (FCode.equals("OffWorkPeoples"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OffWorkPeoples));
		}
		if (FCode.equals("OtherPeoples"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OtherPeoples));
		}
		if (FCode.equals("TaxRegistration"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TaxRegistration));
		}
		if (FCode.equals("BusinessLicence"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusinessLicence));
		}
		if (FCode.equals("OtherCertificates"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OtherCertificates));
		}
		if (FCode.equals("GrpLevel"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpLevel));
		}
		if (FCode.equals("TaxNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TaxNo));
		}
		if (FCode.equals("LegalPersonName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LegalPersonName));
		}
		if (FCode.equals("LegalPersonIDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LegalPersonIDNo));
		}
		if (FCode.equals("BusinessScope"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusinessScope));
		}
		if (FCode.equals("IDStartDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getIDStartDate()));
		}
		if (FCode.equals("IDEndDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getIDEndDate()));
		}
		if (FCode.equals("IDType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IDType));
		}
		if (FCode.equals("IDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
		}
		if (FCode.equals("IDLongEffFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IDLongEffFlag));
		}
		if (FCode.equals("UnifiedSocialCreditNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UnifiedSocialCreditNo));
		}
		if (FCode.equals("InsuredProperty"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredProperty));
		}
		if (FCode.equals("TaxpayerType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TaxpayerType));
		}
		if (FCode.equals("CustomerBankCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerBankCode));
		}
		if (FCode.equals("CustomerBankAccNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerBankAccNo));
		}
		if (FCode.equals("ShareholderName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ShareholderName));
		}
		if (FCode.equals("ShareholderIDType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ShareholderIDType));
		}
		if (FCode.equals("ShareholderIDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ShareholderIDNo));
		}
		if (FCode.equals("ShareholderIDStart"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getShareholderIDStart()));
		}
		if (FCode.equals("ShareholderIDEnd"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getShareholderIDEnd()));
		}
		if (FCode.equals("ShareholderIDLongFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ShareholderIDLongFlag));
		}
		if (FCode.equals("LegalPersonName1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LegalPersonName1));
		}
		if (FCode.equals("LegalPersonIDType1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LegalPersonIDType1));
		}
		if (FCode.equals("LegalPersonIDNo1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LegalPersonIDNo1));
		}
		if (FCode.equals("LegalPersonIDStart1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getLegalPersonIDStart1()));
		}
		if (FCode.equals("LegalPersonIDEnd1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getLegalPersonIDEnd1()));
		}
		if (FCode.equals("LegalPersonIDLongFlag1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LegalPersonIDLongFlag1));
		}
		if (FCode.equals("ResponsibleName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ResponsibleName));
		}
		if (FCode.equals("ResponsibleIDType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ResponsibleIDType));
		}
		if (FCode.equals("ResponsibleIDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ResponsibleIDNo));
		}
		if (FCode.equals("ResponsibleIDStart"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getResponsibleIDStart()));
		}
		if (FCode.equals("ResponsibleIDEnd"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getResponsibleIDEnd()));
		}
		if (FCode.equals("ResponsibleIDLongFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ResponsibleIDLongFlag));
		}
		if (FCode.equals("Authorization"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Authorization));
		}
		if (FCode.equals("RelatedTags"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RelatedTags));
		}
		if (FCode.equals("UnCommonChar"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UnCommonChar));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(EdorNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(EdorType);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(GrpContNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(CustomerNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(PrtNo);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(AddressNo);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(AppntGrade);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(Name);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(PostalAddress);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(ZipCode);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(Phone);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(Password);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(AppntType);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(RelationToInsured);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(ClaimBankCode);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(ClaimBankAccNo);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(ClaimAccName);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(OrganComCode);
				break;
			case 24:
				strFieldValue = String.valueOf(Peoples);
				break;
			case 25:
				strFieldValue = String.valueOf(OnWorkPeoples);
				break;
			case 26:
				strFieldValue = String.valueOf(OffWorkPeoples);
				break;
			case 27:
				strFieldValue = String.valueOf(OtherPeoples);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(TaxRegistration);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(BusinessLicence);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(OtherCertificates);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(GrpLevel);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(TaxNo);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(LegalPersonName);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(LegalPersonIDNo);
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(BusinessScope);
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getIDStartDate()));
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getIDEndDate()));
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(IDType);
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(IDNo);
				break;
			case 40:
				strFieldValue = StrTool.GBKToUnicode(IDLongEffFlag);
				break;
			case 41:
				strFieldValue = StrTool.GBKToUnicode(UnifiedSocialCreditNo);
				break;
			case 42:
				strFieldValue = StrTool.GBKToUnicode(InsuredProperty);
				break;
			case 43:
				strFieldValue = StrTool.GBKToUnicode(TaxpayerType);
				break;
			case 44:
				strFieldValue = StrTool.GBKToUnicode(CustomerBankCode);
				break;
			case 45:
				strFieldValue = StrTool.GBKToUnicode(CustomerBankAccNo);
				break;
			case 46:
				strFieldValue = StrTool.GBKToUnicode(ShareholderName);
				break;
			case 47:
				strFieldValue = StrTool.GBKToUnicode(ShareholderIDType);
				break;
			case 48:
				strFieldValue = StrTool.GBKToUnicode(ShareholderIDNo);
				break;
			case 49:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getShareholderIDStart()));
				break;
			case 50:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getShareholderIDEnd()));
				break;
			case 51:
				strFieldValue = StrTool.GBKToUnicode(ShareholderIDLongFlag);
				break;
			case 52:
				strFieldValue = StrTool.GBKToUnicode(LegalPersonName1);
				break;
			case 53:
				strFieldValue = StrTool.GBKToUnicode(LegalPersonIDType1);
				break;
			case 54:
				strFieldValue = StrTool.GBKToUnicode(LegalPersonIDNo1);
				break;
			case 55:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getLegalPersonIDStart1()));
				break;
			case 56:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getLegalPersonIDEnd1()));
				break;
			case 57:
				strFieldValue = StrTool.GBKToUnicode(LegalPersonIDLongFlag1);
				break;
			case 58:
				strFieldValue = StrTool.GBKToUnicode(ResponsibleName);
				break;
			case 59:
				strFieldValue = StrTool.GBKToUnicode(ResponsibleIDType);
				break;
			case 60:
				strFieldValue = StrTool.GBKToUnicode(ResponsibleIDNo);
				break;
			case 61:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getResponsibleIDStart()));
				break;
			case 62:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getResponsibleIDEnd()));
				break;
			case 63:
				strFieldValue = StrTool.GBKToUnicode(ResponsibleIDLongFlag);
				break;
			case 64:
				strFieldValue = StrTool.GBKToUnicode(Authorization);
				break;
			case 65:
				strFieldValue = StrTool.GBKToUnicode(RelatedTags);
				break;
			case 66:
				strFieldValue = StrTool.GBKToUnicode(UnCommonChar);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("EdorNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EdorNo = FValue.trim();
			}
			else
				EdorNo = null;
		}
		if (FCode.equalsIgnoreCase("EdorType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EdorType = FValue.trim();
			}
			else
				EdorType = null;
		}
		if (FCode.equalsIgnoreCase("GrpContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpContNo = FValue.trim();
			}
			else
				GrpContNo = null;
		}
		if (FCode.equalsIgnoreCase("CustomerNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerNo = FValue.trim();
			}
			else
				CustomerNo = null;
		}
		if (FCode.equalsIgnoreCase("PrtNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PrtNo = FValue.trim();
			}
			else
				PrtNo = null;
		}
		if (FCode.equalsIgnoreCase("AddressNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AddressNo = FValue.trim();
			}
			else
				AddressNo = null;
		}
		if (FCode.equalsIgnoreCase("AppntGrade"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppntGrade = FValue.trim();
			}
			else
				AppntGrade = null;
		}
		if (FCode.equalsIgnoreCase("Name"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Name = FValue.trim();
			}
			else
				Name = null;
		}
		if (FCode.equalsIgnoreCase("PostalAddress"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PostalAddress = FValue.trim();
			}
			else
				PostalAddress = null;
		}
		if (FCode.equalsIgnoreCase("ZipCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ZipCode = FValue.trim();
			}
			else
				ZipCode = null;
		}
		if (FCode.equalsIgnoreCase("Phone"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Phone = FValue.trim();
			}
			else
				Phone = null;
		}
		if (FCode.equalsIgnoreCase("Password"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Password = FValue.trim();
			}
			else
				Password = null;
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("AppntType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppntType = FValue.trim();
			}
			else
				AppntType = null;
		}
		if (FCode.equalsIgnoreCase("RelationToInsured"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RelationToInsured = FValue.trim();
			}
			else
				RelationToInsured = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("ClaimBankCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ClaimBankCode = FValue.trim();
			}
			else
				ClaimBankCode = null;
		}
		if (FCode.equalsIgnoreCase("ClaimBankAccNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ClaimBankAccNo = FValue.trim();
			}
			else
				ClaimBankAccNo = null;
		}
		if (FCode.equalsIgnoreCase("ClaimAccName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ClaimAccName = FValue.trim();
			}
			else
				ClaimAccName = null;
		}
		if (FCode.equalsIgnoreCase("OrganComCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OrganComCode = FValue.trim();
			}
			else
				OrganComCode = null;
		}
		if (FCode.equalsIgnoreCase("Peoples"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				Peoples = i;
			}
		}
		if (FCode.equalsIgnoreCase("OnWorkPeoples"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				OnWorkPeoples = i;
			}
		}
		if (FCode.equalsIgnoreCase("OffWorkPeoples"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				OffWorkPeoples = i;
			}
		}
		if (FCode.equalsIgnoreCase("OtherPeoples"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				OtherPeoples = i;
			}
		}
		if (FCode.equalsIgnoreCase("TaxRegistration"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TaxRegistration = FValue.trim();
			}
			else
				TaxRegistration = null;
		}
		if (FCode.equalsIgnoreCase("BusinessLicence"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusinessLicence = FValue.trim();
			}
			else
				BusinessLicence = null;
		}
		if (FCode.equalsIgnoreCase("OtherCertificates"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OtherCertificates = FValue.trim();
			}
			else
				OtherCertificates = null;
		}
		if (FCode.equalsIgnoreCase("GrpLevel"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpLevel = FValue.trim();
			}
			else
				GrpLevel = null;
		}
		if (FCode.equalsIgnoreCase("TaxNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TaxNo = FValue.trim();
			}
			else
				TaxNo = null;
		}
		if (FCode.equalsIgnoreCase("LegalPersonName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LegalPersonName = FValue.trim();
			}
			else
				LegalPersonName = null;
		}
		if (FCode.equalsIgnoreCase("LegalPersonIDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LegalPersonIDNo = FValue.trim();
			}
			else
				LegalPersonIDNo = null;
		}
		if (FCode.equalsIgnoreCase("BusinessScope"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusinessScope = FValue.trim();
			}
			else
				BusinessScope = null;
		}
		if (FCode.equalsIgnoreCase("IDStartDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				IDStartDate = fDate.getDate( FValue );
			}
			else
				IDStartDate = null;
		}
		if (FCode.equalsIgnoreCase("IDEndDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				IDEndDate = fDate.getDate( FValue );
			}
			else
				IDEndDate = null;
		}
		if (FCode.equalsIgnoreCase("IDType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IDType = FValue.trim();
			}
			else
				IDType = null;
		}
		if (FCode.equalsIgnoreCase("IDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IDNo = FValue.trim();
			}
			else
				IDNo = null;
		}
		if (FCode.equalsIgnoreCase("IDLongEffFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IDLongEffFlag = FValue.trim();
			}
			else
				IDLongEffFlag = null;
		}
		if (FCode.equalsIgnoreCase("UnifiedSocialCreditNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UnifiedSocialCreditNo = FValue.trim();
			}
			else
				UnifiedSocialCreditNo = null;
		}
		if (FCode.equalsIgnoreCase("InsuredProperty"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuredProperty = FValue.trim();
			}
			else
				InsuredProperty = null;
		}
		if (FCode.equalsIgnoreCase("TaxpayerType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TaxpayerType = FValue.trim();
			}
			else
				TaxpayerType = null;
		}
		if (FCode.equalsIgnoreCase("CustomerBankCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerBankCode = FValue.trim();
			}
			else
				CustomerBankCode = null;
		}
		if (FCode.equalsIgnoreCase("CustomerBankAccNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerBankAccNo = FValue.trim();
			}
			else
				CustomerBankAccNo = null;
		}
		if (FCode.equalsIgnoreCase("ShareholderName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ShareholderName = FValue.trim();
			}
			else
				ShareholderName = null;
		}
		if (FCode.equalsIgnoreCase("ShareholderIDType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ShareholderIDType = FValue.trim();
			}
			else
				ShareholderIDType = null;
		}
		if (FCode.equalsIgnoreCase("ShareholderIDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ShareholderIDNo = FValue.trim();
			}
			else
				ShareholderIDNo = null;
		}
		if (FCode.equalsIgnoreCase("ShareholderIDStart"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ShareholderIDStart = fDate.getDate( FValue );
			}
			else
				ShareholderIDStart = null;
		}
		if (FCode.equalsIgnoreCase("ShareholderIDEnd"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ShareholderIDEnd = fDate.getDate( FValue );
			}
			else
				ShareholderIDEnd = null;
		}
		if (FCode.equalsIgnoreCase("ShareholderIDLongFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ShareholderIDLongFlag = FValue.trim();
			}
			else
				ShareholderIDLongFlag = null;
		}
		if (FCode.equalsIgnoreCase("LegalPersonName1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LegalPersonName1 = FValue.trim();
			}
			else
				LegalPersonName1 = null;
		}
		if (FCode.equalsIgnoreCase("LegalPersonIDType1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LegalPersonIDType1 = FValue.trim();
			}
			else
				LegalPersonIDType1 = null;
		}
		if (FCode.equalsIgnoreCase("LegalPersonIDNo1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LegalPersonIDNo1 = FValue.trim();
			}
			else
				LegalPersonIDNo1 = null;
		}
		if (FCode.equalsIgnoreCase("LegalPersonIDStart1"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				LegalPersonIDStart1 = fDate.getDate( FValue );
			}
			else
				LegalPersonIDStart1 = null;
		}
		if (FCode.equalsIgnoreCase("LegalPersonIDEnd1"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				LegalPersonIDEnd1 = fDate.getDate( FValue );
			}
			else
				LegalPersonIDEnd1 = null;
		}
		if (FCode.equalsIgnoreCase("LegalPersonIDLongFlag1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LegalPersonIDLongFlag1 = FValue.trim();
			}
			else
				LegalPersonIDLongFlag1 = null;
		}
		if (FCode.equalsIgnoreCase("ResponsibleName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ResponsibleName = FValue.trim();
			}
			else
				ResponsibleName = null;
		}
		if (FCode.equalsIgnoreCase("ResponsibleIDType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ResponsibleIDType = FValue.trim();
			}
			else
				ResponsibleIDType = null;
		}
		if (FCode.equalsIgnoreCase("ResponsibleIDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ResponsibleIDNo = FValue.trim();
			}
			else
				ResponsibleIDNo = null;
		}
		if (FCode.equalsIgnoreCase("ResponsibleIDStart"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ResponsibleIDStart = fDate.getDate( FValue );
			}
			else
				ResponsibleIDStart = null;
		}
		if (FCode.equalsIgnoreCase("ResponsibleIDEnd"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ResponsibleIDEnd = fDate.getDate( FValue );
			}
			else
				ResponsibleIDEnd = null;
		}
		if (FCode.equalsIgnoreCase("ResponsibleIDLongFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ResponsibleIDLongFlag = FValue.trim();
			}
			else
				ResponsibleIDLongFlag = null;
		}
		if (FCode.equalsIgnoreCase("Authorization"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Authorization = FValue.trim();
			}
			else
				Authorization = null;
		}
		if (FCode.equalsIgnoreCase("RelatedTags"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RelatedTags = FValue.trim();
			}
			else
				RelatedTags = null;
		}
		if (FCode.equalsIgnoreCase("UnCommonChar"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UnCommonChar = FValue.trim();
			}
			else
				UnCommonChar = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LPGrpAppntSchema other = (LPGrpAppntSchema)otherObject;
		return
			(EdorNo == null ? other.getEdorNo() == null : EdorNo.equals(other.getEdorNo()))
			&& (EdorType == null ? other.getEdorType() == null : EdorType.equals(other.getEdorType()))
			&& (GrpContNo == null ? other.getGrpContNo() == null : GrpContNo.equals(other.getGrpContNo()))
			&& (CustomerNo == null ? other.getCustomerNo() == null : CustomerNo.equals(other.getCustomerNo()))
			&& (PrtNo == null ? other.getPrtNo() == null : PrtNo.equals(other.getPrtNo()))
			&& (AddressNo == null ? other.getAddressNo() == null : AddressNo.equals(other.getAddressNo()))
			&& (AppntGrade == null ? other.getAppntGrade() == null : AppntGrade.equals(other.getAppntGrade()))
			&& (Name == null ? other.getName() == null : Name.equals(other.getName()))
			&& (PostalAddress == null ? other.getPostalAddress() == null : PostalAddress.equals(other.getPostalAddress()))
			&& (ZipCode == null ? other.getZipCode() == null : ZipCode.equals(other.getZipCode()))
			&& (Phone == null ? other.getPhone() == null : Phone.equals(other.getPhone()))
			&& (Password == null ? other.getPassword() == null : Password.equals(other.getPassword()))
			&& (State == null ? other.getState() == null : State.equals(other.getState()))
			&& (AppntType == null ? other.getAppntType() == null : AppntType.equals(other.getAppntType()))
			&& (RelationToInsured == null ? other.getRelationToInsured() == null : RelationToInsured.equals(other.getRelationToInsured()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (ClaimBankCode == null ? other.getClaimBankCode() == null : ClaimBankCode.equals(other.getClaimBankCode()))
			&& (ClaimBankAccNo == null ? other.getClaimBankAccNo() == null : ClaimBankAccNo.equals(other.getClaimBankAccNo()))
			&& (ClaimAccName == null ? other.getClaimAccName() == null : ClaimAccName.equals(other.getClaimAccName()))
			&& (OrganComCode == null ? other.getOrganComCode() == null : OrganComCode.equals(other.getOrganComCode()))
			&& Peoples == other.getPeoples()
			&& OnWorkPeoples == other.getOnWorkPeoples()
			&& OffWorkPeoples == other.getOffWorkPeoples()
			&& OtherPeoples == other.getOtherPeoples()
			&& (TaxRegistration == null ? other.getTaxRegistration() == null : TaxRegistration.equals(other.getTaxRegistration()))
			&& (BusinessLicence == null ? other.getBusinessLicence() == null : BusinessLicence.equals(other.getBusinessLicence()))
			&& (OtherCertificates == null ? other.getOtherCertificates() == null : OtherCertificates.equals(other.getOtherCertificates()))
			&& (GrpLevel == null ? other.getGrpLevel() == null : GrpLevel.equals(other.getGrpLevel()))
			&& (TaxNo == null ? other.getTaxNo() == null : TaxNo.equals(other.getTaxNo()))
			&& (LegalPersonName == null ? other.getLegalPersonName() == null : LegalPersonName.equals(other.getLegalPersonName()))
			&& (LegalPersonIDNo == null ? other.getLegalPersonIDNo() == null : LegalPersonIDNo.equals(other.getLegalPersonIDNo()))
			&& (BusinessScope == null ? other.getBusinessScope() == null : BusinessScope.equals(other.getBusinessScope()))
			&& (IDStartDate == null ? other.getIDStartDate() == null : fDate.getString(IDStartDate).equals(other.getIDStartDate()))
			&& (IDEndDate == null ? other.getIDEndDate() == null : fDate.getString(IDEndDate).equals(other.getIDEndDate()))
			&& (IDType == null ? other.getIDType() == null : IDType.equals(other.getIDType()))
			&& (IDNo == null ? other.getIDNo() == null : IDNo.equals(other.getIDNo()))
			&& (IDLongEffFlag == null ? other.getIDLongEffFlag() == null : IDLongEffFlag.equals(other.getIDLongEffFlag()))
			&& (UnifiedSocialCreditNo == null ? other.getUnifiedSocialCreditNo() == null : UnifiedSocialCreditNo.equals(other.getUnifiedSocialCreditNo()))
			&& (InsuredProperty == null ? other.getInsuredProperty() == null : InsuredProperty.equals(other.getInsuredProperty()))
			&& (TaxpayerType == null ? other.getTaxpayerType() == null : TaxpayerType.equals(other.getTaxpayerType()))
			&& (CustomerBankCode == null ? other.getCustomerBankCode() == null : CustomerBankCode.equals(other.getCustomerBankCode()))
			&& (CustomerBankAccNo == null ? other.getCustomerBankAccNo() == null : CustomerBankAccNo.equals(other.getCustomerBankAccNo()))
			&& (ShareholderName == null ? other.getShareholderName() == null : ShareholderName.equals(other.getShareholderName()))
			&& (ShareholderIDType == null ? other.getShareholderIDType() == null : ShareholderIDType.equals(other.getShareholderIDType()))
			&& (ShareholderIDNo == null ? other.getShareholderIDNo() == null : ShareholderIDNo.equals(other.getShareholderIDNo()))
			&& (ShareholderIDStart == null ? other.getShareholderIDStart() == null : fDate.getString(ShareholderIDStart).equals(other.getShareholderIDStart()))
			&& (ShareholderIDEnd == null ? other.getShareholderIDEnd() == null : fDate.getString(ShareholderIDEnd).equals(other.getShareholderIDEnd()))
			&& (ShareholderIDLongFlag == null ? other.getShareholderIDLongFlag() == null : ShareholderIDLongFlag.equals(other.getShareholderIDLongFlag()))
			&& (LegalPersonName1 == null ? other.getLegalPersonName1() == null : LegalPersonName1.equals(other.getLegalPersonName1()))
			&& (LegalPersonIDType1 == null ? other.getLegalPersonIDType1() == null : LegalPersonIDType1.equals(other.getLegalPersonIDType1()))
			&& (LegalPersonIDNo1 == null ? other.getLegalPersonIDNo1() == null : LegalPersonIDNo1.equals(other.getLegalPersonIDNo1()))
			&& (LegalPersonIDStart1 == null ? other.getLegalPersonIDStart1() == null : fDate.getString(LegalPersonIDStart1).equals(other.getLegalPersonIDStart1()))
			&& (LegalPersonIDEnd1 == null ? other.getLegalPersonIDEnd1() == null : fDate.getString(LegalPersonIDEnd1).equals(other.getLegalPersonIDEnd1()))
			&& (LegalPersonIDLongFlag1 == null ? other.getLegalPersonIDLongFlag1() == null : LegalPersonIDLongFlag1.equals(other.getLegalPersonIDLongFlag1()))
			&& (ResponsibleName == null ? other.getResponsibleName() == null : ResponsibleName.equals(other.getResponsibleName()))
			&& (ResponsibleIDType == null ? other.getResponsibleIDType() == null : ResponsibleIDType.equals(other.getResponsibleIDType()))
			&& (ResponsibleIDNo == null ? other.getResponsibleIDNo() == null : ResponsibleIDNo.equals(other.getResponsibleIDNo()))
			&& (ResponsibleIDStart == null ? other.getResponsibleIDStart() == null : fDate.getString(ResponsibleIDStart).equals(other.getResponsibleIDStart()))
			&& (ResponsibleIDEnd == null ? other.getResponsibleIDEnd() == null : fDate.getString(ResponsibleIDEnd).equals(other.getResponsibleIDEnd()))
			&& (ResponsibleIDLongFlag == null ? other.getResponsibleIDLongFlag() == null : ResponsibleIDLongFlag.equals(other.getResponsibleIDLongFlag()))
			&& (Authorization == null ? other.getAuthorization() == null : Authorization.equals(other.getAuthorization()))
			&& (RelatedTags == null ? other.getRelatedTags() == null : RelatedTags.equals(other.getRelatedTags()))
			&& (UnCommonChar == null ? other.getUnCommonChar() == null : UnCommonChar.equals(other.getUnCommonChar()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("EdorNo") ) {
			return 0;
		}
		if( strFieldName.equals("EdorType") ) {
			return 1;
		}
		if( strFieldName.equals("GrpContNo") ) {
			return 2;
		}
		if( strFieldName.equals("CustomerNo") ) {
			return 3;
		}
		if( strFieldName.equals("PrtNo") ) {
			return 4;
		}
		if( strFieldName.equals("AddressNo") ) {
			return 5;
		}
		if( strFieldName.equals("AppntGrade") ) {
			return 6;
		}
		if( strFieldName.equals("Name") ) {
			return 7;
		}
		if( strFieldName.equals("PostalAddress") ) {
			return 8;
		}
		if( strFieldName.equals("ZipCode") ) {
			return 9;
		}
		if( strFieldName.equals("Phone") ) {
			return 10;
		}
		if( strFieldName.equals("Password") ) {
			return 11;
		}
		if( strFieldName.equals("State") ) {
			return 12;
		}
		if( strFieldName.equals("AppntType") ) {
			return 13;
		}
		if( strFieldName.equals("RelationToInsured") ) {
			return 14;
		}
		if( strFieldName.equals("Operator") ) {
			return 15;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 16;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 17;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 18;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 19;
		}
		if( strFieldName.equals("ClaimBankCode") ) {
			return 20;
		}
		if( strFieldName.equals("ClaimBankAccNo") ) {
			return 21;
		}
		if( strFieldName.equals("ClaimAccName") ) {
			return 22;
		}
		if( strFieldName.equals("OrganComCode") ) {
			return 23;
		}
		if( strFieldName.equals("Peoples") ) {
			return 24;
		}
		if( strFieldName.equals("OnWorkPeoples") ) {
			return 25;
		}
		if( strFieldName.equals("OffWorkPeoples") ) {
			return 26;
		}
		if( strFieldName.equals("OtherPeoples") ) {
			return 27;
		}
		if( strFieldName.equals("TaxRegistration") ) {
			return 28;
		}
		if( strFieldName.equals("BusinessLicence") ) {
			return 29;
		}
		if( strFieldName.equals("OtherCertificates") ) {
			return 30;
		}
		if( strFieldName.equals("GrpLevel") ) {
			return 31;
		}
		if( strFieldName.equals("TaxNo") ) {
			return 32;
		}
		if( strFieldName.equals("LegalPersonName") ) {
			return 33;
		}
		if( strFieldName.equals("LegalPersonIDNo") ) {
			return 34;
		}
		if( strFieldName.equals("BusinessScope") ) {
			return 35;
		}
		if( strFieldName.equals("IDStartDate") ) {
			return 36;
		}
		if( strFieldName.equals("IDEndDate") ) {
			return 37;
		}
		if( strFieldName.equals("IDType") ) {
			return 38;
		}
		if( strFieldName.equals("IDNo") ) {
			return 39;
		}
		if( strFieldName.equals("IDLongEffFlag") ) {
			return 40;
		}
		if( strFieldName.equals("UnifiedSocialCreditNo") ) {
			return 41;
		}
		if( strFieldName.equals("InsuredProperty") ) {
			return 42;
		}
		if( strFieldName.equals("TaxpayerType") ) {
			return 43;
		}
		if( strFieldName.equals("CustomerBankCode") ) {
			return 44;
		}
		if( strFieldName.equals("CustomerBankAccNo") ) {
			return 45;
		}
		if( strFieldName.equals("ShareholderName") ) {
			return 46;
		}
		if( strFieldName.equals("ShareholderIDType") ) {
			return 47;
		}
		if( strFieldName.equals("ShareholderIDNo") ) {
			return 48;
		}
		if( strFieldName.equals("ShareholderIDStart") ) {
			return 49;
		}
		if( strFieldName.equals("ShareholderIDEnd") ) {
			return 50;
		}
		if( strFieldName.equals("ShareholderIDLongFlag") ) {
			return 51;
		}
		if( strFieldName.equals("LegalPersonName1") ) {
			return 52;
		}
		if( strFieldName.equals("LegalPersonIDType1") ) {
			return 53;
		}
		if( strFieldName.equals("LegalPersonIDNo1") ) {
			return 54;
		}
		if( strFieldName.equals("LegalPersonIDStart1") ) {
			return 55;
		}
		if( strFieldName.equals("LegalPersonIDEnd1") ) {
			return 56;
		}
		if( strFieldName.equals("LegalPersonIDLongFlag1") ) {
			return 57;
		}
		if( strFieldName.equals("ResponsibleName") ) {
			return 58;
		}
		if( strFieldName.equals("ResponsibleIDType") ) {
			return 59;
		}
		if( strFieldName.equals("ResponsibleIDNo") ) {
			return 60;
		}
		if( strFieldName.equals("ResponsibleIDStart") ) {
			return 61;
		}
		if( strFieldName.equals("ResponsibleIDEnd") ) {
			return 62;
		}
		if( strFieldName.equals("ResponsibleIDLongFlag") ) {
			return 63;
		}
		if( strFieldName.equals("Authorization") ) {
			return 64;
		}
		if( strFieldName.equals("RelatedTags") ) {
			return 65;
		}
		if( strFieldName.equals("UnCommonChar") ) {
			return 66;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "EdorNo";
				break;
			case 1:
				strFieldName = "EdorType";
				break;
			case 2:
				strFieldName = "GrpContNo";
				break;
			case 3:
				strFieldName = "CustomerNo";
				break;
			case 4:
				strFieldName = "PrtNo";
				break;
			case 5:
				strFieldName = "AddressNo";
				break;
			case 6:
				strFieldName = "AppntGrade";
				break;
			case 7:
				strFieldName = "Name";
				break;
			case 8:
				strFieldName = "PostalAddress";
				break;
			case 9:
				strFieldName = "ZipCode";
				break;
			case 10:
				strFieldName = "Phone";
				break;
			case 11:
				strFieldName = "Password";
				break;
			case 12:
				strFieldName = "State";
				break;
			case 13:
				strFieldName = "AppntType";
				break;
			case 14:
				strFieldName = "RelationToInsured";
				break;
			case 15:
				strFieldName = "Operator";
				break;
			case 16:
				strFieldName = "MakeDate";
				break;
			case 17:
				strFieldName = "MakeTime";
				break;
			case 18:
				strFieldName = "ModifyDate";
				break;
			case 19:
				strFieldName = "ModifyTime";
				break;
			case 20:
				strFieldName = "ClaimBankCode";
				break;
			case 21:
				strFieldName = "ClaimBankAccNo";
				break;
			case 22:
				strFieldName = "ClaimAccName";
				break;
			case 23:
				strFieldName = "OrganComCode";
				break;
			case 24:
				strFieldName = "Peoples";
				break;
			case 25:
				strFieldName = "OnWorkPeoples";
				break;
			case 26:
				strFieldName = "OffWorkPeoples";
				break;
			case 27:
				strFieldName = "OtherPeoples";
				break;
			case 28:
				strFieldName = "TaxRegistration";
				break;
			case 29:
				strFieldName = "BusinessLicence";
				break;
			case 30:
				strFieldName = "OtherCertificates";
				break;
			case 31:
				strFieldName = "GrpLevel";
				break;
			case 32:
				strFieldName = "TaxNo";
				break;
			case 33:
				strFieldName = "LegalPersonName";
				break;
			case 34:
				strFieldName = "LegalPersonIDNo";
				break;
			case 35:
				strFieldName = "BusinessScope";
				break;
			case 36:
				strFieldName = "IDStartDate";
				break;
			case 37:
				strFieldName = "IDEndDate";
				break;
			case 38:
				strFieldName = "IDType";
				break;
			case 39:
				strFieldName = "IDNo";
				break;
			case 40:
				strFieldName = "IDLongEffFlag";
				break;
			case 41:
				strFieldName = "UnifiedSocialCreditNo";
				break;
			case 42:
				strFieldName = "InsuredProperty";
				break;
			case 43:
				strFieldName = "TaxpayerType";
				break;
			case 44:
				strFieldName = "CustomerBankCode";
				break;
			case 45:
				strFieldName = "CustomerBankAccNo";
				break;
			case 46:
				strFieldName = "ShareholderName";
				break;
			case 47:
				strFieldName = "ShareholderIDType";
				break;
			case 48:
				strFieldName = "ShareholderIDNo";
				break;
			case 49:
				strFieldName = "ShareholderIDStart";
				break;
			case 50:
				strFieldName = "ShareholderIDEnd";
				break;
			case 51:
				strFieldName = "ShareholderIDLongFlag";
				break;
			case 52:
				strFieldName = "LegalPersonName1";
				break;
			case 53:
				strFieldName = "LegalPersonIDType1";
				break;
			case 54:
				strFieldName = "LegalPersonIDNo1";
				break;
			case 55:
				strFieldName = "LegalPersonIDStart1";
				break;
			case 56:
				strFieldName = "LegalPersonIDEnd1";
				break;
			case 57:
				strFieldName = "LegalPersonIDLongFlag1";
				break;
			case 58:
				strFieldName = "ResponsibleName";
				break;
			case 59:
				strFieldName = "ResponsibleIDType";
				break;
			case 60:
				strFieldName = "ResponsibleIDNo";
				break;
			case 61:
				strFieldName = "ResponsibleIDStart";
				break;
			case 62:
				strFieldName = "ResponsibleIDEnd";
				break;
			case 63:
				strFieldName = "ResponsibleIDLongFlag";
				break;
			case 64:
				strFieldName = "Authorization";
				break;
			case 65:
				strFieldName = "RelatedTags";
				break;
			case 66:
				strFieldName = "UnCommonChar";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("EdorNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EdorType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PrtNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AddressNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppntGrade") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Name") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PostalAddress") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ZipCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Phone") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Password") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppntType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RelationToInsured") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ClaimBankCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ClaimBankAccNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ClaimAccName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OrganComCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Peoples") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("OnWorkPeoples") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("OffWorkPeoples") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("OtherPeoples") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("TaxRegistration") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BusinessLicence") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OtherCertificates") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpLevel") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TaxNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LegalPersonName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LegalPersonIDNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BusinessScope") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IDStartDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("IDEndDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("IDType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IDNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IDLongEffFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UnifiedSocialCreditNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuredProperty") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TaxpayerType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerBankCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerBankAccNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ShareholderName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ShareholderIDType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ShareholderIDNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ShareholderIDStart") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ShareholderIDEnd") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ShareholderIDLongFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LegalPersonName1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LegalPersonIDType1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LegalPersonIDNo1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LegalPersonIDStart1") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("LegalPersonIDEnd1") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("LegalPersonIDLongFlag1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ResponsibleName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ResponsibleIDType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ResponsibleIDNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ResponsibleIDStart") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ResponsibleIDEnd") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ResponsibleIDLongFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Authorization") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RelatedTags") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UnCommonChar") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_INT;
				break;
			case 25:
				nFieldType = Schema.TYPE_INT;
				break;
			case 26:
				nFieldType = Schema.TYPE_INT;
				break;
			case 27:
				nFieldType = Schema.TYPE_INT;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 36:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 37:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 38:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 39:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 40:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 41:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 42:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 43:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 44:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 45:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 46:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 47:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 48:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 49:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 50:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 51:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 52:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 53:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 54:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 55:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 56:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 57:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 58:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 59:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 60:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 61:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 62:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 63:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 64:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 65:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 66:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
