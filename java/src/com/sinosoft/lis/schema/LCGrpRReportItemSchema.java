/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LCGrpRReportItemDB;

/*
 * <p>ClassName: LCGrpRReportItemSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-08-09
 */
public class LCGrpRReportItemSchema implements Schema, Cloneable
{
    // @Field
    /** 集体合同号码 */
    private String GrpContNo;
    /** 打印流水号 */
    private String PrtSeq;
    /** 生调项目编号 */
    private String RReportItemCode;
    /** 生调项目名称 */
    private String RReportItemName;
    /** 备注 */
    private String Remark;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 截止日期 */
    private Date EndDate;

    public static final int FIELDNUM = 11; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LCGrpRReportItemSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "PrtSeq";
        pk[1] = "RReportItemCode";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException
    {
        LCGrpRReportItemSchema cloned = (LCGrpRReportItemSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getGrpContNo()
    {
        return GrpContNo;
    }

    public void setGrpContNo(String aGrpContNo)
    {
        GrpContNo = aGrpContNo;
    }

    public String getPrtSeq()
    {
        return PrtSeq;
    }

    public void setPrtSeq(String aPrtSeq)
    {
        PrtSeq = aPrtSeq;
    }

    public String getRReportItemCode()
    {
        return RReportItemCode;
    }

    public void setRReportItemCode(String aRReportItemCode)
    {
        RReportItemCode = aRReportItemCode;
    }

    public String getRReportItemName()
    {
        return RReportItemName;
    }

    public void setRReportItemName(String aRReportItemName)
    {
        RReportItemName = aRReportItemName;
    }

    public String getRemark()
    {
        return Remark;
    }

    public void setRemark(String aRemark)
    {
        Remark = aRemark;
    }

    public String getOperator()
    {
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    public String getEndDate()
    {
        if (EndDate != null)
        {
            return fDate.getString(EndDate);
        }
        else
        {
            return null;
        }
    }

    public void setEndDate(Date aEndDate)
    {
        EndDate = aEndDate;
    }

    public void setEndDate(String aEndDate)
    {
        if (aEndDate != null && !aEndDate.equals(""))
        {
            EndDate = fDate.getDate(aEndDate);
        }
        else
        {
            EndDate = null;
        }
    }


    /**
     * 使用另外一个 LCGrpRReportItemSchema 对象给 Schema 赋值
     * @param: aLCGrpRReportItemSchema LCGrpRReportItemSchema
     **/
    public void setSchema(LCGrpRReportItemSchema aLCGrpRReportItemSchema)
    {
        this.GrpContNo = aLCGrpRReportItemSchema.getGrpContNo();
        this.PrtSeq = aLCGrpRReportItemSchema.getPrtSeq();
        this.RReportItemCode = aLCGrpRReportItemSchema.getRReportItemCode();
        this.RReportItemName = aLCGrpRReportItemSchema.getRReportItemName();
        this.Remark = aLCGrpRReportItemSchema.getRemark();
        this.Operator = aLCGrpRReportItemSchema.getOperator();
        this.MakeDate = fDate.getDate(aLCGrpRReportItemSchema.getMakeDate());
        this.MakeTime = aLCGrpRReportItemSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLCGrpRReportItemSchema.getModifyDate());
        this.ModifyTime = aLCGrpRReportItemSchema.getModifyTime();
        this.EndDate = fDate.getDate(aLCGrpRReportItemSchema.getEndDate());
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("GrpContNo") == null)
            {
                this.GrpContNo = null;
            }
            else
            {
                this.GrpContNo = rs.getString("GrpContNo").trim();
            }

            if (rs.getString("PrtSeq") == null)
            {
                this.PrtSeq = null;
            }
            else
            {
                this.PrtSeq = rs.getString("PrtSeq").trim();
            }

            if (rs.getString("RReportItemCode") == null)
            {
                this.RReportItemCode = null;
            }
            else
            {
                this.RReportItemCode = rs.getString("RReportItemCode").trim();
            }

            if (rs.getString("RReportItemName") == null)
            {
                this.RReportItemName = null;
            }
            else
            {
                this.RReportItemName = rs.getString("RReportItemName").trim();
            }

            if (rs.getString("Remark") == null)
            {
                this.Remark = null;
            }
            else
            {
                this.Remark = rs.getString("Remark").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            this.EndDate = rs.getDate("EndDate");
        }
        catch (SQLException sqle)
        {
            System.out.println("数据库中的LCGrpRReportItem表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCGrpRReportItemSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LCGrpRReportItemSchema getSchema()
    {
        LCGrpRReportItemSchema aLCGrpRReportItemSchema = new
                LCGrpRReportItemSchema();
        aLCGrpRReportItemSchema.setSchema(this);
        return aLCGrpRReportItemSchema;
    }

    public LCGrpRReportItemDB getDB()
    {
        LCGrpRReportItemDB aDBOper = new LCGrpRReportItemDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCGrpRReportItem描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(GrpContNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PrtSeq));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RReportItemCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RReportItemName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Remark));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(EndDate)));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCGrpRReportItem>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            PrtSeq = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                    SysConst.PACKAGESPILTER);
            RReportItemCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             3, SysConst.PACKAGESPILTER);
            RReportItemName = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             4, SysConst.PACKAGESPILTER);
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                    SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 7, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 9, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                        SysConst.PACKAGESPILTER);
            EndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 11, SysConst.PACKAGESPILTER));
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCGrpRReportItemSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("GrpContNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equals("PrtSeq"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtSeq));
        }
        if (FCode.equals("RReportItemCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RReportItemCode));
        }
        if (FCode.equals("RReportItemName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RReportItemName));
        }
        if (FCode.equals("Remark"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("EndDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getEndDate()));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(GrpContNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(PrtSeq);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(RReportItemCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(RReportItemName);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getEndDate()));
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equalsIgnoreCase("GrpContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
            {
                GrpContNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("PrtSeq"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PrtSeq = FValue.trim();
            }
            else
            {
                PrtSeq = null;
            }
        }
        if (FCode.equalsIgnoreCase("RReportItemCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RReportItemCode = FValue.trim();
            }
            else
            {
                RReportItemCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("RReportItemName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RReportItemName = FValue.trim();
            }
            else
            {
                RReportItemName = null;
            }
        }
        if (FCode.equalsIgnoreCase("Remark"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
            {
                Remark = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("EndDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EndDate = fDate.getDate(FValue);
            }
            else
            {
                EndDate = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LCGrpRReportItemSchema other = (LCGrpRReportItemSchema) otherObject;
        return
                GrpContNo.equals(other.getGrpContNo())
                && PrtSeq.equals(other.getPrtSeq())
                && RReportItemCode.equals(other.getRReportItemCode())
                && RReportItemName.equals(other.getRReportItemName())
                && Remark.equals(other.getRemark())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && fDate.getString(EndDate).equals(other.getEndDate());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("GrpContNo"))
        {
            return 0;
        }
        if (strFieldName.equals("PrtSeq"))
        {
            return 1;
        }
        if (strFieldName.equals("RReportItemCode"))
        {
            return 2;
        }
        if (strFieldName.equals("RReportItemName"))
        {
            return 3;
        }
        if (strFieldName.equals("Remark"))
        {
            return 4;
        }
        if (strFieldName.equals("Operator"))
        {
            return 5;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 6;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 7;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 8;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 9;
        }
        if (strFieldName.equals("EndDate"))
        {
            return 10;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "GrpContNo";
                break;
            case 1:
                strFieldName = "PrtSeq";
                break;
            case 2:
                strFieldName = "RReportItemCode";
                break;
            case 3:
                strFieldName = "RReportItemName";
                break;
            case 4:
                strFieldName = "Remark";
                break;
            case 5:
                strFieldName = "Operator";
                break;
            case 6:
                strFieldName = "MakeDate";
                break;
            case 7:
                strFieldName = "MakeTime";
                break;
            case 8:
                strFieldName = "ModifyDate";
                break;
            case 9:
                strFieldName = "ModifyTime";
                break;
            case 10:
                strFieldName = "EndDate";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("GrpContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PrtSeq"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RReportItemCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RReportItemName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Remark"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EndDate"))
        {
            return Schema.TYPE_DATE;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_DATE;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
