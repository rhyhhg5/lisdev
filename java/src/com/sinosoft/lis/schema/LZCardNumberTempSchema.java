/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LZCardNumberTempDB;

/*
 * <p>ClassName: LZCardNumberTempSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 单证编码
 * @CreateDate：2006-09-05
 */
public class LZCardNumberTempSchema implements Schema, Cloneable {
    // @Field
    /** 印刷批次号 */
    private String PrtNo;
    /** 单证类型 */
    private String CardType;
    /** 业务类型 */
    private String OperateType;
    /** 单证密码 */
    private String CardPassword;
    /** 单证密码（密） */
    private String CardPassword2;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最近一次修改日期 */
    private Date ModifyDate;
    /** 最近一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 10; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LZCardNumberTempSchema() {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "PrtNo";
        pk[1] = "CardType";
        pk[2] = "CardPassword";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LZCardNumberTempSchema cloned = (LZCardNumberTempSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getPrtNo() {
        return PrtNo;
    }

    public void setPrtNo(String aPrtNo) {
        PrtNo = aPrtNo;
    }

    public String getCardType() {
        return CardType;
    }

    public void setCardType(String aCardType) {
        CardType = aCardType;
    }

    public String getOperateType() {
        return OperateType;
    }

    public void setOperateType(String aOperateType) {
        OperateType = aOperateType;
    }

    public String getCardPassword() {
        return CardPassword;
    }

    public void setCardPassword(String aCardPassword) {
        CardPassword = aCardPassword;
    }

    public String getCardPassword2() {
        return CardPassword2;
    }

    public void setCardPassword2(String aCardPassword2) {
        CardPassword2 = aCardPassword2;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LZCardNumberTempSchema 对象给 Schema 赋值
     * @param: aLZCardNumberTempSchema LZCardNumberTempSchema
     **/
    public void setSchema(LZCardNumberTempSchema aLZCardNumberTempSchema) {
        this.PrtNo = aLZCardNumberTempSchema.getPrtNo();
        this.CardType = aLZCardNumberTempSchema.getCardType();
        this.OperateType = aLZCardNumberTempSchema.getOperateType();
        this.CardPassword = aLZCardNumberTempSchema.getCardPassword();
        this.CardPassword2 = aLZCardNumberTempSchema.getCardPassword2();
        this.Operator = aLZCardNumberTempSchema.getOperator();
        this.MakeDate = fDate.getDate(aLZCardNumberTempSchema.getMakeDate());
        this.MakeTime = aLZCardNumberTempSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLZCardNumberTempSchema.getModifyDate());
        this.ModifyTime = aLZCardNumberTempSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("PrtNo") == null) {
                this.PrtNo = null;
            } else {
                this.PrtNo = rs.getString("PrtNo").trim();
            }

            if (rs.getString("CardType") == null) {
                this.CardType = null;
            } else {
                this.CardType = rs.getString("CardType").trim();
            }

            if (rs.getString("OperateType") == null) {
                this.OperateType = null;
            } else {
                this.OperateType = rs.getString("OperateType").trim();
            }

            if (rs.getString("CardPassword") == null) {
                this.CardPassword = null;
            } else {
                this.CardPassword = rs.getString("CardPassword").trim();
            }

            if (rs.getString("CardPassword2") == null) {
                this.CardPassword2 = null;
            } else {
                this.CardPassword2 = rs.getString("CardPassword2").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LZCardNumberTemp表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LZCardNumberTempSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LZCardNumberTempSchema getSchema() {
        LZCardNumberTempSchema aLZCardNumberTempSchema = new
                LZCardNumberTempSchema();
        aLZCardNumberTempSchema.setSchema(this);
        return aLZCardNumberTempSchema;
    }

    public LZCardNumberTempDB getDB() {
        LZCardNumberTempDB aDBOper = new LZCardNumberTempDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLZCardNumberTemp描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(PrtNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CardType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OperateType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CardPassword));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CardPassword2));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLZCardNumberTemp>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                   SysConst.PACKAGESPILTER);
            CardType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            OperateType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                         SysConst.PACKAGESPILTER);
            CardPassword = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                          SysConst.PACKAGESPILTER);
            CardPassword2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                           SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 7, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 9, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                        SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LZCardNumberTempSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("PrtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equals("CardType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CardType));
        }
        if (FCode.equals("OperateType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OperateType));
        }
        if (FCode.equals("CardPassword")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CardPassword));
        }
        if (FCode.equals("CardPassword2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CardPassword2));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(PrtNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(CardType);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(OperateType);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(CardPassword);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(CardPassword2);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("PrtNo")) {
            if (FValue != null && !FValue.equals("")) {
                PrtNo = FValue.trim();
            } else {
                PrtNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("CardType")) {
            if (FValue != null && !FValue.equals("")) {
                CardType = FValue.trim();
            } else {
                CardType = null;
            }
        }
        if (FCode.equalsIgnoreCase("OperateType")) {
            if (FValue != null && !FValue.equals("")) {
                OperateType = FValue.trim();
            } else {
                OperateType = null;
            }
        }
        if (FCode.equalsIgnoreCase("CardPassword")) {
            if (FValue != null && !FValue.equals("")) {
                CardPassword = FValue.trim();
            } else {
                CardPassword = null;
            }
        }
        if (FCode.equalsIgnoreCase("CardPassword2")) {
            if (FValue != null && !FValue.equals("")) {
                CardPassword2 = FValue.trim();
            } else {
                CardPassword2 = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LZCardNumberTempSchema other = (LZCardNumberTempSchema) otherObject;
        return
                PrtNo.equals(other.getPrtNo())
                && CardType.equals(other.getCardType())
                && OperateType.equals(other.getOperateType())
                && CardPassword.equals(other.getCardPassword())
                && CardPassword2.equals(other.getCardPassword2())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("PrtNo")) {
            return 0;
        }
        if (strFieldName.equals("CardType")) {
            return 1;
        }
        if (strFieldName.equals("OperateType")) {
            return 2;
        }
        if (strFieldName.equals("CardPassword")) {
            return 3;
        }
        if (strFieldName.equals("CardPassword2")) {
            return 4;
        }
        if (strFieldName.equals("Operator")) {
            return 5;
        }
        if (strFieldName.equals("MakeDate")) {
            return 6;
        }
        if (strFieldName.equals("MakeTime")) {
            return 7;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 8;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 9;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "PrtNo";
            break;
        case 1:
            strFieldName = "CardType";
            break;
        case 2:
            strFieldName = "OperateType";
            break;
        case 3:
            strFieldName = "CardPassword";
            break;
        case 4:
            strFieldName = "CardPassword2";
            break;
        case 5:
            strFieldName = "Operator";
            break;
        case 6:
            strFieldName = "MakeDate";
            break;
        case 7:
            strFieldName = "MakeTime";
            break;
        case 8:
            strFieldName = "ModifyDate";
            break;
        case 9:
            strFieldName = "ModifyTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("PrtNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CardType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OperateType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CardPassword")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CardPassword2")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
