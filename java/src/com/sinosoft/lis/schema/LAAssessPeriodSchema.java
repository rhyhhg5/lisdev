/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LAAssessPeriodDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LAAssessPeriodSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-03-30
 */
public class LAAssessPeriodSchema implements Schema
{
    // @Field
    /** 代理人职级 */
    private String AgentGrade;
    /** 考核类型 */
    private String AssessType;
    /** 地区类型 */
    private String AreaType;
    /** 考核期限 */
    private String LimitPeriod;
    /** 考核月数 */
    private int AssessMonths;
    /** 展业类型 */
    private String BranchType;
    /** 备注 */
    private String Noti;
    /** 渠道 */
    private String BranchType2;

    public static final int FIELDNUM = 8; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LAAssessPeriodSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[4];
        pk[0] = "AgentGrade";
        pk[1] = "AssessType";
        pk[2] = "AreaType";
        pk[3] = "LimitPeriod";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getAgentGrade()
    {
        if (SysConst.CHANGECHARSET && AgentGrade != null &&
            !AgentGrade.equals(""))
        {
            AgentGrade = StrTool.unicodeToGBK(AgentGrade);
        }
        return AgentGrade;
    }

    public void setAgentGrade(String aAgentGrade)
    {
        AgentGrade = aAgentGrade;
    }

    public String getAssessType()
    {
        if (SysConst.CHANGECHARSET && AssessType != null &&
            !AssessType.equals(""))
        {
            AssessType = StrTool.unicodeToGBK(AssessType);
        }
        return AssessType;
    }

    public void setAssessType(String aAssessType)
    {
        AssessType = aAssessType;
    }

    public String getAreaType()
    {
        if (SysConst.CHANGECHARSET && AreaType != null && !AreaType.equals(""))
        {
            AreaType = StrTool.unicodeToGBK(AreaType);
        }
        return AreaType;
    }

    public void setAreaType(String aAreaType)
    {
        AreaType = aAreaType;
    }

    public String getLimitPeriod()
    {
        if (SysConst.CHANGECHARSET && LimitPeriod != null &&
            !LimitPeriod.equals(""))
        {
            LimitPeriod = StrTool.unicodeToGBK(LimitPeriod);
        }
        return LimitPeriod;
    }

    public void setLimitPeriod(String aLimitPeriod)
    {
        LimitPeriod = aLimitPeriod;
    }

    public int getAssessMonths()
    {
        return AssessMonths;
    }

    public void setAssessMonths(int aAssessMonths)
    {
        AssessMonths = aAssessMonths;
    }

    public void setAssessMonths(String aAssessMonths)
    {
        if (aAssessMonths != null && !aAssessMonths.equals(""))
        {
            Integer tInteger = new Integer(aAssessMonths);
            int i = tInteger.intValue();
            AssessMonths = i;
        }
    }

    public String getBranchType()
    {
        if (SysConst.CHANGECHARSET && BranchType != null &&
            !BranchType.equals(""))
        {
            BranchType = StrTool.unicodeToGBK(BranchType);
        }
        return BranchType;
    }

    public void setBranchType(String aBranchType)
    {
        BranchType = aBranchType;
    }

    public String getNoti()
    {
        if (SysConst.CHANGECHARSET && Noti != null && !Noti.equals(""))
        {
            Noti = StrTool.unicodeToGBK(Noti);
        }
        return Noti;
    }

    public void setNoti(String aNoti)
    {
        Noti = aNoti;
    }

    public String getBranchType2()
    {
        if (SysConst.CHANGECHARSET && BranchType2 != null &&
            !BranchType2.equals(""))
        {
            BranchType2 = StrTool.unicodeToGBK(BranchType2);
        }
        return BranchType2;
    }

    public void setBranchType2(String aBranchType2)
    {
        BranchType2 = aBranchType2;
    }

    /**
     * 使用另外一个 LAAssessPeriodSchema 对象给 Schema 赋值
     * @param: aLAAssessPeriodSchema LAAssessPeriodSchema
     **/
    public void setSchema(LAAssessPeriodSchema aLAAssessPeriodSchema)
    {
        this.AgentGrade = aLAAssessPeriodSchema.getAgentGrade();
        this.AssessType = aLAAssessPeriodSchema.getAssessType();
        this.AreaType = aLAAssessPeriodSchema.getAreaType();
        this.LimitPeriod = aLAAssessPeriodSchema.getLimitPeriod();
        this.AssessMonths = aLAAssessPeriodSchema.getAssessMonths();
        this.BranchType = aLAAssessPeriodSchema.getBranchType();
        this.Noti = aLAAssessPeriodSchema.getNoti();
        this.BranchType2 = aLAAssessPeriodSchema.getBranchType2();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("AgentGrade") == null)
            {
                this.AgentGrade = null;
            }
            else
            {
                this.AgentGrade = rs.getString("AgentGrade").trim();
            }

            if (rs.getString("AssessType") == null)
            {
                this.AssessType = null;
            }
            else
            {
                this.AssessType = rs.getString("AssessType").trim();
            }

            if (rs.getString("AreaType") == null)
            {
                this.AreaType = null;
            }
            else
            {
                this.AreaType = rs.getString("AreaType").trim();
            }

            if (rs.getString("LimitPeriod") == null)
            {
                this.LimitPeriod = null;
            }
            else
            {
                this.LimitPeriod = rs.getString("LimitPeriod").trim();
            }

            this.AssessMonths = rs.getInt("AssessMonths");
            if (rs.getString("BranchType") == null)
            {
                this.BranchType = null;
            }
            else
            {
                this.BranchType = rs.getString("BranchType").trim();
            }

            if (rs.getString("Noti") == null)
            {
                this.Noti = null;
            }
            else
            {
                this.Noti = rs.getString("Noti").trim();
            }

            if (rs.getString("BranchType2") == null)
            {
                this.BranchType2 = null;
            }
            else
            {
                this.BranchType2 = rs.getString("BranchType2").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAssessPeriodSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LAAssessPeriodSchema getSchema()
    {
        LAAssessPeriodSchema aLAAssessPeriodSchema = new LAAssessPeriodSchema();
        aLAAssessPeriodSchema.setSchema(this);
        return aLAAssessPeriodSchema;
    }

    public LAAssessPeriodDB getDB()
    {
        LAAssessPeriodDB aDBOper = new LAAssessPeriodDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAAssessPeriod描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AgentGrade)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AssessType)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AreaType)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(LimitPeriod)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(AssessMonths));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BranchType)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Noti)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BranchType2)));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAAssessPeriod>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            AgentGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                        SysConst.PACKAGESPILTER);
            AssessType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
            AreaType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            LimitPeriod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                         SysConst.PACKAGESPILTER);
            AssessMonths = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 5, SysConst.PACKAGESPILTER))).intValue();
            BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                        SysConst.PACKAGESPILTER);
            Noti = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                  SysConst.PACKAGESPILTER);
            BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                         SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAssessPeriodSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("AgentGrade"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGrade));
        }
        if (FCode.equals("AssessType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AssessType));
        }
        if (FCode.equals("AreaType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AreaType));
        }
        if (FCode.equals("LimitPeriod"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LimitPeriod));
        }
        if (FCode.equals("AssessMonths"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AssessMonths));
        }
        if (FCode.equals("BranchType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
        }
        if (FCode.equals("Noti"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Noti));
        }
        if (FCode.equals("BranchType2"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(AgentGrade);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(AssessType);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(AreaType);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(LimitPeriod);
                break;
            case 4:
                strFieldValue = String.valueOf(AssessMonths);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(BranchType);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(Noti);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(BranchType2);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("AgentGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentGrade = FValue.trim();
            }
            else
            {
                AgentGrade = null;
            }
        }
        if (FCode.equals("AssessType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AssessType = FValue.trim();
            }
            else
            {
                AssessType = null;
            }
        }
        if (FCode.equals("AreaType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AreaType = FValue.trim();
            }
            else
            {
                AreaType = null;
            }
        }
        if (FCode.equals("LimitPeriod"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                LimitPeriod = FValue.trim();
            }
            else
            {
                LimitPeriod = null;
            }
        }
        if (FCode.equals("AssessMonths"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                AssessMonths = i;
            }
        }
        if (FCode.equals("BranchType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchType = FValue.trim();
            }
            else
            {
                BranchType = null;
            }
        }
        if (FCode.equals("Noti"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Noti = FValue.trim();
            }
            else
            {
                Noti = null;
            }
        }
        if (FCode.equals("BranchType2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchType2 = FValue.trim();
            }
            else
            {
                BranchType2 = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LAAssessPeriodSchema other = (LAAssessPeriodSchema) otherObject;
        return
                AgentGrade.equals(other.getAgentGrade())
                && AssessType.equals(other.getAssessType())
                && AreaType.equals(other.getAreaType())
                && LimitPeriod.equals(other.getLimitPeriod())
                && AssessMonths == other.getAssessMonths()
                && BranchType.equals(other.getBranchType())
                && Noti.equals(other.getNoti())
                && BranchType2.equals(other.getBranchType2());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("AgentGrade"))
        {
            return 0;
        }
        if (strFieldName.equals("AssessType"))
        {
            return 1;
        }
        if (strFieldName.equals("AreaType"))
        {
            return 2;
        }
        if (strFieldName.equals("LimitPeriod"))
        {
            return 3;
        }
        if (strFieldName.equals("AssessMonths"))
        {
            return 4;
        }
        if (strFieldName.equals("BranchType"))
        {
            return 5;
        }
        if (strFieldName.equals("Noti"))
        {
            return 6;
        }
        if (strFieldName.equals("BranchType2"))
        {
            return 7;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "AgentGrade";
                break;
            case 1:
                strFieldName = "AssessType";
                break;
            case 2:
                strFieldName = "AreaType";
                break;
            case 3:
                strFieldName = "LimitPeriod";
                break;
            case 4:
                strFieldName = "AssessMonths";
                break;
            case 5:
                strFieldName = "BranchType";
                break;
            case 6:
                strFieldName = "Noti";
                break;
            case 7:
                strFieldName = "BranchType2";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("AgentGrade"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AssessType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AreaType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("LimitPeriod"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AssessMonths"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("BranchType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Noti"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchType2"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_INT;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
