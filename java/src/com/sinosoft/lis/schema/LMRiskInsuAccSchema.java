/*
 * <p>ClassName: LMRiskInsuAccSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-01-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LMRiskInsuAccDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LMRiskInsuAccSchema implements Schema
{
    // @Field
    /** 保险帐户号码 */
    private String InsuAccNo;
    /** 账号类型 */
    private String AccType;
    /** 账户分类 */
    private String AccKind;
    /** 保险帐户名称 */
    private String InsuAccName;
    /** 账户产生位置 */
    private String AccCreatePos;
    /** 账号产生规则 */
    private String AccCreateType;
    /** 账户固定利率 */
    private double AccRate;
    /** 账户对应利率表 */
    private String AccRateTable;
    /** 账户结清计算公式 */
    private String AccCancelCode;
    /** 账户结算方式 */
    private String AccComputeFlag;
    /** 投资类型 */
    private String InvestType;
    /** 基金公司代码 */
    private String FundCompanyCode;
    /** 账户所有者 */
    private String Owner;

    public static final int FIELDNUM = 13; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LMRiskInsuAccSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "InsuAccNo";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getInsuAccNo()
    {
        if (InsuAccNo != null && !InsuAccNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            InsuAccNo = StrTool.unicodeToGBK(InsuAccNo);
        }
        return InsuAccNo;
    }

    public void setInsuAccNo(String aInsuAccNo)
    {
        InsuAccNo = aInsuAccNo;
    }

    public String getAccType()
    {
        if (AccType != null && !AccType.equals("") && SysConst.CHANGECHARSET == true)
        {
            AccType = StrTool.unicodeToGBK(AccType);
        }
        return AccType;
    }

    public void setAccType(String aAccType)
    {
        AccType = aAccType;
    }

    public String getAccKind()
    {
        if (AccKind != null && !AccKind.equals("") && SysConst.CHANGECHARSET == true)
        {
            AccKind = StrTool.unicodeToGBK(AccKind);
        }
        return AccKind;
    }

    public void setAccKind(String aAccKind)
    {
        AccKind = aAccKind;
    }

    public String getInsuAccName()
    {
        if (InsuAccName != null && !InsuAccName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            InsuAccName = StrTool.unicodeToGBK(InsuAccName);
        }
        return InsuAccName;
    }

    public void setInsuAccName(String aInsuAccName)
    {
        InsuAccName = aInsuAccName;
    }

    public String getAccCreatePos()
    {
        if (AccCreatePos != null && !AccCreatePos.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AccCreatePos = StrTool.unicodeToGBK(AccCreatePos);
        }
        return AccCreatePos;
    }

    public void setAccCreatePos(String aAccCreatePos)
    {
        AccCreatePos = aAccCreatePos;
    }

    public String getAccCreateType()
    {
        if (AccCreateType != null && !AccCreateType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AccCreateType = StrTool.unicodeToGBK(AccCreateType);
        }
        return AccCreateType;
    }

    public void setAccCreateType(String aAccCreateType)
    {
        AccCreateType = aAccCreateType;
    }

    public double getAccRate()
    {
        return AccRate;
    }

    public void setAccRate(double aAccRate)
    {
        AccRate = aAccRate;
    }

    public void setAccRate(String aAccRate)
    {
        if (aAccRate != null && !aAccRate.equals(""))
        {
            Double tDouble = new Double(aAccRate);
            double d = tDouble.doubleValue();
            AccRate = d;
        }
    }

    public String getAccRateTable()
    {
        if (AccRateTable != null && !AccRateTable.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AccRateTable = StrTool.unicodeToGBK(AccRateTable);
        }
        return AccRateTable;
    }

    public void setAccRateTable(String aAccRateTable)
    {
        AccRateTable = aAccRateTable;
    }

    public String getAccCancelCode()
    {
        if (AccCancelCode != null && !AccCancelCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AccCancelCode = StrTool.unicodeToGBK(AccCancelCode);
        }
        return AccCancelCode;
    }

    public void setAccCancelCode(String aAccCancelCode)
    {
        AccCancelCode = aAccCancelCode;
    }

    public String getAccComputeFlag()
    {
        if (AccComputeFlag != null && !AccComputeFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AccComputeFlag = StrTool.unicodeToGBK(AccComputeFlag);
        }
        return AccComputeFlag;
    }

    public void setAccComputeFlag(String aAccComputeFlag)
    {
        AccComputeFlag = aAccComputeFlag;
    }

    public String getInvestType()
    {
        if (InvestType != null && !InvestType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            InvestType = StrTool.unicodeToGBK(InvestType);
        }
        return InvestType;
    }

    public void setInvestType(String aInvestType)
    {
        InvestType = aInvestType;
    }

    public String getFundCompanyCode()
    {
        if (FundCompanyCode != null && !FundCompanyCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            FundCompanyCode = StrTool.unicodeToGBK(FundCompanyCode);
        }
        return FundCompanyCode;
    }

    public void setFundCompanyCode(String aFundCompanyCode)
    {
        FundCompanyCode = aFundCompanyCode;
    }

    public String getOwner()
    {
        if (Owner != null && !Owner.equals("") && SysConst.CHANGECHARSET == true)
        {
            Owner = StrTool.unicodeToGBK(Owner);
        }
        return Owner;
    }

    public void setOwner(String aOwner)
    {
        Owner = aOwner;
    }

    /**
     * 使用另外一个 LMRiskInsuAccSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LMRiskInsuAccSchema aLMRiskInsuAccSchema)
    {
        this.InsuAccNo = aLMRiskInsuAccSchema.getInsuAccNo();
        this.AccType = aLMRiskInsuAccSchema.getAccType();
        this.AccKind = aLMRiskInsuAccSchema.getAccKind();
        this.InsuAccName = aLMRiskInsuAccSchema.getInsuAccName();
        this.AccCreatePos = aLMRiskInsuAccSchema.getAccCreatePos();
        this.AccCreateType = aLMRiskInsuAccSchema.getAccCreateType();
        this.AccRate = aLMRiskInsuAccSchema.getAccRate();
        this.AccRateTable = aLMRiskInsuAccSchema.getAccRateTable();
        this.AccCancelCode = aLMRiskInsuAccSchema.getAccCancelCode();
        this.AccComputeFlag = aLMRiskInsuAccSchema.getAccComputeFlag();
        this.InvestType = aLMRiskInsuAccSchema.getInvestType();
        this.FundCompanyCode = aLMRiskInsuAccSchema.getFundCompanyCode();
        this.Owner = aLMRiskInsuAccSchema.getOwner();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("InsuAccNo") == null)
            {
                this.InsuAccNo = null;
            }
            else
            {
                this.InsuAccNo = rs.getString("InsuAccNo").trim();
            }

            if (rs.getString("AccType") == null)
            {
                this.AccType = null;
            }
            else
            {
                this.AccType = rs.getString("AccType").trim();
            }

            if (rs.getString("AccKind") == null)
            {
                this.AccKind = null;
            }
            else
            {
                this.AccKind = rs.getString("AccKind").trim();
            }

            if (rs.getString("InsuAccName") == null)
            {
                this.InsuAccName = null;
            }
            else
            {
                this.InsuAccName = rs.getString("InsuAccName").trim();
            }

            if (rs.getString("AccCreatePos") == null)
            {
                this.AccCreatePos = null;
            }
            else
            {
                this.AccCreatePos = rs.getString("AccCreatePos").trim();
            }

            if (rs.getString("AccCreateType") == null)
            {
                this.AccCreateType = null;
            }
            else
            {
                this.AccCreateType = rs.getString("AccCreateType").trim();
            }

            this.AccRate = rs.getDouble("AccRate");
            if (rs.getString("AccRateTable") == null)
            {
                this.AccRateTable = null;
            }
            else
            {
                this.AccRateTable = rs.getString("AccRateTable").trim();
            }

            if (rs.getString("AccCancelCode") == null)
            {
                this.AccCancelCode = null;
            }
            else
            {
                this.AccCancelCode = rs.getString("AccCancelCode").trim();
            }

            if (rs.getString("AccComputeFlag") == null)
            {
                this.AccComputeFlag = null;
            }
            else
            {
                this.AccComputeFlag = rs.getString("AccComputeFlag").trim();
            }

            if (rs.getString("InvestType") == null)
            {
                this.InvestType = null;
            }
            else
            {
                this.InvestType = rs.getString("InvestType").trim();
            }

            if (rs.getString("FundCompanyCode") == null)
            {
                this.FundCompanyCode = null;
            }
            else
            {
                this.FundCompanyCode = rs.getString("FundCompanyCode").trim();
            }

            if (rs.getString("Owner") == null)
            {
                this.Owner = null;
            }
            else
            {
                this.Owner = rs.getString("Owner").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMRiskInsuAccSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LMRiskInsuAccSchema getSchema()
    {
        LMRiskInsuAccSchema aLMRiskInsuAccSchema = new LMRiskInsuAccSchema();
        aLMRiskInsuAccSchema.setSchema(this);
        return aLMRiskInsuAccSchema;
    }

    public LMRiskInsuAccDB getDB()
    {
        LMRiskInsuAccDB aDBOper = new LMRiskInsuAccDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMRiskInsuAcc描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(InsuAccNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AccType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AccKind)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InsuAccName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AccCreatePos)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AccCreateType)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(AccRate) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AccRateTable)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AccCancelCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AccComputeFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InvestType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(FundCompanyCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Owner));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMRiskInsuAcc>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            InsuAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            AccType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                     SysConst.PACKAGESPILTER);
            AccKind = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                     SysConst.PACKAGESPILTER);
            InsuAccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                         SysConst.PACKAGESPILTER);
            AccCreatePos = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                          SysConst.PACKAGESPILTER);
            AccCreateType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                           SysConst.PACKAGESPILTER);
            AccRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 7, SysConst.PACKAGESPILTER))).doubleValue();
            AccRateTable = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                          SysConst.PACKAGESPILTER);
            AccCancelCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                           SysConst.PACKAGESPILTER);
            AccComputeFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                            10, SysConst.PACKAGESPILTER);
            InvestType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                        SysConst.PACKAGESPILTER);
            FundCompanyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             12, SysConst.PACKAGESPILTER);
            Owner = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                   SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMRiskInsuAccSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("InsuAccNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(InsuAccNo));
        }
        if (FCode.equals("AccType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AccType));
        }
        if (FCode.equals("AccKind"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AccKind));
        }
        if (FCode.equals("InsuAccName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(InsuAccName));
        }
        if (FCode.equals("AccCreatePos"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AccCreatePos));
        }
        if (FCode.equals("AccCreateType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AccCreateType));
        }
        if (FCode.equals("AccRate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AccRate));
        }
        if (FCode.equals("AccRateTable"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AccRateTable));
        }
        if (FCode.equals("AccCancelCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AccCancelCode));
        }
        if (FCode.equals("AccComputeFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AccComputeFlag));
        }
        if (FCode.equals("InvestType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(InvestType));
        }
        if (FCode.equals("FundCompanyCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(FundCompanyCode));
        }
        if (FCode.equals("Owner"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Owner));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(InsuAccNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(AccType);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(AccKind);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(InsuAccName);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(AccCreatePos);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(AccCreateType);
                break;
            case 6:
                strFieldValue = String.valueOf(AccRate);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(AccRateTable);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(AccCancelCode);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(AccComputeFlag);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(InvestType);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(FundCompanyCode);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(Owner);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("InsuAccNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InsuAccNo = FValue.trim();
            }
            else
            {
                InsuAccNo = null;
            }
        }
        if (FCode.equals("AccType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AccType = FValue.trim();
            }
            else
            {
                AccType = null;
            }
        }
        if (FCode.equals("AccKind"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AccKind = FValue.trim();
            }
            else
            {
                AccKind = null;
            }
        }
        if (FCode.equals("InsuAccName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InsuAccName = FValue.trim();
            }
            else
            {
                InsuAccName = null;
            }
        }
        if (FCode.equals("AccCreatePos"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AccCreatePos = FValue.trim();
            }
            else
            {
                AccCreatePos = null;
            }
        }
        if (FCode.equals("AccCreateType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AccCreateType = FValue.trim();
            }
            else
            {
                AccCreateType = null;
            }
        }
        if (FCode.equals("AccRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                AccRate = d;
            }
        }
        if (FCode.equals("AccRateTable"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AccRateTable = FValue.trim();
            }
            else
            {
                AccRateTable = null;
            }
        }
        if (FCode.equals("AccCancelCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AccCancelCode = FValue.trim();
            }
            else
            {
                AccCancelCode = null;
            }
        }
        if (FCode.equals("AccComputeFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AccComputeFlag = FValue.trim();
            }
            else
            {
                AccComputeFlag = null;
            }
        }
        if (FCode.equals("InvestType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InvestType = FValue.trim();
            }
            else
            {
                InvestType = null;
            }
        }
        if (FCode.equals("FundCompanyCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                FundCompanyCode = FValue.trim();
            }
            else
            {
                FundCompanyCode = null;
            }
        }
        if (FCode.equals("Owner"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Owner = FValue.trim();
            }
            else
            {
                Owner = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LMRiskInsuAccSchema other = (LMRiskInsuAccSchema) otherObject;
        return
                InsuAccNo.equals(other.getInsuAccNo())
                && AccType.equals(other.getAccType())
                && AccKind.equals(other.getAccKind())
                && InsuAccName.equals(other.getInsuAccName())
                && AccCreatePos.equals(other.getAccCreatePos())
                && AccCreateType.equals(other.getAccCreateType())
                && AccRate == other.getAccRate()
                && AccRateTable.equals(other.getAccRateTable())
                && AccCancelCode.equals(other.getAccCancelCode())
                && AccComputeFlag.equals(other.getAccComputeFlag())
                && InvestType.equals(other.getInvestType())
                && FundCompanyCode.equals(other.getFundCompanyCode())
                && Owner.equals(other.getOwner());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("InsuAccNo"))
        {
            return 0;
        }
        if (strFieldName.equals("AccType"))
        {
            return 1;
        }
        if (strFieldName.equals("AccKind"))
        {
            return 2;
        }
        if (strFieldName.equals("InsuAccName"))
        {
            return 3;
        }
        if (strFieldName.equals("AccCreatePos"))
        {
            return 4;
        }
        if (strFieldName.equals("AccCreateType"))
        {
            return 5;
        }
        if (strFieldName.equals("AccRate"))
        {
            return 6;
        }
        if (strFieldName.equals("AccRateTable"))
        {
            return 7;
        }
        if (strFieldName.equals("AccCancelCode"))
        {
            return 8;
        }
        if (strFieldName.equals("AccComputeFlag"))
        {
            return 9;
        }
        if (strFieldName.equals("InvestType"))
        {
            return 10;
        }
        if (strFieldName.equals("FundCompanyCode"))
        {
            return 11;
        }
        if (strFieldName.equals("Owner"))
        {
            return 12;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "InsuAccNo";
                break;
            case 1:
                strFieldName = "AccType";
                break;
            case 2:
                strFieldName = "AccKind";
                break;
            case 3:
                strFieldName = "InsuAccName";
                break;
            case 4:
                strFieldName = "AccCreatePos";
                break;
            case 5:
                strFieldName = "AccCreateType";
                break;
            case 6:
                strFieldName = "AccRate";
                break;
            case 7:
                strFieldName = "AccRateTable";
                break;
            case 8:
                strFieldName = "AccCancelCode";
                break;
            case 9:
                strFieldName = "AccComputeFlag";
                break;
            case 10:
                strFieldName = "InvestType";
                break;
            case 11:
                strFieldName = "FundCompanyCode";
                break;
            case 12:
                strFieldName = "Owner";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("InsuAccNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AccType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AccKind"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsuAccName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AccCreatePos"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AccCreateType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AccRate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("AccRateTable"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AccCancelCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AccComputeFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InvestType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FundCompanyCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Owner"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
