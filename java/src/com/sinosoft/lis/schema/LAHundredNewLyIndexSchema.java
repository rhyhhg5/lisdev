/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LAHundredNewLyIndexDB;

/*
 * <p>ClassName: LAHundredNewLyIndexSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 个险双百团队建设方案
 * @CreateDate：2017-05-23
 */
public class LAHundredNewLyIndexSchema implements Schema, Cloneable
{
	// @Field
	/** 团队 */
	private String AgentGroup;
	/** 年月 */
	private String IndexCalNo;
	/** 管理机构 */
	private String ManageCom;
	/** 期缴保费 */
	private double RegularPrem;
	/** 期末人力 */
	private double FinalManPower;
	/** 建设方案起期 */
	private Date ConstructionStartDate;
	/** 奖励金额 */
	private double AwardAmount;
	/** 操作者 */
	private String Opearator;
	/** 生成日期 */
	private Date MakeDate;
	/** 生成时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 12;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LAHundredNewLyIndexSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "AgentGroup";
		pk[1] = "IndexCalNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LAHundredNewLyIndexSchema cloned = (LAHundredNewLyIndexSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getAgentGroup()
	{
		return AgentGroup;
	}
	public void setAgentGroup(String aAgentGroup)
	{
		AgentGroup = aAgentGroup;
	}
	public String getIndexCalNo()
	{
		return IndexCalNo;
	}
	public void setIndexCalNo(String aIndexCalNo)
	{
		IndexCalNo = aIndexCalNo;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public double getRegularPrem()
	{
		return RegularPrem;
	}
	public void setRegularPrem(double aRegularPrem)
	{
		RegularPrem = Arith.round(aRegularPrem,2);
	}
	public void setRegularPrem(String aRegularPrem)
	{
		if (aRegularPrem != null && !aRegularPrem.equals(""))
		{
			Double tDouble = new Double(aRegularPrem);
			double d = tDouble.doubleValue();
                RegularPrem = Arith.round(d,2);
		}
	}

	public double getFinalManPower()
	{
		return FinalManPower;
	}
	public void setFinalManPower(double aFinalManPower)
	{
		FinalManPower = Arith.round(aFinalManPower,2);
	}
	public void setFinalManPower(String aFinalManPower)
	{
		if (aFinalManPower != null && !aFinalManPower.equals(""))
		{
			Double tDouble = new Double(aFinalManPower);
			double d = tDouble.doubleValue();
                FinalManPower = Arith.round(d,2);
		}
	}

	public String getConstructionStartDate()
	{
		if( ConstructionStartDate != null )
			return fDate.getString(ConstructionStartDate);
		else
			return null;
	}
	public void setConstructionStartDate(Date aConstructionStartDate)
	{
		ConstructionStartDate = aConstructionStartDate;
	}
	public void setConstructionStartDate(String aConstructionStartDate)
	{
		if (aConstructionStartDate != null && !aConstructionStartDate.equals("") )
		{
			ConstructionStartDate = fDate.getDate( aConstructionStartDate );
		}
		else
			ConstructionStartDate = null;
	}

	public double getAwardAmount()
	{
		return AwardAmount;
	}
	public void setAwardAmount(double aAwardAmount)
	{
		AwardAmount = Arith.round(aAwardAmount,2);
	}
	public void setAwardAmount(String aAwardAmount)
	{
		if (aAwardAmount != null && !aAwardAmount.equals(""))
		{
			Double tDouble = new Double(aAwardAmount);
			double d = tDouble.doubleValue();
                AwardAmount = Arith.round(d,2);
		}
	}

	public String getOpearator()
	{
		return Opearator;
	}
	public void setOpearator(String aOpearator)
	{
		Opearator = aOpearator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LAHundredNewLyIndexSchema 对象给 Schema 赋值
	* @param: aLAHundredNewLyIndexSchema LAHundredNewLyIndexSchema
	**/
	public void setSchema(LAHundredNewLyIndexSchema aLAHundredNewLyIndexSchema)
	{
		this.AgentGroup = aLAHundredNewLyIndexSchema.getAgentGroup();
		this.IndexCalNo = aLAHundredNewLyIndexSchema.getIndexCalNo();
		this.ManageCom = aLAHundredNewLyIndexSchema.getManageCom();
		this.RegularPrem = aLAHundredNewLyIndexSchema.getRegularPrem();
		this.FinalManPower = aLAHundredNewLyIndexSchema.getFinalManPower();
		this.ConstructionStartDate = fDate.getDate( aLAHundredNewLyIndexSchema.getConstructionStartDate());
		this.AwardAmount = aLAHundredNewLyIndexSchema.getAwardAmount();
		this.Opearator = aLAHundredNewLyIndexSchema.getOpearator();
		this.MakeDate = fDate.getDate( aLAHundredNewLyIndexSchema.getMakeDate());
		this.MakeTime = aLAHundredNewLyIndexSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLAHundredNewLyIndexSchema.getModifyDate());
		this.ModifyTime = aLAHundredNewLyIndexSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("AgentGroup") == null )
				this.AgentGroup = null;
			else
				this.AgentGroup = rs.getString("AgentGroup").trim();

			if( rs.getString("IndexCalNo") == null )
				this.IndexCalNo = null;
			else
				this.IndexCalNo = rs.getString("IndexCalNo").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			this.RegularPrem = rs.getDouble("RegularPrem");
			this.FinalManPower = rs.getDouble("FinalManPower");
			this.ConstructionStartDate = rs.getDate("ConstructionStartDate");
			this.AwardAmount = rs.getDouble("AwardAmount");
			if( rs.getString("Opearator") == null )
				this.Opearator = null;
			else
				this.Opearator = rs.getString("Opearator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LAHundredNewLyIndex表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAHundredNewLyIndexSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LAHundredNewLyIndexSchema getSchema()
	{
		LAHundredNewLyIndexSchema aLAHundredNewLyIndexSchema = new LAHundredNewLyIndexSchema();
		aLAHundredNewLyIndexSchema.setSchema(this);
		return aLAHundredNewLyIndexSchema;
	}

	public LAHundredNewLyIndexDB getDB()
	{
		LAHundredNewLyIndexDB aDBOper = new LAHundredNewLyIndexDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAHundredNewLyIndex描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(AgentGroup)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IndexCalNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(RegularPrem));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(FinalManPower));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ConstructionStartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(AwardAmount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Opearator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAHundredNewLyIndex>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			IndexCalNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			RegularPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,4,SysConst.PACKAGESPILTER))).doubleValue();
			FinalManPower = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,5,SysConst.PACKAGESPILTER))).doubleValue();
			ConstructionStartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,SysConst.PACKAGESPILTER));
			AwardAmount = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,7,SysConst.PACKAGESPILTER))).doubleValue();
			Opearator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAHundredNewLyIndexSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("AgentGroup"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
		}
		if (FCode.equals("IndexCalNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IndexCalNo));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("RegularPrem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RegularPrem));
		}
		if (FCode.equals("FinalManPower"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FinalManPower));
		}
		if (FCode.equals("ConstructionStartDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getConstructionStartDate()));
		}
		if (FCode.equals("AwardAmount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AwardAmount));
		}
		if (FCode.equals("Opearator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Opearator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(AgentGroup);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(IndexCalNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 3:
				strFieldValue = String.valueOf(RegularPrem);
				break;
			case 4:
				strFieldValue = String.valueOf(FinalManPower);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getConstructionStartDate()));
				break;
			case 6:
				strFieldValue = String.valueOf(AwardAmount);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(Opearator);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("AgentGroup"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentGroup = FValue.trim();
			}
			else
				AgentGroup = null;
		}
		if (FCode.equalsIgnoreCase("IndexCalNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IndexCalNo = FValue.trim();
			}
			else
				IndexCalNo = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("RegularPrem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				RegularPrem = d;
			}
		}
		if (FCode.equalsIgnoreCase("FinalManPower"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				FinalManPower = d;
			}
		}
		if (FCode.equalsIgnoreCase("ConstructionStartDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ConstructionStartDate = fDate.getDate( FValue );
			}
			else
				ConstructionStartDate = null;
		}
		if (FCode.equalsIgnoreCase("AwardAmount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				AwardAmount = d;
			}
		}
		if (FCode.equalsIgnoreCase("Opearator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Opearator = FValue.trim();
			}
			else
				Opearator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LAHundredNewLyIndexSchema other = (LAHundredNewLyIndexSchema)otherObject;
		return
			(AgentGroup == null ? other.getAgentGroup() == null : AgentGroup.equals(other.getAgentGroup()))
			&& (IndexCalNo == null ? other.getIndexCalNo() == null : IndexCalNo.equals(other.getIndexCalNo()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& RegularPrem == other.getRegularPrem()
			&& FinalManPower == other.getFinalManPower()
			&& (ConstructionStartDate == null ? other.getConstructionStartDate() == null : fDate.getString(ConstructionStartDate).equals(other.getConstructionStartDate()))
			&& AwardAmount == other.getAwardAmount()
			&& (Opearator == null ? other.getOpearator() == null : Opearator.equals(other.getOpearator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("AgentGroup") ) {
			return 0;
		}
		if( strFieldName.equals("IndexCalNo") ) {
			return 1;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 2;
		}
		if( strFieldName.equals("RegularPrem") ) {
			return 3;
		}
		if( strFieldName.equals("FinalManPower") ) {
			return 4;
		}
		if( strFieldName.equals("ConstructionStartDate") ) {
			return 5;
		}
		if( strFieldName.equals("AwardAmount") ) {
			return 6;
		}
		if( strFieldName.equals("Opearator") ) {
			return 7;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 8;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 9;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 10;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 11;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "AgentGroup";
				break;
			case 1:
				strFieldName = "IndexCalNo";
				break;
			case 2:
				strFieldName = "ManageCom";
				break;
			case 3:
				strFieldName = "RegularPrem";
				break;
			case 4:
				strFieldName = "FinalManPower";
				break;
			case 5:
				strFieldName = "ConstructionStartDate";
				break;
			case 6:
				strFieldName = "AwardAmount";
				break;
			case 7:
				strFieldName = "Opearator";
				break;
			case 8:
				strFieldName = "MakeDate";
				break;
			case 9:
				strFieldName = "MakeTime";
				break;
			case 10:
				strFieldName = "ModifyDate";
				break;
			case 11:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("AgentGroup") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IndexCalNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RegularPrem") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("FinalManPower") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ConstructionStartDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("AwardAmount") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Opearator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 4:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 5:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 6:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
