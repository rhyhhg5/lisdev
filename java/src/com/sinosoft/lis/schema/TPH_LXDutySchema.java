/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.TPH_LXDutyDB;

/*
 * <p>ClassName: TPH_LXDutySchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2015-10-27
 */
public class TPH_LXDutySchema implements Schema, Cloneable
{
	// @Field
	/** 数据批次号 */
	private String dataBatchNo;
	/** 数据流水号 */
	private String dataSerialNo;
	/** 保单号 */
	private String policyNo;
	/** 公司险种代码 */
	private String comCoverageCode;
	/** 公司责任代码 */
	private String liabilityCode;
	/** 责任保额 */
	private double liabilitySa;
	/** 责任状态 */
	private String liabilityStatus;
	/** 处理日期 */
	private Date makeDate;
	/** 处理时间 */
	private String makeTime;
	/** 修改日期 */
	private Date modifyDate;
	/** 修改时间 */
	private String modifyTime;

	public static final int FIELDNUM = 11;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public TPH_LXDutySchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[0];

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		TPH_LXDutySchema cloned = (TPH_LXDutySchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getdataBatchNo()
	{
		return dataBatchNo;
	}
	public void setdataBatchNo(String adataBatchNo)
	{
		dataBatchNo = adataBatchNo;
	}
	public String getdataSerialNo()
	{
		return dataSerialNo;
	}
	public void setdataSerialNo(String adataSerialNo)
	{
		dataSerialNo = adataSerialNo;
	}
	public String getpolicyNo()
	{
		return policyNo;
	}
	public void setpolicyNo(String apolicyNo)
	{
		policyNo = apolicyNo;
	}
	public String getcomCoverageCode()
	{
		return comCoverageCode;
	}
	public void setcomCoverageCode(String acomCoverageCode)
	{
		comCoverageCode = acomCoverageCode;
	}
	public String getliabilityCode()
	{
		return liabilityCode;
	}
	public void setliabilityCode(String aliabilityCode)
	{
		liabilityCode = aliabilityCode;
	}
	public double getliabilitySa()
	{
		return liabilitySa;
	}
	public void setliabilitySa(double aliabilitySa)
	{
		liabilitySa = Arith.round(aliabilitySa,2);
	}
	public void setliabilitySa(String aliabilitySa)
	{
		if (aliabilitySa != null && !aliabilitySa.equals(""))
		{
			Double tDouble = new Double(aliabilitySa);
			double d = tDouble.doubleValue();
                liabilitySa = Arith.round(d,2);
		}
	}

	public String getliabilityStatus()
	{
		return liabilityStatus;
	}
	public void setliabilityStatus(String aliabilityStatus)
	{
		liabilityStatus = aliabilityStatus;
	}
	public String getmakeDate()
	{
		if( makeDate != null )
			return fDate.getString(makeDate);
		else
			return null;
	}
	public void setmakeDate(Date amakeDate)
	{
		makeDate = amakeDate;
	}
	public void setmakeDate(String amakeDate)
	{
		if (amakeDate != null && !amakeDate.equals("") )
		{
			makeDate = fDate.getDate( amakeDate );
		}
		else
			makeDate = null;
	}

	public String getmakeTime()
	{
		return makeTime;
	}
	public void setmakeTime(String amakeTime)
	{
		makeTime = amakeTime;
	}
	public String getmodifyDate()
	{
		if( modifyDate != null )
			return fDate.getString(modifyDate);
		else
			return null;
	}
	public void setmodifyDate(Date amodifyDate)
	{
		modifyDate = amodifyDate;
	}
	public void setmodifyDate(String amodifyDate)
	{
		if (amodifyDate != null && !amodifyDate.equals("") )
		{
			modifyDate = fDate.getDate( amodifyDate );
		}
		else
			modifyDate = null;
	}

	public String getmodifyTime()
	{
		return modifyTime;
	}
	public void setmodifyTime(String amodifyTime)
	{
		modifyTime = amodifyTime;
	}

	/**
	* 使用另外一个 TPH_LXDutySchema 对象给 Schema 赋值
	* @param: aTPH_LXDutySchema TPH_LXDutySchema
	**/
	public void setSchema(TPH_LXDutySchema aTPH_LXDutySchema)
	{
		this.dataBatchNo = aTPH_LXDutySchema.getdataBatchNo();
		this.dataSerialNo = aTPH_LXDutySchema.getdataSerialNo();
		this.policyNo = aTPH_LXDutySchema.getpolicyNo();
		this.comCoverageCode = aTPH_LXDutySchema.getcomCoverageCode();
		this.liabilityCode = aTPH_LXDutySchema.getliabilityCode();
		this.liabilitySa = aTPH_LXDutySchema.getliabilitySa();
		this.liabilityStatus = aTPH_LXDutySchema.getliabilityStatus();
		this.makeDate = fDate.getDate( aTPH_LXDutySchema.getmakeDate());
		this.makeTime = aTPH_LXDutySchema.getmakeTime();
		this.modifyDate = fDate.getDate( aTPH_LXDutySchema.getmodifyDate());
		this.modifyTime = aTPH_LXDutySchema.getmodifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("dataBatchNo") == null )
				this.dataBatchNo = null;
			else
				this.dataBatchNo = rs.getString("dataBatchNo").trim();

			if( rs.getString("dataSerialNo") == null )
				this.dataSerialNo = null;
			else
				this.dataSerialNo = rs.getString("dataSerialNo").trim();

			if( rs.getString("policyNo") == null )
				this.policyNo = null;
			else
				this.policyNo = rs.getString("policyNo").trim();

			if( rs.getString("comCoverageCode") == null )
				this.comCoverageCode = null;
			else
				this.comCoverageCode = rs.getString("comCoverageCode").trim();

			if( rs.getString("liabilityCode") == null )
				this.liabilityCode = null;
			else
				this.liabilityCode = rs.getString("liabilityCode").trim();

			this.liabilitySa = rs.getDouble("liabilitySa");
			if( rs.getString("liabilityStatus") == null )
				this.liabilityStatus = null;
			else
				this.liabilityStatus = rs.getString("liabilityStatus").trim();

			this.makeDate = rs.getDate("makeDate");
			if( rs.getString("makeTime") == null )
				this.makeTime = null;
			else
				this.makeTime = rs.getString("makeTime").trim();

			this.modifyDate = rs.getDate("modifyDate");
			if( rs.getString("modifyTime") == null )
				this.modifyTime = null;
			else
				this.modifyTime = rs.getString("modifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的TPH_LXDuty表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "TPH_LXDutySchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public TPH_LXDutySchema getSchema()
	{
		TPH_LXDutySchema aTPH_LXDutySchema = new TPH_LXDutySchema();
		aTPH_LXDutySchema.setSchema(this);
		return aTPH_LXDutySchema;
	}

	public TPH_LXDutyDB getDB()
	{
		TPH_LXDutyDB aDBOper = new TPH_LXDutyDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpTPH_LXDuty描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(dataBatchNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(dataSerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(policyNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(comCoverageCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(liabilityCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(liabilitySa));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(liabilityStatus)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( makeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(makeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( modifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(modifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpTPH_LXDuty>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			dataBatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			dataSerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			policyNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			comCoverageCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			liabilityCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			liabilitySa = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,6,SysConst.PACKAGESPILTER))).doubleValue();
			liabilityStatus = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			makeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,SysConst.PACKAGESPILTER));
			makeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			modifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,SysConst.PACKAGESPILTER));
			modifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "TPH_LXDutySchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("dataBatchNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(dataBatchNo));
		}
		if (FCode.equals("dataSerialNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(dataSerialNo));
		}
		if (FCode.equals("policyNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(policyNo));
		}
		if (FCode.equals("comCoverageCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(comCoverageCode));
		}
		if (FCode.equals("liabilityCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(liabilityCode));
		}
		if (FCode.equals("liabilitySa"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(liabilitySa));
		}
		if (FCode.equals("liabilityStatus"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(liabilityStatus));
		}
		if (FCode.equals("makeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getmakeDate()));
		}
		if (FCode.equals("makeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(makeTime));
		}
		if (FCode.equals("modifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getmodifyDate()));
		}
		if (FCode.equals("modifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(modifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(dataBatchNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(dataSerialNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(policyNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(comCoverageCode);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(liabilityCode);
				break;
			case 5:
				strFieldValue = String.valueOf(liabilitySa);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(liabilityStatus);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getmakeDate()));
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(makeTime);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getmodifyDate()));
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(modifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("dataBatchNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				dataBatchNo = FValue.trim();
			}
			else
				dataBatchNo = null;
		}
		if (FCode.equalsIgnoreCase("dataSerialNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				dataSerialNo = FValue.trim();
			}
			else
				dataSerialNo = null;
		}
		if (FCode.equalsIgnoreCase("policyNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				policyNo = FValue.trim();
			}
			else
				policyNo = null;
		}
		if (FCode.equalsIgnoreCase("comCoverageCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				comCoverageCode = FValue.trim();
			}
			else
				comCoverageCode = null;
		}
		if (FCode.equalsIgnoreCase("liabilityCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				liabilityCode = FValue.trim();
			}
			else
				liabilityCode = null;
		}
		if (FCode.equalsIgnoreCase("liabilitySa"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				liabilitySa = d;
			}
		}
		if (FCode.equalsIgnoreCase("liabilityStatus"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				liabilityStatus = FValue.trim();
			}
			else
				liabilityStatus = null;
		}
		if (FCode.equalsIgnoreCase("makeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				makeDate = fDate.getDate( FValue );
			}
			else
				makeDate = null;
		}
		if (FCode.equalsIgnoreCase("makeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				makeTime = FValue.trim();
			}
			else
				makeTime = null;
		}
		if (FCode.equalsIgnoreCase("modifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				modifyDate = fDate.getDate( FValue );
			}
			else
				modifyDate = null;
		}
		if (FCode.equalsIgnoreCase("modifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				modifyTime = FValue.trim();
			}
			else
				modifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		TPH_LXDutySchema other = (TPH_LXDutySchema)otherObject;
		return
			(dataBatchNo == null ? other.getdataBatchNo() == null : dataBatchNo.equals(other.getdataBatchNo()))
			&& (dataSerialNo == null ? other.getdataSerialNo() == null : dataSerialNo.equals(other.getdataSerialNo()))
			&& (policyNo == null ? other.getpolicyNo() == null : policyNo.equals(other.getpolicyNo()))
			&& (comCoverageCode == null ? other.getcomCoverageCode() == null : comCoverageCode.equals(other.getcomCoverageCode()))
			&& (liabilityCode == null ? other.getliabilityCode() == null : liabilityCode.equals(other.getliabilityCode()))
			&& liabilitySa == other.getliabilitySa()
			&& (liabilityStatus == null ? other.getliabilityStatus() == null : liabilityStatus.equals(other.getliabilityStatus()))
			&& (makeDate == null ? other.getmakeDate() == null : fDate.getString(makeDate).equals(other.getmakeDate()))
			&& (makeTime == null ? other.getmakeTime() == null : makeTime.equals(other.getmakeTime()))
			&& (modifyDate == null ? other.getmodifyDate() == null : fDate.getString(modifyDate).equals(other.getmodifyDate()))
			&& (modifyTime == null ? other.getmodifyTime() == null : modifyTime.equals(other.getmodifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("dataBatchNo") ) {
			return 0;
		}
		if( strFieldName.equals("dataSerialNo") ) {
			return 1;
		}
		if( strFieldName.equals("policyNo") ) {
			return 2;
		}
		if( strFieldName.equals("comCoverageCode") ) {
			return 3;
		}
		if( strFieldName.equals("liabilityCode") ) {
			return 4;
		}
		if( strFieldName.equals("liabilitySa") ) {
			return 5;
		}
		if( strFieldName.equals("liabilityStatus") ) {
			return 6;
		}
		if( strFieldName.equals("makeDate") ) {
			return 7;
		}
		if( strFieldName.equals("makeTime") ) {
			return 8;
		}
		if( strFieldName.equals("modifyDate") ) {
			return 9;
		}
		if( strFieldName.equals("modifyTime") ) {
			return 10;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "dataBatchNo";
				break;
			case 1:
				strFieldName = "dataSerialNo";
				break;
			case 2:
				strFieldName = "policyNo";
				break;
			case 3:
				strFieldName = "comCoverageCode";
				break;
			case 4:
				strFieldName = "liabilityCode";
				break;
			case 5:
				strFieldName = "liabilitySa";
				break;
			case 6:
				strFieldName = "liabilityStatus";
				break;
			case 7:
				strFieldName = "makeDate";
				break;
			case 8:
				strFieldName = "makeTime";
				break;
			case 9:
				strFieldName = "modifyDate";
				break;
			case 10:
				strFieldName = "modifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("dataBatchNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("dataSerialNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("policyNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("comCoverageCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("liabilityCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("liabilitySa") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("liabilityStatus") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("makeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("makeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("modifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("modifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
