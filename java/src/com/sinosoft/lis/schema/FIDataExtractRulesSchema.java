/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.FIDataExtractRulesDB;

/*
 * <p>ClassName: FIDataExtractRulesSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 财务接口最新版
 * @CreateDate：2011-12-13
 */
public class FIDataExtractRulesSchema implements Schema, Cloneable
{
	// @Field
	/** 版本编号 */
	private String VersionNo;
	/** 规则编码 */
	private String RuleDefID;
	/** 算法编码 */
	private String ArithmeticID;
	/** 算法名称 */
	private String ArithmeticName;
	/** 规则执行对象 */
	private String RunObject;
	/** 算法类型 */
	private String ArithmeticType;
	/** 算法类型明细 */
	private String SubType;
	/** 业务交易大类 */
	private String BuType;
	/** 执行顺序 */
	private int DealOrder;
	/** 处理方式 */
	private String DealMode;
	/** 处理类名称 */
	private String DealClass;
	/** 处理文件名称 */
	private String FileName;
	/** 数据源类型 */
	private String DSType;
	/** 数据源 */
	private String DataSource;
	/** 主sql */
	private String MainSQL;
	/** 辅助sql */
	private String AssistSQL;
	/** 返回信息 */
	private String ReturnMess;
	/** 返回数据存储 */
	private String ReturnStore;
	/** 算法描述 */
	private String ReturnRemark;
	/** 错误类型 */
	private String ErrType;
	/** 上层算法编码 */
	private String PrArithmeticID;
	/** 算法状态 */
	private String State;

	public static final int FIELDNUM = 22;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public FIDataExtractRulesSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "VersionNo";
		pk[1] = "ArithmeticID";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		FIDataExtractRulesSchema cloned = (FIDataExtractRulesSchema) super.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getVersionNo()
	{
		return VersionNo;
	}
	public void setVersionNo(String aVersionNo)
	{
		VersionNo = aVersionNo;
	}
	public String getRuleDefID()
	{
		return RuleDefID;
	}
	public void setRuleDefID(String aRuleDefID)
	{
		RuleDefID = aRuleDefID;
	}
	public String getArithmeticID()
	{
		return ArithmeticID;
	}
	public void setArithmeticID(String aArithmeticID)
	{
		ArithmeticID = aArithmeticID;
	}
	public String getArithmeticName()
	{
		return ArithmeticName;
	}
	public void setArithmeticName(String aArithmeticName)
	{
		ArithmeticName = aArithmeticName;
	}
	public String getRunObject()
	{
		return RunObject;
	}
	public void setRunObject(String aRunObject)
	{
		RunObject = aRunObject;
	}
	public String getArithmeticType()
	{
		return ArithmeticType;
	}
	public void setArithmeticType(String aArithmeticType)
	{
		ArithmeticType = aArithmeticType;
	}
	public String getSubType()
	{
		return SubType;
	}
	public void setSubType(String aSubType)
	{
		SubType = aSubType;
	}
	public String getBuType()
	{
		return BuType;
	}
	public void setBuType(String aBuType)
	{
		BuType = aBuType;
	}
	public int getDealOrder()
	{
		return DealOrder;
	}
	public void setDealOrder(int aDealOrder)
	{
		DealOrder = aDealOrder;
	}
	public void setDealOrder(String aDealOrder)
	{
		if (aDealOrder != null && !aDealOrder.equals(""))
		{
			Integer tInteger = new Integer(aDealOrder);
			int i = tInteger.intValue();
			DealOrder = i;
		}
	}

	public String getDealMode()
	{
		return DealMode;
	}
	public void setDealMode(String aDealMode)
	{
		DealMode = aDealMode;
	}
	public String getDealClass()
	{
		return DealClass;
	}
	public void setDealClass(String aDealClass)
	{
		DealClass = aDealClass;
	}
	public String getFileName()
	{
		return FileName;
	}
	public void setFileName(String aFileName)
	{
		FileName = aFileName;
	}
	public String getDSType()
	{
		return DSType;
	}
	public void setDSType(String aDSType)
	{
		DSType = aDSType;
	}
	public String getDataSource()
	{
		return DataSource;
	}
	public void setDataSource(String aDataSource)
	{
		DataSource = aDataSource;
	}
	public String getMainSQL()
	{
		return MainSQL;
	}
	public void setMainSQL(String aMainSQL)
	{
		MainSQL = aMainSQL;
	}
	public String getAssistSQL()
	{
		return AssistSQL;
	}
	public void setAssistSQL(String aAssistSQL)
	{
		AssistSQL = aAssistSQL;
	}
	public String getReturnMess()
	{
		return ReturnMess;
	}
	public void setReturnMess(String aReturnMess)
	{
		ReturnMess = aReturnMess;
	}
	public String getReturnStore()
	{
		return ReturnStore;
	}
	public void setReturnStore(String aReturnStore)
	{
		ReturnStore = aReturnStore;
	}
	public String getReturnRemark()
	{
		return ReturnRemark;
	}
	public void setReturnRemark(String aReturnRemark)
	{
		ReturnRemark = aReturnRemark;
	}
	public String getErrType()
	{
		return ErrType;
	}
	public void setErrType(String aErrType)
	{
		ErrType = aErrType;
	}
	public String getPrArithmeticID()
	{
		return PrArithmeticID;
	}
	public void setPrArithmeticID(String aPrArithmeticID)
	{
		PrArithmeticID = aPrArithmeticID;
	}
	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}

	/**
	* 使用另外一个 FIDataExtractRulesSchema 对象给 Schema 赋值
	* @param: aFIDataExtractRulesSchema FIDataExtractRulesSchema
	**/
	public void setSchema(FIDataExtractRulesSchema aFIDataExtractRulesSchema)
	{
		this.VersionNo = aFIDataExtractRulesSchema.getVersionNo();
		this.RuleDefID = aFIDataExtractRulesSchema.getRuleDefID();
		this.ArithmeticID = aFIDataExtractRulesSchema.getArithmeticID();
		this.ArithmeticName = aFIDataExtractRulesSchema.getArithmeticName();
		this.RunObject = aFIDataExtractRulesSchema.getRunObject();
		this.ArithmeticType = aFIDataExtractRulesSchema.getArithmeticType();
		this.SubType = aFIDataExtractRulesSchema.getSubType();
		this.BuType = aFIDataExtractRulesSchema.getBuType();
		this.DealOrder = aFIDataExtractRulesSchema.getDealOrder();
		this.DealMode = aFIDataExtractRulesSchema.getDealMode();
		this.DealClass = aFIDataExtractRulesSchema.getDealClass();
		this.FileName = aFIDataExtractRulesSchema.getFileName();
		this.DSType = aFIDataExtractRulesSchema.getDSType();
		this.DataSource = aFIDataExtractRulesSchema.getDataSource();
		this.MainSQL = aFIDataExtractRulesSchema.getMainSQL();
		this.AssistSQL = aFIDataExtractRulesSchema.getAssistSQL();
		this.ReturnMess = aFIDataExtractRulesSchema.getReturnMess();
		this.ReturnStore = aFIDataExtractRulesSchema.getReturnStore();
		this.ReturnRemark = aFIDataExtractRulesSchema.getReturnRemark();
		this.ErrType = aFIDataExtractRulesSchema.getErrType();
		this.PrArithmeticID = aFIDataExtractRulesSchema.getPrArithmeticID();
		this.State = aFIDataExtractRulesSchema.getState();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("VersionNo") == null )
				this.VersionNo = null;
			else
				this.VersionNo = rs.getString("VersionNo").trim();

			if( rs.getString("RuleDefID") == null )
				this.RuleDefID = null;
			else
				this.RuleDefID = rs.getString("RuleDefID").trim();

			if( rs.getString("ArithmeticID") == null )
				this.ArithmeticID = null;
			else
				this.ArithmeticID = rs.getString("ArithmeticID").trim();

			if( rs.getString("ArithmeticName") == null )
				this.ArithmeticName = null;
			else
				this.ArithmeticName = rs.getString("ArithmeticName").trim();

			if( rs.getString("RunObject") == null )
				this.RunObject = null;
			else
				this.RunObject = rs.getString("RunObject").trim();

			if( rs.getString("ArithmeticType") == null )
				this.ArithmeticType = null;
			else
				this.ArithmeticType = rs.getString("ArithmeticType").trim();

			if( rs.getString("SubType") == null )
				this.SubType = null;
			else
				this.SubType = rs.getString("SubType").trim();

			if( rs.getString("BuType") == null )
				this.BuType = null;
			else
				this.BuType = rs.getString("BuType").trim();

			this.DealOrder = rs.getInt("DealOrder");
			if( rs.getString("DealMode") == null )
				this.DealMode = null;
			else
				this.DealMode = rs.getString("DealMode").trim();

			if( rs.getString("DealClass") == null )
				this.DealClass = null;
			else
				this.DealClass = rs.getString("DealClass").trim();

			if( rs.getString("FileName") == null )
				this.FileName = null;
			else
				this.FileName = rs.getString("FileName").trim();

			if( rs.getString("DSType") == null )
				this.DSType = null;
			else
				this.DSType = rs.getString("DSType").trim();

			if( rs.getString("DataSource") == null )
				this.DataSource = null;
			else
				this.DataSource = rs.getString("DataSource").trim();

			if( rs.getString("MainSQL") == null )
				this.MainSQL = null;
			else
				this.MainSQL = rs.getString("MainSQL").trim();

			if( rs.getString("AssistSQL") == null )
				this.AssistSQL = null;
			else
				this.AssistSQL = rs.getString("AssistSQL").trim();

			if( rs.getString("ReturnMess") == null )
				this.ReturnMess = null;
			else
				this.ReturnMess = rs.getString("ReturnMess").trim();

			if( rs.getString("ReturnStore") == null )
				this.ReturnStore = null;
			else
				this.ReturnStore = rs.getString("ReturnStore").trim();

			if( rs.getString("ReturnRemark") == null )
				this.ReturnRemark = null;
			else
				this.ReturnRemark = rs.getString("ReturnRemark").trim();

			if( rs.getString("ErrType") == null )
				this.ErrType = null;
			else
				this.ErrType = rs.getString("ErrType").trim();

			if( rs.getString("PrArithmeticID") == null )
				this.PrArithmeticID = null;
			else
				this.PrArithmeticID = rs.getString("PrArithmeticID").trim();

			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的FIDataExtractRules表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIDataExtractRulesSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public FIDataExtractRulesSchema getSchema()
	{
		FIDataExtractRulesSchema aFIDataExtractRulesSchema = new FIDataExtractRulesSchema();
		aFIDataExtractRulesSchema.setSchema(this);
		return aFIDataExtractRulesSchema;
	}

	public FIDataExtractRulesDB getDB()
	{
		FIDataExtractRulesDB aDBOper = new FIDataExtractRulesDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpFIDataExtractRules描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(VersionNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RuleDefID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ArithmeticID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ArithmeticName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RunObject)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ArithmeticType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SubType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BuType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(DealOrder));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DealMode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DealClass)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FileName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DSType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DataSource)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MainSQL)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AssistSQL)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReturnMess)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReturnStore)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReturnRemark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ErrType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PrArithmeticID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpFIDataExtractRules>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			VersionNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			RuleDefID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			ArithmeticID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			ArithmeticName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			RunObject = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			ArithmeticType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			SubType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			BuType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			DealOrder= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,9,SysConst.PACKAGESPILTER))).intValue();
			DealMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			DealClass = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			FileName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			DSType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			DataSource = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			MainSQL = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			AssistSQL = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			ReturnMess = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			ReturnStore = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			ReturnRemark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			ErrType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			PrArithmeticID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIDataExtractRulesSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("VersionNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(VersionNo));
		}
		if (FCode.equals("RuleDefID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RuleDefID));
		}
		if (FCode.equals("ArithmeticID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ArithmeticID));
		}
		if (FCode.equals("ArithmeticName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ArithmeticName));
		}
		if (FCode.equals("RunObject"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RunObject));
		}
		if (FCode.equals("ArithmeticType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ArithmeticType));
		}
		if (FCode.equals("SubType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SubType));
		}
		if (FCode.equals("BuType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BuType));
		}
		if (FCode.equals("DealOrder"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DealOrder));
		}
		if (FCode.equals("DealMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DealMode));
		}
		if (FCode.equals("DealClass"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DealClass));
		}
		if (FCode.equals("FileName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FileName));
		}
		if (FCode.equals("DSType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DSType));
		}
		if (FCode.equals("DataSource"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DataSource));
		}
		if (FCode.equals("MainSQL"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MainSQL));
		}
		if (FCode.equals("AssistSQL"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AssistSQL));
		}
		if (FCode.equals("ReturnMess"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReturnMess));
		}
		if (FCode.equals("ReturnStore"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReturnStore));
		}
		if (FCode.equals("ReturnRemark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReturnRemark));
		}
		if (FCode.equals("ErrType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ErrType));
		}
		if (FCode.equals("PrArithmeticID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrArithmeticID));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(VersionNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(RuleDefID);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(ArithmeticID);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(ArithmeticName);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(RunObject);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(ArithmeticType);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(SubType);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(BuType);
				break;
			case 8:
				strFieldValue = String.valueOf(DealOrder);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(DealMode);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(DealClass);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(FileName);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(DSType);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(DataSource);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(MainSQL);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(AssistSQL);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(ReturnMess);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(ReturnStore);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(ReturnRemark);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(ErrType);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(PrArithmeticID);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("VersionNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				VersionNo = FValue.trim();
			}
			else
				VersionNo = null;
		}
		if (FCode.equalsIgnoreCase("RuleDefID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RuleDefID = FValue.trim();
			}
			else
				RuleDefID = null;
		}
		if (FCode.equalsIgnoreCase("ArithmeticID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ArithmeticID = FValue.trim();
			}
			else
				ArithmeticID = null;
		}
		if (FCode.equalsIgnoreCase("ArithmeticName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ArithmeticName = FValue.trim();
			}
			else
				ArithmeticName = null;
		}
		if (FCode.equalsIgnoreCase("RunObject"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RunObject = FValue.trim();
			}
			else
				RunObject = null;
		}
		if (FCode.equalsIgnoreCase("ArithmeticType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ArithmeticType = FValue.trim();
			}
			else
				ArithmeticType = null;
		}
		if (FCode.equalsIgnoreCase("SubType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SubType = FValue.trim();
			}
			else
				SubType = null;
		}
		if (FCode.equalsIgnoreCase("BuType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BuType = FValue.trim();
			}
			else
				BuType = null;
		}
		if (FCode.equalsIgnoreCase("DealOrder"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				DealOrder = i;
			}
		}
		if (FCode.equalsIgnoreCase("DealMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DealMode = FValue.trim();
			}
			else
				DealMode = null;
		}
		if (FCode.equalsIgnoreCase("DealClass"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DealClass = FValue.trim();
			}
			else
				DealClass = null;
		}
		if (FCode.equalsIgnoreCase("FileName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FileName = FValue.trim();
			}
			else
				FileName = null;
		}
		if (FCode.equalsIgnoreCase("DSType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DSType = FValue.trim();
			}
			else
				DSType = null;
		}
		if (FCode.equalsIgnoreCase("DataSource"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DataSource = FValue.trim();
			}
			else
				DataSource = null;
		}
		if (FCode.equalsIgnoreCase("MainSQL"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MainSQL = FValue.trim();
			}
			else
				MainSQL = null;
		}
		if (FCode.equalsIgnoreCase("AssistSQL"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AssistSQL = FValue.trim();
			}
			else
				AssistSQL = null;
		}
		if (FCode.equalsIgnoreCase("ReturnMess"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReturnMess = FValue.trim();
			}
			else
				ReturnMess = null;
		}
		if (FCode.equalsIgnoreCase("ReturnStore"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReturnStore = FValue.trim();
			}
			else
				ReturnStore = null;
		}
		if (FCode.equalsIgnoreCase("ReturnRemark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReturnRemark = FValue.trim();
			}
			else
				ReturnRemark = null;
		}
		if (FCode.equalsIgnoreCase("ErrType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ErrType = FValue.trim();
			}
			else
				ErrType = null;
		}
		if (FCode.equalsIgnoreCase("PrArithmeticID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PrArithmeticID = FValue.trim();
			}
			else
				PrArithmeticID = null;
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		FIDataExtractRulesSchema other = (FIDataExtractRulesSchema)otherObject;
		return
			(VersionNo == null ? other.getVersionNo() == null : VersionNo.equals(other.getVersionNo()))
			&& (RuleDefID == null ? other.getRuleDefID() == null : RuleDefID.equals(other.getRuleDefID()))
			&& (ArithmeticID == null ? other.getArithmeticID() == null : ArithmeticID.equals(other.getArithmeticID()))
			&& (ArithmeticName == null ? other.getArithmeticName() == null : ArithmeticName.equals(other.getArithmeticName()))
			&& (RunObject == null ? other.getRunObject() == null : RunObject.equals(other.getRunObject()))
			&& (ArithmeticType == null ? other.getArithmeticType() == null : ArithmeticType.equals(other.getArithmeticType()))
			&& (SubType == null ? other.getSubType() == null : SubType.equals(other.getSubType()))
			&& (BuType == null ? other.getBuType() == null : BuType.equals(other.getBuType()))
			&& DealOrder == other.getDealOrder()
			&& (DealMode == null ? other.getDealMode() == null : DealMode.equals(other.getDealMode()))
			&& (DealClass == null ? other.getDealClass() == null : DealClass.equals(other.getDealClass()))
			&& (FileName == null ? other.getFileName() == null : FileName.equals(other.getFileName()))
			&& (DSType == null ? other.getDSType() == null : DSType.equals(other.getDSType()))
			&& (DataSource == null ? other.getDataSource() == null : DataSource.equals(other.getDataSource()))
			&& (MainSQL == null ? other.getMainSQL() == null : MainSQL.equals(other.getMainSQL()))
			&& (AssistSQL == null ? other.getAssistSQL() == null : AssistSQL.equals(other.getAssistSQL()))
			&& (ReturnMess == null ? other.getReturnMess() == null : ReturnMess.equals(other.getReturnMess()))
			&& (ReturnStore == null ? other.getReturnStore() == null : ReturnStore.equals(other.getReturnStore()))
			&& (ReturnRemark == null ? other.getReturnRemark() == null : ReturnRemark.equals(other.getReturnRemark()))
			&& (ErrType == null ? other.getErrType() == null : ErrType.equals(other.getErrType()))
			&& (PrArithmeticID == null ? other.getPrArithmeticID() == null : PrArithmeticID.equals(other.getPrArithmeticID()))
			&& (State == null ? other.getState() == null : State.equals(other.getState()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("VersionNo") ) {
			return 0;
		}
		if( strFieldName.equals("RuleDefID") ) {
			return 1;
		}
		if( strFieldName.equals("ArithmeticID") ) {
			return 2;
		}
		if( strFieldName.equals("ArithmeticName") ) {
			return 3;
		}
		if( strFieldName.equals("RunObject") ) {
			return 4;
		}
		if( strFieldName.equals("ArithmeticType") ) {
			return 5;
		}
		if( strFieldName.equals("SubType") ) {
			return 6;
		}
		if( strFieldName.equals("BuType") ) {
			return 7;
		}
		if( strFieldName.equals("DealOrder") ) {
			return 8;
		}
		if( strFieldName.equals("DealMode") ) {
			return 9;
		}
		if( strFieldName.equals("DealClass") ) {
			return 10;
		}
		if( strFieldName.equals("FileName") ) {
			return 11;
		}
		if( strFieldName.equals("DSType") ) {
			return 12;
		}
		if( strFieldName.equals("DataSource") ) {
			return 13;
		}
		if( strFieldName.equals("MainSQL") ) {
			return 14;
		}
		if( strFieldName.equals("AssistSQL") ) {
			return 15;
		}
		if( strFieldName.equals("ReturnMess") ) {
			return 16;
		}
		if( strFieldName.equals("ReturnStore") ) {
			return 17;
		}
		if( strFieldName.equals("ReturnRemark") ) {
			return 18;
		}
		if( strFieldName.equals("ErrType") ) {
			return 19;
		}
		if( strFieldName.equals("PrArithmeticID") ) {
			return 20;
		}
		if( strFieldName.equals("State") ) {
			return 21;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "VersionNo";
				break;
			case 1:
				strFieldName = "RuleDefID";
				break;
			case 2:
				strFieldName = "ArithmeticID";
				break;
			case 3:
				strFieldName = "ArithmeticName";
				break;
			case 4:
				strFieldName = "RunObject";
				break;
			case 5:
				strFieldName = "ArithmeticType";
				break;
			case 6:
				strFieldName = "SubType";
				break;
			case 7:
				strFieldName = "BuType";
				break;
			case 8:
				strFieldName = "DealOrder";
				break;
			case 9:
				strFieldName = "DealMode";
				break;
			case 10:
				strFieldName = "DealClass";
				break;
			case 11:
				strFieldName = "FileName";
				break;
			case 12:
				strFieldName = "DSType";
				break;
			case 13:
				strFieldName = "DataSource";
				break;
			case 14:
				strFieldName = "MainSQL";
				break;
			case 15:
				strFieldName = "AssistSQL";
				break;
			case 16:
				strFieldName = "ReturnMess";
				break;
			case 17:
				strFieldName = "ReturnStore";
				break;
			case 18:
				strFieldName = "ReturnRemark";
				break;
			case 19:
				strFieldName = "ErrType";
				break;
			case 20:
				strFieldName = "PrArithmeticID";
				break;
			case 21:
				strFieldName = "State";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("VersionNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RuleDefID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ArithmeticID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ArithmeticName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RunObject") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ArithmeticType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SubType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BuType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DealOrder") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("DealMode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DealClass") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FileName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DSType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DataSource") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MainSQL") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AssistSQL") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReturnMess") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReturnStore") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReturnRemark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ErrType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PrArithmeticID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_INT;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
