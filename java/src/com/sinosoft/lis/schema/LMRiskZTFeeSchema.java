/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LMRiskZTFeeDB;

/*
 * <p>ClassName: LMRiskZTFeeSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2010-12-02
 */
public class LMRiskZTFeeSchema implements Schema, Cloneable
{
	// @Field
	/** 险种编码 */
	private String RiskCode;
	/** 参保起始年期 */
	private int BeginPolYear;
	/** 参保结束年期 */
	private int EndPolYear;
	/** 费用提取比例 */
	private double ExtractRate;

	public static final int FIELDNUM = 4;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LMRiskZTFeeSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[3];
		pk[0] = "RiskCode";
		pk[1] = "BeginPolYear";
		pk[2] = "EndPolYear";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LMRiskZTFeeSchema cloned = (LMRiskZTFeeSchema) super.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getRiskCode()
	{
		return RiskCode;
	}
	public void setRiskCode(String aRiskCode)
	{
		RiskCode = aRiskCode;
	}
	public int getBeginPolYear()
	{
		return BeginPolYear;
	}
	public void setBeginPolYear(int aBeginPolYear)
	{
		BeginPolYear = aBeginPolYear;
	}
	public void setBeginPolYear(String aBeginPolYear)
	{
		if (aBeginPolYear != null && !aBeginPolYear.equals(""))
		{
			Integer tInteger = new Integer(aBeginPolYear);
			int i = tInteger.intValue();
			BeginPolYear = i;
		}
	}

	public int getEndPolYear()
	{
		return EndPolYear;
	}
	public void setEndPolYear(int aEndPolYear)
	{
		EndPolYear = aEndPolYear;
	}
	public void setEndPolYear(String aEndPolYear)
	{
		if (aEndPolYear != null && !aEndPolYear.equals(""))
		{
			Integer tInteger = new Integer(aEndPolYear);
			int i = tInteger.intValue();
			EndPolYear = i;
		}
	}

	public double getExtractRate()
	{
		return ExtractRate;
	}
	public void setExtractRate(double aExtractRate)
	{
		ExtractRate = Arith.round(aExtractRate,6);
	}
	public void setExtractRate(String aExtractRate)
	{
		if (aExtractRate != null && !aExtractRate.equals(""))
		{
			Double tDouble = new Double(aExtractRate);
			double d = tDouble.doubleValue();
                ExtractRate = Arith.round(d,6);
		}
	}


	/**
	* 使用另外一个 LMRiskZTFeeSchema 对象给 Schema 赋值
	* @param: aLMRiskZTFeeSchema LMRiskZTFeeSchema
	**/
	public void setSchema(LMRiskZTFeeSchema aLMRiskZTFeeSchema)
	{
		this.RiskCode = aLMRiskZTFeeSchema.getRiskCode();
		this.BeginPolYear = aLMRiskZTFeeSchema.getBeginPolYear();
		this.EndPolYear = aLMRiskZTFeeSchema.getEndPolYear();
		this.ExtractRate = aLMRiskZTFeeSchema.getExtractRate();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("RiskCode") == null )
				this.RiskCode = null;
			else
				this.RiskCode = rs.getString("RiskCode").trim();

			this.BeginPolYear = rs.getInt("BeginPolYear");
			this.EndPolYear = rs.getInt("EndPolYear");
			this.ExtractRate = rs.getDouble("ExtractRate");
		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LMRiskZTFee表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LMRiskZTFeeSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LMRiskZTFeeSchema getSchema()
	{
		LMRiskZTFeeSchema aLMRiskZTFeeSchema = new LMRiskZTFeeSchema();
		aLMRiskZTFeeSchema.setSchema(this);
		return aLMRiskZTFeeSchema;
	}

	public LMRiskZTFeeDB getDB()
	{
		LMRiskZTFeeDB aDBOper = new LMRiskZTFeeDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMRiskZTFee描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(BeginPolYear));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(EndPolYear));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ExtractRate));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMRiskZTFee>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			BeginPolYear= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,2,SysConst.PACKAGESPILTER))).intValue();
			EndPolYear= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,3,SysConst.PACKAGESPILTER))).intValue();
			ExtractRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,4,SysConst.PACKAGESPILTER))).doubleValue();
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LMRiskZTFeeSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("RiskCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
		}
		if (FCode.equals("BeginPolYear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BeginPolYear));
		}
		if (FCode.equals("EndPolYear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EndPolYear));
		}
		if (FCode.equals("ExtractRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ExtractRate));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(RiskCode);
				break;
			case 1:
				strFieldValue = String.valueOf(BeginPolYear);
				break;
			case 2:
				strFieldValue = String.valueOf(EndPolYear);
				break;
			case 3:
				strFieldValue = String.valueOf(ExtractRate);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("RiskCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskCode = FValue.trim();
			}
			else
				RiskCode = null;
		}
		if (FCode.equalsIgnoreCase("BeginPolYear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				BeginPolYear = i;
			}
		}
		if (FCode.equalsIgnoreCase("EndPolYear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				EndPolYear = i;
			}
		}
		if (FCode.equalsIgnoreCase("ExtractRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ExtractRate = d;
			}
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LMRiskZTFeeSchema other = (LMRiskZTFeeSchema)otherObject;
		return
			(RiskCode == null ? other.getRiskCode() == null : RiskCode.equals(other.getRiskCode()))
			&& BeginPolYear == other.getBeginPolYear()
			&& EndPolYear == other.getEndPolYear()
			&& ExtractRate == other.getExtractRate();
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("RiskCode") ) {
			return 0;
		}
		if( strFieldName.equals("BeginPolYear") ) {
			return 1;
		}
		if( strFieldName.equals("EndPolYear") ) {
			return 2;
		}
		if( strFieldName.equals("ExtractRate") ) {
			return 3;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "RiskCode";
				break;
			case 1:
				strFieldName = "BeginPolYear";
				break;
			case 2:
				strFieldName = "EndPolYear";
				break;
			case 3:
				strFieldName = "ExtractRate";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("RiskCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BeginPolYear") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("EndPolYear") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("ExtractRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_INT;
				break;
			case 2:
				nFieldType = Schema.TYPE_INT;
				break;
			case 3:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
