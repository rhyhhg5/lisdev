/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LAWageCtrlRangeDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

/*
 * <p>ClassName: LAWageCtrlRangeSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-03-29
 */
public class LAWageCtrlRangeSchema implements Schema
{
    // @Field
    /** 提取控制类型 */
    private String CtrlRangeType;
    /** 险种编码 */
    private String RiskCode;
    /** 规定范围起期 */
    private String StartRange;
    /** 规定范围止期 */
    private String EndRange;
    /** 规定结果 */
    private String CalResult;

    public static final int FIELDNUM = 5; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LAWageCtrlRangeSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "CtrlRangeType";
        pk[1] = "RiskCode";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getCtrlRangeType()
    {
        if (SysConst.CHANGECHARSET && CtrlRangeType != null &&
            !CtrlRangeType.equals(""))
        {
            CtrlRangeType = StrTool.unicodeToGBK(CtrlRangeType);
        }
        return CtrlRangeType;
    }

    public void setCtrlRangeType(String aCtrlRangeType)
    {
        CtrlRangeType = aCtrlRangeType;
    }

    public String getRiskCode()
    {
        if (SysConst.CHANGECHARSET && RiskCode != null && !RiskCode.equals(""))
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getStartRange()
    {
        if (SysConst.CHANGECHARSET && StartRange != null &&
            !StartRange.equals(""))
        {
            StartRange = StrTool.unicodeToGBK(StartRange);
        }
        return StartRange;
    }

    public void setStartRange(String aStartRange)
    {
        StartRange = aStartRange;
    }

    public String getEndRange()
    {
        if (SysConst.CHANGECHARSET && EndRange != null && !EndRange.equals(""))
        {
            EndRange = StrTool.unicodeToGBK(EndRange);
        }
        return EndRange;
    }

    public void setEndRange(String aEndRange)
    {
        EndRange = aEndRange;
    }

    public String getCalResult()
    {
        if (SysConst.CHANGECHARSET && CalResult != null && !CalResult.equals(""))
        {
            CalResult = StrTool.unicodeToGBK(CalResult);
        }
        return CalResult;
    }

    public void setCalResult(String aCalResult)
    {
        CalResult = aCalResult;
    }

    /**
     * 使用另外一个 LAWageCtrlRangeSchema 对象给 Schema 赋值
     * @param: aLAWageCtrlRangeSchema LAWageCtrlRangeSchema
     **/
    public void setSchema(LAWageCtrlRangeSchema aLAWageCtrlRangeSchema)
    {
        this.CtrlRangeType = aLAWageCtrlRangeSchema.getCtrlRangeType();
        this.RiskCode = aLAWageCtrlRangeSchema.getRiskCode();
        this.StartRange = aLAWageCtrlRangeSchema.getStartRange();
        this.EndRange = aLAWageCtrlRangeSchema.getEndRange();
        this.CalResult = aLAWageCtrlRangeSchema.getCalResult();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("CtrlRangeType") == null)
            {
                this.CtrlRangeType = null;
            }
            else
            {
                this.CtrlRangeType = rs.getString("CtrlRangeType").trim();
            }

            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("StartRange") == null)
            {
                this.StartRange = null;
            }
            else
            {
                this.StartRange = rs.getString("StartRange").trim();
            }

            if (rs.getString("EndRange") == null)
            {
                this.EndRange = null;
            }
            else
            {
                this.EndRange = rs.getString("EndRange").trim();
            }

            if (rs.getString("CalResult") == null)
            {
                this.CalResult = null;
            }
            else
            {
                this.CalResult = rs.getString("CalResult").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAWageCtrlRangeSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LAWageCtrlRangeSchema getSchema()
    {
        LAWageCtrlRangeSchema aLAWageCtrlRangeSchema = new
                LAWageCtrlRangeSchema();
        aLAWageCtrlRangeSchema.setSchema(this);
        return aLAWageCtrlRangeSchema;
    }

    public LAWageCtrlRangeDB getDB()
    {
        LAWageCtrlRangeDB aDBOper = new LAWageCtrlRangeDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAWageCtrlRange描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(CtrlRangeType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(StartRange)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(EndRange)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CalResult));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAWageCtrlRange>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            CtrlRangeType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                           SysConst.PACKAGESPILTER);
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            StartRange = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                        SysConst.PACKAGESPILTER);
            EndRange = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                      SysConst.PACKAGESPILTER);
            CalResult = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                       SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAWageCtrlRangeSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("CtrlRangeType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CtrlRangeType));
        }
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equals("StartRange"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StartRange));
        }
        if (FCode.equals("EndRange"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EndRange));
        }
        if (FCode.equals("CalResult"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalResult));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(CtrlRangeType);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(StartRange);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(EndRange);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(CalResult);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("CtrlRangeType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CtrlRangeType = FValue.trim();
            }
            else
            {
                CtrlRangeType = null;
            }
        }
        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("StartRange"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                StartRange = FValue.trim();
            }
            else
            {
                StartRange = null;
            }
        }
        if (FCode.equals("EndRange"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EndRange = FValue.trim();
            }
            else
            {
                EndRange = null;
            }
        }
        if (FCode.equals("CalResult"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CalResult = FValue.trim();
            }
            else
            {
                CalResult = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LAWageCtrlRangeSchema other = (LAWageCtrlRangeSchema) otherObject;
        return
                CtrlRangeType.equals(other.getCtrlRangeType())
                && RiskCode.equals(other.getRiskCode())
                && StartRange.equals(other.getStartRange())
                && EndRange.equals(other.getEndRange())
                && CalResult.equals(other.getCalResult());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("CtrlRangeType"))
        {
            return 0;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return 1;
        }
        if (strFieldName.equals("StartRange"))
        {
            return 2;
        }
        if (strFieldName.equals("EndRange"))
        {
            return 3;
        }
        if (strFieldName.equals("CalResult"))
        {
            return 4;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "CtrlRangeType";
                break;
            case 1:
                strFieldName = "RiskCode";
                break;
            case 2:
                strFieldName = "StartRange";
                break;
            case 3:
                strFieldName = "EndRange";
                break;
            case 4:
                strFieldName = "CalResult";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("CtrlRangeType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StartRange"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EndRange"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CalResult"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
