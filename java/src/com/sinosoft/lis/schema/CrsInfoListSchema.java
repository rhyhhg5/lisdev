/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.CrsInfoListDB;

/*
 * <p>ClassName: CrsInfoListSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 交叉销售数据存储表
 * @CreateDate：2017-03-02
 */
public class CrsInfoListSchema implements Schema, Cloneable
{
	// @Field
	/** 流水号 */
	private String EdorNo;
	/** 保单号 */
	private String ContNo;
	/** 销售渠道 */
	private String SaleChnl;
	/** 业务员代码 */
	private String AgentCode;
	/** 业务员组别 */
	private String AgentGroup;
	/** 中介机构代码 */
	private String AgentCom;
	/** 交叉销售渠道 */
	private String Crs_BussType;
	/** 交叉销售类型 */
	private String Crs_SaleChnl;
	/** 对方业务员代码 */
	private String GrpagentCode;
	/** 对方业务员名称 */
	private String GrpagentName;
	/** 对方业务员身份证 */
	private String GrpagentIdNo;
	/** 对方机构代码 */
	private String GrpagentCom;
	/** 状态 */
	private String State;
	/** 失败原因 */
	private String FalseReason;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 处理状态 */
	private String StateFlag;
	/** 保单类型 */
	private String ContType;
	/** 其他 */
	private String Other;

	public static final int FIELDNUM = 21;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public CrsInfoListSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "EdorNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		CrsInfoListSchema cloned = (CrsInfoListSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getEdorNo()
	{
		return EdorNo;
	}
	public void setEdorNo(String aEdorNo)
	{
		EdorNo = aEdorNo;
	}
	public String getContNo()
	{
		return ContNo;
	}
	public void setContNo(String aContNo)
	{
		ContNo = aContNo;
	}
	public String getSaleChnl()
	{
		return SaleChnl;
	}
	public void setSaleChnl(String aSaleChnl)
	{
		SaleChnl = aSaleChnl;
	}
	public String getAgentCode()
	{
		return AgentCode;
	}
	public void setAgentCode(String aAgentCode)
	{
		AgentCode = aAgentCode;
	}
	public String getAgentGroup()
	{
		return AgentGroup;
	}
	public void setAgentGroup(String aAgentGroup)
	{
		AgentGroup = aAgentGroup;
	}
	public String getAgentCom()
	{
		return AgentCom;
	}
	public void setAgentCom(String aAgentCom)
	{
		AgentCom = aAgentCom;
	}
	public String getCrs_BussType()
	{
		return Crs_BussType;
	}
	public void setCrs_BussType(String aCrs_BussType)
	{
		Crs_BussType = aCrs_BussType;
	}
	public String getCrs_SaleChnl()
	{
		return Crs_SaleChnl;
	}
	public void setCrs_SaleChnl(String aCrs_SaleChnl)
	{
		Crs_SaleChnl = aCrs_SaleChnl;
	}
	public String getGrpagentCode()
	{
		return GrpagentCode;
	}
	public void setGrpagentCode(String aGrpagentCode)
	{
		GrpagentCode = aGrpagentCode;
	}
	public String getGrpagentName()
	{
		return GrpagentName;
	}
	public void setGrpagentName(String aGrpagentName)
	{
		GrpagentName = aGrpagentName;
	}
	public String getGrpagentIdNo()
	{
		return GrpagentIdNo;
	}
	public void setGrpagentIdNo(String aGrpagentIdNo)
	{
		GrpagentIdNo = aGrpagentIdNo;
	}
	public String getGrpagentCom()
	{
		return GrpagentCom;
	}
	public void setGrpagentCom(String aGrpagentCom)
	{
		GrpagentCom = aGrpagentCom;
	}
	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}
	public String getFalseReason()
	{
		return FalseReason;
	}
	public void setFalseReason(String aFalseReason)
	{
		FalseReason = aFalseReason;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getStateFlag()
	{
		return StateFlag;
	}
	public void setStateFlag(String aStateFlag)
	{
		StateFlag = aStateFlag;
	}
	public String getContType()
	{
		return ContType;
	}
	public void setContType(String aContType)
	{
		ContType = aContType;
	}
	public String getOther()
	{
		return Other;
	}
	public void setOther(String aOther)
	{
		Other = aOther;
	}

	/**
	* 使用另外一个 CrsInfoListSchema 对象给 Schema 赋值
	* @param: aCrsInfoListSchema CrsInfoListSchema
	**/
	public void setSchema(CrsInfoListSchema aCrsInfoListSchema)
	{
		this.EdorNo = aCrsInfoListSchema.getEdorNo();
		this.ContNo = aCrsInfoListSchema.getContNo();
		this.SaleChnl = aCrsInfoListSchema.getSaleChnl();
		this.AgentCode = aCrsInfoListSchema.getAgentCode();
		this.AgentGroup = aCrsInfoListSchema.getAgentGroup();
		this.AgentCom = aCrsInfoListSchema.getAgentCom();
		this.Crs_BussType = aCrsInfoListSchema.getCrs_BussType();
		this.Crs_SaleChnl = aCrsInfoListSchema.getCrs_SaleChnl();
		this.GrpagentCode = aCrsInfoListSchema.getGrpagentCode();
		this.GrpagentName = aCrsInfoListSchema.getGrpagentName();
		this.GrpagentIdNo = aCrsInfoListSchema.getGrpagentIdNo();
		this.GrpagentCom = aCrsInfoListSchema.getGrpagentCom();
		this.State = aCrsInfoListSchema.getState();
		this.FalseReason = aCrsInfoListSchema.getFalseReason();
		this.MakeDate = fDate.getDate( aCrsInfoListSchema.getMakeDate());
		this.MakeTime = aCrsInfoListSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aCrsInfoListSchema.getModifyDate());
		this.ModifyTime = aCrsInfoListSchema.getModifyTime();
		this.StateFlag = aCrsInfoListSchema.getStateFlag();
		this.ContType = aCrsInfoListSchema.getContType();
		this.Other = aCrsInfoListSchema.getOther();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("EdorNo") == null )
				this.EdorNo = null;
			else
				this.EdorNo = rs.getString("EdorNo").trim();

			if( rs.getString("ContNo") == null )
				this.ContNo = null;
			else
				this.ContNo = rs.getString("ContNo").trim();

			if( rs.getString("SaleChnl") == null )
				this.SaleChnl = null;
			else
				this.SaleChnl = rs.getString("SaleChnl").trim();

			if( rs.getString("AgentCode") == null )
				this.AgentCode = null;
			else
				this.AgentCode = rs.getString("AgentCode").trim();

			if( rs.getString("AgentGroup") == null )
				this.AgentGroup = null;
			else
				this.AgentGroup = rs.getString("AgentGroup").trim();

			if( rs.getString("AgentCom") == null )
				this.AgentCom = null;
			else
				this.AgentCom = rs.getString("AgentCom").trim();

			if( rs.getString("Crs_BussType") == null )
				this.Crs_BussType = null;
			else
				this.Crs_BussType = rs.getString("Crs_BussType").trim();

			if( rs.getString("Crs_SaleChnl") == null )
				this.Crs_SaleChnl = null;
			else
				this.Crs_SaleChnl = rs.getString("Crs_SaleChnl").trim();

			if( rs.getString("GrpagentCode") == null )
				this.GrpagentCode = null;
			else
				this.GrpagentCode = rs.getString("GrpagentCode").trim();

			if( rs.getString("GrpagentName") == null )
				this.GrpagentName = null;
			else
				this.GrpagentName = rs.getString("GrpagentName").trim();

			if( rs.getString("GrpagentIdNo") == null )
				this.GrpagentIdNo = null;
			else
				this.GrpagentIdNo = rs.getString("GrpagentIdNo").trim();

			if( rs.getString("GrpagentCom") == null )
				this.GrpagentCom = null;
			else
				this.GrpagentCom = rs.getString("GrpagentCom").trim();

			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			if( rs.getString("FalseReason") == null )
				this.FalseReason = null;
			else
				this.FalseReason = rs.getString("FalseReason").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("StateFlag") == null )
				this.StateFlag = null;
			else
				this.StateFlag = rs.getString("StateFlag").trim();

			if( rs.getString("ContType") == null )
				this.ContType = null;
			else
				this.ContType = rs.getString("ContType").trim();

			if( rs.getString("Other") == null )
				this.Other = null;
			else
				this.Other = rs.getString("Other").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的CrsInfoList表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "CrsInfoListSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public CrsInfoListSchema getSchema()
	{
		CrsInfoListSchema aCrsInfoListSchema = new CrsInfoListSchema();
		aCrsInfoListSchema.setSchema(this);
		return aCrsInfoListSchema;
	}

	public CrsInfoListDB getDB()
	{
		CrsInfoListDB aDBOper = new CrsInfoListDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpCrsInfoList描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(EdorNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SaleChnl)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentGroup)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Crs_BussType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Crs_SaleChnl)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpagentCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpagentName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpagentIdNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpagentCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FalseReason)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StateFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Other));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpCrsInfoList>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			EdorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			AgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			Crs_BussType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			Crs_SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			GrpagentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			GrpagentName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			GrpagentIdNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			GrpagentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			FalseReason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			StateFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			ContType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			Other = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "CrsInfoListSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("EdorNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EdorNo));
		}
		if (FCode.equals("ContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
		}
		if (FCode.equals("SaleChnl"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
		}
		if (FCode.equals("AgentCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
		}
		if (FCode.equals("AgentGroup"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
		}
		if (FCode.equals("AgentCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
		}
		if (FCode.equals("Crs_BussType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Crs_BussType));
		}
		if (FCode.equals("Crs_SaleChnl"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Crs_SaleChnl));
		}
		if (FCode.equals("GrpagentCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpagentCode));
		}
		if (FCode.equals("GrpagentName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpagentName));
		}
		if (FCode.equals("GrpagentIdNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpagentIdNo));
		}
		if (FCode.equals("GrpagentCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpagentCom));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("FalseReason"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FalseReason));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("StateFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StateFlag));
		}
		if (FCode.equals("ContType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContType));
		}
		if (FCode.equals("Other"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Other));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(EdorNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(ContNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(SaleChnl);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(AgentCode);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(AgentGroup);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(AgentCom);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(Crs_BussType);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(Crs_SaleChnl);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(GrpagentCode);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(GrpagentName);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(GrpagentIdNo);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(GrpagentCom);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(FalseReason);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(StateFlag);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(ContType);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(Other);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("EdorNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EdorNo = FValue.trim();
			}
			else
				EdorNo = null;
		}
		if (FCode.equalsIgnoreCase("ContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContNo = FValue.trim();
			}
			else
				ContNo = null;
		}
		if (FCode.equalsIgnoreCase("SaleChnl"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SaleChnl = FValue.trim();
			}
			else
				SaleChnl = null;
		}
		if (FCode.equalsIgnoreCase("AgentCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCode = FValue.trim();
			}
			else
				AgentCode = null;
		}
		if (FCode.equalsIgnoreCase("AgentGroup"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentGroup = FValue.trim();
			}
			else
				AgentGroup = null;
		}
		if (FCode.equalsIgnoreCase("AgentCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCom = FValue.trim();
			}
			else
				AgentCom = null;
		}
		if (FCode.equalsIgnoreCase("Crs_BussType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Crs_BussType = FValue.trim();
			}
			else
				Crs_BussType = null;
		}
		if (FCode.equalsIgnoreCase("Crs_SaleChnl"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Crs_SaleChnl = FValue.trim();
			}
			else
				Crs_SaleChnl = null;
		}
		if (FCode.equalsIgnoreCase("GrpagentCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpagentCode = FValue.trim();
			}
			else
				GrpagentCode = null;
		}
		if (FCode.equalsIgnoreCase("GrpagentName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpagentName = FValue.trim();
			}
			else
				GrpagentName = null;
		}
		if (FCode.equalsIgnoreCase("GrpagentIdNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpagentIdNo = FValue.trim();
			}
			else
				GrpagentIdNo = null;
		}
		if (FCode.equalsIgnoreCase("GrpagentCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpagentCom = FValue.trim();
			}
			else
				GrpagentCom = null;
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("FalseReason"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FalseReason = FValue.trim();
			}
			else
				FalseReason = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("StateFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StateFlag = FValue.trim();
			}
			else
				StateFlag = null;
		}
		if (FCode.equalsIgnoreCase("ContType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContType = FValue.trim();
			}
			else
				ContType = null;
		}
		if (FCode.equalsIgnoreCase("Other"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Other = FValue.trim();
			}
			else
				Other = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		CrsInfoListSchema other = (CrsInfoListSchema)otherObject;
		return
			(EdorNo == null ? other.getEdorNo() == null : EdorNo.equals(other.getEdorNo()))
			&& (ContNo == null ? other.getContNo() == null : ContNo.equals(other.getContNo()))
			&& (SaleChnl == null ? other.getSaleChnl() == null : SaleChnl.equals(other.getSaleChnl()))
			&& (AgentCode == null ? other.getAgentCode() == null : AgentCode.equals(other.getAgentCode()))
			&& (AgentGroup == null ? other.getAgentGroup() == null : AgentGroup.equals(other.getAgentGroup()))
			&& (AgentCom == null ? other.getAgentCom() == null : AgentCom.equals(other.getAgentCom()))
			&& (Crs_BussType == null ? other.getCrs_BussType() == null : Crs_BussType.equals(other.getCrs_BussType()))
			&& (Crs_SaleChnl == null ? other.getCrs_SaleChnl() == null : Crs_SaleChnl.equals(other.getCrs_SaleChnl()))
			&& (GrpagentCode == null ? other.getGrpagentCode() == null : GrpagentCode.equals(other.getGrpagentCode()))
			&& (GrpagentName == null ? other.getGrpagentName() == null : GrpagentName.equals(other.getGrpagentName()))
			&& (GrpagentIdNo == null ? other.getGrpagentIdNo() == null : GrpagentIdNo.equals(other.getGrpagentIdNo()))
			&& (GrpagentCom == null ? other.getGrpagentCom() == null : GrpagentCom.equals(other.getGrpagentCom()))
			&& (State == null ? other.getState() == null : State.equals(other.getState()))
			&& (FalseReason == null ? other.getFalseReason() == null : FalseReason.equals(other.getFalseReason()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (StateFlag == null ? other.getStateFlag() == null : StateFlag.equals(other.getStateFlag()))
			&& (ContType == null ? other.getContType() == null : ContType.equals(other.getContType()))
			&& (Other == null ? other.getOther() == null : Other.equals(other.getOther()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("EdorNo") ) {
			return 0;
		}
		if( strFieldName.equals("ContNo") ) {
			return 1;
		}
		if( strFieldName.equals("SaleChnl") ) {
			return 2;
		}
		if( strFieldName.equals("AgentCode") ) {
			return 3;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return 4;
		}
		if( strFieldName.equals("AgentCom") ) {
			return 5;
		}
		if( strFieldName.equals("Crs_BussType") ) {
			return 6;
		}
		if( strFieldName.equals("Crs_SaleChnl") ) {
			return 7;
		}
		if( strFieldName.equals("GrpagentCode") ) {
			return 8;
		}
		if( strFieldName.equals("GrpagentName") ) {
			return 9;
		}
		if( strFieldName.equals("GrpagentIdNo") ) {
			return 10;
		}
		if( strFieldName.equals("GrpagentCom") ) {
			return 11;
		}
		if( strFieldName.equals("State") ) {
			return 12;
		}
		if( strFieldName.equals("FalseReason") ) {
			return 13;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 14;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 15;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 16;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 17;
		}
		if( strFieldName.equals("StateFlag") ) {
			return 18;
		}
		if( strFieldName.equals("ContType") ) {
			return 19;
		}
		if( strFieldName.equals("Other") ) {
			return 20;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "EdorNo";
				break;
			case 1:
				strFieldName = "ContNo";
				break;
			case 2:
				strFieldName = "SaleChnl";
				break;
			case 3:
				strFieldName = "AgentCode";
				break;
			case 4:
				strFieldName = "AgentGroup";
				break;
			case 5:
				strFieldName = "AgentCom";
				break;
			case 6:
				strFieldName = "Crs_BussType";
				break;
			case 7:
				strFieldName = "Crs_SaleChnl";
				break;
			case 8:
				strFieldName = "GrpagentCode";
				break;
			case 9:
				strFieldName = "GrpagentName";
				break;
			case 10:
				strFieldName = "GrpagentIdNo";
				break;
			case 11:
				strFieldName = "GrpagentCom";
				break;
			case 12:
				strFieldName = "State";
				break;
			case 13:
				strFieldName = "FalseReason";
				break;
			case 14:
				strFieldName = "MakeDate";
				break;
			case 15:
				strFieldName = "MakeTime";
				break;
			case 16:
				strFieldName = "ModifyDate";
				break;
			case 17:
				strFieldName = "ModifyTime";
				break;
			case 18:
				strFieldName = "StateFlag";
				break;
			case 19:
				strFieldName = "ContType";
				break;
			case 20:
				strFieldName = "Other";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("EdorNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SaleChnl") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Crs_BussType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Crs_SaleChnl") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpagentCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpagentName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpagentIdNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpagentCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FalseReason") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StateFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Other") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
