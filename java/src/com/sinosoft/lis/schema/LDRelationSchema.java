/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LDRelationDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LDRelationSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-07-07
 */
public class LDRelationSchema implements Schema, Cloneable {
    // @Field
    /** 与投保人关系 */
    private String RelationToAppnt;
    /** 其他被保人与主被保人关系 */
    private String RelationToMainInsured;
    /** 其他被保人与投保人关系 */
    private String InsuredToAppnt;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 8; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LDRelationSchema() {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "RelationToAppnt";
        pk[1] = "RelationToMainInsured";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LDRelationSchema cloned = (LDRelationSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getRelationToAppnt() {
        return RelationToAppnt;
    }

    public void setRelationToAppnt(String aRelationToAppnt) {
        RelationToAppnt = aRelationToAppnt;
    }

    public String getRelationToMainInsured() {
        return RelationToMainInsured;
    }

    public void setRelationToMainInsured(String aRelationToMainInsured) {
        RelationToMainInsured = aRelationToMainInsured;
    }

    public String getInsuredToAppnt() {
        return InsuredToAppnt;
    }

    public void setInsuredToAppnt(String aInsuredToAppnt) {
        InsuredToAppnt = aInsuredToAppnt;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LDRelationSchema 对象给 Schema 赋值
     * @param: aLDRelationSchema LDRelationSchema
     **/
    public void setSchema(LDRelationSchema aLDRelationSchema) {
        this.RelationToAppnt = aLDRelationSchema.getRelationToAppnt();
        this.RelationToMainInsured = aLDRelationSchema.getRelationToMainInsured();
        this.InsuredToAppnt = aLDRelationSchema.getInsuredToAppnt();
        this.Operator = aLDRelationSchema.getOperator();
        this.MakeDate = fDate.getDate(aLDRelationSchema.getMakeDate());
        this.MakeTime = aLDRelationSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLDRelationSchema.getModifyDate());
        this.ModifyTime = aLDRelationSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("RelationToAppnt") == null) {
                this.RelationToAppnt = null;
            } else {
                this.RelationToAppnt = rs.getString("RelationToAppnt").trim();
            }

            if (rs.getString("RelationToMainInsured") == null) {
                this.RelationToMainInsured = null;
            } else {
                this.RelationToMainInsured = rs.getString(
                        "RelationToMainInsured").trim();
            }

            if (rs.getString("InsuredToAppnt") == null) {
                this.InsuredToAppnt = null;
            } else {
                this.InsuredToAppnt = rs.getString("InsuredToAppnt").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LDRelation表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDRelationSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LDRelationSchema getSchema() {
        LDRelationSchema aLDRelationSchema = new LDRelationSchema();
        aLDRelationSchema.setSchema(this);
        return aLDRelationSchema;
    }

    public LDRelationDB getDB() {
        LDRelationDB aDBOper = new LDRelationDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDRelation描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(RelationToAppnt));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RelationToMainInsured));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuredToAppnt));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDRelation>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            RelationToAppnt = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             1, SysConst.PACKAGESPILTER);
            RelationToMainInsured = StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 2, SysConst.PACKAGESPILTER);
            InsuredToAppnt = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                            SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 5, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 7, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                        SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDRelationSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("RelationToAppnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelationToAppnt));
        }
        if (FCode.equals("RelationToMainInsured")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(
                    RelationToMainInsured));
        }
        if (FCode.equals("InsuredToAppnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredToAppnt));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(RelationToAppnt);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(RelationToMainInsured);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(InsuredToAppnt);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("RelationToAppnt")) {
            if (FValue != null && !FValue.equals("")) {
                RelationToAppnt = FValue.trim();
            } else {
                RelationToAppnt = null;
            }
        }
        if (FCode.equalsIgnoreCase("RelationToMainInsured")) {
            if (FValue != null && !FValue.equals("")) {
                RelationToMainInsured = FValue.trim();
            } else {
                RelationToMainInsured = null;
            }
        }
        if (FCode.equalsIgnoreCase("InsuredToAppnt")) {
            if (FValue != null && !FValue.equals("")) {
                InsuredToAppnt = FValue.trim();
            } else {
                InsuredToAppnt = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LDRelationSchema other = (LDRelationSchema) otherObject;
        return
                RelationToAppnt.equals(other.getRelationToAppnt())
                && RelationToMainInsured.equals(other.getRelationToMainInsured())
                && InsuredToAppnt.equals(other.getInsuredToAppnt())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("RelationToAppnt")) {
            return 0;
        }
        if (strFieldName.equals("RelationToMainInsured")) {
            return 1;
        }
        if (strFieldName.equals("InsuredToAppnt")) {
            return 2;
        }
        if (strFieldName.equals("Operator")) {
            return 3;
        }
        if (strFieldName.equals("MakeDate")) {
            return 4;
        }
        if (strFieldName.equals("MakeTime")) {
            return 5;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 6;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 7;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "RelationToAppnt";
            break;
        case 1:
            strFieldName = "RelationToMainInsured";
            break;
        case 2:
            strFieldName = "InsuredToAppnt";
            break;
        case 3:
            strFieldName = "Operator";
            break;
        case 4:
            strFieldName = "MakeDate";
            break;
        case 5:
            strFieldName = "MakeTime";
            break;
        case 6:
            strFieldName = "ModifyDate";
            break;
        case 7:
            strFieldName = "ModifyTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("RelationToAppnt")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RelationToMainInsured")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsuredToAppnt")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
