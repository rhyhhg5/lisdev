/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LDDrugDB;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LDDrugSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 需修改的表
 * @CreateDate：2005-05-11
 */
public class LDDrugSchema implements Schema, Cloneable
{
    // @Field
    /** 药品代码 */
    private String DrugCode;
    /** 目录标志 */
    private String IndexFlag;
    /** 药品分类（中西药） */
    private String DrugClass;
    /** 药品名称 */
    private String DrugName;
    /** 拉丁名称 */
    private String DrugLtName;
    /** 英文名称 */
    private String DrugEnName;
    /** 中文简称 */
    private String DrugNameAbbr;
    /** 药品规格代码 */
    private String DrugSpecCode;
    /** 药品剂型代码 */
    private String DrugDoseCode;
    /** 计价单位代码 */
    private String DrugMetrCode;
    /** 使用说明 */
    private String DrgUsage;
    /** 标准价格 */
    private double DrgStdPrice;
    /** 处方药标志 */
    private String RecipeFlag;
    /** 新特药标志 */
    private String SpecialFLag;
    /** 限制使用范围 */
    private String UseLimit;

    public static final int FIELDNUM = 15; // 数据库表的字段个数

    private static String[] PK; // 主键

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LDDrugSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "DrugCode";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException
    {
        LDDrugSchema cloned = (LDDrugSchema)super.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getDrugCode()
    {
        if (SysConst.CHANGECHARSET && DrugCode != null && !DrugCode.equals(""))
        {
            DrugCode = StrTool.unicodeToGBK(DrugCode);
        }
        return DrugCode;
    }

    public void setDrugCode(String aDrugCode)
    {
        DrugCode = aDrugCode;
    }

    public String getIndexFlag()
    {
        if (SysConst.CHANGECHARSET && IndexFlag != null && !IndexFlag.equals(""))
        {
            IndexFlag = StrTool.unicodeToGBK(IndexFlag);
        }
        return IndexFlag;
    }

    public void setIndexFlag(String aIndexFlag)
    {
        IndexFlag = aIndexFlag;
    }

    public String getDrugClass()
    {
        if (SysConst.CHANGECHARSET && DrugClass != null && !DrugClass.equals(""))
        {
            DrugClass = StrTool.unicodeToGBK(DrugClass);
        }
        return DrugClass;
    }

    public void setDrugClass(String aDrugClass)
    {
        DrugClass = aDrugClass;
    }

    public String getDrugName()
    {
        if (SysConst.CHANGECHARSET && DrugName != null && !DrugName.equals(""))
        {
            DrugName = StrTool.unicodeToGBK(DrugName);
        }
        return DrugName;
    }

    public void setDrugName(String aDrugName)
    {
        DrugName = aDrugName;
    }

    public String getDrugLtName()
    {
        if (SysConst.CHANGECHARSET && DrugLtName != null &&
            !DrugLtName.equals(""))
        {
            DrugLtName = StrTool.unicodeToGBK(DrugLtName);
        }
        return DrugLtName;
    }

    public void setDrugLtName(String aDrugLtName)
    {
        DrugLtName = aDrugLtName;
    }

    public String getDrugEnName()
    {
        if (SysConst.CHANGECHARSET && DrugEnName != null &&
            !DrugEnName.equals(""))
        {
            DrugEnName = StrTool.unicodeToGBK(DrugEnName);
        }
        return DrugEnName;
    }

    public void setDrugEnName(String aDrugEnName)
    {
        DrugEnName = aDrugEnName;
    }

    public String getDrugNameAbbr()
    {
        if (SysConst.CHANGECHARSET && DrugNameAbbr != null &&
            !DrugNameAbbr.equals(""))
        {
            DrugNameAbbr = StrTool.unicodeToGBK(DrugNameAbbr);
        }
        return DrugNameAbbr;
    }

    public void setDrugNameAbbr(String aDrugNameAbbr)
    {
        DrugNameAbbr = aDrugNameAbbr;
    }

    public String getDrugSpecCode()
    {
        if (SysConst.CHANGECHARSET && DrugSpecCode != null &&
            !DrugSpecCode.equals(""))
        {
            DrugSpecCode = StrTool.unicodeToGBK(DrugSpecCode);
        }
        return DrugSpecCode;
    }

    public void setDrugSpecCode(String aDrugSpecCode)
    {
        DrugSpecCode = aDrugSpecCode;
    }

    public String getDrugDoseCode()
    {
        if (SysConst.CHANGECHARSET && DrugDoseCode != null &&
            !DrugDoseCode.equals(""))
        {
            DrugDoseCode = StrTool.unicodeToGBK(DrugDoseCode);
        }
        return DrugDoseCode;
    }

    public void setDrugDoseCode(String aDrugDoseCode)
    {
        DrugDoseCode = aDrugDoseCode;
    }

    public String getDrugMetrCode()
    {
        if (SysConst.CHANGECHARSET && DrugMetrCode != null &&
            !DrugMetrCode.equals(""))
        {
            DrugMetrCode = StrTool.unicodeToGBK(DrugMetrCode);
        }
        return DrugMetrCode;
    }

    public void setDrugMetrCode(String aDrugMetrCode)
    {
        DrugMetrCode = aDrugMetrCode;
    }

    public String getDrgUsage()
    {
        if (SysConst.CHANGECHARSET && DrgUsage != null && !DrgUsage.equals(""))
        {
            DrgUsage = StrTool.unicodeToGBK(DrgUsage);
        }
        return DrgUsage;
    }

    public void setDrgUsage(String aDrgUsage)
    {
        DrgUsage = aDrgUsage;
    }

    public double getDrgStdPrice()
    {
        return DrgStdPrice;
    }

    public void setDrgStdPrice(double aDrgStdPrice)
    {
        DrgStdPrice = aDrgStdPrice;
    }

    public void setDrgStdPrice(String aDrgStdPrice)
    {
        if (aDrgStdPrice != null && !aDrgStdPrice.equals(""))
        {
            Double tDouble = new Double(aDrgStdPrice);
            double d = tDouble.doubleValue();
            DrgStdPrice = d;
        }
    }

    public String getRecipeFlag()
    {
        if (SysConst.CHANGECHARSET && RecipeFlag != null &&
            !RecipeFlag.equals(""))
        {
            RecipeFlag = StrTool.unicodeToGBK(RecipeFlag);
        }
        return RecipeFlag;
    }

    public void setRecipeFlag(String aRecipeFlag)
    {
        RecipeFlag = aRecipeFlag;
    }

    public String getSpecialFLag()
    {
        if (SysConst.CHANGECHARSET && SpecialFLag != null &&
            !SpecialFLag.equals(""))
        {
            SpecialFLag = StrTool.unicodeToGBK(SpecialFLag);
        }
        return SpecialFLag;
    }

    public void setSpecialFLag(String aSpecialFLag)
    {
        SpecialFLag = aSpecialFLag;
    }

    public String getUseLimit()
    {
        if (SysConst.CHANGECHARSET && UseLimit != null && !UseLimit.equals(""))
        {
            UseLimit = StrTool.unicodeToGBK(UseLimit);
        }
        return UseLimit;
    }

    public void setUseLimit(String aUseLimit)
    {
        UseLimit = aUseLimit;
    }

    /**
     * 使用另外一个 LDDrugSchema 对象给 Schema 赋值
     * @param: aLDDrugSchema LDDrugSchema
     **/
    public void setSchema(LDDrugSchema aLDDrugSchema)
    {
        this.DrugCode = aLDDrugSchema.getDrugCode();
        this.IndexFlag = aLDDrugSchema.getIndexFlag();
        this.DrugClass = aLDDrugSchema.getDrugClass();
        this.DrugName = aLDDrugSchema.getDrugName();
        this.DrugLtName = aLDDrugSchema.getDrugLtName();
        this.DrugEnName = aLDDrugSchema.getDrugEnName();
        this.DrugNameAbbr = aLDDrugSchema.getDrugNameAbbr();
        this.DrugSpecCode = aLDDrugSchema.getDrugSpecCode();
        this.DrugDoseCode = aLDDrugSchema.getDrugDoseCode();
        this.DrugMetrCode = aLDDrugSchema.getDrugMetrCode();
        this.DrgUsage = aLDDrugSchema.getDrgUsage();
        this.DrgStdPrice = aLDDrugSchema.getDrgStdPrice();
        this.RecipeFlag = aLDDrugSchema.getRecipeFlag();
        this.SpecialFLag = aLDDrugSchema.getSpecialFLag();
        this.UseLimit = aLDDrugSchema.getUseLimit();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("DrugCode") == null)
            {
                this.DrugCode = null;
            }
            else
            {
                this.DrugCode = rs.getString("DrugCode").trim();
            }

            if (rs.getString("IndexFlag") == null)
            {
                this.IndexFlag = null;
            }
            else
            {
                this.IndexFlag = rs.getString("IndexFlag").trim();
            }

            if (rs.getString("DrugClass") == null)
            {
                this.DrugClass = null;
            }
            else
            {
                this.DrugClass = rs.getString("DrugClass").trim();
            }

            if (rs.getString("DrugName") == null)
            {
                this.DrugName = null;
            }
            else
            {
                this.DrugName = rs.getString("DrugName").trim();
            }

            if (rs.getString("DrugLtName") == null)
            {
                this.DrugLtName = null;
            }
            else
            {
                this.DrugLtName = rs.getString("DrugLtName").trim();
            }

            if (rs.getString("DrugEnName") == null)
            {
                this.DrugEnName = null;
            }
            else
            {
                this.DrugEnName = rs.getString("DrugEnName").trim();
            }

            if (rs.getString("DrugNameAbbr") == null)
            {
                this.DrugNameAbbr = null;
            }
            else
            {
                this.DrugNameAbbr = rs.getString("DrugNameAbbr").trim();
            }

            if (rs.getString("DrugSpecCode") == null)
            {
                this.DrugSpecCode = null;
            }
            else
            {
                this.DrugSpecCode = rs.getString("DrugSpecCode").trim();
            }

            if (rs.getString("DrugDoseCode") == null)
            {
                this.DrugDoseCode = null;
            }
            else
            {
                this.DrugDoseCode = rs.getString("DrugDoseCode").trim();
            }

            if (rs.getString("DrugMetrCode") == null)
            {
                this.DrugMetrCode = null;
            }
            else
            {
                this.DrugMetrCode = rs.getString("DrugMetrCode").trim();
            }

            if (rs.getString("DrgUsage") == null)
            {
                this.DrgUsage = null;
            }
            else
            {
                this.DrgUsage = rs.getString("DrgUsage").trim();
            }

            this.DrgStdPrice = rs.getDouble("DrgStdPrice");
            if (rs.getString("RecipeFlag") == null)
            {
                this.RecipeFlag = null;
            }
            else
            {
                this.RecipeFlag = rs.getString("RecipeFlag").trim();
            }

            if (rs.getString("SpecialFLag") == null)
            {
                this.SpecialFLag = null;
            }
            else
            {
                this.SpecialFLag = rs.getString("SpecialFLag").trim();
            }

            if (rs.getString("UseLimit") == null)
            {
                this.UseLimit = null;
            }
            else
            {
                this.UseLimit = rs.getString("UseLimit").trim();
            }

        }
        catch (SQLException sqle)
        {
            System.out.println("数据库中的LDDrug表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDDrugSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LDDrugSchema getSchema()
    {
        LDDrugSchema aLDDrugSchema = new LDDrugSchema();
        aLDDrugSchema.setSchema(this);
        return aLDDrugSchema;
    }

    public LDDrugDB getDB()
    {
        LDDrugDB aDBOper = new LDDrugDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDDrug描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(DrugCode)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(IndexFlag)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(DrugClass)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(DrugName)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(DrugLtName)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(DrugEnName)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(DrugNameAbbr)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(DrugSpecCode)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(DrugDoseCode)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(DrugMetrCode)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(DrgUsage)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DrgStdPrice));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(RecipeFlag)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(SpecialFLag)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(UseLimit)));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDDrug>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            DrugCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            IndexFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                       SysConst.PACKAGESPILTER);
            DrugClass = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                       SysConst.PACKAGESPILTER);
            DrugName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                      SysConst.PACKAGESPILTER);
            DrugLtName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                        SysConst.PACKAGESPILTER);
            DrugEnName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                        SysConst.PACKAGESPILTER);
            DrugNameAbbr = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                          SysConst.PACKAGESPILTER);
            DrugSpecCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                          SysConst.PACKAGESPILTER);
            DrugDoseCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                          SysConst.PACKAGESPILTER);
            DrugMetrCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                          SysConst.PACKAGESPILTER);
            DrgUsage = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                      SysConst.PACKAGESPILTER);
            DrgStdPrice = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 12, SysConst.PACKAGESPILTER))).doubleValue();
            RecipeFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                        SysConst.PACKAGESPILTER);
            SpecialFLag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                         SysConst.PACKAGESPILTER);
            UseLimit = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                      SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDDrugSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("DrugCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DrugCode));
        }
        if (FCode.equals("IndexFlag"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IndexFlag));
        }
        if (FCode.equals("DrugClass"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DrugClass));
        }
        if (FCode.equals("DrugName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DrugName));
        }
        if (FCode.equals("DrugLtName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DrugLtName));
        }
        if (FCode.equals("DrugEnName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DrugEnName));
        }
        if (FCode.equals("DrugNameAbbr"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DrugNameAbbr));
        }
        if (FCode.equals("DrugSpecCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DrugSpecCode));
        }
        if (FCode.equals("DrugDoseCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DrugDoseCode));
        }
        if (FCode.equals("DrugMetrCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DrugMetrCode));
        }
        if (FCode.equals("DrgUsage"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DrgUsage));
        }
        if (FCode.equals("DrgStdPrice"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DrgStdPrice));
        }
        if (FCode.equals("RecipeFlag"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RecipeFlag));
        }
        if (FCode.equals("SpecialFLag"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SpecialFLag));
        }
        if (FCode.equals("UseLimit"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UseLimit));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(DrugCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(IndexFlag);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(DrugClass);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(DrugName);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(DrugLtName);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(DrugEnName);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(DrugNameAbbr);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(DrugSpecCode);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(DrugDoseCode);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(DrugMetrCode);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(DrgUsage);
                break;
            case 11:
                strFieldValue = String.valueOf(DrgStdPrice);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(RecipeFlag);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(SpecialFLag);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(UseLimit);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("DrugCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DrugCode = FValue.trim();
            }
            else
            {
                DrugCode = null;
            }
        }
        if (FCode.equals("IndexFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                IndexFlag = FValue.trim();
            }
            else
            {
                IndexFlag = null;
            }
        }
        if (FCode.equals("DrugClass"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DrugClass = FValue.trim();
            }
            else
            {
                DrugClass = null;
            }
        }
        if (FCode.equals("DrugName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DrugName = FValue.trim();
            }
            else
            {
                DrugName = null;
            }
        }
        if (FCode.equals("DrugLtName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DrugLtName = FValue.trim();
            }
            else
            {
                DrugLtName = null;
            }
        }
        if (FCode.equals("DrugEnName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DrugEnName = FValue.trim();
            }
            else
            {
                DrugEnName = null;
            }
        }
        if (FCode.equals("DrugNameAbbr"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DrugNameAbbr = FValue.trim();
            }
            else
            {
                DrugNameAbbr = null;
            }
        }
        if (FCode.equals("DrugSpecCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DrugSpecCode = FValue.trim();
            }
            else
            {
                DrugSpecCode = null;
            }
        }
        if (FCode.equals("DrugDoseCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DrugDoseCode = FValue.trim();
            }
            else
            {
                DrugDoseCode = null;
            }
        }
        if (FCode.equals("DrugMetrCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DrugMetrCode = FValue.trim();
            }
            else
            {
                DrugMetrCode = null;
            }
        }
        if (FCode.equals("DrgUsage"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DrgUsage = FValue.trim();
            }
            else
            {
                DrgUsage = null;
            }
        }
        if (FCode.equals("DrgStdPrice"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                DrgStdPrice = d;
            }
        }
        if (FCode.equals("RecipeFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RecipeFlag = FValue.trim();
            }
            else
            {
                RecipeFlag = null;
            }
        }
        if (FCode.equals("SpecialFLag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SpecialFLag = FValue.trim();
            }
            else
            {
                SpecialFLag = null;
            }
        }
        if (FCode.equals("UseLimit"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UseLimit = FValue.trim();
            }
            else
            {
                UseLimit = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LDDrugSchema other = (LDDrugSchema) otherObject;
        return
                DrugCode.equals(other.getDrugCode())
                && IndexFlag.equals(other.getIndexFlag())
                && DrugClass.equals(other.getDrugClass())
                && DrugName.equals(other.getDrugName())
                && DrugLtName.equals(other.getDrugLtName())
                && DrugEnName.equals(other.getDrugEnName())
                && DrugNameAbbr.equals(other.getDrugNameAbbr())
                && DrugSpecCode.equals(other.getDrugSpecCode())
                && DrugDoseCode.equals(other.getDrugDoseCode())
                && DrugMetrCode.equals(other.getDrugMetrCode())
                && DrgUsage.equals(other.getDrgUsage())
                && DrgStdPrice == other.getDrgStdPrice()
                && RecipeFlag.equals(other.getRecipeFlag())
                && SpecialFLag.equals(other.getSpecialFLag())
                && UseLimit.equals(other.getUseLimit());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("DrugCode"))
        {
            return 0;
        }
        if (strFieldName.equals("IndexFlag"))
        {
            return 1;
        }
        if (strFieldName.equals("DrugClass"))
        {
            return 2;
        }
        if (strFieldName.equals("DrugName"))
        {
            return 3;
        }
        if (strFieldName.equals("DrugLtName"))
        {
            return 4;
        }
        if (strFieldName.equals("DrugEnName"))
        {
            return 5;
        }
        if (strFieldName.equals("DrugNameAbbr"))
        {
            return 6;
        }
        if (strFieldName.equals("DrugSpecCode"))
        {
            return 7;
        }
        if (strFieldName.equals("DrugDoseCode"))
        {
            return 8;
        }
        if (strFieldName.equals("DrugMetrCode"))
        {
            return 9;
        }
        if (strFieldName.equals("DrgUsage"))
        {
            return 10;
        }
        if (strFieldName.equals("DrgStdPrice"))
        {
            return 11;
        }
        if (strFieldName.equals("RecipeFlag"))
        {
            return 12;
        }
        if (strFieldName.equals("SpecialFLag"))
        {
            return 13;
        }
        if (strFieldName.equals("UseLimit"))
        {
            return 14;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "DrugCode";
                break;
            case 1:
                strFieldName = "IndexFlag";
                break;
            case 2:
                strFieldName = "DrugClass";
                break;
            case 3:
                strFieldName = "DrugName";
                break;
            case 4:
                strFieldName = "DrugLtName";
                break;
            case 5:
                strFieldName = "DrugEnName";
                break;
            case 6:
                strFieldName = "DrugNameAbbr";
                break;
            case 7:
                strFieldName = "DrugSpecCode";
                break;
            case 8:
                strFieldName = "DrugDoseCode";
                break;
            case 9:
                strFieldName = "DrugMetrCode";
                break;
            case 10:
                strFieldName = "DrgUsage";
                break;
            case 11:
                strFieldName = "DrgStdPrice";
                break;
            case 12:
                strFieldName = "RecipeFlag";
                break;
            case 13:
                strFieldName = "SpecialFLag";
                break;
            case 14:
                strFieldName = "UseLimit";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("DrugCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("IndexFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DrugClass"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DrugName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DrugLtName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DrugEnName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DrugNameAbbr"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DrugSpecCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DrugDoseCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DrugMetrCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DrgUsage"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DrgStdPrice"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("RecipeFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SpecialFLag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UseLimit"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
