/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LACertificationDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LACertificationSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-03-23
 */
public class LACertificationSchema implements Schema
{
    // @Field
    /** 业务员编码 */
    private String AgentCode;
    /** 展业证书号 */
    private String CertifNo;
    /** 流水号 */
    private int Idx;
    /** 批准单位 */
    private String AuthorUnit;
    /** 发放日期 */
    private Date GrantDate;
    /** 失效日期 */
    private Date InvalidDate;
    /** 失效原因 */
    private String InvalidRsn;
    /** 补发日期 */
    private Date reissueDate;
    /** 补发原因 */
    private String reissueRsn;
    /** 有效日期 */
    private int ValidPeriod;
    /** 展业证书状态 */
    private String State;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 上一次修改日期 */
    private Date ModifyDate;
    /** 上一次修改时间 */
    private String ModifyTime;
    /** 有效起期 */
    private Date ValidStart;
    /** 有效止期 */
    private Date ValidEnd;
    /** 旧展业证号码 */
    private String OldCeritifNo;
    /** 操作员代码 */
    private String Operator;

    public static final int FIELDNUM = 19; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LACertificationSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "AgentCode";
        pk[1] = "CertifNo";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getAgentCode()
    {
        if (AgentCode != null && !AgentCode.equals("") &&
            SysConst.CHANGECHARSET)
        {
            AgentCode = StrTool.unicodeToGBK(AgentCode);
        }
        return AgentCode;
    }

    public void setAgentCode(String aAgentCode)
    {
        AgentCode = aAgentCode;
    }

    public String getCertifNo()
    {
        if (CertifNo != null && !CertifNo.equals("") && SysConst.CHANGECHARSET)
        {
            CertifNo = StrTool.unicodeToGBK(CertifNo);
        }
        return CertifNo;
    }

    public void setCertifNo(String aCertifNo)
    {
        CertifNo = aCertifNo;
    }

    public int getIdx()
    {
        return Idx;
    }

    public void setIdx(int aIdx)
    {
        Idx = aIdx;
    }

    public void setIdx(String aIdx)
    {
        if (aIdx != null && !aIdx.equals(""))
        {
            Integer tInteger = new Integer(aIdx);
            int i = tInteger.intValue();
            Idx = i;
        }
    }

    public String getAuthorUnit()
    {
        if (AuthorUnit != null && !AuthorUnit.equals("") &&
            SysConst.CHANGECHARSET)
        {
            AuthorUnit = StrTool.unicodeToGBK(AuthorUnit);
        }
        return AuthorUnit;
    }

    public void setAuthorUnit(String aAuthorUnit)
    {
        AuthorUnit = aAuthorUnit;
    }

    public String getGrantDate()
    {
        if (GrantDate != null)
        {
            return fDate.getString(GrantDate);
        }
        else
        {
            return null;
        }
    }

    public void setGrantDate(Date aGrantDate)
    {
        GrantDate = aGrantDate;
    }

    public void setGrantDate(String aGrantDate)
    {
        if (aGrantDate != null && !aGrantDate.equals(""))
        {
            GrantDate = fDate.getDate(aGrantDate);
        }
        else
        {
            GrantDate = null;
        }
    }

    public String getInvalidDate()
    {
        if (InvalidDate != null)
        {
            return fDate.getString(InvalidDate);
        }
        else
        {
            return null;
        }
    }

    public void setInvalidDate(Date aInvalidDate)
    {
        InvalidDate = aInvalidDate;
    }

    public void setInvalidDate(String aInvalidDate)
    {
        if (aInvalidDate != null && !aInvalidDate.equals(""))
        {
            InvalidDate = fDate.getDate(aInvalidDate);
        }
        else
        {
            InvalidDate = null;
        }
    }

    public String getInvalidRsn()
    {
        if (InvalidRsn != null && !InvalidRsn.equals("") &&
            SysConst.CHANGECHARSET)
        {
            InvalidRsn = StrTool.unicodeToGBK(InvalidRsn);
        }
        return InvalidRsn;
    }

    public void setInvalidRsn(String aInvalidRsn)
    {
        InvalidRsn = aInvalidRsn;
    }

    public String getreissueDate()
    {
        if (reissueDate != null)
        {
            return fDate.getString(reissueDate);
        }
        else
        {
            return null;
        }
    }

    public void setreissueDate(Date areissueDate)
    {
        reissueDate = areissueDate;
    }

    public void setreissueDate(String areissueDate)
    {
        if (areissueDate != null && !areissueDate.equals(""))
        {
            reissueDate = fDate.getDate(areissueDate);
        }
        else
        {
            reissueDate = null;
        }
    }

    public String getreissueRsn()
    {
        if (reissueRsn != null && !reissueRsn.equals("") &&
            SysConst.CHANGECHARSET)
        {
            reissueRsn = StrTool.unicodeToGBK(reissueRsn);
        }
        return reissueRsn;
    }

    public void setreissueRsn(String areissueRsn)
    {
        reissueRsn = areissueRsn;
    }

    public int getValidPeriod()
    {
        return ValidPeriod;
    }

    public void setValidPeriod(int aValidPeriod)
    {
        ValidPeriod = aValidPeriod;
    }

    public void setValidPeriod(String aValidPeriod)
    {
        if (aValidPeriod != null && !aValidPeriod.equals(""))
        {
            Integer tInteger = new Integer(aValidPeriod);
            int i = tInteger.intValue();
            ValidPeriod = i;
        }
    }

    public String getState()
    {
        if (State != null && !State.equals("") && SysConst.CHANGECHARSET)
        {
            State = StrTool.unicodeToGBK(State);
        }
        return State;
    }

    public void setState(String aState)
    {
        State = aState;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    public String getValidStart()
    {
        if (ValidStart != null)
        {
            return fDate.getString(ValidStart);
        }
        else
        {
            return null;
        }
    }

    public void setValidStart(Date aValidStart)
    {
        ValidStart = aValidStart;
    }

    public void setValidStart(String aValidStart)
    {
        if (aValidStart != null && !aValidStart.equals(""))
        {
            ValidStart = fDate.getDate(aValidStart);
        }
        else
        {
            ValidStart = null;
        }
    }

    public String getValidEnd()
    {
        if (ValidEnd != null)
        {
            return fDate.getString(ValidEnd);
        }
        else
        {
            return null;
        }
    }

    public void setValidEnd(Date aValidEnd)
    {
        ValidEnd = aValidEnd;
    }

    public void setValidEnd(String aValidEnd)
    {
        if (aValidEnd != null && !aValidEnd.equals(""))
        {
            ValidEnd = fDate.getDate(aValidEnd);
        }
        else
        {
            ValidEnd = null;
        }
    }

    public String getOldCeritifNo()
    {
        if (OldCeritifNo != null && !OldCeritifNo.equals("") &&
            SysConst.CHANGECHARSET)
        {
            OldCeritifNo = StrTool.unicodeToGBK(OldCeritifNo);
        }
        return OldCeritifNo;
    }

    public void setOldCeritifNo(String aOldCeritifNo)
    {
        OldCeritifNo = aOldCeritifNo;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    /**
     * 使用另外一个 LACertificationSchema 对象给 Schema 赋值
     * @param: aLACertificationSchema LACertificationSchema
     **/
    public void setSchema(LACertificationSchema aLACertificationSchema)
    {
        this.AgentCode = aLACertificationSchema.getAgentCode();
        this.CertifNo = aLACertificationSchema.getCertifNo();
        this.Idx = aLACertificationSchema.getIdx();
        this.AuthorUnit = aLACertificationSchema.getAuthorUnit();
        this.GrantDate = fDate.getDate(aLACertificationSchema.getGrantDate());
        this.InvalidDate = fDate.getDate(aLACertificationSchema.getInvalidDate());
        this.InvalidRsn = aLACertificationSchema.getInvalidRsn();
        this.reissueDate = fDate.getDate(aLACertificationSchema.getreissueDate());
        this.reissueRsn = aLACertificationSchema.getreissueRsn();
        this.ValidPeriod = aLACertificationSchema.getValidPeriod();
        this.State = aLACertificationSchema.getState();
        this.MakeDate = fDate.getDate(aLACertificationSchema.getMakeDate());
        this.MakeTime = aLACertificationSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLACertificationSchema.getModifyDate());
        this.ModifyTime = aLACertificationSchema.getModifyTime();
        this.ValidStart = fDate.getDate(aLACertificationSchema.getValidStart());
        this.ValidEnd = fDate.getDate(aLACertificationSchema.getValidEnd());
        this.OldCeritifNo = aLACertificationSchema.getOldCeritifNo();
        this.Operator = aLACertificationSchema.getOperator();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("AgentCode") == null)
            {
                this.AgentCode = null;
            }
            else
            {
                this.AgentCode = rs.getString("AgentCode").trim();
            }

            if (rs.getString("CertifNo") == null)
            {
                this.CertifNo = null;
            }
            else
            {
                this.CertifNo = rs.getString("CertifNo").trim();
            }

            this.Idx = rs.getInt("Idx");
            if (rs.getString("AuthorUnit") == null)
            {
                this.AuthorUnit = null;
            }
            else
            {
                this.AuthorUnit = rs.getString("AuthorUnit").trim();
            }

            this.GrantDate = rs.getDate("GrantDate");
            this.InvalidDate = rs.getDate("InvalidDate");
            if (rs.getString("InvalidRsn") == null)
            {
                this.InvalidRsn = null;
            }
            else
            {
                this.InvalidRsn = rs.getString("InvalidRsn").trim();
            }

            this.reissueDate = rs.getDate("reissueDate");
            if (rs.getString("reissueRsn") == null)
            {
                this.reissueRsn = null;
            }
            else
            {
                this.reissueRsn = rs.getString("reissueRsn").trim();
            }

            this.ValidPeriod = rs.getInt("ValidPeriod");
            if (rs.getString("State") == null)
            {
                this.State = null;
            }
            else
            {
                this.State = rs.getString("State").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            this.ValidStart = rs.getDate("ValidStart");
            this.ValidEnd = rs.getDate("ValidEnd");
            if (rs.getString("OldCeritifNo") == null)
            {
                this.OldCeritifNo = null;
            }
            else
            {
                this.OldCeritifNo = rs.getString("OldCeritifNo").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LACertificationSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LACertificationSchema getSchema()
    {
        LACertificationSchema aLACertificationSchema = new
                LACertificationSchema();
        aLACertificationSchema.setSchema(this);
        return aLACertificationSchema;
    }

    public LACertificationDB getDB()
    {
        LACertificationDB aDBOper = new LACertificationDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLACertification描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(AgentCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CertifNo)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(Idx) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AuthorUnit)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(GrantDate))) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(InvalidDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InvalidRsn)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(reissueDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(reissueRsn)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(ValidPeriod) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(State)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(ValidStart))) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(ValidEnd))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(OldCeritifNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLACertification>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            CertifNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            Idx = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    3, SysConst.PACKAGESPILTER))).intValue();
            AuthorUnit = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                        SysConst.PACKAGESPILTER);
            GrantDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 5, SysConst.PACKAGESPILTER));
            InvalidDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 6, SysConst.PACKAGESPILTER));
            InvalidRsn = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                        SysConst.PACKAGESPILTER);
            reissueDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 8, SysConst.PACKAGESPILTER));
            reissueRsn = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                        SysConst.PACKAGESPILTER);
            ValidPeriod = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 10, SysConst.PACKAGESPILTER))).intValue();
            State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                   SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 12, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 14, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                        SysConst.PACKAGESPILTER);
            ValidStart = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 16, SysConst.PACKAGESPILTER));
            ValidEnd = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 17, SysConst.PACKAGESPILTER));
            OldCeritifNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                          SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                      SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LACertificationSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("AgentCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equals("CertifNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CertifNo));
        }
        if (FCode.equals("Idx"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Idx));
        }
        if (FCode.equals("AuthorUnit"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AuthorUnit));
        }
        if (FCode.equals("GrantDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getGrantDate()));
        }
        if (FCode.equals("InvalidDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getInvalidDate()));
        }
        if (FCode.equals("InvalidRsn"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InvalidRsn));
        }
        if (FCode.equals("reissueDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getreissueDate()));
        }
        if (FCode.equals("reissueRsn"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(reissueRsn));
        }
        if (FCode.equals("ValidPeriod"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ValidPeriod));
        }
        if (FCode.equals("State"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("ValidStart"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getValidStart()));
        }
        if (FCode.equals("ValidEnd"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getValidEnd()));
        }
        if (FCode.equals("OldCeritifNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OldCeritifNo));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(AgentCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(CertifNo);
                break;
            case 2:
                strFieldValue = String.valueOf(Idx);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(AuthorUnit);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getGrantDate()));
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getInvalidDate()));
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(InvalidRsn);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getreissueDate()));
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(reissueRsn);
                break;
            case 9:
                strFieldValue = String.valueOf(ValidPeriod);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(State);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getValidStart()));
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getValidEnd()));
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(OldCeritifNo);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("AgentCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
            {
                AgentCode = null;
            }
        }
        if (FCode.equals("CertifNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CertifNo = FValue.trim();
            }
            else
            {
                CertifNo = null;
            }
        }
        if (FCode.equals("Idx"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                Idx = i;
            }
        }
        if (FCode.equals("AuthorUnit"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AuthorUnit = FValue.trim();
            }
            else
            {
                AuthorUnit = null;
            }
        }
        if (FCode.equals("GrantDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrantDate = fDate.getDate(FValue);
            }
            else
            {
                GrantDate = null;
            }
        }
        if (FCode.equals("InvalidDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InvalidDate = fDate.getDate(FValue);
            }
            else
            {
                InvalidDate = null;
            }
        }
        if (FCode.equals("InvalidRsn"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InvalidRsn = FValue.trim();
            }
            else
            {
                InvalidRsn = null;
            }
        }
        if (FCode.equals("reissueDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                reissueDate = fDate.getDate(FValue);
            }
            else
            {
                reissueDate = null;
            }
        }
        if (FCode.equals("reissueRsn"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                reissueRsn = FValue.trim();
            }
            else
            {
                reissueRsn = null;
            }
        }
        if (FCode.equals("ValidPeriod"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                ValidPeriod = i;
            }
        }
        if (FCode.equals("State"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
            {
                State = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        if (FCode.equals("ValidStart"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ValidStart = fDate.getDate(FValue);
            }
            else
            {
                ValidStart = null;
            }
        }
        if (FCode.equals("ValidEnd"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ValidEnd = fDate.getDate(FValue);
            }
            else
            {
                ValidEnd = null;
            }
        }
        if (FCode.equals("OldCeritifNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OldCeritifNo = FValue.trim();
            }
            else
            {
                OldCeritifNo = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LACertificationSchema other = (LACertificationSchema) otherObject;
        return
                AgentCode.equals(other.getAgentCode())
                && CertifNo.equals(other.getCertifNo())
                && Idx == other.getIdx()
                && AuthorUnit.equals(other.getAuthorUnit())
                && fDate.getString(GrantDate).equals(other.getGrantDate())
                && fDate.getString(InvalidDate).equals(other.getInvalidDate())
                && InvalidRsn.equals(other.getInvalidRsn())
                && fDate.getString(reissueDate).equals(other.getreissueDate())
                && reissueRsn.equals(other.getreissueRsn())
                && ValidPeriod == other.getValidPeriod()
                && State.equals(other.getState())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && fDate.getString(ValidStart).equals(other.getValidStart())
                && fDate.getString(ValidEnd).equals(other.getValidEnd())
                && OldCeritifNo.equals(other.getOldCeritifNo())
                && Operator.equals(other.getOperator());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("AgentCode"))
        {
            return 0;
        }
        if (strFieldName.equals("CertifNo"))
        {
            return 1;
        }
        if (strFieldName.equals("Idx"))
        {
            return 2;
        }
        if (strFieldName.equals("AuthorUnit"))
        {
            return 3;
        }
        if (strFieldName.equals("GrantDate"))
        {
            return 4;
        }
        if (strFieldName.equals("InvalidDate"))
        {
            return 5;
        }
        if (strFieldName.equals("InvalidRsn"))
        {
            return 6;
        }
        if (strFieldName.equals("reissueDate"))
        {
            return 7;
        }
        if (strFieldName.equals("reissueRsn"))
        {
            return 8;
        }
        if (strFieldName.equals("ValidPeriod"))
        {
            return 9;
        }
        if (strFieldName.equals("State"))
        {
            return 10;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 11;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 12;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 13;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 14;
        }
        if (strFieldName.equals("ValidStart"))
        {
            return 15;
        }
        if (strFieldName.equals("ValidEnd"))
        {
            return 16;
        }
        if (strFieldName.equals("OldCeritifNo"))
        {
            return 17;
        }
        if (strFieldName.equals("Operator"))
        {
            return 18;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "AgentCode";
                break;
            case 1:
                strFieldName = "CertifNo";
                break;
            case 2:
                strFieldName = "Idx";
                break;
            case 3:
                strFieldName = "AuthorUnit";
                break;
            case 4:
                strFieldName = "GrantDate";
                break;
            case 5:
                strFieldName = "InvalidDate";
                break;
            case 6:
                strFieldName = "InvalidRsn";
                break;
            case 7:
                strFieldName = "reissueDate";
                break;
            case 8:
                strFieldName = "reissueRsn";
                break;
            case 9:
                strFieldName = "ValidPeriod";
                break;
            case 10:
                strFieldName = "State";
                break;
            case 11:
                strFieldName = "MakeDate";
                break;
            case 12:
                strFieldName = "MakeTime";
                break;
            case 13:
                strFieldName = "ModifyDate";
                break;
            case 14:
                strFieldName = "ModifyTime";
                break;
            case 15:
                strFieldName = "ValidStart";
                break;
            case 16:
                strFieldName = "ValidEnd";
                break;
            case 17:
                strFieldName = "OldCeritifNo";
                break;
            case 18:
                strFieldName = "Operator";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("AgentCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CertifNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Idx"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("AuthorUnit"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrantDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("InvalidDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("InvalidRsn"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("reissueDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("reissueRsn"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ValidPeriod"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("State"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ValidStart"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ValidEnd"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("OldCeritifNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_INT;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 5:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_INT;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 16:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 17:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 18:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
