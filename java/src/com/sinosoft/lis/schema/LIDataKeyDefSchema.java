/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LIDataKeyDefDB;

/*
 * <p>ClassName: LIDataKeyDefSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 财务接口最新版
 * @CreateDate：2008-05-20
 */
public class LIDataKeyDefSchema implements Schema, Cloneable
{
	// @Field
	/** 数据类型编码 */
	private String ClassID;
	/** 主键编号 */
	private String KeyID;
	/** 主键名称 */
	private String KeyName;
	/** 主键序号 */
	private int KeyOrder;
	/** 描述 */
	private String Remark;

	public static final int FIELDNUM = 5;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LIDataKeyDefSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "ClassID";
		pk[1] = "KeyID";

		PK = pk;
	}

            /**
             * Schema克隆
             * @return Object
             * @throws CloneNotSupportedException
             */
            public Object clone()
                    throws CloneNotSupportedException
            {
                LIDataKeyDefSchema cloned = (LIDataKeyDefSchema)super.clone();
                cloned.mErrors = (CErrors) mErrors.clone();
                return cloned;
            }

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getClassID()
	{
		return ClassID;
	}
	public void setClassID(String aClassID)
	{
            ClassID = aClassID;
	}
	public String getKeyID()
	{
		return KeyID;
	}
	public void setKeyID(String aKeyID)
	{
            KeyID = aKeyID;
	}
	public String getKeyName()
	{
		return KeyName;
	}
	public void setKeyName(String aKeyName)
	{
            KeyName = aKeyName;
	}
	public int getKeyOrder()
	{
		return KeyOrder;
	}
	public void setKeyOrder(int aKeyOrder)
	{
            KeyOrder = aKeyOrder;
	}
	public void setKeyOrder(String aKeyOrder)
	{
		if (aKeyOrder != null && !aKeyOrder.equals(""))
		{
			Integer tInteger = new Integer(aKeyOrder);
			int i = tInteger.intValue();
			KeyOrder = i;
		}
	}

	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
            Remark = aRemark;
	}

	/**
	* 使用另外一个 LIDataKeyDefSchema 对象给 Schema 赋值
	* @param: aLIDataKeyDefSchema LIDataKeyDefSchema
	**/
	public void setSchema(LIDataKeyDefSchema aLIDataKeyDefSchema)
	{
		this.ClassID = aLIDataKeyDefSchema.getClassID();
		this.KeyID = aLIDataKeyDefSchema.getKeyID();
		this.KeyName = aLIDataKeyDefSchema.getKeyName();
		this.KeyOrder = aLIDataKeyDefSchema.getKeyOrder();
		this.Remark = aLIDataKeyDefSchema.getRemark();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("ClassID") == null )
				this.ClassID = null;
			else
				this.ClassID = rs.getString("ClassID").trim();

			if( rs.getString("KeyID") == null )
				this.KeyID = null;
			else
				this.KeyID = rs.getString("KeyID").trim();

			if( rs.getString("KeyName") == null )
				this.KeyName = null;
			else
				this.KeyName = rs.getString("KeyName").trim();

			this.KeyOrder = rs.getInt("KeyOrder");
			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LIDataKeyDef表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LIDataKeyDefSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LIDataKeyDefSchema getSchema()
	{
		LIDataKeyDefSchema aLIDataKeyDefSchema = new LIDataKeyDefSchema();
		aLIDataKeyDefSchema.setSchema(this);
		return aLIDataKeyDefSchema;
	}

	public LIDataKeyDefDB getDB()
	{
		LIDataKeyDefDB aDBOper = new LIDataKeyDefDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLIDataKeyDef描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append(StrTool.cTrim(ClassID)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(KeyID)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(KeyName)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(KeyOrder));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Remark));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLIDataKeyDef>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			ClassID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			KeyID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			KeyName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			KeyOrder= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,4,SysConst.PACKAGESPILTER))).intValue();
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LIDataKeyDefSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("ClassID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClassID));
		}
		if (FCode.equals("KeyID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(KeyID));
		}
		if (FCode.equals("KeyName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(KeyName));
		}
		if (FCode.equals("KeyOrder"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(KeyOrder));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(ClassID);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(KeyID);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(KeyName);
				break;
			case 3:
				strFieldValue = String.valueOf(KeyOrder);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("ClassID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ClassID = FValue.trim();
			}
			else
				ClassID = null;
		}
		if (FCode.equalsIgnoreCase("KeyID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				KeyID = FValue.trim();
			}
			else
				KeyID = null;
		}
		if (FCode.equalsIgnoreCase("KeyName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				KeyName = FValue.trim();
			}
			else
				KeyName = null;
		}
		if (FCode.equalsIgnoreCase("KeyOrder"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				KeyOrder = i;
			}
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LIDataKeyDefSchema other = (LIDataKeyDefSchema)otherObject;
		return
			ClassID.equals(other.getClassID())
			&& KeyID.equals(other.getKeyID())
			&& KeyName.equals(other.getKeyName())
			&& KeyOrder == other.getKeyOrder()
			&& Remark.equals(other.getRemark());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("ClassID") ) {
			return 0;
		}
		if( strFieldName.equals("KeyID") ) {
			return 1;
		}
		if( strFieldName.equals("KeyName") ) {
			return 2;
		}
		if( strFieldName.equals("KeyOrder") ) {
			return 3;
		}
		if( strFieldName.equals("Remark") ) {
			return 4;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "ClassID";
				break;
			case 1:
				strFieldName = "KeyID";
				break;
			case 2:
				strFieldName = "KeyName";
				break;
			case 3:
				strFieldName = "KeyOrder";
				break;
			case 4:
				strFieldName = "Remark";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("ClassID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("KeyID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("KeyName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("KeyOrder") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_INT;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
