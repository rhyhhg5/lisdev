/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LABranchTrusteeDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LABranchTrusteeSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-07-25
 */
public class LABranchTrusteeSchema implements Schema, Cloneable
{
    // @Field
    /** 托管人人编码 */
    private String AgentCode;
    /** 被托管人编码 */
    private String BranchManager;
    /** 请假纪录顺序号 */
    private int Idx;
    /** 纪录顺序号 */
    private int IdxNo;
    /** 被托管人机构代码 */
    private String AgentGroup;
    /** 管理机构 */
    private String ManageCom;
    /** 核准人 */
    private String ApproveCode;
    /** 托管开始日期 */
    private Date StartDate;
    /** 托管截止日期 */
    private Date EndDate;
    /** 托管应截止日期 */
    private Date ShouldEndDate;
    /** 托管天数 */
    private String VacDays;
    /** 托管类型 */
    private String AClass;
    /** 托管状态 */
    private String State;
    /** 批注 */
    private String Noti;
    /** 展业类型 */
    private String BranchType;
    /** 渠道 */
    private String BranchType2;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 操作员代码 */
    private String Operator;

    public static final int FIELDNUM = 21; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LABranchTrusteeSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[4];
        pk[0] = "AgentCode";
        pk[1] = "BranchManager";
        pk[2] = "Idx";
        pk[3] = "IdxNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException
    {
        LABranchTrusteeSchema cloned = (LABranchTrusteeSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getAgentCode()
    {
        return AgentCode;
    }

    public void setAgentCode(String aAgentCode)
    {
        AgentCode = aAgentCode;
    }

    public String getBranchManager()
    {
        return BranchManager;
    }

    public void setBranchManager(String aBranchManager)
    {
        BranchManager = aBranchManager;
    }

    public int getIdx()
    {
        return Idx;
    }

    public void setIdx(int aIdx)
    {
        Idx = aIdx;
    }

    public void setIdx(String aIdx)
    {
        if (aIdx != null && !aIdx.equals(""))
        {
            Integer tInteger = new Integer(aIdx);
            int i = tInteger.intValue();
            Idx = i;
        }
    }

    public int getIdxNo()
    {
        return IdxNo;
    }

    public void setIdxNo(int aIdxNo)
    {
        IdxNo = aIdxNo;
    }

    public void setIdxNo(String aIdxNo)
    {
        if (aIdxNo != null && !aIdxNo.equals(""))
        {
            Integer tInteger = new Integer(aIdxNo);
            int i = tInteger.intValue();
            IdxNo = i;
        }
    }

    public String getAgentGroup()
    {
        return AgentGroup;
    }

    public void setAgentGroup(String aAgentGroup)
    {
        AgentGroup = aAgentGroup;
    }

    public String getManageCom()
    {
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public String getApproveCode()
    {
        return ApproveCode;
    }

    public void setApproveCode(String aApproveCode)
    {
        ApproveCode = aApproveCode;
    }

    public String getStartDate()
    {
        if (StartDate != null)
        {
            return fDate.getString(StartDate);
        }
        else
        {
            return null;
        }
    }

    public void setStartDate(Date aStartDate)
    {
        StartDate = aStartDate;
    }

    public void setStartDate(String aStartDate)
    {
        if (aStartDate != null && !aStartDate.equals(""))
        {
            StartDate = fDate.getDate(aStartDate);
        }
        else
        {
            StartDate = null;
        }
    }

    public String getEndDate()
    {
        if (EndDate != null)
        {
            return fDate.getString(EndDate);
        }
        else
        {
            return null;
        }
    }

    public void setEndDate(Date aEndDate)
    {
        EndDate = aEndDate;
    }

    public void setEndDate(String aEndDate)
    {
        if (aEndDate != null && !aEndDate.equals(""))
        {
            EndDate = fDate.getDate(aEndDate);
        }
        else
        {
            EndDate = null;
        }
    }

    public String getShouldEndDate()
    {
        if (ShouldEndDate != null)
        {
            return fDate.getString(ShouldEndDate);
        }
        else
        {
            return null;
        }
    }

    public void setShouldEndDate(Date aShouldEndDate)
    {
        ShouldEndDate = aShouldEndDate;
    }

    public void setShouldEndDate(String aShouldEndDate)
    {
        if (aShouldEndDate != null && !aShouldEndDate.equals(""))
        {
            ShouldEndDate = fDate.getDate(aShouldEndDate);
        }
        else
        {
            ShouldEndDate = null;
        }
    }

    public String getVacDays()
    {
        return VacDays;
    }

    public void setVacDays(String aVacDays)
    {
        VacDays = aVacDays;
    }

    public String getAClass()
    {
        return AClass;
    }

    public void setAClass(String aAClass)
    {
        AClass = aAClass;
    }

    public String getState()
    {
        return State;
    }

    public void setState(String aState)
    {
        State = aState;
    }

    public String getNoti()
    {
        return Noti;
    }

    public void setNoti(String aNoti)
    {
        Noti = aNoti;
    }

    public String getBranchType()
    {
        return BranchType;
    }

    public void setBranchType(String aBranchType)
    {
        BranchType = aBranchType;
    }

    public String getBranchType2()
    {
        return BranchType2;
    }

    public void setBranchType2(String aBranchType2)
    {
        BranchType2 = aBranchType2;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    public String getOperator()
    {
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    /**
     * 使用另外一个 LABranchTrusteeSchema 对象给 Schema 赋值
     * @param: aLABranchTrusteeSchema LABranchTrusteeSchema
     **/
    public void setSchema(LABranchTrusteeSchema aLABranchTrusteeSchema)
    {
        this.AgentCode = aLABranchTrusteeSchema.getAgentCode();
        this.BranchManager = aLABranchTrusteeSchema.getBranchManager();
        this.Idx = aLABranchTrusteeSchema.getIdx();
        this.IdxNo = aLABranchTrusteeSchema.getIdxNo();
        this.AgentGroup = aLABranchTrusteeSchema.getAgentGroup();
        this.ManageCom = aLABranchTrusteeSchema.getManageCom();
        this.ApproveCode = aLABranchTrusteeSchema.getApproveCode();
        this.StartDate = fDate.getDate(aLABranchTrusteeSchema.getStartDate());
        this.EndDate = fDate.getDate(aLABranchTrusteeSchema.getEndDate());
        this.ShouldEndDate = fDate.getDate(aLABranchTrusteeSchema.
                                           getShouldEndDate());
        this.VacDays = aLABranchTrusteeSchema.getVacDays();
        this.AClass = aLABranchTrusteeSchema.getAClass();
        this.State = aLABranchTrusteeSchema.getState();
        this.Noti = aLABranchTrusteeSchema.getNoti();
        this.BranchType = aLABranchTrusteeSchema.getBranchType();
        this.BranchType2 = aLABranchTrusteeSchema.getBranchType2();
        this.MakeDate = fDate.getDate(aLABranchTrusteeSchema.getMakeDate());
        this.MakeTime = aLABranchTrusteeSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLABranchTrusteeSchema.getModifyDate());
        this.ModifyTime = aLABranchTrusteeSchema.getModifyTime();
        this.Operator = aLABranchTrusteeSchema.getOperator();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("AgentCode") == null)
            {
                this.AgentCode = null;
            }
            else
            {
                this.AgentCode = rs.getString("AgentCode").trim();
            }

            if (rs.getString("BranchManager") == null)
            {
                this.BranchManager = null;
            }
            else
            {
                this.BranchManager = rs.getString("BranchManager").trim();
            }

            this.Idx = rs.getInt("Idx");
            this.IdxNo = rs.getInt("IdxNo");
            if (rs.getString("AgentGroup") == null)
            {
                this.AgentGroup = null;
            }
            else
            {
                this.AgentGroup = rs.getString("AgentGroup").trim();
            }

            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("ApproveCode") == null)
            {
                this.ApproveCode = null;
            }
            else
            {
                this.ApproveCode = rs.getString("ApproveCode").trim();
            }

            this.StartDate = rs.getDate("StartDate");
            this.EndDate = rs.getDate("EndDate");
            this.ShouldEndDate = rs.getDate("ShouldEndDate");
            if (rs.getString("VacDays") == null)
            {
                this.VacDays = null;
            }
            else
            {
                this.VacDays = rs.getString("VacDays").trim();
            }

            if (rs.getString("AClass") == null)
            {
                this.AClass = null;
            }
            else
            {
                this.AClass = rs.getString("AClass").trim();
            }

            if (rs.getString("State") == null)
            {
                this.State = null;
            }
            else
            {
                this.State = rs.getString("State").trim();
            }

            if (rs.getString("Noti") == null)
            {
                this.Noti = null;
            }
            else
            {
                this.Noti = rs.getString("Noti").trim();
            }

            if (rs.getString("BranchType") == null)
            {
                this.BranchType = null;
            }
            else
            {
                this.BranchType = rs.getString("BranchType").trim();
            }

            if (rs.getString("BranchType2") == null)
            {
                this.BranchType2 = null;
            }
            else
            {
                this.BranchType2 = rs.getString("BranchType2").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

        }
        catch (SQLException sqle)
        {
            System.out.println("数据库中的LABranchTrustee表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LABranchTrusteeSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LABranchTrusteeSchema getSchema()
    {
        LABranchTrusteeSchema aLABranchTrusteeSchema = new
                LABranchTrusteeSchema();
        aLABranchTrusteeSchema.setSchema(this);
        return aLABranchTrusteeSchema;
    }

    public LABranchTrusteeDB getDB()
    {
        LABranchTrusteeDB aDBOper = new LABranchTrusteeDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLABranchTrustee描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(AgentCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BranchManager));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Idx));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(IdxNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentGroup));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ApproveCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(StartDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(EndDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ShouldEndDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(VacDays));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AClass));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(State));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Noti));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BranchType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BranchType2));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLABranchTrustee>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            BranchManager = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                           SysConst.PACKAGESPILTER);
            Idx = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    3, SysConst.PACKAGESPILTER))).intValue();
            IdxNo = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    4, SysConst.PACKAGESPILTER))).intValue();
            AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                        SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                       SysConst.PACKAGESPILTER);
            ApproveCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                         SysConst.PACKAGESPILTER);
            StartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 8, SysConst.PACKAGESPILTER));
            EndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 9, SysConst.PACKAGESPILTER));
            ShouldEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 10, SysConst.PACKAGESPILTER));
            VacDays = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                     SysConst.PACKAGESPILTER);
            AClass = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                    SysConst.PACKAGESPILTER);
            State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                   SysConst.PACKAGESPILTER);
            Noti = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                  SysConst.PACKAGESPILTER);
            BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                        SysConst.PACKAGESPILTER);
            BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                         SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 17, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 19, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                        SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,
                                      SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LABranchTrusteeSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("AgentCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equals("BranchManager"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchManager));
        }
        if (FCode.equals("Idx"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Idx));
        }
        if (FCode.equals("IdxNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IdxNo));
        }
        if (FCode.equals("AgentGroup"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
        }
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("ApproveCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveCode));
        }
        if (FCode.equals("StartDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getStartDate()));
        }
        if (FCode.equals("EndDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getEndDate()));
        }
        if (FCode.equals("ShouldEndDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getShouldEndDate()));
        }
        if (FCode.equals("VacDays"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VacDays));
        }
        if (FCode.equals("AClass"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AClass));
        }
        if (FCode.equals("State"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equals("Noti"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Noti));
        }
        if (FCode.equals("BranchType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
        }
        if (FCode.equals("BranchType2"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(AgentCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(BranchManager);
                break;
            case 2:
                strFieldValue = String.valueOf(Idx);
                break;
            case 3:
                strFieldValue = String.valueOf(IdxNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(AgentGroup);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(ApproveCode);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getStartDate()));
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getEndDate()));
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getShouldEndDate()));
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(VacDays);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(AClass);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(State);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(Noti);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(BranchType);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(BranchType2);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equalsIgnoreCase("AgentCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
            {
                AgentCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("BranchManager"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchManager = FValue.trim();
            }
            else
            {
                BranchManager = null;
            }
        }
        if (FCode.equalsIgnoreCase("Idx"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                Idx = i;
            }
        }
        if (FCode.equalsIgnoreCase("IdxNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                IdxNo = i;
            }
        }
        if (FCode.equalsIgnoreCase("AgentGroup"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
            {
                AgentGroup = null;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("ApproveCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ApproveCode = FValue.trim();
            }
            else
            {
                ApproveCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("StartDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                StartDate = fDate.getDate(FValue);
            }
            else
            {
                StartDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("EndDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EndDate = fDate.getDate(FValue);
            }
            else
            {
                EndDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ShouldEndDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ShouldEndDate = fDate.getDate(FValue);
            }
            else
            {
                ShouldEndDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("VacDays"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                VacDays = FValue.trim();
            }
            else
            {
                VacDays = null;
            }
        }
        if (FCode.equalsIgnoreCase("AClass"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AClass = FValue.trim();
            }
            else
            {
                AClass = null;
            }
        }
        if (FCode.equalsIgnoreCase("State"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
            {
                State = null;
            }
        }
        if (FCode.equalsIgnoreCase("Noti"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Noti = FValue.trim();
            }
            else
            {
                Noti = null;
            }
        }
        if (FCode.equalsIgnoreCase("BranchType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchType = FValue.trim();
            }
            else
            {
                BranchType = null;
            }
        }
        if (FCode.equalsIgnoreCase("BranchType2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchType2 = FValue.trim();
            }
            else
            {
                BranchType2 = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LABranchTrusteeSchema other = (LABranchTrusteeSchema) otherObject;
        return
                AgentCode.equals(other.getAgentCode())
                && BranchManager.equals(other.getBranchManager())
                && Idx == other.getIdx()
                && IdxNo == other.getIdxNo()
                && AgentGroup.equals(other.getAgentGroup())
                && ManageCom.equals(other.getManageCom())
                && ApproveCode.equals(other.getApproveCode())
                && fDate.getString(StartDate).equals(other.getStartDate())
                && fDate.getString(EndDate).equals(other.getEndDate())
                && fDate.getString(ShouldEndDate).equals(other.getShouldEndDate())
                && VacDays.equals(other.getVacDays())
                && AClass.equals(other.getAClass())
                && State.equals(other.getState())
                && Noti.equals(other.getNoti())
                && BranchType.equals(other.getBranchType())
                && BranchType2.equals(other.getBranchType2())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && Operator.equals(other.getOperator());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("AgentCode"))
        {
            return 0;
        }
        if (strFieldName.equals("BranchManager"))
        {
            return 1;
        }
        if (strFieldName.equals("Idx"))
        {
            return 2;
        }
        if (strFieldName.equals("IdxNo"))
        {
            return 3;
        }
        if (strFieldName.equals("AgentGroup"))
        {
            return 4;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return 5;
        }
        if (strFieldName.equals("ApproveCode"))
        {
            return 6;
        }
        if (strFieldName.equals("StartDate"))
        {
            return 7;
        }
        if (strFieldName.equals("EndDate"))
        {
            return 8;
        }
        if (strFieldName.equals("ShouldEndDate"))
        {
            return 9;
        }
        if (strFieldName.equals("VacDays"))
        {
            return 10;
        }
        if (strFieldName.equals("AClass"))
        {
            return 11;
        }
        if (strFieldName.equals("State"))
        {
            return 12;
        }
        if (strFieldName.equals("Noti"))
        {
            return 13;
        }
        if (strFieldName.equals("BranchType"))
        {
            return 14;
        }
        if (strFieldName.equals("BranchType2"))
        {
            return 15;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 16;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 17;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 18;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 19;
        }
        if (strFieldName.equals("Operator"))
        {
            return 20;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "AgentCode";
                break;
            case 1:
                strFieldName = "BranchManager";
                break;
            case 2:
                strFieldName = "Idx";
                break;
            case 3:
                strFieldName = "IdxNo";
                break;
            case 4:
                strFieldName = "AgentGroup";
                break;
            case 5:
                strFieldName = "ManageCom";
                break;
            case 6:
                strFieldName = "ApproveCode";
                break;
            case 7:
                strFieldName = "StartDate";
                break;
            case 8:
                strFieldName = "EndDate";
                break;
            case 9:
                strFieldName = "ShouldEndDate";
                break;
            case 10:
                strFieldName = "VacDays";
                break;
            case 11:
                strFieldName = "AClass";
                break;
            case 12:
                strFieldName = "State";
                break;
            case 13:
                strFieldName = "Noti";
                break;
            case 14:
                strFieldName = "BranchType";
                break;
            case 15:
                strFieldName = "BranchType2";
                break;
            case 16:
                strFieldName = "MakeDate";
                break;
            case 17:
                strFieldName = "MakeTime";
                break;
            case 18:
                strFieldName = "ModifyDate";
                break;
            case 19:
                strFieldName = "ModifyTime";
                break;
            case 20:
                strFieldName = "Operator";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("AgentCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchManager"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Idx"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("IdxNo"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("AgentGroup"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ApproveCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StartDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("EndDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ShouldEndDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("VacDays"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AClass"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("State"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Noti"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchType2"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_INT;
                break;
            case 3:
                nFieldType = Schema.TYPE_INT;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 8:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 9:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 17:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 18:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 19:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 20:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
