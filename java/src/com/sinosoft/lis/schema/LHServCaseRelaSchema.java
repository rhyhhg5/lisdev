/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LHServCaseRelaDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LHServCaseRelaSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PICCH-郭楠-表修改-服务事件设置_20060703
 * @CreateDate：2006-07-03
 */
public class LHServCaseRelaSchema implements Schema, Cloneable {
    // @Field
    /** 服务事件号码 */
    private String ServCaseCode;
    /** 服务计划号码 */
    private String ServPlanNo;
    /** 服务计划名称 */
    private String ServPlanName;
    /** 机构属性标识 */
    private String ComID;
    /** 服务计划档次 */
    private String ServPlanLevel;
    /** 服务项目号码 */
    private String ServItemNo;
    /** 服务项目状态 */
    private String ServItemState;
    /** 标准服务项目代码 */
    private String ServItemCode;
    /** 保单类型 */
    private String ContType;
    /** 团体保单号 */
    private String GrpContNo;
    /** 团体客户号 */
    private String GrpCustomerNo;
    /** 团体客户名称 */
    private String GrpName;
    /** 保单号 */
    private String ContNo;
    /** 客户号 */
    private String CustomerNo;
    /** 姓名 */
    private String CustomerName;
    /** 管理机构 */
    private String ManageCom;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后修改日期 */
    private Date ModifyDate;
    /** 最后修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 21; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LHServCaseRelaSchema() {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "ServCaseCode";
        pk[1] = "ServItemNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LHServCaseRelaSchema cloned = (LHServCaseRelaSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getServCaseCode() {
        return ServCaseCode;
    }

    public void setServCaseCode(String aServCaseCode) {
        ServCaseCode = aServCaseCode;
    }

    public String getServPlanNo() {
        return ServPlanNo;
    }

    public void setServPlanNo(String aServPlanNo) {
        ServPlanNo = aServPlanNo;
    }

    public String getServPlanName() {
        return ServPlanName;
    }

    public void setServPlanName(String aServPlanName) {
        ServPlanName = aServPlanName;
    }

    public String getComID() {
        return ComID;
    }

    public void setComID(String aComID) {
        ComID = aComID;
    }

    public String getServPlanLevel() {
        return ServPlanLevel;
    }

    public void setServPlanLevel(String aServPlanLevel) {
        ServPlanLevel = aServPlanLevel;
    }

    public String getServItemNo() {
        return ServItemNo;
    }

    public void setServItemNo(String aServItemNo) {
        ServItemNo = aServItemNo;
    }

    public String getServItemState() {
        return ServItemState;
    }

    public void setServItemState(String aServItemState) {
        ServItemState = aServItemState;
    }

    public String getServItemCode() {
        return ServItemCode;
    }

    public void setServItemCode(String aServItemCode) {
        ServItemCode = aServItemCode;
    }

    public String getContType() {
        return ContType;
    }

    public void setContType(String aContType) {
        ContType = aContType;
    }

    public String getGrpContNo() {
        return GrpContNo;
    }

    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }

    public String getGrpCustomerNo() {
        return GrpCustomerNo;
    }

    public void setGrpCustomerNo(String aGrpCustomerNo) {
        GrpCustomerNo = aGrpCustomerNo;
    }

    public String getGrpName() {
        return GrpName;
    }

    public void setGrpName(String aGrpName) {
        GrpName = aGrpName;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }

    public String getCustomerNo() {
        return CustomerNo;
    }

    public void setCustomerNo(String aCustomerNo) {
        CustomerNo = aCustomerNo;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String aCustomerName) {
        CustomerName = aCustomerName;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LHServCaseRelaSchema 对象给 Schema 赋值
     * @param: aLHServCaseRelaSchema LHServCaseRelaSchema
     **/
    public void setSchema(LHServCaseRelaSchema aLHServCaseRelaSchema) {
        this.ServCaseCode = aLHServCaseRelaSchema.getServCaseCode();
        this.ServPlanNo = aLHServCaseRelaSchema.getServPlanNo();
        this.ServPlanName = aLHServCaseRelaSchema.getServPlanName();
        this.ComID = aLHServCaseRelaSchema.getComID();
        this.ServPlanLevel = aLHServCaseRelaSchema.getServPlanLevel();
        this.ServItemNo = aLHServCaseRelaSchema.getServItemNo();
        this.ServItemState = aLHServCaseRelaSchema.getServItemState();
        this.ServItemCode = aLHServCaseRelaSchema.getServItemCode();
        this.ContType = aLHServCaseRelaSchema.getContType();
        this.GrpContNo = aLHServCaseRelaSchema.getGrpContNo();
        this.GrpCustomerNo = aLHServCaseRelaSchema.getGrpCustomerNo();
        this.GrpName = aLHServCaseRelaSchema.getGrpName();
        this.ContNo = aLHServCaseRelaSchema.getContNo();
        this.CustomerNo = aLHServCaseRelaSchema.getCustomerNo();
        this.CustomerName = aLHServCaseRelaSchema.getCustomerName();
        this.ManageCom = aLHServCaseRelaSchema.getManageCom();
        this.Operator = aLHServCaseRelaSchema.getOperator();
        this.MakeDate = fDate.getDate(aLHServCaseRelaSchema.getMakeDate());
        this.MakeTime = aLHServCaseRelaSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLHServCaseRelaSchema.getModifyDate());
        this.ModifyTime = aLHServCaseRelaSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ServCaseCode") == null) {
                this.ServCaseCode = null;
            } else {
                this.ServCaseCode = rs.getString("ServCaseCode").trim();
            }

            if (rs.getString("ServPlanNo") == null) {
                this.ServPlanNo = null;
            } else {
                this.ServPlanNo = rs.getString("ServPlanNo").trim();
            }

            if (rs.getString("ServPlanName") == null) {
                this.ServPlanName = null;
            } else {
                this.ServPlanName = rs.getString("ServPlanName").trim();
            }

            if (rs.getString("ComID") == null) {
                this.ComID = null;
            } else {
                this.ComID = rs.getString("ComID").trim();
            }

            if (rs.getString("ServPlanLevel") == null) {
                this.ServPlanLevel = null;
            } else {
                this.ServPlanLevel = rs.getString("ServPlanLevel").trim();
            }

            if (rs.getString("ServItemNo") == null) {
                this.ServItemNo = null;
            } else {
                this.ServItemNo = rs.getString("ServItemNo").trim();
            }

            if (rs.getString("ServItemState") == null) {
                this.ServItemState = null;
            } else {
                this.ServItemState = rs.getString("ServItemState").trim();
            }

            if (rs.getString("ServItemCode") == null) {
                this.ServItemCode = null;
            } else {
                this.ServItemCode = rs.getString("ServItemCode").trim();
            }

            if (rs.getString("ContType") == null) {
                this.ContType = null;
            } else {
                this.ContType = rs.getString("ContType").trim();
            }

            if (rs.getString("GrpContNo") == null) {
                this.GrpContNo = null;
            } else {
                this.GrpContNo = rs.getString("GrpContNo").trim();
            }

            if (rs.getString("GrpCustomerNo") == null) {
                this.GrpCustomerNo = null;
            } else {
                this.GrpCustomerNo = rs.getString("GrpCustomerNo").trim();
            }

            if (rs.getString("GrpName") == null) {
                this.GrpName = null;
            } else {
                this.GrpName = rs.getString("GrpName").trim();
            }

            if (rs.getString("ContNo") == null) {
                this.ContNo = null;
            } else {
                this.ContNo = rs.getString("ContNo").trim();
            }

            if (rs.getString("CustomerNo") == null) {
                this.CustomerNo = null;
            } else {
                this.CustomerNo = rs.getString("CustomerNo").trim();
            }

            if (rs.getString("CustomerName") == null) {
                this.CustomerName = null;
            } else {
                this.CustomerName = rs.getString("CustomerName").trim();
            }

            if (rs.getString("ManageCom") == null) {
                this.ManageCom = null;
            } else {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LHServCaseRela表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHServCaseRelaSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LHServCaseRelaSchema getSchema() {
        LHServCaseRelaSchema aLHServCaseRelaSchema = new LHServCaseRelaSchema();
        aLHServCaseRelaSchema.setSchema(this);
        return aLHServCaseRelaSchema;
    }

    public LHServCaseRelaDB getDB() {
        LHServCaseRelaDB aDBOper = new LHServCaseRelaDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHServCaseRela描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(ServCaseCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServPlanNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServPlanName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ComID));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServPlanLevel));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServItemNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServItemState));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServItemCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpContNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpCustomerNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CustomerNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CustomerName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHServCaseRela>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            ServCaseCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                          SysConst.PACKAGESPILTER);
            ServPlanNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
            ServPlanName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                          SysConst.PACKAGESPILTER);
            ComID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                   SysConst.PACKAGESPILTER);
            ServPlanLevel = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                           SysConst.PACKAGESPILTER);
            ServItemNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                        SysConst.PACKAGESPILTER);
            ServItemState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                           SysConst.PACKAGESPILTER);
            ServItemCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                          SysConst.PACKAGESPILTER);
            ContType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                      SysConst.PACKAGESPILTER);
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                       SysConst.PACKAGESPILTER);
            GrpCustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                           SysConst.PACKAGESPILTER);
            GrpName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                     SysConst.PACKAGESPILTER);
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                    SysConst.PACKAGESPILTER);
            CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                        SysConst.PACKAGESPILTER);
            CustomerName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                          SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                       SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 18, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 20, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,
                                        SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHServCaseRelaSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("ServCaseCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServCaseCode));
        }
        if (FCode.equals("ServPlanNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServPlanNo));
        }
        if (FCode.equals("ServPlanName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServPlanName));
        }
        if (FCode.equals("ComID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ComID));
        }
        if (FCode.equals("ServPlanLevel")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServPlanLevel));
        }
        if (FCode.equals("ServItemNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServItemNo));
        }
        if (FCode.equals("ServItemState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServItemState));
        }
        if (FCode.equals("ServItemCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServItemCode));
        }
        if (FCode.equals("ContType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContType));
        }
        if (FCode.equals("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equals("GrpCustomerNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpCustomerNo));
        }
        if (FCode.equals("GrpName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpName));
        }
        if (FCode.equals("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equals("CustomerNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
        }
        if (FCode.equals("CustomerName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerName));
        }
        if (FCode.equals("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(ServCaseCode);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(ServPlanNo);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(ServPlanName);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(ComID);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(ServPlanLevel);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(ServItemNo);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(ServItemState);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(ServItemCode);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(ContType);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(GrpContNo);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(GrpCustomerNo);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(GrpName);
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(ContNo);
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(CustomerNo);
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(CustomerName);
            break;
        case 15:
            strFieldValue = StrTool.GBKToUnicode(ManageCom);
            break;
        case 16:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 17:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 18:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 19:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 20:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("ServCaseCode")) {
            if (FValue != null && !FValue.equals("")) {
                ServCaseCode = FValue.trim();
            } else {
                ServCaseCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServPlanNo")) {
            if (FValue != null && !FValue.equals("")) {
                ServPlanNo = FValue.trim();
            } else {
                ServPlanNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServPlanName")) {
            if (FValue != null && !FValue.equals("")) {
                ServPlanName = FValue.trim();
            } else {
                ServPlanName = null;
            }
        }
        if (FCode.equalsIgnoreCase("ComID")) {
            if (FValue != null && !FValue.equals("")) {
                ComID = FValue.trim();
            } else {
                ComID = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServPlanLevel")) {
            if (FValue != null && !FValue.equals("")) {
                ServPlanLevel = FValue.trim();
            } else {
                ServPlanLevel = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServItemNo")) {
            if (FValue != null && !FValue.equals("")) {
                ServItemNo = FValue.trim();
            } else {
                ServItemNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServItemState")) {
            if (FValue != null && !FValue.equals("")) {
                ServItemState = FValue.trim();
            } else {
                ServItemState = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServItemCode")) {
            if (FValue != null && !FValue.equals("")) {
                ServItemCode = FValue.trim();
            } else {
                ServItemCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("ContType")) {
            if (FValue != null && !FValue.equals("")) {
                ContType = FValue.trim();
            } else {
                ContType = null;
            }
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if (FValue != null && !FValue.equals("")) {
                GrpContNo = FValue.trim();
            } else {
                GrpContNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("GrpCustomerNo")) {
            if (FValue != null && !FValue.equals("")) {
                GrpCustomerNo = FValue.trim();
            } else {
                GrpCustomerNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("GrpName")) {
            if (FValue != null && !FValue.equals("")) {
                GrpName = FValue.trim();
            } else {
                GrpName = null;
            }
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if (FValue != null && !FValue.equals("")) {
                ContNo = FValue.trim();
            } else {
                ContNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            if (FValue != null && !FValue.equals("")) {
                CustomerNo = FValue.trim();
            } else {
                CustomerNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("CustomerName")) {
            if (FValue != null && !FValue.equals("")) {
                CustomerName = FValue.trim();
            } else {
                CustomerName = null;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ManageCom = FValue.trim();
            } else {
                ManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LHServCaseRelaSchema other = (LHServCaseRelaSchema) otherObject;
        return
                ServCaseCode.equals(other.getServCaseCode())
                && ServPlanNo.equals(other.getServPlanNo())
                && ServPlanName.equals(other.getServPlanName())
                && ComID.equals(other.getComID())
                && ServPlanLevel.equals(other.getServPlanLevel())
                && ServItemNo.equals(other.getServItemNo())
                && ServItemState.equals(other.getServItemState())
                && ServItemCode.equals(other.getServItemCode())
                && ContType.equals(other.getContType())
                && GrpContNo.equals(other.getGrpContNo())
                && GrpCustomerNo.equals(other.getGrpCustomerNo())
                && GrpName.equals(other.getGrpName())
                && ContNo.equals(other.getContNo())
                && CustomerNo.equals(other.getCustomerNo())
                && CustomerName.equals(other.getCustomerName())
                && ManageCom.equals(other.getManageCom())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("ServCaseCode")) {
            return 0;
        }
        if (strFieldName.equals("ServPlanNo")) {
            return 1;
        }
        if (strFieldName.equals("ServPlanName")) {
            return 2;
        }
        if (strFieldName.equals("ComID")) {
            return 3;
        }
        if (strFieldName.equals("ServPlanLevel")) {
            return 4;
        }
        if (strFieldName.equals("ServItemNo")) {
            return 5;
        }
        if (strFieldName.equals("ServItemState")) {
            return 6;
        }
        if (strFieldName.equals("ServItemCode")) {
            return 7;
        }
        if (strFieldName.equals("ContType")) {
            return 8;
        }
        if (strFieldName.equals("GrpContNo")) {
            return 9;
        }
        if (strFieldName.equals("GrpCustomerNo")) {
            return 10;
        }
        if (strFieldName.equals("GrpName")) {
            return 11;
        }
        if (strFieldName.equals("ContNo")) {
            return 12;
        }
        if (strFieldName.equals("CustomerNo")) {
            return 13;
        }
        if (strFieldName.equals("CustomerName")) {
            return 14;
        }
        if (strFieldName.equals("ManageCom")) {
            return 15;
        }
        if (strFieldName.equals("Operator")) {
            return 16;
        }
        if (strFieldName.equals("MakeDate")) {
            return 17;
        }
        if (strFieldName.equals("MakeTime")) {
            return 18;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 19;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 20;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "ServCaseCode";
            break;
        case 1:
            strFieldName = "ServPlanNo";
            break;
        case 2:
            strFieldName = "ServPlanName";
            break;
        case 3:
            strFieldName = "ComID";
            break;
        case 4:
            strFieldName = "ServPlanLevel";
            break;
        case 5:
            strFieldName = "ServItemNo";
            break;
        case 6:
            strFieldName = "ServItemState";
            break;
        case 7:
            strFieldName = "ServItemCode";
            break;
        case 8:
            strFieldName = "ContType";
            break;
        case 9:
            strFieldName = "GrpContNo";
            break;
        case 10:
            strFieldName = "GrpCustomerNo";
            break;
        case 11:
            strFieldName = "GrpName";
            break;
        case 12:
            strFieldName = "ContNo";
            break;
        case 13:
            strFieldName = "CustomerNo";
            break;
        case 14:
            strFieldName = "CustomerName";
            break;
        case 15:
            strFieldName = "ManageCom";
            break;
        case 16:
            strFieldName = "Operator";
            break;
        case 17:
            strFieldName = "MakeDate";
            break;
        case 18:
            strFieldName = "MakeTime";
            break;
        case 19:
            strFieldName = "ModifyDate";
            break;
        case 20:
            strFieldName = "ModifyTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("ServCaseCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServPlanNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServPlanName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ComID")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServPlanLevel")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServItemNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServItemState")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServItemCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpContNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpCustomerNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CustomerNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CustomerName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 12:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 13:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 14:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 15:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 16:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 17:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 18:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 19:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 20:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
