/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LAClassDescDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

/*
 * <p>ClassName: LAClassDescSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-04-01
 */
public class LAClassDescSchema implements Schema
{
    // @Field
    /** 培训班编码 */
    private String ClassNo;
    /** 展业类型 */
    private String BranchType;
    /** 培训班名称 */
    private String ClassName;
    /** 接受培训班职级 */
    private String ReceiGrade;
    /** 班级序列 */
    private String ClassSeries;
    /** 停办标记 */
    private String StopFlag;
    /** 渠道 */
    private String BranchType2;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最近一次修改日期 */
    private Date ModifyDate;
    /** 最近一次修改时间 */
    private String ModifyTime;
    /** 操作员 */
    private String Operator;

    public static final int FIELDNUM = 12; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LAClassDescSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "ClassNo";
        pk[1] = "BranchType";
        pk[2] = "BranchType2";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getClassNo()
    {
        if (SysConst.CHANGECHARSET && ClassNo != null && !ClassNo.equals(""))
        {
            ClassNo = StrTool.unicodeToGBK(ClassNo);
        }
        return ClassNo;
    }

    public void setClassNo(String aClassNo)
    {
        ClassNo = aClassNo;
    }

    public String getBranchType()
    {
        if (SysConst.CHANGECHARSET && BranchType != null &&
            !BranchType.equals(""))
        {
            BranchType = StrTool.unicodeToGBK(BranchType);
        }
        return BranchType;
    }

    public void setBranchType(String aBranchType)
    {
        BranchType = aBranchType;
    }

    public String getClassName()
    {
        if (SysConst.CHANGECHARSET && ClassName != null && !ClassName.equals(""))
        {
            ClassName = StrTool.unicodeToGBK(ClassName);
        }
        return ClassName;
    }

    public void setClassName(String aClassName)
    {
        ClassName = aClassName;
    }

    public String getReceiGrade()
    {
        if (SysConst.CHANGECHARSET && ReceiGrade != null &&
            !ReceiGrade.equals(""))
        {
            ReceiGrade = StrTool.unicodeToGBK(ReceiGrade);
        }
        return ReceiGrade;
    }

    public void setReceiGrade(String aReceiGrade)
    {
        ReceiGrade = aReceiGrade;
    }

    public String getClassSeries()
    {
        if (SysConst.CHANGECHARSET && ClassSeries != null &&
            !ClassSeries.equals(""))
        {
            ClassSeries = StrTool.unicodeToGBK(ClassSeries);
        }
        return ClassSeries;
    }

    public void setClassSeries(String aClassSeries)
    {
        ClassSeries = aClassSeries;
    }

    public String getStopFlag()
    {
        if (SysConst.CHANGECHARSET && StopFlag != null && !StopFlag.equals(""))
        {
            StopFlag = StrTool.unicodeToGBK(StopFlag);
        }
        return StopFlag;
    }

    public void setStopFlag(String aStopFlag)
    {
        StopFlag = aStopFlag;
    }

    public String getBranchType2()
    {
        if (SysConst.CHANGECHARSET && BranchType2 != null &&
            !BranchType2.equals(""))
        {
            BranchType2 = StrTool.unicodeToGBK(BranchType2);
        }
        return BranchType2;
    }

    public void setBranchType2(String aBranchType2)
    {
        BranchType2 = aBranchType2;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (SysConst.CHANGECHARSET && MakeTime != null && !MakeTime.equals(""))
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (SysConst.CHANGECHARSET && ModifyTime != null &&
            !ModifyTime.equals(""))
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    public String getOperator()
    {
        if (SysConst.CHANGECHARSET && Operator != null && !Operator.equals(""))
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    /**
     * 使用另外一个 LAClassDescSchema 对象给 Schema 赋值
     * @param: aLAClassDescSchema LAClassDescSchema
     **/
    public void setSchema(LAClassDescSchema aLAClassDescSchema)
    {
        this.ClassNo = aLAClassDescSchema.getClassNo();
        this.BranchType = aLAClassDescSchema.getBranchType();
        this.ClassName = aLAClassDescSchema.getClassName();
        this.ReceiGrade = aLAClassDescSchema.getReceiGrade();
        this.ClassSeries = aLAClassDescSchema.getClassSeries();
        this.StopFlag = aLAClassDescSchema.getStopFlag();
        this.BranchType2 = aLAClassDescSchema.getBranchType2();
        this.MakeDate = fDate.getDate(aLAClassDescSchema.getMakeDate());
        this.MakeTime = aLAClassDescSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLAClassDescSchema.getModifyDate());
        this.ModifyTime = aLAClassDescSchema.getModifyTime();
        this.Operator = aLAClassDescSchema.getOperator();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ClassNo") == null)
            {
                this.ClassNo = null;
            }
            else
            {
                this.ClassNo = rs.getString("ClassNo").trim();
            }

            if (rs.getString("BranchType") == null)
            {
                this.BranchType = null;
            }
            else
            {
                this.BranchType = rs.getString("BranchType").trim();
            }

            if (rs.getString("ClassName") == null)
            {
                this.ClassName = null;
            }
            else
            {
                this.ClassName = rs.getString("ClassName").trim();
            }

            if (rs.getString("ReceiGrade") == null)
            {
                this.ReceiGrade = null;
            }
            else
            {
                this.ReceiGrade = rs.getString("ReceiGrade").trim();
            }

            if (rs.getString("ClassSeries") == null)
            {
                this.ClassSeries = null;
            }
            else
            {
                this.ClassSeries = rs.getString("ClassSeries").trim();
            }

            if (rs.getString("StopFlag") == null)
            {
                this.StopFlag = null;
            }
            else
            {
                this.StopFlag = rs.getString("StopFlag").trim();
            }

            if (rs.getString("BranchType2") == null)
            {
                this.BranchType2 = null;
            }
            else
            {
                this.BranchType2 = rs.getString("BranchType2").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAClassDescSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LAClassDescSchema getSchema()
    {
        LAClassDescSchema aLAClassDescSchema = new LAClassDescSchema();
        aLAClassDescSchema.setSchema(this);
        return aLAClassDescSchema;
    }

    public LAClassDescDB getDB()
    {
        LAClassDescDB aDBOper = new LAClassDescDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAClassDesc描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ClassNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BranchType)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ClassName)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ReceiGrade)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ClassSeries)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(StopFlag)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BranchType2)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                MakeDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                ModifyDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Operator)));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAClassDesc>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            ClassNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                     SysConst.PACKAGESPILTER);
            BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
            ClassName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                       SysConst.PACKAGESPILTER);
            ReceiGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                        SysConst.PACKAGESPILTER);
            ClassSeries = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                         SysConst.PACKAGESPILTER);
            StopFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                      SysConst.PACKAGESPILTER);
            BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                         SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 8, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 10, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                        SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                      SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAClassDescSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("ClassNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClassNo));
        }
        if (FCode.equals("BranchType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
        }
        if (FCode.equals("ClassName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClassName));
        }
        if (FCode.equals("ReceiGrade"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiGrade));
        }
        if (FCode.equals("ClassSeries"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClassSeries));
        }
        if (FCode.equals("StopFlag"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StopFlag));
        }
        if (FCode.equals("BranchType2"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ClassNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(BranchType);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ClassName);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(ReceiGrade);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(ClassSeries);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(StopFlag);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(BranchType2);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("ClassNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ClassNo = FValue.trim();
            }
            else
            {
                ClassNo = null;
            }
        }
        if (FCode.equals("BranchType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchType = FValue.trim();
            }
            else
            {
                BranchType = null;
            }
        }
        if (FCode.equals("ClassName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ClassName = FValue.trim();
            }
            else
            {
                ClassName = null;
            }
        }
        if (FCode.equals("ReceiGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ReceiGrade = FValue.trim();
            }
            else
            {
                ReceiGrade = null;
            }
        }
        if (FCode.equals("ClassSeries"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ClassSeries = FValue.trim();
            }
            else
            {
                ClassSeries = null;
            }
        }
        if (FCode.equals("StopFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                StopFlag = FValue.trim();
            }
            else
            {
                StopFlag = null;
            }
        }
        if (FCode.equals("BranchType2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchType2 = FValue.trim();
            }
            else
            {
                BranchType2 = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LAClassDescSchema other = (LAClassDescSchema) otherObject;
        return
                ClassNo.equals(other.getClassNo())
                && BranchType.equals(other.getBranchType())
                && ClassName.equals(other.getClassName())
                && ReceiGrade.equals(other.getReceiGrade())
                && ClassSeries.equals(other.getClassSeries())
                && StopFlag.equals(other.getStopFlag())
                && BranchType2.equals(other.getBranchType2())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && Operator.equals(other.getOperator());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("ClassNo"))
        {
            return 0;
        }
        if (strFieldName.equals("BranchType"))
        {
            return 1;
        }
        if (strFieldName.equals("ClassName"))
        {
            return 2;
        }
        if (strFieldName.equals("ReceiGrade"))
        {
            return 3;
        }
        if (strFieldName.equals("ClassSeries"))
        {
            return 4;
        }
        if (strFieldName.equals("StopFlag"))
        {
            return 5;
        }
        if (strFieldName.equals("BranchType2"))
        {
            return 6;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 7;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 8;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 9;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 10;
        }
        if (strFieldName.equals("Operator"))
        {
            return 11;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "ClassNo";
                break;
            case 1:
                strFieldName = "BranchType";
                break;
            case 2:
                strFieldName = "ClassName";
                break;
            case 3:
                strFieldName = "ReceiGrade";
                break;
            case 4:
                strFieldName = "ClassSeries";
                break;
            case 5:
                strFieldName = "StopFlag";
                break;
            case 6:
                strFieldName = "BranchType2";
                break;
            case 7:
                strFieldName = "MakeDate";
                break;
            case 8:
                strFieldName = "MakeTime";
                break;
            case 9:
                strFieldName = "ModifyDate";
                break;
            case 10:
                strFieldName = "ModifyTime";
                break;
            case 11:
                strFieldName = "Operator";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("ClassNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ClassName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ReceiGrade"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ClassSeries"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StopFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchType2"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
