/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LHFeeChargeDB;

/*
 * <p>ClassName: LHFeeChargeSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PICCH-郭丽颖-表结构修改提交单-20061010.pdm
 * @CreateDate：2006-10-11
 */
public class LHFeeChargeSchema implements Schema, Cloneable {
    // @Field
    /** 任务实施号码 */
    private String TaskExecNo;
    /** 服务任务号码 */
    private String ServTaskNo;
    /** 服务任务标准代码 */
    private String ServTaskCode;
    /** 服务事件号码 */
    private String ServCaseCode;
    /** 服务项目序号 */
    private String ServItemNo;
    /** 服务计划号码 */
    private String ServPlanNo;
    /** 客户号码 */
    private String CustomerNo;
    /** 保单号 */
    private String ContNo;
    /** 团体客户号 */
    private String GrpCustomerNo;
    /** 团体保单号 */
    private String GrpContNo;
    /** 费用类型 */
    private String FeeType;
    /** 应付费用 */
    private double FeeNormal;
    /** 结算费用 */
    private double FeePay;
    /** 扫描件号码 */
    private String PrtNo;
    /** 管理机构 */
    private String ManageCom;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后修改日期 */
    private Date ModifyDate;
    /** 最后修改时间 */
    private String ModifyTime;
    /** 费用实施号码 */
    private String FeeTaskExecNo;

    public static final int FIELDNUM = 21; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LHFeeChargeSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "TaskExecNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LHFeeChargeSchema cloned = (LHFeeChargeSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getTaskExecNo() {
        return TaskExecNo;
    }

    public void setTaskExecNo(String aTaskExecNo) {
        TaskExecNo = aTaskExecNo;
    }

    public String getServTaskNo() {
        return ServTaskNo;
    }

    public void setServTaskNo(String aServTaskNo) {
        ServTaskNo = aServTaskNo;
    }

    public String getServTaskCode() {
        return ServTaskCode;
    }

    public void setServTaskCode(String aServTaskCode) {
        ServTaskCode = aServTaskCode;
    }

    public String getServCaseCode() {
        return ServCaseCode;
    }

    public void setServCaseCode(String aServCaseCode) {
        ServCaseCode = aServCaseCode;
    }

    public String getServItemNo() {
        return ServItemNo;
    }

    public void setServItemNo(String aServItemNo) {
        ServItemNo = aServItemNo;
    }

    public String getServPlanNo() {
        return ServPlanNo;
    }

    public void setServPlanNo(String aServPlanNo) {
        ServPlanNo = aServPlanNo;
    }

    public String getCustomerNo() {
        return CustomerNo;
    }

    public void setCustomerNo(String aCustomerNo) {
        CustomerNo = aCustomerNo;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }

    public String getGrpCustomerNo() {
        return GrpCustomerNo;
    }

    public void setGrpCustomerNo(String aGrpCustomerNo) {
        GrpCustomerNo = aGrpCustomerNo;
    }

    public String getGrpContNo() {
        return GrpContNo;
    }

    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }

    public String getFeeType() {
        return FeeType;
    }

    public void setFeeType(String aFeeType) {
        FeeType = aFeeType;
    }

    public double getFeeNormal() {
        return FeeNormal;
    }

    public void setFeeNormal(double aFeeNormal) {
        FeeNormal = Arith.round(aFeeNormal, 2);
    }

    public void setFeeNormal(String aFeeNormal) {
        if (aFeeNormal != null && !aFeeNormal.equals("")) {
            Double tDouble = new Double(aFeeNormal);
            double d = tDouble.doubleValue();
            FeeNormal = Arith.round(d, 2);
        }
    }

    public double getFeePay() {
        return FeePay;
    }

    public void setFeePay(double aFeePay) {
        FeePay = Arith.round(aFeePay, 2);
    }

    public void setFeePay(String aFeePay) {
        if (aFeePay != null && !aFeePay.equals("")) {
            Double tDouble = new Double(aFeePay);
            double d = tDouble.doubleValue();
            FeePay = Arith.round(d, 2);
        }
    }

    public String getPrtNo() {
        return PrtNo;
    }

    public void setPrtNo(String aPrtNo) {
        PrtNo = aPrtNo;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    public String getFeeTaskExecNo() {
        return FeeTaskExecNo;
    }

    public void setFeeTaskExecNo(String aFeeTaskExecNo) {
        FeeTaskExecNo = aFeeTaskExecNo;
    }

    /**
     * 使用另外一个 LHFeeChargeSchema 对象给 Schema 赋值
     * @param: aLHFeeChargeSchema LHFeeChargeSchema
     **/
    public void setSchema(LHFeeChargeSchema aLHFeeChargeSchema) {
        this.TaskExecNo = aLHFeeChargeSchema.getTaskExecNo();
        this.ServTaskNo = aLHFeeChargeSchema.getServTaskNo();
        this.ServTaskCode = aLHFeeChargeSchema.getServTaskCode();
        this.ServCaseCode = aLHFeeChargeSchema.getServCaseCode();
        this.ServItemNo = aLHFeeChargeSchema.getServItemNo();
        this.ServPlanNo = aLHFeeChargeSchema.getServPlanNo();
        this.CustomerNo = aLHFeeChargeSchema.getCustomerNo();
        this.ContNo = aLHFeeChargeSchema.getContNo();
        this.GrpCustomerNo = aLHFeeChargeSchema.getGrpCustomerNo();
        this.GrpContNo = aLHFeeChargeSchema.getGrpContNo();
        this.FeeType = aLHFeeChargeSchema.getFeeType();
        this.FeeNormal = aLHFeeChargeSchema.getFeeNormal();
        this.FeePay = aLHFeeChargeSchema.getFeePay();
        this.PrtNo = aLHFeeChargeSchema.getPrtNo();
        this.ManageCom = aLHFeeChargeSchema.getManageCom();
        this.Operator = aLHFeeChargeSchema.getOperator();
        this.MakeDate = fDate.getDate(aLHFeeChargeSchema.getMakeDate());
        this.MakeTime = aLHFeeChargeSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLHFeeChargeSchema.getModifyDate());
        this.ModifyTime = aLHFeeChargeSchema.getModifyTime();
        this.FeeTaskExecNo = aLHFeeChargeSchema.getFeeTaskExecNo();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("TaskExecNo") == null) {
                this.TaskExecNo = null;
            } else {
                this.TaskExecNo = rs.getString("TaskExecNo").trim();
            }

            if (rs.getString("ServTaskNo") == null) {
                this.ServTaskNo = null;
            } else {
                this.ServTaskNo = rs.getString("ServTaskNo").trim();
            }

            if (rs.getString("ServTaskCode") == null) {
                this.ServTaskCode = null;
            } else {
                this.ServTaskCode = rs.getString("ServTaskCode").trim();
            }

            if (rs.getString("ServCaseCode") == null) {
                this.ServCaseCode = null;
            } else {
                this.ServCaseCode = rs.getString("ServCaseCode").trim();
            }

            if (rs.getString("ServItemNo") == null) {
                this.ServItemNo = null;
            } else {
                this.ServItemNo = rs.getString("ServItemNo").trim();
            }

            if (rs.getString("ServPlanNo") == null) {
                this.ServPlanNo = null;
            } else {
                this.ServPlanNo = rs.getString("ServPlanNo").trim();
            }

            if (rs.getString("CustomerNo") == null) {
                this.CustomerNo = null;
            } else {
                this.CustomerNo = rs.getString("CustomerNo").trim();
            }

            if (rs.getString("ContNo") == null) {
                this.ContNo = null;
            } else {
                this.ContNo = rs.getString("ContNo").trim();
            }

            if (rs.getString("GrpCustomerNo") == null) {
                this.GrpCustomerNo = null;
            } else {
                this.GrpCustomerNo = rs.getString("GrpCustomerNo").trim();
            }

            if (rs.getString("GrpContNo") == null) {
                this.GrpContNo = null;
            } else {
                this.GrpContNo = rs.getString("GrpContNo").trim();
            }

            if (rs.getString("FeeType") == null) {
                this.FeeType = null;
            } else {
                this.FeeType = rs.getString("FeeType").trim();
            }

            this.FeeNormal = rs.getDouble("FeeNormal");
            this.FeePay = rs.getDouble("FeePay");
            if (rs.getString("PrtNo") == null) {
                this.PrtNo = null;
            } else {
                this.PrtNo = rs.getString("PrtNo").trim();
            }

            if (rs.getString("ManageCom") == null) {
                this.ManageCom = null;
            } else {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("FeeTaskExecNo") == null) {
                this.FeeTaskExecNo = null;
            } else {
                this.FeeTaskExecNo = rs.getString("FeeTaskExecNo").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LHFeeCharge表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHFeeChargeSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LHFeeChargeSchema getSchema() {
        LHFeeChargeSchema aLHFeeChargeSchema = new LHFeeChargeSchema();
        aLHFeeChargeSchema.setSchema(this);
        return aLHFeeChargeSchema;
    }

    public LHFeeChargeDB getDB() {
        LHFeeChargeDB aDBOper = new LHFeeChargeDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHFeeCharge描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(TaskExecNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServTaskNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServTaskCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServCaseCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServItemNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServPlanNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CustomerNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpCustomerNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpContNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FeeType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(FeeNormal));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(FeePay));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PrtNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FeeTaskExecNo));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHFeeCharge>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            TaskExecNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                        SysConst.PACKAGESPILTER);
            ServTaskNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
            ServTaskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                          SysConst.PACKAGESPILTER);
            ServCaseCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                          SysConst.PACKAGESPILTER);
            ServItemNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                        SysConst.PACKAGESPILTER);
            ServPlanNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                        SysConst.PACKAGESPILTER);
            CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                        SysConst.PACKAGESPILTER);
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                    SysConst.PACKAGESPILTER);
            GrpCustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                           SysConst.PACKAGESPILTER);
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                       SysConst.PACKAGESPILTER);
            FeeType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                     SysConst.PACKAGESPILTER);
            FeeNormal = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 12, SysConst.PACKAGESPILTER))).doubleValue();
            FeePay = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    13, SysConst.PACKAGESPILTER))).doubleValue();
            PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                   SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                       SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 17, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 19, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                        SysConst.PACKAGESPILTER);
            FeeTaskExecNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,
                                           SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHFeeChargeSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("TaskExecNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TaskExecNo));
        }
        if (FCode.equals("ServTaskNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServTaskNo));
        }
        if (FCode.equals("ServTaskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServTaskCode));
        }
        if (FCode.equals("ServCaseCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServCaseCode));
        }
        if (FCode.equals("ServItemNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServItemNo));
        }
        if (FCode.equals("ServPlanNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServPlanNo));
        }
        if (FCode.equals("CustomerNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
        }
        if (FCode.equals("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equals("GrpCustomerNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpCustomerNo));
        }
        if (FCode.equals("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equals("FeeType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeType));
        }
        if (FCode.equals("FeeNormal")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeNormal));
        }
        if (FCode.equals("FeePay")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeePay));
        }
        if (FCode.equals("PrtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equals("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("FeeTaskExecNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeTaskExecNo));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(TaskExecNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(ServTaskNo);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(ServTaskCode);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(ServCaseCode);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(ServItemNo);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(ServPlanNo);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(CustomerNo);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(ContNo);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(GrpCustomerNo);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(GrpContNo);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(FeeType);
            break;
        case 11:
            strFieldValue = String.valueOf(FeeNormal);
            break;
        case 12:
            strFieldValue = String.valueOf(FeePay);
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(PrtNo);
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(ManageCom);
            break;
        case 15:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 16:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 17:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 18:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 19:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        case 20:
            strFieldValue = StrTool.GBKToUnicode(FeeTaskExecNo);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("TaskExecNo")) {
            if (FValue != null && !FValue.equals("")) {
                TaskExecNo = FValue.trim();
            } else {
                TaskExecNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServTaskNo")) {
            if (FValue != null && !FValue.equals("")) {
                ServTaskNo = FValue.trim();
            } else {
                ServTaskNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServTaskCode")) {
            if (FValue != null && !FValue.equals("")) {
                ServTaskCode = FValue.trim();
            } else {
                ServTaskCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServCaseCode")) {
            if (FValue != null && !FValue.equals("")) {
                ServCaseCode = FValue.trim();
            } else {
                ServCaseCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServItemNo")) {
            if (FValue != null && !FValue.equals("")) {
                ServItemNo = FValue.trim();
            } else {
                ServItemNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServPlanNo")) {
            if (FValue != null && !FValue.equals("")) {
                ServPlanNo = FValue.trim();
            } else {
                ServPlanNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            if (FValue != null && !FValue.equals("")) {
                CustomerNo = FValue.trim();
            } else {
                CustomerNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if (FValue != null && !FValue.equals("")) {
                ContNo = FValue.trim();
            } else {
                ContNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("GrpCustomerNo")) {
            if (FValue != null && !FValue.equals("")) {
                GrpCustomerNo = FValue.trim();
            } else {
                GrpCustomerNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if (FValue != null && !FValue.equals("")) {
                GrpContNo = FValue.trim();
            } else {
                GrpContNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("FeeType")) {
            if (FValue != null && !FValue.equals("")) {
                FeeType = FValue.trim();
            } else {
                FeeType = null;
            }
        }
        if (FCode.equalsIgnoreCase("FeeNormal")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                FeeNormal = d;
            }
        }
        if (FCode.equalsIgnoreCase("FeePay")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                FeePay = d;
            }
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            if (FValue != null && !FValue.equals("")) {
                PrtNo = FValue.trim();
            } else {
                PrtNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ManageCom = FValue.trim();
            } else {
                ManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("FeeTaskExecNo")) {
            if (FValue != null && !FValue.equals("")) {
                FeeTaskExecNo = FValue.trim();
            } else {
                FeeTaskExecNo = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LHFeeChargeSchema other = (LHFeeChargeSchema) otherObject;
        return
                TaskExecNo.equals(other.getTaskExecNo())
                && ServTaskNo.equals(other.getServTaskNo())
                && ServTaskCode.equals(other.getServTaskCode())
                && ServCaseCode.equals(other.getServCaseCode())
                && ServItemNo.equals(other.getServItemNo())
                && ServPlanNo.equals(other.getServPlanNo())
                && CustomerNo.equals(other.getCustomerNo())
                && ContNo.equals(other.getContNo())
                && GrpCustomerNo.equals(other.getGrpCustomerNo())
                && GrpContNo.equals(other.getGrpContNo())
                && FeeType.equals(other.getFeeType())
                && FeeNormal == other.getFeeNormal()
                && FeePay == other.getFeePay()
                && PrtNo.equals(other.getPrtNo())
                && ManageCom.equals(other.getManageCom())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && FeeTaskExecNo.equals(other.getFeeTaskExecNo());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("TaskExecNo")) {
            return 0;
        }
        if (strFieldName.equals("ServTaskNo")) {
            return 1;
        }
        if (strFieldName.equals("ServTaskCode")) {
            return 2;
        }
        if (strFieldName.equals("ServCaseCode")) {
            return 3;
        }
        if (strFieldName.equals("ServItemNo")) {
            return 4;
        }
        if (strFieldName.equals("ServPlanNo")) {
            return 5;
        }
        if (strFieldName.equals("CustomerNo")) {
            return 6;
        }
        if (strFieldName.equals("ContNo")) {
            return 7;
        }
        if (strFieldName.equals("GrpCustomerNo")) {
            return 8;
        }
        if (strFieldName.equals("GrpContNo")) {
            return 9;
        }
        if (strFieldName.equals("FeeType")) {
            return 10;
        }
        if (strFieldName.equals("FeeNormal")) {
            return 11;
        }
        if (strFieldName.equals("FeePay")) {
            return 12;
        }
        if (strFieldName.equals("PrtNo")) {
            return 13;
        }
        if (strFieldName.equals("ManageCom")) {
            return 14;
        }
        if (strFieldName.equals("Operator")) {
            return 15;
        }
        if (strFieldName.equals("MakeDate")) {
            return 16;
        }
        if (strFieldName.equals("MakeTime")) {
            return 17;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 18;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 19;
        }
        if (strFieldName.equals("FeeTaskExecNo")) {
            return 20;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "TaskExecNo";
            break;
        case 1:
            strFieldName = "ServTaskNo";
            break;
        case 2:
            strFieldName = "ServTaskCode";
            break;
        case 3:
            strFieldName = "ServCaseCode";
            break;
        case 4:
            strFieldName = "ServItemNo";
            break;
        case 5:
            strFieldName = "ServPlanNo";
            break;
        case 6:
            strFieldName = "CustomerNo";
            break;
        case 7:
            strFieldName = "ContNo";
            break;
        case 8:
            strFieldName = "GrpCustomerNo";
            break;
        case 9:
            strFieldName = "GrpContNo";
            break;
        case 10:
            strFieldName = "FeeType";
            break;
        case 11:
            strFieldName = "FeeNormal";
            break;
        case 12:
            strFieldName = "FeePay";
            break;
        case 13:
            strFieldName = "PrtNo";
            break;
        case 14:
            strFieldName = "ManageCom";
            break;
        case 15:
            strFieldName = "Operator";
            break;
        case 16:
            strFieldName = "MakeDate";
            break;
        case 17:
            strFieldName = "MakeTime";
            break;
        case 18:
            strFieldName = "ModifyDate";
            break;
        case 19:
            strFieldName = "ModifyTime";
            break;
        case 20:
            strFieldName = "FeeTaskExecNo";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("TaskExecNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServTaskNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServTaskCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServCaseCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServItemNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServPlanNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CustomerNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpCustomerNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpContNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FeeType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FeeNormal")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("FeePay")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("PrtNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FeeTaskExecNo")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 12:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 13:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 14:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 15:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 16:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 17:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 18:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 19:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 20:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
