/*
 * <p>ClassName: LHFactorySetSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LHFactorySetDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LHFactorySetSchema implements Schema
{
    // @Field
    /** 总单/合同号码 */
    private String ContNo;
    /** 集体保单号码 */
    private String GrpPolNo;
    /** 个单号码 */
    private String PolNo;
    /** 其它号码 */
    private String OtherNo;
    /** 其它号码类别 */
    private String OtherNoType;
    /** 计算编码 */
    private String CalCode;
    /** 参数引用名 */
    private String CalName;
    /** 计算sql */
    private String CalSql;
    /** 参数 */
    private String Params;
    /** 计算备注 */
    private String CalRemark;

    public static final int FIELDNUM = 10; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LHFactorySetSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[6];
        pk[0] = "ContNo";
        pk[1] = "GrpPolNo";
        pk[2] = "PolNo";
        pk[3] = "OtherNo";
        pk[4] = "OtherNoType";
        pk[5] = "CalCode";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getContNo()
    {
        if (ContNo != null && !ContNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            ContNo = StrTool.unicodeToGBK(ContNo);
        }
        return ContNo;
    }

    public void setContNo(String aContNo)
    {
        ContNo = aContNo;
    }

    public String getGrpPolNo()
    {
        if (GrpPolNo != null && !GrpPolNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            GrpPolNo = StrTool.unicodeToGBK(GrpPolNo);
        }
        return GrpPolNo;
    }

    public void setGrpPolNo(String aGrpPolNo)
    {
        GrpPolNo = aGrpPolNo;
    }

    public String getPolNo()
    {
        if (PolNo != null && !PolNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            PolNo = StrTool.unicodeToGBK(PolNo);
        }
        return PolNo;
    }

    public void setPolNo(String aPolNo)
    {
        PolNo = aPolNo;
    }

    public String getOtherNo()
    {
        if (OtherNo != null && !OtherNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            OtherNo = StrTool.unicodeToGBK(OtherNo);
        }
        return OtherNo;
    }

    public void setOtherNo(String aOtherNo)
    {
        OtherNo = aOtherNo;
    }

    public String getOtherNoType()
    {
        if (OtherNoType != null && !OtherNoType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            OtherNoType = StrTool.unicodeToGBK(OtherNoType);
        }
        return OtherNoType;
    }

    public void setOtherNoType(String aOtherNoType)
    {
        OtherNoType = aOtherNoType;
    }

    public String getCalCode()
    {
        if (CalCode != null && !CalCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            CalCode = StrTool.unicodeToGBK(CalCode);
        }
        return CalCode;
    }

    public void setCalCode(String aCalCode)
    {
        CalCode = aCalCode;
    }

    public String getCalName()
    {
        if (CalName != null && !CalName.equals("") && SysConst.CHANGECHARSET == true)
        {
            CalName = StrTool.unicodeToGBK(CalName);
        }
        return CalName;
    }

    public void setCalName(String aCalName)
    {
        CalName = aCalName;
    }

    public String getCalSql()
    {
        if (CalSql != null && !CalSql.equals("") && SysConst.CHANGECHARSET == true)
        {
            CalSql = StrTool.unicodeToGBK(CalSql);
        }
        return CalSql;
    }

    public void setCalSql(String aCalSql)
    {
        CalSql = aCalSql;
    }

    public String getParams()
    {
        if (Params != null && !Params.equals("") && SysConst.CHANGECHARSET == true)
        {
            Params = StrTool.unicodeToGBK(Params);
        }
        return Params;
    }

    public void setParams(String aParams)
    {
        Params = aParams;
    }

    public String getCalRemark()
    {
        if (CalRemark != null && !CalRemark.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CalRemark = StrTool.unicodeToGBK(CalRemark);
        }
        return CalRemark;
    }

    public void setCalRemark(String aCalRemark)
    {
        CalRemark = aCalRemark;
    }

    /**
     * 使用另外一个 LHFactorySetSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LHFactorySetSchema aLHFactorySetSchema)
    {
        this.ContNo = aLHFactorySetSchema.getContNo();
        this.GrpPolNo = aLHFactorySetSchema.getGrpPolNo();
        this.PolNo = aLHFactorySetSchema.getPolNo();
        this.OtherNo = aLHFactorySetSchema.getOtherNo();
        this.OtherNoType = aLHFactorySetSchema.getOtherNoType();
        this.CalCode = aLHFactorySetSchema.getCalCode();
        this.CalName = aLHFactorySetSchema.getCalName();
        this.CalSql = aLHFactorySetSchema.getCalSql();
        this.Params = aLHFactorySetSchema.getParams();
        this.CalRemark = aLHFactorySetSchema.getCalRemark();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ContNo") == null)
            {
                this.ContNo = null;
            }
            else
            {
                this.ContNo = rs.getString("ContNo").trim();
            }

            if (rs.getString("GrpPolNo") == null)
            {
                this.GrpPolNo = null;
            }
            else
            {
                this.GrpPolNo = rs.getString("GrpPolNo").trim();
            }

            if (rs.getString("PolNo") == null)
            {
                this.PolNo = null;
            }
            else
            {
                this.PolNo = rs.getString("PolNo").trim();
            }

            if (rs.getString("OtherNo") == null)
            {
                this.OtherNo = null;
            }
            else
            {
                this.OtherNo = rs.getString("OtherNo").trim();
            }

            if (rs.getString("OtherNoType") == null)
            {
                this.OtherNoType = null;
            }
            else
            {
                this.OtherNoType = rs.getString("OtherNoType").trim();
            }

            if (rs.getString("CalCode") == null)
            {
                this.CalCode = null;
            }
            else
            {
                this.CalCode = rs.getString("CalCode").trim();
            }

            if (rs.getString("CalName") == null)
            {
                this.CalName = null;
            }
            else
            {
                this.CalName = rs.getString("CalName").trim();
            }

            if (rs.getString("CalSql") == null)
            {
                this.CalSql = null;
            }
            else
            {
                this.CalSql = rs.getString("CalSql").trim();
            }

            if (rs.getString("Params") == null)
            {
                this.Params = null;
            }
            else
            {
                this.Params = rs.getString("Params").trim();
            }

            if (rs.getString("CalRemark") == null)
            {
                this.CalRemark = null;
            }
            else
            {
                this.CalRemark = rs.getString("CalRemark").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHFactorySetSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LHFactorySetSchema getSchema()
    {
        LHFactorySetSchema aLHFactorySetSchema = new LHFactorySetSchema();
        aLHFactorySetSchema.setSchema(this);
        return aLHFactorySetSchema;
    }

    public LHFactorySetDB getDB()
    {
        LHFactorySetDB aDBOper = new LHFactorySetDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHFactorySet描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(ContNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GrpPolNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PolNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(OtherNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(OtherNoType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CalCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CalName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CalSql)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Params)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CalRemark));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHFactorySet>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                    SysConst.PACKAGESPILTER);
            GrpPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                   SysConst.PACKAGESPILTER);
            OtherNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                     SysConst.PACKAGESPILTER);
            OtherNoType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                         SysConst.PACKAGESPILTER);
            CalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                     SysConst.PACKAGESPILTER);
            CalName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                     SysConst.PACKAGESPILTER);
            CalSql = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                    SysConst.PACKAGESPILTER);
            Params = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                    SysConst.PACKAGESPILTER);
            CalRemark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                       SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHFactorySetSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("ContNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ContNo));
        }
        if (FCode.equals("GrpPolNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GrpPolNo));
        }
        if (FCode.equals("PolNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PolNo));
        }
        if (FCode.equals("OtherNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(OtherNo));
        }
        if (FCode.equals("OtherNoType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(OtherNoType));
        }
        if (FCode.equals("CalCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CalCode));
        }
        if (FCode.equals("CalName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CalName));
        }
        if (FCode.equals("CalSql"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CalSql));
        }
        if (FCode.equals("Params"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Params));
        }
        if (FCode.equals("CalRemark"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CalRemark));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(GrpPolNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(PolNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(OtherNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(OtherNoType);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(CalCode);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(CalName);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(CalSql);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(Params);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(CalRemark);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("ContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
            {
                ContNo = null;
            }
        }
        if (FCode.equals("GrpPolNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpPolNo = FValue.trim();
            }
            else
            {
                GrpPolNo = null;
            }
        }
        if (FCode.equals("PolNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
            {
                PolNo = null;
            }
        }
        if (FCode.equals("OtherNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OtherNo = FValue.trim();
            }
            else
            {
                OtherNo = null;
            }
        }
        if (FCode.equals("OtherNoType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OtherNoType = FValue.trim();
            }
            else
            {
                OtherNoType = null;
            }
        }
        if (FCode.equals("CalCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CalCode = FValue.trim();
            }
            else
            {
                CalCode = null;
            }
        }
        if (FCode.equals("CalName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CalName = FValue.trim();
            }
            else
            {
                CalName = null;
            }
        }
        if (FCode.equals("CalSql"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CalSql = FValue.trim();
            }
            else
            {
                CalSql = null;
            }
        }
        if (FCode.equals("Params"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Params = FValue.trim();
            }
            else
            {
                Params = null;
            }
        }
        if (FCode.equals("CalRemark"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CalRemark = FValue.trim();
            }
            else
            {
                CalRemark = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LHFactorySetSchema other = (LHFactorySetSchema) otherObject;
        return
                ContNo.equals(other.getContNo())
                && GrpPolNo.equals(other.getGrpPolNo())
                && PolNo.equals(other.getPolNo())
                && OtherNo.equals(other.getOtherNo())
                && OtherNoType.equals(other.getOtherNoType())
                && CalCode.equals(other.getCalCode())
                && CalName.equals(other.getCalName())
                && CalSql.equals(other.getCalSql())
                && Params.equals(other.getParams())
                && CalRemark.equals(other.getCalRemark());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("ContNo"))
        {
            return 0;
        }
        if (strFieldName.equals("GrpPolNo"))
        {
            return 1;
        }
        if (strFieldName.equals("PolNo"))
        {
            return 2;
        }
        if (strFieldName.equals("OtherNo"))
        {
            return 3;
        }
        if (strFieldName.equals("OtherNoType"))
        {
            return 4;
        }
        if (strFieldName.equals("CalCode"))
        {
            return 5;
        }
        if (strFieldName.equals("CalName"))
        {
            return 6;
        }
        if (strFieldName.equals("CalSql"))
        {
            return 7;
        }
        if (strFieldName.equals("Params"))
        {
            return 8;
        }
        if (strFieldName.equals("CalRemark"))
        {
            return 9;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "ContNo";
                break;
            case 1:
                strFieldName = "GrpPolNo";
                break;
            case 2:
                strFieldName = "PolNo";
                break;
            case 3:
                strFieldName = "OtherNo";
                break;
            case 4:
                strFieldName = "OtherNoType";
                break;
            case 5:
                strFieldName = "CalCode";
                break;
            case 6:
                strFieldName = "CalName";
                break;
            case 7:
                strFieldName = "CalSql";
                break;
            case 8:
                strFieldName = "Params";
                break;
            case 9:
                strFieldName = "CalRemark";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("ContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpPolNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PolNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OtherNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OtherNoType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CalCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CalName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CalSql"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Params"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CalRemark"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
