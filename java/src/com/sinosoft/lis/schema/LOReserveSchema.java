/*
 * <p>ClassName: LOReserveSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2004-11-09
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LOReserveDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LOReserveSchema implements Schema
{
    // @Field
    /** 管理机构 */
    private String ManageCom;
    /** 保单号码 */
    private String PolNo;
    /** 保单状态 */
    private String PolStatus;
    /** 险种编码 */
    private String RiskCode;
    /** 销售渠道 */
    private String SaleChnl;
    /** 被保人客户号 */
    private String InsuredNo;
    /** 被保人性别 */
    private String InsuredSex;
    /** 被保人出日日期 */
    private Date InsuredBirthday;
    /** 被保人投保年龄 */
    private int InsuredAppAge;
    /** 第二被保人性别 */
    private String SecInsuredSex;
    /** 第二被保人出生日期 */
    private Date SecBirthDay;
    /** 第二被保人年龄 */
    private int SecInsuredAge;
    /** 保单生效日期 */
    private Date CValiDate;
    /** 最近复效日期 */
    private Date LastRevDate;
    /** 入机日期 */
    private Date MakeDate;
    /** 保险期间 */
    private int Years;
    /** 交费期间 */
    private int PayYears;
    /** 交费间隔 */
    private int PayIntv;
    /** 每期保费 */
    private double StandPrem;
    /** 基本保额 */
    private double Amnt;
    /** 失效日期 */
    private Date InvalidDate;
    /** 免交开始日期 */
    private Date FreeStartDate;
    /** 被保人死亡日期 */
    private Date DeadDate;
    /** 该领未领金额 */
    private double SouldGetMoney;
    /** 合同终止原因 */
    private String StopReason;
    /** 合同终止日期 */
    private Date StopDate;
    /** 养老金领取年龄 */
    private int RevGetAge;
    /** 养老金开始领取日期 */
    private Date RevGetDate;
    /** 养老金首次领取金额 */
    private double RevGetMoney;
    /** 养老金领取方式 */
    private int RevGetIntv;
    /** 年金给付金额 */
    private double AnnMoney;
    /** 死亡伤残给付金额 */
    private double DeathMoney;
    /** 医疗及重疾给付金额 */
    private double MedMoney;
    /** 满期给付金额 */
    private double ExpMoney;
    /** 累计已付金额 */
    private double TotalMoney;
    /** 职业加费 */
    private double OccPrem;
    /** 健康加费 */
    private double HealthPrem;
    /** 上会计年度保费收入 */
    private double LastPayPrem;
    /** 保单年度 */
    private int YearIdx;
    /** 计算年度 */
    private int CalYear;
    /** 责任金三 */
    private double Rev3;
    /** 责任金一 */
    private double Rev1;
    /** 责任金二 */
    private double Rev2;
    /** 责任金四 */
    private double Rev4;
    /** 保费计算检查标志 */
    private int CheckFlag;
    /** 准备金计算检查标志 */
    private int CalFlag;
    /** 数据提取日期 */
    private Date ModifyDate;
    /** 签单日期 */
    private Date SignDate;
    /** 本年累计保费 */
    private double ThisPayPrem;
    /** 交至日期 */
    private Date PaytoDate;
    /** 管理费比例 */
    private double ManageFeeRate;
    /** 保险年龄年期标志 */
    private String InsuYearFlag;
    /** 缴费年龄年期标志 */
    private String PayEndYearFlag;
    /** 退保金额 */
    private double SurMoney;
    /** 吸烟标志 */
    private String SmokingFlag;
    /** 职业类别 */
    private String OccupationType;
    /** 保单送达日期 */
    private Date CustomGetpoldate;
    /** 打折系数 */
    private double FloatRate;
    /** 集体保单号 */
    private String Groupno;
    /** 退费金额 */
    private double Getmoney;

    public static final int FIELDNUM = 60; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LOReserveSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "PolNo";
        pk[1] = "YearIdx";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getManageCom()
    {
        if (ManageCom != null && !ManageCom.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ManageCom = StrTool.unicodeToGBK(ManageCom);
        }
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public String getPolNo()
    {
        if (PolNo != null && !PolNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            PolNo = StrTool.unicodeToGBK(PolNo);
        }
        return PolNo;
    }

    public void setPolNo(String aPolNo)
    {
        PolNo = aPolNo;
    }

    public String getPolStatus()
    {
        if (PolStatus != null && !PolStatus.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            PolStatus = StrTool.unicodeToGBK(PolStatus);
        }
        return PolStatus;
    }

    public void setPolStatus(String aPolStatus)
    {
        PolStatus = aPolStatus;
    }

    public String getRiskCode()
    {
        if (RiskCode != null && !RiskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getSaleChnl()
    {
        if (SaleChnl != null && !SaleChnl.equals("") && SysConst.CHANGECHARSET == true)
        {
            SaleChnl = StrTool.unicodeToGBK(SaleChnl);
        }
        return SaleChnl;
    }

    public void setSaleChnl(String aSaleChnl)
    {
        SaleChnl = aSaleChnl;
    }

    public String getInsuredNo()
    {
        if (InsuredNo != null && !InsuredNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            InsuredNo = StrTool.unicodeToGBK(InsuredNo);
        }
        return InsuredNo;
    }

    public void setInsuredNo(String aInsuredNo)
    {
        InsuredNo = aInsuredNo;
    }

    public String getInsuredSex()
    {
        if (InsuredSex != null && !InsuredSex.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            InsuredSex = StrTool.unicodeToGBK(InsuredSex);
        }
        return InsuredSex;
    }

    public void setInsuredSex(String aInsuredSex)
    {
        InsuredSex = aInsuredSex;
    }

    public String getInsuredBirthday()
    {
        if (InsuredBirthday != null)
        {
            return fDate.getString(InsuredBirthday);
        }
        else
        {
            return null;
        }
    }

    public void setInsuredBirthday(Date aInsuredBirthday)
    {
        InsuredBirthday = aInsuredBirthday;
    }

    public void setInsuredBirthday(String aInsuredBirthday)
    {
        if (aInsuredBirthday != null && !aInsuredBirthday.equals(""))
        {
            InsuredBirthday = fDate.getDate(aInsuredBirthday);
        }
        else
        {
            InsuredBirthday = null;
        }
    }

    public int getInsuredAppAge()
    {
        return InsuredAppAge;
    }

    public void setInsuredAppAge(int aInsuredAppAge)
    {
        InsuredAppAge = aInsuredAppAge;
    }

    public void setInsuredAppAge(String aInsuredAppAge)
    {
        if (aInsuredAppAge != null && !aInsuredAppAge.equals(""))
        {
            Integer tInteger = new Integer(aInsuredAppAge);
            int i = tInteger.intValue();
            InsuredAppAge = i;
        }
    }

    public String getSecInsuredSex()
    {
        if (SecInsuredSex != null && !SecInsuredSex.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            SecInsuredSex = StrTool.unicodeToGBK(SecInsuredSex);
        }
        return SecInsuredSex;
    }

    public void setSecInsuredSex(String aSecInsuredSex)
    {
        SecInsuredSex = aSecInsuredSex;
    }

    public String getSecBirthDay()
    {
        if (SecBirthDay != null)
        {
            return fDate.getString(SecBirthDay);
        }
        else
        {
            return null;
        }
    }

    public void setSecBirthDay(Date aSecBirthDay)
    {
        SecBirthDay = aSecBirthDay;
    }

    public void setSecBirthDay(String aSecBirthDay)
    {
        if (aSecBirthDay != null && !aSecBirthDay.equals(""))
        {
            SecBirthDay = fDate.getDate(aSecBirthDay);
        }
        else
        {
            SecBirthDay = null;
        }
    }

    public int getSecInsuredAge()
    {
        return SecInsuredAge;
    }

    public void setSecInsuredAge(int aSecInsuredAge)
    {
        SecInsuredAge = aSecInsuredAge;
    }

    public void setSecInsuredAge(String aSecInsuredAge)
    {
        if (aSecInsuredAge != null && !aSecInsuredAge.equals(""))
        {
            Integer tInteger = new Integer(aSecInsuredAge);
            int i = tInteger.intValue();
            SecInsuredAge = i;
        }
    }

    public String getCValiDate()
    {
        if (CValiDate != null)
        {
            return fDate.getString(CValiDate);
        }
        else
        {
            return null;
        }
    }

    public void setCValiDate(Date aCValiDate)
    {
        CValiDate = aCValiDate;
    }

    public void setCValiDate(String aCValiDate)
    {
        if (aCValiDate != null && !aCValiDate.equals(""))
        {
            CValiDate = fDate.getDate(aCValiDate);
        }
        else
        {
            CValiDate = null;
        }
    }

    public String getLastRevDate()
    {
        if (LastRevDate != null)
        {
            return fDate.getString(LastRevDate);
        }
        else
        {
            return null;
        }
    }

    public void setLastRevDate(Date aLastRevDate)
    {
        LastRevDate = aLastRevDate;
    }

    public void setLastRevDate(String aLastRevDate)
    {
        if (aLastRevDate != null && !aLastRevDate.equals(""))
        {
            LastRevDate = fDate.getDate(aLastRevDate);
        }
        else
        {
            LastRevDate = null;
        }
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public int getYears()
    {
        return Years;
    }

    public void setYears(int aYears)
    {
        Years = aYears;
    }

    public void setYears(String aYears)
    {
        if (aYears != null && !aYears.equals(""))
        {
            Integer tInteger = new Integer(aYears);
            int i = tInteger.intValue();
            Years = i;
        }
    }

    public int getPayYears()
    {
        return PayYears;
    }

    public void setPayYears(int aPayYears)
    {
        PayYears = aPayYears;
    }

    public void setPayYears(String aPayYears)
    {
        if (aPayYears != null && !aPayYears.equals(""))
        {
            Integer tInteger = new Integer(aPayYears);
            int i = tInteger.intValue();
            PayYears = i;
        }
    }

    public int getPayIntv()
    {
        return PayIntv;
    }

    public void setPayIntv(int aPayIntv)
    {
        PayIntv = aPayIntv;
    }

    public void setPayIntv(String aPayIntv)
    {
        if (aPayIntv != null && !aPayIntv.equals(""))
        {
            Integer tInteger = new Integer(aPayIntv);
            int i = tInteger.intValue();
            PayIntv = i;
        }
    }

    public double getStandPrem()
    {
        return StandPrem;
    }

    public void setStandPrem(double aStandPrem)
    {
        StandPrem = aStandPrem;
    }

    public void setStandPrem(String aStandPrem)
    {
        if (aStandPrem != null && !aStandPrem.equals(""))
        {
            Double tDouble = new Double(aStandPrem);
            double d = tDouble.doubleValue();
            StandPrem = d;
        }
    }

    public double getAmnt()
    {
        return Amnt;
    }

    public void setAmnt(double aAmnt)
    {
        Amnt = aAmnt;
    }

    public void setAmnt(String aAmnt)
    {
        if (aAmnt != null && !aAmnt.equals(""))
        {
            Double tDouble = new Double(aAmnt);
            double d = tDouble.doubleValue();
            Amnt = d;
        }
    }

    public String getInvalidDate()
    {
        if (InvalidDate != null)
        {
            return fDate.getString(InvalidDate);
        }
        else
        {
            return null;
        }
    }

    public void setInvalidDate(Date aInvalidDate)
    {
        InvalidDate = aInvalidDate;
    }

    public void setInvalidDate(String aInvalidDate)
    {
        if (aInvalidDate != null && !aInvalidDate.equals(""))
        {
            InvalidDate = fDate.getDate(aInvalidDate);
        }
        else
        {
            InvalidDate = null;
        }
    }

    public String getFreeStartDate()
    {
        if (FreeStartDate != null)
        {
            return fDate.getString(FreeStartDate);
        }
        else
        {
            return null;
        }
    }

    public void setFreeStartDate(Date aFreeStartDate)
    {
        FreeStartDate = aFreeStartDate;
    }

    public void setFreeStartDate(String aFreeStartDate)
    {
        if (aFreeStartDate != null && !aFreeStartDate.equals(""))
        {
            FreeStartDate = fDate.getDate(aFreeStartDate);
        }
        else
        {
            FreeStartDate = null;
        }
    }

    public String getDeadDate()
    {
        if (DeadDate != null)
        {
            return fDate.getString(DeadDate);
        }
        else
        {
            return null;
        }
    }

    public void setDeadDate(Date aDeadDate)
    {
        DeadDate = aDeadDate;
    }

    public void setDeadDate(String aDeadDate)
    {
        if (aDeadDate != null && !aDeadDate.equals(""))
        {
            DeadDate = fDate.getDate(aDeadDate);
        }
        else
        {
            DeadDate = null;
        }
    }

    public double getSouldGetMoney()
    {
        return SouldGetMoney;
    }

    public void setSouldGetMoney(double aSouldGetMoney)
    {
        SouldGetMoney = aSouldGetMoney;
    }

    public void setSouldGetMoney(String aSouldGetMoney)
    {
        if (aSouldGetMoney != null && !aSouldGetMoney.equals(""))
        {
            Double tDouble = new Double(aSouldGetMoney);
            double d = tDouble.doubleValue();
            SouldGetMoney = d;
        }
    }

    public String getStopReason()
    {
        if (StopReason != null && !StopReason.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            StopReason = StrTool.unicodeToGBK(StopReason);
        }
        return StopReason;
    }

    public void setStopReason(String aStopReason)
    {
        StopReason = aStopReason;
    }

    public String getStopDate()
    {
        if (StopDate != null)
        {
            return fDate.getString(StopDate);
        }
        else
        {
            return null;
        }
    }

    public void setStopDate(Date aStopDate)
    {
        StopDate = aStopDate;
    }

    public void setStopDate(String aStopDate)
    {
        if (aStopDate != null && !aStopDate.equals(""))
        {
            StopDate = fDate.getDate(aStopDate);
        }
        else
        {
            StopDate = null;
        }
    }

    public int getRevGetAge()
    {
        return RevGetAge;
    }

    public void setRevGetAge(int aRevGetAge)
    {
        RevGetAge = aRevGetAge;
    }

    public void setRevGetAge(String aRevGetAge)
    {
        if (aRevGetAge != null && !aRevGetAge.equals(""))
        {
            Integer tInteger = new Integer(aRevGetAge);
            int i = tInteger.intValue();
            RevGetAge = i;
        }
    }

    public String getRevGetDate()
    {
        if (RevGetDate != null)
        {
            return fDate.getString(RevGetDate);
        }
        else
        {
            return null;
        }
    }

    public void setRevGetDate(Date aRevGetDate)
    {
        RevGetDate = aRevGetDate;
    }

    public void setRevGetDate(String aRevGetDate)
    {
        if (aRevGetDate != null && !aRevGetDate.equals(""))
        {
            RevGetDate = fDate.getDate(aRevGetDate);
        }
        else
        {
            RevGetDate = null;
        }
    }

    public double getRevGetMoney()
    {
        return RevGetMoney;
    }

    public void setRevGetMoney(double aRevGetMoney)
    {
        RevGetMoney = aRevGetMoney;
    }

    public void setRevGetMoney(String aRevGetMoney)
    {
        if (aRevGetMoney != null && !aRevGetMoney.equals(""))
        {
            Double tDouble = new Double(aRevGetMoney);
            double d = tDouble.doubleValue();
            RevGetMoney = d;
        }
    }

    public int getRevGetIntv()
    {
        return RevGetIntv;
    }

    public void setRevGetIntv(int aRevGetIntv)
    {
        RevGetIntv = aRevGetIntv;
    }

    public void setRevGetIntv(String aRevGetIntv)
    {
        if (aRevGetIntv != null && !aRevGetIntv.equals(""))
        {
            Integer tInteger = new Integer(aRevGetIntv);
            int i = tInteger.intValue();
            RevGetIntv = i;
        }
    }

    public double getAnnMoney()
    {
        return AnnMoney;
    }

    public void setAnnMoney(double aAnnMoney)
    {
        AnnMoney = aAnnMoney;
    }

    public void setAnnMoney(String aAnnMoney)
    {
        if (aAnnMoney != null && !aAnnMoney.equals(""))
        {
            Double tDouble = new Double(aAnnMoney);
            double d = tDouble.doubleValue();
            AnnMoney = d;
        }
    }

    public double getDeathMoney()
    {
        return DeathMoney;
    }

    public void setDeathMoney(double aDeathMoney)
    {
        DeathMoney = aDeathMoney;
    }

    public void setDeathMoney(String aDeathMoney)
    {
        if (aDeathMoney != null && !aDeathMoney.equals(""))
        {
            Double tDouble = new Double(aDeathMoney);
            double d = tDouble.doubleValue();
            DeathMoney = d;
        }
    }

    public double getMedMoney()
    {
        return MedMoney;
    }

    public void setMedMoney(double aMedMoney)
    {
        MedMoney = aMedMoney;
    }

    public void setMedMoney(String aMedMoney)
    {
        if (aMedMoney != null && !aMedMoney.equals(""))
        {
            Double tDouble = new Double(aMedMoney);
            double d = tDouble.doubleValue();
            MedMoney = d;
        }
    }

    public double getExpMoney()
    {
        return ExpMoney;
    }

    public void setExpMoney(double aExpMoney)
    {
        ExpMoney = aExpMoney;
    }

    public void setExpMoney(String aExpMoney)
    {
        if (aExpMoney != null && !aExpMoney.equals(""))
        {
            Double tDouble = new Double(aExpMoney);
            double d = tDouble.doubleValue();
            ExpMoney = d;
        }
    }

    public double getTotalMoney()
    {
        return TotalMoney;
    }

    public void setTotalMoney(double aTotalMoney)
    {
        TotalMoney = aTotalMoney;
    }

    public void setTotalMoney(String aTotalMoney)
    {
        if (aTotalMoney != null && !aTotalMoney.equals(""))
        {
            Double tDouble = new Double(aTotalMoney);
            double d = tDouble.doubleValue();
            TotalMoney = d;
        }
    }

    public double getOccPrem()
    {
        return OccPrem;
    }

    public void setOccPrem(double aOccPrem)
    {
        OccPrem = aOccPrem;
    }

    public void setOccPrem(String aOccPrem)
    {
        if (aOccPrem != null && !aOccPrem.equals(""))
        {
            Double tDouble = new Double(aOccPrem);
            double d = tDouble.doubleValue();
            OccPrem = d;
        }
    }

    public double getHealthPrem()
    {
        return HealthPrem;
    }

    public void setHealthPrem(double aHealthPrem)
    {
        HealthPrem = aHealthPrem;
    }

    public void setHealthPrem(String aHealthPrem)
    {
        if (aHealthPrem != null && !aHealthPrem.equals(""))
        {
            Double tDouble = new Double(aHealthPrem);
            double d = tDouble.doubleValue();
            HealthPrem = d;
        }
    }

    public double getLastPayPrem()
    {
        return LastPayPrem;
    }

    public void setLastPayPrem(double aLastPayPrem)
    {
        LastPayPrem = aLastPayPrem;
    }

    public void setLastPayPrem(String aLastPayPrem)
    {
        if (aLastPayPrem != null && !aLastPayPrem.equals(""))
        {
            Double tDouble = new Double(aLastPayPrem);
            double d = tDouble.doubleValue();
            LastPayPrem = d;
        }
    }

    public int getYearIdx()
    {
        return YearIdx;
    }

    public void setYearIdx(int aYearIdx)
    {
        YearIdx = aYearIdx;
    }

    public void setYearIdx(String aYearIdx)
    {
        if (aYearIdx != null && !aYearIdx.equals(""))
        {
            Integer tInteger = new Integer(aYearIdx);
            int i = tInteger.intValue();
            YearIdx = i;
        }
    }

    public int getCalYear()
    {
        return CalYear;
    }

    public void setCalYear(int aCalYear)
    {
        CalYear = aCalYear;
    }

    public void setCalYear(String aCalYear)
    {
        if (aCalYear != null && !aCalYear.equals(""))
        {
            Integer tInteger = new Integer(aCalYear);
            int i = tInteger.intValue();
            CalYear = i;
        }
    }

    public double getRev3()
    {
        return Rev3;
    }

    public void setRev3(double aRev3)
    {
        Rev3 = aRev3;
    }

    public void setRev3(String aRev3)
    {
        if (aRev3 != null && !aRev3.equals(""))
        {
            Double tDouble = new Double(aRev3);
            double d = tDouble.doubleValue();
            Rev3 = d;
        }
    }

    public double getRev1()
    {
        return Rev1;
    }

    public void setRev1(double aRev1)
    {
        Rev1 = aRev1;
    }

    public void setRev1(String aRev1)
    {
        if (aRev1 != null && !aRev1.equals(""))
        {
            Double tDouble = new Double(aRev1);
            double d = tDouble.doubleValue();
            Rev1 = d;
        }
    }

    public double getRev2()
    {
        return Rev2;
    }

    public void setRev2(double aRev2)
    {
        Rev2 = aRev2;
    }

    public void setRev2(String aRev2)
    {
        if (aRev2 != null && !aRev2.equals(""))
        {
            Double tDouble = new Double(aRev2);
            double d = tDouble.doubleValue();
            Rev2 = d;
        }
    }

    public double getRev4()
    {
        return Rev4;
    }

    public void setRev4(double aRev4)
    {
        Rev4 = aRev4;
    }

    public void setRev4(String aRev4)
    {
        if (aRev4 != null && !aRev4.equals(""))
        {
            Double tDouble = new Double(aRev4);
            double d = tDouble.doubleValue();
            Rev4 = d;
        }
    }

    public int getCheckFlag()
    {
        return CheckFlag;
    }

    public void setCheckFlag(int aCheckFlag)
    {
        CheckFlag = aCheckFlag;
    }

    public void setCheckFlag(String aCheckFlag)
    {
        if (aCheckFlag != null && !aCheckFlag.equals(""))
        {
            Integer tInteger = new Integer(aCheckFlag);
            int i = tInteger.intValue();
            CheckFlag = i;
        }
    }

    public int getCalFlag()
    {
        return CalFlag;
    }

    public void setCalFlag(int aCalFlag)
    {
        CalFlag = aCalFlag;
    }

    public void setCalFlag(String aCalFlag)
    {
        if (aCalFlag != null && !aCalFlag.equals(""))
        {
            Integer tInteger = new Integer(aCalFlag);
            int i = tInteger.intValue();
            CalFlag = i;
        }
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getSignDate()
    {
        if (SignDate != null)
        {
            return fDate.getString(SignDate);
        }
        else
        {
            return null;
        }
    }

    public void setSignDate(Date aSignDate)
    {
        SignDate = aSignDate;
    }

    public void setSignDate(String aSignDate)
    {
        if (aSignDate != null && !aSignDate.equals(""))
        {
            SignDate = fDate.getDate(aSignDate);
        }
        else
        {
            SignDate = null;
        }
    }

    public double getThisPayPrem()
    {
        return ThisPayPrem;
    }

    public void setThisPayPrem(double aThisPayPrem)
    {
        ThisPayPrem = aThisPayPrem;
    }

    public void setThisPayPrem(String aThisPayPrem)
    {
        if (aThisPayPrem != null && !aThisPayPrem.equals(""))
        {
            Double tDouble = new Double(aThisPayPrem);
            double d = tDouble.doubleValue();
            ThisPayPrem = d;
        }
    }

    public String getPaytoDate()
    {
        if (PaytoDate != null)
        {
            return fDate.getString(PaytoDate);
        }
        else
        {
            return null;
        }
    }

    public void setPaytoDate(Date aPaytoDate)
    {
        PaytoDate = aPaytoDate;
    }

    public void setPaytoDate(String aPaytoDate)
    {
        if (aPaytoDate != null && !aPaytoDate.equals(""))
        {
            PaytoDate = fDate.getDate(aPaytoDate);
        }
        else
        {
            PaytoDate = null;
        }
    }

    public double getManageFeeRate()
    {
        return ManageFeeRate;
    }

    public void setManageFeeRate(double aManageFeeRate)
    {
        ManageFeeRate = aManageFeeRate;
    }

    public void setManageFeeRate(String aManageFeeRate)
    {
        if (aManageFeeRate != null && !aManageFeeRate.equals(""))
        {
            Double tDouble = new Double(aManageFeeRate);
            double d = tDouble.doubleValue();
            ManageFeeRate = d;
        }
    }

    public String getInsuYearFlag()
    {
        if (InsuYearFlag != null && !InsuYearFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            InsuYearFlag = StrTool.unicodeToGBK(InsuYearFlag);
        }
        return InsuYearFlag;
    }

    public void setInsuYearFlag(String aInsuYearFlag)
    {
        InsuYearFlag = aInsuYearFlag;
    }

    public String getPayEndYearFlag()
    {
        if (PayEndYearFlag != null && !PayEndYearFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            PayEndYearFlag = StrTool.unicodeToGBK(PayEndYearFlag);
        }
        return PayEndYearFlag;
    }

    public void setPayEndYearFlag(String aPayEndYearFlag)
    {
        PayEndYearFlag = aPayEndYearFlag;
    }

    public double getSurMoney()
    {
        return SurMoney;
    }

    public void setSurMoney(double aSurMoney)
    {
        SurMoney = aSurMoney;
    }

    public void setSurMoney(String aSurMoney)
    {
        if (aSurMoney != null && !aSurMoney.equals(""))
        {
            Double tDouble = new Double(aSurMoney);
            double d = tDouble.doubleValue();
            SurMoney = d;
        }
    }

    public String getSmokingFlag()
    {
        if (SmokingFlag != null && !SmokingFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            SmokingFlag = StrTool.unicodeToGBK(SmokingFlag);
        }
        return SmokingFlag;
    }

    public void setSmokingFlag(String aSmokingFlag)
    {
        SmokingFlag = aSmokingFlag;
    }

    public String getOccupationType()
    {
        if (OccupationType != null && !OccupationType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            OccupationType = StrTool.unicodeToGBK(OccupationType);
        }
        return OccupationType;
    }

    public void setOccupationType(String aOccupationType)
    {
        OccupationType = aOccupationType;
    }

    public String getCustomGetpoldate()
    {
        if (CustomGetpoldate != null)
        {
            return fDate.getString(CustomGetpoldate);
        }
        else
        {
            return null;
        }
    }

    public void setCustomGetpoldate(Date aCustomGetpoldate)
    {
        CustomGetpoldate = aCustomGetpoldate;
    }

    public void setCustomGetpoldate(String aCustomGetpoldate)
    {
        if (aCustomGetpoldate != null && !aCustomGetpoldate.equals(""))
        {
            CustomGetpoldate = fDate.getDate(aCustomGetpoldate);
        }
        else
        {
            CustomGetpoldate = null;
        }
    }

    public double getFloatRate()
    {
        return FloatRate;
    }

    public void setFloatRate(double aFloatRate)
    {
        FloatRate = aFloatRate;
    }

    public void setFloatRate(String aFloatRate)
    {
        if (aFloatRate != null && !aFloatRate.equals(""))
        {
            Double tDouble = new Double(aFloatRate);
            double d = tDouble.doubleValue();
            FloatRate = d;
        }
    }

    public String getGroupno()
    {
        if (Groupno != null && !Groupno.equals("") && SysConst.CHANGECHARSET == true)
        {
            Groupno = StrTool.unicodeToGBK(Groupno);
        }
        return Groupno;
    }

    public void setGroupno(String aGroupno)
    {
        Groupno = aGroupno;
    }

    public double getGetmoney()
    {
        return Getmoney;
    }

    public void setGetmoney(double aGetmoney)
    {
        Getmoney = aGetmoney;
    }

    public void setGetmoney(String aGetmoney)
    {
        if (aGetmoney != null && !aGetmoney.equals(""))
        {
            Double tDouble = new Double(aGetmoney);
            double d = tDouble.doubleValue();
            Getmoney = d;
        }
    }


    /**
     * 使用另外一个 LOReserveSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LOReserveSchema aLOReserveSchema)
    {
        this.ManageCom = aLOReserveSchema.getManageCom();
        this.PolNo = aLOReserveSchema.getPolNo();
        this.PolStatus = aLOReserveSchema.getPolStatus();
        this.RiskCode = aLOReserveSchema.getRiskCode();
        this.SaleChnl = aLOReserveSchema.getSaleChnl();
        this.InsuredNo = aLOReserveSchema.getInsuredNo();
        this.InsuredSex = aLOReserveSchema.getInsuredSex();
        this.InsuredBirthday = fDate.getDate(aLOReserveSchema.
                                             getInsuredBirthday());
        this.InsuredAppAge = aLOReserveSchema.getInsuredAppAge();
        this.SecInsuredSex = aLOReserveSchema.getSecInsuredSex();
        this.SecBirthDay = fDate.getDate(aLOReserveSchema.getSecBirthDay());
        this.SecInsuredAge = aLOReserveSchema.getSecInsuredAge();
        this.CValiDate = fDate.getDate(aLOReserveSchema.getCValiDate());
        this.LastRevDate = fDate.getDate(aLOReserveSchema.getLastRevDate());
        this.MakeDate = fDate.getDate(aLOReserveSchema.getMakeDate());
        this.Years = aLOReserveSchema.getYears();
        this.PayYears = aLOReserveSchema.getPayYears();
        this.PayIntv = aLOReserveSchema.getPayIntv();
        this.StandPrem = aLOReserveSchema.getStandPrem();
        this.Amnt = aLOReserveSchema.getAmnt();
        this.InvalidDate = fDate.getDate(aLOReserveSchema.getInvalidDate());
        this.FreeStartDate = fDate.getDate(aLOReserveSchema.getFreeStartDate());
        this.DeadDate = fDate.getDate(aLOReserveSchema.getDeadDate());
        this.SouldGetMoney = aLOReserveSchema.getSouldGetMoney();
        this.StopReason = aLOReserveSchema.getStopReason();
        this.StopDate = fDate.getDate(aLOReserveSchema.getStopDate());
        this.RevGetAge = aLOReserveSchema.getRevGetAge();
        this.RevGetDate = fDate.getDate(aLOReserveSchema.getRevGetDate());
        this.RevGetMoney = aLOReserveSchema.getRevGetMoney();
        this.RevGetIntv = aLOReserveSchema.getRevGetIntv();
        this.AnnMoney = aLOReserveSchema.getAnnMoney();
        this.DeathMoney = aLOReserveSchema.getDeathMoney();
        this.MedMoney = aLOReserveSchema.getMedMoney();
        this.ExpMoney = aLOReserveSchema.getExpMoney();
        this.TotalMoney = aLOReserveSchema.getTotalMoney();
        this.OccPrem = aLOReserveSchema.getOccPrem();
        this.HealthPrem = aLOReserveSchema.getHealthPrem();
        this.LastPayPrem = aLOReserveSchema.getLastPayPrem();
        this.YearIdx = aLOReserveSchema.getYearIdx();
        this.CalYear = aLOReserveSchema.getCalYear();
        this.Rev3 = aLOReserveSchema.getRev3();
        this.Rev1 = aLOReserveSchema.getRev1();
        this.Rev2 = aLOReserveSchema.getRev2();
        this.Rev4 = aLOReserveSchema.getRev4();
        this.CheckFlag = aLOReserveSchema.getCheckFlag();
        this.CalFlag = aLOReserveSchema.getCalFlag();
        this.ModifyDate = fDate.getDate(aLOReserveSchema.getModifyDate());
        this.SignDate = fDate.getDate(aLOReserveSchema.getSignDate());
        this.ThisPayPrem = aLOReserveSchema.getThisPayPrem();
        this.PaytoDate = fDate.getDate(aLOReserveSchema.getPaytoDate());
        this.ManageFeeRate = aLOReserveSchema.getManageFeeRate();
        this.InsuYearFlag = aLOReserveSchema.getInsuYearFlag();
        this.PayEndYearFlag = aLOReserveSchema.getPayEndYearFlag();
        this.SurMoney = aLOReserveSchema.getSurMoney();
        this.SmokingFlag = aLOReserveSchema.getSmokingFlag();
        this.OccupationType = aLOReserveSchema.getOccupationType();
        this.CustomGetpoldate = fDate.getDate(aLOReserveSchema.
                                              getCustomGetpoldate());
        this.FloatRate = aLOReserveSchema.getFloatRate();
        this.Groupno = aLOReserveSchema.getGroupno();
        this.Getmoney = aLOReserveSchema.getGetmoney();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("PolNo") == null)
            {
                this.PolNo = null;
            }
            else
            {
                this.PolNo = rs.getString("PolNo").trim();
            }

            if (rs.getString("PolStatus") == null)
            {
                this.PolStatus = null;
            }
            else
            {
                this.PolStatus = rs.getString("PolStatus").trim();
            }

            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("SaleChnl") == null)
            {
                this.SaleChnl = null;
            }
            else
            {
                this.SaleChnl = rs.getString("SaleChnl").trim();
            }

            if (rs.getString("InsuredNo") == null)
            {
                this.InsuredNo = null;
            }
            else
            {
                this.InsuredNo = rs.getString("InsuredNo").trim();
            }

            if (rs.getString("InsuredSex") == null)
            {
                this.InsuredSex = null;
            }
            else
            {
                this.InsuredSex = rs.getString("InsuredSex").trim();
            }

            this.InsuredBirthday = rs.getDate("InsuredBirthday");
            this.InsuredAppAge = rs.getInt("InsuredAppAge");
            if (rs.getString("SecInsuredSex") == null)
            {
                this.SecInsuredSex = null;
            }
            else
            {
                this.SecInsuredSex = rs.getString("SecInsuredSex").trim();
            }

            this.SecBirthDay = rs.getDate("SecBirthDay");
            this.SecInsuredAge = rs.getInt("SecInsuredAge");
            this.CValiDate = rs.getDate("CValiDate");
            this.LastRevDate = rs.getDate("LastRevDate");
            this.MakeDate = rs.getDate("MakeDate");
            this.Years = rs.getInt("Years");
            this.PayYears = rs.getInt("PayYears");
            this.PayIntv = rs.getInt("PayIntv");
            this.StandPrem = rs.getDouble("StandPrem");
            this.Amnt = rs.getDouble("Amnt");
            this.InvalidDate = rs.getDate("InvalidDate");
            this.FreeStartDate = rs.getDate("FreeStartDate");
            this.DeadDate = rs.getDate("DeadDate");
            this.SouldGetMoney = rs.getDouble("SouldGetMoney");
            if (rs.getString("StopReason") == null)
            {
                this.StopReason = null;
            }
            else
            {
                this.StopReason = rs.getString("StopReason").trim();
            }

            this.StopDate = rs.getDate("StopDate");
            this.RevGetAge = rs.getInt("RevGetAge");
            this.RevGetDate = rs.getDate("RevGetDate");
            this.RevGetMoney = rs.getDouble("RevGetMoney");
            this.RevGetIntv = rs.getInt("RevGetIntv");
            this.AnnMoney = rs.getDouble("AnnMoney");
            this.DeathMoney = rs.getDouble("DeathMoney");
            this.MedMoney = rs.getDouble("MedMoney");
            this.ExpMoney = rs.getDouble("ExpMoney");
            this.TotalMoney = rs.getDouble("TotalMoney");
            this.OccPrem = rs.getDouble("OccPrem");
            this.HealthPrem = rs.getDouble("HealthPrem");
            this.LastPayPrem = rs.getDouble("LastPayPrem");
            this.YearIdx = rs.getInt("YearIdx");
            this.CalYear = rs.getInt("CalYear");
            this.Rev3 = rs.getDouble("Rev3");
            this.Rev1 = rs.getDouble("Rev1");
            this.Rev2 = rs.getDouble("Rev2");
            this.Rev4 = rs.getDouble("Rev4");
            this.CheckFlag = rs.getInt("CheckFlag");
            this.CalFlag = rs.getInt("CalFlag");
            this.ModifyDate = rs.getDate("ModifyDate");
            this.SignDate = rs.getDate("SignDate");
            this.ThisPayPrem = rs.getDouble("ThisPayPrem");
            this.PaytoDate = rs.getDate("PaytoDate");
            this.ManageFeeRate = rs.getDouble("ManageFeeRate");
            if (rs.getString("InsuYearFlag") == null)
            {
                this.InsuYearFlag = null;
            }
            else
            {
                this.InsuYearFlag = rs.getString("InsuYearFlag").trim();
            }

            if (rs.getString("PayEndYearFlag") == null)
            {
                this.PayEndYearFlag = null;
            }
            else
            {
                this.PayEndYearFlag = rs.getString("PayEndYearFlag").trim();
            }

            this.SurMoney = rs.getDouble("SurMoney");
            if (rs.getString("SmokingFlag") == null)
            {
                this.SmokingFlag = null;
            }
            else
            {
                this.SmokingFlag = rs.getString("SmokingFlag").trim();
            }

            if (rs.getString("OccupationType") == null)
            {
                this.OccupationType = null;
            }
            else
            {
                this.OccupationType = rs.getString("OccupationType").trim();
            }

            this.CustomGetpoldate = rs.getDate("CustomGetpoldate");
            this.FloatRate = rs.getDouble("FloatRate");
            if (rs.getString("Groupno") == null)
            {
                this.Groupno = null;
            }
            else
            {
                this.Groupno = rs.getString("Groupno").trim();
            }

            this.Getmoney = rs.getDouble("Getmoney");
        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LOReserveSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LOReserveSchema getSchema()
    {
        LOReserveSchema aLOReserveSchema = new LOReserveSchema();
        aLOReserveSchema.setSchema(this);
        return aLOReserveSchema;
    }

    public LOReserveDB getDB()
    {
        LOReserveDB aDBOper = new LOReserveDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOReserve描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(ManageCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PolNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PolStatus)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SaleChnl)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InsuredNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InsuredSex)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            InsuredBirthday))) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(InsuredAppAge) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SecInsuredSex)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            SecBirthDay))) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(SecInsuredAge) + SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            CValiDate))) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            LastRevDate))) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(Years) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(PayYears) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(PayIntv) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(StandPrem) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(Amnt) + SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            InvalidDate))) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            FreeStartDate))) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(DeadDate))) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(SouldGetMoney) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(StopReason)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(StopDate))) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(RevGetAge) + SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            RevGetDate))) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(RevGetMoney) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(RevGetIntv) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(AnnMoney) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(DeathMoney) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(MedMoney) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(ExpMoney) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(TotalMoney) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(OccPrem) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(HealthPrem) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(LastPayPrem) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(YearIdx) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(CalYear) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(Rev3) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(Rev1) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(Rev2) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(Rev4) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(CheckFlag) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(CalFlag) + SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(SignDate))) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(ThisPayPrem) + SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            PaytoDate))) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(ManageFeeRate) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InsuYearFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PayEndYearFlag)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(SurMoney) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SmokingFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(OccupationType)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            CustomGetpoldate))) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(FloatRate) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Groupno)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(Getmoney);
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOReserve>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                   SysConst.PACKAGESPILTER);
            PolStatus = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                       SysConst.PACKAGESPILTER);
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                      SysConst.PACKAGESPILTER);
            SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                      SysConst.PACKAGESPILTER);
            InsuredNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                       SysConst.PACKAGESPILTER);
            InsuredSex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                        SysConst.PACKAGESPILTER);
            InsuredBirthday = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 8, SysConst.PACKAGESPILTER));
            InsuredAppAge = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 9, SysConst.PACKAGESPILTER))).intValue();
            SecInsuredSex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                           SysConst.PACKAGESPILTER);
            SecBirthDay = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 11, SysConst.PACKAGESPILTER));
            SecInsuredAge = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 12, SysConst.PACKAGESPILTER))).intValue();
            CValiDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 13, SysConst.PACKAGESPILTER));
            LastRevDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 14, SysConst.PACKAGESPILTER));
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 15, SysConst.PACKAGESPILTER));
            Years = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    16, SysConst.PACKAGESPILTER))).intValue();
            PayYears = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 17, SysConst.PACKAGESPILTER))).intValue();
            PayIntv = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 18, SysConst.PACKAGESPILTER))).intValue();
            StandPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 19, SysConst.PACKAGESPILTER))).doubleValue();
            Amnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    20, SysConst.PACKAGESPILTER))).doubleValue();
            InvalidDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 21, SysConst.PACKAGESPILTER));
            FreeStartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 22, SysConst.PACKAGESPILTER));
            DeadDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 23, SysConst.PACKAGESPILTER));
            SouldGetMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 24, SysConst.PACKAGESPILTER))).doubleValue();
            StopReason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,
                                        SysConst.PACKAGESPILTER);
            StopDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 26, SysConst.PACKAGESPILTER));
            RevGetAge = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 27, SysConst.PACKAGESPILTER))).intValue();
            RevGetDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 28, SysConst.PACKAGESPILTER));
            RevGetMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 29, SysConst.PACKAGESPILTER))).doubleValue();
            RevGetIntv = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 30, SysConst.PACKAGESPILTER))).intValue();
            AnnMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 31, SysConst.PACKAGESPILTER))).doubleValue();
            DeathMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 32, SysConst.PACKAGESPILTER))).doubleValue();
            MedMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 33, SysConst.PACKAGESPILTER))).doubleValue();
            ExpMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 34, SysConst.PACKAGESPILTER))).doubleValue();
            TotalMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 35, SysConst.PACKAGESPILTER))).doubleValue();
            OccPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 36, SysConst.PACKAGESPILTER))).doubleValue();
            HealthPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 37, SysConst.PACKAGESPILTER))).doubleValue();
            LastPayPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 38, SysConst.PACKAGESPILTER))).doubleValue();
            YearIdx = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 39, SysConst.PACKAGESPILTER))).intValue();
            CalYear = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 40, SysConst.PACKAGESPILTER))).intValue();
            Rev3 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    41, SysConst.PACKAGESPILTER))).doubleValue();
            Rev1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    42, SysConst.PACKAGESPILTER))).doubleValue();
            Rev2 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    43, SysConst.PACKAGESPILTER))).doubleValue();
            Rev4 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    44, SysConst.PACKAGESPILTER))).doubleValue();
            CheckFlag = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 45, SysConst.PACKAGESPILTER))).intValue();
            CalFlag = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 46, SysConst.PACKAGESPILTER))).intValue();
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 47, SysConst.PACKAGESPILTER));
            SignDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 48, SysConst.PACKAGESPILTER));
            ThisPayPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 49, SysConst.PACKAGESPILTER))).doubleValue();
            PaytoDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 50, SysConst.PACKAGESPILTER));
            ManageFeeRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 51, SysConst.PACKAGESPILTER))).doubleValue();
            InsuYearFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 52,
                                          SysConst.PACKAGESPILTER);
            PayEndYearFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                            53, SysConst.PACKAGESPILTER);
            SurMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 54, SysConst.PACKAGESPILTER))).doubleValue();
            SmokingFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 55,
                                         SysConst.PACKAGESPILTER);
            OccupationType = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                            56, SysConst.PACKAGESPILTER);
            CustomGetpoldate = fDate.getDate(StrTool.getStr(StrTool.
                    GBKToUnicode(strMessage), 57, SysConst.PACKAGESPILTER));
            FloatRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 58, SysConst.PACKAGESPILTER))).doubleValue();
            Groupno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 59,
                                     SysConst.PACKAGESPILTER);
            Getmoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 60, SysConst.PACKAGESPILTER))).doubleValue();
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LOReserveSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ManageCom));
        }
        if (FCode.equals("PolNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PolNo));
        }
        if (FCode.equals("PolStatus"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PolStatus));
        }
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskCode));
        }
        if (FCode.equals("SaleChnl"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SaleChnl));
        }
        if (FCode.equals("InsuredNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(InsuredNo));
        }
        if (FCode.equals("InsuredSex"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(InsuredSex));
        }
        if (FCode.equals("InsuredBirthday"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getInsuredBirthday()));
        }
        if (FCode.equals("InsuredAppAge"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(InsuredAppAge));
        }
        if (FCode.equals("SecInsuredSex"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SecInsuredSex));
        }
        if (FCode.equals("SecBirthDay"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getSecBirthDay()));
        }
        if (FCode.equals("SecInsuredAge"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SecInsuredAge));
        }
        if (FCode.equals("CValiDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getCValiDate()));
        }
        if (FCode.equals("LastRevDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getLastRevDate()));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("Years"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Years));
        }
        if (FCode.equals("PayYears"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PayYears));
        }
        if (FCode.equals("PayIntv"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PayIntv));
        }
        if (FCode.equals("StandPrem"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(StandPrem));
        }
        if (FCode.equals("Amnt"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Amnt));
        }
        if (FCode.equals("InvalidDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getInvalidDate()));
        }
        if (FCode.equals("FreeStartDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getFreeStartDate()));
        }
        if (FCode.equals("DeadDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getDeadDate()));
        }
        if (FCode.equals("SouldGetMoney"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SouldGetMoney));
        }
        if (FCode.equals("StopReason"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(StopReason));
        }
        if (FCode.equals("StopDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getStopDate()));
        }
        if (FCode.equals("RevGetAge"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RevGetAge));
        }
        if (FCode.equals("RevGetDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getRevGetDate()));
        }
        if (FCode.equals("RevGetMoney"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RevGetMoney));
        }
        if (FCode.equals("RevGetIntv"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RevGetIntv));
        }
        if (FCode.equals("AnnMoney"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AnnMoney));
        }
        if (FCode.equals("DeathMoney"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DeathMoney));
        }
        if (FCode.equals("MedMoney"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MedMoney));
        }
        if (FCode.equals("ExpMoney"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ExpMoney));
        }
        if (FCode.equals("TotalMoney"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(TotalMoney));
        }
        if (FCode.equals("OccPrem"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(OccPrem));
        }
        if (FCode.equals("HealthPrem"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(HealthPrem));
        }
        if (FCode.equals("LastPayPrem"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(LastPayPrem));
        }
        if (FCode.equals("YearIdx"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(YearIdx));
        }
        if (FCode.equals("CalYear"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CalYear));
        }
        if (FCode.equals("Rev3"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Rev3));
        }
        if (FCode.equals("Rev1"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Rev1));
        }
        if (FCode.equals("Rev2"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Rev2));
        }
        if (FCode.equals("Rev4"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Rev4));
        }
        if (FCode.equals("CheckFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CheckFlag));
        }
        if (FCode.equals("CalFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CalFlag));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("SignDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getSignDate()));
        }
        if (FCode.equals("ThisPayPrem"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ThisPayPrem));
        }
        if (FCode.equals("PaytoDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getPaytoDate()));
        }
        if (FCode.equals("ManageFeeRate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ManageFeeRate));
        }
        if (FCode.equals("InsuYearFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(InsuYearFlag));
        }
        if (FCode.equals("PayEndYearFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PayEndYearFlag));
        }
        if (FCode.equals("SurMoney"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SurMoney));
        }
        if (FCode.equals("SmokingFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SmokingFlag));
        }
        if (FCode.equals("OccupationType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(OccupationType));
        }
        if (FCode.equals("CustomGetpoldate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getCustomGetpoldate()));
        }
        if (FCode.equals("FloatRate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(FloatRate));
        }
        if (FCode.equals("Groupno"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Groupno));
        }
        if (FCode.equals("Getmoney"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Getmoney));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(PolNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(PolStatus);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(SaleChnl);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(InsuredNo);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(InsuredSex);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getInsuredBirthday()));
                break;
            case 8:
                strFieldValue = String.valueOf(InsuredAppAge);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(SecInsuredSex);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getSecBirthDay()));
                break;
            case 11:
                strFieldValue = String.valueOf(SecInsuredAge);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getCValiDate()));
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getLastRevDate()));
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 15:
                strFieldValue = String.valueOf(Years);
                break;
            case 16:
                strFieldValue = String.valueOf(PayYears);
                break;
            case 17:
                strFieldValue = String.valueOf(PayIntv);
                break;
            case 18:
                strFieldValue = String.valueOf(StandPrem);
                break;
            case 19:
                strFieldValue = String.valueOf(Amnt);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getInvalidDate()));
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getFreeStartDate()));
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getDeadDate()));
                break;
            case 23:
                strFieldValue = String.valueOf(SouldGetMoney);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(StopReason);
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getStopDate()));
                break;
            case 26:
                strFieldValue = String.valueOf(RevGetAge);
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getRevGetDate()));
                break;
            case 28:
                strFieldValue = String.valueOf(RevGetMoney);
                break;
            case 29:
                strFieldValue = String.valueOf(RevGetIntv);
                break;
            case 30:
                strFieldValue = String.valueOf(AnnMoney);
                break;
            case 31:
                strFieldValue = String.valueOf(DeathMoney);
                break;
            case 32:
                strFieldValue = String.valueOf(MedMoney);
                break;
            case 33:
                strFieldValue = String.valueOf(ExpMoney);
                break;
            case 34:
                strFieldValue = String.valueOf(TotalMoney);
                break;
            case 35:
                strFieldValue = String.valueOf(OccPrem);
                break;
            case 36:
                strFieldValue = String.valueOf(HealthPrem);
                break;
            case 37:
                strFieldValue = String.valueOf(LastPayPrem);
                break;
            case 38:
                strFieldValue = String.valueOf(YearIdx);
                break;
            case 39:
                strFieldValue = String.valueOf(CalYear);
                break;
            case 40:
                strFieldValue = String.valueOf(Rev3);
                break;
            case 41:
                strFieldValue = String.valueOf(Rev1);
                break;
            case 42:
                strFieldValue = String.valueOf(Rev2);
                break;
            case 43:
                strFieldValue = String.valueOf(Rev4);
                break;
            case 44:
                strFieldValue = String.valueOf(CheckFlag);
                break;
            case 45:
                strFieldValue = String.valueOf(CalFlag);
                break;
            case 46:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 47:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getSignDate()));
                break;
            case 48:
                strFieldValue = String.valueOf(ThisPayPrem);
                break;
            case 49:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getPaytoDate()));
                break;
            case 50:
                strFieldValue = String.valueOf(ManageFeeRate);
                break;
            case 51:
                strFieldValue = StrTool.GBKToUnicode(InsuYearFlag);
                break;
            case 52:
                strFieldValue = StrTool.GBKToUnicode(PayEndYearFlag);
                break;
            case 53:
                strFieldValue = String.valueOf(SurMoney);
                break;
            case 54:
                strFieldValue = StrTool.GBKToUnicode(SmokingFlag);
                break;
            case 55:
                strFieldValue = StrTool.GBKToUnicode(OccupationType);
                break;
            case 56:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getCustomGetpoldate()));
                break;
            case 57:
                strFieldValue = String.valueOf(FloatRate);
                break;
            case 58:
                strFieldValue = StrTool.GBKToUnicode(Groupno);
                break;
            case 59:
                strFieldValue = String.valueOf(Getmoney);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("PolNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
            {
                PolNo = null;
            }
        }
        if (FCode.equals("PolStatus"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PolStatus = FValue.trim();
            }
            else
            {
                PolStatus = null;
            }
        }
        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("SaleChnl"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SaleChnl = FValue.trim();
            }
            else
            {
                SaleChnl = null;
            }
        }
        if (FCode.equals("InsuredNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InsuredNo = FValue.trim();
            }
            else
            {
                InsuredNo = null;
            }
        }
        if (FCode.equals("InsuredSex"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InsuredSex = FValue.trim();
            }
            else
            {
                InsuredSex = null;
            }
        }
        if (FCode.equals("InsuredBirthday"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InsuredBirthday = fDate.getDate(FValue);
            }
            else
            {
                InsuredBirthday = null;
            }
        }
        if (FCode.equals("InsuredAppAge"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                InsuredAppAge = i;
            }
        }
        if (FCode.equals("SecInsuredSex"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SecInsuredSex = FValue.trim();
            }
            else
            {
                SecInsuredSex = null;
            }
        }
        if (FCode.equals("SecBirthDay"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SecBirthDay = fDate.getDate(FValue);
            }
            else
            {
                SecBirthDay = null;
            }
        }
        if (FCode.equals("SecInsuredAge"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                SecInsuredAge = i;
            }
        }
        if (FCode.equals("CValiDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CValiDate = fDate.getDate(FValue);
            }
            else
            {
                CValiDate = null;
            }
        }
        if (FCode.equals("LastRevDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                LastRevDate = fDate.getDate(FValue);
            }
            else
            {
                LastRevDate = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("Years"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                Years = i;
            }
        }
        if (FCode.equals("PayYears"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                PayYears = i;
            }
        }
        if (FCode.equals("PayIntv"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                PayIntv = i;
            }
        }
        if (FCode.equals("StandPrem"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                StandPrem = d;
            }
        }
        if (FCode.equals("Amnt"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Amnt = d;
            }
        }
        if (FCode.equals("InvalidDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InvalidDate = fDate.getDate(FValue);
            }
            else
            {
                InvalidDate = null;
            }
        }
        if (FCode.equals("FreeStartDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                FreeStartDate = fDate.getDate(FValue);
            }
            else
            {
                FreeStartDate = null;
            }
        }
        if (FCode.equals("DeadDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DeadDate = fDate.getDate(FValue);
            }
            else
            {
                DeadDate = null;
            }
        }
        if (FCode.equals("SouldGetMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                SouldGetMoney = d;
            }
        }
        if (FCode.equals("StopReason"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                StopReason = FValue.trim();
            }
            else
            {
                StopReason = null;
            }
        }
        if (FCode.equals("StopDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                StopDate = fDate.getDate(FValue);
            }
            else
            {
                StopDate = null;
            }
        }
        if (FCode.equals("RevGetAge"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                RevGetAge = i;
            }
        }
        if (FCode.equals("RevGetDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RevGetDate = fDate.getDate(FValue);
            }
            else
            {
                RevGetDate = null;
            }
        }
        if (FCode.equals("RevGetMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                RevGetMoney = d;
            }
        }
        if (FCode.equals("RevGetIntv"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                RevGetIntv = i;
            }
        }
        if (FCode.equals("AnnMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                AnnMoney = d;
            }
        }
        if (FCode.equals("DeathMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                DeathMoney = d;
            }
        }
        if (FCode.equals("MedMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                MedMoney = d;
            }
        }
        if (FCode.equals("ExpMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                ExpMoney = d;
            }
        }
        if (FCode.equals("TotalMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                TotalMoney = d;
            }
        }
        if (FCode.equals("OccPrem"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                OccPrem = d;
            }
        }
        if (FCode.equals("HealthPrem"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                HealthPrem = d;
            }
        }
        if (FCode.equals("LastPayPrem"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                LastPayPrem = d;
            }
        }
        if (FCode.equals("YearIdx"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                YearIdx = i;
            }
        }
        if (FCode.equals("CalYear"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                CalYear = i;
            }
        }
        if (FCode.equals("Rev3"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Rev3 = d;
            }
        }
        if (FCode.equals("Rev1"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Rev1 = d;
            }
        }
        if (FCode.equals("Rev2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Rev2 = d;
            }
        }
        if (FCode.equals("Rev4"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Rev4 = d;
            }
        }
        if (FCode.equals("CheckFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                CheckFlag = i;
            }
        }
        if (FCode.equals("CalFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                CalFlag = i;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("SignDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SignDate = fDate.getDate(FValue);
            }
            else
            {
                SignDate = null;
            }
        }
        if (FCode.equals("ThisPayPrem"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                ThisPayPrem = d;
            }
        }
        if (FCode.equals("PaytoDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PaytoDate = fDate.getDate(FValue);
            }
            else
            {
                PaytoDate = null;
            }
        }
        if (FCode.equals("ManageFeeRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                ManageFeeRate = d;
            }
        }
        if (FCode.equals("InsuYearFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InsuYearFlag = FValue.trim();
            }
            else
            {
                InsuYearFlag = null;
            }
        }
        if (FCode.equals("PayEndYearFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PayEndYearFlag = FValue.trim();
            }
            else
            {
                PayEndYearFlag = null;
            }
        }
        if (FCode.equals("SurMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                SurMoney = d;
            }
        }
        if (FCode.equals("SmokingFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SmokingFlag = FValue.trim();
            }
            else
            {
                SmokingFlag = null;
            }
        }
        if (FCode.equals("OccupationType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OccupationType = FValue.trim();
            }
            else
            {
                OccupationType = null;
            }
        }
        if (FCode.equals("CustomGetpoldate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CustomGetpoldate = fDate.getDate(FValue);
            }
            else
            {
                CustomGetpoldate = null;
            }
        }
        if (FCode.equals("FloatRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                FloatRate = d;
            }
        }
        if (FCode.equals("Groupno"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Groupno = FValue.trim();
            }
            else
            {
                Groupno = null;
            }
        }
        if (FCode.equals("Getmoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Getmoney = d;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LOReserveSchema other = (LOReserveSchema) otherObject;
        return
                ManageCom.equals(other.getManageCom())
                && PolNo.equals(other.getPolNo())
                && PolStatus.equals(other.getPolStatus())
                && RiskCode.equals(other.getRiskCode())
                && SaleChnl.equals(other.getSaleChnl())
                && InsuredNo.equals(other.getInsuredNo())
                && InsuredSex.equals(other.getInsuredSex())
                &&
                fDate.getString(InsuredBirthday).equals(other.
                getInsuredBirthday())
                && InsuredAppAge == other.getInsuredAppAge()
                && SecInsuredSex.equals(other.getSecInsuredSex())
                && fDate.getString(SecBirthDay).equals(other.getSecBirthDay())
                && SecInsuredAge == other.getSecInsuredAge()
                && fDate.getString(CValiDate).equals(other.getCValiDate())
                && fDate.getString(LastRevDate).equals(other.getLastRevDate())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && Years == other.getYears()
                && PayYears == other.getPayYears()
                && PayIntv == other.getPayIntv()
                && StandPrem == other.getStandPrem()
                && Amnt == other.getAmnt()
                && fDate.getString(InvalidDate).equals(other.getInvalidDate())
                && fDate.getString(FreeStartDate).equals(other.getFreeStartDate())
                && fDate.getString(DeadDate).equals(other.getDeadDate())
                && SouldGetMoney == other.getSouldGetMoney()
                && StopReason.equals(other.getStopReason())
                && fDate.getString(StopDate).equals(other.getStopDate())
                && RevGetAge == other.getRevGetAge()
                && fDate.getString(RevGetDate).equals(other.getRevGetDate())
                && RevGetMoney == other.getRevGetMoney()
                && RevGetIntv == other.getRevGetIntv()
                && AnnMoney == other.getAnnMoney()
                && DeathMoney == other.getDeathMoney()
                && MedMoney == other.getMedMoney()
                && ExpMoney == other.getExpMoney()
                && TotalMoney == other.getTotalMoney()
                && OccPrem == other.getOccPrem()
                && HealthPrem == other.getHealthPrem()
                && LastPayPrem == other.getLastPayPrem()
                && YearIdx == other.getYearIdx()
                && CalYear == other.getCalYear()
                && Rev3 == other.getRev3()
                && Rev1 == other.getRev1()
                && Rev2 == other.getRev2()
                && Rev4 == other.getRev4()
                && CheckFlag == other.getCheckFlag()
                && CalFlag == other.getCalFlag()
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && fDate.getString(SignDate).equals(other.getSignDate())
                && ThisPayPrem == other.getThisPayPrem()
                && fDate.getString(PaytoDate).equals(other.getPaytoDate())
                && ManageFeeRate == other.getManageFeeRate()
                && InsuYearFlag.equals(other.getInsuYearFlag())
                && PayEndYearFlag.equals(other.getPayEndYearFlag())
                && SurMoney == other.getSurMoney()
                && SmokingFlag.equals(other.getSmokingFlag())
                && OccupationType.equals(other.getOccupationType())
                &&
                fDate.getString(CustomGetpoldate).equals(other.
                getCustomGetpoldate())
                && FloatRate == other.getFloatRate()
                && Groupno.equals(other.getGroupno())
                && Getmoney == other.getGetmoney();
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("ManageCom"))
        {
            return 0;
        }
        if (strFieldName.equals("PolNo"))
        {
            return 1;
        }
        if (strFieldName.equals("PolStatus"))
        {
            return 2;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return 3;
        }
        if (strFieldName.equals("SaleChnl"))
        {
            return 4;
        }
        if (strFieldName.equals("InsuredNo"))
        {
            return 5;
        }
        if (strFieldName.equals("InsuredSex"))
        {
            return 6;
        }
        if (strFieldName.equals("InsuredBirthday"))
        {
            return 7;
        }
        if (strFieldName.equals("InsuredAppAge"))
        {
            return 8;
        }
        if (strFieldName.equals("SecInsuredSex"))
        {
            return 9;
        }
        if (strFieldName.equals("SecBirthDay"))
        {
            return 10;
        }
        if (strFieldName.equals("SecInsuredAge"))
        {
            return 11;
        }
        if (strFieldName.equals("CValiDate"))
        {
            return 12;
        }
        if (strFieldName.equals("LastRevDate"))
        {
            return 13;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 14;
        }
        if (strFieldName.equals("Years"))
        {
            return 15;
        }
        if (strFieldName.equals("PayYears"))
        {
            return 16;
        }
        if (strFieldName.equals("PayIntv"))
        {
            return 17;
        }
        if (strFieldName.equals("StandPrem"))
        {
            return 18;
        }
        if (strFieldName.equals("Amnt"))
        {
            return 19;
        }
        if (strFieldName.equals("InvalidDate"))
        {
            return 20;
        }
        if (strFieldName.equals("FreeStartDate"))
        {
            return 21;
        }
        if (strFieldName.equals("DeadDate"))
        {
            return 22;
        }
        if (strFieldName.equals("SouldGetMoney"))
        {
            return 23;
        }
        if (strFieldName.equals("StopReason"))
        {
            return 24;
        }
        if (strFieldName.equals("StopDate"))
        {
            return 25;
        }
        if (strFieldName.equals("RevGetAge"))
        {
            return 26;
        }
        if (strFieldName.equals("RevGetDate"))
        {
            return 27;
        }
        if (strFieldName.equals("RevGetMoney"))
        {
            return 28;
        }
        if (strFieldName.equals("RevGetIntv"))
        {
            return 29;
        }
        if (strFieldName.equals("AnnMoney"))
        {
            return 30;
        }
        if (strFieldName.equals("DeathMoney"))
        {
            return 31;
        }
        if (strFieldName.equals("MedMoney"))
        {
            return 32;
        }
        if (strFieldName.equals("ExpMoney"))
        {
            return 33;
        }
        if (strFieldName.equals("TotalMoney"))
        {
            return 34;
        }
        if (strFieldName.equals("OccPrem"))
        {
            return 35;
        }
        if (strFieldName.equals("HealthPrem"))
        {
            return 36;
        }
        if (strFieldName.equals("LastPayPrem"))
        {
            return 37;
        }
        if (strFieldName.equals("YearIdx"))
        {
            return 38;
        }
        if (strFieldName.equals("CalYear"))
        {
            return 39;
        }
        if (strFieldName.equals("Rev3"))
        {
            return 40;
        }
        if (strFieldName.equals("Rev1"))
        {
            return 41;
        }
        if (strFieldName.equals("Rev2"))
        {
            return 42;
        }
        if (strFieldName.equals("Rev4"))
        {
            return 43;
        }
        if (strFieldName.equals("CheckFlag"))
        {
            return 44;
        }
        if (strFieldName.equals("CalFlag"))
        {
            return 45;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 46;
        }
        if (strFieldName.equals("SignDate"))
        {
            return 47;
        }
        if (strFieldName.equals("ThisPayPrem"))
        {
            return 48;
        }
        if (strFieldName.equals("PaytoDate"))
        {
            return 49;
        }
        if (strFieldName.equals("ManageFeeRate"))
        {
            return 50;
        }
        if (strFieldName.equals("InsuYearFlag"))
        {
            return 51;
        }
        if (strFieldName.equals("PayEndYearFlag"))
        {
            return 52;
        }
        if (strFieldName.equals("SurMoney"))
        {
            return 53;
        }
        if (strFieldName.equals("SmokingFlag"))
        {
            return 54;
        }
        if (strFieldName.equals("OccupationType"))
        {
            return 55;
        }
        if (strFieldName.equals("CustomGetpoldate"))
        {
            return 56;
        }
        if (strFieldName.equals("FloatRate"))
        {
            return 57;
        }
        if (strFieldName.equals("Groupno"))
        {
            return 58;
        }
        if (strFieldName.equals("Getmoney"))
        {
            return 59;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "ManageCom";
                break;
            case 1:
                strFieldName = "PolNo";
                break;
            case 2:
                strFieldName = "PolStatus";
                break;
            case 3:
                strFieldName = "RiskCode";
                break;
            case 4:
                strFieldName = "SaleChnl";
                break;
            case 5:
                strFieldName = "InsuredNo";
                break;
            case 6:
                strFieldName = "InsuredSex";
                break;
            case 7:
                strFieldName = "InsuredBirthday";
                break;
            case 8:
                strFieldName = "InsuredAppAge";
                break;
            case 9:
                strFieldName = "SecInsuredSex";
                break;
            case 10:
                strFieldName = "SecBirthDay";
                break;
            case 11:
                strFieldName = "SecInsuredAge";
                break;
            case 12:
                strFieldName = "CValiDate";
                break;
            case 13:
                strFieldName = "LastRevDate";
                break;
            case 14:
                strFieldName = "MakeDate";
                break;
            case 15:
                strFieldName = "Years";
                break;
            case 16:
                strFieldName = "PayYears";
                break;
            case 17:
                strFieldName = "PayIntv";
                break;
            case 18:
                strFieldName = "StandPrem";
                break;
            case 19:
                strFieldName = "Amnt";
                break;
            case 20:
                strFieldName = "InvalidDate";
                break;
            case 21:
                strFieldName = "FreeStartDate";
                break;
            case 22:
                strFieldName = "DeadDate";
                break;
            case 23:
                strFieldName = "SouldGetMoney";
                break;
            case 24:
                strFieldName = "StopReason";
                break;
            case 25:
                strFieldName = "StopDate";
                break;
            case 26:
                strFieldName = "RevGetAge";
                break;
            case 27:
                strFieldName = "RevGetDate";
                break;
            case 28:
                strFieldName = "RevGetMoney";
                break;
            case 29:
                strFieldName = "RevGetIntv";
                break;
            case 30:
                strFieldName = "AnnMoney";
                break;
            case 31:
                strFieldName = "DeathMoney";
                break;
            case 32:
                strFieldName = "MedMoney";
                break;
            case 33:
                strFieldName = "ExpMoney";
                break;
            case 34:
                strFieldName = "TotalMoney";
                break;
            case 35:
                strFieldName = "OccPrem";
                break;
            case 36:
                strFieldName = "HealthPrem";
                break;
            case 37:
                strFieldName = "LastPayPrem";
                break;
            case 38:
                strFieldName = "YearIdx";
                break;
            case 39:
                strFieldName = "CalYear";
                break;
            case 40:
                strFieldName = "Rev3";
                break;
            case 41:
                strFieldName = "Rev1";
                break;
            case 42:
                strFieldName = "Rev2";
                break;
            case 43:
                strFieldName = "Rev4";
                break;
            case 44:
                strFieldName = "CheckFlag";
                break;
            case 45:
                strFieldName = "CalFlag";
                break;
            case 46:
                strFieldName = "ModifyDate";
                break;
            case 47:
                strFieldName = "SignDate";
                break;
            case 48:
                strFieldName = "ThisPayPrem";
                break;
            case 49:
                strFieldName = "PaytoDate";
                break;
            case 50:
                strFieldName = "ManageFeeRate";
                break;
            case 51:
                strFieldName = "InsuYearFlag";
                break;
            case 52:
                strFieldName = "PayEndYearFlag";
                break;
            case 53:
                strFieldName = "SurMoney";
                break;
            case 54:
                strFieldName = "SmokingFlag";
                break;
            case 55:
                strFieldName = "OccupationType";
                break;
            case 56:
                strFieldName = "CustomGetpoldate";
                break;
            case 57:
                strFieldName = "FloatRate";
                break;
            case 58:
                strFieldName = "Groupno";
                break;
            case 59:
                strFieldName = "Getmoney";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PolNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PolStatus"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SaleChnl"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsuredNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsuredSex"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsuredBirthday"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("InsuredAppAge"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("SecInsuredSex"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SecBirthDay"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("SecInsuredAge"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("CValiDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("LastRevDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("Years"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("PayYears"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("PayIntv"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("StandPrem"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Amnt"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("InvalidDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("FreeStartDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("DeadDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("SouldGetMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("StopReason"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StopDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("RevGetAge"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("RevGetDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("RevGetMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("RevGetIntv"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("AnnMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("DeathMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("MedMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("ExpMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("TotalMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("OccPrem"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("HealthPrem"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("LastPayPrem"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("YearIdx"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("CalYear"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("Rev3"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Rev1"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Rev2"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Rev4"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("CheckFlag"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("CalFlag"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("SignDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ThisPayPrem"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("PaytoDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ManageFeeRate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("InsuYearFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PayEndYearFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SurMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("SmokingFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OccupationType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CustomGetpoldate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("FloatRate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Groupno"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Getmoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 8:
                nFieldType = Schema.TYPE_INT;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 11:
                nFieldType = Schema.TYPE_INT;
                break;
            case 12:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 13:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 14:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 15:
                nFieldType = Schema.TYPE_INT;
                break;
            case 16:
                nFieldType = Schema.TYPE_INT;
                break;
            case 17:
                nFieldType = Schema.TYPE_INT;
                break;
            case 18:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 19:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 20:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 21:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 22:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 23:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 24:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 25:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 26:
                nFieldType = Schema.TYPE_INT;
                break;
            case 27:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 28:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 29:
                nFieldType = Schema.TYPE_INT;
                break;
            case 30:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 31:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 32:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 33:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 34:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 35:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 36:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 37:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 38:
                nFieldType = Schema.TYPE_INT;
                break;
            case 39:
                nFieldType = Schema.TYPE_INT;
                break;
            case 40:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 41:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 42:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 43:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 44:
                nFieldType = Schema.TYPE_INT;
                break;
            case 45:
                nFieldType = Schema.TYPE_INT;
                break;
            case 46:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 47:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 48:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 49:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 50:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 51:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 52:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 53:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 54:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 55:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 56:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 57:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 58:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 59:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
