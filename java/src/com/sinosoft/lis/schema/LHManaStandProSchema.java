/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LHManaStandProDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LHManaStandProSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PICCH-郭丽颖-表结构修改提交单-200609011
 * @CreateDate：2006-09-11
 */
public class LHManaStandProSchema implements Schema, Cloneable {
    // @Field
    /** 标准问题代码 */
    private String StandCode;
    /** 标准问题级别 */
    private String StandLevel;
    /** 标准问题内容 */
    private String StandContent;
    /** 问题所属问卷 */
    private String QuerAffiliated;
    /** 标准问题类型 */
    private String StandQuerType;
    /** 问题答案类型 */
    private String QuerResType;
    /** 答案附属信息 */
    private String ResuAffiInfo;
    /** 标准单位 */
    private String StandUnit;
    /** 问题说明 */
    private String QuesExplain;
    /** 问题状态 */
    private String QuesStauts;
    /** 管理机构 */
    private String ManageCom;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后修改日期 */
    private Date ModifyDate;
    /** 最后修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 16; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LHManaStandProSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "StandCode";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LHManaStandProSchema cloned = (LHManaStandProSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getStandCode() {
        return StandCode;
    }

    public void setStandCode(String aStandCode) {
        StandCode = aStandCode;
    }

    public String getStandLevel() {
        return StandLevel;
    }

    public void setStandLevel(String aStandLevel) {
        StandLevel = aStandLevel;
    }

    public String getStandContent() {
        return StandContent;
    }

    public void setStandContent(String aStandContent) {
        StandContent = aStandContent;
    }

    public String getQuerAffiliated() {
        return QuerAffiliated;
    }

    public void setQuerAffiliated(String aQuerAffiliated) {
        QuerAffiliated = aQuerAffiliated;
    }

    public String getStandQuerType() {
        return StandQuerType;
    }

    public void setStandQuerType(String aStandQuerType) {
        StandQuerType = aStandQuerType;
    }

    public String getQuerResType() {
        return QuerResType;
    }

    public void setQuerResType(String aQuerResType) {
        QuerResType = aQuerResType;
    }

    public String getResuAffiInfo() {
        return ResuAffiInfo;
    }

    public void setResuAffiInfo(String aResuAffiInfo) {
        ResuAffiInfo = aResuAffiInfo;
    }

    public String getStandUnit() {
        return StandUnit;
    }

    public void setStandUnit(String aStandUnit) {
        StandUnit = aStandUnit;
    }

    public String getQuesExplain() {
        return QuesExplain;
    }

    public void setQuesExplain(String aQuesExplain) {
        QuesExplain = aQuesExplain;
    }

    public String getQuesStauts() {
        return QuesStauts;
    }

    public void setQuesStauts(String aQuesStauts) {
        QuesStauts = aQuesStauts;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LHManaStandProSchema 对象给 Schema 赋值
     * @param: aLHManaStandProSchema LHManaStandProSchema
     **/
    public void setSchema(LHManaStandProSchema aLHManaStandProSchema) {
        this.StandCode = aLHManaStandProSchema.getStandCode();
        this.StandLevel = aLHManaStandProSchema.getStandLevel();
        this.StandContent = aLHManaStandProSchema.getStandContent();
        this.QuerAffiliated = aLHManaStandProSchema.getQuerAffiliated();
        this.StandQuerType = aLHManaStandProSchema.getStandQuerType();
        this.QuerResType = aLHManaStandProSchema.getQuerResType();
        this.ResuAffiInfo = aLHManaStandProSchema.getResuAffiInfo();
        this.StandUnit = aLHManaStandProSchema.getStandUnit();
        this.QuesExplain = aLHManaStandProSchema.getQuesExplain();
        this.QuesStauts = aLHManaStandProSchema.getQuesStauts();
        this.ManageCom = aLHManaStandProSchema.getManageCom();
        this.Operator = aLHManaStandProSchema.getOperator();
        this.MakeDate = fDate.getDate(aLHManaStandProSchema.getMakeDate());
        this.MakeTime = aLHManaStandProSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLHManaStandProSchema.getModifyDate());
        this.ModifyTime = aLHManaStandProSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("StandCode") == null) {
                this.StandCode = null;
            } else {
                this.StandCode = rs.getString("StandCode").trim();
            }

            if (rs.getString("StandLevel") == null) {
                this.StandLevel = null;
            } else {
                this.StandLevel = rs.getString("StandLevel").trim();
            }

            if (rs.getString("StandContent") == null) {
                this.StandContent = null;
            } else {
                this.StandContent = rs.getString("StandContent").trim();
            }

            if (rs.getString("QuerAffiliated") == null) {
                this.QuerAffiliated = null;
            } else {
                this.QuerAffiliated = rs.getString("QuerAffiliated").trim();
            }

            if (rs.getString("StandQuerType") == null) {
                this.StandQuerType = null;
            } else {
                this.StandQuerType = rs.getString("StandQuerType").trim();
            }

            if (rs.getString("QuerResType") == null) {
                this.QuerResType = null;
            } else {
                this.QuerResType = rs.getString("QuerResType").trim();
            }

            if (rs.getString("ResuAffiInfo") == null) {
                this.ResuAffiInfo = null;
            } else {
                this.ResuAffiInfo = rs.getString("ResuAffiInfo").trim();
            }

            if (rs.getString("StandUnit") == null) {
                this.StandUnit = null;
            } else {
                this.StandUnit = rs.getString("StandUnit").trim();
            }

            if (rs.getString("QuesExplain") == null) {
                this.QuesExplain = null;
            } else {
                this.QuesExplain = rs.getString("QuesExplain").trim();
            }

            if (rs.getString("QuesStauts") == null) {
                this.QuesStauts = null;
            } else {
                this.QuesStauts = rs.getString("QuesStauts").trim();
            }

            if (rs.getString("ManageCom") == null) {
                this.ManageCom = null;
            } else {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LHManaStandPro表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHManaStandProSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LHManaStandProSchema getSchema() {
        LHManaStandProSchema aLHManaStandProSchema = new LHManaStandProSchema();
        aLHManaStandProSchema.setSchema(this);
        return aLHManaStandProSchema;
    }

    public LHManaStandProDB getDB() {
        LHManaStandProDB aDBOper = new LHManaStandProDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHManaStandPro描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(StandCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandLevel));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandContent));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(QuerAffiliated));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandQuerType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(QuerResType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ResuAffiInfo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandUnit));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(QuesExplain));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(QuesStauts));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHManaStandPro>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            StandCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            StandLevel = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
            StandContent = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                          SysConst.PACKAGESPILTER);
            QuerAffiliated = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                            SysConst.PACKAGESPILTER);
            StandQuerType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                           SysConst.PACKAGESPILTER);
            QuerResType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                         SysConst.PACKAGESPILTER);
            ResuAffiInfo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                          SysConst.PACKAGESPILTER);
            StandUnit = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                       SysConst.PACKAGESPILTER);
            QuesExplain = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                         SysConst.PACKAGESPILTER);
            QuesStauts = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                        SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                       SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 13, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 15, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                        SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHManaStandProSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("StandCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandCode));
        }
        if (FCode.equals("StandLevel")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandLevel));
        }
        if (FCode.equals("StandContent")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandContent));
        }
        if (FCode.equals("QuerAffiliated")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(QuerAffiliated));
        }
        if (FCode.equals("StandQuerType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandQuerType));
        }
        if (FCode.equals("QuerResType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(QuerResType));
        }
        if (FCode.equals("ResuAffiInfo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ResuAffiInfo));
        }
        if (FCode.equals("StandUnit")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandUnit));
        }
        if (FCode.equals("QuesExplain")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(QuesExplain));
        }
        if (FCode.equals("QuesStauts")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(QuesStauts));
        }
        if (FCode.equals("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(StandCode);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(StandLevel);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(StandContent);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(QuerAffiliated);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(StandQuerType);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(QuerResType);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(ResuAffiInfo);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(StandUnit);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(QuesExplain);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(QuesStauts);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(ManageCom);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 15:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("StandCode")) {
            if (FValue != null && !FValue.equals("")) {
                StandCode = FValue.trim();
            } else {
                StandCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("StandLevel")) {
            if (FValue != null && !FValue.equals("")) {
                StandLevel = FValue.trim();
            } else {
                StandLevel = null;
            }
        }
        if (FCode.equalsIgnoreCase("StandContent")) {
            if (FValue != null && !FValue.equals("")) {
                StandContent = FValue.trim();
            } else {
                StandContent = null;
            }
        }
        if (FCode.equalsIgnoreCase("QuerAffiliated")) {
            if (FValue != null && !FValue.equals("")) {
                QuerAffiliated = FValue.trim();
            } else {
                QuerAffiliated = null;
            }
        }
        if (FCode.equalsIgnoreCase("StandQuerType")) {
            if (FValue != null && !FValue.equals("")) {
                StandQuerType = FValue.trim();
            } else {
                StandQuerType = null;
            }
        }
        if (FCode.equalsIgnoreCase("QuerResType")) {
            if (FValue != null && !FValue.equals("")) {
                QuerResType = FValue.trim();
            } else {
                QuerResType = null;
            }
        }
        if (FCode.equalsIgnoreCase("ResuAffiInfo")) {
            if (FValue != null && !FValue.equals("")) {
                ResuAffiInfo = FValue.trim();
            } else {
                ResuAffiInfo = null;
            }
        }
        if (FCode.equalsIgnoreCase("StandUnit")) {
            if (FValue != null && !FValue.equals("")) {
                StandUnit = FValue.trim();
            } else {
                StandUnit = null;
            }
        }
        if (FCode.equalsIgnoreCase("QuesExplain")) {
            if (FValue != null && !FValue.equals("")) {
                QuesExplain = FValue.trim();
            } else {
                QuesExplain = null;
            }
        }
        if (FCode.equalsIgnoreCase("QuesStauts")) {
            if (FValue != null && !FValue.equals("")) {
                QuesStauts = FValue.trim();
            } else {
                QuesStauts = null;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ManageCom = FValue.trim();
            } else {
                ManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LHManaStandProSchema other = (LHManaStandProSchema) otherObject;
        return
                StandCode.equals(other.getStandCode())
                && StandLevel.equals(other.getStandLevel())
                && StandContent.equals(other.getStandContent())
                && QuerAffiliated.equals(other.getQuerAffiliated())
                && StandQuerType.equals(other.getStandQuerType())
                && QuerResType.equals(other.getQuerResType())
                && ResuAffiInfo.equals(other.getResuAffiInfo())
                && StandUnit.equals(other.getStandUnit())
                && QuesExplain.equals(other.getQuesExplain())
                && QuesStauts.equals(other.getQuesStauts())
                && ManageCom.equals(other.getManageCom())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("StandCode")) {
            return 0;
        }
        if (strFieldName.equals("StandLevel")) {
            return 1;
        }
        if (strFieldName.equals("StandContent")) {
            return 2;
        }
        if (strFieldName.equals("QuerAffiliated")) {
            return 3;
        }
        if (strFieldName.equals("StandQuerType")) {
            return 4;
        }
        if (strFieldName.equals("QuerResType")) {
            return 5;
        }
        if (strFieldName.equals("ResuAffiInfo")) {
            return 6;
        }
        if (strFieldName.equals("StandUnit")) {
            return 7;
        }
        if (strFieldName.equals("QuesExplain")) {
            return 8;
        }
        if (strFieldName.equals("QuesStauts")) {
            return 9;
        }
        if (strFieldName.equals("ManageCom")) {
            return 10;
        }
        if (strFieldName.equals("Operator")) {
            return 11;
        }
        if (strFieldName.equals("MakeDate")) {
            return 12;
        }
        if (strFieldName.equals("MakeTime")) {
            return 13;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 14;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 15;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "StandCode";
            break;
        case 1:
            strFieldName = "StandLevel";
            break;
        case 2:
            strFieldName = "StandContent";
            break;
        case 3:
            strFieldName = "QuerAffiliated";
            break;
        case 4:
            strFieldName = "StandQuerType";
            break;
        case 5:
            strFieldName = "QuerResType";
            break;
        case 6:
            strFieldName = "ResuAffiInfo";
            break;
        case 7:
            strFieldName = "StandUnit";
            break;
        case 8:
            strFieldName = "QuesExplain";
            break;
        case 9:
            strFieldName = "QuesStauts";
            break;
        case 10:
            strFieldName = "ManageCom";
            break;
        case 11:
            strFieldName = "Operator";
            break;
        case 12:
            strFieldName = "MakeDate";
            break;
        case 13:
            strFieldName = "MakeTime";
            break;
        case 14:
            strFieldName = "ModifyDate";
            break;
        case 15:
            strFieldName = "ModifyTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("StandCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StandLevel")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StandContent")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("QuerAffiliated")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StandQuerType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("QuerResType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ResuAffiInfo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StandUnit")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("QuesExplain")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("QuesStauts")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 12:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 13:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 14:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 15:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
