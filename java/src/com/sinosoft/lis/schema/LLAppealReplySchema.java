/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LLAppealReplyDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

/*
 * <p>ClassName: LLAppealReplySchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-02-28
 */
public class LLAppealReplySchema implements Schema
{
    // @Field
    /** 回复人 */
    private String ReplyerNo;
    /** 回复时间 */
    private Date ReplyDate;
    /** 管理机构 */
    private String MngCom;
    /** 处理结论 */
    private String DealResult;
    /** 处理依据 */
    private String DealReson;

    public static final int FIELDNUM = 5; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LLAppealReplySchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[0];

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getReplyerNo()
    {
        if (ReplyerNo != null && !ReplyerNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ReplyerNo = StrTool.unicodeToGBK(ReplyerNo);
        }
        return ReplyerNo;
    }

    public void setReplyerNo(String aReplyerNo)
    {
        ReplyerNo = aReplyerNo;
    }

    public String getReplyDate()
    {
        if (ReplyDate != null)
        {
            return fDate.getString(ReplyDate);
        }
        else
        {
            return null;
        }
    }

    public void setReplyDate(Date aReplyDate)
    {
        ReplyDate = aReplyDate;
    }

    public void setReplyDate(String aReplyDate)
    {
        if (aReplyDate != null && !aReplyDate.equals(""))
        {
            ReplyDate = fDate.getDate(aReplyDate);
        }
        else
        {
            ReplyDate = null;
        }
    }

    public String getMngCom()
    {
        if (MngCom != null && !MngCom.equals("") && SysConst.CHANGECHARSET == true)
        {
            MngCom = StrTool.unicodeToGBK(MngCom);
        }
        return MngCom;
    }

    public void setMngCom(String aMngCom)
    {
        MngCom = aMngCom;
    }

    public String getDealResult()
    {
        if (DealResult != null && !DealResult.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            DealResult = StrTool.unicodeToGBK(DealResult);
        }
        return DealResult;
    }

    public void setDealResult(String aDealResult)
    {
        DealResult = aDealResult;
    }

    public String getDealReson()
    {
        if (DealReson != null && !DealReson.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            DealReson = StrTool.unicodeToGBK(DealReson);
        }
        return DealReson;
    }

    public void setDealReson(String aDealReson)
    {
        DealReson = aDealReson;
    }

    /**
     * 使用另外一个 LLAppealReplySchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LLAppealReplySchema aLLAppealReplySchema)
    {
        this.ReplyerNo = aLLAppealReplySchema.getReplyerNo();
        this.ReplyDate = fDate.getDate(aLLAppealReplySchema.getReplyDate());
        this.MngCom = aLLAppealReplySchema.getMngCom();
        this.DealResult = aLLAppealReplySchema.getDealResult();
        this.DealReson = aLLAppealReplySchema.getDealReson();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ReplyerNo") == null)
            {
                this.ReplyerNo = null;
            }
            else
            {
                this.ReplyerNo = rs.getString("ReplyerNo").trim();
            }

            this.ReplyDate = rs.getDate("ReplyDate");
            if (rs.getString("MngCom") == null)
            {
                this.MngCom = null;
            }
            else
            {
                this.MngCom = rs.getString("MngCom").trim();
            }

            if (rs.getString("DealResult") == null)
            {
                this.DealResult = null;
            }
            else
            {
                this.DealResult = rs.getString("DealResult").trim();
            }

            if (rs.getString("DealReson") == null)
            {
                this.DealReson = null;
            }
            else
            {
                this.DealReson = rs.getString("DealReson").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLAppealReplySchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LLAppealReplySchema getSchema()
    {
        LLAppealReplySchema aLLAppealReplySchema = new LLAppealReplySchema();
        aLLAppealReplySchema.setSchema(this);
        return aLLAppealReplySchema;
    }

    public LLAppealReplyDB getDB()
    {
        LLAppealReplyDB aDBOper = new LLAppealReplyDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLAppealReply描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(ReplyerNo)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ReplyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MngCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(DealResult)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(DealReson));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLAppealReply>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            ReplyerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            ReplyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 2, SysConst.PACKAGESPILTER));
            MngCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                    SysConst.PACKAGESPILTER);
            DealResult = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                        SysConst.PACKAGESPILTER);
            DealReson = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                       SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLAppealReplySchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("ReplyerNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ReplyerNo));
        }
        if (FCode.equals("ReplyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getReplyDate()));
        }
        if (FCode.equals("MngCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MngCom));
        }
        if (FCode.equals("DealResult"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DealResult));
        }
        if (FCode.equals("DealReson"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DealReson));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ReplyerNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getReplyDate()));
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(MngCom);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(DealResult);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(DealReson);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("ReplyerNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ReplyerNo = FValue.trim();
            }
            else
            {
                ReplyerNo = null;
            }
        }
        if (FCode.equals("ReplyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ReplyDate = fDate.getDate(FValue);
            }
            else
            {
                ReplyDate = null;
            }
        }
        if (FCode.equals("MngCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MngCom = FValue.trim();
            }
            else
            {
                MngCom = null;
            }
        }
        if (FCode.equals("DealResult"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DealResult = FValue.trim();
            }
            else
            {
                DealResult = null;
            }
        }
        if (FCode.equals("DealReson"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DealReson = FValue.trim();
            }
            else
            {
                DealReson = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LLAppealReplySchema other = (LLAppealReplySchema) otherObject;
        return
                ReplyerNo.equals(other.getReplyerNo())
                && fDate.getString(ReplyDate).equals(other.getReplyDate())
                && MngCom.equals(other.getMngCom())
                && DealResult.equals(other.getDealResult())
                && DealReson.equals(other.getDealReson());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("ReplyerNo"))
        {
            return 0;
        }
        if (strFieldName.equals("ReplyDate"))
        {
            return 1;
        }
        if (strFieldName.equals("MngCom"))
        {
            return 2;
        }
        if (strFieldName.equals("DealResult"))
        {
            return 3;
        }
        if (strFieldName.equals("DealReson"))
        {
            return 4;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "ReplyerNo";
                break;
            case 1:
                strFieldName = "ReplyDate";
                break;
            case 2:
                strFieldName = "MngCom";
                break;
            case 3:
                strFieldName = "DealResult";
                break;
            case 4:
                strFieldName = "DealReson";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("ReplyerNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ReplyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MngCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DealResult"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DealReson"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
