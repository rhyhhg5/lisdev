/*
 * <p>ClassName: LMEdorZTAccSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LMEdorZTAccDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LMEdorZTAccSchema implements Schema
{
    // @Field
    /** 险种编码 */
    private String RiskCode;
    /** 保险帐户号码 */
    private String InsuAccNo;
    /** 算法编码 */
    private String CalCode;
    /** 计算方式参考 */
    private String CalCodeType;

    public static final int FIELDNUM = 4; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LMEdorZTAccSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "RiskCode";
        pk[1] = "InsuAccNo";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getRiskCode()
    {
        if (RiskCode != null && !RiskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getInsuAccNo()
    {
        if (InsuAccNo != null && !InsuAccNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            InsuAccNo = StrTool.unicodeToGBK(InsuAccNo);
        }
        return InsuAccNo;
    }

    public void setInsuAccNo(String aInsuAccNo)
    {
        InsuAccNo = aInsuAccNo;
    }

    public String getCalCode()
    {
        if (CalCode != null && !CalCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            CalCode = StrTool.unicodeToGBK(CalCode);
        }
        return CalCode;
    }

    public void setCalCode(String aCalCode)
    {
        CalCode = aCalCode;
    }

    public String getCalCodeType()
    {
        if (CalCodeType != null && !CalCodeType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CalCodeType = StrTool.unicodeToGBK(CalCodeType);
        }
        return CalCodeType;
    }

    public void setCalCodeType(String aCalCodeType)
    {
        CalCodeType = aCalCodeType;
    }

    /**
     * 使用另外一个 LMEdorZTAccSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LMEdorZTAccSchema aLMEdorZTAccSchema)
    {
        this.RiskCode = aLMEdorZTAccSchema.getRiskCode();
        this.InsuAccNo = aLMEdorZTAccSchema.getInsuAccNo();
        this.CalCode = aLMEdorZTAccSchema.getCalCode();
        this.CalCodeType = aLMEdorZTAccSchema.getCalCodeType();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("InsuAccNo") == null)
            {
                this.InsuAccNo = null;
            }
            else
            {
                this.InsuAccNo = rs.getString("InsuAccNo").trim();
            }

            if (rs.getString("CalCode") == null)
            {
                this.CalCode = null;
            }
            else
            {
                this.CalCode = rs.getString("CalCode").trim();
            }

            if (rs.getString("CalCodeType") == null)
            {
                this.CalCodeType = null;
            }
            else
            {
                this.CalCodeType = rs.getString("CalCodeType").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMEdorZTAccSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LMEdorZTAccSchema getSchema()
    {
        LMEdorZTAccSchema aLMEdorZTAccSchema = new LMEdorZTAccSchema();
        aLMEdorZTAccSchema.setSchema(this);
        return aLMEdorZTAccSchema;
    }

    public LMEdorZTAccDB getDB()
    {
        LMEdorZTAccDB aDBOper = new LMEdorZTAccDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMEdorZTAcc描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InsuAccNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CalCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CalCodeType));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMEdorZTAcc>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            InsuAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                       SysConst.PACKAGESPILTER);
            CalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                     SysConst.PACKAGESPILTER);
            CalCodeType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                         SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMEdorZTAccSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskCode));
        }
        if (FCode.equals("InsuAccNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(InsuAccNo));
        }
        if (FCode.equals("CalCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CalCode));
        }
        if (FCode.equals("CalCodeType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CalCodeType));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(InsuAccNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(CalCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(CalCodeType);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("InsuAccNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InsuAccNo = FValue.trim();
            }
            else
            {
                InsuAccNo = null;
            }
        }
        if (FCode.equals("CalCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CalCode = FValue.trim();
            }
            else
            {
                CalCode = null;
            }
        }
        if (FCode.equals("CalCodeType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CalCodeType = FValue.trim();
            }
            else
            {
                CalCodeType = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LMEdorZTAccSchema other = (LMEdorZTAccSchema) otherObject;
        return
                RiskCode.equals(other.getRiskCode())
                && InsuAccNo.equals(other.getInsuAccNo())
                && CalCode.equals(other.getCalCode())
                && CalCodeType.equals(other.getCalCodeType());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("RiskCode"))
        {
            return 0;
        }
        if (strFieldName.equals("InsuAccNo"))
        {
            return 1;
        }
        if (strFieldName.equals("CalCode"))
        {
            return 2;
        }
        if (strFieldName.equals("CalCodeType"))
        {
            return 3;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "InsuAccNo";
                break;
            case 2:
                strFieldName = "CalCode";
                break;
            case 3:
                strFieldName = "CalCodeType";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsuAccNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CalCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CalCodeType"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
