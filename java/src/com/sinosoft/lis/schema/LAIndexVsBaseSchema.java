/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LAIndexVsBaseDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LAIndexVsBaseSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-03-30
 */
public class LAIndexVsBaseSchema implements Schema
{
    // @Field
    /** 代理人级别 */
    private String AgentGrade;
    /** 基础指标代码 */
    private String BaseIndexCode;
    /** 佣金名称 */
    private String WageName;
    /** 展业类型 */
    private String BranchType;
    /** Sql语句编码 */
    private String CalCode;
    /** 指标编码 */
    private String IndexCode;
    /** 佣金对应表名 */
    private String CTableName;
    /** 佣金对应字段名 */
    private String CColName;
    /** 计算顺序 */
    private int IndexOrder;
    /** 打印标志 */
    private String PrintFlag;
    /** 佣金类型 */
    private String IndexType;
    /** 渠道 */
    private String BranchType2;

    public static final int FIELDNUM = 12; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LAIndexVsBaseSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "AgentGrade";
        pk[1] = "BaseIndexCode";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getAgentGrade()
    {
        if (SysConst.CHANGECHARSET && AgentGrade != null &&
            !AgentGrade.equals(""))
        {
            AgentGrade = StrTool.unicodeToGBK(AgentGrade);
        }
        return AgentGrade;
    }

    public void setAgentGrade(String aAgentGrade)
    {
        AgentGrade = aAgentGrade;
    }

    public String getBaseIndexCode()
    {
        if (SysConst.CHANGECHARSET && BaseIndexCode != null &&
            !BaseIndexCode.equals(""))
        {
            BaseIndexCode = StrTool.unicodeToGBK(BaseIndexCode);
        }
        return BaseIndexCode;
    }

    public void setBaseIndexCode(String aBaseIndexCode)
    {
        BaseIndexCode = aBaseIndexCode;
    }

    public String getWageName()
    {
        if (SysConst.CHANGECHARSET && WageName != null && !WageName.equals(""))
        {
            WageName = StrTool.unicodeToGBK(WageName);
        }
        return WageName;
    }

    public void setWageName(String aWageName)
    {
        WageName = aWageName;
    }

    public String getBranchType()
    {
        if (SysConst.CHANGECHARSET && BranchType != null &&
            !BranchType.equals(""))
        {
            BranchType = StrTool.unicodeToGBK(BranchType);
        }
        return BranchType;
    }

    public void setBranchType(String aBranchType)
    {
        BranchType = aBranchType;
    }

    public String getCalCode()
    {
        if (SysConst.CHANGECHARSET && CalCode != null && !CalCode.equals(""))
        {
            CalCode = StrTool.unicodeToGBK(CalCode);
        }
        return CalCode;
    }

    public void setCalCode(String aCalCode)
    {
        CalCode = aCalCode;
    }

    public String getIndexCode()
    {
        if (SysConst.CHANGECHARSET && IndexCode != null && !IndexCode.equals(""))
        {
            IndexCode = StrTool.unicodeToGBK(IndexCode);
        }
        return IndexCode;
    }

    public void setIndexCode(String aIndexCode)
    {
        IndexCode = aIndexCode;
    }

    public String getCTableName()
    {
        if (SysConst.CHANGECHARSET && CTableName != null &&
            !CTableName.equals(""))
        {
            CTableName = StrTool.unicodeToGBK(CTableName);
        }
        return CTableName;
    }

    public void setCTableName(String aCTableName)
    {
        CTableName = aCTableName;
    }

    public String getCColName()
    {
        if (SysConst.CHANGECHARSET && CColName != null && !CColName.equals(""))
        {
            CColName = StrTool.unicodeToGBK(CColName);
        }
        return CColName;
    }

    public void setCColName(String aCColName)
    {
        CColName = aCColName;
    }

    public int getIndexOrder()
    {
        return IndexOrder;
    }

    public void setIndexOrder(int aIndexOrder)
    {
        IndexOrder = aIndexOrder;
    }

    public void setIndexOrder(String aIndexOrder)
    {
        if (aIndexOrder != null && !aIndexOrder.equals(""))
        {
            Integer tInteger = new Integer(aIndexOrder);
            int i = tInteger.intValue();
            IndexOrder = i;
        }
    }

    public String getPrintFlag()
    {
        if (SysConst.CHANGECHARSET && PrintFlag != null && !PrintFlag.equals(""))
        {
            PrintFlag = StrTool.unicodeToGBK(PrintFlag);
        }
        return PrintFlag;
    }

    public void setPrintFlag(String aPrintFlag)
    {
        PrintFlag = aPrintFlag;
    }

    public String getIndexType()
    {
        if (SysConst.CHANGECHARSET && IndexType != null && !IndexType.equals(""))
        {
            IndexType = StrTool.unicodeToGBK(IndexType);
        }
        return IndexType;
    }

    public void setIndexType(String aIndexType)
    {
        IndexType = aIndexType;
    }

    public String getBranchType2()
    {
        if (SysConst.CHANGECHARSET && BranchType2 != null &&
            !BranchType2.equals(""))
        {
            BranchType2 = StrTool.unicodeToGBK(BranchType2);
        }
        return BranchType2;
    }

    public void setBranchType2(String aBranchType2)
    {
        BranchType2 = aBranchType2;
    }

    /**
     * 使用另外一个 LAIndexVsBaseSchema 对象给 Schema 赋值
     * @param: aLAIndexVsBaseSchema LAIndexVsBaseSchema
     **/
    public void setSchema(LAIndexVsBaseSchema aLAIndexVsBaseSchema)
    {
        this.AgentGrade = aLAIndexVsBaseSchema.getAgentGrade();
        this.BaseIndexCode = aLAIndexVsBaseSchema.getBaseIndexCode();
        this.WageName = aLAIndexVsBaseSchema.getWageName();
        this.BranchType = aLAIndexVsBaseSchema.getBranchType();
        this.CalCode = aLAIndexVsBaseSchema.getCalCode();
        this.IndexCode = aLAIndexVsBaseSchema.getIndexCode();
        this.CTableName = aLAIndexVsBaseSchema.getCTableName();
        this.CColName = aLAIndexVsBaseSchema.getCColName();
        this.IndexOrder = aLAIndexVsBaseSchema.getIndexOrder();
        this.PrintFlag = aLAIndexVsBaseSchema.getPrintFlag();
        this.IndexType = aLAIndexVsBaseSchema.getIndexType();
        this.BranchType2 = aLAIndexVsBaseSchema.getBranchType2();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("AgentGrade") == null)
            {
                this.AgentGrade = null;
            }
            else
            {
                this.AgentGrade = rs.getString("AgentGrade").trim();
            }

            if (rs.getString("BaseIndexCode") == null)
            {
                this.BaseIndexCode = null;
            }
            else
            {
                this.BaseIndexCode = rs.getString("BaseIndexCode").trim();
            }

            if (rs.getString("WageName") == null)
            {
                this.WageName = null;
            }
            else
            {
                this.WageName = rs.getString("WageName").trim();
            }

            if (rs.getString("BranchType") == null)
            {
                this.BranchType = null;
            }
            else
            {
                this.BranchType = rs.getString("BranchType").trim();
            }

            if (rs.getString("CalCode") == null)
            {
                this.CalCode = null;
            }
            else
            {
                this.CalCode = rs.getString("CalCode").trim();
            }

            if (rs.getString("IndexCode") == null)
            {
                this.IndexCode = null;
            }
            else
            {
                this.IndexCode = rs.getString("IndexCode").trim();
            }

            if (rs.getString("CTableName") == null)
            {
                this.CTableName = null;
            }
            else
            {
                this.CTableName = rs.getString("CTableName").trim();
            }

            if (rs.getString("CColName") == null)
            {
                this.CColName = null;
            }
            else
            {
                this.CColName = rs.getString("CColName").trim();
            }

            this.IndexOrder = rs.getInt("IndexOrder");
            if (rs.getString("PrintFlag") == null)
            {
                this.PrintFlag = null;
            }
            else
            {
                this.PrintFlag = rs.getString("PrintFlag").trim();
            }

            if (rs.getString("IndexType") == null)
            {
                this.IndexType = null;
            }
            else
            {
                this.IndexType = rs.getString("IndexType").trim();
            }

            if (rs.getString("BranchType2") == null)
            {
                this.BranchType2 = null;
            }
            else
            {
                this.BranchType2 = rs.getString("BranchType2").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAIndexVsBaseSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LAIndexVsBaseSchema getSchema()
    {
        LAIndexVsBaseSchema aLAIndexVsBaseSchema = new LAIndexVsBaseSchema();
        aLAIndexVsBaseSchema.setSchema(this);
        return aLAIndexVsBaseSchema;
    }

    public LAIndexVsBaseDB getDB()
    {
        LAIndexVsBaseDB aDBOper = new LAIndexVsBaseDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAIndexVsBase描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AgentGrade)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BaseIndexCode)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(WageName)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BranchType)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(CalCode)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(IndexCode)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(CTableName)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(CColName)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(IndexOrder));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(PrintFlag)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(IndexType)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BranchType2)));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAIndexVsBase>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            AgentGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                        SysConst.PACKAGESPILTER);
            BaseIndexCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                           SysConst.PACKAGESPILTER);
            WageName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                        SysConst.PACKAGESPILTER);
            CalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                     SysConst.PACKAGESPILTER);
            IndexCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                       SysConst.PACKAGESPILTER);
            CTableName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                        SysConst.PACKAGESPILTER);
            CColName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                      SysConst.PACKAGESPILTER);
            IndexOrder = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 9, SysConst.PACKAGESPILTER))).intValue();
            PrintFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                       SysConst.PACKAGESPILTER);
            IndexType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                       SysConst.PACKAGESPILTER);
            BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                         SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAIndexVsBaseSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("AgentGrade"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGrade));
        }
        if (FCode.equals("BaseIndexCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BaseIndexCode));
        }
        if (FCode.equals("WageName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(WageName));
        }
        if (FCode.equals("BranchType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
        }
        if (FCode.equals("CalCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalCode));
        }
        if (FCode.equals("IndexCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IndexCode));
        }
        if (FCode.equals("CTableName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CTableName));
        }
        if (FCode.equals("CColName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CColName));
        }
        if (FCode.equals("IndexOrder"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IndexOrder));
        }
        if (FCode.equals("PrintFlag"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrintFlag));
        }
        if (FCode.equals("IndexType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IndexType));
        }
        if (FCode.equals("BranchType2"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(AgentGrade);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(BaseIndexCode);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(WageName);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(BranchType);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(CalCode);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(IndexCode);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(CTableName);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(CColName);
                break;
            case 8:
                strFieldValue = String.valueOf(IndexOrder);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(PrintFlag);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(IndexType);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(BranchType2);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("AgentGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentGrade = FValue.trim();
            }
            else
            {
                AgentGrade = null;
            }
        }
        if (FCode.equals("BaseIndexCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BaseIndexCode = FValue.trim();
            }
            else
            {
                BaseIndexCode = null;
            }
        }
        if (FCode.equals("WageName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                WageName = FValue.trim();
            }
            else
            {
                WageName = null;
            }
        }
        if (FCode.equals("BranchType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchType = FValue.trim();
            }
            else
            {
                BranchType = null;
            }
        }
        if (FCode.equals("CalCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CalCode = FValue.trim();
            }
            else
            {
                CalCode = null;
            }
        }
        if (FCode.equals("IndexCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                IndexCode = FValue.trim();
            }
            else
            {
                IndexCode = null;
            }
        }
        if (FCode.equals("CTableName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CTableName = FValue.trim();
            }
            else
            {
                CTableName = null;
            }
        }
        if (FCode.equals("CColName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CColName = FValue.trim();
            }
            else
            {
                CColName = null;
            }
        }
        if (FCode.equals("IndexOrder"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                IndexOrder = i;
            }
        }
        if (FCode.equals("PrintFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PrintFlag = FValue.trim();
            }
            else
            {
                PrintFlag = null;
            }
        }
        if (FCode.equals("IndexType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                IndexType = FValue.trim();
            }
            else
            {
                IndexType = null;
            }
        }
        if (FCode.equals("BranchType2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchType2 = FValue.trim();
            }
            else
            {
                BranchType2 = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LAIndexVsBaseSchema other = (LAIndexVsBaseSchema) otherObject;
        return
                AgentGrade.equals(other.getAgentGrade())
                && BaseIndexCode.equals(other.getBaseIndexCode())
                && WageName.equals(other.getWageName())
                && BranchType.equals(other.getBranchType())
                && CalCode.equals(other.getCalCode())
                && IndexCode.equals(other.getIndexCode())
                && CTableName.equals(other.getCTableName())
                && CColName.equals(other.getCColName())
                && IndexOrder == other.getIndexOrder()
                && PrintFlag.equals(other.getPrintFlag())
                && IndexType.equals(other.getIndexType())
                && BranchType2.equals(other.getBranchType2());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("AgentGrade"))
        {
            return 0;
        }
        if (strFieldName.equals("BaseIndexCode"))
        {
            return 1;
        }
        if (strFieldName.equals("WageName"))
        {
            return 2;
        }
        if (strFieldName.equals("BranchType"))
        {
            return 3;
        }
        if (strFieldName.equals("CalCode"))
        {
            return 4;
        }
        if (strFieldName.equals("IndexCode"))
        {
            return 5;
        }
        if (strFieldName.equals("CTableName"))
        {
            return 6;
        }
        if (strFieldName.equals("CColName"))
        {
            return 7;
        }
        if (strFieldName.equals("IndexOrder"))
        {
            return 8;
        }
        if (strFieldName.equals("PrintFlag"))
        {
            return 9;
        }
        if (strFieldName.equals("IndexType"))
        {
            return 10;
        }
        if (strFieldName.equals("BranchType2"))
        {
            return 11;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "AgentGrade";
                break;
            case 1:
                strFieldName = "BaseIndexCode";
                break;
            case 2:
                strFieldName = "WageName";
                break;
            case 3:
                strFieldName = "BranchType";
                break;
            case 4:
                strFieldName = "CalCode";
                break;
            case 5:
                strFieldName = "IndexCode";
                break;
            case 6:
                strFieldName = "CTableName";
                break;
            case 7:
                strFieldName = "CColName";
                break;
            case 8:
                strFieldName = "IndexOrder";
                break;
            case 9:
                strFieldName = "PrintFlag";
                break;
            case 10:
                strFieldName = "IndexType";
                break;
            case 11:
                strFieldName = "BranchType2";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("AgentGrade"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BaseIndexCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("WageName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CalCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("IndexCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CTableName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CColName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("IndexOrder"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("PrintFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("IndexType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchType2"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_INT;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
