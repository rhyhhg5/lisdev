/*
 * <p>ClassName: LFDateSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: MSREPORT
 * @CreateDate：2005-01-10
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LFDateDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LFDateSchema implements Schema
{
    // @Field
    /** 类型 */
    private String RepType;
    /** 统计年月 */
    private String StatMon;
    /** 报表开始日期 */
    private Date StartDate;
    /** 报表结束日期 */
    private Date EndDate;

    public static final int FIELDNUM = 4; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LFDateSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "RepType";
        pk[1] = "StatMon";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getRepType()
    {
        if (RepType != null && !RepType.equals("") && SysConst.CHANGECHARSET == true)
        {
            RepType = StrTool.unicodeToGBK(RepType);
        }
        return RepType;
    }

    public void setRepType(String aRepType)
    {
        RepType = aRepType;
    }

    public String getStatMon()
    {
        if (StatMon != null && !StatMon.equals("") && SysConst.CHANGECHARSET == true)
        {
            StatMon = StrTool.unicodeToGBK(StatMon);
        }
        return StatMon;
    }

    public void setStatMon(String aStatMon)
    {
        StatMon = aStatMon;
    }

    public String getStartDate()
    {
        if (StartDate != null)
        {
            return fDate.getString(StartDate);
        }
        else
        {
            return null;
        }
    }

    public void setStartDate(Date aStartDate)
    {
        StartDate = aStartDate;
    }

    public void setStartDate(String aStartDate)
    {
        if (aStartDate != null && !aStartDate.equals(""))
        {
            StartDate = fDate.getDate(aStartDate);
        }
        else
        {
            StartDate = null;
        }
    }

    public String getEndDate()
    {
        if (EndDate != null)
        {
            return fDate.getString(EndDate);
        }
        else
        {
            return null;
        }
    }

    public void setEndDate(Date aEndDate)
    {
        EndDate = aEndDate;
    }

    public void setEndDate(String aEndDate)
    {
        if (aEndDate != null && !aEndDate.equals(""))
        {
            EndDate = fDate.getDate(aEndDate);
        }
        else
        {
            EndDate = null;
        }
    }


    /**
     * 使用另外一个 LFDateSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LFDateSchema aLFDateSchema)
    {
        this.RepType = aLFDateSchema.getRepType();
        this.StatMon = aLFDateSchema.getStatMon();
        this.StartDate = fDate.getDate(aLFDateSchema.getStartDate());
        this.EndDate = fDate.getDate(aLFDateSchema.getEndDate());
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("RepType") == null)
            {
                this.RepType = null;
            }
            else
            {
                this.RepType = rs.getString("RepType").trim();
            }

            if (rs.getString("StatMon") == null)
            {
                this.StatMon = null;
            }
            else
            {
                this.StatMon = rs.getString("StatMon").trim();
            }

            this.StartDate = rs.getDate("StartDate");
            this.EndDate = rs.getDate("EndDate");
        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LFDateSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LFDateSchema getSchema()
    {
        LFDateSchema aLFDateSchema = new LFDateSchema();
        aLFDateSchema.setSchema(this);
        return aLFDateSchema;
    }

    public LFDateDB getDB()
    {
        LFDateDB aDBOper = new LFDateDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLFDate描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(RepType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(StatMon)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            StartDate))) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(EndDate)));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLFDate>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            RepType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                     SysConst.PACKAGESPILTER);
            StatMon = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                     SysConst.PACKAGESPILTER);
            StartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 3, SysConst.PACKAGESPILTER));
            EndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 4, SysConst.PACKAGESPILTER));
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LFDateSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("RepType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RepType));
        }
        if (FCode.equals("StatMon"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(StatMon));
        }
        if (FCode.equals("StartDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getStartDate()));
        }
        if (FCode.equals("EndDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getEndDate()));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(RepType);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(StatMon);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getStartDate()));
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getEndDate()));
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("RepType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RepType = FValue.trim();
            }
            else
            {
                RepType = null;
            }
        }
        if (FCode.equals("StatMon"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                StatMon = FValue.trim();
            }
            else
            {
                StatMon = null;
            }
        }
        if (FCode.equals("StartDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                StartDate = fDate.getDate(FValue);
            }
            else
            {
                StartDate = null;
            }
        }
        if (FCode.equals("EndDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EndDate = fDate.getDate(FValue);
            }
            else
            {
                EndDate = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LFDateSchema other = (LFDateSchema) otherObject;
        return
                RepType.equals(other.getRepType())
                && StatMon.equals(other.getStatMon())
                && fDate.getString(StartDate).equals(other.getStartDate())
                && fDate.getString(EndDate).equals(other.getEndDate());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("RepType"))
        {
            return 0;
        }
        if (strFieldName.equals("StatMon"))
        {
            return 1;
        }
        if (strFieldName.equals("StartDate"))
        {
            return 2;
        }
        if (strFieldName.equals("EndDate"))
        {
            return 3;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "RepType";
                break;
            case 1:
                strFieldName = "StatMon";
                break;
            case 2:
                strFieldName = "StartDate";
                break;
            case 3:
                strFieldName = "EndDate";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("RepType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StatMon"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StartDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("EndDate"))
        {
            return Schema.TYPE_DATE;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 3:
                nFieldType = Schema.TYPE_DATE;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
