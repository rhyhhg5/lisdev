/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.TPH_LCCoverageDB;

/*
 * <p>ClassName: TPH_LCCoverageSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: SY
 * @CreateDate：2015-11-02
 */
public class TPH_LCCoverageSchema implements Schema, Cloneable
{
	// @Field
	/** 保单号 */
	private String policyNo;
	/** 分单号 */
	private String sequenceNo;
	/** 被保人客户号 */
	private String insuredNo;
	/** 公司产品组代码 */
	private String coveragePackageCode;
	/** 险种首期保费 */
	private double initialPremium;
	/** 公司险种名称 */
	private String comCoverageName;
	/** 投保年龄 */
	private double ageOfInception;
	/** 险种申请日期 */
	private Date coverageApplicationDate;
	/** 险种生效日期 */
	private Date coverageEffectiveDate;
	/** 险种满期日 */
	private Date coverageExpirationDate;
	/** 主附险标志 */
	private String coverageType;
	/** 险种年度保额 */
	private double sa;
	/** 自动续保标志 */
	private String renewIndi;
	/** 险种状态 */
	private String coverageStatus;
	/** 公司险种代码 */
	private String comCoverageCode;
	/** 处理日期 */
	private Date makeDate;
	/** 处理时间 */
	private String makeTime;
	/** 险种当期风险保费 */
	private double riskPremium;

	public static final int FIELDNUM = 18;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public TPH_LCCoverageSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[4];
		pk[0] = "policyNo";
		pk[1] = "sequenceNo";
		pk[2] = "insuredNo";
		pk[3] = "comCoverageCode";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		TPH_LCCoverageSchema cloned = (TPH_LCCoverageSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getpolicyNo()
	{
		return policyNo;
	}
	public void setpolicyNo(String apolicyNo)
	{
		policyNo = apolicyNo;
	}
	public String getsequenceNo()
	{
		return sequenceNo;
	}
	public void setsequenceNo(String asequenceNo)
	{
		sequenceNo = asequenceNo;
	}
	public String getinsuredNo()
	{
		return insuredNo;
	}
	public void setinsuredNo(String ainsuredNo)
	{
		insuredNo = ainsuredNo;
	}
	public String getcoveragePackageCode()
	{
		return coveragePackageCode;
	}
	public void setcoveragePackageCode(String acoveragePackageCode)
	{
		coveragePackageCode = acoveragePackageCode;
	}
	public double getinitialPremium()
	{
		return initialPremium;
	}
	public void setinitialPremium(double ainitialPremium)
	{
		initialPremium = Arith.round(ainitialPremium,2);
	}
	public void setinitialPremium(String ainitialPremium)
	{
		if (ainitialPremium != null && !ainitialPremium.equals(""))
		{
			Double tDouble = new Double(ainitialPremium);
			double d = tDouble.doubleValue();
                initialPremium = Arith.round(d,2);
		}
	}

	public String getcomCoverageName()
	{
		return comCoverageName;
	}
	public void setcomCoverageName(String acomCoverageName)
	{
		comCoverageName = acomCoverageName;
	}
	public double getageOfInception()
	{
		return ageOfInception;
	}
	public void setageOfInception(double aageOfInception)
	{
		ageOfInception = Arith.round(aageOfInception,0);
	}
	public void setageOfInception(String aageOfInception)
	{
		if (aageOfInception != null && !aageOfInception.equals(""))
		{
			Double tDouble = new Double(aageOfInception);
			double d = tDouble.doubleValue();
                ageOfInception = Arith.round(d,0);
		}
	}

	public String getcoverageApplicationDate()
	{
		if( coverageApplicationDate != null )
			return fDate.getString(coverageApplicationDate);
		else
			return null;
	}
	public void setcoverageApplicationDate(Date acoverageApplicationDate)
	{
		coverageApplicationDate = acoverageApplicationDate;
	}
	public void setcoverageApplicationDate(String acoverageApplicationDate)
	{
		if (acoverageApplicationDate != null && !acoverageApplicationDate.equals("") )
		{
			coverageApplicationDate = fDate.getDate( acoverageApplicationDate );
		}
		else
			coverageApplicationDate = null;
	}

	public String getcoverageEffectiveDate()
	{
		if( coverageEffectiveDate != null )
			return fDate.getString(coverageEffectiveDate);
		else
			return null;
	}
	public void setcoverageEffectiveDate(Date acoverageEffectiveDate)
	{
		coverageEffectiveDate = acoverageEffectiveDate;
	}
	public void setcoverageEffectiveDate(String acoverageEffectiveDate)
	{
		if (acoverageEffectiveDate != null && !acoverageEffectiveDate.equals("") )
		{
			coverageEffectiveDate = fDate.getDate( acoverageEffectiveDate );
		}
		else
			coverageEffectiveDate = null;
	}

	public String getcoverageExpirationDate()
	{
		if( coverageExpirationDate != null )
			return fDate.getString(coverageExpirationDate);
		else
			return null;
	}
	public void setcoverageExpirationDate(Date acoverageExpirationDate)
	{
		coverageExpirationDate = acoverageExpirationDate;
	}
	public void setcoverageExpirationDate(String acoverageExpirationDate)
	{
		if (acoverageExpirationDate != null && !acoverageExpirationDate.equals("") )
		{
			coverageExpirationDate = fDate.getDate( acoverageExpirationDate );
		}
		else
			coverageExpirationDate = null;
	}

	public String getcoverageType()
	{
		return coverageType;
	}
	public void setcoverageType(String acoverageType)
	{
		coverageType = acoverageType;
	}
	public double getsa()
	{
		return sa;
	}
	public void setsa(double asa)
	{
		sa = Arith.round(asa,2);
	}
	public void setsa(String asa)
	{
		if (asa != null && !asa.equals(""))
		{
			Double tDouble = new Double(asa);
			double d = tDouble.doubleValue();
                sa = Arith.round(d,2);
		}
	}

	public String getrenewIndi()
	{
		return renewIndi;
	}
	public void setrenewIndi(String arenewIndi)
	{
		renewIndi = arenewIndi;
	}
	public String getcoverageStatus()
	{
		return coverageStatus;
	}
	public void setcoverageStatus(String acoverageStatus)
	{
		coverageStatus = acoverageStatus;
	}
	public String getcomCoverageCode()
	{
		return comCoverageCode;
	}
	public void setcomCoverageCode(String acomCoverageCode)
	{
		comCoverageCode = acomCoverageCode;
	}
	public String getmakeDate()
	{
		if( makeDate != null )
			return fDate.getString(makeDate);
		else
			return null;
	}
	public void setmakeDate(Date amakeDate)
	{
		makeDate = amakeDate;
	}
	public void setmakeDate(String amakeDate)
	{
		if (amakeDate != null && !amakeDate.equals("") )
		{
			makeDate = fDate.getDate( amakeDate );
		}
		else
			makeDate = null;
	}

	public String getmakeTime()
	{
		return makeTime;
	}
	public void setmakeTime(String amakeTime)
	{
		makeTime = amakeTime;
	}
	public double getriskPremium()
	{
		return riskPremium;
	}
	public void setriskPremium(double ariskPremium)
	{
		riskPremium = Arith.round(ariskPremium,2);
	}
	public void setriskPremium(String ariskPremium)
	{
		if (ariskPremium != null && !ariskPremium.equals(""))
		{
			Double tDouble = new Double(ariskPremium);
			double d = tDouble.doubleValue();
                riskPremium = Arith.round(d,2);
		}
	}


	/**
	* 使用另外一个 TPH_LCCoverageSchema 对象给 Schema 赋值
	* @param: aTPH_LCCoverageSchema TPH_LCCoverageSchema
	**/
	public void setSchema(TPH_LCCoverageSchema aTPH_LCCoverageSchema)
	{
		this.policyNo = aTPH_LCCoverageSchema.getpolicyNo();
		this.sequenceNo = aTPH_LCCoverageSchema.getsequenceNo();
		this.insuredNo = aTPH_LCCoverageSchema.getinsuredNo();
		this.coveragePackageCode = aTPH_LCCoverageSchema.getcoveragePackageCode();
		this.initialPremium = aTPH_LCCoverageSchema.getinitialPremium();
		this.comCoverageName = aTPH_LCCoverageSchema.getcomCoverageName();
		this.ageOfInception = aTPH_LCCoverageSchema.getageOfInception();
		this.coverageApplicationDate = fDate.getDate( aTPH_LCCoverageSchema.getcoverageApplicationDate());
		this.coverageEffectiveDate = fDate.getDate( aTPH_LCCoverageSchema.getcoverageEffectiveDate());
		this.coverageExpirationDate = fDate.getDate( aTPH_LCCoverageSchema.getcoverageExpirationDate());
		this.coverageType = aTPH_LCCoverageSchema.getcoverageType();
		this.sa = aTPH_LCCoverageSchema.getsa();
		this.renewIndi = aTPH_LCCoverageSchema.getrenewIndi();
		this.coverageStatus = aTPH_LCCoverageSchema.getcoverageStatus();
		this.comCoverageCode = aTPH_LCCoverageSchema.getcomCoverageCode();
		this.makeDate = fDate.getDate( aTPH_LCCoverageSchema.getmakeDate());
		this.makeTime = aTPH_LCCoverageSchema.getmakeTime();
		this.riskPremium = aTPH_LCCoverageSchema.getriskPremium();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("policyNo") == null )
				this.policyNo = null;
			else
				this.policyNo = rs.getString("policyNo").trim();

			if( rs.getString("sequenceNo") == null )
				this.sequenceNo = null;
			else
				this.sequenceNo = rs.getString("sequenceNo").trim();

			if( rs.getString("insuredNo") == null )
				this.insuredNo = null;
			else
				this.insuredNo = rs.getString("insuredNo").trim();

			if( rs.getString("coveragePackageCode") == null )
				this.coveragePackageCode = null;
			else
				this.coveragePackageCode = rs.getString("coveragePackageCode").trim();

			this.initialPremium = rs.getDouble("initialPremium");
			if( rs.getString("comCoverageName") == null )
				this.comCoverageName = null;
			else
				this.comCoverageName = rs.getString("comCoverageName").trim();

			this.ageOfInception = rs.getDouble("ageOfInception");
			this.coverageApplicationDate = rs.getDate("coverageApplicationDate");
			this.coverageEffectiveDate = rs.getDate("coverageEffectiveDate");
			this.coverageExpirationDate = rs.getDate("coverageExpirationDate");
			if( rs.getString("coverageType") == null )
				this.coverageType = null;
			else
				this.coverageType = rs.getString("coverageType").trim();

			this.sa = rs.getDouble("sa");
			if( rs.getString("renewIndi") == null )
				this.renewIndi = null;
			else
				this.renewIndi = rs.getString("renewIndi").trim();

			if( rs.getString("coverageStatus") == null )
				this.coverageStatus = null;
			else
				this.coverageStatus = rs.getString("coverageStatus").trim();

			if( rs.getString("comCoverageCode") == null )
				this.comCoverageCode = null;
			else
				this.comCoverageCode = rs.getString("comCoverageCode").trim();

			this.makeDate = rs.getDate("makeDate");
			if( rs.getString("makeTime") == null )
				this.makeTime = null;
			else
				this.makeTime = rs.getString("makeTime").trim();

			this.riskPremium = rs.getDouble("riskPremium");
		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的TPH_LCCoverage表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "TPH_LCCoverageSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public TPH_LCCoverageSchema getSchema()
	{
		TPH_LCCoverageSchema aTPH_LCCoverageSchema = new TPH_LCCoverageSchema();
		aTPH_LCCoverageSchema.setSchema(this);
		return aTPH_LCCoverageSchema;
	}

	public TPH_LCCoverageDB getDB()
	{
		TPH_LCCoverageDB aDBOper = new TPH_LCCoverageDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpTPH_LCCoverage描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(policyNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(sequenceNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(insuredNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(coveragePackageCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(initialPremium));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(comCoverageName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ageOfInception));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( coverageApplicationDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( coverageEffectiveDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( coverageExpirationDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(coverageType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(sa));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(renewIndi)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(coverageStatus)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(comCoverageCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( makeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(makeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(riskPremium));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpTPH_LCCoverage>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			policyNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			sequenceNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			insuredNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			coveragePackageCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			initialPremium = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,5,SysConst.PACKAGESPILTER))).doubleValue();
			comCoverageName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			ageOfInception = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,7,SysConst.PACKAGESPILTER))).doubleValue();
			coverageApplicationDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,SysConst.PACKAGESPILTER));
			coverageEffectiveDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,SysConst.PACKAGESPILTER));
			coverageExpirationDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,SysConst.PACKAGESPILTER));
			coverageType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			sa = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,12,SysConst.PACKAGESPILTER))).doubleValue();
			renewIndi = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			coverageStatus = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			comCoverageCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			makeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,SysConst.PACKAGESPILTER));
			makeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			riskPremium = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,18,SysConst.PACKAGESPILTER))).doubleValue();
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "TPH_LCCoverageSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("policyNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(policyNo));
		}
		if (FCode.equals("sequenceNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(sequenceNo));
		}
		if (FCode.equals("insuredNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(insuredNo));
		}
		if (FCode.equals("coveragePackageCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(coveragePackageCode));
		}
		if (FCode.equals("initialPremium"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(initialPremium));
		}
		if (FCode.equals("comCoverageName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(comCoverageName));
		}
		if (FCode.equals("ageOfInception"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ageOfInception));
		}
		if (FCode.equals("coverageApplicationDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getcoverageApplicationDate()));
		}
		if (FCode.equals("coverageEffectiveDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getcoverageEffectiveDate()));
		}
		if (FCode.equals("coverageExpirationDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getcoverageExpirationDate()));
		}
		if (FCode.equals("coverageType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(coverageType));
		}
		if (FCode.equals("sa"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(sa));
		}
		if (FCode.equals("renewIndi"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(renewIndi));
		}
		if (FCode.equals("coverageStatus"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(coverageStatus));
		}
		if (FCode.equals("comCoverageCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(comCoverageCode));
		}
		if (FCode.equals("makeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getmakeDate()));
		}
		if (FCode.equals("makeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(makeTime));
		}
		if (FCode.equals("riskPremium"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(riskPremium));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(policyNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(sequenceNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(insuredNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(coveragePackageCode);
				break;
			case 4:
				strFieldValue = String.valueOf(initialPremium);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(comCoverageName);
				break;
			case 6:
				strFieldValue = String.valueOf(ageOfInception);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getcoverageApplicationDate()));
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getcoverageEffectiveDate()));
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getcoverageExpirationDate()));
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(coverageType);
				break;
			case 11:
				strFieldValue = String.valueOf(sa);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(renewIndi);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(coverageStatus);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(comCoverageCode);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getmakeDate()));
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(makeTime);
				break;
			case 17:
				strFieldValue = String.valueOf(riskPremium);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("policyNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				policyNo = FValue.trim();
			}
			else
				policyNo = null;
		}
		if (FCode.equalsIgnoreCase("sequenceNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				sequenceNo = FValue.trim();
			}
			else
				sequenceNo = null;
		}
		if (FCode.equalsIgnoreCase("insuredNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				insuredNo = FValue.trim();
			}
			else
				insuredNo = null;
		}
		if (FCode.equalsIgnoreCase("coveragePackageCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				coveragePackageCode = FValue.trim();
			}
			else
				coveragePackageCode = null;
		}
		if (FCode.equalsIgnoreCase("initialPremium"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				initialPremium = d;
			}
		}
		if (FCode.equalsIgnoreCase("comCoverageName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				comCoverageName = FValue.trim();
			}
			else
				comCoverageName = null;
		}
		if (FCode.equalsIgnoreCase("ageOfInception"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ageOfInception = d;
			}
		}
		if (FCode.equalsIgnoreCase("coverageApplicationDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				coverageApplicationDate = fDate.getDate( FValue );
			}
			else
				coverageApplicationDate = null;
		}
		if (FCode.equalsIgnoreCase("coverageEffectiveDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				coverageEffectiveDate = fDate.getDate( FValue );
			}
			else
				coverageEffectiveDate = null;
		}
		if (FCode.equalsIgnoreCase("coverageExpirationDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				coverageExpirationDate = fDate.getDate( FValue );
			}
			else
				coverageExpirationDate = null;
		}
		if (FCode.equalsIgnoreCase("coverageType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				coverageType = FValue.trim();
			}
			else
				coverageType = null;
		}
		if (FCode.equalsIgnoreCase("sa"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				sa = d;
			}
		}
		if (FCode.equalsIgnoreCase("renewIndi"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				renewIndi = FValue.trim();
			}
			else
				renewIndi = null;
		}
		if (FCode.equalsIgnoreCase("coverageStatus"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				coverageStatus = FValue.trim();
			}
			else
				coverageStatus = null;
		}
		if (FCode.equalsIgnoreCase("comCoverageCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				comCoverageCode = FValue.trim();
			}
			else
				comCoverageCode = null;
		}
		if (FCode.equalsIgnoreCase("makeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				makeDate = fDate.getDate( FValue );
			}
			else
				makeDate = null;
		}
		if (FCode.equalsIgnoreCase("makeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				makeTime = FValue.trim();
			}
			else
				makeTime = null;
		}
		if (FCode.equalsIgnoreCase("riskPremium"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				riskPremium = d;
			}
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		TPH_LCCoverageSchema other = (TPH_LCCoverageSchema)otherObject;
		return
			(policyNo == null ? other.getpolicyNo() == null : policyNo.equals(other.getpolicyNo()))
			&& (sequenceNo == null ? other.getsequenceNo() == null : sequenceNo.equals(other.getsequenceNo()))
			&& (insuredNo == null ? other.getinsuredNo() == null : insuredNo.equals(other.getinsuredNo()))
			&& (coveragePackageCode == null ? other.getcoveragePackageCode() == null : coveragePackageCode.equals(other.getcoveragePackageCode()))
			&& initialPremium == other.getinitialPremium()
			&& (comCoverageName == null ? other.getcomCoverageName() == null : comCoverageName.equals(other.getcomCoverageName()))
			&& ageOfInception == other.getageOfInception()
			&& (coverageApplicationDate == null ? other.getcoverageApplicationDate() == null : fDate.getString(coverageApplicationDate).equals(other.getcoverageApplicationDate()))
			&& (coverageEffectiveDate == null ? other.getcoverageEffectiveDate() == null : fDate.getString(coverageEffectiveDate).equals(other.getcoverageEffectiveDate()))
			&& (coverageExpirationDate == null ? other.getcoverageExpirationDate() == null : fDate.getString(coverageExpirationDate).equals(other.getcoverageExpirationDate()))
			&& (coverageType == null ? other.getcoverageType() == null : coverageType.equals(other.getcoverageType()))
			&& sa == other.getsa()
			&& (renewIndi == null ? other.getrenewIndi() == null : renewIndi.equals(other.getrenewIndi()))
			&& (coverageStatus == null ? other.getcoverageStatus() == null : coverageStatus.equals(other.getcoverageStatus()))
			&& (comCoverageCode == null ? other.getcomCoverageCode() == null : comCoverageCode.equals(other.getcomCoverageCode()))
			&& (makeDate == null ? other.getmakeDate() == null : fDate.getString(makeDate).equals(other.getmakeDate()))
			&& (makeTime == null ? other.getmakeTime() == null : makeTime.equals(other.getmakeTime()))
			&& riskPremium == other.getriskPremium();
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("policyNo") ) {
			return 0;
		}
		if( strFieldName.equals("sequenceNo") ) {
			return 1;
		}
		if( strFieldName.equals("insuredNo") ) {
			return 2;
		}
		if( strFieldName.equals("coveragePackageCode") ) {
			return 3;
		}
		if( strFieldName.equals("initialPremium") ) {
			return 4;
		}
		if( strFieldName.equals("comCoverageName") ) {
			return 5;
		}
		if( strFieldName.equals("ageOfInception") ) {
			return 6;
		}
		if( strFieldName.equals("coverageApplicationDate") ) {
			return 7;
		}
		if( strFieldName.equals("coverageEffectiveDate") ) {
			return 8;
		}
		if( strFieldName.equals("coverageExpirationDate") ) {
			return 9;
		}
		if( strFieldName.equals("coverageType") ) {
			return 10;
		}
		if( strFieldName.equals("sa") ) {
			return 11;
		}
		if( strFieldName.equals("renewIndi") ) {
			return 12;
		}
		if( strFieldName.equals("coverageStatus") ) {
			return 13;
		}
		if( strFieldName.equals("comCoverageCode") ) {
			return 14;
		}
		if( strFieldName.equals("makeDate") ) {
			return 15;
		}
		if( strFieldName.equals("makeTime") ) {
			return 16;
		}
		if( strFieldName.equals("riskPremium") ) {
			return 17;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "policyNo";
				break;
			case 1:
				strFieldName = "sequenceNo";
				break;
			case 2:
				strFieldName = "insuredNo";
				break;
			case 3:
				strFieldName = "coveragePackageCode";
				break;
			case 4:
				strFieldName = "initialPremium";
				break;
			case 5:
				strFieldName = "comCoverageName";
				break;
			case 6:
				strFieldName = "ageOfInception";
				break;
			case 7:
				strFieldName = "coverageApplicationDate";
				break;
			case 8:
				strFieldName = "coverageEffectiveDate";
				break;
			case 9:
				strFieldName = "coverageExpirationDate";
				break;
			case 10:
				strFieldName = "coverageType";
				break;
			case 11:
				strFieldName = "sa";
				break;
			case 12:
				strFieldName = "renewIndi";
				break;
			case 13:
				strFieldName = "coverageStatus";
				break;
			case 14:
				strFieldName = "comCoverageCode";
				break;
			case 15:
				strFieldName = "makeDate";
				break;
			case 16:
				strFieldName = "makeTime";
				break;
			case 17:
				strFieldName = "riskPremium";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("policyNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("sequenceNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("insuredNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("coveragePackageCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("initialPremium") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("comCoverageName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ageOfInception") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("coverageApplicationDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("coverageEffectiveDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("coverageExpirationDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("coverageType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("sa") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("renewIndi") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("coverageStatus") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("comCoverageCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("makeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("makeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("riskPremium") ) {
			return Schema.TYPE_DOUBLE;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 7:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 8:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 9:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
