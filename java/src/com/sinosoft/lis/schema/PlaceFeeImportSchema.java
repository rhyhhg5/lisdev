/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.PlaceFeeImportDB;

/*
 * <p>ClassName: PlaceFeeImportSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2013-03-11
 */
public class PlaceFeeImportSchema implements Schema, Cloneable
{
	// @Field
	/** 职场编码 */
	private String PlaceNo;
	/** 费用类型 */
	private String FeeType;
	/** 序列号 */
	private String SOrder;
	/** 支付日期 */
	private Date PayDate;
	/** 支付金额 */
	private double Money;
	/** 支付租金摊销起期 */
	private Date PayBeginDate;
	/** 支付租金摊销止期 */
	private Date PayEndDate;
	/** 操作人 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 修改日期 */
	private Date ModifyDate;
	/** 修改时间 */
	private String ModifyTime;
	/** 备用字符串类型1 */
	private String StandByString1;
	/** 备用字符串类型2 */
	private String StandByString2;
	/** 备用数字类型1 */
	private double StandByNum1;
	/** 备用数字类型2 */
	private double StandByNum2;
	/** 备用日期类型1 */
	private Date StandByDate1;
	/** 备用日期类型2 */
	private Date StandByDate2;

	public static final int FIELDNUM = 18;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public PlaceFeeImportSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[3];
		pk[0] = "PlaceNo";
		pk[1] = "FeeType";
		pk[2] = "SOrder";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		PlaceFeeImportSchema cloned = (PlaceFeeImportSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getPlaceNo()
	{
		return PlaceNo;
	}
	public void setPlaceNo(String aPlaceNo)
	{
		PlaceNo = aPlaceNo;
	}
	public String getFeeType()
	{
		return FeeType;
	}
	public void setFeeType(String aFeeType)
	{
		FeeType = aFeeType;
	}
	public String getSOrder()
	{
		return SOrder;
	}
	public void setSOrder(String aSOrder)
	{
		SOrder = aSOrder;
	}
	public String getPayDate()
	{
		if( PayDate != null )
			return fDate.getString(PayDate);
		else
			return null;
	}
	public void setPayDate(Date aPayDate)
	{
		PayDate = aPayDate;
	}
	public void setPayDate(String aPayDate)
	{
		if (aPayDate != null && !aPayDate.equals("") )
		{
			PayDate = fDate.getDate( aPayDate );
		}
		else
			PayDate = null;
	}

	public double getMoney()
	{
		return Money;
	}
	public void setMoney(double aMoney)
	{
		Money = Arith.round(aMoney,2);
	}
	public void setMoney(String aMoney)
	{
		if (aMoney != null && !aMoney.equals(""))
		{
			Double tDouble = new Double(aMoney);
			double d = tDouble.doubleValue();
                Money = Arith.round(d,2);
		}
	}

	public String getPayBeginDate()
	{
		if( PayBeginDate != null )
			return fDate.getString(PayBeginDate);
		else
			return null;
	}
	public void setPayBeginDate(Date aPayBeginDate)
	{
		PayBeginDate = aPayBeginDate;
	}
	public void setPayBeginDate(String aPayBeginDate)
	{
		if (aPayBeginDate != null && !aPayBeginDate.equals("") )
		{
			PayBeginDate = fDate.getDate( aPayBeginDate );
		}
		else
			PayBeginDate = null;
	}

	public String getPayEndDate()
	{
		if( PayEndDate != null )
			return fDate.getString(PayEndDate);
		else
			return null;
	}
	public void setPayEndDate(Date aPayEndDate)
	{
		PayEndDate = aPayEndDate;
	}
	public void setPayEndDate(String aPayEndDate)
	{
		if (aPayEndDate != null && !aPayEndDate.equals("") )
		{
			PayEndDate = fDate.getDate( aPayEndDate );
		}
		else
			PayEndDate = null;
	}

	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getStandByString1()
	{
		return StandByString1;
	}
	public void setStandByString1(String aStandByString1)
	{
		StandByString1 = aStandByString1;
	}
	public String getStandByString2()
	{
		return StandByString2;
	}
	public void setStandByString2(String aStandByString2)
	{
		StandByString2 = aStandByString2;
	}
	public double getStandByNum1()
	{
		return StandByNum1;
	}
	public void setStandByNum1(double aStandByNum1)
	{
		StandByNum1 = Arith.round(aStandByNum1,2);
	}
	public void setStandByNum1(String aStandByNum1)
	{
		if (aStandByNum1 != null && !aStandByNum1.equals(""))
		{
			Double tDouble = new Double(aStandByNum1);
			double d = tDouble.doubleValue();
                StandByNum1 = Arith.round(d,2);
		}
	}

	public double getStandByNum2()
	{
		return StandByNum2;
	}
	public void setStandByNum2(double aStandByNum2)
	{
		StandByNum2 = Arith.round(aStandByNum2,2);
	}
	public void setStandByNum2(String aStandByNum2)
	{
		if (aStandByNum2 != null && !aStandByNum2.equals(""))
		{
			Double tDouble = new Double(aStandByNum2);
			double d = tDouble.doubleValue();
                StandByNum2 = Arith.round(d,2);
		}
	}

	public String getStandByDate1()
	{
		if( StandByDate1 != null )
			return fDate.getString(StandByDate1);
		else
			return null;
	}
	public void setStandByDate1(Date aStandByDate1)
	{
		StandByDate1 = aStandByDate1;
	}
	public void setStandByDate1(String aStandByDate1)
	{
		if (aStandByDate1 != null && !aStandByDate1.equals("") )
		{
			StandByDate1 = fDate.getDate( aStandByDate1 );
		}
		else
			StandByDate1 = null;
	}

	public String getStandByDate2()
	{
		if( StandByDate2 != null )
			return fDate.getString(StandByDate2);
		else
			return null;
	}
	public void setStandByDate2(Date aStandByDate2)
	{
		StandByDate2 = aStandByDate2;
	}
	public void setStandByDate2(String aStandByDate2)
	{
		if (aStandByDate2 != null && !aStandByDate2.equals("") )
		{
			StandByDate2 = fDate.getDate( aStandByDate2 );
		}
		else
			StandByDate2 = null;
	}


	/**
	* 使用另外一个 PlaceFeeImportSchema 对象给 Schema 赋值
	* @param: aPlaceFeeImportSchema PlaceFeeImportSchema
	**/
	public void setSchema(PlaceFeeImportSchema aPlaceFeeImportSchema)
	{
		this.PlaceNo = aPlaceFeeImportSchema.getPlaceNo();
		this.FeeType = aPlaceFeeImportSchema.getFeeType();
		this.SOrder = aPlaceFeeImportSchema.getSOrder();
		this.PayDate = fDate.getDate( aPlaceFeeImportSchema.getPayDate());
		this.Money = aPlaceFeeImportSchema.getMoney();
		this.PayBeginDate = fDate.getDate( aPlaceFeeImportSchema.getPayBeginDate());
		this.PayEndDate = fDate.getDate( aPlaceFeeImportSchema.getPayEndDate());
		this.Operator = aPlaceFeeImportSchema.getOperator();
		this.MakeDate = fDate.getDate( aPlaceFeeImportSchema.getMakeDate());
		this.MakeTime = aPlaceFeeImportSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aPlaceFeeImportSchema.getModifyDate());
		this.ModifyTime = aPlaceFeeImportSchema.getModifyTime();
		this.StandByString1 = aPlaceFeeImportSchema.getStandByString1();
		this.StandByString2 = aPlaceFeeImportSchema.getStandByString2();
		this.StandByNum1 = aPlaceFeeImportSchema.getStandByNum1();
		this.StandByNum2 = aPlaceFeeImportSchema.getStandByNum2();
		this.StandByDate1 = fDate.getDate( aPlaceFeeImportSchema.getStandByDate1());
		this.StandByDate2 = fDate.getDate( aPlaceFeeImportSchema.getStandByDate2());
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("PlaceNo") == null )
				this.PlaceNo = null;
			else
				this.PlaceNo = rs.getString("PlaceNo").trim();

			if( rs.getString("FeeType") == null )
				this.FeeType = null;
			else
				this.FeeType = rs.getString("FeeType").trim();

			if( rs.getString("SOrder") == null )
				this.SOrder = null;
			else
				this.SOrder = rs.getString("SOrder").trim();

			this.PayDate = rs.getDate("PayDate");
			this.Money = rs.getDouble("Money");
			this.PayBeginDate = rs.getDate("PayBeginDate");
			this.PayEndDate = rs.getDate("PayEndDate");
			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("StandByString1") == null )
				this.StandByString1 = null;
			else
				this.StandByString1 = rs.getString("StandByString1").trim();

			if( rs.getString("StandByString2") == null )
				this.StandByString2 = null;
			else
				this.StandByString2 = rs.getString("StandByString2").trim();

			this.StandByNum1 = rs.getDouble("StandByNum1");
			this.StandByNum2 = rs.getDouble("StandByNum2");
			this.StandByDate1 = rs.getDate("StandByDate1");
			this.StandByDate2 = rs.getDate("StandByDate2");
		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的PlaceFeeImport表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "PlaceFeeImportSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public PlaceFeeImportSchema getSchema()
	{
		PlaceFeeImportSchema aPlaceFeeImportSchema = new PlaceFeeImportSchema();
		aPlaceFeeImportSchema.setSchema(this);
		return aPlaceFeeImportSchema;
	}

	public PlaceFeeImportDB getDB()
	{
		PlaceFeeImportDB aDBOper = new PlaceFeeImportDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpPlaceFeeImport描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(PlaceNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FeeType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SOrder)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( PayDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Money));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( PayBeginDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( PayEndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandByString1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandByString2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(StandByNum1));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(StandByNum2));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( StandByDate1 ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( StandByDate2 )));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpPlaceFeeImport>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			PlaceNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			FeeType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			SOrder = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			PayDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,SysConst.PACKAGESPILTER));
			Money = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,5,SysConst.PACKAGESPILTER))).doubleValue();
			PayBeginDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,SysConst.PACKAGESPILTER));
			PayEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,SysConst.PACKAGESPILTER));
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			StandByString1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			StandByString2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			StandByNum1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,15,SysConst.PACKAGESPILTER))).doubleValue();
			StandByNum2 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,16,SysConst.PACKAGESPILTER))).doubleValue();
			StandByDate1 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,SysConst.PACKAGESPILTER));
			StandByDate2 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,SysConst.PACKAGESPILTER));
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "PlaceFeeImportSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("PlaceNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PlaceNo));
		}
		if (FCode.equals("FeeType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FeeType));
		}
		if (FCode.equals("SOrder"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SOrder));
		}
		if (FCode.equals("PayDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getPayDate()));
		}
		if (FCode.equals("Money"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Money));
		}
		if (FCode.equals("PayBeginDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getPayBeginDate()));
		}
		if (FCode.equals("PayEndDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getPayEndDate()));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("StandByString1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandByString1));
		}
		if (FCode.equals("StandByString2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandByString2));
		}
		if (FCode.equals("StandByNum1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandByNum1));
		}
		if (FCode.equals("StandByNum2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandByNum2));
		}
		if (FCode.equals("StandByDate1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStandByDate1()));
		}
		if (FCode.equals("StandByDate2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStandByDate2()));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(PlaceNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(FeeType);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(SOrder);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getPayDate()));
				break;
			case 4:
				strFieldValue = String.valueOf(Money);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getPayBeginDate()));
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getPayEndDate()));
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(StandByString1);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(StandByString2);
				break;
			case 14:
				strFieldValue = String.valueOf(StandByNum1);
				break;
			case 15:
				strFieldValue = String.valueOf(StandByNum2);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStandByDate1()));
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStandByDate2()));
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("PlaceNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PlaceNo = FValue.trim();
			}
			else
				PlaceNo = null;
		}
		if (FCode.equalsIgnoreCase("FeeType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FeeType = FValue.trim();
			}
			else
				FeeType = null;
		}
		if (FCode.equalsIgnoreCase("SOrder"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SOrder = FValue.trim();
			}
			else
				SOrder = null;
		}
		if (FCode.equalsIgnoreCase("PayDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				PayDate = fDate.getDate( FValue );
			}
			else
				PayDate = null;
		}
		if (FCode.equalsIgnoreCase("Money"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Money = d;
			}
		}
		if (FCode.equalsIgnoreCase("PayBeginDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				PayBeginDate = fDate.getDate( FValue );
			}
			else
				PayBeginDate = null;
		}
		if (FCode.equalsIgnoreCase("PayEndDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				PayEndDate = fDate.getDate( FValue );
			}
			else
				PayEndDate = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("StandByString1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandByString1 = FValue.trim();
			}
			else
				StandByString1 = null;
		}
		if (FCode.equalsIgnoreCase("StandByString2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandByString2 = FValue.trim();
			}
			else
				StandByString2 = null;
		}
		if (FCode.equalsIgnoreCase("StandByNum1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				StandByNum1 = d;
			}
		}
		if (FCode.equalsIgnoreCase("StandByNum2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				StandByNum2 = d;
			}
		}
		if (FCode.equalsIgnoreCase("StandByDate1"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				StandByDate1 = fDate.getDate( FValue );
			}
			else
				StandByDate1 = null;
		}
		if (FCode.equalsIgnoreCase("StandByDate2"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				StandByDate2 = fDate.getDate( FValue );
			}
			else
				StandByDate2 = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		PlaceFeeImportSchema other = (PlaceFeeImportSchema)otherObject;
		return
			(PlaceNo == null ? other.getPlaceNo() == null : PlaceNo.equals(other.getPlaceNo()))
			&& (FeeType == null ? other.getFeeType() == null : FeeType.equals(other.getFeeType()))
			&& (SOrder == null ? other.getSOrder() == null : SOrder.equals(other.getSOrder()))
			&& (PayDate == null ? other.getPayDate() == null : fDate.getString(PayDate).equals(other.getPayDate()))
			&& Money == other.getMoney()
			&& (PayBeginDate == null ? other.getPayBeginDate() == null : fDate.getString(PayBeginDate).equals(other.getPayBeginDate()))
			&& (PayEndDate == null ? other.getPayEndDate() == null : fDate.getString(PayEndDate).equals(other.getPayEndDate()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (StandByString1 == null ? other.getStandByString1() == null : StandByString1.equals(other.getStandByString1()))
			&& (StandByString2 == null ? other.getStandByString2() == null : StandByString2.equals(other.getStandByString2()))
			&& StandByNum1 == other.getStandByNum1()
			&& StandByNum2 == other.getStandByNum2()
			&& (StandByDate1 == null ? other.getStandByDate1() == null : fDate.getString(StandByDate1).equals(other.getStandByDate1()))
			&& (StandByDate2 == null ? other.getStandByDate2() == null : fDate.getString(StandByDate2).equals(other.getStandByDate2()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("PlaceNo") ) {
			return 0;
		}
		if( strFieldName.equals("FeeType") ) {
			return 1;
		}
		if( strFieldName.equals("SOrder") ) {
			return 2;
		}
		if( strFieldName.equals("PayDate") ) {
			return 3;
		}
		if( strFieldName.equals("Money") ) {
			return 4;
		}
		if( strFieldName.equals("PayBeginDate") ) {
			return 5;
		}
		if( strFieldName.equals("PayEndDate") ) {
			return 6;
		}
		if( strFieldName.equals("Operator") ) {
			return 7;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 8;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 9;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 10;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 11;
		}
		if( strFieldName.equals("StandByString1") ) {
			return 12;
		}
		if( strFieldName.equals("StandByString2") ) {
			return 13;
		}
		if( strFieldName.equals("StandByNum1") ) {
			return 14;
		}
		if( strFieldName.equals("StandByNum2") ) {
			return 15;
		}
		if( strFieldName.equals("StandByDate1") ) {
			return 16;
		}
		if( strFieldName.equals("StandByDate2") ) {
			return 17;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "PlaceNo";
				break;
			case 1:
				strFieldName = "FeeType";
				break;
			case 2:
				strFieldName = "SOrder";
				break;
			case 3:
				strFieldName = "PayDate";
				break;
			case 4:
				strFieldName = "Money";
				break;
			case 5:
				strFieldName = "PayBeginDate";
				break;
			case 6:
				strFieldName = "PayEndDate";
				break;
			case 7:
				strFieldName = "Operator";
				break;
			case 8:
				strFieldName = "MakeDate";
				break;
			case 9:
				strFieldName = "MakeTime";
				break;
			case 10:
				strFieldName = "ModifyDate";
				break;
			case 11:
				strFieldName = "ModifyTime";
				break;
			case 12:
				strFieldName = "StandByString1";
				break;
			case 13:
				strFieldName = "StandByString2";
				break;
			case 14:
				strFieldName = "StandByNum1";
				break;
			case 15:
				strFieldName = "StandByNum2";
				break;
			case 16:
				strFieldName = "StandByDate1";
				break;
			case 17:
				strFieldName = "StandByDate2";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("PlaceNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FeeType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SOrder") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Money") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("PayBeginDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("PayEndDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandByString1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandByString2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandByNum1") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("StandByNum2") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("StandByDate1") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("StandByDate2") ) {
			return Schema.TYPE_DATE;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 4:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 5:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 6:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 15:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 16:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 17:
				nFieldType = Schema.TYPE_DATE;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
