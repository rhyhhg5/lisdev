/*
 * <p>ClassName: LWTransitionSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 工作流模型
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LWTransitionDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LWTransitionSchema implements Schema
{
    // @Field
    /** 转移id */
    private String TransitionID;
    /** 转移名 */
    private String TransitionName;
    /** 转移说明 */
    private String TransitionDesc;
    /** 转移起点 */
    private String TransitionStart;
    /** 转移终点 */
    private String TransitionEnd;
    /** 转移条件 */
    private String TransitionCond;
    /** 转移条件类型 */
    private String TransitionCondT;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 12; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LWTransitionSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "TransitionID";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getTransitionID()
    {
        if (TransitionID != null && !TransitionID.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            TransitionID = StrTool.unicodeToGBK(TransitionID);
        }
        return TransitionID;
    }

    public void setTransitionID(String aTransitionID)
    {
        TransitionID = aTransitionID;
    }

    public String getTransitionName()
    {
        if (TransitionName != null && !TransitionName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            TransitionName = StrTool.unicodeToGBK(TransitionName);
        }
        return TransitionName;
    }

    public void setTransitionName(String aTransitionName)
    {
        TransitionName = aTransitionName;
    }

    public String getTransitionDesc()
    {
        if (TransitionDesc != null && !TransitionDesc.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            TransitionDesc = StrTool.unicodeToGBK(TransitionDesc);
        }
        return TransitionDesc;
    }

    public void setTransitionDesc(String aTransitionDesc)
    {
        TransitionDesc = aTransitionDesc;
    }

    public String getTransitionStart()
    {
        if (TransitionStart != null && !TransitionStart.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            TransitionStart = StrTool.unicodeToGBK(TransitionStart);
        }
        return TransitionStart;
    }

    public void setTransitionStart(String aTransitionStart)
    {
        TransitionStart = aTransitionStart;
    }

    public String getTransitionEnd()
    {
        if (TransitionEnd != null && !TransitionEnd.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            TransitionEnd = StrTool.unicodeToGBK(TransitionEnd);
        }
        return TransitionEnd;
    }

    public void setTransitionEnd(String aTransitionEnd)
    {
        TransitionEnd = aTransitionEnd;
    }

    public String getTransitionCond()
    {
        if (TransitionCond != null && !TransitionCond.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            TransitionCond = StrTool.unicodeToGBK(TransitionCond);
        }
        return TransitionCond;
    }

    public void setTransitionCond(String aTransitionCond)
    {
        TransitionCond = aTransitionCond;
    }

    public String getTransitionCondT()
    {
        if (TransitionCondT != null && !TransitionCondT.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            TransitionCondT = StrTool.unicodeToGBK(TransitionCondT);
        }
        return TransitionCondT;
    }

    public void setTransitionCondT(String aTransitionCondT)
    {
        TransitionCondT = aTransitionCondT;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LWTransitionSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LWTransitionSchema aLWTransitionSchema)
    {
        this.TransitionID = aLWTransitionSchema.getTransitionID();
        this.TransitionName = aLWTransitionSchema.getTransitionName();
        this.TransitionDesc = aLWTransitionSchema.getTransitionDesc();
        this.TransitionStart = aLWTransitionSchema.getTransitionStart();
        this.TransitionEnd = aLWTransitionSchema.getTransitionEnd();
        this.TransitionCond = aLWTransitionSchema.getTransitionCond();
        this.TransitionCondT = aLWTransitionSchema.getTransitionCondT();
        this.Operator = aLWTransitionSchema.getOperator();
        this.MakeDate = fDate.getDate(aLWTransitionSchema.getMakeDate());
        this.MakeTime = aLWTransitionSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLWTransitionSchema.getModifyDate());
        this.ModifyTime = aLWTransitionSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("TransitionID") == null)
            {
                this.TransitionID = null;
            }
            else
            {
                this.TransitionID = rs.getString("TransitionID").trim();
            }

            if (rs.getString("TransitionName") == null)
            {
                this.TransitionName = null;
            }
            else
            {
                this.TransitionName = rs.getString("TransitionName").trim();
            }

            if (rs.getString("TransitionDesc") == null)
            {
                this.TransitionDesc = null;
            }
            else
            {
                this.TransitionDesc = rs.getString("TransitionDesc").trim();
            }

            if (rs.getString("TransitionStart") == null)
            {
                this.TransitionStart = null;
            }
            else
            {
                this.TransitionStart = rs.getString("TransitionStart").trim();
            }

            if (rs.getString("TransitionEnd") == null)
            {
                this.TransitionEnd = null;
            }
            else
            {
                this.TransitionEnd = rs.getString("TransitionEnd").trim();
            }

            if (rs.getString("TransitionCond") == null)
            {
                this.TransitionCond = null;
            }
            else
            {
                this.TransitionCond = rs.getString("TransitionCond").trim();
            }

            if (rs.getString("TransitionCondT") == null)
            {
                this.TransitionCondT = null;
            }
            else
            {
                this.TransitionCondT = rs.getString("TransitionCondT").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LWTransitionSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LWTransitionSchema getSchema()
    {
        LWTransitionSchema aLWTransitionSchema = new LWTransitionSchema();
        aLWTransitionSchema.setSchema(this);
        return aLWTransitionSchema;
    }

    public LWTransitionDB getDB()
    {
        LWTransitionDB aDBOper = new LWTransitionDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLWTransition描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(TransitionID)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(TransitionName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(TransitionDesc)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(TransitionStart)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(TransitionEnd)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(TransitionCond)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(TransitionCondT)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLWTransition>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            TransitionID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                          SysConst.PACKAGESPILTER);
            TransitionName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                            SysConst.PACKAGESPILTER);
            TransitionDesc = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                            SysConst.PACKAGESPILTER);
            TransitionStart = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             4, SysConst.PACKAGESPILTER);
            TransitionEnd = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                           SysConst.PACKAGESPILTER);
            TransitionCond = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                            SysConst.PACKAGESPILTER);
            TransitionCondT = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             7, SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 9, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 11, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LWTransitionSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("TransitionID"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(TransitionID));
        }
        if (FCode.equals("TransitionName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(TransitionName));
        }
        if (FCode.equals("TransitionDesc"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(TransitionDesc));
        }
        if (FCode.equals("TransitionStart"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(TransitionStart));
        }
        if (FCode.equals("TransitionEnd"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(TransitionEnd));
        }
        if (FCode.equals("TransitionCond"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(TransitionCond));
        }
        if (FCode.equals("TransitionCondT"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(TransitionCondT));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(TransitionID);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(TransitionName);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(TransitionDesc);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(TransitionStart);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(TransitionEnd);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(TransitionCond);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(TransitionCondT);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("TransitionID"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                TransitionID = FValue.trim();
            }
            else
            {
                TransitionID = null;
            }
        }
        if (FCode.equals("TransitionName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                TransitionName = FValue.trim();
            }
            else
            {
                TransitionName = null;
            }
        }
        if (FCode.equals("TransitionDesc"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                TransitionDesc = FValue.trim();
            }
            else
            {
                TransitionDesc = null;
            }
        }
        if (FCode.equals("TransitionStart"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                TransitionStart = FValue.trim();
            }
            else
            {
                TransitionStart = null;
            }
        }
        if (FCode.equals("TransitionEnd"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                TransitionEnd = FValue.trim();
            }
            else
            {
                TransitionEnd = null;
            }
        }
        if (FCode.equals("TransitionCond"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                TransitionCond = FValue.trim();
            }
            else
            {
                TransitionCond = null;
            }
        }
        if (FCode.equals("TransitionCondT"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                TransitionCondT = FValue.trim();
            }
            else
            {
                TransitionCondT = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LWTransitionSchema other = (LWTransitionSchema) otherObject;
        return
                TransitionID.equals(other.getTransitionID())
                && TransitionName.equals(other.getTransitionName())
                && TransitionDesc.equals(other.getTransitionDesc())
                && TransitionStart.equals(other.getTransitionStart())
                && TransitionEnd.equals(other.getTransitionEnd())
                && TransitionCond.equals(other.getTransitionCond())
                && TransitionCondT.equals(other.getTransitionCondT())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("TransitionID"))
        {
            return 0;
        }
        if (strFieldName.equals("TransitionName"))
        {
            return 1;
        }
        if (strFieldName.equals("TransitionDesc"))
        {
            return 2;
        }
        if (strFieldName.equals("TransitionStart"))
        {
            return 3;
        }
        if (strFieldName.equals("TransitionEnd"))
        {
            return 4;
        }
        if (strFieldName.equals("TransitionCond"))
        {
            return 5;
        }
        if (strFieldName.equals("TransitionCondT"))
        {
            return 6;
        }
        if (strFieldName.equals("Operator"))
        {
            return 7;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 8;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 9;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 10;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 11;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "TransitionID";
                break;
            case 1:
                strFieldName = "TransitionName";
                break;
            case 2:
                strFieldName = "TransitionDesc";
                break;
            case 3:
                strFieldName = "TransitionStart";
                break;
            case 4:
                strFieldName = "TransitionEnd";
                break;
            case 5:
                strFieldName = "TransitionCond";
                break;
            case 6:
                strFieldName = "TransitionCondT";
                break;
            case 7:
                strFieldName = "Operator";
                break;
            case 8:
                strFieldName = "MakeDate";
                break;
            case 9:
                strFieldName = "MakeTime";
                break;
            case 10:
                strFieldName = "ModifyDate";
                break;
            case 11:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("TransitionID"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("TransitionName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("TransitionDesc"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("TransitionStart"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("TransitionEnd"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("TransitionCond"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("TransitionCondT"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
