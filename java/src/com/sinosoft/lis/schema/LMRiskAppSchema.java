/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LMRiskAppDB;

/*
 * <p>ClassName: LMRiskAppSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 保单登记表结构扩充
 * @CreateDate：2018-07-11
 */
public class LMRiskAppSchema implements Schema, Cloneable
{
	// @Field
	/** 险种编码 */
	private String RiskCode;
	/** 险种版本 */
	private String RiskVer;
	/** 险种名称 */
	private String RiskName;
	/** 险类编码 */
	private String KindCode;
	/** 险种分类 */
	private String RiskType;
	/** 险种分类1 */
	private String RiskType1;
	/** 险种分类2 */
	private String RiskType2;
	/** 险种性质 */
	private String RiskProp;
	/** 险种类别 */
	private String RiskPeriod;
	/** 险种细分 */
	private String RiskTypeDetail;
	/** 险种标记 */
	private String RiskFlag;
	/** 保单类型 */
	private String PolType;
	/** 投资标记 */
	private String InvestFlag;
	/** 分红标记 */
	private String BonusFlag;
	/** 红利领取方式 */
	private String BonusMode;
	/** 有无名单标记 */
	private String ListFlag;
	/** 主附险标记 */
	private String SubRiskFlag;
	/** 计算精确位 */
	private int CalDigital;
	/** 计算取舍方法 */
	private String CalChoMode;
	/** 风险保额倍数 */
	private int RiskAmntMult;
	/** 保险期限标志 */
	private String InsuPeriodFlag;
	/** 保险期间上限 */
	private int MaxEndPeriod;
	/** 满期截至年龄 */
	private int AgeLmt;
	/** 签单日算法 */
	private int SignDateCalMode;
	/** 协议险标记 */
	private String ProtocolFlag;
	/** 协议险给付金改变标记 */
	private String GetChgFlag;
	/** 协议缴费标记 */
	private String ProtocolPayFlag;
	/** 保障计划标记 */
	private String EnsuPlanFlag;
	/** 保障计划调整标记 */
	private String EnsuPlanAdjFlag;
	/** 开办日期 */
	private Date StartDate;
	/** 停办日期 */
	private Date EndDate;
	/** 最小投保人年龄 */
	private int MinAppntAge;
	/** 最大投保人年龄 */
	private int MaxAppntAge;
	/** 最大被保人年龄 */
	private int MaxInsuredAge;
	/** 最小被保人年龄 */
	private int MinInsuredAge;
	/** 投保使用利率 */
	private double AppInterest;
	/** 投保使用费率 */
	private double AppPremRate;
	/** 多被保人标记 */
	private String InsuredFlag;
	/** 共保标记 */
	private String ShareFlag;
	/** 受益人标记 */
	private String BnfFlag;
	/** 暂缴费标记 */
	private String TempPayFlag;
	/** 录入缴费项标记 */
	private String InpPayPlan;
	/** 告知标记 */
	private String ImpartFlag;
	/** 保险经历标记 */
	private String InsuExpeFlag;
	/** 提供借款标记 */
	private String LoanFalg;
	/** 抵押标记 */
	private String MortagageFlag;
	/** 利差返还标记 */
	private String IDifReturnFlag;
	/** 减额缴清标记 */
	private String CutAmntStopPay;
	/** 分保率 */
	private double RinsRate;
	/** 销售标记 */
	private String SaleFlag;
	/** 磁盘文件投保标记 */
	private String FileAppFlag;
	/** 管理部门 */
	private String MngCom;
	/** 自动垫缴标志 */
	private String AutoPayFlag;
	/** 是否打印医院列表标记 */
	private String NeedPrintHospital;
	/** 是否打印伤残给付表标记 */
	private String NeedPrintGet;
	/** 险种分类3 */
	private String RiskType3;
	/** 险种分类4 */
	private String RiskType4;
	/** 险种分类5 */
	private String RiskType5;
	/** 签单后不需要打印 */
	private String NotPrintPol;
	/** 录单时是否需要设置保单送达日期 */
	private String NeedGetPolDate;
	/** 是否从暂缴费表中读取银行的账号和户名 */
	private String NeedReReadBank;
	/** 险种分类6 */
	private String RiskType6;
	/** 险种分类7 */
	private String RiskType7;
	/** 险种分类8 */
	private String RiskType8;
	/** 险种分类9 */
	private String RiskType9;
	/** 被保险人年龄单位 */
	private String InsuredAgeFlag;
	/** 税优险标记 */
	private String TaxOptimal;
	/** 最大续保年龄 */
	private String MaxRnewAge;

	public static final int FIELDNUM = 68;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LMRiskAppSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "RiskCode";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LMRiskAppSchema cloned = (LMRiskAppSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getRiskCode()
	{
		return RiskCode;
	}
	public void setRiskCode(String aRiskCode)
	{
		RiskCode = aRiskCode;
	}
	public String getRiskVer()
	{
		return RiskVer;
	}
	public void setRiskVer(String aRiskVer)
	{
		RiskVer = aRiskVer;
	}
	public String getRiskName()
	{
		return RiskName;
	}
	public void setRiskName(String aRiskName)
	{
		RiskName = aRiskName;
	}
	public String getKindCode()
	{
		return KindCode;
	}
	public void setKindCode(String aKindCode)
	{
		KindCode = aKindCode;
	}
	public String getRiskType()
	{
		return RiskType;
	}
	public void setRiskType(String aRiskType)
	{
		RiskType = aRiskType;
	}
	public String getRiskType1()
	{
		return RiskType1;
	}
	public void setRiskType1(String aRiskType1)
	{
		RiskType1 = aRiskType1;
	}
	public String getRiskType2()
	{
		return RiskType2;
	}
	public void setRiskType2(String aRiskType2)
	{
		RiskType2 = aRiskType2;
	}
	public String getRiskProp()
	{
		return RiskProp;
	}
	public void setRiskProp(String aRiskProp)
	{
		RiskProp = aRiskProp;
	}
	public String getRiskPeriod()
	{
		return RiskPeriod;
	}
	public void setRiskPeriod(String aRiskPeriod)
	{
		RiskPeriod = aRiskPeriod;
	}
	public String getRiskTypeDetail()
	{
		return RiskTypeDetail;
	}
	public void setRiskTypeDetail(String aRiskTypeDetail)
	{
		RiskTypeDetail = aRiskTypeDetail;
	}
	public String getRiskFlag()
	{
		return RiskFlag;
	}
	public void setRiskFlag(String aRiskFlag)
	{
		RiskFlag = aRiskFlag;
	}
	public String getPolType()
	{
		return PolType;
	}
	public void setPolType(String aPolType)
	{
		PolType = aPolType;
	}
	public String getInvestFlag()
	{
		return InvestFlag;
	}
	public void setInvestFlag(String aInvestFlag)
	{
		InvestFlag = aInvestFlag;
	}
	public String getBonusFlag()
	{
		return BonusFlag;
	}
	public void setBonusFlag(String aBonusFlag)
	{
		BonusFlag = aBonusFlag;
	}
	public String getBonusMode()
	{
		return BonusMode;
	}
	public void setBonusMode(String aBonusMode)
	{
		BonusMode = aBonusMode;
	}
	public String getListFlag()
	{
		return ListFlag;
	}
	public void setListFlag(String aListFlag)
	{
		ListFlag = aListFlag;
	}
	public String getSubRiskFlag()
	{
		return SubRiskFlag;
	}
	public void setSubRiskFlag(String aSubRiskFlag)
	{
		SubRiskFlag = aSubRiskFlag;
	}
	public int getCalDigital()
	{
		return CalDigital;
	}
	public void setCalDigital(int aCalDigital)
	{
		CalDigital = aCalDigital;
	}
	public void setCalDigital(String aCalDigital)
	{
		if (aCalDigital != null && !aCalDigital.equals(""))
		{
			Integer tInteger = new Integer(aCalDigital);
			int i = tInteger.intValue();
			CalDigital = i;
		}
	}

	public String getCalChoMode()
	{
		return CalChoMode;
	}
	public void setCalChoMode(String aCalChoMode)
	{
		CalChoMode = aCalChoMode;
	}
	public int getRiskAmntMult()
	{
		return RiskAmntMult;
	}
	public void setRiskAmntMult(int aRiskAmntMult)
	{
		RiskAmntMult = aRiskAmntMult;
	}
	public void setRiskAmntMult(String aRiskAmntMult)
	{
		if (aRiskAmntMult != null && !aRiskAmntMult.equals(""))
		{
			Integer tInteger = new Integer(aRiskAmntMult);
			int i = tInteger.intValue();
			RiskAmntMult = i;
		}
	}

	public String getInsuPeriodFlag()
	{
		return InsuPeriodFlag;
	}
	public void setInsuPeriodFlag(String aInsuPeriodFlag)
	{
		InsuPeriodFlag = aInsuPeriodFlag;
	}
	public int getMaxEndPeriod()
	{
		return MaxEndPeriod;
	}
	public void setMaxEndPeriod(int aMaxEndPeriod)
	{
		MaxEndPeriod = aMaxEndPeriod;
	}
	public void setMaxEndPeriod(String aMaxEndPeriod)
	{
		if (aMaxEndPeriod != null && !aMaxEndPeriod.equals(""))
		{
			Integer tInteger = new Integer(aMaxEndPeriod);
			int i = tInteger.intValue();
			MaxEndPeriod = i;
		}
	}

	public int getAgeLmt()
	{
		return AgeLmt;
	}
	public void setAgeLmt(int aAgeLmt)
	{
		AgeLmt = aAgeLmt;
	}
	public void setAgeLmt(String aAgeLmt)
	{
		if (aAgeLmt != null && !aAgeLmt.equals(""))
		{
			Integer tInteger = new Integer(aAgeLmt);
			int i = tInteger.intValue();
			AgeLmt = i;
		}
	}

	public int getSignDateCalMode()
	{
		return SignDateCalMode;
	}
	public void setSignDateCalMode(int aSignDateCalMode)
	{
		SignDateCalMode = aSignDateCalMode;
	}
	public void setSignDateCalMode(String aSignDateCalMode)
	{
		if (aSignDateCalMode != null && !aSignDateCalMode.equals(""))
		{
			Integer tInteger = new Integer(aSignDateCalMode);
			int i = tInteger.intValue();
			SignDateCalMode = i;
		}
	}

	public String getProtocolFlag()
	{
		return ProtocolFlag;
	}
	public void setProtocolFlag(String aProtocolFlag)
	{
		ProtocolFlag = aProtocolFlag;
	}
	public String getGetChgFlag()
	{
		return GetChgFlag;
	}
	public void setGetChgFlag(String aGetChgFlag)
	{
		GetChgFlag = aGetChgFlag;
	}
	public String getProtocolPayFlag()
	{
		return ProtocolPayFlag;
	}
	public void setProtocolPayFlag(String aProtocolPayFlag)
	{
		ProtocolPayFlag = aProtocolPayFlag;
	}
	public String getEnsuPlanFlag()
	{
		return EnsuPlanFlag;
	}
	public void setEnsuPlanFlag(String aEnsuPlanFlag)
	{
		EnsuPlanFlag = aEnsuPlanFlag;
	}
	public String getEnsuPlanAdjFlag()
	{
		return EnsuPlanAdjFlag;
	}
	public void setEnsuPlanAdjFlag(String aEnsuPlanAdjFlag)
	{
		EnsuPlanAdjFlag = aEnsuPlanAdjFlag;
	}
	public String getStartDate()
	{
		if( StartDate != null )
			return fDate.getString(StartDate);
		else
			return null;
	}
	public void setStartDate(Date aStartDate)
	{
		StartDate = aStartDate;
	}
	public void setStartDate(String aStartDate)
	{
		if (aStartDate != null && !aStartDate.equals("") )
		{
			StartDate = fDate.getDate( aStartDate );
		}
		else
			StartDate = null;
	}

	public String getEndDate()
	{
		if( EndDate != null )
			return fDate.getString(EndDate);
		else
			return null;
	}
	public void setEndDate(Date aEndDate)
	{
		EndDate = aEndDate;
	}
	public void setEndDate(String aEndDate)
	{
		if (aEndDate != null && !aEndDate.equals("") )
		{
			EndDate = fDate.getDate( aEndDate );
		}
		else
			EndDate = null;
	}

	public int getMinAppntAge()
	{
		return MinAppntAge;
	}
	public void setMinAppntAge(int aMinAppntAge)
	{
		MinAppntAge = aMinAppntAge;
	}
	public void setMinAppntAge(String aMinAppntAge)
	{
		if (aMinAppntAge != null && !aMinAppntAge.equals(""))
		{
			Integer tInteger = new Integer(aMinAppntAge);
			int i = tInteger.intValue();
			MinAppntAge = i;
		}
	}

	public int getMaxAppntAge()
	{
		return MaxAppntAge;
	}
	public void setMaxAppntAge(int aMaxAppntAge)
	{
		MaxAppntAge = aMaxAppntAge;
	}
	public void setMaxAppntAge(String aMaxAppntAge)
	{
		if (aMaxAppntAge != null && !aMaxAppntAge.equals(""))
		{
			Integer tInteger = new Integer(aMaxAppntAge);
			int i = tInteger.intValue();
			MaxAppntAge = i;
		}
	}

	public int getMaxInsuredAge()
	{
		return MaxInsuredAge;
	}
	public void setMaxInsuredAge(int aMaxInsuredAge)
	{
		MaxInsuredAge = aMaxInsuredAge;
	}
	public void setMaxInsuredAge(String aMaxInsuredAge)
	{
		if (aMaxInsuredAge != null && !aMaxInsuredAge.equals(""))
		{
			Integer tInteger = new Integer(aMaxInsuredAge);
			int i = tInteger.intValue();
			MaxInsuredAge = i;
		}
	}

	public int getMinInsuredAge()
	{
		return MinInsuredAge;
	}
	public void setMinInsuredAge(int aMinInsuredAge)
	{
		MinInsuredAge = aMinInsuredAge;
	}
	public void setMinInsuredAge(String aMinInsuredAge)
	{
		if (aMinInsuredAge != null && !aMinInsuredAge.equals(""))
		{
			Integer tInteger = new Integer(aMinInsuredAge);
			int i = tInteger.intValue();
			MinInsuredAge = i;
		}
	}

	public double getAppInterest()
	{
		return AppInterest;
	}
	public void setAppInterest(double aAppInterest)
	{
		AppInterest = Arith.round(aAppInterest,4);
	}
	public void setAppInterest(String aAppInterest)
	{
		if (aAppInterest != null && !aAppInterest.equals(""))
		{
			Double tDouble = new Double(aAppInterest);
			double d = tDouble.doubleValue();
                AppInterest = Arith.round(d,4);
		}
	}

	public double getAppPremRate()
	{
		return AppPremRate;
	}
	public void setAppPremRate(double aAppPremRate)
	{
		AppPremRate = Arith.round(aAppPremRate,4);
	}
	public void setAppPremRate(String aAppPremRate)
	{
		if (aAppPremRate != null && !aAppPremRate.equals(""))
		{
			Double tDouble = new Double(aAppPremRate);
			double d = tDouble.doubleValue();
                AppPremRate = Arith.round(d,4);
		}
	}

	public String getInsuredFlag()
	{
		return InsuredFlag;
	}
	public void setInsuredFlag(String aInsuredFlag)
	{
		InsuredFlag = aInsuredFlag;
	}
	public String getShareFlag()
	{
		return ShareFlag;
	}
	public void setShareFlag(String aShareFlag)
	{
		ShareFlag = aShareFlag;
	}
	public String getBnfFlag()
	{
		return BnfFlag;
	}
	public void setBnfFlag(String aBnfFlag)
	{
		BnfFlag = aBnfFlag;
	}
	public String getTempPayFlag()
	{
		return TempPayFlag;
	}
	public void setTempPayFlag(String aTempPayFlag)
	{
		TempPayFlag = aTempPayFlag;
	}
	public String getInpPayPlan()
	{
		return InpPayPlan;
	}
	public void setInpPayPlan(String aInpPayPlan)
	{
		InpPayPlan = aInpPayPlan;
	}
	public String getImpartFlag()
	{
		return ImpartFlag;
	}
	public void setImpartFlag(String aImpartFlag)
	{
		ImpartFlag = aImpartFlag;
	}
	public String getInsuExpeFlag()
	{
		return InsuExpeFlag;
	}
	public void setInsuExpeFlag(String aInsuExpeFlag)
	{
		InsuExpeFlag = aInsuExpeFlag;
	}
	public String getLoanFalg()
	{
		return LoanFalg;
	}
	public void setLoanFalg(String aLoanFalg)
	{
		LoanFalg = aLoanFalg;
	}
	public String getMortagageFlag()
	{
		return MortagageFlag;
	}
	public void setMortagageFlag(String aMortagageFlag)
	{
		MortagageFlag = aMortagageFlag;
	}
	public String getIDifReturnFlag()
	{
		return IDifReturnFlag;
	}
	public void setIDifReturnFlag(String aIDifReturnFlag)
	{
		IDifReturnFlag = aIDifReturnFlag;
	}
	public String getCutAmntStopPay()
	{
		return CutAmntStopPay;
	}
	public void setCutAmntStopPay(String aCutAmntStopPay)
	{
		CutAmntStopPay = aCutAmntStopPay;
	}
	public double getRinsRate()
	{
		return RinsRate;
	}
	public void setRinsRate(double aRinsRate)
	{
		RinsRate = Arith.round(aRinsRate,4);
	}
	public void setRinsRate(String aRinsRate)
	{
		if (aRinsRate != null && !aRinsRate.equals(""))
		{
			Double tDouble = new Double(aRinsRate);
			double d = tDouble.doubleValue();
                RinsRate = Arith.round(d,4);
		}
	}

	public String getSaleFlag()
	{
		return SaleFlag;
	}
	public void setSaleFlag(String aSaleFlag)
	{
		SaleFlag = aSaleFlag;
	}
	public String getFileAppFlag()
	{
		return FileAppFlag;
	}
	public void setFileAppFlag(String aFileAppFlag)
	{
		FileAppFlag = aFileAppFlag;
	}
	public String getMngCom()
	{
		return MngCom;
	}
	public void setMngCom(String aMngCom)
	{
		MngCom = aMngCom;
	}
	public String getAutoPayFlag()
	{
		return AutoPayFlag;
	}
	public void setAutoPayFlag(String aAutoPayFlag)
	{
		AutoPayFlag = aAutoPayFlag;
	}
	public String getNeedPrintHospital()
	{
		return NeedPrintHospital;
	}
	public void setNeedPrintHospital(String aNeedPrintHospital)
	{
		NeedPrintHospital = aNeedPrintHospital;
	}
	public String getNeedPrintGet()
	{
		return NeedPrintGet;
	}
	public void setNeedPrintGet(String aNeedPrintGet)
	{
		NeedPrintGet = aNeedPrintGet;
	}
	public String getRiskType3()
	{
		return RiskType3;
	}
	public void setRiskType3(String aRiskType3)
	{
		RiskType3 = aRiskType3;
	}
	public String getRiskType4()
	{
		return RiskType4;
	}
	public void setRiskType4(String aRiskType4)
	{
		RiskType4 = aRiskType4;
	}
	public String getRiskType5()
	{
		return RiskType5;
	}
	public void setRiskType5(String aRiskType5)
	{
		RiskType5 = aRiskType5;
	}
	public String getNotPrintPol()
	{
		return NotPrintPol;
	}
	public void setNotPrintPol(String aNotPrintPol)
	{
		NotPrintPol = aNotPrintPol;
	}
	public String getNeedGetPolDate()
	{
		return NeedGetPolDate;
	}
	public void setNeedGetPolDate(String aNeedGetPolDate)
	{
		NeedGetPolDate = aNeedGetPolDate;
	}
	public String getNeedReReadBank()
	{
		return NeedReReadBank;
	}
	public void setNeedReReadBank(String aNeedReReadBank)
	{
		NeedReReadBank = aNeedReReadBank;
	}
	public String getRiskType6()
	{
		return RiskType6;
	}
	public void setRiskType6(String aRiskType6)
	{
		RiskType6 = aRiskType6;
	}
	public String getRiskType7()
	{
		return RiskType7;
	}
	public void setRiskType7(String aRiskType7)
	{
		RiskType7 = aRiskType7;
	}
	public String getRiskType8()
	{
		return RiskType8;
	}
	public void setRiskType8(String aRiskType8)
	{
		RiskType8 = aRiskType8;
	}
	public String getRiskType9()
	{
		return RiskType9;
	}
	public void setRiskType9(String aRiskType9)
	{
		RiskType9 = aRiskType9;
	}
	public String getInsuredAgeFlag()
	{
		return InsuredAgeFlag;
	}
	public void setInsuredAgeFlag(String aInsuredAgeFlag)
	{
		InsuredAgeFlag = aInsuredAgeFlag;
	}
	public String getTaxOptimal()
	{
		return TaxOptimal;
	}
	public void setTaxOptimal(String aTaxOptimal)
	{
		TaxOptimal = aTaxOptimal;
	}
	public String getMaxRnewAge()
	{
		return MaxRnewAge;
	}
	public void setMaxRnewAge(String aMaxRnewAge)
	{
		MaxRnewAge = aMaxRnewAge;
	}

	/**
	* 使用另外一个 LMRiskAppSchema 对象给 Schema 赋值
	* @param: aLMRiskAppSchema LMRiskAppSchema
	**/
	public void setSchema(LMRiskAppSchema aLMRiskAppSchema)
	{
		this.RiskCode = aLMRiskAppSchema.getRiskCode();
		this.RiskVer = aLMRiskAppSchema.getRiskVer();
		this.RiskName = aLMRiskAppSchema.getRiskName();
		this.KindCode = aLMRiskAppSchema.getKindCode();
		this.RiskType = aLMRiskAppSchema.getRiskType();
		this.RiskType1 = aLMRiskAppSchema.getRiskType1();
		this.RiskType2 = aLMRiskAppSchema.getRiskType2();
		this.RiskProp = aLMRiskAppSchema.getRiskProp();
		this.RiskPeriod = aLMRiskAppSchema.getRiskPeriod();
		this.RiskTypeDetail = aLMRiskAppSchema.getRiskTypeDetail();
		this.RiskFlag = aLMRiskAppSchema.getRiskFlag();
		this.PolType = aLMRiskAppSchema.getPolType();
		this.InvestFlag = aLMRiskAppSchema.getInvestFlag();
		this.BonusFlag = aLMRiskAppSchema.getBonusFlag();
		this.BonusMode = aLMRiskAppSchema.getBonusMode();
		this.ListFlag = aLMRiskAppSchema.getListFlag();
		this.SubRiskFlag = aLMRiskAppSchema.getSubRiskFlag();
		this.CalDigital = aLMRiskAppSchema.getCalDigital();
		this.CalChoMode = aLMRiskAppSchema.getCalChoMode();
		this.RiskAmntMult = aLMRiskAppSchema.getRiskAmntMult();
		this.InsuPeriodFlag = aLMRiskAppSchema.getInsuPeriodFlag();
		this.MaxEndPeriod = aLMRiskAppSchema.getMaxEndPeriod();
		this.AgeLmt = aLMRiskAppSchema.getAgeLmt();
		this.SignDateCalMode = aLMRiskAppSchema.getSignDateCalMode();
		this.ProtocolFlag = aLMRiskAppSchema.getProtocolFlag();
		this.GetChgFlag = aLMRiskAppSchema.getGetChgFlag();
		this.ProtocolPayFlag = aLMRiskAppSchema.getProtocolPayFlag();
		this.EnsuPlanFlag = aLMRiskAppSchema.getEnsuPlanFlag();
		this.EnsuPlanAdjFlag = aLMRiskAppSchema.getEnsuPlanAdjFlag();
		this.StartDate = fDate.getDate( aLMRiskAppSchema.getStartDate());
		this.EndDate = fDate.getDate( aLMRiskAppSchema.getEndDate());
		this.MinAppntAge = aLMRiskAppSchema.getMinAppntAge();
		this.MaxAppntAge = aLMRiskAppSchema.getMaxAppntAge();
		this.MaxInsuredAge = aLMRiskAppSchema.getMaxInsuredAge();
		this.MinInsuredAge = aLMRiskAppSchema.getMinInsuredAge();
		this.AppInterest = aLMRiskAppSchema.getAppInterest();
		this.AppPremRate = aLMRiskAppSchema.getAppPremRate();
		this.InsuredFlag = aLMRiskAppSchema.getInsuredFlag();
		this.ShareFlag = aLMRiskAppSchema.getShareFlag();
		this.BnfFlag = aLMRiskAppSchema.getBnfFlag();
		this.TempPayFlag = aLMRiskAppSchema.getTempPayFlag();
		this.InpPayPlan = aLMRiskAppSchema.getInpPayPlan();
		this.ImpartFlag = aLMRiskAppSchema.getImpartFlag();
		this.InsuExpeFlag = aLMRiskAppSchema.getInsuExpeFlag();
		this.LoanFalg = aLMRiskAppSchema.getLoanFalg();
		this.MortagageFlag = aLMRiskAppSchema.getMortagageFlag();
		this.IDifReturnFlag = aLMRiskAppSchema.getIDifReturnFlag();
		this.CutAmntStopPay = aLMRiskAppSchema.getCutAmntStopPay();
		this.RinsRate = aLMRiskAppSchema.getRinsRate();
		this.SaleFlag = aLMRiskAppSchema.getSaleFlag();
		this.FileAppFlag = aLMRiskAppSchema.getFileAppFlag();
		this.MngCom = aLMRiskAppSchema.getMngCom();
		this.AutoPayFlag = aLMRiskAppSchema.getAutoPayFlag();
		this.NeedPrintHospital = aLMRiskAppSchema.getNeedPrintHospital();
		this.NeedPrintGet = aLMRiskAppSchema.getNeedPrintGet();
		this.RiskType3 = aLMRiskAppSchema.getRiskType3();
		this.RiskType4 = aLMRiskAppSchema.getRiskType4();
		this.RiskType5 = aLMRiskAppSchema.getRiskType5();
		this.NotPrintPol = aLMRiskAppSchema.getNotPrintPol();
		this.NeedGetPolDate = aLMRiskAppSchema.getNeedGetPolDate();
		this.NeedReReadBank = aLMRiskAppSchema.getNeedReReadBank();
		this.RiskType6 = aLMRiskAppSchema.getRiskType6();
		this.RiskType7 = aLMRiskAppSchema.getRiskType7();
		this.RiskType8 = aLMRiskAppSchema.getRiskType8();
		this.RiskType9 = aLMRiskAppSchema.getRiskType9();
		this.InsuredAgeFlag = aLMRiskAppSchema.getInsuredAgeFlag();
		this.TaxOptimal = aLMRiskAppSchema.getTaxOptimal();
		this.MaxRnewAge = aLMRiskAppSchema.getMaxRnewAge();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("RiskCode") == null )
				this.RiskCode = null;
			else
				this.RiskCode = rs.getString("RiskCode").trim();

			if( rs.getString("RiskVer") == null )
				this.RiskVer = null;
			else
				this.RiskVer = rs.getString("RiskVer").trim();

			if( rs.getString("RiskName") == null )
				this.RiskName = null;
			else
				this.RiskName = rs.getString("RiskName").trim();

			if( rs.getString("KindCode") == null )
				this.KindCode = null;
			else
				this.KindCode = rs.getString("KindCode").trim();

			if( rs.getString("RiskType") == null )
				this.RiskType = null;
			else
				this.RiskType = rs.getString("RiskType").trim();

			if( rs.getString("RiskType1") == null )
				this.RiskType1 = null;
			else
				this.RiskType1 = rs.getString("RiskType1").trim();

			if( rs.getString("RiskType2") == null )
				this.RiskType2 = null;
			else
				this.RiskType2 = rs.getString("RiskType2").trim();

			if( rs.getString("RiskProp") == null )
				this.RiskProp = null;
			else
				this.RiskProp = rs.getString("RiskProp").trim();

			if( rs.getString("RiskPeriod") == null )
				this.RiskPeriod = null;
			else
				this.RiskPeriod = rs.getString("RiskPeriod").trim();

			if( rs.getString("RiskTypeDetail") == null )
				this.RiskTypeDetail = null;
			else
				this.RiskTypeDetail = rs.getString("RiskTypeDetail").trim();

			if( rs.getString("RiskFlag") == null )
				this.RiskFlag = null;
			else
				this.RiskFlag = rs.getString("RiskFlag").trim();

			if( rs.getString("PolType") == null )
				this.PolType = null;
			else
				this.PolType = rs.getString("PolType").trim();

			if( rs.getString("InvestFlag") == null )
				this.InvestFlag = null;
			else
				this.InvestFlag = rs.getString("InvestFlag").trim();

			if( rs.getString("BonusFlag") == null )
				this.BonusFlag = null;
			else
				this.BonusFlag = rs.getString("BonusFlag").trim();

			if( rs.getString("BonusMode") == null )
				this.BonusMode = null;
			else
				this.BonusMode = rs.getString("BonusMode").trim();

			if( rs.getString("ListFlag") == null )
				this.ListFlag = null;
			else
				this.ListFlag = rs.getString("ListFlag").trim();

			if( rs.getString("SubRiskFlag") == null )
				this.SubRiskFlag = null;
			else
				this.SubRiskFlag = rs.getString("SubRiskFlag").trim();

			this.CalDigital = rs.getInt("CalDigital");
			if( rs.getString("CalChoMode") == null )
				this.CalChoMode = null;
			else
				this.CalChoMode = rs.getString("CalChoMode").trim();

			this.RiskAmntMult = rs.getInt("RiskAmntMult");
			if( rs.getString("InsuPeriodFlag") == null )
				this.InsuPeriodFlag = null;
			else
				this.InsuPeriodFlag = rs.getString("InsuPeriodFlag").trim();

			this.MaxEndPeriod = rs.getInt("MaxEndPeriod");
			this.AgeLmt = rs.getInt("AgeLmt");
			this.SignDateCalMode = rs.getInt("SignDateCalMode");
			if( rs.getString("ProtocolFlag") == null )
				this.ProtocolFlag = null;
			else
				this.ProtocolFlag = rs.getString("ProtocolFlag").trim();

			if( rs.getString("GetChgFlag") == null )
				this.GetChgFlag = null;
			else
				this.GetChgFlag = rs.getString("GetChgFlag").trim();

			if( rs.getString("ProtocolPayFlag") == null )
				this.ProtocolPayFlag = null;
			else
				this.ProtocolPayFlag = rs.getString("ProtocolPayFlag").trim();

			if( rs.getString("EnsuPlanFlag") == null )
				this.EnsuPlanFlag = null;
			else
				this.EnsuPlanFlag = rs.getString("EnsuPlanFlag").trim();

			if( rs.getString("EnsuPlanAdjFlag") == null )
				this.EnsuPlanAdjFlag = null;
			else
				this.EnsuPlanAdjFlag = rs.getString("EnsuPlanAdjFlag").trim();

			this.StartDate = rs.getDate("StartDate");
			this.EndDate = rs.getDate("EndDate");
			this.MinAppntAge = rs.getInt("MinAppntAge");
			this.MaxAppntAge = rs.getInt("MaxAppntAge");
			this.MaxInsuredAge = rs.getInt("MaxInsuredAge");
			this.MinInsuredAge = rs.getInt("MinInsuredAge");
			this.AppInterest = rs.getDouble("AppInterest");
			this.AppPremRate = rs.getDouble("AppPremRate");
			if( rs.getString("InsuredFlag") == null )
				this.InsuredFlag = null;
			else
				this.InsuredFlag = rs.getString("InsuredFlag").trim();

			if( rs.getString("ShareFlag") == null )
				this.ShareFlag = null;
			else
				this.ShareFlag = rs.getString("ShareFlag").trim();

			if( rs.getString("BnfFlag") == null )
				this.BnfFlag = null;
			else
				this.BnfFlag = rs.getString("BnfFlag").trim();

			if( rs.getString("TempPayFlag") == null )
				this.TempPayFlag = null;
			else
				this.TempPayFlag = rs.getString("TempPayFlag").trim();

			if( rs.getString("InpPayPlan") == null )
				this.InpPayPlan = null;
			else
				this.InpPayPlan = rs.getString("InpPayPlan").trim();

			if( rs.getString("ImpartFlag") == null )
				this.ImpartFlag = null;
			else
				this.ImpartFlag = rs.getString("ImpartFlag").trim();

			if( rs.getString("InsuExpeFlag") == null )
				this.InsuExpeFlag = null;
			else
				this.InsuExpeFlag = rs.getString("InsuExpeFlag").trim();

			if( rs.getString("LoanFalg") == null )
				this.LoanFalg = null;
			else
				this.LoanFalg = rs.getString("LoanFalg").trim();

			if( rs.getString("MortagageFlag") == null )
				this.MortagageFlag = null;
			else
				this.MortagageFlag = rs.getString("MortagageFlag").trim();

			if( rs.getString("IDifReturnFlag") == null )
				this.IDifReturnFlag = null;
			else
				this.IDifReturnFlag = rs.getString("IDifReturnFlag").trim();

			if( rs.getString("CutAmntStopPay") == null )
				this.CutAmntStopPay = null;
			else
				this.CutAmntStopPay = rs.getString("CutAmntStopPay").trim();

			this.RinsRate = rs.getDouble("RinsRate");
			if( rs.getString("SaleFlag") == null )
				this.SaleFlag = null;
			else
				this.SaleFlag = rs.getString("SaleFlag").trim();

			if( rs.getString("FileAppFlag") == null )
				this.FileAppFlag = null;
			else
				this.FileAppFlag = rs.getString("FileAppFlag").trim();

			if( rs.getString("MngCom") == null )
				this.MngCom = null;
			else
				this.MngCom = rs.getString("MngCom").trim();

			if( rs.getString("AutoPayFlag") == null )
				this.AutoPayFlag = null;
			else
				this.AutoPayFlag = rs.getString("AutoPayFlag").trim();

			if( rs.getString("NeedPrintHospital") == null )
				this.NeedPrintHospital = null;
			else
				this.NeedPrintHospital = rs.getString("NeedPrintHospital").trim();

			if( rs.getString("NeedPrintGet") == null )
				this.NeedPrintGet = null;
			else
				this.NeedPrintGet = rs.getString("NeedPrintGet").trim();

			if( rs.getString("RiskType3") == null )
				this.RiskType3 = null;
			else
				this.RiskType3 = rs.getString("RiskType3").trim();

			if( rs.getString("RiskType4") == null )
				this.RiskType4 = null;
			else
				this.RiskType4 = rs.getString("RiskType4").trim();

			if( rs.getString("RiskType5") == null )
				this.RiskType5 = null;
			else
				this.RiskType5 = rs.getString("RiskType5").trim();

			if( rs.getString("NotPrintPol") == null )
				this.NotPrintPol = null;
			else
				this.NotPrintPol = rs.getString("NotPrintPol").trim();

			if( rs.getString("NeedGetPolDate") == null )
				this.NeedGetPolDate = null;
			else
				this.NeedGetPolDate = rs.getString("NeedGetPolDate").trim();

			if( rs.getString("NeedReReadBank") == null )
				this.NeedReReadBank = null;
			else
				this.NeedReReadBank = rs.getString("NeedReReadBank").trim();

			if( rs.getString("RiskType6") == null )
				this.RiskType6 = null;
			else
				this.RiskType6 = rs.getString("RiskType6").trim();

			if( rs.getString("RiskType7") == null )
				this.RiskType7 = null;
			else
				this.RiskType7 = rs.getString("RiskType7").trim();

			if( rs.getString("RiskType8") == null )
				this.RiskType8 = null;
			else
				this.RiskType8 = rs.getString("RiskType8").trim();

			if( rs.getString("RiskType9") == null )
				this.RiskType9 = null;
			else
				this.RiskType9 = rs.getString("RiskType9").trim();

			if( rs.getString("InsuredAgeFlag") == null )
				this.InsuredAgeFlag = null;
			else
				this.InsuredAgeFlag = rs.getString("InsuredAgeFlag").trim();

			if( rs.getString("TaxOptimal") == null )
				this.TaxOptimal = null;
			else
				this.TaxOptimal = rs.getString("TaxOptimal").trim();

			if( rs.getString("MaxRnewAge") == null )
				this.MaxRnewAge = null;
			else
				this.MaxRnewAge = rs.getString("MaxRnewAge").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LMRiskApp表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LMRiskAppSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LMRiskAppSchema getSchema()
	{
		LMRiskAppSchema aLMRiskAppSchema = new LMRiskAppSchema();
		aLMRiskAppSchema.setSchema(this);
		return aLMRiskAppSchema;
	}

	public LMRiskAppDB getDB()
	{
		LMRiskAppDB aDBOper = new LMRiskAppDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMRiskApp描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskVer)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(KindCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskType1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskType2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskProp)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskPeriod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskTypeDetail)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PolType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InvestFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BonusFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BonusMode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ListFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SubRiskFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(CalDigital));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CalChoMode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(RiskAmntMult));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuPeriodFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(MaxEndPeriod));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(AgeLmt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SignDateCalMode));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProtocolFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GetChgFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProtocolPayFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EnsuPlanFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EnsuPlanAdjFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( StartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( EndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(MinAppntAge));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(MaxAppntAge));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(MaxInsuredAge));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(MinInsuredAge));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(AppInterest));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(AppPremRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuredFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ShareFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BnfFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TempPayFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InpPayPlan)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ImpartFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuExpeFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LoanFalg)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MortagageFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IDifReturnFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CutAmntStopPay)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(RinsRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SaleFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FileAppFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MngCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AutoPayFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(NeedPrintHospital)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(NeedPrintGet)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskType3)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskType4)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskType5)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(NotPrintPol)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(NeedGetPolDate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(NeedReReadBank)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskType6)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskType7)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskType8)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskType9)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuredAgeFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TaxOptimal)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MaxRnewAge));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMRiskApp>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			RiskVer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			RiskName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			KindCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			RiskType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			RiskType1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			RiskType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			RiskProp = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			RiskPeriod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			RiskTypeDetail = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			RiskFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			PolType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			InvestFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			BonusFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			BonusMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			ListFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			SubRiskFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			CalDigital= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,18,SysConst.PACKAGESPILTER))).intValue();
			CalChoMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			RiskAmntMult= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,20,SysConst.PACKAGESPILTER))).intValue();
			InsuPeriodFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			MaxEndPeriod= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,22,SysConst.PACKAGESPILTER))).intValue();
			AgeLmt= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,23,SysConst.PACKAGESPILTER))).intValue();
			SignDateCalMode= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,24,SysConst.PACKAGESPILTER))).intValue();
			ProtocolFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			GetChgFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			ProtocolPayFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			EnsuPlanFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			EnsuPlanAdjFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			StartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30,SysConst.PACKAGESPILTER));
			EndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31,SysConst.PACKAGESPILTER));
			MinAppntAge= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,32,SysConst.PACKAGESPILTER))).intValue();
			MaxAppntAge= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,33,SysConst.PACKAGESPILTER))).intValue();
			MaxInsuredAge= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,34,SysConst.PACKAGESPILTER))).intValue();
			MinInsuredAge= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,35,SysConst.PACKAGESPILTER))).intValue();
			AppInterest = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,36,SysConst.PACKAGESPILTER))).doubleValue();
			AppPremRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,37,SysConst.PACKAGESPILTER))).doubleValue();
			InsuredFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
			ShareFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
			BnfFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
			TempPayFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41, SysConst.PACKAGESPILTER );
			InpPayPlan = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
			ImpartFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43, SysConst.PACKAGESPILTER );
			InsuExpeFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44, SysConst.PACKAGESPILTER );
			LoanFalg = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 45, SysConst.PACKAGESPILTER );
			MortagageFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 46, SysConst.PACKAGESPILTER );
			IDifReturnFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 47, SysConst.PACKAGESPILTER );
			CutAmntStopPay = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 48, SysConst.PACKAGESPILTER );
			RinsRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,49,SysConst.PACKAGESPILTER))).doubleValue();
			SaleFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 50, SysConst.PACKAGESPILTER );
			FileAppFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 51, SysConst.PACKAGESPILTER );
			MngCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 52, SysConst.PACKAGESPILTER );
			AutoPayFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 53, SysConst.PACKAGESPILTER );
			NeedPrintHospital = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 54, SysConst.PACKAGESPILTER );
			NeedPrintGet = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 55, SysConst.PACKAGESPILTER );
			RiskType3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 56, SysConst.PACKAGESPILTER );
			RiskType4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 57, SysConst.PACKAGESPILTER );
			RiskType5 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 58, SysConst.PACKAGESPILTER );
			NotPrintPol = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 59, SysConst.PACKAGESPILTER );
			NeedGetPolDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 60, SysConst.PACKAGESPILTER );
			NeedReReadBank = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 61, SysConst.PACKAGESPILTER );
			RiskType6 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 62, SysConst.PACKAGESPILTER );
			RiskType7 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 63, SysConst.PACKAGESPILTER );
			RiskType8 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 64, SysConst.PACKAGESPILTER );
			RiskType9 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 65, SysConst.PACKAGESPILTER );
			InsuredAgeFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 66, SysConst.PACKAGESPILTER );
			TaxOptimal = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 67, SysConst.PACKAGESPILTER );
			MaxRnewAge = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 68, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LMRiskAppSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("RiskCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
		}
		if (FCode.equals("RiskVer"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskVer));
		}
		if (FCode.equals("RiskName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskName));
		}
		if (FCode.equals("KindCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(KindCode));
		}
		if (FCode.equals("RiskType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskType));
		}
		if (FCode.equals("RiskType1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskType1));
		}
		if (FCode.equals("RiskType2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskType2));
		}
		if (FCode.equals("RiskProp"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskProp));
		}
		if (FCode.equals("RiskPeriod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskPeriod));
		}
		if (FCode.equals("RiskTypeDetail"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskTypeDetail));
		}
		if (FCode.equals("RiskFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskFlag));
		}
		if (FCode.equals("PolType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolType));
		}
		if (FCode.equals("InvestFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InvestFlag));
		}
		if (FCode.equals("BonusFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BonusFlag));
		}
		if (FCode.equals("BonusMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BonusMode));
		}
		if (FCode.equals("ListFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ListFlag));
		}
		if (FCode.equals("SubRiskFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SubRiskFlag));
		}
		if (FCode.equals("CalDigital"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CalDigital));
		}
		if (FCode.equals("CalChoMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CalChoMode));
		}
		if (FCode.equals("RiskAmntMult"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskAmntMult));
		}
		if (FCode.equals("InsuPeriodFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuPeriodFlag));
		}
		if (FCode.equals("MaxEndPeriod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MaxEndPeriod));
		}
		if (FCode.equals("AgeLmt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgeLmt));
		}
		if (FCode.equals("SignDateCalMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SignDateCalMode));
		}
		if (FCode.equals("ProtocolFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProtocolFlag));
		}
		if (FCode.equals("GetChgFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GetChgFlag));
		}
		if (FCode.equals("ProtocolPayFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProtocolPayFlag));
		}
		if (FCode.equals("EnsuPlanFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EnsuPlanFlag));
		}
		if (FCode.equals("EnsuPlanAdjFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EnsuPlanAdjFlag));
		}
		if (FCode.equals("StartDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStartDate()));
		}
		if (FCode.equals("EndDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEndDate()));
		}
		if (FCode.equals("MinAppntAge"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MinAppntAge));
		}
		if (FCode.equals("MaxAppntAge"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MaxAppntAge));
		}
		if (FCode.equals("MaxInsuredAge"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MaxInsuredAge));
		}
		if (FCode.equals("MinInsuredAge"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MinInsuredAge));
		}
		if (FCode.equals("AppInterest"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppInterest));
		}
		if (FCode.equals("AppPremRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppPremRate));
		}
		if (FCode.equals("InsuredFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredFlag));
		}
		if (FCode.equals("ShareFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ShareFlag));
		}
		if (FCode.equals("BnfFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BnfFlag));
		}
		if (FCode.equals("TempPayFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TempPayFlag));
		}
		if (FCode.equals("InpPayPlan"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InpPayPlan));
		}
		if (FCode.equals("ImpartFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ImpartFlag));
		}
		if (FCode.equals("InsuExpeFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuExpeFlag));
		}
		if (FCode.equals("LoanFalg"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LoanFalg));
		}
		if (FCode.equals("MortagageFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MortagageFlag));
		}
		if (FCode.equals("IDifReturnFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IDifReturnFlag));
		}
		if (FCode.equals("CutAmntStopPay"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CutAmntStopPay));
		}
		if (FCode.equals("RinsRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RinsRate));
		}
		if (FCode.equals("SaleFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SaleFlag));
		}
		if (FCode.equals("FileAppFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FileAppFlag));
		}
		if (FCode.equals("MngCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MngCom));
		}
		if (FCode.equals("AutoPayFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AutoPayFlag));
		}
		if (FCode.equals("NeedPrintHospital"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(NeedPrintHospital));
		}
		if (FCode.equals("NeedPrintGet"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(NeedPrintGet));
		}
		if (FCode.equals("RiskType3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskType3));
		}
		if (FCode.equals("RiskType4"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskType4));
		}
		if (FCode.equals("RiskType5"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskType5));
		}
		if (FCode.equals("NotPrintPol"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(NotPrintPol));
		}
		if (FCode.equals("NeedGetPolDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(NeedGetPolDate));
		}
		if (FCode.equals("NeedReReadBank"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(NeedReReadBank));
		}
		if (FCode.equals("RiskType6"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskType6));
		}
		if (FCode.equals("RiskType7"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskType7));
		}
		if (FCode.equals("RiskType8"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskType8));
		}
		if (FCode.equals("RiskType9"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskType9));
		}
		if (FCode.equals("InsuredAgeFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredAgeFlag));
		}
		if (FCode.equals("TaxOptimal"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TaxOptimal));
		}
		if (FCode.equals("MaxRnewAge"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MaxRnewAge));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(RiskCode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(RiskVer);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(RiskName);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(KindCode);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(RiskType);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(RiskType1);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(RiskType2);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(RiskProp);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(RiskPeriod);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(RiskTypeDetail);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(RiskFlag);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(PolType);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(InvestFlag);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(BonusFlag);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(BonusMode);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(ListFlag);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(SubRiskFlag);
				break;
			case 17:
				strFieldValue = String.valueOf(CalDigital);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(CalChoMode);
				break;
			case 19:
				strFieldValue = String.valueOf(RiskAmntMult);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(InsuPeriodFlag);
				break;
			case 21:
				strFieldValue = String.valueOf(MaxEndPeriod);
				break;
			case 22:
				strFieldValue = String.valueOf(AgeLmt);
				break;
			case 23:
				strFieldValue = String.valueOf(SignDateCalMode);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(ProtocolFlag);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(GetChgFlag);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(ProtocolPayFlag);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(EnsuPlanFlag);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(EnsuPlanAdjFlag);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStartDate()));
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEndDate()));
				break;
			case 31:
				strFieldValue = String.valueOf(MinAppntAge);
				break;
			case 32:
				strFieldValue = String.valueOf(MaxAppntAge);
				break;
			case 33:
				strFieldValue = String.valueOf(MaxInsuredAge);
				break;
			case 34:
				strFieldValue = String.valueOf(MinInsuredAge);
				break;
			case 35:
				strFieldValue = String.valueOf(AppInterest);
				break;
			case 36:
				strFieldValue = String.valueOf(AppPremRate);
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(InsuredFlag);
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(ShareFlag);
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(BnfFlag);
				break;
			case 40:
				strFieldValue = StrTool.GBKToUnicode(TempPayFlag);
				break;
			case 41:
				strFieldValue = StrTool.GBKToUnicode(InpPayPlan);
				break;
			case 42:
				strFieldValue = StrTool.GBKToUnicode(ImpartFlag);
				break;
			case 43:
				strFieldValue = StrTool.GBKToUnicode(InsuExpeFlag);
				break;
			case 44:
				strFieldValue = StrTool.GBKToUnicode(LoanFalg);
				break;
			case 45:
				strFieldValue = StrTool.GBKToUnicode(MortagageFlag);
				break;
			case 46:
				strFieldValue = StrTool.GBKToUnicode(IDifReturnFlag);
				break;
			case 47:
				strFieldValue = StrTool.GBKToUnicode(CutAmntStopPay);
				break;
			case 48:
				strFieldValue = String.valueOf(RinsRate);
				break;
			case 49:
				strFieldValue = StrTool.GBKToUnicode(SaleFlag);
				break;
			case 50:
				strFieldValue = StrTool.GBKToUnicode(FileAppFlag);
				break;
			case 51:
				strFieldValue = StrTool.GBKToUnicode(MngCom);
				break;
			case 52:
				strFieldValue = StrTool.GBKToUnicode(AutoPayFlag);
				break;
			case 53:
				strFieldValue = StrTool.GBKToUnicode(NeedPrintHospital);
				break;
			case 54:
				strFieldValue = StrTool.GBKToUnicode(NeedPrintGet);
				break;
			case 55:
				strFieldValue = StrTool.GBKToUnicode(RiskType3);
				break;
			case 56:
				strFieldValue = StrTool.GBKToUnicode(RiskType4);
				break;
			case 57:
				strFieldValue = StrTool.GBKToUnicode(RiskType5);
				break;
			case 58:
				strFieldValue = StrTool.GBKToUnicode(NotPrintPol);
				break;
			case 59:
				strFieldValue = StrTool.GBKToUnicode(NeedGetPolDate);
				break;
			case 60:
				strFieldValue = StrTool.GBKToUnicode(NeedReReadBank);
				break;
			case 61:
				strFieldValue = StrTool.GBKToUnicode(RiskType6);
				break;
			case 62:
				strFieldValue = StrTool.GBKToUnicode(RiskType7);
				break;
			case 63:
				strFieldValue = StrTool.GBKToUnicode(RiskType8);
				break;
			case 64:
				strFieldValue = StrTool.GBKToUnicode(RiskType9);
				break;
			case 65:
				strFieldValue = StrTool.GBKToUnicode(InsuredAgeFlag);
				break;
			case 66:
				strFieldValue = StrTool.GBKToUnicode(TaxOptimal);
				break;
			case 67:
				strFieldValue = StrTool.GBKToUnicode(MaxRnewAge);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("RiskCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskCode = FValue.trim();
			}
			else
				RiskCode = null;
		}
		if (FCode.equalsIgnoreCase("RiskVer"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskVer = FValue.trim();
			}
			else
				RiskVer = null;
		}
		if (FCode.equalsIgnoreCase("RiskName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskName = FValue.trim();
			}
			else
				RiskName = null;
		}
		if (FCode.equalsIgnoreCase("KindCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				KindCode = FValue.trim();
			}
			else
				KindCode = null;
		}
		if (FCode.equalsIgnoreCase("RiskType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskType = FValue.trim();
			}
			else
				RiskType = null;
		}
		if (FCode.equalsIgnoreCase("RiskType1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskType1 = FValue.trim();
			}
			else
				RiskType1 = null;
		}
		if (FCode.equalsIgnoreCase("RiskType2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskType2 = FValue.trim();
			}
			else
				RiskType2 = null;
		}
		if (FCode.equalsIgnoreCase("RiskProp"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskProp = FValue.trim();
			}
			else
				RiskProp = null;
		}
		if (FCode.equalsIgnoreCase("RiskPeriod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskPeriod = FValue.trim();
			}
			else
				RiskPeriod = null;
		}
		if (FCode.equalsIgnoreCase("RiskTypeDetail"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskTypeDetail = FValue.trim();
			}
			else
				RiskTypeDetail = null;
		}
		if (FCode.equalsIgnoreCase("RiskFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskFlag = FValue.trim();
			}
			else
				RiskFlag = null;
		}
		if (FCode.equalsIgnoreCase("PolType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PolType = FValue.trim();
			}
			else
				PolType = null;
		}
		if (FCode.equalsIgnoreCase("InvestFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InvestFlag = FValue.trim();
			}
			else
				InvestFlag = null;
		}
		if (FCode.equalsIgnoreCase("BonusFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BonusFlag = FValue.trim();
			}
			else
				BonusFlag = null;
		}
		if (FCode.equalsIgnoreCase("BonusMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BonusMode = FValue.trim();
			}
			else
				BonusMode = null;
		}
		if (FCode.equalsIgnoreCase("ListFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ListFlag = FValue.trim();
			}
			else
				ListFlag = null;
		}
		if (FCode.equalsIgnoreCase("SubRiskFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SubRiskFlag = FValue.trim();
			}
			else
				SubRiskFlag = null;
		}
		if (FCode.equalsIgnoreCase("CalDigital"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				CalDigital = i;
			}
		}
		if (FCode.equalsIgnoreCase("CalChoMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CalChoMode = FValue.trim();
			}
			else
				CalChoMode = null;
		}
		if (FCode.equalsIgnoreCase("RiskAmntMult"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				RiskAmntMult = i;
			}
		}
		if (FCode.equalsIgnoreCase("InsuPeriodFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuPeriodFlag = FValue.trim();
			}
			else
				InsuPeriodFlag = null;
		}
		if (FCode.equalsIgnoreCase("MaxEndPeriod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				MaxEndPeriod = i;
			}
		}
		if (FCode.equalsIgnoreCase("AgeLmt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				AgeLmt = i;
			}
		}
		if (FCode.equalsIgnoreCase("SignDateCalMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				SignDateCalMode = i;
			}
		}
		if (FCode.equalsIgnoreCase("ProtocolFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProtocolFlag = FValue.trim();
			}
			else
				ProtocolFlag = null;
		}
		if (FCode.equalsIgnoreCase("GetChgFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GetChgFlag = FValue.trim();
			}
			else
				GetChgFlag = null;
		}
		if (FCode.equalsIgnoreCase("ProtocolPayFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProtocolPayFlag = FValue.trim();
			}
			else
				ProtocolPayFlag = null;
		}
		if (FCode.equalsIgnoreCase("EnsuPlanFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EnsuPlanFlag = FValue.trim();
			}
			else
				EnsuPlanFlag = null;
		}
		if (FCode.equalsIgnoreCase("EnsuPlanAdjFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EnsuPlanAdjFlag = FValue.trim();
			}
			else
				EnsuPlanAdjFlag = null;
		}
		if (FCode.equalsIgnoreCase("StartDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				StartDate = fDate.getDate( FValue );
			}
			else
				StartDate = null;
		}
		if (FCode.equalsIgnoreCase("EndDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				EndDate = fDate.getDate( FValue );
			}
			else
				EndDate = null;
		}
		if (FCode.equalsIgnoreCase("MinAppntAge"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				MinAppntAge = i;
			}
		}
		if (FCode.equalsIgnoreCase("MaxAppntAge"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				MaxAppntAge = i;
			}
		}
		if (FCode.equalsIgnoreCase("MaxInsuredAge"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				MaxInsuredAge = i;
			}
		}
		if (FCode.equalsIgnoreCase("MinInsuredAge"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				MinInsuredAge = i;
			}
		}
		if (FCode.equalsIgnoreCase("AppInterest"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				AppInterest = d;
			}
		}
		if (FCode.equalsIgnoreCase("AppPremRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				AppPremRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("InsuredFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuredFlag = FValue.trim();
			}
			else
				InsuredFlag = null;
		}
		if (FCode.equalsIgnoreCase("ShareFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ShareFlag = FValue.trim();
			}
			else
				ShareFlag = null;
		}
		if (FCode.equalsIgnoreCase("BnfFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BnfFlag = FValue.trim();
			}
			else
				BnfFlag = null;
		}
		if (FCode.equalsIgnoreCase("TempPayFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TempPayFlag = FValue.trim();
			}
			else
				TempPayFlag = null;
		}
		if (FCode.equalsIgnoreCase("InpPayPlan"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InpPayPlan = FValue.trim();
			}
			else
				InpPayPlan = null;
		}
		if (FCode.equalsIgnoreCase("ImpartFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ImpartFlag = FValue.trim();
			}
			else
				ImpartFlag = null;
		}
		if (FCode.equalsIgnoreCase("InsuExpeFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuExpeFlag = FValue.trim();
			}
			else
				InsuExpeFlag = null;
		}
		if (FCode.equalsIgnoreCase("LoanFalg"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LoanFalg = FValue.trim();
			}
			else
				LoanFalg = null;
		}
		if (FCode.equalsIgnoreCase("MortagageFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MortagageFlag = FValue.trim();
			}
			else
				MortagageFlag = null;
		}
		if (FCode.equalsIgnoreCase("IDifReturnFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IDifReturnFlag = FValue.trim();
			}
			else
				IDifReturnFlag = null;
		}
		if (FCode.equalsIgnoreCase("CutAmntStopPay"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CutAmntStopPay = FValue.trim();
			}
			else
				CutAmntStopPay = null;
		}
		if (FCode.equalsIgnoreCase("RinsRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				RinsRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("SaleFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SaleFlag = FValue.trim();
			}
			else
				SaleFlag = null;
		}
		if (FCode.equalsIgnoreCase("FileAppFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FileAppFlag = FValue.trim();
			}
			else
				FileAppFlag = null;
		}
		if (FCode.equalsIgnoreCase("MngCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MngCom = FValue.trim();
			}
			else
				MngCom = null;
		}
		if (FCode.equalsIgnoreCase("AutoPayFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AutoPayFlag = FValue.trim();
			}
			else
				AutoPayFlag = null;
		}
		if (FCode.equalsIgnoreCase("NeedPrintHospital"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				NeedPrintHospital = FValue.trim();
			}
			else
				NeedPrintHospital = null;
		}
		if (FCode.equalsIgnoreCase("NeedPrintGet"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				NeedPrintGet = FValue.trim();
			}
			else
				NeedPrintGet = null;
		}
		if (FCode.equalsIgnoreCase("RiskType3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskType3 = FValue.trim();
			}
			else
				RiskType3 = null;
		}
		if (FCode.equalsIgnoreCase("RiskType4"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskType4 = FValue.trim();
			}
			else
				RiskType4 = null;
		}
		if (FCode.equalsIgnoreCase("RiskType5"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskType5 = FValue.trim();
			}
			else
				RiskType5 = null;
		}
		if (FCode.equalsIgnoreCase("NotPrintPol"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				NotPrintPol = FValue.trim();
			}
			else
				NotPrintPol = null;
		}
		if (FCode.equalsIgnoreCase("NeedGetPolDate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				NeedGetPolDate = FValue.trim();
			}
			else
				NeedGetPolDate = null;
		}
		if (FCode.equalsIgnoreCase("NeedReReadBank"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				NeedReReadBank = FValue.trim();
			}
			else
				NeedReReadBank = null;
		}
		if (FCode.equalsIgnoreCase("RiskType6"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskType6 = FValue.trim();
			}
			else
				RiskType6 = null;
		}
		if (FCode.equalsIgnoreCase("RiskType7"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskType7 = FValue.trim();
			}
			else
				RiskType7 = null;
		}
		if (FCode.equalsIgnoreCase("RiskType8"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskType8 = FValue.trim();
			}
			else
				RiskType8 = null;
		}
		if (FCode.equalsIgnoreCase("RiskType9"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskType9 = FValue.trim();
			}
			else
				RiskType9 = null;
		}
		if (FCode.equalsIgnoreCase("InsuredAgeFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuredAgeFlag = FValue.trim();
			}
			else
				InsuredAgeFlag = null;
		}
		if (FCode.equalsIgnoreCase("TaxOptimal"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TaxOptimal = FValue.trim();
			}
			else
				TaxOptimal = null;
		}
		if (FCode.equalsIgnoreCase("MaxRnewAge"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MaxRnewAge = FValue.trim();
			}
			else
				MaxRnewAge = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LMRiskAppSchema other = (LMRiskAppSchema)otherObject;
		return
			(RiskCode == null ? other.getRiskCode() == null : RiskCode.equals(other.getRiskCode()))
			&& (RiskVer == null ? other.getRiskVer() == null : RiskVer.equals(other.getRiskVer()))
			&& (RiskName == null ? other.getRiskName() == null : RiskName.equals(other.getRiskName()))
			&& (KindCode == null ? other.getKindCode() == null : KindCode.equals(other.getKindCode()))
			&& (RiskType == null ? other.getRiskType() == null : RiskType.equals(other.getRiskType()))
			&& (RiskType1 == null ? other.getRiskType1() == null : RiskType1.equals(other.getRiskType1()))
			&& (RiskType2 == null ? other.getRiskType2() == null : RiskType2.equals(other.getRiskType2()))
			&& (RiskProp == null ? other.getRiskProp() == null : RiskProp.equals(other.getRiskProp()))
			&& (RiskPeriod == null ? other.getRiskPeriod() == null : RiskPeriod.equals(other.getRiskPeriod()))
			&& (RiskTypeDetail == null ? other.getRiskTypeDetail() == null : RiskTypeDetail.equals(other.getRiskTypeDetail()))
			&& (RiskFlag == null ? other.getRiskFlag() == null : RiskFlag.equals(other.getRiskFlag()))
			&& (PolType == null ? other.getPolType() == null : PolType.equals(other.getPolType()))
			&& (InvestFlag == null ? other.getInvestFlag() == null : InvestFlag.equals(other.getInvestFlag()))
			&& (BonusFlag == null ? other.getBonusFlag() == null : BonusFlag.equals(other.getBonusFlag()))
			&& (BonusMode == null ? other.getBonusMode() == null : BonusMode.equals(other.getBonusMode()))
			&& (ListFlag == null ? other.getListFlag() == null : ListFlag.equals(other.getListFlag()))
			&& (SubRiskFlag == null ? other.getSubRiskFlag() == null : SubRiskFlag.equals(other.getSubRiskFlag()))
			&& CalDigital == other.getCalDigital()
			&& (CalChoMode == null ? other.getCalChoMode() == null : CalChoMode.equals(other.getCalChoMode()))
			&& RiskAmntMult == other.getRiskAmntMult()
			&& (InsuPeriodFlag == null ? other.getInsuPeriodFlag() == null : InsuPeriodFlag.equals(other.getInsuPeriodFlag()))
			&& MaxEndPeriod == other.getMaxEndPeriod()
			&& AgeLmt == other.getAgeLmt()
			&& SignDateCalMode == other.getSignDateCalMode()
			&& (ProtocolFlag == null ? other.getProtocolFlag() == null : ProtocolFlag.equals(other.getProtocolFlag()))
			&& (GetChgFlag == null ? other.getGetChgFlag() == null : GetChgFlag.equals(other.getGetChgFlag()))
			&& (ProtocolPayFlag == null ? other.getProtocolPayFlag() == null : ProtocolPayFlag.equals(other.getProtocolPayFlag()))
			&& (EnsuPlanFlag == null ? other.getEnsuPlanFlag() == null : EnsuPlanFlag.equals(other.getEnsuPlanFlag()))
			&& (EnsuPlanAdjFlag == null ? other.getEnsuPlanAdjFlag() == null : EnsuPlanAdjFlag.equals(other.getEnsuPlanAdjFlag()))
			&& (StartDate == null ? other.getStartDate() == null : fDate.getString(StartDate).equals(other.getStartDate()))
			&& (EndDate == null ? other.getEndDate() == null : fDate.getString(EndDate).equals(other.getEndDate()))
			&& MinAppntAge == other.getMinAppntAge()
			&& MaxAppntAge == other.getMaxAppntAge()
			&& MaxInsuredAge == other.getMaxInsuredAge()
			&& MinInsuredAge == other.getMinInsuredAge()
			&& AppInterest == other.getAppInterest()
			&& AppPremRate == other.getAppPremRate()
			&& (InsuredFlag == null ? other.getInsuredFlag() == null : InsuredFlag.equals(other.getInsuredFlag()))
			&& (ShareFlag == null ? other.getShareFlag() == null : ShareFlag.equals(other.getShareFlag()))
			&& (BnfFlag == null ? other.getBnfFlag() == null : BnfFlag.equals(other.getBnfFlag()))
			&& (TempPayFlag == null ? other.getTempPayFlag() == null : TempPayFlag.equals(other.getTempPayFlag()))
			&& (InpPayPlan == null ? other.getInpPayPlan() == null : InpPayPlan.equals(other.getInpPayPlan()))
			&& (ImpartFlag == null ? other.getImpartFlag() == null : ImpartFlag.equals(other.getImpartFlag()))
			&& (InsuExpeFlag == null ? other.getInsuExpeFlag() == null : InsuExpeFlag.equals(other.getInsuExpeFlag()))
			&& (LoanFalg == null ? other.getLoanFalg() == null : LoanFalg.equals(other.getLoanFalg()))
			&& (MortagageFlag == null ? other.getMortagageFlag() == null : MortagageFlag.equals(other.getMortagageFlag()))
			&& (IDifReturnFlag == null ? other.getIDifReturnFlag() == null : IDifReturnFlag.equals(other.getIDifReturnFlag()))
			&& (CutAmntStopPay == null ? other.getCutAmntStopPay() == null : CutAmntStopPay.equals(other.getCutAmntStopPay()))
			&& RinsRate == other.getRinsRate()
			&& (SaleFlag == null ? other.getSaleFlag() == null : SaleFlag.equals(other.getSaleFlag()))
			&& (FileAppFlag == null ? other.getFileAppFlag() == null : FileAppFlag.equals(other.getFileAppFlag()))
			&& (MngCom == null ? other.getMngCom() == null : MngCom.equals(other.getMngCom()))
			&& (AutoPayFlag == null ? other.getAutoPayFlag() == null : AutoPayFlag.equals(other.getAutoPayFlag()))
			&& (NeedPrintHospital == null ? other.getNeedPrintHospital() == null : NeedPrintHospital.equals(other.getNeedPrintHospital()))
			&& (NeedPrintGet == null ? other.getNeedPrintGet() == null : NeedPrintGet.equals(other.getNeedPrintGet()))
			&& (RiskType3 == null ? other.getRiskType3() == null : RiskType3.equals(other.getRiskType3()))
			&& (RiskType4 == null ? other.getRiskType4() == null : RiskType4.equals(other.getRiskType4()))
			&& (RiskType5 == null ? other.getRiskType5() == null : RiskType5.equals(other.getRiskType5()))
			&& (NotPrintPol == null ? other.getNotPrintPol() == null : NotPrintPol.equals(other.getNotPrintPol()))
			&& (NeedGetPolDate == null ? other.getNeedGetPolDate() == null : NeedGetPolDate.equals(other.getNeedGetPolDate()))
			&& (NeedReReadBank == null ? other.getNeedReReadBank() == null : NeedReReadBank.equals(other.getNeedReReadBank()))
			&& (RiskType6 == null ? other.getRiskType6() == null : RiskType6.equals(other.getRiskType6()))
			&& (RiskType7 == null ? other.getRiskType7() == null : RiskType7.equals(other.getRiskType7()))
			&& (RiskType8 == null ? other.getRiskType8() == null : RiskType8.equals(other.getRiskType8()))
			&& (RiskType9 == null ? other.getRiskType9() == null : RiskType9.equals(other.getRiskType9()))
			&& (InsuredAgeFlag == null ? other.getInsuredAgeFlag() == null : InsuredAgeFlag.equals(other.getInsuredAgeFlag()))
			&& (TaxOptimal == null ? other.getTaxOptimal() == null : TaxOptimal.equals(other.getTaxOptimal()))
			&& (MaxRnewAge == null ? other.getMaxRnewAge() == null : MaxRnewAge.equals(other.getMaxRnewAge()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("RiskCode") ) {
			return 0;
		}
		if( strFieldName.equals("RiskVer") ) {
			return 1;
		}
		if( strFieldName.equals("RiskName") ) {
			return 2;
		}
		if( strFieldName.equals("KindCode") ) {
			return 3;
		}
		if( strFieldName.equals("RiskType") ) {
			return 4;
		}
		if( strFieldName.equals("RiskType1") ) {
			return 5;
		}
		if( strFieldName.equals("RiskType2") ) {
			return 6;
		}
		if( strFieldName.equals("RiskProp") ) {
			return 7;
		}
		if( strFieldName.equals("RiskPeriod") ) {
			return 8;
		}
		if( strFieldName.equals("RiskTypeDetail") ) {
			return 9;
		}
		if( strFieldName.equals("RiskFlag") ) {
			return 10;
		}
		if( strFieldName.equals("PolType") ) {
			return 11;
		}
		if( strFieldName.equals("InvestFlag") ) {
			return 12;
		}
		if( strFieldName.equals("BonusFlag") ) {
			return 13;
		}
		if( strFieldName.equals("BonusMode") ) {
			return 14;
		}
		if( strFieldName.equals("ListFlag") ) {
			return 15;
		}
		if( strFieldName.equals("SubRiskFlag") ) {
			return 16;
		}
		if( strFieldName.equals("CalDigital") ) {
			return 17;
		}
		if( strFieldName.equals("CalChoMode") ) {
			return 18;
		}
		if( strFieldName.equals("RiskAmntMult") ) {
			return 19;
		}
		if( strFieldName.equals("InsuPeriodFlag") ) {
			return 20;
		}
		if( strFieldName.equals("MaxEndPeriod") ) {
			return 21;
		}
		if( strFieldName.equals("AgeLmt") ) {
			return 22;
		}
		if( strFieldName.equals("SignDateCalMode") ) {
			return 23;
		}
		if( strFieldName.equals("ProtocolFlag") ) {
			return 24;
		}
		if( strFieldName.equals("GetChgFlag") ) {
			return 25;
		}
		if( strFieldName.equals("ProtocolPayFlag") ) {
			return 26;
		}
		if( strFieldName.equals("EnsuPlanFlag") ) {
			return 27;
		}
		if( strFieldName.equals("EnsuPlanAdjFlag") ) {
			return 28;
		}
		if( strFieldName.equals("StartDate") ) {
			return 29;
		}
		if( strFieldName.equals("EndDate") ) {
			return 30;
		}
		if( strFieldName.equals("MinAppntAge") ) {
			return 31;
		}
		if( strFieldName.equals("MaxAppntAge") ) {
			return 32;
		}
		if( strFieldName.equals("MaxInsuredAge") ) {
			return 33;
		}
		if( strFieldName.equals("MinInsuredAge") ) {
			return 34;
		}
		if( strFieldName.equals("AppInterest") ) {
			return 35;
		}
		if( strFieldName.equals("AppPremRate") ) {
			return 36;
		}
		if( strFieldName.equals("InsuredFlag") ) {
			return 37;
		}
		if( strFieldName.equals("ShareFlag") ) {
			return 38;
		}
		if( strFieldName.equals("BnfFlag") ) {
			return 39;
		}
		if( strFieldName.equals("TempPayFlag") ) {
			return 40;
		}
		if( strFieldName.equals("InpPayPlan") ) {
			return 41;
		}
		if( strFieldName.equals("ImpartFlag") ) {
			return 42;
		}
		if( strFieldName.equals("InsuExpeFlag") ) {
			return 43;
		}
		if( strFieldName.equals("LoanFalg") ) {
			return 44;
		}
		if( strFieldName.equals("MortagageFlag") ) {
			return 45;
		}
		if( strFieldName.equals("IDifReturnFlag") ) {
			return 46;
		}
		if( strFieldName.equals("CutAmntStopPay") ) {
			return 47;
		}
		if( strFieldName.equals("RinsRate") ) {
			return 48;
		}
		if( strFieldName.equals("SaleFlag") ) {
			return 49;
		}
		if( strFieldName.equals("FileAppFlag") ) {
			return 50;
		}
		if( strFieldName.equals("MngCom") ) {
			return 51;
		}
		if( strFieldName.equals("AutoPayFlag") ) {
			return 52;
		}
		if( strFieldName.equals("NeedPrintHospital") ) {
			return 53;
		}
		if( strFieldName.equals("NeedPrintGet") ) {
			return 54;
		}
		if( strFieldName.equals("RiskType3") ) {
			return 55;
		}
		if( strFieldName.equals("RiskType4") ) {
			return 56;
		}
		if( strFieldName.equals("RiskType5") ) {
			return 57;
		}
		if( strFieldName.equals("NotPrintPol") ) {
			return 58;
		}
		if( strFieldName.equals("NeedGetPolDate") ) {
			return 59;
		}
		if( strFieldName.equals("NeedReReadBank") ) {
			return 60;
		}
		if( strFieldName.equals("RiskType6") ) {
			return 61;
		}
		if( strFieldName.equals("RiskType7") ) {
			return 62;
		}
		if( strFieldName.equals("RiskType8") ) {
			return 63;
		}
		if( strFieldName.equals("RiskType9") ) {
			return 64;
		}
		if( strFieldName.equals("InsuredAgeFlag") ) {
			return 65;
		}
		if( strFieldName.equals("TaxOptimal") ) {
			return 66;
		}
		if( strFieldName.equals("MaxRnewAge") ) {
			return 67;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "RiskCode";
				break;
			case 1:
				strFieldName = "RiskVer";
				break;
			case 2:
				strFieldName = "RiskName";
				break;
			case 3:
				strFieldName = "KindCode";
				break;
			case 4:
				strFieldName = "RiskType";
				break;
			case 5:
				strFieldName = "RiskType1";
				break;
			case 6:
				strFieldName = "RiskType2";
				break;
			case 7:
				strFieldName = "RiskProp";
				break;
			case 8:
				strFieldName = "RiskPeriod";
				break;
			case 9:
				strFieldName = "RiskTypeDetail";
				break;
			case 10:
				strFieldName = "RiskFlag";
				break;
			case 11:
				strFieldName = "PolType";
				break;
			case 12:
				strFieldName = "InvestFlag";
				break;
			case 13:
				strFieldName = "BonusFlag";
				break;
			case 14:
				strFieldName = "BonusMode";
				break;
			case 15:
				strFieldName = "ListFlag";
				break;
			case 16:
				strFieldName = "SubRiskFlag";
				break;
			case 17:
				strFieldName = "CalDigital";
				break;
			case 18:
				strFieldName = "CalChoMode";
				break;
			case 19:
				strFieldName = "RiskAmntMult";
				break;
			case 20:
				strFieldName = "InsuPeriodFlag";
				break;
			case 21:
				strFieldName = "MaxEndPeriod";
				break;
			case 22:
				strFieldName = "AgeLmt";
				break;
			case 23:
				strFieldName = "SignDateCalMode";
				break;
			case 24:
				strFieldName = "ProtocolFlag";
				break;
			case 25:
				strFieldName = "GetChgFlag";
				break;
			case 26:
				strFieldName = "ProtocolPayFlag";
				break;
			case 27:
				strFieldName = "EnsuPlanFlag";
				break;
			case 28:
				strFieldName = "EnsuPlanAdjFlag";
				break;
			case 29:
				strFieldName = "StartDate";
				break;
			case 30:
				strFieldName = "EndDate";
				break;
			case 31:
				strFieldName = "MinAppntAge";
				break;
			case 32:
				strFieldName = "MaxAppntAge";
				break;
			case 33:
				strFieldName = "MaxInsuredAge";
				break;
			case 34:
				strFieldName = "MinInsuredAge";
				break;
			case 35:
				strFieldName = "AppInterest";
				break;
			case 36:
				strFieldName = "AppPremRate";
				break;
			case 37:
				strFieldName = "InsuredFlag";
				break;
			case 38:
				strFieldName = "ShareFlag";
				break;
			case 39:
				strFieldName = "BnfFlag";
				break;
			case 40:
				strFieldName = "TempPayFlag";
				break;
			case 41:
				strFieldName = "InpPayPlan";
				break;
			case 42:
				strFieldName = "ImpartFlag";
				break;
			case 43:
				strFieldName = "InsuExpeFlag";
				break;
			case 44:
				strFieldName = "LoanFalg";
				break;
			case 45:
				strFieldName = "MortagageFlag";
				break;
			case 46:
				strFieldName = "IDifReturnFlag";
				break;
			case 47:
				strFieldName = "CutAmntStopPay";
				break;
			case 48:
				strFieldName = "RinsRate";
				break;
			case 49:
				strFieldName = "SaleFlag";
				break;
			case 50:
				strFieldName = "FileAppFlag";
				break;
			case 51:
				strFieldName = "MngCom";
				break;
			case 52:
				strFieldName = "AutoPayFlag";
				break;
			case 53:
				strFieldName = "NeedPrintHospital";
				break;
			case 54:
				strFieldName = "NeedPrintGet";
				break;
			case 55:
				strFieldName = "RiskType3";
				break;
			case 56:
				strFieldName = "RiskType4";
				break;
			case 57:
				strFieldName = "RiskType5";
				break;
			case 58:
				strFieldName = "NotPrintPol";
				break;
			case 59:
				strFieldName = "NeedGetPolDate";
				break;
			case 60:
				strFieldName = "NeedReReadBank";
				break;
			case 61:
				strFieldName = "RiskType6";
				break;
			case 62:
				strFieldName = "RiskType7";
				break;
			case 63:
				strFieldName = "RiskType8";
				break;
			case 64:
				strFieldName = "RiskType9";
				break;
			case 65:
				strFieldName = "InsuredAgeFlag";
				break;
			case 66:
				strFieldName = "TaxOptimal";
				break;
			case 67:
				strFieldName = "MaxRnewAge";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("RiskCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskVer") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("KindCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskType1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskType2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskProp") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskPeriod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskTypeDetail") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InvestFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BonusFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BonusMode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ListFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SubRiskFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CalDigital") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("CalChoMode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskAmntMult") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("InsuPeriodFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MaxEndPeriod") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("AgeLmt") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("SignDateCalMode") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("ProtocolFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GetChgFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ProtocolPayFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EnsuPlanFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EnsuPlanAdjFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StartDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("EndDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MinAppntAge") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("MaxAppntAge") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("MaxInsuredAge") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("MinInsuredAge") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("AppInterest") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("AppPremRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("InsuredFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ShareFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BnfFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TempPayFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InpPayPlan") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ImpartFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuExpeFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LoanFalg") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MortagageFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IDifReturnFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CutAmntStopPay") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RinsRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SaleFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FileAppFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MngCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AutoPayFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("NeedPrintHospital") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("NeedPrintGet") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskType3") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskType4") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskType5") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("NotPrintPol") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("NeedGetPolDate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("NeedReReadBank") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskType6") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskType7") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskType8") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskType9") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuredAgeFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TaxOptimal") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MaxRnewAge") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_INT;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_INT;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_INT;
				break;
			case 22:
				nFieldType = Schema.TYPE_INT;
				break;
			case 23:
				nFieldType = Schema.TYPE_INT;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 30:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 31:
				nFieldType = Schema.TYPE_INT;
				break;
			case 32:
				nFieldType = Schema.TYPE_INT;
				break;
			case 33:
				nFieldType = Schema.TYPE_INT;
				break;
			case 34:
				nFieldType = Schema.TYPE_INT;
				break;
			case 35:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 36:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 37:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 38:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 39:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 40:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 41:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 42:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 43:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 44:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 45:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 46:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 47:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 48:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 49:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 50:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 51:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 52:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 53:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 54:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 55:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 56:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 57:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 58:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 59:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 60:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 61:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 62:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 63:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 64:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 65:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 66:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 67:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
