/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LCBriGrpImportDetailDB;

/*
 * <p>ClassName: LCBriGrpImportDetailSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 大连补充工伤险承保导入接口业务
 * @CreateDate：2011-08-10
 */
public class LCBriGrpImportDetailSchema implements Schema, Cloneable
{
	// @Field
	/** 批次号 */
	private String BatchNo;
	/** 业务号码 */
	private String BussNo;
	/** 导入状态 */
	private String ImportState;
	/** 导入日期 */
	private Date ImportDate;
	/** 任务确认状态 */
	private String ConfState;
	/** 任务确认时间 */
	private Date ConfDate;
	/** 处理标志 */
	private String DealFlag;
	/** 处理说明 */
	private String DealRemark;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 是否有效标志 */
	private String EnableFlag;
	/** 保单合同号 */
	private String GrpContNo;
	/** 投保单位名称 */
	private String GrpName;
	/** 组织机构代码 */
	private String OrgancomCode;
	/** 保险证编码/社保登记编号 */
	private String OtherCertificates;
	/** 保单生效日期 */
	private Date CValiDate;
	/** 保单满期日期 */
	private Date CInValidate;
	/** 投保单申请日期 */
	private Date PolApplyDate;
	/** 保单签发日期 */
	private Date SignDate;
	/** 财务到帐日期 */
	private Date ConfMakeDate;
	/** 保单保费 */
	private double Prem;
	/** 缴费方式 */
	private String PayMode;

	public static final int FIELDNUM = 25;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LCBriGrpImportDetailSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "BatchNo";
		pk[1] = "BussNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LCBriGrpImportDetailSchema cloned = (LCBriGrpImportDetailSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getBatchNo()
	{
		return BatchNo;
	}
	public void setBatchNo(String aBatchNo)
	{
		BatchNo = aBatchNo;
	}
	public String getBussNo()
	{
		return BussNo;
	}
	public void setBussNo(String aBussNo)
	{
		BussNo = aBussNo;
	}
	public String getImportState()
	{
		return ImportState;
	}
	public void setImportState(String aImportState)
	{
		ImportState = aImportState;
	}
	public String getImportDate()
	{
		if( ImportDate != null )
			return fDate.getString(ImportDate);
		else
			return null;
	}
	public void setImportDate(Date aImportDate)
	{
		ImportDate = aImportDate;
	}
	public void setImportDate(String aImportDate)
	{
		if (aImportDate != null && !aImportDate.equals("") )
		{
			ImportDate = fDate.getDate( aImportDate );
		}
		else
			ImportDate = null;
	}

	public String getConfState()
	{
		return ConfState;
	}
	public void setConfState(String aConfState)
	{
		ConfState = aConfState;
	}
	public String getConfDate()
	{
		if( ConfDate != null )
			return fDate.getString(ConfDate);
		else
			return null;
	}
	public void setConfDate(Date aConfDate)
	{
		ConfDate = aConfDate;
	}
	public void setConfDate(String aConfDate)
	{
		if (aConfDate != null && !aConfDate.equals("") )
		{
			ConfDate = fDate.getDate( aConfDate );
		}
		else
			ConfDate = null;
	}

	public String getDealFlag()
	{
		return DealFlag;
	}
	public void setDealFlag(String aDealFlag)
	{
		DealFlag = aDealFlag;
	}
	public String getDealRemark()
	{
		return DealRemark;
	}
	public void setDealRemark(String aDealRemark)
	{
		DealRemark = aDealRemark;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getEnableFlag()
	{
		return EnableFlag;
	}
	public void setEnableFlag(String aEnableFlag)
	{
		EnableFlag = aEnableFlag;
	}
	public String getGrpContNo()
	{
		return GrpContNo;
	}
	public void setGrpContNo(String aGrpContNo)
	{
		GrpContNo = aGrpContNo;
	}
	public String getGrpName()
	{
		return GrpName;
	}
	public void setGrpName(String aGrpName)
	{
		GrpName = aGrpName;
	}
	public String getOrgancomCode()
	{
		return OrgancomCode;
	}
	public void setOrgancomCode(String aOrgancomCode)
	{
		OrgancomCode = aOrgancomCode;
	}
	public String getOtherCertificates()
	{
		return OtherCertificates;
	}
	public void setOtherCertificates(String aOtherCertificates)
	{
		OtherCertificates = aOtherCertificates;
	}
	public String getCValiDate()
	{
		if( CValiDate != null )
			return fDate.getString(CValiDate);
		else
			return null;
	}
	public void setCValiDate(Date aCValiDate)
	{
		CValiDate = aCValiDate;
	}
	public void setCValiDate(String aCValiDate)
	{
		if (aCValiDate != null && !aCValiDate.equals("") )
		{
			CValiDate = fDate.getDate( aCValiDate );
		}
		else
			CValiDate = null;
	}

	public String getCInValidate()
	{
		if( CInValidate != null )
			return fDate.getString(CInValidate);
		else
			return null;
	}
	public void setCInValidate(Date aCInValidate)
	{
		CInValidate = aCInValidate;
	}
	public void setCInValidate(String aCInValidate)
	{
		if (aCInValidate != null && !aCInValidate.equals("") )
		{
			CInValidate = fDate.getDate( aCInValidate );
		}
		else
			CInValidate = null;
	}

	public String getPolApplyDate()
	{
		if( PolApplyDate != null )
			return fDate.getString(PolApplyDate);
		else
			return null;
	}
	public void setPolApplyDate(Date aPolApplyDate)
	{
		PolApplyDate = aPolApplyDate;
	}
	public void setPolApplyDate(String aPolApplyDate)
	{
		if (aPolApplyDate != null && !aPolApplyDate.equals("") )
		{
			PolApplyDate = fDate.getDate( aPolApplyDate );
		}
		else
			PolApplyDate = null;
	}

	public String getSignDate()
	{
		if( SignDate != null )
			return fDate.getString(SignDate);
		else
			return null;
	}
	public void setSignDate(Date aSignDate)
	{
		SignDate = aSignDate;
	}
	public void setSignDate(String aSignDate)
	{
		if (aSignDate != null && !aSignDate.equals("") )
		{
			SignDate = fDate.getDate( aSignDate );
		}
		else
			SignDate = null;
	}

	public String getConfMakeDate()
	{
		if( ConfMakeDate != null )
			return fDate.getString(ConfMakeDate);
		else
			return null;
	}
	public void setConfMakeDate(Date aConfMakeDate)
	{
		ConfMakeDate = aConfMakeDate;
	}
	public void setConfMakeDate(String aConfMakeDate)
	{
		if (aConfMakeDate != null && !aConfMakeDate.equals("") )
		{
			ConfMakeDate = fDate.getDate( aConfMakeDate );
		}
		else
			ConfMakeDate = null;
	}

	public double getPrem()
	{
		return Prem;
	}
	public void setPrem(double aPrem)
	{
		Prem = Arith.round(aPrem,2);
	}
	public void setPrem(String aPrem)
	{
		if (aPrem != null && !aPrem.equals(""))
		{
			Double tDouble = new Double(aPrem);
			double d = tDouble.doubleValue();
                Prem = Arith.round(d,2);
		}
	}

	public String getPayMode()
	{
		return PayMode;
	}
	public void setPayMode(String aPayMode)
	{
		PayMode = aPayMode;
	}

	/**
	* 使用另外一个 LCBriGrpImportDetailSchema 对象给 Schema 赋值
	* @param: aLCBriGrpImportDetailSchema LCBriGrpImportDetailSchema
	**/
	public void setSchema(LCBriGrpImportDetailSchema aLCBriGrpImportDetailSchema)
	{
		this.BatchNo = aLCBriGrpImportDetailSchema.getBatchNo();
		this.BussNo = aLCBriGrpImportDetailSchema.getBussNo();
		this.ImportState = aLCBriGrpImportDetailSchema.getImportState();
		this.ImportDate = fDate.getDate( aLCBriGrpImportDetailSchema.getImportDate());
		this.ConfState = aLCBriGrpImportDetailSchema.getConfState();
		this.ConfDate = fDate.getDate( aLCBriGrpImportDetailSchema.getConfDate());
		this.DealFlag = aLCBriGrpImportDetailSchema.getDealFlag();
		this.DealRemark = aLCBriGrpImportDetailSchema.getDealRemark();
		this.Operator = aLCBriGrpImportDetailSchema.getOperator();
		this.MakeDate = fDate.getDate( aLCBriGrpImportDetailSchema.getMakeDate());
		this.MakeTime = aLCBriGrpImportDetailSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLCBriGrpImportDetailSchema.getModifyDate());
		this.ModifyTime = aLCBriGrpImportDetailSchema.getModifyTime();
		this.EnableFlag = aLCBriGrpImportDetailSchema.getEnableFlag();
		this.GrpContNo = aLCBriGrpImportDetailSchema.getGrpContNo();
		this.GrpName = aLCBriGrpImportDetailSchema.getGrpName();
		this.OrgancomCode = aLCBriGrpImportDetailSchema.getOrgancomCode();
		this.OtherCertificates = aLCBriGrpImportDetailSchema.getOtherCertificates();
		this.CValiDate = fDate.getDate( aLCBriGrpImportDetailSchema.getCValiDate());
		this.CInValidate = fDate.getDate( aLCBriGrpImportDetailSchema.getCInValidate());
		this.PolApplyDate = fDate.getDate( aLCBriGrpImportDetailSchema.getPolApplyDate());
		this.SignDate = fDate.getDate( aLCBriGrpImportDetailSchema.getSignDate());
		this.ConfMakeDate = fDate.getDate( aLCBriGrpImportDetailSchema.getConfMakeDate());
		this.Prem = aLCBriGrpImportDetailSchema.getPrem();
		this.PayMode = aLCBriGrpImportDetailSchema.getPayMode();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("BatchNo") == null )
				this.BatchNo = null;
			else
				this.BatchNo = rs.getString("BatchNo").trim();

			if( rs.getString("BussNo") == null )
				this.BussNo = null;
			else
				this.BussNo = rs.getString("BussNo").trim();

			if( rs.getString("ImportState") == null )
				this.ImportState = null;
			else
				this.ImportState = rs.getString("ImportState").trim();

			this.ImportDate = rs.getDate("ImportDate");
			if( rs.getString("ConfState") == null )
				this.ConfState = null;
			else
				this.ConfState = rs.getString("ConfState").trim();

			this.ConfDate = rs.getDate("ConfDate");
			if( rs.getString("DealFlag") == null )
				this.DealFlag = null;
			else
				this.DealFlag = rs.getString("DealFlag").trim();

			if( rs.getString("DealRemark") == null )
				this.DealRemark = null;
			else
				this.DealRemark = rs.getString("DealRemark").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("EnableFlag") == null )
				this.EnableFlag = null;
			else
				this.EnableFlag = rs.getString("EnableFlag").trim();

			if( rs.getString("GrpContNo") == null )
				this.GrpContNo = null;
			else
				this.GrpContNo = rs.getString("GrpContNo").trim();

			if( rs.getString("GrpName") == null )
				this.GrpName = null;
			else
				this.GrpName = rs.getString("GrpName").trim();

			if( rs.getString("OrgancomCode") == null )
				this.OrgancomCode = null;
			else
				this.OrgancomCode = rs.getString("OrgancomCode").trim();

			if( rs.getString("OtherCertificates") == null )
				this.OtherCertificates = null;
			else
				this.OtherCertificates = rs.getString("OtherCertificates").trim();

			this.CValiDate = rs.getDate("CValiDate");
			this.CInValidate = rs.getDate("CInValidate");
			this.PolApplyDate = rs.getDate("PolApplyDate");
			this.SignDate = rs.getDate("SignDate");
			this.ConfMakeDate = rs.getDate("ConfMakeDate");
			this.Prem = rs.getDouble("Prem");
			if( rs.getString("PayMode") == null )
				this.PayMode = null;
			else
				this.PayMode = rs.getString("PayMode").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LCBriGrpImportDetail表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCBriGrpImportDetailSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LCBriGrpImportDetailSchema getSchema()
	{
		LCBriGrpImportDetailSchema aLCBriGrpImportDetailSchema = new LCBriGrpImportDetailSchema();
		aLCBriGrpImportDetailSchema.setSchema(this);
		return aLCBriGrpImportDetailSchema;
	}

	public LCBriGrpImportDetailDB getDB()
	{
		LCBriGrpImportDetailDB aDBOper = new LCBriGrpImportDetailDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCBriGrpImportDetail描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(BatchNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BussNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ImportState)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ImportDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ConfState)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ConfDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DealFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DealRemark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EnableFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OrgancomCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OtherCertificates)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CValiDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CInValidate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( PolApplyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( SignDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ConfMakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Prem));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayMode));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCBriGrpImportDetail>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			BatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			BussNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			ImportState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			ImportDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,SysConst.PACKAGESPILTER));
			ConfState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			ConfDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,SysConst.PACKAGESPILTER));
			DealFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			DealRemark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			EnableFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			GrpName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			OrgancomCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			OtherCertificates = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			CValiDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,SysConst.PACKAGESPILTER));
			CInValidate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,SysConst.PACKAGESPILTER));
			PolApplyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,SysConst.PACKAGESPILTER));
			SignDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,SysConst.PACKAGESPILTER));
			ConfMakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,SysConst.PACKAGESPILTER));
			Prem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,24,SysConst.PACKAGESPILTER))).doubleValue();
			PayMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCBriGrpImportDetailSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("BatchNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BatchNo));
		}
		if (FCode.equals("BussNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BussNo));
		}
		if (FCode.equals("ImportState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ImportState));
		}
		if (FCode.equals("ImportDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getImportDate()));
		}
		if (FCode.equals("ConfState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ConfState));
		}
		if (FCode.equals("ConfDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getConfDate()));
		}
		if (FCode.equals("DealFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DealFlag));
		}
		if (FCode.equals("DealRemark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DealRemark));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("EnableFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EnableFlag));
		}
		if (FCode.equals("GrpContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
		}
		if (FCode.equals("GrpName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpName));
		}
		if (FCode.equals("OrgancomCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OrgancomCode));
		}
		if (FCode.equals("OtherCertificates"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OtherCertificates));
		}
		if (FCode.equals("CValiDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCValiDate()));
		}
		if (FCode.equals("CInValidate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCInValidate()));
		}
		if (FCode.equals("PolApplyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getPolApplyDate()));
		}
		if (FCode.equals("SignDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getSignDate()));
		}
		if (FCode.equals("ConfMakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getConfMakeDate()));
		}
		if (FCode.equals("Prem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Prem));
		}
		if (FCode.equals("PayMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayMode));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(BatchNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(BussNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(ImportState);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getImportDate()));
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(ConfState);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getConfDate()));
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(DealFlag);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(DealRemark);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(EnableFlag);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(GrpContNo);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(GrpName);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(OrgancomCode);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(OtherCertificates);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCValiDate()));
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCInValidate()));
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getPolApplyDate()));
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getSignDate()));
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getConfMakeDate()));
				break;
			case 23:
				strFieldValue = String.valueOf(Prem);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(PayMode);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("BatchNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BatchNo = FValue.trim();
			}
			else
				BatchNo = null;
		}
		if (FCode.equalsIgnoreCase("BussNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BussNo = FValue.trim();
			}
			else
				BussNo = null;
		}
		if (FCode.equalsIgnoreCase("ImportState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ImportState = FValue.trim();
			}
			else
				ImportState = null;
		}
		if (FCode.equalsIgnoreCase("ImportDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ImportDate = fDate.getDate( FValue );
			}
			else
				ImportDate = null;
		}
		if (FCode.equalsIgnoreCase("ConfState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ConfState = FValue.trim();
			}
			else
				ConfState = null;
		}
		if (FCode.equalsIgnoreCase("ConfDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ConfDate = fDate.getDate( FValue );
			}
			else
				ConfDate = null;
		}
		if (FCode.equalsIgnoreCase("DealFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DealFlag = FValue.trim();
			}
			else
				DealFlag = null;
		}
		if (FCode.equalsIgnoreCase("DealRemark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DealRemark = FValue.trim();
			}
			else
				DealRemark = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("EnableFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EnableFlag = FValue.trim();
			}
			else
				EnableFlag = null;
		}
		if (FCode.equalsIgnoreCase("GrpContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpContNo = FValue.trim();
			}
			else
				GrpContNo = null;
		}
		if (FCode.equalsIgnoreCase("GrpName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpName = FValue.trim();
			}
			else
				GrpName = null;
		}
		if (FCode.equalsIgnoreCase("OrgancomCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OrgancomCode = FValue.trim();
			}
			else
				OrgancomCode = null;
		}
		if (FCode.equalsIgnoreCase("OtherCertificates"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OtherCertificates = FValue.trim();
			}
			else
				OtherCertificates = null;
		}
		if (FCode.equalsIgnoreCase("CValiDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CValiDate = fDate.getDate( FValue );
			}
			else
				CValiDate = null;
		}
		if (FCode.equalsIgnoreCase("CInValidate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CInValidate = fDate.getDate( FValue );
			}
			else
				CInValidate = null;
		}
		if (FCode.equalsIgnoreCase("PolApplyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				PolApplyDate = fDate.getDate( FValue );
			}
			else
				PolApplyDate = null;
		}
		if (FCode.equalsIgnoreCase("SignDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				SignDate = fDate.getDate( FValue );
			}
			else
				SignDate = null;
		}
		if (FCode.equalsIgnoreCase("ConfMakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ConfMakeDate = fDate.getDate( FValue );
			}
			else
				ConfMakeDate = null;
		}
		if (FCode.equalsIgnoreCase("Prem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Prem = d;
			}
		}
		if (FCode.equalsIgnoreCase("PayMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayMode = FValue.trim();
			}
			else
				PayMode = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LCBriGrpImportDetailSchema other = (LCBriGrpImportDetailSchema)otherObject;
		return
			(BatchNo == null ? other.getBatchNo() == null : BatchNo.equals(other.getBatchNo()))
			&& (BussNo == null ? other.getBussNo() == null : BussNo.equals(other.getBussNo()))
			&& (ImportState == null ? other.getImportState() == null : ImportState.equals(other.getImportState()))
			&& (ImportDate == null ? other.getImportDate() == null : fDate.getString(ImportDate).equals(other.getImportDate()))
			&& (ConfState == null ? other.getConfState() == null : ConfState.equals(other.getConfState()))
			&& (ConfDate == null ? other.getConfDate() == null : fDate.getString(ConfDate).equals(other.getConfDate()))
			&& (DealFlag == null ? other.getDealFlag() == null : DealFlag.equals(other.getDealFlag()))
			&& (DealRemark == null ? other.getDealRemark() == null : DealRemark.equals(other.getDealRemark()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (EnableFlag == null ? other.getEnableFlag() == null : EnableFlag.equals(other.getEnableFlag()))
			&& (GrpContNo == null ? other.getGrpContNo() == null : GrpContNo.equals(other.getGrpContNo()))
			&& (GrpName == null ? other.getGrpName() == null : GrpName.equals(other.getGrpName()))
			&& (OrgancomCode == null ? other.getOrgancomCode() == null : OrgancomCode.equals(other.getOrgancomCode()))
			&& (OtherCertificates == null ? other.getOtherCertificates() == null : OtherCertificates.equals(other.getOtherCertificates()))
			&& (CValiDate == null ? other.getCValiDate() == null : fDate.getString(CValiDate).equals(other.getCValiDate()))
			&& (CInValidate == null ? other.getCInValidate() == null : fDate.getString(CInValidate).equals(other.getCInValidate()))
			&& (PolApplyDate == null ? other.getPolApplyDate() == null : fDate.getString(PolApplyDate).equals(other.getPolApplyDate()))
			&& (SignDate == null ? other.getSignDate() == null : fDate.getString(SignDate).equals(other.getSignDate()))
			&& (ConfMakeDate == null ? other.getConfMakeDate() == null : fDate.getString(ConfMakeDate).equals(other.getConfMakeDate()))
			&& Prem == other.getPrem()
			&& (PayMode == null ? other.getPayMode() == null : PayMode.equals(other.getPayMode()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("BatchNo") ) {
			return 0;
		}
		if( strFieldName.equals("BussNo") ) {
			return 1;
		}
		if( strFieldName.equals("ImportState") ) {
			return 2;
		}
		if( strFieldName.equals("ImportDate") ) {
			return 3;
		}
		if( strFieldName.equals("ConfState") ) {
			return 4;
		}
		if( strFieldName.equals("ConfDate") ) {
			return 5;
		}
		if( strFieldName.equals("DealFlag") ) {
			return 6;
		}
		if( strFieldName.equals("DealRemark") ) {
			return 7;
		}
		if( strFieldName.equals("Operator") ) {
			return 8;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 9;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 10;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 11;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 12;
		}
		if( strFieldName.equals("EnableFlag") ) {
			return 13;
		}
		if( strFieldName.equals("GrpContNo") ) {
			return 14;
		}
		if( strFieldName.equals("GrpName") ) {
			return 15;
		}
		if( strFieldName.equals("OrgancomCode") ) {
			return 16;
		}
		if( strFieldName.equals("OtherCertificates") ) {
			return 17;
		}
		if( strFieldName.equals("CValiDate") ) {
			return 18;
		}
		if( strFieldName.equals("CInValidate") ) {
			return 19;
		}
		if( strFieldName.equals("PolApplyDate") ) {
			return 20;
		}
		if( strFieldName.equals("SignDate") ) {
			return 21;
		}
		if( strFieldName.equals("ConfMakeDate") ) {
			return 22;
		}
		if( strFieldName.equals("Prem") ) {
			return 23;
		}
		if( strFieldName.equals("PayMode") ) {
			return 24;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "BatchNo";
				break;
			case 1:
				strFieldName = "BussNo";
				break;
			case 2:
				strFieldName = "ImportState";
				break;
			case 3:
				strFieldName = "ImportDate";
				break;
			case 4:
				strFieldName = "ConfState";
				break;
			case 5:
				strFieldName = "ConfDate";
				break;
			case 6:
				strFieldName = "DealFlag";
				break;
			case 7:
				strFieldName = "DealRemark";
				break;
			case 8:
				strFieldName = "Operator";
				break;
			case 9:
				strFieldName = "MakeDate";
				break;
			case 10:
				strFieldName = "MakeTime";
				break;
			case 11:
				strFieldName = "ModifyDate";
				break;
			case 12:
				strFieldName = "ModifyTime";
				break;
			case 13:
				strFieldName = "EnableFlag";
				break;
			case 14:
				strFieldName = "GrpContNo";
				break;
			case 15:
				strFieldName = "GrpName";
				break;
			case 16:
				strFieldName = "OrgancomCode";
				break;
			case 17:
				strFieldName = "OtherCertificates";
				break;
			case 18:
				strFieldName = "CValiDate";
				break;
			case 19:
				strFieldName = "CInValidate";
				break;
			case 20:
				strFieldName = "PolApplyDate";
				break;
			case 21:
				strFieldName = "SignDate";
				break;
			case 22:
				strFieldName = "ConfMakeDate";
				break;
			case 23:
				strFieldName = "Prem";
				break;
			case 24:
				strFieldName = "PayMode";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("BatchNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BussNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ImportState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ImportDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ConfState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ConfDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("DealFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DealRemark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EnableFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OrgancomCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OtherCertificates") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CValiDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("CInValidate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("PolApplyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("SignDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ConfMakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Prem") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("PayMode") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 19:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 20:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 21:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 22:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 23:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
