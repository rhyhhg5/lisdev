/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LRPolDutyResultDB;

/*
 * <p>ClassName: LRPolDutyResultSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 模拟表结构更改
 * @CreateDate：2008-12-03
 */
public class LRPolDutyResultSchema implements Schema, Cloneable
{
	// @Field
	/** 保单号码 */
	private String PolNo;
	/** 再保合同号 */
	private String ReContCode;
	/** 给付责任编码 */
	private String GetDutyCode;
	/** 临分标志 */
	private String TempCessFlag;
	/** 续保次数 */
	private int ReNewCount;
	/** 续期次数 */
	private int PayCount;
	/** 险种计算分类 */
	private String RiskCalSort;
	/** 险种编码 */
	private String RiskCode;
	/** 险种版本 */
	private String RiskVersion;
	/** 分保计算开始日期 */
	private Date CessStartDate;
	/** 分保计算结束日期 */
	private Date CessEndDate;
	/** 分保比例 */
	private double CessionRate;
	/** 分保额 */
	private double CessionAmount;
	/** 分保费率 */
	private double CessPremRate;
	/** 分保费 */
	private double CessPrem;
	/** 分保佣金比例 */
	private double CessCommRate;
	/** 分保佣金 */
	private double CessComm;
	/** 额外保费 */
	private double ExPrem;
	/** 额外分保费率 */
	private double ExCessPremRate;
	/** 额外分保费 */
	private double ExCessPrem;
	/** 额外分保佣金比例 */
	private double ExcessCommRate;
	/** 额外分保佣金 */
	private double ExCessComm;
	/** 累计风险保额 */
	private double SumRiskAmount;
	/** 当前风险保额 */
	private double NowRiskAmount;
	/** 水平额外加费率 */
	private double LvevlExAddFee;
	/** 特殊佣金比例 */
	private double SpeCemmRate;
	/** 特殊佣金 */
	private double SpeCemm;
	/** 再保险手续费比例 */
	private double ReProcFeeRate;
	/** 分保手续费 */
	private double ReProcFee;
	/** 选择折扣比例 */
	private double ChoiRebaFeeRate;
	/** 选择折扣 */
	private double ChoiRebaFee;
	/** 再保管理费比例 */
	private double ManageFeeRate;
	/** 再保管理费 */
	private double ManageFee;
	/** 管理机构 */
	private String ManageCom;
	/** 操作人 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 当期保费 */
	private double CurrentPrem;
	/** 应分保费合计 */
	private double ShouldCessPrem;
	/** 应缴期数 */
	private int ShouldCount;
	/** 待缴期数 */
	private int RemainCount;

	public static final int FIELDNUM = 43;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LRPolDutyResultSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[5];
		pk[0] = "PolNo";
		pk[1] = "ReContCode";
		pk[2] = "GetDutyCode";
		pk[3] = "ReNewCount";
		pk[4] = "PayCount";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LRPolDutyResultSchema cloned = (LRPolDutyResultSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getPolNo()
	{
		return PolNo;
	}
	public void setPolNo(String aPolNo)
	{
		PolNo = aPolNo;
	}
	public String getReContCode()
	{
		return ReContCode;
	}
	public void setReContCode(String aReContCode)
	{
		ReContCode = aReContCode;
	}
	public String getGetDutyCode()
	{
		return GetDutyCode;
	}
	public void setGetDutyCode(String aGetDutyCode)
	{
		GetDutyCode = aGetDutyCode;
	}
	public String getTempCessFlag()
	{
		return TempCessFlag;
	}
	public void setTempCessFlag(String aTempCessFlag)
	{
		TempCessFlag = aTempCessFlag;
	}
	public int getReNewCount()
	{
		return ReNewCount;
	}
	public void setReNewCount(int aReNewCount)
	{
		ReNewCount = aReNewCount;
	}
	public void setReNewCount(String aReNewCount)
	{
		if (aReNewCount != null && !aReNewCount.equals(""))
		{
			Integer tInteger = new Integer(aReNewCount);
			int i = tInteger.intValue();
			ReNewCount = i;
		}
	}

	public int getPayCount()
	{
		return PayCount;
	}
	public void setPayCount(int aPayCount)
	{
		PayCount = aPayCount;
	}
	public void setPayCount(String aPayCount)
	{
		if (aPayCount != null && !aPayCount.equals(""))
		{
			Integer tInteger = new Integer(aPayCount);
			int i = tInteger.intValue();
			PayCount = i;
		}
	}

	public String getRiskCalSort()
	{
		return RiskCalSort;
	}
	public void setRiskCalSort(String aRiskCalSort)
	{
		RiskCalSort = aRiskCalSort;
	}
	public String getRiskCode()
	{
		return RiskCode;
	}
	public void setRiskCode(String aRiskCode)
	{
		RiskCode = aRiskCode;
	}
	public String getRiskVersion()
	{
		return RiskVersion;
	}
	public void setRiskVersion(String aRiskVersion)
	{
		RiskVersion = aRiskVersion;
	}
	public String getCessStartDate()
	{
		if( CessStartDate != null )
			return fDate.getString(CessStartDate);
		else
			return null;
	}
	public void setCessStartDate(Date aCessStartDate)
	{
		CessStartDate = aCessStartDate;
	}
	public void setCessStartDate(String aCessStartDate)
	{
		if (aCessStartDate != null && !aCessStartDate.equals("") )
		{
			CessStartDate = fDate.getDate( aCessStartDate );
		}
		else
			CessStartDate = null;
	}

	public String getCessEndDate()
	{
		if( CessEndDate != null )
			return fDate.getString(CessEndDate);
		else
			return null;
	}
	public void setCessEndDate(Date aCessEndDate)
	{
		CessEndDate = aCessEndDate;
	}
	public void setCessEndDate(String aCessEndDate)
	{
		if (aCessEndDate != null && !aCessEndDate.equals("") )
		{
			CessEndDate = fDate.getDate( aCessEndDate );
		}
		else
			CessEndDate = null;
	}

	public double getCessionRate()
	{
		return CessionRate;
	}
	public void setCessionRate(double aCessionRate)
	{
		CessionRate = Arith.round(aCessionRate,2);
	}
	public void setCessionRate(String aCessionRate)
	{
		if (aCessionRate != null && !aCessionRate.equals(""))
		{
			Double tDouble = new Double(aCessionRate);
			double d = tDouble.doubleValue();
                CessionRate = Arith.round(d,2);
		}
	}

	public double getCessionAmount()
	{
		return CessionAmount;
	}
	public void setCessionAmount(double aCessionAmount)
	{
		CessionAmount = Arith.round(aCessionAmount,2);
	}
	public void setCessionAmount(String aCessionAmount)
	{
		if (aCessionAmount != null && !aCessionAmount.equals(""))
		{
			Double tDouble = new Double(aCessionAmount);
			double d = tDouble.doubleValue();
                CessionAmount = Arith.round(d,2);
		}
	}

	public double getCessPremRate()
	{
		return CessPremRate;
	}
	public void setCessPremRate(double aCessPremRate)
	{
		CessPremRate = Arith.round(aCessPremRate,2);
	}
	public void setCessPremRate(String aCessPremRate)
	{
		if (aCessPremRate != null && !aCessPremRate.equals(""))
		{
			Double tDouble = new Double(aCessPremRate);
			double d = tDouble.doubleValue();
                CessPremRate = Arith.round(d,2);
		}
	}

	public double getCessPrem()
	{
		return CessPrem;
	}
	public void setCessPrem(double aCessPrem)
	{
		CessPrem = Arith.round(aCessPrem,2);
	}
	public void setCessPrem(String aCessPrem)
	{
		if (aCessPrem != null && !aCessPrem.equals(""))
		{
			Double tDouble = new Double(aCessPrem);
			double d = tDouble.doubleValue();
                CessPrem = Arith.round(d,2);
		}
	}

	public double getCessCommRate()
	{
		return CessCommRate;
	}
	public void setCessCommRate(double aCessCommRate)
	{
		CessCommRate = Arith.round(aCessCommRate,2);
	}
	public void setCessCommRate(String aCessCommRate)
	{
		if (aCessCommRate != null && !aCessCommRate.equals(""))
		{
			Double tDouble = new Double(aCessCommRate);
			double d = tDouble.doubleValue();
                CessCommRate = Arith.round(d,2);
		}
	}

	public double getCessComm()
	{
		return CessComm;
	}
	public void setCessComm(double aCessComm)
	{
		CessComm = Arith.round(aCessComm,2);
	}
	public void setCessComm(String aCessComm)
	{
		if (aCessComm != null && !aCessComm.equals(""))
		{
			Double tDouble = new Double(aCessComm);
			double d = tDouble.doubleValue();
                CessComm = Arith.round(d,2);
		}
	}

	public double getExPrem()
	{
		return ExPrem;
	}
	public void setExPrem(double aExPrem)
	{
		ExPrem = Arith.round(aExPrem,2);
	}
	public void setExPrem(String aExPrem)
	{
		if (aExPrem != null && !aExPrem.equals(""))
		{
			Double tDouble = new Double(aExPrem);
			double d = tDouble.doubleValue();
                ExPrem = Arith.round(d,2);
		}
	}

	public double getExCessPremRate()
	{
		return ExCessPremRate;
	}
	public void setExCessPremRate(double aExCessPremRate)
	{
		ExCessPremRate = Arith.round(aExCessPremRate,2);
	}
	public void setExCessPremRate(String aExCessPremRate)
	{
		if (aExCessPremRate != null && !aExCessPremRate.equals(""))
		{
			Double tDouble = new Double(aExCessPremRate);
			double d = tDouble.doubleValue();
                ExCessPremRate = Arith.round(d,2);
		}
	}

	public double getExCessPrem()
	{
		return ExCessPrem;
	}
	public void setExCessPrem(double aExCessPrem)
	{
		ExCessPrem = Arith.round(aExCessPrem,2);
	}
	public void setExCessPrem(String aExCessPrem)
	{
		if (aExCessPrem != null && !aExCessPrem.equals(""))
		{
			Double tDouble = new Double(aExCessPrem);
			double d = tDouble.doubleValue();
                ExCessPrem = Arith.round(d,2);
		}
	}

	public double getExcessCommRate()
	{
		return ExcessCommRate;
	}
	public void setExcessCommRate(double aExcessCommRate)
	{
		ExcessCommRate = Arith.round(aExcessCommRate,2);
	}
	public void setExcessCommRate(String aExcessCommRate)
	{
		if (aExcessCommRate != null && !aExcessCommRate.equals(""))
		{
			Double tDouble = new Double(aExcessCommRate);
			double d = tDouble.doubleValue();
                ExcessCommRate = Arith.round(d,2);
		}
	}

	public double getExCessComm()
	{
		return ExCessComm;
	}
	public void setExCessComm(double aExCessComm)
	{
		ExCessComm = Arith.round(aExCessComm,2);
	}
	public void setExCessComm(String aExCessComm)
	{
		if (aExCessComm != null && !aExCessComm.equals(""))
		{
			Double tDouble = new Double(aExCessComm);
			double d = tDouble.doubleValue();
                ExCessComm = Arith.round(d,2);
		}
	}

	public double getSumRiskAmount()
	{
		return SumRiskAmount;
	}
	public void setSumRiskAmount(double aSumRiskAmount)
	{
		SumRiskAmount = Arith.round(aSumRiskAmount,2);
	}
	public void setSumRiskAmount(String aSumRiskAmount)
	{
		if (aSumRiskAmount != null && !aSumRiskAmount.equals(""))
		{
			Double tDouble = new Double(aSumRiskAmount);
			double d = tDouble.doubleValue();
                SumRiskAmount = Arith.round(d,2);
		}
	}

	public double getNowRiskAmount()
	{
		return NowRiskAmount;
	}
	public void setNowRiskAmount(double aNowRiskAmount)
	{
		NowRiskAmount = Arith.round(aNowRiskAmount,2);
	}
	public void setNowRiskAmount(String aNowRiskAmount)
	{
		if (aNowRiskAmount != null && !aNowRiskAmount.equals(""))
		{
			Double tDouble = new Double(aNowRiskAmount);
			double d = tDouble.doubleValue();
                NowRiskAmount = Arith.round(d,2);
		}
	}

	public double getLvevlExAddFee()
	{
		return LvevlExAddFee;
	}
	public void setLvevlExAddFee(double aLvevlExAddFee)
	{
		LvevlExAddFee = Arith.round(aLvevlExAddFee,2);
	}
	public void setLvevlExAddFee(String aLvevlExAddFee)
	{
		if (aLvevlExAddFee != null && !aLvevlExAddFee.equals(""))
		{
			Double tDouble = new Double(aLvevlExAddFee);
			double d = tDouble.doubleValue();
                LvevlExAddFee = Arith.round(d,2);
		}
	}

	public double getSpeCemmRate()
	{
		return SpeCemmRate;
	}
	public void setSpeCemmRate(double aSpeCemmRate)
	{
		SpeCemmRate = Arith.round(aSpeCemmRate,2);
	}
	public void setSpeCemmRate(String aSpeCemmRate)
	{
		if (aSpeCemmRate != null && !aSpeCemmRate.equals(""))
		{
			Double tDouble = new Double(aSpeCemmRate);
			double d = tDouble.doubleValue();
                SpeCemmRate = Arith.round(d,2);
		}
	}

	public double getSpeCemm()
	{
		return SpeCemm;
	}
	public void setSpeCemm(double aSpeCemm)
	{
		SpeCemm = Arith.round(aSpeCemm,2);
	}
	public void setSpeCemm(String aSpeCemm)
	{
		if (aSpeCemm != null && !aSpeCemm.equals(""))
		{
			Double tDouble = new Double(aSpeCemm);
			double d = tDouble.doubleValue();
                SpeCemm = Arith.round(d,2);
		}
	}

	public double getReProcFeeRate()
	{
		return ReProcFeeRate;
	}
	public void setReProcFeeRate(double aReProcFeeRate)
	{
		ReProcFeeRate = Arith.round(aReProcFeeRate,2);
	}
	public void setReProcFeeRate(String aReProcFeeRate)
	{
		if (aReProcFeeRate != null && !aReProcFeeRate.equals(""))
		{
			Double tDouble = new Double(aReProcFeeRate);
			double d = tDouble.doubleValue();
                ReProcFeeRate = Arith.round(d,2);
		}
	}

	public double getReProcFee()
	{
		return ReProcFee;
	}
	public void setReProcFee(double aReProcFee)
	{
		ReProcFee = Arith.round(aReProcFee,2);
	}
	public void setReProcFee(String aReProcFee)
	{
		if (aReProcFee != null && !aReProcFee.equals(""))
		{
			Double tDouble = new Double(aReProcFee);
			double d = tDouble.doubleValue();
                ReProcFee = Arith.round(d,2);
		}
	}

	public double getChoiRebaFeeRate()
	{
		return ChoiRebaFeeRate;
	}
	public void setChoiRebaFeeRate(double aChoiRebaFeeRate)
	{
		ChoiRebaFeeRate = Arith.round(aChoiRebaFeeRate,2);
	}
	public void setChoiRebaFeeRate(String aChoiRebaFeeRate)
	{
		if (aChoiRebaFeeRate != null && !aChoiRebaFeeRate.equals(""))
		{
			Double tDouble = new Double(aChoiRebaFeeRate);
			double d = tDouble.doubleValue();
                ChoiRebaFeeRate = Arith.round(d,2);
		}
	}

	public double getChoiRebaFee()
	{
		return ChoiRebaFee;
	}
	public void setChoiRebaFee(double aChoiRebaFee)
	{
		ChoiRebaFee = Arith.round(aChoiRebaFee,2);
	}
	public void setChoiRebaFee(String aChoiRebaFee)
	{
		if (aChoiRebaFee != null && !aChoiRebaFee.equals(""))
		{
			Double tDouble = new Double(aChoiRebaFee);
			double d = tDouble.doubleValue();
                ChoiRebaFee = Arith.round(d,2);
		}
	}

	public double getManageFeeRate()
	{
		return ManageFeeRate;
	}
	public void setManageFeeRate(double aManageFeeRate)
	{
		ManageFeeRate = Arith.round(aManageFeeRate,2);
	}
	public void setManageFeeRate(String aManageFeeRate)
	{
		if (aManageFeeRate != null && !aManageFeeRate.equals(""))
		{
			Double tDouble = new Double(aManageFeeRate);
			double d = tDouble.doubleValue();
                ManageFeeRate = Arith.round(d,2);
		}
	}

	public double getManageFee()
	{
		return ManageFee;
	}
	public void setManageFee(double aManageFee)
	{
		ManageFee = Arith.round(aManageFee,2);
	}
	public void setManageFee(String aManageFee)
	{
		if (aManageFee != null && !aManageFee.equals(""))
		{
			Double tDouble = new Double(aManageFee);
			double d = tDouble.doubleValue();
                ManageFee = Arith.round(d,2);
		}
	}

	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public double getCurrentPrem()
	{
		return CurrentPrem;
	}
	public void setCurrentPrem(double aCurrentPrem)
	{
		CurrentPrem = Arith.round(aCurrentPrem,2);
	}
	public void setCurrentPrem(String aCurrentPrem)
	{
		if (aCurrentPrem != null && !aCurrentPrem.equals(""))
		{
			Double tDouble = new Double(aCurrentPrem);
			double d = tDouble.doubleValue();
                CurrentPrem = Arith.round(d,2);
		}
	}

	public double getShouldCessPrem()
	{
		return ShouldCessPrem;
	}
	public void setShouldCessPrem(double aShouldCessPrem)
	{
		ShouldCessPrem = Arith.round(aShouldCessPrem,2);
	}
	public void setShouldCessPrem(String aShouldCessPrem)
	{
		if (aShouldCessPrem != null && !aShouldCessPrem.equals(""))
		{
			Double tDouble = new Double(aShouldCessPrem);
			double d = tDouble.doubleValue();
                ShouldCessPrem = Arith.round(d,2);
		}
	}

	public int getShouldCount()
	{
		return ShouldCount;
	}
	public void setShouldCount(int aShouldCount)
	{
		ShouldCount = aShouldCount;
	}
	public void setShouldCount(String aShouldCount)
	{
		if (aShouldCount != null && !aShouldCount.equals(""))
		{
			Integer tInteger = new Integer(aShouldCount);
			int i = tInteger.intValue();
			ShouldCount = i;
		}
	}

	public int getRemainCount()
	{
		return RemainCount;
	}
	public void setRemainCount(int aRemainCount)
	{
		RemainCount = aRemainCount;
	}
	public void setRemainCount(String aRemainCount)
	{
		if (aRemainCount != null && !aRemainCount.equals(""))
		{
			Integer tInteger = new Integer(aRemainCount);
			int i = tInteger.intValue();
			RemainCount = i;
		}
	}


	/**
	* 使用另外一个 LRPolDutyResultSchema 对象给 Schema 赋值
	* @param: aLRPolDutyResultSchema LRPolDutyResultSchema
	**/
	public void setSchema(LRPolDutyResultSchema aLRPolDutyResultSchema)
	{
		this.PolNo = aLRPolDutyResultSchema.getPolNo();
		this.ReContCode = aLRPolDutyResultSchema.getReContCode();
		this.GetDutyCode = aLRPolDutyResultSchema.getGetDutyCode();
		this.TempCessFlag = aLRPolDutyResultSchema.getTempCessFlag();
		this.ReNewCount = aLRPolDutyResultSchema.getReNewCount();
		this.PayCount = aLRPolDutyResultSchema.getPayCount();
		this.RiskCalSort = aLRPolDutyResultSchema.getRiskCalSort();
		this.RiskCode = aLRPolDutyResultSchema.getRiskCode();
		this.RiskVersion = aLRPolDutyResultSchema.getRiskVersion();
		this.CessStartDate = fDate.getDate( aLRPolDutyResultSchema.getCessStartDate());
		this.CessEndDate = fDate.getDate( aLRPolDutyResultSchema.getCessEndDate());
		this.CessionRate = aLRPolDutyResultSchema.getCessionRate();
		this.CessionAmount = aLRPolDutyResultSchema.getCessionAmount();
		this.CessPremRate = aLRPolDutyResultSchema.getCessPremRate();
		this.CessPrem = aLRPolDutyResultSchema.getCessPrem();
		this.CessCommRate = aLRPolDutyResultSchema.getCessCommRate();
		this.CessComm = aLRPolDutyResultSchema.getCessComm();
		this.ExPrem = aLRPolDutyResultSchema.getExPrem();
		this.ExCessPremRate = aLRPolDutyResultSchema.getExCessPremRate();
		this.ExCessPrem = aLRPolDutyResultSchema.getExCessPrem();
		this.ExcessCommRate = aLRPolDutyResultSchema.getExcessCommRate();
		this.ExCessComm = aLRPolDutyResultSchema.getExCessComm();
		this.SumRiskAmount = aLRPolDutyResultSchema.getSumRiskAmount();
		this.NowRiskAmount = aLRPolDutyResultSchema.getNowRiskAmount();
		this.LvevlExAddFee = aLRPolDutyResultSchema.getLvevlExAddFee();
		this.SpeCemmRate = aLRPolDutyResultSchema.getSpeCemmRate();
		this.SpeCemm = aLRPolDutyResultSchema.getSpeCemm();
		this.ReProcFeeRate = aLRPolDutyResultSchema.getReProcFeeRate();
		this.ReProcFee = aLRPolDutyResultSchema.getReProcFee();
		this.ChoiRebaFeeRate = aLRPolDutyResultSchema.getChoiRebaFeeRate();
		this.ChoiRebaFee = aLRPolDutyResultSchema.getChoiRebaFee();
		this.ManageFeeRate = aLRPolDutyResultSchema.getManageFeeRate();
		this.ManageFee = aLRPolDutyResultSchema.getManageFee();
		this.ManageCom = aLRPolDutyResultSchema.getManageCom();
		this.Operator = aLRPolDutyResultSchema.getOperator();
		this.MakeDate = fDate.getDate( aLRPolDutyResultSchema.getMakeDate());
		this.MakeTime = aLRPolDutyResultSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLRPolDutyResultSchema.getModifyDate());
		this.ModifyTime = aLRPolDutyResultSchema.getModifyTime();
		this.CurrentPrem = aLRPolDutyResultSchema.getCurrentPrem();
		this.ShouldCessPrem = aLRPolDutyResultSchema.getShouldCessPrem();
		this.ShouldCount = aLRPolDutyResultSchema.getShouldCount();
		this.RemainCount = aLRPolDutyResultSchema.getRemainCount();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("PolNo") == null )
				this.PolNo = null;
			else
				this.PolNo = rs.getString("PolNo").trim();

			if( rs.getString("ReContCode") == null )
				this.ReContCode = null;
			else
				this.ReContCode = rs.getString("ReContCode").trim();

			if( rs.getString("GetDutyCode") == null )
				this.GetDutyCode = null;
			else
				this.GetDutyCode = rs.getString("GetDutyCode").trim();

			if( rs.getString("TempCessFlag") == null )
				this.TempCessFlag = null;
			else
				this.TempCessFlag = rs.getString("TempCessFlag").trim();

			this.ReNewCount = rs.getInt("ReNewCount");
			this.PayCount = rs.getInt("PayCount");
			if( rs.getString("RiskCalSort") == null )
				this.RiskCalSort = null;
			else
				this.RiskCalSort = rs.getString("RiskCalSort").trim();

			if( rs.getString("RiskCode") == null )
				this.RiskCode = null;
			else
				this.RiskCode = rs.getString("RiskCode").trim();

			if( rs.getString("RiskVersion") == null )
				this.RiskVersion = null;
			else
				this.RiskVersion = rs.getString("RiskVersion").trim();

			this.CessStartDate = rs.getDate("CessStartDate");
			this.CessEndDate = rs.getDate("CessEndDate");
			this.CessionRate = rs.getDouble("CessionRate");
			this.CessionAmount = rs.getDouble("CessionAmount");
			this.CessPremRate = rs.getDouble("CessPremRate");
			this.CessPrem = rs.getDouble("CessPrem");
			this.CessCommRate = rs.getDouble("CessCommRate");
			this.CessComm = rs.getDouble("CessComm");
			this.ExPrem = rs.getDouble("ExPrem");
			this.ExCessPremRate = rs.getDouble("ExCessPremRate");
			this.ExCessPrem = rs.getDouble("ExCessPrem");
			this.ExcessCommRate = rs.getDouble("ExcessCommRate");
			this.ExCessComm = rs.getDouble("ExCessComm");
			this.SumRiskAmount = rs.getDouble("SumRiskAmount");
			this.NowRiskAmount = rs.getDouble("NowRiskAmount");
			this.LvevlExAddFee = rs.getDouble("LvevlExAddFee");
			this.SpeCemmRate = rs.getDouble("SpeCemmRate");
			this.SpeCemm = rs.getDouble("SpeCemm");
			this.ReProcFeeRate = rs.getDouble("ReProcFeeRate");
			this.ReProcFee = rs.getDouble("ReProcFee");
			this.ChoiRebaFeeRate = rs.getDouble("ChoiRebaFeeRate");
			this.ChoiRebaFee = rs.getDouble("ChoiRebaFee");
			this.ManageFeeRate = rs.getDouble("ManageFeeRate");
			this.ManageFee = rs.getDouble("ManageFee");
			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			this.CurrentPrem = rs.getDouble("CurrentPrem");
			this.ShouldCessPrem = rs.getDouble("ShouldCessPrem");
			this.ShouldCount = rs.getInt("ShouldCount");
			this.RemainCount = rs.getInt("RemainCount");
		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LRPolDutyResult表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LRPolDutyResultSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LRPolDutyResultSchema getSchema()
	{
		LRPolDutyResultSchema aLRPolDutyResultSchema = new LRPolDutyResultSchema();
		aLRPolDutyResultSchema.setSchema(this);
		return aLRPolDutyResultSchema;
	}

	public LRPolDutyResultDB getDB()
	{
		LRPolDutyResultDB aDBOper = new LRPolDutyResultDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLRPolDutyResult描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(PolNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReContCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GetDutyCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TempCessFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ReNewCount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PayCount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskCalSort)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskVersion)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CessStartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CessEndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(CessionRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(CessionAmount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(CessPremRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(CessPrem));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(CessCommRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(CessComm));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ExPrem));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ExCessPremRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ExCessPrem));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ExcessCommRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ExCessComm));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SumRiskAmount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(NowRiskAmount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(LvevlExAddFee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SpeCemmRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SpeCemm));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ReProcFeeRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ReProcFee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ChoiRebaFeeRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ChoiRebaFee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ManageFeeRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ManageFee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(CurrentPrem));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ShouldCessPrem));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ShouldCount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(RemainCount));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLRPolDutyResult>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			ReContCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			GetDutyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			TempCessFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			ReNewCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,5,SysConst.PACKAGESPILTER))).intValue();
			PayCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,6,SysConst.PACKAGESPILTER))).intValue();
			RiskCalSort = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			RiskVersion = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			CessStartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,SysConst.PACKAGESPILTER));
			CessEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
			CessionRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,12,SysConst.PACKAGESPILTER))).doubleValue();
			CessionAmount = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,13,SysConst.PACKAGESPILTER))).doubleValue();
			CessPremRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,14,SysConst.PACKAGESPILTER))).doubleValue();
			CessPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,15,SysConst.PACKAGESPILTER))).doubleValue();
			CessCommRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,16,SysConst.PACKAGESPILTER))).doubleValue();
			CessComm = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,17,SysConst.PACKAGESPILTER))).doubleValue();
			ExPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,18,SysConst.PACKAGESPILTER))).doubleValue();
			ExCessPremRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,19,SysConst.PACKAGESPILTER))).doubleValue();
			ExCessPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,20,SysConst.PACKAGESPILTER))).doubleValue();
			ExcessCommRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,21,SysConst.PACKAGESPILTER))).doubleValue();
			ExCessComm = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,22,SysConst.PACKAGESPILTER))).doubleValue();
			SumRiskAmount = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,23,SysConst.PACKAGESPILTER))).doubleValue();
			NowRiskAmount = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,24,SysConst.PACKAGESPILTER))).doubleValue();
			LvevlExAddFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,25,SysConst.PACKAGESPILTER))).doubleValue();
			SpeCemmRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,26,SysConst.PACKAGESPILTER))).doubleValue();
			SpeCemm = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,27,SysConst.PACKAGESPILTER))).doubleValue();
			ReProcFeeRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,28,SysConst.PACKAGESPILTER))).doubleValue();
			ReProcFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,29,SysConst.PACKAGESPILTER))).doubleValue();
			ChoiRebaFeeRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,30,SysConst.PACKAGESPILTER))).doubleValue();
			ChoiRebaFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,31,SysConst.PACKAGESPILTER))).doubleValue();
			ManageFeeRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,32,SysConst.PACKAGESPILTER))).doubleValue();
			ManageFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,33,SysConst.PACKAGESPILTER))).doubleValue();
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
			CurrentPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,40,SysConst.PACKAGESPILTER))).doubleValue();
			ShouldCessPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,41,SysConst.PACKAGESPILTER))).doubleValue();
			ShouldCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,42,SysConst.PACKAGESPILTER))).intValue();
			RemainCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,43,SysConst.PACKAGESPILTER))).intValue();
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LRPolDutyResultSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("PolNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
		}
		if (FCode.equals("ReContCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReContCode));
		}
		if (FCode.equals("GetDutyCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GetDutyCode));
		}
		if (FCode.equals("TempCessFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TempCessFlag));
		}
		if (FCode.equals("ReNewCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReNewCount));
		}
		if (FCode.equals("PayCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayCount));
		}
		if (FCode.equals("RiskCalSort"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCalSort));
		}
		if (FCode.equals("RiskCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
		}
		if (FCode.equals("RiskVersion"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskVersion));
		}
		if (FCode.equals("CessStartDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCessStartDate()));
		}
		if (FCode.equals("CessEndDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCessEndDate()));
		}
		if (FCode.equals("CessionRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CessionRate));
		}
		if (FCode.equals("CessionAmount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CessionAmount));
		}
		if (FCode.equals("CessPremRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CessPremRate));
		}
		if (FCode.equals("CessPrem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CessPrem));
		}
		if (FCode.equals("CessCommRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CessCommRate));
		}
		if (FCode.equals("CessComm"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CessComm));
		}
		if (FCode.equals("ExPrem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ExPrem));
		}
		if (FCode.equals("ExCessPremRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ExCessPremRate));
		}
		if (FCode.equals("ExCessPrem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ExCessPrem));
		}
		if (FCode.equals("ExcessCommRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ExcessCommRate));
		}
		if (FCode.equals("ExCessComm"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ExCessComm));
		}
		if (FCode.equals("SumRiskAmount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SumRiskAmount));
		}
		if (FCode.equals("NowRiskAmount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(NowRiskAmount));
		}
		if (FCode.equals("LvevlExAddFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LvevlExAddFee));
		}
		if (FCode.equals("SpeCemmRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SpeCemmRate));
		}
		if (FCode.equals("SpeCemm"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SpeCemm));
		}
		if (FCode.equals("ReProcFeeRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReProcFeeRate));
		}
		if (FCode.equals("ReProcFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReProcFee));
		}
		if (FCode.equals("ChoiRebaFeeRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ChoiRebaFeeRate));
		}
		if (FCode.equals("ChoiRebaFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ChoiRebaFee));
		}
		if (FCode.equals("ManageFeeRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageFeeRate));
		}
		if (FCode.equals("ManageFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageFee));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("CurrentPrem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CurrentPrem));
		}
		if (FCode.equals("ShouldCessPrem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ShouldCessPrem));
		}
		if (FCode.equals("ShouldCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ShouldCount));
		}
		if (FCode.equals("RemainCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RemainCount));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(PolNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(ReContCode);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(GetDutyCode);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(TempCessFlag);
				break;
			case 4:
				strFieldValue = String.valueOf(ReNewCount);
				break;
			case 5:
				strFieldValue = String.valueOf(PayCount);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(RiskCalSort);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(RiskCode);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(RiskVersion);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCessStartDate()));
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCessEndDate()));
				break;
			case 11:
				strFieldValue = String.valueOf(CessionRate);
				break;
			case 12:
				strFieldValue = String.valueOf(CessionAmount);
				break;
			case 13:
				strFieldValue = String.valueOf(CessPremRate);
				break;
			case 14:
				strFieldValue = String.valueOf(CessPrem);
				break;
			case 15:
				strFieldValue = String.valueOf(CessCommRate);
				break;
			case 16:
				strFieldValue = String.valueOf(CessComm);
				break;
			case 17:
				strFieldValue = String.valueOf(ExPrem);
				break;
			case 18:
				strFieldValue = String.valueOf(ExCessPremRate);
				break;
			case 19:
				strFieldValue = String.valueOf(ExCessPrem);
				break;
			case 20:
				strFieldValue = String.valueOf(ExcessCommRate);
				break;
			case 21:
				strFieldValue = String.valueOf(ExCessComm);
				break;
			case 22:
				strFieldValue = String.valueOf(SumRiskAmount);
				break;
			case 23:
				strFieldValue = String.valueOf(NowRiskAmount);
				break;
			case 24:
				strFieldValue = String.valueOf(LvevlExAddFee);
				break;
			case 25:
				strFieldValue = String.valueOf(SpeCemmRate);
				break;
			case 26:
				strFieldValue = String.valueOf(SpeCemm);
				break;
			case 27:
				strFieldValue = String.valueOf(ReProcFeeRate);
				break;
			case 28:
				strFieldValue = String.valueOf(ReProcFee);
				break;
			case 29:
				strFieldValue = String.valueOf(ChoiRebaFeeRate);
				break;
			case 30:
				strFieldValue = String.valueOf(ChoiRebaFee);
				break;
			case 31:
				strFieldValue = String.valueOf(ManageFeeRate);
				break;
			case 32:
				strFieldValue = String.valueOf(ManageFee);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 39:
				strFieldValue = String.valueOf(CurrentPrem);
				break;
			case 40:
				strFieldValue = String.valueOf(ShouldCessPrem);
				break;
			case 41:
				strFieldValue = String.valueOf(ShouldCount);
				break;
			case 42:
				strFieldValue = String.valueOf(RemainCount);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("PolNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PolNo = FValue.trim();
			}
			else
				PolNo = null;
		}
		if (FCode.equalsIgnoreCase("ReContCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReContCode = FValue.trim();
			}
			else
				ReContCode = null;
		}
		if (FCode.equalsIgnoreCase("GetDutyCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GetDutyCode = FValue.trim();
			}
			else
				GetDutyCode = null;
		}
		if (FCode.equalsIgnoreCase("TempCessFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TempCessFlag = FValue.trim();
			}
			else
				TempCessFlag = null;
		}
		if (FCode.equalsIgnoreCase("ReNewCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				ReNewCount = i;
			}
		}
		if (FCode.equalsIgnoreCase("PayCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PayCount = i;
			}
		}
		if (FCode.equalsIgnoreCase("RiskCalSort"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskCalSort = FValue.trim();
			}
			else
				RiskCalSort = null;
		}
		if (FCode.equalsIgnoreCase("RiskCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskCode = FValue.trim();
			}
			else
				RiskCode = null;
		}
		if (FCode.equalsIgnoreCase("RiskVersion"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskVersion = FValue.trim();
			}
			else
				RiskVersion = null;
		}
		if (FCode.equalsIgnoreCase("CessStartDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CessStartDate = fDate.getDate( FValue );
			}
			else
				CessStartDate = null;
		}
		if (FCode.equalsIgnoreCase("CessEndDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CessEndDate = fDate.getDate( FValue );
			}
			else
				CessEndDate = null;
		}
		if (FCode.equalsIgnoreCase("CessionRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				CessionRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("CessionAmount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				CessionAmount = d;
			}
		}
		if (FCode.equalsIgnoreCase("CessPremRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				CessPremRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("CessPrem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				CessPrem = d;
			}
		}
		if (FCode.equalsIgnoreCase("CessCommRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				CessCommRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("CessComm"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				CessComm = d;
			}
		}
		if (FCode.equalsIgnoreCase("ExPrem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ExPrem = d;
			}
		}
		if (FCode.equalsIgnoreCase("ExCessPremRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ExCessPremRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("ExCessPrem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ExCessPrem = d;
			}
		}
		if (FCode.equalsIgnoreCase("ExcessCommRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ExcessCommRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("ExCessComm"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ExCessComm = d;
			}
		}
		if (FCode.equalsIgnoreCase("SumRiskAmount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SumRiskAmount = d;
			}
		}
		if (FCode.equalsIgnoreCase("NowRiskAmount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				NowRiskAmount = d;
			}
		}
		if (FCode.equalsIgnoreCase("LvevlExAddFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				LvevlExAddFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("SpeCemmRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SpeCemmRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("SpeCemm"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SpeCemm = d;
			}
		}
		if (FCode.equalsIgnoreCase("ReProcFeeRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ReProcFeeRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("ReProcFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ReProcFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("ChoiRebaFeeRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ChoiRebaFeeRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("ChoiRebaFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ChoiRebaFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("ManageFeeRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ManageFeeRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("ManageFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ManageFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("CurrentPrem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				CurrentPrem = d;
			}
		}
		if (FCode.equalsIgnoreCase("ShouldCessPrem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ShouldCessPrem = d;
			}
		}
		if (FCode.equalsIgnoreCase("ShouldCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				ShouldCount = i;
			}
		}
		if (FCode.equalsIgnoreCase("RemainCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				RemainCount = i;
			}
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LRPolDutyResultSchema other = (LRPolDutyResultSchema)otherObject;
		return
			PolNo.equals(other.getPolNo())
			&& ReContCode.equals(other.getReContCode())
			&& GetDutyCode.equals(other.getGetDutyCode())
			&& TempCessFlag.equals(other.getTempCessFlag())
			&& ReNewCount == other.getReNewCount()
			&& PayCount == other.getPayCount()
			&& RiskCalSort.equals(other.getRiskCalSort())
			&& RiskCode.equals(other.getRiskCode())
			&& RiskVersion.equals(other.getRiskVersion())
			&& fDate.getString(CessStartDate).equals(other.getCessStartDate())
			&& fDate.getString(CessEndDate).equals(other.getCessEndDate())
			&& CessionRate == other.getCessionRate()
			&& CessionAmount == other.getCessionAmount()
			&& CessPremRate == other.getCessPremRate()
			&& CessPrem == other.getCessPrem()
			&& CessCommRate == other.getCessCommRate()
			&& CessComm == other.getCessComm()
			&& ExPrem == other.getExPrem()
			&& ExCessPremRate == other.getExCessPremRate()
			&& ExCessPrem == other.getExCessPrem()
			&& ExcessCommRate == other.getExcessCommRate()
			&& ExCessComm == other.getExCessComm()
			&& SumRiskAmount == other.getSumRiskAmount()
			&& NowRiskAmount == other.getNowRiskAmount()
			&& LvevlExAddFee == other.getLvevlExAddFee()
			&& SpeCemmRate == other.getSpeCemmRate()
			&& SpeCemm == other.getSpeCemm()
			&& ReProcFeeRate == other.getReProcFeeRate()
			&& ReProcFee == other.getReProcFee()
			&& ChoiRebaFeeRate == other.getChoiRebaFeeRate()
			&& ChoiRebaFee == other.getChoiRebaFee()
			&& ManageFeeRate == other.getManageFeeRate()
			&& ManageFee == other.getManageFee()
			&& ManageCom.equals(other.getManageCom())
			&& Operator.equals(other.getOperator())
			&& fDate.getString(MakeDate).equals(other.getMakeDate())
			&& MakeTime.equals(other.getMakeTime())
			&& fDate.getString(ModifyDate).equals(other.getModifyDate())
			&& ModifyTime.equals(other.getModifyTime())
			&& CurrentPrem == other.getCurrentPrem()
			&& ShouldCessPrem == other.getShouldCessPrem()
			&& ShouldCount == other.getShouldCount()
			&& RemainCount == other.getRemainCount();
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("PolNo") ) {
			return 0;
		}
		if( strFieldName.equals("ReContCode") ) {
			return 1;
		}
		if( strFieldName.equals("GetDutyCode") ) {
			return 2;
		}
		if( strFieldName.equals("TempCessFlag") ) {
			return 3;
		}
		if( strFieldName.equals("ReNewCount") ) {
			return 4;
		}
		if( strFieldName.equals("PayCount") ) {
			return 5;
		}
		if( strFieldName.equals("RiskCalSort") ) {
			return 6;
		}
		if( strFieldName.equals("RiskCode") ) {
			return 7;
		}
		if( strFieldName.equals("RiskVersion") ) {
			return 8;
		}
		if( strFieldName.equals("CessStartDate") ) {
			return 9;
		}
		if( strFieldName.equals("CessEndDate") ) {
			return 10;
		}
		if( strFieldName.equals("CessionRate") ) {
			return 11;
		}
		if( strFieldName.equals("CessionAmount") ) {
			return 12;
		}
		if( strFieldName.equals("CessPremRate") ) {
			return 13;
		}
		if( strFieldName.equals("CessPrem") ) {
			return 14;
		}
		if( strFieldName.equals("CessCommRate") ) {
			return 15;
		}
		if( strFieldName.equals("CessComm") ) {
			return 16;
		}
		if( strFieldName.equals("ExPrem") ) {
			return 17;
		}
		if( strFieldName.equals("ExCessPremRate") ) {
			return 18;
		}
		if( strFieldName.equals("ExCessPrem") ) {
			return 19;
		}
		if( strFieldName.equals("ExcessCommRate") ) {
			return 20;
		}
		if( strFieldName.equals("ExCessComm") ) {
			return 21;
		}
		if( strFieldName.equals("SumRiskAmount") ) {
			return 22;
		}
		if( strFieldName.equals("NowRiskAmount") ) {
			return 23;
		}
		if( strFieldName.equals("LvevlExAddFee") ) {
			return 24;
		}
		if( strFieldName.equals("SpeCemmRate") ) {
			return 25;
		}
		if( strFieldName.equals("SpeCemm") ) {
			return 26;
		}
		if( strFieldName.equals("ReProcFeeRate") ) {
			return 27;
		}
		if( strFieldName.equals("ReProcFee") ) {
			return 28;
		}
		if( strFieldName.equals("ChoiRebaFeeRate") ) {
			return 29;
		}
		if( strFieldName.equals("ChoiRebaFee") ) {
			return 30;
		}
		if( strFieldName.equals("ManageFeeRate") ) {
			return 31;
		}
		if( strFieldName.equals("ManageFee") ) {
			return 32;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 33;
		}
		if( strFieldName.equals("Operator") ) {
			return 34;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 35;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 36;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 37;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 38;
		}
		if( strFieldName.equals("CurrentPrem") ) {
			return 39;
		}
		if( strFieldName.equals("ShouldCessPrem") ) {
			return 40;
		}
		if( strFieldName.equals("ShouldCount") ) {
			return 41;
		}
		if( strFieldName.equals("RemainCount") ) {
			return 42;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "PolNo";
				break;
			case 1:
				strFieldName = "ReContCode";
				break;
			case 2:
				strFieldName = "GetDutyCode";
				break;
			case 3:
				strFieldName = "TempCessFlag";
				break;
			case 4:
				strFieldName = "ReNewCount";
				break;
			case 5:
				strFieldName = "PayCount";
				break;
			case 6:
				strFieldName = "RiskCalSort";
				break;
			case 7:
				strFieldName = "RiskCode";
				break;
			case 8:
				strFieldName = "RiskVersion";
				break;
			case 9:
				strFieldName = "CessStartDate";
				break;
			case 10:
				strFieldName = "CessEndDate";
				break;
			case 11:
				strFieldName = "CessionRate";
				break;
			case 12:
				strFieldName = "CessionAmount";
				break;
			case 13:
				strFieldName = "CessPremRate";
				break;
			case 14:
				strFieldName = "CessPrem";
				break;
			case 15:
				strFieldName = "CessCommRate";
				break;
			case 16:
				strFieldName = "CessComm";
				break;
			case 17:
				strFieldName = "ExPrem";
				break;
			case 18:
				strFieldName = "ExCessPremRate";
				break;
			case 19:
				strFieldName = "ExCessPrem";
				break;
			case 20:
				strFieldName = "ExcessCommRate";
				break;
			case 21:
				strFieldName = "ExCessComm";
				break;
			case 22:
				strFieldName = "SumRiskAmount";
				break;
			case 23:
				strFieldName = "NowRiskAmount";
				break;
			case 24:
				strFieldName = "LvevlExAddFee";
				break;
			case 25:
				strFieldName = "SpeCemmRate";
				break;
			case 26:
				strFieldName = "SpeCemm";
				break;
			case 27:
				strFieldName = "ReProcFeeRate";
				break;
			case 28:
				strFieldName = "ReProcFee";
				break;
			case 29:
				strFieldName = "ChoiRebaFeeRate";
				break;
			case 30:
				strFieldName = "ChoiRebaFee";
				break;
			case 31:
				strFieldName = "ManageFeeRate";
				break;
			case 32:
				strFieldName = "ManageFee";
				break;
			case 33:
				strFieldName = "ManageCom";
				break;
			case 34:
				strFieldName = "Operator";
				break;
			case 35:
				strFieldName = "MakeDate";
				break;
			case 36:
				strFieldName = "MakeTime";
				break;
			case 37:
				strFieldName = "ModifyDate";
				break;
			case 38:
				strFieldName = "ModifyTime";
				break;
			case 39:
				strFieldName = "CurrentPrem";
				break;
			case 40:
				strFieldName = "ShouldCessPrem";
				break;
			case 41:
				strFieldName = "ShouldCount";
				break;
			case 42:
				strFieldName = "RemainCount";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("PolNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReContCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GetDutyCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TempCessFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReNewCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("PayCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("RiskCalSort") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskVersion") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CessStartDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("CessEndDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("CessionRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("CessionAmount") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("CessPremRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("CessPrem") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("CessCommRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("CessComm") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ExPrem") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ExCessPremRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ExCessPrem") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ExcessCommRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ExCessComm") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SumRiskAmount") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("NowRiskAmount") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("LvevlExAddFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SpeCemmRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SpeCemm") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ReProcFeeRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ReProcFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ChoiRebaFeeRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ChoiRebaFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ManageFeeRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ManageFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CurrentPrem") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ShouldCessPrem") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ShouldCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("RemainCount") ) {
			return Schema.TYPE_INT;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_INT;
				break;
			case 5:
				nFieldType = Schema.TYPE_INT;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 10:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 11:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 12:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 13:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 14:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 15:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 16:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 17:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 18:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 19:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 20:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 21:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 22:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 23:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 24:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 25:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 26:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 27:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 28:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 29:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 30:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 31:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 32:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 36:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 37:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 38:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 39:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 40:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 41:
				nFieldType = Schema.TYPE_INT;
				break;
			case 42:
				nFieldType = Schema.TYPE_INT;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
