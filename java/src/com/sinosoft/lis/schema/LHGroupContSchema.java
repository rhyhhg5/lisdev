/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LHGroupContDB;

/*
 * <p>ClassName: LHGroupContSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-09-23
 */
public class LHGroupContSchema implements Schema, Cloneable {
    // @Field
    /** 合同编号 */
    private String ContraNo;
    /** 合作机构代码 */
    private String HospitCode;
    /** 合同名称 */
    private String ContraName;
    /** 签订日期 */
    private Date IdiogrDate;
    /** 合同起始日期 */
    private Date ContraBeginDate;
    /** 合同终止日期 */
    private Date ContraEndDate;
    /** 合同状态 */
    private String ContraState;
    /** 甲方签订人代码 */
    private String FirstPerson;
    /** 乙方签订人 */
    private String SecondPerson;
    /** 联系人 */
    private String linkman;
    /** 联系电话 */
    private String Phone;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 管理机构 */
    private String ManageCom;

    public static final int FIELDNUM = 17; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LHGroupContSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "ContraNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LHGroupContSchema cloned = (LHGroupContSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getContraNo() {
        return ContraNo;
    }

    public void setContraNo(String aContraNo) {
        ContraNo = aContraNo;
    }

    public String getHospitCode() {
        return HospitCode;
    }

    public void setHospitCode(String aHospitCode) {
        HospitCode = aHospitCode;
    }

    public String getContraName() {
        return ContraName;
    }

    public void setContraName(String aContraName) {
        ContraName = aContraName;
    }

    public String getIdiogrDate() {
        if (IdiogrDate != null) {
            return fDate.getString(IdiogrDate);
        } else {
            return null;
        }
    }

    public void setIdiogrDate(Date aIdiogrDate) {
        IdiogrDate = aIdiogrDate;
    }

    public void setIdiogrDate(String aIdiogrDate) {
        if (aIdiogrDate != null && !aIdiogrDate.equals("")) {
            IdiogrDate = fDate.getDate(aIdiogrDate);
        } else {
            IdiogrDate = null;
        }
    }

    public String getContraBeginDate() {
        if (ContraBeginDate != null) {
            return fDate.getString(ContraBeginDate);
        } else {
            return null;
        }
    }

    public void setContraBeginDate(Date aContraBeginDate) {
        ContraBeginDate = aContraBeginDate;
    }

    public void setContraBeginDate(String aContraBeginDate) {
        if (aContraBeginDate != null && !aContraBeginDate.equals("")) {
            ContraBeginDate = fDate.getDate(aContraBeginDate);
        } else {
            ContraBeginDate = null;
        }
    }

    public String getContraEndDate() {
        if (ContraEndDate != null) {
            return fDate.getString(ContraEndDate);
        } else {
            return null;
        }
    }

    public void setContraEndDate(Date aContraEndDate) {
        ContraEndDate = aContraEndDate;
    }

    public void setContraEndDate(String aContraEndDate) {
        if (aContraEndDate != null && !aContraEndDate.equals("")) {
            ContraEndDate = fDate.getDate(aContraEndDate);
        } else {
            ContraEndDate = null;
        }
    }

    public String getContraState() {
        return ContraState;
    }

    public void setContraState(String aContraState) {
        ContraState = aContraState;
    }

    public String getFirstPerson() {
        return FirstPerson;
    }

    public void setFirstPerson(String aFirstPerson) {
        FirstPerson = aFirstPerson;
    }

    public String getSecondPerson() {
        return SecondPerson;
    }

    public void setSecondPerson(String aSecondPerson) {
        SecondPerson = aSecondPerson;
    }

    public String getlinkman() {
        return linkman;
    }

    public void setlinkman(String alinkman) {
        linkman = alinkman;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String aPhone) {
        Phone = aPhone;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }

    /**
     * 使用另外一个 LHGroupContSchema 对象给 Schema 赋值
     * @param: aLHGroupContSchema LHGroupContSchema
     **/
    public void setSchema(LHGroupContSchema aLHGroupContSchema) {
        this.ContraNo = aLHGroupContSchema.getContraNo();
        this.HospitCode = aLHGroupContSchema.getHospitCode();
        this.ContraName = aLHGroupContSchema.getContraName();
        this.IdiogrDate = fDate.getDate(aLHGroupContSchema.getIdiogrDate());
        this.ContraBeginDate = fDate.getDate(aLHGroupContSchema.
                                             getContraBeginDate());
        this.ContraEndDate = fDate.getDate(aLHGroupContSchema.getContraEndDate());
        this.ContraState = aLHGroupContSchema.getContraState();
        this.FirstPerson = aLHGroupContSchema.getFirstPerson();
        this.SecondPerson = aLHGroupContSchema.getSecondPerson();
        this.linkman = aLHGroupContSchema.getlinkman();
        this.Phone = aLHGroupContSchema.getPhone();
        this.Operator = aLHGroupContSchema.getOperator();
        this.MakeDate = fDate.getDate(aLHGroupContSchema.getMakeDate());
        this.MakeTime = aLHGroupContSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLHGroupContSchema.getModifyDate());
        this.ModifyTime = aLHGroupContSchema.getModifyTime();
        this.ManageCom = aLHGroupContSchema.getManageCom();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ContraNo") == null) {
                this.ContraNo = null;
            } else {
                this.ContraNo = rs.getString("ContraNo").trim();
            }

            if (rs.getString("HospitCode") == null) {
                this.HospitCode = null;
            } else {
                this.HospitCode = rs.getString("HospitCode").trim();
            }

            if (rs.getString("ContraName") == null) {
                this.ContraName = null;
            } else {
                this.ContraName = rs.getString("ContraName").trim();
            }

            this.IdiogrDate = rs.getDate("IdiogrDate");
            this.ContraBeginDate = rs.getDate("ContraBeginDate");
            this.ContraEndDate = rs.getDate("ContraEndDate");
            if (rs.getString("ContraState") == null) {
                this.ContraState = null;
            } else {
                this.ContraState = rs.getString("ContraState").trim();
            }

            if (rs.getString("FirstPerson") == null) {
                this.FirstPerson = null;
            } else {
                this.FirstPerson = rs.getString("FirstPerson").trim();
            }

            if (rs.getString("SecondPerson") == null) {
                this.SecondPerson = null;
            } else {
                this.SecondPerson = rs.getString("SecondPerson").trim();
            }

            if (rs.getString("linkman") == null) {
                this.linkman = null;
            } else {
                this.linkman = rs.getString("linkman").trim();
            }

            if (rs.getString("Phone") == null) {
                this.Phone = null;
            } else {
                this.Phone = rs.getString("Phone").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("ManageCom") == null) {
                this.ManageCom = null;
            } else {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LHGroupCont表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHGroupContSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LHGroupContSchema getSchema() {
        LHGroupContSchema aLHGroupContSchema = new LHGroupContSchema();
        aLHGroupContSchema.setSchema(this);
        return aLHGroupContSchema;
    }

    public LHGroupContDB getDB() {
        LHGroupContDB aDBOper = new LHGroupContDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHGroupCont描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(ContraNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(HospitCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContraName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(IdiogrDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ContraBeginDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ContraEndDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContraState));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FirstPerson));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SecondPerson));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(linkman));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Phone));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHGroupCont>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            ContraNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            HospitCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
            ContraName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                        SysConst.PACKAGESPILTER);
            IdiogrDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 4, SysConst.PACKAGESPILTER));
            ContraBeginDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 5, SysConst.PACKAGESPILTER));
            ContraEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 6, SysConst.PACKAGESPILTER));
            ContraState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                         SysConst.PACKAGESPILTER);
            FirstPerson = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                         SysConst.PACKAGESPILTER);
            SecondPerson = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                          SysConst.PACKAGESPILTER);
            linkman = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                     SysConst.PACKAGESPILTER);
            Phone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                   SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 13, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 15, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                        SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                       SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHGroupContSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("ContraNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContraNo));
        }
        if (FCode.equals("HospitCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HospitCode));
        }
        if (FCode.equals("ContraName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContraName));
        }
        if (FCode.equals("IdiogrDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getIdiogrDate()));
        }
        if (FCode.equals("ContraBeginDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getContraBeginDate()));
        }
        if (FCode.equals("ContraEndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getContraEndDate()));
        }
        if (FCode.equals("ContraState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContraState));
        }
        if (FCode.equals("FirstPerson")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FirstPerson));
        }
        if (FCode.equals("SecondPerson")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SecondPerson));
        }
        if (FCode.equals("linkman")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(linkman));
        }
        if (FCode.equals("Phone")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Phone));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(ContraNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(HospitCode);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(ContraName);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getIdiogrDate()));
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getContraBeginDate()));
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getContraEndDate()));
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(ContraState);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(FirstPerson);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(SecondPerson);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(linkman);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(Phone);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 15:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        case 16:
            strFieldValue = StrTool.GBKToUnicode(ManageCom);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("ContraNo")) {
            if (FValue != null && !FValue.equals("")) {
                ContraNo = FValue.trim();
            } else {
                ContraNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("HospitCode")) {
            if (FValue != null && !FValue.equals("")) {
                HospitCode = FValue.trim();
            } else {
                HospitCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("ContraName")) {
            if (FValue != null && !FValue.equals("")) {
                ContraName = FValue.trim();
            } else {
                ContraName = null;
            }
        }
        if (FCode.equalsIgnoreCase("IdiogrDate")) {
            if (FValue != null && !FValue.equals("")) {
                IdiogrDate = fDate.getDate(FValue);
            } else {
                IdiogrDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ContraBeginDate")) {
            if (FValue != null && !FValue.equals("")) {
                ContraBeginDate = fDate.getDate(FValue);
            } else {
                ContraBeginDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ContraEndDate")) {
            if (FValue != null && !FValue.equals("")) {
                ContraEndDate = fDate.getDate(FValue);
            } else {
                ContraEndDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ContraState")) {
            if (FValue != null && !FValue.equals("")) {
                ContraState = FValue.trim();
            } else {
                ContraState = null;
            }
        }
        if (FCode.equalsIgnoreCase("FirstPerson")) {
            if (FValue != null && !FValue.equals("")) {
                FirstPerson = FValue.trim();
            } else {
                FirstPerson = null;
            }
        }
        if (FCode.equalsIgnoreCase("SecondPerson")) {
            if (FValue != null && !FValue.equals("")) {
                SecondPerson = FValue.trim();
            } else {
                SecondPerson = null;
            }
        }
        if (FCode.equalsIgnoreCase("linkman")) {
            if (FValue != null && !FValue.equals("")) {
                linkman = FValue.trim();
            } else {
                linkman = null;
            }
        }
        if (FCode.equalsIgnoreCase("Phone")) {
            if (FValue != null && !FValue.equals("")) {
                Phone = FValue.trim();
            } else {
                Phone = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ManageCom = FValue.trim();
            } else {
                ManageCom = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LHGroupContSchema other = (LHGroupContSchema) otherObject;
        return
                ContraNo.equals(other.getContraNo())
                && HospitCode.equals(other.getHospitCode())
                && ContraName.equals(other.getContraName())
                && fDate.getString(IdiogrDate).equals(other.getIdiogrDate())
                &&
                fDate.getString(ContraBeginDate).equals(other.getContraBeginDate())
                && fDate.getString(ContraEndDate).equals(other.getContraEndDate())
                && ContraState.equals(other.getContraState())
                && FirstPerson.equals(other.getFirstPerson())
                && SecondPerson.equals(other.getSecondPerson())
                && linkman.equals(other.getlinkman())
                && Phone.equals(other.getPhone())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && ManageCom.equals(other.getManageCom());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("ContraNo")) {
            return 0;
        }
        if (strFieldName.equals("HospitCode")) {
            return 1;
        }
        if (strFieldName.equals("ContraName")) {
            return 2;
        }
        if (strFieldName.equals("IdiogrDate")) {
            return 3;
        }
        if (strFieldName.equals("ContraBeginDate")) {
            return 4;
        }
        if (strFieldName.equals("ContraEndDate")) {
            return 5;
        }
        if (strFieldName.equals("ContraState")) {
            return 6;
        }
        if (strFieldName.equals("FirstPerson")) {
            return 7;
        }
        if (strFieldName.equals("SecondPerson")) {
            return 8;
        }
        if (strFieldName.equals("linkman")) {
            return 9;
        }
        if (strFieldName.equals("Phone")) {
            return 10;
        }
        if (strFieldName.equals("Operator")) {
            return 11;
        }
        if (strFieldName.equals("MakeDate")) {
            return 12;
        }
        if (strFieldName.equals("MakeTime")) {
            return 13;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 14;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 15;
        }
        if (strFieldName.equals("ManageCom")) {
            return 16;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "ContraNo";
            break;
        case 1:
            strFieldName = "HospitCode";
            break;
        case 2:
            strFieldName = "ContraName";
            break;
        case 3:
            strFieldName = "IdiogrDate";
            break;
        case 4:
            strFieldName = "ContraBeginDate";
            break;
        case 5:
            strFieldName = "ContraEndDate";
            break;
        case 6:
            strFieldName = "ContraState";
            break;
        case 7:
            strFieldName = "FirstPerson";
            break;
        case 8:
            strFieldName = "SecondPerson";
            break;
        case 9:
            strFieldName = "linkman";
            break;
        case 10:
            strFieldName = "Phone";
            break;
        case 11:
            strFieldName = "Operator";
            break;
        case 12:
            strFieldName = "MakeDate";
            break;
        case 13:
            strFieldName = "MakeTime";
            break;
        case 14:
            strFieldName = "ModifyDate";
            break;
        case 15:
            strFieldName = "ModifyTime";
            break;
        case 16:
            strFieldName = "ManageCom";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("ContraNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("HospitCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContraName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("IdiogrDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ContraBeginDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ContraEndDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ContraState")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FirstPerson")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SecondPerson")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("linkman")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Phone")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 4:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 5:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 12:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 13:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 14:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 15:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 16:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
