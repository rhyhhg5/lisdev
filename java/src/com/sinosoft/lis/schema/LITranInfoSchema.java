/*
 * <p>ClassName: LITranInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LITranInfoDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LITranInfoSchema implements Schema
{
    // @Field
    /** 批次号 */
    private String BatchNo;
    /** 单证号 */
    private String BillId;
    /** 业务号 */
    private String BussNo;
    /** 保单号码 */
    private String PolNo;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 业务日期 */
    private Date BussDate;
    /** 凭证号 */
    private int VoucherID;
    /** 管理机构 */
    private String ManageCom;

    public static final int FIELDNUM = 9; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LITranInfoSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[4];
        pk[0] = "BatchNo";
        pk[1] = "BillId";
        pk[2] = "BussNo";
        pk[3] = "PolNo";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getBatchNo()
    {
        if (BatchNo != null && !BatchNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            BatchNo = StrTool.unicodeToGBK(BatchNo);
        }
        return BatchNo;
    }

    public void setBatchNo(String aBatchNo)
    {
        BatchNo = aBatchNo;
    }

    public String getBillId()
    {
        if (BillId != null && !BillId.equals("") && SysConst.CHANGECHARSET == true)
        {
            BillId = StrTool.unicodeToGBK(BillId);
        }
        return BillId;
    }

    public void setBillId(String aBillId)
    {
        BillId = aBillId;
    }

    public String getBussNo()
    {
        if (BussNo != null && !BussNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            BussNo = StrTool.unicodeToGBK(BussNo);
        }
        return BussNo;
    }

    public void setBussNo(String aBussNo)
    {
        BussNo = aBussNo;
    }

    public String getPolNo()
    {
        if (PolNo != null && !PolNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            PolNo = StrTool.unicodeToGBK(PolNo);
        }
        return PolNo;
    }

    public void setPolNo(String aPolNo)
    {
        PolNo = aPolNo;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getBussDate()
    {
        if (BussDate != null)
        {
            return fDate.getString(BussDate);
        }
        else
        {
            return null;
        }
    }

    public void setBussDate(Date aBussDate)
    {
        BussDate = aBussDate;
    }

    public void setBussDate(String aBussDate)
    {
        if (aBussDate != null && !aBussDate.equals(""))
        {
            BussDate = fDate.getDate(aBussDate);
        }
        else
        {
            BussDate = null;
        }
    }

    public int getVoucherID()
    {
        return VoucherID;
    }

    public void setVoucherID(int aVoucherID)
    {
        VoucherID = aVoucherID;
    }

    public void setVoucherID(String aVoucherID)
    {
        if (aVoucherID != null && !aVoucherID.equals(""))
        {
            Integer tInteger = new Integer(aVoucherID);
            int i = tInteger.intValue();
            VoucherID = i;
        }
    }

    public String getManageCom()
    {
        if (ManageCom != null && !ManageCom.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ManageCom = StrTool.unicodeToGBK(ManageCom);
        }
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    /**
     * 使用另外一个 LITranInfoSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LITranInfoSchema aLITranInfoSchema)
    {
        this.BatchNo = aLITranInfoSchema.getBatchNo();
        this.BillId = aLITranInfoSchema.getBillId();
        this.BussNo = aLITranInfoSchema.getBussNo();
        this.PolNo = aLITranInfoSchema.getPolNo();
        this.MakeDate = fDate.getDate(aLITranInfoSchema.getMakeDate());
        this.MakeTime = aLITranInfoSchema.getMakeTime();
        this.BussDate = fDate.getDate(aLITranInfoSchema.getBussDate());
        this.VoucherID = aLITranInfoSchema.getVoucherID();
        this.ManageCom = aLITranInfoSchema.getManageCom();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("BatchNo") == null)
            {
                this.BatchNo = null;
            }
            else
            {
                this.BatchNo = rs.getString("BatchNo").trim();
            }

            if (rs.getString("BillId") == null)
            {
                this.BillId = null;
            }
            else
            {
                this.BillId = rs.getString("BillId").trim();
            }

            if (rs.getString("BussNo") == null)
            {
                this.BussNo = null;
            }
            else
            {
                this.BussNo = rs.getString("BussNo").trim();
            }

            if (rs.getString("PolNo") == null)
            {
                this.PolNo = null;
            }
            else
            {
                this.PolNo = rs.getString("PolNo").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.BussDate = rs.getDate("BussDate");
            this.VoucherID = rs.getInt("VoucherID");
            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LITranInfoSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LITranInfoSchema getSchema()
    {
        LITranInfoSchema aLITranInfoSchema = new LITranInfoSchema();
        aLITranInfoSchema.setSchema(this);
        return aLITranInfoSchema;
    }

    public LITranInfoDB getDB()
    {
        LITranInfoDB aDBOper = new LITranInfoDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLITranInfo描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(BatchNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(BillId)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(BussNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PolNo)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(BussDate))) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(VoucherID) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ManageCom));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLITranInfo>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            BatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                     SysConst.PACKAGESPILTER);
            BillId = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                    SysConst.PACKAGESPILTER);
            BussNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                    SysConst.PACKAGESPILTER);
            PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                   SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 5, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                      SysConst.PACKAGESPILTER);
            BussDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 7, SysConst.PACKAGESPILTER));
            VoucherID = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 8, SysConst.PACKAGESPILTER))).intValue();
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                       SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LITranInfoSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("BatchNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(BatchNo));
        }
        if (FCode.equals("BillId"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(BillId));
        }
        if (FCode.equals("BussNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(BussNo));
        }
        if (FCode.equals("PolNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PolNo));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("BussDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getBussDate()));
        }
        if (FCode.equals("VoucherID"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(VoucherID));
        }
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ManageCom));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(BatchNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(BillId);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(BussNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(PolNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getBussDate()));
                break;
            case 7:
                strFieldValue = String.valueOf(VoucherID);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("BatchNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BatchNo = FValue.trim();
            }
            else
            {
                BatchNo = null;
            }
        }
        if (FCode.equals("BillId"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BillId = FValue.trim();
            }
            else
            {
                BillId = null;
            }
        }
        if (FCode.equals("BussNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BussNo = FValue.trim();
            }
            else
            {
                BussNo = null;
            }
        }
        if (FCode.equals("PolNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
            {
                PolNo = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("BussDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BussDate = fDate.getDate(FValue);
            }
            else
            {
                BussDate = null;
            }
        }
        if (FCode.equals("VoucherID"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                VoucherID = i;
            }
        }
        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LITranInfoSchema other = (LITranInfoSchema) otherObject;
        return
                BatchNo.equals(other.getBatchNo())
                && BillId.equals(other.getBillId())
                && BussNo.equals(other.getBussNo())
                && PolNo.equals(other.getPolNo())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(BussDate).equals(other.getBussDate())
                && VoucherID == other.getVoucherID()
                && ManageCom.equals(other.getManageCom());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("BatchNo"))
        {
            return 0;
        }
        if (strFieldName.equals("BillId"))
        {
            return 1;
        }
        if (strFieldName.equals("BussNo"))
        {
            return 2;
        }
        if (strFieldName.equals("PolNo"))
        {
            return 3;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 4;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 5;
        }
        if (strFieldName.equals("BussDate"))
        {
            return 6;
        }
        if (strFieldName.equals("VoucherID"))
        {
            return 7;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return 8;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "BatchNo";
                break;
            case 1:
                strFieldName = "BillId";
                break;
            case 2:
                strFieldName = "BussNo";
                break;
            case 3:
                strFieldName = "PolNo";
                break;
            case 4:
                strFieldName = "MakeDate";
                break;
            case 5:
                strFieldName = "MakeTime";
                break;
            case 6:
                strFieldName = "BussDate";
                break;
            case 7:
                strFieldName = "VoucherID";
                break;
            case 8:
                strFieldName = "ManageCom";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("BatchNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BillId"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BussNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PolNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BussDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("VoucherID"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 7:
                nFieldType = Schema.TYPE_INT;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
