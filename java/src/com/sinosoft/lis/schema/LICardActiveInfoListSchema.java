/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LICardActiveInfoListDB;

/*
 * <p>ClassName: LICardActiveInfoListSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 新寿险业务系统模型
 * @CreateDate：2012-08-08
 */
public class LICardActiveInfoListSchema implements Schema, Cloneable
{
	// @Field
	/** 批次号 */
	private String BatchNo;
	/** 单证号 */
	private String CardNo;
	/** 被保人流水号 */
	private String SequenceNo;
	/** 卡类型 */
	private String CardType;
	/** 生效日期 */
	private Date CValidate;
	/** 激活日期 */
	private Date ActiveDate;
	/** 激活地址 */
	private String ActiveIP;
	/** 处理标志 */
	private String DealFlag;
	/** 处理日期 */
	private Date DealDate;
	/** 姓名 */
	private String Name;
	/** 性别 */
	private String Sex;
	/** 出生日期 */
	private Date Birthday;
	/** 证件类别 */
	private String IdType;
	/** 证件号 */
	private String IdNo;
	/** 职业类别 */
	private String OccupationType;
	/** 职业代码 */
	private String OccupationCode;
	/** 通讯地址 */
	private String PostalAddress;
	/** 通讯邮编 */
	private String ZipCode;
	/** 通讯电话 */
	private String Phone;
	/** 手机 */
	private String Mobile;
	/** 单位名称 */
	private String GrpName;
	/** 单位电话 */
	private String CompanyPhone;
	/** E_mail */
	private String EMail;
	/** 状态 */
	private String State;
	/** 操作员 */
	private String Operator;
	/** 创建日期 */
	private Date MakeDate;
	/** 创建时间 */
	private String MakeTime;
	/** 修改日期 */
	private Date ModifyDate;
	/** 修改时间 */
	private String ModifyTime;
	/** 保费 */
	private String Prem;
	/** 保额 */
	private String Amnt;
	/** 档次 */
	private String Mult;
	/** 份数 */
	private String Copys;
	/** 票号 */
	private String TicketNo;
	/** 班次 */
	private String TeamNo;
	/** 座位号 */
	private String SeatNo;
	/** 起始站 */
	private String From;
	/** 终点站 */
	private String To;
	/** 说明 */
	private String Remark;
	/** 卡状态 */
	private String CardStatus;
	/** 保障期间 */
	private int InsuYear;
	/** 保障期间标志 */
	private String InsuYearFlag;
	/** 终止日期 */
	private Date InActiveDate;

	public static final int FIELDNUM = 43;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LICardActiveInfoListSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "CardNo";
		pk[1] = "SequenceNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LICardActiveInfoListSchema cloned = (LICardActiveInfoListSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getBatchNo()
	{
		return BatchNo;
	}
	public void setBatchNo(String aBatchNo)
	{
		BatchNo = aBatchNo;
	}
	public String getCardNo()
	{
		return CardNo;
	}
	public void setCardNo(String aCardNo)
	{
		CardNo = aCardNo;
	}
	public String getSequenceNo()
	{
		return SequenceNo;
	}
	public void setSequenceNo(String aSequenceNo)
	{
		SequenceNo = aSequenceNo;
	}
	public String getCardType()
	{
		return CardType;
	}
	public void setCardType(String aCardType)
	{
		CardType = aCardType;
	}
	public String getCValidate()
	{
		if( CValidate != null )
			return fDate.getString(CValidate);
		else
			return null;
	}
	public void setCValidate(Date aCValidate)
	{
		CValidate = aCValidate;
	}
	public void setCValidate(String aCValidate)
	{
		if (aCValidate != null && !aCValidate.equals("") )
		{
			CValidate = fDate.getDate( aCValidate );
		}
		else
			CValidate = null;
	}

	public String getActiveDate()
	{
		if( ActiveDate != null )
			return fDate.getString(ActiveDate);
		else
			return null;
	}
	public void setActiveDate(Date aActiveDate)
	{
		ActiveDate = aActiveDate;
	}
	public void setActiveDate(String aActiveDate)
	{
		if (aActiveDate != null && !aActiveDate.equals("") )
		{
			ActiveDate = fDate.getDate( aActiveDate );
		}
		else
			ActiveDate = null;
	}

	public String getActiveIP()
	{
		return ActiveIP;
	}
	public void setActiveIP(String aActiveIP)
	{
		ActiveIP = aActiveIP;
	}
	public String getDealFlag()
	{
		return DealFlag;
	}
	public void setDealFlag(String aDealFlag)
	{
		DealFlag = aDealFlag;
	}
	public String getDealDate()
	{
		if( DealDate != null )
			return fDate.getString(DealDate);
		else
			return null;
	}
	public void setDealDate(Date aDealDate)
	{
		DealDate = aDealDate;
	}
	public void setDealDate(String aDealDate)
	{
		if (aDealDate != null && !aDealDate.equals("") )
		{
			DealDate = fDate.getDate( aDealDate );
		}
		else
			DealDate = null;
	}

	public String getName()
	{
		return Name;
	}
	public void setName(String aName)
	{
		Name = aName;
	}
	public String getSex()
	{
		return Sex;
	}
	public void setSex(String aSex)
	{
		Sex = aSex;
	}
	public String getBirthday()
	{
		if( Birthday != null )
			return fDate.getString(Birthday);
		else
			return null;
	}
	public void setBirthday(Date aBirthday)
	{
		Birthday = aBirthday;
	}
	public void setBirthday(String aBirthday)
	{
		if (aBirthday != null && !aBirthday.equals("") )
		{
			Birthday = fDate.getDate( aBirthday );
		}
		else
			Birthday = null;
	}

	public String getIdType()
	{
		return IdType;
	}
	public void setIdType(String aIdType)
	{
		IdType = aIdType;
	}
	public String getIdNo()
	{
		return IdNo;
	}
	public void setIdNo(String aIdNo)
	{
		IdNo = aIdNo;
	}
	public String getOccupationType()
	{
		return OccupationType;
	}
	public void setOccupationType(String aOccupationType)
	{
		OccupationType = aOccupationType;
	}
	public String getOccupationCode()
	{
		return OccupationCode;
	}
	public void setOccupationCode(String aOccupationCode)
	{
		OccupationCode = aOccupationCode;
	}
	public String getPostalAddress()
	{
		return PostalAddress;
	}
	public void setPostalAddress(String aPostalAddress)
	{
		PostalAddress = aPostalAddress;
	}
	public String getZipCode()
	{
		return ZipCode;
	}
	public void setZipCode(String aZipCode)
	{
		ZipCode = aZipCode;
	}
	public String getPhone()
	{
		return Phone;
	}
	public void setPhone(String aPhone)
	{
		Phone = aPhone;
	}
	public String getMobile()
	{
		return Mobile;
	}
	public void setMobile(String aMobile)
	{
		Mobile = aMobile;
	}
	public String getGrpName()
	{
		return GrpName;
	}
	public void setGrpName(String aGrpName)
	{
		GrpName = aGrpName;
	}
	public String getCompanyPhone()
	{
		return CompanyPhone;
	}
	public void setCompanyPhone(String aCompanyPhone)
	{
		CompanyPhone = aCompanyPhone;
	}
	public String getEMail()
	{
		return EMail;
	}
	public void setEMail(String aEMail)
	{
		EMail = aEMail;
	}
	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getPrem()
	{
		return Prem;
	}
	public void setPrem(String aPrem)
	{
		Prem = aPrem;
	}
	public String getAmnt()
	{
		return Amnt;
	}
	public void setAmnt(String aAmnt)
	{
		Amnt = aAmnt;
	}
	public String getMult()
	{
		return Mult;
	}
	public void setMult(String aMult)
	{
		Mult = aMult;
	}
	public String getCopys()
	{
		return Copys;
	}
	public void setCopys(String aCopys)
	{
		Copys = aCopys;
	}
	public String getTicketNo()
	{
		return TicketNo;
	}
	public void setTicketNo(String aTicketNo)
	{
		TicketNo = aTicketNo;
	}
	public String getTeamNo()
	{
		return TeamNo;
	}
	public void setTeamNo(String aTeamNo)
	{
		TeamNo = aTeamNo;
	}
	public String getSeatNo()
	{
		return SeatNo;
	}
	public void setSeatNo(String aSeatNo)
	{
		SeatNo = aSeatNo;
	}
	public String getFrom()
	{
		return From;
	}
	public void setFrom(String aFrom)
	{
		From = aFrom;
	}
	public String getTo()
	{
		return To;
	}
	public void setTo(String aTo)
	{
		To = aTo;
	}
	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
		Remark = aRemark;
	}
	public String getCardStatus()
	{
		return CardStatus;
	}
	public void setCardStatus(String aCardStatus)
	{
		CardStatus = aCardStatus;
	}
	public int getInsuYear()
	{
		return InsuYear;
	}
	public void setInsuYear(int aInsuYear)
	{
		InsuYear = aInsuYear;
	}
	public void setInsuYear(String aInsuYear)
	{
		if (aInsuYear != null && !aInsuYear.equals(""))
		{
			Integer tInteger = new Integer(aInsuYear);
			int i = tInteger.intValue();
			InsuYear = i;
		}
	}

	public String getInsuYearFlag()
	{
		return InsuYearFlag;
	}
	public void setInsuYearFlag(String aInsuYearFlag)
	{
		InsuYearFlag = aInsuYearFlag;
	}
	public String getInActiveDate()
	{
		if( InActiveDate != null )
			return fDate.getString(InActiveDate);
		else
			return null;
	}
	public void setInActiveDate(Date aInActiveDate)
	{
		InActiveDate = aInActiveDate;
	}
	public void setInActiveDate(String aInActiveDate)
	{
		if (aInActiveDate != null && !aInActiveDate.equals("") )
		{
			InActiveDate = fDate.getDate( aInActiveDate );
		}
		else
			InActiveDate = null;
	}


	/**
	* 使用另外一个 LICardActiveInfoListSchema 对象给 Schema 赋值
	* @param: aLICardActiveInfoListSchema LICardActiveInfoListSchema
	**/
	public void setSchema(LICardActiveInfoListSchema aLICardActiveInfoListSchema)
	{
		this.BatchNo = aLICardActiveInfoListSchema.getBatchNo();
		this.CardNo = aLICardActiveInfoListSchema.getCardNo();
		this.SequenceNo = aLICardActiveInfoListSchema.getSequenceNo();
		this.CardType = aLICardActiveInfoListSchema.getCardType();
		this.CValidate = fDate.getDate( aLICardActiveInfoListSchema.getCValidate());
		this.ActiveDate = fDate.getDate( aLICardActiveInfoListSchema.getActiveDate());
		this.ActiveIP = aLICardActiveInfoListSchema.getActiveIP();
		this.DealFlag = aLICardActiveInfoListSchema.getDealFlag();
		this.DealDate = fDate.getDate( aLICardActiveInfoListSchema.getDealDate());
		this.Name = aLICardActiveInfoListSchema.getName();
		this.Sex = aLICardActiveInfoListSchema.getSex();
		this.Birthday = fDate.getDate( aLICardActiveInfoListSchema.getBirthday());
		this.IdType = aLICardActiveInfoListSchema.getIdType();
		this.IdNo = aLICardActiveInfoListSchema.getIdNo();
		this.OccupationType = aLICardActiveInfoListSchema.getOccupationType();
		this.OccupationCode = aLICardActiveInfoListSchema.getOccupationCode();
		this.PostalAddress = aLICardActiveInfoListSchema.getPostalAddress();
		this.ZipCode = aLICardActiveInfoListSchema.getZipCode();
		this.Phone = aLICardActiveInfoListSchema.getPhone();
		this.Mobile = aLICardActiveInfoListSchema.getMobile();
		this.GrpName = aLICardActiveInfoListSchema.getGrpName();
		this.CompanyPhone = aLICardActiveInfoListSchema.getCompanyPhone();
		this.EMail = aLICardActiveInfoListSchema.getEMail();
		this.State = aLICardActiveInfoListSchema.getState();
		this.Operator = aLICardActiveInfoListSchema.getOperator();
		this.MakeDate = fDate.getDate( aLICardActiveInfoListSchema.getMakeDate());
		this.MakeTime = aLICardActiveInfoListSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLICardActiveInfoListSchema.getModifyDate());
		this.ModifyTime = aLICardActiveInfoListSchema.getModifyTime();
		this.Prem = aLICardActiveInfoListSchema.getPrem();
		this.Amnt = aLICardActiveInfoListSchema.getAmnt();
		this.Mult = aLICardActiveInfoListSchema.getMult();
		this.Copys = aLICardActiveInfoListSchema.getCopys();
		this.TicketNo = aLICardActiveInfoListSchema.getTicketNo();
		this.TeamNo = aLICardActiveInfoListSchema.getTeamNo();
		this.SeatNo = aLICardActiveInfoListSchema.getSeatNo();
		this.From = aLICardActiveInfoListSchema.getFrom();
		this.To = aLICardActiveInfoListSchema.getTo();
		this.Remark = aLICardActiveInfoListSchema.getRemark();
		this.CardStatus = aLICardActiveInfoListSchema.getCardStatus();
		this.InsuYear = aLICardActiveInfoListSchema.getInsuYear();
		this.InsuYearFlag = aLICardActiveInfoListSchema.getInsuYearFlag();
		this.InActiveDate = fDate.getDate( aLICardActiveInfoListSchema.getInActiveDate());
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("BatchNo") == null )
				this.BatchNo = null;
			else
				this.BatchNo = rs.getString("BatchNo").trim();

			if( rs.getString("CardNo") == null )
				this.CardNo = null;
			else
				this.CardNo = rs.getString("CardNo").trim();

			if( rs.getString("SequenceNo") == null )
				this.SequenceNo = null;
			else
				this.SequenceNo = rs.getString("SequenceNo").trim();

			if( rs.getString("CardType") == null )
				this.CardType = null;
			else
				this.CardType = rs.getString("CardType").trim();

			this.CValidate = rs.getDate("CValidate");
			this.ActiveDate = rs.getDate("ActiveDate");
			if( rs.getString("ActiveIP") == null )
				this.ActiveIP = null;
			else
				this.ActiveIP = rs.getString("ActiveIP").trim();

			if( rs.getString("DealFlag") == null )
				this.DealFlag = null;
			else
				this.DealFlag = rs.getString("DealFlag").trim();

			this.DealDate = rs.getDate("DealDate");
			if( rs.getString("Name") == null )
				this.Name = null;
			else
				this.Name = rs.getString("Name").trim();

			if( rs.getString("Sex") == null )
				this.Sex = null;
			else
				this.Sex = rs.getString("Sex").trim();

			this.Birthday = rs.getDate("Birthday");
			if( rs.getString("IdType") == null )
				this.IdType = null;
			else
				this.IdType = rs.getString("IdType").trim();

			if( rs.getString("IdNo") == null )
				this.IdNo = null;
			else
				this.IdNo = rs.getString("IdNo").trim();

			if( rs.getString("OccupationType") == null )
				this.OccupationType = null;
			else
				this.OccupationType = rs.getString("OccupationType").trim();

			if( rs.getString("OccupationCode") == null )
				this.OccupationCode = null;
			else
				this.OccupationCode = rs.getString("OccupationCode").trim();

			if( rs.getString("PostalAddress") == null )
				this.PostalAddress = null;
			else
				this.PostalAddress = rs.getString("PostalAddress").trim();

			if( rs.getString("ZipCode") == null )
				this.ZipCode = null;
			else
				this.ZipCode = rs.getString("ZipCode").trim();

			if( rs.getString("Phone") == null )
				this.Phone = null;
			else
				this.Phone = rs.getString("Phone").trim();

			if( rs.getString("Mobile") == null )
				this.Mobile = null;
			else
				this.Mobile = rs.getString("Mobile").trim();

			if( rs.getString("GrpName") == null )
				this.GrpName = null;
			else
				this.GrpName = rs.getString("GrpName").trim();

			if( rs.getString("CompanyPhone") == null )
				this.CompanyPhone = null;
			else
				this.CompanyPhone = rs.getString("CompanyPhone").trim();

			if( rs.getString("EMail") == null )
				this.EMail = null;
			else
				this.EMail = rs.getString("EMail").trim();

			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("Prem") == null )
				this.Prem = null;
			else
				this.Prem = rs.getString("Prem").trim();

			if( rs.getString("Amnt") == null )
				this.Amnt = null;
			else
				this.Amnt = rs.getString("Amnt").trim();

			if( rs.getString("Mult") == null )
				this.Mult = null;
			else
				this.Mult = rs.getString("Mult").trim();

			if( rs.getString("Copys") == null )
				this.Copys = null;
			else
				this.Copys = rs.getString("Copys").trim();

			if( rs.getString("TicketNo") == null )
				this.TicketNo = null;
			else
				this.TicketNo = rs.getString("TicketNo").trim();

			if( rs.getString("TeamNo") == null )
				this.TeamNo = null;
			else
				this.TeamNo = rs.getString("TeamNo").trim();

			if( rs.getString("SeatNo") == null )
				this.SeatNo = null;
			else
				this.SeatNo = rs.getString("SeatNo").trim();

			if( rs.getString("From") == null )
				this.From = null;
			else
				this.From = rs.getString("From").trim();

			if( rs.getString("To") == null )
				this.To = null;
			else
				this.To = rs.getString("To").trim();

			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

			if( rs.getString("CardStatus") == null )
				this.CardStatus = null;
			else
				this.CardStatus = rs.getString("CardStatus").trim();

			this.InsuYear = rs.getInt("InsuYear");
			if( rs.getString("InsuYearFlag") == null )
				this.InsuYearFlag = null;
			else
				this.InsuYearFlag = rs.getString("InsuYearFlag").trim();

			this.InActiveDate = rs.getDate("InActiveDate");
		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LICardActiveInfoList表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LICardActiveInfoListSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LICardActiveInfoListSchema getSchema()
	{
		LICardActiveInfoListSchema aLICardActiveInfoListSchema = new LICardActiveInfoListSchema();
		aLICardActiveInfoListSchema.setSchema(this);
		return aLICardActiveInfoListSchema;
	}

	public LICardActiveInfoListDB getDB()
	{
		LICardActiveInfoListDB aDBOper = new LICardActiveInfoListDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLICardActiveInfoList描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(BatchNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CardNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SequenceNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CardType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CValidate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ActiveDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ActiveIP)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DealFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( DealDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Name)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Sex)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Birthday ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IdType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IdNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OccupationType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OccupationCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PostalAddress)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ZipCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Phone)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Mobile)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CompanyPhone)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EMail)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Prem)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Amnt)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Mult)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Copys)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TicketNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TeamNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SeatNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(From)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(To)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CardStatus)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(InsuYear));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuYearFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( InActiveDate )));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLICardActiveInfoList>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			BatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			CardNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			SequenceNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			CardType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			CValidate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,SysConst.PACKAGESPILTER));
			ActiveDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,SysConst.PACKAGESPILTER));
			ActiveIP = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			DealFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			DealDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,SysConst.PACKAGESPILTER));
			Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			Sex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			Birthday = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
			IdType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			IdNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			OccupationType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			OccupationCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			PostalAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			ZipCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			Phone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			Mobile = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			GrpName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			CompanyPhone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			EMail = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			Prem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			Amnt = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			Mult = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
			Copys = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			TicketNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			TeamNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			SeatNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
			From = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
			To = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
			CardStatus = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
			InsuYear= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,41,SysConst.PACKAGESPILTER))).intValue();
			InsuYearFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
			InActiveDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43,SysConst.PACKAGESPILTER));
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LICardActiveInfoListSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("BatchNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BatchNo));
		}
		if (FCode.equals("CardNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CardNo));
		}
		if (FCode.equals("SequenceNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SequenceNo));
		}
		if (FCode.equals("CardType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CardType));
		}
		if (FCode.equals("CValidate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCValidate()));
		}
		if (FCode.equals("ActiveDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getActiveDate()));
		}
		if (FCode.equals("ActiveIP"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ActiveIP));
		}
		if (FCode.equals("DealFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DealFlag));
		}
		if (FCode.equals("DealDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getDealDate()));
		}
		if (FCode.equals("Name"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
		}
		if (FCode.equals("Sex"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Sex));
		}
		if (FCode.equals("Birthday"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getBirthday()));
		}
		if (FCode.equals("IdType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IdType));
		}
		if (FCode.equals("IdNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IdNo));
		}
		if (FCode.equals("OccupationType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationType));
		}
		if (FCode.equals("OccupationCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationCode));
		}
		if (FCode.equals("PostalAddress"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PostalAddress));
		}
		if (FCode.equals("ZipCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ZipCode));
		}
		if (FCode.equals("Phone"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Phone));
		}
		if (FCode.equals("Mobile"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Mobile));
		}
		if (FCode.equals("GrpName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpName));
		}
		if (FCode.equals("CompanyPhone"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CompanyPhone));
		}
		if (FCode.equals("EMail"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EMail));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("Prem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Prem));
		}
		if (FCode.equals("Amnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Amnt));
		}
		if (FCode.equals("Mult"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Mult));
		}
		if (FCode.equals("Copys"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Copys));
		}
		if (FCode.equals("TicketNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TicketNo));
		}
		if (FCode.equals("TeamNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TeamNo));
		}
		if (FCode.equals("SeatNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SeatNo));
		}
		if (FCode.equals("From"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(From));
		}
		if (FCode.equals("To"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(To));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (FCode.equals("CardStatus"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CardStatus));
		}
		if (FCode.equals("InsuYear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuYear));
		}
		if (FCode.equals("InsuYearFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuYearFlag));
		}
		if (FCode.equals("InActiveDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getInActiveDate()));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(BatchNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(CardNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(SequenceNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(CardType);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCValidate()));
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getActiveDate()));
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(ActiveIP);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(DealFlag);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getDealDate()));
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(Name);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(Sex);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getBirthday()));
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(IdType);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(IdNo);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(OccupationType);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(OccupationCode);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(PostalAddress);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(ZipCode);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(Phone);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(Mobile);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(GrpName);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(CompanyPhone);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(EMail);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(Prem);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(Amnt);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(Mult);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(Copys);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(TicketNo);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(TeamNo);
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(SeatNo);
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(From);
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(To);
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(CardStatus);
				break;
			case 40:
				strFieldValue = String.valueOf(InsuYear);
				break;
			case 41:
				strFieldValue = StrTool.GBKToUnicode(InsuYearFlag);
				break;
			case 42:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getInActiveDate()));
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("BatchNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BatchNo = FValue.trim();
			}
			else
				BatchNo = null;
		}
		if (FCode.equalsIgnoreCase("CardNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CardNo = FValue.trim();
			}
			else
				CardNo = null;
		}
		if (FCode.equalsIgnoreCase("SequenceNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SequenceNo = FValue.trim();
			}
			else
				SequenceNo = null;
		}
		if (FCode.equalsIgnoreCase("CardType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CardType = FValue.trim();
			}
			else
				CardType = null;
		}
		if (FCode.equalsIgnoreCase("CValidate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CValidate = fDate.getDate( FValue );
			}
			else
				CValidate = null;
		}
		if (FCode.equalsIgnoreCase("ActiveDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ActiveDate = fDate.getDate( FValue );
			}
			else
				ActiveDate = null;
		}
		if (FCode.equalsIgnoreCase("ActiveIP"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ActiveIP = FValue.trim();
			}
			else
				ActiveIP = null;
		}
		if (FCode.equalsIgnoreCase("DealFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DealFlag = FValue.trim();
			}
			else
				DealFlag = null;
		}
		if (FCode.equalsIgnoreCase("DealDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				DealDate = fDate.getDate( FValue );
			}
			else
				DealDate = null;
		}
		if (FCode.equalsIgnoreCase("Name"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Name = FValue.trim();
			}
			else
				Name = null;
		}
		if (FCode.equalsIgnoreCase("Sex"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Sex = FValue.trim();
			}
			else
				Sex = null;
		}
		if (FCode.equalsIgnoreCase("Birthday"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Birthday = fDate.getDate( FValue );
			}
			else
				Birthday = null;
		}
		if (FCode.equalsIgnoreCase("IdType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IdType = FValue.trim();
			}
			else
				IdType = null;
		}
		if (FCode.equalsIgnoreCase("IdNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IdNo = FValue.trim();
			}
			else
				IdNo = null;
		}
		if (FCode.equalsIgnoreCase("OccupationType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OccupationType = FValue.trim();
			}
			else
				OccupationType = null;
		}
		if (FCode.equalsIgnoreCase("OccupationCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OccupationCode = FValue.trim();
			}
			else
				OccupationCode = null;
		}
		if (FCode.equalsIgnoreCase("PostalAddress"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PostalAddress = FValue.trim();
			}
			else
				PostalAddress = null;
		}
		if (FCode.equalsIgnoreCase("ZipCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ZipCode = FValue.trim();
			}
			else
				ZipCode = null;
		}
		if (FCode.equalsIgnoreCase("Phone"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Phone = FValue.trim();
			}
			else
				Phone = null;
		}
		if (FCode.equalsIgnoreCase("Mobile"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Mobile = FValue.trim();
			}
			else
				Mobile = null;
		}
		if (FCode.equalsIgnoreCase("GrpName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpName = FValue.trim();
			}
			else
				GrpName = null;
		}
		if (FCode.equalsIgnoreCase("CompanyPhone"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CompanyPhone = FValue.trim();
			}
			else
				CompanyPhone = null;
		}
		if (FCode.equalsIgnoreCase("EMail"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EMail = FValue.trim();
			}
			else
				EMail = null;
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("Prem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Prem = FValue.trim();
			}
			else
				Prem = null;
		}
		if (FCode.equalsIgnoreCase("Amnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Amnt = FValue.trim();
			}
			else
				Amnt = null;
		}
		if (FCode.equalsIgnoreCase("Mult"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Mult = FValue.trim();
			}
			else
				Mult = null;
		}
		if (FCode.equalsIgnoreCase("Copys"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Copys = FValue.trim();
			}
			else
				Copys = null;
		}
		if (FCode.equalsIgnoreCase("TicketNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TicketNo = FValue.trim();
			}
			else
				TicketNo = null;
		}
		if (FCode.equalsIgnoreCase("TeamNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TeamNo = FValue.trim();
			}
			else
				TeamNo = null;
		}
		if (FCode.equalsIgnoreCase("SeatNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SeatNo = FValue.trim();
			}
			else
				SeatNo = null;
		}
		if (FCode.equalsIgnoreCase("From"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				From = FValue.trim();
			}
			else
				From = null;
		}
		if (FCode.equalsIgnoreCase("To"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				To = FValue.trim();
			}
			else
				To = null;
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		if (FCode.equalsIgnoreCase("CardStatus"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CardStatus = FValue.trim();
			}
			else
				CardStatus = null;
		}
		if (FCode.equalsIgnoreCase("InsuYear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				InsuYear = i;
			}
		}
		if (FCode.equalsIgnoreCase("InsuYearFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuYearFlag = FValue.trim();
			}
			else
				InsuYearFlag = null;
		}
		if (FCode.equalsIgnoreCase("InActiveDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				InActiveDate = fDate.getDate( FValue );
			}
			else
				InActiveDate = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LICardActiveInfoListSchema other = (LICardActiveInfoListSchema)otherObject;
		return
			(BatchNo == null ? other.getBatchNo() == null : BatchNo.equals(other.getBatchNo()))
			&& (CardNo == null ? other.getCardNo() == null : CardNo.equals(other.getCardNo()))
			&& (SequenceNo == null ? other.getSequenceNo() == null : SequenceNo.equals(other.getSequenceNo()))
			&& (CardType == null ? other.getCardType() == null : CardType.equals(other.getCardType()))
			&& (CValidate == null ? other.getCValidate() == null : fDate.getString(CValidate).equals(other.getCValidate()))
			&& (ActiveDate == null ? other.getActiveDate() == null : fDate.getString(ActiveDate).equals(other.getActiveDate()))
			&& (ActiveIP == null ? other.getActiveIP() == null : ActiveIP.equals(other.getActiveIP()))
			&& (DealFlag == null ? other.getDealFlag() == null : DealFlag.equals(other.getDealFlag()))
			&& (DealDate == null ? other.getDealDate() == null : fDate.getString(DealDate).equals(other.getDealDate()))
			&& (Name == null ? other.getName() == null : Name.equals(other.getName()))
			&& (Sex == null ? other.getSex() == null : Sex.equals(other.getSex()))
			&& (Birthday == null ? other.getBirthday() == null : fDate.getString(Birthday).equals(other.getBirthday()))
			&& (IdType == null ? other.getIdType() == null : IdType.equals(other.getIdType()))
			&& (IdNo == null ? other.getIdNo() == null : IdNo.equals(other.getIdNo()))
			&& (OccupationType == null ? other.getOccupationType() == null : OccupationType.equals(other.getOccupationType()))
			&& (OccupationCode == null ? other.getOccupationCode() == null : OccupationCode.equals(other.getOccupationCode()))
			&& (PostalAddress == null ? other.getPostalAddress() == null : PostalAddress.equals(other.getPostalAddress()))
			&& (ZipCode == null ? other.getZipCode() == null : ZipCode.equals(other.getZipCode()))
			&& (Phone == null ? other.getPhone() == null : Phone.equals(other.getPhone()))
			&& (Mobile == null ? other.getMobile() == null : Mobile.equals(other.getMobile()))
			&& (GrpName == null ? other.getGrpName() == null : GrpName.equals(other.getGrpName()))
			&& (CompanyPhone == null ? other.getCompanyPhone() == null : CompanyPhone.equals(other.getCompanyPhone()))
			&& (EMail == null ? other.getEMail() == null : EMail.equals(other.getEMail()))
			&& (State == null ? other.getState() == null : State.equals(other.getState()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (Prem == null ? other.getPrem() == null : Prem.equals(other.getPrem()))
			&& (Amnt == null ? other.getAmnt() == null : Amnt.equals(other.getAmnt()))
			&& (Mult == null ? other.getMult() == null : Mult.equals(other.getMult()))
			&& (Copys == null ? other.getCopys() == null : Copys.equals(other.getCopys()))
			&& (TicketNo == null ? other.getTicketNo() == null : TicketNo.equals(other.getTicketNo()))
			&& (TeamNo == null ? other.getTeamNo() == null : TeamNo.equals(other.getTeamNo()))
			&& (SeatNo == null ? other.getSeatNo() == null : SeatNo.equals(other.getSeatNo()))
			&& (From == null ? other.getFrom() == null : From.equals(other.getFrom()))
			&& (To == null ? other.getTo() == null : To.equals(other.getTo()))
			&& (Remark == null ? other.getRemark() == null : Remark.equals(other.getRemark()))
			&& (CardStatus == null ? other.getCardStatus() == null : CardStatus.equals(other.getCardStatus()))
			&& InsuYear == other.getInsuYear()
			&& (InsuYearFlag == null ? other.getInsuYearFlag() == null : InsuYearFlag.equals(other.getInsuYearFlag()))
			&& (InActiveDate == null ? other.getInActiveDate() == null : fDate.getString(InActiveDate).equals(other.getInActiveDate()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("BatchNo") ) {
			return 0;
		}
		if( strFieldName.equals("CardNo") ) {
			return 1;
		}
		if( strFieldName.equals("SequenceNo") ) {
			return 2;
		}
		if( strFieldName.equals("CardType") ) {
			return 3;
		}
		if( strFieldName.equals("CValidate") ) {
			return 4;
		}
		if( strFieldName.equals("ActiveDate") ) {
			return 5;
		}
		if( strFieldName.equals("ActiveIP") ) {
			return 6;
		}
		if( strFieldName.equals("DealFlag") ) {
			return 7;
		}
		if( strFieldName.equals("DealDate") ) {
			return 8;
		}
		if( strFieldName.equals("Name") ) {
			return 9;
		}
		if( strFieldName.equals("Sex") ) {
			return 10;
		}
		if( strFieldName.equals("Birthday") ) {
			return 11;
		}
		if( strFieldName.equals("IdType") ) {
			return 12;
		}
		if( strFieldName.equals("IdNo") ) {
			return 13;
		}
		if( strFieldName.equals("OccupationType") ) {
			return 14;
		}
		if( strFieldName.equals("OccupationCode") ) {
			return 15;
		}
		if( strFieldName.equals("PostalAddress") ) {
			return 16;
		}
		if( strFieldName.equals("ZipCode") ) {
			return 17;
		}
		if( strFieldName.equals("Phone") ) {
			return 18;
		}
		if( strFieldName.equals("Mobile") ) {
			return 19;
		}
		if( strFieldName.equals("GrpName") ) {
			return 20;
		}
		if( strFieldName.equals("CompanyPhone") ) {
			return 21;
		}
		if( strFieldName.equals("EMail") ) {
			return 22;
		}
		if( strFieldName.equals("State") ) {
			return 23;
		}
		if( strFieldName.equals("Operator") ) {
			return 24;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 25;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 26;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 27;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 28;
		}
		if( strFieldName.equals("Prem") ) {
			return 29;
		}
		if( strFieldName.equals("Amnt") ) {
			return 30;
		}
		if( strFieldName.equals("Mult") ) {
			return 31;
		}
		if( strFieldName.equals("Copys") ) {
			return 32;
		}
		if( strFieldName.equals("TicketNo") ) {
			return 33;
		}
		if( strFieldName.equals("TeamNo") ) {
			return 34;
		}
		if( strFieldName.equals("SeatNo") ) {
			return 35;
		}
		if( strFieldName.equals("From") ) {
			return 36;
		}
		if( strFieldName.equals("To") ) {
			return 37;
		}
		if( strFieldName.equals("Remark") ) {
			return 38;
		}
		if( strFieldName.equals("CardStatus") ) {
			return 39;
		}
		if( strFieldName.equals("InsuYear") ) {
			return 40;
		}
		if( strFieldName.equals("InsuYearFlag") ) {
			return 41;
		}
		if( strFieldName.equals("InActiveDate") ) {
			return 42;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "BatchNo";
				break;
			case 1:
				strFieldName = "CardNo";
				break;
			case 2:
				strFieldName = "SequenceNo";
				break;
			case 3:
				strFieldName = "CardType";
				break;
			case 4:
				strFieldName = "CValidate";
				break;
			case 5:
				strFieldName = "ActiveDate";
				break;
			case 6:
				strFieldName = "ActiveIP";
				break;
			case 7:
				strFieldName = "DealFlag";
				break;
			case 8:
				strFieldName = "DealDate";
				break;
			case 9:
				strFieldName = "Name";
				break;
			case 10:
				strFieldName = "Sex";
				break;
			case 11:
				strFieldName = "Birthday";
				break;
			case 12:
				strFieldName = "IdType";
				break;
			case 13:
				strFieldName = "IdNo";
				break;
			case 14:
				strFieldName = "OccupationType";
				break;
			case 15:
				strFieldName = "OccupationCode";
				break;
			case 16:
				strFieldName = "PostalAddress";
				break;
			case 17:
				strFieldName = "ZipCode";
				break;
			case 18:
				strFieldName = "Phone";
				break;
			case 19:
				strFieldName = "Mobile";
				break;
			case 20:
				strFieldName = "GrpName";
				break;
			case 21:
				strFieldName = "CompanyPhone";
				break;
			case 22:
				strFieldName = "EMail";
				break;
			case 23:
				strFieldName = "State";
				break;
			case 24:
				strFieldName = "Operator";
				break;
			case 25:
				strFieldName = "MakeDate";
				break;
			case 26:
				strFieldName = "MakeTime";
				break;
			case 27:
				strFieldName = "ModifyDate";
				break;
			case 28:
				strFieldName = "ModifyTime";
				break;
			case 29:
				strFieldName = "Prem";
				break;
			case 30:
				strFieldName = "Amnt";
				break;
			case 31:
				strFieldName = "Mult";
				break;
			case 32:
				strFieldName = "Copys";
				break;
			case 33:
				strFieldName = "TicketNo";
				break;
			case 34:
				strFieldName = "TeamNo";
				break;
			case 35:
				strFieldName = "SeatNo";
				break;
			case 36:
				strFieldName = "From";
				break;
			case 37:
				strFieldName = "To";
				break;
			case 38:
				strFieldName = "Remark";
				break;
			case 39:
				strFieldName = "CardStatus";
				break;
			case 40:
				strFieldName = "InsuYear";
				break;
			case 41:
				strFieldName = "InsuYearFlag";
				break;
			case 42:
				strFieldName = "InActiveDate";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("BatchNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CardNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SequenceNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CardType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CValidate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ActiveDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ActiveIP") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DealFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DealDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Name") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Sex") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Birthday") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("IdType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IdNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OccupationType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OccupationCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PostalAddress") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ZipCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Phone") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Mobile") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CompanyPhone") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EMail") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Prem") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Amnt") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Mult") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Copys") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TicketNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TeamNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SeatNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("From") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("To") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CardStatus") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuYear") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("InsuYearFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InActiveDate") ) {
			return Schema.TYPE_DATE;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 5:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 36:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 37:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 38:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 39:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 40:
				nFieldType = Schema.TYPE_INT;
				break;
			case 41:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 42:
				nFieldType = Schema.TYPE_DATE;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
