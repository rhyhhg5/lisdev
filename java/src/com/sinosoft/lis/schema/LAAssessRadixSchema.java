/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LAAssessRadixDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LAAssessRadixSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-04-01
 */
public class LAAssessRadixSchema implements Schema
{
    // @Field
    /** 流水号 */
    private String RadixNo;
    /** 代理人职级 */
    private String AgentGrade;
    /** 考核类型 */
    private String AssessType;
    /** 地区类型 */
    private String AreaType;
    /** 展业类型 */
    private String BranchType;
    /** 是否降级属性 */
    private String Flag1;
    /** 是否同业衔接人员 */
    private String Flag2;
    /** 是否第一次考核 */
    private String FirstAssessFlag;
    /** 考核期限 */
    private int AssessPeriod;
    /** 比较期限 */
    private int ComPeriod;
    /** 下限 */
    private int MinLimit;
    /** 上限 */
    private int MaxLimit;
    /** 备注 */
    private String Noti;
    /** 渠道 */
    private String BranchType2;

    public static final int FIELDNUM = 14; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LAAssessRadixSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[6];
        pk[0] = "RadixNo";
        pk[1] = "AgentGrade";
        pk[2] = "AssessType";
        pk[3] = "AreaType";
        pk[4] = "BranchType";
        pk[5] = "BranchType2";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getRadixNo()
    {
        if (SysConst.CHANGECHARSET && RadixNo != null && !RadixNo.equals(""))
        {
            RadixNo = StrTool.unicodeToGBK(RadixNo);
        }
        return RadixNo;
    }

    public void setRadixNo(String aRadixNo)
    {
        RadixNo = aRadixNo;
    }

    public String getAgentGrade()
    {
        if (SysConst.CHANGECHARSET && AgentGrade != null &&
            !AgentGrade.equals(""))
        {
            AgentGrade = StrTool.unicodeToGBK(AgentGrade);
        }
        return AgentGrade;
    }

    public void setAgentGrade(String aAgentGrade)
    {
        AgentGrade = aAgentGrade;
    }

    public String getAssessType()
    {
        if (SysConst.CHANGECHARSET && AssessType != null &&
            !AssessType.equals(""))
        {
            AssessType = StrTool.unicodeToGBK(AssessType);
        }
        return AssessType;
    }

    public void setAssessType(String aAssessType)
    {
        AssessType = aAssessType;
    }

    public String getAreaType()
    {
        if (SysConst.CHANGECHARSET && AreaType != null && !AreaType.equals(""))
        {
            AreaType = StrTool.unicodeToGBK(AreaType);
        }
        return AreaType;
    }

    public void setAreaType(String aAreaType)
    {
        AreaType = aAreaType;
    }

    public String getBranchType()
    {
        if (SysConst.CHANGECHARSET && BranchType != null &&
            !BranchType.equals(""))
        {
            BranchType = StrTool.unicodeToGBK(BranchType);
        }
        return BranchType;
    }

    public void setBranchType(String aBranchType)
    {
        BranchType = aBranchType;
    }

    public String getFlag1()
    {
        if (SysConst.CHANGECHARSET && Flag1 != null && !Flag1.equals(""))
        {
            Flag1 = StrTool.unicodeToGBK(Flag1);
        }
        return Flag1;
    }

    public void setFlag1(String aFlag1)
    {
        Flag1 = aFlag1;
    }

    public String getFlag2()
    {
        if (SysConst.CHANGECHARSET && Flag2 != null && !Flag2.equals(""))
        {
            Flag2 = StrTool.unicodeToGBK(Flag2);
        }
        return Flag2;
    }

    public void setFlag2(String aFlag2)
    {
        Flag2 = aFlag2;
    }

    public String getFirstAssessFlag()
    {
        if (SysConst.CHANGECHARSET && FirstAssessFlag != null &&
            !FirstAssessFlag.equals(""))
        {
            FirstAssessFlag = StrTool.unicodeToGBK(FirstAssessFlag);
        }
        return FirstAssessFlag;
    }

    public void setFirstAssessFlag(String aFirstAssessFlag)
    {
        FirstAssessFlag = aFirstAssessFlag;
    }

    public int getAssessPeriod()
    {
        return AssessPeriod;
    }

    public void setAssessPeriod(int aAssessPeriod)
    {
        AssessPeriod = aAssessPeriod;
    }

    public void setAssessPeriod(String aAssessPeriod)
    {
        if (aAssessPeriod != null && !aAssessPeriod.equals(""))
        {
            Integer tInteger = new Integer(aAssessPeriod);
            int i = tInteger.intValue();
            AssessPeriod = i;
        }
    }

    public int getComPeriod()
    {
        return ComPeriod;
    }

    public void setComPeriod(int aComPeriod)
    {
        ComPeriod = aComPeriod;
    }

    public void setComPeriod(String aComPeriod)
    {
        if (aComPeriod != null && !aComPeriod.equals(""))
        {
            Integer tInteger = new Integer(aComPeriod);
            int i = tInteger.intValue();
            ComPeriod = i;
        }
    }

    public int getMinLimit()
    {
        return MinLimit;
    }

    public void setMinLimit(int aMinLimit)
    {
        MinLimit = aMinLimit;
    }

    public void setMinLimit(String aMinLimit)
    {
        if (aMinLimit != null && !aMinLimit.equals(""))
        {
            Integer tInteger = new Integer(aMinLimit);
            int i = tInteger.intValue();
            MinLimit = i;
        }
    }

    public int getMaxLimit()
    {
        return MaxLimit;
    }

    public void setMaxLimit(int aMaxLimit)
    {
        MaxLimit = aMaxLimit;
    }

    public void setMaxLimit(String aMaxLimit)
    {
        if (aMaxLimit != null && !aMaxLimit.equals(""))
        {
            Integer tInteger = new Integer(aMaxLimit);
            int i = tInteger.intValue();
            MaxLimit = i;
        }
    }

    public String getNoti()
    {
        if (SysConst.CHANGECHARSET && Noti != null && !Noti.equals(""))
        {
            Noti = StrTool.unicodeToGBK(Noti);
        }
        return Noti;
    }

    public void setNoti(String aNoti)
    {
        Noti = aNoti;
    }

    public String getBranchType2()
    {
        if (SysConst.CHANGECHARSET && BranchType2 != null &&
            !BranchType2.equals(""))
        {
            BranchType2 = StrTool.unicodeToGBK(BranchType2);
        }
        return BranchType2;
    }

    public void setBranchType2(String aBranchType2)
    {
        BranchType2 = aBranchType2;
    }

    /**
     * 使用另外一个 LAAssessRadixSchema 对象给 Schema 赋值
     * @param: aLAAssessRadixSchema LAAssessRadixSchema
     **/
    public void setSchema(LAAssessRadixSchema aLAAssessRadixSchema)
    {
        this.RadixNo = aLAAssessRadixSchema.getRadixNo();
        this.AgentGrade = aLAAssessRadixSchema.getAgentGrade();
        this.AssessType = aLAAssessRadixSchema.getAssessType();
        this.AreaType = aLAAssessRadixSchema.getAreaType();
        this.BranchType = aLAAssessRadixSchema.getBranchType();
        this.Flag1 = aLAAssessRadixSchema.getFlag1();
        this.Flag2 = aLAAssessRadixSchema.getFlag2();
        this.FirstAssessFlag = aLAAssessRadixSchema.getFirstAssessFlag();
        this.AssessPeriod = aLAAssessRadixSchema.getAssessPeriod();
        this.ComPeriod = aLAAssessRadixSchema.getComPeriod();
        this.MinLimit = aLAAssessRadixSchema.getMinLimit();
        this.MaxLimit = aLAAssessRadixSchema.getMaxLimit();
        this.Noti = aLAAssessRadixSchema.getNoti();
        this.BranchType2 = aLAAssessRadixSchema.getBranchType2();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("RadixNo") == null)
            {
                this.RadixNo = null;
            }
            else
            {
                this.RadixNo = rs.getString("RadixNo").trim();
            }

            if (rs.getString("AgentGrade") == null)
            {
                this.AgentGrade = null;
            }
            else
            {
                this.AgentGrade = rs.getString("AgentGrade").trim();
            }

            if (rs.getString("AssessType") == null)
            {
                this.AssessType = null;
            }
            else
            {
                this.AssessType = rs.getString("AssessType").trim();
            }

            if (rs.getString("AreaType") == null)
            {
                this.AreaType = null;
            }
            else
            {
                this.AreaType = rs.getString("AreaType").trim();
            }

            if (rs.getString("BranchType") == null)
            {
                this.BranchType = null;
            }
            else
            {
                this.BranchType = rs.getString("BranchType").trim();
            }

            if (rs.getString("Flag1") == null)
            {
                this.Flag1 = null;
            }
            else
            {
                this.Flag1 = rs.getString("Flag1").trim();
            }

            if (rs.getString("Flag2") == null)
            {
                this.Flag2 = null;
            }
            else
            {
                this.Flag2 = rs.getString("Flag2").trim();
            }

            if (rs.getString("FirstAssessFlag") == null)
            {
                this.FirstAssessFlag = null;
            }
            else
            {
                this.FirstAssessFlag = rs.getString("FirstAssessFlag").trim();
            }

            this.AssessPeriod = rs.getInt("AssessPeriod");
            this.ComPeriod = rs.getInt("ComPeriod");
            this.MinLimit = rs.getInt("MinLimit");
            this.MaxLimit = rs.getInt("MaxLimit");
            if (rs.getString("Noti") == null)
            {
                this.Noti = null;
            }
            else
            {
                this.Noti = rs.getString("Noti").trim();
            }

            if (rs.getString("BranchType2") == null)
            {
                this.BranchType2 = null;
            }
            else
            {
                this.BranchType2 = rs.getString("BranchType2").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAssessRadixSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LAAssessRadixSchema getSchema()
    {
        LAAssessRadixSchema aLAAssessRadixSchema = new LAAssessRadixSchema();
        aLAAssessRadixSchema.setSchema(this);
        return aLAAssessRadixSchema;
    }

    public LAAssessRadixDB getDB()
    {
        LAAssessRadixDB aDBOper = new LAAssessRadixDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAAssessRadix描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(RadixNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AgentGrade)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AssessType)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AreaType)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BranchType)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Flag1)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Flag2)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(FirstAssessFlag)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(AssessPeriod));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(ComPeriod));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(MinLimit));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(MaxLimit));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Noti)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BranchType2)));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAAssessRadix>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            RadixNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                     SysConst.PACKAGESPILTER);
            AgentGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
            AssessType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                        SysConst.PACKAGESPILTER);
            AreaType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                      SysConst.PACKAGESPILTER);
            BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                        SysConst.PACKAGESPILTER);
            Flag1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                   SysConst.PACKAGESPILTER);
            Flag2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                   SysConst.PACKAGESPILTER);
            FirstAssessFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             8, SysConst.PACKAGESPILTER);
            AssessPeriod = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 9, SysConst.PACKAGESPILTER))).intValue();
            ComPeriod = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 10, SysConst.PACKAGESPILTER))).intValue();
            MinLimit = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 11, SysConst.PACKAGESPILTER))).intValue();
            MaxLimit = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 12, SysConst.PACKAGESPILTER))).intValue();
            Noti = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                  SysConst.PACKAGESPILTER);
            BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                         SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAssessRadixSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("RadixNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RadixNo));
        }
        if (FCode.equals("AgentGrade"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGrade));
        }
        if (FCode.equals("AssessType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AssessType));
        }
        if (FCode.equals("AreaType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AreaType));
        }
        if (FCode.equals("BranchType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
        }
        if (FCode.equals("Flag1"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Flag1));
        }
        if (FCode.equals("Flag2"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Flag2));
        }
        if (FCode.equals("FirstAssessFlag"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FirstAssessFlag));
        }
        if (FCode.equals("AssessPeriod"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AssessPeriod));
        }
        if (FCode.equals("ComPeriod"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ComPeriod));
        }
        if (FCode.equals("MinLimit"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MinLimit));
        }
        if (FCode.equals("MaxLimit"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MaxLimit));
        }
        if (FCode.equals("Noti"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Noti));
        }
        if (FCode.equals("BranchType2"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(RadixNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(AgentGrade);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(AssessType);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(AreaType);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(BranchType);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(Flag1);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(Flag2);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(FirstAssessFlag);
                break;
            case 8:
                strFieldValue = String.valueOf(AssessPeriod);
                break;
            case 9:
                strFieldValue = String.valueOf(ComPeriod);
                break;
            case 10:
                strFieldValue = String.valueOf(MinLimit);
                break;
            case 11:
                strFieldValue = String.valueOf(MaxLimit);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(Noti);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(BranchType2);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("RadixNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RadixNo = FValue.trim();
            }
            else
            {
                RadixNo = null;
            }
        }
        if (FCode.equals("AgentGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentGrade = FValue.trim();
            }
            else
            {
                AgentGrade = null;
            }
        }
        if (FCode.equals("AssessType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AssessType = FValue.trim();
            }
            else
            {
                AssessType = null;
            }
        }
        if (FCode.equals("AreaType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AreaType = FValue.trim();
            }
            else
            {
                AreaType = null;
            }
        }
        if (FCode.equals("BranchType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchType = FValue.trim();
            }
            else
            {
                BranchType = null;
            }
        }
        if (FCode.equals("Flag1"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Flag1 = FValue.trim();
            }
            else
            {
                Flag1 = null;
            }
        }
        if (FCode.equals("Flag2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Flag2 = FValue.trim();
            }
            else
            {
                Flag2 = null;
            }
        }
        if (FCode.equals("FirstAssessFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                FirstAssessFlag = FValue.trim();
            }
            else
            {
                FirstAssessFlag = null;
            }
        }
        if (FCode.equals("AssessPeriod"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                AssessPeriod = i;
            }
        }
        if (FCode.equals("ComPeriod"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                ComPeriod = i;
            }
        }
        if (FCode.equals("MinLimit"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                MinLimit = i;
            }
        }
        if (FCode.equals("MaxLimit"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                MaxLimit = i;
            }
        }
        if (FCode.equals("Noti"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Noti = FValue.trim();
            }
            else
            {
                Noti = null;
            }
        }
        if (FCode.equals("BranchType2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchType2 = FValue.trim();
            }
            else
            {
                BranchType2 = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LAAssessRadixSchema other = (LAAssessRadixSchema) otherObject;
        return
                RadixNo.equals(other.getRadixNo())
                && AgentGrade.equals(other.getAgentGrade())
                && AssessType.equals(other.getAssessType())
                && AreaType.equals(other.getAreaType())
                && BranchType.equals(other.getBranchType())
                && Flag1.equals(other.getFlag1())
                && Flag2.equals(other.getFlag2())
                && FirstAssessFlag.equals(other.getFirstAssessFlag())
                && AssessPeriod == other.getAssessPeriod()
                && ComPeriod == other.getComPeriod()
                && MinLimit == other.getMinLimit()
                && MaxLimit == other.getMaxLimit()
                && Noti.equals(other.getNoti())
                && BranchType2.equals(other.getBranchType2());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("RadixNo"))
        {
            return 0;
        }
        if (strFieldName.equals("AgentGrade"))
        {
            return 1;
        }
        if (strFieldName.equals("AssessType"))
        {
            return 2;
        }
        if (strFieldName.equals("AreaType"))
        {
            return 3;
        }
        if (strFieldName.equals("BranchType"))
        {
            return 4;
        }
        if (strFieldName.equals("Flag1"))
        {
            return 5;
        }
        if (strFieldName.equals("Flag2"))
        {
            return 6;
        }
        if (strFieldName.equals("FirstAssessFlag"))
        {
            return 7;
        }
        if (strFieldName.equals("AssessPeriod"))
        {
            return 8;
        }
        if (strFieldName.equals("ComPeriod"))
        {
            return 9;
        }
        if (strFieldName.equals("MinLimit"))
        {
            return 10;
        }
        if (strFieldName.equals("MaxLimit"))
        {
            return 11;
        }
        if (strFieldName.equals("Noti"))
        {
            return 12;
        }
        if (strFieldName.equals("BranchType2"))
        {
            return 13;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "RadixNo";
                break;
            case 1:
                strFieldName = "AgentGrade";
                break;
            case 2:
                strFieldName = "AssessType";
                break;
            case 3:
                strFieldName = "AreaType";
                break;
            case 4:
                strFieldName = "BranchType";
                break;
            case 5:
                strFieldName = "Flag1";
                break;
            case 6:
                strFieldName = "Flag2";
                break;
            case 7:
                strFieldName = "FirstAssessFlag";
                break;
            case 8:
                strFieldName = "AssessPeriod";
                break;
            case 9:
                strFieldName = "ComPeriod";
                break;
            case 10:
                strFieldName = "MinLimit";
                break;
            case 11:
                strFieldName = "MaxLimit";
                break;
            case 12:
                strFieldName = "Noti";
                break;
            case 13:
                strFieldName = "BranchType2";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("RadixNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentGrade"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AssessType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AreaType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Flag1"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Flag2"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FirstAssessFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AssessPeriod"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("ComPeriod"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("MinLimit"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("MaxLimit"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("Noti"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchType2"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_INT;
                break;
            case 9:
                nFieldType = Schema.TYPE_INT;
                break;
            case 10:
                nFieldType = Schema.TYPE_INT;
                break;
            case 11:
                nFieldType = Schema.TYPE_INT;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
