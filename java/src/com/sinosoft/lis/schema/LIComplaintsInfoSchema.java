/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LIComplaintsInfoDB;

/*
 * <p>ClassName: LIComplaintsInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 回访（中科软）
 * @CreateDate：2009-12-22
 */
public class LIComplaintsInfoSchema implements Schema, Cloneable
{
	// @Field
	/** 咨诉id */
	private String ComplaintID;
	/** 来电号码 */
	private String TelephoneNo;
	/** 咨诉类型 */
	private String ComplaintType;
	/** 客户类型 */
	private String CustomerNo;
	/** 是否后续处理 */
	private String NeedDealFlag;
	/** 对应其它号码 */
	private String OtherNo;
	/** 其它号码类型 */
	private String OtherNoType;
	/** 客户姓名 */
	private String CustomerName;
	/** 来电机构 */
	private String TelephoneCom;
	/** 问题件类型 */
	private String IssueType;
	/** 问题件状态 */
	private String IssueState;
	/** 承办日期 */
	private Date UndertakeDate;
	/** 承办时间 */
	private String UndertakeTime;
	/** 转交机构 */
	private String TransmitCom;
	/** 转交日期 */
	private Date TransmitDate;
	/** 转交时间 */
	private String TransmitTime;
	/** 处理人 */
	private String DealOperator;
	/** 预约日期 */
	private Date BookingDate;
	/** 预约时间 */
	private String BookingTime;
	/** 客户联系方式 */
	private String CustomerAddress;
	/** 咨诉内容 */
	private String ComplaintContent;
	/** 处理信息 */
	private String DealContent;
	/** 受理人 */
	private String AcceptOperator;
	/** 受理机构 */
	private String AcceptManageCom;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 特急案件标识 */
	private String UrgencySign;

	public static final int FIELDNUM = 30;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LIComplaintsInfoSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "ComplaintID";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LIComplaintsInfoSchema cloned = (LIComplaintsInfoSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getComplaintID()
	{
		return ComplaintID;
	}
	public void setComplaintID(String aComplaintID)
	{
		ComplaintID = aComplaintID;
	}
	public String getTelephoneNo()
	{
		return TelephoneNo;
	}
	public void setTelephoneNo(String aTelephoneNo)
	{
		TelephoneNo = aTelephoneNo;
	}
	public String getComplaintType()
	{
		return ComplaintType;
	}
	public void setComplaintType(String aComplaintType)
	{
		ComplaintType = aComplaintType;
	}
	public String getCustomerNo()
	{
		return CustomerNo;
	}
	public void setCustomerNo(String aCustomerNo)
	{
		CustomerNo = aCustomerNo;
	}
	public String getNeedDealFlag()
	{
		return NeedDealFlag;
	}
	public void setNeedDealFlag(String aNeedDealFlag)
	{
		NeedDealFlag = aNeedDealFlag;
	}
	public String getOtherNo()
	{
		return OtherNo;
	}
	public void setOtherNo(String aOtherNo)
	{
		OtherNo = aOtherNo;
	}
	public String getOtherNoType()
	{
		return OtherNoType;
	}
	public void setOtherNoType(String aOtherNoType)
	{
		OtherNoType = aOtherNoType;
	}
	public String getCustomerName()
	{
		return CustomerName;
	}
	public void setCustomerName(String aCustomerName)
	{
		CustomerName = aCustomerName;
	}
	public String getTelephoneCom()
	{
		return TelephoneCom;
	}
	public void setTelephoneCom(String aTelephoneCom)
	{
		TelephoneCom = aTelephoneCom;
	}
	public String getIssueType()
	{
		return IssueType;
	}
	public void setIssueType(String aIssueType)
	{
		IssueType = aIssueType;
	}
	public String getIssueState()
	{
		return IssueState;
	}
	public void setIssueState(String aIssueState)
	{
		IssueState = aIssueState;
	}
	public String getUndertakeDate()
	{
		if( UndertakeDate != null )
			return fDate.getString(UndertakeDate);
		else
			return null;
	}
	public void setUndertakeDate(Date aUndertakeDate)
	{
		UndertakeDate = aUndertakeDate;
	}
	public void setUndertakeDate(String aUndertakeDate)
	{
		if (aUndertakeDate != null && !aUndertakeDate.equals("") )
		{
			UndertakeDate = fDate.getDate( aUndertakeDate );
		}
		else
			UndertakeDate = null;
	}

	public String getUndertakeTime()
	{
		return UndertakeTime;
	}
	public void setUndertakeTime(String aUndertakeTime)
	{
		UndertakeTime = aUndertakeTime;
	}
	public String getTransmitCom()
	{
		return TransmitCom;
	}
	public void setTransmitCom(String aTransmitCom)
	{
		TransmitCom = aTransmitCom;
	}
	public String getTransmitDate()
	{
		if( TransmitDate != null )
			return fDate.getString(TransmitDate);
		else
			return null;
	}
	public void setTransmitDate(Date aTransmitDate)
	{
		TransmitDate = aTransmitDate;
	}
	public void setTransmitDate(String aTransmitDate)
	{
		if (aTransmitDate != null && !aTransmitDate.equals("") )
		{
			TransmitDate = fDate.getDate( aTransmitDate );
		}
		else
			TransmitDate = null;
	}

	public String getTransmitTime()
	{
		return TransmitTime;
	}
	public void setTransmitTime(String aTransmitTime)
	{
		TransmitTime = aTransmitTime;
	}
	public String getDealOperator()
	{
		return DealOperator;
	}
	public void setDealOperator(String aDealOperator)
	{
		DealOperator = aDealOperator;
	}
	public String getBookingDate()
	{
		if( BookingDate != null )
			return fDate.getString(BookingDate);
		else
			return null;
	}
	public void setBookingDate(Date aBookingDate)
	{
		BookingDate = aBookingDate;
	}
	public void setBookingDate(String aBookingDate)
	{
		if (aBookingDate != null && !aBookingDate.equals("") )
		{
			BookingDate = fDate.getDate( aBookingDate );
		}
		else
			BookingDate = null;
	}

	public String getBookingTime()
	{
		return BookingTime;
	}
	public void setBookingTime(String aBookingTime)
	{
		BookingTime = aBookingTime;
	}
	public String getCustomerAddress()
	{
		return CustomerAddress;
	}
	public void setCustomerAddress(String aCustomerAddress)
	{
		CustomerAddress = aCustomerAddress;
	}
	public String getComplaintContent()
	{
		return ComplaintContent;
	}
	public void setComplaintContent(String aComplaintContent)
	{
		ComplaintContent = aComplaintContent;
	}
	public String getDealContent()
	{
		return DealContent;
	}
	public void setDealContent(String aDealContent)
	{
		DealContent = aDealContent;
	}
	public String getAcceptOperator()
	{
		return AcceptOperator;
	}
	public void setAcceptOperator(String aAcceptOperator)
	{
		AcceptOperator = aAcceptOperator;
	}
	public String getAcceptManageCom()
	{
		return AcceptManageCom;
	}
	public void setAcceptManageCom(String aAcceptManageCom)
	{
		AcceptManageCom = aAcceptManageCom;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getUrgencySign()
	{
		return UrgencySign;
	}
	public void setUrgencySign(String aUrgencySign)
	{
		UrgencySign = aUrgencySign;
	}

	/**
	* 使用另外一个 LIComplaintsInfoSchema 对象给 Schema 赋值
	* @param: aLIComplaintsInfoSchema LIComplaintsInfoSchema
	**/
	public void setSchema(LIComplaintsInfoSchema aLIComplaintsInfoSchema)
	{
		this.ComplaintID = aLIComplaintsInfoSchema.getComplaintID();
		this.TelephoneNo = aLIComplaintsInfoSchema.getTelephoneNo();
		this.ComplaintType = aLIComplaintsInfoSchema.getComplaintType();
		this.CustomerNo = aLIComplaintsInfoSchema.getCustomerNo();
		this.NeedDealFlag = aLIComplaintsInfoSchema.getNeedDealFlag();
		this.OtherNo = aLIComplaintsInfoSchema.getOtherNo();
		this.OtherNoType = aLIComplaintsInfoSchema.getOtherNoType();
		this.CustomerName = aLIComplaintsInfoSchema.getCustomerName();
		this.TelephoneCom = aLIComplaintsInfoSchema.getTelephoneCom();
		this.IssueType = aLIComplaintsInfoSchema.getIssueType();
		this.IssueState = aLIComplaintsInfoSchema.getIssueState();
		this.UndertakeDate = fDate.getDate( aLIComplaintsInfoSchema.getUndertakeDate());
		this.UndertakeTime = aLIComplaintsInfoSchema.getUndertakeTime();
		this.TransmitCom = aLIComplaintsInfoSchema.getTransmitCom();
		this.TransmitDate = fDate.getDate( aLIComplaintsInfoSchema.getTransmitDate());
		this.TransmitTime = aLIComplaintsInfoSchema.getTransmitTime();
		this.DealOperator = aLIComplaintsInfoSchema.getDealOperator();
		this.BookingDate = fDate.getDate( aLIComplaintsInfoSchema.getBookingDate());
		this.BookingTime = aLIComplaintsInfoSchema.getBookingTime();
		this.CustomerAddress = aLIComplaintsInfoSchema.getCustomerAddress();
		this.ComplaintContent = aLIComplaintsInfoSchema.getComplaintContent();
		this.DealContent = aLIComplaintsInfoSchema.getDealContent();
		this.AcceptOperator = aLIComplaintsInfoSchema.getAcceptOperator();
		this.AcceptManageCom = aLIComplaintsInfoSchema.getAcceptManageCom();
		this.Operator = aLIComplaintsInfoSchema.getOperator();
		this.MakeDate = fDate.getDate( aLIComplaintsInfoSchema.getMakeDate());
		this.MakeTime = aLIComplaintsInfoSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLIComplaintsInfoSchema.getModifyDate());
		this.ModifyTime = aLIComplaintsInfoSchema.getModifyTime();
		this.UrgencySign = aLIComplaintsInfoSchema.getUrgencySign();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("ComplaintID") == null )
				this.ComplaintID = null;
			else
				this.ComplaintID = rs.getString("ComplaintID").trim();

			if( rs.getString("TelephoneNo") == null )
				this.TelephoneNo = null;
			else
				this.TelephoneNo = rs.getString("TelephoneNo").trim();

			if( rs.getString("ComplaintType") == null )
				this.ComplaintType = null;
			else
				this.ComplaintType = rs.getString("ComplaintType").trim();

			if( rs.getString("CustomerNo") == null )
				this.CustomerNo = null;
			else
				this.CustomerNo = rs.getString("CustomerNo").trim();

			if( rs.getString("NeedDealFlag") == null )
				this.NeedDealFlag = null;
			else
				this.NeedDealFlag = rs.getString("NeedDealFlag").trim();

			if( rs.getString("OtherNo") == null )
				this.OtherNo = null;
			else
				this.OtherNo = rs.getString("OtherNo").trim();

			if( rs.getString("OtherNoType") == null )
				this.OtherNoType = null;
			else
				this.OtherNoType = rs.getString("OtherNoType").trim();

			if( rs.getString("CustomerName") == null )
				this.CustomerName = null;
			else
				this.CustomerName = rs.getString("CustomerName").trim();

			if( rs.getString("TelephoneCom") == null )
				this.TelephoneCom = null;
			else
				this.TelephoneCom = rs.getString("TelephoneCom").trim();

			if( rs.getString("IssueType") == null )
				this.IssueType = null;
			else
				this.IssueType = rs.getString("IssueType").trim();

			if( rs.getString("IssueState") == null )
				this.IssueState = null;
			else
				this.IssueState = rs.getString("IssueState").trim();

			this.UndertakeDate = rs.getDate("UndertakeDate");
			if( rs.getString("UndertakeTime") == null )
				this.UndertakeTime = null;
			else
				this.UndertakeTime = rs.getString("UndertakeTime").trim();

			if( rs.getString("TransmitCom") == null )
				this.TransmitCom = null;
			else
				this.TransmitCom = rs.getString("TransmitCom").trim();

			this.TransmitDate = rs.getDate("TransmitDate");
			if( rs.getString("TransmitTime") == null )
				this.TransmitTime = null;
			else
				this.TransmitTime = rs.getString("TransmitTime").trim();

			if( rs.getString("DealOperator") == null )
				this.DealOperator = null;
			else
				this.DealOperator = rs.getString("DealOperator").trim();

			this.BookingDate = rs.getDate("BookingDate");
			if( rs.getString("BookingTime") == null )
				this.BookingTime = null;
			else
				this.BookingTime = rs.getString("BookingTime").trim();

			if( rs.getString("CustomerAddress") == null )
				this.CustomerAddress = null;
			else
				this.CustomerAddress = rs.getString("CustomerAddress").trim();

			if( rs.getString("ComplaintContent") == null )
				this.ComplaintContent = null;
			else
				this.ComplaintContent = rs.getString("ComplaintContent").trim();

			if( rs.getString("DealContent") == null )
				this.DealContent = null;
			else
				this.DealContent = rs.getString("DealContent").trim();

			if( rs.getString("AcceptOperator") == null )
				this.AcceptOperator = null;
			else
				this.AcceptOperator = rs.getString("AcceptOperator").trim();

			if( rs.getString("AcceptManageCom") == null )
				this.AcceptManageCom = null;
			else
				this.AcceptManageCom = rs.getString("AcceptManageCom").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("UrgencySign") == null )
				this.UrgencySign = null;
			else
				this.UrgencySign = rs.getString("UrgencySign").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LIComplaintsInfo表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LIComplaintsInfoSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LIComplaintsInfoSchema getSchema()
	{
		LIComplaintsInfoSchema aLIComplaintsInfoSchema = new LIComplaintsInfoSchema();
		aLIComplaintsInfoSchema.setSchema(this);
		return aLIComplaintsInfoSchema;
	}

	public LIComplaintsInfoDB getDB()
	{
		LIComplaintsInfoDB aDBOper = new LIComplaintsInfoDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLIComplaintsInfo描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(ComplaintID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TelephoneNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ComplaintType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(NeedDealFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OtherNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OtherNoType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TelephoneCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IssueType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IssueState)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( UndertakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UndertakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TransmitCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( TransmitDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TransmitTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DealOperator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( BookingDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BookingTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerAddress)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ComplaintContent)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DealContent)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AcceptOperator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AcceptManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UrgencySign));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLIComplaintsInfo>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			ComplaintID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			TelephoneNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			ComplaintType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			NeedDealFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			OtherNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			OtherNoType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			CustomerName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			TelephoneCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			IssueType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			IssueState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			UndertakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
			UndertakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			TransmitCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			TransmitDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,SysConst.PACKAGESPILTER));
			TransmitTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			DealOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			BookingDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,SysConst.PACKAGESPILTER));
			BookingTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			CustomerAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			ComplaintContent = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			DealContent = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			AcceptOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			AcceptManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			UrgencySign = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LIComplaintsInfoSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("ComplaintID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ComplaintID));
		}
		if (FCode.equals("TelephoneNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TelephoneNo));
		}
		if (FCode.equals("ComplaintType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ComplaintType));
		}
		if (FCode.equals("CustomerNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
		}
		if (FCode.equals("NeedDealFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(NeedDealFlag));
		}
		if (FCode.equals("OtherNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNo));
		}
		if (FCode.equals("OtherNoType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNoType));
		}
		if (FCode.equals("CustomerName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerName));
		}
		if (FCode.equals("TelephoneCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TelephoneCom));
		}
		if (FCode.equals("IssueType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IssueType));
		}
		if (FCode.equals("IssueState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IssueState));
		}
		if (FCode.equals("UndertakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getUndertakeDate()));
		}
		if (FCode.equals("UndertakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UndertakeTime));
		}
		if (FCode.equals("TransmitCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransmitCom));
		}
		if (FCode.equals("TransmitDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getTransmitDate()));
		}
		if (FCode.equals("TransmitTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransmitTime));
		}
		if (FCode.equals("DealOperator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DealOperator));
		}
		if (FCode.equals("BookingDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getBookingDate()));
		}
		if (FCode.equals("BookingTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BookingTime));
		}
		if (FCode.equals("CustomerAddress"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerAddress));
		}
		if (FCode.equals("ComplaintContent"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ComplaintContent));
		}
		if (FCode.equals("DealContent"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DealContent));
		}
		if (FCode.equals("AcceptOperator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AcceptOperator));
		}
		if (FCode.equals("AcceptManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AcceptManageCom));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("UrgencySign"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UrgencySign));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(ComplaintID);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(TelephoneNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(ComplaintType);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(CustomerNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(NeedDealFlag);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(OtherNo);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(OtherNoType);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(CustomerName);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(TelephoneCom);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(IssueType);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(IssueState);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getUndertakeDate()));
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(UndertakeTime);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(TransmitCom);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getTransmitDate()));
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(TransmitTime);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(DealOperator);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getBookingDate()));
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(BookingTime);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(CustomerAddress);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(ComplaintContent);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(DealContent);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(AcceptOperator);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(AcceptManageCom);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(UrgencySign);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("ComplaintID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ComplaintID = FValue.trim();
			}
			else
				ComplaintID = null;
		}
		if (FCode.equalsIgnoreCase("TelephoneNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TelephoneNo = FValue.trim();
			}
			else
				TelephoneNo = null;
		}
		if (FCode.equalsIgnoreCase("ComplaintType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ComplaintType = FValue.trim();
			}
			else
				ComplaintType = null;
		}
		if (FCode.equalsIgnoreCase("CustomerNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerNo = FValue.trim();
			}
			else
				CustomerNo = null;
		}
		if (FCode.equalsIgnoreCase("NeedDealFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				NeedDealFlag = FValue.trim();
			}
			else
				NeedDealFlag = null;
		}
		if (FCode.equalsIgnoreCase("OtherNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OtherNo = FValue.trim();
			}
			else
				OtherNo = null;
		}
		if (FCode.equalsIgnoreCase("OtherNoType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OtherNoType = FValue.trim();
			}
			else
				OtherNoType = null;
		}
		if (FCode.equalsIgnoreCase("CustomerName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerName = FValue.trim();
			}
			else
				CustomerName = null;
		}
		if (FCode.equalsIgnoreCase("TelephoneCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TelephoneCom = FValue.trim();
			}
			else
				TelephoneCom = null;
		}
		if (FCode.equalsIgnoreCase("IssueType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IssueType = FValue.trim();
			}
			else
				IssueType = null;
		}
		if (FCode.equalsIgnoreCase("IssueState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IssueState = FValue.trim();
			}
			else
				IssueState = null;
		}
		if (FCode.equalsIgnoreCase("UndertakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				UndertakeDate = fDate.getDate( FValue );
			}
			else
				UndertakeDate = null;
		}
		if (FCode.equalsIgnoreCase("UndertakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UndertakeTime = FValue.trim();
			}
			else
				UndertakeTime = null;
		}
		if (FCode.equalsIgnoreCase("TransmitCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TransmitCom = FValue.trim();
			}
			else
				TransmitCom = null;
		}
		if (FCode.equalsIgnoreCase("TransmitDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				TransmitDate = fDate.getDate( FValue );
			}
			else
				TransmitDate = null;
		}
		if (FCode.equalsIgnoreCase("TransmitTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TransmitTime = FValue.trim();
			}
			else
				TransmitTime = null;
		}
		if (FCode.equalsIgnoreCase("DealOperator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DealOperator = FValue.trim();
			}
			else
				DealOperator = null;
		}
		if (FCode.equalsIgnoreCase("BookingDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				BookingDate = fDate.getDate( FValue );
			}
			else
				BookingDate = null;
		}
		if (FCode.equalsIgnoreCase("BookingTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BookingTime = FValue.trim();
			}
			else
				BookingTime = null;
		}
		if (FCode.equalsIgnoreCase("CustomerAddress"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerAddress = FValue.trim();
			}
			else
				CustomerAddress = null;
		}
		if (FCode.equalsIgnoreCase("ComplaintContent"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ComplaintContent = FValue.trim();
			}
			else
				ComplaintContent = null;
		}
		if (FCode.equalsIgnoreCase("DealContent"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DealContent = FValue.trim();
			}
			else
				DealContent = null;
		}
		if (FCode.equalsIgnoreCase("AcceptOperator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AcceptOperator = FValue.trim();
			}
			else
				AcceptOperator = null;
		}
		if (FCode.equalsIgnoreCase("AcceptManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AcceptManageCom = FValue.trim();
			}
			else
				AcceptManageCom = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("UrgencySign"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UrgencySign = FValue.trim();
			}
			else
				UrgencySign = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LIComplaintsInfoSchema other = (LIComplaintsInfoSchema)otherObject;
		return
			(ComplaintID == null ? other.getComplaintID() == null : ComplaintID.equals(other.getComplaintID()))
			&& (TelephoneNo == null ? other.getTelephoneNo() == null : TelephoneNo.equals(other.getTelephoneNo()))
			&& (ComplaintType == null ? other.getComplaintType() == null : ComplaintType.equals(other.getComplaintType()))
			&& (CustomerNo == null ? other.getCustomerNo() == null : CustomerNo.equals(other.getCustomerNo()))
			&& (NeedDealFlag == null ? other.getNeedDealFlag() == null : NeedDealFlag.equals(other.getNeedDealFlag()))
			&& (OtherNo == null ? other.getOtherNo() == null : OtherNo.equals(other.getOtherNo()))
			&& (OtherNoType == null ? other.getOtherNoType() == null : OtherNoType.equals(other.getOtherNoType()))
			&& (CustomerName == null ? other.getCustomerName() == null : CustomerName.equals(other.getCustomerName()))
			&& (TelephoneCom == null ? other.getTelephoneCom() == null : TelephoneCom.equals(other.getTelephoneCom()))
			&& (IssueType == null ? other.getIssueType() == null : IssueType.equals(other.getIssueType()))
			&& (IssueState == null ? other.getIssueState() == null : IssueState.equals(other.getIssueState()))
			&& (UndertakeDate == null ? other.getUndertakeDate() == null : fDate.getString(UndertakeDate).equals(other.getUndertakeDate()))
			&& (UndertakeTime == null ? other.getUndertakeTime() == null : UndertakeTime.equals(other.getUndertakeTime()))
			&& (TransmitCom == null ? other.getTransmitCom() == null : TransmitCom.equals(other.getTransmitCom()))
			&& (TransmitDate == null ? other.getTransmitDate() == null : fDate.getString(TransmitDate).equals(other.getTransmitDate()))
			&& (TransmitTime == null ? other.getTransmitTime() == null : TransmitTime.equals(other.getTransmitTime()))
			&& (DealOperator == null ? other.getDealOperator() == null : DealOperator.equals(other.getDealOperator()))
			&& (BookingDate == null ? other.getBookingDate() == null : fDate.getString(BookingDate).equals(other.getBookingDate()))
			&& (BookingTime == null ? other.getBookingTime() == null : BookingTime.equals(other.getBookingTime()))
			&& (CustomerAddress == null ? other.getCustomerAddress() == null : CustomerAddress.equals(other.getCustomerAddress()))
			&& (ComplaintContent == null ? other.getComplaintContent() == null : ComplaintContent.equals(other.getComplaintContent()))
			&& (DealContent == null ? other.getDealContent() == null : DealContent.equals(other.getDealContent()))
			&& (AcceptOperator == null ? other.getAcceptOperator() == null : AcceptOperator.equals(other.getAcceptOperator()))
			&& (AcceptManageCom == null ? other.getAcceptManageCom() == null : AcceptManageCom.equals(other.getAcceptManageCom()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (UrgencySign == null ? other.getUrgencySign() == null : UrgencySign.equals(other.getUrgencySign()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("ComplaintID") ) {
			return 0;
		}
		if( strFieldName.equals("TelephoneNo") ) {
			return 1;
		}
		if( strFieldName.equals("ComplaintType") ) {
			return 2;
		}
		if( strFieldName.equals("CustomerNo") ) {
			return 3;
		}
		if( strFieldName.equals("NeedDealFlag") ) {
			return 4;
		}
		if( strFieldName.equals("OtherNo") ) {
			return 5;
		}
		if( strFieldName.equals("OtherNoType") ) {
			return 6;
		}
		if( strFieldName.equals("CustomerName") ) {
			return 7;
		}
		if( strFieldName.equals("TelephoneCom") ) {
			return 8;
		}
		if( strFieldName.equals("IssueType") ) {
			return 9;
		}
		if( strFieldName.equals("IssueState") ) {
			return 10;
		}
		if( strFieldName.equals("UndertakeDate") ) {
			return 11;
		}
		if( strFieldName.equals("UndertakeTime") ) {
			return 12;
		}
		if( strFieldName.equals("TransmitCom") ) {
			return 13;
		}
		if( strFieldName.equals("TransmitDate") ) {
			return 14;
		}
		if( strFieldName.equals("TransmitTime") ) {
			return 15;
		}
		if( strFieldName.equals("DealOperator") ) {
			return 16;
		}
		if( strFieldName.equals("BookingDate") ) {
			return 17;
		}
		if( strFieldName.equals("BookingTime") ) {
			return 18;
		}
		if( strFieldName.equals("CustomerAddress") ) {
			return 19;
		}
		if( strFieldName.equals("ComplaintContent") ) {
			return 20;
		}
		if( strFieldName.equals("DealContent") ) {
			return 21;
		}
		if( strFieldName.equals("AcceptOperator") ) {
			return 22;
		}
		if( strFieldName.equals("AcceptManageCom") ) {
			return 23;
		}
		if( strFieldName.equals("Operator") ) {
			return 24;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 25;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 26;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 27;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 28;
		}
		if( strFieldName.equals("UrgencySign") ) {
			return 29;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "ComplaintID";
				break;
			case 1:
				strFieldName = "TelephoneNo";
				break;
			case 2:
				strFieldName = "ComplaintType";
				break;
			case 3:
				strFieldName = "CustomerNo";
				break;
			case 4:
				strFieldName = "NeedDealFlag";
				break;
			case 5:
				strFieldName = "OtherNo";
				break;
			case 6:
				strFieldName = "OtherNoType";
				break;
			case 7:
				strFieldName = "CustomerName";
				break;
			case 8:
				strFieldName = "TelephoneCom";
				break;
			case 9:
				strFieldName = "IssueType";
				break;
			case 10:
				strFieldName = "IssueState";
				break;
			case 11:
				strFieldName = "UndertakeDate";
				break;
			case 12:
				strFieldName = "UndertakeTime";
				break;
			case 13:
				strFieldName = "TransmitCom";
				break;
			case 14:
				strFieldName = "TransmitDate";
				break;
			case 15:
				strFieldName = "TransmitTime";
				break;
			case 16:
				strFieldName = "DealOperator";
				break;
			case 17:
				strFieldName = "BookingDate";
				break;
			case 18:
				strFieldName = "BookingTime";
				break;
			case 19:
				strFieldName = "CustomerAddress";
				break;
			case 20:
				strFieldName = "ComplaintContent";
				break;
			case 21:
				strFieldName = "DealContent";
				break;
			case 22:
				strFieldName = "AcceptOperator";
				break;
			case 23:
				strFieldName = "AcceptManageCom";
				break;
			case 24:
				strFieldName = "Operator";
				break;
			case 25:
				strFieldName = "MakeDate";
				break;
			case 26:
				strFieldName = "MakeTime";
				break;
			case 27:
				strFieldName = "ModifyDate";
				break;
			case 28:
				strFieldName = "ModifyTime";
				break;
			case 29:
				strFieldName = "UrgencySign";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("ComplaintID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TelephoneNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ComplaintType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("NeedDealFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OtherNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OtherNoType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TelephoneCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IssueType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IssueState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UndertakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("UndertakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TransmitCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TransmitDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("TransmitTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DealOperator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BookingDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("BookingTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerAddress") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ComplaintContent") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DealContent") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AcceptOperator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AcceptManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UrgencySign") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
