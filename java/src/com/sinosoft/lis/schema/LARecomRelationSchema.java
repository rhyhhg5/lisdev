/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LARecomRelationDB;

/*
 * <p>ClassName: LARecomRelationSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-12-30
 */
public class LARecomRelationSchema implements Schema, Cloneable
{
	// @Field
	/** 推荐级别 */
	private String RecomLevel;
	/** 推荐代数目 */
	private int RecomGens;
	/** 被推荐人 */
	private String AgentCode;
	/** 推荐人 */
	private String RecomAgentCode;
	/** 被推荐机构 */
	private String AgentGroup;
	/** 推荐起期 */
	private Date StartDate;
	/** 推荐止期 */
	private Date EndDate;
	/** 推荐关系存在标记 */
	private String RecomFlag;
	/** 推荐津贴抽取标记 */
	private String RecomComFlag;
	/** 推荐津贴抽取起始年度 */
	private int RecomStartYear;
	/** 推荐津贴抽取比例 */
	private double Rate;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最近修改日期 */
	private Date ModifyDate;
	/** 最近修改时间 */
	private String ModifyTime;
	/** 操作员代码 */
	private String Operator;
	/** 行销比例扣除标志 */
	private String DeductFlag;
	/** 继承推荐津贴计算标志 */
	private String InheritCalFlag;
	/** 推荐奖金有效标志 */
	private String RecomBonusFlag;
	/** 行销扣除比例 */
	private double DeductRate;
	/** 被推荐人职级 */
	private String AgentGrade;
	/** 推荐人职级 */
	private String RecomAgentGrade;

	public static final int FIELDNUM = 22;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LARecomRelationSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[3];
		pk[0] = "RecomLevel";
		pk[1] = "RecomGens";
		pk[2] = "AgentCode";

		PK = pk;
	}

            /**
             * Schema克隆
             * @return Object
             * @throws CloneNotSupportedException
             */
            public Object clone()
                    throws CloneNotSupportedException
            {
                LARecomRelationSchema cloned = (LARecomRelationSchema)super.clone();
                cloned.fDate = (FDate) fDate.clone();
                cloned.mErrors = (CErrors) mErrors.clone();
                return cloned;
            }

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getRecomLevel()
	{
		return RecomLevel;
	}
	public void setRecomLevel(String aRecomLevel)
	{
            RecomLevel = aRecomLevel;
	}
	public int getRecomGens()
	{
		return RecomGens;
	}
	public void setRecomGens(int aRecomGens)
	{
            RecomGens = aRecomGens;
	}
	public void setRecomGens(String aRecomGens)
	{
		if (aRecomGens != null && !aRecomGens.equals(""))
		{
			Integer tInteger = new Integer(aRecomGens);
			int i = tInteger.intValue();
			RecomGens = i;
		}
	}

	public String getAgentCode()
	{
		return AgentCode;
	}
	public void setAgentCode(String aAgentCode)
	{
            AgentCode = aAgentCode;
	}
	public String getRecomAgentCode()
	{
		return RecomAgentCode;
	}
	public void setRecomAgentCode(String aRecomAgentCode)
	{
            RecomAgentCode = aRecomAgentCode;
	}
	public String getAgentGroup()
	{
		return AgentGroup;
	}
	public void setAgentGroup(String aAgentGroup)
	{
            AgentGroup = aAgentGroup;
	}
	public String getStartDate()
	{
		if( StartDate != null )
			return fDate.getString(StartDate);
		else
			return null;
	}
	public void setStartDate(Date aStartDate)
	{
            StartDate = aStartDate;
	}
	public void setStartDate(String aStartDate)
	{
		if (aStartDate != null && !aStartDate.equals("") )
		{
			StartDate = fDate.getDate( aStartDate );
		}
		else
			StartDate = null;
	}

	public String getEndDate()
	{
		if( EndDate != null )
			return fDate.getString(EndDate);
		else
			return null;
	}
	public void setEndDate(Date aEndDate)
	{
            EndDate = aEndDate;
	}
	public void setEndDate(String aEndDate)
	{
		if (aEndDate != null && !aEndDate.equals("") )
		{
			EndDate = fDate.getDate( aEndDate );
		}
		else
			EndDate = null;
	}

	public String getRecomFlag()
	{
		return RecomFlag;
	}
	public void setRecomFlag(String aRecomFlag)
	{
            RecomFlag = aRecomFlag;
	}
	public String getRecomComFlag()
	{
		return RecomComFlag;
	}
	public void setRecomComFlag(String aRecomComFlag)
	{
            RecomComFlag = aRecomComFlag;
	}
	public int getRecomStartYear()
	{
		return RecomStartYear;
	}
	public void setRecomStartYear(int aRecomStartYear)
	{
            RecomStartYear = aRecomStartYear;
	}
	public void setRecomStartYear(String aRecomStartYear)
	{
		if (aRecomStartYear != null && !aRecomStartYear.equals(""))
		{
			Integer tInteger = new Integer(aRecomStartYear);
			int i = tInteger.intValue();
			RecomStartYear = i;
		}
	}

	public double getRate()
	{
		return Rate;
	}
	public void setRate(double aRate)
	{
            Rate = Arith.round(aRate,2);
	}
	public void setRate(String aRate)
	{
		if (aRate != null && !aRate.equals(""))
		{
			Double tDouble = new Double(aRate);
			double d = tDouble.doubleValue();
                Rate = Arith.round(d,2);
		}
	}

	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
            MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
            MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
            ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
            ModifyTime = aModifyTime;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
            Operator = aOperator;
	}
	public String getDeductFlag()
	{
		return DeductFlag;
	}
	public void setDeductFlag(String aDeductFlag)
	{
            DeductFlag = aDeductFlag;
	}
	public String getInheritCalFlag()
	{
		return InheritCalFlag;
	}
	public void setInheritCalFlag(String aInheritCalFlag)
	{
            InheritCalFlag = aInheritCalFlag;
	}
	public String getRecomBonusFlag()
	{
		return RecomBonusFlag;
	}
	public void setRecomBonusFlag(String aRecomBonusFlag)
	{
            RecomBonusFlag = aRecomBonusFlag;
	}
	public double getDeductRate()
	{
		return DeductRate;
	}
	public void setDeductRate(double aDeductRate)
	{
            DeductRate = Arith.round(aDeductRate,2);
	}
	public void setDeductRate(String aDeductRate)
	{
		if (aDeductRate != null && !aDeductRate.equals(""))
		{
			Double tDouble = new Double(aDeductRate);
			double d = tDouble.doubleValue();
                DeductRate = Arith.round(d,2);
		}
	}

	public String getAgentGrade()
	{
		return AgentGrade;
	}
	public void setAgentGrade(String aAgentGrade)
	{
            AgentGrade = aAgentGrade;
	}
	public String getRecomAgentGrade()
	{
		return RecomAgentGrade;
	}
	public void setRecomAgentGrade(String aRecomAgentGrade)
	{
            RecomAgentGrade = aRecomAgentGrade;
	}

	/**
	* 使用另外一个 LARecomRelationSchema 对象给 Schema 赋值
	* @param: aLARecomRelationSchema LARecomRelationSchema
	**/
	public void setSchema(LARecomRelationSchema aLARecomRelationSchema)
	{
		this.RecomLevel = aLARecomRelationSchema.getRecomLevel();
		this.RecomGens = aLARecomRelationSchema.getRecomGens();
		this.AgentCode = aLARecomRelationSchema.getAgentCode();
		this.RecomAgentCode = aLARecomRelationSchema.getRecomAgentCode();
		this.AgentGroup = aLARecomRelationSchema.getAgentGroup();
		this.StartDate = fDate.getDate( aLARecomRelationSchema.getStartDate());
		this.EndDate = fDate.getDate( aLARecomRelationSchema.getEndDate());
		this.RecomFlag = aLARecomRelationSchema.getRecomFlag();
		this.RecomComFlag = aLARecomRelationSchema.getRecomComFlag();
		this.RecomStartYear = aLARecomRelationSchema.getRecomStartYear();
		this.Rate = aLARecomRelationSchema.getRate();
		this.MakeDate = fDate.getDate( aLARecomRelationSchema.getMakeDate());
		this.MakeTime = aLARecomRelationSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLARecomRelationSchema.getModifyDate());
		this.ModifyTime = aLARecomRelationSchema.getModifyTime();
		this.Operator = aLARecomRelationSchema.getOperator();
		this.DeductFlag = aLARecomRelationSchema.getDeductFlag();
		this.InheritCalFlag = aLARecomRelationSchema.getInheritCalFlag();
		this.RecomBonusFlag = aLARecomRelationSchema.getRecomBonusFlag();
		this.DeductRate = aLARecomRelationSchema.getDeductRate();
		this.AgentGrade = aLARecomRelationSchema.getAgentGrade();
		this.RecomAgentGrade = aLARecomRelationSchema.getRecomAgentGrade();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("RecomLevel") == null )
				this.RecomLevel = null;
			else
				this.RecomLevel = rs.getString("RecomLevel").trim();

			this.RecomGens = rs.getInt("RecomGens");
			if( rs.getString("AgentCode") == null )
				this.AgentCode = null;
			else
				this.AgentCode = rs.getString("AgentCode").trim();

			if( rs.getString("RecomAgentCode") == null )
				this.RecomAgentCode = null;
			else
				this.RecomAgentCode = rs.getString("RecomAgentCode").trim();

			if( rs.getString("AgentGroup") == null )
				this.AgentGroup = null;
			else
				this.AgentGroup = rs.getString("AgentGroup").trim();

			this.StartDate = rs.getDate("StartDate");
			this.EndDate = rs.getDate("EndDate");
			if( rs.getString("RecomFlag") == null )
				this.RecomFlag = null;
			else
				this.RecomFlag = rs.getString("RecomFlag").trim();

			if( rs.getString("RecomComFlag") == null )
				this.RecomComFlag = null;
			else
				this.RecomComFlag = rs.getString("RecomComFlag").trim();

			this.RecomStartYear = rs.getInt("RecomStartYear");
			this.Rate = rs.getDouble("Rate");
			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			if( rs.getString("DeductFlag") == null )
				this.DeductFlag = null;
			else
				this.DeductFlag = rs.getString("DeductFlag").trim();

			if( rs.getString("InheritCalFlag") == null )
				this.InheritCalFlag = null;
			else
				this.InheritCalFlag = rs.getString("InheritCalFlag").trim();

			if( rs.getString("RecomBonusFlag") == null )
				this.RecomBonusFlag = null;
			else
				this.RecomBonusFlag = rs.getString("RecomBonusFlag").trim();

			this.DeductRate = rs.getDouble("DeductRate");
			if( rs.getString("AgentGrade") == null )
				this.AgentGrade = null;
			else
				this.AgentGrade = rs.getString("AgentGrade").trim();

			if( rs.getString("RecomAgentGrade") == null )
				this.RecomAgentGrade = null;
			else
				this.RecomAgentGrade = rs.getString("RecomAgentGrade").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LARecomRelation表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LARecomRelationSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LARecomRelationSchema getSchema()
	{
		LARecomRelationSchema aLARecomRelationSchema = new LARecomRelationSchema();
		aLARecomRelationSchema.setSchema(this);
		return aLARecomRelationSchema;
	}

	public LARecomRelationDB getDB()
	{
		LARecomRelationDB aDBOper = new LARecomRelationDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLARecomRelation描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append(StrTool.cTrim(RecomLevel)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(RecomGens));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(AgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(RecomAgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(AgentGroup)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( StartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( EndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(RecomFlag)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(RecomComFlag)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(RecomStartYear));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(Rate));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(DeductFlag)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(InheritCalFlag)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(RecomBonusFlag)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(DeductRate));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(AgentGrade)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(RecomAgentGrade));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLARecomRelation>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			RecomLevel = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			RecomGens= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,2,SysConst.PACKAGESPILTER))).intValue();
			AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			RecomAgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			StartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,SysConst.PACKAGESPILTER));
			EndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,SysConst.PACKAGESPILTER));
			RecomFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			RecomComFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			RecomStartYear= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,10,SysConst.PACKAGESPILTER))).intValue();
			Rate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,11,SysConst.PACKAGESPILTER))).doubleValue();
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			DeductFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			InheritCalFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			RecomBonusFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			DeductRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,20,SysConst.PACKAGESPILTER))).doubleValue();
			AgentGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			RecomAgentGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LARecomRelationSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("RecomLevel"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RecomLevel));
		}
		if (FCode.equals("RecomGens"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RecomGens));
		}
		if (FCode.equals("AgentCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
		}
		if (FCode.equals("RecomAgentCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RecomAgentCode));
		}
		if (FCode.equals("AgentGroup"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
		}
		if (FCode.equals("StartDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStartDate()));
		}
		if (FCode.equals("EndDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEndDate()));
		}
		if (FCode.equals("RecomFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RecomFlag));
		}
		if (FCode.equals("RecomComFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RecomComFlag));
		}
		if (FCode.equals("RecomStartYear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RecomStartYear));
		}
		if (FCode.equals("Rate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Rate));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("DeductFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DeductFlag));
		}
		if (FCode.equals("InheritCalFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InheritCalFlag));
		}
		if (FCode.equals("RecomBonusFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RecomBonusFlag));
		}
		if (FCode.equals("DeductRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DeductRate));
		}
		if (FCode.equals("AgentGrade"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGrade));
		}
		if (FCode.equals("RecomAgentGrade"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RecomAgentGrade));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(RecomLevel);
				break;
			case 1:
				strFieldValue = String.valueOf(RecomGens);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(AgentCode);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(RecomAgentCode);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(AgentGroup);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStartDate()));
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEndDate()));
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(RecomFlag);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(RecomComFlag);
				break;
			case 9:
				strFieldValue = String.valueOf(RecomStartYear);
				break;
			case 10:
				strFieldValue = String.valueOf(Rate);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(DeductFlag);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(InheritCalFlag);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(RecomBonusFlag);
				break;
			case 19:
				strFieldValue = String.valueOf(DeductRate);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(AgentGrade);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(RecomAgentGrade);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("RecomLevel"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RecomLevel = FValue.trim();
			}
			else
				RecomLevel = null;
		}
		if (FCode.equalsIgnoreCase("RecomGens"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				RecomGens = i;
			}
		}
		if (FCode.equalsIgnoreCase("AgentCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCode = FValue.trim();
			}
			else
				AgentCode = null;
		}
		if (FCode.equalsIgnoreCase("RecomAgentCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RecomAgentCode = FValue.trim();
			}
			else
				RecomAgentCode = null;
		}
		if (FCode.equalsIgnoreCase("AgentGroup"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentGroup = FValue.trim();
			}
			else
				AgentGroup = null;
		}
		if (FCode.equalsIgnoreCase("StartDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				StartDate = fDate.getDate( FValue );
			}
			else
				StartDate = null;
		}
		if (FCode.equalsIgnoreCase("EndDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				EndDate = fDate.getDate( FValue );
			}
			else
				EndDate = null;
		}
		if (FCode.equalsIgnoreCase("RecomFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RecomFlag = FValue.trim();
			}
			else
				RecomFlag = null;
		}
		if (FCode.equalsIgnoreCase("RecomComFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RecomComFlag = FValue.trim();
			}
			else
				RecomComFlag = null;
		}
		if (FCode.equalsIgnoreCase("RecomStartYear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				RecomStartYear = i;
			}
		}
		if (FCode.equalsIgnoreCase("Rate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Rate = d;
			}
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("DeductFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DeductFlag = FValue.trim();
			}
			else
				DeductFlag = null;
		}
		if (FCode.equalsIgnoreCase("InheritCalFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InheritCalFlag = FValue.trim();
			}
			else
				InheritCalFlag = null;
		}
		if (FCode.equalsIgnoreCase("RecomBonusFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RecomBonusFlag = FValue.trim();
			}
			else
				RecomBonusFlag = null;
		}
		if (FCode.equalsIgnoreCase("DeductRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				DeductRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("AgentGrade"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentGrade = FValue.trim();
			}
			else
				AgentGrade = null;
		}
		if (FCode.equalsIgnoreCase("RecomAgentGrade"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RecomAgentGrade = FValue.trim();
			}
			else
				RecomAgentGrade = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LARecomRelationSchema other = (LARecomRelationSchema)otherObject;
		return
			RecomLevel.equals(other.getRecomLevel())
			&& RecomGens == other.getRecomGens()
			&& AgentCode.equals(other.getAgentCode())
			&& RecomAgentCode.equals(other.getRecomAgentCode())
			&& AgentGroup.equals(other.getAgentGroup())
			&& fDate.getString(StartDate).equals(other.getStartDate())
			&& fDate.getString(EndDate).equals(other.getEndDate())
			&& RecomFlag.equals(other.getRecomFlag())
			&& RecomComFlag.equals(other.getRecomComFlag())
			&& RecomStartYear == other.getRecomStartYear()
			&& Rate == other.getRate()
			&& fDate.getString(MakeDate).equals(other.getMakeDate())
			&& MakeTime.equals(other.getMakeTime())
			&& fDate.getString(ModifyDate).equals(other.getModifyDate())
			&& ModifyTime.equals(other.getModifyTime())
			&& Operator.equals(other.getOperator())
			&& DeductFlag.equals(other.getDeductFlag())
			&& InheritCalFlag.equals(other.getInheritCalFlag())
			&& RecomBonusFlag.equals(other.getRecomBonusFlag())
			&& DeductRate == other.getDeductRate()
			&& AgentGrade.equals(other.getAgentGrade())
			&& RecomAgentGrade.equals(other.getRecomAgentGrade());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("RecomLevel") ) {
			return 0;
		}
		if( strFieldName.equals("RecomGens") ) {
			return 1;
		}
		if( strFieldName.equals("AgentCode") ) {
			return 2;
		}
		if( strFieldName.equals("RecomAgentCode") ) {
			return 3;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return 4;
		}
		if( strFieldName.equals("StartDate") ) {
			return 5;
		}
		if( strFieldName.equals("EndDate") ) {
			return 6;
		}
		if( strFieldName.equals("RecomFlag") ) {
			return 7;
		}
		if( strFieldName.equals("RecomComFlag") ) {
			return 8;
		}
		if( strFieldName.equals("RecomStartYear") ) {
			return 9;
		}
		if( strFieldName.equals("Rate") ) {
			return 10;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 11;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 12;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 13;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 14;
		}
		if( strFieldName.equals("Operator") ) {
			return 15;
		}
		if( strFieldName.equals("DeductFlag") ) {
			return 16;
		}
		if( strFieldName.equals("InheritCalFlag") ) {
			return 17;
		}
		if( strFieldName.equals("RecomBonusFlag") ) {
			return 18;
		}
		if( strFieldName.equals("DeductRate") ) {
			return 19;
		}
		if( strFieldName.equals("AgentGrade") ) {
			return 20;
		}
		if( strFieldName.equals("RecomAgentGrade") ) {
			return 21;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "RecomLevel";
				break;
			case 1:
				strFieldName = "RecomGens";
				break;
			case 2:
				strFieldName = "AgentCode";
				break;
			case 3:
				strFieldName = "RecomAgentCode";
				break;
			case 4:
				strFieldName = "AgentGroup";
				break;
			case 5:
				strFieldName = "StartDate";
				break;
			case 6:
				strFieldName = "EndDate";
				break;
			case 7:
				strFieldName = "RecomFlag";
				break;
			case 8:
				strFieldName = "RecomComFlag";
				break;
			case 9:
				strFieldName = "RecomStartYear";
				break;
			case 10:
				strFieldName = "Rate";
				break;
			case 11:
				strFieldName = "MakeDate";
				break;
			case 12:
				strFieldName = "MakeTime";
				break;
			case 13:
				strFieldName = "ModifyDate";
				break;
			case 14:
				strFieldName = "ModifyTime";
				break;
			case 15:
				strFieldName = "Operator";
				break;
			case 16:
				strFieldName = "DeductFlag";
				break;
			case 17:
				strFieldName = "InheritCalFlag";
				break;
			case 18:
				strFieldName = "RecomBonusFlag";
				break;
			case 19:
				strFieldName = "DeductRate";
				break;
			case 20:
				strFieldName = "AgentGrade";
				break;
			case 21:
				strFieldName = "RecomAgentGrade";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("RecomLevel") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RecomGens") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("AgentCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RecomAgentCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StartDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("EndDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("RecomFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RecomComFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RecomStartYear") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("Rate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DeductFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InheritCalFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RecomBonusFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DeductRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("AgentGrade") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RecomAgentGrade") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_INT;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 6:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_INT;
				break;
			case 10:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
