/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LLMAffixDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

/*
 * <p>ClassName: LLMAffixSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-02-28
 */
public class LLMAffixSchema implements Schema
{
    // @Field
    /** 材料类型代码 */
    private String AffixTypeCode;
    /** 材料类型名称 */
    private String AffixTypeName;
    /** 材料代码 */
    private String AffixCode;
    /** 材料名称 */
    private String AffixName;
    /** 管理机构 */
    private String ManageCome;

    public static final int FIELDNUM = 5; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LLMAffixSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "AffixTypeCode";
        pk[1] = "AffixCode";
        pk[2] = "ManageCome";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getAffixTypeCode()
    {
        if (AffixTypeCode != null && !AffixTypeCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AffixTypeCode = StrTool.unicodeToGBK(AffixTypeCode);
        }
        return AffixTypeCode;
    }

    public void setAffixTypeCode(String aAffixTypeCode)
    {
        AffixTypeCode = aAffixTypeCode;
    }

    public String getAffixTypeName()
    {
        if (AffixTypeName != null && !AffixTypeName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AffixTypeName = StrTool.unicodeToGBK(AffixTypeName);
        }
        return AffixTypeName;
    }

    public void setAffixTypeName(String aAffixTypeName)
    {
        AffixTypeName = aAffixTypeName;
    }

    public String getAffixCode()
    {
        if (AffixCode != null && !AffixCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AffixCode = StrTool.unicodeToGBK(AffixCode);
        }
        return AffixCode;
    }

    public void setAffixCode(String aAffixCode)
    {
        AffixCode = aAffixCode;
    }

    public String getAffixName()
    {
        if (AffixName != null && !AffixName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AffixName = StrTool.unicodeToGBK(AffixName);
        }
        return AffixName;
    }

    public void setAffixName(String aAffixName)
    {
        AffixName = aAffixName;
    }

    public String getManageCome()
    {
        if (ManageCome != null && !ManageCome.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ManageCome = StrTool.unicodeToGBK(ManageCome);
        }
        return ManageCome;
    }

    public void setManageCome(String aManageCome)
    {
        ManageCome = aManageCome;
    }

    /**
     * 使用另外一个 LLMAffixSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LLMAffixSchema aLLMAffixSchema)
    {
        this.AffixTypeCode = aLLMAffixSchema.getAffixTypeCode();
        this.AffixTypeName = aLLMAffixSchema.getAffixTypeName();
        this.AffixCode = aLLMAffixSchema.getAffixCode();
        this.AffixName = aLLMAffixSchema.getAffixName();
        this.ManageCome = aLLMAffixSchema.getManageCome();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("AffixTypeCode") == null)
            {
                this.AffixTypeCode = null;
            }
            else
            {
                this.AffixTypeCode = rs.getString("AffixTypeCode").trim();
            }

            if (rs.getString("AffixTypeName") == null)
            {
                this.AffixTypeName = null;
            }
            else
            {
                this.AffixTypeName = rs.getString("AffixTypeName").trim();
            }

            if (rs.getString("AffixCode") == null)
            {
                this.AffixCode = null;
            }
            else
            {
                this.AffixCode = rs.getString("AffixCode").trim();
            }

            if (rs.getString("AffixName") == null)
            {
                this.AffixName = null;
            }
            else
            {
                this.AffixName = rs.getString("AffixName").trim();
            }

            if (rs.getString("ManageCome") == null)
            {
                this.ManageCome = null;
            }
            else
            {
                this.ManageCome = rs.getString("ManageCome").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLMAffixSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LLMAffixSchema getSchema()
    {
        LLMAffixSchema aLLMAffixSchema = new LLMAffixSchema();
        aLLMAffixSchema.setSchema(this);
        return aLLMAffixSchema;
    }

    public LLMAffixDB getDB()
    {
        LLMAffixDB aDBOper = new LLMAffixDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLMAffix描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(AffixTypeCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AffixTypeName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AffixCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AffixName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ManageCome));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLMAffix>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            AffixTypeCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                           SysConst.PACKAGESPILTER);
            AffixTypeName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                           SysConst.PACKAGESPILTER);
            AffixCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                       SysConst.PACKAGESPILTER);
            AffixName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                       SysConst.PACKAGESPILTER);
            ManageCome = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLMAffixSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("AffixTypeCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AffixTypeCode));
        }
        if (FCode.equals("AffixTypeName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AffixTypeName));
        }
        if (FCode.equals("AffixCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AffixCode));
        }
        if (FCode.equals("AffixName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AffixName));
        }
        if (FCode.equals("ManageCome"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ManageCome));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(AffixTypeCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(AffixTypeName);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(AffixCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(AffixName);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(ManageCome);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("AffixTypeCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AffixTypeCode = FValue.trim();
            }
            else
            {
                AffixTypeCode = null;
            }
        }
        if (FCode.equals("AffixTypeName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AffixTypeName = FValue.trim();
            }
            else
            {
                AffixTypeName = null;
            }
        }
        if (FCode.equals("AffixCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AffixCode = FValue.trim();
            }
            else
            {
                AffixCode = null;
            }
        }
        if (FCode.equals("AffixName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AffixName = FValue.trim();
            }
            else
            {
                AffixName = null;
            }
        }
        if (FCode.equals("ManageCome"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCome = FValue.trim();
            }
            else
            {
                ManageCome = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LLMAffixSchema other = (LLMAffixSchema) otherObject;
        return
                AffixTypeCode.equals(other.getAffixTypeCode())
                && AffixTypeName.equals(other.getAffixTypeName())
                && AffixCode.equals(other.getAffixCode())
                && AffixName.equals(other.getAffixName())
                && ManageCome.equals(other.getManageCome());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("AffixTypeCode"))
        {
            return 0;
        }
        if (strFieldName.equals("AffixTypeName"))
        {
            return 1;
        }
        if (strFieldName.equals("AffixCode"))
        {
            return 2;
        }
        if (strFieldName.equals("AffixName"))
        {
            return 3;
        }
        if (strFieldName.equals("ManageCome"))
        {
            return 4;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "AffixTypeCode";
                break;
            case 1:
                strFieldName = "AffixTypeName";
                break;
            case 2:
                strFieldName = "AffixCode";
                break;
            case 3:
                strFieldName = "AffixName";
                break;
            case 4:
                strFieldName = "ManageCome";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("AffixTypeCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AffixTypeName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AffixCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AffixName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCome"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
