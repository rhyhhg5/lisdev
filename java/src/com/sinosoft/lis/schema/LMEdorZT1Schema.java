/*
 * <p>ClassName: LMEdorZT1Schema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LMEdorZT1DB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LMEdorZT1Schema implements Schema
{
    // @Field
    /** 险种编码 */
    private String RiskCode;
    /** 责任编码 */
    private String DutyCode;
    /** 交费计划编码 */
    private String PayPlanCode;
    /** 期交算法编码 */
    private String CycPayCalCode;
    /** 趸交算法编码 */
    private String OnePayCalCode;
    /** 期缴时间间隔 */
    private String CycPayIntvType;
    /** 趸缴时间间隔 */
    private String OnePayIntvType;
    /** 生存退保生存金计算类型 */
    private String LiveGetType;
    /** 死亡退保生存金计算类型 */
    private String DeadGetType;
    /** 外部转移退保生存金计算类型 */
    private String OutGetType;
    /** 现金价值计算公式 */
    private String CashValueCode;
    /** 计算方式参考 */
    private String CalCodeType;
    /** 退保年度计算类型 */
    private String ZTYearType;

    public static final int FIELDNUM = 13; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LMEdorZT1Schema()
    {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "RiskCode";
        pk[1] = "DutyCode";
        pk[2] = "PayPlanCode";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getRiskCode()
    {
        if (RiskCode != null && !RiskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getDutyCode()
    {
        if (DutyCode != null && !DutyCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            DutyCode = StrTool.unicodeToGBK(DutyCode);
        }
        return DutyCode;
    }

    public void setDutyCode(String aDutyCode)
    {
        DutyCode = aDutyCode;
    }

    public String getPayPlanCode()
    {
        if (PayPlanCode != null && !PayPlanCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            PayPlanCode = StrTool.unicodeToGBK(PayPlanCode);
        }
        return PayPlanCode;
    }

    public void setPayPlanCode(String aPayPlanCode)
    {
        PayPlanCode = aPayPlanCode;
    }

    public String getCycPayCalCode()
    {
        if (CycPayCalCode != null && !CycPayCalCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CycPayCalCode = StrTool.unicodeToGBK(CycPayCalCode);
        }
        return CycPayCalCode;
    }

    public void setCycPayCalCode(String aCycPayCalCode)
    {
        CycPayCalCode = aCycPayCalCode;
    }

    public String getOnePayCalCode()
    {
        if (OnePayCalCode != null && !OnePayCalCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            OnePayCalCode = StrTool.unicodeToGBK(OnePayCalCode);
        }
        return OnePayCalCode;
    }

    public void setOnePayCalCode(String aOnePayCalCode)
    {
        OnePayCalCode = aOnePayCalCode;
    }

    public String getCycPayIntvType()
    {
        if (CycPayIntvType != null && !CycPayIntvType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CycPayIntvType = StrTool.unicodeToGBK(CycPayIntvType);
        }
        return CycPayIntvType;
    }

    public void setCycPayIntvType(String aCycPayIntvType)
    {
        CycPayIntvType = aCycPayIntvType;
    }

    public String getOnePayIntvType()
    {
        if (OnePayIntvType != null && !OnePayIntvType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            OnePayIntvType = StrTool.unicodeToGBK(OnePayIntvType);
        }
        return OnePayIntvType;
    }

    public void setOnePayIntvType(String aOnePayIntvType)
    {
        OnePayIntvType = aOnePayIntvType;
    }

    public String getLiveGetType()
    {
        if (LiveGetType != null && !LiveGetType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            LiveGetType = StrTool.unicodeToGBK(LiveGetType);
        }
        return LiveGetType;
    }

    public void setLiveGetType(String aLiveGetType)
    {
        LiveGetType = aLiveGetType;
    }

    public String getDeadGetType()
    {
        if (DeadGetType != null && !DeadGetType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            DeadGetType = StrTool.unicodeToGBK(DeadGetType);
        }
        return DeadGetType;
    }

    public void setDeadGetType(String aDeadGetType)
    {
        DeadGetType = aDeadGetType;
    }

    public String getOutGetType()
    {
        if (OutGetType != null && !OutGetType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            OutGetType = StrTool.unicodeToGBK(OutGetType);
        }
        return OutGetType;
    }

    public void setOutGetType(String aOutGetType)
    {
        OutGetType = aOutGetType;
    }

    public String getCashValueCode()
    {
        if (CashValueCode != null && !CashValueCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CashValueCode = StrTool.unicodeToGBK(CashValueCode);
        }
        return CashValueCode;
    }

    public void setCashValueCode(String aCashValueCode)
    {
        CashValueCode = aCashValueCode;
    }

    public String getCalCodeType()
    {
        if (CalCodeType != null && !CalCodeType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CalCodeType = StrTool.unicodeToGBK(CalCodeType);
        }
        return CalCodeType;
    }

    public void setCalCodeType(String aCalCodeType)
    {
        CalCodeType = aCalCodeType;
    }

    public String getZTYearType()
    {
        if (ZTYearType != null && !ZTYearType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ZTYearType = StrTool.unicodeToGBK(ZTYearType);
        }
        return ZTYearType;
    }

    public void setZTYearType(String aZTYearType)
    {
        ZTYearType = aZTYearType;
    }

    /**
     * 使用另外一个 LMEdorZT1Schema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LMEdorZT1Schema aLMEdorZT1Schema)
    {
        this.RiskCode = aLMEdorZT1Schema.getRiskCode();
        this.DutyCode = aLMEdorZT1Schema.getDutyCode();
        this.PayPlanCode = aLMEdorZT1Schema.getPayPlanCode();
        this.CycPayCalCode = aLMEdorZT1Schema.getCycPayCalCode();
        this.OnePayCalCode = aLMEdorZT1Schema.getOnePayCalCode();
        this.CycPayIntvType = aLMEdorZT1Schema.getCycPayIntvType();
        this.OnePayIntvType = aLMEdorZT1Schema.getOnePayIntvType();
        this.LiveGetType = aLMEdorZT1Schema.getLiveGetType();
        this.DeadGetType = aLMEdorZT1Schema.getDeadGetType();
        this.OutGetType = aLMEdorZT1Schema.getOutGetType();
        this.CashValueCode = aLMEdorZT1Schema.getCashValueCode();
        this.CalCodeType = aLMEdorZT1Schema.getCalCodeType();
        this.ZTYearType = aLMEdorZT1Schema.getZTYearType();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("DutyCode") == null)
            {
                this.DutyCode = null;
            }
            else
            {
                this.DutyCode = rs.getString("DutyCode").trim();
            }

            if (rs.getString("PayPlanCode") == null)
            {
                this.PayPlanCode = null;
            }
            else
            {
                this.PayPlanCode = rs.getString("PayPlanCode").trim();
            }

            if (rs.getString("CycPayCalCode") == null)
            {
                this.CycPayCalCode = null;
            }
            else
            {
                this.CycPayCalCode = rs.getString("CycPayCalCode").trim();
            }

            if (rs.getString("OnePayCalCode") == null)
            {
                this.OnePayCalCode = null;
            }
            else
            {
                this.OnePayCalCode = rs.getString("OnePayCalCode").trim();
            }

            if (rs.getString("CycPayIntvType") == null)
            {
                this.CycPayIntvType = null;
            }
            else
            {
                this.CycPayIntvType = rs.getString("CycPayIntvType").trim();
            }

            if (rs.getString("OnePayIntvType") == null)
            {
                this.OnePayIntvType = null;
            }
            else
            {
                this.OnePayIntvType = rs.getString("OnePayIntvType").trim();
            }

            if (rs.getString("LiveGetType") == null)
            {
                this.LiveGetType = null;
            }
            else
            {
                this.LiveGetType = rs.getString("LiveGetType").trim();
            }

            if (rs.getString("DeadGetType") == null)
            {
                this.DeadGetType = null;
            }
            else
            {
                this.DeadGetType = rs.getString("DeadGetType").trim();
            }

            if (rs.getString("OutGetType") == null)
            {
                this.OutGetType = null;
            }
            else
            {
                this.OutGetType = rs.getString("OutGetType").trim();
            }

            if (rs.getString("CashValueCode") == null)
            {
                this.CashValueCode = null;
            }
            else
            {
                this.CashValueCode = rs.getString("CashValueCode").trim();
            }

            if (rs.getString("CalCodeType") == null)
            {
                this.CalCodeType = null;
            }
            else
            {
                this.CalCodeType = rs.getString("CalCodeType").trim();
            }

            if (rs.getString("ZTYearType") == null)
            {
                this.ZTYearType = null;
            }
            else
            {
                this.ZTYearType = rs.getString("ZTYearType").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMEdorZT1Schema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LMEdorZT1Schema getSchema()
    {
        LMEdorZT1Schema aLMEdorZT1Schema = new LMEdorZT1Schema();
        aLMEdorZT1Schema.setSchema(this);
        return aLMEdorZT1Schema;
    }

    public LMEdorZT1DB getDB()
    {
        LMEdorZT1DB aDBOper = new LMEdorZT1DB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMEdorZT1描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(DutyCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PayPlanCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CycPayCalCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(OnePayCalCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CycPayIntvType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(OnePayIntvType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(LiveGetType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(DeadGetType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(OutGetType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CashValueCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CalCodeType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ZTYearType));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMEdorZT1>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            DutyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            PayPlanCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                         SysConst.PACKAGESPILTER);
            CycPayCalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                           SysConst.PACKAGESPILTER);
            OnePayCalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                           SysConst.PACKAGESPILTER);
            CycPayIntvType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                            SysConst.PACKAGESPILTER);
            OnePayIntvType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                            SysConst.PACKAGESPILTER);
            LiveGetType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                         SysConst.PACKAGESPILTER);
            DeadGetType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                         SysConst.PACKAGESPILTER);
            OutGetType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                        SysConst.PACKAGESPILTER);
            CashValueCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                           SysConst.PACKAGESPILTER);
            CalCodeType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                         SysConst.PACKAGESPILTER);
            ZTYearType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMEdorZT1Schema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskCode));
        }
        if (FCode.equals("DutyCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DutyCode));
        }
        if (FCode.equals("PayPlanCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PayPlanCode));
        }
        if (FCode.equals("CycPayCalCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CycPayCalCode));
        }
        if (FCode.equals("OnePayCalCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(OnePayCalCode));
        }
        if (FCode.equals("CycPayIntvType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CycPayIntvType));
        }
        if (FCode.equals("OnePayIntvType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(OnePayIntvType));
        }
        if (FCode.equals("LiveGetType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(LiveGetType));
        }
        if (FCode.equals("DeadGetType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DeadGetType));
        }
        if (FCode.equals("OutGetType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(OutGetType));
        }
        if (FCode.equals("CashValueCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CashValueCode));
        }
        if (FCode.equals("CalCodeType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CalCodeType));
        }
        if (FCode.equals("ZTYearType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ZTYearType));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(DutyCode);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(PayPlanCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(CycPayCalCode);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(OnePayCalCode);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(CycPayIntvType);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(OnePayIntvType);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(LiveGetType);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(DeadGetType);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(OutGetType);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(CashValueCode);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(CalCodeType);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(ZTYearType);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("DutyCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DutyCode = FValue.trim();
            }
            else
            {
                DutyCode = null;
            }
        }
        if (FCode.equals("PayPlanCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PayPlanCode = FValue.trim();
            }
            else
            {
                PayPlanCode = null;
            }
        }
        if (FCode.equals("CycPayCalCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CycPayCalCode = FValue.trim();
            }
            else
            {
                CycPayCalCode = null;
            }
        }
        if (FCode.equals("OnePayCalCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OnePayCalCode = FValue.trim();
            }
            else
            {
                OnePayCalCode = null;
            }
        }
        if (FCode.equals("CycPayIntvType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CycPayIntvType = FValue.trim();
            }
            else
            {
                CycPayIntvType = null;
            }
        }
        if (FCode.equals("OnePayIntvType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OnePayIntvType = FValue.trim();
            }
            else
            {
                OnePayIntvType = null;
            }
        }
        if (FCode.equals("LiveGetType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                LiveGetType = FValue.trim();
            }
            else
            {
                LiveGetType = null;
            }
        }
        if (FCode.equals("DeadGetType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DeadGetType = FValue.trim();
            }
            else
            {
                DeadGetType = null;
            }
        }
        if (FCode.equals("OutGetType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OutGetType = FValue.trim();
            }
            else
            {
                OutGetType = null;
            }
        }
        if (FCode.equals("CashValueCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CashValueCode = FValue.trim();
            }
            else
            {
                CashValueCode = null;
            }
        }
        if (FCode.equals("CalCodeType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CalCodeType = FValue.trim();
            }
            else
            {
                CalCodeType = null;
            }
        }
        if (FCode.equals("ZTYearType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ZTYearType = FValue.trim();
            }
            else
            {
                ZTYearType = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LMEdorZT1Schema other = (LMEdorZT1Schema) otherObject;
        return
                RiskCode.equals(other.getRiskCode())
                && DutyCode.equals(other.getDutyCode())
                && PayPlanCode.equals(other.getPayPlanCode())
                && CycPayCalCode.equals(other.getCycPayCalCode())
                && OnePayCalCode.equals(other.getOnePayCalCode())
                && CycPayIntvType.equals(other.getCycPayIntvType())
                && OnePayIntvType.equals(other.getOnePayIntvType())
                && LiveGetType.equals(other.getLiveGetType())
                && DeadGetType.equals(other.getDeadGetType())
                && OutGetType.equals(other.getOutGetType())
                && CashValueCode.equals(other.getCashValueCode())
                && CalCodeType.equals(other.getCalCodeType())
                && ZTYearType.equals(other.getZTYearType());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("RiskCode"))
        {
            return 0;
        }
        if (strFieldName.equals("DutyCode"))
        {
            return 1;
        }
        if (strFieldName.equals("PayPlanCode"))
        {
            return 2;
        }
        if (strFieldName.equals("CycPayCalCode"))
        {
            return 3;
        }
        if (strFieldName.equals("OnePayCalCode"))
        {
            return 4;
        }
        if (strFieldName.equals("CycPayIntvType"))
        {
            return 5;
        }
        if (strFieldName.equals("OnePayIntvType"))
        {
            return 6;
        }
        if (strFieldName.equals("LiveGetType"))
        {
            return 7;
        }
        if (strFieldName.equals("DeadGetType"))
        {
            return 8;
        }
        if (strFieldName.equals("OutGetType"))
        {
            return 9;
        }
        if (strFieldName.equals("CashValueCode"))
        {
            return 10;
        }
        if (strFieldName.equals("CalCodeType"))
        {
            return 11;
        }
        if (strFieldName.equals("ZTYearType"))
        {
            return 12;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "DutyCode";
                break;
            case 2:
                strFieldName = "PayPlanCode";
                break;
            case 3:
                strFieldName = "CycPayCalCode";
                break;
            case 4:
                strFieldName = "OnePayCalCode";
                break;
            case 5:
                strFieldName = "CycPayIntvType";
                break;
            case 6:
                strFieldName = "OnePayIntvType";
                break;
            case 7:
                strFieldName = "LiveGetType";
                break;
            case 8:
                strFieldName = "DeadGetType";
                break;
            case 9:
                strFieldName = "OutGetType";
                break;
            case 10:
                strFieldName = "CashValueCode";
                break;
            case 11:
                strFieldName = "CalCodeType";
                break;
            case 12:
                strFieldName = "ZTYearType";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DutyCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PayPlanCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CycPayCalCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OnePayCalCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CycPayIntvType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OnePayIntvType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("LiveGetType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DeadGetType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OutGetType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CashValueCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CalCodeType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ZTYearType"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
