/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.FIVoucherBnDB;

/*
 * <p>ClassName: FIVoucherBnSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 财务接口最新版
 * @CreateDate：2011-12-13
 */
public class FIVoucherBnSchema implements Schema, Cloneable
{
	// @Field
	/** 版本编号 */
	private String VersionNo;
	/** 凭证编号 */
	private String VoucherID;
	/** 凭证名称 */
	private String VoucherName;
	/** 业务交易编号 */
	private String BusinessID;
	/** 业务交易名称 */
	private String BusinessName;

	public static final int FIELDNUM = 5;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public FIVoucherBnSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[3];
		pk[0] = "VersionNo";
		pk[1] = "VoucherID";
		pk[2] = "BusinessID";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		FIVoucherBnSchema cloned = (FIVoucherBnSchema) super.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getVersionNo()
	{
		return VersionNo;
	}
	public void setVersionNo(String aVersionNo)
	{
		VersionNo = aVersionNo;
	}
	public String getVoucherID()
	{
		return VoucherID;
	}
	public void setVoucherID(String aVoucherID)
	{
		VoucherID = aVoucherID;
	}
	public String getVoucherName()
	{
		return VoucherName;
	}
	public void setVoucherName(String aVoucherName)
	{
		VoucherName = aVoucherName;
	}
	public String getBusinessID()
	{
		return BusinessID;
	}
	public void setBusinessID(String aBusinessID)
	{
		BusinessID = aBusinessID;
	}
	public String getBusinessName()
	{
		return BusinessName;
	}
	public void setBusinessName(String aBusinessName)
	{
		BusinessName = aBusinessName;
	}

	/**
	* 使用另外一个 FIVoucherBnSchema 对象给 Schema 赋值
	* @param: aFIVoucherBnSchema FIVoucherBnSchema
	**/
	public void setSchema(FIVoucherBnSchema aFIVoucherBnSchema)
	{
		this.VersionNo = aFIVoucherBnSchema.getVersionNo();
		this.VoucherID = aFIVoucherBnSchema.getVoucherID();
		this.VoucherName = aFIVoucherBnSchema.getVoucherName();
		this.BusinessID = aFIVoucherBnSchema.getBusinessID();
		this.BusinessName = aFIVoucherBnSchema.getBusinessName();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("VersionNo") == null )
				this.VersionNo = null;
			else
				this.VersionNo = rs.getString("VersionNo").trim();

			if( rs.getString("VoucherID") == null )
				this.VoucherID = null;
			else
				this.VoucherID = rs.getString("VoucherID").trim();

			if( rs.getString("VoucherName") == null )
				this.VoucherName = null;
			else
				this.VoucherName = rs.getString("VoucherName").trim();

			if( rs.getString("BusinessID") == null )
				this.BusinessID = null;
			else
				this.BusinessID = rs.getString("BusinessID").trim();

			if( rs.getString("BusinessName") == null )
				this.BusinessName = null;
			else
				this.BusinessName = rs.getString("BusinessName").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的FIVoucherBn表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIVoucherBnSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public FIVoucherBnSchema getSchema()
	{
		FIVoucherBnSchema aFIVoucherBnSchema = new FIVoucherBnSchema();
		aFIVoucherBnSchema.setSchema(this);
		return aFIVoucherBnSchema;
	}

	public FIVoucherBnDB getDB()
	{
		FIVoucherBnDB aDBOper = new FIVoucherBnDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpFIVoucherBn描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(VersionNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(VoucherID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(VoucherName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusinessID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusinessName));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpFIVoucherBn>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			VersionNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			VoucherID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			VoucherName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			BusinessID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			BusinessName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIVoucherBnSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("VersionNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(VersionNo));
		}
		if (FCode.equals("VoucherID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(VoucherID));
		}
		if (FCode.equals("VoucherName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(VoucherName));
		}
		if (FCode.equals("BusinessID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusinessID));
		}
		if (FCode.equals("BusinessName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusinessName));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(VersionNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(VoucherID);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(VoucherName);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(BusinessID);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(BusinessName);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("VersionNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				VersionNo = FValue.trim();
			}
			else
				VersionNo = null;
		}
		if (FCode.equalsIgnoreCase("VoucherID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				VoucherID = FValue.trim();
			}
			else
				VoucherID = null;
		}
		if (FCode.equalsIgnoreCase("VoucherName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				VoucherName = FValue.trim();
			}
			else
				VoucherName = null;
		}
		if (FCode.equalsIgnoreCase("BusinessID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusinessID = FValue.trim();
			}
			else
				BusinessID = null;
		}
		if (FCode.equalsIgnoreCase("BusinessName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusinessName = FValue.trim();
			}
			else
				BusinessName = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		FIVoucherBnSchema other = (FIVoucherBnSchema)otherObject;
		return
			(VersionNo == null ? other.getVersionNo() == null : VersionNo.equals(other.getVersionNo()))
			&& (VoucherID == null ? other.getVoucherID() == null : VoucherID.equals(other.getVoucherID()))
			&& (VoucherName == null ? other.getVoucherName() == null : VoucherName.equals(other.getVoucherName()))
			&& (BusinessID == null ? other.getBusinessID() == null : BusinessID.equals(other.getBusinessID()))
			&& (BusinessName == null ? other.getBusinessName() == null : BusinessName.equals(other.getBusinessName()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("VersionNo") ) {
			return 0;
		}
		if( strFieldName.equals("VoucherID") ) {
			return 1;
		}
		if( strFieldName.equals("VoucherName") ) {
			return 2;
		}
		if( strFieldName.equals("BusinessID") ) {
			return 3;
		}
		if( strFieldName.equals("BusinessName") ) {
			return 4;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "VersionNo";
				break;
			case 1:
				strFieldName = "VoucherID";
				break;
			case 2:
				strFieldName = "VoucherName";
				break;
			case 3:
				strFieldName = "BusinessID";
				break;
			case 4:
				strFieldName = "BusinessName";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("VersionNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("VoucherID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("VoucherName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BusinessID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BusinessName") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
