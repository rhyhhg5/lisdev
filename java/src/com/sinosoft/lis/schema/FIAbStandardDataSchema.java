/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.FIAbStandardDataDB;

/*
 * <p>ClassName: FIAbStandardDataSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 财务接口最新版
 * @CreateDate：2011-12-13
 */
public class FIAbStandardDataSchema implements Schema, Cloneable
{
	// @Field
	/** 流水号 */
	private String SerialNo;
	/** 号码类型 */
	private String NoType;
	/** 业务流水号码 */
	private String ASerialNo;
	/** 二次处理标记 */
	private String SecondaryFlag;
	/** 二次处理类型 */
	private String SecondaryType;
	/** 业务交易编号 */
	private String BusTypeID;
	/** 费用编码 */
	private String CostID;
	/** 业务大类 */
	private String BusinessType;
	/** 业务类别 */
	private String BusinessDetail;
	/** 费用类型 */
	private String FeeType;
	/** 费用明细 */
	private String FeeDetail;
	/** 业务索引类型 */
	private String IndexCode;
	/** 业务索引号码 */
	private String IndexNo;
	/** 个团标志 */
	private String ListFlag;
	/** 集体合同号码 */
	private String GrpContNo;
	/** 集体保单险种号码 */
	private String GrpPolNo;
	/** 合同号码 */
	private String ContNo;
	/** 保单险种号码 */
	private String PolNo;
	/** 保全批单号 */
	private String EndorsementNo;
	/** 票据号/报盘批次号 */
	private String NotesNo;
	/** 首续年标志 */
	private String FirstYearFlag;
	/** 首续期标志 */
	private String FirstTermFlag;
	/** 红利领取方式 */
	private String BonusType;
	/** 是否异地 */
	private String DifComFlag;
	/** 险种编码 */
	private String RiskCode;
	/** 长短险标志 */
	private String RiskPeriod;
	/** 险种分类 */
	private String RiskType;
	/** 险种分类1 */
	private String RiskType1;
	/** 险种分类2 */
	private String RiskType2;
	/** 成本中心 */
	private String CostCenter;
	/** 中介机构类别 */
	private String ACType;
	/** 代理机构 */
	private String AgentCom;
	/** 代理人组别 */
	private String AgentGroup;
	/** 代理人编码 */
	private String AgentCode;
	/** 客户编号 */
	private String CustomerID;
	/** 供应商号码 */
	private String SupplierNo;
	/** 销售渠道 */
	private String SaleChnl;
	/** 渠道明细 */
	private String SaleChnlDetail;
	/** 管理机构 */
	private String ManageCom;
	/** 对方机构 */
	private String ExecuteCom;
	/** 险种生效日期 */
	private Date CValiDate;
	/** 交费间隔 */
	private int PayIntv;
	/** 原交至日期 */
	private Date LastPayToDate;
	/** 现交至日期 */
	private Date CurPayToDate;
	/** 保单年度 */
	private int PolYear;
	/** 保险年期 */
	private int Years;
	/** 保费收入类型 */
	private String PremiumType;
	/** 第几次交费 */
	private int PayCount;
	/** 收付费方式 */
	private String PayMode;
	/** 银行编码 */
	private String BankCode;
	/** 账户名 */
	private String AccName;
	/** 银行帐号 */
	private String BankAccNo;
	/** 费用金额 */
	private double SumActuMoney;
	/** 月结日期 */
	private Date MothDate;
	/** 业务日期 */
	private Date BusinessDate;
	/** 记账日期 */
	private Date AccountDate;
	/** 现金流量 */
	private String CashFlowNo;
	/** 计费首年 */
	private String FirstYear;
	/** 市场类型 */
	private String MarketType;
	/** 出纳类型 */
	private String OperationType;
	/** 预算信息 */
	private String Budget;
	/** 币别 */
	private String Currency;
	/** 字符数据信息01 */
	private String StringInfo01;
	/** 字符数据信息02 */
	private String StringInfo02;
	/** 字符数据信息03 */
	private String StringInfo03;
	/** 日期数据信息01 */
	private Date DateInfo01;
	/** 日期数据信息02 */
	private Date DateInfo02;
	/** 二次处理关联号 */
	private String RelatedNo;
	/** 执行事件号 */
	private String EventNo;
	/** 执行状态 */
	private String State;
	/** 读取状态 */
	private String ReadState;
	/** 校检标记 */
	private String CheckFlag;
	/** 操作员 */
	private String Operator;
	/** 创建日期 */
	private Date MakeDate;
	/** 创建时间 */
	private String MakeTime;

	public static final int FIELDNUM = 75;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public FIAbStandardDataSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "SerialNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		FIAbStandardDataSchema cloned = (FIAbStandardDataSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getSerialNo()
	{
		return SerialNo;
	}
	public void setSerialNo(String aSerialNo)
	{
		SerialNo = aSerialNo;
	}
	public String getNoType()
	{
		return NoType;
	}
	public void setNoType(String aNoType)
	{
		NoType = aNoType;
	}
	public String getASerialNo()
	{
		return ASerialNo;
	}
	public void setASerialNo(String aASerialNo)
	{
		ASerialNo = aASerialNo;
	}
	public String getSecondaryFlag()
	{
		return SecondaryFlag;
	}
	public void setSecondaryFlag(String aSecondaryFlag)
	{
		SecondaryFlag = aSecondaryFlag;
	}
	public String getSecondaryType()
	{
		return SecondaryType;
	}
	public void setSecondaryType(String aSecondaryType)
	{
		SecondaryType = aSecondaryType;
	}
	public String getBusTypeID()
	{
		return BusTypeID;
	}
	public void setBusTypeID(String aBusTypeID)
	{
		BusTypeID = aBusTypeID;
	}
	public String getCostID()
	{
		return CostID;
	}
	public void setCostID(String aCostID)
	{
		CostID = aCostID;
	}
	public String getBusinessType()
	{
		return BusinessType;
	}
	public void setBusinessType(String aBusinessType)
	{
		BusinessType = aBusinessType;
	}
	public String getBusinessDetail()
	{
		return BusinessDetail;
	}
	public void setBusinessDetail(String aBusinessDetail)
	{
		BusinessDetail = aBusinessDetail;
	}
	public String getFeeType()
	{
		return FeeType;
	}
	public void setFeeType(String aFeeType)
	{
		FeeType = aFeeType;
	}
	public String getFeeDetail()
	{
		return FeeDetail;
	}
	public void setFeeDetail(String aFeeDetail)
	{
		FeeDetail = aFeeDetail;
	}
	public String getIndexCode()
	{
		return IndexCode;
	}
	public void setIndexCode(String aIndexCode)
	{
		IndexCode = aIndexCode;
	}
	public String getIndexNo()
	{
		return IndexNo;
	}
	public void setIndexNo(String aIndexNo)
	{
		IndexNo = aIndexNo;
	}
	public String getListFlag()
	{
		return ListFlag;
	}
	public void setListFlag(String aListFlag)
	{
		ListFlag = aListFlag;
	}
	public String getGrpContNo()
	{
		return GrpContNo;
	}
	public void setGrpContNo(String aGrpContNo)
	{
		GrpContNo = aGrpContNo;
	}
	public String getGrpPolNo()
	{
		return GrpPolNo;
	}
	public void setGrpPolNo(String aGrpPolNo)
	{
		GrpPolNo = aGrpPolNo;
	}
	public String getContNo()
	{
		return ContNo;
	}
	public void setContNo(String aContNo)
	{
		ContNo = aContNo;
	}
	public String getPolNo()
	{
		return PolNo;
	}
	public void setPolNo(String aPolNo)
	{
		PolNo = aPolNo;
	}
	public String getEndorsementNo()
	{
		return EndorsementNo;
	}
	public void setEndorsementNo(String aEndorsementNo)
	{
		EndorsementNo = aEndorsementNo;
	}
	public String getNotesNo()
	{
		return NotesNo;
	}
	public void setNotesNo(String aNotesNo)
	{
		NotesNo = aNotesNo;
	}
	public String getFirstYearFlag()
	{
		return FirstYearFlag;
	}
	public void setFirstYearFlag(String aFirstYearFlag)
	{
		FirstYearFlag = aFirstYearFlag;
	}
	public String getFirstTermFlag()
	{
		return FirstTermFlag;
	}
	public void setFirstTermFlag(String aFirstTermFlag)
	{
		FirstTermFlag = aFirstTermFlag;
	}
	public String getBonusType()
	{
		return BonusType;
	}
	public void setBonusType(String aBonusType)
	{
		BonusType = aBonusType;
	}
	public String getDifComFlag()
	{
		return DifComFlag;
	}
	public void setDifComFlag(String aDifComFlag)
	{
		DifComFlag = aDifComFlag;
	}
	public String getRiskCode()
	{
		return RiskCode;
	}
	public void setRiskCode(String aRiskCode)
	{
		RiskCode = aRiskCode;
	}
	public String getRiskPeriod()
	{
		return RiskPeriod;
	}
	public void setRiskPeriod(String aRiskPeriod)
	{
		RiskPeriod = aRiskPeriod;
	}
	public String getRiskType()
	{
		return RiskType;
	}
	public void setRiskType(String aRiskType)
	{
		RiskType = aRiskType;
	}
	public String getRiskType1()
	{
		return RiskType1;
	}
	public void setRiskType1(String aRiskType1)
	{
		RiskType1 = aRiskType1;
	}
	public String getRiskType2()
	{
		return RiskType2;
	}
	public void setRiskType2(String aRiskType2)
	{
		RiskType2 = aRiskType2;
	}
	public String getCostCenter()
	{
		return CostCenter;
	}
	public void setCostCenter(String aCostCenter)
	{
		CostCenter = aCostCenter;
	}
	public String getACType()
	{
		return ACType;
	}
	public void setACType(String aACType)
	{
		ACType = aACType;
	}
	public String getAgentCom()
	{
		return AgentCom;
	}
	public void setAgentCom(String aAgentCom)
	{
		AgentCom = aAgentCom;
	}
	public String getAgentGroup()
	{
		return AgentGroup;
	}
	public void setAgentGroup(String aAgentGroup)
	{
		AgentGroup = aAgentGroup;
	}
	public String getAgentCode()
	{
		return AgentCode;
	}
	public void setAgentCode(String aAgentCode)
	{
		AgentCode = aAgentCode;
	}
	public String getCustomerID()
	{
		return CustomerID;
	}
	public void setCustomerID(String aCustomerID)
	{
		CustomerID = aCustomerID;
	}
	public String getSupplierNo()
	{
		return SupplierNo;
	}
	public void setSupplierNo(String aSupplierNo)
	{
		SupplierNo = aSupplierNo;
	}
	public String getSaleChnl()
	{
		return SaleChnl;
	}
	public void setSaleChnl(String aSaleChnl)
	{
		SaleChnl = aSaleChnl;
	}
	public String getSaleChnlDetail()
	{
		return SaleChnlDetail;
	}
	public void setSaleChnlDetail(String aSaleChnlDetail)
	{
		SaleChnlDetail = aSaleChnlDetail;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getExecuteCom()
	{
		return ExecuteCom;
	}
	public void setExecuteCom(String aExecuteCom)
	{
		ExecuteCom = aExecuteCom;
	}
	public String getCValiDate()
	{
		if( CValiDate != null )
			return fDate.getString(CValiDate);
		else
			return null;
	}
	public void setCValiDate(Date aCValiDate)
	{
		CValiDate = aCValiDate;
	}
	public void setCValiDate(String aCValiDate)
	{
		if (aCValiDate != null && !aCValiDate.equals("") )
		{
			CValiDate = fDate.getDate( aCValiDate );
		}
		else
			CValiDate = null;
	}

	public int getPayIntv()
	{
		return PayIntv;
	}
	public void setPayIntv(int aPayIntv)
	{
		PayIntv = aPayIntv;
	}
	public void setPayIntv(String aPayIntv)
	{
		if (aPayIntv != null && !aPayIntv.equals(""))
		{
			Integer tInteger = new Integer(aPayIntv);
			int i = tInteger.intValue();
			PayIntv = i;
		}
	}

	public String getLastPayToDate()
	{
		if( LastPayToDate != null )
			return fDate.getString(LastPayToDate);
		else
			return null;
	}
	public void setLastPayToDate(Date aLastPayToDate)
	{
		LastPayToDate = aLastPayToDate;
	}
	public void setLastPayToDate(String aLastPayToDate)
	{
		if (aLastPayToDate != null && !aLastPayToDate.equals("") )
		{
			LastPayToDate = fDate.getDate( aLastPayToDate );
		}
		else
			LastPayToDate = null;
	}

	public String getCurPayToDate()
	{
		if( CurPayToDate != null )
			return fDate.getString(CurPayToDate);
		else
			return null;
	}
	public void setCurPayToDate(Date aCurPayToDate)
	{
		CurPayToDate = aCurPayToDate;
	}
	public void setCurPayToDate(String aCurPayToDate)
	{
		if (aCurPayToDate != null && !aCurPayToDate.equals("") )
		{
			CurPayToDate = fDate.getDate( aCurPayToDate );
		}
		else
			CurPayToDate = null;
	}

	public int getPolYear()
	{
		return PolYear;
	}
	public void setPolYear(int aPolYear)
	{
		PolYear = aPolYear;
	}
	public void setPolYear(String aPolYear)
	{
		if (aPolYear != null && !aPolYear.equals(""))
		{
			Integer tInteger = new Integer(aPolYear);
			int i = tInteger.intValue();
			PolYear = i;
		}
	}

	public int getYears()
	{
		return Years;
	}
	public void setYears(int aYears)
	{
		Years = aYears;
	}
	public void setYears(String aYears)
	{
		if (aYears != null && !aYears.equals(""))
		{
			Integer tInteger = new Integer(aYears);
			int i = tInteger.intValue();
			Years = i;
		}
	}

	public String getPremiumType()
	{
		return PremiumType;
	}
	public void setPremiumType(String aPremiumType)
	{
		PremiumType = aPremiumType;
	}
	public int getPayCount()
	{
		return PayCount;
	}
	public void setPayCount(int aPayCount)
	{
		PayCount = aPayCount;
	}
	public void setPayCount(String aPayCount)
	{
		if (aPayCount != null && !aPayCount.equals(""))
		{
			Integer tInteger = new Integer(aPayCount);
			int i = tInteger.intValue();
			PayCount = i;
		}
	}

	public String getPayMode()
	{
		return PayMode;
	}
	public void setPayMode(String aPayMode)
	{
		PayMode = aPayMode;
	}
	public String getBankCode()
	{
		return BankCode;
	}
	public void setBankCode(String aBankCode)
	{
		BankCode = aBankCode;
	}
	public String getAccName()
	{
		return AccName;
	}
	public void setAccName(String aAccName)
	{
		AccName = aAccName;
	}
	public String getBankAccNo()
	{
		return BankAccNo;
	}
	public void setBankAccNo(String aBankAccNo)
	{
		BankAccNo = aBankAccNo;
	}
	public double getSumActuMoney()
	{
		return SumActuMoney;
	}
	public void setSumActuMoney(double aSumActuMoney)
	{
		SumActuMoney = Arith.round(aSumActuMoney,2);
	}
	public void setSumActuMoney(String aSumActuMoney)
	{
		if (aSumActuMoney != null && !aSumActuMoney.equals(""))
		{
			Double tDouble = new Double(aSumActuMoney);
			double d = tDouble.doubleValue();
                SumActuMoney = Arith.round(d,2);
		}
	}

	public String getMothDate()
	{
		if( MothDate != null )
			return fDate.getString(MothDate);
		else
			return null;
	}
	public void setMothDate(Date aMothDate)
	{
		MothDate = aMothDate;
	}
	public void setMothDate(String aMothDate)
	{
		if (aMothDate != null && !aMothDate.equals("") )
		{
			MothDate = fDate.getDate( aMothDate );
		}
		else
			MothDate = null;
	}

	public String getBusinessDate()
	{
		if( BusinessDate != null )
			return fDate.getString(BusinessDate);
		else
			return null;
	}
	public void setBusinessDate(Date aBusinessDate)
	{
		BusinessDate = aBusinessDate;
	}
	public void setBusinessDate(String aBusinessDate)
	{
		if (aBusinessDate != null && !aBusinessDate.equals("") )
		{
			BusinessDate = fDate.getDate( aBusinessDate );
		}
		else
			BusinessDate = null;
	}

	public String getAccountDate()
	{
		if( AccountDate != null )
			return fDate.getString(AccountDate);
		else
			return null;
	}
	public void setAccountDate(Date aAccountDate)
	{
		AccountDate = aAccountDate;
	}
	public void setAccountDate(String aAccountDate)
	{
		if (aAccountDate != null && !aAccountDate.equals("") )
		{
			AccountDate = fDate.getDate( aAccountDate );
		}
		else
			AccountDate = null;
	}

	public String getCashFlowNo()
	{
		return CashFlowNo;
	}
	public void setCashFlowNo(String aCashFlowNo)
	{
		CashFlowNo = aCashFlowNo;
	}
	public String getFirstYear()
	{
		return FirstYear;
	}
	public void setFirstYear(String aFirstYear)
	{
		FirstYear = aFirstYear;
	}
	public String getMarketType()
	{
		return MarketType;
	}
	public void setMarketType(String aMarketType)
	{
		MarketType = aMarketType;
	}
	public String getOperationType()
	{
		return OperationType;
	}
	public void setOperationType(String aOperationType)
	{
		OperationType = aOperationType;
	}
	public String getBudget()
	{
		return Budget;
	}
	public void setBudget(String aBudget)
	{
		Budget = aBudget;
	}
	public String getCurrency()
	{
		return Currency;
	}
	public void setCurrency(String aCurrency)
	{
		Currency = aCurrency;
	}
	public String getStringInfo01()
	{
		return StringInfo01;
	}
	public void setStringInfo01(String aStringInfo01)
	{
		StringInfo01 = aStringInfo01;
	}
	public String getStringInfo02()
	{
		return StringInfo02;
	}
	public void setStringInfo02(String aStringInfo02)
	{
		StringInfo02 = aStringInfo02;
	}
	public String getStringInfo03()
	{
		return StringInfo03;
	}
	public void setStringInfo03(String aStringInfo03)
	{
		StringInfo03 = aStringInfo03;
	}
	public String getDateInfo01()
	{
		if( DateInfo01 != null )
			return fDate.getString(DateInfo01);
		else
			return null;
	}
	public void setDateInfo01(Date aDateInfo01)
	{
		DateInfo01 = aDateInfo01;
	}
	public void setDateInfo01(String aDateInfo01)
	{
		if (aDateInfo01 != null && !aDateInfo01.equals("") )
		{
			DateInfo01 = fDate.getDate( aDateInfo01 );
		}
		else
			DateInfo01 = null;
	}

	public String getDateInfo02()
	{
		if( DateInfo02 != null )
			return fDate.getString(DateInfo02);
		else
			return null;
	}
	public void setDateInfo02(Date aDateInfo02)
	{
		DateInfo02 = aDateInfo02;
	}
	public void setDateInfo02(String aDateInfo02)
	{
		if (aDateInfo02 != null && !aDateInfo02.equals("") )
		{
			DateInfo02 = fDate.getDate( aDateInfo02 );
		}
		else
			DateInfo02 = null;
	}

	public String getRelatedNo()
	{
		return RelatedNo;
	}
	public void setRelatedNo(String aRelatedNo)
	{
		RelatedNo = aRelatedNo;
	}
	public String getEventNo()
	{
		return EventNo;
	}
	public void setEventNo(String aEventNo)
	{
		EventNo = aEventNo;
	}
	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}
	public String getReadState()
	{
		return ReadState;
	}
	public void setReadState(String aReadState)
	{
		ReadState = aReadState;
	}
	public String getCheckFlag()
	{
		return CheckFlag;
	}
	public void setCheckFlag(String aCheckFlag)
	{
		CheckFlag = aCheckFlag;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}

	/**
	* 使用另外一个 FIAbStandardDataSchema 对象给 Schema 赋值
	* @param: aFIAbStandardDataSchema FIAbStandardDataSchema
	**/
	public void setSchema(FIAbStandardDataSchema aFIAbStandardDataSchema)
	{
		this.SerialNo = aFIAbStandardDataSchema.getSerialNo();
		this.NoType = aFIAbStandardDataSchema.getNoType();
		this.ASerialNo = aFIAbStandardDataSchema.getASerialNo();
		this.SecondaryFlag = aFIAbStandardDataSchema.getSecondaryFlag();
		this.SecondaryType = aFIAbStandardDataSchema.getSecondaryType();
		this.BusTypeID = aFIAbStandardDataSchema.getBusTypeID();
		this.CostID = aFIAbStandardDataSchema.getCostID();
		this.BusinessType = aFIAbStandardDataSchema.getBusinessType();
		this.BusinessDetail = aFIAbStandardDataSchema.getBusinessDetail();
		this.FeeType = aFIAbStandardDataSchema.getFeeType();
		this.FeeDetail = aFIAbStandardDataSchema.getFeeDetail();
		this.IndexCode = aFIAbStandardDataSchema.getIndexCode();
		this.IndexNo = aFIAbStandardDataSchema.getIndexNo();
		this.ListFlag = aFIAbStandardDataSchema.getListFlag();
		this.GrpContNo = aFIAbStandardDataSchema.getGrpContNo();
		this.GrpPolNo = aFIAbStandardDataSchema.getGrpPolNo();
		this.ContNo = aFIAbStandardDataSchema.getContNo();
		this.PolNo = aFIAbStandardDataSchema.getPolNo();
		this.EndorsementNo = aFIAbStandardDataSchema.getEndorsementNo();
		this.NotesNo = aFIAbStandardDataSchema.getNotesNo();
		this.FirstYearFlag = aFIAbStandardDataSchema.getFirstYearFlag();
		this.FirstTermFlag = aFIAbStandardDataSchema.getFirstTermFlag();
		this.BonusType = aFIAbStandardDataSchema.getBonusType();
		this.DifComFlag = aFIAbStandardDataSchema.getDifComFlag();
		this.RiskCode = aFIAbStandardDataSchema.getRiskCode();
		this.RiskPeriod = aFIAbStandardDataSchema.getRiskPeriod();
		this.RiskType = aFIAbStandardDataSchema.getRiskType();
		this.RiskType1 = aFIAbStandardDataSchema.getRiskType1();
		this.RiskType2 = aFIAbStandardDataSchema.getRiskType2();
		this.CostCenter = aFIAbStandardDataSchema.getCostCenter();
		this.ACType = aFIAbStandardDataSchema.getACType();
		this.AgentCom = aFIAbStandardDataSchema.getAgentCom();
		this.AgentGroup = aFIAbStandardDataSchema.getAgentGroup();
		this.AgentCode = aFIAbStandardDataSchema.getAgentCode();
		this.CustomerID = aFIAbStandardDataSchema.getCustomerID();
		this.SupplierNo = aFIAbStandardDataSchema.getSupplierNo();
		this.SaleChnl = aFIAbStandardDataSchema.getSaleChnl();
		this.SaleChnlDetail = aFIAbStandardDataSchema.getSaleChnlDetail();
		this.ManageCom = aFIAbStandardDataSchema.getManageCom();
		this.ExecuteCom = aFIAbStandardDataSchema.getExecuteCom();
		this.CValiDate = fDate.getDate( aFIAbStandardDataSchema.getCValiDate());
		this.PayIntv = aFIAbStandardDataSchema.getPayIntv();
		this.LastPayToDate = fDate.getDate( aFIAbStandardDataSchema.getLastPayToDate());
		this.CurPayToDate = fDate.getDate( aFIAbStandardDataSchema.getCurPayToDate());
		this.PolYear = aFIAbStandardDataSchema.getPolYear();
		this.Years = aFIAbStandardDataSchema.getYears();
		this.PremiumType = aFIAbStandardDataSchema.getPremiumType();
		this.PayCount = aFIAbStandardDataSchema.getPayCount();
		this.PayMode = aFIAbStandardDataSchema.getPayMode();
		this.BankCode = aFIAbStandardDataSchema.getBankCode();
		this.AccName = aFIAbStandardDataSchema.getAccName();
		this.BankAccNo = aFIAbStandardDataSchema.getBankAccNo();
		this.SumActuMoney = aFIAbStandardDataSchema.getSumActuMoney();
		this.MothDate = fDate.getDate( aFIAbStandardDataSchema.getMothDate());
		this.BusinessDate = fDate.getDate( aFIAbStandardDataSchema.getBusinessDate());
		this.AccountDate = fDate.getDate( aFIAbStandardDataSchema.getAccountDate());
		this.CashFlowNo = aFIAbStandardDataSchema.getCashFlowNo();
		this.FirstYear = aFIAbStandardDataSchema.getFirstYear();
		this.MarketType = aFIAbStandardDataSchema.getMarketType();
		this.OperationType = aFIAbStandardDataSchema.getOperationType();
		this.Budget = aFIAbStandardDataSchema.getBudget();
		this.Currency = aFIAbStandardDataSchema.getCurrency();
		this.StringInfo01 = aFIAbStandardDataSchema.getStringInfo01();
		this.StringInfo02 = aFIAbStandardDataSchema.getStringInfo02();
		this.StringInfo03 = aFIAbStandardDataSchema.getStringInfo03();
		this.DateInfo01 = fDate.getDate( aFIAbStandardDataSchema.getDateInfo01());
		this.DateInfo02 = fDate.getDate( aFIAbStandardDataSchema.getDateInfo02());
		this.RelatedNo = aFIAbStandardDataSchema.getRelatedNo();
		this.EventNo = aFIAbStandardDataSchema.getEventNo();
		this.State = aFIAbStandardDataSchema.getState();
		this.ReadState = aFIAbStandardDataSchema.getReadState();
		this.CheckFlag = aFIAbStandardDataSchema.getCheckFlag();
		this.Operator = aFIAbStandardDataSchema.getOperator();
		this.MakeDate = fDate.getDate( aFIAbStandardDataSchema.getMakeDate());
		this.MakeTime = aFIAbStandardDataSchema.getMakeTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("SerialNo") == null )
				this.SerialNo = null;
			else
				this.SerialNo = rs.getString("SerialNo").trim();

			if( rs.getString("NoType") == null )
				this.NoType = null;
			else
				this.NoType = rs.getString("NoType").trim();

			if( rs.getString("ASerialNo") == null )
				this.ASerialNo = null;
			else
				this.ASerialNo = rs.getString("ASerialNo").trim();

			if( rs.getString("SecondaryFlag") == null )
				this.SecondaryFlag = null;
			else
				this.SecondaryFlag = rs.getString("SecondaryFlag").trim();

			if( rs.getString("SecondaryType") == null )
				this.SecondaryType = null;
			else
				this.SecondaryType = rs.getString("SecondaryType").trim();

			if( rs.getString("BusTypeID") == null )
				this.BusTypeID = null;
			else
				this.BusTypeID = rs.getString("BusTypeID").trim();

			if( rs.getString("CostID") == null )
				this.CostID = null;
			else
				this.CostID = rs.getString("CostID").trim();

			if( rs.getString("BusinessType") == null )
				this.BusinessType = null;
			else
				this.BusinessType = rs.getString("BusinessType").trim();

			if( rs.getString("BusinessDetail") == null )
				this.BusinessDetail = null;
			else
				this.BusinessDetail = rs.getString("BusinessDetail").trim();

			if( rs.getString("FeeType") == null )
				this.FeeType = null;
			else
				this.FeeType = rs.getString("FeeType").trim();

			if( rs.getString("FeeDetail") == null )
				this.FeeDetail = null;
			else
				this.FeeDetail = rs.getString("FeeDetail").trim();

			if( rs.getString("IndexCode") == null )
				this.IndexCode = null;
			else
				this.IndexCode = rs.getString("IndexCode").trim();

			if( rs.getString("IndexNo") == null )
				this.IndexNo = null;
			else
				this.IndexNo = rs.getString("IndexNo").trim();

			if( rs.getString("ListFlag") == null )
				this.ListFlag = null;
			else
				this.ListFlag = rs.getString("ListFlag").trim();

			if( rs.getString("GrpContNo") == null )
				this.GrpContNo = null;
			else
				this.GrpContNo = rs.getString("GrpContNo").trim();

			if( rs.getString("GrpPolNo") == null )
				this.GrpPolNo = null;
			else
				this.GrpPolNo = rs.getString("GrpPolNo").trim();

			if( rs.getString("ContNo") == null )
				this.ContNo = null;
			else
				this.ContNo = rs.getString("ContNo").trim();

			if( rs.getString("PolNo") == null )
				this.PolNo = null;
			else
				this.PolNo = rs.getString("PolNo").trim();

			if( rs.getString("EndorsementNo") == null )
				this.EndorsementNo = null;
			else
				this.EndorsementNo = rs.getString("EndorsementNo").trim();

			if( rs.getString("NotesNo") == null )
				this.NotesNo = null;
			else
				this.NotesNo = rs.getString("NotesNo").trim();

			if( rs.getString("FirstYearFlag") == null )
				this.FirstYearFlag = null;
			else
				this.FirstYearFlag = rs.getString("FirstYearFlag").trim();

			if( rs.getString("FirstTermFlag") == null )
				this.FirstTermFlag = null;
			else
				this.FirstTermFlag = rs.getString("FirstTermFlag").trim();

			if( rs.getString("BonusType") == null )
				this.BonusType = null;
			else
				this.BonusType = rs.getString("BonusType").trim();

			if( rs.getString("DifComFlag") == null )
				this.DifComFlag = null;
			else
				this.DifComFlag = rs.getString("DifComFlag").trim();

			if( rs.getString("RiskCode") == null )
				this.RiskCode = null;
			else
				this.RiskCode = rs.getString("RiskCode").trim();

			if( rs.getString("RiskPeriod") == null )
				this.RiskPeriod = null;
			else
				this.RiskPeriod = rs.getString("RiskPeriod").trim();

			if( rs.getString("RiskType") == null )
				this.RiskType = null;
			else
				this.RiskType = rs.getString("RiskType").trim();

			if( rs.getString("RiskType1") == null )
				this.RiskType1 = null;
			else
				this.RiskType1 = rs.getString("RiskType1").trim();

			if( rs.getString("RiskType2") == null )
				this.RiskType2 = null;
			else
				this.RiskType2 = rs.getString("RiskType2").trim();

			if( rs.getString("CostCenter") == null )
				this.CostCenter = null;
			else
				this.CostCenter = rs.getString("CostCenter").trim();

			if( rs.getString("ACType") == null )
				this.ACType = null;
			else
				this.ACType = rs.getString("ACType").trim();

			if( rs.getString("AgentCom") == null )
				this.AgentCom = null;
			else
				this.AgentCom = rs.getString("AgentCom").trim();

			if( rs.getString("AgentGroup") == null )
				this.AgentGroup = null;
			else
				this.AgentGroup = rs.getString("AgentGroup").trim();

			if( rs.getString("AgentCode") == null )
				this.AgentCode = null;
			else
				this.AgentCode = rs.getString("AgentCode").trim();

			if( rs.getString("CustomerID") == null )
				this.CustomerID = null;
			else
				this.CustomerID = rs.getString("CustomerID").trim();

			if( rs.getString("SupplierNo") == null )
				this.SupplierNo = null;
			else
				this.SupplierNo = rs.getString("SupplierNo").trim();

			if( rs.getString("SaleChnl") == null )
				this.SaleChnl = null;
			else
				this.SaleChnl = rs.getString("SaleChnl").trim();

			if( rs.getString("SaleChnlDetail") == null )
				this.SaleChnlDetail = null;
			else
				this.SaleChnlDetail = rs.getString("SaleChnlDetail").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("ExecuteCom") == null )
				this.ExecuteCom = null;
			else
				this.ExecuteCom = rs.getString("ExecuteCom").trim();

			this.CValiDate = rs.getDate("CValiDate");
			this.PayIntv = rs.getInt("PayIntv");
			this.LastPayToDate = rs.getDate("LastPayToDate");
			this.CurPayToDate = rs.getDate("CurPayToDate");
			this.PolYear = rs.getInt("PolYear");
			this.Years = rs.getInt("Years");
			if( rs.getString("PremiumType") == null )
				this.PremiumType = null;
			else
				this.PremiumType = rs.getString("PremiumType").trim();

			this.PayCount = rs.getInt("PayCount");
			if( rs.getString("PayMode") == null )
				this.PayMode = null;
			else
				this.PayMode = rs.getString("PayMode").trim();

			if( rs.getString("BankCode") == null )
				this.BankCode = null;
			else
				this.BankCode = rs.getString("BankCode").trim();

			if( rs.getString("AccName") == null )
				this.AccName = null;
			else
				this.AccName = rs.getString("AccName").trim();

			if( rs.getString("BankAccNo") == null )
				this.BankAccNo = null;
			else
				this.BankAccNo = rs.getString("BankAccNo").trim();

			this.SumActuMoney = rs.getDouble("SumActuMoney");
			this.MothDate = rs.getDate("MothDate");
			this.BusinessDate = rs.getDate("BusinessDate");
			this.AccountDate = rs.getDate("AccountDate");
			if( rs.getString("CashFlowNo") == null )
				this.CashFlowNo = null;
			else
				this.CashFlowNo = rs.getString("CashFlowNo").trim();

			if( rs.getString("FirstYear") == null )
				this.FirstYear = null;
			else
				this.FirstYear = rs.getString("FirstYear").trim();

			if( rs.getString("MarketType") == null )
				this.MarketType = null;
			else
				this.MarketType = rs.getString("MarketType").trim();

			if( rs.getString("OperationType") == null )
				this.OperationType = null;
			else
				this.OperationType = rs.getString("OperationType").trim();

			if( rs.getString("Budget") == null )
				this.Budget = null;
			else
				this.Budget = rs.getString("Budget").trim();

			if( rs.getString("Currency") == null )
				this.Currency = null;
			else
				this.Currency = rs.getString("Currency").trim();

			if( rs.getString("StringInfo01") == null )
				this.StringInfo01 = null;
			else
				this.StringInfo01 = rs.getString("StringInfo01").trim();

			if( rs.getString("StringInfo02") == null )
				this.StringInfo02 = null;
			else
				this.StringInfo02 = rs.getString("StringInfo02").trim();

			if( rs.getString("StringInfo03") == null )
				this.StringInfo03 = null;
			else
				this.StringInfo03 = rs.getString("StringInfo03").trim();

			this.DateInfo01 = rs.getDate("DateInfo01");
			this.DateInfo02 = rs.getDate("DateInfo02");
			if( rs.getString("RelatedNo") == null )
				this.RelatedNo = null;
			else
				this.RelatedNo = rs.getString("RelatedNo").trim();

			if( rs.getString("EventNo") == null )
				this.EventNo = null;
			else
				this.EventNo = rs.getString("EventNo").trim();

			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			if( rs.getString("ReadState") == null )
				this.ReadState = null;
			else
				this.ReadState = rs.getString("ReadState").trim();

			if( rs.getString("CheckFlag") == null )
				this.CheckFlag = null;
			else
				this.CheckFlag = rs.getString("CheckFlag").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的FIAbStandardData表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIAbStandardDataSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public FIAbStandardDataSchema getSchema()
	{
		FIAbStandardDataSchema aFIAbStandardDataSchema = new FIAbStandardDataSchema();
		aFIAbStandardDataSchema.setSchema(this);
		return aFIAbStandardDataSchema;
	}

	public FIAbStandardDataDB getDB()
	{
		FIAbStandardDataDB aDBOper = new FIAbStandardDataDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpFIAbStandardData描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(SerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(NoType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ASerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SecondaryFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SecondaryType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusTypeID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CostID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusinessType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusinessDetail)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FeeType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FeeDetail)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IndexCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IndexNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ListFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpPolNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PolNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EndorsementNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(NotesNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FirstYearFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FirstTermFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BonusType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DifComFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskPeriod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskType1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskType2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CostCenter)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ACType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentGroup)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SupplierNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SaleChnl)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SaleChnlDetail)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ExecuteCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CValiDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PayIntv));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( LastPayToDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CurPayToDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PolYear));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Years));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PremiumType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PayCount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayMode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SumActuMoney));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MothDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( BusinessDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( AccountDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CashFlowNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FirstYear)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MarketType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OperationType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Budget)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Currency)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StringInfo01)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StringInfo02)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StringInfo03)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( DateInfo01 ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( DateInfo02 ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RelatedNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EventNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReadState)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CheckFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpFIAbStandardData>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			NoType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			ASerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			SecondaryFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			SecondaryType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			BusTypeID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			CostID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			BusinessType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			BusinessDetail = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			FeeType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			FeeDetail = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			IndexCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			IndexNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			ListFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			GrpPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			EndorsementNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			NotesNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			FirstYearFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			FirstTermFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			BonusType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			DifComFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			RiskPeriod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			RiskType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			RiskType1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			RiskType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			CostCenter = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			ACType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			AgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
			AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			CustomerID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			SupplierNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
			SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
			SaleChnlDetail = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
			ExecuteCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
			CValiDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41,SysConst.PACKAGESPILTER));
			PayIntv= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,42,SysConst.PACKAGESPILTER))).intValue();
			LastPayToDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43,SysConst.PACKAGESPILTER));
			CurPayToDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44,SysConst.PACKAGESPILTER));
			PolYear= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,45,SysConst.PACKAGESPILTER))).intValue();
			Years= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,46,SysConst.PACKAGESPILTER))).intValue();
			PremiumType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 47, SysConst.PACKAGESPILTER );
			PayCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,48,SysConst.PACKAGESPILTER))).intValue();
			PayMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 49, SysConst.PACKAGESPILTER );
			BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 50, SysConst.PACKAGESPILTER );
			AccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 51, SysConst.PACKAGESPILTER );
			BankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 52, SysConst.PACKAGESPILTER );
			SumActuMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,53,SysConst.PACKAGESPILTER))).doubleValue();
			MothDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 54,SysConst.PACKAGESPILTER));
			BusinessDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 55,SysConst.PACKAGESPILTER));
			AccountDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 56,SysConst.PACKAGESPILTER));
			CashFlowNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 57, SysConst.PACKAGESPILTER );
			FirstYear = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 58, SysConst.PACKAGESPILTER );
			MarketType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 59, SysConst.PACKAGESPILTER );
			OperationType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 60, SysConst.PACKAGESPILTER );
			Budget = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 61, SysConst.PACKAGESPILTER );
			Currency = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 62, SysConst.PACKAGESPILTER );
			StringInfo01 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 63, SysConst.PACKAGESPILTER );
			StringInfo02 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 64, SysConst.PACKAGESPILTER );
			StringInfo03 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 65, SysConst.PACKAGESPILTER );
			DateInfo01 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 66,SysConst.PACKAGESPILTER));
			DateInfo02 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 67,SysConst.PACKAGESPILTER));
			RelatedNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 68, SysConst.PACKAGESPILTER );
			EventNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 69, SysConst.PACKAGESPILTER );
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 70, SysConst.PACKAGESPILTER );
			ReadState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 71, SysConst.PACKAGESPILTER );
			CheckFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 72, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 73, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 74,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 75, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIAbStandardDataSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("SerialNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
		}
		if (FCode.equals("NoType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(NoType));
		}
		if (FCode.equals("ASerialNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ASerialNo));
		}
		if (FCode.equals("SecondaryFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SecondaryFlag));
		}
		if (FCode.equals("SecondaryType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SecondaryType));
		}
		if (FCode.equals("BusTypeID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusTypeID));
		}
		if (FCode.equals("CostID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CostID));
		}
		if (FCode.equals("BusinessType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusinessType));
		}
		if (FCode.equals("BusinessDetail"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusinessDetail));
		}
		if (FCode.equals("FeeType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FeeType));
		}
		if (FCode.equals("FeeDetail"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FeeDetail));
		}
		if (FCode.equals("IndexCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IndexCode));
		}
		if (FCode.equals("IndexNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IndexNo));
		}
		if (FCode.equals("ListFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ListFlag));
		}
		if (FCode.equals("GrpContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
		}
		if (FCode.equals("GrpPolNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpPolNo));
		}
		if (FCode.equals("ContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
		}
		if (FCode.equals("PolNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
		}
		if (FCode.equals("EndorsementNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EndorsementNo));
		}
		if (FCode.equals("NotesNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(NotesNo));
		}
		if (FCode.equals("FirstYearFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FirstYearFlag));
		}
		if (FCode.equals("FirstTermFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FirstTermFlag));
		}
		if (FCode.equals("BonusType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BonusType));
		}
		if (FCode.equals("DifComFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DifComFlag));
		}
		if (FCode.equals("RiskCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
		}
		if (FCode.equals("RiskPeriod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskPeriod));
		}
		if (FCode.equals("RiskType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskType));
		}
		if (FCode.equals("RiskType1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskType1));
		}
		if (FCode.equals("RiskType2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskType2));
		}
		if (FCode.equals("CostCenter"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CostCenter));
		}
		if (FCode.equals("ACType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ACType));
		}
		if (FCode.equals("AgentCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
		}
		if (FCode.equals("AgentGroup"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
		}
		if (FCode.equals("AgentCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
		}
		if (FCode.equals("CustomerID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerID));
		}
		if (FCode.equals("SupplierNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SupplierNo));
		}
		if (FCode.equals("SaleChnl"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
		}
		if (FCode.equals("SaleChnlDetail"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnlDetail));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("ExecuteCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ExecuteCom));
		}
		if (FCode.equals("CValiDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCValiDate()));
		}
		if (FCode.equals("PayIntv"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayIntv));
		}
		if (FCode.equals("LastPayToDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getLastPayToDate()));
		}
		if (FCode.equals("CurPayToDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCurPayToDate()));
		}
		if (FCode.equals("PolYear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolYear));
		}
		if (FCode.equals("Years"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Years));
		}
		if (FCode.equals("PremiumType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PremiumType));
		}
		if (FCode.equals("PayCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayCount));
		}
		if (FCode.equals("PayMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayMode));
		}
		if (FCode.equals("BankCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
		}
		if (FCode.equals("AccName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccName));
		}
		if (FCode.equals("BankAccNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccNo));
		}
		if (FCode.equals("SumActuMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SumActuMoney));
		}
		if (FCode.equals("MothDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMothDate()));
		}
		if (FCode.equals("BusinessDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getBusinessDate()));
		}
		if (FCode.equals("AccountDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getAccountDate()));
		}
		if (FCode.equals("CashFlowNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CashFlowNo));
		}
		if (FCode.equals("FirstYear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FirstYear));
		}
		if (FCode.equals("MarketType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MarketType));
		}
		if (FCode.equals("OperationType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OperationType));
		}
		if (FCode.equals("Budget"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Budget));
		}
		if (FCode.equals("Currency"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Currency));
		}
		if (FCode.equals("StringInfo01"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StringInfo01));
		}
		if (FCode.equals("StringInfo02"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StringInfo02));
		}
		if (FCode.equals("StringInfo03"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StringInfo03));
		}
		if (FCode.equals("DateInfo01"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getDateInfo01()));
		}
		if (FCode.equals("DateInfo02"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getDateInfo02()));
		}
		if (FCode.equals("RelatedNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RelatedNo));
		}
		if (FCode.equals("EventNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EventNo));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("ReadState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReadState));
		}
		if (FCode.equals("CheckFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CheckFlag));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(SerialNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(NoType);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(ASerialNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(SecondaryFlag);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(SecondaryType);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(BusTypeID);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(CostID);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(BusinessType);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(BusinessDetail);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(FeeType);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(FeeDetail);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(IndexCode);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(IndexNo);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(ListFlag);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(GrpContNo);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(GrpPolNo);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(ContNo);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(PolNo);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(EndorsementNo);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(NotesNo);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(FirstYearFlag);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(FirstTermFlag);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(BonusType);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(DifComFlag);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(RiskCode);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(RiskPeriod);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(RiskType);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(RiskType1);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(RiskType2);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(CostCenter);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(ACType);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(AgentCom);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(AgentGroup);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(AgentCode);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(CustomerID);
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(SupplierNo);
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(SaleChnl);
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(SaleChnlDetail);
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(ExecuteCom);
				break;
			case 40:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCValiDate()));
				break;
			case 41:
				strFieldValue = String.valueOf(PayIntv);
				break;
			case 42:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getLastPayToDate()));
				break;
			case 43:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCurPayToDate()));
				break;
			case 44:
				strFieldValue = String.valueOf(PolYear);
				break;
			case 45:
				strFieldValue = String.valueOf(Years);
				break;
			case 46:
				strFieldValue = StrTool.GBKToUnicode(PremiumType);
				break;
			case 47:
				strFieldValue = String.valueOf(PayCount);
				break;
			case 48:
				strFieldValue = StrTool.GBKToUnicode(PayMode);
				break;
			case 49:
				strFieldValue = StrTool.GBKToUnicode(BankCode);
				break;
			case 50:
				strFieldValue = StrTool.GBKToUnicode(AccName);
				break;
			case 51:
				strFieldValue = StrTool.GBKToUnicode(BankAccNo);
				break;
			case 52:
				strFieldValue = String.valueOf(SumActuMoney);
				break;
			case 53:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMothDate()));
				break;
			case 54:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getBusinessDate()));
				break;
			case 55:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getAccountDate()));
				break;
			case 56:
				strFieldValue = StrTool.GBKToUnicode(CashFlowNo);
				break;
			case 57:
				strFieldValue = StrTool.GBKToUnicode(FirstYear);
				break;
			case 58:
				strFieldValue = StrTool.GBKToUnicode(MarketType);
				break;
			case 59:
				strFieldValue = StrTool.GBKToUnicode(OperationType);
				break;
			case 60:
				strFieldValue = StrTool.GBKToUnicode(Budget);
				break;
			case 61:
				strFieldValue = StrTool.GBKToUnicode(Currency);
				break;
			case 62:
				strFieldValue = StrTool.GBKToUnicode(StringInfo01);
				break;
			case 63:
				strFieldValue = StrTool.GBKToUnicode(StringInfo02);
				break;
			case 64:
				strFieldValue = StrTool.GBKToUnicode(StringInfo03);
				break;
			case 65:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getDateInfo01()));
				break;
			case 66:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getDateInfo02()));
				break;
			case 67:
				strFieldValue = StrTool.GBKToUnicode(RelatedNo);
				break;
			case 68:
				strFieldValue = StrTool.GBKToUnicode(EventNo);
				break;
			case 69:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 70:
				strFieldValue = StrTool.GBKToUnicode(ReadState);
				break;
			case 71:
				strFieldValue = StrTool.GBKToUnicode(CheckFlag);
				break;
			case 72:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 73:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 74:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("SerialNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SerialNo = FValue.trim();
			}
			else
				SerialNo = null;
		}
		if (FCode.equalsIgnoreCase("NoType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				NoType = FValue.trim();
			}
			else
				NoType = null;
		}
		if (FCode.equalsIgnoreCase("ASerialNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ASerialNo = FValue.trim();
			}
			else
				ASerialNo = null;
		}
		if (FCode.equalsIgnoreCase("SecondaryFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SecondaryFlag = FValue.trim();
			}
			else
				SecondaryFlag = null;
		}
		if (FCode.equalsIgnoreCase("SecondaryType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SecondaryType = FValue.trim();
			}
			else
				SecondaryType = null;
		}
		if (FCode.equalsIgnoreCase("BusTypeID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusTypeID = FValue.trim();
			}
			else
				BusTypeID = null;
		}
		if (FCode.equalsIgnoreCase("CostID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CostID = FValue.trim();
			}
			else
				CostID = null;
		}
		if (FCode.equalsIgnoreCase("BusinessType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusinessType = FValue.trim();
			}
			else
				BusinessType = null;
		}
		if (FCode.equalsIgnoreCase("BusinessDetail"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusinessDetail = FValue.trim();
			}
			else
				BusinessDetail = null;
		}
		if (FCode.equalsIgnoreCase("FeeType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FeeType = FValue.trim();
			}
			else
				FeeType = null;
		}
		if (FCode.equalsIgnoreCase("FeeDetail"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FeeDetail = FValue.trim();
			}
			else
				FeeDetail = null;
		}
		if (FCode.equalsIgnoreCase("IndexCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IndexCode = FValue.trim();
			}
			else
				IndexCode = null;
		}
		if (FCode.equalsIgnoreCase("IndexNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IndexNo = FValue.trim();
			}
			else
				IndexNo = null;
		}
		if (FCode.equalsIgnoreCase("ListFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ListFlag = FValue.trim();
			}
			else
				ListFlag = null;
		}
		if (FCode.equalsIgnoreCase("GrpContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpContNo = FValue.trim();
			}
			else
				GrpContNo = null;
		}
		if (FCode.equalsIgnoreCase("GrpPolNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpPolNo = FValue.trim();
			}
			else
				GrpPolNo = null;
		}
		if (FCode.equalsIgnoreCase("ContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContNo = FValue.trim();
			}
			else
				ContNo = null;
		}
		if (FCode.equalsIgnoreCase("PolNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PolNo = FValue.trim();
			}
			else
				PolNo = null;
		}
		if (FCode.equalsIgnoreCase("EndorsementNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EndorsementNo = FValue.trim();
			}
			else
				EndorsementNo = null;
		}
		if (FCode.equalsIgnoreCase("NotesNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				NotesNo = FValue.trim();
			}
			else
				NotesNo = null;
		}
		if (FCode.equalsIgnoreCase("FirstYearFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FirstYearFlag = FValue.trim();
			}
			else
				FirstYearFlag = null;
		}
		if (FCode.equalsIgnoreCase("FirstTermFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FirstTermFlag = FValue.trim();
			}
			else
				FirstTermFlag = null;
		}
		if (FCode.equalsIgnoreCase("BonusType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BonusType = FValue.trim();
			}
			else
				BonusType = null;
		}
		if (FCode.equalsIgnoreCase("DifComFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DifComFlag = FValue.trim();
			}
			else
				DifComFlag = null;
		}
		if (FCode.equalsIgnoreCase("RiskCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskCode = FValue.trim();
			}
			else
				RiskCode = null;
		}
		if (FCode.equalsIgnoreCase("RiskPeriod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskPeriod = FValue.trim();
			}
			else
				RiskPeriod = null;
		}
		if (FCode.equalsIgnoreCase("RiskType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskType = FValue.trim();
			}
			else
				RiskType = null;
		}
		if (FCode.equalsIgnoreCase("RiskType1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskType1 = FValue.trim();
			}
			else
				RiskType1 = null;
		}
		if (FCode.equalsIgnoreCase("RiskType2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskType2 = FValue.trim();
			}
			else
				RiskType2 = null;
		}
		if (FCode.equalsIgnoreCase("CostCenter"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CostCenter = FValue.trim();
			}
			else
				CostCenter = null;
		}
		if (FCode.equalsIgnoreCase("ACType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ACType = FValue.trim();
			}
			else
				ACType = null;
		}
		if (FCode.equalsIgnoreCase("AgentCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCom = FValue.trim();
			}
			else
				AgentCom = null;
		}
		if (FCode.equalsIgnoreCase("AgentGroup"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentGroup = FValue.trim();
			}
			else
				AgentGroup = null;
		}
		if (FCode.equalsIgnoreCase("AgentCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCode = FValue.trim();
			}
			else
				AgentCode = null;
		}
		if (FCode.equalsIgnoreCase("CustomerID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerID = FValue.trim();
			}
			else
				CustomerID = null;
		}
		if (FCode.equalsIgnoreCase("SupplierNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SupplierNo = FValue.trim();
			}
			else
				SupplierNo = null;
		}
		if (FCode.equalsIgnoreCase("SaleChnl"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SaleChnl = FValue.trim();
			}
			else
				SaleChnl = null;
		}
		if (FCode.equalsIgnoreCase("SaleChnlDetail"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SaleChnlDetail = FValue.trim();
			}
			else
				SaleChnlDetail = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("ExecuteCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ExecuteCom = FValue.trim();
			}
			else
				ExecuteCom = null;
		}
		if (FCode.equalsIgnoreCase("CValiDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CValiDate = fDate.getDate( FValue );
			}
			else
				CValiDate = null;
		}
		if (FCode.equalsIgnoreCase("PayIntv"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PayIntv = i;
			}
		}
		if (FCode.equalsIgnoreCase("LastPayToDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				LastPayToDate = fDate.getDate( FValue );
			}
			else
				LastPayToDate = null;
		}
		if (FCode.equalsIgnoreCase("CurPayToDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CurPayToDate = fDate.getDate( FValue );
			}
			else
				CurPayToDate = null;
		}
		if (FCode.equalsIgnoreCase("PolYear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PolYear = i;
			}
		}
		if (FCode.equalsIgnoreCase("Years"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				Years = i;
			}
		}
		if (FCode.equalsIgnoreCase("PremiumType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PremiumType = FValue.trim();
			}
			else
				PremiumType = null;
		}
		if (FCode.equalsIgnoreCase("PayCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PayCount = i;
			}
		}
		if (FCode.equalsIgnoreCase("PayMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayMode = FValue.trim();
			}
			else
				PayMode = null;
		}
		if (FCode.equalsIgnoreCase("BankCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankCode = FValue.trim();
			}
			else
				BankCode = null;
		}
		if (FCode.equalsIgnoreCase("AccName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccName = FValue.trim();
			}
			else
				AccName = null;
		}
		if (FCode.equalsIgnoreCase("BankAccNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankAccNo = FValue.trim();
			}
			else
				BankAccNo = null;
		}
		if (FCode.equalsIgnoreCase("SumActuMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SumActuMoney = d;
			}
		}
		if (FCode.equalsIgnoreCase("MothDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MothDate = fDate.getDate( FValue );
			}
			else
				MothDate = null;
		}
		if (FCode.equalsIgnoreCase("BusinessDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				BusinessDate = fDate.getDate( FValue );
			}
			else
				BusinessDate = null;
		}
		if (FCode.equalsIgnoreCase("AccountDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				AccountDate = fDate.getDate( FValue );
			}
			else
				AccountDate = null;
		}
		if (FCode.equalsIgnoreCase("CashFlowNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CashFlowNo = FValue.trim();
			}
			else
				CashFlowNo = null;
		}
		if (FCode.equalsIgnoreCase("FirstYear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FirstYear = FValue.trim();
			}
			else
				FirstYear = null;
		}
		if (FCode.equalsIgnoreCase("MarketType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MarketType = FValue.trim();
			}
			else
				MarketType = null;
		}
		if (FCode.equalsIgnoreCase("OperationType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OperationType = FValue.trim();
			}
			else
				OperationType = null;
		}
		if (FCode.equalsIgnoreCase("Budget"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Budget = FValue.trim();
			}
			else
				Budget = null;
		}
		if (FCode.equalsIgnoreCase("Currency"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Currency = FValue.trim();
			}
			else
				Currency = null;
		}
		if (FCode.equalsIgnoreCase("StringInfo01"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StringInfo01 = FValue.trim();
			}
			else
				StringInfo01 = null;
		}
		if (FCode.equalsIgnoreCase("StringInfo02"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StringInfo02 = FValue.trim();
			}
			else
				StringInfo02 = null;
		}
		if (FCode.equalsIgnoreCase("StringInfo03"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StringInfo03 = FValue.trim();
			}
			else
				StringInfo03 = null;
		}
		if (FCode.equalsIgnoreCase("DateInfo01"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				DateInfo01 = fDate.getDate( FValue );
			}
			else
				DateInfo01 = null;
		}
		if (FCode.equalsIgnoreCase("DateInfo02"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				DateInfo02 = fDate.getDate( FValue );
			}
			else
				DateInfo02 = null;
		}
		if (FCode.equalsIgnoreCase("RelatedNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RelatedNo = FValue.trim();
			}
			else
				RelatedNo = null;
		}
		if (FCode.equalsIgnoreCase("EventNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EventNo = FValue.trim();
			}
			else
				EventNo = null;
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("ReadState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReadState = FValue.trim();
			}
			else
				ReadState = null;
		}
		if (FCode.equalsIgnoreCase("CheckFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CheckFlag = FValue.trim();
			}
			else
				CheckFlag = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		FIAbStandardDataSchema other = (FIAbStandardDataSchema)otherObject;
		return
			(SerialNo == null ? other.getSerialNo() == null : SerialNo.equals(other.getSerialNo()))
			&& (NoType == null ? other.getNoType() == null : NoType.equals(other.getNoType()))
			&& (ASerialNo == null ? other.getASerialNo() == null : ASerialNo.equals(other.getASerialNo()))
			&& (SecondaryFlag == null ? other.getSecondaryFlag() == null : SecondaryFlag.equals(other.getSecondaryFlag()))
			&& (SecondaryType == null ? other.getSecondaryType() == null : SecondaryType.equals(other.getSecondaryType()))
			&& (BusTypeID == null ? other.getBusTypeID() == null : BusTypeID.equals(other.getBusTypeID()))
			&& (CostID == null ? other.getCostID() == null : CostID.equals(other.getCostID()))
			&& (BusinessType == null ? other.getBusinessType() == null : BusinessType.equals(other.getBusinessType()))
			&& (BusinessDetail == null ? other.getBusinessDetail() == null : BusinessDetail.equals(other.getBusinessDetail()))
			&& (FeeType == null ? other.getFeeType() == null : FeeType.equals(other.getFeeType()))
			&& (FeeDetail == null ? other.getFeeDetail() == null : FeeDetail.equals(other.getFeeDetail()))
			&& (IndexCode == null ? other.getIndexCode() == null : IndexCode.equals(other.getIndexCode()))
			&& (IndexNo == null ? other.getIndexNo() == null : IndexNo.equals(other.getIndexNo()))
			&& (ListFlag == null ? other.getListFlag() == null : ListFlag.equals(other.getListFlag()))
			&& (GrpContNo == null ? other.getGrpContNo() == null : GrpContNo.equals(other.getGrpContNo()))
			&& (GrpPolNo == null ? other.getGrpPolNo() == null : GrpPolNo.equals(other.getGrpPolNo()))
			&& (ContNo == null ? other.getContNo() == null : ContNo.equals(other.getContNo()))
			&& (PolNo == null ? other.getPolNo() == null : PolNo.equals(other.getPolNo()))
			&& (EndorsementNo == null ? other.getEndorsementNo() == null : EndorsementNo.equals(other.getEndorsementNo()))
			&& (NotesNo == null ? other.getNotesNo() == null : NotesNo.equals(other.getNotesNo()))
			&& (FirstYearFlag == null ? other.getFirstYearFlag() == null : FirstYearFlag.equals(other.getFirstYearFlag()))
			&& (FirstTermFlag == null ? other.getFirstTermFlag() == null : FirstTermFlag.equals(other.getFirstTermFlag()))
			&& (BonusType == null ? other.getBonusType() == null : BonusType.equals(other.getBonusType()))
			&& (DifComFlag == null ? other.getDifComFlag() == null : DifComFlag.equals(other.getDifComFlag()))
			&& (RiskCode == null ? other.getRiskCode() == null : RiskCode.equals(other.getRiskCode()))
			&& (RiskPeriod == null ? other.getRiskPeriod() == null : RiskPeriod.equals(other.getRiskPeriod()))
			&& (RiskType == null ? other.getRiskType() == null : RiskType.equals(other.getRiskType()))
			&& (RiskType1 == null ? other.getRiskType1() == null : RiskType1.equals(other.getRiskType1()))
			&& (RiskType2 == null ? other.getRiskType2() == null : RiskType2.equals(other.getRiskType2()))
			&& (CostCenter == null ? other.getCostCenter() == null : CostCenter.equals(other.getCostCenter()))
			&& (ACType == null ? other.getACType() == null : ACType.equals(other.getACType()))
			&& (AgentCom == null ? other.getAgentCom() == null : AgentCom.equals(other.getAgentCom()))
			&& (AgentGroup == null ? other.getAgentGroup() == null : AgentGroup.equals(other.getAgentGroup()))
			&& (AgentCode == null ? other.getAgentCode() == null : AgentCode.equals(other.getAgentCode()))
			&& (CustomerID == null ? other.getCustomerID() == null : CustomerID.equals(other.getCustomerID()))
			&& (SupplierNo == null ? other.getSupplierNo() == null : SupplierNo.equals(other.getSupplierNo()))
			&& (SaleChnl == null ? other.getSaleChnl() == null : SaleChnl.equals(other.getSaleChnl()))
			&& (SaleChnlDetail == null ? other.getSaleChnlDetail() == null : SaleChnlDetail.equals(other.getSaleChnlDetail()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (ExecuteCom == null ? other.getExecuteCom() == null : ExecuteCom.equals(other.getExecuteCom()))
			&& (CValiDate == null ? other.getCValiDate() == null : fDate.getString(CValiDate).equals(other.getCValiDate()))
			&& PayIntv == other.getPayIntv()
			&& (LastPayToDate == null ? other.getLastPayToDate() == null : fDate.getString(LastPayToDate).equals(other.getLastPayToDate()))
			&& (CurPayToDate == null ? other.getCurPayToDate() == null : fDate.getString(CurPayToDate).equals(other.getCurPayToDate()))
			&& PolYear == other.getPolYear()
			&& Years == other.getYears()
			&& (PremiumType == null ? other.getPremiumType() == null : PremiumType.equals(other.getPremiumType()))
			&& PayCount == other.getPayCount()
			&& (PayMode == null ? other.getPayMode() == null : PayMode.equals(other.getPayMode()))
			&& (BankCode == null ? other.getBankCode() == null : BankCode.equals(other.getBankCode()))
			&& (AccName == null ? other.getAccName() == null : AccName.equals(other.getAccName()))
			&& (BankAccNo == null ? other.getBankAccNo() == null : BankAccNo.equals(other.getBankAccNo()))
			&& SumActuMoney == other.getSumActuMoney()
			&& (MothDate == null ? other.getMothDate() == null : fDate.getString(MothDate).equals(other.getMothDate()))
			&& (BusinessDate == null ? other.getBusinessDate() == null : fDate.getString(BusinessDate).equals(other.getBusinessDate()))
			&& (AccountDate == null ? other.getAccountDate() == null : fDate.getString(AccountDate).equals(other.getAccountDate()))
			&& (CashFlowNo == null ? other.getCashFlowNo() == null : CashFlowNo.equals(other.getCashFlowNo()))
			&& (FirstYear == null ? other.getFirstYear() == null : FirstYear.equals(other.getFirstYear()))
			&& (MarketType == null ? other.getMarketType() == null : MarketType.equals(other.getMarketType()))
			&& (OperationType == null ? other.getOperationType() == null : OperationType.equals(other.getOperationType()))
			&& (Budget == null ? other.getBudget() == null : Budget.equals(other.getBudget()))
			&& (Currency == null ? other.getCurrency() == null : Currency.equals(other.getCurrency()))
			&& (StringInfo01 == null ? other.getStringInfo01() == null : StringInfo01.equals(other.getStringInfo01()))
			&& (StringInfo02 == null ? other.getStringInfo02() == null : StringInfo02.equals(other.getStringInfo02()))
			&& (StringInfo03 == null ? other.getStringInfo03() == null : StringInfo03.equals(other.getStringInfo03()))
			&& (DateInfo01 == null ? other.getDateInfo01() == null : fDate.getString(DateInfo01).equals(other.getDateInfo01()))
			&& (DateInfo02 == null ? other.getDateInfo02() == null : fDate.getString(DateInfo02).equals(other.getDateInfo02()))
			&& (RelatedNo == null ? other.getRelatedNo() == null : RelatedNo.equals(other.getRelatedNo()))
			&& (EventNo == null ? other.getEventNo() == null : EventNo.equals(other.getEventNo()))
			&& (State == null ? other.getState() == null : State.equals(other.getState()))
			&& (ReadState == null ? other.getReadState() == null : ReadState.equals(other.getReadState()))
			&& (CheckFlag == null ? other.getCheckFlag() == null : CheckFlag.equals(other.getCheckFlag()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("SerialNo") ) {
			return 0;
		}
		if( strFieldName.equals("NoType") ) {
			return 1;
		}
		if( strFieldName.equals("ASerialNo") ) {
			return 2;
		}
		if( strFieldName.equals("SecondaryFlag") ) {
			return 3;
		}
		if( strFieldName.equals("SecondaryType") ) {
			return 4;
		}
		if( strFieldName.equals("BusTypeID") ) {
			return 5;
		}
		if( strFieldName.equals("CostID") ) {
			return 6;
		}
		if( strFieldName.equals("BusinessType") ) {
			return 7;
		}
		if( strFieldName.equals("BusinessDetail") ) {
			return 8;
		}
		if( strFieldName.equals("FeeType") ) {
			return 9;
		}
		if( strFieldName.equals("FeeDetail") ) {
			return 10;
		}
		if( strFieldName.equals("IndexCode") ) {
			return 11;
		}
		if( strFieldName.equals("IndexNo") ) {
			return 12;
		}
		if( strFieldName.equals("ListFlag") ) {
			return 13;
		}
		if( strFieldName.equals("GrpContNo") ) {
			return 14;
		}
		if( strFieldName.equals("GrpPolNo") ) {
			return 15;
		}
		if( strFieldName.equals("ContNo") ) {
			return 16;
		}
		if( strFieldName.equals("PolNo") ) {
			return 17;
		}
		if( strFieldName.equals("EndorsementNo") ) {
			return 18;
		}
		if( strFieldName.equals("NotesNo") ) {
			return 19;
		}
		if( strFieldName.equals("FirstYearFlag") ) {
			return 20;
		}
		if( strFieldName.equals("FirstTermFlag") ) {
			return 21;
		}
		if( strFieldName.equals("BonusType") ) {
			return 22;
		}
		if( strFieldName.equals("DifComFlag") ) {
			return 23;
		}
		if( strFieldName.equals("RiskCode") ) {
			return 24;
		}
		if( strFieldName.equals("RiskPeriod") ) {
			return 25;
		}
		if( strFieldName.equals("RiskType") ) {
			return 26;
		}
		if( strFieldName.equals("RiskType1") ) {
			return 27;
		}
		if( strFieldName.equals("RiskType2") ) {
			return 28;
		}
		if( strFieldName.equals("CostCenter") ) {
			return 29;
		}
		if( strFieldName.equals("ACType") ) {
			return 30;
		}
		if( strFieldName.equals("AgentCom") ) {
			return 31;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return 32;
		}
		if( strFieldName.equals("AgentCode") ) {
			return 33;
		}
		if( strFieldName.equals("CustomerID") ) {
			return 34;
		}
		if( strFieldName.equals("SupplierNo") ) {
			return 35;
		}
		if( strFieldName.equals("SaleChnl") ) {
			return 36;
		}
		if( strFieldName.equals("SaleChnlDetail") ) {
			return 37;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 38;
		}
		if( strFieldName.equals("ExecuteCom") ) {
			return 39;
		}
		if( strFieldName.equals("CValiDate") ) {
			return 40;
		}
		if( strFieldName.equals("PayIntv") ) {
			return 41;
		}
		if( strFieldName.equals("LastPayToDate") ) {
			return 42;
		}
		if( strFieldName.equals("CurPayToDate") ) {
			return 43;
		}
		if( strFieldName.equals("PolYear") ) {
			return 44;
		}
		if( strFieldName.equals("Years") ) {
			return 45;
		}
		if( strFieldName.equals("PremiumType") ) {
			return 46;
		}
		if( strFieldName.equals("PayCount") ) {
			return 47;
		}
		if( strFieldName.equals("PayMode") ) {
			return 48;
		}
		if( strFieldName.equals("BankCode") ) {
			return 49;
		}
		if( strFieldName.equals("AccName") ) {
			return 50;
		}
		if( strFieldName.equals("BankAccNo") ) {
			return 51;
		}
		if( strFieldName.equals("SumActuMoney") ) {
			return 52;
		}
		if( strFieldName.equals("MothDate") ) {
			return 53;
		}
		if( strFieldName.equals("BusinessDate") ) {
			return 54;
		}
		if( strFieldName.equals("AccountDate") ) {
			return 55;
		}
		if( strFieldName.equals("CashFlowNo") ) {
			return 56;
		}
		if( strFieldName.equals("FirstYear") ) {
			return 57;
		}
		if( strFieldName.equals("MarketType") ) {
			return 58;
		}
		if( strFieldName.equals("OperationType") ) {
			return 59;
		}
		if( strFieldName.equals("Budget") ) {
			return 60;
		}
		if( strFieldName.equals("Currency") ) {
			return 61;
		}
		if( strFieldName.equals("StringInfo01") ) {
			return 62;
		}
		if( strFieldName.equals("StringInfo02") ) {
			return 63;
		}
		if( strFieldName.equals("StringInfo03") ) {
			return 64;
		}
		if( strFieldName.equals("DateInfo01") ) {
			return 65;
		}
		if( strFieldName.equals("DateInfo02") ) {
			return 66;
		}
		if( strFieldName.equals("RelatedNo") ) {
			return 67;
		}
		if( strFieldName.equals("EventNo") ) {
			return 68;
		}
		if( strFieldName.equals("State") ) {
			return 69;
		}
		if( strFieldName.equals("ReadState") ) {
			return 70;
		}
		if( strFieldName.equals("CheckFlag") ) {
			return 71;
		}
		if( strFieldName.equals("Operator") ) {
			return 72;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 73;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 74;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "SerialNo";
				break;
			case 1:
				strFieldName = "NoType";
				break;
			case 2:
				strFieldName = "ASerialNo";
				break;
			case 3:
				strFieldName = "SecondaryFlag";
				break;
			case 4:
				strFieldName = "SecondaryType";
				break;
			case 5:
				strFieldName = "BusTypeID";
				break;
			case 6:
				strFieldName = "CostID";
				break;
			case 7:
				strFieldName = "BusinessType";
				break;
			case 8:
				strFieldName = "BusinessDetail";
				break;
			case 9:
				strFieldName = "FeeType";
				break;
			case 10:
				strFieldName = "FeeDetail";
				break;
			case 11:
				strFieldName = "IndexCode";
				break;
			case 12:
				strFieldName = "IndexNo";
				break;
			case 13:
				strFieldName = "ListFlag";
				break;
			case 14:
				strFieldName = "GrpContNo";
				break;
			case 15:
				strFieldName = "GrpPolNo";
				break;
			case 16:
				strFieldName = "ContNo";
				break;
			case 17:
				strFieldName = "PolNo";
				break;
			case 18:
				strFieldName = "EndorsementNo";
				break;
			case 19:
				strFieldName = "NotesNo";
				break;
			case 20:
				strFieldName = "FirstYearFlag";
				break;
			case 21:
				strFieldName = "FirstTermFlag";
				break;
			case 22:
				strFieldName = "BonusType";
				break;
			case 23:
				strFieldName = "DifComFlag";
				break;
			case 24:
				strFieldName = "RiskCode";
				break;
			case 25:
				strFieldName = "RiskPeriod";
				break;
			case 26:
				strFieldName = "RiskType";
				break;
			case 27:
				strFieldName = "RiskType1";
				break;
			case 28:
				strFieldName = "RiskType2";
				break;
			case 29:
				strFieldName = "CostCenter";
				break;
			case 30:
				strFieldName = "ACType";
				break;
			case 31:
				strFieldName = "AgentCom";
				break;
			case 32:
				strFieldName = "AgentGroup";
				break;
			case 33:
				strFieldName = "AgentCode";
				break;
			case 34:
				strFieldName = "CustomerID";
				break;
			case 35:
				strFieldName = "SupplierNo";
				break;
			case 36:
				strFieldName = "SaleChnl";
				break;
			case 37:
				strFieldName = "SaleChnlDetail";
				break;
			case 38:
				strFieldName = "ManageCom";
				break;
			case 39:
				strFieldName = "ExecuteCom";
				break;
			case 40:
				strFieldName = "CValiDate";
				break;
			case 41:
				strFieldName = "PayIntv";
				break;
			case 42:
				strFieldName = "LastPayToDate";
				break;
			case 43:
				strFieldName = "CurPayToDate";
				break;
			case 44:
				strFieldName = "PolYear";
				break;
			case 45:
				strFieldName = "Years";
				break;
			case 46:
				strFieldName = "PremiumType";
				break;
			case 47:
				strFieldName = "PayCount";
				break;
			case 48:
				strFieldName = "PayMode";
				break;
			case 49:
				strFieldName = "BankCode";
				break;
			case 50:
				strFieldName = "AccName";
				break;
			case 51:
				strFieldName = "BankAccNo";
				break;
			case 52:
				strFieldName = "SumActuMoney";
				break;
			case 53:
				strFieldName = "MothDate";
				break;
			case 54:
				strFieldName = "BusinessDate";
				break;
			case 55:
				strFieldName = "AccountDate";
				break;
			case 56:
				strFieldName = "CashFlowNo";
				break;
			case 57:
				strFieldName = "FirstYear";
				break;
			case 58:
				strFieldName = "MarketType";
				break;
			case 59:
				strFieldName = "OperationType";
				break;
			case 60:
				strFieldName = "Budget";
				break;
			case 61:
				strFieldName = "Currency";
				break;
			case 62:
				strFieldName = "StringInfo01";
				break;
			case 63:
				strFieldName = "StringInfo02";
				break;
			case 64:
				strFieldName = "StringInfo03";
				break;
			case 65:
				strFieldName = "DateInfo01";
				break;
			case 66:
				strFieldName = "DateInfo02";
				break;
			case 67:
				strFieldName = "RelatedNo";
				break;
			case 68:
				strFieldName = "EventNo";
				break;
			case 69:
				strFieldName = "State";
				break;
			case 70:
				strFieldName = "ReadState";
				break;
			case 71:
				strFieldName = "CheckFlag";
				break;
			case 72:
				strFieldName = "Operator";
				break;
			case 73:
				strFieldName = "MakeDate";
				break;
			case 74:
				strFieldName = "MakeTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("SerialNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("NoType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ASerialNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SecondaryFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SecondaryType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BusTypeID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CostID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BusinessType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BusinessDetail") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FeeType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FeeDetail") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IndexCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IndexNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ListFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpPolNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EndorsementNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("NotesNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FirstYearFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FirstTermFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BonusType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DifComFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskPeriod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskType1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskType2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CostCenter") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ACType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SupplierNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SaleChnl") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SaleChnlDetail") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ExecuteCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CValiDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("PayIntv") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("LastPayToDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("CurPayToDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("PolYear") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("Years") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("PremiumType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("PayMode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankAccNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SumActuMoney") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("MothDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("BusinessDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("AccountDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("CashFlowNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FirstYear") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MarketType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OperationType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Budget") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Currency") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StringInfo01") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StringInfo02") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StringInfo03") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DateInfo01") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("DateInfo02") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("RelatedNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EventNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReadState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CheckFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 36:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 37:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 38:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 39:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 40:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 41:
				nFieldType = Schema.TYPE_INT;
				break;
			case 42:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 43:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 44:
				nFieldType = Schema.TYPE_INT;
				break;
			case 45:
				nFieldType = Schema.TYPE_INT;
				break;
			case 46:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 47:
				nFieldType = Schema.TYPE_INT;
				break;
			case 48:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 49:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 50:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 51:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 52:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 53:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 54:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 55:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 56:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 57:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 58:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 59:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 60:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 61:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 62:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 63:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 64:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 65:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 66:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 67:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 68:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 69:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 70:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 71:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 72:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 73:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 74:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
