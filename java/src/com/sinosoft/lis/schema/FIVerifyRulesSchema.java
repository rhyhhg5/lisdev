/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.FIVerifyRulesDB;

/*
 * <p>ClassName: FIVerifyRulesSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 财务接口最新版
 * @CreateDate：2011-12-13
 */
public class FIVerifyRulesSchema implements Schema, Cloneable
{
	// @Field
	/** 版本编号 */
	private String VersionNo;
	/** 规则编码 */
	private String RuleDefID;
	/** 算法编码 */
	private String ArithmeticID;
	/** 算法名称 */
	private String ArithmeticName;
	/** 算法类型 */
	private String ArithmeticType;
	/** 算法类型明细 */
	private String SubType;
	/** 当前节点 */
	private String Node;
	/** 父节点 */
	private String ParentNode;
	/** 处理方式 */
	private String DealMode;
	/** 处理类名称 */
	private String DealClass;
	/** 处理文件名称 */
	private String FileName;
	/** 财务sql */
	private String FinDealSQL;
	/** 业务sql */
	private String BusDealSQL;
	/** 执行节点id */
	private String CallPointID;
	/** 返回数据标记 */
	private String ReturnFlag;
	/** 返回数据存储 */
	private String ReturnStore;
	/** 算法描述 */
	private String ReturnRemark;
	/** 算法状态 */
	private String State;
	/** 错误类型 */
	private String ErrType;

	public static final int FIELDNUM = 19;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public FIVerifyRulesSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[3];
		pk[0] = "VersionNo";
		pk[1] = "RuleDefID";
		pk[2] = "ArithmeticID";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		FIVerifyRulesSchema cloned = (FIVerifyRulesSchema) super.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getVersionNo()
	{
		return VersionNo;
	}
	public void setVersionNo(String aVersionNo)
	{
		VersionNo = aVersionNo;
	}
	public String getRuleDefID()
	{
		return RuleDefID;
	}
	public void setRuleDefID(String aRuleDefID)
	{
		RuleDefID = aRuleDefID;
	}
	public String getArithmeticID()
	{
		return ArithmeticID;
	}
	public void setArithmeticID(String aArithmeticID)
	{
		ArithmeticID = aArithmeticID;
	}
	public String getArithmeticName()
	{
		return ArithmeticName;
	}
	public void setArithmeticName(String aArithmeticName)
	{
		ArithmeticName = aArithmeticName;
	}
	public String getArithmeticType()
	{
		return ArithmeticType;
	}
	public void setArithmeticType(String aArithmeticType)
	{
		ArithmeticType = aArithmeticType;
	}
	public String getSubType()
	{
		return SubType;
	}
	public void setSubType(String aSubType)
	{
		SubType = aSubType;
	}
	public String getNode()
	{
		return Node;
	}
	public void setNode(String aNode)
	{
		Node = aNode;
	}
	public String getParentNode()
	{
		return ParentNode;
	}
	public void setParentNode(String aParentNode)
	{
		ParentNode = aParentNode;
	}
	public String getDealMode()
	{
		return DealMode;
	}
	public void setDealMode(String aDealMode)
	{
		DealMode = aDealMode;
	}
	public String getDealClass()
	{
		return DealClass;
	}
	public void setDealClass(String aDealClass)
	{
		DealClass = aDealClass;
	}
	public String getFileName()
	{
		return FileName;
	}
	public void setFileName(String aFileName)
	{
		FileName = aFileName;
	}
	public String getFinDealSQL()
	{
		return FinDealSQL;
	}
	public void setFinDealSQL(String aFinDealSQL)
	{
		FinDealSQL = aFinDealSQL;
	}
	public String getBusDealSQL()
	{
		return BusDealSQL;
	}
	public void setBusDealSQL(String aBusDealSQL)
	{
		BusDealSQL = aBusDealSQL;
	}
	public String getCallPointID()
	{
		return CallPointID;
	}
	public void setCallPointID(String aCallPointID)
	{
		CallPointID = aCallPointID;
	}
	public String getReturnFlag()
	{
		return ReturnFlag;
	}
	public void setReturnFlag(String aReturnFlag)
	{
		ReturnFlag = aReturnFlag;
	}
	public String getReturnStore()
	{
		return ReturnStore;
	}
	public void setReturnStore(String aReturnStore)
	{
		ReturnStore = aReturnStore;
	}
	public String getReturnRemark()
	{
		return ReturnRemark;
	}
	public void setReturnRemark(String aReturnRemark)
	{
		ReturnRemark = aReturnRemark;
	}
	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}
	public String getErrType()
	{
		return ErrType;
	}
	public void setErrType(String aErrType)
	{
		ErrType = aErrType;
	}

	/**
	* 使用另外一个 FIVerifyRulesSchema 对象给 Schema 赋值
	* @param: aFIVerifyRulesSchema FIVerifyRulesSchema
	**/
	public void setSchema(FIVerifyRulesSchema aFIVerifyRulesSchema)
	{
		this.VersionNo = aFIVerifyRulesSchema.getVersionNo();
		this.RuleDefID = aFIVerifyRulesSchema.getRuleDefID();
		this.ArithmeticID = aFIVerifyRulesSchema.getArithmeticID();
		this.ArithmeticName = aFIVerifyRulesSchema.getArithmeticName();
		this.ArithmeticType = aFIVerifyRulesSchema.getArithmeticType();
		this.SubType = aFIVerifyRulesSchema.getSubType();
		this.Node = aFIVerifyRulesSchema.getNode();
		this.ParentNode = aFIVerifyRulesSchema.getParentNode();
		this.DealMode = aFIVerifyRulesSchema.getDealMode();
		this.DealClass = aFIVerifyRulesSchema.getDealClass();
		this.FileName = aFIVerifyRulesSchema.getFileName();
		this.FinDealSQL = aFIVerifyRulesSchema.getFinDealSQL();
		this.BusDealSQL = aFIVerifyRulesSchema.getBusDealSQL();
		this.CallPointID = aFIVerifyRulesSchema.getCallPointID();
		this.ReturnFlag = aFIVerifyRulesSchema.getReturnFlag();
		this.ReturnStore = aFIVerifyRulesSchema.getReturnStore();
		this.ReturnRemark = aFIVerifyRulesSchema.getReturnRemark();
		this.State = aFIVerifyRulesSchema.getState();
		this.ErrType = aFIVerifyRulesSchema.getErrType();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("VersionNo") == null )
				this.VersionNo = null;
			else
				this.VersionNo = rs.getString("VersionNo").trim();

			if( rs.getString("RuleDefID") == null )
				this.RuleDefID = null;
			else
				this.RuleDefID = rs.getString("RuleDefID").trim();

			if( rs.getString("ArithmeticID") == null )
				this.ArithmeticID = null;
			else
				this.ArithmeticID = rs.getString("ArithmeticID").trim();

			if( rs.getString("ArithmeticName") == null )
				this.ArithmeticName = null;
			else
				this.ArithmeticName = rs.getString("ArithmeticName").trim();

			if( rs.getString("ArithmeticType") == null )
				this.ArithmeticType = null;
			else
				this.ArithmeticType = rs.getString("ArithmeticType").trim();

			if( rs.getString("SubType") == null )
				this.SubType = null;
			else
				this.SubType = rs.getString("SubType").trim();

			if( rs.getString("Node") == null )
				this.Node = null;
			else
				this.Node = rs.getString("Node").trim();

			if( rs.getString("ParentNode") == null )
				this.ParentNode = null;
			else
				this.ParentNode = rs.getString("ParentNode").trim();

			if( rs.getString("DealMode") == null )
				this.DealMode = null;
			else
				this.DealMode = rs.getString("DealMode").trim();

			if( rs.getString("DealClass") == null )
				this.DealClass = null;
			else
				this.DealClass = rs.getString("DealClass").trim();

			if( rs.getString("FileName") == null )
				this.FileName = null;
			else
				this.FileName = rs.getString("FileName").trim();

			if( rs.getString("FinDealSQL") == null )
				this.FinDealSQL = null;
			else
				this.FinDealSQL = rs.getString("FinDealSQL").trim();

			if( rs.getString("BusDealSQL") == null )
				this.BusDealSQL = null;
			else
				this.BusDealSQL = rs.getString("BusDealSQL").trim();

			if( rs.getString("CallPointID") == null )
				this.CallPointID = null;
			else
				this.CallPointID = rs.getString("CallPointID").trim();

			if( rs.getString("ReturnFlag") == null )
				this.ReturnFlag = null;
			else
				this.ReturnFlag = rs.getString("ReturnFlag").trim();

			if( rs.getString("ReturnStore") == null )
				this.ReturnStore = null;
			else
				this.ReturnStore = rs.getString("ReturnStore").trim();

			if( rs.getString("ReturnRemark") == null )
				this.ReturnRemark = null;
			else
				this.ReturnRemark = rs.getString("ReturnRemark").trim();

			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			if( rs.getString("ErrType") == null )
				this.ErrType = null;
			else
				this.ErrType = rs.getString("ErrType").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的FIVerifyRules表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIVerifyRulesSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public FIVerifyRulesSchema getSchema()
	{
		FIVerifyRulesSchema aFIVerifyRulesSchema = new FIVerifyRulesSchema();
		aFIVerifyRulesSchema.setSchema(this);
		return aFIVerifyRulesSchema;
	}

	public FIVerifyRulesDB getDB()
	{
		FIVerifyRulesDB aDBOper = new FIVerifyRulesDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpFIVerifyRules描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(VersionNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RuleDefID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ArithmeticID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ArithmeticName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ArithmeticType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SubType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Node)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ParentNode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DealMode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DealClass)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FileName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FinDealSQL)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusDealSQL)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CallPointID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReturnFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReturnStore)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReturnRemark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ErrType));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpFIVerifyRules>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			VersionNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			RuleDefID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			ArithmeticID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			ArithmeticName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			ArithmeticType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			SubType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			Node = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			ParentNode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			DealMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			DealClass = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			FileName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			FinDealSQL = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			BusDealSQL = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			CallPointID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			ReturnFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			ReturnStore = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			ReturnRemark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			ErrType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIVerifyRulesSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("VersionNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(VersionNo));
		}
		if (FCode.equals("RuleDefID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RuleDefID));
		}
		if (FCode.equals("ArithmeticID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ArithmeticID));
		}
		if (FCode.equals("ArithmeticName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ArithmeticName));
		}
		if (FCode.equals("ArithmeticType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ArithmeticType));
		}
		if (FCode.equals("SubType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SubType));
		}
		if (FCode.equals("Node"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Node));
		}
		if (FCode.equals("ParentNode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ParentNode));
		}
		if (FCode.equals("DealMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DealMode));
		}
		if (FCode.equals("DealClass"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DealClass));
		}
		if (FCode.equals("FileName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FileName));
		}
		if (FCode.equals("FinDealSQL"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FinDealSQL));
		}
		if (FCode.equals("BusDealSQL"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusDealSQL));
		}
		if (FCode.equals("CallPointID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CallPointID));
		}
		if (FCode.equals("ReturnFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReturnFlag));
		}
		if (FCode.equals("ReturnStore"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReturnStore));
		}
		if (FCode.equals("ReturnRemark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReturnRemark));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("ErrType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ErrType));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(VersionNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(RuleDefID);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(ArithmeticID);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(ArithmeticName);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(ArithmeticType);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(SubType);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(Node);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(ParentNode);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(DealMode);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(DealClass);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(FileName);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(FinDealSQL);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(BusDealSQL);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(CallPointID);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(ReturnFlag);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(ReturnStore);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(ReturnRemark);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(ErrType);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("VersionNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				VersionNo = FValue.trim();
			}
			else
				VersionNo = null;
		}
		if (FCode.equalsIgnoreCase("RuleDefID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RuleDefID = FValue.trim();
			}
			else
				RuleDefID = null;
		}
		if (FCode.equalsIgnoreCase("ArithmeticID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ArithmeticID = FValue.trim();
			}
			else
				ArithmeticID = null;
		}
		if (FCode.equalsIgnoreCase("ArithmeticName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ArithmeticName = FValue.trim();
			}
			else
				ArithmeticName = null;
		}
		if (FCode.equalsIgnoreCase("ArithmeticType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ArithmeticType = FValue.trim();
			}
			else
				ArithmeticType = null;
		}
		if (FCode.equalsIgnoreCase("SubType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SubType = FValue.trim();
			}
			else
				SubType = null;
		}
		if (FCode.equalsIgnoreCase("Node"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Node = FValue.trim();
			}
			else
				Node = null;
		}
		if (FCode.equalsIgnoreCase("ParentNode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ParentNode = FValue.trim();
			}
			else
				ParentNode = null;
		}
		if (FCode.equalsIgnoreCase("DealMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DealMode = FValue.trim();
			}
			else
				DealMode = null;
		}
		if (FCode.equalsIgnoreCase("DealClass"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DealClass = FValue.trim();
			}
			else
				DealClass = null;
		}
		if (FCode.equalsIgnoreCase("FileName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FileName = FValue.trim();
			}
			else
				FileName = null;
		}
		if (FCode.equalsIgnoreCase("FinDealSQL"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FinDealSQL = FValue.trim();
			}
			else
				FinDealSQL = null;
		}
		if (FCode.equalsIgnoreCase("BusDealSQL"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusDealSQL = FValue.trim();
			}
			else
				BusDealSQL = null;
		}
		if (FCode.equalsIgnoreCase("CallPointID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CallPointID = FValue.trim();
			}
			else
				CallPointID = null;
		}
		if (FCode.equalsIgnoreCase("ReturnFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReturnFlag = FValue.trim();
			}
			else
				ReturnFlag = null;
		}
		if (FCode.equalsIgnoreCase("ReturnStore"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReturnStore = FValue.trim();
			}
			else
				ReturnStore = null;
		}
		if (FCode.equalsIgnoreCase("ReturnRemark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReturnRemark = FValue.trim();
			}
			else
				ReturnRemark = null;
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("ErrType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ErrType = FValue.trim();
			}
			else
				ErrType = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		FIVerifyRulesSchema other = (FIVerifyRulesSchema)otherObject;
		return
			(VersionNo == null ? other.getVersionNo() == null : VersionNo.equals(other.getVersionNo()))
			&& (RuleDefID == null ? other.getRuleDefID() == null : RuleDefID.equals(other.getRuleDefID()))
			&& (ArithmeticID == null ? other.getArithmeticID() == null : ArithmeticID.equals(other.getArithmeticID()))
			&& (ArithmeticName == null ? other.getArithmeticName() == null : ArithmeticName.equals(other.getArithmeticName()))
			&& (ArithmeticType == null ? other.getArithmeticType() == null : ArithmeticType.equals(other.getArithmeticType()))
			&& (SubType == null ? other.getSubType() == null : SubType.equals(other.getSubType()))
			&& (Node == null ? other.getNode() == null : Node.equals(other.getNode()))
			&& (ParentNode == null ? other.getParentNode() == null : ParentNode.equals(other.getParentNode()))
			&& (DealMode == null ? other.getDealMode() == null : DealMode.equals(other.getDealMode()))
			&& (DealClass == null ? other.getDealClass() == null : DealClass.equals(other.getDealClass()))
			&& (FileName == null ? other.getFileName() == null : FileName.equals(other.getFileName()))
			&& (FinDealSQL == null ? other.getFinDealSQL() == null : FinDealSQL.equals(other.getFinDealSQL()))
			&& (BusDealSQL == null ? other.getBusDealSQL() == null : BusDealSQL.equals(other.getBusDealSQL()))
			&& (CallPointID == null ? other.getCallPointID() == null : CallPointID.equals(other.getCallPointID()))
			&& (ReturnFlag == null ? other.getReturnFlag() == null : ReturnFlag.equals(other.getReturnFlag()))
			&& (ReturnStore == null ? other.getReturnStore() == null : ReturnStore.equals(other.getReturnStore()))
			&& (ReturnRemark == null ? other.getReturnRemark() == null : ReturnRemark.equals(other.getReturnRemark()))
			&& (State == null ? other.getState() == null : State.equals(other.getState()))
			&& (ErrType == null ? other.getErrType() == null : ErrType.equals(other.getErrType()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("VersionNo") ) {
			return 0;
		}
		if( strFieldName.equals("RuleDefID") ) {
			return 1;
		}
		if( strFieldName.equals("ArithmeticID") ) {
			return 2;
		}
		if( strFieldName.equals("ArithmeticName") ) {
			return 3;
		}
		if( strFieldName.equals("ArithmeticType") ) {
			return 4;
		}
		if( strFieldName.equals("SubType") ) {
			return 5;
		}
		if( strFieldName.equals("Node") ) {
			return 6;
		}
		if( strFieldName.equals("ParentNode") ) {
			return 7;
		}
		if( strFieldName.equals("DealMode") ) {
			return 8;
		}
		if( strFieldName.equals("DealClass") ) {
			return 9;
		}
		if( strFieldName.equals("FileName") ) {
			return 10;
		}
		if( strFieldName.equals("FinDealSQL") ) {
			return 11;
		}
		if( strFieldName.equals("BusDealSQL") ) {
			return 12;
		}
		if( strFieldName.equals("CallPointID") ) {
			return 13;
		}
		if( strFieldName.equals("ReturnFlag") ) {
			return 14;
		}
		if( strFieldName.equals("ReturnStore") ) {
			return 15;
		}
		if( strFieldName.equals("ReturnRemark") ) {
			return 16;
		}
		if( strFieldName.equals("State") ) {
			return 17;
		}
		if( strFieldName.equals("ErrType") ) {
			return 18;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "VersionNo";
				break;
			case 1:
				strFieldName = "RuleDefID";
				break;
			case 2:
				strFieldName = "ArithmeticID";
				break;
			case 3:
				strFieldName = "ArithmeticName";
				break;
			case 4:
				strFieldName = "ArithmeticType";
				break;
			case 5:
				strFieldName = "SubType";
				break;
			case 6:
				strFieldName = "Node";
				break;
			case 7:
				strFieldName = "ParentNode";
				break;
			case 8:
				strFieldName = "DealMode";
				break;
			case 9:
				strFieldName = "DealClass";
				break;
			case 10:
				strFieldName = "FileName";
				break;
			case 11:
				strFieldName = "FinDealSQL";
				break;
			case 12:
				strFieldName = "BusDealSQL";
				break;
			case 13:
				strFieldName = "CallPointID";
				break;
			case 14:
				strFieldName = "ReturnFlag";
				break;
			case 15:
				strFieldName = "ReturnStore";
				break;
			case 16:
				strFieldName = "ReturnRemark";
				break;
			case 17:
				strFieldName = "State";
				break;
			case 18:
				strFieldName = "ErrType";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("VersionNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RuleDefID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ArithmeticID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ArithmeticName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ArithmeticType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SubType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Node") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ParentNode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DealMode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DealClass") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FileName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FinDealSQL") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BusDealSQL") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CallPointID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReturnFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReturnStore") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReturnRemark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ErrType") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
