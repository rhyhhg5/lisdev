/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LLSurveyDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LLSurveySchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-06-24
 */
public class LLSurveySchema implements Schema, Cloneable
{
    // @Field
    /** 调查号 */
    private String SurveyNo;
    /** 对应号码 */
    private String OtherNo;
    /** 其他对应号码类型 */
    private String OtherNoType;
    /** 分报案号(事件号) */
    private String SubRptNo;
    /** 提起阶段 */
    private String StartPhase;
    /** 出险人客户号 */
    private String CustomerNo;
    /** 出险人名称 */
    private String CustomerName;
    /** 调查类别 */
    private String SurveyClass;
    /** 调查类型 */
    private String SurveyType;
    /** 调查地点 */
    private String SurveySite;
    /** 调查原因 */
    private String SurveyRCode;
    /** 调查原因内容 */
    private String SurveyRDesc;
    /** 调查内容 */
    private String Content;
    /** 院内调查结论/回复内容 */
    private String result;
    /** 调查开始日期/分配日期 */
    private Date SurveyStartDate;
    /** 调查结束日期 */
    private Date SurveyEndDate;
    /** 调查报告状态 */
    private String SurveyFlag;
    /** 调查回复人 */
    private String SurveyOperator;
    /** 提起人 */
    private String StartMan;
    /** 审核人 */
    private String Confer;
    /** 审核意见 */
    private String ConfNote;
    /** 审核时间 */
    private Date ConfDate;
    /** 管理机构 */
    private String MngCom;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 提起案件状态 */
    private String RgtState;
    /** 事故描述 */
    private String AccidentDesc;
    /** 院外调查回复 */
    private String OHresult;

    public static final int FIELDNUM = 31; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LLSurveySchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[5];
        pk[0] = "SurveyNo";
        pk[1] = "OtherNo";
        pk[2] = "OtherNoType";
        pk[3] = "SubRptNo";
        pk[4] = "StartPhase";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException
    {
        LLSurveySchema cloned = (LLSurveySchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getSurveyNo()
    {
        return SurveyNo;
    }

    public void setSurveyNo(String aSurveyNo)
    {
        SurveyNo = aSurveyNo;
    }

    public String getOtherNo()
    {
        return OtherNo;
    }

    public void setOtherNo(String aOtherNo)
    {
        OtherNo = aOtherNo;
    }

    public String getOtherNoType()
    {
        return OtherNoType;
    }

    public void setOtherNoType(String aOtherNoType)
    {
        OtherNoType = aOtherNoType;
    }

    public String getSubRptNo()
    {
        return SubRptNo;
    }

    public void setSubRptNo(String aSubRptNo)
    {
        SubRptNo = aSubRptNo;
    }

    public String getStartPhase()
    {
        return StartPhase;
    }

    public void setStartPhase(String aStartPhase)
    {
        StartPhase = aStartPhase;
    }

    public String getCustomerNo()
    {
        return CustomerNo;
    }

    public void setCustomerNo(String aCustomerNo)
    {
        CustomerNo = aCustomerNo;
    }

    public String getCustomerName()
    {
        return CustomerName;
    }

    public void setCustomerName(String aCustomerName)
    {
        CustomerName = aCustomerName;
    }

    public String getSurveyClass()
    {
        return SurveyClass;
    }

    public void setSurveyClass(String aSurveyClass)
    {
        SurveyClass = aSurveyClass;
    }

    public String getSurveyType()
    {
        return SurveyType;
    }

    public void setSurveyType(String aSurveyType)
    {
        SurveyType = aSurveyType;
    }

    public String getSurveySite()
    {
        return SurveySite;
    }

    public void setSurveySite(String aSurveySite)
    {
        SurveySite = aSurveySite;
    }

    public String getSurveyRCode()
    {
        return SurveyRCode;
    }

    public void setSurveyRCode(String aSurveyRCode)
    {
        SurveyRCode = aSurveyRCode;
    }

    public String getSurveyRDesc()
    {
        return SurveyRDesc;
    }

    public void setSurveyRDesc(String aSurveyRDesc)
    {
        SurveyRDesc = aSurveyRDesc;
    }

    public String getContent()
    {
        return Content;
    }

    public void setContent(String aContent)
    {
        Content = aContent;
    }

    public String getresult()
    {
        return result;
    }

    public void setresult(String aresult)
    {
        result = aresult;
    }

    public String getSurveyStartDate()
    {
        if (SurveyStartDate != null)
        {
            return fDate.getString(SurveyStartDate);
        }
        else
        {
            return null;
        }
    }

    public void setSurveyStartDate(Date aSurveyStartDate)
    {
        SurveyStartDate = aSurveyStartDate;
    }

    public void setSurveyStartDate(String aSurveyStartDate)
    {
        if (aSurveyStartDate != null && !aSurveyStartDate.equals(""))
        {
            SurveyStartDate = fDate.getDate(aSurveyStartDate);
        }
        else
        {
            SurveyStartDate = null;
        }
    }

    public String getSurveyEndDate()
    {
        if (SurveyEndDate != null)
        {
            return fDate.getString(SurveyEndDate);
        }
        else
        {
            return null;
        }
    }

    public void setSurveyEndDate(Date aSurveyEndDate)
    {
        SurveyEndDate = aSurveyEndDate;
    }

    public void setSurveyEndDate(String aSurveyEndDate)
    {
        if (aSurveyEndDate != null && !aSurveyEndDate.equals(""))
        {
            SurveyEndDate = fDate.getDate(aSurveyEndDate);
        }
        else
        {
            SurveyEndDate = null;
        }
    }

    public String getSurveyFlag()
    {
        return SurveyFlag;
    }

    public void setSurveyFlag(String aSurveyFlag)
    {
        SurveyFlag = aSurveyFlag;
    }

    public String getSurveyOperator()
    {
        return SurveyOperator;
    }

    public void setSurveyOperator(String aSurveyOperator)
    {
        SurveyOperator = aSurveyOperator;
    }

    public String getStartMan()
    {
        return StartMan;
    }

    public void setStartMan(String aStartMan)
    {
        StartMan = aStartMan;
    }

    public String getConfer()
    {
        return Confer;
    }

    public void setConfer(String aConfer)
    {
        Confer = aConfer;
    }

    public String getConfNote()
    {
        return ConfNote;
    }

    public void setConfNote(String aConfNote)
    {
        ConfNote = aConfNote;
    }

    public String getConfDate()
    {
        if (ConfDate != null)
        {
            return fDate.getString(ConfDate);
        }
        else
        {
            return null;
        }
    }

    public void setConfDate(Date aConfDate)
    {
        ConfDate = aConfDate;
    }

    public void setConfDate(String aConfDate)
    {
        if (aConfDate != null && !aConfDate.equals(""))
        {
            ConfDate = fDate.getDate(aConfDate);
        }
        else
        {
            ConfDate = null;
        }
    }

    public String getMngCom()
    {
        return MngCom;
    }

    public void setMngCom(String aMngCom)
    {
        MngCom = aMngCom;
    }

    public String getOperator()
    {
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    public String getRgtState()
    {
        return RgtState;
    }

    public void setRgtState(String aRgtState)
    {
        RgtState = aRgtState;
    }

    public String getAccidentDesc()
    {
        return AccidentDesc;
    }

    public void setAccidentDesc(String aAccidentDesc)
    {
        AccidentDesc = aAccidentDesc;
    }

    public String getOHresult()
    {
        return OHresult;
    }

    public void setOHresult(String aOHresult)
    {
        OHresult = aOHresult;
    }

    /**
     * 使用另外一个 LLSurveySchema 对象给 Schema 赋值
     * @param: aLLSurveySchema LLSurveySchema
     **/
    public void setSchema(LLSurveySchema aLLSurveySchema)
    {
        this.SurveyNo = aLLSurveySchema.getSurveyNo();
        this.OtherNo = aLLSurveySchema.getOtherNo();
        this.OtherNoType = aLLSurveySchema.getOtherNoType();
        this.SubRptNo = aLLSurveySchema.getSubRptNo();
        this.StartPhase = aLLSurveySchema.getStartPhase();
        this.CustomerNo = aLLSurveySchema.getCustomerNo();
        this.CustomerName = aLLSurveySchema.getCustomerName();
        this.SurveyClass = aLLSurveySchema.getSurveyClass();
        this.SurveyType = aLLSurveySchema.getSurveyType();
        this.SurveySite = aLLSurveySchema.getSurveySite();
        this.SurveyRCode = aLLSurveySchema.getSurveyRCode();
        this.SurveyRDesc = aLLSurveySchema.getSurveyRDesc();
        this.Content = aLLSurveySchema.getContent();
        this.result = aLLSurveySchema.getresult();
        this.SurveyStartDate = fDate.getDate(aLLSurveySchema.getSurveyStartDate());
        this.SurveyEndDate = fDate.getDate(aLLSurveySchema.getSurveyEndDate());
        this.SurveyFlag = aLLSurveySchema.getSurveyFlag();
        this.SurveyOperator = aLLSurveySchema.getSurveyOperator();
        this.StartMan = aLLSurveySchema.getStartMan();
        this.Confer = aLLSurveySchema.getConfer();
        this.ConfNote = aLLSurveySchema.getConfNote();
        this.ConfDate = fDate.getDate(aLLSurveySchema.getConfDate());
        this.MngCom = aLLSurveySchema.getMngCom();
        this.Operator = aLLSurveySchema.getOperator();
        this.MakeDate = fDate.getDate(aLLSurveySchema.getMakeDate());
        this.MakeTime = aLLSurveySchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLLSurveySchema.getModifyDate());
        this.ModifyTime = aLLSurveySchema.getModifyTime();
        this.RgtState = aLLSurveySchema.getRgtState();
        this.AccidentDesc = aLLSurveySchema.getAccidentDesc();
        this.OHresult = aLLSurveySchema.getOHresult();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("SurveyNo") == null)
            {
                this.SurveyNo = null;
            }
            else
            {
                this.SurveyNo = rs.getString("SurveyNo").trim();
            }

            if (rs.getString("OtherNo") == null)
            {
                this.OtherNo = null;
            }
            else
            {
                this.OtherNo = rs.getString("OtherNo").trim();
            }

            if (rs.getString("OtherNoType") == null)
            {
                this.OtherNoType = null;
            }
            else
            {
                this.OtherNoType = rs.getString("OtherNoType").trim();
            }

            if (rs.getString("SubRptNo") == null)
            {
                this.SubRptNo = null;
            }
            else
            {
                this.SubRptNo = rs.getString("SubRptNo").trim();
            }

            if (rs.getString("StartPhase") == null)
            {
                this.StartPhase = null;
            }
            else
            {
                this.StartPhase = rs.getString("StartPhase").trim();
            }

            if (rs.getString("CustomerNo") == null)
            {
                this.CustomerNo = null;
            }
            else
            {
                this.CustomerNo = rs.getString("CustomerNo").trim();
            }

            if (rs.getString("CustomerName") == null)
            {
                this.CustomerName = null;
            }
            else
            {
                this.CustomerName = rs.getString("CustomerName").trim();
            }

            if (rs.getString("SurveyClass") == null)
            {
                this.SurveyClass = null;
            }
            else
            {
                this.SurveyClass = rs.getString("SurveyClass").trim();
            }

            if (rs.getString("SurveyType") == null)
            {
                this.SurveyType = null;
            }
            else
            {
                this.SurveyType = rs.getString("SurveyType").trim();
            }

            if (rs.getString("SurveySite") == null)
            {
                this.SurveySite = null;
            }
            else
            {
                this.SurveySite = rs.getString("SurveySite").trim();
            }

            if (rs.getString("SurveyRCode") == null)
            {
                this.SurveyRCode = null;
            }
            else
            {
                this.SurveyRCode = rs.getString("SurveyRCode").trim();
            }

            if (rs.getString("SurveyRDesc") == null)
            {
                this.SurveyRDesc = null;
            }
            else
            {
                this.SurveyRDesc = rs.getString("SurveyRDesc").trim();
            }

            if (rs.getString("Content") == null)
            {
                this.Content = null;
            }
            else
            {
                this.Content = rs.getString("Content").trim();
            }

            if (rs.getString("result") == null)
            {
                this.result = null;
            }
            else
            {
                this.result = rs.getString("result").trim();
            }

            this.SurveyStartDate = rs.getDate("SurveyStartDate");
            this.SurveyEndDate = rs.getDate("SurveyEndDate");
            if (rs.getString("SurveyFlag") == null)
            {
                this.SurveyFlag = null;
            }
            else
            {
                this.SurveyFlag = rs.getString("SurveyFlag").trim();
            }

            if (rs.getString("SurveyOperator") == null)
            {
                this.SurveyOperator = null;
            }
            else
            {
                this.SurveyOperator = rs.getString("SurveyOperator").trim();
            }

            if (rs.getString("StartMan") == null)
            {
                this.StartMan = null;
            }
            else
            {
                this.StartMan = rs.getString("StartMan").trim();
            }

            if (rs.getString("Confer") == null)
            {
                this.Confer = null;
            }
            else
            {
                this.Confer = rs.getString("Confer").trim();
            }

            if (rs.getString("ConfNote") == null)
            {
                this.ConfNote = null;
            }
            else
            {
                this.ConfNote = rs.getString("ConfNote").trim();
            }

            this.ConfDate = rs.getDate("ConfDate");
            if (rs.getString("MngCom") == null)
            {
                this.MngCom = null;
            }
            else
            {
                this.MngCom = rs.getString("MngCom").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("RgtState") == null)
            {
                this.RgtState = null;
            }
            else
            {
                this.RgtState = rs.getString("RgtState").trim();
            }

            if (rs.getString("AccidentDesc") == null)
            {
                this.AccidentDesc = null;
            }
            else
            {
                this.AccidentDesc = rs.getString("AccidentDesc").trim();
            }

            if (rs.getString("OHresult") == null)
            {
                this.OHresult = null;
            }
            else
            {
                this.OHresult = rs.getString("OHresult").trim();
            }

        }
        catch (SQLException sqle)
        {
            System.out.println("数据库中的LLSurvey表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLSurveySchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LLSurveySchema getSchema()
    {
        LLSurveySchema aLLSurveySchema = new LLSurveySchema();
        aLLSurveySchema.setSchema(this);
        return aLLSurveySchema;
    }

    public LLSurveyDB getDB()
    {
        LLSurveyDB aDBOper = new LLSurveyDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLSurvey描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(SurveyNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OtherNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OtherNoType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SubRptNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StartPhase));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CustomerNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CustomerName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SurveyClass));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SurveyType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SurveySite));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SurveyRCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SurveyRDesc));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Content));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(result));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(SurveyStartDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(SurveyEndDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SurveyFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SurveyOperator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StartMan));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Confer));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ConfNote));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ConfDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MngCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RgtState));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AccidentDesc));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OHresult));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLSurvey>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            SurveyNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            OtherNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                     SysConst.PACKAGESPILTER);
            OtherNoType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                         SysConst.PACKAGESPILTER);
            SubRptNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                      SysConst.PACKAGESPILTER);
            StartPhase = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                        SysConst.PACKAGESPILTER);
            CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                        SysConst.PACKAGESPILTER);
            CustomerName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                          SysConst.PACKAGESPILTER);
            SurveyClass = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                         SysConst.PACKAGESPILTER);
            SurveyType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                        SysConst.PACKAGESPILTER);
            SurveySite = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                        SysConst.PACKAGESPILTER);
            SurveyRCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                         SysConst.PACKAGESPILTER);
            SurveyRDesc = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                         SysConst.PACKAGESPILTER);
            Content = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                     SysConst.PACKAGESPILTER);
            result = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                    SysConst.PACKAGESPILTER);
            SurveyStartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 15, SysConst.PACKAGESPILTER));
            SurveyEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 16, SysConst.PACKAGESPILTER));
            SurveyFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                        SysConst.PACKAGESPILTER);
            SurveyOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                            18, SysConst.PACKAGESPILTER);
            StartMan = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                      SysConst.PACKAGESPILTER);
            Confer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                    SysConst.PACKAGESPILTER);
            ConfNote = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,
                                      SysConst.PACKAGESPILTER);
            ConfDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 22, SysConst.PACKAGESPILTER));
            MngCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,
                                    SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 25, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 27, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28,
                                        SysConst.PACKAGESPILTER);
            RgtState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29,
                                      SysConst.PACKAGESPILTER);
            AccidentDesc = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30,
                                          SysConst.PACKAGESPILTER);
            OHresult = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31,
                                      SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLSurveySchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("SurveyNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SurveyNo));
        }
        if (FCode.equals("OtherNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNo));
        }
        if (FCode.equals("OtherNoType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNoType));
        }
        if (FCode.equals("SubRptNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SubRptNo));
        }
        if (FCode.equals("StartPhase"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StartPhase));
        }
        if (FCode.equals("CustomerNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
        }
        if (FCode.equals("CustomerName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerName));
        }
        if (FCode.equals("SurveyClass"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SurveyClass));
        }
        if (FCode.equals("SurveyType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SurveyType));
        }
        if (FCode.equals("SurveySite"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SurveySite));
        }
        if (FCode.equals("SurveyRCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SurveyRCode));
        }
        if (FCode.equals("SurveyRDesc"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SurveyRDesc));
        }
        if (FCode.equals("Content"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Content));
        }
        if (FCode.equals("result"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(result));
        }
        if (FCode.equals("SurveyStartDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getSurveyStartDate()));
        }
        if (FCode.equals("SurveyEndDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getSurveyEndDate()));
        }
        if (FCode.equals("SurveyFlag"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SurveyFlag));
        }
        if (FCode.equals("SurveyOperator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SurveyOperator));
        }
        if (FCode.equals("StartMan"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StartMan));
        }
        if (FCode.equals("Confer"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Confer));
        }
        if (FCode.equals("ConfNote"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ConfNote));
        }
        if (FCode.equals("ConfDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getConfDate()));
        }
        if (FCode.equals("MngCom"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MngCom));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("RgtState"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RgtState));
        }
        if (FCode.equals("AccidentDesc"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccidentDesc));
        }
        if (FCode.equals("OHresult"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OHresult));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(SurveyNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(OtherNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(OtherNoType);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(SubRptNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(StartPhase);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(CustomerNo);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(CustomerName);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(SurveyClass);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(SurveyType);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(SurveySite);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(SurveyRCode);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(SurveyRDesc);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(Content);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(result);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getSurveyStartDate()));
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getSurveyEndDate()));
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(SurveyFlag);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(SurveyOperator);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(StartMan);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(Confer);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(ConfNote);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getConfDate()));
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(MngCom);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(RgtState);
                break;
            case 29:
                strFieldValue = StrTool.GBKToUnicode(AccidentDesc);
                break;
            case 30:
                strFieldValue = StrTool.GBKToUnicode(OHresult);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("SurveyNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SurveyNo = FValue.trim();
            }
            else
            {
                SurveyNo = null;
            }
        }
        if (FCode.equals("OtherNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OtherNo = FValue.trim();
            }
            else
            {
                OtherNo = null;
            }
        }
        if (FCode.equals("OtherNoType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OtherNoType = FValue.trim();
            }
            else
            {
                OtherNoType = null;
            }
        }
        if (FCode.equals("SubRptNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SubRptNo = FValue.trim();
            }
            else
            {
                SubRptNo = null;
            }
        }
        if (FCode.equals("StartPhase"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                StartPhase = FValue.trim();
            }
            else
            {
                StartPhase = null;
            }
        }
        if (FCode.equals("CustomerNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CustomerNo = FValue.trim();
            }
            else
            {
                CustomerNo = null;
            }
        }
        if (FCode.equals("CustomerName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CustomerName = FValue.trim();
            }
            else
            {
                CustomerName = null;
            }
        }
        if (FCode.equals("SurveyClass"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SurveyClass = FValue.trim();
            }
            else
            {
                SurveyClass = null;
            }
        }
        if (FCode.equals("SurveyType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SurveyType = FValue.trim();
            }
            else
            {
                SurveyType = null;
            }
        }
        if (FCode.equals("SurveySite"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SurveySite = FValue.trim();
            }
            else
            {
                SurveySite = null;
            }
        }
        if (FCode.equals("SurveyRCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SurveyRCode = FValue.trim();
            }
            else
            {
                SurveyRCode = null;
            }
        }
        if (FCode.equals("SurveyRDesc"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SurveyRDesc = FValue.trim();
            }
            else
            {
                SurveyRDesc = null;
            }
        }
        if (FCode.equals("Content"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Content = FValue.trim();
            }
            else
            {
                Content = null;
            }
        }
        if (FCode.equals("result"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                result = FValue.trim();
            }
            else
            {
                result = null;
            }
        }
        if (FCode.equals("SurveyStartDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SurveyStartDate = fDate.getDate(FValue);
            }
            else
            {
                SurveyStartDate = null;
            }
        }
        if (FCode.equals("SurveyEndDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SurveyEndDate = fDate.getDate(FValue);
            }
            else
            {
                SurveyEndDate = null;
            }
        }
        if (FCode.equals("SurveyFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SurveyFlag = FValue.trim();
            }
            else
            {
                SurveyFlag = null;
            }
        }
        if (FCode.equals("SurveyOperator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SurveyOperator = FValue.trim();
            }
            else
            {
                SurveyOperator = null;
            }
        }
        if (FCode.equals("StartMan"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                StartMan = FValue.trim();
            }
            else
            {
                StartMan = null;
            }
        }
        if (FCode.equals("Confer"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Confer = FValue.trim();
            }
            else
            {
                Confer = null;
            }
        }
        if (FCode.equals("ConfNote"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ConfNote = FValue.trim();
            }
            else
            {
                ConfNote = null;
            }
        }
        if (FCode.equals("ConfDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ConfDate = fDate.getDate(FValue);
            }
            else
            {
                ConfDate = null;
            }
        }
        if (FCode.equals("MngCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MngCom = FValue.trim();
            }
            else
            {
                MngCom = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        if (FCode.equals("RgtState"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RgtState = FValue.trim();
            }
            else
            {
                RgtState = null;
            }
        }
        if (FCode.equals("AccidentDesc"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AccidentDesc = FValue.trim();
            }
            else
            {
                AccidentDesc = null;
            }
        }
        if (FCode.equals("OHresult"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OHresult = FValue.trim();
            }
            else
            {
                OHresult = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LLSurveySchema other = (LLSurveySchema) otherObject;
        return
                SurveyNo.equals(other.getSurveyNo())
                && OtherNo.equals(other.getOtherNo())
                && OtherNoType.equals(other.getOtherNoType())
                && SubRptNo.equals(other.getSubRptNo())
                && StartPhase.equals(other.getStartPhase())
                && CustomerNo.equals(other.getCustomerNo())
                && CustomerName.equals(other.getCustomerName())
                && SurveyClass.equals(other.getSurveyClass())
                && SurveyType.equals(other.getSurveyType())
                && SurveySite.equals(other.getSurveySite())
                && SurveyRCode.equals(other.getSurveyRCode())
                && SurveyRDesc.equals(other.getSurveyRDesc())
                && Content.equals(other.getContent())
                && result.equals(other.getresult())
                &&
                fDate.getString(SurveyStartDate).equals(other.getSurveyStartDate())
                && fDate.getString(SurveyEndDate).equals(other.getSurveyEndDate())
                && SurveyFlag.equals(other.getSurveyFlag())
                && SurveyOperator.equals(other.getSurveyOperator())
                && StartMan.equals(other.getStartMan())
                && Confer.equals(other.getConfer())
                && ConfNote.equals(other.getConfNote())
                && fDate.getString(ConfDate).equals(other.getConfDate())
                && MngCom.equals(other.getMngCom())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && RgtState.equals(other.getRgtState())
                && AccidentDesc.equals(other.getAccidentDesc())
                && OHresult.equals(other.getOHresult());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("SurveyNo"))
        {
            return 0;
        }
        if (strFieldName.equals("OtherNo"))
        {
            return 1;
        }
        if (strFieldName.equals("OtherNoType"))
        {
            return 2;
        }
        if (strFieldName.equals("SubRptNo"))
        {
            return 3;
        }
        if (strFieldName.equals("StartPhase"))
        {
            return 4;
        }
        if (strFieldName.equals("CustomerNo"))
        {
            return 5;
        }
        if (strFieldName.equals("CustomerName"))
        {
            return 6;
        }
        if (strFieldName.equals("SurveyClass"))
        {
            return 7;
        }
        if (strFieldName.equals("SurveyType"))
        {
            return 8;
        }
        if (strFieldName.equals("SurveySite"))
        {
            return 9;
        }
        if (strFieldName.equals("SurveyRCode"))
        {
            return 10;
        }
        if (strFieldName.equals("SurveyRDesc"))
        {
            return 11;
        }
        if (strFieldName.equals("Content"))
        {
            return 12;
        }
        if (strFieldName.equals("result"))
        {
            return 13;
        }
        if (strFieldName.equals("SurveyStartDate"))
        {
            return 14;
        }
        if (strFieldName.equals("SurveyEndDate"))
        {
            return 15;
        }
        if (strFieldName.equals("SurveyFlag"))
        {
            return 16;
        }
        if (strFieldName.equals("SurveyOperator"))
        {
            return 17;
        }
        if (strFieldName.equals("StartMan"))
        {
            return 18;
        }
        if (strFieldName.equals("Confer"))
        {
            return 19;
        }
        if (strFieldName.equals("ConfNote"))
        {
            return 20;
        }
        if (strFieldName.equals("ConfDate"))
        {
            return 21;
        }
        if (strFieldName.equals("MngCom"))
        {
            return 22;
        }
        if (strFieldName.equals("Operator"))
        {
            return 23;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 24;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 25;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 26;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 27;
        }
        if (strFieldName.equals("RgtState"))
        {
            return 28;
        }
        if (strFieldName.equals("AccidentDesc"))
        {
            return 29;
        }
        if (strFieldName.equals("OHresult"))
        {
            return 30;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "SurveyNo";
                break;
            case 1:
                strFieldName = "OtherNo";
                break;
            case 2:
                strFieldName = "OtherNoType";
                break;
            case 3:
                strFieldName = "SubRptNo";
                break;
            case 4:
                strFieldName = "StartPhase";
                break;
            case 5:
                strFieldName = "CustomerNo";
                break;
            case 6:
                strFieldName = "CustomerName";
                break;
            case 7:
                strFieldName = "SurveyClass";
                break;
            case 8:
                strFieldName = "SurveyType";
                break;
            case 9:
                strFieldName = "SurveySite";
                break;
            case 10:
                strFieldName = "SurveyRCode";
                break;
            case 11:
                strFieldName = "SurveyRDesc";
                break;
            case 12:
                strFieldName = "Content";
                break;
            case 13:
                strFieldName = "result";
                break;
            case 14:
                strFieldName = "SurveyStartDate";
                break;
            case 15:
                strFieldName = "SurveyEndDate";
                break;
            case 16:
                strFieldName = "SurveyFlag";
                break;
            case 17:
                strFieldName = "SurveyOperator";
                break;
            case 18:
                strFieldName = "StartMan";
                break;
            case 19:
                strFieldName = "Confer";
                break;
            case 20:
                strFieldName = "ConfNote";
                break;
            case 21:
                strFieldName = "ConfDate";
                break;
            case 22:
                strFieldName = "MngCom";
                break;
            case 23:
                strFieldName = "Operator";
                break;
            case 24:
                strFieldName = "MakeDate";
                break;
            case 25:
                strFieldName = "MakeTime";
                break;
            case 26:
                strFieldName = "ModifyDate";
                break;
            case 27:
                strFieldName = "ModifyTime";
                break;
            case 28:
                strFieldName = "RgtState";
                break;
            case 29:
                strFieldName = "AccidentDesc";
                break;
            case 30:
                strFieldName = "OHresult";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("SurveyNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OtherNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OtherNoType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SubRptNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StartPhase"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CustomerNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CustomerName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SurveyClass"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SurveyType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SurveySite"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SurveyRCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SurveyRDesc"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Content"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("result"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SurveyStartDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("SurveyEndDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("SurveyFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SurveyOperator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StartMan"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Confer"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ConfNote"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ConfDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MngCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RgtState"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AccidentDesc"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OHresult"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 15:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 17:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 18:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 19:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 20:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 21:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 22:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 23:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 24:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 25:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 26:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 27:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 28:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 29:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 30:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
