/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.TRANSTASK_ZBX_SLDB;

/*
 * <p>ClassName: TRANSTASK_ZBX_SLSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2017-10-19
 */
public class TRANSTASK_ZBX_SLSchema implements Schema, Cloneable
{
	// @Field
	/** Ciitc_task_no */
	private String Ciitc_Task_No;
	/** Id */
	private int ID;
	/** Batchno */
	private String BatchNo;
	/** Proposalno */
	private String ProposalNo;
	/** Filename */
	private String FileName;
	/** Server_ip */
	private String Server_ip;
	/** Server_user */
	private String Server_user;
	/** Server_password */
	private String Server_password;
	/** Server_port */
	private String Server_port;
	/** Localfilepaths */
	private String LocalFilePaths;
	/** Remotefilepaths */
	private String RemoteFilePaths;
	/** Sourcechannel */
	private String SourceChannel;
	/** Priority */
	private String PrioRity;
	/** Filemd5 */
	private String FileMD5;
	/** Cookie */
	private String Cookie;
	/** Task_id */
	private String task_id;
	/** Transtype */
	private String TransType;
	/** Reqmessage */
	private InputStream ReqMessage;
	/** Flag */
	private String Flag;
	/** Status */
	private String Status;
	/** Transmsg */
	private String TransMsg;
	/** Filemakedate */
	private Date FileMakeDate;
	/** Makedate */
	private Date MakeDate;
	/** Modifydate */
	private Date ModifyDate;
	/** Bak1 */
	private String bak1;
	/** Bak2 */
	private String bak2;
	/** Bak3 */
	private String bak3;

	public static final int FIELDNUM = 27;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public TRANSTASK_ZBX_SLSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "Ciitc_Task_No";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		TRANSTASK_ZBX_SLSchema cloned = (TRANSTASK_ZBX_SLSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getCiitc_Task_No()
	{
		return Ciitc_Task_No;
	}
	public void setCiitc_Task_No(String aCiitc_Task_No)
	{
		Ciitc_Task_No = aCiitc_Task_No;
	}
	public int getID()
	{
		return ID;
	}
	public void setID(int aID)
	{
		ID = aID;
	}
	public void setID(String aID)
	{
		if (aID != null && !aID.equals(""))
		{
			Integer tInteger = new Integer(aID);
			int i = tInteger.intValue();
			ID = i;
		}
	}

	public String getBatchNo()
	{
		return BatchNo;
	}
	public void setBatchNo(String aBatchNo)
	{
		BatchNo = aBatchNo;
	}
	public String getProposalNo()
	{
		return ProposalNo;
	}
	public void setProposalNo(String aProposalNo)
	{
		ProposalNo = aProposalNo;
	}
	public String getFileName()
	{
		return FileName;
	}
	public void setFileName(String aFileName)
	{
		FileName = aFileName;
	}
	public String getServer_ip()
	{
		return Server_ip;
	}
	public void setServer_ip(String aServer_ip)
	{
		Server_ip = aServer_ip;
	}
	public String getServer_user()
	{
		return Server_user;
	}
	public void setServer_user(String aServer_user)
	{
		Server_user = aServer_user;
	}
	public String getServer_password()
	{
		return Server_password;
	}
	public void setServer_password(String aServer_password)
	{
		Server_password = aServer_password;
	}
	public String getServer_port()
	{
		return Server_port;
	}
	public void setServer_port(String aServer_port)
	{
		Server_port = aServer_port;
	}
	public String getLocalFilePaths()
	{
		return LocalFilePaths;
	}
	public void setLocalFilePaths(String aLocalFilePaths)
	{
		LocalFilePaths = aLocalFilePaths;
	}
	public String getRemoteFilePaths()
	{
		return RemoteFilePaths;
	}
	public void setRemoteFilePaths(String aRemoteFilePaths)
	{
		RemoteFilePaths = aRemoteFilePaths;
	}
	public String getSourceChannel()
	{
		return SourceChannel;
	}
	public void setSourceChannel(String aSourceChannel)
	{
		SourceChannel = aSourceChannel;
	}
	public String getPrioRity()
	{
		return PrioRity;
	}
	public void setPrioRity(String aPrioRity)
	{
		PrioRity = aPrioRity;
	}
	public String getFileMD5()
	{
		return FileMD5;
	}
	public void setFileMD5(String aFileMD5)
	{
		FileMD5 = aFileMD5;
	}
	public String getCookie()
	{
		return Cookie;
	}
	public void setCookie(String aCookie)
	{
		Cookie = aCookie;
	}
	public String gettask_id()
	{
		return task_id;
	}
	public void settask_id(String atask_id)
	{
		task_id = atask_id;
	}
	public String getTransType()
	{
		return TransType;
	}
	public void setTransType(String aTransType)
	{
		TransType = aTransType;
	}
	public InputStream getReqMessage()
	{
		return ReqMessage;
	}
	public void setReqMessage(InputStream aReqMessage)
	{
		ReqMessage = aReqMessage;
	}
	public void setReqMessage(String aReqMessage)
	{
	}

	public String getFlag()
	{
		return Flag;
	}
	public void setFlag(String aFlag)
	{
		Flag = aFlag;
	}
	public String getStatus()
	{
		return Status;
	}
	public void setStatus(String aStatus)
	{
		Status = aStatus;
	}
	public String getTransMsg()
	{
		return TransMsg;
	}
	public void setTransMsg(String aTransMsg)
	{
		TransMsg = aTransMsg;
	}
	public String getFileMakeDate()
	{
		if( FileMakeDate != null )
			return fDate.getString(FileMakeDate);
		else
			return null;
	}
	public void setFileMakeDate(Date aFileMakeDate)
	{
		FileMakeDate = aFileMakeDate;
	}
	public void setFileMakeDate(String aFileMakeDate)
	{
		if (aFileMakeDate != null && !aFileMakeDate.equals("") )
		{
			FileMakeDate = fDate.getDate( aFileMakeDate );
		}
		else
			FileMakeDate = null;
	}

	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getbak1()
	{
		return bak1;
	}
	public void setbak1(String abak1)
	{
		bak1 = abak1;
	}
	public String getbak2()
	{
		return bak2;
	}
	public void setbak2(String abak2)
	{
		bak2 = abak2;
	}
	public String getbak3()
	{
		return bak3;
	}
	public void setbak3(String abak3)
	{
		bak3 = abak3;
	}

	/**
	* 使用另外一个 TRANSTASK_ZBX_SLSchema 对象给 Schema 赋值
	* @param: aTRANSTASK_ZBX_SLSchema TRANSTASK_ZBX_SLSchema
	**/
	public void setSchema(TRANSTASK_ZBX_SLSchema aTRANSTASK_ZBX_SLSchema)
	{
		this.Ciitc_Task_No = aTRANSTASK_ZBX_SLSchema.getCiitc_Task_No();
		this.ID = aTRANSTASK_ZBX_SLSchema.getID();
		this.BatchNo = aTRANSTASK_ZBX_SLSchema.getBatchNo();
		this.ProposalNo = aTRANSTASK_ZBX_SLSchema.getProposalNo();
		this.FileName = aTRANSTASK_ZBX_SLSchema.getFileName();
		this.Server_ip = aTRANSTASK_ZBX_SLSchema.getServer_ip();
		this.Server_user = aTRANSTASK_ZBX_SLSchema.getServer_user();
		this.Server_password = aTRANSTASK_ZBX_SLSchema.getServer_password();
		this.Server_port = aTRANSTASK_ZBX_SLSchema.getServer_port();
		this.LocalFilePaths = aTRANSTASK_ZBX_SLSchema.getLocalFilePaths();
		this.RemoteFilePaths = aTRANSTASK_ZBX_SLSchema.getRemoteFilePaths();
		this.SourceChannel = aTRANSTASK_ZBX_SLSchema.getSourceChannel();
		this.PrioRity = aTRANSTASK_ZBX_SLSchema.getPrioRity();
		this.FileMD5 = aTRANSTASK_ZBX_SLSchema.getFileMD5();
		this.Cookie = aTRANSTASK_ZBX_SLSchema.getCookie();
		this.task_id = aTRANSTASK_ZBX_SLSchema.gettask_id();
		this.TransType = aTRANSTASK_ZBX_SLSchema.getTransType();
		this.ReqMessage = aTRANSTASK_ZBX_SLSchema.getReqMessage();
		this.Flag = aTRANSTASK_ZBX_SLSchema.getFlag();
		this.Status = aTRANSTASK_ZBX_SLSchema.getStatus();
		this.TransMsg = aTRANSTASK_ZBX_SLSchema.getTransMsg();
		this.FileMakeDate = fDate.getDate( aTRANSTASK_ZBX_SLSchema.getFileMakeDate());
		this.MakeDate = fDate.getDate( aTRANSTASK_ZBX_SLSchema.getMakeDate());
		this.ModifyDate = fDate.getDate( aTRANSTASK_ZBX_SLSchema.getModifyDate());
		this.bak1 = aTRANSTASK_ZBX_SLSchema.getbak1();
		this.bak2 = aTRANSTASK_ZBX_SLSchema.getbak2();
		this.bak3 = aTRANSTASK_ZBX_SLSchema.getbak3();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("Ciitc_Task_No") == null )
				this.Ciitc_Task_No = null;
			else
				this.Ciitc_Task_No = rs.getString("Ciitc_Task_No").trim();

			this.ID = rs.getInt("ID");
			if( rs.getString("BatchNo") == null )
				this.BatchNo = null;
			else
				this.BatchNo = rs.getString("BatchNo").trim();

			if( rs.getString("ProposalNo") == null )
				this.ProposalNo = null;
			else
				this.ProposalNo = rs.getString("ProposalNo").trim();

			if( rs.getString("FileName") == null )
				this.FileName = null;
			else
				this.FileName = rs.getString("FileName").trim();

			if( rs.getString("Server_ip") == null )
				this.Server_ip = null;
			else
				this.Server_ip = rs.getString("Server_ip").trim();

			if( rs.getString("Server_user") == null )
				this.Server_user = null;
			else
				this.Server_user = rs.getString("Server_user").trim();

			if( rs.getString("Server_password") == null )
				this.Server_password = null;
			else
				this.Server_password = rs.getString("Server_password").trim();

			if( rs.getString("Server_port") == null )
				this.Server_port = null;
			else
				this.Server_port = rs.getString("Server_port").trim();

			if( rs.getString("LocalFilePaths") == null )
				this.LocalFilePaths = null;
			else
				this.LocalFilePaths = rs.getString("LocalFilePaths").trim();

			if( rs.getString("RemoteFilePaths") == null )
				this.RemoteFilePaths = null;
			else
				this.RemoteFilePaths = rs.getString("RemoteFilePaths").trim();

			if( rs.getString("SourceChannel") == null )
				this.SourceChannel = null;
			else
				this.SourceChannel = rs.getString("SourceChannel").trim();

			if( rs.getString("PrioRity") == null )
				this.PrioRity = null;
			else
				this.PrioRity = rs.getString("PrioRity").trim();

			if( rs.getString("FileMD5") == null )
				this.FileMD5 = null;
			else
				this.FileMD5 = rs.getString("FileMD5").trim();

			if( rs.getString("Cookie") == null )
				this.Cookie = null;
			else
				this.Cookie = rs.getString("Cookie").trim();

			if( rs.getString("task_id") == null )
				this.task_id = null;
			else
				this.task_id = rs.getString("task_id").trim();

			if( rs.getString("TransType") == null )
				this.TransType = null;
			else
				this.TransType = rs.getString("TransType").trim();

			this.ReqMessage = rs.getBinaryStream("ReqMessage");
			if( rs.getString("Flag") == null )
				this.Flag = null;
			else
				this.Flag = rs.getString("Flag").trim();

			if( rs.getString("Status") == null )
				this.Status = null;
			else
				this.Status = rs.getString("Status").trim();

			if( rs.getString("TransMsg") == null )
				this.TransMsg = null;
			else
				this.TransMsg = rs.getString("TransMsg").trim();

			this.FileMakeDate = rs.getDate("FileMakeDate");
			this.MakeDate = rs.getDate("MakeDate");
			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("bak1") == null )
				this.bak1 = null;
			else
				this.bak1 = rs.getString("bak1").trim();

			if( rs.getString("bak2") == null )
				this.bak2 = null;
			else
				this.bak2 = rs.getString("bak2").trim();

			if( rs.getString("bak3") == null )
				this.bak3 = null;
			else
				this.bak3 = rs.getString("bak3").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的TRANSTASK_ZBX_SL表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "TRANSTASK_ZBX_SLSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public TRANSTASK_ZBX_SLSchema getSchema()
	{
		TRANSTASK_ZBX_SLSchema aTRANSTASK_ZBX_SLSchema = new TRANSTASK_ZBX_SLSchema();
		aTRANSTASK_ZBX_SLSchema.setSchema(this);
		return aTRANSTASK_ZBX_SLSchema;
	}

	public TRANSTASK_ZBX_SLDB getDB()
	{
		TRANSTASK_ZBX_SLDB aDBOper = new TRANSTASK_ZBX_SLDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpTRANSTASK_ZBX_SL描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(Ciitc_Task_No)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ID));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BatchNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProposalNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FileName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Server_ip)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Server_user)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Server_password)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Server_port)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LocalFilePaths)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RemoteFilePaths)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SourceChannel)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PrioRity)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FileMD5)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Cookie)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(task_id)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TransType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( 1 );strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Flag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Status)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TransMsg)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( FileMakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak3));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpTRANSTASK_ZBX_SL>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			Ciitc_Task_No = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			ID= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,2,SysConst.PACKAGESPILTER))).intValue();
			BatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			ProposalNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			FileName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			Server_ip = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			Server_user = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			Server_password = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			Server_port = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			LocalFilePaths = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			RemoteFilePaths = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			SourceChannel = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			PrioRity = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			FileMD5 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			Cookie = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			task_id = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			TransType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			
			Flag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			Status = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			TransMsg = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			FileMakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,SysConst.PACKAGESPILTER));
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,SysConst.PACKAGESPILTER));
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,SysConst.PACKAGESPILTER));
			bak1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			bak2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			bak3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "TRANSTASK_ZBX_SLSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("Ciitc_Task_No"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Ciitc_Task_No));
		}
		if (FCode.equals("ID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ID));
		}
		if (FCode.equals("BatchNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BatchNo));
		}
		if (FCode.equals("ProposalNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProposalNo));
		}
		if (FCode.equals("FileName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FileName));
		}
		if (FCode.equals("Server_ip"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Server_ip));
		}
		if (FCode.equals("Server_user"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Server_user));
		}
		if (FCode.equals("Server_password"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Server_password));
		}
		if (FCode.equals("Server_port"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Server_port));
		}
		if (FCode.equals("LocalFilePaths"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LocalFilePaths));
		}
		if (FCode.equals("RemoteFilePaths"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RemoteFilePaths));
		}
		if (FCode.equals("SourceChannel"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SourceChannel));
		}
		if (FCode.equals("PrioRity"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrioRity));
		}
		if (FCode.equals("FileMD5"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FileMD5));
		}
		if (FCode.equals("Cookie"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Cookie));
		}
		if (FCode.equals("task_id"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(task_id));
		}
		if (FCode.equals("TransType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransType));
		}
		if (FCode.equals("ReqMessage"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReqMessage));
		}
		if (FCode.equals("Flag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Flag));
		}
		if (FCode.equals("Status"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Status));
		}
		if (FCode.equals("TransMsg"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransMsg));
		}
		if (FCode.equals("FileMakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getFileMakeDate()));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("bak1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak1));
		}
		if (FCode.equals("bak2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak2));
		}
		if (FCode.equals("bak3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak3));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(Ciitc_Task_No);
				break;
			case 1:
				strFieldValue = String.valueOf(ID);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(BatchNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(ProposalNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(FileName);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(Server_ip);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(Server_user);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(Server_password);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(Server_port);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(LocalFilePaths);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(RemoteFilePaths);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(SourceChannel);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(PrioRity);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(FileMD5);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(Cookie);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(task_id);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(TransType);
				break;
			case 17:
				strFieldValue = String.valueOf(ReqMessage);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(Flag);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(Status);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(TransMsg);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getFileMakeDate()));
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(bak1);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(bak2);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(bak3);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("Ciitc_Task_No"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Ciitc_Task_No = FValue.trim();
			}
			else
				Ciitc_Task_No = null;
		}
		if (FCode.equalsIgnoreCase("ID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				ID = i;
			}
		}
		if (FCode.equalsIgnoreCase("BatchNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BatchNo = FValue.trim();
			}
			else
				BatchNo = null;
		}
		if (FCode.equalsIgnoreCase("ProposalNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProposalNo = FValue.trim();
			}
			else
				ProposalNo = null;
		}
		if (FCode.equalsIgnoreCase("FileName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FileName = FValue.trim();
			}
			else
				FileName = null;
		}
		if (FCode.equalsIgnoreCase("Server_ip"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Server_ip = FValue.trim();
			}
			else
				Server_ip = null;
		}
		if (FCode.equalsIgnoreCase("Server_user"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Server_user = FValue.trim();
			}
			else
				Server_user = null;
		}
		if (FCode.equalsIgnoreCase("Server_password"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Server_password = FValue.trim();
			}
			else
				Server_password = null;
		}
		if (FCode.equalsIgnoreCase("Server_port"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Server_port = FValue.trim();
			}
			else
				Server_port = null;
		}
		if (FCode.equalsIgnoreCase("LocalFilePaths"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LocalFilePaths = FValue.trim();
			}
			else
				LocalFilePaths = null;
		}
		if (FCode.equalsIgnoreCase("RemoteFilePaths"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RemoteFilePaths = FValue.trim();
			}
			else
				RemoteFilePaths = null;
		}
		if (FCode.equalsIgnoreCase("SourceChannel"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SourceChannel = FValue.trim();
			}
			else
				SourceChannel = null;
		}
		if (FCode.equalsIgnoreCase("PrioRity"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PrioRity = FValue.trim();
			}
			else
				PrioRity = null;
		}
		if (FCode.equalsIgnoreCase("FileMD5"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FileMD5 = FValue.trim();
			}
			else
				FileMD5 = null;
		}
		if (FCode.equalsIgnoreCase("Cookie"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Cookie = FValue.trim();
			}
			else
				Cookie = null;
		}
		if (FCode.equalsIgnoreCase("task_id"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				task_id = FValue.trim();
			}
			else
				task_id = null;
		}
		if (FCode.equalsIgnoreCase("TransType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TransType = FValue.trim();
			}
			else
				TransType = null;
		}
		if (FCode.equalsIgnoreCase("ReqMessage"))
		{
		}
		if (FCode.equalsIgnoreCase("Flag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Flag = FValue.trim();
			}
			else
				Flag = null;
		}
		if (FCode.equalsIgnoreCase("Status"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Status = FValue.trim();
			}
			else
				Status = null;
		}
		if (FCode.equalsIgnoreCase("TransMsg"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TransMsg = FValue.trim();
			}
			else
				TransMsg = null;
		}
		if (FCode.equalsIgnoreCase("FileMakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				FileMakeDate = fDate.getDate( FValue );
			}
			else
				FileMakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("bak1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak1 = FValue.trim();
			}
			else
				bak1 = null;
		}
		if (FCode.equalsIgnoreCase("bak2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak2 = FValue.trim();
			}
			else
				bak2 = null;
		}
		if (FCode.equalsIgnoreCase("bak3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak3 = FValue.trim();
			}
			else
				bak3 = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		TRANSTASK_ZBX_SLSchema other = (TRANSTASK_ZBX_SLSchema)otherObject;
		return
			(Ciitc_Task_No == null ? other.getCiitc_Task_No() == null : Ciitc_Task_No.equals(other.getCiitc_Task_No()))
			&& ID == other.getID()
			&& (BatchNo == null ? other.getBatchNo() == null : BatchNo.equals(other.getBatchNo()))
			&& (ProposalNo == null ? other.getProposalNo() == null : ProposalNo.equals(other.getProposalNo()))
			&& (FileName == null ? other.getFileName() == null : FileName.equals(other.getFileName()))
			&& (Server_ip == null ? other.getServer_ip() == null : Server_ip.equals(other.getServer_ip()))
			&& (Server_user == null ? other.getServer_user() == null : Server_user.equals(other.getServer_user()))
			&& (Server_password == null ? other.getServer_password() == null : Server_password.equals(other.getServer_password()))
			&& (Server_port == null ? other.getServer_port() == null : Server_port.equals(other.getServer_port()))
			&& (LocalFilePaths == null ? other.getLocalFilePaths() == null : LocalFilePaths.equals(other.getLocalFilePaths()))
			&& (RemoteFilePaths == null ? other.getRemoteFilePaths() == null : RemoteFilePaths.equals(other.getRemoteFilePaths()))
			&& (SourceChannel == null ? other.getSourceChannel() == null : SourceChannel.equals(other.getSourceChannel()))
			&& (PrioRity == null ? other.getPrioRity() == null : PrioRity.equals(other.getPrioRity()))
			&& (FileMD5 == null ? other.getFileMD5() == null : FileMD5.equals(other.getFileMD5()))
			&& (Cookie == null ? other.getCookie() == null : Cookie.equals(other.getCookie()))
			&& (task_id == null ? other.gettask_id() == null : task_id.equals(other.gettask_id()))
			&& (TransType == null ? other.getTransType() == null : TransType.equals(other.getTransType()))
			
			&& (Flag == null ? other.getFlag() == null : Flag.equals(other.getFlag()))
			&& (Status == null ? other.getStatus() == null : Status.equals(other.getStatus()))
			&& (TransMsg == null ? other.getTransMsg() == null : TransMsg.equals(other.getTransMsg()))
			&& (FileMakeDate == null ? other.getFileMakeDate() == null : fDate.getString(FileMakeDate).equals(other.getFileMakeDate()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (bak1 == null ? other.getbak1() == null : bak1.equals(other.getbak1()))
			&& (bak2 == null ? other.getbak2() == null : bak2.equals(other.getbak2()))
			&& (bak3 == null ? other.getbak3() == null : bak3.equals(other.getbak3()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("Ciitc_Task_No") ) {
			return 0;
		}
		if( strFieldName.equals("ID") ) {
			return 1;
		}
		if( strFieldName.equals("BatchNo") ) {
			return 2;
		}
		if( strFieldName.equals("ProposalNo") ) {
			return 3;
		}
		if( strFieldName.equals("FileName") ) {
			return 4;
		}
		if( strFieldName.equals("Server_ip") ) {
			return 5;
		}
		if( strFieldName.equals("Server_user") ) {
			return 6;
		}
		if( strFieldName.equals("Server_password") ) {
			return 7;
		}
		if( strFieldName.equals("Server_port") ) {
			return 8;
		}
		if( strFieldName.equals("LocalFilePaths") ) {
			return 9;
		}
		if( strFieldName.equals("RemoteFilePaths") ) {
			return 10;
		}
		if( strFieldName.equals("SourceChannel") ) {
			return 11;
		}
		if( strFieldName.equals("PrioRity") ) {
			return 12;
		}
		if( strFieldName.equals("FileMD5") ) {
			return 13;
		}
		if( strFieldName.equals("Cookie") ) {
			return 14;
		}
		if( strFieldName.equals("task_id") ) {
			return 15;
		}
		if( strFieldName.equals("TransType") ) {
			return 16;
		}
		if( strFieldName.equals("ReqMessage") ) {
			return 17;
		}
		if( strFieldName.equals("Flag") ) {
			return 18;
		}
		if( strFieldName.equals("Status") ) {
			return 19;
		}
		if( strFieldName.equals("TransMsg") ) {
			return 20;
		}
		if( strFieldName.equals("FileMakeDate") ) {
			return 21;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 22;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 23;
		}
		if( strFieldName.equals("bak1") ) {
			return 24;
		}
		if( strFieldName.equals("bak2") ) {
			return 25;
		}
		if( strFieldName.equals("bak3") ) {
			return 26;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "Ciitc_Task_No";
				break;
			case 1:
				strFieldName = "ID";
				break;
			case 2:
				strFieldName = "BatchNo";
				break;
			case 3:
				strFieldName = "ProposalNo";
				break;
			case 4:
				strFieldName = "FileName";
				break;
			case 5:
				strFieldName = "Server_ip";
				break;
			case 6:
				strFieldName = "Server_user";
				break;
			case 7:
				strFieldName = "Server_password";
				break;
			case 8:
				strFieldName = "Server_port";
				break;
			case 9:
				strFieldName = "LocalFilePaths";
				break;
			case 10:
				strFieldName = "RemoteFilePaths";
				break;
			case 11:
				strFieldName = "SourceChannel";
				break;
			case 12:
				strFieldName = "PrioRity";
				break;
			case 13:
				strFieldName = "FileMD5";
				break;
			case 14:
				strFieldName = "Cookie";
				break;
			case 15:
				strFieldName = "task_id";
				break;
			case 16:
				strFieldName = "TransType";
				break;
			case 17:
				strFieldName = "ReqMessage";
				break;
			case 18:
				strFieldName = "Flag";
				break;
			case 19:
				strFieldName = "Status";
				break;
			case 20:
				strFieldName = "TransMsg";
				break;
			case 21:
				strFieldName = "FileMakeDate";
				break;
			case 22:
				strFieldName = "MakeDate";
				break;
			case 23:
				strFieldName = "ModifyDate";
				break;
			case 24:
				strFieldName = "bak1";
				break;
			case 25:
				strFieldName = "bak2";
				break;
			case 26:
				strFieldName = "bak3";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("Ciitc_Task_No") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ID") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("BatchNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ProposalNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FileName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Server_ip") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Server_user") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Server_password") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Server_port") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LocalFilePaths") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RemoteFilePaths") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SourceChannel") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PrioRity") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FileMD5") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Cookie") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("task_id") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TransType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReqMessage") ) {
			return Schema.TYPE_NOFOUND;
		}
		if( strFieldName.equals("Flag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Status") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TransMsg") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FileMakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("bak1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak3") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_INT;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_NOFOUND;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 22:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 23:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
