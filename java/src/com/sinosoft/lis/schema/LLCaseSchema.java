/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LLCaseDB;

/*
 * <p>ClassName: LLCaseSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2016-02-29
 */
public class LLCaseSchema implements Schema, Cloneable
{
	// @Field
	/** 分案号(个人理赔号) */
	private String CaseNo;
	/** 立案号(申请登记号) */
	private String RgtNo;
	/** 案件类型 */
	private String RgtType;
	/** 案件状态 */
	private String RgtState;
	/** 出险人客户号 */
	private String CustomerNo;
	/** 出险人名称 */
	private String CustomerName;
	/** 出险类型 */
	private String AccidentType;
	/** 收据信息标志 */
	private String ReceiptFlag;
	/** 医院信息标志 */
	private String HospitalFlag;
	/** 调查报告标志 */
	private String SurveyFlag;
	/** 立案日期 */
	private Date RgtDate;
	/** 审核日期 */
	private Date HandleDate;
	/** 理算日期 */
	private Date ClaimCalDate;
	/** 材料齐备日期 */
	private Date AffixGetDate;
	/** 账单录入标记 */
	private String FeeInputFlag;
	/** 入院日期 */
	private Date InHospitalDate;
	/** 出院日期 */
	private Date OutHospitalDate;
	/** 无效住院天数 */
	private int InvaliHosDays;
	/** 实际住院天数 */
	private int InHospitalDays;
	/** 确诊日期 */
	private Date DianoseDate;
	/** 联系地址 */
	private String PostalAddress;
	/** 联系电话 */
	private String Phone;
	/** 出险开始日期 */
	private Date AccStartDate;
	/** 出险日期 */
	private Date AccidentDate;
	/** 出险地点 */
	private String AccidentSite;
	/** 死亡日期 */
	private Date DeathDate;
	/** 事故者现状 */
	private String CustState;
	/** 事故经过描述 */
	private String AccdentDesc;
	/** 出险人生日 */
	private Date CustBirthday;
	/** 出险人性别 */
	private String CustomerSex;
	/** 出险人年龄 */
	private double CustomerAge;
	/** 证件类型 */
	private String IDType;
	/** 证件号码 */
	private String IDNo;
	/** 审核人 */
	private String Handler;
	/** 审核状态 */
	private String UWState;
	/** 当前处理人 */
	private String Dealer;
	/** 申诉标志 */
	private String AppealFlag;
	/** 全部保单是否统一给付 */
	private String TogetherGet;
	/** 团体批处理标志 */
	private String GrpDealFlag;
	/** 赔付金领取方式 */
	private String GetMode;
	/** 赔付金领取间隔 */
	private int GetIntv;
	/** 核算标记 */
	private String CalFlag;
	/** 核赔标记 */
	private String UWFlag;
	/** 拒赔标记 */
	private String DeclineFlag;
	/** 结案标记 */
	private String EndCaseFlag;
	/** 结案日期 */
	private Date EndCaseDate;
	/** 管理机构 */
	private String MngCom;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 银行编码 */
	private String BankCode;
	/** 银行帐号 */
	private String BankAccNo;
	/** 银行帐户名 */
	private String AccName;
	/** 保险金领取方式 */
	private String CaseGetMode;
	/** 账户修改原因 */
	private String AccModifyReason;
	/** 报备产生日期 */
	private Date CaseNoDate;
	/** 案件属性 */
	private String CaseProp;
	/** 备注 */
	private String Remark;
	/** 案件撤消原因 */
	private String CancleReason;
	/** 案件撤消备注 */
	private String CancleRemark;
	/** 案件撤消人 */
	private String Cancler;
	/** 案件撤消时间 */
	private Date CancleDate;
	/** 立案人 */
	private String Rigister;
	/** 理算人 */
	private String Claimer;
	/** 审批人 */
	private String UWer;
	/** 审批日期 */
	private Date UWDate;
	/** 签批人 */
	private String Signer;
	/** 签批日期 */
	private Date SignerDate;
	/** 赔付险种编码 */
	private String RiskCode;
	/** 其他证件类型 */
	private String OtherIDType;
	/** 其他证件号码 */
	private String OtherIDNo;
	/** 团体客户号 */
	private String GrpNo;
	/** 单位名称 */
	private String GrpName;
	/** 合同处理标志 */
	private String ContDealFlag;
	/** 被保人手机号 */
	private String MobilePhone;
	/** 预付赔款回销标记 */
	private String PrePaidFlag;
	/** 领款人与被保人关系 */
	private String RelaDrawerInsured;

	public static final int FIELDNUM = 79;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LLCaseSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "CaseNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LLCaseSchema cloned = (LLCaseSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getCaseNo()
	{
		return CaseNo;
	}
	public void setCaseNo(String aCaseNo)
	{
		CaseNo = aCaseNo;
	}
	public String getRgtNo()
	{
		return RgtNo;
	}
	public void setRgtNo(String aRgtNo)
	{
		RgtNo = aRgtNo;
	}
	public String getRgtType()
	{
		return RgtType;
	}
	public void setRgtType(String aRgtType)
	{
		RgtType = aRgtType;
	}
	public String getRgtState()
	{
		return RgtState;
	}
	public void setRgtState(String aRgtState)
	{
		RgtState = aRgtState;
	}
	public String getCustomerNo()
	{
		return CustomerNo;
	}
	public void setCustomerNo(String aCustomerNo)
	{
		CustomerNo = aCustomerNo;
	}
	public String getCustomerName()
	{
		return CustomerName;
	}
	public void setCustomerName(String aCustomerName)
	{
		CustomerName = aCustomerName;
	}
	public String getAccidentType()
	{
		return AccidentType;
	}
	public void setAccidentType(String aAccidentType)
	{
		AccidentType = aAccidentType;
	}
	public String getReceiptFlag()
	{
		return ReceiptFlag;
	}
	public void setReceiptFlag(String aReceiptFlag)
	{
		ReceiptFlag = aReceiptFlag;
	}
	public String getHospitalFlag()
	{
		return HospitalFlag;
	}
	public void setHospitalFlag(String aHospitalFlag)
	{
		HospitalFlag = aHospitalFlag;
	}
	public String getSurveyFlag()
	{
		return SurveyFlag;
	}
	public void setSurveyFlag(String aSurveyFlag)
	{
		SurveyFlag = aSurveyFlag;
	}
	public String getRgtDate()
	{
		if( RgtDate != null )
			return fDate.getString(RgtDate);
		else
			return null;
	}
	public void setRgtDate(Date aRgtDate)
	{
		RgtDate = aRgtDate;
	}
	public void setRgtDate(String aRgtDate)
	{
		if (aRgtDate != null && !aRgtDate.equals("") )
		{
			RgtDate = fDate.getDate( aRgtDate );
		}
		else
			RgtDate = null;
	}

	public String getHandleDate()
	{
		if( HandleDate != null )
			return fDate.getString(HandleDate);
		else
			return null;
	}
	public void setHandleDate(Date aHandleDate)
	{
		HandleDate = aHandleDate;
	}
	public void setHandleDate(String aHandleDate)
	{
		if (aHandleDate != null && !aHandleDate.equals("") )
		{
			HandleDate = fDate.getDate( aHandleDate );
		}
		else
			HandleDate = null;
	}

	public String getClaimCalDate()
	{
		if( ClaimCalDate != null )
			return fDate.getString(ClaimCalDate);
		else
			return null;
	}
	public void setClaimCalDate(Date aClaimCalDate)
	{
		ClaimCalDate = aClaimCalDate;
	}
	public void setClaimCalDate(String aClaimCalDate)
	{
		if (aClaimCalDate != null && !aClaimCalDate.equals("") )
		{
			ClaimCalDate = fDate.getDate( aClaimCalDate );
		}
		else
			ClaimCalDate = null;
	}

	public String getAffixGetDate()
	{
		if( AffixGetDate != null )
			return fDate.getString(AffixGetDate);
		else
			return null;
	}
	public void setAffixGetDate(Date aAffixGetDate)
	{
		AffixGetDate = aAffixGetDate;
	}
	public void setAffixGetDate(String aAffixGetDate)
	{
		if (aAffixGetDate != null && !aAffixGetDate.equals("") )
		{
			AffixGetDate = fDate.getDate( aAffixGetDate );
		}
		else
			AffixGetDate = null;
	}

	public String getFeeInputFlag()
	{
		return FeeInputFlag;
	}
	public void setFeeInputFlag(String aFeeInputFlag)
	{
		FeeInputFlag = aFeeInputFlag;
	}
	public String getInHospitalDate()
	{
		if( InHospitalDate != null )
			return fDate.getString(InHospitalDate);
		else
			return null;
	}
	public void setInHospitalDate(Date aInHospitalDate)
	{
		InHospitalDate = aInHospitalDate;
	}
	public void setInHospitalDate(String aInHospitalDate)
	{
		if (aInHospitalDate != null && !aInHospitalDate.equals("") )
		{
			InHospitalDate = fDate.getDate( aInHospitalDate );
		}
		else
			InHospitalDate = null;
	}

	public String getOutHospitalDate()
	{
		if( OutHospitalDate != null )
			return fDate.getString(OutHospitalDate);
		else
			return null;
	}
	public void setOutHospitalDate(Date aOutHospitalDate)
	{
		OutHospitalDate = aOutHospitalDate;
	}
	public void setOutHospitalDate(String aOutHospitalDate)
	{
		if (aOutHospitalDate != null && !aOutHospitalDate.equals("") )
		{
			OutHospitalDate = fDate.getDate( aOutHospitalDate );
		}
		else
			OutHospitalDate = null;
	}

	public int getInvaliHosDays()
	{
		return InvaliHosDays;
	}
	public void setInvaliHosDays(int aInvaliHosDays)
	{
		InvaliHosDays = aInvaliHosDays;
	}
	public void setInvaliHosDays(String aInvaliHosDays)
	{
		if (aInvaliHosDays != null && !aInvaliHosDays.equals(""))
		{
			Integer tInteger = new Integer(aInvaliHosDays);
			int i = tInteger.intValue();
			InvaliHosDays = i;
		}
	}

	public int getInHospitalDays()
	{
		return InHospitalDays;
	}
	public void setInHospitalDays(int aInHospitalDays)
	{
		InHospitalDays = aInHospitalDays;
	}
	public void setInHospitalDays(String aInHospitalDays)
	{
		if (aInHospitalDays != null && !aInHospitalDays.equals(""))
		{
			Integer tInteger = new Integer(aInHospitalDays);
			int i = tInteger.intValue();
			InHospitalDays = i;
		}
	}

	public String getDianoseDate()
	{
		if( DianoseDate != null )
			return fDate.getString(DianoseDate);
		else
			return null;
	}
	public void setDianoseDate(Date aDianoseDate)
	{
		DianoseDate = aDianoseDate;
	}
	public void setDianoseDate(String aDianoseDate)
	{
		if (aDianoseDate != null && !aDianoseDate.equals("") )
		{
			DianoseDate = fDate.getDate( aDianoseDate );
		}
		else
			DianoseDate = null;
	}

	public String getPostalAddress()
	{
		return PostalAddress;
	}
	public void setPostalAddress(String aPostalAddress)
	{
		PostalAddress = aPostalAddress;
	}
	public String getPhone()
	{
		return Phone;
	}
	public void setPhone(String aPhone)
	{
		Phone = aPhone;
	}
	public String getAccStartDate()
	{
		if( AccStartDate != null )
			return fDate.getString(AccStartDate);
		else
			return null;
	}
	public void setAccStartDate(Date aAccStartDate)
	{
		AccStartDate = aAccStartDate;
	}
	public void setAccStartDate(String aAccStartDate)
	{
		if (aAccStartDate != null && !aAccStartDate.equals("") )
		{
			AccStartDate = fDate.getDate( aAccStartDate );
		}
		else
			AccStartDate = null;
	}

	public String getAccidentDate()
	{
		if( AccidentDate != null )
			return fDate.getString(AccidentDate);
		else
			return null;
	}
	public void setAccidentDate(Date aAccidentDate)
	{
		AccidentDate = aAccidentDate;
	}
	public void setAccidentDate(String aAccidentDate)
	{
		if (aAccidentDate != null && !aAccidentDate.equals("") )
		{
			AccidentDate = fDate.getDate( aAccidentDate );
		}
		else
			AccidentDate = null;
	}

	public String getAccidentSite()
	{
		return AccidentSite;
	}
	public void setAccidentSite(String aAccidentSite)
	{
		AccidentSite = aAccidentSite;
	}
	public String getDeathDate()
	{
		if( DeathDate != null )
			return fDate.getString(DeathDate);
		else
			return null;
	}
	public void setDeathDate(Date aDeathDate)
	{
		DeathDate = aDeathDate;
	}
	public void setDeathDate(String aDeathDate)
	{
		if (aDeathDate != null && !aDeathDate.equals("") )
		{
			DeathDate = fDate.getDate( aDeathDate );
		}
		else
			DeathDate = null;
	}

	public String getCustState()
	{
		return CustState;
	}
	public void setCustState(String aCustState)
	{
		CustState = aCustState;
	}
	public String getAccdentDesc()
	{
		return AccdentDesc;
	}
	public void setAccdentDesc(String aAccdentDesc)
	{
		AccdentDesc = aAccdentDesc;
	}
	public String getCustBirthday()
	{
		if( CustBirthday != null )
			return fDate.getString(CustBirthday);
		else
			return null;
	}
	public void setCustBirthday(Date aCustBirthday)
	{
		CustBirthday = aCustBirthday;
	}
	public void setCustBirthday(String aCustBirthday)
	{
		if (aCustBirthday != null && !aCustBirthday.equals("") )
		{
			CustBirthday = fDate.getDate( aCustBirthday );
		}
		else
			CustBirthday = null;
	}

	public String getCustomerSex()
	{
		return CustomerSex;
	}
	public void setCustomerSex(String aCustomerSex)
	{
		CustomerSex = aCustomerSex;
	}
	public double getCustomerAge()
	{
		return CustomerAge;
	}
	public void setCustomerAge(double aCustomerAge)
	{
		CustomerAge = Arith.round(aCustomerAge,0);
	}
	public void setCustomerAge(String aCustomerAge)
	{
		if (aCustomerAge != null && !aCustomerAge.equals(""))
		{
			Double tDouble = new Double(aCustomerAge);
			double d = tDouble.doubleValue();
                CustomerAge = Arith.round(d,0);
		}
	}

	public String getIDType()
	{
		return IDType;
	}
	public void setIDType(String aIDType)
	{
		IDType = aIDType;
	}
	public String getIDNo()
	{
		return IDNo;
	}
	public void setIDNo(String aIDNo)
	{
		IDNo = aIDNo;
	}
	public String getHandler()
	{
		return Handler;
	}
	public void setHandler(String aHandler)
	{
		Handler = aHandler;
	}
	public String getUWState()
	{
		return UWState;
	}
	public void setUWState(String aUWState)
	{
		UWState = aUWState;
	}
	public String getDealer()
	{
		return Dealer;
	}
	public void setDealer(String aDealer)
	{
		Dealer = aDealer;
	}
	public String getAppealFlag()
	{
		return AppealFlag;
	}
	public void setAppealFlag(String aAppealFlag)
	{
		AppealFlag = aAppealFlag;
	}
	public String getTogetherGet()
	{
		return TogetherGet;
	}
	public void setTogetherGet(String aTogetherGet)
	{
		TogetherGet = aTogetherGet;
	}
	public String getGrpDealFlag()
	{
		return GrpDealFlag;
	}
	public void setGrpDealFlag(String aGrpDealFlag)
	{
		GrpDealFlag = aGrpDealFlag;
	}
	public String getGetMode()
	{
		return GetMode;
	}
	public void setGetMode(String aGetMode)
	{
		GetMode = aGetMode;
	}
	public int getGetIntv()
	{
		return GetIntv;
	}
	public void setGetIntv(int aGetIntv)
	{
		GetIntv = aGetIntv;
	}
	public void setGetIntv(String aGetIntv)
	{
		if (aGetIntv != null && !aGetIntv.equals(""))
		{
			Integer tInteger = new Integer(aGetIntv);
			int i = tInteger.intValue();
			GetIntv = i;
		}
	}

	public String getCalFlag()
	{
		return CalFlag;
	}
	public void setCalFlag(String aCalFlag)
	{
		CalFlag = aCalFlag;
	}
	public String getUWFlag()
	{
		return UWFlag;
	}
	public void setUWFlag(String aUWFlag)
	{
		UWFlag = aUWFlag;
	}
	public String getDeclineFlag()
	{
		return DeclineFlag;
	}
	public void setDeclineFlag(String aDeclineFlag)
	{
		DeclineFlag = aDeclineFlag;
	}
	public String getEndCaseFlag()
	{
		return EndCaseFlag;
	}
	public void setEndCaseFlag(String aEndCaseFlag)
	{
		EndCaseFlag = aEndCaseFlag;
	}
	public String getEndCaseDate()
	{
		if( EndCaseDate != null )
			return fDate.getString(EndCaseDate);
		else
			return null;
	}
	public void setEndCaseDate(Date aEndCaseDate)
	{
		EndCaseDate = aEndCaseDate;
	}
	public void setEndCaseDate(String aEndCaseDate)
	{
		if (aEndCaseDate != null && !aEndCaseDate.equals("") )
		{
			EndCaseDate = fDate.getDate( aEndCaseDate );
		}
		else
			EndCaseDate = null;
	}

	public String getMngCom()
	{
		return MngCom;
	}
	public void setMngCom(String aMngCom)
	{
		MngCom = aMngCom;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getBankCode()
	{
		return BankCode;
	}
	public void setBankCode(String aBankCode)
	{
		BankCode = aBankCode;
	}
	public String getBankAccNo()
	{
		return BankAccNo;
	}
	public void setBankAccNo(String aBankAccNo)
	{
		BankAccNo = aBankAccNo;
	}
	public String getAccName()
	{
		return AccName;
	}
	public void setAccName(String aAccName)
	{
		AccName = aAccName;
	}
	public String getCaseGetMode()
	{
		return CaseGetMode;
	}
	public void setCaseGetMode(String aCaseGetMode)
	{
		CaseGetMode = aCaseGetMode;
	}
	public String getAccModifyReason()
	{
		return AccModifyReason;
	}
	public void setAccModifyReason(String aAccModifyReason)
	{
		AccModifyReason = aAccModifyReason;
	}
	public String getCaseNoDate()
	{
		if( CaseNoDate != null )
			return fDate.getString(CaseNoDate);
		else
			return null;
	}
	public void setCaseNoDate(Date aCaseNoDate)
	{
		CaseNoDate = aCaseNoDate;
	}
	public void setCaseNoDate(String aCaseNoDate)
	{
		if (aCaseNoDate != null && !aCaseNoDate.equals("") )
		{
			CaseNoDate = fDate.getDate( aCaseNoDate );
		}
		else
			CaseNoDate = null;
	}

	public String getCaseProp()
	{
		return CaseProp;
	}
	public void setCaseProp(String aCaseProp)
	{
		CaseProp = aCaseProp;
	}
	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
		Remark = aRemark;
	}
	public String getCancleReason()
	{
		return CancleReason;
	}
	public void setCancleReason(String aCancleReason)
	{
		CancleReason = aCancleReason;
	}
	public String getCancleRemark()
	{
		return CancleRemark;
	}
	public void setCancleRemark(String aCancleRemark)
	{
		CancleRemark = aCancleRemark;
	}
	public String getCancler()
	{
		return Cancler;
	}
	public void setCancler(String aCancler)
	{
		Cancler = aCancler;
	}
	public String getCancleDate()
	{
		if( CancleDate != null )
			return fDate.getString(CancleDate);
		else
			return null;
	}
	public void setCancleDate(Date aCancleDate)
	{
		CancleDate = aCancleDate;
	}
	public void setCancleDate(String aCancleDate)
	{
		if (aCancleDate != null && !aCancleDate.equals("") )
		{
			CancleDate = fDate.getDate( aCancleDate );
		}
		else
			CancleDate = null;
	}

	public String getRigister()
	{
		return Rigister;
	}
	public void setRigister(String aRigister)
	{
		Rigister = aRigister;
	}
	public String getClaimer()
	{
		return Claimer;
	}
	public void setClaimer(String aClaimer)
	{
		Claimer = aClaimer;
	}
	public String getUWer()
	{
		return UWer;
	}
	public void setUWer(String aUWer)
	{
		UWer = aUWer;
	}
	public String getUWDate()
	{
		if( UWDate != null )
			return fDate.getString(UWDate);
		else
			return null;
	}
	public void setUWDate(Date aUWDate)
	{
		UWDate = aUWDate;
	}
	public void setUWDate(String aUWDate)
	{
		if (aUWDate != null && !aUWDate.equals("") )
		{
			UWDate = fDate.getDate( aUWDate );
		}
		else
			UWDate = null;
	}

	public String getSigner()
	{
		return Signer;
	}
	public void setSigner(String aSigner)
	{
		Signer = aSigner;
	}
	public String getSignerDate()
	{
		if( SignerDate != null )
			return fDate.getString(SignerDate);
		else
			return null;
	}
	public void setSignerDate(Date aSignerDate)
	{
		SignerDate = aSignerDate;
	}
	public void setSignerDate(String aSignerDate)
	{
		if (aSignerDate != null && !aSignerDate.equals("") )
		{
			SignerDate = fDate.getDate( aSignerDate );
		}
		else
			SignerDate = null;
	}

	public String getRiskCode()
	{
		return RiskCode;
	}
	public void setRiskCode(String aRiskCode)
	{
		RiskCode = aRiskCode;
	}
	public String getOtherIDType()
	{
		return OtherIDType;
	}
	public void setOtherIDType(String aOtherIDType)
	{
		OtherIDType = aOtherIDType;
	}
	public String getOtherIDNo()
	{
		return OtherIDNo;
	}
	public void setOtherIDNo(String aOtherIDNo)
	{
		OtherIDNo = aOtherIDNo;
	}
	public String getGrpNo()
	{
		return GrpNo;
	}
	public void setGrpNo(String aGrpNo)
	{
		GrpNo = aGrpNo;
	}
	public String getGrpName()
	{
		return GrpName;
	}
	public void setGrpName(String aGrpName)
	{
		GrpName = aGrpName;
	}
	public String getContDealFlag()
	{
		return ContDealFlag;
	}
	public void setContDealFlag(String aContDealFlag)
	{
		ContDealFlag = aContDealFlag;
	}
	public String getMobilePhone()
	{
		return MobilePhone;
	}
	public void setMobilePhone(String aMobilePhone)
	{
		MobilePhone = aMobilePhone;
	}
	public String getPrePaidFlag()
	{
		return PrePaidFlag;
	}
	public void setPrePaidFlag(String aPrePaidFlag)
	{
		PrePaidFlag = aPrePaidFlag;
	}
	public String getRelaDrawerInsured()
	{
		return RelaDrawerInsured;
	}
	public void setRelaDrawerInsured(String aRelaDrawerInsured)
	{
		RelaDrawerInsured = aRelaDrawerInsured;
	}

	/**
	* 使用另外一个 LLCaseSchema 对象给 Schema 赋值
	* @param: aLLCaseSchema LLCaseSchema
	**/
	public void setSchema(LLCaseSchema aLLCaseSchema)
	{
		this.CaseNo = aLLCaseSchema.getCaseNo();
		this.RgtNo = aLLCaseSchema.getRgtNo();
		this.RgtType = aLLCaseSchema.getRgtType();
		this.RgtState = aLLCaseSchema.getRgtState();
		this.CustomerNo = aLLCaseSchema.getCustomerNo();
		this.CustomerName = aLLCaseSchema.getCustomerName();
		this.AccidentType = aLLCaseSchema.getAccidentType();
		this.ReceiptFlag = aLLCaseSchema.getReceiptFlag();
		this.HospitalFlag = aLLCaseSchema.getHospitalFlag();
		this.SurveyFlag = aLLCaseSchema.getSurveyFlag();
		this.RgtDate = fDate.getDate( aLLCaseSchema.getRgtDate());
		this.HandleDate = fDate.getDate( aLLCaseSchema.getHandleDate());
		this.ClaimCalDate = fDate.getDate( aLLCaseSchema.getClaimCalDate());
		this.AffixGetDate = fDate.getDate( aLLCaseSchema.getAffixGetDate());
		this.FeeInputFlag = aLLCaseSchema.getFeeInputFlag();
		this.InHospitalDate = fDate.getDate( aLLCaseSchema.getInHospitalDate());
		this.OutHospitalDate = fDate.getDate( aLLCaseSchema.getOutHospitalDate());
		this.InvaliHosDays = aLLCaseSchema.getInvaliHosDays();
		this.InHospitalDays = aLLCaseSchema.getInHospitalDays();
		this.DianoseDate = fDate.getDate( aLLCaseSchema.getDianoseDate());
		this.PostalAddress = aLLCaseSchema.getPostalAddress();
		this.Phone = aLLCaseSchema.getPhone();
		this.AccStartDate = fDate.getDate( aLLCaseSchema.getAccStartDate());
		this.AccidentDate = fDate.getDate( aLLCaseSchema.getAccidentDate());
		this.AccidentSite = aLLCaseSchema.getAccidentSite();
		this.DeathDate = fDate.getDate( aLLCaseSchema.getDeathDate());
		this.CustState = aLLCaseSchema.getCustState();
		this.AccdentDesc = aLLCaseSchema.getAccdentDesc();
		this.CustBirthday = fDate.getDate( aLLCaseSchema.getCustBirthday());
		this.CustomerSex = aLLCaseSchema.getCustomerSex();
		this.CustomerAge = aLLCaseSchema.getCustomerAge();
		this.IDType = aLLCaseSchema.getIDType();
		this.IDNo = aLLCaseSchema.getIDNo();
		this.Handler = aLLCaseSchema.getHandler();
		this.UWState = aLLCaseSchema.getUWState();
		this.Dealer = aLLCaseSchema.getDealer();
		this.AppealFlag = aLLCaseSchema.getAppealFlag();
		this.TogetherGet = aLLCaseSchema.getTogetherGet();
		this.GrpDealFlag = aLLCaseSchema.getGrpDealFlag();
		this.GetMode = aLLCaseSchema.getGetMode();
		this.GetIntv = aLLCaseSchema.getGetIntv();
		this.CalFlag = aLLCaseSchema.getCalFlag();
		this.UWFlag = aLLCaseSchema.getUWFlag();
		this.DeclineFlag = aLLCaseSchema.getDeclineFlag();
		this.EndCaseFlag = aLLCaseSchema.getEndCaseFlag();
		this.EndCaseDate = fDate.getDate( aLLCaseSchema.getEndCaseDate());
		this.MngCom = aLLCaseSchema.getMngCom();
		this.Operator = aLLCaseSchema.getOperator();
		this.MakeDate = fDate.getDate( aLLCaseSchema.getMakeDate());
		this.MakeTime = aLLCaseSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLLCaseSchema.getModifyDate());
		this.ModifyTime = aLLCaseSchema.getModifyTime();
		this.BankCode = aLLCaseSchema.getBankCode();
		this.BankAccNo = aLLCaseSchema.getBankAccNo();
		this.AccName = aLLCaseSchema.getAccName();
		this.CaseGetMode = aLLCaseSchema.getCaseGetMode();
		this.AccModifyReason = aLLCaseSchema.getAccModifyReason();
		this.CaseNoDate = fDate.getDate( aLLCaseSchema.getCaseNoDate());
		this.CaseProp = aLLCaseSchema.getCaseProp();
		this.Remark = aLLCaseSchema.getRemark();
		this.CancleReason = aLLCaseSchema.getCancleReason();
		this.CancleRemark = aLLCaseSchema.getCancleRemark();
		this.Cancler = aLLCaseSchema.getCancler();
		this.CancleDate = fDate.getDate( aLLCaseSchema.getCancleDate());
		this.Rigister = aLLCaseSchema.getRigister();
		this.Claimer = aLLCaseSchema.getClaimer();
		this.UWer = aLLCaseSchema.getUWer();
		this.UWDate = fDate.getDate( aLLCaseSchema.getUWDate());
		this.Signer = aLLCaseSchema.getSigner();
		this.SignerDate = fDate.getDate( aLLCaseSchema.getSignerDate());
		this.RiskCode = aLLCaseSchema.getRiskCode();
		this.OtherIDType = aLLCaseSchema.getOtherIDType();
		this.OtherIDNo = aLLCaseSchema.getOtherIDNo();
		this.GrpNo = aLLCaseSchema.getGrpNo();
		this.GrpName = aLLCaseSchema.getGrpName();
		this.ContDealFlag = aLLCaseSchema.getContDealFlag();
		this.MobilePhone = aLLCaseSchema.getMobilePhone();
		this.PrePaidFlag = aLLCaseSchema.getPrePaidFlag();
		this.RelaDrawerInsured = aLLCaseSchema.getRelaDrawerInsured();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("CaseNo") == null )
				this.CaseNo = null;
			else
				this.CaseNo = rs.getString("CaseNo").trim();

			if( rs.getString("RgtNo") == null )
				this.RgtNo = null;
			else
				this.RgtNo = rs.getString("RgtNo").trim();

			if( rs.getString("RgtType") == null )
				this.RgtType = null;
			else
				this.RgtType = rs.getString("RgtType").trim();

			if( rs.getString("RgtState") == null )
				this.RgtState = null;
			else
				this.RgtState = rs.getString("RgtState").trim();

			if( rs.getString("CustomerNo") == null )
				this.CustomerNo = null;
			else
				this.CustomerNo = rs.getString("CustomerNo").trim();

			if( rs.getString("CustomerName") == null )
				this.CustomerName = null;
			else
				this.CustomerName = rs.getString("CustomerName").trim();

			if( rs.getString("AccidentType") == null )
				this.AccidentType = null;
			else
				this.AccidentType = rs.getString("AccidentType").trim();

			if( rs.getString("ReceiptFlag") == null )
				this.ReceiptFlag = null;
			else
				this.ReceiptFlag = rs.getString("ReceiptFlag").trim();

			if( rs.getString("HospitalFlag") == null )
				this.HospitalFlag = null;
			else
				this.HospitalFlag = rs.getString("HospitalFlag").trim();

			if( rs.getString("SurveyFlag") == null )
				this.SurveyFlag = null;
			else
				this.SurveyFlag = rs.getString("SurveyFlag").trim();

			this.RgtDate = rs.getDate("RgtDate");
			this.HandleDate = rs.getDate("HandleDate");
			this.ClaimCalDate = rs.getDate("ClaimCalDate");
			this.AffixGetDate = rs.getDate("AffixGetDate");
			if( rs.getString("FeeInputFlag") == null )
				this.FeeInputFlag = null;
			else
				this.FeeInputFlag = rs.getString("FeeInputFlag").trim();

			this.InHospitalDate = rs.getDate("InHospitalDate");
			this.OutHospitalDate = rs.getDate("OutHospitalDate");
			this.InvaliHosDays = rs.getInt("InvaliHosDays");
			this.InHospitalDays = rs.getInt("InHospitalDays");
			this.DianoseDate = rs.getDate("DianoseDate");
			if( rs.getString("PostalAddress") == null )
				this.PostalAddress = null;
			else
				this.PostalAddress = rs.getString("PostalAddress").trim();

			if( rs.getString("Phone") == null )
				this.Phone = null;
			else
				this.Phone = rs.getString("Phone").trim();

			this.AccStartDate = rs.getDate("AccStartDate");
			this.AccidentDate = rs.getDate("AccidentDate");
			if( rs.getString("AccidentSite") == null )
				this.AccidentSite = null;
			else
				this.AccidentSite = rs.getString("AccidentSite").trim();

			this.DeathDate = rs.getDate("DeathDate");
			if( rs.getString("CustState") == null )
				this.CustState = null;
			else
				this.CustState = rs.getString("CustState").trim();

			if( rs.getString("AccdentDesc") == null )
				this.AccdentDesc = null;
			else
				this.AccdentDesc = rs.getString("AccdentDesc").trim();

			this.CustBirthday = rs.getDate("CustBirthday");
			if( rs.getString("CustomerSex") == null )
				this.CustomerSex = null;
			else
				this.CustomerSex = rs.getString("CustomerSex").trim();

			this.CustomerAge = rs.getDouble("CustomerAge");
			if( rs.getString("IDType") == null )
				this.IDType = null;
			else
				this.IDType = rs.getString("IDType").trim();

			if( rs.getString("IDNo") == null )
				this.IDNo = null;
			else
				this.IDNo = rs.getString("IDNo").trim();

			if( rs.getString("Handler") == null )
				this.Handler = null;
			else
				this.Handler = rs.getString("Handler").trim();

			if( rs.getString("UWState") == null )
				this.UWState = null;
			else
				this.UWState = rs.getString("UWState").trim();

			if( rs.getString("Dealer") == null )
				this.Dealer = null;
			else
				this.Dealer = rs.getString("Dealer").trim();

			if( rs.getString("AppealFlag") == null )
				this.AppealFlag = null;
			else
				this.AppealFlag = rs.getString("AppealFlag").trim();

			if( rs.getString("TogetherGet") == null )
				this.TogetherGet = null;
			else
				this.TogetherGet = rs.getString("TogetherGet").trim();

			if( rs.getString("GrpDealFlag") == null )
				this.GrpDealFlag = null;
			else
				this.GrpDealFlag = rs.getString("GrpDealFlag").trim();

			if( rs.getString("GetMode") == null )
				this.GetMode = null;
			else
				this.GetMode = rs.getString("GetMode").trim();

			this.GetIntv = rs.getInt("GetIntv");
			if( rs.getString("CalFlag") == null )
				this.CalFlag = null;
			else
				this.CalFlag = rs.getString("CalFlag").trim();

			if( rs.getString("UWFlag") == null )
				this.UWFlag = null;
			else
				this.UWFlag = rs.getString("UWFlag").trim();

			if( rs.getString("DeclineFlag") == null )
				this.DeclineFlag = null;
			else
				this.DeclineFlag = rs.getString("DeclineFlag").trim();

			if( rs.getString("EndCaseFlag") == null )
				this.EndCaseFlag = null;
			else
				this.EndCaseFlag = rs.getString("EndCaseFlag").trim();

			this.EndCaseDate = rs.getDate("EndCaseDate");
			if( rs.getString("MngCom") == null )
				this.MngCom = null;
			else
				this.MngCom = rs.getString("MngCom").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("BankCode") == null )
				this.BankCode = null;
			else
				this.BankCode = rs.getString("BankCode").trim();

			if( rs.getString("BankAccNo") == null )
				this.BankAccNo = null;
			else
				this.BankAccNo = rs.getString("BankAccNo").trim();

			if( rs.getString("AccName") == null )
				this.AccName = null;
			else
				this.AccName = rs.getString("AccName").trim();

			if( rs.getString("CaseGetMode") == null )
				this.CaseGetMode = null;
			else
				this.CaseGetMode = rs.getString("CaseGetMode").trim();

			if( rs.getString("AccModifyReason") == null )
				this.AccModifyReason = null;
			else
				this.AccModifyReason = rs.getString("AccModifyReason").trim();

			this.CaseNoDate = rs.getDate("CaseNoDate");
			if( rs.getString("CaseProp") == null )
				this.CaseProp = null;
			else
				this.CaseProp = rs.getString("CaseProp").trim();

			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

			if( rs.getString("CancleReason") == null )
				this.CancleReason = null;
			else
				this.CancleReason = rs.getString("CancleReason").trim();

			if( rs.getString("CancleRemark") == null )
				this.CancleRemark = null;
			else
				this.CancleRemark = rs.getString("CancleRemark").trim();

			if( rs.getString("Cancler") == null )
				this.Cancler = null;
			else
				this.Cancler = rs.getString("Cancler").trim();

			this.CancleDate = rs.getDate("CancleDate");
			if( rs.getString("Rigister") == null )
				this.Rigister = null;
			else
				this.Rigister = rs.getString("Rigister").trim();

			if( rs.getString("Claimer") == null )
				this.Claimer = null;
			else
				this.Claimer = rs.getString("Claimer").trim();

			if( rs.getString("UWer") == null )
				this.UWer = null;
			else
				this.UWer = rs.getString("UWer").trim();

			this.UWDate = rs.getDate("UWDate");
			if( rs.getString("Signer") == null )
				this.Signer = null;
			else
				this.Signer = rs.getString("Signer").trim();

			this.SignerDate = rs.getDate("SignerDate");
			if( rs.getString("RiskCode") == null )
				this.RiskCode = null;
			else
				this.RiskCode = rs.getString("RiskCode").trim();

			if( rs.getString("OtherIDType") == null )
				this.OtherIDType = null;
			else
				this.OtherIDType = rs.getString("OtherIDType").trim();

			if( rs.getString("OtherIDNo") == null )
				this.OtherIDNo = null;
			else
				this.OtherIDNo = rs.getString("OtherIDNo").trim();

			if( rs.getString("GrpNo") == null )
				this.GrpNo = null;
			else
				this.GrpNo = rs.getString("GrpNo").trim();

			if( rs.getString("GrpName") == null )
				this.GrpName = null;
			else
				this.GrpName = rs.getString("GrpName").trim();

			if( rs.getString("ContDealFlag") == null )
				this.ContDealFlag = null;
			else
				this.ContDealFlag = rs.getString("ContDealFlag").trim();

			if( rs.getString("MobilePhone") == null )
				this.MobilePhone = null;
			else
				this.MobilePhone = rs.getString("MobilePhone").trim();

			if( rs.getString("PrePaidFlag") == null )
				this.PrePaidFlag = null;
			else
				this.PrePaidFlag = rs.getString("PrePaidFlag").trim();

			if( rs.getString("RelaDrawerInsured") == null )
				this.RelaDrawerInsured = null;
			else
				this.RelaDrawerInsured = rs.getString("RelaDrawerInsured").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LLCase表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLCaseSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LLCaseSchema getSchema()
	{
		LLCaseSchema aLLCaseSchema = new LLCaseSchema();
		aLLCaseSchema.setSchema(this);
		return aLLCaseSchema;
	}

	public LLCaseDB getDB()
	{
		LLCaseDB aDBOper = new LLCaseDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLCase描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(CaseNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RgtNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RgtType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RgtState)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccidentType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReceiptFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HospitalFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SurveyFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( RgtDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( HandleDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ClaimCalDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( AffixGetDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FeeInputFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( InHospitalDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( OutHospitalDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(InvaliHosDays));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(InHospitalDays));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( DianoseDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PostalAddress)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Phone)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( AccStartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( AccidentDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccidentSite)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( DeathDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustState)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccdentDesc)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CustBirthday ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerSex)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(CustomerAge));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IDType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IDNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Handler)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UWState)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Dealer)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppealFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TogetherGet)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpDealFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GetMode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(GetIntv));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CalFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UWFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DeclineFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EndCaseFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( EndCaseDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MngCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CaseGetMode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccModifyReason)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CaseNoDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CaseProp)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CancleReason)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CancleRemark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Cancler)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CancleDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Rigister)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Claimer)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UWer)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( UWDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Signer)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( SignerDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OtherIDType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OtherIDNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContDealFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MobilePhone)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PrePaidFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RelaDrawerInsured));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLCase>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			CaseNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			RgtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			RgtType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			RgtState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			CustomerName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			AccidentType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			ReceiptFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			HospitalFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			SurveyFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			RgtDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
			HandleDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
			ClaimCalDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,SysConst.PACKAGESPILTER));
			AffixGetDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,SysConst.PACKAGESPILTER));
			FeeInputFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			InHospitalDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,SysConst.PACKAGESPILTER));
			OutHospitalDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,SysConst.PACKAGESPILTER));
			InvaliHosDays= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,18,SysConst.PACKAGESPILTER))).intValue();
			InHospitalDays= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,19,SysConst.PACKAGESPILTER))).intValue();
			DianoseDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,SysConst.PACKAGESPILTER));
			PostalAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			Phone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			AccStartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,SysConst.PACKAGESPILTER));
			AccidentDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,SysConst.PACKAGESPILTER));
			AccidentSite = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			DeathDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26,SysConst.PACKAGESPILTER));
			CustState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			AccdentDesc = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			CustBirthday = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29,SysConst.PACKAGESPILTER));
			CustomerSex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			CustomerAge = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,31,SysConst.PACKAGESPILTER))).doubleValue();
			IDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
			IDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			Handler = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			UWState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			Dealer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
			AppealFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
			TogetherGet = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
			GrpDealFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
			GetMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
			GetIntv= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,41,SysConst.PACKAGESPILTER))).intValue();
			CalFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
			UWFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43, SysConst.PACKAGESPILTER );
			DeclineFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44, SysConst.PACKAGESPILTER );
			EndCaseFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 45, SysConst.PACKAGESPILTER );
			EndCaseDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 46,SysConst.PACKAGESPILTER));
			MngCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 47, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 48, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 49,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 50, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 51,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 52, SysConst.PACKAGESPILTER );
			BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 53, SysConst.PACKAGESPILTER );
			BankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 54, SysConst.PACKAGESPILTER );
			AccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 55, SysConst.PACKAGESPILTER );
			CaseGetMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 56, SysConst.PACKAGESPILTER );
			AccModifyReason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 57, SysConst.PACKAGESPILTER );
			CaseNoDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 58,SysConst.PACKAGESPILTER));
			CaseProp = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 59, SysConst.PACKAGESPILTER );
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 60, SysConst.PACKAGESPILTER );
			CancleReason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 61, SysConst.PACKAGESPILTER );
			CancleRemark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 62, SysConst.PACKAGESPILTER );
			Cancler = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 63, SysConst.PACKAGESPILTER );
			CancleDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 64,SysConst.PACKAGESPILTER));
			Rigister = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 65, SysConst.PACKAGESPILTER );
			Claimer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 66, SysConst.PACKAGESPILTER );
			UWer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 67, SysConst.PACKAGESPILTER );
			UWDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 68,SysConst.PACKAGESPILTER));
			Signer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 69, SysConst.PACKAGESPILTER );
			SignerDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 70,SysConst.PACKAGESPILTER));
			RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 71, SysConst.PACKAGESPILTER );
			OtherIDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 72, SysConst.PACKAGESPILTER );
			OtherIDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 73, SysConst.PACKAGESPILTER );
			GrpNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 74, SysConst.PACKAGESPILTER );
			GrpName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 75, SysConst.PACKAGESPILTER );
			ContDealFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 76, SysConst.PACKAGESPILTER );
			MobilePhone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 77, SysConst.PACKAGESPILTER );
			PrePaidFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 78, SysConst.PACKAGESPILTER );
			RelaDrawerInsured = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 79, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLCaseSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("CaseNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CaseNo));
		}
		if (FCode.equals("RgtNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RgtNo));
		}
		if (FCode.equals("RgtType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RgtType));
		}
		if (FCode.equals("RgtState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RgtState));
		}
		if (FCode.equals("CustomerNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
		}
		if (FCode.equals("CustomerName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerName));
		}
		if (FCode.equals("AccidentType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccidentType));
		}
		if (FCode.equals("ReceiptFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiptFlag));
		}
		if (FCode.equals("HospitalFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HospitalFlag));
		}
		if (FCode.equals("SurveyFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SurveyFlag));
		}
		if (FCode.equals("RgtDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getRgtDate()));
		}
		if (FCode.equals("HandleDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getHandleDate()));
		}
		if (FCode.equals("ClaimCalDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getClaimCalDate()));
		}
		if (FCode.equals("AffixGetDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getAffixGetDate()));
		}
		if (FCode.equals("FeeInputFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FeeInputFlag));
		}
		if (FCode.equals("InHospitalDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getInHospitalDate()));
		}
		if (FCode.equals("OutHospitalDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getOutHospitalDate()));
		}
		if (FCode.equals("InvaliHosDays"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InvaliHosDays));
		}
		if (FCode.equals("InHospitalDays"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InHospitalDays));
		}
		if (FCode.equals("DianoseDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getDianoseDate()));
		}
		if (FCode.equals("PostalAddress"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PostalAddress));
		}
		if (FCode.equals("Phone"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Phone));
		}
		if (FCode.equals("AccStartDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getAccStartDate()));
		}
		if (FCode.equals("AccidentDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getAccidentDate()));
		}
		if (FCode.equals("AccidentSite"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccidentSite));
		}
		if (FCode.equals("DeathDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getDeathDate()));
		}
		if (FCode.equals("CustState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustState));
		}
		if (FCode.equals("AccdentDesc"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccdentDesc));
		}
		if (FCode.equals("CustBirthday"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCustBirthday()));
		}
		if (FCode.equals("CustomerSex"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerSex));
		}
		if (FCode.equals("CustomerAge"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerAge));
		}
		if (FCode.equals("IDType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IDType));
		}
		if (FCode.equals("IDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
		}
		if (FCode.equals("Handler"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Handler));
		}
		if (FCode.equals("UWState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UWState));
		}
		if (FCode.equals("Dealer"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Dealer));
		}
		if (FCode.equals("AppealFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppealFlag));
		}
		if (FCode.equals("TogetherGet"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TogetherGet));
		}
		if (FCode.equals("GrpDealFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpDealFlag));
		}
		if (FCode.equals("GetMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GetMode));
		}
		if (FCode.equals("GetIntv"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GetIntv));
		}
		if (FCode.equals("CalFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CalFlag));
		}
		if (FCode.equals("UWFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UWFlag));
		}
		if (FCode.equals("DeclineFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DeclineFlag));
		}
		if (FCode.equals("EndCaseFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EndCaseFlag));
		}
		if (FCode.equals("EndCaseDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEndCaseDate()));
		}
		if (FCode.equals("MngCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MngCom));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("BankCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
		}
		if (FCode.equals("BankAccNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccNo));
		}
		if (FCode.equals("AccName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccName));
		}
		if (FCode.equals("CaseGetMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CaseGetMode));
		}
		if (FCode.equals("AccModifyReason"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccModifyReason));
		}
		if (FCode.equals("CaseNoDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCaseNoDate()));
		}
		if (FCode.equals("CaseProp"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CaseProp));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (FCode.equals("CancleReason"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CancleReason));
		}
		if (FCode.equals("CancleRemark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CancleRemark));
		}
		if (FCode.equals("Cancler"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Cancler));
		}
		if (FCode.equals("CancleDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCancleDate()));
		}
		if (FCode.equals("Rigister"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Rigister));
		}
		if (FCode.equals("Claimer"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Claimer));
		}
		if (FCode.equals("UWer"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UWer));
		}
		if (FCode.equals("UWDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getUWDate()));
		}
		if (FCode.equals("Signer"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Signer));
		}
		if (FCode.equals("SignerDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getSignerDate()));
		}
		if (FCode.equals("RiskCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
		}
		if (FCode.equals("OtherIDType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OtherIDType));
		}
		if (FCode.equals("OtherIDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OtherIDNo));
		}
		if (FCode.equals("GrpNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpNo));
		}
		if (FCode.equals("GrpName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpName));
		}
		if (FCode.equals("ContDealFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContDealFlag));
		}
		if (FCode.equals("MobilePhone"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MobilePhone));
		}
		if (FCode.equals("PrePaidFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrePaidFlag));
		}
		if (FCode.equals("RelaDrawerInsured"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RelaDrawerInsured));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(CaseNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(RgtNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(RgtType);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(RgtState);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(CustomerNo);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(CustomerName);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(AccidentType);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(ReceiptFlag);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(HospitalFlag);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(SurveyFlag);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getRgtDate()));
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getHandleDate()));
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getClaimCalDate()));
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getAffixGetDate()));
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(FeeInputFlag);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getInHospitalDate()));
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getOutHospitalDate()));
				break;
			case 17:
				strFieldValue = String.valueOf(InvaliHosDays);
				break;
			case 18:
				strFieldValue = String.valueOf(InHospitalDays);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getDianoseDate()));
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(PostalAddress);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(Phone);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getAccStartDate()));
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getAccidentDate()));
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(AccidentSite);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getDeathDate()));
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(CustState);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(AccdentDesc);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCustBirthday()));
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(CustomerSex);
				break;
			case 30:
				strFieldValue = String.valueOf(CustomerAge);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(IDType);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(IDNo);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(Handler);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(UWState);
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(Dealer);
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(AppealFlag);
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(TogetherGet);
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(GrpDealFlag);
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(GetMode);
				break;
			case 40:
				strFieldValue = String.valueOf(GetIntv);
				break;
			case 41:
				strFieldValue = StrTool.GBKToUnicode(CalFlag);
				break;
			case 42:
				strFieldValue = StrTool.GBKToUnicode(UWFlag);
				break;
			case 43:
				strFieldValue = StrTool.GBKToUnicode(DeclineFlag);
				break;
			case 44:
				strFieldValue = StrTool.GBKToUnicode(EndCaseFlag);
				break;
			case 45:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEndCaseDate()));
				break;
			case 46:
				strFieldValue = StrTool.GBKToUnicode(MngCom);
				break;
			case 47:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 48:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 49:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 50:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 51:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 52:
				strFieldValue = StrTool.GBKToUnicode(BankCode);
				break;
			case 53:
				strFieldValue = StrTool.GBKToUnicode(BankAccNo);
				break;
			case 54:
				strFieldValue = StrTool.GBKToUnicode(AccName);
				break;
			case 55:
				strFieldValue = StrTool.GBKToUnicode(CaseGetMode);
				break;
			case 56:
				strFieldValue = StrTool.GBKToUnicode(AccModifyReason);
				break;
			case 57:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCaseNoDate()));
				break;
			case 58:
				strFieldValue = StrTool.GBKToUnicode(CaseProp);
				break;
			case 59:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			case 60:
				strFieldValue = StrTool.GBKToUnicode(CancleReason);
				break;
			case 61:
				strFieldValue = StrTool.GBKToUnicode(CancleRemark);
				break;
			case 62:
				strFieldValue = StrTool.GBKToUnicode(Cancler);
				break;
			case 63:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCancleDate()));
				break;
			case 64:
				strFieldValue = StrTool.GBKToUnicode(Rigister);
				break;
			case 65:
				strFieldValue = StrTool.GBKToUnicode(Claimer);
				break;
			case 66:
				strFieldValue = StrTool.GBKToUnicode(UWer);
				break;
			case 67:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getUWDate()));
				break;
			case 68:
				strFieldValue = StrTool.GBKToUnicode(Signer);
				break;
			case 69:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getSignerDate()));
				break;
			case 70:
				strFieldValue = StrTool.GBKToUnicode(RiskCode);
				break;
			case 71:
				strFieldValue = StrTool.GBKToUnicode(OtherIDType);
				break;
			case 72:
				strFieldValue = StrTool.GBKToUnicode(OtherIDNo);
				break;
			case 73:
				strFieldValue = StrTool.GBKToUnicode(GrpNo);
				break;
			case 74:
				strFieldValue = StrTool.GBKToUnicode(GrpName);
				break;
			case 75:
				strFieldValue = StrTool.GBKToUnicode(ContDealFlag);
				break;
			case 76:
				strFieldValue = StrTool.GBKToUnicode(MobilePhone);
				break;
			case 77:
				strFieldValue = StrTool.GBKToUnicode(PrePaidFlag);
				break;
			case 78:
				strFieldValue = StrTool.GBKToUnicode(RelaDrawerInsured);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("CaseNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CaseNo = FValue.trim();
			}
			else
				CaseNo = null;
		}
		if (FCode.equalsIgnoreCase("RgtNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RgtNo = FValue.trim();
			}
			else
				RgtNo = null;
		}
		if (FCode.equalsIgnoreCase("RgtType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RgtType = FValue.trim();
			}
			else
				RgtType = null;
		}
		if (FCode.equalsIgnoreCase("RgtState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RgtState = FValue.trim();
			}
			else
				RgtState = null;
		}
		if (FCode.equalsIgnoreCase("CustomerNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerNo = FValue.trim();
			}
			else
				CustomerNo = null;
		}
		if (FCode.equalsIgnoreCase("CustomerName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerName = FValue.trim();
			}
			else
				CustomerName = null;
		}
		if (FCode.equalsIgnoreCase("AccidentType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccidentType = FValue.trim();
			}
			else
				AccidentType = null;
		}
		if (FCode.equalsIgnoreCase("ReceiptFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReceiptFlag = FValue.trim();
			}
			else
				ReceiptFlag = null;
		}
		if (FCode.equalsIgnoreCase("HospitalFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HospitalFlag = FValue.trim();
			}
			else
				HospitalFlag = null;
		}
		if (FCode.equalsIgnoreCase("SurveyFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SurveyFlag = FValue.trim();
			}
			else
				SurveyFlag = null;
		}
		if (FCode.equalsIgnoreCase("RgtDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				RgtDate = fDate.getDate( FValue );
			}
			else
				RgtDate = null;
		}
		if (FCode.equalsIgnoreCase("HandleDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				HandleDate = fDate.getDate( FValue );
			}
			else
				HandleDate = null;
		}
		if (FCode.equalsIgnoreCase("ClaimCalDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ClaimCalDate = fDate.getDate( FValue );
			}
			else
				ClaimCalDate = null;
		}
		if (FCode.equalsIgnoreCase("AffixGetDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				AffixGetDate = fDate.getDate( FValue );
			}
			else
				AffixGetDate = null;
		}
		if (FCode.equalsIgnoreCase("FeeInputFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FeeInputFlag = FValue.trim();
			}
			else
				FeeInputFlag = null;
		}
		if (FCode.equalsIgnoreCase("InHospitalDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				InHospitalDate = fDate.getDate( FValue );
			}
			else
				InHospitalDate = null;
		}
		if (FCode.equalsIgnoreCase("OutHospitalDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				OutHospitalDate = fDate.getDate( FValue );
			}
			else
				OutHospitalDate = null;
		}
		if (FCode.equalsIgnoreCase("InvaliHosDays"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				InvaliHosDays = i;
			}
		}
		if (FCode.equalsIgnoreCase("InHospitalDays"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				InHospitalDays = i;
			}
		}
		if (FCode.equalsIgnoreCase("DianoseDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				DianoseDate = fDate.getDate( FValue );
			}
			else
				DianoseDate = null;
		}
		if (FCode.equalsIgnoreCase("PostalAddress"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PostalAddress = FValue.trim();
			}
			else
				PostalAddress = null;
		}
		if (FCode.equalsIgnoreCase("Phone"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Phone = FValue.trim();
			}
			else
				Phone = null;
		}
		if (FCode.equalsIgnoreCase("AccStartDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				AccStartDate = fDate.getDate( FValue );
			}
			else
				AccStartDate = null;
		}
		if (FCode.equalsIgnoreCase("AccidentDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				AccidentDate = fDate.getDate( FValue );
			}
			else
				AccidentDate = null;
		}
		if (FCode.equalsIgnoreCase("AccidentSite"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccidentSite = FValue.trim();
			}
			else
				AccidentSite = null;
		}
		if (FCode.equalsIgnoreCase("DeathDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				DeathDate = fDate.getDate( FValue );
			}
			else
				DeathDate = null;
		}
		if (FCode.equalsIgnoreCase("CustState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustState = FValue.trim();
			}
			else
				CustState = null;
		}
		if (FCode.equalsIgnoreCase("AccdentDesc"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccdentDesc = FValue.trim();
			}
			else
				AccdentDesc = null;
		}
		if (FCode.equalsIgnoreCase("CustBirthday"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CustBirthday = fDate.getDate( FValue );
			}
			else
				CustBirthday = null;
		}
		if (FCode.equalsIgnoreCase("CustomerSex"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerSex = FValue.trim();
			}
			else
				CustomerSex = null;
		}
		if (FCode.equalsIgnoreCase("CustomerAge"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				CustomerAge = d;
			}
		}
		if (FCode.equalsIgnoreCase("IDType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IDType = FValue.trim();
			}
			else
				IDType = null;
		}
		if (FCode.equalsIgnoreCase("IDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IDNo = FValue.trim();
			}
			else
				IDNo = null;
		}
		if (FCode.equalsIgnoreCase("Handler"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Handler = FValue.trim();
			}
			else
				Handler = null;
		}
		if (FCode.equalsIgnoreCase("UWState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UWState = FValue.trim();
			}
			else
				UWState = null;
		}
		if (FCode.equalsIgnoreCase("Dealer"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Dealer = FValue.trim();
			}
			else
				Dealer = null;
		}
		if (FCode.equalsIgnoreCase("AppealFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppealFlag = FValue.trim();
			}
			else
				AppealFlag = null;
		}
		if (FCode.equalsIgnoreCase("TogetherGet"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TogetherGet = FValue.trim();
			}
			else
				TogetherGet = null;
		}
		if (FCode.equalsIgnoreCase("GrpDealFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpDealFlag = FValue.trim();
			}
			else
				GrpDealFlag = null;
		}
		if (FCode.equalsIgnoreCase("GetMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GetMode = FValue.trim();
			}
			else
				GetMode = null;
		}
		if (FCode.equalsIgnoreCase("GetIntv"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				GetIntv = i;
			}
		}
		if (FCode.equalsIgnoreCase("CalFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CalFlag = FValue.trim();
			}
			else
				CalFlag = null;
		}
		if (FCode.equalsIgnoreCase("UWFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UWFlag = FValue.trim();
			}
			else
				UWFlag = null;
		}
		if (FCode.equalsIgnoreCase("DeclineFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DeclineFlag = FValue.trim();
			}
			else
				DeclineFlag = null;
		}
		if (FCode.equalsIgnoreCase("EndCaseFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EndCaseFlag = FValue.trim();
			}
			else
				EndCaseFlag = null;
		}
		if (FCode.equalsIgnoreCase("EndCaseDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				EndCaseDate = fDate.getDate( FValue );
			}
			else
				EndCaseDate = null;
		}
		if (FCode.equalsIgnoreCase("MngCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MngCom = FValue.trim();
			}
			else
				MngCom = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("BankCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankCode = FValue.trim();
			}
			else
				BankCode = null;
		}
		if (FCode.equalsIgnoreCase("BankAccNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankAccNo = FValue.trim();
			}
			else
				BankAccNo = null;
		}
		if (FCode.equalsIgnoreCase("AccName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccName = FValue.trim();
			}
			else
				AccName = null;
		}
		if (FCode.equalsIgnoreCase("CaseGetMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CaseGetMode = FValue.trim();
			}
			else
				CaseGetMode = null;
		}
		if (FCode.equalsIgnoreCase("AccModifyReason"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccModifyReason = FValue.trim();
			}
			else
				AccModifyReason = null;
		}
		if (FCode.equalsIgnoreCase("CaseNoDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CaseNoDate = fDate.getDate( FValue );
			}
			else
				CaseNoDate = null;
		}
		if (FCode.equalsIgnoreCase("CaseProp"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CaseProp = FValue.trim();
			}
			else
				CaseProp = null;
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		if (FCode.equalsIgnoreCase("CancleReason"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CancleReason = FValue.trim();
			}
			else
				CancleReason = null;
		}
		if (FCode.equalsIgnoreCase("CancleRemark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CancleRemark = FValue.trim();
			}
			else
				CancleRemark = null;
		}
		if (FCode.equalsIgnoreCase("Cancler"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Cancler = FValue.trim();
			}
			else
				Cancler = null;
		}
		if (FCode.equalsIgnoreCase("CancleDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CancleDate = fDate.getDate( FValue );
			}
			else
				CancleDate = null;
		}
		if (FCode.equalsIgnoreCase("Rigister"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Rigister = FValue.trim();
			}
			else
				Rigister = null;
		}
		if (FCode.equalsIgnoreCase("Claimer"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Claimer = FValue.trim();
			}
			else
				Claimer = null;
		}
		if (FCode.equalsIgnoreCase("UWer"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UWer = FValue.trim();
			}
			else
				UWer = null;
		}
		if (FCode.equalsIgnoreCase("UWDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				UWDate = fDate.getDate( FValue );
			}
			else
				UWDate = null;
		}
		if (FCode.equalsIgnoreCase("Signer"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Signer = FValue.trim();
			}
			else
				Signer = null;
		}
		if (FCode.equalsIgnoreCase("SignerDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				SignerDate = fDate.getDate( FValue );
			}
			else
				SignerDate = null;
		}
		if (FCode.equalsIgnoreCase("RiskCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskCode = FValue.trim();
			}
			else
				RiskCode = null;
		}
		if (FCode.equalsIgnoreCase("OtherIDType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OtherIDType = FValue.trim();
			}
			else
				OtherIDType = null;
		}
		if (FCode.equalsIgnoreCase("OtherIDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OtherIDNo = FValue.trim();
			}
			else
				OtherIDNo = null;
		}
		if (FCode.equalsIgnoreCase("GrpNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpNo = FValue.trim();
			}
			else
				GrpNo = null;
		}
		if (FCode.equalsIgnoreCase("GrpName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpName = FValue.trim();
			}
			else
				GrpName = null;
		}
		if (FCode.equalsIgnoreCase("ContDealFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContDealFlag = FValue.trim();
			}
			else
				ContDealFlag = null;
		}
		if (FCode.equalsIgnoreCase("MobilePhone"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MobilePhone = FValue.trim();
			}
			else
				MobilePhone = null;
		}
		if (FCode.equalsIgnoreCase("PrePaidFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PrePaidFlag = FValue.trim();
			}
			else
				PrePaidFlag = null;
		}
		if (FCode.equalsIgnoreCase("RelaDrawerInsured"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RelaDrawerInsured = FValue.trim();
			}
			else
				RelaDrawerInsured = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LLCaseSchema other = (LLCaseSchema)otherObject;
		return
			(CaseNo == null ? other.getCaseNo() == null : CaseNo.equals(other.getCaseNo()))
			&& (RgtNo == null ? other.getRgtNo() == null : RgtNo.equals(other.getRgtNo()))
			&& (RgtType == null ? other.getRgtType() == null : RgtType.equals(other.getRgtType()))
			&& (RgtState == null ? other.getRgtState() == null : RgtState.equals(other.getRgtState()))
			&& (CustomerNo == null ? other.getCustomerNo() == null : CustomerNo.equals(other.getCustomerNo()))
			&& (CustomerName == null ? other.getCustomerName() == null : CustomerName.equals(other.getCustomerName()))
			&& (AccidentType == null ? other.getAccidentType() == null : AccidentType.equals(other.getAccidentType()))
			&& (ReceiptFlag == null ? other.getReceiptFlag() == null : ReceiptFlag.equals(other.getReceiptFlag()))
			&& (HospitalFlag == null ? other.getHospitalFlag() == null : HospitalFlag.equals(other.getHospitalFlag()))
			&& (SurveyFlag == null ? other.getSurveyFlag() == null : SurveyFlag.equals(other.getSurveyFlag()))
			&& (RgtDate == null ? other.getRgtDate() == null : fDate.getString(RgtDate).equals(other.getRgtDate()))
			&& (HandleDate == null ? other.getHandleDate() == null : fDate.getString(HandleDate).equals(other.getHandleDate()))
			&& (ClaimCalDate == null ? other.getClaimCalDate() == null : fDate.getString(ClaimCalDate).equals(other.getClaimCalDate()))
			&& (AffixGetDate == null ? other.getAffixGetDate() == null : fDate.getString(AffixGetDate).equals(other.getAffixGetDate()))
			&& (FeeInputFlag == null ? other.getFeeInputFlag() == null : FeeInputFlag.equals(other.getFeeInputFlag()))
			&& (InHospitalDate == null ? other.getInHospitalDate() == null : fDate.getString(InHospitalDate).equals(other.getInHospitalDate()))
			&& (OutHospitalDate == null ? other.getOutHospitalDate() == null : fDate.getString(OutHospitalDate).equals(other.getOutHospitalDate()))
			&& InvaliHosDays == other.getInvaliHosDays()
			&& InHospitalDays == other.getInHospitalDays()
			&& (DianoseDate == null ? other.getDianoseDate() == null : fDate.getString(DianoseDate).equals(other.getDianoseDate()))
			&& (PostalAddress == null ? other.getPostalAddress() == null : PostalAddress.equals(other.getPostalAddress()))
			&& (Phone == null ? other.getPhone() == null : Phone.equals(other.getPhone()))
			&& (AccStartDate == null ? other.getAccStartDate() == null : fDate.getString(AccStartDate).equals(other.getAccStartDate()))
			&& (AccidentDate == null ? other.getAccidentDate() == null : fDate.getString(AccidentDate).equals(other.getAccidentDate()))
			&& (AccidentSite == null ? other.getAccidentSite() == null : AccidentSite.equals(other.getAccidentSite()))
			&& (DeathDate == null ? other.getDeathDate() == null : fDate.getString(DeathDate).equals(other.getDeathDate()))
			&& (CustState == null ? other.getCustState() == null : CustState.equals(other.getCustState()))
			&& (AccdentDesc == null ? other.getAccdentDesc() == null : AccdentDesc.equals(other.getAccdentDesc()))
			&& (CustBirthday == null ? other.getCustBirthday() == null : fDate.getString(CustBirthday).equals(other.getCustBirthday()))
			&& (CustomerSex == null ? other.getCustomerSex() == null : CustomerSex.equals(other.getCustomerSex()))
			&& CustomerAge == other.getCustomerAge()
			&& (IDType == null ? other.getIDType() == null : IDType.equals(other.getIDType()))
			&& (IDNo == null ? other.getIDNo() == null : IDNo.equals(other.getIDNo()))
			&& (Handler == null ? other.getHandler() == null : Handler.equals(other.getHandler()))
			&& (UWState == null ? other.getUWState() == null : UWState.equals(other.getUWState()))
			&& (Dealer == null ? other.getDealer() == null : Dealer.equals(other.getDealer()))
			&& (AppealFlag == null ? other.getAppealFlag() == null : AppealFlag.equals(other.getAppealFlag()))
			&& (TogetherGet == null ? other.getTogetherGet() == null : TogetherGet.equals(other.getTogetherGet()))
			&& (GrpDealFlag == null ? other.getGrpDealFlag() == null : GrpDealFlag.equals(other.getGrpDealFlag()))
			&& (GetMode == null ? other.getGetMode() == null : GetMode.equals(other.getGetMode()))
			&& GetIntv == other.getGetIntv()
			&& (CalFlag == null ? other.getCalFlag() == null : CalFlag.equals(other.getCalFlag()))
			&& (UWFlag == null ? other.getUWFlag() == null : UWFlag.equals(other.getUWFlag()))
			&& (DeclineFlag == null ? other.getDeclineFlag() == null : DeclineFlag.equals(other.getDeclineFlag()))
			&& (EndCaseFlag == null ? other.getEndCaseFlag() == null : EndCaseFlag.equals(other.getEndCaseFlag()))
			&& (EndCaseDate == null ? other.getEndCaseDate() == null : fDate.getString(EndCaseDate).equals(other.getEndCaseDate()))
			&& (MngCom == null ? other.getMngCom() == null : MngCom.equals(other.getMngCom()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (BankCode == null ? other.getBankCode() == null : BankCode.equals(other.getBankCode()))
			&& (BankAccNo == null ? other.getBankAccNo() == null : BankAccNo.equals(other.getBankAccNo()))
			&& (AccName == null ? other.getAccName() == null : AccName.equals(other.getAccName()))
			&& (CaseGetMode == null ? other.getCaseGetMode() == null : CaseGetMode.equals(other.getCaseGetMode()))
			&& (AccModifyReason == null ? other.getAccModifyReason() == null : AccModifyReason.equals(other.getAccModifyReason()))
			&& (CaseNoDate == null ? other.getCaseNoDate() == null : fDate.getString(CaseNoDate).equals(other.getCaseNoDate()))
			&& (CaseProp == null ? other.getCaseProp() == null : CaseProp.equals(other.getCaseProp()))
			&& (Remark == null ? other.getRemark() == null : Remark.equals(other.getRemark()))
			&& (CancleReason == null ? other.getCancleReason() == null : CancleReason.equals(other.getCancleReason()))
			&& (CancleRemark == null ? other.getCancleRemark() == null : CancleRemark.equals(other.getCancleRemark()))
			&& (Cancler == null ? other.getCancler() == null : Cancler.equals(other.getCancler()))
			&& (CancleDate == null ? other.getCancleDate() == null : fDate.getString(CancleDate).equals(other.getCancleDate()))
			&& (Rigister == null ? other.getRigister() == null : Rigister.equals(other.getRigister()))
			&& (Claimer == null ? other.getClaimer() == null : Claimer.equals(other.getClaimer()))
			&& (UWer == null ? other.getUWer() == null : UWer.equals(other.getUWer()))
			&& (UWDate == null ? other.getUWDate() == null : fDate.getString(UWDate).equals(other.getUWDate()))
			&& (Signer == null ? other.getSigner() == null : Signer.equals(other.getSigner()))
			&& (SignerDate == null ? other.getSignerDate() == null : fDate.getString(SignerDate).equals(other.getSignerDate()))
			&& (RiskCode == null ? other.getRiskCode() == null : RiskCode.equals(other.getRiskCode()))
			&& (OtherIDType == null ? other.getOtherIDType() == null : OtherIDType.equals(other.getOtherIDType()))
			&& (OtherIDNo == null ? other.getOtherIDNo() == null : OtherIDNo.equals(other.getOtherIDNo()))
			&& (GrpNo == null ? other.getGrpNo() == null : GrpNo.equals(other.getGrpNo()))
			&& (GrpName == null ? other.getGrpName() == null : GrpName.equals(other.getGrpName()))
			&& (ContDealFlag == null ? other.getContDealFlag() == null : ContDealFlag.equals(other.getContDealFlag()))
			&& (MobilePhone == null ? other.getMobilePhone() == null : MobilePhone.equals(other.getMobilePhone()))
			&& (PrePaidFlag == null ? other.getPrePaidFlag() == null : PrePaidFlag.equals(other.getPrePaidFlag()))
			&& (RelaDrawerInsured == null ? other.getRelaDrawerInsured() == null : RelaDrawerInsured.equals(other.getRelaDrawerInsured()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("CaseNo") ) {
			return 0;
		}
		if( strFieldName.equals("RgtNo") ) {
			return 1;
		}
		if( strFieldName.equals("RgtType") ) {
			return 2;
		}
		if( strFieldName.equals("RgtState") ) {
			return 3;
		}
		if( strFieldName.equals("CustomerNo") ) {
			return 4;
		}
		if( strFieldName.equals("CustomerName") ) {
			return 5;
		}
		if( strFieldName.equals("AccidentType") ) {
			return 6;
		}
		if( strFieldName.equals("ReceiptFlag") ) {
			return 7;
		}
		if( strFieldName.equals("HospitalFlag") ) {
			return 8;
		}
		if( strFieldName.equals("SurveyFlag") ) {
			return 9;
		}
		if( strFieldName.equals("RgtDate") ) {
			return 10;
		}
		if( strFieldName.equals("HandleDate") ) {
			return 11;
		}
		if( strFieldName.equals("ClaimCalDate") ) {
			return 12;
		}
		if( strFieldName.equals("AffixGetDate") ) {
			return 13;
		}
		if( strFieldName.equals("FeeInputFlag") ) {
			return 14;
		}
		if( strFieldName.equals("InHospitalDate") ) {
			return 15;
		}
		if( strFieldName.equals("OutHospitalDate") ) {
			return 16;
		}
		if( strFieldName.equals("InvaliHosDays") ) {
			return 17;
		}
		if( strFieldName.equals("InHospitalDays") ) {
			return 18;
		}
		if( strFieldName.equals("DianoseDate") ) {
			return 19;
		}
		if( strFieldName.equals("PostalAddress") ) {
			return 20;
		}
		if( strFieldName.equals("Phone") ) {
			return 21;
		}
		if( strFieldName.equals("AccStartDate") ) {
			return 22;
		}
		if( strFieldName.equals("AccidentDate") ) {
			return 23;
		}
		if( strFieldName.equals("AccidentSite") ) {
			return 24;
		}
		if( strFieldName.equals("DeathDate") ) {
			return 25;
		}
		if( strFieldName.equals("CustState") ) {
			return 26;
		}
		if( strFieldName.equals("AccdentDesc") ) {
			return 27;
		}
		if( strFieldName.equals("CustBirthday") ) {
			return 28;
		}
		if( strFieldName.equals("CustomerSex") ) {
			return 29;
		}
		if( strFieldName.equals("CustomerAge") ) {
			return 30;
		}
		if( strFieldName.equals("IDType") ) {
			return 31;
		}
		if( strFieldName.equals("IDNo") ) {
			return 32;
		}
		if( strFieldName.equals("Handler") ) {
			return 33;
		}
		if( strFieldName.equals("UWState") ) {
			return 34;
		}
		if( strFieldName.equals("Dealer") ) {
			return 35;
		}
		if( strFieldName.equals("AppealFlag") ) {
			return 36;
		}
		if( strFieldName.equals("TogetherGet") ) {
			return 37;
		}
		if( strFieldName.equals("GrpDealFlag") ) {
			return 38;
		}
		if( strFieldName.equals("GetMode") ) {
			return 39;
		}
		if( strFieldName.equals("GetIntv") ) {
			return 40;
		}
		if( strFieldName.equals("CalFlag") ) {
			return 41;
		}
		if( strFieldName.equals("UWFlag") ) {
			return 42;
		}
		if( strFieldName.equals("DeclineFlag") ) {
			return 43;
		}
		if( strFieldName.equals("EndCaseFlag") ) {
			return 44;
		}
		if( strFieldName.equals("EndCaseDate") ) {
			return 45;
		}
		if( strFieldName.equals("MngCom") ) {
			return 46;
		}
		if( strFieldName.equals("Operator") ) {
			return 47;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 48;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 49;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 50;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 51;
		}
		if( strFieldName.equals("BankCode") ) {
			return 52;
		}
		if( strFieldName.equals("BankAccNo") ) {
			return 53;
		}
		if( strFieldName.equals("AccName") ) {
			return 54;
		}
		if( strFieldName.equals("CaseGetMode") ) {
			return 55;
		}
		if( strFieldName.equals("AccModifyReason") ) {
			return 56;
		}
		if( strFieldName.equals("CaseNoDate") ) {
			return 57;
		}
		if( strFieldName.equals("CaseProp") ) {
			return 58;
		}
		if( strFieldName.equals("Remark") ) {
			return 59;
		}
		if( strFieldName.equals("CancleReason") ) {
			return 60;
		}
		if( strFieldName.equals("CancleRemark") ) {
			return 61;
		}
		if( strFieldName.equals("Cancler") ) {
			return 62;
		}
		if( strFieldName.equals("CancleDate") ) {
			return 63;
		}
		if( strFieldName.equals("Rigister") ) {
			return 64;
		}
		if( strFieldName.equals("Claimer") ) {
			return 65;
		}
		if( strFieldName.equals("UWer") ) {
			return 66;
		}
		if( strFieldName.equals("UWDate") ) {
			return 67;
		}
		if( strFieldName.equals("Signer") ) {
			return 68;
		}
		if( strFieldName.equals("SignerDate") ) {
			return 69;
		}
		if( strFieldName.equals("RiskCode") ) {
			return 70;
		}
		if( strFieldName.equals("OtherIDType") ) {
			return 71;
		}
		if( strFieldName.equals("OtherIDNo") ) {
			return 72;
		}
		if( strFieldName.equals("GrpNo") ) {
			return 73;
		}
		if( strFieldName.equals("GrpName") ) {
			return 74;
		}
		if( strFieldName.equals("ContDealFlag") ) {
			return 75;
		}
		if( strFieldName.equals("MobilePhone") ) {
			return 76;
		}
		if( strFieldName.equals("PrePaidFlag") ) {
			return 77;
		}
		if( strFieldName.equals("RelaDrawerInsured") ) {
			return 78;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "CaseNo";
				break;
			case 1:
				strFieldName = "RgtNo";
				break;
			case 2:
				strFieldName = "RgtType";
				break;
			case 3:
				strFieldName = "RgtState";
				break;
			case 4:
				strFieldName = "CustomerNo";
				break;
			case 5:
				strFieldName = "CustomerName";
				break;
			case 6:
				strFieldName = "AccidentType";
				break;
			case 7:
				strFieldName = "ReceiptFlag";
				break;
			case 8:
				strFieldName = "HospitalFlag";
				break;
			case 9:
				strFieldName = "SurveyFlag";
				break;
			case 10:
				strFieldName = "RgtDate";
				break;
			case 11:
				strFieldName = "HandleDate";
				break;
			case 12:
				strFieldName = "ClaimCalDate";
				break;
			case 13:
				strFieldName = "AffixGetDate";
				break;
			case 14:
				strFieldName = "FeeInputFlag";
				break;
			case 15:
				strFieldName = "InHospitalDate";
				break;
			case 16:
				strFieldName = "OutHospitalDate";
				break;
			case 17:
				strFieldName = "InvaliHosDays";
				break;
			case 18:
				strFieldName = "InHospitalDays";
				break;
			case 19:
				strFieldName = "DianoseDate";
				break;
			case 20:
				strFieldName = "PostalAddress";
				break;
			case 21:
				strFieldName = "Phone";
				break;
			case 22:
				strFieldName = "AccStartDate";
				break;
			case 23:
				strFieldName = "AccidentDate";
				break;
			case 24:
				strFieldName = "AccidentSite";
				break;
			case 25:
				strFieldName = "DeathDate";
				break;
			case 26:
				strFieldName = "CustState";
				break;
			case 27:
				strFieldName = "AccdentDesc";
				break;
			case 28:
				strFieldName = "CustBirthday";
				break;
			case 29:
				strFieldName = "CustomerSex";
				break;
			case 30:
				strFieldName = "CustomerAge";
				break;
			case 31:
				strFieldName = "IDType";
				break;
			case 32:
				strFieldName = "IDNo";
				break;
			case 33:
				strFieldName = "Handler";
				break;
			case 34:
				strFieldName = "UWState";
				break;
			case 35:
				strFieldName = "Dealer";
				break;
			case 36:
				strFieldName = "AppealFlag";
				break;
			case 37:
				strFieldName = "TogetherGet";
				break;
			case 38:
				strFieldName = "GrpDealFlag";
				break;
			case 39:
				strFieldName = "GetMode";
				break;
			case 40:
				strFieldName = "GetIntv";
				break;
			case 41:
				strFieldName = "CalFlag";
				break;
			case 42:
				strFieldName = "UWFlag";
				break;
			case 43:
				strFieldName = "DeclineFlag";
				break;
			case 44:
				strFieldName = "EndCaseFlag";
				break;
			case 45:
				strFieldName = "EndCaseDate";
				break;
			case 46:
				strFieldName = "MngCom";
				break;
			case 47:
				strFieldName = "Operator";
				break;
			case 48:
				strFieldName = "MakeDate";
				break;
			case 49:
				strFieldName = "MakeTime";
				break;
			case 50:
				strFieldName = "ModifyDate";
				break;
			case 51:
				strFieldName = "ModifyTime";
				break;
			case 52:
				strFieldName = "BankCode";
				break;
			case 53:
				strFieldName = "BankAccNo";
				break;
			case 54:
				strFieldName = "AccName";
				break;
			case 55:
				strFieldName = "CaseGetMode";
				break;
			case 56:
				strFieldName = "AccModifyReason";
				break;
			case 57:
				strFieldName = "CaseNoDate";
				break;
			case 58:
				strFieldName = "CaseProp";
				break;
			case 59:
				strFieldName = "Remark";
				break;
			case 60:
				strFieldName = "CancleReason";
				break;
			case 61:
				strFieldName = "CancleRemark";
				break;
			case 62:
				strFieldName = "Cancler";
				break;
			case 63:
				strFieldName = "CancleDate";
				break;
			case 64:
				strFieldName = "Rigister";
				break;
			case 65:
				strFieldName = "Claimer";
				break;
			case 66:
				strFieldName = "UWer";
				break;
			case 67:
				strFieldName = "UWDate";
				break;
			case 68:
				strFieldName = "Signer";
				break;
			case 69:
				strFieldName = "SignerDate";
				break;
			case 70:
				strFieldName = "RiskCode";
				break;
			case 71:
				strFieldName = "OtherIDType";
				break;
			case 72:
				strFieldName = "OtherIDNo";
				break;
			case 73:
				strFieldName = "GrpNo";
				break;
			case 74:
				strFieldName = "GrpName";
				break;
			case 75:
				strFieldName = "ContDealFlag";
				break;
			case 76:
				strFieldName = "MobilePhone";
				break;
			case 77:
				strFieldName = "PrePaidFlag";
				break;
			case 78:
				strFieldName = "RelaDrawerInsured";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("CaseNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RgtNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RgtType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RgtState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccidentType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReceiptFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HospitalFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SurveyFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RgtDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("HandleDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ClaimCalDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("AffixGetDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("FeeInputFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InHospitalDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("OutHospitalDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("InvaliHosDays") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("InHospitalDays") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("DianoseDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("PostalAddress") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Phone") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccStartDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("AccidentDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("AccidentSite") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DeathDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("CustState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccdentDesc") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustBirthday") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("CustomerSex") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerAge") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("IDType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IDNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Handler") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UWState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Dealer") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppealFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TogetherGet") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpDealFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GetMode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GetIntv") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("CalFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UWFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DeclineFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EndCaseFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EndCaseDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MngCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankAccNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CaseGetMode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccModifyReason") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CaseNoDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("CaseProp") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CancleReason") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CancleRemark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Cancler") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CancleDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Rigister") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Claimer") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UWer") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UWDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Signer") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SignerDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("RiskCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OtherIDType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OtherIDNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContDealFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MobilePhone") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PrePaidFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RelaDrawerInsured") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 12:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 13:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 16:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 17:
				nFieldType = Schema.TYPE_INT;
				break;
			case 18:
				nFieldType = Schema.TYPE_INT;
				break;
			case 19:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 23:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 36:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 37:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 38:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 39:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 40:
				nFieldType = Schema.TYPE_INT;
				break;
			case 41:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 42:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 43:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 44:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 45:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 46:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 47:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 48:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 49:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 50:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 51:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 52:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 53:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 54:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 55:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 56:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 57:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 58:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 59:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 60:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 61:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 62:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 63:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 64:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 65:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 66:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 67:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 68:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 69:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 70:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 71:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 72:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 73:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 74:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 75:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 76:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 77:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 78:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
