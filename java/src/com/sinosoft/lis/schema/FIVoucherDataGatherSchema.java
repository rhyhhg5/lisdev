/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.FIVoucherDataGatherDB;

/*
 * <p>ClassName: FIVoucherDataGatherSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_Update
 * @CreateDate：2014-09-10
 */
public class FIVoucherDataGatherSchema implements Schema, Cloneable
{
	// @Field
	/** 批次号 */
	private String BatchNo;
	/** 版本编号 */
	private String VersionNo;
	/** 汇总数据流水号 */
	private String VSerialNo;
	/** 核心业务系统凭证类型 */
	private String ImportType;
	/** 借贷标识 */
	private String DCFlag;
	/** 凭证类型 */
	private String VoucherType;
	/** 币种 */
	private String Currency;
	/** 机构代码 */
	private String DepCode;
	/** 对方机构 */
	private String FinMangerCount;
	/** 科目代码 */
	private String AccountCode;
	/** 客户 */
	private String BClient;
	/** 成本中心 */
	private String CostCenter;
	/** 险种编码 */
	private String RiskCode;
	/** 市场类型 */
	private String MarketType;
	/** 渠道 */
	private String Chinal;
	/** 期数 */
	private String PCont;
	/** 现金流量码 */
	private String CashFlowNo;
	/** 记帐日期 */
	private Date ChargeDate;
	/** 记帐时间 */
	private String ChargeTime;
	/** 金额 */
	private double SumMoney;
	/** 凭证年度 */
	private String VoucherYear;
	/** 保单年度 */
	private int PolYear;
	/** 保险年期 */
	private int Years;
	/** 供应商号 */
	private String AgentNo;
	/** 保费收入类型 */
	private String PremiumType;
	/** 计费首年 */
	private String FirstYear;
	/** 备用字符串一 */
	private String StandByString1;
	/** 备用字符串二 */
	private String StandByString2;
	/** 备用字符串三 */
	private String StandByString3;
	/** 备用数字一 */
	private double StandByNum1;
	/** 备用数字二 */
	private double StandByNum2;
	/** 备用日期一 */
	private Date StandByDate1;
	/** 备用日期二 */
	private Date StandByDate2;
	/** 读取状态 */
	private String ReadState;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 操作员 */
	private String Operator;
	/** 保单号 */
	private String ContNo;

	public static final int FIELDNUM = 40;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public FIVoucherDataGatherSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "VSerialNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		FIVoucherDataGatherSchema cloned = (FIVoucherDataGatherSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getBatchNo()
	{
		return BatchNo;
	}
	public void setBatchNo(String aBatchNo)
	{
		BatchNo = aBatchNo;
	}
	public String getVersionNo()
	{
		return VersionNo;
	}
	public void setVersionNo(String aVersionNo)
	{
		VersionNo = aVersionNo;
	}
	public String getVSerialNo()
	{
		return VSerialNo;
	}
	public void setVSerialNo(String aVSerialNo)
	{
		VSerialNo = aVSerialNo;
	}
	public String getImportType()
	{
		return ImportType;
	}
	public void setImportType(String aImportType)
	{
		ImportType = aImportType;
	}
	public String getDCFlag()
	{
		return DCFlag;
	}
	public void setDCFlag(String aDCFlag)
	{
		DCFlag = aDCFlag;
	}
	public String getVoucherType()
	{
		return VoucherType;
	}
	public void setVoucherType(String aVoucherType)
	{
		VoucherType = aVoucherType;
	}
	public String getCurrency()
	{
		return Currency;
	}
	public void setCurrency(String aCurrency)
	{
		Currency = aCurrency;
	}
	public String getDepCode()
	{
		return DepCode;
	}
	public void setDepCode(String aDepCode)
	{
		DepCode = aDepCode;
	}
	public String getFinMangerCount()
	{
		return FinMangerCount;
	}
	public void setFinMangerCount(String aFinMangerCount)
	{
		FinMangerCount = aFinMangerCount;
	}
	public String getAccountCode()
	{
		return AccountCode;
	}
	public void setAccountCode(String aAccountCode)
	{
		AccountCode = aAccountCode;
	}
	public String getBClient()
	{
		return BClient;
	}
	public void setBClient(String aBClient)
	{
		BClient = aBClient;
	}
	public String getCostCenter()
	{
		return CostCenter;
	}
	public void setCostCenter(String aCostCenter)
	{
		CostCenter = aCostCenter;
	}
	public String getRiskCode()
	{
		return RiskCode;
	}
	public void setRiskCode(String aRiskCode)
	{
		RiskCode = aRiskCode;
	}
	public String getMarketType()
	{
		return MarketType;
	}
	public void setMarketType(String aMarketType)
	{
		MarketType = aMarketType;
	}
	public String getChinal()
	{
		return Chinal;
	}
	public void setChinal(String aChinal)
	{
		Chinal = aChinal;
	}
	public String getPCont()
	{
		return PCont;
	}
	public void setPCont(String aPCont)
	{
		PCont = aPCont;
	}
	public String getCashFlowNo()
	{
		return CashFlowNo;
	}
	public void setCashFlowNo(String aCashFlowNo)
	{
		CashFlowNo = aCashFlowNo;
	}
	public String getChargeDate()
	{
		if( ChargeDate != null )
			return fDate.getString(ChargeDate);
		else
			return null;
	}
	public void setChargeDate(Date aChargeDate)
	{
		ChargeDate = aChargeDate;
	}
	public void setChargeDate(String aChargeDate)
	{
		if (aChargeDate != null && !aChargeDate.equals("") )
		{
			ChargeDate = fDate.getDate( aChargeDate );
		}
		else
			ChargeDate = null;
	}

	public String getChargeTime()
	{
		return ChargeTime;
	}
	public void setChargeTime(String aChargeTime)
	{
		ChargeTime = aChargeTime;
	}
	public double getSumMoney()
	{
		return SumMoney;
	}
	public void setSumMoney(double aSumMoney)
	{
		SumMoney = Arith.round(aSumMoney,2);
	}
	public void setSumMoney(String aSumMoney)
	{
		if (aSumMoney != null && !aSumMoney.equals(""))
		{
			Double tDouble = new Double(aSumMoney);
			double d = tDouble.doubleValue();
                SumMoney = Arith.round(d,2);
		}
	}

	public String getVoucherYear()
	{
		return VoucherYear;
	}
	public void setVoucherYear(String aVoucherYear)
	{
		VoucherYear = aVoucherYear;
	}
	public int getPolYear()
	{
		return PolYear;
	}
	public void setPolYear(int aPolYear)
	{
		PolYear = aPolYear;
	}
	public void setPolYear(String aPolYear)
	{
		if (aPolYear != null && !aPolYear.equals(""))
		{
			Integer tInteger = new Integer(aPolYear);
			int i = tInteger.intValue();
			PolYear = i;
		}
	}

	public int getYears()
	{
		return Years;
	}
	public void setYears(int aYears)
	{
		Years = aYears;
	}
	public void setYears(String aYears)
	{
		if (aYears != null && !aYears.equals(""))
		{
			Integer tInteger = new Integer(aYears);
			int i = tInteger.intValue();
			Years = i;
		}
	}

	public String getAgentNo()
	{
		return AgentNo;
	}
	public void setAgentNo(String aAgentNo)
	{
		AgentNo = aAgentNo;
	}
	public String getPremiumType()
	{
		return PremiumType;
	}
	public void setPremiumType(String aPremiumType)
	{
		PremiumType = aPremiumType;
	}
	public String getFirstYear()
	{
		return FirstYear;
	}
	public void setFirstYear(String aFirstYear)
	{
		FirstYear = aFirstYear;
	}
	public String getStandByString1()
	{
		return StandByString1;
	}
	public void setStandByString1(String aStandByString1)
	{
		StandByString1 = aStandByString1;
	}
	public String getStandByString2()
	{
		return StandByString2;
	}
	public void setStandByString2(String aStandByString2)
	{
		StandByString2 = aStandByString2;
	}
	public String getStandByString3()
	{
		return StandByString3;
	}
	public void setStandByString3(String aStandByString3)
	{
		StandByString3 = aStandByString3;
	}
	public double getStandByNum1()
	{
		return StandByNum1;
	}
	public void setStandByNum1(double aStandByNum1)
	{
		StandByNum1 = Arith.round(aStandByNum1,2);
	}
	public void setStandByNum1(String aStandByNum1)
	{
		if (aStandByNum1 != null && !aStandByNum1.equals(""))
		{
			Double tDouble = new Double(aStandByNum1);
			double d = tDouble.doubleValue();
                StandByNum1 = Arith.round(d,2);
		}
	}

	public double getStandByNum2()
	{
		return StandByNum2;
	}
	public void setStandByNum2(double aStandByNum2)
	{
		StandByNum2 = Arith.round(aStandByNum2,2);
	}
	public void setStandByNum2(String aStandByNum2)
	{
		if (aStandByNum2 != null && !aStandByNum2.equals(""))
		{
			Double tDouble = new Double(aStandByNum2);
			double d = tDouble.doubleValue();
                StandByNum2 = Arith.round(d,2);
		}
	}

	public String getStandByDate1()
	{
		if( StandByDate1 != null )
			return fDate.getString(StandByDate1);
		else
			return null;
	}
	public void setStandByDate1(Date aStandByDate1)
	{
		StandByDate1 = aStandByDate1;
	}
	public void setStandByDate1(String aStandByDate1)
	{
		if (aStandByDate1 != null && !aStandByDate1.equals("") )
		{
			StandByDate1 = fDate.getDate( aStandByDate1 );
		}
		else
			StandByDate1 = null;
	}

	public String getStandByDate2()
	{
		if( StandByDate2 != null )
			return fDate.getString(StandByDate2);
		else
			return null;
	}
	public void setStandByDate2(Date aStandByDate2)
	{
		StandByDate2 = aStandByDate2;
	}
	public void setStandByDate2(String aStandByDate2)
	{
		if (aStandByDate2 != null && !aStandByDate2.equals("") )
		{
			StandByDate2 = fDate.getDate( aStandByDate2 );
		}
		else
			StandByDate2 = null;
	}

	public String getReadState()
	{
		return ReadState;
	}
	public void setReadState(String aReadState)
	{
		ReadState = aReadState;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getContNo()
	{
		return ContNo;
	}
	public void setContNo(String aContNo)
	{
		ContNo = aContNo;
	}

	/**
	* 使用另外一个 FIVoucherDataGatherSchema 对象给 Schema 赋值
	* @param: aFIVoucherDataGatherSchema FIVoucherDataGatherSchema
	**/
	public void setSchema(FIVoucherDataGatherSchema aFIVoucherDataGatherSchema)
	{
		this.BatchNo = aFIVoucherDataGatherSchema.getBatchNo();
		this.VersionNo = aFIVoucherDataGatherSchema.getVersionNo();
		this.VSerialNo = aFIVoucherDataGatherSchema.getVSerialNo();
		this.ImportType = aFIVoucherDataGatherSchema.getImportType();
		this.DCFlag = aFIVoucherDataGatherSchema.getDCFlag();
		this.VoucherType = aFIVoucherDataGatherSchema.getVoucherType();
		this.Currency = aFIVoucherDataGatherSchema.getCurrency();
		this.DepCode = aFIVoucherDataGatherSchema.getDepCode();
		this.FinMangerCount = aFIVoucherDataGatherSchema.getFinMangerCount();
		this.AccountCode = aFIVoucherDataGatherSchema.getAccountCode();
		this.BClient = aFIVoucherDataGatherSchema.getBClient();
		this.CostCenter = aFIVoucherDataGatherSchema.getCostCenter();
		this.RiskCode = aFIVoucherDataGatherSchema.getRiskCode();
		this.MarketType = aFIVoucherDataGatherSchema.getMarketType();
		this.Chinal = aFIVoucherDataGatherSchema.getChinal();
		this.PCont = aFIVoucherDataGatherSchema.getPCont();
		this.CashFlowNo = aFIVoucherDataGatherSchema.getCashFlowNo();
		this.ChargeDate = fDate.getDate( aFIVoucherDataGatherSchema.getChargeDate());
		this.ChargeTime = aFIVoucherDataGatherSchema.getChargeTime();
		this.SumMoney = aFIVoucherDataGatherSchema.getSumMoney();
		this.VoucherYear = aFIVoucherDataGatherSchema.getVoucherYear();
		this.PolYear = aFIVoucherDataGatherSchema.getPolYear();
		this.Years = aFIVoucherDataGatherSchema.getYears();
		this.AgentNo = aFIVoucherDataGatherSchema.getAgentNo();
		this.PremiumType = aFIVoucherDataGatherSchema.getPremiumType();
		this.FirstYear = aFIVoucherDataGatherSchema.getFirstYear();
		this.StandByString1 = aFIVoucherDataGatherSchema.getStandByString1();
		this.StandByString2 = aFIVoucherDataGatherSchema.getStandByString2();
		this.StandByString3 = aFIVoucherDataGatherSchema.getStandByString3();
		this.StandByNum1 = aFIVoucherDataGatherSchema.getStandByNum1();
		this.StandByNum2 = aFIVoucherDataGatherSchema.getStandByNum2();
		this.StandByDate1 = fDate.getDate( aFIVoucherDataGatherSchema.getStandByDate1());
		this.StandByDate2 = fDate.getDate( aFIVoucherDataGatherSchema.getStandByDate2());
		this.ReadState = aFIVoucherDataGatherSchema.getReadState();
		this.MakeDate = fDate.getDate( aFIVoucherDataGatherSchema.getMakeDate());
		this.MakeTime = aFIVoucherDataGatherSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aFIVoucherDataGatherSchema.getModifyDate());
		this.ModifyTime = aFIVoucherDataGatherSchema.getModifyTime();
		this.Operator = aFIVoucherDataGatherSchema.getOperator();
		this.ContNo = aFIVoucherDataGatherSchema.getContNo();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("BatchNo") == null )
				this.BatchNo = null;
			else
				this.BatchNo = rs.getString("BatchNo").trim();

			if( rs.getString("VersionNo") == null )
				this.VersionNo = null;
			else
				this.VersionNo = rs.getString("VersionNo").trim();

			if( rs.getString("VSerialNo") == null )
				this.VSerialNo = null;
			else
				this.VSerialNo = rs.getString("VSerialNo").trim();

			if( rs.getString("ImportType") == null )
				this.ImportType = null;
			else
				this.ImportType = rs.getString("ImportType").trim();

			if( rs.getString("DCFlag") == null )
				this.DCFlag = null;
			else
				this.DCFlag = rs.getString("DCFlag").trim();

			if( rs.getString("VoucherType") == null )
				this.VoucherType = null;
			else
				this.VoucherType = rs.getString("VoucherType").trim();

			if( rs.getString("Currency") == null )
				this.Currency = null;
			else
				this.Currency = rs.getString("Currency").trim();

			if( rs.getString("DepCode") == null )
				this.DepCode = null;
			else
				this.DepCode = rs.getString("DepCode").trim();

			if( rs.getString("FinMangerCount") == null )
				this.FinMangerCount = null;
			else
				this.FinMangerCount = rs.getString("FinMangerCount").trim();

			if( rs.getString("AccountCode") == null )
				this.AccountCode = null;
			else
				this.AccountCode = rs.getString("AccountCode").trim();

			if( rs.getString("BClient") == null )
				this.BClient = null;
			else
				this.BClient = rs.getString("BClient").trim();

			if( rs.getString("CostCenter") == null )
				this.CostCenter = null;
			else
				this.CostCenter = rs.getString("CostCenter").trim();

			if( rs.getString("RiskCode") == null )
				this.RiskCode = null;
			else
				this.RiskCode = rs.getString("RiskCode").trim();

			if( rs.getString("MarketType") == null )
				this.MarketType = null;
			else
				this.MarketType = rs.getString("MarketType").trim();

			if( rs.getString("Chinal") == null )
				this.Chinal = null;
			else
				this.Chinal = rs.getString("Chinal").trim();

			if( rs.getString("PCont") == null )
				this.PCont = null;
			else
				this.PCont = rs.getString("PCont").trim();

			if( rs.getString("CashFlowNo") == null )
				this.CashFlowNo = null;
			else
				this.CashFlowNo = rs.getString("CashFlowNo").trim();

			this.ChargeDate = rs.getDate("ChargeDate");
			if( rs.getString("ChargeTime") == null )
				this.ChargeTime = null;
			else
				this.ChargeTime = rs.getString("ChargeTime").trim();

			this.SumMoney = rs.getDouble("SumMoney");
			if( rs.getString("VoucherYear") == null )
				this.VoucherYear = null;
			else
				this.VoucherYear = rs.getString("VoucherYear").trim();

			this.PolYear = rs.getInt("PolYear");
			this.Years = rs.getInt("Years");
			if( rs.getString("AgentNo") == null )
				this.AgentNo = null;
			else
				this.AgentNo = rs.getString("AgentNo").trim();

			if( rs.getString("PremiumType") == null )
				this.PremiumType = null;
			else
				this.PremiumType = rs.getString("PremiumType").trim();

			if( rs.getString("FirstYear") == null )
				this.FirstYear = null;
			else
				this.FirstYear = rs.getString("FirstYear").trim();

			if( rs.getString("StandByString1") == null )
				this.StandByString1 = null;
			else
				this.StandByString1 = rs.getString("StandByString1").trim();

			if( rs.getString("StandByString2") == null )
				this.StandByString2 = null;
			else
				this.StandByString2 = rs.getString("StandByString2").trim();

			if( rs.getString("StandByString3") == null )
				this.StandByString3 = null;
			else
				this.StandByString3 = rs.getString("StandByString3").trim();

			this.StandByNum1 = rs.getDouble("StandByNum1");
			this.StandByNum2 = rs.getDouble("StandByNum2");
			this.StandByDate1 = rs.getDate("StandByDate1");
			this.StandByDate2 = rs.getDate("StandByDate2");
			if( rs.getString("ReadState") == null )
				this.ReadState = null;
			else
				this.ReadState = rs.getString("ReadState").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			if( rs.getString("ContNo") == null )
				this.ContNo = null;
			else
				this.ContNo = rs.getString("ContNo").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的FIVoucherDataGather表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIVoucherDataGatherSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public FIVoucherDataGatherSchema getSchema()
	{
		FIVoucherDataGatherSchema aFIVoucherDataGatherSchema = new FIVoucherDataGatherSchema();
		aFIVoucherDataGatherSchema.setSchema(this);
		return aFIVoucherDataGatherSchema;
	}

	public FIVoucherDataGatherDB getDB()
	{
		FIVoucherDataGatherDB aDBOper = new FIVoucherDataGatherDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpFIVoucherDataGather描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(BatchNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(VersionNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(VSerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ImportType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DCFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(VoucherType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Currency)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DepCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FinMangerCount)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccountCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BClient)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CostCenter)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MarketType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Chinal)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PCont)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CashFlowNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ChargeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ChargeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SumMoney));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(VoucherYear)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PolYear));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Years));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PremiumType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FirstYear)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandByString1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandByString2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandByString3)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(StandByNum1));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(StandByNum2));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( StandByDate1 ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( StandByDate2 ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReadState)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContNo));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpFIVoucherDataGather>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			BatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			VersionNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			VSerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			ImportType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			DCFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			VoucherType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			Currency = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			DepCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			FinMangerCount = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			AccountCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			BClient = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			CostCenter = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			MarketType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			Chinal = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			PCont = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			CashFlowNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			ChargeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,SysConst.PACKAGESPILTER));
			ChargeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			SumMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,20,SysConst.PACKAGESPILTER))).doubleValue();
			VoucherYear = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			PolYear= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,22,SysConst.PACKAGESPILTER))).intValue();
			Years= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,23,SysConst.PACKAGESPILTER))).intValue();
			AgentNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			PremiumType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			FirstYear = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			StandByString1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			StandByString2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			StandByString3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			StandByNum1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,30,SysConst.PACKAGESPILTER))).doubleValue();
			StandByNum2 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,31,SysConst.PACKAGESPILTER))).doubleValue();
			StandByDate1 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32,SysConst.PACKAGESPILTER));
			StandByDate2 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33,SysConst.PACKAGESPILTER));
			ReadState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
			ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIVoucherDataGatherSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("BatchNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BatchNo));
		}
		if (FCode.equals("VersionNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(VersionNo));
		}
		if (FCode.equals("VSerialNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(VSerialNo));
		}
		if (FCode.equals("ImportType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ImportType));
		}
		if (FCode.equals("DCFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DCFlag));
		}
		if (FCode.equals("VoucherType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(VoucherType));
		}
		if (FCode.equals("Currency"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Currency));
		}
		if (FCode.equals("DepCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DepCode));
		}
		if (FCode.equals("FinMangerCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FinMangerCount));
		}
		if (FCode.equals("AccountCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccountCode));
		}
		if (FCode.equals("BClient"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BClient));
		}
		if (FCode.equals("CostCenter"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CostCenter));
		}
		if (FCode.equals("RiskCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
		}
		if (FCode.equals("MarketType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MarketType));
		}
		if (FCode.equals("Chinal"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Chinal));
		}
		if (FCode.equals("PCont"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PCont));
		}
		if (FCode.equals("CashFlowNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CashFlowNo));
		}
		if (FCode.equals("ChargeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getChargeDate()));
		}
		if (FCode.equals("ChargeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ChargeTime));
		}
		if (FCode.equals("SumMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SumMoney));
		}
		if (FCode.equals("VoucherYear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(VoucherYear));
		}
		if (FCode.equals("PolYear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolYear));
		}
		if (FCode.equals("Years"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Years));
		}
		if (FCode.equals("AgentNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentNo));
		}
		if (FCode.equals("PremiumType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PremiumType));
		}
		if (FCode.equals("FirstYear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FirstYear));
		}
		if (FCode.equals("StandByString1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandByString1));
		}
		if (FCode.equals("StandByString2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandByString2));
		}
		if (FCode.equals("StandByString3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandByString3));
		}
		if (FCode.equals("StandByNum1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandByNum1));
		}
		if (FCode.equals("StandByNum2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandByNum2));
		}
		if (FCode.equals("StandByDate1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStandByDate1()));
		}
		if (FCode.equals("StandByDate2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStandByDate2()));
		}
		if (FCode.equals("ReadState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReadState));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("ContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(BatchNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(VersionNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(VSerialNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(ImportType);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(DCFlag);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(VoucherType);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(Currency);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(DepCode);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(FinMangerCount);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(AccountCode);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(BClient);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(CostCenter);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(RiskCode);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(MarketType);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(Chinal);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(PCont);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(CashFlowNo);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getChargeDate()));
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(ChargeTime);
				break;
			case 19:
				strFieldValue = String.valueOf(SumMoney);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(VoucherYear);
				break;
			case 21:
				strFieldValue = String.valueOf(PolYear);
				break;
			case 22:
				strFieldValue = String.valueOf(Years);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(AgentNo);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(PremiumType);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(FirstYear);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(StandByString1);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(StandByString2);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(StandByString3);
				break;
			case 29:
				strFieldValue = String.valueOf(StandByNum1);
				break;
			case 30:
				strFieldValue = String.valueOf(StandByNum2);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStandByDate1()));
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStandByDate2()));
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(ReadState);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(ContNo);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("BatchNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BatchNo = FValue.trim();
			}
			else
				BatchNo = null;
		}
		if (FCode.equalsIgnoreCase("VersionNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				VersionNo = FValue.trim();
			}
			else
				VersionNo = null;
		}
		if (FCode.equalsIgnoreCase("VSerialNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				VSerialNo = FValue.trim();
			}
			else
				VSerialNo = null;
		}
		if (FCode.equalsIgnoreCase("ImportType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ImportType = FValue.trim();
			}
			else
				ImportType = null;
		}
		if (FCode.equalsIgnoreCase("DCFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DCFlag = FValue.trim();
			}
			else
				DCFlag = null;
		}
		if (FCode.equalsIgnoreCase("VoucherType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				VoucherType = FValue.trim();
			}
			else
				VoucherType = null;
		}
		if (FCode.equalsIgnoreCase("Currency"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Currency = FValue.trim();
			}
			else
				Currency = null;
		}
		if (FCode.equalsIgnoreCase("DepCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DepCode = FValue.trim();
			}
			else
				DepCode = null;
		}
		if (FCode.equalsIgnoreCase("FinMangerCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FinMangerCount = FValue.trim();
			}
			else
				FinMangerCount = null;
		}
		if (FCode.equalsIgnoreCase("AccountCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccountCode = FValue.trim();
			}
			else
				AccountCode = null;
		}
		if (FCode.equalsIgnoreCase("BClient"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BClient = FValue.trim();
			}
			else
				BClient = null;
		}
		if (FCode.equalsIgnoreCase("CostCenter"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CostCenter = FValue.trim();
			}
			else
				CostCenter = null;
		}
		if (FCode.equalsIgnoreCase("RiskCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskCode = FValue.trim();
			}
			else
				RiskCode = null;
		}
		if (FCode.equalsIgnoreCase("MarketType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MarketType = FValue.trim();
			}
			else
				MarketType = null;
		}
		if (FCode.equalsIgnoreCase("Chinal"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Chinal = FValue.trim();
			}
			else
				Chinal = null;
		}
		if (FCode.equalsIgnoreCase("PCont"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PCont = FValue.trim();
			}
			else
				PCont = null;
		}
		if (FCode.equalsIgnoreCase("CashFlowNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CashFlowNo = FValue.trim();
			}
			else
				CashFlowNo = null;
		}
		if (FCode.equalsIgnoreCase("ChargeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ChargeDate = fDate.getDate( FValue );
			}
			else
				ChargeDate = null;
		}
		if (FCode.equalsIgnoreCase("ChargeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ChargeTime = FValue.trim();
			}
			else
				ChargeTime = null;
		}
		if (FCode.equalsIgnoreCase("SumMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SumMoney = d;
			}
		}
		if (FCode.equalsIgnoreCase("VoucherYear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				VoucherYear = FValue.trim();
			}
			else
				VoucherYear = null;
		}
		if (FCode.equalsIgnoreCase("PolYear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PolYear = i;
			}
		}
		if (FCode.equalsIgnoreCase("Years"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				Years = i;
			}
		}
		if (FCode.equalsIgnoreCase("AgentNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentNo = FValue.trim();
			}
			else
				AgentNo = null;
		}
		if (FCode.equalsIgnoreCase("PremiumType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PremiumType = FValue.trim();
			}
			else
				PremiumType = null;
		}
		if (FCode.equalsIgnoreCase("FirstYear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FirstYear = FValue.trim();
			}
			else
				FirstYear = null;
		}
		if (FCode.equalsIgnoreCase("StandByString1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandByString1 = FValue.trim();
			}
			else
				StandByString1 = null;
		}
		if (FCode.equalsIgnoreCase("StandByString2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandByString2 = FValue.trim();
			}
			else
				StandByString2 = null;
		}
		if (FCode.equalsIgnoreCase("StandByString3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandByString3 = FValue.trim();
			}
			else
				StandByString3 = null;
		}
		if (FCode.equalsIgnoreCase("StandByNum1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				StandByNum1 = d;
			}
		}
		if (FCode.equalsIgnoreCase("StandByNum2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				StandByNum2 = d;
			}
		}
		if (FCode.equalsIgnoreCase("StandByDate1"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				StandByDate1 = fDate.getDate( FValue );
			}
			else
				StandByDate1 = null;
		}
		if (FCode.equalsIgnoreCase("StandByDate2"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				StandByDate2 = fDate.getDate( FValue );
			}
			else
				StandByDate2 = null;
		}
		if (FCode.equalsIgnoreCase("ReadState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReadState = FValue.trim();
			}
			else
				ReadState = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("ContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContNo = FValue.trim();
			}
			else
				ContNo = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		FIVoucherDataGatherSchema other = (FIVoucherDataGatherSchema)otherObject;
		return
			(BatchNo == null ? other.getBatchNo() == null : BatchNo.equals(other.getBatchNo()))
			&& (VersionNo == null ? other.getVersionNo() == null : VersionNo.equals(other.getVersionNo()))
			&& (VSerialNo == null ? other.getVSerialNo() == null : VSerialNo.equals(other.getVSerialNo()))
			&& (ImportType == null ? other.getImportType() == null : ImportType.equals(other.getImportType()))
			&& (DCFlag == null ? other.getDCFlag() == null : DCFlag.equals(other.getDCFlag()))
			&& (VoucherType == null ? other.getVoucherType() == null : VoucherType.equals(other.getVoucherType()))
			&& (Currency == null ? other.getCurrency() == null : Currency.equals(other.getCurrency()))
			&& (DepCode == null ? other.getDepCode() == null : DepCode.equals(other.getDepCode()))
			&& (FinMangerCount == null ? other.getFinMangerCount() == null : FinMangerCount.equals(other.getFinMangerCount()))
			&& (AccountCode == null ? other.getAccountCode() == null : AccountCode.equals(other.getAccountCode()))
			&& (BClient == null ? other.getBClient() == null : BClient.equals(other.getBClient()))
			&& (CostCenter == null ? other.getCostCenter() == null : CostCenter.equals(other.getCostCenter()))
			&& (RiskCode == null ? other.getRiskCode() == null : RiskCode.equals(other.getRiskCode()))
			&& (MarketType == null ? other.getMarketType() == null : MarketType.equals(other.getMarketType()))
			&& (Chinal == null ? other.getChinal() == null : Chinal.equals(other.getChinal()))
			&& (PCont == null ? other.getPCont() == null : PCont.equals(other.getPCont()))
			&& (CashFlowNo == null ? other.getCashFlowNo() == null : CashFlowNo.equals(other.getCashFlowNo()))
			&& (ChargeDate == null ? other.getChargeDate() == null : fDate.getString(ChargeDate).equals(other.getChargeDate()))
			&& (ChargeTime == null ? other.getChargeTime() == null : ChargeTime.equals(other.getChargeTime()))
			&& SumMoney == other.getSumMoney()
			&& (VoucherYear == null ? other.getVoucherYear() == null : VoucherYear.equals(other.getVoucherYear()))
			&& PolYear == other.getPolYear()
			&& Years == other.getYears()
			&& (AgentNo == null ? other.getAgentNo() == null : AgentNo.equals(other.getAgentNo()))
			&& (PremiumType == null ? other.getPremiumType() == null : PremiumType.equals(other.getPremiumType()))
			&& (FirstYear == null ? other.getFirstYear() == null : FirstYear.equals(other.getFirstYear()))
			&& (StandByString1 == null ? other.getStandByString1() == null : StandByString1.equals(other.getStandByString1()))
			&& (StandByString2 == null ? other.getStandByString2() == null : StandByString2.equals(other.getStandByString2()))
			&& (StandByString3 == null ? other.getStandByString3() == null : StandByString3.equals(other.getStandByString3()))
			&& StandByNum1 == other.getStandByNum1()
			&& StandByNum2 == other.getStandByNum2()
			&& (StandByDate1 == null ? other.getStandByDate1() == null : fDate.getString(StandByDate1).equals(other.getStandByDate1()))
			&& (StandByDate2 == null ? other.getStandByDate2() == null : fDate.getString(StandByDate2).equals(other.getStandByDate2()))
			&& (ReadState == null ? other.getReadState() == null : ReadState.equals(other.getReadState()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (ContNo == null ? other.getContNo() == null : ContNo.equals(other.getContNo()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("BatchNo") ) {
			return 0;
		}
		if( strFieldName.equals("VersionNo") ) {
			return 1;
		}
		if( strFieldName.equals("VSerialNo") ) {
			return 2;
		}
		if( strFieldName.equals("ImportType") ) {
			return 3;
		}
		if( strFieldName.equals("DCFlag") ) {
			return 4;
		}
		if( strFieldName.equals("VoucherType") ) {
			return 5;
		}
		if( strFieldName.equals("Currency") ) {
			return 6;
		}
		if( strFieldName.equals("DepCode") ) {
			return 7;
		}
		if( strFieldName.equals("FinMangerCount") ) {
			return 8;
		}
		if( strFieldName.equals("AccountCode") ) {
			return 9;
		}
		if( strFieldName.equals("BClient") ) {
			return 10;
		}
		if( strFieldName.equals("CostCenter") ) {
			return 11;
		}
		if( strFieldName.equals("RiskCode") ) {
			return 12;
		}
		if( strFieldName.equals("MarketType") ) {
			return 13;
		}
		if( strFieldName.equals("Chinal") ) {
			return 14;
		}
		if( strFieldName.equals("PCont") ) {
			return 15;
		}
		if( strFieldName.equals("CashFlowNo") ) {
			return 16;
		}
		if( strFieldName.equals("ChargeDate") ) {
			return 17;
		}
		if( strFieldName.equals("ChargeTime") ) {
			return 18;
		}
		if( strFieldName.equals("SumMoney") ) {
			return 19;
		}
		if( strFieldName.equals("VoucherYear") ) {
			return 20;
		}
		if( strFieldName.equals("PolYear") ) {
			return 21;
		}
		if( strFieldName.equals("Years") ) {
			return 22;
		}
		if( strFieldName.equals("AgentNo") ) {
			return 23;
		}
		if( strFieldName.equals("PremiumType") ) {
			return 24;
		}
		if( strFieldName.equals("FirstYear") ) {
			return 25;
		}
		if( strFieldName.equals("StandByString1") ) {
			return 26;
		}
		if( strFieldName.equals("StandByString2") ) {
			return 27;
		}
		if( strFieldName.equals("StandByString3") ) {
			return 28;
		}
		if( strFieldName.equals("StandByNum1") ) {
			return 29;
		}
		if( strFieldName.equals("StandByNum2") ) {
			return 30;
		}
		if( strFieldName.equals("StandByDate1") ) {
			return 31;
		}
		if( strFieldName.equals("StandByDate2") ) {
			return 32;
		}
		if( strFieldName.equals("ReadState") ) {
			return 33;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 34;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 35;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 36;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 37;
		}
		if( strFieldName.equals("Operator") ) {
			return 38;
		}
		if( strFieldName.equals("ContNo") ) {
			return 39;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "BatchNo";
				break;
			case 1:
				strFieldName = "VersionNo";
				break;
			case 2:
				strFieldName = "VSerialNo";
				break;
			case 3:
				strFieldName = "ImportType";
				break;
			case 4:
				strFieldName = "DCFlag";
				break;
			case 5:
				strFieldName = "VoucherType";
				break;
			case 6:
				strFieldName = "Currency";
				break;
			case 7:
				strFieldName = "DepCode";
				break;
			case 8:
				strFieldName = "FinMangerCount";
				break;
			case 9:
				strFieldName = "AccountCode";
				break;
			case 10:
				strFieldName = "BClient";
				break;
			case 11:
				strFieldName = "CostCenter";
				break;
			case 12:
				strFieldName = "RiskCode";
				break;
			case 13:
				strFieldName = "MarketType";
				break;
			case 14:
				strFieldName = "Chinal";
				break;
			case 15:
				strFieldName = "PCont";
				break;
			case 16:
				strFieldName = "CashFlowNo";
				break;
			case 17:
				strFieldName = "ChargeDate";
				break;
			case 18:
				strFieldName = "ChargeTime";
				break;
			case 19:
				strFieldName = "SumMoney";
				break;
			case 20:
				strFieldName = "VoucherYear";
				break;
			case 21:
				strFieldName = "PolYear";
				break;
			case 22:
				strFieldName = "Years";
				break;
			case 23:
				strFieldName = "AgentNo";
				break;
			case 24:
				strFieldName = "PremiumType";
				break;
			case 25:
				strFieldName = "FirstYear";
				break;
			case 26:
				strFieldName = "StandByString1";
				break;
			case 27:
				strFieldName = "StandByString2";
				break;
			case 28:
				strFieldName = "StandByString3";
				break;
			case 29:
				strFieldName = "StandByNum1";
				break;
			case 30:
				strFieldName = "StandByNum2";
				break;
			case 31:
				strFieldName = "StandByDate1";
				break;
			case 32:
				strFieldName = "StandByDate2";
				break;
			case 33:
				strFieldName = "ReadState";
				break;
			case 34:
				strFieldName = "MakeDate";
				break;
			case 35:
				strFieldName = "MakeTime";
				break;
			case 36:
				strFieldName = "ModifyDate";
				break;
			case 37:
				strFieldName = "ModifyTime";
				break;
			case 38:
				strFieldName = "Operator";
				break;
			case 39:
				strFieldName = "ContNo";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("BatchNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("VersionNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("VSerialNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ImportType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DCFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("VoucherType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Currency") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DepCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FinMangerCount") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccountCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BClient") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CostCenter") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MarketType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Chinal") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PCont") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CashFlowNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ChargeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ChargeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SumMoney") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("VoucherYear") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolYear") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("Years") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("AgentNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PremiumType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FirstYear") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandByString1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandByString2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandByString3") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandByNum1") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("StandByNum2") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("StandByDate1") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("StandByDate2") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ReadState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContNo") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_INT;
				break;
			case 22:
				nFieldType = Schema.TYPE_INT;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 30:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 31:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 32:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 35:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 36:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 37:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 38:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 39:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
