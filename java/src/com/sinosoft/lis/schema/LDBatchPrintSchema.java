/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LDBatchPrintDB;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LDBatchPrintSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-07-25
 */
public class LDBatchPrintSchema implements Schema, Cloneable
{
    // @Field
    /** 单证编码 */
    private String Code;
    /** 管理机构 */
    private String ManageCom;
    /** 单证编码名称 */
    private String Codename;
    /** 部门 */
    private String Department;
    /** 销售渠道 */
    private String Salechnl;
    /** 单证类型 */
    private String Cardtype;
    /** 定时程序代码 */
    private String Taskcode;
    /** 备用字段 */
    private String Standby;

    public static final int FIELDNUM = 8; // 数据库表的字段个数

    private static String[] PK; // 主键

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LDBatchPrintSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "Code";
        pk[1] = "ManageCom";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException
    {
        LDBatchPrintSchema cloned = (LDBatchPrintSchema)super.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getCode()
    {
        return Code;
    }

    public void setCode(String aCode)
    {
        Code = aCode;
    }

    public String getManageCom()
    {
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public String getCodename()
    {
        return Codename;
    }

    public void setCodename(String aCodename)
    {
        Codename = aCodename;
    }

    public String getDepartment()
    {
        return Department;
    }

    public void setDepartment(String aDepartment)
    {
        Department = aDepartment;
    }

    public String getSalechnl()
    {
        return Salechnl;
    }

    public void setSalechnl(String aSalechnl)
    {
        Salechnl = aSalechnl;
    }

    public String getCardtype()
    {
        return Cardtype;
    }

    public void setCardtype(String aCardtype)
    {
        Cardtype = aCardtype;
    }

    public String getTaskcode()
    {
        return Taskcode;
    }

    public void setTaskcode(String aTaskcode)
    {
        Taskcode = aTaskcode;
    }

    public String getStandby()
    {
        return Standby;
    }

    public void setStandby(String aStandby)
    {
        Standby = aStandby;
    }

    /**
     * 使用另外一个 LDBatchPrintSchema 对象给 Schema 赋值
     * @param: aLDBatchPrintSchema LDBatchPrintSchema
     **/
    public void setSchema(LDBatchPrintSchema aLDBatchPrintSchema)
    {
        this.Code = aLDBatchPrintSchema.getCode();
        this.ManageCom = aLDBatchPrintSchema.getManageCom();
        this.Codename = aLDBatchPrintSchema.getCodename();
        this.Department = aLDBatchPrintSchema.getDepartment();
        this.Salechnl = aLDBatchPrintSchema.getSalechnl();
        this.Cardtype = aLDBatchPrintSchema.getCardtype();
        this.Taskcode = aLDBatchPrintSchema.getTaskcode();
        this.Standby = aLDBatchPrintSchema.getStandby();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("Code") == null)
            {
                this.Code = null;
            }
            else
            {
                this.Code = rs.getString("Code").trim();
            }

            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("Codename") == null)
            {
                this.Codename = null;
            }
            else
            {
                this.Codename = rs.getString("Codename").trim();
            }

            if (rs.getString("Department") == null)
            {
                this.Department = null;
            }
            else
            {
                this.Department = rs.getString("Department").trim();
            }

            if (rs.getString("Salechnl") == null)
            {
                this.Salechnl = null;
            }
            else
            {
                this.Salechnl = rs.getString("Salechnl").trim();
            }

            if (rs.getString("Cardtype") == null)
            {
                this.Cardtype = null;
            }
            else
            {
                this.Cardtype = rs.getString("Cardtype").trim();
            }

            if (rs.getString("Taskcode") == null)
            {
                this.Taskcode = null;
            }
            else
            {
                this.Taskcode = rs.getString("Taskcode").trim();
            }

            if (rs.getString("Standby") == null)
            {
                this.Standby = null;
            }
            else
            {
                this.Standby = rs.getString("Standby").trim();
            }

        }
        catch (SQLException sqle)
        {
            System.out.println("数据库中的LDBatchPrint表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDBatchPrintSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LDBatchPrintSchema getSchema()
    {
        LDBatchPrintSchema aLDBatchPrintSchema = new LDBatchPrintSchema();
        aLDBatchPrintSchema.setSchema(this);
        return aLDBatchPrintSchema;
    }

    public LDBatchPrintDB getDB()
    {
        LDBatchPrintDB aDBOper = new LDBatchPrintDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDBatchPrint描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(Code));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Codename));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Department));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Salechnl));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Cardtype));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Taskcode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Standby));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDBatchPrint>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            Code = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                  SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                       SysConst.PACKAGESPILTER);
            Codename = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            Department = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                        SysConst.PACKAGESPILTER);
            Salechnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                      SysConst.PACKAGESPILTER);
            Cardtype = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                      SysConst.PACKAGESPILTER);
            Taskcode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                      SysConst.PACKAGESPILTER);
            Standby = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                     SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDBatchPrintSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("Code"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Code));
        }
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("Codename"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Codename));
        }
        if (FCode.equals("Department"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Department));
        }
        if (FCode.equals("Salechnl"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Salechnl));
        }
        if (FCode.equals("Cardtype"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Cardtype));
        }
        if (FCode.equals("Taskcode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Taskcode));
        }
        if (FCode.equals("Standby"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Standby));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(Code);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(Codename);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(Department);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(Salechnl);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(Cardtype);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(Taskcode);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(Standby);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equalsIgnoreCase("Code"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Code = FValue.trim();
            }
            else
            {
                Code = null;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("Codename"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Codename = FValue.trim();
            }
            else
            {
                Codename = null;
            }
        }
        if (FCode.equalsIgnoreCase("Department"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Department = FValue.trim();
            }
            else
            {
                Department = null;
            }
        }
        if (FCode.equalsIgnoreCase("Salechnl"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Salechnl = FValue.trim();
            }
            else
            {
                Salechnl = null;
            }
        }
        if (FCode.equalsIgnoreCase("Cardtype"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Cardtype = FValue.trim();
            }
            else
            {
                Cardtype = null;
            }
        }
        if (FCode.equalsIgnoreCase("Taskcode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Taskcode = FValue.trim();
            }
            else
            {
                Taskcode = null;
            }
        }
        if (FCode.equalsIgnoreCase("Standby"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Standby = FValue.trim();
            }
            else
            {
                Standby = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LDBatchPrintSchema other = (LDBatchPrintSchema) otherObject;
        return
                Code.equals(other.getCode())
                && ManageCom.equals(other.getManageCom())
                && Codename.equals(other.getCodename())
                && Department.equals(other.getDepartment())
                && Salechnl.equals(other.getSalechnl())
                && Cardtype.equals(other.getCardtype())
                && Taskcode.equals(other.getTaskcode())
                && Standby.equals(other.getStandby());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("Code"))
        {
            return 0;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return 1;
        }
        if (strFieldName.equals("Codename"))
        {
            return 2;
        }
        if (strFieldName.equals("Department"))
        {
            return 3;
        }
        if (strFieldName.equals("Salechnl"))
        {
            return 4;
        }
        if (strFieldName.equals("Cardtype"))
        {
            return 5;
        }
        if (strFieldName.equals("Taskcode"))
        {
            return 6;
        }
        if (strFieldName.equals("Standby"))
        {
            return 7;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "Code";
                break;
            case 1:
                strFieldName = "ManageCom";
                break;
            case 2:
                strFieldName = "Codename";
                break;
            case 3:
                strFieldName = "Department";
                break;
            case 4:
                strFieldName = "Salechnl";
                break;
            case 5:
                strFieldName = "Cardtype";
                break;
            case 6:
                strFieldName = "Taskcode";
                break;
            case 7:
                strFieldName = "Standby";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("Code"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Codename"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Department"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Salechnl"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Cardtype"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Taskcode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Standby"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
