/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LAQualificationBDB;

/*
 * <p>ClassName: LAQualificationBSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LAQualificationB
 * @CreateDate：2012-05-11
 */
public class LAQualificationBSchema implements Schema, Cloneable
{
	// @Field
	/** 转储号 */
	private String EdorNo;
	/** 业务员编码 */
	private String AgentCode;
	/** 资格证书号 */
	private String QualifNo;
	/** 流水号 */
	private int Idx;
	/** 批准单位 */
	private String GrantUnit;
	/** 发放日期 */
	private Date GrantDate;
	/** 失效日期 */
	private Date InvalidDate;
	/** 失效原因 */
	private String InvalidRsn;
	/** 补发日期 */
	private Date reissueDate;
	/** 补发原因 */
	private String reissueRsn;
	/** 有效日期 */
	private int ValidPeriod;
	/** 资格证书状态 */
	private String State;
	/** 通过考试日期 */
	private Date PasExamDate;
	/** 考试年度 */
	private String ExamYear;
	/** 考试次数 */
	private String ExamTimes;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 上一次修改日期 */
	private Date ModifyDate;
	/** 上一次修改时间 */
	private String ModifyTime;
	/** 有效起期 */
	private Date ValidStart;
	/** 有效止期 */
	private Date ValidEnd;
	/** 旧资格证号码 */
	private String OldQualifNo;
	/** 入机日期1 */
	private Date MakeDate1;
	/** 入机时间1 */
	private String MakeTime1;
	/** 操作员1 */
	private String Operator1;

	public static final int FIELDNUM = 26;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LAQualificationBSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[3];
		pk[0] = "EdorNo";
		pk[1] = "AgentCode";
		pk[2] = "QualifNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LAQualificationBSchema cloned = (LAQualificationBSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getEdorNo()
	{
		return EdorNo;
	}
	public void setEdorNo(String aEdorNo)
	{
		EdorNo = aEdorNo;
	}
	public String getAgentCode()
	{
		return AgentCode;
	}
	public void setAgentCode(String aAgentCode)
	{
		AgentCode = aAgentCode;
	}
	public String getQualifNo()
	{
		return QualifNo;
	}
	public void setQualifNo(String aQualifNo)
	{
		QualifNo = aQualifNo;
	}
	public int getIdx()
	{
		return Idx;
	}
	public void setIdx(int aIdx)
	{
		Idx = aIdx;
	}
	public void setIdx(String aIdx)
	{
		if (aIdx != null && !aIdx.equals(""))
		{
			Integer tInteger = new Integer(aIdx);
			int i = tInteger.intValue();
			Idx = i;
		}
	}

	public String getGrantUnit()
	{
		return GrantUnit;
	}
	public void setGrantUnit(String aGrantUnit)
	{
		GrantUnit = aGrantUnit;
	}
	public String getGrantDate()
	{
		if( GrantDate != null )
			return fDate.getString(GrantDate);
		else
			return null;
	}
	public void setGrantDate(Date aGrantDate)
	{
		GrantDate = aGrantDate;
	}
	public void setGrantDate(String aGrantDate)
	{
		if (aGrantDate != null && !aGrantDate.equals("") )
		{
			GrantDate = fDate.getDate( aGrantDate );
		}
		else
			GrantDate = null;
	}

	public String getInvalidDate()
	{
		if( InvalidDate != null )
			return fDate.getString(InvalidDate);
		else
			return null;
	}
	public void setInvalidDate(Date aInvalidDate)
	{
		InvalidDate = aInvalidDate;
	}
	public void setInvalidDate(String aInvalidDate)
	{
		if (aInvalidDate != null && !aInvalidDate.equals("") )
		{
			InvalidDate = fDate.getDate( aInvalidDate );
		}
		else
			InvalidDate = null;
	}

	public String getInvalidRsn()
	{
		return InvalidRsn;
	}
	public void setInvalidRsn(String aInvalidRsn)
	{
		InvalidRsn = aInvalidRsn;
	}
	public String getreissueDate()
	{
		if( reissueDate != null )
			return fDate.getString(reissueDate);
		else
			return null;
	}
	public void setreissueDate(Date areissueDate)
	{
		reissueDate = areissueDate;
	}
	public void setreissueDate(String areissueDate)
	{
		if (areissueDate != null && !areissueDate.equals("") )
		{
			reissueDate = fDate.getDate( areissueDate );
		}
		else
			reissueDate = null;
	}

	public String getreissueRsn()
	{
		return reissueRsn;
	}
	public void setreissueRsn(String areissueRsn)
	{
		reissueRsn = areissueRsn;
	}
	public int getValidPeriod()
	{
		return ValidPeriod;
	}
	public void setValidPeriod(int aValidPeriod)
	{
		ValidPeriod = aValidPeriod;
	}
	public void setValidPeriod(String aValidPeriod)
	{
		if (aValidPeriod != null && !aValidPeriod.equals(""))
		{
			Integer tInteger = new Integer(aValidPeriod);
			int i = tInteger.intValue();
			ValidPeriod = i;
		}
	}

	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}
	public String getPasExamDate()
	{
		if( PasExamDate != null )
			return fDate.getString(PasExamDate);
		else
			return null;
	}
	public void setPasExamDate(Date aPasExamDate)
	{
		PasExamDate = aPasExamDate;
	}
	public void setPasExamDate(String aPasExamDate)
	{
		if (aPasExamDate != null && !aPasExamDate.equals("") )
		{
			PasExamDate = fDate.getDate( aPasExamDate );
		}
		else
			PasExamDate = null;
	}

	public String getExamYear()
	{
		return ExamYear;
	}
	public void setExamYear(String aExamYear)
	{
		ExamYear = aExamYear;
	}
	public String getExamTimes()
	{
		return ExamTimes;
	}
	public void setExamTimes(String aExamTimes)
	{
		ExamTimes = aExamTimes;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getValidStart()
	{
		if( ValidStart != null )
			return fDate.getString(ValidStart);
		else
			return null;
	}
	public void setValidStart(Date aValidStart)
	{
		ValidStart = aValidStart;
	}
	public void setValidStart(String aValidStart)
	{
		if (aValidStart != null && !aValidStart.equals("") )
		{
			ValidStart = fDate.getDate( aValidStart );
		}
		else
			ValidStart = null;
	}

	public String getValidEnd()
	{
		if( ValidEnd != null )
			return fDate.getString(ValidEnd);
		else
			return null;
	}
	public void setValidEnd(Date aValidEnd)
	{
		ValidEnd = aValidEnd;
	}
	public void setValidEnd(String aValidEnd)
	{
		if (aValidEnd != null && !aValidEnd.equals("") )
		{
			ValidEnd = fDate.getDate( aValidEnd );
		}
		else
			ValidEnd = null;
	}

	public String getOldQualifNo()
	{
		return OldQualifNo;
	}
	public void setOldQualifNo(String aOldQualifNo)
	{
		OldQualifNo = aOldQualifNo;
	}
	public String getMakeDate1()
	{
		if( MakeDate1 != null )
			return fDate.getString(MakeDate1);
		else
			return null;
	}
	public void setMakeDate1(Date aMakeDate1)
	{
		MakeDate1 = aMakeDate1;
	}
	public void setMakeDate1(String aMakeDate1)
	{
		if (aMakeDate1 != null && !aMakeDate1.equals("") )
		{
			MakeDate1 = fDate.getDate( aMakeDate1 );
		}
		else
			MakeDate1 = null;
	}

	public String getMakeTime1()
	{
		return MakeTime1;
	}
	public void setMakeTime1(String aMakeTime1)
	{
		MakeTime1 = aMakeTime1;
	}
	public String getOperator1()
	{
		return Operator1;
	}
	public void setOperator1(String aOperator1)
	{
		Operator1 = aOperator1;
	}

	/**
	* 使用另外一个 LAQualificationBSchema 对象给 Schema 赋值
	* @param: aLAQualificationBSchema LAQualificationBSchema
	**/
	public void setSchema(LAQualificationBSchema aLAQualificationBSchema)
	{
		this.EdorNo = aLAQualificationBSchema.getEdorNo();
		this.AgentCode = aLAQualificationBSchema.getAgentCode();
		this.QualifNo = aLAQualificationBSchema.getQualifNo();
		this.Idx = aLAQualificationBSchema.getIdx();
		this.GrantUnit = aLAQualificationBSchema.getGrantUnit();
		this.GrantDate = fDate.getDate( aLAQualificationBSchema.getGrantDate());
		this.InvalidDate = fDate.getDate( aLAQualificationBSchema.getInvalidDate());
		this.InvalidRsn = aLAQualificationBSchema.getInvalidRsn();
		this.reissueDate = fDate.getDate( aLAQualificationBSchema.getreissueDate());
		this.reissueRsn = aLAQualificationBSchema.getreissueRsn();
		this.ValidPeriod = aLAQualificationBSchema.getValidPeriod();
		this.State = aLAQualificationBSchema.getState();
		this.PasExamDate = fDate.getDate( aLAQualificationBSchema.getPasExamDate());
		this.ExamYear = aLAQualificationBSchema.getExamYear();
		this.ExamTimes = aLAQualificationBSchema.getExamTimes();
		this.Operator = aLAQualificationBSchema.getOperator();
		this.MakeDate = fDate.getDate( aLAQualificationBSchema.getMakeDate());
		this.MakeTime = aLAQualificationBSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLAQualificationBSchema.getModifyDate());
		this.ModifyTime = aLAQualificationBSchema.getModifyTime();
		this.ValidStart = fDate.getDate( aLAQualificationBSchema.getValidStart());
		this.ValidEnd = fDate.getDate( aLAQualificationBSchema.getValidEnd());
		this.OldQualifNo = aLAQualificationBSchema.getOldQualifNo();
		this.MakeDate1 = fDate.getDate( aLAQualificationBSchema.getMakeDate1());
		this.MakeTime1 = aLAQualificationBSchema.getMakeTime1();
		this.Operator1 = aLAQualificationBSchema.getOperator1();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("EdorNo") == null )
				this.EdorNo = null;
			else
				this.EdorNo = rs.getString("EdorNo").trim();

			if( rs.getString("AgentCode") == null )
				this.AgentCode = null;
			else
				this.AgentCode = rs.getString("AgentCode").trim();

			if( rs.getString("QualifNo") == null )
				this.QualifNo = null;
			else
				this.QualifNo = rs.getString("QualifNo").trim();

			this.Idx = rs.getInt("Idx");
			if( rs.getString("GrantUnit") == null )
				this.GrantUnit = null;
			else
				this.GrantUnit = rs.getString("GrantUnit").trim();

			this.GrantDate = rs.getDate("GrantDate");
			this.InvalidDate = rs.getDate("InvalidDate");
			if( rs.getString("InvalidRsn") == null )
				this.InvalidRsn = null;
			else
				this.InvalidRsn = rs.getString("InvalidRsn").trim();

			this.reissueDate = rs.getDate("reissueDate");
			if( rs.getString("reissueRsn") == null )
				this.reissueRsn = null;
			else
				this.reissueRsn = rs.getString("reissueRsn").trim();

			this.ValidPeriod = rs.getInt("ValidPeriod");
			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			this.PasExamDate = rs.getDate("PasExamDate");
			if( rs.getString("ExamYear") == null )
				this.ExamYear = null;
			else
				this.ExamYear = rs.getString("ExamYear").trim();

			if( rs.getString("ExamTimes") == null )
				this.ExamTimes = null;
			else
				this.ExamTimes = rs.getString("ExamTimes").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			this.ValidStart = rs.getDate("ValidStart");
			this.ValidEnd = rs.getDate("ValidEnd");
			if( rs.getString("OldQualifNo") == null )
				this.OldQualifNo = null;
			else
				this.OldQualifNo = rs.getString("OldQualifNo").trim();

			this.MakeDate1 = rs.getDate("MakeDate1");
			if( rs.getString("MakeTime1") == null )
				this.MakeTime1 = null;
			else
				this.MakeTime1 = rs.getString("MakeTime1").trim();

			if( rs.getString("Operator1") == null )
				this.Operator1 = null;
			else
				this.Operator1 = rs.getString("Operator1").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LAQualificationB表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAQualificationBSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LAQualificationBSchema getSchema()
	{
		LAQualificationBSchema aLAQualificationBSchema = new LAQualificationBSchema();
		aLAQualificationBSchema.setSchema(this);
		return aLAQualificationBSchema;
	}

	public LAQualificationBDB getDB()
	{
		LAQualificationBDB aDBOper = new LAQualificationBDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAQualificationB描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(EdorNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(QualifNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Idx));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrantUnit)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( GrantDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( InvalidDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InvalidRsn)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( reissueDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(reissueRsn)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ValidPeriod));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( PasExamDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ExamYear)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ExamTimes)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ValidStart ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ValidEnd ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OldQualifNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate1 ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator1));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAQualificationB>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			EdorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			QualifNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			Idx= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,4,SysConst.PACKAGESPILTER))).intValue();
			GrantUnit = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			GrantDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,SysConst.PACKAGESPILTER));
			InvalidDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,SysConst.PACKAGESPILTER));
			InvalidRsn = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			reissueDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,SysConst.PACKAGESPILTER));
			reissueRsn = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			ValidPeriod= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,11,SysConst.PACKAGESPILTER))).intValue();
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			PasExamDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,SysConst.PACKAGESPILTER));
			ExamYear = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			ExamTimes = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			ValidStart = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,SysConst.PACKAGESPILTER));
			ValidEnd = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,SysConst.PACKAGESPILTER));
			OldQualifNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			MakeDate1 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,SysConst.PACKAGESPILTER));
			MakeTime1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			Operator1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAQualificationBSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("EdorNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EdorNo));
		}
		if (FCode.equals("AgentCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
		}
		if (FCode.equals("QualifNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(QualifNo));
		}
		if (FCode.equals("Idx"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Idx));
		}
		if (FCode.equals("GrantUnit"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrantUnit));
		}
		if (FCode.equals("GrantDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getGrantDate()));
		}
		if (FCode.equals("InvalidDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getInvalidDate()));
		}
		if (FCode.equals("InvalidRsn"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InvalidRsn));
		}
		if (FCode.equals("reissueDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getreissueDate()));
		}
		if (FCode.equals("reissueRsn"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(reissueRsn));
		}
		if (FCode.equals("ValidPeriod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ValidPeriod));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("PasExamDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getPasExamDate()));
		}
		if (FCode.equals("ExamYear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ExamYear));
		}
		if (FCode.equals("ExamTimes"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ExamTimes));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("ValidStart"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getValidStart()));
		}
		if (FCode.equals("ValidEnd"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getValidEnd()));
		}
		if (FCode.equals("OldQualifNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OldQualifNo));
		}
		if (FCode.equals("MakeDate1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate1()));
		}
		if (FCode.equals("MakeTime1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime1));
		}
		if (FCode.equals("Operator1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator1));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(EdorNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(AgentCode);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(QualifNo);
				break;
			case 3:
				strFieldValue = String.valueOf(Idx);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(GrantUnit);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getGrantDate()));
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getInvalidDate()));
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(InvalidRsn);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getreissueDate()));
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(reissueRsn);
				break;
			case 10:
				strFieldValue = String.valueOf(ValidPeriod);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getPasExamDate()));
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(ExamYear);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(ExamTimes);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getValidStart()));
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getValidEnd()));
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(OldQualifNo);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate1()));
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(MakeTime1);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(Operator1);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("EdorNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EdorNo = FValue.trim();
			}
			else
				EdorNo = null;
		}
		if (FCode.equalsIgnoreCase("AgentCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCode = FValue.trim();
			}
			else
				AgentCode = null;
		}
		if (FCode.equalsIgnoreCase("QualifNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				QualifNo = FValue.trim();
			}
			else
				QualifNo = null;
		}
		if (FCode.equalsIgnoreCase("Idx"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				Idx = i;
			}
		}
		if (FCode.equalsIgnoreCase("GrantUnit"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrantUnit = FValue.trim();
			}
			else
				GrantUnit = null;
		}
		if (FCode.equalsIgnoreCase("GrantDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				GrantDate = fDate.getDate( FValue );
			}
			else
				GrantDate = null;
		}
		if (FCode.equalsIgnoreCase("InvalidDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				InvalidDate = fDate.getDate( FValue );
			}
			else
				InvalidDate = null;
		}
		if (FCode.equalsIgnoreCase("InvalidRsn"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InvalidRsn = FValue.trim();
			}
			else
				InvalidRsn = null;
		}
		if (FCode.equalsIgnoreCase("reissueDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				reissueDate = fDate.getDate( FValue );
			}
			else
				reissueDate = null;
		}
		if (FCode.equalsIgnoreCase("reissueRsn"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				reissueRsn = FValue.trim();
			}
			else
				reissueRsn = null;
		}
		if (FCode.equalsIgnoreCase("ValidPeriod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				ValidPeriod = i;
			}
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("PasExamDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				PasExamDate = fDate.getDate( FValue );
			}
			else
				PasExamDate = null;
		}
		if (FCode.equalsIgnoreCase("ExamYear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ExamYear = FValue.trim();
			}
			else
				ExamYear = null;
		}
		if (FCode.equalsIgnoreCase("ExamTimes"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ExamTimes = FValue.trim();
			}
			else
				ExamTimes = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("ValidStart"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ValidStart = fDate.getDate( FValue );
			}
			else
				ValidStart = null;
		}
		if (FCode.equalsIgnoreCase("ValidEnd"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ValidEnd = fDate.getDate( FValue );
			}
			else
				ValidEnd = null;
		}
		if (FCode.equalsIgnoreCase("OldQualifNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OldQualifNo = FValue.trim();
			}
			else
				OldQualifNo = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate1"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate1 = fDate.getDate( FValue );
			}
			else
				MakeDate1 = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime1 = FValue.trim();
			}
			else
				MakeTime1 = null;
		}
		if (FCode.equalsIgnoreCase("Operator1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator1 = FValue.trim();
			}
			else
				Operator1 = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LAQualificationBSchema other = (LAQualificationBSchema)otherObject;
		return
			(EdorNo == null ? other.getEdorNo() == null : EdorNo.equals(other.getEdorNo()))
			&& (AgentCode == null ? other.getAgentCode() == null : AgentCode.equals(other.getAgentCode()))
			&& (QualifNo == null ? other.getQualifNo() == null : QualifNo.equals(other.getQualifNo()))
			&& Idx == other.getIdx()
			&& (GrantUnit == null ? other.getGrantUnit() == null : GrantUnit.equals(other.getGrantUnit()))
			&& (GrantDate == null ? other.getGrantDate() == null : fDate.getString(GrantDate).equals(other.getGrantDate()))
			&& (InvalidDate == null ? other.getInvalidDate() == null : fDate.getString(InvalidDate).equals(other.getInvalidDate()))
			&& (InvalidRsn == null ? other.getInvalidRsn() == null : InvalidRsn.equals(other.getInvalidRsn()))
			&& (reissueDate == null ? other.getreissueDate() == null : fDate.getString(reissueDate).equals(other.getreissueDate()))
			&& (reissueRsn == null ? other.getreissueRsn() == null : reissueRsn.equals(other.getreissueRsn()))
			&& ValidPeriod == other.getValidPeriod()
			&& (State == null ? other.getState() == null : State.equals(other.getState()))
			&& (PasExamDate == null ? other.getPasExamDate() == null : fDate.getString(PasExamDate).equals(other.getPasExamDate()))
			&& (ExamYear == null ? other.getExamYear() == null : ExamYear.equals(other.getExamYear()))
			&& (ExamTimes == null ? other.getExamTimes() == null : ExamTimes.equals(other.getExamTimes()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (ValidStart == null ? other.getValidStart() == null : fDate.getString(ValidStart).equals(other.getValidStart()))
			&& (ValidEnd == null ? other.getValidEnd() == null : fDate.getString(ValidEnd).equals(other.getValidEnd()))
			&& (OldQualifNo == null ? other.getOldQualifNo() == null : OldQualifNo.equals(other.getOldQualifNo()))
			&& (MakeDate1 == null ? other.getMakeDate1() == null : fDate.getString(MakeDate1).equals(other.getMakeDate1()))
			&& (MakeTime1 == null ? other.getMakeTime1() == null : MakeTime1.equals(other.getMakeTime1()))
			&& (Operator1 == null ? other.getOperator1() == null : Operator1.equals(other.getOperator1()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("EdorNo") ) {
			return 0;
		}
		if( strFieldName.equals("AgentCode") ) {
			return 1;
		}
		if( strFieldName.equals("QualifNo") ) {
			return 2;
		}
		if( strFieldName.equals("Idx") ) {
			return 3;
		}
		if( strFieldName.equals("GrantUnit") ) {
			return 4;
		}
		if( strFieldName.equals("GrantDate") ) {
			return 5;
		}
		if( strFieldName.equals("InvalidDate") ) {
			return 6;
		}
		if( strFieldName.equals("InvalidRsn") ) {
			return 7;
		}
		if( strFieldName.equals("reissueDate") ) {
			return 8;
		}
		if( strFieldName.equals("reissueRsn") ) {
			return 9;
		}
		if( strFieldName.equals("ValidPeriod") ) {
			return 10;
		}
		if( strFieldName.equals("State") ) {
			return 11;
		}
		if( strFieldName.equals("PasExamDate") ) {
			return 12;
		}
		if( strFieldName.equals("ExamYear") ) {
			return 13;
		}
		if( strFieldName.equals("ExamTimes") ) {
			return 14;
		}
		if( strFieldName.equals("Operator") ) {
			return 15;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 16;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 17;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 18;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 19;
		}
		if( strFieldName.equals("ValidStart") ) {
			return 20;
		}
		if( strFieldName.equals("ValidEnd") ) {
			return 21;
		}
		if( strFieldName.equals("OldQualifNo") ) {
			return 22;
		}
		if( strFieldName.equals("MakeDate1") ) {
			return 23;
		}
		if( strFieldName.equals("MakeTime1") ) {
			return 24;
		}
		if( strFieldName.equals("Operator1") ) {
			return 25;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "EdorNo";
				break;
			case 1:
				strFieldName = "AgentCode";
				break;
			case 2:
				strFieldName = "QualifNo";
				break;
			case 3:
				strFieldName = "Idx";
				break;
			case 4:
				strFieldName = "GrantUnit";
				break;
			case 5:
				strFieldName = "GrantDate";
				break;
			case 6:
				strFieldName = "InvalidDate";
				break;
			case 7:
				strFieldName = "InvalidRsn";
				break;
			case 8:
				strFieldName = "reissueDate";
				break;
			case 9:
				strFieldName = "reissueRsn";
				break;
			case 10:
				strFieldName = "ValidPeriod";
				break;
			case 11:
				strFieldName = "State";
				break;
			case 12:
				strFieldName = "PasExamDate";
				break;
			case 13:
				strFieldName = "ExamYear";
				break;
			case 14:
				strFieldName = "ExamTimes";
				break;
			case 15:
				strFieldName = "Operator";
				break;
			case 16:
				strFieldName = "MakeDate";
				break;
			case 17:
				strFieldName = "MakeTime";
				break;
			case 18:
				strFieldName = "ModifyDate";
				break;
			case 19:
				strFieldName = "ModifyTime";
				break;
			case 20:
				strFieldName = "ValidStart";
				break;
			case 21:
				strFieldName = "ValidEnd";
				break;
			case 22:
				strFieldName = "OldQualifNo";
				break;
			case 23:
				strFieldName = "MakeDate1";
				break;
			case 24:
				strFieldName = "MakeTime1";
				break;
			case 25:
				strFieldName = "Operator1";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("EdorNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("QualifNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Idx") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("GrantUnit") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrantDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("InvalidDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("InvalidRsn") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("reissueDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("reissueRsn") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ValidPeriod") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PasExamDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ExamYear") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ExamTimes") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ValidStart") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ValidEnd") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("OldQualifNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate1") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator1") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_INT;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 6:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_INT;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 21:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
