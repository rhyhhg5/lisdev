/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LLIssuePolDB;

/*
 * <p>ClassName: LLIssuePolSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-09-19
 */
public class LLIssuePolSchema implements Schema, Cloneable {
    // @Field
    /** 理赔号 */
    private String CaseNo;
    /** 团体批次号 */
    private String RgtNo;
    /** 问题件号 */
    private String IssueNo;
    /** 留言流水号 */
    private String MessageNo;
    /** 留言类型 */
    private String MessageType;
    /** 提问人 */
    private String RequrieMan;
    /** 提起时案件状态 */
    private String RgtState;
    /** 问题件类型 */
    private String IssueType;
    /** 主题 */
    private String Subject;
    /** 内容 */
    private String MessageCont;
    /** 指定答复人 */
    private String Replyer;
    /** 退回状态 */
    private String BackRgtState;
    /** 状态 */
    private String State;
    /** 操作员 */
    private String Operator;
    /** 管理机构 */
    private String ManageCom;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 19; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LLIssuePolSchema() {
        mErrors = new CErrors();

        String[] pk = new String[4];
        pk[0] = "CaseNo";
        pk[1] = "IssueNo";
        pk[2] = "MessageNo";
        pk[3] = "MessageType";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LLIssuePolSchema cloned = (LLIssuePolSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getCaseNo() {
        return CaseNo;
    }

    public void setCaseNo(String aCaseNo) {
        CaseNo = aCaseNo;
    }

    public String getRgtNo() {
        return RgtNo;
    }

    public void setRgtNo(String aRgtNo) {
        RgtNo = aRgtNo;
    }

    public String getIssueNo() {
        return IssueNo;
    }

    public void setIssueNo(String aIssueNo) {
        IssueNo = aIssueNo;
    }

    public String getMessageNo() {
        return MessageNo;
    }

    public void setMessageNo(String aMessageNo) {
        MessageNo = aMessageNo;
    }

    public String getMessageType() {
        return MessageType;
    }

    public void setMessageType(String aMessageType) {
        MessageType = aMessageType;
    }

    public String getRequrieMan() {
        return RequrieMan;
    }

    public void setRequrieMan(String aRequrieMan) {
        RequrieMan = aRequrieMan;
    }

    public String getRgtState() {
        return RgtState;
    }

    public void setRgtState(String aRgtState) {
        RgtState = aRgtState;
    }

    public String getIssueType() {
        return IssueType;
    }

    public void setIssueType(String aIssueType) {
        IssueType = aIssueType;
    }

    public String getSubject() {
        return Subject;
    }

    public void setSubject(String aSubject) {
        Subject = aSubject;
    }

    public String getMessageCont() {
        return MessageCont;
    }

    public void setMessageCont(String aMessageCont) {
        MessageCont = aMessageCont;
    }

    public String getReplyer() {
        return Replyer;
    }

    public void setReplyer(String aReplyer) {
        Replyer = aReplyer;
    }

    public String getBackRgtState() {
        return BackRgtState;
    }

    public void setBackRgtState(String aBackRgtState) {
        BackRgtState = aBackRgtState;
    }

    public String getState() {
        return State;
    }

    public void setState(String aState) {
        State = aState;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LLIssuePolSchema 对象给 Schema 赋值
     * @param: aLLIssuePolSchema LLIssuePolSchema
     **/
    public void setSchema(LLIssuePolSchema aLLIssuePolSchema) {
        this.CaseNo = aLLIssuePolSchema.getCaseNo();
        this.RgtNo = aLLIssuePolSchema.getRgtNo();
        this.IssueNo = aLLIssuePolSchema.getIssueNo();
        this.MessageNo = aLLIssuePolSchema.getMessageNo();
        this.MessageType = aLLIssuePolSchema.getMessageType();
        this.RequrieMan = aLLIssuePolSchema.getRequrieMan();
        this.RgtState = aLLIssuePolSchema.getRgtState();
        this.IssueType = aLLIssuePolSchema.getIssueType();
        this.Subject = aLLIssuePolSchema.getSubject();
        this.MessageCont = aLLIssuePolSchema.getMessageCont();
        this.Replyer = aLLIssuePolSchema.getReplyer();
        this.BackRgtState = aLLIssuePolSchema.getBackRgtState();
        this.State = aLLIssuePolSchema.getState();
        this.Operator = aLLIssuePolSchema.getOperator();
        this.ManageCom = aLLIssuePolSchema.getManageCom();
        this.MakeDate = fDate.getDate(aLLIssuePolSchema.getMakeDate());
        this.MakeTime = aLLIssuePolSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLLIssuePolSchema.getModifyDate());
        this.ModifyTime = aLLIssuePolSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("CaseNo") == null) {
                this.CaseNo = null;
            } else {
                this.CaseNo = rs.getString("CaseNo").trim();
            }

            if (rs.getString("RgtNo") == null) {
                this.RgtNo = null;
            } else {
                this.RgtNo = rs.getString("RgtNo").trim();
            }

            if (rs.getString("IssueNo") == null) {
                this.IssueNo = null;
            } else {
                this.IssueNo = rs.getString("IssueNo").trim();
            }

            if (rs.getString("MessageNo") == null) {
                this.MessageNo = null;
            } else {
                this.MessageNo = rs.getString("MessageNo").trim();
            }

            if (rs.getString("MessageType") == null) {
                this.MessageType = null;
            } else {
                this.MessageType = rs.getString("MessageType").trim();
            }

            if (rs.getString("RequrieMan") == null) {
                this.RequrieMan = null;
            } else {
                this.RequrieMan = rs.getString("RequrieMan").trim();
            }

            if (rs.getString("RgtState") == null) {
                this.RgtState = null;
            } else {
                this.RgtState = rs.getString("RgtState").trim();
            }

            if (rs.getString("IssueType") == null) {
                this.IssueType = null;
            } else {
                this.IssueType = rs.getString("IssueType").trim();
            }

            if (rs.getString("Subject") == null) {
                this.Subject = null;
            } else {
                this.Subject = rs.getString("Subject").trim();
            }

            if (rs.getString("MessageCont") == null) {
                this.MessageCont = null;
            } else {
                this.MessageCont = rs.getString("MessageCont").trim();
            }

            if (rs.getString("Replyer") == null) {
                this.Replyer = null;
            } else {
                this.Replyer = rs.getString("Replyer").trim();
            }

            if (rs.getString("BackRgtState") == null) {
                this.BackRgtState = null;
            } else {
                this.BackRgtState = rs.getString("BackRgtState").trim();
            }

            if (rs.getString("State") == null) {
                this.State = null;
            } else {
                this.State = rs.getString("State").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            if (rs.getString("ManageCom") == null) {
                this.ManageCom = null;
            } else {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LLIssuePol表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLIssuePolSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LLIssuePolSchema getSchema() {
        LLIssuePolSchema aLLIssuePolSchema = new LLIssuePolSchema();
        aLLIssuePolSchema.setSchema(this);
        return aLLIssuePolSchema;
    }

    public LLIssuePolDB getDB() {
        LLIssuePolDB aDBOper = new LLIssuePolDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLIssuePol描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(CaseNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RgtNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IssueNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MessageNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MessageType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RequrieMan));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RgtState));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IssueType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Subject));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MessageCont));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Replyer));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BackRgtState));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(State));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLIssuePol>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            CaseNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                    SysConst.PACKAGESPILTER);
            RgtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                   SysConst.PACKAGESPILTER);
            IssueNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                     SysConst.PACKAGESPILTER);
            MessageNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                       SysConst.PACKAGESPILTER);
            MessageType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                         SysConst.PACKAGESPILTER);
            RequrieMan = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                        SysConst.PACKAGESPILTER);
            RgtState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                      SysConst.PACKAGESPILTER);
            IssueType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                       SysConst.PACKAGESPILTER);
            Subject = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                     SysConst.PACKAGESPILTER);
            MessageCont = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                         SysConst.PACKAGESPILTER);
            Replyer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                     SysConst.PACKAGESPILTER);
            BackRgtState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                          SysConst.PACKAGESPILTER);
            State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                   SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                      SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                       SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 16, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 18, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                        SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLIssuePolSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("CaseNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CaseNo));
        }
        if (FCode.equals("RgtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RgtNo));
        }
        if (FCode.equals("IssueNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IssueNo));
        }
        if (FCode.equals("MessageNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MessageNo));
        }
        if (FCode.equals("MessageType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MessageType));
        }
        if (FCode.equals("RequrieMan")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RequrieMan));
        }
        if (FCode.equals("RgtState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RgtState));
        }
        if (FCode.equals("IssueType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IssueType));
        }
        if (FCode.equals("Subject")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Subject));
        }
        if (FCode.equals("MessageCont")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MessageCont));
        }
        if (FCode.equals("Replyer")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Replyer));
        }
        if (FCode.equals("BackRgtState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BackRgtState));
        }
        if (FCode.equals("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(CaseNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(RgtNo);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(IssueNo);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(MessageNo);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(MessageType);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(RequrieMan);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(RgtState);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(IssueType);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(Subject);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(MessageCont);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(Replyer);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(BackRgtState);
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(State);
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(ManageCom);
            break;
        case 15:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 16:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 17:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 18:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("CaseNo")) {
            if (FValue != null && !FValue.equals("")) {
                CaseNo = FValue.trim();
            } else {
                CaseNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("RgtNo")) {
            if (FValue != null && !FValue.equals("")) {
                RgtNo = FValue.trim();
            } else {
                RgtNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("IssueNo")) {
            if (FValue != null && !FValue.equals("")) {
                IssueNo = FValue.trim();
            } else {
                IssueNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("MessageNo")) {
            if (FValue != null && !FValue.equals("")) {
                MessageNo = FValue.trim();
            } else {
                MessageNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("MessageType")) {
            if (FValue != null && !FValue.equals("")) {
                MessageType = FValue.trim();
            } else {
                MessageType = null;
            }
        }
        if (FCode.equalsIgnoreCase("RequrieMan")) {
            if (FValue != null && !FValue.equals("")) {
                RequrieMan = FValue.trim();
            } else {
                RequrieMan = null;
            }
        }
        if (FCode.equalsIgnoreCase("RgtState")) {
            if (FValue != null && !FValue.equals("")) {
                RgtState = FValue.trim();
            } else {
                RgtState = null;
            }
        }
        if (FCode.equalsIgnoreCase("IssueType")) {
            if (FValue != null && !FValue.equals("")) {
                IssueType = FValue.trim();
            } else {
                IssueType = null;
            }
        }
        if (FCode.equalsIgnoreCase("Subject")) {
            if (FValue != null && !FValue.equals("")) {
                Subject = FValue.trim();
            } else {
                Subject = null;
            }
        }
        if (FCode.equalsIgnoreCase("MessageCont")) {
            if (FValue != null && !FValue.equals("")) {
                MessageCont = FValue.trim();
            } else {
                MessageCont = null;
            }
        }
        if (FCode.equalsIgnoreCase("Replyer")) {
            if (FValue != null && !FValue.equals("")) {
                Replyer = FValue.trim();
            } else {
                Replyer = null;
            }
        }
        if (FCode.equalsIgnoreCase("BackRgtState")) {
            if (FValue != null && !FValue.equals("")) {
                BackRgtState = FValue.trim();
            } else {
                BackRgtState = null;
            }
        }
        if (FCode.equalsIgnoreCase("State")) {
            if (FValue != null && !FValue.equals("")) {
                State = FValue.trim();
            } else {
                State = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ManageCom = FValue.trim();
            } else {
                ManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LLIssuePolSchema other = (LLIssuePolSchema) otherObject;
        return
                CaseNo.equals(other.getCaseNo())
                && RgtNo.equals(other.getRgtNo())
                && IssueNo.equals(other.getIssueNo())
                && MessageNo.equals(other.getMessageNo())
                && MessageType.equals(other.getMessageType())
                && RequrieMan.equals(other.getRequrieMan())
                && RgtState.equals(other.getRgtState())
                && IssueType.equals(other.getIssueType())
                && Subject.equals(other.getSubject())
                && MessageCont.equals(other.getMessageCont())
                && Replyer.equals(other.getReplyer())
                && BackRgtState.equals(other.getBackRgtState())
                && State.equals(other.getState())
                && Operator.equals(other.getOperator())
                && ManageCom.equals(other.getManageCom())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("CaseNo")) {
            return 0;
        }
        if (strFieldName.equals("RgtNo")) {
            return 1;
        }
        if (strFieldName.equals("IssueNo")) {
            return 2;
        }
        if (strFieldName.equals("MessageNo")) {
            return 3;
        }
        if (strFieldName.equals("MessageType")) {
            return 4;
        }
        if (strFieldName.equals("RequrieMan")) {
            return 5;
        }
        if (strFieldName.equals("RgtState")) {
            return 6;
        }
        if (strFieldName.equals("IssueType")) {
            return 7;
        }
        if (strFieldName.equals("Subject")) {
            return 8;
        }
        if (strFieldName.equals("MessageCont")) {
            return 9;
        }
        if (strFieldName.equals("Replyer")) {
            return 10;
        }
        if (strFieldName.equals("BackRgtState")) {
            return 11;
        }
        if (strFieldName.equals("State")) {
            return 12;
        }
        if (strFieldName.equals("Operator")) {
            return 13;
        }
        if (strFieldName.equals("ManageCom")) {
            return 14;
        }
        if (strFieldName.equals("MakeDate")) {
            return 15;
        }
        if (strFieldName.equals("MakeTime")) {
            return 16;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 17;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 18;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "CaseNo";
            break;
        case 1:
            strFieldName = "RgtNo";
            break;
        case 2:
            strFieldName = "IssueNo";
            break;
        case 3:
            strFieldName = "MessageNo";
            break;
        case 4:
            strFieldName = "MessageType";
            break;
        case 5:
            strFieldName = "RequrieMan";
            break;
        case 6:
            strFieldName = "RgtState";
            break;
        case 7:
            strFieldName = "IssueType";
            break;
        case 8:
            strFieldName = "Subject";
            break;
        case 9:
            strFieldName = "MessageCont";
            break;
        case 10:
            strFieldName = "Replyer";
            break;
        case 11:
            strFieldName = "BackRgtState";
            break;
        case 12:
            strFieldName = "State";
            break;
        case 13:
            strFieldName = "Operator";
            break;
        case 14:
            strFieldName = "ManageCom";
            break;
        case 15:
            strFieldName = "MakeDate";
            break;
        case 16:
            strFieldName = "MakeTime";
            break;
        case 17:
            strFieldName = "ModifyDate";
            break;
        case 18:
            strFieldName = "ModifyTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("CaseNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RgtNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("IssueNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MessageNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MessageType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RequrieMan")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RgtState")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("IssueType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Subject")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MessageCont")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Replyer")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BackRgtState")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("State")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 12:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 13:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 14:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 15:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 16:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 17:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 18:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
