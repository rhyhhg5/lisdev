/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LHCaseTaskRelaDB;

/*
 * <p>ClassName: LHCaseTaskRelaSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PICCH-郭楠-表结构修改-服务实施管理-20060731
 * @CreateDate：2006-08-01
 */
public class LHCaseTaskRelaSchema implements Schema, Cloneable {
    // @Field
    /** 服务事件号码 */
    private String ServCaseCode;
    /** 服务任务号码 */
    private String ServTaskNo;
    /** 服务任务代码 */
    private String ServTaskCode;
    /** 服务任务确认 */
    private String ServTaskAffirm;
    /** 服务任务状态 */
    private String ServTaskState;
    /** 任务操作方式 */
    private String TaskOperType;
    /** 任务实施人员 */
    private String ExecuteOperator;
    /** 计划实施时间 */
    private Date PlanExeDate;
    /** 任务完成时间 */
    private Date TaskFinishDate;
    /** 服务任务说明 */
    private String TaskDesc;
    /** 管理机构 */
    private String ManageCom;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后修改日期 */
    private Date ModifyDate;
    /** 最后修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 16; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LHCaseTaskRelaSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "ServTaskNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LHCaseTaskRelaSchema cloned = (LHCaseTaskRelaSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getServCaseCode() {
        return ServCaseCode;
    }

    public void setServCaseCode(String aServCaseCode) {
        ServCaseCode = aServCaseCode;
    }

    public String getServTaskNo() {
        return ServTaskNo;
    }

    public void setServTaskNo(String aServTaskNo) {
        ServTaskNo = aServTaskNo;
    }

    public String getServTaskCode() {
        return ServTaskCode;
    }

    public void setServTaskCode(String aServTaskCode) {
        ServTaskCode = aServTaskCode;
    }

    public String getServTaskAffirm() {
        return ServTaskAffirm;
    }

    public void setServTaskAffirm(String aServTaskAffirm) {
        ServTaskAffirm = aServTaskAffirm;
    }

    public String getServTaskState() {
        return ServTaskState;
    }

    public void setServTaskState(String aServTaskState) {
        ServTaskState = aServTaskState;
    }

    public String getTaskOperType() {
        return TaskOperType;
    }

    public void setTaskOperType(String aTaskOperType) {
        TaskOperType = aTaskOperType;
    }

    public String getExecuteOperator() {
        return ExecuteOperator;
    }

    public void setExecuteOperator(String aExecuteOperator) {
        ExecuteOperator = aExecuteOperator;
    }

    public String getPlanExeDate() {
        if (PlanExeDate != null) {
            return fDate.getString(PlanExeDate);
        } else {
            return null;
        }
    }

    public void setPlanExeDate(Date aPlanExeDate) {
        PlanExeDate = aPlanExeDate;
    }

    public void setPlanExeDate(String aPlanExeDate) {
        if (aPlanExeDate != null && !aPlanExeDate.equals("")) {
            PlanExeDate = fDate.getDate(aPlanExeDate);
        } else {
            PlanExeDate = null;
        }
    }

    public String getTaskFinishDate() {
        if (TaskFinishDate != null) {
            return fDate.getString(TaskFinishDate);
        } else {
            return null;
        }
    }

    public void setTaskFinishDate(Date aTaskFinishDate) {
        TaskFinishDate = aTaskFinishDate;
    }

    public void setTaskFinishDate(String aTaskFinishDate) {
        if (aTaskFinishDate != null && !aTaskFinishDate.equals("")) {
            TaskFinishDate = fDate.getDate(aTaskFinishDate);
        } else {
            TaskFinishDate = null;
        }
    }

    public String getTaskDesc() {
        return TaskDesc;
    }

    public void setTaskDesc(String aTaskDesc) {
        TaskDesc = aTaskDesc;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LHCaseTaskRelaSchema 对象给 Schema 赋值
     * @param: aLHCaseTaskRelaSchema LHCaseTaskRelaSchema
     **/
    public void setSchema(LHCaseTaskRelaSchema aLHCaseTaskRelaSchema) {
        this.ServCaseCode = aLHCaseTaskRelaSchema.getServCaseCode();
        this.ServTaskNo = aLHCaseTaskRelaSchema.getServTaskNo();
        this.ServTaskCode = aLHCaseTaskRelaSchema.getServTaskCode();
        this.ServTaskAffirm = aLHCaseTaskRelaSchema.getServTaskAffirm();
        this.ServTaskState = aLHCaseTaskRelaSchema.getServTaskState();
        this.TaskOperType = aLHCaseTaskRelaSchema.getTaskOperType();
        this.ExecuteOperator = aLHCaseTaskRelaSchema.getExecuteOperator();
        this.PlanExeDate = fDate.getDate(aLHCaseTaskRelaSchema.getPlanExeDate());
        this.TaskFinishDate = fDate.getDate(aLHCaseTaskRelaSchema.
                                            getTaskFinishDate());
        this.TaskDesc = aLHCaseTaskRelaSchema.getTaskDesc();
        this.ManageCom = aLHCaseTaskRelaSchema.getManageCom();
        this.Operator = aLHCaseTaskRelaSchema.getOperator();
        this.MakeDate = fDate.getDate(aLHCaseTaskRelaSchema.getMakeDate());
        this.MakeTime = aLHCaseTaskRelaSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLHCaseTaskRelaSchema.getModifyDate());
        this.ModifyTime = aLHCaseTaskRelaSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ServCaseCode") == null) {
                this.ServCaseCode = null;
            } else {
                this.ServCaseCode = rs.getString("ServCaseCode").trim();
            }

            if (rs.getString("ServTaskNo") == null) {
                this.ServTaskNo = null;
            } else {
                this.ServTaskNo = rs.getString("ServTaskNo").trim();
            }

            if (rs.getString("ServTaskCode") == null) {
                this.ServTaskCode = null;
            } else {
                this.ServTaskCode = rs.getString("ServTaskCode").trim();
            }

            if (rs.getString("ServTaskAffirm") == null) {
                this.ServTaskAffirm = null;
            } else {
                this.ServTaskAffirm = rs.getString("ServTaskAffirm").trim();
            }

            if (rs.getString("ServTaskState") == null) {
                this.ServTaskState = null;
            } else {
                this.ServTaskState = rs.getString("ServTaskState").trim();
            }

            if (rs.getString("TaskOperType") == null) {
                this.TaskOperType = null;
            } else {
                this.TaskOperType = rs.getString("TaskOperType").trim();
            }

            if (rs.getString("ExecuteOperator") == null) {
                this.ExecuteOperator = null;
            } else {
                this.ExecuteOperator = rs.getString("ExecuteOperator").trim();
            }

            this.PlanExeDate = rs.getDate("PlanExeDate");
            this.TaskFinishDate = rs.getDate("TaskFinishDate");
            if (rs.getString("TaskDesc") == null) {
                this.TaskDesc = null;
            } else {
                this.TaskDesc = rs.getString("TaskDesc").trim();
            }

            if (rs.getString("ManageCom") == null) {
                this.ManageCom = null;
            } else {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LHCaseTaskRela表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHCaseTaskRelaSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LHCaseTaskRelaSchema getSchema() {
        LHCaseTaskRelaSchema aLHCaseTaskRelaSchema = new LHCaseTaskRelaSchema();
        aLHCaseTaskRelaSchema.setSchema(this);
        return aLHCaseTaskRelaSchema;
    }

    public LHCaseTaskRelaDB getDB() {
        LHCaseTaskRelaDB aDBOper = new LHCaseTaskRelaDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHCaseTaskRela描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(ServCaseCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServTaskNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServTaskCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServTaskAffirm));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServTaskState));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TaskOperType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ExecuteOperator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(PlanExeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(TaskFinishDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TaskDesc));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHCaseTaskRela>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            ServCaseCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                          SysConst.PACKAGESPILTER);
            ServTaskNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
            ServTaskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                          SysConst.PACKAGESPILTER);
            ServTaskAffirm = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                            SysConst.PACKAGESPILTER);
            ServTaskState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                           SysConst.PACKAGESPILTER);
            TaskOperType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                          SysConst.PACKAGESPILTER);
            ExecuteOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             7, SysConst.PACKAGESPILTER);
            PlanExeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 8, SysConst.PACKAGESPILTER));
            TaskFinishDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 9, SysConst.PACKAGESPILTER));
            TaskDesc = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                      SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                       SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 13, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 15, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                        SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHCaseTaskRelaSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("ServCaseCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServCaseCode));
        }
        if (FCode.equals("ServTaskNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServTaskNo));
        }
        if (FCode.equals("ServTaskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServTaskCode));
        }
        if (FCode.equals("ServTaskAffirm")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServTaskAffirm));
        }
        if (FCode.equals("ServTaskState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServTaskState));
        }
        if (FCode.equals("TaskOperType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TaskOperType));
        }
        if (FCode.equals("ExecuteOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExecuteOperator));
        }
        if (FCode.equals("PlanExeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getPlanExeDate()));
        }
        if (FCode.equals("TaskFinishDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getTaskFinishDate()));
        }
        if (FCode.equals("TaskDesc")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TaskDesc));
        }
        if (FCode.equals("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(ServCaseCode);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(ServTaskNo);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(ServTaskCode);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(ServTaskAffirm);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(ServTaskState);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(TaskOperType);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(ExecuteOperator);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getPlanExeDate()));
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getTaskFinishDate()));
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(TaskDesc);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(ManageCom);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 15:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("ServCaseCode")) {
            if (FValue != null && !FValue.equals("")) {
                ServCaseCode = FValue.trim();
            } else {
                ServCaseCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServTaskNo")) {
            if (FValue != null && !FValue.equals("")) {
                ServTaskNo = FValue.trim();
            } else {
                ServTaskNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServTaskCode")) {
            if (FValue != null && !FValue.equals("")) {
                ServTaskCode = FValue.trim();
            } else {
                ServTaskCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServTaskAffirm")) {
            if (FValue != null && !FValue.equals("")) {
                ServTaskAffirm = FValue.trim();
            } else {
                ServTaskAffirm = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServTaskState")) {
            if (FValue != null && !FValue.equals("")) {
                ServTaskState = FValue.trim();
            } else {
                ServTaskState = null;
            }
        }
        if (FCode.equalsIgnoreCase("TaskOperType")) {
            if (FValue != null && !FValue.equals("")) {
                TaskOperType = FValue.trim();
            } else {
                TaskOperType = null;
            }
        }
        if (FCode.equalsIgnoreCase("ExecuteOperator")) {
            if (FValue != null && !FValue.equals("")) {
                ExecuteOperator = FValue.trim();
            } else {
                ExecuteOperator = null;
            }
        }
        if (FCode.equalsIgnoreCase("PlanExeDate")) {
            if (FValue != null && !FValue.equals("")) {
                PlanExeDate = fDate.getDate(FValue);
            } else {
                PlanExeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("TaskFinishDate")) {
            if (FValue != null && !FValue.equals("")) {
                TaskFinishDate = fDate.getDate(FValue);
            } else {
                TaskFinishDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("TaskDesc")) {
            if (FValue != null && !FValue.equals("")) {
                TaskDesc = FValue.trim();
            } else {
                TaskDesc = null;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ManageCom = FValue.trim();
            } else {
                ManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LHCaseTaskRelaSchema other = (LHCaseTaskRelaSchema) otherObject;
        return
                ServCaseCode.equals(other.getServCaseCode())
                && ServTaskNo.equals(other.getServTaskNo())
                && ServTaskCode.equals(other.getServTaskCode())
                && ServTaskAffirm.equals(other.getServTaskAffirm())
                && ServTaskState.equals(other.getServTaskState())
                && TaskOperType.equals(other.getTaskOperType())
                && ExecuteOperator.equals(other.getExecuteOperator())
                && fDate.getString(PlanExeDate).equals(other.getPlanExeDate())
                &&
                fDate.getString(TaskFinishDate).equals(other.getTaskFinishDate())
                && TaskDesc.equals(other.getTaskDesc())
                && ManageCom.equals(other.getManageCom())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("ServCaseCode")) {
            return 0;
        }
        if (strFieldName.equals("ServTaskNo")) {
            return 1;
        }
        if (strFieldName.equals("ServTaskCode")) {
            return 2;
        }
        if (strFieldName.equals("ServTaskAffirm")) {
            return 3;
        }
        if (strFieldName.equals("ServTaskState")) {
            return 4;
        }
        if (strFieldName.equals("TaskOperType")) {
            return 5;
        }
        if (strFieldName.equals("ExecuteOperator")) {
            return 6;
        }
        if (strFieldName.equals("PlanExeDate")) {
            return 7;
        }
        if (strFieldName.equals("TaskFinishDate")) {
            return 8;
        }
        if (strFieldName.equals("TaskDesc")) {
            return 9;
        }
        if (strFieldName.equals("ManageCom")) {
            return 10;
        }
        if (strFieldName.equals("Operator")) {
            return 11;
        }
        if (strFieldName.equals("MakeDate")) {
            return 12;
        }
        if (strFieldName.equals("MakeTime")) {
            return 13;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 14;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 15;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "ServCaseCode";
            break;
        case 1:
            strFieldName = "ServTaskNo";
            break;
        case 2:
            strFieldName = "ServTaskCode";
            break;
        case 3:
            strFieldName = "ServTaskAffirm";
            break;
        case 4:
            strFieldName = "ServTaskState";
            break;
        case 5:
            strFieldName = "TaskOperType";
            break;
        case 6:
            strFieldName = "ExecuteOperator";
            break;
        case 7:
            strFieldName = "PlanExeDate";
            break;
        case 8:
            strFieldName = "TaskFinishDate";
            break;
        case 9:
            strFieldName = "TaskDesc";
            break;
        case 10:
            strFieldName = "ManageCom";
            break;
        case 11:
            strFieldName = "Operator";
            break;
        case 12:
            strFieldName = "MakeDate";
            break;
        case 13:
            strFieldName = "MakeTime";
            break;
        case 14:
            strFieldName = "ModifyDate";
            break;
        case 15:
            strFieldName = "ModifyTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("ServCaseCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServTaskNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServTaskCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServTaskAffirm")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServTaskState")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("TaskOperType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ExecuteOperator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PlanExeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("TaskFinishDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("TaskDesc")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 8:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 12:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 13:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 14:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 15:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
