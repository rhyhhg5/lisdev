/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LHEvalueExpDetailDB;

/*
 * <p>ClassName: LHEvalueExpDetailSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PICCH-郭丽颖-表结构评估管理-20061013.pdm
 * @CreateDate：2006-10-16
 */
public class LHEvalueExpDetailSchema implements Schema, Cloneable {
    // @Field
    /** 流水号 */
    private String EvalExportNo;
    /** 客户评估号码 */
    private String CusEvalCode;
    /** 标准问题代码 */
    private String StandCode;
    /** 健管信息类型 */
    private String InfoType;
    /** 健管档案信息代码 */
    private String FileInfoCode;
    /** 选项答案 */
    private String OptionResult;
    /** 文字答案 */
    private String LetterResult;
    /** 问题状态 */
    private String QuesStatus;
    /** 管理机构 */
    private String ManageCom;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后修改日期 */
    private Date ModifyDate;
    /** 最后修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 14; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LHEvalueExpDetailSchema() {
        mErrors = new CErrors();

        String[] pk = new String[0];

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LHEvalueExpDetailSchema cloned = (LHEvalueExpDetailSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getEvalExportNo() {
        return EvalExportNo;
    }

    public void setEvalExportNo(String aEvalExportNo) {
        EvalExportNo = aEvalExportNo;
    }

    public String getCusEvalCode() {
        return CusEvalCode;
    }

    public void setCusEvalCode(String aCusEvalCode) {
        CusEvalCode = aCusEvalCode;
    }

    public String getStandCode() {
        return StandCode;
    }

    public void setStandCode(String aStandCode) {
        StandCode = aStandCode;
    }

    public String getInfoType() {
        return InfoType;
    }

    public void setInfoType(String aInfoType) {
        InfoType = aInfoType;
    }

    public String getFileInfoCode() {
        return FileInfoCode;
    }

    public void setFileInfoCode(String aFileInfoCode) {
        FileInfoCode = aFileInfoCode;
    }

    public String getOptionResult() {
        return OptionResult;
    }

    public void setOptionResult(String aOptionResult) {
        OptionResult = aOptionResult;
    }

    public String getLetterResult() {
        return LetterResult;
    }

    public void setLetterResult(String aLetterResult) {
        LetterResult = aLetterResult;
    }

    public String getQuesStatus() {
        return QuesStatus;
    }

    public void setQuesStatus(String aQuesStatus) {
        QuesStatus = aQuesStatus;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LHEvalueExpDetailSchema 对象给 Schema 赋值
     * @param: aLHEvalueExpDetailSchema LHEvalueExpDetailSchema
     **/
    public void setSchema(LHEvalueExpDetailSchema aLHEvalueExpDetailSchema) {
        this.EvalExportNo = aLHEvalueExpDetailSchema.getEvalExportNo();
        this.CusEvalCode = aLHEvalueExpDetailSchema.getCusEvalCode();
        this.StandCode = aLHEvalueExpDetailSchema.getStandCode();
        this.InfoType = aLHEvalueExpDetailSchema.getInfoType();
        this.FileInfoCode = aLHEvalueExpDetailSchema.getFileInfoCode();
        this.OptionResult = aLHEvalueExpDetailSchema.getOptionResult();
        this.LetterResult = aLHEvalueExpDetailSchema.getLetterResult();
        this.QuesStatus = aLHEvalueExpDetailSchema.getQuesStatus();
        this.ManageCom = aLHEvalueExpDetailSchema.getManageCom();
        this.Operator = aLHEvalueExpDetailSchema.getOperator();
        this.MakeDate = fDate.getDate(aLHEvalueExpDetailSchema.getMakeDate());
        this.MakeTime = aLHEvalueExpDetailSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLHEvalueExpDetailSchema.getModifyDate());
        this.ModifyTime = aLHEvalueExpDetailSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("EvalExportNo") == null) {
                this.EvalExportNo = null;
            } else {
                this.EvalExportNo = rs.getString("EvalExportNo").trim();
            }

            if (rs.getString("CusEvalCode") == null) {
                this.CusEvalCode = null;
            } else {
                this.CusEvalCode = rs.getString("CusEvalCode").trim();
            }

            if (rs.getString("StandCode") == null) {
                this.StandCode = null;
            } else {
                this.StandCode = rs.getString("StandCode").trim();
            }

            if (rs.getString("InfoType") == null) {
                this.InfoType = null;
            } else {
                this.InfoType = rs.getString("InfoType").trim();
            }

            if (rs.getString("FileInfoCode") == null) {
                this.FileInfoCode = null;
            } else {
                this.FileInfoCode = rs.getString("FileInfoCode").trim();
            }

            if (rs.getString("OptionResult") == null) {
                this.OptionResult = null;
            } else {
                this.OptionResult = rs.getString("OptionResult").trim();
            }

            if (rs.getString("LetterResult") == null) {
                this.LetterResult = null;
            } else {
                this.LetterResult = rs.getString("LetterResult").trim();
            }

            if (rs.getString("QuesStatus") == null) {
                this.QuesStatus = null;
            } else {
                this.QuesStatus = rs.getString("QuesStatus").trim();
            }

            if (rs.getString("ManageCom") == null) {
                this.ManageCom = null;
            } else {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LHEvalueExpDetail表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHEvalueExpDetailSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LHEvalueExpDetailSchema getSchema() {
        LHEvalueExpDetailSchema aLHEvalueExpDetailSchema = new
                LHEvalueExpDetailSchema();
        aLHEvalueExpDetailSchema.setSchema(this);
        return aLHEvalueExpDetailSchema;
    }

    public LHEvalueExpDetailDB getDB() {
        LHEvalueExpDetailDB aDBOper = new LHEvalueExpDetailDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHEvalueExpDetail描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(EvalExportNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CusEvalCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InfoType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FileInfoCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OptionResult));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(LetterResult));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(QuesStatus));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHEvalueExpDetail>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            EvalExportNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                          SysConst.PACKAGESPILTER);
            CusEvalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                         SysConst.PACKAGESPILTER);
            StandCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                       SysConst.PACKAGESPILTER);
            InfoType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                      SysConst.PACKAGESPILTER);
            FileInfoCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                          SysConst.PACKAGESPILTER);
            OptionResult = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                          SysConst.PACKAGESPILTER);
            LetterResult = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                          SysConst.PACKAGESPILTER);
            QuesStatus = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                        SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                       SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 11, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 13, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                        SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHEvalueExpDetailSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("EvalExportNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EvalExportNo));
        }
        if (FCode.equals("CusEvalCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CusEvalCode));
        }
        if (FCode.equals("StandCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandCode));
        }
        if (FCode.equals("InfoType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InfoType));
        }
        if (FCode.equals("FileInfoCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FileInfoCode));
        }
        if (FCode.equals("OptionResult")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OptionResult));
        }
        if (FCode.equals("LetterResult")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LetterResult));
        }
        if (FCode.equals("QuesStatus")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(QuesStatus));
        }
        if (FCode.equals("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(EvalExportNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(CusEvalCode);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(StandCode);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(InfoType);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(FileInfoCode);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(OptionResult);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(LetterResult);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(QuesStatus);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(ManageCom);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("EvalExportNo")) {
            if (FValue != null && !FValue.equals("")) {
                EvalExportNo = FValue.trim();
            } else {
                EvalExportNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("CusEvalCode")) {
            if (FValue != null && !FValue.equals("")) {
                CusEvalCode = FValue.trim();
            } else {
                CusEvalCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("StandCode")) {
            if (FValue != null && !FValue.equals("")) {
                StandCode = FValue.trim();
            } else {
                StandCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("InfoType")) {
            if (FValue != null && !FValue.equals("")) {
                InfoType = FValue.trim();
            } else {
                InfoType = null;
            }
        }
        if (FCode.equalsIgnoreCase("FileInfoCode")) {
            if (FValue != null && !FValue.equals("")) {
                FileInfoCode = FValue.trim();
            } else {
                FileInfoCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("OptionResult")) {
            if (FValue != null && !FValue.equals("")) {
                OptionResult = FValue.trim();
            } else {
                OptionResult = null;
            }
        }
        if (FCode.equalsIgnoreCase("LetterResult")) {
            if (FValue != null && !FValue.equals("")) {
                LetterResult = FValue.trim();
            } else {
                LetterResult = null;
            }
        }
        if (FCode.equalsIgnoreCase("QuesStatus")) {
            if (FValue != null && !FValue.equals("")) {
                QuesStatus = FValue.trim();
            } else {
                QuesStatus = null;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ManageCom = FValue.trim();
            } else {
                ManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LHEvalueExpDetailSchema other = (LHEvalueExpDetailSchema) otherObject;
        return
                EvalExportNo.equals(other.getEvalExportNo())
                && CusEvalCode.equals(other.getCusEvalCode())
                && StandCode.equals(other.getStandCode())
                && InfoType.equals(other.getInfoType())
                && FileInfoCode.equals(other.getFileInfoCode())
                && OptionResult.equals(other.getOptionResult())
                && LetterResult.equals(other.getLetterResult())
                && QuesStatus.equals(other.getQuesStatus())
                && ManageCom.equals(other.getManageCom())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("EvalExportNo")) {
            return 0;
        }
        if (strFieldName.equals("CusEvalCode")) {
            return 1;
        }
        if (strFieldName.equals("StandCode")) {
            return 2;
        }
        if (strFieldName.equals("InfoType")) {
            return 3;
        }
        if (strFieldName.equals("FileInfoCode")) {
            return 4;
        }
        if (strFieldName.equals("OptionResult")) {
            return 5;
        }
        if (strFieldName.equals("LetterResult")) {
            return 6;
        }
        if (strFieldName.equals("QuesStatus")) {
            return 7;
        }
        if (strFieldName.equals("ManageCom")) {
            return 8;
        }
        if (strFieldName.equals("Operator")) {
            return 9;
        }
        if (strFieldName.equals("MakeDate")) {
            return 10;
        }
        if (strFieldName.equals("MakeTime")) {
            return 11;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 12;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 13;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "EvalExportNo";
            break;
        case 1:
            strFieldName = "CusEvalCode";
            break;
        case 2:
            strFieldName = "StandCode";
            break;
        case 3:
            strFieldName = "InfoType";
            break;
        case 4:
            strFieldName = "FileInfoCode";
            break;
        case 5:
            strFieldName = "OptionResult";
            break;
        case 6:
            strFieldName = "LetterResult";
            break;
        case 7:
            strFieldName = "QuesStatus";
            break;
        case 8:
            strFieldName = "ManageCom";
            break;
        case 9:
            strFieldName = "Operator";
            break;
        case 10:
            strFieldName = "MakeDate";
            break;
        case 11:
            strFieldName = "MakeTime";
            break;
        case 12:
            strFieldName = "ModifyDate";
            break;
        case 13:
            strFieldName = "ModifyTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("EvalExportNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CusEvalCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StandCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InfoType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FileInfoCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OptionResult")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("LetterResult")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("QuesStatus")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 12:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 13:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
