/*
 * <p>ClassName: LLClaimAccountSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 理赔表结构调整
 * @CreateDate：2004-12-16
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LLClaimAccountDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LLClaimAccountSchema implements Schema
{
    // @Field
    /** 赔案号 */
    private String ClmNo;
    /** 拒赔号 */
    private String DeclineNo;
    /** 帐户号码 */
    private String AccNo;
    /** 给付责任编码 */
    private String GetDutyCode;
    /** 集体合同号 */
    private String GrpContNo;
    /** 集体保单号 */
    private String GrpPolNo;
    /** 个单合同号 */
    private String ContNo;
    /** 保单号 */
    private String PolNo;
    /** 险类代码 */
    private String KindCode;
    /** 险种版本号 */
    private String RiskVer;
    /** 险种代码 */
    private String RiskCode;
    /** 保单管理机构 */
    private String PolMngCom;
    /** 销售渠道 */
    private String SaleChnl;
    /** 代理人代码 */
    private String AgentCode;
    /** 代理人组别 */
    private String AgentGroup;
    /** 账户赔付金额 */
    private double AccPayMoney;
    /** 管理机构 */
    private String MngCom;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 22; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LLClaimAccountSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[5];
        pk[0] = "ClmNo";
        pk[1] = "DeclineNo";
        pk[2] = "AccNo";
        pk[3] = "GetDutyCode";
        pk[4] = "PolNo";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getClmNo()
    {
        if (ClmNo != null && !ClmNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            ClmNo = StrTool.unicodeToGBK(ClmNo);
        }
        return ClmNo;
    }

    public void setClmNo(String aClmNo)
    {
        ClmNo = aClmNo;
    }

    public String getDeclineNo()
    {
        if (DeclineNo != null && !DeclineNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            DeclineNo = StrTool.unicodeToGBK(DeclineNo);
        }
        return DeclineNo;
    }

    public void setDeclineNo(String aDeclineNo)
    {
        DeclineNo = aDeclineNo;
    }

    public String getAccNo()
    {
        if (AccNo != null && !AccNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            AccNo = StrTool.unicodeToGBK(AccNo);
        }
        return AccNo;
    }

    public void setAccNo(String aAccNo)
    {
        AccNo = aAccNo;
    }

    public String getGetDutyCode()
    {
        if (GetDutyCode != null && !GetDutyCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GetDutyCode = StrTool.unicodeToGBK(GetDutyCode);
        }
        return GetDutyCode;
    }

    public void setGetDutyCode(String aGetDutyCode)
    {
        GetDutyCode = aGetDutyCode;
    }

    public String getGrpContNo()
    {
        if (GrpContNo != null && !GrpContNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GrpContNo = StrTool.unicodeToGBK(GrpContNo);
        }
        return GrpContNo;
    }

    public void setGrpContNo(String aGrpContNo)
    {
        GrpContNo = aGrpContNo;
    }

    public String getGrpPolNo()
    {
        if (GrpPolNo != null && !GrpPolNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            GrpPolNo = StrTool.unicodeToGBK(GrpPolNo);
        }
        return GrpPolNo;
    }

    public void setGrpPolNo(String aGrpPolNo)
    {
        GrpPolNo = aGrpPolNo;
    }

    public String getContNo()
    {
        if (ContNo != null && !ContNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            ContNo = StrTool.unicodeToGBK(ContNo);
        }
        return ContNo;
    }

    public void setContNo(String aContNo)
    {
        ContNo = aContNo;
    }

    public String getPolNo()
    {
        if (PolNo != null && !PolNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            PolNo = StrTool.unicodeToGBK(PolNo);
        }
        return PolNo;
    }

    public void setPolNo(String aPolNo)
    {
        PolNo = aPolNo;
    }

    public String getKindCode()
    {
        if (KindCode != null && !KindCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            KindCode = StrTool.unicodeToGBK(KindCode);
        }
        return KindCode;
    }

    public void setKindCode(String aKindCode)
    {
        KindCode = aKindCode;
    }

    public String getRiskVer()
    {
        if (RiskVer != null && !RiskVer.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskVer = StrTool.unicodeToGBK(RiskVer);
        }
        return RiskVer;
    }

    public void setRiskVer(String aRiskVer)
    {
        RiskVer = aRiskVer;
    }

    public String getRiskCode()
    {
        if (RiskCode != null && !RiskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getPolMngCom()
    {
        if (PolMngCom != null && !PolMngCom.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            PolMngCom = StrTool.unicodeToGBK(PolMngCom);
        }
        return PolMngCom;
    }

    public void setPolMngCom(String aPolMngCom)
    {
        PolMngCom = aPolMngCom;
    }

    public String getSaleChnl()
    {
        if (SaleChnl != null && !SaleChnl.equals("") && SysConst.CHANGECHARSET == true)
        {
            SaleChnl = StrTool.unicodeToGBK(SaleChnl);
        }
        return SaleChnl;
    }

    public void setSaleChnl(String aSaleChnl)
    {
        SaleChnl = aSaleChnl;
    }

    public String getAgentCode()
    {
        if (AgentCode != null && !AgentCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AgentCode = StrTool.unicodeToGBK(AgentCode);
        }
        return AgentCode;
    }

    public void setAgentCode(String aAgentCode)
    {
        AgentCode = aAgentCode;
    }

    public String getAgentGroup()
    {
        if (AgentGroup != null && !AgentGroup.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AgentGroup = StrTool.unicodeToGBK(AgentGroup);
        }
        return AgentGroup;
    }

    public void setAgentGroup(String aAgentGroup)
    {
        AgentGroup = aAgentGroup;
    }

    public double getAccPayMoney()
    {
        return AccPayMoney;
    }

    public void setAccPayMoney(double aAccPayMoney)
    {
        AccPayMoney = aAccPayMoney;
    }

    public void setAccPayMoney(String aAccPayMoney)
    {
        if (aAccPayMoney != null && !aAccPayMoney.equals(""))
        {
            Double tDouble = new Double(aAccPayMoney);
            double d = tDouble.doubleValue();
            AccPayMoney = d;
        }
    }

    public String getMngCom()
    {
        if (MngCom != null && !MngCom.equals("") && SysConst.CHANGECHARSET == true)
        {
            MngCom = StrTool.unicodeToGBK(MngCom);
        }
        return MngCom;
    }

    public void setMngCom(String aMngCom)
    {
        MngCom = aMngCom;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LLClaimAccountSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LLClaimAccountSchema aLLClaimAccountSchema)
    {
        this.ClmNo = aLLClaimAccountSchema.getClmNo();
        this.DeclineNo = aLLClaimAccountSchema.getDeclineNo();
        this.AccNo = aLLClaimAccountSchema.getAccNo();
        this.GetDutyCode = aLLClaimAccountSchema.getGetDutyCode();
        this.GrpContNo = aLLClaimAccountSchema.getGrpContNo();
        this.GrpPolNo = aLLClaimAccountSchema.getGrpPolNo();
        this.ContNo = aLLClaimAccountSchema.getContNo();
        this.PolNo = aLLClaimAccountSchema.getPolNo();
        this.KindCode = aLLClaimAccountSchema.getKindCode();
        this.RiskVer = aLLClaimAccountSchema.getRiskVer();
        this.RiskCode = aLLClaimAccountSchema.getRiskCode();
        this.PolMngCom = aLLClaimAccountSchema.getPolMngCom();
        this.SaleChnl = aLLClaimAccountSchema.getSaleChnl();
        this.AgentCode = aLLClaimAccountSchema.getAgentCode();
        this.AgentGroup = aLLClaimAccountSchema.getAgentGroup();
        this.AccPayMoney = aLLClaimAccountSchema.getAccPayMoney();
        this.MngCom = aLLClaimAccountSchema.getMngCom();
        this.Operator = aLLClaimAccountSchema.getOperator();
        this.MakeDate = fDate.getDate(aLLClaimAccountSchema.getMakeDate());
        this.MakeTime = aLLClaimAccountSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLLClaimAccountSchema.getModifyDate());
        this.ModifyTime = aLLClaimAccountSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ClmNo") == null)
            {
                this.ClmNo = null;
            }
            else
            {
                this.ClmNo = rs.getString("ClmNo").trim();
            }

            if (rs.getString("DeclineNo") == null)
            {
                this.DeclineNo = null;
            }
            else
            {
                this.DeclineNo = rs.getString("DeclineNo").trim();
            }

            if (rs.getString("AccNo") == null)
            {
                this.AccNo = null;
            }
            else
            {
                this.AccNo = rs.getString("AccNo").trim();
            }

            if (rs.getString("GetDutyCode") == null)
            {
                this.GetDutyCode = null;
            }
            else
            {
                this.GetDutyCode = rs.getString("GetDutyCode").trim();
            }

            if (rs.getString("GrpContNo") == null)
            {
                this.GrpContNo = null;
            }
            else
            {
                this.GrpContNo = rs.getString("GrpContNo").trim();
            }

            if (rs.getString("GrpPolNo") == null)
            {
                this.GrpPolNo = null;
            }
            else
            {
                this.GrpPolNo = rs.getString("GrpPolNo").trim();
            }

            if (rs.getString("ContNo") == null)
            {
                this.ContNo = null;
            }
            else
            {
                this.ContNo = rs.getString("ContNo").trim();
            }

            if (rs.getString("PolNo") == null)
            {
                this.PolNo = null;
            }
            else
            {
                this.PolNo = rs.getString("PolNo").trim();
            }

            if (rs.getString("KindCode") == null)
            {
                this.KindCode = null;
            }
            else
            {
                this.KindCode = rs.getString("KindCode").trim();
            }

            if (rs.getString("RiskVer") == null)
            {
                this.RiskVer = null;
            }
            else
            {
                this.RiskVer = rs.getString("RiskVer").trim();
            }

            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("PolMngCom") == null)
            {
                this.PolMngCom = null;
            }
            else
            {
                this.PolMngCom = rs.getString("PolMngCom").trim();
            }

            if (rs.getString("SaleChnl") == null)
            {
                this.SaleChnl = null;
            }
            else
            {
                this.SaleChnl = rs.getString("SaleChnl").trim();
            }

            if (rs.getString("AgentCode") == null)
            {
                this.AgentCode = null;
            }
            else
            {
                this.AgentCode = rs.getString("AgentCode").trim();
            }

            if (rs.getString("AgentGroup") == null)
            {
                this.AgentGroup = null;
            }
            else
            {
                this.AgentGroup = rs.getString("AgentGroup").trim();
            }

            this.AccPayMoney = rs.getDouble("AccPayMoney");
            if (rs.getString("MngCom") == null)
            {
                this.MngCom = null;
            }
            else
            {
                this.MngCom = rs.getString("MngCom").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLClaimAccountSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LLClaimAccountSchema getSchema()
    {
        LLClaimAccountSchema aLLClaimAccountSchema = new LLClaimAccountSchema();
        aLLClaimAccountSchema.setSchema(this);
        return aLLClaimAccountSchema;
    }

    public LLClaimAccountDB getDB()
    {
        LLClaimAccountDB aDBOper = new LLClaimAccountDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLClaimAccount描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(ClmNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(DeclineNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AccNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GetDutyCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GrpContNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GrpPolNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ContNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PolNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(KindCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskVer)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PolMngCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SaleChnl)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgentCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgentGroup)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(AccPayMoney) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MngCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLClaimAccount>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            ClmNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                   SysConst.PACKAGESPILTER);
            DeclineNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                       SysConst.PACKAGESPILTER);
            AccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                   SysConst.PACKAGESPILTER);
            GetDutyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                         SysConst.PACKAGESPILTER);
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                       SysConst.PACKAGESPILTER);
            GrpPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                      SysConst.PACKAGESPILTER);
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                    SysConst.PACKAGESPILTER);
            PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                   SysConst.PACKAGESPILTER);
            KindCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                      SysConst.PACKAGESPILTER);
            RiskVer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                     SysConst.PACKAGESPILTER);
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                      SysConst.PACKAGESPILTER);
            PolMngCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                       SysConst.PACKAGESPILTER);
            SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                      SysConst.PACKAGESPILTER);
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                       SysConst.PACKAGESPILTER);
            AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                        SysConst.PACKAGESPILTER);
            AccPayMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 16, SysConst.PACKAGESPILTER))).doubleValue();
            MngCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                    SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 19, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 21, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLClaimAccountSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("ClmNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ClmNo));
        }
        if (FCode.equals("DeclineNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DeclineNo));
        }
        if (FCode.equals("AccNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AccNo));
        }
        if (FCode.equals("GetDutyCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetDutyCode));
        }
        if (FCode.equals("GrpContNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GrpContNo));
        }
        if (FCode.equals("GrpPolNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GrpPolNo));
        }
        if (FCode.equals("ContNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ContNo));
        }
        if (FCode.equals("PolNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PolNo));
        }
        if (FCode.equals("KindCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(KindCode));
        }
        if (FCode.equals("RiskVer"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskVer));
        }
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskCode));
        }
        if (FCode.equals("PolMngCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PolMngCom));
        }
        if (FCode.equals("SaleChnl"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SaleChnl));
        }
        if (FCode.equals("AgentCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AgentCode));
        }
        if (FCode.equals("AgentGroup"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AgentGroup));
        }
        if (FCode.equals("AccPayMoney"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AccPayMoney));
        }
        if (FCode.equals("MngCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MngCom));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ClmNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(DeclineNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(AccNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(GetDutyCode);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(GrpContNo);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(GrpPolNo);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(PolNo);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(KindCode);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(RiskVer);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(PolMngCom);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(SaleChnl);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(AgentCode);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(AgentGroup);
                break;
            case 15:
                strFieldValue = String.valueOf(AccPayMoney);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(MngCom);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("ClmNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ClmNo = FValue.trim();
            }
            else
            {
                ClmNo = null;
            }
        }
        if (FCode.equals("DeclineNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DeclineNo = FValue.trim();
            }
            else
            {
                DeclineNo = null;
            }
        }
        if (FCode.equals("AccNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AccNo = FValue.trim();
            }
            else
            {
                AccNo = null;
            }
        }
        if (FCode.equals("GetDutyCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetDutyCode = FValue.trim();
            }
            else
            {
                GetDutyCode = null;
            }
        }
        if (FCode.equals("GrpContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
            {
                GrpContNo = null;
            }
        }
        if (FCode.equals("GrpPolNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpPolNo = FValue.trim();
            }
            else
            {
                GrpPolNo = null;
            }
        }
        if (FCode.equals("ContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
            {
                ContNo = null;
            }
        }
        if (FCode.equals("PolNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
            {
                PolNo = null;
            }
        }
        if (FCode.equals("KindCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                KindCode = FValue.trim();
            }
            else
            {
                KindCode = null;
            }
        }
        if (FCode.equals("RiskVer"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskVer = FValue.trim();
            }
            else
            {
                RiskVer = null;
            }
        }
        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("PolMngCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PolMngCom = FValue.trim();
            }
            else
            {
                PolMngCom = null;
            }
        }
        if (FCode.equals("SaleChnl"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SaleChnl = FValue.trim();
            }
            else
            {
                SaleChnl = null;
            }
        }
        if (FCode.equals("AgentCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
            {
                AgentCode = null;
            }
        }
        if (FCode.equals("AgentGroup"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
            {
                AgentGroup = null;
            }
        }
        if (FCode.equals("AccPayMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                AccPayMoney = d;
            }
        }
        if (FCode.equals("MngCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MngCom = FValue.trim();
            }
            else
            {
                MngCom = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LLClaimAccountSchema other = (LLClaimAccountSchema) otherObject;
        return
                ClmNo.equals(other.getClmNo())
                && DeclineNo.equals(other.getDeclineNo())
                && AccNo.equals(other.getAccNo())
                && GetDutyCode.equals(other.getGetDutyCode())
                && GrpContNo.equals(other.getGrpContNo())
                && GrpPolNo.equals(other.getGrpPolNo())
                && ContNo.equals(other.getContNo())
                && PolNo.equals(other.getPolNo())
                && KindCode.equals(other.getKindCode())
                && RiskVer.equals(other.getRiskVer())
                && RiskCode.equals(other.getRiskCode())
                && PolMngCom.equals(other.getPolMngCom())
                && SaleChnl.equals(other.getSaleChnl())
                && AgentCode.equals(other.getAgentCode())
                && AgentGroup.equals(other.getAgentGroup())
                && AccPayMoney == other.getAccPayMoney()
                && MngCom.equals(other.getMngCom())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("ClmNo"))
        {
            return 0;
        }
        if (strFieldName.equals("DeclineNo"))
        {
            return 1;
        }
        if (strFieldName.equals("AccNo"))
        {
            return 2;
        }
        if (strFieldName.equals("GetDutyCode"))
        {
            return 3;
        }
        if (strFieldName.equals("GrpContNo"))
        {
            return 4;
        }
        if (strFieldName.equals("GrpPolNo"))
        {
            return 5;
        }
        if (strFieldName.equals("ContNo"))
        {
            return 6;
        }
        if (strFieldName.equals("PolNo"))
        {
            return 7;
        }
        if (strFieldName.equals("KindCode"))
        {
            return 8;
        }
        if (strFieldName.equals("RiskVer"))
        {
            return 9;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return 10;
        }
        if (strFieldName.equals("PolMngCom"))
        {
            return 11;
        }
        if (strFieldName.equals("SaleChnl"))
        {
            return 12;
        }
        if (strFieldName.equals("AgentCode"))
        {
            return 13;
        }
        if (strFieldName.equals("AgentGroup"))
        {
            return 14;
        }
        if (strFieldName.equals("AccPayMoney"))
        {
            return 15;
        }
        if (strFieldName.equals("MngCom"))
        {
            return 16;
        }
        if (strFieldName.equals("Operator"))
        {
            return 17;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 18;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 19;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 20;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 21;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "ClmNo";
                break;
            case 1:
                strFieldName = "DeclineNo";
                break;
            case 2:
                strFieldName = "AccNo";
                break;
            case 3:
                strFieldName = "GetDutyCode";
                break;
            case 4:
                strFieldName = "GrpContNo";
                break;
            case 5:
                strFieldName = "GrpPolNo";
                break;
            case 6:
                strFieldName = "ContNo";
                break;
            case 7:
                strFieldName = "PolNo";
                break;
            case 8:
                strFieldName = "KindCode";
                break;
            case 9:
                strFieldName = "RiskVer";
                break;
            case 10:
                strFieldName = "RiskCode";
                break;
            case 11:
                strFieldName = "PolMngCom";
                break;
            case 12:
                strFieldName = "SaleChnl";
                break;
            case 13:
                strFieldName = "AgentCode";
                break;
            case 14:
                strFieldName = "AgentGroup";
                break;
            case 15:
                strFieldName = "AccPayMoney";
                break;
            case 16:
                strFieldName = "MngCom";
                break;
            case 17:
                strFieldName = "Operator";
                break;
            case 18:
                strFieldName = "MakeDate";
                break;
            case 19:
                strFieldName = "MakeTime";
                break;
            case 20:
                strFieldName = "ModifyDate";
                break;
            case 21:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("ClmNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DeclineNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AccNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetDutyCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpPolNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PolNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("KindCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskVer"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PolMngCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SaleChnl"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentGroup"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AccPayMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("MngCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 17:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 18:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 19:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 20:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 21:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
