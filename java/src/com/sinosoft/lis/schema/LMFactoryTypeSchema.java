/*
 * <p>ClassName: LMFactoryTypeSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 健康险要素设计
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LMFactoryTypeDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LMFactoryTypeSchema implements Schema
{
    // @Field
    /** 要素类型 */
    private String FactoryType;
    /** 要素类型名称 */
    private String FactoryTypeName;
    /** 备注 */
    private String Remark;

    public static final int FIELDNUM = 3; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LMFactoryTypeSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "FactoryType";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getFactoryType()
    {
        if (FactoryType != null && !FactoryType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            FactoryType = StrTool.unicodeToGBK(FactoryType);
        }
        return FactoryType;
    }

    public void setFactoryType(String aFactoryType)
    {
        FactoryType = aFactoryType;
    }

    public String getFactoryTypeName()
    {
        if (FactoryTypeName != null && !FactoryTypeName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            FactoryTypeName = StrTool.unicodeToGBK(FactoryTypeName);
        }
        return FactoryTypeName;
    }

    public void setFactoryTypeName(String aFactoryTypeName)
    {
        FactoryTypeName = aFactoryTypeName;
    }

    public String getRemark()
    {
        if (Remark != null && !Remark.equals("") && SysConst.CHANGECHARSET == true)
        {
            Remark = StrTool.unicodeToGBK(Remark);
        }
        return Remark;
    }

    public void setRemark(String aRemark)
    {
        Remark = aRemark;
    }

    /**
     * 使用另外一个 LMFactoryTypeSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LMFactoryTypeSchema aLMFactoryTypeSchema)
    {
        this.FactoryType = aLMFactoryTypeSchema.getFactoryType();
        this.FactoryTypeName = aLMFactoryTypeSchema.getFactoryTypeName();
        this.Remark = aLMFactoryTypeSchema.getRemark();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("FactoryType") == null)
            {
                this.FactoryType = null;
            }
            else
            {
                this.FactoryType = rs.getString("FactoryType").trim();
            }

            if (rs.getString("FactoryTypeName") == null)
            {
                this.FactoryTypeName = null;
            }
            else
            {
                this.FactoryTypeName = rs.getString("FactoryTypeName").trim();
            }

            if (rs.getString("Remark") == null)
            {
                this.Remark = null;
            }
            else
            {
                this.Remark = rs.getString("Remark").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMFactoryTypeSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LMFactoryTypeSchema getSchema()
    {
        LMFactoryTypeSchema aLMFactoryTypeSchema = new LMFactoryTypeSchema();
        aLMFactoryTypeSchema.setSchema(this);
        return aLMFactoryTypeSchema;
    }

    public LMFactoryTypeDB getDB()
    {
        LMFactoryTypeDB aDBOper = new LMFactoryTypeDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMFactoryType描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(FactoryType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(FactoryTypeName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Remark));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMFactoryType>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            FactoryType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                         SysConst.PACKAGESPILTER);
            FactoryTypeName = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             2, SysConst.PACKAGESPILTER);
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                    SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMFactoryTypeSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("FactoryType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(FactoryType));
        }
        if (FCode.equals("FactoryTypeName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(FactoryTypeName));
        }
        if (FCode.equals("Remark"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Remark));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(FactoryType);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(FactoryTypeName);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("FactoryType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                FactoryType = FValue.trim();
            }
            else
            {
                FactoryType = null;
            }
        }
        if (FCode.equals("FactoryTypeName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                FactoryTypeName = FValue.trim();
            }
            else
            {
                FactoryTypeName = null;
            }
        }
        if (FCode.equals("Remark"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
            {
                Remark = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LMFactoryTypeSchema other = (LMFactoryTypeSchema) otherObject;
        return
                FactoryType.equals(other.getFactoryType())
                && FactoryTypeName.equals(other.getFactoryTypeName())
                && Remark.equals(other.getRemark());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("FactoryType"))
        {
            return 0;
        }
        if (strFieldName.equals("FactoryTypeName"))
        {
            return 1;
        }
        if (strFieldName.equals("Remark"))
        {
            return 2;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "FactoryType";
                break;
            case 1:
                strFieldName = "FactoryTypeName";
                break;
            case 2:
                strFieldName = "Remark";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("FactoryType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FactoryTypeName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Remark"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
