/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LACaseInfoDB;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LACaseInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 新表结构
 * @CreateDate：2005-07-06
 */
public class LACaseInfoSchema implements Schema, Cloneable
{
    // @Field
    /** 立案编号 */
    private String CaseNo;
    /** 投诉单编号 */
    private String AppealNo;
    /** 立案日期 */
    private Date CaseDate;
    /** 立案人 */
    private String AgentPutCase;
    /** 案发日期 */
    private Date CaseHappenDate;
    /** 案件处理状态 */
    private String State;
    /** 结案人单位 */
    private String EndCaseUnit;
    /** 结案人 */
    private String AgentEndCase;
    /** 结案人电话 */
    private String AgentEndTel;
    /** 直接损失 */
    private double DirectLoss;
    /** 追回损失 */
    private double ReplevyLoss;
    /** 处理意见 */
    private String DealMind;
    /** 展业类型 */
    private String BranchType;
    /** 渠道 */
    private String BranchType2;
    /** 扣分值 */
    private String DeductCent;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最近一次修改日期 */
    private Date ModifyDate;
    /** 最近一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 20; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LACaseInfoSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "CaseNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException
    {
        LACaseInfoSchema cloned = (LACaseInfoSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getCaseNo()
    {
        return CaseNo;
    }

    public void setCaseNo(String aCaseNo)
    {
        CaseNo = aCaseNo;
    }

    public String getAppealNo()
    {
        return AppealNo;
    }

    public void setAppealNo(String aAppealNo)
    {
        AppealNo = aAppealNo;
    }

    public String getCaseDate()
    {
        if (CaseDate != null)
        {
            return fDate.getString(CaseDate);
        }
        else
        {
            return null;
        }
    }

    public void setCaseDate(Date aCaseDate)
    {
        CaseDate = aCaseDate;
    }

    public void setCaseDate(String aCaseDate)
    {
        if (aCaseDate != null && !aCaseDate.equals(""))
        {
            CaseDate = fDate.getDate(aCaseDate);
        }
        else
        {
            CaseDate = null;
        }
    }

    public String getAgentPutCase()
    {
        return AgentPutCase;
    }

    public void setAgentPutCase(String aAgentPutCase)
    {
        AgentPutCase = aAgentPutCase;
    }

    public String getCaseHappenDate()
    {
        if (CaseHappenDate != null)
        {
            return fDate.getString(CaseHappenDate);
        }
        else
        {
            return null;
        }
    }

    public void setCaseHappenDate(Date aCaseHappenDate)
    {
        CaseHappenDate = aCaseHappenDate;
    }

    public void setCaseHappenDate(String aCaseHappenDate)
    {
        if (aCaseHappenDate != null && !aCaseHappenDate.equals(""))
        {
            CaseHappenDate = fDate.getDate(aCaseHappenDate);
        }
        else
        {
            CaseHappenDate = null;
        }
    }

    public String getState()
    {
        return State;
    }

    public void setState(String aState)
    {
        State = aState;
    }

    public String getEndCaseUnit()
    {
        return EndCaseUnit;
    }

    public void setEndCaseUnit(String aEndCaseUnit)
    {
        EndCaseUnit = aEndCaseUnit;
    }

    public String getAgentEndCase()
    {
        return AgentEndCase;
    }

    public void setAgentEndCase(String aAgentEndCase)
    {
        AgentEndCase = aAgentEndCase;
    }

    public String getAgentEndTel()
    {
        return AgentEndTel;
    }

    public void setAgentEndTel(String aAgentEndTel)
    {
        AgentEndTel = aAgentEndTel;
    }

    public double getDirectLoss()
    {
        return DirectLoss;
    }

    public void setDirectLoss(double aDirectLoss)
    {
        DirectLoss = Arith.round(aDirectLoss, 2);
    }

    public void setDirectLoss(String aDirectLoss)
    {
        if (aDirectLoss != null && !aDirectLoss.equals(""))
        {
            Double tDouble = new Double(aDirectLoss);
            double d = tDouble.doubleValue();
            DirectLoss = Arith.round(d, 2);
        }
    }

    public double getReplevyLoss()
    {
        return ReplevyLoss;
    }

    public void setReplevyLoss(double aReplevyLoss)
    {
        ReplevyLoss = Arith.round(aReplevyLoss, 2);
    }

    public void setReplevyLoss(String aReplevyLoss)
    {
        if (aReplevyLoss != null && !aReplevyLoss.equals(""))
        {
            Double tDouble = new Double(aReplevyLoss);
            double d = tDouble.doubleValue();
            ReplevyLoss = Arith.round(d, 2);
        }
    }

    public String getDealMind()
    {
        return DealMind;
    }

    public void setDealMind(String aDealMind)
    {
        DealMind = aDealMind;
    }

    public String getBranchType()
    {
        return BranchType;
    }

    public void setBranchType(String aBranchType)
    {
        BranchType = aBranchType;
    }

    public String getBranchType2()
    {
        return BranchType2;
    }

    public void setBranchType2(String aBranchType2)
    {
        BranchType2 = aBranchType2;
    }

    public String getDeductCent()
    {
        return DeductCent;
    }

    public void setDeductCent(String aDeductCent)
    {
        DeductCent = aDeductCent;
    }

    public String getOperator()
    {
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LACaseInfoSchema 对象给 Schema 赋值
     * @param: aLACaseInfoSchema LACaseInfoSchema
     **/
    public void setSchema(LACaseInfoSchema aLACaseInfoSchema)
    {
        this.CaseNo = aLACaseInfoSchema.getCaseNo();
        this.AppealNo = aLACaseInfoSchema.getAppealNo();
        this.CaseDate = fDate.getDate(aLACaseInfoSchema.getCaseDate());
        this.AgentPutCase = aLACaseInfoSchema.getAgentPutCase();
        this.CaseHappenDate = fDate.getDate(aLACaseInfoSchema.getCaseHappenDate());
        this.State = aLACaseInfoSchema.getState();
        this.EndCaseUnit = aLACaseInfoSchema.getEndCaseUnit();
        this.AgentEndCase = aLACaseInfoSchema.getAgentEndCase();
        this.AgentEndTel = aLACaseInfoSchema.getAgentEndTel();
        this.DirectLoss = aLACaseInfoSchema.getDirectLoss();
        this.ReplevyLoss = aLACaseInfoSchema.getReplevyLoss();
        this.DealMind = aLACaseInfoSchema.getDealMind();
        this.BranchType = aLACaseInfoSchema.getBranchType();
        this.BranchType2 = aLACaseInfoSchema.getBranchType2();
        this.DeductCent = aLACaseInfoSchema.getDeductCent();
        this.Operator = aLACaseInfoSchema.getOperator();
        this.MakeDate = fDate.getDate(aLACaseInfoSchema.getMakeDate());
        this.MakeTime = aLACaseInfoSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLACaseInfoSchema.getModifyDate());
        this.ModifyTime = aLACaseInfoSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("CaseNo") == null)
            {
                this.CaseNo = null;
            }
            else
            {
                this.CaseNo = rs.getString("CaseNo").trim();
            }

            if (rs.getString("AppealNo") == null)
            {
                this.AppealNo = null;
            }
            else
            {
                this.AppealNo = rs.getString("AppealNo").trim();
            }

            this.CaseDate = rs.getDate("CaseDate");
            if (rs.getString("AgentPutCase") == null)
            {
                this.AgentPutCase = null;
            }
            else
            {
                this.AgentPutCase = rs.getString("AgentPutCase").trim();
            }

            this.CaseHappenDate = rs.getDate("CaseHappenDate");
            if (rs.getString("State") == null)
            {
                this.State = null;
            }
            else
            {
                this.State = rs.getString("State").trim();
            }

            if (rs.getString("EndCaseUnit") == null)
            {
                this.EndCaseUnit = null;
            }
            else
            {
                this.EndCaseUnit = rs.getString("EndCaseUnit").trim();
            }

            if (rs.getString("AgentEndCase") == null)
            {
                this.AgentEndCase = null;
            }
            else
            {
                this.AgentEndCase = rs.getString("AgentEndCase").trim();
            }

            if (rs.getString("AgentEndTel") == null)
            {
                this.AgentEndTel = null;
            }
            else
            {
                this.AgentEndTel = rs.getString("AgentEndTel").trim();
            }

            this.DirectLoss = rs.getDouble("DirectLoss");
            this.ReplevyLoss = rs.getDouble("ReplevyLoss");
            if (rs.getString("DealMind") == null)
            {
                this.DealMind = null;
            }
            else
            {
                this.DealMind = rs.getString("DealMind").trim();
            }

            if (rs.getString("BranchType") == null)
            {
                this.BranchType = null;
            }
            else
            {
                this.BranchType = rs.getString("BranchType").trim();
            }

            if (rs.getString("BranchType2") == null)
            {
                this.BranchType2 = null;
            }
            else
            {
                this.BranchType2 = rs.getString("BranchType2").trim();
            }

            if (rs.getString("DeductCent") == null)
            {
                this.DeductCent = null;
            }
            else
            {
                this.DeductCent = rs.getString("DeductCent").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            System.out.println("数据库中的LACaseInfo表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LACaseInfoSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LACaseInfoSchema getSchema()
    {
        LACaseInfoSchema aLACaseInfoSchema = new LACaseInfoSchema();
        aLACaseInfoSchema.setSchema(this);
        return aLACaseInfoSchema;
    }

    public LACaseInfoDB getDB()
    {
        LACaseInfoDB aDBOper = new LACaseInfoDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLACaseInfo描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(CaseNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppealNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(CaseDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentPutCase));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(CaseHappenDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(State));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EndCaseUnit));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentEndCase));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentEndTel));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DirectLoss));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(ReplevyLoss));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DealMind));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BranchType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BranchType2));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DeductCent));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLACaseInfo>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            CaseNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                    SysConst.PACKAGESPILTER);
            AppealNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            CaseDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 3, SysConst.PACKAGESPILTER));
            AgentPutCase = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                          SysConst.PACKAGESPILTER);
            CaseHappenDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 5, SysConst.PACKAGESPILTER));
            State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                   SysConst.PACKAGESPILTER);
            EndCaseUnit = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                         SysConst.PACKAGESPILTER);
            AgentEndCase = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                          SysConst.PACKAGESPILTER);
            AgentEndTel = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                         SysConst.PACKAGESPILTER);
            DirectLoss = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 10, SysConst.PACKAGESPILTER))).doubleValue();
            ReplevyLoss = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 11, SysConst.PACKAGESPILTER))).doubleValue();
            DealMind = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                      SysConst.PACKAGESPILTER);
            BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                        SysConst.PACKAGESPILTER);
            BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                         SysConst.PACKAGESPILTER);
            DeductCent = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                        SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 17, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 19, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LACaseInfoSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("CaseNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CaseNo));
        }
        if (FCode.equals("AppealNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppealNo));
        }
        if (FCode.equals("CaseDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getCaseDate()));
        }
        if (FCode.equals("AgentPutCase"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentPutCase));
        }
        if (FCode.equals("CaseHappenDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getCaseHappenDate()));
        }
        if (FCode.equals("State"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equals("EndCaseUnit"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EndCaseUnit));
        }
        if (FCode.equals("AgentEndCase"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentEndCase));
        }
        if (FCode.equals("AgentEndTel"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentEndTel));
        }
        if (FCode.equals("DirectLoss"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DirectLoss));
        }
        if (FCode.equals("ReplevyLoss"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReplevyLoss));
        }
        if (FCode.equals("DealMind"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DealMind));
        }
        if (FCode.equals("BranchType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
        }
        if (FCode.equals("BranchType2"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
        }
        if (FCode.equals("DeductCent"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DeductCent));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(CaseNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(AppealNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getCaseDate()));
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(AgentPutCase);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getCaseHappenDate()));
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(State);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(EndCaseUnit);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(AgentEndCase);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(AgentEndTel);
                break;
            case 9:
                strFieldValue = String.valueOf(DirectLoss);
                break;
            case 10:
                strFieldValue = String.valueOf(ReplevyLoss);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(DealMind);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(BranchType);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(BranchType2);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(DeductCent);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("CaseNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CaseNo = FValue.trim();
            }
            else
            {
                CaseNo = null;
            }
        }
        if (FCode.equals("AppealNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppealNo = FValue.trim();
            }
            else
            {
                AppealNo = null;
            }
        }
        if (FCode.equals("CaseDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CaseDate = fDate.getDate(FValue);
            }
            else
            {
                CaseDate = null;
            }
        }
        if (FCode.equals("AgentPutCase"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentPutCase = FValue.trim();
            }
            else
            {
                AgentPutCase = null;
            }
        }
        if (FCode.equals("CaseHappenDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CaseHappenDate = fDate.getDate(FValue);
            }
            else
            {
                CaseHappenDate = null;
            }
        }
        if (FCode.equals("State"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
            {
                State = null;
            }
        }
        if (FCode.equals("EndCaseUnit"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EndCaseUnit = FValue.trim();
            }
            else
            {
                EndCaseUnit = null;
            }
        }
        if (FCode.equals("AgentEndCase"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentEndCase = FValue.trim();
            }
            else
            {
                AgentEndCase = null;
            }
        }
        if (FCode.equals("AgentEndTel"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentEndTel = FValue.trim();
            }
            else
            {
                AgentEndTel = null;
            }
        }
        if (FCode.equals("DirectLoss"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                DirectLoss = d;
            }
        }
        if (FCode.equals("ReplevyLoss"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                ReplevyLoss = d;
            }
        }
        if (FCode.equals("DealMind"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DealMind = FValue.trim();
            }
            else
            {
                DealMind = null;
            }
        }
        if (FCode.equals("BranchType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchType = FValue.trim();
            }
            else
            {
                BranchType = null;
            }
        }
        if (FCode.equals("BranchType2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchType2 = FValue.trim();
            }
            else
            {
                BranchType2 = null;
            }
        }
        if (FCode.equals("DeductCent"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DeductCent = FValue.trim();
            }
            else
            {
                DeductCent = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LACaseInfoSchema other = (LACaseInfoSchema) otherObject;
        return
                CaseNo.equals(other.getCaseNo())
                && AppealNo.equals(other.getAppealNo())
                && fDate.getString(CaseDate).equals(other.getCaseDate())
                && AgentPutCase.equals(other.getAgentPutCase())
                &&
                fDate.getString(CaseHappenDate).equals(other.getCaseHappenDate())
                && State.equals(other.getState())
                && EndCaseUnit.equals(other.getEndCaseUnit())
                && AgentEndCase.equals(other.getAgentEndCase())
                && AgentEndTel.equals(other.getAgentEndTel())
                && DirectLoss == other.getDirectLoss()
                && ReplevyLoss == other.getReplevyLoss()
                && DealMind.equals(other.getDealMind())
                && BranchType.equals(other.getBranchType())
                && BranchType2.equals(other.getBranchType2())
                && DeductCent.equals(other.getDeductCent())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("CaseNo"))
        {
            return 0;
        }
        if (strFieldName.equals("AppealNo"))
        {
            return 1;
        }
        if (strFieldName.equals("CaseDate"))
        {
            return 2;
        }
        if (strFieldName.equals("AgentPutCase"))
        {
            return 3;
        }
        if (strFieldName.equals("CaseHappenDate"))
        {
            return 4;
        }
        if (strFieldName.equals("State"))
        {
            return 5;
        }
        if (strFieldName.equals("EndCaseUnit"))
        {
            return 6;
        }
        if (strFieldName.equals("AgentEndCase"))
        {
            return 7;
        }
        if (strFieldName.equals("AgentEndTel"))
        {
            return 8;
        }
        if (strFieldName.equals("DirectLoss"))
        {
            return 9;
        }
        if (strFieldName.equals("ReplevyLoss"))
        {
            return 10;
        }
        if (strFieldName.equals("DealMind"))
        {
            return 11;
        }
        if (strFieldName.equals("BranchType"))
        {
            return 12;
        }
        if (strFieldName.equals("BranchType2"))
        {
            return 13;
        }
        if (strFieldName.equals("DeductCent"))
        {
            return 14;
        }
        if (strFieldName.equals("Operator"))
        {
            return 15;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 16;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 17;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 18;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 19;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "CaseNo";
                break;
            case 1:
                strFieldName = "AppealNo";
                break;
            case 2:
                strFieldName = "CaseDate";
                break;
            case 3:
                strFieldName = "AgentPutCase";
                break;
            case 4:
                strFieldName = "CaseHappenDate";
                break;
            case 5:
                strFieldName = "State";
                break;
            case 6:
                strFieldName = "EndCaseUnit";
                break;
            case 7:
                strFieldName = "AgentEndCase";
                break;
            case 8:
                strFieldName = "AgentEndTel";
                break;
            case 9:
                strFieldName = "DirectLoss";
                break;
            case 10:
                strFieldName = "ReplevyLoss";
                break;
            case 11:
                strFieldName = "DealMind";
                break;
            case 12:
                strFieldName = "BranchType";
                break;
            case 13:
                strFieldName = "BranchType2";
                break;
            case 14:
                strFieldName = "DeductCent";
                break;
            case 15:
                strFieldName = "Operator";
                break;
            case 16:
                strFieldName = "MakeDate";
                break;
            case 17:
                strFieldName = "MakeTime";
                break;
            case 18:
                strFieldName = "ModifyDate";
                break;
            case 19:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("CaseNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppealNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CaseDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("AgentPutCase"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CaseHappenDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("State"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EndCaseUnit"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentEndCase"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentEndTel"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DirectLoss"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("ReplevyLoss"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("DealMind"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchType2"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DeductCent"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 10:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 17:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 18:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 19:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
