/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LAAssessAccessoryBDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

/*
 * <p>ClassName: LAAssessAccessoryBSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-03-30
 */
public class LAAssessAccessoryBSchema implements Schema
{
    // @Field
    /** 转储号码 */
    private String EdorNo;
    /** 转储类型 */
    private String EdorType;
    /** 转储时间 */
    private Date EdorDate;
    /** 指标计算编码 */
    private String IndexCalNo;
    /** 考核类型 */
    private String AssessType;
    /** 考核对象编码 */
    private String AgentCode;
    /** 展业类型 */
    private String BranchType;
    /** 展业机构外部编码 */
    private String BranchAttr;
    /** 代理人展业机构代码 */
    private String AgentGroup;
    /** 管理机构 */
    private String ManageCom;
    /** 建议人事变动标志 */
    private String ModifyFlag;
    /** 原代理人系列 */
    private String AgentSeries;
    /** 原代理人级别 */
    private String AgentGrade;
    /** 建议代理人系列 */
    private String CalAgentSeries;
    /** 建议代理人级别 */
    private String CalAgentGrade;
    /** 新代理人系列 */
    private String AgentSeries1;
    /** 新代理人级别 */
    private String AgentGrade1;
    /** 新代理人组别 */
    private String AgentGroupNew;
    /** 确认人代码 */
    private String Confirmer;
    /** 确认日期 */
    private Date ConfirmDate;
    /** 状态 */
    private String State;
    /** 是否是基本法考核 */
    private String StandAssessFlag;
    /** 是否是第一次考核 */
    private String FirstAssessFlag;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 渠道 */
    private String BranchType2;

    public static final int FIELDNUM = 29; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LAAssessAccessoryBSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[5];
        pk[0] = "EdorNo";
        pk[1] = "EdorType";
        pk[2] = "IndexCalNo";
        pk[3] = "AssessType";
        pk[4] = "AgentCode";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getEdorNo()
    {
        if (SysConst.CHANGECHARSET && EdorNo != null && !EdorNo.equals(""))
        {
            EdorNo = StrTool.unicodeToGBK(EdorNo);
        }
        return EdorNo;
    }

    public void setEdorNo(String aEdorNo)
    {
        EdorNo = aEdorNo;
    }

    public String getEdorType()
    {
        if (SysConst.CHANGECHARSET && EdorType != null && !EdorType.equals(""))
        {
            EdorType = StrTool.unicodeToGBK(EdorType);
        }
        return EdorType;
    }

    public void setEdorType(String aEdorType)
    {
        EdorType = aEdorType;
    }

    public String getEdorDate()
    {
        if (EdorDate != null)
        {
            return fDate.getString(EdorDate);
        }
        else
        {
            return null;
        }
    }

    public void setEdorDate(Date aEdorDate)
    {
        EdorDate = aEdorDate;
    }

    public void setEdorDate(String aEdorDate)
    {
        if (aEdorDate != null && !aEdorDate.equals(""))
        {
            EdorDate = fDate.getDate(aEdorDate);
        }
        else
        {
            EdorDate = null;
        }
    }

    public String getIndexCalNo()
    {
        if (SysConst.CHANGECHARSET && IndexCalNo != null &&
            !IndexCalNo.equals(""))
        {
            IndexCalNo = StrTool.unicodeToGBK(IndexCalNo);
        }
        return IndexCalNo;
    }

    public void setIndexCalNo(String aIndexCalNo)
    {
        IndexCalNo = aIndexCalNo;
    }

    public String getAssessType()
    {
        if (SysConst.CHANGECHARSET && AssessType != null &&
            !AssessType.equals(""))
        {
            AssessType = StrTool.unicodeToGBK(AssessType);
        }
        return AssessType;
    }

    public void setAssessType(String aAssessType)
    {
        AssessType = aAssessType;
    }

    public String getAgentCode()
    {
        if (SysConst.CHANGECHARSET && AgentCode != null && !AgentCode.equals(""))
        {
            AgentCode = StrTool.unicodeToGBK(AgentCode);
        }
        return AgentCode;
    }

    public void setAgentCode(String aAgentCode)
    {
        AgentCode = aAgentCode;
    }

    public String getBranchType()
    {
        if (SysConst.CHANGECHARSET && BranchType != null &&
            !BranchType.equals(""))
        {
            BranchType = StrTool.unicodeToGBK(BranchType);
        }
        return BranchType;
    }

    public void setBranchType(String aBranchType)
    {
        BranchType = aBranchType;
    }

    public String getBranchAttr()
    {
        if (SysConst.CHANGECHARSET && BranchAttr != null &&
            !BranchAttr.equals(""))
        {
            BranchAttr = StrTool.unicodeToGBK(BranchAttr);
        }
        return BranchAttr;
    }

    public void setBranchAttr(String aBranchAttr)
    {
        BranchAttr = aBranchAttr;
    }

    public String getAgentGroup()
    {
        if (SysConst.CHANGECHARSET && AgentGroup != null &&
            !AgentGroup.equals(""))
        {
            AgentGroup = StrTool.unicodeToGBK(AgentGroup);
        }
        return AgentGroup;
    }

    public void setAgentGroup(String aAgentGroup)
    {
        AgentGroup = aAgentGroup;
    }

    public String getManageCom()
    {
        if (SysConst.CHANGECHARSET && ManageCom != null && !ManageCom.equals(""))
        {
            ManageCom = StrTool.unicodeToGBK(ManageCom);
        }
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public String getModifyFlag()
    {
        if (SysConst.CHANGECHARSET && ModifyFlag != null &&
            !ModifyFlag.equals(""))
        {
            ModifyFlag = StrTool.unicodeToGBK(ModifyFlag);
        }
        return ModifyFlag;
    }

    public void setModifyFlag(String aModifyFlag)
    {
        ModifyFlag = aModifyFlag;
    }

    public String getAgentSeries()
    {
        if (SysConst.CHANGECHARSET && AgentSeries != null &&
            !AgentSeries.equals(""))
        {
            AgentSeries = StrTool.unicodeToGBK(AgentSeries);
        }
        return AgentSeries;
    }

    public void setAgentSeries(String aAgentSeries)
    {
        AgentSeries = aAgentSeries;
    }

    public String getAgentGrade()
    {
        if (SysConst.CHANGECHARSET && AgentGrade != null &&
            !AgentGrade.equals(""))
        {
            AgentGrade = StrTool.unicodeToGBK(AgentGrade);
        }
        return AgentGrade;
    }

    public void setAgentGrade(String aAgentGrade)
    {
        AgentGrade = aAgentGrade;
    }

    public String getCalAgentSeries()
    {
        if (SysConst.CHANGECHARSET && CalAgentSeries != null &&
            !CalAgentSeries.equals(""))
        {
            CalAgentSeries = StrTool.unicodeToGBK(CalAgentSeries);
        }
        return CalAgentSeries;
    }

    public void setCalAgentSeries(String aCalAgentSeries)
    {
        CalAgentSeries = aCalAgentSeries;
    }

    public String getCalAgentGrade()
    {
        if (SysConst.CHANGECHARSET && CalAgentGrade != null &&
            !CalAgentGrade.equals(""))
        {
            CalAgentGrade = StrTool.unicodeToGBK(CalAgentGrade);
        }
        return CalAgentGrade;
    }

    public void setCalAgentGrade(String aCalAgentGrade)
    {
        CalAgentGrade = aCalAgentGrade;
    }

    public String getAgentSeries1()
    {
        if (SysConst.CHANGECHARSET && AgentSeries1 != null &&
            !AgentSeries1.equals(""))
        {
            AgentSeries1 = StrTool.unicodeToGBK(AgentSeries1);
        }
        return AgentSeries1;
    }

    public void setAgentSeries1(String aAgentSeries1)
    {
        AgentSeries1 = aAgentSeries1;
    }

    public String getAgentGrade1()
    {
        if (SysConst.CHANGECHARSET && AgentGrade1 != null &&
            !AgentGrade1.equals(""))
        {
            AgentGrade1 = StrTool.unicodeToGBK(AgentGrade1);
        }
        return AgentGrade1;
    }

    public void setAgentGrade1(String aAgentGrade1)
    {
        AgentGrade1 = aAgentGrade1;
    }

    public String getAgentGroupNew()
    {
        if (SysConst.CHANGECHARSET && AgentGroupNew != null &&
            !AgentGroupNew.equals(""))
        {
            AgentGroupNew = StrTool.unicodeToGBK(AgentGroupNew);
        }
        return AgentGroupNew;
    }

    public void setAgentGroupNew(String aAgentGroupNew)
    {
        AgentGroupNew = aAgentGroupNew;
    }

    public String getConfirmer()
    {
        if (SysConst.CHANGECHARSET && Confirmer != null && !Confirmer.equals(""))
        {
            Confirmer = StrTool.unicodeToGBK(Confirmer);
        }
        return Confirmer;
    }

    public void setConfirmer(String aConfirmer)
    {
        Confirmer = aConfirmer;
    }

    public String getConfirmDate()
    {
        if (ConfirmDate != null)
        {
            return fDate.getString(ConfirmDate);
        }
        else
        {
            return null;
        }
    }

    public void setConfirmDate(Date aConfirmDate)
    {
        ConfirmDate = aConfirmDate;
    }

    public void setConfirmDate(String aConfirmDate)
    {
        if (aConfirmDate != null && !aConfirmDate.equals(""))
        {
            ConfirmDate = fDate.getDate(aConfirmDate);
        }
        else
        {
            ConfirmDate = null;
        }
    }

    public String getState()
    {
        if (SysConst.CHANGECHARSET && State != null && !State.equals(""))
        {
            State = StrTool.unicodeToGBK(State);
        }
        return State;
    }

    public void setState(String aState)
    {
        State = aState;
    }

    public String getStandAssessFlag()
    {
        if (SysConst.CHANGECHARSET && StandAssessFlag != null &&
            !StandAssessFlag.equals(""))
        {
            StandAssessFlag = StrTool.unicodeToGBK(StandAssessFlag);
        }
        return StandAssessFlag;
    }

    public void setStandAssessFlag(String aStandAssessFlag)
    {
        StandAssessFlag = aStandAssessFlag;
    }

    public String getFirstAssessFlag()
    {
        if (SysConst.CHANGECHARSET && FirstAssessFlag != null &&
            !FirstAssessFlag.equals(""))
        {
            FirstAssessFlag = StrTool.unicodeToGBK(FirstAssessFlag);
        }
        return FirstAssessFlag;
    }

    public void setFirstAssessFlag(String aFirstAssessFlag)
    {
        FirstAssessFlag = aFirstAssessFlag;
    }

    public String getOperator()
    {
        if (SysConst.CHANGECHARSET && Operator != null && !Operator.equals(""))
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (SysConst.CHANGECHARSET && MakeTime != null && !MakeTime.equals(""))
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (SysConst.CHANGECHARSET && ModifyTime != null &&
            !ModifyTime.equals(""))
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    public String getBranchType2()
    {
        if (SysConst.CHANGECHARSET && BranchType2 != null &&
            !BranchType2.equals(""))
        {
            BranchType2 = StrTool.unicodeToGBK(BranchType2);
        }
        return BranchType2;
    }

    public void setBranchType2(String aBranchType2)
    {
        BranchType2 = aBranchType2;
    }

    /**
     * 使用另外一个 LAAssessAccessoryBSchema 对象给 Schema 赋值
     * @param: aLAAssessAccessoryBSchema LAAssessAccessoryBSchema
     **/
    public void setSchema(LAAssessAccessoryBSchema aLAAssessAccessoryBSchema)
    {
        this.EdorNo = aLAAssessAccessoryBSchema.getEdorNo();
        this.EdorType = aLAAssessAccessoryBSchema.getEdorType();
        this.EdorDate = fDate.getDate(aLAAssessAccessoryBSchema.getEdorDate());
        this.IndexCalNo = aLAAssessAccessoryBSchema.getIndexCalNo();
        this.AssessType = aLAAssessAccessoryBSchema.getAssessType();
        this.AgentCode = aLAAssessAccessoryBSchema.getAgentCode();
        this.BranchType = aLAAssessAccessoryBSchema.getBranchType();
        this.BranchAttr = aLAAssessAccessoryBSchema.getBranchAttr();
        this.AgentGroup = aLAAssessAccessoryBSchema.getAgentGroup();
        this.ManageCom = aLAAssessAccessoryBSchema.getManageCom();
        this.ModifyFlag = aLAAssessAccessoryBSchema.getModifyFlag();
        this.AgentSeries = aLAAssessAccessoryBSchema.getAgentSeries();
        this.AgentGrade = aLAAssessAccessoryBSchema.getAgentGrade();
        this.CalAgentSeries = aLAAssessAccessoryBSchema.getCalAgentSeries();
        this.CalAgentGrade = aLAAssessAccessoryBSchema.getCalAgentGrade();
        this.AgentSeries1 = aLAAssessAccessoryBSchema.getAgentSeries1();
        this.AgentGrade1 = aLAAssessAccessoryBSchema.getAgentGrade1();
        this.AgentGroupNew = aLAAssessAccessoryBSchema.getAgentGroupNew();
        this.Confirmer = aLAAssessAccessoryBSchema.getConfirmer();
        this.ConfirmDate = fDate.getDate(aLAAssessAccessoryBSchema.
                                         getConfirmDate());
        this.State = aLAAssessAccessoryBSchema.getState();
        this.StandAssessFlag = aLAAssessAccessoryBSchema.getStandAssessFlag();
        this.FirstAssessFlag = aLAAssessAccessoryBSchema.getFirstAssessFlag();
        this.Operator = aLAAssessAccessoryBSchema.getOperator();
        this.MakeDate = fDate.getDate(aLAAssessAccessoryBSchema.getMakeDate());
        this.MakeTime = aLAAssessAccessoryBSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLAAssessAccessoryBSchema.getModifyDate());
        this.ModifyTime = aLAAssessAccessoryBSchema.getModifyTime();
        this.BranchType2 = aLAAssessAccessoryBSchema.getBranchType2();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("EdorNo") == null)
            {
                this.EdorNo = null;
            }
            else
            {
                this.EdorNo = rs.getString("EdorNo").trim();
            }

            if (rs.getString("EdorType") == null)
            {
                this.EdorType = null;
            }
            else
            {
                this.EdorType = rs.getString("EdorType").trim();
            }

            this.EdorDate = rs.getDate("EdorDate");
            if (rs.getString("IndexCalNo") == null)
            {
                this.IndexCalNo = null;
            }
            else
            {
                this.IndexCalNo = rs.getString("IndexCalNo").trim();
            }

            if (rs.getString("AssessType") == null)
            {
                this.AssessType = null;
            }
            else
            {
                this.AssessType = rs.getString("AssessType").trim();
            }

            if (rs.getString("AgentCode") == null)
            {
                this.AgentCode = null;
            }
            else
            {
                this.AgentCode = rs.getString("AgentCode").trim();
            }

            if (rs.getString("BranchType") == null)
            {
                this.BranchType = null;
            }
            else
            {
                this.BranchType = rs.getString("BranchType").trim();
            }

            if (rs.getString("BranchAttr") == null)
            {
                this.BranchAttr = null;
            }
            else
            {
                this.BranchAttr = rs.getString("BranchAttr").trim();
            }

            if (rs.getString("AgentGroup") == null)
            {
                this.AgentGroup = null;
            }
            else
            {
                this.AgentGroup = rs.getString("AgentGroup").trim();
            }

            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("ModifyFlag") == null)
            {
                this.ModifyFlag = null;
            }
            else
            {
                this.ModifyFlag = rs.getString("ModifyFlag").trim();
            }

            if (rs.getString("AgentSeries") == null)
            {
                this.AgentSeries = null;
            }
            else
            {
                this.AgentSeries = rs.getString("AgentSeries").trim();
            }

            if (rs.getString("AgentGrade") == null)
            {
                this.AgentGrade = null;
            }
            else
            {
                this.AgentGrade = rs.getString("AgentGrade").trim();
            }

            if (rs.getString("CalAgentSeries") == null)
            {
                this.CalAgentSeries = null;
            }
            else
            {
                this.CalAgentSeries = rs.getString("CalAgentSeries").trim();
            }

            if (rs.getString("CalAgentGrade") == null)
            {
                this.CalAgentGrade = null;
            }
            else
            {
                this.CalAgentGrade = rs.getString("CalAgentGrade").trim();
            }

            if (rs.getString("AgentSeries1") == null)
            {
                this.AgentSeries1 = null;
            }
            else
            {
                this.AgentSeries1 = rs.getString("AgentSeries1").trim();
            }

            if (rs.getString("AgentGrade1") == null)
            {
                this.AgentGrade1 = null;
            }
            else
            {
                this.AgentGrade1 = rs.getString("AgentGrade1").trim();
            }

            if (rs.getString("AgentGroupNew") == null)
            {
                this.AgentGroupNew = null;
            }
            else
            {
                this.AgentGroupNew = rs.getString("AgentGroupNew").trim();
            }

            if (rs.getString("Confirmer") == null)
            {
                this.Confirmer = null;
            }
            else
            {
                this.Confirmer = rs.getString("Confirmer").trim();
            }

            this.ConfirmDate = rs.getDate("ConfirmDate");
            if (rs.getString("State") == null)
            {
                this.State = null;
            }
            else
            {
                this.State = rs.getString("State").trim();
            }

            if (rs.getString("StandAssessFlag") == null)
            {
                this.StandAssessFlag = null;
            }
            else
            {
                this.StandAssessFlag = rs.getString("StandAssessFlag").trim();
            }

            if (rs.getString("FirstAssessFlag") == null)
            {
                this.FirstAssessFlag = null;
            }
            else
            {
                this.FirstAssessFlag = rs.getString("FirstAssessFlag").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("BranchType2") == null)
            {
                this.BranchType2 = null;
            }
            else
            {
                this.BranchType2 = rs.getString("BranchType2").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAssessAccessoryBSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LAAssessAccessoryBSchema getSchema()
    {
        LAAssessAccessoryBSchema aLAAssessAccessoryBSchema = new
                LAAssessAccessoryBSchema();
        aLAAssessAccessoryBSchema.setSchema(this);
        return aLAAssessAccessoryBSchema;
    }

    public LAAssessAccessoryBDB getDB()
    {
        LAAssessAccessoryBDB aDBOper = new LAAssessAccessoryBDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAAssessAccessoryB描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(EdorNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(EdorType)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                EdorDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(IndexCalNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AssessType)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AgentCode)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BranchType)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BranchAttr)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AgentGroup)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ManageCom)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ModifyFlag)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AgentSeries)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AgentGrade)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(CalAgentSeries)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(CalAgentGrade)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AgentSeries1)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AgentGrade1)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AgentGroupNew)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Confirmer)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                ConfirmDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(State)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(StandAssessFlag)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(FirstAssessFlag)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Operator)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                MakeDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                ModifyDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BranchType2)));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAAssessAccessoryB>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            EdorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                    SysConst.PACKAGESPILTER);
            EdorType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            EdorDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 3, SysConst.PACKAGESPILTER));
            IndexCalNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                        SysConst.PACKAGESPILTER);
            AssessType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                        SysConst.PACKAGESPILTER);
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                       SysConst.PACKAGESPILTER);
            BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                        SysConst.PACKAGESPILTER);
            BranchAttr = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                        SysConst.PACKAGESPILTER);
            AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                        SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                       SysConst.PACKAGESPILTER);
            ModifyFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                        SysConst.PACKAGESPILTER);
            AgentSeries = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                         SysConst.PACKAGESPILTER);
            AgentGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                        SysConst.PACKAGESPILTER);
            CalAgentSeries = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                            14, SysConst.PACKAGESPILTER);
            CalAgentGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                           SysConst.PACKAGESPILTER);
            AgentSeries1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                          SysConst.PACKAGESPILTER);
            AgentGrade1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                         SysConst.PACKAGESPILTER);
            AgentGroupNew = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                           SysConst.PACKAGESPILTER);
            Confirmer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                       SysConst.PACKAGESPILTER);
            ConfirmDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 20, SysConst.PACKAGESPILTER));
            State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,
                                   SysConst.PACKAGESPILTER);
            StandAssessFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             22, SysConst.PACKAGESPILTER);
            FirstAssessFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             23, SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 25, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 27, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28,
                                        SysConst.PACKAGESPILTER);
            BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29,
                                         SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAssessAccessoryBSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("EdorNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorNo));
        }
        if (FCode.equals("EdorType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorType));
        }
        if (FCode.equals("EdorDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getEdorDate()));
        }
        if (FCode.equals("IndexCalNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IndexCalNo));
        }
        if (FCode.equals("AssessType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AssessType));
        }
        if (FCode.equals("AgentCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equals("BranchType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
        }
        if (FCode.equals("BranchAttr"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchAttr));
        }
        if (FCode.equals("AgentGroup"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
        }
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("ModifyFlag"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyFlag));
        }
        if (FCode.equals("AgentSeries"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentSeries));
        }
        if (FCode.equals("AgentGrade"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGrade));
        }
        if (FCode.equals("CalAgentSeries"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalAgentSeries));
        }
        if (FCode.equals("CalAgentGrade"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalAgentGrade));
        }
        if (FCode.equals("AgentSeries1"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentSeries1));
        }
        if (FCode.equals("AgentGrade1"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGrade1));
        }
        if (FCode.equals("AgentGroupNew"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroupNew));
        }
        if (FCode.equals("Confirmer"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Confirmer));
        }
        if (FCode.equals("ConfirmDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getConfirmDate()));
        }
        if (FCode.equals("State"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equals("StandAssessFlag"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandAssessFlag));
        }
        if (FCode.equals("FirstAssessFlag"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FirstAssessFlag));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("BranchType2"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(EdorNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(EdorType);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getEdorDate()));
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(IndexCalNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(AssessType);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(AgentCode);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(BranchType);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(BranchAttr);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(AgentGroup);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(ModifyFlag);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(AgentSeries);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(AgentGrade);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(CalAgentSeries);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(CalAgentGrade);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(AgentSeries1);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(AgentGrade1);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(AgentGroupNew);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(Confirmer);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getConfirmDate()));
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(State);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(StandAssessFlag);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(FirstAssessFlag);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(BranchType2);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("EdorNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorNo = FValue.trim();
            }
            else
            {
                EdorNo = null;
            }
        }
        if (FCode.equals("EdorType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorType = FValue.trim();
            }
            else
            {
                EdorType = null;
            }
        }
        if (FCode.equals("EdorDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorDate = fDate.getDate(FValue);
            }
            else
            {
                EdorDate = null;
            }
        }
        if (FCode.equals("IndexCalNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                IndexCalNo = FValue.trim();
            }
            else
            {
                IndexCalNo = null;
            }
        }
        if (FCode.equals("AssessType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AssessType = FValue.trim();
            }
            else
            {
                AssessType = null;
            }
        }
        if (FCode.equals("AgentCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
            {
                AgentCode = null;
            }
        }
        if (FCode.equals("BranchType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchType = FValue.trim();
            }
            else
            {
                BranchType = null;
            }
        }
        if (FCode.equals("BranchAttr"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchAttr = FValue.trim();
            }
            else
            {
                BranchAttr = null;
            }
        }
        if (FCode.equals("AgentGroup"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
            {
                AgentGroup = null;
            }
        }
        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("ModifyFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyFlag = FValue.trim();
            }
            else
            {
                ModifyFlag = null;
            }
        }
        if (FCode.equals("AgentSeries"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentSeries = FValue.trim();
            }
            else
            {
                AgentSeries = null;
            }
        }
        if (FCode.equals("AgentGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentGrade = FValue.trim();
            }
            else
            {
                AgentGrade = null;
            }
        }
        if (FCode.equals("CalAgentSeries"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CalAgentSeries = FValue.trim();
            }
            else
            {
                CalAgentSeries = null;
            }
        }
        if (FCode.equals("CalAgentGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CalAgentGrade = FValue.trim();
            }
            else
            {
                CalAgentGrade = null;
            }
        }
        if (FCode.equals("AgentSeries1"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentSeries1 = FValue.trim();
            }
            else
            {
                AgentSeries1 = null;
            }
        }
        if (FCode.equals("AgentGrade1"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentGrade1 = FValue.trim();
            }
            else
            {
                AgentGrade1 = null;
            }
        }
        if (FCode.equals("AgentGroupNew"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentGroupNew = FValue.trim();
            }
            else
            {
                AgentGroupNew = null;
            }
        }
        if (FCode.equals("Confirmer"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Confirmer = FValue.trim();
            }
            else
            {
                Confirmer = null;
            }
        }
        if (FCode.equals("ConfirmDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ConfirmDate = fDate.getDate(FValue);
            }
            else
            {
                ConfirmDate = null;
            }
        }
        if (FCode.equals("State"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
            {
                State = null;
            }
        }
        if (FCode.equals("StandAssessFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                StandAssessFlag = FValue.trim();
            }
            else
            {
                StandAssessFlag = null;
            }
        }
        if (FCode.equals("FirstAssessFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                FirstAssessFlag = FValue.trim();
            }
            else
            {
                FirstAssessFlag = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        if (FCode.equals("BranchType2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchType2 = FValue.trim();
            }
            else
            {
                BranchType2 = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LAAssessAccessoryBSchema other = (LAAssessAccessoryBSchema) otherObject;
        return
                EdorNo.equals(other.getEdorNo())
                && EdorType.equals(other.getEdorType())
                && fDate.getString(EdorDate).equals(other.getEdorDate())
                && IndexCalNo.equals(other.getIndexCalNo())
                && AssessType.equals(other.getAssessType())
                && AgentCode.equals(other.getAgentCode())
                && BranchType.equals(other.getBranchType())
                && BranchAttr.equals(other.getBranchAttr())
                && AgentGroup.equals(other.getAgentGroup())
                && ManageCom.equals(other.getManageCom())
                && ModifyFlag.equals(other.getModifyFlag())
                && AgentSeries.equals(other.getAgentSeries())
                && AgentGrade.equals(other.getAgentGrade())
                && CalAgentSeries.equals(other.getCalAgentSeries())
                && CalAgentGrade.equals(other.getCalAgentGrade())
                && AgentSeries1.equals(other.getAgentSeries1())
                && AgentGrade1.equals(other.getAgentGrade1())
                && AgentGroupNew.equals(other.getAgentGroupNew())
                && Confirmer.equals(other.getConfirmer())
                && fDate.getString(ConfirmDate).equals(other.getConfirmDate())
                && State.equals(other.getState())
                && StandAssessFlag.equals(other.getStandAssessFlag())
                && FirstAssessFlag.equals(other.getFirstAssessFlag())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && BranchType2.equals(other.getBranchType2());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("EdorNo"))
        {
            return 0;
        }
        if (strFieldName.equals("EdorType"))
        {
            return 1;
        }
        if (strFieldName.equals("EdorDate"))
        {
            return 2;
        }
        if (strFieldName.equals("IndexCalNo"))
        {
            return 3;
        }
        if (strFieldName.equals("AssessType"))
        {
            return 4;
        }
        if (strFieldName.equals("AgentCode"))
        {
            return 5;
        }
        if (strFieldName.equals("BranchType"))
        {
            return 6;
        }
        if (strFieldName.equals("BranchAttr"))
        {
            return 7;
        }
        if (strFieldName.equals("AgentGroup"))
        {
            return 8;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return 9;
        }
        if (strFieldName.equals("ModifyFlag"))
        {
            return 10;
        }
        if (strFieldName.equals("AgentSeries"))
        {
            return 11;
        }
        if (strFieldName.equals("AgentGrade"))
        {
            return 12;
        }
        if (strFieldName.equals("CalAgentSeries"))
        {
            return 13;
        }
        if (strFieldName.equals("CalAgentGrade"))
        {
            return 14;
        }
        if (strFieldName.equals("AgentSeries1"))
        {
            return 15;
        }
        if (strFieldName.equals("AgentGrade1"))
        {
            return 16;
        }
        if (strFieldName.equals("AgentGroupNew"))
        {
            return 17;
        }
        if (strFieldName.equals("Confirmer"))
        {
            return 18;
        }
        if (strFieldName.equals("ConfirmDate"))
        {
            return 19;
        }
        if (strFieldName.equals("State"))
        {
            return 20;
        }
        if (strFieldName.equals("StandAssessFlag"))
        {
            return 21;
        }
        if (strFieldName.equals("FirstAssessFlag"))
        {
            return 22;
        }
        if (strFieldName.equals("Operator"))
        {
            return 23;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 24;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 25;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 26;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 27;
        }
        if (strFieldName.equals("BranchType2"))
        {
            return 28;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "EdorNo";
                break;
            case 1:
                strFieldName = "EdorType";
                break;
            case 2:
                strFieldName = "EdorDate";
                break;
            case 3:
                strFieldName = "IndexCalNo";
                break;
            case 4:
                strFieldName = "AssessType";
                break;
            case 5:
                strFieldName = "AgentCode";
                break;
            case 6:
                strFieldName = "BranchType";
                break;
            case 7:
                strFieldName = "BranchAttr";
                break;
            case 8:
                strFieldName = "AgentGroup";
                break;
            case 9:
                strFieldName = "ManageCom";
                break;
            case 10:
                strFieldName = "ModifyFlag";
                break;
            case 11:
                strFieldName = "AgentSeries";
                break;
            case 12:
                strFieldName = "AgentGrade";
                break;
            case 13:
                strFieldName = "CalAgentSeries";
                break;
            case 14:
                strFieldName = "CalAgentGrade";
                break;
            case 15:
                strFieldName = "AgentSeries1";
                break;
            case 16:
                strFieldName = "AgentGrade1";
                break;
            case 17:
                strFieldName = "AgentGroupNew";
                break;
            case 18:
                strFieldName = "Confirmer";
                break;
            case 19:
                strFieldName = "ConfirmDate";
                break;
            case 20:
                strFieldName = "State";
                break;
            case 21:
                strFieldName = "StandAssessFlag";
                break;
            case 22:
                strFieldName = "FirstAssessFlag";
                break;
            case 23:
                strFieldName = "Operator";
                break;
            case 24:
                strFieldName = "MakeDate";
                break;
            case 25:
                strFieldName = "MakeTime";
                break;
            case 26:
                strFieldName = "ModifyDate";
                break;
            case 27:
                strFieldName = "ModifyTime";
                break;
            case 28:
                strFieldName = "BranchType2";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("EdorNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EdorType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EdorDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("IndexCalNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AssessType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchAttr"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentGroup"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentSeries"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentGrade"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CalAgentSeries"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CalAgentGrade"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentSeries1"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentGrade1"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentGroupNew"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Confirmer"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ConfirmDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("State"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StandAssessFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FirstAssessFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchType2"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 17:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 18:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 19:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 20:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 21:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 22:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 23:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 24:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 25:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 26:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 27:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 28:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
