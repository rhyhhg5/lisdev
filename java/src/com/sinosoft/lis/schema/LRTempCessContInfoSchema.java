/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LRTempCessContInfoDB;

/*
 * <p>ClassName: LRTempCessContInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2010-12-17
 */
public class LRTempCessContInfoSchema implements Schema, Cloneable
{
	// @Field
	/** 临分协议编号 */
	private String TempContCode;
	/** 险种编码 */
	private String RiskCode;
	/** 给付责任编码 */
	private String GetDutyCode;
	/** 分保方式 */
	private String CessionMode;
	/** 最低再保保额 */
	private double CalMode;
	/** 最高再保保额 */
	private double RetainAmnt;
	/** 分保比例 */
	private double CessionRate;
	/** 接受份额 */
	private double AccQuot;
	/** 再保险手续费比例 */
	private double ReinProcFeeRate;
	/** 特殊佣金比例 */
	private double SpeComRate;
	/** 选择折扣比例 */
	private double ChoiRebaRate;
	/** 盈余佣金比例 */
	private double ProfitCom;
	/** 管理费比例 */
	private double ReinManFeeRate;
	/** 理赔通知限额 */
	private double ClaimNoticeLmt;
	/** 理赔参与限额 */
	private double ClaimAttLmt;
	/** 备用字段1 */
	private double Reserve1;
	/** 备用字段2 */
	private double Reserve2;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 修改日期 */
	private Date ModifyDate;
	/** 修改时间 */
	private String ModifyTime;
	/** 操作人 */
	private String Operator;
	/** 管理机构 */
	private String ManageCom;
	/** 保障计划号 */
	private String ContPlanCode;

	public static final int FIELDNUM = 24;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LRTempCessContInfoSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[4];
		pk[0] = "TempContCode";
		pk[1] = "RiskCode";
		pk[2] = "GetDutyCode";
		pk[3] = "ContPlanCode";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LRTempCessContInfoSchema cloned = (LRTempCessContInfoSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getTempContCode()
	{
		return TempContCode;
	}
	public void setTempContCode(String aTempContCode)
	{
		TempContCode = aTempContCode;
	}
	public String getRiskCode()
	{
		return RiskCode;
	}
	public void setRiskCode(String aRiskCode)
	{
		RiskCode = aRiskCode;
	}
	public String getGetDutyCode()
	{
		return GetDutyCode;
	}
	public void setGetDutyCode(String aGetDutyCode)
	{
		GetDutyCode = aGetDutyCode;
	}
	public String getCessionMode()
	{
		return CessionMode;
	}
	public void setCessionMode(String aCessionMode)
	{
		CessionMode = aCessionMode;
	}
	public double getCalMode()
	{
		return CalMode;
	}
	public void setCalMode(double aCalMode)
	{
		CalMode = Arith.round(aCalMode,2);
	}
	public void setCalMode(String aCalMode)
	{
		if (aCalMode != null && !aCalMode.equals(""))
		{
			Double tDouble = new Double(aCalMode);
			double d = tDouble.doubleValue();
                CalMode = Arith.round(d,2);
		}
	}

	public double getRetainAmnt()
	{
		return RetainAmnt;
	}
	public void setRetainAmnt(double aRetainAmnt)
	{
		RetainAmnt = Arith.round(aRetainAmnt,2);
	}
	public void setRetainAmnt(String aRetainAmnt)
	{
		if (aRetainAmnt != null && !aRetainAmnt.equals(""))
		{
			Double tDouble = new Double(aRetainAmnt);
			double d = tDouble.doubleValue();
                RetainAmnt = Arith.round(d,2);
		}
	}

	public double getCessionRate()
	{
		return CessionRate;
	}
	public void setCessionRate(double aCessionRate)
	{
		CessionRate = Arith.round(aCessionRate,3);
	}
	public void setCessionRate(String aCessionRate)
	{
		if (aCessionRate != null && !aCessionRate.equals(""))
		{
			Double tDouble = new Double(aCessionRate);
			double d = tDouble.doubleValue();
                CessionRate = Arith.round(d,3);
		}
	}

	public double getAccQuot()
	{
		return AccQuot;
	}
	public void setAccQuot(double aAccQuot)
	{
		AccQuot = Arith.round(aAccQuot,3);
	}
	public void setAccQuot(String aAccQuot)
	{
		if (aAccQuot != null && !aAccQuot.equals(""))
		{
			Double tDouble = new Double(aAccQuot);
			double d = tDouble.doubleValue();
                AccQuot = Arith.round(d,3);
		}
	}

	public double getReinProcFeeRate()
	{
		return ReinProcFeeRate;
	}
	public void setReinProcFeeRate(double aReinProcFeeRate)
	{
		ReinProcFeeRate = Arith.round(aReinProcFeeRate,2);
	}
	public void setReinProcFeeRate(String aReinProcFeeRate)
	{
		if (aReinProcFeeRate != null && !aReinProcFeeRate.equals(""))
		{
			Double tDouble = new Double(aReinProcFeeRate);
			double d = tDouble.doubleValue();
                ReinProcFeeRate = Arith.round(d,2);
		}
	}

	public double getSpeComRate()
	{
		return SpeComRate;
	}
	public void setSpeComRate(double aSpeComRate)
	{
		SpeComRate = Arith.round(aSpeComRate,2);
	}
	public void setSpeComRate(String aSpeComRate)
	{
		if (aSpeComRate != null && !aSpeComRate.equals(""))
		{
			Double tDouble = new Double(aSpeComRate);
			double d = tDouble.doubleValue();
                SpeComRate = Arith.round(d,2);
		}
	}

	public double getChoiRebaRate()
	{
		return ChoiRebaRate;
	}
	public void setChoiRebaRate(double aChoiRebaRate)
	{
		ChoiRebaRate = Arith.round(aChoiRebaRate,2);
	}
	public void setChoiRebaRate(String aChoiRebaRate)
	{
		if (aChoiRebaRate != null && !aChoiRebaRate.equals(""))
		{
			Double tDouble = new Double(aChoiRebaRate);
			double d = tDouble.doubleValue();
                ChoiRebaRate = Arith.round(d,2);
		}
	}

	public double getProfitCom()
	{
		return ProfitCom;
	}
	public void setProfitCom(double aProfitCom)
	{
		ProfitCom = Arith.round(aProfitCom,2);
	}
	public void setProfitCom(String aProfitCom)
	{
		if (aProfitCom != null && !aProfitCom.equals(""))
		{
			Double tDouble = new Double(aProfitCom);
			double d = tDouble.doubleValue();
                ProfitCom = Arith.round(d,2);
		}
	}

	public double getReinManFeeRate()
	{
		return ReinManFeeRate;
	}
	public void setReinManFeeRate(double aReinManFeeRate)
	{
		ReinManFeeRate = Arith.round(aReinManFeeRate,2);
	}
	public void setReinManFeeRate(String aReinManFeeRate)
	{
		if (aReinManFeeRate != null && !aReinManFeeRate.equals(""))
		{
			Double tDouble = new Double(aReinManFeeRate);
			double d = tDouble.doubleValue();
                ReinManFeeRate = Arith.round(d,2);
		}
	}

	public double getClaimNoticeLmt()
	{
		return ClaimNoticeLmt;
	}
	public void setClaimNoticeLmt(double aClaimNoticeLmt)
	{
		ClaimNoticeLmt = Arith.round(aClaimNoticeLmt,2);
	}
	public void setClaimNoticeLmt(String aClaimNoticeLmt)
	{
		if (aClaimNoticeLmt != null && !aClaimNoticeLmt.equals(""))
		{
			Double tDouble = new Double(aClaimNoticeLmt);
			double d = tDouble.doubleValue();
                ClaimNoticeLmt = Arith.round(d,2);
		}
	}

	public double getClaimAttLmt()
	{
		return ClaimAttLmt;
	}
	public void setClaimAttLmt(double aClaimAttLmt)
	{
		ClaimAttLmt = Arith.round(aClaimAttLmt,2);
	}
	public void setClaimAttLmt(String aClaimAttLmt)
	{
		if (aClaimAttLmt != null && !aClaimAttLmt.equals(""))
		{
			Double tDouble = new Double(aClaimAttLmt);
			double d = tDouble.doubleValue();
                ClaimAttLmt = Arith.round(d,2);
		}
	}

	public double getReserve1()
	{
		return Reserve1;
	}
	public void setReserve1(double aReserve1)
	{
		Reserve1 = Arith.round(aReserve1,3);
	}
	public void setReserve1(String aReserve1)
	{
		if (aReserve1 != null && !aReserve1.equals(""))
		{
			Double tDouble = new Double(aReserve1);
			double d = tDouble.doubleValue();
                Reserve1 = Arith.round(d,3);
		}
	}

	public double getReserve2()
	{
		return Reserve2;
	}
	public void setReserve2(double aReserve2)
	{
		Reserve2 = Arith.round(aReserve2,2);
	}
	public void setReserve2(String aReserve2)
	{
		if (aReserve2 != null && !aReserve2.equals(""))
		{
			Double tDouble = new Double(aReserve2);
			double d = tDouble.doubleValue();
                Reserve2 = Arith.round(d,2);
		}
	}

	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getContPlanCode()
	{
		return ContPlanCode;
	}
	public void setContPlanCode(String aContPlanCode)
	{
		ContPlanCode = aContPlanCode;
	}

	/**
	* 使用另外一个 LRTempCessContInfoSchema 对象给 Schema 赋值
	* @param: aLRTempCessContInfoSchema LRTempCessContInfoSchema
	**/
	public void setSchema(LRTempCessContInfoSchema aLRTempCessContInfoSchema)
	{
		this.TempContCode = aLRTempCessContInfoSchema.getTempContCode();
		this.RiskCode = aLRTempCessContInfoSchema.getRiskCode();
		this.GetDutyCode = aLRTempCessContInfoSchema.getGetDutyCode();
		this.CessionMode = aLRTempCessContInfoSchema.getCessionMode();
		this.CalMode = aLRTempCessContInfoSchema.getCalMode();
		this.RetainAmnt = aLRTempCessContInfoSchema.getRetainAmnt();
		this.CessionRate = aLRTempCessContInfoSchema.getCessionRate();
		this.AccQuot = aLRTempCessContInfoSchema.getAccQuot();
		this.ReinProcFeeRate = aLRTempCessContInfoSchema.getReinProcFeeRate();
		this.SpeComRate = aLRTempCessContInfoSchema.getSpeComRate();
		this.ChoiRebaRate = aLRTempCessContInfoSchema.getChoiRebaRate();
		this.ProfitCom = aLRTempCessContInfoSchema.getProfitCom();
		this.ReinManFeeRate = aLRTempCessContInfoSchema.getReinManFeeRate();
		this.ClaimNoticeLmt = aLRTempCessContInfoSchema.getClaimNoticeLmt();
		this.ClaimAttLmt = aLRTempCessContInfoSchema.getClaimAttLmt();
		this.Reserve1 = aLRTempCessContInfoSchema.getReserve1();
		this.Reserve2 = aLRTempCessContInfoSchema.getReserve2();
		this.MakeDate = fDate.getDate( aLRTempCessContInfoSchema.getMakeDate());
		this.MakeTime = aLRTempCessContInfoSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLRTempCessContInfoSchema.getModifyDate());
		this.ModifyTime = aLRTempCessContInfoSchema.getModifyTime();
		this.Operator = aLRTempCessContInfoSchema.getOperator();
		this.ManageCom = aLRTempCessContInfoSchema.getManageCom();
		this.ContPlanCode = aLRTempCessContInfoSchema.getContPlanCode();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("TempContCode") == null )
				this.TempContCode = null;
			else
				this.TempContCode = rs.getString("TempContCode").trim();

			if( rs.getString("RiskCode") == null )
				this.RiskCode = null;
			else
				this.RiskCode = rs.getString("RiskCode").trim();

			if( rs.getString("GetDutyCode") == null )
				this.GetDutyCode = null;
			else
				this.GetDutyCode = rs.getString("GetDutyCode").trim();

			if( rs.getString("CessionMode") == null )
				this.CessionMode = null;
			else
				this.CessionMode = rs.getString("CessionMode").trim();

			this.CalMode = rs.getDouble("CalMode");
			this.RetainAmnt = rs.getDouble("RetainAmnt");
			this.CessionRate = rs.getDouble("CessionRate");
			this.AccQuot = rs.getDouble("AccQuot");
			this.ReinProcFeeRate = rs.getDouble("ReinProcFeeRate");
			this.SpeComRate = rs.getDouble("SpeComRate");
			this.ChoiRebaRate = rs.getDouble("ChoiRebaRate");
			this.ProfitCom = rs.getDouble("ProfitCom");
			this.ReinManFeeRate = rs.getDouble("ReinManFeeRate");
			this.ClaimNoticeLmt = rs.getDouble("ClaimNoticeLmt");
			this.ClaimAttLmt = rs.getDouble("ClaimAttLmt");
			this.Reserve1 = rs.getDouble("Reserve1");
			this.Reserve2 = rs.getDouble("Reserve2");
			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("ContPlanCode") == null )
				this.ContPlanCode = null;
			else
				this.ContPlanCode = rs.getString("ContPlanCode").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LRTempCessContInfo表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LRTempCessContInfoSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LRTempCessContInfoSchema getSchema()
	{
		LRTempCessContInfoSchema aLRTempCessContInfoSchema = new LRTempCessContInfoSchema();
		aLRTempCessContInfoSchema.setSchema(this);
		return aLRTempCessContInfoSchema;
	}

	public LRTempCessContInfoDB getDB()
	{
		LRTempCessContInfoDB aDBOper = new LRTempCessContInfoDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLRTempCessContInfo描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(TempContCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GetDutyCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CessionMode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(CalMode));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(RetainAmnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(CessionRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(AccQuot));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ReinProcFeeRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SpeComRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ChoiRebaRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ProfitCom));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ReinManFeeRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ClaimNoticeLmt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ClaimAttLmt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Reserve1));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Reserve2));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContPlanCode));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLRTempCessContInfo>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			TempContCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			GetDutyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			CessionMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			CalMode = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,5,SysConst.PACKAGESPILTER))).doubleValue();
			RetainAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,6,SysConst.PACKAGESPILTER))).doubleValue();
			CessionRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,7,SysConst.PACKAGESPILTER))).doubleValue();
			AccQuot = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,8,SysConst.PACKAGESPILTER))).doubleValue();
			ReinProcFeeRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,9,SysConst.PACKAGESPILTER))).doubleValue();
			SpeComRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,10,SysConst.PACKAGESPILTER))).doubleValue();
			ChoiRebaRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,11,SysConst.PACKAGESPILTER))).doubleValue();
			ProfitCom = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,12,SysConst.PACKAGESPILTER))).doubleValue();
			ReinManFeeRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,13,SysConst.PACKAGESPILTER))).doubleValue();
			ClaimNoticeLmt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,14,SysConst.PACKAGESPILTER))).doubleValue();
			ClaimAttLmt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,15,SysConst.PACKAGESPILTER))).doubleValue();
			Reserve1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,16,SysConst.PACKAGESPILTER))).doubleValue();
			Reserve2 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,17,SysConst.PACKAGESPILTER))).doubleValue();
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			ContPlanCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LRTempCessContInfoSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("TempContCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TempContCode));
		}
		if (FCode.equals("RiskCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
		}
		if (FCode.equals("GetDutyCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GetDutyCode));
		}
		if (FCode.equals("CessionMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CessionMode));
		}
		if (FCode.equals("CalMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CalMode));
		}
		if (FCode.equals("RetainAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RetainAmnt));
		}
		if (FCode.equals("CessionRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CessionRate));
		}
		if (FCode.equals("AccQuot"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccQuot));
		}
		if (FCode.equals("ReinProcFeeRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReinProcFeeRate));
		}
		if (FCode.equals("SpeComRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SpeComRate));
		}
		if (FCode.equals("ChoiRebaRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ChoiRebaRate));
		}
		if (FCode.equals("ProfitCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProfitCom));
		}
		if (FCode.equals("ReinManFeeRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReinManFeeRate));
		}
		if (FCode.equals("ClaimNoticeLmt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClaimNoticeLmt));
		}
		if (FCode.equals("ClaimAttLmt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClaimAttLmt));
		}
		if (FCode.equals("Reserve1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Reserve1));
		}
		if (FCode.equals("Reserve2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Reserve2));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("ContPlanCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContPlanCode));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(TempContCode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(RiskCode);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(GetDutyCode);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(CessionMode);
				break;
			case 4:
				strFieldValue = String.valueOf(CalMode);
				break;
			case 5:
				strFieldValue = String.valueOf(RetainAmnt);
				break;
			case 6:
				strFieldValue = String.valueOf(CessionRate);
				break;
			case 7:
				strFieldValue = String.valueOf(AccQuot);
				break;
			case 8:
				strFieldValue = String.valueOf(ReinProcFeeRate);
				break;
			case 9:
				strFieldValue = String.valueOf(SpeComRate);
				break;
			case 10:
				strFieldValue = String.valueOf(ChoiRebaRate);
				break;
			case 11:
				strFieldValue = String.valueOf(ProfitCom);
				break;
			case 12:
				strFieldValue = String.valueOf(ReinManFeeRate);
				break;
			case 13:
				strFieldValue = String.valueOf(ClaimNoticeLmt);
				break;
			case 14:
				strFieldValue = String.valueOf(ClaimAttLmt);
				break;
			case 15:
				strFieldValue = String.valueOf(Reserve1);
				break;
			case 16:
				strFieldValue = String.valueOf(Reserve2);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(ContPlanCode);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("TempContCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TempContCode = FValue.trim();
			}
			else
				TempContCode = null;
		}
		if (FCode.equalsIgnoreCase("RiskCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskCode = FValue.trim();
			}
			else
				RiskCode = null;
		}
		if (FCode.equalsIgnoreCase("GetDutyCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GetDutyCode = FValue.trim();
			}
			else
				GetDutyCode = null;
		}
		if (FCode.equalsIgnoreCase("CessionMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CessionMode = FValue.trim();
			}
			else
				CessionMode = null;
		}
		if (FCode.equalsIgnoreCase("CalMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				CalMode = d;
			}
		}
		if (FCode.equalsIgnoreCase("RetainAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				RetainAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("CessionRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				CessionRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("AccQuot"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				AccQuot = d;
			}
		}
		if (FCode.equalsIgnoreCase("ReinProcFeeRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ReinProcFeeRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("SpeComRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SpeComRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("ChoiRebaRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ChoiRebaRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("ProfitCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ProfitCom = d;
			}
		}
		if (FCode.equalsIgnoreCase("ReinManFeeRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ReinManFeeRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("ClaimNoticeLmt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ClaimNoticeLmt = d;
			}
		}
		if (FCode.equalsIgnoreCase("ClaimAttLmt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ClaimAttLmt = d;
			}
		}
		if (FCode.equalsIgnoreCase("Reserve1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Reserve1 = d;
			}
		}
		if (FCode.equalsIgnoreCase("Reserve2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Reserve2 = d;
			}
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("ContPlanCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContPlanCode = FValue.trim();
			}
			else
				ContPlanCode = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LRTempCessContInfoSchema other = (LRTempCessContInfoSchema)otherObject;
		return
			(TempContCode == null ? other.getTempContCode() == null : TempContCode.equals(other.getTempContCode()))
			&& (RiskCode == null ? other.getRiskCode() == null : RiskCode.equals(other.getRiskCode()))
			&& (GetDutyCode == null ? other.getGetDutyCode() == null : GetDutyCode.equals(other.getGetDutyCode()))
			&& (CessionMode == null ? other.getCessionMode() == null : CessionMode.equals(other.getCessionMode()))
			&& CalMode == other.getCalMode()
			&& RetainAmnt == other.getRetainAmnt()
			&& CessionRate == other.getCessionRate()
			&& AccQuot == other.getAccQuot()
			&& ReinProcFeeRate == other.getReinProcFeeRate()
			&& SpeComRate == other.getSpeComRate()
			&& ChoiRebaRate == other.getChoiRebaRate()
			&& ProfitCom == other.getProfitCom()
			&& ReinManFeeRate == other.getReinManFeeRate()
			&& ClaimNoticeLmt == other.getClaimNoticeLmt()
			&& ClaimAttLmt == other.getClaimAttLmt()
			&& Reserve1 == other.getReserve1()
			&& Reserve2 == other.getReserve2()
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (ContPlanCode == null ? other.getContPlanCode() == null : ContPlanCode.equals(other.getContPlanCode()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("TempContCode") ) {
			return 0;
		}
		if( strFieldName.equals("RiskCode") ) {
			return 1;
		}
		if( strFieldName.equals("GetDutyCode") ) {
			return 2;
		}
		if( strFieldName.equals("CessionMode") ) {
			return 3;
		}
		if( strFieldName.equals("CalMode") ) {
			return 4;
		}
		if( strFieldName.equals("RetainAmnt") ) {
			return 5;
		}
		if( strFieldName.equals("CessionRate") ) {
			return 6;
		}
		if( strFieldName.equals("AccQuot") ) {
			return 7;
		}
		if( strFieldName.equals("ReinProcFeeRate") ) {
			return 8;
		}
		if( strFieldName.equals("SpeComRate") ) {
			return 9;
		}
		if( strFieldName.equals("ChoiRebaRate") ) {
			return 10;
		}
		if( strFieldName.equals("ProfitCom") ) {
			return 11;
		}
		if( strFieldName.equals("ReinManFeeRate") ) {
			return 12;
		}
		if( strFieldName.equals("ClaimNoticeLmt") ) {
			return 13;
		}
		if( strFieldName.equals("ClaimAttLmt") ) {
			return 14;
		}
		if( strFieldName.equals("Reserve1") ) {
			return 15;
		}
		if( strFieldName.equals("Reserve2") ) {
			return 16;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 17;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 18;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 19;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 20;
		}
		if( strFieldName.equals("Operator") ) {
			return 21;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 22;
		}
		if( strFieldName.equals("ContPlanCode") ) {
			return 23;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "TempContCode";
				break;
			case 1:
				strFieldName = "RiskCode";
				break;
			case 2:
				strFieldName = "GetDutyCode";
				break;
			case 3:
				strFieldName = "CessionMode";
				break;
			case 4:
				strFieldName = "CalMode";
				break;
			case 5:
				strFieldName = "RetainAmnt";
				break;
			case 6:
				strFieldName = "CessionRate";
				break;
			case 7:
				strFieldName = "AccQuot";
				break;
			case 8:
				strFieldName = "ReinProcFeeRate";
				break;
			case 9:
				strFieldName = "SpeComRate";
				break;
			case 10:
				strFieldName = "ChoiRebaRate";
				break;
			case 11:
				strFieldName = "ProfitCom";
				break;
			case 12:
				strFieldName = "ReinManFeeRate";
				break;
			case 13:
				strFieldName = "ClaimNoticeLmt";
				break;
			case 14:
				strFieldName = "ClaimAttLmt";
				break;
			case 15:
				strFieldName = "Reserve1";
				break;
			case 16:
				strFieldName = "Reserve2";
				break;
			case 17:
				strFieldName = "MakeDate";
				break;
			case 18:
				strFieldName = "MakeTime";
				break;
			case 19:
				strFieldName = "ModifyDate";
				break;
			case 20:
				strFieldName = "ModifyTime";
				break;
			case 21:
				strFieldName = "Operator";
				break;
			case 22:
				strFieldName = "ManageCom";
				break;
			case 23:
				strFieldName = "ContPlanCode";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("TempContCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GetDutyCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CessionMode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CalMode") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("RetainAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("CessionRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("AccQuot") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ReinProcFeeRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SpeComRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ChoiRebaRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ProfitCom") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ReinManFeeRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ClaimNoticeLmt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ClaimAttLmt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Reserve1") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Reserve2") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContPlanCode") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 5:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 6:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 7:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 8:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 9:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 10:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 11:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 12:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 13:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 14:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 15:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 16:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 17:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
