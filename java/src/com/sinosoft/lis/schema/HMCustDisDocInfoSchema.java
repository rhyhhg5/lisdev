/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.HMCustDisDocInfoDB;

/*
 * <p>ClassName: HMCustDisDocInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2010-03-11
 */
public class HMCustDisDocInfoSchema implements Schema, Cloneable
{
	// @Field
	/** 流水号 */
	private String SerialNo;
	/** 客户号码 */
	private String CustomerNo;
	/** 客户姓名 */
	private String CustomerName;
	/** 分案号 */
	private String CaseNo;
	/** 赔案号 */
	private String ClmNo;
	/** 疾病编码 */
	private String DiseaseNo;
	/** 疾病名称 */
	private String DiseaseName;
	/** 就诊医院编码 */
	private String HospitalNo;
	/** 就诊医院名称 */
	private String HospitalName;
	/** 治疗经过 */
	private String CureProcess;
	/** 转诊情况 */
	private String TransTreatment;
	/** 备注 */
	private String Remark;
	/** 操作员代码 */
	private String Operator;
	/** 管理机构 */
	private String ManageCom;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 住院起始日期 */
	private Date HospStartDate;
	/** 住院结束日期 */
	private Date HospEndDate;
	/** 实际住院天数 */
	private int RealHospDate;
	/** 出院情况简述 */
	private String OutHospDescribe;
	/** 并发症 */
	private String Complication;
	/** 费用总额（发生费用） */
	private double SumFee;
	/** 医疗费用理赔金额 */
	private double RealMedicinePay;
	/** 备用字段1 */
	private String StandbyField1;
	/** 备用字段2 */
	private String StandbyField2;
	/** 备用字段3 */
	private String StandbyField3;

	public static final int FIELDNUM = 28;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public HMCustDisDocInfoSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "SerialNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		HMCustDisDocInfoSchema cloned = (HMCustDisDocInfoSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getSerialNo()
	{
		return SerialNo;
	}
	public void setSerialNo(String aSerialNo)
	{
		SerialNo = aSerialNo;
	}
	public String getCustomerNo()
	{
		return CustomerNo;
	}
	public void setCustomerNo(String aCustomerNo)
	{
		CustomerNo = aCustomerNo;
	}
	public String getCustomerName()
	{
		return CustomerName;
	}
	public void setCustomerName(String aCustomerName)
	{
		CustomerName = aCustomerName;
	}
	public String getCaseNo()
	{
		return CaseNo;
	}
	public void setCaseNo(String aCaseNo)
	{
		CaseNo = aCaseNo;
	}
	public String getClmNo()
	{
		return ClmNo;
	}
	public void setClmNo(String aClmNo)
	{
		ClmNo = aClmNo;
	}
	public String getDiseaseNo()
	{
		return DiseaseNo;
	}
	public void setDiseaseNo(String aDiseaseNo)
	{
		DiseaseNo = aDiseaseNo;
	}
	public String getDiseaseName()
	{
		return DiseaseName;
	}
	public void setDiseaseName(String aDiseaseName)
	{
		DiseaseName = aDiseaseName;
	}
	public String getHospitalNo()
	{
		return HospitalNo;
	}
	public void setHospitalNo(String aHospitalNo)
	{
		HospitalNo = aHospitalNo;
	}
	public String getHospitalName()
	{
		return HospitalName;
	}
	public void setHospitalName(String aHospitalName)
	{
		HospitalName = aHospitalName;
	}
	public String getCureProcess()
	{
		return CureProcess;
	}
	public void setCureProcess(String aCureProcess)
	{
		CureProcess = aCureProcess;
	}
	public String getTransTreatment()
	{
		return TransTreatment;
	}
	public void setTransTreatment(String aTransTreatment)
	{
		TransTreatment = aTransTreatment;
	}
	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
		Remark = aRemark;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getHospStartDate()
	{
		if( HospStartDate != null )
			return fDate.getString(HospStartDate);
		else
			return null;
	}
	public void setHospStartDate(Date aHospStartDate)
	{
		HospStartDate = aHospStartDate;
	}
	public void setHospStartDate(String aHospStartDate)
	{
		if (aHospStartDate != null && !aHospStartDate.equals("") )
		{
			HospStartDate = fDate.getDate( aHospStartDate );
		}
		else
			HospStartDate = null;
	}

	public String getHospEndDate()
	{
		if( HospEndDate != null )
			return fDate.getString(HospEndDate);
		else
			return null;
	}
	public void setHospEndDate(Date aHospEndDate)
	{
		HospEndDate = aHospEndDate;
	}
	public void setHospEndDate(String aHospEndDate)
	{
		if (aHospEndDate != null && !aHospEndDate.equals("") )
		{
			HospEndDate = fDate.getDate( aHospEndDate );
		}
		else
			HospEndDate = null;
	}

	public int getRealHospDate()
	{
		return RealHospDate;
	}
	public void setRealHospDate(int aRealHospDate)
	{
		RealHospDate = aRealHospDate;
	}
	public void setRealHospDate(String aRealHospDate)
	{
		if (aRealHospDate != null && !aRealHospDate.equals(""))
		{
			Integer tInteger = new Integer(aRealHospDate);
			int i = tInteger.intValue();
			RealHospDate = i;
		}
	}

	public String getOutHospDescribe()
	{
		return OutHospDescribe;
	}
	public void setOutHospDescribe(String aOutHospDescribe)
	{
		OutHospDescribe = aOutHospDescribe;
	}
	public String getComplication()
	{
		return Complication;
	}
	public void setComplication(String aComplication)
	{
		Complication = aComplication;
	}
	public double getSumFee()
	{
		return SumFee;
	}
	public void setSumFee(double aSumFee)
	{
		SumFee = Arith.round(aSumFee,2);
	}
	public void setSumFee(String aSumFee)
	{
		if (aSumFee != null && !aSumFee.equals(""))
		{
			Double tDouble = new Double(aSumFee);
			double d = tDouble.doubleValue();
                SumFee = Arith.round(d,2);
		}
	}

	public double getRealMedicinePay()
	{
		return RealMedicinePay;
	}
	public void setRealMedicinePay(double aRealMedicinePay)
	{
		RealMedicinePay = Arith.round(aRealMedicinePay,2);
	}
	public void setRealMedicinePay(String aRealMedicinePay)
	{
		if (aRealMedicinePay != null && !aRealMedicinePay.equals(""))
		{
			Double tDouble = new Double(aRealMedicinePay);
			double d = tDouble.doubleValue();
                RealMedicinePay = Arith.round(d,2);
		}
	}

	public String getStandbyField1()
	{
		return StandbyField1;
	}
	public void setStandbyField1(String aStandbyField1)
	{
		StandbyField1 = aStandbyField1;
	}
	public String getStandbyField2()
	{
		return StandbyField2;
	}
	public void setStandbyField2(String aStandbyField2)
	{
		StandbyField2 = aStandbyField2;
	}
	public String getStandbyField3()
	{
		return StandbyField3;
	}
	public void setStandbyField3(String aStandbyField3)
	{
		StandbyField3 = aStandbyField3;
	}

	/**
	* 使用另外一个 HMCustDisDocInfoSchema 对象给 Schema 赋值
	* @param: aHMCustDisDocInfoSchema HMCustDisDocInfoSchema
	**/
	public void setSchema(HMCustDisDocInfoSchema aHMCustDisDocInfoSchema)
	{
		this.SerialNo = aHMCustDisDocInfoSchema.getSerialNo();
		this.CustomerNo = aHMCustDisDocInfoSchema.getCustomerNo();
		this.CustomerName = aHMCustDisDocInfoSchema.getCustomerName();
		this.CaseNo = aHMCustDisDocInfoSchema.getCaseNo();
		this.ClmNo = aHMCustDisDocInfoSchema.getClmNo();
		this.DiseaseNo = aHMCustDisDocInfoSchema.getDiseaseNo();
		this.DiseaseName = aHMCustDisDocInfoSchema.getDiseaseName();
		this.HospitalNo = aHMCustDisDocInfoSchema.getHospitalNo();
		this.HospitalName = aHMCustDisDocInfoSchema.getHospitalName();
		this.CureProcess = aHMCustDisDocInfoSchema.getCureProcess();
		this.TransTreatment = aHMCustDisDocInfoSchema.getTransTreatment();
		this.Remark = aHMCustDisDocInfoSchema.getRemark();
		this.Operator = aHMCustDisDocInfoSchema.getOperator();
		this.ManageCom = aHMCustDisDocInfoSchema.getManageCom();
		this.MakeDate = fDate.getDate( aHMCustDisDocInfoSchema.getMakeDate());
		this.MakeTime = aHMCustDisDocInfoSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aHMCustDisDocInfoSchema.getModifyDate());
		this.ModifyTime = aHMCustDisDocInfoSchema.getModifyTime();
		this.HospStartDate = fDate.getDate( aHMCustDisDocInfoSchema.getHospStartDate());
		this.HospEndDate = fDate.getDate( aHMCustDisDocInfoSchema.getHospEndDate());
		this.RealHospDate = aHMCustDisDocInfoSchema.getRealHospDate();
		this.OutHospDescribe = aHMCustDisDocInfoSchema.getOutHospDescribe();
		this.Complication = aHMCustDisDocInfoSchema.getComplication();
		this.SumFee = aHMCustDisDocInfoSchema.getSumFee();
		this.RealMedicinePay = aHMCustDisDocInfoSchema.getRealMedicinePay();
		this.StandbyField1 = aHMCustDisDocInfoSchema.getStandbyField1();
		this.StandbyField2 = aHMCustDisDocInfoSchema.getStandbyField2();
		this.StandbyField3 = aHMCustDisDocInfoSchema.getStandbyField3();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("SerialNo") == null )
				this.SerialNo = null;
			else
				this.SerialNo = rs.getString("SerialNo").trim();

			if( rs.getString("CustomerNo") == null )
				this.CustomerNo = null;
			else
				this.CustomerNo = rs.getString("CustomerNo").trim();

			if( rs.getString("CustomerName") == null )
				this.CustomerName = null;
			else
				this.CustomerName = rs.getString("CustomerName").trim();

			if( rs.getString("CaseNo") == null )
				this.CaseNo = null;
			else
				this.CaseNo = rs.getString("CaseNo").trim();

			if( rs.getString("ClmNo") == null )
				this.ClmNo = null;
			else
				this.ClmNo = rs.getString("ClmNo").trim();

			if( rs.getString("DiseaseNo") == null )
				this.DiseaseNo = null;
			else
				this.DiseaseNo = rs.getString("DiseaseNo").trim();

			if( rs.getString("DiseaseName") == null )
				this.DiseaseName = null;
			else
				this.DiseaseName = rs.getString("DiseaseName").trim();

			if( rs.getString("HospitalNo") == null )
				this.HospitalNo = null;
			else
				this.HospitalNo = rs.getString("HospitalNo").trim();

			if( rs.getString("HospitalName") == null )
				this.HospitalName = null;
			else
				this.HospitalName = rs.getString("HospitalName").trim();

			if( rs.getString("CureProcess") == null )
				this.CureProcess = null;
			else
				this.CureProcess = rs.getString("CureProcess").trim();

			if( rs.getString("TransTreatment") == null )
				this.TransTreatment = null;
			else
				this.TransTreatment = rs.getString("TransTreatment").trim();

			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			this.HospStartDate = rs.getDate("HospStartDate");
			this.HospEndDate = rs.getDate("HospEndDate");
			this.RealHospDate = rs.getInt("RealHospDate");
			if( rs.getString("OutHospDescribe") == null )
				this.OutHospDescribe = null;
			else
				this.OutHospDescribe = rs.getString("OutHospDescribe").trim();

			if( rs.getString("Complication") == null )
				this.Complication = null;
			else
				this.Complication = rs.getString("Complication").trim();

			this.SumFee = rs.getDouble("SumFee");
			this.RealMedicinePay = rs.getDouble("RealMedicinePay");
			if( rs.getString("StandbyField1") == null )
				this.StandbyField1 = null;
			else
				this.StandbyField1 = rs.getString("StandbyField1").trim();

			if( rs.getString("StandbyField2") == null )
				this.StandbyField2 = null;
			else
				this.StandbyField2 = rs.getString("StandbyField2").trim();

			if( rs.getString("StandbyField3") == null )
				this.StandbyField3 = null;
			else
				this.StandbyField3 = rs.getString("StandbyField3").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的HMCustDisDocInfo表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "HMCustDisDocInfoSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public HMCustDisDocInfoSchema getSchema()
	{
		HMCustDisDocInfoSchema aHMCustDisDocInfoSchema = new HMCustDisDocInfoSchema();
		aHMCustDisDocInfoSchema.setSchema(this);
		return aHMCustDisDocInfoSchema;
	}

	public HMCustDisDocInfoDB getDB()
	{
		HMCustDisDocInfoDB aDBOper = new HMCustDisDocInfoDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpHMCustDisDocInfo描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(SerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CaseNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ClmNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DiseaseNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DiseaseName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HospitalNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HospitalName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CureProcess)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TransTreatment)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( HospStartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( HospEndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(RealHospDate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OutHospDescribe)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Complication)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SumFee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(RealMedicinePay));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandbyField1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandbyField2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandbyField3));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpHMCustDisDocInfo>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			CustomerName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			CaseNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			ClmNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			DiseaseNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			DiseaseName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			HospitalNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			HospitalName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			CureProcess = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			TransTreatment = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			HospStartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,SysConst.PACKAGESPILTER));
			HospEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,SysConst.PACKAGESPILTER));
			RealHospDate= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,21,SysConst.PACKAGESPILTER))).intValue();
			OutHospDescribe = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			Complication = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			SumFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,24,SysConst.PACKAGESPILTER))).doubleValue();
			RealMedicinePay = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,25,SysConst.PACKAGESPILTER))).doubleValue();
			StandbyField1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			StandbyField2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			StandbyField3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "HMCustDisDocInfoSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("SerialNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
		}
		if (FCode.equals("CustomerNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
		}
		if (FCode.equals("CustomerName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerName));
		}
		if (FCode.equals("CaseNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CaseNo));
		}
		if (FCode.equals("ClmNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClmNo));
		}
		if (FCode.equals("DiseaseNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DiseaseNo));
		}
		if (FCode.equals("DiseaseName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DiseaseName));
		}
		if (FCode.equals("HospitalNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HospitalNo));
		}
		if (FCode.equals("HospitalName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HospitalName));
		}
		if (FCode.equals("CureProcess"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CureProcess));
		}
		if (FCode.equals("TransTreatment"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransTreatment));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("HospStartDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getHospStartDate()));
		}
		if (FCode.equals("HospEndDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getHospEndDate()));
		}
		if (FCode.equals("RealHospDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RealHospDate));
		}
		if (FCode.equals("OutHospDescribe"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OutHospDescribe));
		}
		if (FCode.equals("Complication"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Complication));
		}
		if (FCode.equals("SumFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SumFee));
		}
		if (FCode.equals("RealMedicinePay"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RealMedicinePay));
		}
		if (FCode.equals("StandbyField1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyField1));
		}
		if (FCode.equals("StandbyField2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyField2));
		}
		if (FCode.equals("StandbyField3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyField3));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(SerialNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(CustomerNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(CustomerName);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(CaseNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(ClmNo);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(DiseaseNo);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(DiseaseName);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(HospitalNo);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(HospitalName);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(CureProcess);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(TransTreatment);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getHospStartDate()));
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getHospEndDate()));
				break;
			case 20:
				strFieldValue = String.valueOf(RealHospDate);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(OutHospDescribe);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(Complication);
				break;
			case 23:
				strFieldValue = String.valueOf(SumFee);
				break;
			case 24:
				strFieldValue = String.valueOf(RealMedicinePay);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(StandbyField1);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(StandbyField2);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(StandbyField3);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("SerialNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SerialNo = FValue.trim();
			}
			else
				SerialNo = null;
		}
		if (FCode.equalsIgnoreCase("CustomerNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerNo = FValue.trim();
			}
			else
				CustomerNo = null;
		}
		if (FCode.equalsIgnoreCase("CustomerName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerName = FValue.trim();
			}
			else
				CustomerName = null;
		}
		if (FCode.equalsIgnoreCase("CaseNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CaseNo = FValue.trim();
			}
			else
				CaseNo = null;
		}
		if (FCode.equalsIgnoreCase("ClmNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ClmNo = FValue.trim();
			}
			else
				ClmNo = null;
		}
		if (FCode.equalsIgnoreCase("DiseaseNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DiseaseNo = FValue.trim();
			}
			else
				DiseaseNo = null;
		}
		if (FCode.equalsIgnoreCase("DiseaseName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DiseaseName = FValue.trim();
			}
			else
				DiseaseName = null;
		}
		if (FCode.equalsIgnoreCase("HospitalNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HospitalNo = FValue.trim();
			}
			else
				HospitalNo = null;
		}
		if (FCode.equalsIgnoreCase("HospitalName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HospitalName = FValue.trim();
			}
			else
				HospitalName = null;
		}
		if (FCode.equalsIgnoreCase("CureProcess"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CureProcess = FValue.trim();
			}
			else
				CureProcess = null;
		}
		if (FCode.equalsIgnoreCase("TransTreatment"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TransTreatment = FValue.trim();
			}
			else
				TransTreatment = null;
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("HospStartDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				HospStartDate = fDate.getDate( FValue );
			}
			else
				HospStartDate = null;
		}
		if (FCode.equalsIgnoreCase("HospEndDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				HospEndDate = fDate.getDate( FValue );
			}
			else
				HospEndDate = null;
		}
		if (FCode.equalsIgnoreCase("RealHospDate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				RealHospDate = i;
			}
		}
		if (FCode.equalsIgnoreCase("OutHospDescribe"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OutHospDescribe = FValue.trim();
			}
			else
				OutHospDescribe = null;
		}
		if (FCode.equalsIgnoreCase("Complication"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Complication = FValue.trim();
			}
			else
				Complication = null;
		}
		if (FCode.equalsIgnoreCase("SumFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SumFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("RealMedicinePay"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				RealMedicinePay = d;
			}
		}
		if (FCode.equalsIgnoreCase("StandbyField1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandbyField1 = FValue.trim();
			}
			else
				StandbyField1 = null;
		}
		if (FCode.equalsIgnoreCase("StandbyField2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandbyField2 = FValue.trim();
			}
			else
				StandbyField2 = null;
		}
		if (FCode.equalsIgnoreCase("StandbyField3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandbyField3 = FValue.trim();
			}
			else
				StandbyField3 = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		HMCustDisDocInfoSchema other = (HMCustDisDocInfoSchema)otherObject;
		return
			(SerialNo == null ? other.getSerialNo() == null : SerialNo.equals(other.getSerialNo()))
			&& (CustomerNo == null ? other.getCustomerNo() == null : CustomerNo.equals(other.getCustomerNo()))
			&& (CustomerName == null ? other.getCustomerName() == null : CustomerName.equals(other.getCustomerName()))
			&& (CaseNo == null ? other.getCaseNo() == null : CaseNo.equals(other.getCaseNo()))
			&& (ClmNo == null ? other.getClmNo() == null : ClmNo.equals(other.getClmNo()))
			&& (DiseaseNo == null ? other.getDiseaseNo() == null : DiseaseNo.equals(other.getDiseaseNo()))
			&& (DiseaseName == null ? other.getDiseaseName() == null : DiseaseName.equals(other.getDiseaseName()))
			&& (HospitalNo == null ? other.getHospitalNo() == null : HospitalNo.equals(other.getHospitalNo()))
			&& (HospitalName == null ? other.getHospitalName() == null : HospitalName.equals(other.getHospitalName()))
			&& (CureProcess == null ? other.getCureProcess() == null : CureProcess.equals(other.getCureProcess()))
			&& (TransTreatment == null ? other.getTransTreatment() == null : TransTreatment.equals(other.getTransTreatment()))
			&& (Remark == null ? other.getRemark() == null : Remark.equals(other.getRemark()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (HospStartDate == null ? other.getHospStartDate() == null : fDate.getString(HospStartDate).equals(other.getHospStartDate()))
			&& (HospEndDate == null ? other.getHospEndDate() == null : fDate.getString(HospEndDate).equals(other.getHospEndDate()))
			&& RealHospDate == other.getRealHospDate()
			&& (OutHospDescribe == null ? other.getOutHospDescribe() == null : OutHospDescribe.equals(other.getOutHospDescribe()))
			&& (Complication == null ? other.getComplication() == null : Complication.equals(other.getComplication()))
			&& SumFee == other.getSumFee()
			&& RealMedicinePay == other.getRealMedicinePay()
			&& (StandbyField1 == null ? other.getStandbyField1() == null : StandbyField1.equals(other.getStandbyField1()))
			&& (StandbyField2 == null ? other.getStandbyField2() == null : StandbyField2.equals(other.getStandbyField2()))
			&& (StandbyField3 == null ? other.getStandbyField3() == null : StandbyField3.equals(other.getStandbyField3()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("SerialNo") ) {
			return 0;
		}
		if( strFieldName.equals("CustomerNo") ) {
			return 1;
		}
		if( strFieldName.equals("CustomerName") ) {
			return 2;
		}
		if( strFieldName.equals("CaseNo") ) {
			return 3;
		}
		if( strFieldName.equals("ClmNo") ) {
			return 4;
		}
		if( strFieldName.equals("DiseaseNo") ) {
			return 5;
		}
		if( strFieldName.equals("DiseaseName") ) {
			return 6;
		}
		if( strFieldName.equals("HospitalNo") ) {
			return 7;
		}
		if( strFieldName.equals("HospitalName") ) {
			return 8;
		}
		if( strFieldName.equals("CureProcess") ) {
			return 9;
		}
		if( strFieldName.equals("TransTreatment") ) {
			return 10;
		}
		if( strFieldName.equals("Remark") ) {
			return 11;
		}
		if( strFieldName.equals("Operator") ) {
			return 12;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 13;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 14;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 15;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 16;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 17;
		}
		if( strFieldName.equals("HospStartDate") ) {
			return 18;
		}
		if( strFieldName.equals("HospEndDate") ) {
			return 19;
		}
		if( strFieldName.equals("RealHospDate") ) {
			return 20;
		}
		if( strFieldName.equals("OutHospDescribe") ) {
			return 21;
		}
		if( strFieldName.equals("Complication") ) {
			return 22;
		}
		if( strFieldName.equals("SumFee") ) {
			return 23;
		}
		if( strFieldName.equals("RealMedicinePay") ) {
			return 24;
		}
		if( strFieldName.equals("StandbyField1") ) {
			return 25;
		}
		if( strFieldName.equals("StandbyField2") ) {
			return 26;
		}
		if( strFieldName.equals("StandbyField3") ) {
			return 27;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "SerialNo";
				break;
			case 1:
				strFieldName = "CustomerNo";
				break;
			case 2:
				strFieldName = "CustomerName";
				break;
			case 3:
				strFieldName = "CaseNo";
				break;
			case 4:
				strFieldName = "ClmNo";
				break;
			case 5:
				strFieldName = "DiseaseNo";
				break;
			case 6:
				strFieldName = "DiseaseName";
				break;
			case 7:
				strFieldName = "HospitalNo";
				break;
			case 8:
				strFieldName = "HospitalName";
				break;
			case 9:
				strFieldName = "CureProcess";
				break;
			case 10:
				strFieldName = "TransTreatment";
				break;
			case 11:
				strFieldName = "Remark";
				break;
			case 12:
				strFieldName = "Operator";
				break;
			case 13:
				strFieldName = "ManageCom";
				break;
			case 14:
				strFieldName = "MakeDate";
				break;
			case 15:
				strFieldName = "MakeTime";
				break;
			case 16:
				strFieldName = "ModifyDate";
				break;
			case 17:
				strFieldName = "ModifyTime";
				break;
			case 18:
				strFieldName = "HospStartDate";
				break;
			case 19:
				strFieldName = "HospEndDate";
				break;
			case 20:
				strFieldName = "RealHospDate";
				break;
			case 21:
				strFieldName = "OutHospDescribe";
				break;
			case 22:
				strFieldName = "Complication";
				break;
			case 23:
				strFieldName = "SumFee";
				break;
			case 24:
				strFieldName = "RealMedicinePay";
				break;
			case 25:
				strFieldName = "StandbyField1";
				break;
			case 26:
				strFieldName = "StandbyField2";
				break;
			case 27:
				strFieldName = "StandbyField3";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("SerialNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CaseNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ClmNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DiseaseNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DiseaseName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HospitalNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HospitalName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CureProcess") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TransTreatment") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HospStartDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("HospEndDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("RealHospDate") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("OutHospDescribe") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Complication") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SumFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("RealMedicinePay") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("StandbyField1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandbyField2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandbyField3") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 19:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 20:
				nFieldType = Schema.TYPE_INT;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 24:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
