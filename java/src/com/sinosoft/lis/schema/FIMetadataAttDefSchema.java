/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.FIMetadataAttDefDB;

/*
 * <p>ClassName: FIMetadataAttDefSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 财务接口最新版
 * @CreateDate：2011-12-13
 */
public class FIMetadataAttDefSchema implements Schema, Cloneable
{
	// @Field
	/** 元数据编号 */
	private String MetadataNo;
	/** 属性编码 */
	private String AttNo;
	/** 属性名称 */
	private String AttName;
	/** 属性类型 */
	private String AttType;
	/** 状态 */
	private String State;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;

	public static final int FIELDNUM = 7;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public FIMetadataAttDefSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "MetadataNo";
		pk[1] = "AttNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		FIMetadataAttDefSchema cloned = (FIMetadataAttDefSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getMetadataNo()
	{
		return MetadataNo;
	}
	public void setMetadataNo(String aMetadataNo)
	{
		MetadataNo = aMetadataNo;
	}
	public String getAttNo()
	{
		return AttNo;
	}
	public void setAttNo(String aAttNo)
	{
		AttNo = aAttNo;
	}
	public String getAttName()
	{
		return AttName;
	}
	public void setAttName(String aAttName)
	{
		AttName = aAttName;
	}
	public String getAttType()
	{
		return AttType;
	}
	public void setAttType(String aAttType)
	{
		AttType = aAttType;
	}
	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}

	/**
	* 使用另外一个 FIMetadataAttDefSchema 对象给 Schema 赋值
	* @param: aFIMetadataAttDefSchema FIMetadataAttDefSchema
	**/
	public void setSchema(FIMetadataAttDefSchema aFIMetadataAttDefSchema)
	{
		this.MetadataNo = aFIMetadataAttDefSchema.getMetadataNo();
		this.AttNo = aFIMetadataAttDefSchema.getAttNo();
		this.AttName = aFIMetadataAttDefSchema.getAttName();
		this.AttType = aFIMetadataAttDefSchema.getAttType();
		this.State = aFIMetadataAttDefSchema.getState();
		this.MakeDate = fDate.getDate( aFIMetadataAttDefSchema.getMakeDate());
		this.MakeTime = aFIMetadataAttDefSchema.getMakeTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("MetadataNo") == null )
				this.MetadataNo = null;
			else
				this.MetadataNo = rs.getString("MetadataNo").trim();

			if( rs.getString("AttNo") == null )
				this.AttNo = null;
			else
				this.AttNo = rs.getString("AttNo").trim();

			if( rs.getString("AttName") == null )
				this.AttName = null;
			else
				this.AttName = rs.getString("AttName").trim();

			if( rs.getString("AttType") == null )
				this.AttType = null;
			else
				this.AttType = rs.getString("AttType").trim();

			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的FIMetadataAttDef表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIMetadataAttDefSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public FIMetadataAttDefSchema getSchema()
	{
		FIMetadataAttDefSchema aFIMetadataAttDefSchema = new FIMetadataAttDefSchema();
		aFIMetadataAttDefSchema.setSchema(this);
		return aFIMetadataAttDefSchema;
	}

	public FIMetadataAttDefDB getDB()
	{
		FIMetadataAttDefDB aDBOper = new FIMetadataAttDefDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpFIMetadataAttDef描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(MetadataNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AttNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AttName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AttType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpFIMetadataAttDef>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			MetadataNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			AttNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			AttName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			AttType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIMetadataAttDefSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("MetadataNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MetadataNo));
		}
		if (FCode.equals("AttNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AttNo));
		}
		if (FCode.equals("AttName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AttName));
		}
		if (FCode.equals("AttType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AttType));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(MetadataNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(AttNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(AttName);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(AttType);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("MetadataNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MetadataNo = FValue.trim();
			}
			else
				MetadataNo = null;
		}
		if (FCode.equalsIgnoreCase("AttNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AttNo = FValue.trim();
			}
			else
				AttNo = null;
		}
		if (FCode.equalsIgnoreCase("AttName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AttName = FValue.trim();
			}
			else
				AttName = null;
		}
		if (FCode.equalsIgnoreCase("AttType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AttType = FValue.trim();
			}
			else
				AttType = null;
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		FIMetadataAttDefSchema other = (FIMetadataAttDefSchema)otherObject;
		return
			(MetadataNo == null ? other.getMetadataNo() == null : MetadataNo.equals(other.getMetadataNo()))
			&& (AttNo == null ? other.getAttNo() == null : AttNo.equals(other.getAttNo()))
			&& (AttName == null ? other.getAttName() == null : AttName.equals(other.getAttName()))
			&& (AttType == null ? other.getAttType() == null : AttType.equals(other.getAttType()))
			&& (State == null ? other.getState() == null : State.equals(other.getState()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("MetadataNo") ) {
			return 0;
		}
		if( strFieldName.equals("AttNo") ) {
			return 1;
		}
		if( strFieldName.equals("AttName") ) {
			return 2;
		}
		if( strFieldName.equals("AttType") ) {
			return 3;
		}
		if( strFieldName.equals("State") ) {
			return 4;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 5;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 6;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "MetadataNo";
				break;
			case 1:
				strFieldName = "AttNo";
				break;
			case 2:
				strFieldName = "AttName";
				break;
			case 3:
				strFieldName = "AttType";
				break;
			case 4:
				strFieldName = "State";
				break;
			case 5:
				strFieldName = "MakeDate";
				break;
			case 6:
				strFieldName = "MakeTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("MetadataNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AttNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AttName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AttType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
