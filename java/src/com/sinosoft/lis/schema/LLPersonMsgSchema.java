/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LLPersonMsgDB;

/*
 * <p>ClassName: LLPersonMsgSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2016-11-25
 */
public class LLPersonMsgSchema implements Schema, Cloneable
{
	// @Field
	/** 姓名 */
	private String Name;
	/** 身份证号码 */
	private String ID;
	/** Securityno */
	private String SecurityNo;
	/** Insuredstat */
	private String InsuredStat;
	/** Sex */
	private String Sex;
	/** Hospitalname */
	private String HospitalName;
	/** Hospitalcode */
	private String HospitalCode;
	/** Feetype */
	private String FeeType;
	/** Receiptno */
	private String ReceiptNo;
	/** Feedate */
	private Date FeeDate;
	/** Hospstartdate */
	private Date HospStartDate;
	/** Hospenddate */
	private Date HospEndDate;
	/** Realhospdate */
	private Date RealHospDate;
	/** Applyamnt */
	private double ApplyAmnt;
	/** Getlimit */
	private String GetLimit;
	/** Accountpay */
	private String AccountPay;
	/** Planfee */
	private String PlanFee;
	/** Midamnt */
	private String MidAmnt;
	/** Supinhosfee */
	private String SupInHosFee;
	/** Highamnt1 */
	private String HighAmnt1;
	/** Selfpay1 */
	private String SelfPay1;
	/** Selfpay2 */
	private String SelfPay2;
	/** Selfamnt */
	private String SelfAmnt;
	/** Otherorganamnt */
	private String OtherOrganAmnt;
	/** Diseasename */
	private String DiseaseName;
	/** Diseasecode */
	private String DiseaseCode;
	/** Accdate */
	private Date AccDate;
	/** Affixno */
	private String AffixNo;
	/** Originflag */
	private String OriginFlag;
	/** Getdutycode */
	private String GetDutyCode;
	/** 银行编码 */
	private String BankCode;
	/** Bankname */
	private String BankName;
	/** 账户名 */
	private String AccountName;
	/** 银行账户 */
	private String Account;
	/** Getdutykind */
	private String GetDutyKind;
	/** Customerno */
	private String CustomerNo;
	/** Inpatientno */
	private String InpatientNo;
	/** Makedate */
	private Date MakeDate;
	/** Mobilephone */
	private String MobilePhone;
	/** Refuseamnt */
	private String RefuseAmnt;
	/** Reimbursement */
	private String Reimbursement;
	/** Remark */
	private String Remark;
	/** 联系手机 */
	private String PhoneNumber;
	/** 电子邮箱 */
	private String EMail;
	/** 赔款领取方式 */
	private String ReceiveMethod;
	/** 签约银行 */
	private String SignBank;
	/** 申报金额 */
	private String DeclareAmount;
	/** 是否回销预付赔款标志 */
	private String AdvanceFlag;
	/** 领款人 */
	private String Drawer;
	/** 领款人身份证号 */
	private String DrawerID;

	public static final int FIELDNUM = 50;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LLPersonMsgSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[0];

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LLPersonMsgSchema cloned = (LLPersonMsgSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getName()
	{
		return Name;
	}
	public void setName(String aName)
	{
		Name = aName;
	}
	public String getID()
	{
		return ID;
	}
	public void setID(String aID)
	{
		ID = aID;
	}
	public String getSecurityNo()
	{
		return SecurityNo;
	}
	public void setSecurityNo(String aSecurityNo)
	{
		SecurityNo = aSecurityNo;
	}
	public String getInsuredStat()
	{
		return InsuredStat;
	}
	public void setInsuredStat(String aInsuredStat)
	{
		InsuredStat = aInsuredStat;
	}
	public String getSex()
	{
		return Sex;
	}
	public void setSex(String aSex)
	{
		Sex = aSex;
	}
	public String getHospitalName()
	{
		return HospitalName;
	}
	public void setHospitalName(String aHospitalName)
	{
		HospitalName = aHospitalName;
	}
	public String getHospitalCode()
	{
		return HospitalCode;
	}
	public void setHospitalCode(String aHospitalCode)
	{
		HospitalCode = aHospitalCode;
	}
	public String getFeeType()
	{
		return FeeType;
	}
	public void setFeeType(String aFeeType)
	{
		FeeType = aFeeType;
	}
	public String getReceiptNo()
	{
		return ReceiptNo;
	}
	public void setReceiptNo(String aReceiptNo)
	{
		ReceiptNo = aReceiptNo;
	}
	public String getFeeDate()
	{
		if( FeeDate != null )
			return fDate.getString(FeeDate);
		else
			return null;
	}
	public void setFeeDate(Date aFeeDate)
	{
		FeeDate = aFeeDate;
	}
	public void setFeeDate(String aFeeDate)
	{
		if (aFeeDate != null && !aFeeDate.equals("") )
		{
			FeeDate = fDate.getDate( aFeeDate );
		}
		else
			FeeDate = null;
	}

	public String getHospStartDate()
	{
		if( HospStartDate != null )
			return fDate.getString(HospStartDate);
		else
			return null;
	}
	public void setHospStartDate(Date aHospStartDate)
	{
		HospStartDate = aHospStartDate;
	}
	public void setHospStartDate(String aHospStartDate)
	{
		if (aHospStartDate != null && !aHospStartDate.equals("") )
		{
			HospStartDate = fDate.getDate( aHospStartDate );
		}
		else
			HospStartDate = null;
	}

	public String getHospEndDate()
	{
		if( HospEndDate != null )
			return fDate.getString(HospEndDate);
		else
			return null;
	}
	public void setHospEndDate(Date aHospEndDate)
	{
		HospEndDate = aHospEndDate;
	}
	public void setHospEndDate(String aHospEndDate)
	{
		if (aHospEndDate != null && !aHospEndDate.equals("") )
		{
			HospEndDate = fDate.getDate( aHospEndDate );
		}
		else
			HospEndDate = null;
	}

	public String getRealHospDate()
	{
		if( RealHospDate != null )
			return fDate.getString(RealHospDate);
		else
			return null;
	}
	public void setRealHospDate(Date aRealHospDate)
	{
		RealHospDate = aRealHospDate;
	}
	public void setRealHospDate(String aRealHospDate)
	{
		if (aRealHospDate != null && !aRealHospDate.equals("") )
		{
			RealHospDate = fDate.getDate( aRealHospDate );
		}
		else
			RealHospDate = null;
	}

	public double getApplyAmnt()
	{
		return ApplyAmnt;
	}
	public void setApplyAmnt(double aApplyAmnt)
	{
		ApplyAmnt = Arith.round(aApplyAmnt,2);
	}
	public void setApplyAmnt(String aApplyAmnt)
	{
		if (aApplyAmnt != null && !aApplyAmnt.equals(""))
		{
			Double tDouble = new Double(aApplyAmnt);
			double d = tDouble.doubleValue();
                ApplyAmnt = Arith.round(d,2);
		}
	}

	public String getGetLimit()
	{
		return GetLimit;
	}
	public void setGetLimit(String aGetLimit)
	{
		GetLimit = aGetLimit;
	}
	public String getAccountPay()
	{
		return AccountPay;
	}
	public void setAccountPay(String aAccountPay)
	{
		AccountPay = aAccountPay;
	}
	public String getPlanFee()
	{
		return PlanFee;
	}
	public void setPlanFee(String aPlanFee)
	{
		PlanFee = aPlanFee;
	}
	public String getMidAmnt()
	{
		return MidAmnt;
	}
	public void setMidAmnt(String aMidAmnt)
	{
		MidAmnt = aMidAmnt;
	}
	public String getSupInHosFee()
	{
		return SupInHosFee;
	}
	public void setSupInHosFee(String aSupInHosFee)
	{
		SupInHosFee = aSupInHosFee;
	}
	public String getHighAmnt1()
	{
		return HighAmnt1;
	}
	public void setHighAmnt1(String aHighAmnt1)
	{
		HighAmnt1 = aHighAmnt1;
	}
	public String getSelfPay1()
	{
		return SelfPay1;
	}
	public void setSelfPay1(String aSelfPay1)
	{
		SelfPay1 = aSelfPay1;
	}
	public String getSelfPay2()
	{
		return SelfPay2;
	}
	public void setSelfPay2(String aSelfPay2)
	{
		SelfPay2 = aSelfPay2;
	}
	public String getSelfAmnt()
	{
		return SelfAmnt;
	}
	public void setSelfAmnt(String aSelfAmnt)
	{
		SelfAmnt = aSelfAmnt;
	}
	public String getOtherOrganAmnt()
	{
		return OtherOrganAmnt;
	}
	public void setOtherOrganAmnt(String aOtherOrganAmnt)
	{
		OtherOrganAmnt = aOtherOrganAmnt;
	}
	public String getDiseaseName()
	{
		return DiseaseName;
	}
	public void setDiseaseName(String aDiseaseName)
	{
		DiseaseName = aDiseaseName;
	}
	public String getDiseaseCode()
	{
		return DiseaseCode;
	}
	public void setDiseaseCode(String aDiseaseCode)
	{
		DiseaseCode = aDiseaseCode;
	}
	public String getAccDate()
	{
		if( AccDate != null )
			return fDate.getString(AccDate);
		else
			return null;
	}
	public void setAccDate(Date aAccDate)
	{
		AccDate = aAccDate;
	}
	public void setAccDate(String aAccDate)
	{
		if (aAccDate != null && !aAccDate.equals("") )
		{
			AccDate = fDate.getDate( aAccDate );
		}
		else
			AccDate = null;
	}

	public String getAffixNo()
	{
		return AffixNo;
	}
	public void setAffixNo(String aAffixNo)
	{
		AffixNo = aAffixNo;
	}
	public String getOriginFlag()
	{
		return OriginFlag;
	}
	public void setOriginFlag(String aOriginFlag)
	{
		OriginFlag = aOriginFlag;
	}
	public String getGetDutyCode()
	{
		return GetDutyCode;
	}
	public void setGetDutyCode(String aGetDutyCode)
	{
		GetDutyCode = aGetDutyCode;
	}
	public String getBankCode()
	{
		return BankCode;
	}
	public void setBankCode(String aBankCode)
	{
		BankCode = aBankCode;
	}
	public String getBankName()
	{
		return BankName;
	}
	public void setBankName(String aBankName)
	{
		BankName = aBankName;
	}
	public String getAccountName()
	{
		return AccountName;
	}
	public void setAccountName(String aAccountName)
	{
		AccountName = aAccountName;
	}
	public String getAccount()
	{
		return Account;
	}
	public void setAccount(String aAccount)
	{
		Account = aAccount;
	}
	public String getGetDutyKind()
	{
		return GetDutyKind;
	}
	public void setGetDutyKind(String aGetDutyKind)
	{
		GetDutyKind = aGetDutyKind;
	}
	public String getCustomerNo()
	{
		return CustomerNo;
	}
	public void setCustomerNo(String aCustomerNo)
	{
		CustomerNo = aCustomerNo;
	}
	public String getInpatientNo()
	{
		return InpatientNo;
	}
	public void setInpatientNo(String aInpatientNo)
	{
		InpatientNo = aInpatientNo;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMobilePhone()
	{
		return MobilePhone;
	}
	public void setMobilePhone(String aMobilePhone)
	{
		MobilePhone = aMobilePhone;
	}
	public String getRefuseAmnt()
	{
		return RefuseAmnt;
	}
	public void setRefuseAmnt(String aRefuseAmnt)
	{
		RefuseAmnt = aRefuseAmnt;
	}
	public String getReimbursement()
	{
		return Reimbursement;
	}
	public void setReimbursement(String aReimbursement)
	{
		Reimbursement = aReimbursement;
	}
	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
		Remark = aRemark;
	}
	public String getPhoneNumber()
	{
		return PhoneNumber;
	}
	public void setPhoneNumber(String aPhoneNumber)
	{
		PhoneNumber = aPhoneNumber;
	}
	public String getEMail()
	{
		return EMail;
	}
	public void setEMail(String aEMail)
	{
		EMail = aEMail;
	}
	public String getReceiveMethod()
	{
		return ReceiveMethod;
	}
	public void setReceiveMethod(String aReceiveMethod)
	{
		ReceiveMethod = aReceiveMethod;
	}
	public String getSignBank()
	{
		return SignBank;
	}
	public void setSignBank(String aSignBank)
	{
		SignBank = aSignBank;
	}
	public String getDeclareAmount()
	{
		return DeclareAmount;
	}
	public void setDeclareAmount(String aDeclareAmount)
	{
		DeclareAmount = aDeclareAmount;
	}
	public String getAdvanceFlag()
	{
		return AdvanceFlag;
	}
	public void setAdvanceFlag(String aAdvanceFlag)
	{
		AdvanceFlag = aAdvanceFlag;
	}
	public String getDrawer()
	{
		return Drawer;
	}
	public void setDrawer(String aDrawer)
	{
		Drawer = aDrawer;
	}
	public String getDrawerID()
	{
		return DrawerID;
	}
	public void setDrawerID(String aDrawerID)
	{
		DrawerID = aDrawerID;
	}

	/**
	* 使用另外一个 LLPersonMsgSchema 对象给 Schema 赋值
	* @param: aLLPersonMsgSchema LLPersonMsgSchema
	**/
	public void setSchema(LLPersonMsgSchema aLLPersonMsgSchema)
	{
		this.Name = aLLPersonMsgSchema.getName();
		this.ID = aLLPersonMsgSchema.getID();
		this.SecurityNo = aLLPersonMsgSchema.getSecurityNo();
		this.InsuredStat = aLLPersonMsgSchema.getInsuredStat();
		this.Sex = aLLPersonMsgSchema.getSex();
		this.HospitalName = aLLPersonMsgSchema.getHospitalName();
		this.HospitalCode = aLLPersonMsgSchema.getHospitalCode();
		this.FeeType = aLLPersonMsgSchema.getFeeType();
		this.ReceiptNo = aLLPersonMsgSchema.getReceiptNo();
		this.FeeDate = fDate.getDate( aLLPersonMsgSchema.getFeeDate());
		this.HospStartDate = fDate.getDate( aLLPersonMsgSchema.getHospStartDate());
		this.HospEndDate = fDate.getDate( aLLPersonMsgSchema.getHospEndDate());
		this.RealHospDate = fDate.getDate( aLLPersonMsgSchema.getRealHospDate());
		this.ApplyAmnt = aLLPersonMsgSchema.getApplyAmnt();
		this.GetLimit = aLLPersonMsgSchema.getGetLimit();
		this.AccountPay = aLLPersonMsgSchema.getAccountPay();
		this.PlanFee = aLLPersonMsgSchema.getPlanFee();
		this.MidAmnt = aLLPersonMsgSchema.getMidAmnt();
		this.SupInHosFee = aLLPersonMsgSchema.getSupInHosFee();
		this.HighAmnt1 = aLLPersonMsgSchema.getHighAmnt1();
		this.SelfPay1 = aLLPersonMsgSchema.getSelfPay1();
		this.SelfPay2 = aLLPersonMsgSchema.getSelfPay2();
		this.SelfAmnt = aLLPersonMsgSchema.getSelfAmnt();
		this.OtherOrganAmnt = aLLPersonMsgSchema.getOtherOrganAmnt();
		this.DiseaseName = aLLPersonMsgSchema.getDiseaseName();
		this.DiseaseCode = aLLPersonMsgSchema.getDiseaseCode();
		this.AccDate = fDate.getDate( aLLPersonMsgSchema.getAccDate());
		this.AffixNo = aLLPersonMsgSchema.getAffixNo();
		this.OriginFlag = aLLPersonMsgSchema.getOriginFlag();
		this.GetDutyCode = aLLPersonMsgSchema.getGetDutyCode();
		this.BankCode = aLLPersonMsgSchema.getBankCode();
		this.BankName = aLLPersonMsgSchema.getBankName();
		this.AccountName = aLLPersonMsgSchema.getAccountName();
		this.Account = aLLPersonMsgSchema.getAccount();
		this.GetDutyKind = aLLPersonMsgSchema.getGetDutyKind();
		this.CustomerNo = aLLPersonMsgSchema.getCustomerNo();
		this.InpatientNo = aLLPersonMsgSchema.getInpatientNo();
		this.MakeDate = fDate.getDate( aLLPersonMsgSchema.getMakeDate());
		this.MobilePhone = aLLPersonMsgSchema.getMobilePhone();
		this.RefuseAmnt = aLLPersonMsgSchema.getRefuseAmnt();
		this.Reimbursement = aLLPersonMsgSchema.getReimbursement();
		this.Remark = aLLPersonMsgSchema.getRemark();
		this.PhoneNumber = aLLPersonMsgSchema.getPhoneNumber();
		this.EMail = aLLPersonMsgSchema.getEMail();
		this.ReceiveMethod = aLLPersonMsgSchema.getReceiveMethod();
		this.SignBank = aLLPersonMsgSchema.getSignBank();
		this.DeclareAmount = aLLPersonMsgSchema.getDeclareAmount();
		this.AdvanceFlag = aLLPersonMsgSchema.getAdvanceFlag();
		this.Drawer = aLLPersonMsgSchema.getDrawer();
		this.DrawerID = aLLPersonMsgSchema.getDrawerID();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("Name") == null )
				this.Name = null;
			else
				this.Name = rs.getString("Name").trim();

			if( rs.getString("ID") == null )
				this.ID = null;
			else
				this.ID = rs.getString("ID").trim();

			if( rs.getString("SecurityNo") == null )
				this.SecurityNo = null;
			else
				this.SecurityNo = rs.getString("SecurityNo").trim();

			if( rs.getString("InsuredStat") == null )
				this.InsuredStat = null;
			else
				this.InsuredStat = rs.getString("InsuredStat").trim();

			if( rs.getString("Sex") == null )
				this.Sex = null;
			else
				this.Sex = rs.getString("Sex").trim();

			if( rs.getString("HospitalName") == null )
				this.HospitalName = null;
			else
				this.HospitalName = rs.getString("HospitalName").trim();

			if( rs.getString("HospitalCode") == null )
				this.HospitalCode = null;
			else
				this.HospitalCode = rs.getString("HospitalCode").trim();

			if( rs.getString("FeeType") == null )
				this.FeeType = null;
			else
				this.FeeType = rs.getString("FeeType").trim();

			if( rs.getString("ReceiptNo") == null )
				this.ReceiptNo = null;
			else
				this.ReceiptNo = rs.getString("ReceiptNo").trim();

			this.FeeDate = rs.getDate("FeeDate");
			this.HospStartDate = rs.getDate("HospStartDate");
			this.HospEndDate = rs.getDate("HospEndDate");
			this.RealHospDate = rs.getDate("RealHospDate");
			this.ApplyAmnt = rs.getDouble("ApplyAmnt");
			if( rs.getString("GetLimit") == null )
				this.GetLimit = null;
			else
				this.GetLimit = rs.getString("GetLimit").trim();

			if( rs.getString("AccountPay") == null )
				this.AccountPay = null;
			else
				this.AccountPay = rs.getString("AccountPay").trim();

			if( rs.getString("PlanFee") == null )
				this.PlanFee = null;
			else
				this.PlanFee = rs.getString("PlanFee").trim();

			if( rs.getString("MidAmnt") == null )
				this.MidAmnt = null;
			else
				this.MidAmnt = rs.getString("MidAmnt").trim();

			if( rs.getString("SupInHosFee") == null )
				this.SupInHosFee = null;
			else
				this.SupInHosFee = rs.getString("SupInHosFee").trim();

			if( rs.getString("HighAmnt1") == null )
				this.HighAmnt1 = null;
			else
				this.HighAmnt1 = rs.getString("HighAmnt1").trim();

			if( rs.getString("SelfPay1") == null )
				this.SelfPay1 = null;
			else
				this.SelfPay1 = rs.getString("SelfPay1").trim();

			if( rs.getString("SelfPay2") == null )
				this.SelfPay2 = null;
			else
				this.SelfPay2 = rs.getString("SelfPay2").trim();

			if( rs.getString("SelfAmnt") == null )
				this.SelfAmnt = null;
			else
				this.SelfAmnt = rs.getString("SelfAmnt").trim();

			if( rs.getString("OtherOrganAmnt") == null )
				this.OtherOrganAmnt = null;
			else
				this.OtherOrganAmnt = rs.getString("OtherOrganAmnt").trim();

			if( rs.getString("DiseaseName") == null )
				this.DiseaseName = null;
			else
				this.DiseaseName = rs.getString("DiseaseName").trim();

			if( rs.getString("DiseaseCode") == null )
				this.DiseaseCode = null;
			else
				this.DiseaseCode = rs.getString("DiseaseCode").trim();

			this.AccDate = rs.getDate("AccDate");
			if( rs.getString("AffixNo") == null )
				this.AffixNo = null;
			else
				this.AffixNo = rs.getString("AffixNo").trim();

			if( rs.getString("OriginFlag") == null )
				this.OriginFlag = null;
			else
				this.OriginFlag = rs.getString("OriginFlag").trim();

			if( rs.getString("GetDutyCode") == null )
				this.GetDutyCode = null;
			else
				this.GetDutyCode = rs.getString("GetDutyCode").trim();

			if( rs.getString("BankCode") == null )
				this.BankCode = null;
			else
				this.BankCode = rs.getString("BankCode").trim();

			if( rs.getString("BankName") == null )
				this.BankName = null;
			else
				this.BankName = rs.getString("BankName").trim();

			if( rs.getString("AccountName") == null )
				this.AccountName = null;
			else
				this.AccountName = rs.getString("AccountName").trim();

			if( rs.getString("Account") == null )
				this.Account = null;
			else
				this.Account = rs.getString("Account").trim();

			if( rs.getString("GetDutyKind") == null )
				this.GetDutyKind = null;
			else
				this.GetDutyKind = rs.getString("GetDutyKind").trim();

			if( rs.getString("CustomerNo") == null )
				this.CustomerNo = null;
			else
				this.CustomerNo = rs.getString("CustomerNo").trim();

			if( rs.getString("InpatientNo") == null )
				this.InpatientNo = null;
			else
				this.InpatientNo = rs.getString("InpatientNo").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MobilePhone") == null )
				this.MobilePhone = null;
			else
				this.MobilePhone = rs.getString("MobilePhone").trim();

			if( rs.getString("RefuseAmnt") == null )
				this.RefuseAmnt = null;
			else
				this.RefuseAmnt = rs.getString("RefuseAmnt").trim();

			if( rs.getString("Reimbursement") == null )
				this.Reimbursement = null;
			else
				this.Reimbursement = rs.getString("Reimbursement").trim();

			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

			if( rs.getString("PhoneNumber") == null )
				this.PhoneNumber = null;
			else
				this.PhoneNumber = rs.getString("PhoneNumber").trim();

			if( rs.getString("EMail") == null )
				this.EMail = null;
			else
				this.EMail = rs.getString("EMail").trim();

			if( rs.getString("ReceiveMethod") == null )
				this.ReceiveMethod = null;
			else
				this.ReceiveMethod = rs.getString("ReceiveMethod").trim();

			if( rs.getString("SignBank") == null )
				this.SignBank = null;
			else
				this.SignBank = rs.getString("SignBank").trim();

			if( rs.getString("DeclareAmount") == null )
				this.DeclareAmount = null;
			else
				this.DeclareAmount = rs.getString("DeclareAmount").trim();

			if( rs.getString("AdvanceFlag") == null )
				this.AdvanceFlag = null;
			else
				this.AdvanceFlag = rs.getString("AdvanceFlag").trim();

			if( rs.getString("Drawer") == null )
				this.Drawer = null;
			else
				this.Drawer = rs.getString("Drawer").trim();

			if( rs.getString("DrawerID") == null )
				this.DrawerID = null;
			else
				this.DrawerID = rs.getString("DrawerID").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LLPersonMsg表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLPersonMsgSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LLPersonMsgSchema getSchema()
	{
		LLPersonMsgSchema aLLPersonMsgSchema = new LLPersonMsgSchema();
		aLLPersonMsgSchema.setSchema(this);
		return aLLPersonMsgSchema;
	}

	public LLPersonMsgDB getDB()
	{
		LLPersonMsgDB aDBOper = new LLPersonMsgDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLPersonMsg描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(Name)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SecurityNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuredStat)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Sex)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HospitalName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HospitalCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FeeType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReceiptNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( FeeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( HospStartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( HospEndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( RealHospDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ApplyAmnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GetLimit)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccountPay)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PlanFee)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MidAmnt)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SupInHosFee)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HighAmnt1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SelfPay1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SelfPay2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SelfAmnt)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OtherOrganAmnt)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DiseaseName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DiseaseCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( AccDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AffixNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OriginFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GetDutyCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccountName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Account)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GetDutyKind)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InpatientNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MobilePhone)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RefuseAmnt)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Reimbursement)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PhoneNumber)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EMail)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReceiveMethod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SignBank)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DeclareAmount)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AdvanceFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Drawer)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DrawerID));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLPersonMsg>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			ID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			SecurityNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			InsuredStat = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			Sex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			HospitalName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			HospitalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			FeeType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			ReceiptNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			FeeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,SysConst.PACKAGESPILTER));
			HospStartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
			HospEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
			RealHospDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,SysConst.PACKAGESPILTER));
			ApplyAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,14,SysConst.PACKAGESPILTER))).doubleValue();
			GetLimit = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			AccountPay = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			PlanFee = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			MidAmnt = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			SupInHosFee = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			HighAmnt1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			SelfPay1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			SelfPay2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			SelfAmnt = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			OtherOrganAmnt = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			DiseaseName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			DiseaseCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			AccDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27,SysConst.PACKAGESPILTER));
			AffixNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			OriginFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			GetDutyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			BankName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
			AccountName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			Account = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			GetDutyKind = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
			InpatientNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38,SysConst.PACKAGESPILTER));
			MobilePhone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
			RefuseAmnt = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
			Reimbursement = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41, SysConst.PACKAGESPILTER );
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
			PhoneNumber = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43, SysConst.PACKAGESPILTER );
			EMail = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44, SysConst.PACKAGESPILTER );
			ReceiveMethod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 45, SysConst.PACKAGESPILTER );
			SignBank = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 46, SysConst.PACKAGESPILTER );
			DeclareAmount = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 47, SysConst.PACKAGESPILTER );
			AdvanceFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 48, SysConst.PACKAGESPILTER );
			Drawer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 49, SysConst.PACKAGESPILTER );
			DrawerID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 50, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLPersonMsgSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("Name"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
		}
		if (FCode.equals("ID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ID));
		}
		if (FCode.equals("SecurityNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SecurityNo));
		}
		if (FCode.equals("InsuredStat"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredStat));
		}
		if (FCode.equals("Sex"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Sex));
		}
		if (FCode.equals("HospitalName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HospitalName));
		}
		if (FCode.equals("HospitalCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HospitalCode));
		}
		if (FCode.equals("FeeType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FeeType));
		}
		if (FCode.equals("ReceiptNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiptNo));
		}
		if (FCode.equals("FeeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getFeeDate()));
		}
		if (FCode.equals("HospStartDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getHospStartDate()));
		}
		if (FCode.equals("HospEndDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getHospEndDate()));
		}
		if (FCode.equals("RealHospDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getRealHospDate()));
		}
		if (FCode.equals("ApplyAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ApplyAmnt));
		}
		if (FCode.equals("GetLimit"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GetLimit));
		}
		if (FCode.equals("AccountPay"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccountPay));
		}
		if (FCode.equals("PlanFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PlanFee));
		}
		if (FCode.equals("MidAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MidAmnt));
		}
		if (FCode.equals("SupInHosFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SupInHosFee));
		}
		if (FCode.equals("HighAmnt1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HighAmnt1));
		}
		if (FCode.equals("SelfPay1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SelfPay1));
		}
		if (FCode.equals("SelfPay2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SelfPay2));
		}
		if (FCode.equals("SelfAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SelfAmnt));
		}
		if (FCode.equals("OtherOrganAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OtherOrganAmnt));
		}
		if (FCode.equals("DiseaseName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DiseaseName));
		}
		if (FCode.equals("DiseaseCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DiseaseCode));
		}
		if (FCode.equals("AccDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getAccDate()));
		}
		if (FCode.equals("AffixNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AffixNo));
		}
		if (FCode.equals("OriginFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OriginFlag));
		}
		if (FCode.equals("GetDutyCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GetDutyCode));
		}
		if (FCode.equals("BankCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
		}
		if (FCode.equals("BankName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankName));
		}
		if (FCode.equals("AccountName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccountName));
		}
		if (FCode.equals("Account"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Account));
		}
		if (FCode.equals("GetDutyKind"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GetDutyKind));
		}
		if (FCode.equals("CustomerNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
		}
		if (FCode.equals("InpatientNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InpatientNo));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MobilePhone"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MobilePhone));
		}
		if (FCode.equals("RefuseAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RefuseAmnt));
		}
		if (FCode.equals("Reimbursement"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Reimbursement));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (FCode.equals("PhoneNumber"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PhoneNumber));
		}
		if (FCode.equals("EMail"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EMail));
		}
		if (FCode.equals("ReceiveMethod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiveMethod));
		}
		if (FCode.equals("SignBank"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SignBank));
		}
		if (FCode.equals("DeclareAmount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DeclareAmount));
		}
		if (FCode.equals("AdvanceFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AdvanceFlag));
		}
		if (FCode.equals("Drawer"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Drawer));
		}
		if (FCode.equals("DrawerID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DrawerID));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(Name);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(ID);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(SecurityNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(InsuredStat);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(Sex);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(HospitalName);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(HospitalCode);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(FeeType);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(ReceiptNo);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getFeeDate()));
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getHospStartDate()));
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getHospEndDate()));
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getRealHospDate()));
				break;
			case 13:
				strFieldValue = String.valueOf(ApplyAmnt);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(GetLimit);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(AccountPay);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(PlanFee);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(MidAmnt);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(SupInHosFee);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(HighAmnt1);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(SelfPay1);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(SelfPay2);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(SelfAmnt);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(OtherOrganAmnt);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(DiseaseName);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(DiseaseCode);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getAccDate()));
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(AffixNo);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(OriginFlag);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(GetDutyCode);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(BankCode);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(BankName);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(AccountName);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(Account);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(GetDutyKind);
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(CustomerNo);
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(InpatientNo);
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(MobilePhone);
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(RefuseAmnt);
				break;
			case 40:
				strFieldValue = StrTool.GBKToUnicode(Reimbursement);
				break;
			case 41:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			case 42:
				strFieldValue = StrTool.GBKToUnicode(PhoneNumber);
				break;
			case 43:
				strFieldValue = StrTool.GBKToUnicode(EMail);
				break;
			case 44:
				strFieldValue = StrTool.GBKToUnicode(ReceiveMethod);
				break;
			case 45:
				strFieldValue = StrTool.GBKToUnicode(SignBank);
				break;
			case 46:
				strFieldValue = StrTool.GBKToUnicode(DeclareAmount);
				break;
			case 47:
				strFieldValue = StrTool.GBKToUnicode(AdvanceFlag);
				break;
			case 48:
				strFieldValue = StrTool.GBKToUnicode(Drawer);
				break;
			case 49:
				strFieldValue = StrTool.GBKToUnicode(DrawerID);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("Name"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Name = FValue.trim();
			}
			else
				Name = null;
		}
		if (FCode.equalsIgnoreCase("ID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ID = FValue.trim();
			}
			else
				ID = null;
		}
		if (FCode.equalsIgnoreCase("SecurityNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SecurityNo = FValue.trim();
			}
			else
				SecurityNo = null;
		}
		if (FCode.equalsIgnoreCase("InsuredStat"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuredStat = FValue.trim();
			}
			else
				InsuredStat = null;
		}
		if (FCode.equalsIgnoreCase("Sex"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Sex = FValue.trim();
			}
			else
				Sex = null;
		}
		if (FCode.equalsIgnoreCase("HospitalName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HospitalName = FValue.trim();
			}
			else
				HospitalName = null;
		}
		if (FCode.equalsIgnoreCase("HospitalCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HospitalCode = FValue.trim();
			}
			else
				HospitalCode = null;
		}
		if (FCode.equalsIgnoreCase("FeeType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FeeType = FValue.trim();
			}
			else
				FeeType = null;
		}
		if (FCode.equalsIgnoreCase("ReceiptNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReceiptNo = FValue.trim();
			}
			else
				ReceiptNo = null;
		}
		if (FCode.equalsIgnoreCase("FeeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				FeeDate = fDate.getDate( FValue );
			}
			else
				FeeDate = null;
		}
		if (FCode.equalsIgnoreCase("HospStartDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				HospStartDate = fDate.getDate( FValue );
			}
			else
				HospStartDate = null;
		}
		if (FCode.equalsIgnoreCase("HospEndDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				HospEndDate = fDate.getDate( FValue );
			}
			else
				HospEndDate = null;
		}
		if (FCode.equalsIgnoreCase("RealHospDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				RealHospDate = fDate.getDate( FValue );
			}
			else
				RealHospDate = null;
		}
		if (FCode.equalsIgnoreCase("ApplyAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ApplyAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("GetLimit"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GetLimit = FValue.trim();
			}
			else
				GetLimit = null;
		}
		if (FCode.equalsIgnoreCase("AccountPay"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccountPay = FValue.trim();
			}
			else
				AccountPay = null;
		}
		if (FCode.equalsIgnoreCase("PlanFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PlanFee = FValue.trim();
			}
			else
				PlanFee = null;
		}
		if (FCode.equalsIgnoreCase("MidAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MidAmnt = FValue.trim();
			}
			else
				MidAmnt = null;
		}
		if (FCode.equalsIgnoreCase("SupInHosFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SupInHosFee = FValue.trim();
			}
			else
				SupInHosFee = null;
		}
		if (FCode.equalsIgnoreCase("HighAmnt1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HighAmnt1 = FValue.trim();
			}
			else
				HighAmnt1 = null;
		}
		if (FCode.equalsIgnoreCase("SelfPay1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SelfPay1 = FValue.trim();
			}
			else
				SelfPay1 = null;
		}
		if (FCode.equalsIgnoreCase("SelfPay2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SelfPay2 = FValue.trim();
			}
			else
				SelfPay2 = null;
		}
		if (FCode.equalsIgnoreCase("SelfAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SelfAmnt = FValue.trim();
			}
			else
				SelfAmnt = null;
		}
		if (FCode.equalsIgnoreCase("OtherOrganAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OtherOrganAmnt = FValue.trim();
			}
			else
				OtherOrganAmnt = null;
		}
		if (FCode.equalsIgnoreCase("DiseaseName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DiseaseName = FValue.trim();
			}
			else
				DiseaseName = null;
		}
		if (FCode.equalsIgnoreCase("DiseaseCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DiseaseCode = FValue.trim();
			}
			else
				DiseaseCode = null;
		}
		if (FCode.equalsIgnoreCase("AccDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				AccDate = fDate.getDate( FValue );
			}
			else
				AccDate = null;
		}
		if (FCode.equalsIgnoreCase("AffixNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AffixNo = FValue.trim();
			}
			else
				AffixNo = null;
		}
		if (FCode.equalsIgnoreCase("OriginFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OriginFlag = FValue.trim();
			}
			else
				OriginFlag = null;
		}
		if (FCode.equalsIgnoreCase("GetDutyCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GetDutyCode = FValue.trim();
			}
			else
				GetDutyCode = null;
		}
		if (FCode.equalsIgnoreCase("BankCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankCode = FValue.trim();
			}
			else
				BankCode = null;
		}
		if (FCode.equalsIgnoreCase("BankName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankName = FValue.trim();
			}
			else
				BankName = null;
		}
		if (FCode.equalsIgnoreCase("AccountName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccountName = FValue.trim();
			}
			else
				AccountName = null;
		}
		if (FCode.equalsIgnoreCase("Account"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Account = FValue.trim();
			}
			else
				Account = null;
		}
		if (FCode.equalsIgnoreCase("GetDutyKind"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GetDutyKind = FValue.trim();
			}
			else
				GetDutyKind = null;
		}
		if (FCode.equalsIgnoreCase("CustomerNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerNo = FValue.trim();
			}
			else
				CustomerNo = null;
		}
		if (FCode.equalsIgnoreCase("InpatientNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InpatientNo = FValue.trim();
			}
			else
				InpatientNo = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MobilePhone"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MobilePhone = FValue.trim();
			}
			else
				MobilePhone = null;
		}
		if (FCode.equalsIgnoreCase("RefuseAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RefuseAmnt = FValue.trim();
			}
			else
				RefuseAmnt = null;
		}
		if (FCode.equalsIgnoreCase("Reimbursement"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Reimbursement = FValue.trim();
			}
			else
				Reimbursement = null;
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		if (FCode.equalsIgnoreCase("PhoneNumber"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PhoneNumber = FValue.trim();
			}
			else
				PhoneNumber = null;
		}
		if (FCode.equalsIgnoreCase("EMail"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EMail = FValue.trim();
			}
			else
				EMail = null;
		}
		if (FCode.equalsIgnoreCase("ReceiveMethod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReceiveMethod = FValue.trim();
			}
			else
				ReceiveMethod = null;
		}
		if (FCode.equalsIgnoreCase("SignBank"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SignBank = FValue.trim();
			}
			else
				SignBank = null;
		}
		if (FCode.equalsIgnoreCase("DeclareAmount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DeclareAmount = FValue.trim();
			}
			else
				DeclareAmount = null;
		}
		if (FCode.equalsIgnoreCase("AdvanceFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AdvanceFlag = FValue.trim();
			}
			else
				AdvanceFlag = null;
		}
		if (FCode.equalsIgnoreCase("Drawer"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Drawer = FValue.trim();
			}
			else
				Drawer = null;
		}
		if (FCode.equalsIgnoreCase("DrawerID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DrawerID = FValue.trim();
			}
			else
				DrawerID = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LLPersonMsgSchema other = (LLPersonMsgSchema)otherObject;
		return
			(Name == null ? other.getName() == null : Name.equals(other.getName()))
			&& (ID == null ? other.getID() == null : ID.equals(other.getID()))
			&& (SecurityNo == null ? other.getSecurityNo() == null : SecurityNo.equals(other.getSecurityNo()))
			&& (InsuredStat == null ? other.getInsuredStat() == null : InsuredStat.equals(other.getInsuredStat()))
			&& (Sex == null ? other.getSex() == null : Sex.equals(other.getSex()))
			&& (HospitalName == null ? other.getHospitalName() == null : HospitalName.equals(other.getHospitalName()))
			&& (HospitalCode == null ? other.getHospitalCode() == null : HospitalCode.equals(other.getHospitalCode()))
			&& (FeeType == null ? other.getFeeType() == null : FeeType.equals(other.getFeeType()))
			&& (ReceiptNo == null ? other.getReceiptNo() == null : ReceiptNo.equals(other.getReceiptNo()))
			&& (FeeDate == null ? other.getFeeDate() == null : fDate.getString(FeeDate).equals(other.getFeeDate()))
			&& (HospStartDate == null ? other.getHospStartDate() == null : fDate.getString(HospStartDate).equals(other.getHospStartDate()))
			&& (HospEndDate == null ? other.getHospEndDate() == null : fDate.getString(HospEndDate).equals(other.getHospEndDate()))
			&& (RealHospDate == null ? other.getRealHospDate() == null : fDate.getString(RealHospDate).equals(other.getRealHospDate()))
			&& ApplyAmnt == other.getApplyAmnt()
			&& (GetLimit == null ? other.getGetLimit() == null : GetLimit.equals(other.getGetLimit()))
			&& (AccountPay == null ? other.getAccountPay() == null : AccountPay.equals(other.getAccountPay()))
			&& (PlanFee == null ? other.getPlanFee() == null : PlanFee.equals(other.getPlanFee()))
			&& (MidAmnt == null ? other.getMidAmnt() == null : MidAmnt.equals(other.getMidAmnt()))
			&& (SupInHosFee == null ? other.getSupInHosFee() == null : SupInHosFee.equals(other.getSupInHosFee()))
			&& (HighAmnt1 == null ? other.getHighAmnt1() == null : HighAmnt1.equals(other.getHighAmnt1()))
			&& (SelfPay1 == null ? other.getSelfPay1() == null : SelfPay1.equals(other.getSelfPay1()))
			&& (SelfPay2 == null ? other.getSelfPay2() == null : SelfPay2.equals(other.getSelfPay2()))
			&& (SelfAmnt == null ? other.getSelfAmnt() == null : SelfAmnt.equals(other.getSelfAmnt()))
			&& (OtherOrganAmnt == null ? other.getOtherOrganAmnt() == null : OtherOrganAmnt.equals(other.getOtherOrganAmnt()))
			&& (DiseaseName == null ? other.getDiseaseName() == null : DiseaseName.equals(other.getDiseaseName()))
			&& (DiseaseCode == null ? other.getDiseaseCode() == null : DiseaseCode.equals(other.getDiseaseCode()))
			&& (AccDate == null ? other.getAccDate() == null : fDate.getString(AccDate).equals(other.getAccDate()))
			&& (AffixNo == null ? other.getAffixNo() == null : AffixNo.equals(other.getAffixNo()))
			&& (OriginFlag == null ? other.getOriginFlag() == null : OriginFlag.equals(other.getOriginFlag()))
			&& (GetDutyCode == null ? other.getGetDutyCode() == null : GetDutyCode.equals(other.getGetDutyCode()))
			&& (BankCode == null ? other.getBankCode() == null : BankCode.equals(other.getBankCode()))
			&& (BankName == null ? other.getBankName() == null : BankName.equals(other.getBankName()))
			&& (AccountName == null ? other.getAccountName() == null : AccountName.equals(other.getAccountName()))
			&& (Account == null ? other.getAccount() == null : Account.equals(other.getAccount()))
			&& (GetDutyKind == null ? other.getGetDutyKind() == null : GetDutyKind.equals(other.getGetDutyKind()))
			&& (CustomerNo == null ? other.getCustomerNo() == null : CustomerNo.equals(other.getCustomerNo()))
			&& (InpatientNo == null ? other.getInpatientNo() == null : InpatientNo.equals(other.getInpatientNo()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MobilePhone == null ? other.getMobilePhone() == null : MobilePhone.equals(other.getMobilePhone()))
			&& (RefuseAmnt == null ? other.getRefuseAmnt() == null : RefuseAmnt.equals(other.getRefuseAmnt()))
			&& (Reimbursement == null ? other.getReimbursement() == null : Reimbursement.equals(other.getReimbursement()))
			&& (Remark == null ? other.getRemark() == null : Remark.equals(other.getRemark()))
			&& (PhoneNumber == null ? other.getPhoneNumber() == null : PhoneNumber.equals(other.getPhoneNumber()))
			&& (EMail == null ? other.getEMail() == null : EMail.equals(other.getEMail()))
			&& (ReceiveMethod == null ? other.getReceiveMethod() == null : ReceiveMethod.equals(other.getReceiveMethod()))
			&& (SignBank == null ? other.getSignBank() == null : SignBank.equals(other.getSignBank()))
			&& (DeclareAmount == null ? other.getDeclareAmount() == null : DeclareAmount.equals(other.getDeclareAmount()))
			&& (AdvanceFlag == null ? other.getAdvanceFlag() == null : AdvanceFlag.equals(other.getAdvanceFlag()))
			&& (Drawer == null ? other.getDrawer() == null : Drawer.equals(other.getDrawer()))
			&& (DrawerID == null ? other.getDrawerID() == null : DrawerID.equals(other.getDrawerID()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("Name") ) {
			return 0;
		}
		if( strFieldName.equals("ID") ) {
			return 1;
		}
		if( strFieldName.equals("SecurityNo") ) {
			return 2;
		}
		if( strFieldName.equals("InsuredStat") ) {
			return 3;
		}
		if( strFieldName.equals("Sex") ) {
			return 4;
		}
		if( strFieldName.equals("HospitalName") ) {
			return 5;
		}
		if( strFieldName.equals("HospitalCode") ) {
			return 6;
		}
		if( strFieldName.equals("FeeType") ) {
			return 7;
		}
		if( strFieldName.equals("ReceiptNo") ) {
			return 8;
		}
		if( strFieldName.equals("FeeDate") ) {
			return 9;
		}
		if( strFieldName.equals("HospStartDate") ) {
			return 10;
		}
		if( strFieldName.equals("HospEndDate") ) {
			return 11;
		}
		if( strFieldName.equals("RealHospDate") ) {
			return 12;
		}
		if( strFieldName.equals("ApplyAmnt") ) {
			return 13;
		}
		if( strFieldName.equals("GetLimit") ) {
			return 14;
		}
		if( strFieldName.equals("AccountPay") ) {
			return 15;
		}
		if( strFieldName.equals("PlanFee") ) {
			return 16;
		}
		if( strFieldName.equals("MidAmnt") ) {
			return 17;
		}
		if( strFieldName.equals("SupInHosFee") ) {
			return 18;
		}
		if( strFieldName.equals("HighAmnt1") ) {
			return 19;
		}
		if( strFieldName.equals("SelfPay1") ) {
			return 20;
		}
		if( strFieldName.equals("SelfPay2") ) {
			return 21;
		}
		if( strFieldName.equals("SelfAmnt") ) {
			return 22;
		}
		if( strFieldName.equals("OtherOrganAmnt") ) {
			return 23;
		}
		if( strFieldName.equals("DiseaseName") ) {
			return 24;
		}
		if( strFieldName.equals("DiseaseCode") ) {
			return 25;
		}
		if( strFieldName.equals("AccDate") ) {
			return 26;
		}
		if( strFieldName.equals("AffixNo") ) {
			return 27;
		}
		if( strFieldName.equals("OriginFlag") ) {
			return 28;
		}
		if( strFieldName.equals("GetDutyCode") ) {
			return 29;
		}
		if( strFieldName.equals("BankCode") ) {
			return 30;
		}
		if( strFieldName.equals("BankName") ) {
			return 31;
		}
		if( strFieldName.equals("AccountName") ) {
			return 32;
		}
		if( strFieldName.equals("Account") ) {
			return 33;
		}
		if( strFieldName.equals("GetDutyKind") ) {
			return 34;
		}
		if( strFieldName.equals("CustomerNo") ) {
			return 35;
		}
		if( strFieldName.equals("InpatientNo") ) {
			return 36;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 37;
		}
		if( strFieldName.equals("MobilePhone") ) {
			return 38;
		}
		if( strFieldName.equals("RefuseAmnt") ) {
			return 39;
		}
		if( strFieldName.equals("Reimbursement") ) {
			return 40;
		}
		if( strFieldName.equals("Remark") ) {
			return 41;
		}
		if( strFieldName.equals("PhoneNumber") ) {
			return 42;
		}
		if( strFieldName.equals("EMail") ) {
			return 43;
		}
		if( strFieldName.equals("ReceiveMethod") ) {
			return 44;
		}
		if( strFieldName.equals("SignBank") ) {
			return 45;
		}
		if( strFieldName.equals("DeclareAmount") ) {
			return 46;
		}
		if( strFieldName.equals("AdvanceFlag") ) {
			return 47;
		}
		if( strFieldName.equals("Drawer") ) {
			return 48;
		}
		if( strFieldName.equals("DrawerID") ) {
			return 49;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "Name";
				break;
			case 1:
				strFieldName = "ID";
				break;
			case 2:
				strFieldName = "SecurityNo";
				break;
			case 3:
				strFieldName = "InsuredStat";
				break;
			case 4:
				strFieldName = "Sex";
				break;
			case 5:
				strFieldName = "HospitalName";
				break;
			case 6:
				strFieldName = "HospitalCode";
				break;
			case 7:
				strFieldName = "FeeType";
				break;
			case 8:
				strFieldName = "ReceiptNo";
				break;
			case 9:
				strFieldName = "FeeDate";
				break;
			case 10:
				strFieldName = "HospStartDate";
				break;
			case 11:
				strFieldName = "HospEndDate";
				break;
			case 12:
				strFieldName = "RealHospDate";
				break;
			case 13:
				strFieldName = "ApplyAmnt";
				break;
			case 14:
				strFieldName = "GetLimit";
				break;
			case 15:
				strFieldName = "AccountPay";
				break;
			case 16:
				strFieldName = "PlanFee";
				break;
			case 17:
				strFieldName = "MidAmnt";
				break;
			case 18:
				strFieldName = "SupInHosFee";
				break;
			case 19:
				strFieldName = "HighAmnt1";
				break;
			case 20:
				strFieldName = "SelfPay1";
				break;
			case 21:
				strFieldName = "SelfPay2";
				break;
			case 22:
				strFieldName = "SelfAmnt";
				break;
			case 23:
				strFieldName = "OtherOrganAmnt";
				break;
			case 24:
				strFieldName = "DiseaseName";
				break;
			case 25:
				strFieldName = "DiseaseCode";
				break;
			case 26:
				strFieldName = "AccDate";
				break;
			case 27:
				strFieldName = "AffixNo";
				break;
			case 28:
				strFieldName = "OriginFlag";
				break;
			case 29:
				strFieldName = "GetDutyCode";
				break;
			case 30:
				strFieldName = "BankCode";
				break;
			case 31:
				strFieldName = "BankName";
				break;
			case 32:
				strFieldName = "AccountName";
				break;
			case 33:
				strFieldName = "Account";
				break;
			case 34:
				strFieldName = "GetDutyKind";
				break;
			case 35:
				strFieldName = "CustomerNo";
				break;
			case 36:
				strFieldName = "InpatientNo";
				break;
			case 37:
				strFieldName = "MakeDate";
				break;
			case 38:
				strFieldName = "MobilePhone";
				break;
			case 39:
				strFieldName = "RefuseAmnt";
				break;
			case 40:
				strFieldName = "Reimbursement";
				break;
			case 41:
				strFieldName = "Remark";
				break;
			case 42:
				strFieldName = "PhoneNumber";
				break;
			case 43:
				strFieldName = "EMail";
				break;
			case 44:
				strFieldName = "ReceiveMethod";
				break;
			case 45:
				strFieldName = "SignBank";
				break;
			case 46:
				strFieldName = "DeclareAmount";
				break;
			case 47:
				strFieldName = "AdvanceFlag";
				break;
			case 48:
				strFieldName = "Drawer";
				break;
			case 49:
				strFieldName = "DrawerID";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("Name") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SecurityNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuredStat") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Sex") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HospitalName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HospitalCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FeeType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReceiptNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FeeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("HospStartDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("HospEndDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("RealHospDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ApplyAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("GetLimit") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccountPay") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PlanFee") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MidAmnt") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SupInHosFee") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HighAmnt1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SelfPay1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SelfPay2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SelfAmnt") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OtherOrganAmnt") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DiseaseName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DiseaseCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("AffixNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OriginFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GetDutyCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccountName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Account") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GetDutyKind") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InpatientNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MobilePhone") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RefuseAmnt") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Reimbursement") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PhoneNumber") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EMail") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReceiveMethod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SignBank") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DeclareAmount") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AdvanceFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Drawer") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DrawerID") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 10:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 12:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 13:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 36:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 37:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 38:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 39:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 40:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 41:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 42:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 43:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 44:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 45:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 46:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 47:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 48:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 49:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
