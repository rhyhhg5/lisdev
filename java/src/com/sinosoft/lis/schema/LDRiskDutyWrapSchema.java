/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LDRiskDutyWrapDB;

/*
 * <p>ClassName: LDRiskDutyWrapSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2007-03-13
 */
public class LDRiskDutyWrapSchema implements Schema, Cloneable {
    // @Field
    /** 套餐编码 */
    private String RiskWrapCode;
    /** 险种编码 */
    private String RiskCode;
    /** 责任名称 */
    private String DutyCode;
    /** 计划要素 */
    private String CalFactor;
    /** 计划要素类型 */
    private String CalFactorType;
    /** 计划要素值 */
    private String CalFactorValue;
    /** 计划要素名称 */
    private String CalFactorName;
    /** 主险编码 */
    private String MainRiskCode;
    /** 可选属性 */
    private String ChooseFlag;
    /** 计算sql */
    private String CalSql;
    /** 排序 */
    private int Order;
    /** 要素使用函数 */
    private String Function;
    /** 险种要素标记 */
    private String FactorType;

    public static final int FIELDNUM = 13; // 数据库表的字段个数

    private static String[] PK; // 主键

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LDRiskDutyWrapSchema() {
        mErrors = new CErrors();

        String[] pk = new String[4];
        pk[0] = "RiskWrapCode";
        pk[1] = "RiskCode";
        pk[2] = "DutyCode";
        pk[3] = "CalFactor";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LDRiskDutyWrapSchema cloned = (LDRiskDutyWrapSchema)super.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getRiskWrapCode() {
        return RiskWrapCode;
    }

    public void setRiskWrapCode(String aRiskWrapCode) {
        RiskWrapCode = aRiskWrapCode;
    }

    public String getRiskCode() {
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }

    public String getDutyCode() {
        return DutyCode;
    }

    public void setDutyCode(String aDutyCode) {
        DutyCode = aDutyCode;
    }

    public String getCalFactor() {
        return CalFactor;
    }

    public void setCalFactor(String aCalFactor) {
        CalFactor = aCalFactor;
    }

    public String getCalFactorType() {
        return CalFactorType;
    }

    public void setCalFactorType(String aCalFactorType) {
        CalFactorType = aCalFactorType;
    }

    public String getCalFactorValue() {
        return CalFactorValue;
    }

    public void setCalFactorValue(String aCalFactorValue) {
        CalFactorValue = aCalFactorValue;
    }

    public String getCalFactorName() {
        return CalFactorName;
    }

    public void setCalFactorName(String aCalFactorName) {
        CalFactorName = aCalFactorName;
    }

    public String getMainRiskCode() {
        return MainRiskCode;
    }

    public void setMainRiskCode(String aMainRiskCode) {
        MainRiskCode = aMainRiskCode;
    }

    public String getChooseFlag() {
        return ChooseFlag;
    }

    public void setChooseFlag(String aChooseFlag) {
        ChooseFlag = aChooseFlag;
    }

    public String getCalSql() {
        return CalSql;
    }

    public void setCalSql(String aCalSql) {
        CalSql = aCalSql;
    }

    public int getOrder() {
        return Order;
    }

    public void setOrder(int aOrder) {
        Order = aOrder;
    }

    public void setOrder(String aOrder) {
        if (aOrder != null && !aOrder.equals("")) {
            Integer tInteger = new Integer(aOrder);
            int i = tInteger.intValue();
            Order = i;
        }
    }

    public String getFunction() {
        return Function;
    }

    public void setFunction(String aFunction) {
        Function = aFunction;
    }

    public String getFactorType() {
        return FactorType;
    }

    public void setFactorType(String aFactorType) {
        FactorType = aFactorType;
    }

    /**
     * 使用另外一个 LDRiskDutyWrapSchema 对象给 Schema 赋值
     * @param: aLDRiskDutyWrapSchema LDRiskDutyWrapSchema
     **/
    public void setSchema(LDRiskDutyWrapSchema aLDRiskDutyWrapSchema) {
        this.RiskWrapCode = aLDRiskDutyWrapSchema.getRiskWrapCode();
        this.RiskCode = aLDRiskDutyWrapSchema.getRiskCode();
        this.DutyCode = aLDRiskDutyWrapSchema.getDutyCode();
        this.CalFactor = aLDRiskDutyWrapSchema.getCalFactor();
        this.CalFactorType = aLDRiskDutyWrapSchema.getCalFactorType();
        this.CalFactorValue = aLDRiskDutyWrapSchema.getCalFactorValue();
        this.CalFactorName = aLDRiskDutyWrapSchema.getCalFactorName();
        this.MainRiskCode = aLDRiskDutyWrapSchema.getMainRiskCode();
        this.ChooseFlag = aLDRiskDutyWrapSchema.getChooseFlag();
        this.CalSql = aLDRiskDutyWrapSchema.getCalSql();
        this.Order = aLDRiskDutyWrapSchema.getOrder();
        this.Function = aLDRiskDutyWrapSchema.getFunction();
        this.FactorType = aLDRiskDutyWrapSchema.getFactorType();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("RiskWrapCode") == null) {
                this.RiskWrapCode = null;
            } else {
                this.RiskWrapCode = rs.getString("RiskWrapCode").trim();
            }

            if (rs.getString("RiskCode") == null) {
                this.RiskCode = null;
            } else {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("DutyCode") == null) {
                this.DutyCode = null;
            } else {
                this.DutyCode = rs.getString("DutyCode").trim();
            }

            if (rs.getString("CalFactor") == null) {
                this.CalFactor = null;
            } else {
                this.CalFactor = rs.getString("CalFactor").trim();
            }

            if (rs.getString("CalFactorType") == null) {
                this.CalFactorType = null;
            } else {
                this.CalFactorType = rs.getString("CalFactorType").trim();
            }

            if (rs.getString("CalFactorValue") == null) {
                this.CalFactorValue = null;
            } else {
                this.CalFactorValue = rs.getString("CalFactorValue").trim();
            }

            if (rs.getString("CalFactorName") == null) {
                this.CalFactorName = null;
            } else {
                this.CalFactorName = rs.getString("CalFactorName").trim();
            }

            if (rs.getString("MainRiskCode") == null) {
                this.MainRiskCode = null;
            } else {
                this.MainRiskCode = rs.getString("MainRiskCode").trim();
            }

            if (rs.getString("ChooseFlag") == null) {
                this.ChooseFlag = null;
            } else {
                this.ChooseFlag = rs.getString("ChooseFlag").trim();
            }

            if (rs.getString("CalSql") == null) {
                this.CalSql = null;
            } else {
                this.CalSql = rs.getString("CalSql").trim();
            }

            this.Order = rs.getInt("Order");
            if (rs.getString("Function") == null) {
                this.Function = null;
            } else {
                this.Function = rs.getString("Function").trim();
            }

            if (rs.getString("FactorType") == null) {
                this.FactorType = null;
            } else {
                this.FactorType = rs.getString("FactorType").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LDRiskDutyWrap表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDRiskDutyWrapSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LDRiskDutyWrapSchema getSchema() {
        LDRiskDutyWrapSchema aLDRiskDutyWrapSchema = new LDRiskDutyWrapSchema();
        aLDRiskDutyWrapSchema.setSchema(this);
        return aLDRiskDutyWrapSchema;
    }

    public LDRiskDutyWrapDB getDB() {
        LDRiskDutyWrapDB aDBOper = new LDRiskDutyWrapDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDRiskDutyWrap描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(RiskWrapCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DutyCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CalFactor));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CalFactorType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CalFactorValue));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CalFactorName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MainRiskCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ChooseFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CalSql));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Order));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Function));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FactorType));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDRiskDutyWrap>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            RiskWrapCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                          SysConst.PACKAGESPILTER);
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            DutyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            CalFactor = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                       SysConst.PACKAGESPILTER);
            CalFactorType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                           SysConst.PACKAGESPILTER);
            CalFactorValue = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                            SysConst.PACKAGESPILTER);
            CalFactorName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                           SysConst.PACKAGESPILTER);
            MainRiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                          SysConst.PACKAGESPILTER);
            ChooseFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                        SysConst.PACKAGESPILTER);
            CalSql = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                    SysConst.PACKAGESPILTER);
            Order = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    11, SysConst.PACKAGESPILTER))).intValue();
            Function = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                      SysConst.PACKAGESPILTER);
            FactorType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                        SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDRiskDutyWrapSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("RiskWrapCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskWrapCode));
        }
        if (FCode.equals("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equals("DutyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DutyCode));
        }
        if (FCode.equals("CalFactor")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalFactor));
        }
        if (FCode.equals("CalFactorType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalFactorType));
        }
        if (FCode.equals("CalFactorValue")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalFactorValue));
        }
        if (FCode.equals("CalFactorName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalFactorName));
        }
        if (FCode.equals("MainRiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MainRiskCode));
        }
        if (FCode.equals("ChooseFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChooseFlag));
        }
        if (FCode.equals("CalSql")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalSql));
        }
        if (FCode.equals("Order")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Order));
        }
        if (FCode.equals("Function")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Function));
        }
        if (FCode.equals("FactorType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FactorType));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(RiskWrapCode);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(RiskCode);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(DutyCode);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(CalFactor);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(CalFactorType);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(CalFactorValue);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(CalFactorName);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(MainRiskCode);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(ChooseFlag);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(CalSql);
            break;
        case 10:
            strFieldValue = String.valueOf(Order);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(Function);
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(FactorType);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("RiskWrapCode")) {
            if (FValue != null && !FValue.equals("")) {
                RiskWrapCode = FValue.trim();
            } else {
                RiskWrapCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if (FValue != null && !FValue.equals("")) {
                RiskCode = FValue.trim();
            } else {
                RiskCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("DutyCode")) {
            if (FValue != null && !FValue.equals("")) {
                DutyCode = FValue.trim();
            } else {
                DutyCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("CalFactor")) {
            if (FValue != null && !FValue.equals("")) {
                CalFactor = FValue.trim();
            } else {
                CalFactor = null;
            }
        }
        if (FCode.equalsIgnoreCase("CalFactorType")) {
            if (FValue != null && !FValue.equals("")) {
                CalFactorType = FValue.trim();
            } else {
                CalFactorType = null;
            }
        }
        if (FCode.equalsIgnoreCase("CalFactorValue")) {
            if (FValue != null && !FValue.equals("")) {
                CalFactorValue = FValue.trim();
            } else {
                CalFactorValue = null;
            }
        }
        if (FCode.equalsIgnoreCase("CalFactorName")) {
            if (FValue != null && !FValue.equals("")) {
                CalFactorName = FValue.trim();
            } else {
                CalFactorName = null;
            }
        }
        if (FCode.equalsIgnoreCase("MainRiskCode")) {
            if (FValue != null && !FValue.equals("")) {
                MainRiskCode = FValue.trim();
            } else {
                MainRiskCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("ChooseFlag")) {
            if (FValue != null && !FValue.equals("")) {
                ChooseFlag = FValue.trim();
            } else {
                ChooseFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("CalSql")) {
            if (FValue != null && !FValue.equals("")) {
                CalSql = FValue.trim();
            } else {
                CalSql = null;
            }
        }
        if (FCode.equalsIgnoreCase("Order")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                Order = i;
            }
        }
        if (FCode.equalsIgnoreCase("Function")) {
            if (FValue != null && !FValue.equals("")) {
                Function = FValue.trim();
            } else {
                Function = null;
            }
        }
        if (FCode.equalsIgnoreCase("FactorType")) {
            if (FValue != null && !FValue.equals("")) {
                FactorType = FValue.trim();
            } else {
                FactorType = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LDRiskDutyWrapSchema other = (LDRiskDutyWrapSchema) otherObject;
        return
                RiskWrapCode.equals(other.getRiskWrapCode())
                && RiskCode.equals(other.getRiskCode())
                && DutyCode.equals(other.getDutyCode())
                && CalFactor.equals(other.getCalFactor())
                && CalFactorType.equals(other.getCalFactorType())
                && CalFactorValue.equals(other.getCalFactorValue())
                && CalFactorName.equals(other.getCalFactorName())
                && MainRiskCode.equals(other.getMainRiskCode())
                && ChooseFlag.equals(other.getChooseFlag())
                && CalSql.equals(other.getCalSql())
                && Order == other.getOrder()
                && Function.equals(other.getFunction())
                && FactorType.equals(other.getFactorType());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("RiskWrapCode")) {
            return 0;
        }
        if (strFieldName.equals("RiskCode")) {
            return 1;
        }
        if (strFieldName.equals("DutyCode")) {
            return 2;
        }
        if (strFieldName.equals("CalFactor")) {
            return 3;
        }
        if (strFieldName.equals("CalFactorType")) {
            return 4;
        }
        if (strFieldName.equals("CalFactorValue")) {
            return 5;
        }
        if (strFieldName.equals("CalFactorName")) {
            return 6;
        }
        if (strFieldName.equals("MainRiskCode")) {
            return 7;
        }
        if (strFieldName.equals("ChooseFlag")) {
            return 8;
        }
        if (strFieldName.equals("CalSql")) {
            return 9;
        }
        if (strFieldName.equals("Order")) {
            return 10;
        }
        if (strFieldName.equals("Function")) {
            return 11;
        }
        if (strFieldName.equals("FactorType")) {
            return 12;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "RiskWrapCode";
            break;
        case 1:
            strFieldName = "RiskCode";
            break;
        case 2:
            strFieldName = "DutyCode";
            break;
        case 3:
            strFieldName = "CalFactor";
            break;
        case 4:
            strFieldName = "CalFactorType";
            break;
        case 5:
            strFieldName = "CalFactorValue";
            break;
        case 6:
            strFieldName = "CalFactorName";
            break;
        case 7:
            strFieldName = "MainRiskCode";
            break;
        case 8:
            strFieldName = "ChooseFlag";
            break;
        case 9:
            strFieldName = "CalSql";
            break;
        case 10:
            strFieldName = "Order";
            break;
        case 11:
            strFieldName = "Function";
            break;
        case 12:
            strFieldName = "FactorType";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("RiskWrapCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DutyCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CalFactor")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CalFactorType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CalFactorValue")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CalFactorName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MainRiskCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ChooseFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CalSql")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Order")) {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("Function")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FactorType")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_INT;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 12:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
