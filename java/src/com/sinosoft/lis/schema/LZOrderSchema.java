/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LZOrderDB;

/*
 * <p>ClassName: LZOrderSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 单证管理
 * @CreateDate：2006-07-21
 */
public class LZOrderSchema implements Schema, Cloneable {
    // @Field
    /** 批次号 */
    private String SerialNo;
    /** 截止日期 */
    private Date EndDate;
    /** 订单状态 */
    private String State;
    /** 订单制定人 */
    private String DescPerson;
    /** 制定机构 */
    private String DescCom;
    /** 归属日期 */
    private Date AttachDate;
    /** 订单总金额金额 */
    private double SumOrderMoney;
    /** 授权人 */
    private String ConfirmPerson;
    /** 授权机构 */
    private String ConfirmCom;
    /** 授权日期 */
    private Date ConfirmDate;
    /** 注释 */
    private String Note;
    /** 操作人 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 修改日期 */
    private Date ModifyDate;
    /** 修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 16; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LZOrderSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "SerialNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LZOrderSchema cloned = (LZOrderSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getSerialNo() {
        return SerialNo;
    }

    public void setSerialNo(String aSerialNo) {
        SerialNo = aSerialNo;
    }

    public String getEndDate() {
        if (EndDate != null) {
            return fDate.getString(EndDate);
        } else {
            return null;
        }
    }

    public void setEndDate(Date aEndDate) {
        EndDate = aEndDate;
    }

    public void setEndDate(String aEndDate) {
        if (aEndDate != null && !aEndDate.equals("")) {
            EndDate = fDate.getDate(aEndDate);
        } else {
            EndDate = null;
        }
    }

    public String getState() {
        return State;
    }

    public void setState(String aState) {
        State = aState;
    }

    public String getDescPerson() {
        return DescPerson;
    }

    public void setDescPerson(String aDescPerson) {
        DescPerson = aDescPerson;
    }

    public String getDescCom() {
        return DescCom;
    }

    public void setDescCom(String aDescCom) {
        DescCom = aDescCom;
    }

    public String getAttachDate() {
        if (AttachDate != null) {
            return fDate.getString(AttachDate);
        } else {
            return null;
        }
    }

    public void setAttachDate(Date aAttachDate) {
        AttachDate = aAttachDate;
    }

    public void setAttachDate(String aAttachDate) {
        if (aAttachDate != null && !aAttachDate.equals("")) {
            AttachDate = fDate.getDate(aAttachDate);
        } else {
            AttachDate = null;
        }
    }

    public double getSumOrderMoney() {
        return SumOrderMoney;
    }

    public void setSumOrderMoney(double aSumOrderMoney) {
        SumOrderMoney = Arith.round(aSumOrderMoney, 2);
    }

    public void setSumOrderMoney(String aSumOrderMoney) {
        if (aSumOrderMoney != null && !aSumOrderMoney.equals("")) {
            Double tDouble = new Double(aSumOrderMoney);
            double d = tDouble.doubleValue();
            SumOrderMoney = Arith.round(d, 2);
        }
    }

    public String getConfirmPerson() {
        return ConfirmPerson;
    }

    public void setConfirmPerson(String aConfirmPerson) {
        ConfirmPerson = aConfirmPerson;
    }

    public String getConfirmCom() {
        return ConfirmCom;
    }

    public void setConfirmCom(String aConfirmCom) {
        ConfirmCom = aConfirmCom;
    }

    public String getConfirmDate() {
        if (ConfirmDate != null) {
            return fDate.getString(ConfirmDate);
        } else {
            return null;
        }
    }

    public void setConfirmDate(Date aConfirmDate) {
        ConfirmDate = aConfirmDate;
    }

    public void setConfirmDate(String aConfirmDate) {
        if (aConfirmDate != null && !aConfirmDate.equals("")) {
            ConfirmDate = fDate.getDate(aConfirmDate);
        } else {
            ConfirmDate = null;
        }
    }

    public String getNote() {
        return Note;
    }

    public void setNote(String aNote) {
        Note = aNote;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LZOrderSchema 对象给 Schema 赋值
     * @param: aLZOrderSchema LZOrderSchema
     **/
    public void setSchema(LZOrderSchema aLZOrderSchema) {
        this.SerialNo = aLZOrderSchema.getSerialNo();
        this.EndDate = fDate.getDate(aLZOrderSchema.getEndDate());
        this.State = aLZOrderSchema.getState();
        this.DescPerson = aLZOrderSchema.getDescPerson();
        this.DescCom = aLZOrderSchema.getDescCom();
        this.AttachDate = fDate.getDate(aLZOrderSchema.getAttachDate());
        this.SumOrderMoney = aLZOrderSchema.getSumOrderMoney();
        this.ConfirmPerson = aLZOrderSchema.getConfirmPerson();
        this.ConfirmCom = aLZOrderSchema.getConfirmCom();
        this.ConfirmDate = fDate.getDate(aLZOrderSchema.getConfirmDate());
        this.Note = aLZOrderSchema.getNote();
        this.Operator = aLZOrderSchema.getOperator();
        this.MakeDate = fDate.getDate(aLZOrderSchema.getMakeDate());
        this.MakeTime = aLZOrderSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLZOrderSchema.getModifyDate());
        this.ModifyTime = aLZOrderSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("SerialNo") == null) {
                this.SerialNo = null;
            } else {
                this.SerialNo = rs.getString("SerialNo").trim();
            }

            this.EndDate = rs.getDate("EndDate");
            if (rs.getString("State") == null) {
                this.State = null;
            } else {
                this.State = rs.getString("State").trim();
            }

            if (rs.getString("DescPerson") == null) {
                this.DescPerson = null;
            } else {
                this.DescPerson = rs.getString("DescPerson").trim();
            }

            if (rs.getString("DescCom") == null) {
                this.DescCom = null;
            } else {
                this.DescCom = rs.getString("DescCom").trim();
            }

            this.AttachDate = rs.getDate("AttachDate");
            this.SumOrderMoney = rs.getDouble("SumOrderMoney");
            if (rs.getString("ConfirmPerson") == null) {
                this.ConfirmPerson = null;
            } else {
                this.ConfirmPerson = rs.getString("ConfirmPerson").trim();
            }

            if (rs.getString("ConfirmCom") == null) {
                this.ConfirmCom = null;
            } else {
                this.ConfirmCom = rs.getString("ConfirmCom").trim();
            }

            this.ConfirmDate = rs.getDate("ConfirmDate");
            if (rs.getString("Note") == null) {
                this.Note = null;
            } else {
                this.Note = rs.getString("Note").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LZOrder表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LZOrderSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LZOrderSchema getSchema() {
        LZOrderSchema aLZOrderSchema = new LZOrderSchema();
        aLZOrderSchema.setSchema(this);
        return aLZOrderSchema;
    }

    public LZOrderDB getDB() {
        LZOrderDB aDBOper = new LZOrderDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLZOrder描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(SerialNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(EndDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(State));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DescPerson));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DescCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(AttachDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SumOrderMoney));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ConfirmPerson));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ConfirmCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ConfirmDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Note));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLZOrder>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            EndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 2, SysConst.PACKAGESPILTER));
            State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                   SysConst.PACKAGESPILTER);
            DescPerson = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                        SysConst.PACKAGESPILTER);
            DescCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                     SysConst.PACKAGESPILTER);
            AttachDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 6, SysConst.PACKAGESPILTER));
            SumOrderMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 7, SysConst.PACKAGESPILTER))).doubleValue();
            ConfirmPerson = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                           SysConst.PACKAGESPILTER);
            ConfirmCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                        SysConst.PACKAGESPILTER);
            ConfirmDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 10, SysConst.PACKAGESPILTER));
            Note = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                  SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 13, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 15, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                        SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LZOrderSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equals("EndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getEndDate()));
        }
        if (FCode.equals("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equals("DescPerson")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DescPerson));
        }
        if (FCode.equals("DescCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DescCom));
        }
        if (FCode.equals("AttachDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getAttachDate()));
        }
        if (FCode.equals("SumOrderMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumOrderMoney));
        }
        if (FCode.equals("ConfirmPerson")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ConfirmPerson));
        }
        if (FCode.equals("ConfirmCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ConfirmCom));
        }
        if (FCode.equals("ConfirmDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getConfirmDate()));
        }
        if (FCode.equals("Note")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Note));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(SerialNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.getEndDate()));
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(State);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(DescPerson);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(DescCom);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getAttachDate()));
            break;
        case 6:
            strFieldValue = String.valueOf(SumOrderMoney);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(ConfirmPerson);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(ConfirmCom);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getConfirmDate()));
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(Note);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 15:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("SerialNo")) {
            if (FValue != null && !FValue.equals("")) {
                SerialNo = FValue.trim();
            } else {
                SerialNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            if (FValue != null && !FValue.equals("")) {
                EndDate = fDate.getDate(FValue);
            } else {
                EndDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("State")) {
            if (FValue != null && !FValue.equals("")) {
                State = FValue.trim();
            } else {
                State = null;
            }
        }
        if (FCode.equalsIgnoreCase("DescPerson")) {
            if (FValue != null && !FValue.equals("")) {
                DescPerson = FValue.trim();
            } else {
                DescPerson = null;
            }
        }
        if (FCode.equalsIgnoreCase("DescCom")) {
            if (FValue != null && !FValue.equals("")) {
                DescCom = FValue.trim();
            } else {
                DescCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("AttachDate")) {
            if (FValue != null && !FValue.equals("")) {
                AttachDate = fDate.getDate(FValue);
            } else {
                AttachDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("SumOrderMoney")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                SumOrderMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("ConfirmPerson")) {
            if (FValue != null && !FValue.equals("")) {
                ConfirmPerson = FValue.trim();
            } else {
                ConfirmPerson = null;
            }
        }
        if (FCode.equalsIgnoreCase("ConfirmCom")) {
            if (FValue != null && !FValue.equals("")) {
                ConfirmCom = FValue.trim();
            } else {
                ConfirmCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("ConfirmDate")) {
            if (FValue != null && !FValue.equals("")) {
                ConfirmDate = fDate.getDate(FValue);
            } else {
                ConfirmDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("Note")) {
            if (FValue != null && !FValue.equals("")) {
                Note = FValue.trim();
            } else {
                Note = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LZOrderSchema other = (LZOrderSchema) otherObject;
        return
                SerialNo.equals(other.getSerialNo())
                && fDate.getString(EndDate).equals(other.getEndDate())
                && State.equals(other.getState())
                && DescPerson.equals(other.getDescPerson())
                && DescCom.equals(other.getDescCom())
                && fDate.getString(AttachDate).equals(other.getAttachDate())
                && SumOrderMoney == other.getSumOrderMoney()
                && ConfirmPerson.equals(other.getConfirmPerson())
                && ConfirmCom.equals(other.getConfirmCom())
                && fDate.getString(ConfirmDate).equals(other.getConfirmDate())
                && Note.equals(other.getNote())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("SerialNo")) {
            return 0;
        }
        if (strFieldName.equals("EndDate")) {
            return 1;
        }
        if (strFieldName.equals("State")) {
            return 2;
        }
        if (strFieldName.equals("DescPerson")) {
            return 3;
        }
        if (strFieldName.equals("DescCom")) {
            return 4;
        }
        if (strFieldName.equals("AttachDate")) {
            return 5;
        }
        if (strFieldName.equals("SumOrderMoney")) {
            return 6;
        }
        if (strFieldName.equals("ConfirmPerson")) {
            return 7;
        }
        if (strFieldName.equals("ConfirmCom")) {
            return 8;
        }
        if (strFieldName.equals("ConfirmDate")) {
            return 9;
        }
        if (strFieldName.equals("Note")) {
            return 10;
        }
        if (strFieldName.equals("Operator")) {
            return 11;
        }
        if (strFieldName.equals("MakeDate")) {
            return 12;
        }
        if (strFieldName.equals("MakeTime")) {
            return 13;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 14;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 15;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "SerialNo";
            break;
        case 1:
            strFieldName = "EndDate";
            break;
        case 2:
            strFieldName = "State";
            break;
        case 3:
            strFieldName = "DescPerson";
            break;
        case 4:
            strFieldName = "DescCom";
            break;
        case 5:
            strFieldName = "AttachDate";
            break;
        case 6:
            strFieldName = "SumOrderMoney";
            break;
        case 7:
            strFieldName = "ConfirmPerson";
            break;
        case 8:
            strFieldName = "ConfirmCom";
            break;
        case 9:
            strFieldName = "ConfirmDate";
            break;
        case 10:
            strFieldName = "Note";
            break;
        case 11:
            strFieldName = "Operator";
            break;
        case 12:
            strFieldName = "MakeDate";
            break;
        case 13:
            strFieldName = "MakeTime";
            break;
        case 14:
            strFieldName = "ModifyDate";
            break;
        case 15:
            strFieldName = "ModifyTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("SerialNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EndDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("State")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DescPerson")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DescCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AttachDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("SumOrderMoney")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("ConfirmPerson")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ConfirmCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ConfirmDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("Note")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 6:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 12:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 13:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 14:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 15:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
