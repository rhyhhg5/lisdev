/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LJAGetHealthDB;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LJAGetHealthSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PICCH-郭丽颖-新增健管赔付表-20061130
 * @CreateDate：2006-12-01
 */
public class LJAGetHealthSchema implements Schema, Cloneable {
    // @Field
    /** 实付批次号码 */
    private String ActuGetNo;
    /** 其它号码 */
    private String OtherNo;
    /** 其它号码类型 */
    private String OtherNoType;
    /** 团体合同号码 */
    private String GrpContNo;
    /** 合同号码 */
    private String ContNo;
    /** 保单号码 */
    private String PolNo;
    /** 客户号码 */
    private String CustomerNo;
    /** 服务项目号码 */
    private String ServItemNo;
    /** 服务任务号码 */
    private String ServTaskNo;
    /** 险类编码 */
    private String KindCode;
    /** 险种编码 */
    private String RiskCode;
    /** 险种版本 */
    private String RiskVersion;
    /** 销售渠道 */
    private String SaleChnl;
    /** 代理人编码 */
    private String AgentCode;
    /** 代理人组别 */
    private String AgentGroup;
    /** 给付日期 */
    private Date PayDate;
    /** 财务到帐日期 */
    private Date EnterAccDate;
    /** 财务确认日期 */
    private Date ConfDate;
    /** 赔付金额 */
    private double Pay;
    /** 管理机构 */
    private String ManageCom;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 25; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LJAGetHealthSchema() {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "ActuGetNo";
        pk[1] = "OtherNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LJAGetHealthSchema cloned = (LJAGetHealthSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getActuGetNo() {
        return ActuGetNo;
    }

    public void setActuGetNo(String aActuGetNo) {
        ActuGetNo = aActuGetNo;
    }

    public String getOtherNo() {
        return OtherNo;
    }

    public void setOtherNo(String aOtherNo) {
        OtherNo = aOtherNo;
    }

    public String getOtherNoType() {
        return OtherNoType;
    }

    public void setOtherNoType(String aOtherNoType) {
        OtherNoType = aOtherNoType;
    }

    public String getGrpContNo() {
        return GrpContNo;
    }

    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }

    public String getPolNo() {
        return PolNo;
    }

    public void setPolNo(String aPolNo) {
        PolNo = aPolNo;
    }

    public String getCustomerNo() {
        return CustomerNo;
    }

    public void setCustomerNo(String aCustomerNo) {
        CustomerNo = aCustomerNo;
    }

    public String getServItemNo() {
        return ServItemNo;
    }

    public void setServItemNo(String aServItemNo) {
        ServItemNo = aServItemNo;
    }

    public String getServTaskNo() {
        return ServTaskNo;
    }

    public void setServTaskNo(String aServTaskNo) {
        ServTaskNo = aServTaskNo;
    }

    public String getKindCode() {
        return KindCode;
    }

    public void setKindCode(String aKindCode) {
        KindCode = aKindCode;
    }

    public String getRiskCode() {
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }

    public String getRiskVersion() {
        return RiskVersion;
    }

    public void setRiskVersion(String aRiskVersion) {
        RiskVersion = aRiskVersion;
    }

    public String getSaleChnl() {
        return SaleChnl;
    }

    public void setSaleChnl(String aSaleChnl) {
        SaleChnl = aSaleChnl;
    }

    public String getAgentCode() {
        return AgentCode;
    }

    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }

    public String getAgentGroup() {
        return AgentGroup;
    }

    public void setAgentGroup(String aAgentGroup) {
        AgentGroup = aAgentGroup;
    }

    public String getPayDate() {
        if (PayDate != null) {
            return fDate.getString(PayDate);
        } else {
            return null;
        }
    }

    public void setPayDate(Date aPayDate) {
        PayDate = aPayDate;
    }

    public void setPayDate(String aPayDate) {
        if (aPayDate != null && !aPayDate.equals("")) {
            PayDate = fDate.getDate(aPayDate);
        } else {
            PayDate = null;
        }
    }

    public String getEnterAccDate() {
        if (EnterAccDate != null) {
            return fDate.getString(EnterAccDate);
        } else {
            return null;
        }
    }

    public void setEnterAccDate(Date aEnterAccDate) {
        EnterAccDate = aEnterAccDate;
    }

    public void setEnterAccDate(String aEnterAccDate) {
        if (aEnterAccDate != null && !aEnterAccDate.equals("")) {
            EnterAccDate = fDate.getDate(aEnterAccDate);
        } else {
            EnterAccDate = null;
        }
    }

    public String getConfDate() {
        if (ConfDate != null) {
            return fDate.getString(ConfDate);
        } else {
            return null;
        }
    }

    public void setConfDate(Date aConfDate) {
        ConfDate = aConfDate;
    }

    public void setConfDate(String aConfDate) {
        if (aConfDate != null && !aConfDate.equals("")) {
            ConfDate = fDate.getDate(aConfDate);
        } else {
            ConfDate = null;
        }
    }

    public double getPay() {
        return Pay;
    }

    public void setPay(double aPay) {
        Pay = Arith.round(aPay, 2);
    }

    public void setPay(String aPay) {
        if (aPay != null && !aPay.equals("")) {
            Double tDouble = new Double(aPay);
            double d = tDouble.doubleValue();
            Pay = Arith.round(d, 2);
        }
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LJAGetHealthSchema 对象给 Schema 赋值
     * @param: aLJAGetHealthSchema LJAGetHealthSchema
     **/
    public void setSchema(LJAGetHealthSchema aLJAGetHealthSchema) {
        this.ActuGetNo = aLJAGetHealthSchema.getActuGetNo();
        this.OtherNo = aLJAGetHealthSchema.getOtherNo();
        this.OtherNoType = aLJAGetHealthSchema.getOtherNoType();
        this.GrpContNo = aLJAGetHealthSchema.getGrpContNo();
        this.ContNo = aLJAGetHealthSchema.getContNo();
        this.PolNo = aLJAGetHealthSchema.getPolNo();
        this.CustomerNo = aLJAGetHealthSchema.getCustomerNo();
        this.ServItemNo = aLJAGetHealthSchema.getServItemNo();
        this.ServTaskNo = aLJAGetHealthSchema.getServTaskNo();
        this.KindCode = aLJAGetHealthSchema.getKindCode();
        this.RiskCode = aLJAGetHealthSchema.getRiskCode();
        this.RiskVersion = aLJAGetHealthSchema.getRiskVersion();
        this.SaleChnl = aLJAGetHealthSchema.getSaleChnl();
        this.AgentCode = aLJAGetHealthSchema.getAgentCode();
        this.AgentGroup = aLJAGetHealthSchema.getAgentGroup();
        this.PayDate = fDate.getDate(aLJAGetHealthSchema.getPayDate());
        this.EnterAccDate = fDate.getDate(aLJAGetHealthSchema.getEnterAccDate());
        this.ConfDate = fDate.getDate(aLJAGetHealthSchema.getConfDate());
        this.Pay = aLJAGetHealthSchema.getPay();
        this.ManageCom = aLJAGetHealthSchema.getManageCom();
        this.Operator = aLJAGetHealthSchema.getOperator();
        this.MakeDate = fDate.getDate(aLJAGetHealthSchema.getMakeDate());
        this.MakeTime = aLJAGetHealthSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLJAGetHealthSchema.getModifyDate());
        this.ModifyTime = aLJAGetHealthSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ActuGetNo") == null) {
                this.ActuGetNo = null;
            } else {
                this.ActuGetNo = rs.getString("ActuGetNo").trim();
            }

            if (rs.getString("OtherNo") == null) {
                this.OtherNo = null;
            } else {
                this.OtherNo = rs.getString("OtherNo").trim();
            }

            if (rs.getString("OtherNoType") == null) {
                this.OtherNoType = null;
            } else {
                this.OtherNoType = rs.getString("OtherNoType").trim();
            }

            if (rs.getString("GrpContNo") == null) {
                this.GrpContNo = null;
            } else {
                this.GrpContNo = rs.getString("GrpContNo").trim();
            }

            if (rs.getString("ContNo") == null) {
                this.ContNo = null;
            } else {
                this.ContNo = rs.getString("ContNo").trim();
            }

            if (rs.getString("PolNo") == null) {
                this.PolNo = null;
            } else {
                this.PolNo = rs.getString("PolNo").trim();
            }

            if (rs.getString("CustomerNo") == null) {
                this.CustomerNo = null;
            } else {
                this.CustomerNo = rs.getString("CustomerNo").trim();
            }

            if (rs.getString("ServItemNo") == null) {
                this.ServItemNo = null;
            } else {
                this.ServItemNo = rs.getString("ServItemNo").trim();
            }

            if (rs.getString("ServTaskNo") == null) {
                this.ServTaskNo = null;
            } else {
                this.ServTaskNo = rs.getString("ServTaskNo").trim();
            }

            if (rs.getString("KindCode") == null) {
                this.KindCode = null;
            } else {
                this.KindCode = rs.getString("KindCode").trim();
            }

            if (rs.getString("RiskCode") == null) {
                this.RiskCode = null;
            } else {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("RiskVersion") == null) {
                this.RiskVersion = null;
            } else {
                this.RiskVersion = rs.getString("RiskVersion").trim();
            }

            if (rs.getString("SaleChnl") == null) {
                this.SaleChnl = null;
            } else {
                this.SaleChnl = rs.getString("SaleChnl").trim();
            }

            if (rs.getString("AgentCode") == null) {
                this.AgentCode = null;
            } else {
                this.AgentCode = rs.getString("AgentCode").trim();
            }

            if (rs.getString("AgentGroup") == null) {
                this.AgentGroup = null;
            } else {
                this.AgentGroup = rs.getString("AgentGroup").trim();
            }

            this.PayDate = rs.getDate("PayDate");
            this.EnterAccDate = rs.getDate("EnterAccDate");
            this.ConfDate = rs.getDate("ConfDate");
            this.Pay = rs.getDouble("Pay");
            if (rs.getString("ManageCom") == null) {
                this.ManageCom = null;
            } else {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LJAGetHealth表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJAGetHealthSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LJAGetHealthSchema getSchema() {
        LJAGetHealthSchema aLJAGetHealthSchema = new LJAGetHealthSchema();
        aLJAGetHealthSchema.setSchema(this);
        return aLJAGetHealthSchema;
    }

    public LJAGetHealthDB getDB() {
        LJAGetHealthDB aDBOper = new LJAGetHealthDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLJAGetHealth描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(ActuGetNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OtherNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OtherNoType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpContNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PolNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CustomerNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServItemNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServTaskNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(KindCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskVersion));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SaleChnl));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentGroup));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(PayDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(EnterAccDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ConfDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Pay));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLJAGetHealth>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            ActuGetNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            OtherNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                     SysConst.PACKAGESPILTER);
            OtherNoType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                         SysConst.PACKAGESPILTER);
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                       SysConst.PACKAGESPILTER);
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                    SysConst.PACKAGESPILTER);
            PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                   SysConst.PACKAGESPILTER);
            CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                        SysConst.PACKAGESPILTER);
            ServItemNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                        SysConst.PACKAGESPILTER);
            ServTaskNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                        SysConst.PACKAGESPILTER);
            KindCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                      SysConst.PACKAGESPILTER);
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                      SysConst.PACKAGESPILTER);
            RiskVersion = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                         SysConst.PACKAGESPILTER);
            SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                      SysConst.PACKAGESPILTER);
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                       SysConst.PACKAGESPILTER);
            AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                        SysConst.PACKAGESPILTER);
            PayDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 16, SysConst.PACKAGESPILTER));
            EnterAccDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 17, SysConst.PACKAGESPILTER));
            ConfDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 18, SysConst.PACKAGESPILTER));
            Pay = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    19, SysConst.PACKAGESPILTER))).doubleValue();
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                       SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 22, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 24, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,
                                        SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJAGetHealthSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("ActuGetNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ActuGetNo));
        }
        if (FCode.equals("OtherNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNo));
        }
        if (FCode.equals("OtherNoType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNoType));
        }
        if (FCode.equals("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equals("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equals("PolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
        }
        if (FCode.equals("CustomerNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
        }
        if (FCode.equals("ServItemNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServItemNo));
        }
        if (FCode.equals("ServTaskNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServTaskNo));
        }
        if (FCode.equals("KindCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(KindCode));
        }
        if (FCode.equals("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equals("RiskVersion")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskVersion));
        }
        if (FCode.equals("SaleChnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
        }
        if (FCode.equals("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equals("AgentGroup")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
        }
        if (FCode.equals("PayDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getPayDate()));
        }
        if (FCode.equals("EnterAccDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getEnterAccDate()));
        }
        if (FCode.equals("ConfDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getConfDate()));
        }
        if (FCode.equals("Pay")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Pay));
        }
        if (FCode.equals("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(ActuGetNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(OtherNo);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(OtherNoType);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(GrpContNo);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(ContNo);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(PolNo);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(CustomerNo);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(ServItemNo);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(ServTaskNo);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(KindCode);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(RiskCode);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(RiskVersion);
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(SaleChnl);
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(AgentCode);
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(AgentGroup);
            break;
        case 15:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.getPayDate()));
            break;
        case 16:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getEnterAccDate()));
            break;
        case 17:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getConfDate()));
            break;
        case 18:
            strFieldValue = String.valueOf(Pay);
            break;
        case 19:
            strFieldValue = StrTool.GBKToUnicode(ManageCom);
            break;
        case 20:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 21:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 22:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 23:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 24:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("ActuGetNo")) {
            if (FValue != null && !FValue.equals("")) {
                ActuGetNo = FValue.trim();
            } else {
                ActuGetNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("OtherNo")) {
            if (FValue != null && !FValue.equals("")) {
                OtherNo = FValue.trim();
            } else {
                OtherNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("OtherNoType")) {
            if (FValue != null && !FValue.equals("")) {
                OtherNoType = FValue.trim();
            } else {
                OtherNoType = null;
            }
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if (FValue != null && !FValue.equals("")) {
                GrpContNo = FValue.trim();
            } else {
                GrpContNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if (FValue != null && !FValue.equals("")) {
                ContNo = FValue.trim();
            } else {
                ContNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            if (FValue != null && !FValue.equals("")) {
                PolNo = FValue.trim();
            } else {
                PolNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            if (FValue != null && !FValue.equals("")) {
                CustomerNo = FValue.trim();
            } else {
                CustomerNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServItemNo")) {
            if (FValue != null && !FValue.equals("")) {
                ServItemNo = FValue.trim();
            } else {
                ServItemNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServTaskNo")) {
            if (FValue != null && !FValue.equals("")) {
                ServTaskNo = FValue.trim();
            } else {
                ServTaskNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("KindCode")) {
            if (FValue != null && !FValue.equals("")) {
                KindCode = FValue.trim();
            } else {
                KindCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if (FValue != null && !FValue.equals("")) {
                RiskCode = FValue.trim();
            } else {
                RiskCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("RiskVersion")) {
            if (FValue != null && !FValue.equals("")) {
                RiskVersion = FValue.trim();
            } else {
                RiskVersion = null;
            }
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            if (FValue != null && !FValue.equals("")) {
                SaleChnl = FValue.trim();
            } else {
                SaleChnl = null;
            }
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            if (FValue != null && !FValue.equals("")) {
                AgentCode = FValue.trim();
            } else {
                AgentCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            if (FValue != null && !FValue.equals("")) {
                AgentGroup = FValue.trim();
            } else {
                AgentGroup = null;
            }
        }
        if (FCode.equalsIgnoreCase("PayDate")) {
            if (FValue != null && !FValue.equals("")) {
                PayDate = fDate.getDate(FValue);
            } else {
                PayDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("EnterAccDate")) {
            if (FValue != null && !FValue.equals("")) {
                EnterAccDate = fDate.getDate(FValue);
            } else {
                EnterAccDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ConfDate")) {
            if (FValue != null && !FValue.equals("")) {
                ConfDate = fDate.getDate(FValue);
            } else {
                ConfDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("Pay")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Pay = d;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ManageCom = FValue.trim();
            } else {
                ManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LJAGetHealthSchema other = (LJAGetHealthSchema) otherObject;
        return
                ActuGetNo.equals(other.getActuGetNo())
                && OtherNo.equals(other.getOtherNo())
                && OtherNoType.equals(other.getOtherNoType())
                && GrpContNo.equals(other.getGrpContNo())
                && ContNo.equals(other.getContNo())
                && PolNo.equals(other.getPolNo())
                && CustomerNo.equals(other.getCustomerNo())
                && ServItemNo.equals(other.getServItemNo())
                && ServTaskNo.equals(other.getServTaskNo())
                && KindCode.equals(other.getKindCode())
                && RiskCode.equals(other.getRiskCode())
                && RiskVersion.equals(other.getRiskVersion())
                && SaleChnl.equals(other.getSaleChnl())
                && AgentCode.equals(other.getAgentCode())
                && AgentGroup.equals(other.getAgentGroup())
                && fDate.getString(PayDate).equals(other.getPayDate())
                && fDate.getString(EnterAccDate).equals(other.getEnterAccDate())
                && fDate.getString(ConfDate).equals(other.getConfDate())
                && Pay == other.getPay()
                && ManageCom.equals(other.getManageCom())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("ActuGetNo")) {
            return 0;
        }
        if (strFieldName.equals("OtherNo")) {
            return 1;
        }
        if (strFieldName.equals("OtherNoType")) {
            return 2;
        }
        if (strFieldName.equals("GrpContNo")) {
            return 3;
        }
        if (strFieldName.equals("ContNo")) {
            return 4;
        }
        if (strFieldName.equals("PolNo")) {
            return 5;
        }
        if (strFieldName.equals("CustomerNo")) {
            return 6;
        }
        if (strFieldName.equals("ServItemNo")) {
            return 7;
        }
        if (strFieldName.equals("ServTaskNo")) {
            return 8;
        }
        if (strFieldName.equals("KindCode")) {
            return 9;
        }
        if (strFieldName.equals("RiskCode")) {
            return 10;
        }
        if (strFieldName.equals("RiskVersion")) {
            return 11;
        }
        if (strFieldName.equals("SaleChnl")) {
            return 12;
        }
        if (strFieldName.equals("AgentCode")) {
            return 13;
        }
        if (strFieldName.equals("AgentGroup")) {
            return 14;
        }
        if (strFieldName.equals("PayDate")) {
            return 15;
        }
        if (strFieldName.equals("EnterAccDate")) {
            return 16;
        }
        if (strFieldName.equals("ConfDate")) {
            return 17;
        }
        if (strFieldName.equals("Pay")) {
            return 18;
        }
        if (strFieldName.equals("ManageCom")) {
            return 19;
        }
        if (strFieldName.equals("Operator")) {
            return 20;
        }
        if (strFieldName.equals("MakeDate")) {
            return 21;
        }
        if (strFieldName.equals("MakeTime")) {
            return 22;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 23;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 24;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "ActuGetNo";
            break;
        case 1:
            strFieldName = "OtherNo";
            break;
        case 2:
            strFieldName = "OtherNoType";
            break;
        case 3:
            strFieldName = "GrpContNo";
            break;
        case 4:
            strFieldName = "ContNo";
            break;
        case 5:
            strFieldName = "PolNo";
            break;
        case 6:
            strFieldName = "CustomerNo";
            break;
        case 7:
            strFieldName = "ServItemNo";
            break;
        case 8:
            strFieldName = "ServTaskNo";
            break;
        case 9:
            strFieldName = "KindCode";
            break;
        case 10:
            strFieldName = "RiskCode";
            break;
        case 11:
            strFieldName = "RiskVersion";
            break;
        case 12:
            strFieldName = "SaleChnl";
            break;
        case 13:
            strFieldName = "AgentCode";
            break;
        case 14:
            strFieldName = "AgentGroup";
            break;
        case 15:
            strFieldName = "PayDate";
            break;
        case 16:
            strFieldName = "EnterAccDate";
            break;
        case 17:
            strFieldName = "ConfDate";
            break;
        case 18:
            strFieldName = "Pay";
            break;
        case 19:
            strFieldName = "ManageCom";
            break;
        case 20:
            strFieldName = "Operator";
            break;
        case 21:
            strFieldName = "MakeDate";
            break;
        case 22:
            strFieldName = "MakeTime";
            break;
        case 23:
            strFieldName = "ModifyDate";
            break;
        case 24:
            strFieldName = "ModifyTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("ActuGetNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OtherNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OtherNoType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpContNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PolNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CustomerNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServItemNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServTaskNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("KindCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskVersion")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SaleChnl")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentGroup")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PayDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("EnterAccDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ConfDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("Pay")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("ManageCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 12:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 13:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 14:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 15:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 16:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 17:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 18:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 19:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 20:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 21:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 22:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 23:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 24:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
