/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LQClaimGrpPeoplesDB;

/*
 * <p>ClassName: LQClaimGrpPeoplesSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2017-08-08
 */
public class LQClaimGrpPeoplesSchema implements Schema, Cloneable
{
	// @Field
	/** 集体合同号码 */
	private String Grpcontno;
	/** 集体保单险种号码 */
	private String Grppolno;
	/** 险种编码 */
	private String RiskCode;
	/** 管理机构 */
	private String ManageCom;
	/** 销售渠道 */
	private String Salechnl;
	/** 签单日期 */
	private Date Signdate;

	public static final int FIELDNUM = 6;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LQClaimGrpPeoplesSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "Grppolno";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LQClaimGrpPeoplesSchema cloned = (LQClaimGrpPeoplesSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getGrpcontno()
	{
		return Grpcontno;
	}
	public void setGrpcontno(String aGrpcontno)
	{
		Grpcontno = aGrpcontno;
	}
	public String getGrppolno()
	{
		return Grppolno;
	}
	public void setGrppolno(String aGrppolno)
	{
		Grppolno = aGrppolno;
	}
	public String getRiskCode()
	{
		return RiskCode;
	}
	public void setRiskCode(String aRiskCode)
	{
		RiskCode = aRiskCode;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getSalechnl()
	{
		return Salechnl;
	}
	public void setSalechnl(String aSalechnl)
	{
		Salechnl = aSalechnl;
	}
	public String getSigndate()
	{
		if( Signdate != null )
			return fDate.getString(Signdate);
		else
			return null;
	}
	public void setSigndate(Date aSigndate)
	{
		Signdate = aSigndate;
	}
	public void setSigndate(String aSigndate)
	{
		if (aSigndate != null && !aSigndate.equals("") )
		{
			Signdate = fDate.getDate( aSigndate );
		}
		else
			Signdate = null;
	}


	/**
	* 使用另外一个 LQClaimGrpPeoplesSchema 对象给 Schema 赋值
	* @param: aLQClaimGrpPeoplesSchema LQClaimGrpPeoplesSchema
	**/
	public void setSchema(LQClaimGrpPeoplesSchema aLQClaimGrpPeoplesSchema)
	{
		this.Grpcontno = aLQClaimGrpPeoplesSchema.getGrpcontno();
		this.Grppolno = aLQClaimGrpPeoplesSchema.getGrppolno();
		this.RiskCode = aLQClaimGrpPeoplesSchema.getRiskCode();
		this.ManageCom = aLQClaimGrpPeoplesSchema.getManageCom();
		this.Salechnl = aLQClaimGrpPeoplesSchema.getSalechnl();
		this.Signdate = fDate.getDate( aLQClaimGrpPeoplesSchema.getSigndate());
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("Grpcontno") == null )
				this.Grpcontno = null;
			else
				this.Grpcontno = rs.getString("Grpcontno").trim();

			if( rs.getString("Grppolno") == null )
				this.Grppolno = null;
			else
				this.Grppolno = rs.getString("Grppolno").trim();

			if( rs.getString("RiskCode") == null )
				this.RiskCode = null;
			else
				this.RiskCode = rs.getString("RiskCode").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("Salechnl") == null )
				this.Salechnl = null;
			else
				this.Salechnl = rs.getString("Salechnl").trim();

			this.Signdate = rs.getDate("Signdate");
		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LQClaimGrpPeoples表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LQClaimGrpPeoplesSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LQClaimGrpPeoplesSchema getSchema()
	{
		LQClaimGrpPeoplesSchema aLQClaimGrpPeoplesSchema = new LQClaimGrpPeoplesSchema();
		aLQClaimGrpPeoplesSchema.setSchema(this);
		return aLQClaimGrpPeoplesSchema;
	}

	public LQClaimGrpPeoplesDB getDB()
	{
		LQClaimGrpPeoplesDB aDBOper = new LQClaimGrpPeoplesDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLQClaimGrpPeoples描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(Grpcontno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Grppolno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Salechnl)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Signdate )));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLQClaimGrpPeoples>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			Grpcontno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			Grppolno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			Salechnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			Signdate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,SysConst.PACKAGESPILTER));
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LQClaimGrpPeoplesSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("Grpcontno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Grpcontno));
		}
		if (FCode.equals("Grppolno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Grppolno));
		}
		if (FCode.equals("RiskCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("Salechnl"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Salechnl));
		}
		if (FCode.equals("Signdate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getSigndate()));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(Grpcontno);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(Grppolno);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(RiskCode);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(Salechnl);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getSigndate()));
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("Grpcontno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Grpcontno = FValue.trim();
			}
			else
				Grpcontno = null;
		}
		if (FCode.equalsIgnoreCase("Grppolno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Grppolno = FValue.trim();
			}
			else
				Grppolno = null;
		}
		if (FCode.equalsIgnoreCase("RiskCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskCode = FValue.trim();
			}
			else
				RiskCode = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("Salechnl"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Salechnl = FValue.trim();
			}
			else
				Salechnl = null;
		}
		if (FCode.equalsIgnoreCase("Signdate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Signdate = fDate.getDate( FValue );
			}
			else
				Signdate = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LQClaimGrpPeoplesSchema other = (LQClaimGrpPeoplesSchema)otherObject;
		return
			(Grpcontno == null ? other.getGrpcontno() == null : Grpcontno.equals(other.getGrpcontno()))
			&& (Grppolno == null ? other.getGrppolno() == null : Grppolno.equals(other.getGrppolno()))
			&& (RiskCode == null ? other.getRiskCode() == null : RiskCode.equals(other.getRiskCode()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (Salechnl == null ? other.getSalechnl() == null : Salechnl.equals(other.getSalechnl()))
			&& (Signdate == null ? other.getSigndate() == null : fDate.getString(Signdate).equals(other.getSigndate()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("Grpcontno") ) {
			return 0;
		}
		if( strFieldName.equals("Grppolno") ) {
			return 1;
		}
		if( strFieldName.equals("RiskCode") ) {
			return 2;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 3;
		}
		if( strFieldName.equals("Salechnl") ) {
			return 4;
		}
		if( strFieldName.equals("Signdate") ) {
			return 5;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "Grpcontno";
				break;
			case 1:
				strFieldName = "Grppolno";
				break;
			case 2:
				strFieldName = "RiskCode";
				break;
			case 3:
				strFieldName = "ManageCom";
				break;
			case 4:
				strFieldName = "Salechnl";
				break;
			case 5:
				strFieldName = "Signdate";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("Grpcontno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Grppolno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Salechnl") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Signdate") ) {
			return Schema.TYPE_DATE;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_DATE;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
