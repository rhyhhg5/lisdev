/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LCContGetPolDB;

/*
 * <p>ClassName: LCContGetPolSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-01-19
 */
public class LCContGetPolSchema implements Schema, Cloneable {
    // @Field
    /** 合同号码 */
    private String ContNo;
    /** 印刷号码 */
    private String PrtNo;
    /** 总单类型 */
    private String ContType;
    /** 保单生效日期 */
    private Date CValiDate;
    /** 投保人客户号码 */
    private String AppntNo;
    /** 投保人名称 */
    private String AppntName;
    /** 保费 */
    private double Prem;
    /** 代理人编码 */
    private String AgentCode;
    /** 代理人姓名 */
    private String AgentName;
    /** 签单日期 */
    private Date SignDate;
    /** 保单管理机构 */
    private String ManageCom;
    /** 保单接收管理机构 */
    private String ReceiveManageCom;
    /** 保单接收操作人员 */
    private String ReceiveOperator;
    /** 保单接收日期 */
    private Date ReceiveDate;
    /** 保单接收时间 */
    private String ReceiveTime;
    /** 回销状态 */
    private String GetpolState;
    /** 签收日期 */
    private Date GetpolDate;
    /** 签收人员 */
    private String GetpolMan;
    /** 递交人员 */
    private String SendPolMan;
    /** 签收操作员管理机构 */
    private String GetPolManageCom;
    /** 签收操作员 */
    private String GetPolOperator;
    /** 签收操作日期 */
    private Date GetpolMakeDate;
    /** 签收操作时间 */
    private String GetpolMakeTime;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 27; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LCContGetPolSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "ContNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LCContGetPolSchema cloned = (LCContGetPolSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }

    public String getPrtNo() {
        return PrtNo;
    }

    public void setPrtNo(String aPrtNo) {
        PrtNo = aPrtNo;
    }

    public String getContType() {
        return ContType;
    }

    public void setContType(String aContType) {
        ContType = aContType;
    }

    public String getCValiDate() {
        if (CValiDate != null) {
            return fDate.getString(CValiDate);
        } else {
            return null;
        }
    }

    public void setCValiDate(Date aCValiDate) {
        CValiDate = aCValiDate;
    }

    public void setCValiDate(String aCValiDate) {
        if (aCValiDate != null && !aCValiDate.equals("")) {
            CValiDate = fDate.getDate(aCValiDate);
        } else {
            CValiDate = null;
        }
    }

    public String getAppntNo() {
        return AppntNo;
    }

    public void setAppntNo(String aAppntNo) {
        AppntNo = aAppntNo;
    }

    public String getAppntName() {
        return AppntName;
    }

    public void setAppntName(String aAppntName) {
        AppntName = aAppntName;
    }

    public double getPrem() {
        return Prem;
    }

    public void setPrem(double aPrem) {
        Prem = Arith.round(aPrem, 2);
    }

    public void setPrem(String aPrem) {
        if (aPrem != null && !aPrem.equals("")) {
            Double tDouble = new Double(aPrem);
            double d = tDouble.doubleValue();
            Prem = Arith.round(d, 2);
        }
    }

    public String getAgentCode() {
        return AgentCode;
    }

    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }

    public String getAgentName() {
        return AgentName;
    }

    public void setAgentName(String aAgentName) {
        AgentName = aAgentName;
    }

    public String getSignDate() {
        if (SignDate != null) {
            return fDate.getString(SignDate);
        } else {
            return null;
        }
    }

    public void setSignDate(Date aSignDate) {
        SignDate = aSignDate;
    }

    public void setSignDate(String aSignDate) {
        if (aSignDate != null && !aSignDate.equals("")) {
            SignDate = fDate.getDate(aSignDate);
        } else {
            SignDate = null;
        }
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }

    public String getReceiveManageCom() {
        return ReceiveManageCom;
    }

    public void setReceiveManageCom(String aReceiveManageCom) {
        ReceiveManageCom = aReceiveManageCom;
    }

    public String getReceiveOperator() {
        return ReceiveOperator;
    }

    public void setReceiveOperator(String aReceiveOperator) {
        ReceiveOperator = aReceiveOperator;
    }

    public String getReceiveDate() {
        if (ReceiveDate != null) {
            return fDate.getString(ReceiveDate);
        } else {
            return null;
        }
    }

    public void setReceiveDate(Date aReceiveDate) {
        ReceiveDate = aReceiveDate;
    }

    public void setReceiveDate(String aReceiveDate) {
        if (aReceiveDate != null && !aReceiveDate.equals("")) {
            ReceiveDate = fDate.getDate(aReceiveDate);
        } else {
            ReceiveDate = null;
        }
    }

    public String getReceiveTime() {
        return ReceiveTime;
    }

    public void setReceiveTime(String aReceiveTime) {
        ReceiveTime = aReceiveTime;
    }

    public String getGetpolState() {
        return GetpolState;
    }

    public void setGetpolState(String aGetpolState) {
        GetpolState = aGetpolState;
    }

    public String getGetpolDate() {
        if (GetpolDate != null) {
            return fDate.getString(GetpolDate);
        } else {
            return null;
        }
    }

    public void setGetpolDate(Date aGetpolDate) {
        GetpolDate = aGetpolDate;
    }

    public void setGetpolDate(String aGetpolDate) {
        if (aGetpolDate != null && !aGetpolDate.equals("")) {
            GetpolDate = fDate.getDate(aGetpolDate);
        } else {
            GetpolDate = null;
        }
    }

    public String getGetpolMan() {
        return GetpolMan;
    }

    public void setGetpolMan(String aGetpolMan) {
        GetpolMan = aGetpolMan;
    }

    public String getSendPolMan() {
        return SendPolMan;
    }

    public void setSendPolMan(String aSendPolMan) {
        SendPolMan = aSendPolMan;
    }

    public String getGetPolManageCom() {
        return GetPolManageCom;
    }

    public void setGetPolManageCom(String aGetPolManageCom) {
        GetPolManageCom = aGetPolManageCom;
    }

    public String getGetPolOperator() {
        return GetPolOperator;
    }

    public void setGetPolOperator(String aGetPolOperator) {
        GetPolOperator = aGetPolOperator;
    }

    public String getGetpolMakeDate() {
        if (GetpolMakeDate != null) {
            return fDate.getString(GetpolMakeDate);
        } else {
            return null;
        }
    }

    public void setGetpolMakeDate(Date aGetpolMakeDate) {
        GetpolMakeDate = aGetpolMakeDate;
    }

    public void setGetpolMakeDate(String aGetpolMakeDate) {
        if (aGetpolMakeDate != null && !aGetpolMakeDate.equals("")) {
            GetpolMakeDate = fDate.getDate(aGetpolMakeDate);
        } else {
            GetpolMakeDate = null;
        }
    }

    public String getGetpolMakeTime() {
        return GetpolMakeTime;
    }

    public void setGetpolMakeTime(String aGetpolMakeTime) {
        GetpolMakeTime = aGetpolMakeTime;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LCContGetPolSchema 对象给 Schema 赋值
     * @param: aLCContGetPolSchema LCContGetPolSchema
     **/
    public void setSchema(LCContGetPolSchema aLCContGetPolSchema) {
        this.ContNo = aLCContGetPolSchema.getContNo();
        this.PrtNo = aLCContGetPolSchema.getPrtNo();
        this.ContType = aLCContGetPolSchema.getContType();
        this.CValiDate = fDate.getDate(aLCContGetPolSchema.getCValiDate());
        this.AppntNo = aLCContGetPolSchema.getAppntNo();
        this.AppntName = aLCContGetPolSchema.getAppntName();
        this.Prem = aLCContGetPolSchema.getPrem();
        this.AgentCode = aLCContGetPolSchema.getAgentCode();
        this.AgentName = aLCContGetPolSchema.getAgentName();
        this.SignDate = fDate.getDate(aLCContGetPolSchema.getSignDate());
        this.ManageCom = aLCContGetPolSchema.getManageCom();
        this.ReceiveManageCom = aLCContGetPolSchema.getReceiveManageCom();
        this.ReceiveOperator = aLCContGetPolSchema.getReceiveOperator();
        this.ReceiveDate = fDate.getDate(aLCContGetPolSchema.getReceiveDate());
        this.ReceiveTime = aLCContGetPolSchema.getReceiveTime();
        this.GetpolState = aLCContGetPolSchema.getGetpolState();
        this.GetpolDate = fDate.getDate(aLCContGetPolSchema.getGetpolDate());
        this.GetpolMan = aLCContGetPolSchema.getGetpolMan();
        this.SendPolMan = aLCContGetPolSchema.getSendPolMan();
        this.GetPolManageCom = aLCContGetPolSchema.getGetPolManageCom();
        this.GetPolOperator = aLCContGetPolSchema.getGetPolOperator();
        this.GetpolMakeDate = fDate.getDate(aLCContGetPolSchema.
                                            getGetpolMakeDate());
        this.GetpolMakeTime = aLCContGetPolSchema.getGetpolMakeTime();
        this.MakeDate = fDate.getDate(aLCContGetPolSchema.getMakeDate());
        this.MakeTime = aLCContGetPolSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLCContGetPolSchema.getModifyDate());
        this.ModifyTime = aLCContGetPolSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ContNo") == null) {
                this.ContNo = null;
            } else {
                this.ContNo = rs.getString("ContNo").trim();
            }

            if (rs.getString("PrtNo") == null) {
                this.PrtNo = null;
            } else {
                this.PrtNo = rs.getString("PrtNo").trim();
            }

            if (rs.getString("ContType") == null) {
                this.ContType = null;
            } else {
                this.ContType = rs.getString("ContType").trim();
            }

            this.CValiDate = rs.getDate("CValiDate");
            if (rs.getString("AppntNo") == null) {
                this.AppntNo = null;
            } else {
                this.AppntNo = rs.getString("AppntNo").trim();
            }

            if (rs.getString("AppntName") == null) {
                this.AppntName = null;
            } else {
                this.AppntName = rs.getString("AppntName").trim();
            }

            this.Prem = rs.getDouble("Prem");
            if (rs.getString("AgentCode") == null) {
                this.AgentCode = null;
            } else {
                this.AgentCode = rs.getString("AgentCode").trim();
            }

            if (rs.getString("AgentName") == null) {
                this.AgentName = null;
            } else {
                this.AgentName = rs.getString("AgentName").trim();
            }

            this.SignDate = rs.getDate("SignDate");
            if (rs.getString("ManageCom") == null) {
                this.ManageCom = null;
            } else {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("ReceiveManageCom") == null) {
                this.ReceiveManageCom = null;
            } else {
                this.ReceiveManageCom = rs.getString("ReceiveManageCom").trim();
            }

            if (rs.getString("ReceiveOperator") == null) {
                this.ReceiveOperator = null;
            } else {
                this.ReceiveOperator = rs.getString("ReceiveOperator").trim();
            }

            this.ReceiveDate = rs.getDate("ReceiveDate");
            if (rs.getString("ReceiveTime") == null) {
                this.ReceiveTime = null;
            } else {
                this.ReceiveTime = rs.getString("ReceiveTime").trim();
            }

            if (rs.getString("GetpolState") == null) {
                this.GetpolState = null;
            } else {
                this.GetpolState = rs.getString("GetpolState").trim();
            }

            this.GetpolDate = rs.getDate("GetpolDate");
            if (rs.getString("GetpolMan") == null) {
                this.GetpolMan = null;
            } else {
                this.GetpolMan = rs.getString("GetpolMan").trim();
            }

            if (rs.getString("SendPolMan") == null) {
                this.SendPolMan = null;
            } else {
                this.SendPolMan = rs.getString("SendPolMan").trim();
            }

            if (rs.getString("GetPolManageCom") == null) {
                this.GetPolManageCom = null;
            } else {
                this.GetPolManageCom = rs.getString("GetPolManageCom").trim();
            }

            if (rs.getString("GetPolOperator") == null) {
                this.GetPolOperator = null;
            } else {
                this.GetPolOperator = rs.getString("GetPolOperator").trim();
            }

            this.GetpolMakeDate = rs.getDate("GetpolMakeDate");
            if (rs.getString("GetpolMakeTime") == null) {
                this.GetpolMakeTime = null;
            } else {
                this.GetpolMakeTime = rs.getString("GetpolMakeTime").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LCContGetPol表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCContGetPolSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LCContGetPolSchema getSchema() {
        LCContGetPolSchema aLCContGetPolSchema = new LCContGetPolSchema();
        aLCContGetPolSchema.setSchema(this);
        return aLCContGetPolSchema;
    }

    public LCContGetPolDB getDB() {
        LCContGetPolDB aDBOper = new LCContGetPolDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCContGetPol描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(ContNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PrtNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(CValiDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppntNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppntName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Prem));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(SignDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ReceiveManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ReceiveOperator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ReceiveDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ReceiveTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GetpolState));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(GetpolDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GetpolMan));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SendPolMan));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GetPolManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GetPolOperator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(GetpolMakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GetpolMakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCContGetPol>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                    SysConst.PACKAGESPILTER);
            PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                   SysConst.PACKAGESPILTER);
            ContType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            CValiDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 4, SysConst.PACKAGESPILTER));
            AppntNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                     SysConst.PACKAGESPILTER);
            AppntName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                       SysConst.PACKAGESPILTER);
            Prem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    7, SysConst.PACKAGESPILTER))).doubleValue();
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                       SysConst.PACKAGESPILTER);
            AgentName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                       SysConst.PACKAGESPILTER);
            SignDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 10, SysConst.PACKAGESPILTER));
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                       SysConst.PACKAGESPILTER);
            ReceiveManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                              12, SysConst.PACKAGESPILTER);
            ReceiveOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             13, SysConst.PACKAGESPILTER);
            ReceiveDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 14, SysConst.PACKAGESPILTER));
            ReceiveTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                         SysConst.PACKAGESPILTER);
            GetpolState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                         SysConst.PACKAGESPILTER);
            GetpolDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 17, SysConst.PACKAGESPILTER));
            GetpolMan = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                       SysConst.PACKAGESPILTER);
            SendPolMan = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                        SysConst.PACKAGESPILTER);
            GetPolManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             20, SysConst.PACKAGESPILTER);
            GetPolOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                            21, SysConst.PACKAGESPILTER);
            GetpolMakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 22, SysConst.PACKAGESPILTER));
            GetpolMakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                            23, SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 24, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 26, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27,
                                        SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCContGetPolSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equals("PrtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equals("ContType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContType));
        }
        if (FCode.equals("CValiDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getCValiDate()));
        }
        if (FCode.equals("AppntNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
        }
        if (FCode.equals("AppntName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntName));
        }
        if (FCode.equals("Prem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Prem));
        }
        if (FCode.equals("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equals("AgentName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentName));
        }
        if (FCode.equals("SignDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getSignDate()));
        }
        if (FCode.equals("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("ReceiveManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiveManageCom));
        }
        if (FCode.equals("ReceiveOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiveOperator));
        }
        if (FCode.equals("ReceiveDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getReceiveDate()));
        }
        if (FCode.equals("ReceiveTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiveTime));
        }
        if (FCode.equals("GetpolState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetpolState));
        }
        if (FCode.equals("GetpolDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getGetpolDate()));
        }
        if (FCode.equals("GetpolMan")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetpolMan));
        }
        if (FCode.equals("SendPolMan")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SendPolMan));
        }
        if (FCode.equals("GetPolManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetPolManageCom));
        }
        if (FCode.equals("GetPolOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetPolOperator));
        }
        if (FCode.equals("GetpolMakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getGetpolMakeDate()));
        }
        if (FCode.equals("GetpolMakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetpolMakeTime));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(ContNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(PrtNo);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(ContType);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getCValiDate()));
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(AppntNo);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(AppntName);
            break;
        case 6:
            strFieldValue = String.valueOf(Prem);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(AgentCode);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(AgentName);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getSignDate()));
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(ManageCom);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(ReceiveManageCom);
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(ReceiveOperator);
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getReceiveDate()));
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(ReceiveTime);
            break;
        case 15:
            strFieldValue = StrTool.GBKToUnicode(GetpolState);
            break;
        case 16:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getGetpolDate()));
            break;
        case 17:
            strFieldValue = StrTool.GBKToUnicode(GetpolMan);
            break;
        case 18:
            strFieldValue = StrTool.GBKToUnicode(SendPolMan);
            break;
        case 19:
            strFieldValue = StrTool.GBKToUnicode(GetPolManageCom);
            break;
        case 20:
            strFieldValue = StrTool.GBKToUnicode(GetPolOperator);
            break;
        case 21:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getGetpolMakeDate()));
            break;
        case 22:
            strFieldValue = StrTool.GBKToUnicode(GetpolMakeTime);
            break;
        case 23:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 24:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 25:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 26:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("ContNo")) {
            if (FValue != null && !FValue.equals("")) {
                ContNo = FValue.trim();
            } else {
                ContNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            if (FValue != null && !FValue.equals("")) {
                PrtNo = FValue.trim();
            } else {
                PrtNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ContType")) {
            if (FValue != null && !FValue.equals("")) {
                ContType = FValue.trim();
            } else {
                ContType = null;
            }
        }
        if (FCode.equalsIgnoreCase("CValiDate")) {
            if (FValue != null && !FValue.equals("")) {
                CValiDate = fDate.getDate(FValue);
            } else {
                CValiDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            if (FValue != null && !FValue.equals("")) {
                AppntNo = FValue.trim();
            } else {
                AppntNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("AppntName")) {
            if (FValue != null && !FValue.equals("")) {
                AppntName = FValue.trim();
            } else {
                AppntName = null;
            }
        }
        if (FCode.equalsIgnoreCase("Prem")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Prem = d;
            }
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            if (FValue != null && !FValue.equals("")) {
                AgentCode = FValue.trim();
            } else {
                AgentCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("AgentName")) {
            if (FValue != null && !FValue.equals("")) {
                AgentName = FValue.trim();
            } else {
                AgentName = null;
            }
        }
        if (FCode.equalsIgnoreCase("SignDate")) {
            if (FValue != null && !FValue.equals("")) {
                SignDate = fDate.getDate(FValue);
            } else {
                SignDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ManageCom = FValue.trim();
            } else {
                ManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("ReceiveManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ReceiveManageCom = FValue.trim();
            } else {
                ReceiveManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("ReceiveOperator")) {
            if (FValue != null && !FValue.equals("")) {
                ReceiveOperator = FValue.trim();
            } else {
                ReceiveOperator = null;
            }
        }
        if (FCode.equalsIgnoreCase("ReceiveDate")) {
            if (FValue != null && !FValue.equals("")) {
                ReceiveDate = fDate.getDate(FValue);
            } else {
                ReceiveDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ReceiveTime")) {
            if (FValue != null && !FValue.equals("")) {
                ReceiveTime = FValue.trim();
            } else {
                ReceiveTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("GetpolState")) {
            if (FValue != null && !FValue.equals("")) {
                GetpolState = FValue.trim();
            } else {
                GetpolState = null;
            }
        }
        if (FCode.equalsIgnoreCase("GetpolDate")) {
            if (FValue != null && !FValue.equals("")) {
                GetpolDate = fDate.getDate(FValue);
            } else {
                GetpolDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("GetpolMan")) {
            if (FValue != null && !FValue.equals("")) {
                GetpolMan = FValue.trim();
            } else {
                GetpolMan = null;
            }
        }
        if (FCode.equalsIgnoreCase("SendPolMan")) {
            if (FValue != null && !FValue.equals("")) {
                SendPolMan = FValue.trim();
            } else {
                SendPolMan = null;
            }
        }
        if (FCode.equalsIgnoreCase("GetPolManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                GetPolManageCom = FValue.trim();
            } else {
                GetPolManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("GetPolOperator")) {
            if (FValue != null && !FValue.equals("")) {
                GetPolOperator = FValue.trim();
            } else {
                GetPolOperator = null;
            }
        }
        if (FCode.equalsIgnoreCase("GetpolMakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                GetpolMakeDate = fDate.getDate(FValue);
            } else {
                GetpolMakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("GetpolMakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                GetpolMakeTime = FValue.trim();
            } else {
                GetpolMakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LCContGetPolSchema other = (LCContGetPolSchema) otherObject;
        return
                ContNo.equals(other.getContNo())
                && PrtNo.equals(other.getPrtNo())
                && ContType.equals(other.getContType())
                && fDate.getString(CValiDate).equals(other.getCValiDate())
                && AppntNo.equals(other.getAppntNo())
                && AppntName.equals(other.getAppntName())
                && Prem == other.getPrem()
                && AgentCode.equals(other.getAgentCode())
                && AgentName.equals(other.getAgentName())
                && fDate.getString(SignDate).equals(other.getSignDate())
                && ManageCom.equals(other.getManageCom())
                && ReceiveManageCom.equals(other.getReceiveManageCom())
                && ReceiveOperator.equals(other.getReceiveOperator())
                && fDate.getString(ReceiveDate).equals(other.getReceiveDate())
                && ReceiveTime.equals(other.getReceiveTime())
                && GetpolState.equals(other.getGetpolState())
                && fDate.getString(GetpolDate).equals(other.getGetpolDate())
                && GetpolMan.equals(other.getGetpolMan())
                && SendPolMan.equals(other.getSendPolMan())
                && GetPolManageCom.equals(other.getGetPolManageCom())
                && GetPolOperator.equals(other.getGetPolOperator())
                &&
                fDate.getString(GetpolMakeDate).equals(other.getGetpolMakeDate())
                && GetpolMakeTime.equals(other.getGetpolMakeTime())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("ContNo")) {
            return 0;
        }
        if (strFieldName.equals("PrtNo")) {
            return 1;
        }
        if (strFieldName.equals("ContType")) {
            return 2;
        }
        if (strFieldName.equals("CValiDate")) {
            return 3;
        }
        if (strFieldName.equals("AppntNo")) {
            return 4;
        }
        if (strFieldName.equals("AppntName")) {
            return 5;
        }
        if (strFieldName.equals("Prem")) {
            return 6;
        }
        if (strFieldName.equals("AgentCode")) {
            return 7;
        }
        if (strFieldName.equals("AgentName")) {
            return 8;
        }
        if (strFieldName.equals("SignDate")) {
            return 9;
        }
        if (strFieldName.equals("ManageCom")) {
            return 10;
        }
        if (strFieldName.equals("ReceiveManageCom")) {
            return 11;
        }
        if (strFieldName.equals("ReceiveOperator")) {
            return 12;
        }
        if (strFieldName.equals("ReceiveDate")) {
            return 13;
        }
        if (strFieldName.equals("ReceiveTime")) {
            return 14;
        }
        if (strFieldName.equals("GetpolState")) {
            return 15;
        }
        if (strFieldName.equals("GetpolDate")) {
            return 16;
        }
        if (strFieldName.equals("GetpolMan")) {
            return 17;
        }
        if (strFieldName.equals("SendPolMan")) {
            return 18;
        }
        if (strFieldName.equals("GetPolManageCom")) {
            return 19;
        }
        if (strFieldName.equals("GetPolOperator")) {
            return 20;
        }
        if (strFieldName.equals("GetpolMakeDate")) {
            return 21;
        }
        if (strFieldName.equals("GetpolMakeTime")) {
            return 22;
        }
        if (strFieldName.equals("MakeDate")) {
            return 23;
        }
        if (strFieldName.equals("MakeTime")) {
            return 24;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 25;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 26;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "ContNo";
            break;
        case 1:
            strFieldName = "PrtNo";
            break;
        case 2:
            strFieldName = "ContType";
            break;
        case 3:
            strFieldName = "CValiDate";
            break;
        case 4:
            strFieldName = "AppntNo";
            break;
        case 5:
            strFieldName = "AppntName";
            break;
        case 6:
            strFieldName = "Prem";
            break;
        case 7:
            strFieldName = "AgentCode";
            break;
        case 8:
            strFieldName = "AgentName";
            break;
        case 9:
            strFieldName = "SignDate";
            break;
        case 10:
            strFieldName = "ManageCom";
            break;
        case 11:
            strFieldName = "ReceiveManageCom";
            break;
        case 12:
            strFieldName = "ReceiveOperator";
            break;
        case 13:
            strFieldName = "ReceiveDate";
            break;
        case 14:
            strFieldName = "ReceiveTime";
            break;
        case 15:
            strFieldName = "GetpolState";
            break;
        case 16:
            strFieldName = "GetpolDate";
            break;
        case 17:
            strFieldName = "GetpolMan";
            break;
        case 18:
            strFieldName = "SendPolMan";
            break;
        case 19:
            strFieldName = "GetPolManageCom";
            break;
        case 20:
            strFieldName = "GetPolOperator";
            break;
        case 21:
            strFieldName = "GetpolMakeDate";
            break;
        case 22:
            strFieldName = "GetpolMakeTime";
            break;
        case 23:
            strFieldName = "MakeDate";
            break;
        case 24:
            strFieldName = "MakeTime";
            break;
        case 25:
            strFieldName = "ModifyDate";
            break;
        case 26:
            strFieldName = "ModifyTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("ContNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PrtNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CValiDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("AppntNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppntName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Prem")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("AgentCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SignDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ManageCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ReceiveManageCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ReceiveOperator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ReceiveDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ReceiveTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetpolState")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetpolDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("GetpolMan")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SendPolMan")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetPolManageCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetPolOperator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetpolMakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("GetpolMakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 12:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 13:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 14:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 15:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 16:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 17:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 18:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 19:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 20:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 21:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 22:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 23:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 24:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 25:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 26:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
