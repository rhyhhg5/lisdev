/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LAWageRadix2DB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LAWageRadix2Schema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-03-30
 */
public class LAWageRadix2Schema implements Schema
{
    // @Field
    /** 顺序号 */
    private int Idx;
    /** 代理人职级 */
    private String AgentGrade;
    /** 奖金代码 */
    private String WageCode;
    /** 奖金名称 */
    private String WageName;
    /** 版本号 */
    private String AreaType;
    /** 渠道类型 */
    private String ChannelType;
    /** 展业类型 */
    private String BranchType;
    /** 条件1 */
    private double Cond1;
    /** 条件2 */
    private double Cond2;
    /** 提取比例 */
    private double DrawRate;
    /** 发放年数/月数限制 */
    private double LimitPeriod;
    /** 奖金金额 */
    private double RewardMoney;
    /** 发放时间起期 */
    private double DrawStart;
    /** 发放时间止期 */
    private double DrawEnd;
    /** 其它提取比例 */
    private double DrawRateOth;
    /** 渠道 */
    private String BranchType2;

    public static final int FIELDNUM = 16; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LAWageRadix2Schema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "Idx";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public int getIdx()
    {
        return Idx;
    }

    public void setIdx(int aIdx)
    {
        Idx = aIdx;
    }

    public void setIdx(String aIdx)
    {
        if (aIdx != null && !aIdx.equals(""))
        {
            Integer tInteger = new Integer(aIdx);
            int i = tInteger.intValue();
            Idx = i;
        }
    }

    public String getAgentGrade()
    {
        if (SysConst.CHANGECHARSET && AgentGrade != null &&
            !AgentGrade.equals(""))
        {
            AgentGrade = StrTool.unicodeToGBK(AgentGrade);
        }
        return AgentGrade;
    }

    public void setAgentGrade(String aAgentGrade)
    {
        AgentGrade = aAgentGrade;
    }

    public String getWageCode()
    {
        if (SysConst.CHANGECHARSET && WageCode != null && !WageCode.equals(""))
        {
            WageCode = StrTool.unicodeToGBK(WageCode);
        }
        return WageCode;
    }

    public void setWageCode(String aWageCode)
    {
        WageCode = aWageCode;
    }

    public String getWageName()
    {
        if (SysConst.CHANGECHARSET && WageName != null && !WageName.equals(""))
        {
            WageName = StrTool.unicodeToGBK(WageName);
        }
        return WageName;
    }

    public void setWageName(String aWageName)
    {
        WageName = aWageName;
    }

    public String getAreaType()
    {
        if (SysConst.CHANGECHARSET && AreaType != null && !AreaType.equals(""))
        {
            AreaType = StrTool.unicodeToGBK(AreaType);
        }
        return AreaType;
    }

    public void setAreaType(String aAreaType)
    {
        AreaType = aAreaType;
    }

    public String getChannelType()
    {
        if (SysConst.CHANGECHARSET && ChannelType != null &&
            !ChannelType.equals(""))
        {
            ChannelType = StrTool.unicodeToGBK(ChannelType);
        }
        return ChannelType;
    }

    public void setChannelType(String aChannelType)
    {
        ChannelType = aChannelType;
    }

    public String getBranchType()
    {
        if (SysConst.CHANGECHARSET && BranchType != null &&
            !BranchType.equals(""))
        {
            BranchType = StrTool.unicodeToGBK(BranchType);
        }
        return BranchType;
    }

    public void setBranchType(String aBranchType)
    {
        BranchType = aBranchType;
    }

    public double getCond1()
    {
        return Cond1;
    }

    public void setCond1(double aCond1)
    {
        Cond1 = aCond1;
    }

    public void setCond1(String aCond1)
    {
        if (aCond1 != null && !aCond1.equals(""))
        {
            Double tDouble = new Double(aCond1);
            double d = tDouble.doubleValue();
            Cond1 = d;
        }
    }

    public double getCond2()
    {
        return Cond2;
    }

    public void setCond2(double aCond2)
    {
        Cond2 = aCond2;
    }

    public void setCond2(String aCond2)
    {
        if (aCond2 != null && !aCond2.equals(""))
        {
            Double tDouble = new Double(aCond2);
            double d = tDouble.doubleValue();
            Cond2 = d;
        }
    }

    public double getDrawRate()
    {
        return DrawRate;
    }

    public void setDrawRate(double aDrawRate)
    {
        DrawRate = aDrawRate;
    }

    public void setDrawRate(String aDrawRate)
    {
        if (aDrawRate != null && !aDrawRate.equals(""))
        {
            Double tDouble = new Double(aDrawRate);
            double d = tDouble.doubleValue();
            DrawRate = d;
        }
    }

    public double getLimitPeriod()
    {
        return LimitPeriod;
    }

    public void setLimitPeriod(double aLimitPeriod)
    {
        LimitPeriod = aLimitPeriod;
    }

    public void setLimitPeriod(String aLimitPeriod)
    {
        if (aLimitPeriod != null && !aLimitPeriod.equals(""))
        {
            Double tDouble = new Double(aLimitPeriod);
            double d = tDouble.doubleValue();
            LimitPeriod = d;
        }
    }

    public double getRewardMoney()
    {
        return RewardMoney;
    }

    public void setRewardMoney(double aRewardMoney)
    {
        RewardMoney = aRewardMoney;
    }

    public void setRewardMoney(String aRewardMoney)
    {
        if (aRewardMoney != null && !aRewardMoney.equals(""))
        {
            Double tDouble = new Double(aRewardMoney);
            double d = tDouble.doubleValue();
            RewardMoney = d;
        }
    }

    public double getDrawStart()
    {
        return DrawStart;
    }

    public void setDrawStart(double aDrawStart)
    {
        DrawStart = aDrawStart;
    }

    public void setDrawStart(String aDrawStart)
    {
        if (aDrawStart != null && !aDrawStart.equals(""))
        {
            Double tDouble = new Double(aDrawStart);
            double d = tDouble.doubleValue();
            DrawStart = d;
        }
    }

    public double getDrawEnd()
    {
        return DrawEnd;
    }

    public void setDrawEnd(double aDrawEnd)
    {
        DrawEnd = aDrawEnd;
    }

    public void setDrawEnd(String aDrawEnd)
    {
        if (aDrawEnd != null && !aDrawEnd.equals(""))
        {
            Double tDouble = new Double(aDrawEnd);
            double d = tDouble.doubleValue();
            DrawEnd = d;
        }
    }

    public double getDrawRateOth()
    {
        return DrawRateOth;
    }

    public void setDrawRateOth(double aDrawRateOth)
    {
        DrawRateOth = aDrawRateOth;
    }

    public void setDrawRateOth(String aDrawRateOth)
    {
        if (aDrawRateOth != null && !aDrawRateOth.equals(""))
        {
            Double tDouble = new Double(aDrawRateOth);
            double d = tDouble.doubleValue();
            DrawRateOth = d;
        }
    }

    public String getBranchType2()
    {
        if (SysConst.CHANGECHARSET && BranchType2 != null &&
            !BranchType2.equals(""))
        {
            BranchType2 = StrTool.unicodeToGBK(BranchType2);
        }
        return BranchType2;
    }

    public void setBranchType2(String aBranchType2)
    {
        BranchType2 = aBranchType2;
    }

    /**
     * 使用另外一个 LAWageRadix2Schema 对象给 Schema 赋值
     * @param: aLAWageRadix2Schema LAWageRadix2Schema
     **/
    public void setSchema(LAWageRadix2Schema aLAWageRadix2Schema)
    {
        this.Idx = aLAWageRadix2Schema.getIdx();
        this.AgentGrade = aLAWageRadix2Schema.getAgentGrade();
        this.WageCode = aLAWageRadix2Schema.getWageCode();
        this.WageName = aLAWageRadix2Schema.getWageName();
        this.AreaType = aLAWageRadix2Schema.getAreaType();
        this.ChannelType = aLAWageRadix2Schema.getChannelType();
        this.BranchType = aLAWageRadix2Schema.getBranchType();
        this.Cond1 = aLAWageRadix2Schema.getCond1();
        this.Cond2 = aLAWageRadix2Schema.getCond2();
        this.DrawRate = aLAWageRadix2Schema.getDrawRate();
        this.LimitPeriod = aLAWageRadix2Schema.getLimitPeriod();
        this.RewardMoney = aLAWageRadix2Schema.getRewardMoney();
        this.DrawStart = aLAWageRadix2Schema.getDrawStart();
        this.DrawEnd = aLAWageRadix2Schema.getDrawEnd();
        this.DrawRateOth = aLAWageRadix2Schema.getDrawRateOth();
        this.BranchType2 = aLAWageRadix2Schema.getBranchType2();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            this.Idx = rs.getInt("Idx");
            if (rs.getString("AgentGrade") == null)
            {
                this.AgentGrade = null;
            }
            else
            {
                this.AgentGrade = rs.getString("AgentGrade").trim();
            }

            if (rs.getString("WageCode") == null)
            {
                this.WageCode = null;
            }
            else
            {
                this.WageCode = rs.getString("WageCode").trim();
            }

            if (rs.getString("WageName") == null)
            {
                this.WageName = null;
            }
            else
            {
                this.WageName = rs.getString("WageName").trim();
            }

            if (rs.getString("AreaType") == null)
            {
                this.AreaType = null;
            }
            else
            {
                this.AreaType = rs.getString("AreaType").trim();
            }

            if (rs.getString("ChannelType") == null)
            {
                this.ChannelType = null;
            }
            else
            {
                this.ChannelType = rs.getString("ChannelType").trim();
            }

            if (rs.getString("BranchType") == null)
            {
                this.BranchType = null;
            }
            else
            {
                this.BranchType = rs.getString("BranchType").trim();
            }

            this.Cond1 = rs.getDouble("Cond1");
            this.Cond2 = rs.getDouble("Cond2");
            this.DrawRate = rs.getDouble("DrawRate");
            this.LimitPeriod = rs.getDouble("LimitPeriod");
            this.RewardMoney = rs.getDouble("RewardMoney");
            this.DrawStart = rs.getDouble("DrawStart");
            this.DrawEnd = rs.getDouble("DrawEnd");
            this.DrawRateOth = rs.getDouble("DrawRateOth");
            if (rs.getString("BranchType2") == null)
            {
                this.BranchType2 = null;
            }
            else
            {
                this.BranchType2 = rs.getString("BranchType2").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAWageRadix2Schema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LAWageRadix2Schema getSchema()
    {
        LAWageRadix2Schema aLAWageRadix2Schema = new LAWageRadix2Schema();
        aLAWageRadix2Schema.setSchema(this);
        return aLAWageRadix2Schema;
    }

    public LAWageRadix2DB getDB()
    {
        LAWageRadix2DB aDBOper = new LAWageRadix2DB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAWageRadix2描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(ChgData.chgData(Idx));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AgentGrade)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(WageCode)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(WageName)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AreaType)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ChannelType)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BranchType)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Cond1));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Cond2));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DrawRate));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(LimitPeriod));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(RewardMoney));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DrawStart));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DrawEnd));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DrawRateOth));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BranchType2)));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAWageRadix2>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            Idx = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    1, SysConst.PACKAGESPILTER))).intValue();
            AgentGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
            WageCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            WageName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                      SysConst.PACKAGESPILTER);
            AreaType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                      SysConst.PACKAGESPILTER);
            ChannelType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                         SysConst.PACKAGESPILTER);
            BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                        SysConst.PACKAGESPILTER);
            Cond1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    8, SysConst.PACKAGESPILTER))).doubleValue();
            Cond2 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    9, SysConst.PACKAGESPILTER))).doubleValue();
            DrawRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 10, SysConst.PACKAGESPILTER))).doubleValue();
            LimitPeriod = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 11, SysConst.PACKAGESPILTER))).doubleValue();
            RewardMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 12, SysConst.PACKAGESPILTER))).doubleValue();
            DrawStart = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 13, SysConst.PACKAGESPILTER))).doubleValue();
            DrawEnd = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 14, SysConst.PACKAGESPILTER))).doubleValue();
            DrawRateOth = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 15, SysConst.PACKAGESPILTER))).doubleValue();
            BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                         SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAWageRadix2Schema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("Idx"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Idx));
        }
        if (FCode.equals("AgentGrade"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGrade));
        }
        if (FCode.equals("WageCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(WageCode));
        }
        if (FCode.equals("WageName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(WageName));
        }
        if (FCode.equals("AreaType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AreaType));
        }
        if (FCode.equals("ChannelType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChannelType));
        }
        if (FCode.equals("BranchType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
        }
        if (FCode.equals("Cond1"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Cond1));
        }
        if (FCode.equals("Cond2"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Cond2));
        }
        if (FCode.equals("DrawRate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DrawRate));
        }
        if (FCode.equals("LimitPeriod"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LimitPeriod));
        }
        if (FCode.equals("RewardMoney"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RewardMoney));
        }
        if (FCode.equals("DrawStart"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DrawStart));
        }
        if (FCode.equals("DrawEnd"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DrawEnd));
        }
        if (FCode.equals("DrawRateOth"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DrawRateOth));
        }
        if (FCode.equals("BranchType2"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = String.valueOf(Idx);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(AgentGrade);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(WageCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(WageName);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(AreaType);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(ChannelType);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(BranchType);
                break;
            case 7:
                strFieldValue = String.valueOf(Cond1);
                break;
            case 8:
                strFieldValue = String.valueOf(Cond2);
                break;
            case 9:
                strFieldValue = String.valueOf(DrawRate);
                break;
            case 10:
                strFieldValue = String.valueOf(LimitPeriod);
                break;
            case 11:
                strFieldValue = String.valueOf(RewardMoney);
                break;
            case 12:
                strFieldValue = String.valueOf(DrawStart);
                break;
            case 13:
                strFieldValue = String.valueOf(DrawEnd);
                break;
            case 14:
                strFieldValue = String.valueOf(DrawRateOth);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(BranchType2);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("Idx"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                Idx = i;
            }
        }
        if (FCode.equals("AgentGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentGrade = FValue.trim();
            }
            else
            {
                AgentGrade = null;
            }
        }
        if (FCode.equals("WageCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                WageCode = FValue.trim();
            }
            else
            {
                WageCode = null;
            }
        }
        if (FCode.equals("WageName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                WageName = FValue.trim();
            }
            else
            {
                WageName = null;
            }
        }
        if (FCode.equals("AreaType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AreaType = FValue.trim();
            }
            else
            {
                AreaType = null;
            }
        }
        if (FCode.equals("ChannelType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ChannelType = FValue.trim();
            }
            else
            {
                ChannelType = null;
            }
        }
        if (FCode.equals("BranchType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchType = FValue.trim();
            }
            else
            {
                BranchType = null;
            }
        }
        if (FCode.equals("Cond1"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Cond1 = d;
            }
        }
        if (FCode.equals("Cond2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Cond2 = d;
            }
        }
        if (FCode.equals("DrawRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                DrawRate = d;
            }
        }
        if (FCode.equals("LimitPeriod"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                LimitPeriod = d;
            }
        }
        if (FCode.equals("RewardMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                RewardMoney = d;
            }
        }
        if (FCode.equals("DrawStart"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                DrawStart = d;
            }
        }
        if (FCode.equals("DrawEnd"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                DrawEnd = d;
            }
        }
        if (FCode.equals("DrawRateOth"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                DrawRateOth = d;
            }
        }
        if (FCode.equals("BranchType2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchType2 = FValue.trim();
            }
            else
            {
                BranchType2 = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LAWageRadix2Schema other = (LAWageRadix2Schema) otherObject;
        return
                Idx == other.getIdx()
                && AgentGrade.equals(other.getAgentGrade())
                && WageCode.equals(other.getWageCode())
                && WageName.equals(other.getWageName())
                && AreaType.equals(other.getAreaType())
                && ChannelType.equals(other.getChannelType())
                && BranchType.equals(other.getBranchType())
                && Cond1 == other.getCond1()
                && Cond2 == other.getCond2()
                && DrawRate == other.getDrawRate()
                && LimitPeriod == other.getLimitPeriod()
                && RewardMoney == other.getRewardMoney()
                && DrawStart == other.getDrawStart()
                && DrawEnd == other.getDrawEnd()
                && DrawRateOth == other.getDrawRateOth()
                && BranchType2.equals(other.getBranchType2());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("Idx"))
        {
            return 0;
        }
        if (strFieldName.equals("AgentGrade"))
        {
            return 1;
        }
        if (strFieldName.equals("WageCode"))
        {
            return 2;
        }
        if (strFieldName.equals("WageName"))
        {
            return 3;
        }
        if (strFieldName.equals("AreaType"))
        {
            return 4;
        }
        if (strFieldName.equals("ChannelType"))
        {
            return 5;
        }
        if (strFieldName.equals("BranchType"))
        {
            return 6;
        }
        if (strFieldName.equals("Cond1"))
        {
            return 7;
        }
        if (strFieldName.equals("Cond2"))
        {
            return 8;
        }
        if (strFieldName.equals("DrawRate"))
        {
            return 9;
        }
        if (strFieldName.equals("LimitPeriod"))
        {
            return 10;
        }
        if (strFieldName.equals("RewardMoney"))
        {
            return 11;
        }
        if (strFieldName.equals("DrawStart"))
        {
            return 12;
        }
        if (strFieldName.equals("DrawEnd"))
        {
            return 13;
        }
        if (strFieldName.equals("DrawRateOth"))
        {
            return 14;
        }
        if (strFieldName.equals("BranchType2"))
        {
            return 15;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "Idx";
                break;
            case 1:
                strFieldName = "AgentGrade";
                break;
            case 2:
                strFieldName = "WageCode";
                break;
            case 3:
                strFieldName = "WageName";
                break;
            case 4:
                strFieldName = "AreaType";
                break;
            case 5:
                strFieldName = "ChannelType";
                break;
            case 6:
                strFieldName = "BranchType";
                break;
            case 7:
                strFieldName = "Cond1";
                break;
            case 8:
                strFieldName = "Cond2";
                break;
            case 9:
                strFieldName = "DrawRate";
                break;
            case 10:
                strFieldName = "LimitPeriod";
                break;
            case 11:
                strFieldName = "RewardMoney";
                break;
            case 12:
                strFieldName = "DrawStart";
                break;
            case 13:
                strFieldName = "DrawEnd";
                break;
            case 14:
                strFieldName = "DrawRateOth";
                break;
            case 15:
                strFieldName = "BranchType2";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("Idx"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("AgentGrade"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("WageCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("WageName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AreaType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ChannelType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Cond1"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Cond2"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("DrawRate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("LimitPeriod"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("RewardMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("DrawStart"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("DrawEnd"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("DrawRateOth"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("BranchType2"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_INT;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 8:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 9:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 10:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 11:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 12:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 13:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 14:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
