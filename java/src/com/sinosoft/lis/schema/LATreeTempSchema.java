/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LATreeTempDB;

/*
 * <p>ClassName: LATreeTempSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 职级异动管理
 * @CreateDate：2006-09-27
 */
public class LATreeTempSchema implements Schema, Cloneable
{
	// @Field
	/** 职级生效年月 */
	private String CValiMonth;
	/** 变更次数 */
	private int TempCount;
	/** 代理人编码 */
	private String AgentCode;
	/** 代理人展业机构代码 */
	private String AgentGroup;
	/** 管理机构 */
	private String ManageCom;
	/** 代理人系列 */
	private String AgentSeries;
	/** 代理人级别 */
	private String AgentGrade;
	/** 代理人上次系列 */
	private String AgentLastSeries;
	/** 代理人上次级别 */
	private String AgentLastGrade;
	/** 上级代理人 */
	private String UpAgent;
	/** 前职级起聘日期 */
	private Date OldStartDate;
	/** 前职级解聘日期 */
	private Date OldEndDate;
	/** 现职级起聘日期 */
	private Date StartDate;
	/** 考核开始日期 */
	private Date AstartDate;
	/** 考核类型 */
	private String AssessType;
	/** 考核状态 */
	private String State;
	/** 渠道 */
	private String BranchType2;
	/** 展业类型 */
	private String BranchType;
	/** 生效标志 */
	private String CValiFlag;
	/** 操作员代码 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 24;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LATreeTempSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[4];
		pk[0] = "CValiMonth";
		pk[1] = "TempCount";
		pk[2] = "AgentCode";
		pk[3] = "CValiFlag";

		PK = pk;
	}

            /**
             * Schema克隆
             * @return Object
             * @throws CloneNotSupportedException
             */
            public Object clone()
                    throws CloneNotSupportedException
            {
                LATreeTempSchema cloned = (LATreeTempSchema)super.clone();
                cloned.fDate = (FDate) fDate.clone();
                cloned.mErrors = (CErrors) mErrors.clone();
                return cloned;
            }

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getCValiMonth()
	{
		return CValiMonth;
	}
	public void setCValiMonth(String aCValiMonth)
	{
            CValiMonth = aCValiMonth;
	}
	public int getTempCount()
	{
		return TempCount;
	}
	public void setTempCount(int aTempCount)
	{
            TempCount = aTempCount;
	}
	public void setTempCount(String aTempCount)
	{
		if (aTempCount != null && !aTempCount.equals(""))
		{
			Integer tInteger = new Integer(aTempCount);
			int i = tInteger.intValue();
			TempCount = i;
		}
	}

	public String getAgentCode()
	{
		return AgentCode;
	}
	public void setAgentCode(String aAgentCode)
	{
            AgentCode = aAgentCode;
	}
	public String getAgentGroup()
	{
		return AgentGroup;
	}
	public void setAgentGroup(String aAgentGroup)
	{
            AgentGroup = aAgentGroup;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
            ManageCom = aManageCom;
	}
	public String getAgentSeries()
	{
		return AgentSeries;
	}
	public void setAgentSeries(String aAgentSeries)
	{
            AgentSeries = aAgentSeries;
	}
	public String getAgentGrade()
	{
		return AgentGrade;
	}
	public void setAgentGrade(String aAgentGrade)
	{
            AgentGrade = aAgentGrade;
	}
	public String getAgentLastSeries()
	{
		return AgentLastSeries;
	}
	public void setAgentLastSeries(String aAgentLastSeries)
	{
            AgentLastSeries = aAgentLastSeries;
	}
	public String getAgentLastGrade()
	{
		return AgentLastGrade;
	}
	public void setAgentLastGrade(String aAgentLastGrade)
	{
            AgentLastGrade = aAgentLastGrade;
	}
	public String getUpAgent()
	{
		return UpAgent;
	}
	public void setUpAgent(String aUpAgent)
	{
            UpAgent = aUpAgent;
	}
	public String getOldStartDate()
	{
		if( OldStartDate != null )
			return fDate.getString(OldStartDate);
		else
			return null;
	}
	public void setOldStartDate(Date aOldStartDate)
	{
            OldStartDate = aOldStartDate;
	}
	public void setOldStartDate(String aOldStartDate)
	{
		if (aOldStartDate != null && !aOldStartDate.equals("") )
		{
			OldStartDate = fDate.getDate( aOldStartDate );
		}
		else
			OldStartDate = null;
	}

	public String getOldEndDate()
	{
		if( OldEndDate != null )
			return fDate.getString(OldEndDate);
		else
			return null;
	}
	public void setOldEndDate(Date aOldEndDate)
	{
            OldEndDate = aOldEndDate;
	}
	public void setOldEndDate(String aOldEndDate)
	{
		if (aOldEndDate != null && !aOldEndDate.equals("") )
		{
			OldEndDate = fDate.getDate( aOldEndDate );
		}
		else
			OldEndDate = null;
	}

	public String getStartDate()
	{
		if( StartDate != null )
			return fDate.getString(StartDate);
		else
			return null;
	}
	public void setStartDate(Date aStartDate)
	{
            StartDate = aStartDate;
	}
	public void setStartDate(String aStartDate)
	{
		if (aStartDate != null && !aStartDate.equals("") )
		{
			StartDate = fDate.getDate( aStartDate );
		}
		else
			StartDate = null;
	}

	public String getAstartDate()
	{
		if( AstartDate != null )
			return fDate.getString(AstartDate);
		else
			return null;
	}
	public void setAstartDate(Date aAstartDate)
	{
            AstartDate = aAstartDate;
	}
	public void setAstartDate(String aAstartDate)
	{
		if (aAstartDate != null && !aAstartDate.equals("") )
		{
			AstartDate = fDate.getDate( aAstartDate );
		}
		else
			AstartDate = null;
	}

	public String getAssessType()
	{
		return AssessType;
	}
	public void setAssessType(String aAssessType)
	{
            AssessType = aAssessType;
	}
	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
            State = aState;
	}
	public String getBranchType2()
	{
		return BranchType2;
	}
	public void setBranchType2(String aBranchType2)
	{
            BranchType2 = aBranchType2;
	}
	public String getBranchType()
	{
		return BranchType;
	}
	public void setBranchType(String aBranchType)
	{
            BranchType = aBranchType;
	}
	public String getCValiFlag()
	{
		return CValiFlag;
	}
	public void setCValiFlag(String aCValiFlag)
	{
            CValiFlag = aCValiFlag;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
            Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
            MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
            MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
            ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
            ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LATreeTempSchema 对象给 Schema 赋值
	* @param: aLATreeTempSchema LATreeTempSchema
	**/
	public void setSchema(LATreeTempSchema aLATreeTempSchema)
	{
		this.CValiMonth = aLATreeTempSchema.getCValiMonth();
		this.TempCount = aLATreeTempSchema.getTempCount();
		this.AgentCode = aLATreeTempSchema.getAgentCode();
		this.AgentGroup = aLATreeTempSchema.getAgentGroup();
		this.ManageCom = aLATreeTempSchema.getManageCom();
		this.AgentSeries = aLATreeTempSchema.getAgentSeries();
		this.AgentGrade = aLATreeTempSchema.getAgentGrade();
		this.AgentLastSeries = aLATreeTempSchema.getAgentLastSeries();
		this.AgentLastGrade = aLATreeTempSchema.getAgentLastGrade();
		this.UpAgent = aLATreeTempSchema.getUpAgent();
		this.OldStartDate = fDate.getDate( aLATreeTempSchema.getOldStartDate());
		this.OldEndDate = fDate.getDate( aLATreeTempSchema.getOldEndDate());
		this.StartDate = fDate.getDate( aLATreeTempSchema.getStartDate());
		this.AstartDate = fDate.getDate( aLATreeTempSchema.getAstartDate());
		this.AssessType = aLATreeTempSchema.getAssessType();
		this.State = aLATreeTempSchema.getState();
		this.BranchType2 = aLATreeTempSchema.getBranchType2();
		this.BranchType = aLATreeTempSchema.getBranchType();
		this.CValiFlag = aLATreeTempSchema.getCValiFlag();
		this.Operator = aLATreeTempSchema.getOperator();
		this.MakeDate = fDate.getDate( aLATreeTempSchema.getMakeDate());
		this.MakeTime = aLATreeTempSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLATreeTempSchema.getModifyDate());
		this.ModifyTime = aLATreeTempSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("CValiMonth") == null )
				this.CValiMonth = null;
			else
				this.CValiMonth = rs.getString("CValiMonth").trim();

			this.TempCount = rs.getInt("TempCount");
			if( rs.getString("AgentCode") == null )
				this.AgentCode = null;
			else
				this.AgentCode = rs.getString("AgentCode").trim();

			if( rs.getString("AgentGroup") == null )
				this.AgentGroup = null;
			else
				this.AgentGroup = rs.getString("AgentGroup").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("AgentSeries") == null )
				this.AgentSeries = null;
			else
				this.AgentSeries = rs.getString("AgentSeries").trim();

			if( rs.getString("AgentGrade") == null )
				this.AgentGrade = null;
			else
				this.AgentGrade = rs.getString("AgentGrade").trim();

			if( rs.getString("AgentLastSeries") == null )
				this.AgentLastSeries = null;
			else
				this.AgentLastSeries = rs.getString("AgentLastSeries").trim();

			if( rs.getString("AgentLastGrade") == null )
				this.AgentLastGrade = null;
			else
				this.AgentLastGrade = rs.getString("AgentLastGrade").trim();

			if( rs.getString("UpAgent") == null )
				this.UpAgent = null;
			else
				this.UpAgent = rs.getString("UpAgent").trim();

			this.OldStartDate = rs.getDate("OldStartDate");
			this.OldEndDate = rs.getDate("OldEndDate");
			this.StartDate = rs.getDate("StartDate");
			this.AstartDate = rs.getDate("AstartDate");
			if( rs.getString("AssessType") == null )
				this.AssessType = null;
			else
				this.AssessType = rs.getString("AssessType").trim();

			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			if( rs.getString("BranchType2") == null )
				this.BranchType2 = null;
			else
				this.BranchType2 = rs.getString("BranchType2").trim();

			if( rs.getString("BranchType") == null )
				this.BranchType = null;
			else
				this.BranchType = rs.getString("BranchType").trim();

			if( rs.getString("CValiFlag") == null )
				this.CValiFlag = null;
			else
				this.CValiFlag = rs.getString("CValiFlag").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LATreeTemp表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LATreeTempSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LATreeTempSchema getSchema()
	{
		LATreeTempSchema aLATreeTempSchema = new LATreeTempSchema();
		aLATreeTempSchema.setSchema(this);
		return aLATreeTempSchema;
	}

	public LATreeTempDB getDB()
	{
		LATreeTempDB aDBOper = new LATreeTempDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLATreeTemp描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append(StrTool.cTrim(CValiMonth)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(TempCount));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(AgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(AgentGroup)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(AgentSeries)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(AgentGrade)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(AgentLastSeries)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(AgentLastGrade)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(UpAgent)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( OldStartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( OldEndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( StartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( AstartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(AssessType)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(BranchType2)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(BranchType)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(CValiFlag)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLATreeTemp>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			CValiMonth = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			TempCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,2,SysConst.PACKAGESPILTER))).intValue();
			AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			AgentSeries = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			AgentGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			AgentLastSeries = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			AgentLastGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			UpAgent = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			OldStartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
			OldEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
			StartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,SysConst.PACKAGESPILTER));
			AstartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,SysConst.PACKAGESPILTER));
			AssessType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			CValiFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LATreeTempSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("CValiMonth"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CValiMonth));
		}
		if (FCode.equals("TempCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TempCount));
		}
		if (FCode.equals("AgentCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
		}
		if (FCode.equals("AgentGroup"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("AgentSeries"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentSeries));
		}
		if (FCode.equals("AgentGrade"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGrade));
		}
		if (FCode.equals("AgentLastSeries"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentLastSeries));
		}
		if (FCode.equals("AgentLastGrade"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentLastGrade));
		}
		if (FCode.equals("UpAgent"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UpAgent));
		}
		if (FCode.equals("OldStartDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getOldStartDate()));
		}
		if (FCode.equals("OldEndDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getOldEndDate()));
		}
		if (FCode.equals("StartDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStartDate()));
		}
		if (FCode.equals("AstartDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getAstartDate()));
		}
		if (FCode.equals("AssessType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AssessType));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("BranchType2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
		}
		if (FCode.equals("BranchType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
		}
		if (FCode.equals("CValiFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CValiFlag));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(CValiMonth);
				break;
			case 1:
				strFieldValue = String.valueOf(TempCount);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(AgentCode);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(AgentGroup);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(AgentSeries);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(AgentGrade);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(AgentLastSeries);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(AgentLastGrade);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(UpAgent);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getOldStartDate()));
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getOldEndDate()));
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStartDate()));
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getAstartDate()));
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(AssessType);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(BranchType2);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(BranchType);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(CValiFlag);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("CValiMonth"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CValiMonth = FValue.trim();
			}
			else
				CValiMonth = null;
		}
		if (FCode.equalsIgnoreCase("TempCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				TempCount = i;
			}
		}
		if (FCode.equalsIgnoreCase("AgentCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCode = FValue.trim();
			}
			else
				AgentCode = null;
		}
		if (FCode.equalsIgnoreCase("AgentGroup"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentGroup = FValue.trim();
			}
			else
				AgentGroup = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("AgentSeries"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentSeries = FValue.trim();
			}
			else
				AgentSeries = null;
		}
		if (FCode.equalsIgnoreCase("AgentGrade"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentGrade = FValue.trim();
			}
			else
				AgentGrade = null;
		}
		if (FCode.equalsIgnoreCase("AgentLastSeries"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentLastSeries = FValue.trim();
			}
			else
				AgentLastSeries = null;
		}
		if (FCode.equalsIgnoreCase("AgentLastGrade"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentLastGrade = FValue.trim();
			}
			else
				AgentLastGrade = null;
		}
		if (FCode.equalsIgnoreCase("UpAgent"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UpAgent = FValue.trim();
			}
			else
				UpAgent = null;
		}
		if (FCode.equalsIgnoreCase("OldStartDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				OldStartDate = fDate.getDate( FValue );
			}
			else
				OldStartDate = null;
		}
		if (FCode.equalsIgnoreCase("OldEndDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				OldEndDate = fDate.getDate( FValue );
			}
			else
				OldEndDate = null;
		}
		if (FCode.equalsIgnoreCase("StartDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				StartDate = fDate.getDate( FValue );
			}
			else
				StartDate = null;
		}
		if (FCode.equalsIgnoreCase("AstartDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				AstartDate = fDate.getDate( FValue );
			}
			else
				AstartDate = null;
		}
		if (FCode.equalsIgnoreCase("AssessType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AssessType = FValue.trim();
			}
			else
				AssessType = null;
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("BranchType2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType2 = FValue.trim();
			}
			else
				BranchType2 = null;
		}
		if (FCode.equalsIgnoreCase("BranchType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType = FValue.trim();
			}
			else
				BranchType = null;
		}
		if (FCode.equalsIgnoreCase("CValiFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CValiFlag = FValue.trim();
			}
			else
				CValiFlag = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LATreeTempSchema other = (LATreeTempSchema)otherObject;
		return
			CValiMonth.equals(other.getCValiMonth())
			&& TempCount == other.getTempCount()
			&& AgentCode.equals(other.getAgentCode())
			&& AgentGroup.equals(other.getAgentGroup())
			&& ManageCom.equals(other.getManageCom())
			&& AgentSeries.equals(other.getAgentSeries())
			&& AgentGrade.equals(other.getAgentGrade())
			&& AgentLastSeries.equals(other.getAgentLastSeries())
			&& AgentLastGrade.equals(other.getAgentLastGrade())
			&& UpAgent.equals(other.getUpAgent())
			&& fDate.getString(OldStartDate).equals(other.getOldStartDate())
			&& fDate.getString(OldEndDate).equals(other.getOldEndDate())
			&& fDate.getString(StartDate).equals(other.getStartDate())
			&& fDate.getString(AstartDate).equals(other.getAstartDate())
			&& AssessType.equals(other.getAssessType())
			&& State.equals(other.getState())
			&& BranchType2.equals(other.getBranchType2())
			&& BranchType.equals(other.getBranchType())
			&& CValiFlag.equals(other.getCValiFlag())
			&& Operator.equals(other.getOperator())
			&& fDate.getString(MakeDate).equals(other.getMakeDate())
			&& MakeTime.equals(other.getMakeTime())
			&& fDate.getString(ModifyDate).equals(other.getModifyDate())
			&& ModifyTime.equals(other.getModifyTime());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("CValiMonth") ) {
			return 0;
		}
		if( strFieldName.equals("TempCount") ) {
			return 1;
		}
		if( strFieldName.equals("AgentCode") ) {
			return 2;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return 3;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 4;
		}
		if( strFieldName.equals("AgentSeries") ) {
			return 5;
		}
		if( strFieldName.equals("AgentGrade") ) {
			return 6;
		}
		if( strFieldName.equals("AgentLastSeries") ) {
			return 7;
		}
		if( strFieldName.equals("AgentLastGrade") ) {
			return 8;
		}
		if( strFieldName.equals("UpAgent") ) {
			return 9;
		}
		if( strFieldName.equals("OldStartDate") ) {
			return 10;
		}
		if( strFieldName.equals("OldEndDate") ) {
			return 11;
		}
		if( strFieldName.equals("StartDate") ) {
			return 12;
		}
		if( strFieldName.equals("AstartDate") ) {
			return 13;
		}
		if( strFieldName.equals("AssessType") ) {
			return 14;
		}
		if( strFieldName.equals("State") ) {
			return 15;
		}
		if( strFieldName.equals("BranchType2") ) {
			return 16;
		}
		if( strFieldName.equals("BranchType") ) {
			return 17;
		}
		if( strFieldName.equals("CValiFlag") ) {
			return 18;
		}
		if( strFieldName.equals("Operator") ) {
			return 19;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 20;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 21;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 22;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 23;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "CValiMonth";
				break;
			case 1:
				strFieldName = "TempCount";
				break;
			case 2:
				strFieldName = "AgentCode";
				break;
			case 3:
				strFieldName = "AgentGroup";
				break;
			case 4:
				strFieldName = "ManageCom";
				break;
			case 5:
				strFieldName = "AgentSeries";
				break;
			case 6:
				strFieldName = "AgentGrade";
				break;
			case 7:
				strFieldName = "AgentLastSeries";
				break;
			case 8:
				strFieldName = "AgentLastGrade";
				break;
			case 9:
				strFieldName = "UpAgent";
				break;
			case 10:
				strFieldName = "OldStartDate";
				break;
			case 11:
				strFieldName = "OldEndDate";
				break;
			case 12:
				strFieldName = "StartDate";
				break;
			case 13:
				strFieldName = "AstartDate";
				break;
			case 14:
				strFieldName = "AssessType";
				break;
			case 15:
				strFieldName = "State";
				break;
			case 16:
				strFieldName = "BranchType2";
				break;
			case 17:
				strFieldName = "BranchType";
				break;
			case 18:
				strFieldName = "CValiFlag";
				break;
			case 19:
				strFieldName = "Operator";
				break;
			case 20:
				strFieldName = "MakeDate";
				break;
			case 21:
				strFieldName = "MakeTime";
				break;
			case 22:
				strFieldName = "ModifyDate";
				break;
			case 23:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("CValiMonth") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TempCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("AgentCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentSeries") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentGrade") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentLastSeries") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentLastGrade") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UpAgent") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OldStartDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("OldEndDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("StartDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("AstartDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("AssessType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchType2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CValiFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_INT;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 12:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 13:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
