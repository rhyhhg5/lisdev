/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LSGrpDB;

/*
 * <p>ClassName: LSGrpSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: SY
 * @CreateDate：2015-08-24
 */
public class LSGrpSchema implements Schema, Cloneable
{
	// @Field
	/** 团体编号 */
	private String GrpNo;
	/** 单位名称 */
	private String GrpName;
	/** 税务登记证号码 */
	private String TaxRegistration;
	/** 组织机构代码 */
	private String OrgancomCode;
	/** 单位地址 */
	private String GrpAddress;
	/** 邮政编码 */
	private String GrpZipcode;
	/** 单位性质 */
	private String GrpNature;
	/** 行业类别 */
	private String BusinessType;
	/** 在职员工总人数 */
	private int Peoples;
	/** 主营业务 */
	private String MainBussiness;
	/** 单位法人代表 */
	private String LeaglePerson;
	/** 单位电话号码 */
	private String Phone;
	/** 业务经办人姓名 */
	private String HandlerName;
	/** 所在部门 */
	private String Department;
	/** 联系电话 */
	private String HandlerPhone;
	/** 开户银行 */
	private String BankCode;
	/** 银行帐户名 */
	private String BankAccName;
	/** 银行账号 */
	private String BankAccNo;
	/** 状态 */
	private String State;
	/** 备注 */
	private String Remark;
	/** 操作员 */
	private String Operator;
	/** 处理日期 */
	private Date MakeDate;
	/** 处理时间 */
	private String MakeTime;
	/** 修改日期 */
	private Date ModifyDate;
	/** 修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 25;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LSGrpSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "GrpNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LSGrpSchema cloned = (LSGrpSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getGrpNo()
	{
		return GrpNo;
	}
	public void setGrpNo(String aGrpNo)
	{
		GrpNo = aGrpNo;
	}
	public String getGrpName()
	{
		return GrpName;
	}
	public void setGrpName(String aGrpName)
	{
		GrpName = aGrpName;
	}
	public String getTaxRegistration()
	{
		return TaxRegistration;
	}
	public void setTaxRegistration(String aTaxRegistration)
	{
		TaxRegistration = aTaxRegistration;
	}
	public String getOrgancomCode()
	{
		return OrgancomCode;
	}
	public void setOrgancomCode(String aOrgancomCode)
	{
		OrgancomCode = aOrgancomCode;
	}
	public String getGrpAddress()
	{
		return GrpAddress;
	}
	public void setGrpAddress(String aGrpAddress)
	{
		GrpAddress = aGrpAddress;
	}
	public String getGrpZipcode()
	{
		return GrpZipcode;
	}
	public void setGrpZipcode(String aGrpZipcode)
	{
		GrpZipcode = aGrpZipcode;
	}
	public String getGrpNature()
	{
		return GrpNature;
	}
	public void setGrpNature(String aGrpNature)
	{
		GrpNature = aGrpNature;
	}
	public String getBusinessType()
	{
		return BusinessType;
	}
	public void setBusinessType(String aBusinessType)
	{
		BusinessType = aBusinessType;
	}
	public int getPeoples()
	{
		return Peoples;
	}
	public void setPeoples(int aPeoples)
	{
		Peoples = aPeoples;
	}
	public void setPeoples(String aPeoples)
	{
		if (aPeoples != null && !aPeoples.equals(""))
		{
			Integer tInteger = new Integer(aPeoples);
			int i = tInteger.intValue();
			Peoples = i;
		}
	}

	public String getMainBussiness()
	{
		return MainBussiness;
	}
	public void setMainBussiness(String aMainBussiness)
	{
		MainBussiness = aMainBussiness;
	}
	public String getLeaglePerson()
	{
		return LeaglePerson;
	}
	public void setLeaglePerson(String aLeaglePerson)
	{
		LeaglePerson = aLeaglePerson;
	}
	public String getPhone()
	{
		return Phone;
	}
	public void setPhone(String aPhone)
	{
		Phone = aPhone;
	}
	public String getHandlerName()
	{
		return HandlerName;
	}
	public void setHandlerName(String aHandlerName)
	{
		HandlerName = aHandlerName;
	}
	public String getDepartment()
	{
		return Department;
	}
	public void setDepartment(String aDepartment)
	{
		Department = aDepartment;
	}
	public String getHandlerPhone()
	{
		return HandlerPhone;
	}
	public void setHandlerPhone(String aHandlerPhone)
	{
		HandlerPhone = aHandlerPhone;
	}
	public String getBankCode()
	{
		return BankCode;
	}
	public void setBankCode(String aBankCode)
	{
		BankCode = aBankCode;
	}
	public String getBankAccName()
	{
		return BankAccName;
	}
	public void setBankAccName(String aBankAccName)
	{
		BankAccName = aBankAccName;
	}
	public String getBankAccNo()
	{
		return BankAccNo;
	}
	public void setBankAccNo(String aBankAccNo)
	{
		BankAccNo = aBankAccNo;
	}
	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}
	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
		Remark = aRemark;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LSGrpSchema 对象给 Schema 赋值
	* @param: aLSGrpSchema LSGrpSchema
	**/
	public void setSchema(LSGrpSchema aLSGrpSchema)
	{
		this.GrpNo = aLSGrpSchema.getGrpNo();
		this.GrpName = aLSGrpSchema.getGrpName();
		this.TaxRegistration = aLSGrpSchema.getTaxRegistration();
		this.OrgancomCode = aLSGrpSchema.getOrgancomCode();
		this.GrpAddress = aLSGrpSchema.getGrpAddress();
		this.GrpZipcode = aLSGrpSchema.getGrpZipcode();
		this.GrpNature = aLSGrpSchema.getGrpNature();
		this.BusinessType = aLSGrpSchema.getBusinessType();
		this.Peoples = aLSGrpSchema.getPeoples();
		this.MainBussiness = aLSGrpSchema.getMainBussiness();
		this.LeaglePerson = aLSGrpSchema.getLeaglePerson();
		this.Phone = aLSGrpSchema.getPhone();
		this.HandlerName = aLSGrpSchema.getHandlerName();
		this.Department = aLSGrpSchema.getDepartment();
		this.HandlerPhone = aLSGrpSchema.getHandlerPhone();
		this.BankCode = aLSGrpSchema.getBankCode();
		this.BankAccName = aLSGrpSchema.getBankAccName();
		this.BankAccNo = aLSGrpSchema.getBankAccNo();
		this.State = aLSGrpSchema.getState();
		this.Remark = aLSGrpSchema.getRemark();
		this.Operator = aLSGrpSchema.getOperator();
		this.MakeDate = fDate.getDate( aLSGrpSchema.getMakeDate());
		this.MakeTime = aLSGrpSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLSGrpSchema.getModifyDate());
		this.ModifyTime = aLSGrpSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("GrpNo") == null )
				this.GrpNo = null;
			else
				this.GrpNo = rs.getString("GrpNo").trim();

			if( rs.getString("GrpName") == null )
				this.GrpName = null;
			else
				this.GrpName = rs.getString("GrpName").trim();

			if( rs.getString("TaxRegistration") == null )
				this.TaxRegistration = null;
			else
				this.TaxRegistration = rs.getString("TaxRegistration").trim();

			if( rs.getString("OrgancomCode") == null )
				this.OrgancomCode = null;
			else
				this.OrgancomCode = rs.getString("OrgancomCode").trim();

			if( rs.getString("GrpAddress") == null )
				this.GrpAddress = null;
			else
				this.GrpAddress = rs.getString("GrpAddress").trim();

			if( rs.getString("GrpZipcode") == null )
				this.GrpZipcode = null;
			else
				this.GrpZipcode = rs.getString("GrpZipcode").trim();

			if( rs.getString("GrpNature") == null )
				this.GrpNature = null;
			else
				this.GrpNature = rs.getString("GrpNature").trim();

			if( rs.getString("BusinessType") == null )
				this.BusinessType = null;
			else
				this.BusinessType = rs.getString("BusinessType").trim();

			this.Peoples = rs.getInt("Peoples");
			if( rs.getString("MainBussiness") == null )
				this.MainBussiness = null;
			else
				this.MainBussiness = rs.getString("MainBussiness").trim();

			if( rs.getString("LeaglePerson") == null )
				this.LeaglePerson = null;
			else
				this.LeaglePerson = rs.getString("LeaglePerson").trim();

			if( rs.getString("Phone") == null )
				this.Phone = null;
			else
				this.Phone = rs.getString("Phone").trim();

			if( rs.getString("HandlerName") == null )
				this.HandlerName = null;
			else
				this.HandlerName = rs.getString("HandlerName").trim();

			if( rs.getString("Department") == null )
				this.Department = null;
			else
				this.Department = rs.getString("Department").trim();

			if( rs.getString("HandlerPhone") == null )
				this.HandlerPhone = null;
			else
				this.HandlerPhone = rs.getString("HandlerPhone").trim();

			if( rs.getString("BankCode") == null )
				this.BankCode = null;
			else
				this.BankCode = rs.getString("BankCode").trim();

			if( rs.getString("BankAccName") == null )
				this.BankAccName = null;
			else
				this.BankAccName = rs.getString("BankAccName").trim();

			if( rs.getString("BankAccNo") == null )
				this.BankAccNo = null;
			else
				this.BankAccNo = rs.getString("BankAccNo").trim();

			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LSGrp表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LSGrpSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LSGrpSchema getSchema()
	{
		LSGrpSchema aLSGrpSchema = new LSGrpSchema();
		aLSGrpSchema.setSchema(this);
		return aLSGrpSchema;
	}

	public LSGrpDB getDB()
	{
		LSGrpDB aDBOper = new LSGrpDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLSGrp描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(GrpNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TaxRegistration)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OrgancomCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpAddress)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpZipcode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpNature)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusinessType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Peoples));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MainBussiness)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LeaglePerson)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Phone)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HandlerName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Department)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HandlerPhone)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankAccName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLSGrp>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			GrpNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			GrpName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			TaxRegistration = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			OrgancomCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			GrpAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			GrpZipcode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			GrpNature = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			BusinessType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			Peoples= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,9,SysConst.PACKAGESPILTER))).intValue();
			MainBussiness = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			LeaglePerson = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			Phone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			HandlerName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			Department = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			HandlerPhone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			BankAccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			BankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LSGrpSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("GrpNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpNo));
		}
		if (FCode.equals("GrpName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpName));
		}
		if (FCode.equals("TaxRegistration"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TaxRegistration));
		}
		if (FCode.equals("OrgancomCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OrgancomCode));
		}
		if (FCode.equals("GrpAddress"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpAddress));
		}
		if (FCode.equals("GrpZipcode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpZipcode));
		}
		if (FCode.equals("GrpNature"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpNature));
		}
		if (FCode.equals("BusinessType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusinessType));
		}
		if (FCode.equals("Peoples"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Peoples));
		}
		if (FCode.equals("MainBussiness"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MainBussiness));
		}
		if (FCode.equals("LeaglePerson"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LeaglePerson));
		}
		if (FCode.equals("Phone"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Phone));
		}
		if (FCode.equals("HandlerName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HandlerName));
		}
		if (FCode.equals("Department"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Department));
		}
		if (FCode.equals("HandlerPhone"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HandlerPhone));
		}
		if (FCode.equals("BankCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
		}
		if (FCode.equals("BankAccName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccName));
		}
		if (FCode.equals("BankAccNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccNo));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(GrpNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(GrpName);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(TaxRegistration);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(OrgancomCode);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(GrpAddress);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(GrpZipcode);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(GrpNature);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(BusinessType);
				break;
			case 8:
				strFieldValue = String.valueOf(Peoples);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(MainBussiness);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(LeaglePerson);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(Phone);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(HandlerName);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(Department);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(HandlerPhone);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(BankCode);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(BankAccName);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(BankAccNo);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("GrpNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpNo = FValue.trim();
			}
			else
				GrpNo = null;
		}
		if (FCode.equalsIgnoreCase("GrpName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpName = FValue.trim();
			}
			else
				GrpName = null;
		}
		if (FCode.equalsIgnoreCase("TaxRegistration"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TaxRegistration = FValue.trim();
			}
			else
				TaxRegistration = null;
		}
		if (FCode.equalsIgnoreCase("OrgancomCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OrgancomCode = FValue.trim();
			}
			else
				OrgancomCode = null;
		}
		if (FCode.equalsIgnoreCase("GrpAddress"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpAddress = FValue.trim();
			}
			else
				GrpAddress = null;
		}
		if (FCode.equalsIgnoreCase("GrpZipcode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpZipcode = FValue.trim();
			}
			else
				GrpZipcode = null;
		}
		if (FCode.equalsIgnoreCase("GrpNature"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpNature = FValue.trim();
			}
			else
				GrpNature = null;
		}
		if (FCode.equalsIgnoreCase("BusinessType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusinessType = FValue.trim();
			}
			else
				BusinessType = null;
		}
		if (FCode.equalsIgnoreCase("Peoples"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				Peoples = i;
			}
		}
		if (FCode.equalsIgnoreCase("MainBussiness"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MainBussiness = FValue.trim();
			}
			else
				MainBussiness = null;
		}
		if (FCode.equalsIgnoreCase("LeaglePerson"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LeaglePerson = FValue.trim();
			}
			else
				LeaglePerson = null;
		}
		if (FCode.equalsIgnoreCase("Phone"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Phone = FValue.trim();
			}
			else
				Phone = null;
		}
		if (FCode.equalsIgnoreCase("HandlerName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HandlerName = FValue.trim();
			}
			else
				HandlerName = null;
		}
		if (FCode.equalsIgnoreCase("Department"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Department = FValue.trim();
			}
			else
				Department = null;
		}
		if (FCode.equalsIgnoreCase("HandlerPhone"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HandlerPhone = FValue.trim();
			}
			else
				HandlerPhone = null;
		}
		if (FCode.equalsIgnoreCase("BankCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankCode = FValue.trim();
			}
			else
				BankCode = null;
		}
		if (FCode.equalsIgnoreCase("BankAccName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankAccName = FValue.trim();
			}
			else
				BankAccName = null;
		}
		if (FCode.equalsIgnoreCase("BankAccNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankAccNo = FValue.trim();
			}
			else
				BankAccNo = null;
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LSGrpSchema other = (LSGrpSchema)otherObject;
		return
			(GrpNo == null ? other.getGrpNo() == null : GrpNo.equals(other.getGrpNo()))
			&& (GrpName == null ? other.getGrpName() == null : GrpName.equals(other.getGrpName()))
			&& (TaxRegistration == null ? other.getTaxRegistration() == null : TaxRegistration.equals(other.getTaxRegistration()))
			&& (OrgancomCode == null ? other.getOrgancomCode() == null : OrgancomCode.equals(other.getOrgancomCode()))
			&& (GrpAddress == null ? other.getGrpAddress() == null : GrpAddress.equals(other.getGrpAddress()))
			&& (GrpZipcode == null ? other.getGrpZipcode() == null : GrpZipcode.equals(other.getGrpZipcode()))
			&& (GrpNature == null ? other.getGrpNature() == null : GrpNature.equals(other.getGrpNature()))
			&& (BusinessType == null ? other.getBusinessType() == null : BusinessType.equals(other.getBusinessType()))
			&& Peoples == other.getPeoples()
			&& (MainBussiness == null ? other.getMainBussiness() == null : MainBussiness.equals(other.getMainBussiness()))
			&& (LeaglePerson == null ? other.getLeaglePerson() == null : LeaglePerson.equals(other.getLeaglePerson()))
			&& (Phone == null ? other.getPhone() == null : Phone.equals(other.getPhone()))
			&& (HandlerName == null ? other.getHandlerName() == null : HandlerName.equals(other.getHandlerName()))
			&& (Department == null ? other.getDepartment() == null : Department.equals(other.getDepartment()))
			&& (HandlerPhone == null ? other.getHandlerPhone() == null : HandlerPhone.equals(other.getHandlerPhone()))
			&& (BankCode == null ? other.getBankCode() == null : BankCode.equals(other.getBankCode()))
			&& (BankAccName == null ? other.getBankAccName() == null : BankAccName.equals(other.getBankAccName()))
			&& (BankAccNo == null ? other.getBankAccNo() == null : BankAccNo.equals(other.getBankAccNo()))
			&& (State == null ? other.getState() == null : State.equals(other.getState()))
			&& (Remark == null ? other.getRemark() == null : Remark.equals(other.getRemark()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("GrpNo") ) {
			return 0;
		}
		if( strFieldName.equals("GrpName") ) {
			return 1;
		}
		if( strFieldName.equals("TaxRegistration") ) {
			return 2;
		}
		if( strFieldName.equals("OrgancomCode") ) {
			return 3;
		}
		if( strFieldName.equals("GrpAddress") ) {
			return 4;
		}
		if( strFieldName.equals("GrpZipcode") ) {
			return 5;
		}
		if( strFieldName.equals("GrpNature") ) {
			return 6;
		}
		if( strFieldName.equals("BusinessType") ) {
			return 7;
		}
		if( strFieldName.equals("Peoples") ) {
			return 8;
		}
		if( strFieldName.equals("MainBussiness") ) {
			return 9;
		}
		if( strFieldName.equals("LeaglePerson") ) {
			return 10;
		}
		if( strFieldName.equals("Phone") ) {
			return 11;
		}
		if( strFieldName.equals("HandlerName") ) {
			return 12;
		}
		if( strFieldName.equals("Department") ) {
			return 13;
		}
		if( strFieldName.equals("HandlerPhone") ) {
			return 14;
		}
		if( strFieldName.equals("BankCode") ) {
			return 15;
		}
		if( strFieldName.equals("BankAccName") ) {
			return 16;
		}
		if( strFieldName.equals("BankAccNo") ) {
			return 17;
		}
		if( strFieldName.equals("State") ) {
			return 18;
		}
		if( strFieldName.equals("Remark") ) {
			return 19;
		}
		if( strFieldName.equals("Operator") ) {
			return 20;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 21;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 22;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 23;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 24;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "GrpNo";
				break;
			case 1:
				strFieldName = "GrpName";
				break;
			case 2:
				strFieldName = "TaxRegistration";
				break;
			case 3:
				strFieldName = "OrgancomCode";
				break;
			case 4:
				strFieldName = "GrpAddress";
				break;
			case 5:
				strFieldName = "GrpZipcode";
				break;
			case 6:
				strFieldName = "GrpNature";
				break;
			case 7:
				strFieldName = "BusinessType";
				break;
			case 8:
				strFieldName = "Peoples";
				break;
			case 9:
				strFieldName = "MainBussiness";
				break;
			case 10:
				strFieldName = "LeaglePerson";
				break;
			case 11:
				strFieldName = "Phone";
				break;
			case 12:
				strFieldName = "HandlerName";
				break;
			case 13:
				strFieldName = "Department";
				break;
			case 14:
				strFieldName = "HandlerPhone";
				break;
			case 15:
				strFieldName = "BankCode";
				break;
			case 16:
				strFieldName = "BankAccName";
				break;
			case 17:
				strFieldName = "BankAccNo";
				break;
			case 18:
				strFieldName = "State";
				break;
			case 19:
				strFieldName = "Remark";
				break;
			case 20:
				strFieldName = "Operator";
				break;
			case 21:
				strFieldName = "MakeDate";
				break;
			case 22:
				strFieldName = "MakeTime";
				break;
			case 23:
				strFieldName = "ModifyDate";
				break;
			case 24:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("GrpNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TaxRegistration") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OrgancomCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpAddress") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpZipcode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpNature") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BusinessType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Peoples") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("MainBussiness") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LeaglePerson") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Phone") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HandlerName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Department") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HandlerPhone") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankAccName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankAccNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_INT;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
