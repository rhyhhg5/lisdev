/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LLSecuDetailDB;

/*
 * <p>ClassName: LLSecuDetailSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-10-31
 */
public class LLSecuDetailSchema implements Schema, Cloneable {
    // @Field
    /** 案件号 */
    private String CaseNo;
    /** 登记号 */
    private String RgtNo;
    /** 帐单号 */
    private String MainFeeNo;
    /** 序列号 */
    private String SerialNo;
    /** 分段区间下限 */
    private double DownLimit;
    /** 分段区间上限 */
    private double UpLimit;
    /** 统筹比例 */
    private double PlanPayRate;
    /** 统筹金额 */
    private double PlanFee;
    /** 个人负担金额 */
    private double SelfPay;
    /** 管理机构 */
    private String MngCom;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 15; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LLSecuDetailSchema() {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "CaseNo";
        pk[1] = "MainFeeNo";
        pk[2] = "SerialNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LLSecuDetailSchema cloned = (LLSecuDetailSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getCaseNo() {
        return CaseNo;
    }

    public void setCaseNo(String aCaseNo) {
        CaseNo = aCaseNo;
    }

    public String getRgtNo() {
        return RgtNo;
    }

    public void setRgtNo(String aRgtNo) {
        RgtNo = aRgtNo;
    }

    public String getMainFeeNo() {
        return MainFeeNo;
    }

    public void setMainFeeNo(String aMainFeeNo) {
        MainFeeNo = aMainFeeNo;
    }

    public String getSerialNo() {
        return SerialNo;
    }

    public void setSerialNo(String aSerialNo) {
        SerialNo = aSerialNo;
    }

    public double getDownLimit() {
        return DownLimit;
    }

    public void setDownLimit(double aDownLimit) {
        DownLimit = Arith.round(aDownLimit, 4);
    }

    public void setDownLimit(String aDownLimit) {
        if (aDownLimit != null && !aDownLimit.equals("")) {
            Double tDouble = new Double(aDownLimit);
            double d = tDouble.doubleValue();
            DownLimit = Arith.round(d, 4);
        }
    }

    public double getUpLimit() {
        return UpLimit;
    }

    public void setUpLimit(double aUpLimit) {
        UpLimit = Arith.round(aUpLimit, 4);
    }

    public void setUpLimit(String aUpLimit) {
        if (aUpLimit != null && !aUpLimit.equals("")) {
            Double tDouble = new Double(aUpLimit);
            double d = tDouble.doubleValue();
            UpLimit = Arith.round(d, 4);
        }
    }

    public double getPlanPayRate() {
        return PlanPayRate;
    }

    public void setPlanPayRate(double aPlanPayRate) {
        PlanPayRate = Arith.round(aPlanPayRate, 4);
    }

    public void setPlanPayRate(String aPlanPayRate) {
        if (aPlanPayRate != null && !aPlanPayRate.equals("")) {
            Double tDouble = new Double(aPlanPayRate);
            double d = tDouble.doubleValue();
            PlanPayRate = Arith.round(d, 4);
        }
    }

    public double getPlanFee() {
        return PlanFee;
    }

    public void setPlanFee(double aPlanFee) {
        PlanFee = Arith.round(aPlanFee, 4);
    }

    public void setPlanFee(String aPlanFee) {
        if (aPlanFee != null && !aPlanFee.equals("")) {
            Double tDouble = new Double(aPlanFee);
            double d = tDouble.doubleValue();
            PlanFee = Arith.round(d, 4);
        }
    }

    public double getSelfPay() {
        return SelfPay;
    }

    public void setSelfPay(double aSelfPay) {
        SelfPay = Arith.round(aSelfPay, 4);
    }

    public void setSelfPay(String aSelfPay) {
        if (aSelfPay != null && !aSelfPay.equals("")) {
            Double tDouble = new Double(aSelfPay);
            double d = tDouble.doubleValue();
            SelfPay = Arith.round(d, 4);
        }
    }

    public String getMngCom() {
        return MngCom;
    }

    public void setMngCom(String aMngCom) {
        MngCom = aMngCom;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LLSecuDetailSchema 对象给 Schema 赋值
     * @param: aLLSecuDetailSchema LLSecuDetailSchema
     **/
    public void setSchema(LLSecuDetailSchema aLLSecuDetailSchema) {
        this.CaseNo = aLLSecuDetailSchema.getCaseNo();
        this.RgtNo = aLLSecuDetailSchema.getRgtNo();
        this.MainFeeNo = aLLSecuDetailSchema.getMainFeeNo();
        this.SerialNo = aLLSecuDetailSchema.getSerialNo();
        this.DownLimit = aLLSecuDetailSchema.getDownLimit();
        this.UpLimit = aLLSecuDetailSchema.getUpLimit();
        this.PlanPayRate = aLLSecuDetailSchema.getPlanPayRate();
        this.PlanFee = aLLSecuDetailSchema.getPlanFee();
        this.SelfPay = aLLSecuDetailSchema.getSelfPay();
        this.MngCom = aLLSecuDetailSchema.getMngCom();
        this.Operator = aLLSecuDetailSchema.getOperator();
        this.MakeDate = fDate.getDate(aLLSecuDetailSchema.getMakeDate());
        this.MakeTime = aLLSecuDetailSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLLSecuDetailSchema.getModifyDate());
        this.ModifyTime = aLLSecuDetailSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("CaseNo") == null) {
                this.CaseNo = null;
            } else {
                this.CaseNo = rs.getString("CaseNo").trim();
            }

            if (rs.getString("RgtNo") == null) {
                this.RgtNo = null;
            } else {
                this.RgtNo = rs.getString("RgtNo").trim();
            }

            if (rs.getString("MainFeeNo") == null) {
                this.MainFeeNo = null;
            } else {
                this.MainFeeNo = rs.getString("MainFeeNo").trim();
            }

            if (rs.getString("SerialNo") == null) {
                this.SerialNo = null;
            } else {
                this.SerialNo = rs.getString("SerialNo").trim();
            }

            this.DownLimit = rs.getDouble("DownLimit");
            this.UpLimit = rs.getDouble("UpLimit");
            this.PlanPayRate = rs.getDouble("PlanPayRate");
            this.PlanFee = rs.getDouble("PlanFee");
            this.SelfPay = rs.getDouble("SelfPay");
            if (rs.getString("MngCom") == null) {
                this.MngCom = null;
            } else {
                this.MngCom = rs.getString("MngCom").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LLSecuDetail表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLSecuDetailSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LLSecuDetailSchema getSchema() {
        LLSecuDetailSchema aLLSecuDetailSchema = new LLSecuDetailSchema();
        aLLSecuDetailSchema.setSchema(this);
        return aLLSecuDetailSchema;
    }

    public LLSecuDetailDB getDB() {
        LLSecuDetailDB aDBOper = new LLSecuDetailDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLSecuDetail描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(CaseNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RgtNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MainFeeNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SerialNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DownLimit));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(UpLimit));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PlanPayRate));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PlanFee));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SelfPay));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MngCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLSecuDetail>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            CaseNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                    SysConst.PACKAGESPILTER);
            RgtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                   SysConst.PACKAGESPILTER);
            MainFeeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                       SysConst.PACKAGESPILTER);
            SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                      SysConst.PACKAGESPILTER);
            DownLimit = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 5, SysConst.PACKAGESPILTER))).doubleValue();
            UpLimit = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 6, SysConst.PACKAGESPILTER))).doubleValue();
            PlanPayRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 7, SysConst.PACKAGESPILTER))).doubleValue();
            PlanFee = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 8, SysConst.PACKAGESPILTER))).doubleValue();
            SelfPay = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 9, SysConst.PACKAGESPILTER))).doubleValue();
            MngCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                    SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 12, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 14, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                        SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLSecuDetailSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("CaseNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CaseNo));
        }
        if (FCode.equals("RgtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RgtNo));
        }
        if (FCode.equals("MainFeeNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MainFeeNo));
        }
        if (FCode.equals("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equals("DownLimit")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DownLimit));
        }
        if (FCode.equals("UpLimit")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UpLimit));
        }
        if (FCode.equals("PlanPayRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PlanPayRate));
        }
        if (FCode.equals("PlanFee")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PlanFee));
        }
        if (FCode.equals("SelfPay")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SelfPay));
        }
        if (FCode.equals("MngCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MngCom));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(CaseNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(RgtNo);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(MainFeeNo);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(SerialNo);
            break;
        case 4:
            strFieldValue = String.valueOf(DownLimit);
            break;
        case 5:
            strFieldValue = String.valueOf(UpLimit);
            break;
        case 6:
            strFieldValue = String.valueOf(PlanPayRate);
            break;
        case 7:
            strFieldValue = String.valueOf(PlanFee);
            break;
        case 8:
            strFieldValue = String.valueOf(SelfPay);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(MngCom);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("CaseNo")) {
            if (FValue != null && !FValue.equals("")) {
                CaseNo = FValue.trim();
            } else {
                CaseNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("RgtNo")) {
            if (FValue != null && !FValue.equals("")) {
                RgtNo = FValue.trim();
            } else {
                RgtNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("MainFeeNo")) {
            if (FValue != null && !FValue.equals("")) {
                MainFeeNo = FValue.trim();
            } else {
                MainFeeNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            if (FValue != null && !FValue.equals("")) {
                SerialNo = FValue.trim();
            } else {
                SerialNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("DownLimit")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                DownLimit = d;
            }
        }
        if (FCode.equalsIgnoreCase("UpLimit")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                UpLimit = d;
            }
        }
        if (FCode.equalsIgnoreCase("PlanPayRate")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                PlanPayRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("PlanFee")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                PlanFee = d;
            }
        }
        if (FCode.equalsIgnoreCase("SelfPay")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                SelfPay = d;
            }
        }
        if (FCode.equalsIgnoreCase("MngCom")) {
            if (FValue != null && !FValue.equals("")) {
                MngCom = FValue.trim();
            } else {
                MngCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LLSecuDetailSchema other = (LLSecuDetailSchema) otherObject;
        return
                CaseNo.equals(other.getCaseNo())
                && RgtNo.equals(other.getRgtNo())
                && MainFeeNo.equals(other.getMainFeeNo())
                && SerialNo.equals(other.getSerialNo())
                && DownLimit == other.getDownLimit()
                && UpLimit == other.getUpLimit()
                && PlanPayRate == other.getPlanPayRate()
                && PlanFee == other.getPlanFee()
                && SelfPay == other.getSelfPay()
                && MngCom.equals(other.getMngCom())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("CaseNo")) {
            return 0;
        }
        if (strFieldName.equals("RgtNo")) {
            return 1;
        }
        if (strFieldName.equals("MainFeeNo")) {
            return 2;
        }
        if (strFieldName.equals("SerialNo")) {
            return 3;
        }
        if (strFieldName.equals("DownLimit")) {
            return 4;
        }
        if (strFieldName.equals("UpLimit")) {
            return 5;
        }
        if (strFieldName.equals("PlanPayRate")) {
            return 6;
        }
        if (strFieldName.equals("PlanFee")) {
            return 7;
        }
        if (strFieldName.equals("SelfPay")) {
            return 8;
        }
        if (strFieldName.equals("MngCom")) {
            return 9;
        }
        if (strFieldName.equals("Operator")) {
            return 10;
        }
        if (strFieldName.equals("MakeDate")) {
            return 11;
        }
        if (strFieldName.equals("MakeTime")) {
            return 12;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 13;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 14;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "CaseNo";
            break;
        case 1:
            strFieldName = "RgtNo";
            break;
        case 2:
            strFieldName = "MainFeeNo";
            break;
        case 3:
            strFieldName = "SerialNo";
            break;
        case 4:
            strFieldName = "DownLimit";
            break;
        case 5:
            strFieldName = "UpLimit";
            break;
        case 6:
            strFieldName = "PlanPayRate";
            break;
        case 7:
            strFieldName = "PlanFee";
            break;
        case 8:
            strFieldName = "SelfPay";
            break;
        case 9:
            strFieldName = "MngCom";
            break;
        case 10:
            strFieldName = "Operator";
            break;
        case 11:
            strFieldName = "MakeDate";
            break;
        case 12:
            strFieldName = "MakeTime";
            break;
        case 13:
            strFieldName = "ModifyDate";
            break;
        case 14:
            strFieldName = "ModifyTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("CaseNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RgtNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MainFeeNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SerialNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DownLimit")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("UpLimit")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("PlanPayRate")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("PlanFee")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("SelfPay")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("MngCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 5:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 6:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 7:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 8:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 12:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 13:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 14:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
