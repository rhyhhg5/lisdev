/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.InterfaceTableDB;

/*
 * <p>ClassName: InterfaceTableSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_Update
 * @CreateDate：2014-09-10
 */
public class InterfaceTableSchema implements Schema, Cloneable
{
	// @Field
	/** 流水号 */
	private String SerialNo;
	/** 批次号 */
	private String BatchNo;
	/** 核心业务系统凭证类型 */
	private String ImportType;
	/** 借贷标识 */
	private String DCFlag;
	/** 凭证类型 */
	private String VoucherType;
	/** 币种 */
	private String Currency;
	/** 机构代码 */
	private String DepCode;
	/** 对方机构 */
	private String FinMangerCount;
	/** 科目代码 */
	private String AccountCode;
	/** 客户 */
	private String BClient;
	/** 成本中心 */
	private String CostCenter;
	/** 险种编码 */
	private String RiskCode;
	/** 市场类型 */
	private String MarketType;
	/** 渠道 */
	private String Chinal;
	/** 期数 */
	private String PCont;
	/** 现金流量码 */
	private String CashFlowNo;
	/** 记帐日期 */
	private Date ChargeDate;
	/** 记帐时间 */
	private String ChargeTime;
	/** 金额 */
	private double SumMoney;
	/** 凭证号 */
	private String VoucherID;
	/** 读取状态 */
	private String ReadState;
	/** 凭证年度 */
	private String VoucherYear;
	/** 回写凭证日期 */
	private Date BackVDate;
	/** 回写凭证时间 */
	private String BackVTime;
	/** 读取日期 */
	private Date ReadDate;
	/** 读取时间 */
	private String ReadTime;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 操作员 */
	private String Operator;
	/** 保单年度 */
	private int PolYear;
	/** 保险年期 */
	private int Years;
	/** 供应商编码 */
	private String AgentNo;
	/** 保费收入类型 */
	private String PremiumType;
	/** 缴费首年 */
	private String FirstYear;
	/** 保单号 */
	private String ContNo;

	public static final int FIELDNUM = 37;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public InterfaceTableSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "SerialNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		InterfaceTableSchema cloned = (InterfaceTableSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getSerialNo()
	{
		return SerialNo;
	}
	public void setSerialNo(String aSerialNo)
	{
		SerialNo = aSerialNo;
	}
	public String getBatchNo()
	{
		return BatchNo;
	}
	public void setBatchNo(String aBatchNo)
	{
		BatchNo = aBatchNo;
	}
	public String getImportType()
	{
		return ImportType;
	}
	public void setImportType(String aImportType)
	{
		ImportType = aImportType;
	}
	public String getDCFlag()
	{
		return DCFlag;
	}
	public void setDCFlag(String aDCFlag)
	{
		DCFlag = aDCFlag;
	}
	public String getVoucherType()
	{
		return VoucherType;
	}
	public void setVoucherType(String aVoucherType)
	{
		VoucherType = aVoucherType;
	}
	public String getCurrency()
	{
		return Currency;
	}
	public void setCurrency(String aCurrency)
	{
		Currency = aCurrency;
	}
	public String getDepCode()
	{
		return DepCode;
	}
	public void setDepCode(String aDepCode)
	{
		DepCode = aDepCode;
	}
	public String getFinMangerCount()
	{
		return FinMangerCount;
	}
	public void setFinMangerCount(String aFinMangerCount)
	{
		FinMangerCount = aFinMangerCount;
	}
	public String getAccountCode()
	{
		return AccountCode;
	}
	public void setAccountCode(String aAccountCode)
	{
		AccountCode = aAccountCode;
	}
	public String getBClient()
	{
		return BClient;
	}
	public void setBClient(String aBClient)
	{
		BClient = aBClient;
	}
	public String getCostCenter()
	{
		return CostCenter;
	}
	public void setCostCenter(String aCostCenter)
	{
		CostCenter = aCostCenter;
	}
	public String getRiskCode()
	{
		return RiskCode;
	}
	public void setRiskCode(String aRiskCode)
	{
		RiskCode = aRiskCode;
	}
	public String getMarketType()
	{
		return MarketType;
	}
	public void setMarketType(String aMarketType)
	{
		MarketType = aMarketType;
	}
	public String getChinal()
	{
		return Chinal;
	}
	public void setChinal(String aChinal)
	{
		Chinal = aChinal;
	}
	public String getPCont()
	{
		return PCont;
	}
	public void setPCont(String aPCont)
	{
		PCont = aPCont;
	}
	public String getCashFlowNo()
	{
		return CashFlowNo;
	}
	public void setCashFlowNo(String aCashFlowNo)
	{
		CashFlowNo = aCashFlowNo;
	}
	public String getChargeDate()
	{
		if( ChargeDate != null )
			return fDate.getString(ChargeDate);
		else
			return null;
	}
	public void setChargeDate(Date aChargeDate)
	{
		ChargeDate = aChargeDate;
	}
	public void setChargeDate(String aChargeDate)
	{
		if (aChargeDate != null && !aChargeDate.equals("") )
		{
			ChargeDate = fDate.getDate( aChargeDate );
		}
		else
			ChargeDate = null;
	}

	public String getChargeTime()
	{
		return ChargeTime;
	}
	public void setChargeTime(String aChargeTime)
	{
		ChargeTime = aChargeTime;
	}
	public double getSumMoney()
	{
		return SumMoney;
	}
	public void setSumMoney(double aSumMoney)
	{
		SumMoney = Arith.round(aSumMoney,2);
	}
	public void setSumMoney(String aSumMoney)
	{
		if (aSumMoney != null && !aSumMoney.equals(""))
		{
			Double tDouble = new Double(aSumMoney);
			double d = tDouble.doubleValue();
                SumMoney = Arith.round(d,2);
		}
	}

	public String getVoucherID()
	{
		return VoucherID;
	}
	public void setVoucherID(String aVoucherID)
	{
		VoucherID = aVoucherID;
	}
	public String getReadState()
	{
		return ReadState;
	}
	public void setReadState(String aReadState)
	{
		ReadState = aReadState;
	}
	public String getVoucherYear()
	{
		return VoucherYear;
	}
	public void setVoucherYear(String aVoucherYear)
	{
		VoucherYear = aVoucherYear;
	}
	public String getBackVDate()
	{
		if( BackVDate != null )
			return fDate.getString(BackVDate);
		else
			return null;
	}
	public void setBackVDate(Date aBackVDate)
	{
		BackVDate = aBackVDate;
	}
	public void setBackVDate(String aBackVDate)
	{
		if (aBackVDate != null && !aBackVDate.equals("") )
		{
			BackVDate = fDate.getDate( aBackVDate );
		}
		else
			BackVDate = null;
	}

	public String getBackVTime()
	{
		return BackVTime;
	}
	public void setBackVTime(String aBackVTime)
	{
		BackVTime = aBackVTime;
	}
	public String getReadDate()
	{
		if( ReadDate != null )
			return fDate.getString(ReadDate);
		else
			return null;
	}
	public void setReadDate(Date aReadDate)
	{
		ReadDate = aReadDate;
	}
	public void setReadDate(String aReadDate)
	{
		if (aReadDate != null && !aReadDate.equals("") )
		{
			ReadDate = fDate.getDate( aReadDate );
		}
		else
			ReadDate = null;
	}

	public String getReadTime()
	{
		return ReadTime;
	}
	public void setReadTime(String aReadTime)
	{
		ReadTime = aReadTime;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public int getPolYear()
	{
		return PolYear;
	}
	public void setPolYear(int aPolYear)
	{
		PolYear = aPolYear;
	}
	public void setPolYear(String aPolYear)
	{
		if (aPolYear != null && !aPolYear.equals(""))
		{
			Integer tInteger = new Integer(aPolYear);
			int i = tInteger.intValue();
			PolYear = i;
		}
	}

	public int getYears()
	{
		return Years;
	}
	public void setYears(int aYears)
	{
		Years = aYears;
	}
	public void setYears(String aYears)
	{
		if (aYears != null && !aYears.equals(""))
		{
			Integer tInteger = new Integer(aYears);
			int i = tInteger.intValue();
			Years = i;
		}
	}

	public String getAgentNo()
	{
		return AgentNo;
	}
	public void setAgentNo(String aAgentNo)
	{
		AgentNo = aAgentNo;
	}
	public String getPremiumType()
	{
		return PremiumType;
	}
	public void setPremiumType(String aPremiumType)
	{
		PremiumType = aPremiumType;
	}
	public String getFirstYear()
	{
		return FirstYear;
	}
	public void setFirstYear(String aFirstYear)
	{
		FirstYear = aFirstYear;
	}
	public String getContNo()
	{
		return ContNo;
	}
	public void setContNo(String aContNo)
	{
		ContNo = aContNo;
	}

	/**
	* 使用另外一个 InterfaceTableSchema 对象给 Schema 赋值
	* @param: aInterfaceTableSchema InterfaceTableSchema
	**/
	public void setSchema(InterfaceTableSchema aInterfaceTableSchema)
	{
		this.SerialNo = aInterfaceTableSchema.getSerialNo();
		this.BatchNo = aInterfaceTableSchema.getBatchNo();
		this.ImportType = aInterfaceTableSchema.getImportType();
		this.DCFlag = aInterfaceTableSchema.getDCFlag();
		this.VoucherType = aInterfaceTableSchema.getVoucherType();
		this.Currency = aInterfaceTableSchema.getCurrency();
		this.DepCode = aInterfaceTableSchema.getDepCode();
		this.FinMangerCount = aInterfaceTableSchema.getFinMangerCount();
		this.AccountCode = aInterfaceTableSchema.getAccountCode();
		this.BClient = aInterfaceTableSchema.getBClient();
		this.CostCenter = aInterfaceTableSchema.getCostCenter();
		this.RiskCode = aInterfaceTableSchema.getRiskCode();
		this.MarketType = aInterfaceTableSchema.getMarketType();
		this.Chinal = aInterfaceTableSchema.getChinal();
		this.PCont = aInterfaceTableSchema.getPCont();
		this.CashFlowNo = aInterfaceTableSchema.getCashFlowNo();
		this.ChargeDate = fDate.getDate( aInterfaceTableSchema.getChargeDate());
		this.ChargeTime = aInterfaceTableSchema.getChargeTime();
		this.SumMoney = aInterfaceTableSchema.getSumMoney();
		this.VoucherID = aInterfaceTableSchema.getVoucherID();
		this.ReadState = aInterfaceTableSchema.getReadState();
		this.VoucherYear = aInterfaceTableSchema.getVoucherYear();
		this.BackVDate = fDate.getDate( aInterfaceTableSchema.getBackVDate());
		this.BackVTime = aInterfaceTableSchema.getBackVTime();
		this.ReadDate = fDate.getDate( aInterfaceTableSchema.getReadDate());
		this.ReadTime = aInterfaceTableSchema.getReadTime();
		this.MakeDate = fDate.getDate( aInterfaceTableSchema.getMakeDate());
		this.MakeTime = aInterfaceTableSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aInterfaceTableSchema.getModifyDate());
		this.ModifyTime = aInterfaceTableSchema.getModifyTime();
		this.Operator = aInterfaceTableSchema.getOperator();
		this.PolYear = aInterfaceTableSchema.getPolYear();
		this.Years = aInterfaceTableSchema.getYears();
		this.AgentNo = aInterfaceTableSchema.getAgentNo();
		this.PremiumType = aInterfaceTableSchema.getPremiumType();
		this.FirstYear = aInterfaceTableSchema.getFirstYear();
		this.ContNo = aInterfaceTableSchema.getContNo();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("SerialNo") == null )
				this.SerialNo = null;
			else
				this.SerialNo = rs.getString("SerialNo").trim();

			if( rs.getString("BatchNo") == null )
				this.BatchNo = null;
			else
				this.BatchNo = rs.getString("BatchNo").trim();

			if( rs.getString("ImportType") == null )
				this.ImportType = null;
			else
				this.ImportType = rs.getString("ImportType").trim();

			if( rs.getString("DCFlag") == null )
				this.DCFlag = null;
			else
				this.DCFlag = rs.getString("DCFlag").trim();

			if( rs.getString("VoucherType") == null )
				this.VoucherType = null;
			else
				this.VoucherType = rs.getString("VoucherType").trim();

			if( rs.getString("Currency") == null )
				this.Currency = null;
			else
				this.Currency = rs.getString("Currency").trim();

			if( rs.getString("DepCode") == null )
				this.DepCode = null;
			else
				this.DepCode = rs.getString("DepCode").trim();

			if( rs.getString("FinMangerCount") == null )
				this.FinMangerCount = null;
			else
				this.FinMangerCount = rs.getString("FinMangerCount").trim();

			if( rs.getString("AccountCode") == null )
				this.AccountCode = null;
			else
				this.AccountCode = rs.getString("AccountCode").trim();

			if( rs.getString("BClient") == null )
				this.BClient = null;
			else
				this.BClient = rs.getString("BClient").trim();

			if( rs.getString("CostCenter") == null )
				this.CostCenter = null;
			else
				this.CostCenter = rs.getString("CostCenter").trim();

			if( rs.getString("RiskCode") == null )
				this.RiskCode = null;
			else
				this.RiskCode = rs.getString("RiskCode").trim();

			if( rs.getString("MarketType") == null )
				this.MarketType = null;
			else
				this.MarketType = rs.getString("MarketType").trim();

			if( rs.getString("Chinal") == null )
				this.Chinal = null;
			else
				this.Chinal = rs.getString("Chinal").trim();

			if( rs.getString("PCont") == null )
				this.PCont = null;
			else
				this.PCont = rs.getString("PCont").trim();

			if( rs.getString("CashFlowNo") == null )
				this.CashFlowNo = null;
			else
				this.CashFlowNo = rs.getString("CashFlowNo").trim();

			this.ChargeDate = rs.getDate("ChargeDate");
			if( rs.getString("ChargeTime") == null )
				this.ChargeTime = null;
			else
				this.ChargeTime = rs.getString("ChargeTime").trim();

			this.SumMoney = rs.getDouble("SumMoney");
			if( rs.getString("VoucherID") == null )
				this.VoucherID = null;
			else
				this.VoucherID = rs.getString("VoucherID").trim();

			if( rs.getString("ReadState") == null )
				this.ReadState = null;
			else
				this.ReadState = rs.getString("ReadState").trim();

			if( rs.getString("VoucherYear") == null )
				this.VoucherYear = null;
			else
				this.VoucherYear = rs.getString("VoucherYear").trim();

			this.BackVDate = rs.getDate("BackVDate");
			if( rs.getString("BackVTime") == null )
				this.BackVTime = null;
			else
				this.BackVTime = rs.getString("BackVTime").trim();

			this.ReadDate = rs.getDate("ReadDate");
			if( rs.getString("ReadTime") == null )
				this.ReadTime = null;
			else
				this.ReadTime = rs.getString("ReadTime").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.PolYear = rs.getInt("PolYear");
			this.Years = rs.getInt("Years");
			if( rs.getString("AgentNo") == null )
				this.AgentNo = null;
			else
				this.AgentNo = rs.getString("AgentNo").trim();

			if( rs.getString("PremiumType") == null )
				this.PremiumType = null;
			else
				this.PremiumType = rs.getString("PremiumType").trim();

			if( rs.getString("FirstYear") == null )
				this.FirstYear = null;
			else
				this.FirstYear = rs.getString("FirstYear").trim();

			if( rs.getString("ContNo") == null )
				this.ContNo = null;
			else
				this.ContNo = rs.getString("ContNo").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的InterfaceTable表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "InterfaceTableSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public InterfaceTableSchema getSchema()
	{
		InterfaceTableSchema aInterfaceTableSchema = new InterfaceTableSchema();
		aInterfaceTableSchema.setSchema(this);
		return aInterfaceTableSchema;
	}

	public InterfaceTableDB getDB()
	{
		InterfaceTableDB aDBOper = new InterfaceTableDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpInterfaceTable描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(SerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BatchNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ImportType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DCFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(VoucherType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Currency)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DepCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FinMangerCount)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccountCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BClient)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CostCenter)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MarketType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Chinal)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PCont)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CashFlowNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ChargeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ChargeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SumMoney));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(VoucherID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReadState)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(VoucherYear)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( BackVDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BackVTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ReadDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReadTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PolYear));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Years));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PremiumType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FirstYear)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContNo));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpInterfaceTable>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			BatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			ImportType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			DCFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			VoucherType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			Currency = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			DepCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			FinMangerCount = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			AccountCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			BClient = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			CostCenter = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			MarketType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			Chinal = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			PCont = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			CashFlowNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			ChargeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,SysConst.PACKAGESPILTER));
			ChargeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			SumMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,19,SysConst.PACKAGESPILTER))).doubleValue();
			VoucherID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			ReadState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			VoucherYear = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			BackVDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,SysConst.PACKAGESPILTER));
			BackVTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			ReadDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,SysConst.PACKAGESPILTER));
			ReadTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			PolYear= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,32,SysConst.PACKAGESPILTER))).intValue();
			Years= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,33,SysConst.PACKAGESPILTER))).intValue();
			AgentNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			PremiumType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			FirstYear = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
			ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "InterfaceTableSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("SerialNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
		}
		if (FCode.equals("BatchNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BatchNo));
		}
		if (FCode.equals("ImportType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ImportType));
		}
		if (FCode.equals("DCFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DCFlag));
		}
		if (FCode.equals("VoucherType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(VoucherType));
		}
		if (FCode.equals("Currency"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Currency));
		}
		if (FCode.equals("DepCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DepCode));
		}
		if (FCode.equals("FinMangerCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FinMangerCount));
		}
		if (FCode.equals("AccountCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccountCode));
		}
		if (FCode.equals("BClient"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BClient));
		}
		if (FCode.equals("CostCenter"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CostCenter));
		}
		if (FCode.equals("RiskCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
		}
		if (FCode.equals("MarketType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MarketType));
		}
		if (FCode.equals("Chinal"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Chinal));
		}
		if (FCode.equals("PCont"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PCont));
		}
		if (FCode.equals("CashFlowNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CashFlowNo));
		}
		if (FCode.equals("ChargeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getChargeDate()));
		}
		if (FCode.equals("ChargeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ChargeTime));
		}
		if (FCode.equals("SumMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SumMoney));
		}
		if (FCode.equals("VoucherID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(VoucherID));
		}
		if (FCode.equals("ReadState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReadState));
		}
		if (FCode.equals("VoucherYear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(VoucherYear));
		}
		if (FCode.equals("BackVDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getBackVDate()));
		}
		if (FCode.equals("BackVTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BackVTime));
		}
		if (FCode.equals("ReadDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getReadDate()));
		}
		if (FCode.equals("ReadTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReadTime));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("PolYear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolYear));
		}
		if (FCode.equals("Years"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Years));
		}
		if (FCode.equals("AgentNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentNo));
		}
		if (FCode.equals("PremiumType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PremiumType));
		}
		if (FCode.equals("FirstYear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FirstYear));
		}
		if (FCode.equals("ContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(SerialNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(BatchNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(ImportType);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(DCFlag);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(VoucherType);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(Currency);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(DepCode);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(FinMangerCount);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(AccountCode);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(BClient);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(CostCenter);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(RiskCode);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(MarketType);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(Chinal);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(PCont);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(CashFlowNo);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getChargeDate()));
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(ChargeTime);
				break;
			case 18:
				strFieldValue = String.valueOf(SumMoney);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(VoucherID);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(ReadState);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(VoucherYear);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getBackVDate()));
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(BackVTime);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getReadDate()));
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(ReadTime);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 31:
				strFieldValue = String.valueOf(PolYear);
				break;
			case 32:
				strFieldValue = String.valueOf(Years);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(AgentNo);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(PremiumType);
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(FirstYear);
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(ContNo);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("SerialNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SerialNo = FValue.trim();
			}
			else
				SerialNo = null;
		}
		if (FCode.equalsIgnoreCase("BatchNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BatchNo = FValue.trim();
			}
			else
				BatchNo = null;
		}
		if (FCode.equalsIgnoreCase("ImportType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ImportType = FValue.trim();
			}
			else
				ImportType = null;
		}
		if (FCode.equalsIgnoreCase("DCFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DCFlag = FValue.trim();
			}
			else
				DCFlag = null;
		}
		if (FCode.equalsIgnoreCase("VoucherType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				VoucherType = FValue.trim();
			}
			else
				VoucherType = null;
		}
		if (FCode.equalsIgnoreCase("Currency"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Currency = FValue.trim();
			}
			else
				Currency = null;
		}
		if (FCode.equalsIgnoreCase("DepCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DepCode = FValue.trim();
			}
			else
				DepCode = null;
		}
		if (FCode.equalsIgnoreCase("FinMangerCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FinMangerCount = FValue.trim();
			}
			else
				FinMangerCount = null;
		}
		if (FCode.equalsIgnoreCase("AccountCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccountCode = FValue.trim();
			}
			else
				AccountCode = null;
		}
		if (FCode.equalsIgnoreCase("BClient"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BClient = FValue.trim();
			}
			else
				BClient = null;
		}
		if (FCode.equalsIgnoreCase("CostCenter"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CostCenter = FValue.trim();
			}
			else
				CostCenter = null;
		}
		if (FCode.equalsIgnoreCase("RiskCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskCode = FValue.trim();
			}
			else
				RiskCode = null;
		}
		if (FCode.equalsIgnoreCase("MarketType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MarketType = FValue.trim();
			}
			else
				MarketType = null;
		}
		if (FCode.equalsIgnoreCase("Chinal"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Chinal = FValue.trim();
			}
			else
				Chinal = null;
		}
		if (FCode.equalsIgnoreCase("PCont"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PCont = FValue.trim();
			}
			else
				PCont = null;
		}
		if (FCode.equalsIgnoreCase("CashFlowNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CashFlowNo = FValue.trim();
			}
			else
				CashFlowNo = null;
		}
		if (FCode.equalsIgnoreCase("ChargeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ChargeDate = fDate.getDate( FValue );
			}
			else
				ChargeDate = null;
		}
		if (FCode.equalsIgnoreCase("ChargeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ChargeTime = FValue.trim();
			}
			else
				ChargeTime = null;
		}
		if (FCode.equalsIgnoreCase("SumMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SumMoney = d;
			}
		}
		if (FCode.equalsIgnoreCase("VoucherID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				VoucherID = FValue.trim();
			}
			else
				VoucherID = null;
		}
		if (FCode.equalsIgnoreCase("ReadState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReadState = FValue.trim();
			}
			else
				ReadState = null;
		}
		if (FCode.equalsIgnoreCase("VoucherYear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				VoucherYear = FValue.trim();
			}
			else
				VoucherYear = null;
		}
		if (FCode.equalsIgnoreCase("BackVDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				BackVDate = fDate.getDate( FValue );
			}
			else
				BackVDate = null;
		}
		if (FCode.equalsIgnoreCase("BackVTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BackVTime = FValue.trim();
			}
			else
				BackVTime = null;
		}
		if (FCode.equalsIgnoreCase("ReadDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ReadDate = fDate.getDate( FValue );
			}
			else
				ReadDate = null;
		}
		if (FCode.equalsIgnoreCase("ReadTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReadTime = FValue.trim();
			}
			else
				ReadTime = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("PolYear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PolYear = i;
			}
		}
		if (FCode.equalsIgnoreCase("Years"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				Years = i;
			}
		}
		if (FCode.equalsIgnoreCase("AgentNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentNo = FValue.trim();
			}
			else
				AgentNo = null;
		}
		if (FCode.equalsIgnoreCase("PremiumType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PremiumType = FValue.trim();
			}
			else
				PremiumType = null;
		}
		if (FCode.equalsIgnoreCase("FirstYear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FirstYear = FValue.trim();
			}
			else
				FirstYear = null;
		}
		if (FCode.equalsIgnoreCase("ContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContNo = FValue.trim();
			}
			else
				ContNo = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		InterfaceTableSchema other = (InterfaceTableSchema)otherObject;
		return
			(SerialNo == null ? other.getSerialNo() == null : SerialNo.equals(other.getSerialNo()))
			&& (BatchNo == null ? other.getBatchNo() == null : BatchNo.equals(other.getBatchNo()))
			&& (ImportType == null ? other.getImportType() == null : ImportType.equals(other.getImportType()))
			&& (DCFlag == null ? other.getDCFlag() == null : DCFlag.equals(other.getDCFlag()))
			&& (VoucherType == null ? other.getVoucherType() == null : VoucherType.equals(other.getVoucherType()))
			&& (Currency == null ? other.getCurrency() == null : Currency.equals(other.getCurrency()))
			&& (DepCode == null ? other.getDepCode() == null : DepCode.equals(other.getDepCode()))
			&& (FinMangerCount == null ? other.getFinMangerCount() == null : FinMangerCount.equals(other.getFinMangerCount()))
			&& (AccountCode == null ? other.getAccountCode() == null : AccountCode.equals(other.getAccountCode()))
			&& (BClient == null ? other.getBClient() == null : BClient.equals(other.getBClient()))
			&& (CostCenter == null ? other.getCostCenter() == null : CostCenter.equals(other.getCostCenter()))
			&& (RiskCode == null ? other.getRiskCode() == null : RiskCode.equals(other.getRiskCode()))
			&& (MarketType == null ? other.getMarketType() == null : MarketType.equals(other.getMarketType()))
			&& (Chinal == null ? other.getChinal() == null : Chinal.equals(other.getChinal()))
			&& (PCont == null ? other.getPCont() == null : PCont.equals(other.getPCont()))
			&& (CashFlowNo == null ? other.getCashFlowNo() == null : CashFlowNo.equals(other.getCashFlowNo()))
			&& (ChargeDate == null ? other.getChargeDate() == null : fDate.getString(ChargeDate).equals(other.getChargeDate()))
			&& (ChargeTime == null ? other.getChargeTime() == null : ChargeTime.equals(other.getChargeTime()))
			&& SumMoney == other.getSumMoney()
			&& (VoucherID == null ? other.getVoucherID() == null : VoucherID.equals(other.getVoucherID()))
			&& (ReadState == null ? other.getReadState() == null : ReadState.equals(other.getReadState()))
			&& (VoucherYear == null ? other.getVoucherYear() == null : VoucherYear.equals(other.getVoucherYear()))
			&& (BackVDate == null ? other.getBackVDate() == null : fDate.getString(BackVDate).equals(other.getBackVDate()))
			&& (BackVTime == null ? other.getBackVTime() == null : BackVTime.equals(other.getBackVTime()))
			&& (ReadDate == null ? other.getReadDate() == null : fDate.getString(ReadDate).equals(other.getReadDate()))
			&& (ReadTime == null ? other.getReadTime() == null : ReadTime.equals(other.getReadTime()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& PolYear == other.getPolYear()
			&& Years == other.getYears()
			&& (AgentNo == null ? other.getAgentNo() == null : AgentNo.equals(other.getAgentNo()))
			&& (PremiumType == null ? other.getPremiumType() == null : PremiumType.equals(other.getPremiumType()))
			&& (FirstYear == null ? other.getFirstYear() == null : FirstYear.equals(other.getFirstYear()))
			&& (ContNo == null ? other.getContNo() == null : ContNo.equals(other.getContNo()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("SerialNo") ) {
			return 0;
		}
		if( strFieldName.equals("BatchNo") ) {
			return 1;
		}
		if( strFieldName.equals("ImportType") ) {
			return 2;
		}
		if( strFieldName.equals("DCFlag") ) {
			return 3;
		}
		if( strFieldName.equals("VoucherType") ) {
			return 4;
		}
		if( strFieldName.equals("Currency") ) {
			return 5;
		}
		if( strFieldName.equals("DepCode") ) {
			return 6;
		}
		if( strFieldName.equals("FinMangerCount") ) {
			return 7;
		}
		if( strFieldName.equals("AccountCode") ) {
			return 8;
		}
		if( strFieldName.equals("BClient") ) {
			return 9;
		}
		if( strFieldName.equals("CostCenter") ) {
			return 10;
		}
		if( strFieldName.equals("RiskCode") ) {
			return 11;
		}
		if( strFieldName.equals("MarketType") ) {
			return 12;
		}
		if( strFieldName.equals("Chinal") ) {
			return 13;
		}
		if( strFieldName.equals("PCont") ) {
			return 14;
		}
		if( strFieldName.equals("CashFlowNo") ) {
			return 15;
		}
		if( strFieldName.equals("ChargeDate") ) {
			return 16;
		}
		if( strFieldName.equals("ChargeTime") ) {
			return 17;
		}
		if( strFieldName.equals("SumMoney") ) {
			return 18;
		}
		if( strFieldName.equals("VoucherID") ) {
			return 19;
		}
		if( strFieldName.equals("ReadState") ) {
			return 20;
		}
		if( strFieldName.equals("VoucherYear") ) {
			return 21;
		}
		if( strFieldName.equals("BackVDate") ) {
			return 22;
		}
		if( strFieldName.equals("BackVTime") ) {
			return 23;
		}
		if( strFieldName.equals("ReadDate") ) {
			return 24;
		}
		if( strFieldName.equals("ReadTime") ) {
			return 25;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 26;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 27;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 28;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 29;
		}
		if( strFieldName.equals("Operator") ) {
			return 30;
		}
		if( strFieldName.equals("PolYear") ) {
			return 31;
		}
		if( strFieldName.equals("Years") ) {
			return 32;
		}
		if( strFieldName.equals("AgentNo") ) {
			return 33;
		}
		if( strFieldName.equals("PremiumType") ) {
			return 34;
		}
		if( strFieldName.equals("FirstYear") ) {
			return 35;
		}
		if( strFieldName.equals("ContNo") ) {
			return 36;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "SerialNo";
				break;
			case 1:
				strFieldName = "BatchNo";
				break;
			case 2:
				strFieldName = "ImportType";
				break;
			case 3:
				strFieldName = "DCFlag";
				break;
			case 4:
				strFieldName = "VoucherType";
				break;
			case 5:
				strFieldName = "Currency";
				break;
			case 6:
				strFieldName = "DepCode";
				break;
			case 7:
				strFieldName = "FinMangerCount";
				break;
			case 8:
				strFieldName = "AccountCode";
				break;
			case 9:
				strFieldName = "BClient";
				break;
			case 10:
				strFieldName = "CostCenter";
				break;
			case 11:
				strFieldName = "RiskCode";
				break;
			case 12:
				strFieldName = "MarketType";
				break;
			case 13:
				strFieldName = "Chinal";
				break;
			case 14:
				strFieldName = "PCont";
				break;
			case 15:
				strFieldName = "CashFlowNo";
				break;
			case 16:
				strFieldName = "ChargeDate";
				break;
			case 17:
				strFieldName = "ChargeTime";
				break;
			case 18:
				strFieldName = "SumMoney";
				break;
			case 19:
				strFieldName = "VoucherID";
				break;
			case 20:
				strFieldName = "ReadState";
				break;
			case 21:
				strFieldName = "VoucherYear";
				break;
			case 22:
				strFieldName = "BackVDate";
				break;
			case 23:
				strFieldName = "BackVTime";
				break;
			case 24:
				strFieldName = "ReadDate";
				break;
			case 25:
				strFieldName = "ReadTime";
				break;
			case 26:
				strFieldName = "MakeDate";
				break;
			case 27:
				strFieldName = "MakeTime";
				break;
			case 28:
				strFieldName = "ModifyDate";
				break;
			case 29:
				strFieldName = "ModifyTime";
				break;
			case 30:
				strFieldName = "Operator";
				break;
			case 31:
				strFieldName = "PolYear";
				break;
			case 32:
				strFieldName = "Years";
				break;
			case 33:
				strFieldName = "AgentNo";
				break;
			case 34:
				strFieldName = "PremiumType";
				break;
			case 35:
				strFieldName = "FirstYear";
				break;
			case 36:
				strFieldName = "ContNo";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("SerialNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BatchNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ImportType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DCFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("VoucherType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Currency") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DepCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FinMangerCount") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccountCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BClient") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CostCenter") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MarketType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Chinal") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PCont") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CashFlowNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ChargeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ChargeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SumMoney") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("VoucherID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReadState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("VoucherYear") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BackVDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("BackVTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReadDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ReadTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolYear") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("Years") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("AgentNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PremiumType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FirstYear") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContNo") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_INT;
				break;
			case 32:
				nFieldType = Schema.TYPE_INT;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 36:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
