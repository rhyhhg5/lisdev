/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LCInvoiceInfoDB;

/*
 * <p>ClassName: LCInvoiceInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 发票打印导出接口
 * @CreateDate：2007-05-25
 */
public class LCInvoiceInfoSchema implements Schema, Cloneable
{
	// @Field
	/** 机构代码 */
	private String ComCode;
	/** 纳税人识别号 */
	private String TaxpayerNo;
	/** 纳税人名称 */
	private String TaxpayerName;
	/** 发票代码 */
	private String InvoiceCode;
	/** 发票扩展代码 */
	private String InvoiceCodeExp;
	/** 发票起始号 */
	private String InvoiceStartNo;
	/** 发票终止号 */
	private String InvoiceEndNo;

	public static final int FIELDNUM = 7;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LCInvoiceInfoSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[0];

		PK = pk;
	}

            /**
             * Schema克隆
             * @return Object
             * @throws CloneNotSupportedException
             */
            public Object clone()
                    throws CloneNotSupportedException
            {
                LCInvoiceInfoSchema cloned = (LCInvoiceInfoSchema)super.clone();
                cloned.mErrors = (CErrors) mErrors.clone();
                return cloned;
            }

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getComCode()
	{
		return ComCode;
	}
	public void setComCode(String aComCode)
	{
            ComCode = aComCode;
	}
	public String getTaxpayerNo()
	{
		return TaxpayerNo;
	}
	public void setTaxpayerNo(String aTaxpayerNo)
	{
            TaxpayerNo = aTaxpayerNo;
	}
	public String getTaxpayerName()
	{
		return TaxpayerName;
	}
	public void setTaxpayerName(String aTaxpayerName)
	{
            TaxpayerName = aTaxpayerName;
	}
	public String getInvoiceCode()
	{
		return InvoiceCode;
	}
	public void setInvoiceCode(String aInvoiceCode)
	{
            InvoiceCode = aInvoiceCode;
	}
	public String getInvoiceCodeExp()
	{
		return InvoiceCodeExp;
	}
	public void setInvoiceCodeExp(String aInvoiceCodeExp)
	{
            InvoiceCodeExp = aInvoiceCodeExp;
	}
	public String getInvoiceStartNo()
	{
		return InvoiceStartNo;
	}
	public void setInvoiceStartNo(String aInvoiceStartNo)
	{
            InvoiceStartNo = aInvoiceStartNo;
	}
	public String getInvoiceEndNo()
	{
		return InvoiceEndNo;
	}
	public void setInvoiceEndNo(String aInvoiceEndNo)
	{
            InvoiceEndNo = aInvoiceEndNo;
	}

	/**
	* 使用另外一个 LCInvoiceInfoSchema 对象给 Schema 赋值
	* @param: aLCInvoiceInfoSchema LCInvoiceInfoSchema
	**/
	public void setSchema(LCInvoiceInfoSchema aLCInvoiceInfoSchema)
	{
		this.ComCode = aLCInvoiceInfoSchema.getComCode();
		this.TaxpayerNo = aLCInvoiceInfoSchema.getTaxpayerNo();
		this.TaxpayerName = aLCInvoiceInfoSchema.getTaxpayerName();
		this.InvoiceCode = aLCInvoiceInfoSchema.getInvoiceCode();
		this.InvoiceCodeExp = aLCInvoiceInfoSchema.getInvoiceCodeExp();
		this.InvoiceStartNo = aLCInvoiceInfoSchema.getInvoiceStartNo();
		this.InvoiceEndNo = aLCInvoiceInfoSchema.getInvoiceEndNo();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("ComCode") == null )
				this.ComCode = null;
			else
				this.ComCode = rs.getString("ComCode").trim();

			if( rs.getString("TaxpayerNo") == null )
				this.TaxpayerNo = null;
			else
				this.TaxpayerNo = rs.getString("TaxpayerNo").trim();

			if( rs.getString("TaxpayerName") == null )
				this.TaxpayerName = null;
			else
				this.TaxpayerName = rs.getString("TaxpayerName").trim();

			if( rs.getString("InvoiceCode") == null )
				this.InvoiceCode = null;
			else
				this.InvoiceCode = rs.getString("InvoiceCode").trim();

			if( rs.getString("InvoiceCodeExp") == null )
				this.InvoiceCodeExp = null;
			else
				this.InvoiceCodeExp = rs.getString("InvoiceCodeExp").trim();

			if( rs.getString("InvoiceStartNo") == null )
				this.InvoiceStartNo = null;
			else
				this.InvoiceStartNo = rs.getString("InvoiceStartNo").trim();

			if( rs.getString("InvoiceEndNo") == null )
				this.InvoiceEndNo = null;
			else
				this.InvoiceEndNo = rs.getString("InvoiceEndNo").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LCInvoiceInfo表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCInvoiceInfoSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LCInvoiceInfoSchema getSchema()
	{
		LCInvoiceInfoSchema aLCInvoiceInfoSchema = new LCInvoiceInfoSchema();
		aLCInvoiceInfoSchema.setSchema(this);
		return aLCInvoiceInfoSchema;
	}

	public LCInvoiceInfoDB getDB()
	{
		LCInvoiceInfoDB aDBOper = new LCInvoiceInfoDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCInvoiceInfo描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append(StrTool.cTrim(ComCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(TaxpayerNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(TaxpayerName)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(InvoiceCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(InvoiceCodeExp)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(InvoiceStartNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(InvoiceEndNo));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCInvoiceInfo>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			ComCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			TaxpayerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			TaxpayerName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			InvoiceCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			InvoiceCodeExp = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			InvoiceStartNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			InvoiceEndNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCInvoiceInfoSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("ComCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ComCode));
		}
		if (FCode.equals("TaxpayerNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TaxpayerNo));
		}
		if (FCode.equals("TaxpayerName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TaxpayerName));
		}
		if (FCode.equals("InvoiceCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InvoiceCode));
		}
		if (FCode.equals("InvoiceCodeExp"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InvoiceCodeExp));
		}
		if (FCode.equals("InvoiceStartNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InvoiceStartNo));
		}
		if (FCode.equals("InvoiceEndNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InvoiceEndNo));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(ComCode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(TaxpayerNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(TaxpayerName);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(InvoiceCode);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(InvoiceCodeExp);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(InvoiceStartNo);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(InvoiceEndNo);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("ComCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ComCode = FValue.trim();
			}
			else
				ComCode = null;
		}
		if (FCode.equalsIgnoreCase("TaxpayerNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TaxpayerNo = FValue.trim();
			}
			else
				TaxpayerNo = null;
		}
		if (FCode.equalsIgnoreCase("TaxpayerName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TaxpayerName = FValue.trim();
			}
			else
				TaxpayerName = null;
		}
		if (FCode.equalsIgnoreCase("InvoiceCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InvoiceCode = FValue.trim();
			}
			else
				InvoiceCode = null;
		}
		if (FCode.equalsIgnoreCase("InvoiceCodeExp"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InvoiceCodeExp = FValue.trim();
			}
			else
				InvoiceCodeExp = null;
		}
		if (FCode.equalsIgnoreCase("InvoiceStartNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InvoiceStartNo = FValue.trim();
			}
			else
				InvoiceStartNo = null;
		}
		if (FCode.equalsIgnoreCase("InvoiceEndNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InvoiceEndNo = FValue.trim();
			}
			else
				InvoiceEndNo = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LCInvoiceInfoSchema other = (LCInvoiceInfoSchema)otherObject;
		return
			ComCode.equals(other.getComCode())
			&& TaxpayerNo.equals(other.getTaxpayerNo())
			&& TaxpayerName.equals(other.getTaxpayerName())
			&& InvoiceCode.equals(other.getInvoiceCode())
			&& InvoiceCodeExp.equals(other.getInvoiceCodeExp())
			&& InvoiceStartNo.equals(other.getInvoiceStartNo())
			&& InvoiceEndNo.equals(other.getInvoiceEndNo());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("ComCode") ) {
			return 0;
		}
		if( strFieldName.equals("TaxpayerNo") ) {
			return 1;
		}
		if( strFieldName.equals("TaxpayerName") ) {
			return 2;
		}
		if( strFieldName.equals("InvoiceCode") ) {
			return 3;
		}
		if( strFieldName.equals("InvoiceCodeExp") ) {
			return 4;
		}
		if( strFieldName.equals("InvoiceStartNo") ) {
			return 5;
		}
		if( strFieldName.equals("InvoiceEndNo") ) {
			return 6;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "ComCode";
				break;
			case 1:
				strFieldName = "TaxpayerNo";
				break;
			case 2:
				strFieldName = "TaxpayerName";
				break;
			case 3:
				strFieldName = "InvoiceCode";
				break;
			case 4:
				strFieldName = "InvoiceCodeExp";
				break;
			case 5:
				strFieldName = "InvoiceStartNo";
				break;
			case 6:
				strFieldName = "InvoiceEndNo";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("ComCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TaxpayerNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TaxpayerName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InvoiceCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InvoiceCodeExp") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InvoiceStartNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InvoiceEndNo") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
