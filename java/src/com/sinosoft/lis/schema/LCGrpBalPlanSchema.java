/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LCGrpBalPlanDB;

/*
 * <p>ClassName: LCGrpBalPlanSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 定期结算
 * @CreateDate：2006-03-14
 */
public class LCGrpBalPlanSchema implements Schema, Cloneable
{
	// @Field
	/** 集体合同号码 */
	private String GrpContNo;
	/** 已结算次数 */
	private int BalTimes;
	/** 起始日期 */
	private Date BalStartDate;
	/** 终止日期 */
	private Date BalEndDate;
	/** 结算日期 */
	private Date BaltoDate;
	/** 下一对应日 */
	private Date NextDate;
	/** 结算频次 */
	private int BalIntv;
	/** 结算金额 */
	private double BalMoney;
	/** 状态 */
	private String State;
	/** 提前结算标志 */
	private String Flag;
	/** 累计结算金额 */
	private double SumBal;
	/** 管理机构 */
	private String ManageCom;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 17;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LCGrpBalPlanSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "GrpContNo";

		PK = pk;
	}

            /**
             * Schema克隆
             * @return Object
             * @throws CloneNotSupportedException
             */
            public Object clone()
                    throws CloneNotSupportedException
            {
                LCGrpBalPlanSchema cloned = (LCGrpBalPlanSchema)super.clone();
                cloned.fDate = (FDate) fDate.clone();
                cloned.mErrors = (CErrors) mErrors.clone();
                return cloned;
            }

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getGrpContNo()
	{
		return GrpContNo;
	}
	public void setGrpContNo(String aGrpContNo)
	{
            GrpContNo = aGrpContNo;
	}
	public int getBalTimes()
	{
		return BalTimes;
	}
	public void setBalTimes(int aBalTimes)
	{
            BalTimes = aBalTimes;
	}
	public void setBalTimes(String aBalTimes)
	{
		if (aBalTimes != null && !aBalTimes.equals(""))
		{
			Integer tInteger = new Integer(aBalTimes);
			int i = tInteger.intValue();
			BalTimes = i;
		}
	}

	public String getBalStartDate()
	{
		if( BalStartDate != null )
			return fDate.getString(BalStartDate);
		else
			return null;
	}
	public void setBalStartDate(Date aBalStartDate)
	{
            BalStartDate = aBalStartDate;
	}
	public void setBalStartDate(String aBalStartDate)
	{
		if (aBalStartDate != null && !aBalStartDate.equals("") )
		{
			BalStartDate = fDate.getDate( aBalStartDate );
		}
		else
			BalStartDate = null;
	}

	public String getBalEndDate()
	{
		if( BalEndDate != null )
			return fDate.getString(BalEndDate);
		else
			return null;
	}
	public void setBalEndDate(Date aBalEndDate)
	{
            BalEndDate = aBalEndDate;
	}
	public void setBalEndDate(String aBalEndDate)
	{
		if (aBalEndDate != null && !aBalEndDate.equals("") )
		{
			BalEndDate = fDate.getDate( aBalEndDate );
		}
		else
			BalEndDate = null;
	}

	public String getBaltoDate()
	{
		if( BaltoDate != null )
			return fDate.getString(BaltoDate);
		else
			return null;
	}
	public void setBaltoDate(Date aBaltoDate)
	{
            BaltoDate = aBaltoDate;
	}
	public void setBaltoDate(String aBaltoDate)
	{
		if (aBaltoDate != null && !aBaltoDate.equals("") )
		{
			BaltoDate = fDate.getDate( aBaltoDate );
		}
		else
			BaltoDate = null;
	}

	public String getNextDate()
	{
		if( NextDate != null )
			return fDate.getString(NextDate);
		else
			return null;
	}
	public void setNextDate(Date aNextDate)
	{
            NextDate = aNextDate;
	}
	public void setNextDate(String aNextDate)
	{
		if (aNextDate != null && !aNextDate.equals("") )
		{
			NextDate = fDate.getDate( aNextDate );
		}
		else
			NextDate = null;
	}

	public int getBalIntv()
	{
		return BalIntv;
	}
	public void setBalIntv(int aBalIntv)
	{
            BalIntv = aBalIntv;
	}
	public void setBalIntv(String aBalIntv)
	{
		if (aBalIntv != null && !aBalIntv.equals(""))
		{
			Integer tInteger = new Integer(aBalIntv);
			int i = tInteger.intValue();
			BalIntv = i;
		}
	}

	public double getBalMoney()
	{
		return BalMoney;
	}
	public void setBalMoney(double aBalMoney)
	{
            BalMoney = Arith.round(aBalMoney,2);
	}
	public void setBalMoney(String aBalMoney)
	{
		if (aBalMoney != null && !aBalMoney.equals(""))
		{
			Double tDouble = new Double(aBalMoney);
			double d = tDouble.doubleValue();
                BalMoney = Arith.round(d,2);
		}
	}

	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
            State = aState;
	}
	public String getFlag()
	{
		return Flag;
	}
	public void setFlag(String aFlag)
	{
            Flag = aFlag;
	}
	public double getSumBal()
	{
		return SumBal;
	}
	public void setSumBal(double aSumBal)
	{
            SumBal = Arith.round(aSumBal,2);
	}
	public void setSumBal(String aSumBal)
	{
		if (aSumBal != null && !aSumBal.equals(""))
		{
			Double tDouble = new Double(aSumBal);
			double d = tDouble.doubleValue();
                SumBal = Arith.round(d,2);
		}
	}

	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
            ManageCom = aManageCom;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
            Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
            MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
            MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
            ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
            ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LCGrpBalPlanSchema 对象给 Schema 赋值
	* @param: aLCGrpBalPlanSchema LCGrpBalPlanSchema
	**/
	public void setSchema(LCGrpBalPlanSchema aLCGrpBalPlanSchema)
	{
		this.GrpContNo = aLCGrpBalPlanSchema.getGrpContNo();
		this.BalTimes = aLCGrpBalPlanSchema.getBalTimes();
		this.BalStartDate = fDate.getDate( aLCGrpBalPlanSchema.getBalStartDate());
		this.BalEndDate = fDate.getDate( aLCGrpBalPlanSchema.getBalEndDate());
		this.BaltoDate = fDate.getDate( aLCGrpBalPlanSchema.getBaltoDate());
		this.NextDate = fDate.getDate( aLCGrpBalPlanSchema.getNextDate());
		this.BalIntv = aLCGrpBalPlanSchema.getBalIntv();
		this.BalMoney = aLCGrpBalPlanSchema.getBalMoney();
		this.State = aLCGrpBalPlanSchema.getState();
		this.Flag = aLCGrpBalPlanSchema.getFlag();
		this.SumBal = aLCGrpBalPlanSchema.getSumBal();
		this.ManageCom = aLCGrpBalPlanSchema.getManageCom();
		this.Operator = aLCGrpBalPlanSchema.getOperator();
		this.MakeDate = fDate.getDate( aLCGrpBalPlanSchema.getMakeDate());
		this.MakeTime = aLCGrpBalPlanSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLCGrpBalPlanSchema.getModifyDate());
		this.ModifyTime = aLCGrpBalPlanSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("GrpContNo") == null )
				this.GrpContNo = null;
			else
				this.GrpContNo = rs.getString("GrpContNo").trim();

			this.BalTimes = rs.getInt("BalTimes");
			this.BalStartDate = rs.getDate("BalStartDate");
			this.BalEndDate = rs.getDate("BalEndDate");
			this.BaltoDate = rs.getDate("BaltoDate");
			this.NextDate = rs.getDate("NextDate");
			this.BalIntv = rs.getInt("BalIntv");
			this.BalMoney = rs.getDouble("BalMoney");
			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			if( rs.getString("Flag") == null )
				this.Flag = null;
			else
				this.Flag = rs.getString("Flag").trim();

			this.SumBal = rs.getDouble("SumBal");
			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LCGrpBalPlan表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCGrpBalPlanSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LCGrpBalPlanSchema getSchema()
	{
		LCGrpBalPlanSchema aLCGrpBalPlanSchema = new LCGrpBalPlanSchema();
		aLCGrpBalPlanSchema.setSchema(this);
		return aLCGrpBalPlanSchema;
	}

	public LCGrpBalPlanDB getDB()
	{
		LCGrpBalPlanDB aDBOper = new LCGrpBalPlanDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCGrpBalPlan描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(BalTimes));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( BalStartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( BalEndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( BaltoDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( NextDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(BalIntv));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(BalMoney));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Flag)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(SumBal));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCGrpBalPlan>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			BalTimes= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,2,SysConst.PACKAGESPILTER))).intValue();
			BalStartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,SysConst.PACKAGESPILTER));
			BalEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,SysConst.PACKAGESPILTER));
			BaltoDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,SysConst.PACKAGESPILTER));
			NextDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,SysConst.PACKAGESPILTER));
			BalIntv= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,7,SysConst.PACKAGESPILTER))).intValue();
			BalMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,8,SysConst.PACKAGESPILTER))).doubleValue();
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			Flag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			SumBal = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,11,SysConst.PACKAGESPILTER))).doubleValue();
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCGrpBalPlanSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("GrpContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
		}
		if (FCode.equals("BalTimes"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BalTimes));
		}
		if (FCode.equals("BalStartDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getBalStartDate()));
		}
		if (FCode.equals("BalEndDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getBalEndDate()));
		}
		if (FCode.equals("BaltoDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getBaltoDate()));
		}
		if (FCode.equals("NextDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getNextDate()));
		}
		if (FCode.equals("BalIntv"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BalIntv));
		}
		if (FCode.equals("BalMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BalMoney));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("Flag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Flag));
		}
		if (FCode.equals("SumBal"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SumBal));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(GrpContNo);
				break;
			case 1:
				strFieldValue = String.valueOf(BalTimes);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getBalStartDate()));
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getBalEndDate()));
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getBaltoDate()));
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getNextDate()));
				break;
			case 6:
				strFieldValue = String.valueOf(BalIntv);
				break;
			case 7:
				strFieldValue = String.valueOf(BalMoney);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(Flag);
				break;
			case 10:
				strFieldValue = String.valueOf(SumBal);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("GrpContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpContNo = FValue.trim();
			}
			else
				GrpContNo = null;
		}
		if (FCode.equalsIgnoreCase("BalTimes"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				BalTimes = i;
			}
		}
		if (FCode.equalsIgnoreCase("BalStartDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				BalStartDate = fDate.getDate( FValue );
			}
			else
				BalStartDate = null;
		}
		if (FCode.equalsIgnoreCase("BalEndDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				BalEndDate = fDate.getDate( FValue );
			}
			else
				BalEndDate = null;
		}
		if (FCode.equalsIgnoreCase("BaltoDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				BaltoDate = fDate.getDate( FValue );
			}
			else
				BaltoDate = null;
		}
		if (FCode.equalsIgnoreCase("NextDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				NextDate = fDate.getDate( FValue );
			}
			else
				NextDate = null;
		}
		if (FCode.equalsIgnoreCase("BalIntv"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				BalIntv = i;
			}
		}
		if (FCode.equalsIgnoreCase("BalMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				BalMoney = d;
			}
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("Flag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Flag = FValue.trim();
			}
			else
				Flag = null;
		}
		if (FCode.equalsIgnoreCase("SumBal"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SumBal = d;
			}
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LCGrpBalPlanSchema other = (LCGrpBalPlanSchema)otherObject;
		return
			GrpContNo.equals(other.getGrpContNo())
			&& BalTimes == other.getBalTimes()
			&& fDate.getString(BalStartDate).equals(other.getBalStartDate())
			&& fDate.getString(BalEndDate).equals(other.getBalEndDate())
			&& fDate.getString(BaltoDate).equals(other.getBaltoDate())
			&& fDate.getString(NextDate).equals(other.getNextDate())
			&& BalIntv == other.getBalIntv()
			&& BalMoney == other.getBalMoney()
			&& State.equals(other.getState())
			&& Flag.equals(other.getFlag())
			&& SumBal == other.getSumBal()
			&& ManageCom.equals(other.getManageCom())
			&& Operator.equals(other.getOperator())
			&& fDate.getString(MakeDate).equals(other.getMakeDate())
			&& MakeTime.equals(other.getMakeTime())
			&& fDate.getString(ModifyDate).equals(other.getModifyDate())
			&& ModifyTime.equals(other.getModifyTime());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("GrpContNo") ) {
			return 0;
		}
		if( strFieldName.equals("BalTimes") ) {
			return 1;
		}
		if( strFieldName.equals("BalStartDate") ) {
			return 2;
		}
		if( strFieldName.equals("BalEndDate") ) {
			return 3;
		}
		if( strFieldName.equals("BaltoDate") ) {
			return 4;
		}
		if( strFieldName.equals("NextDate") ) {
			return 5;
		}
		if( strFieldName.equals("BalIntv") ) {
			return 6;
		}
		if( strFieldName.equals("BalMoney") ) {
			return 7;
		}
		if( strFieldName.equals("State") ) {
			return 8;
		}
		if( strFieldName.equals("Flag") ) {
			return 9;
		}
		if( strFieldName.equals("SumBal") ) {
			return 10;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 11;
		}
		if( strFieldName.equals("Operator") ) {
			return 12;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 13;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 14;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 15;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 16;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "GrpContNo";
				break;
			case 1:
				strFieldName = "BalTimes";
				break;
			case 2:
				strFieldName = "BalStartDate";
				break;
			case 3:
				strFieldName = "BalEndDate";
				break;
			case 4:
				strFieldName = "BaltoDate";
				break;
			case 5:
				strFieldName = "NextDate";
				break;
			case 6:
				strFieldName = "BalIntv";
				break;
			case 7:
				strFieldName = "BalMoney";
				break;
			case 8:
				strFieldName = "State";
				break;
			case 9:
				strFieldName = "Flag";
				break;
			case 10:
				strFieldName = "SumBal";
				break;
			case 11:
				strFieldName = "ManageCom";
				break;
			case 12:
				strFieldName = "Operator";
				break;
			case 13:
				strFieldName = "MakeDate";
				break;
			case 14:
				strFieldName = "MakeTime";
				break;
			case 15:
				strFieldName = "ModifyDate";
				break;
			case 16:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("GrpContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BalTimes") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("BalStartDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("BalEndDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("BaltoDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("NextDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("BalIntv") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("BalMoney") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Flag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SumBal") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_INT;
				break;
			case 2:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 3:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 4:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 5:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 6:
				nFieldType = Schema.TYPE_INT;
				break;
			case 7:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
