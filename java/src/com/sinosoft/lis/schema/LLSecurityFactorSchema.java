/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LLSecurityFactorDB;

/*
 * <p>ClassName: LLSecurityFactorSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-11-27
 */
public class LLSecurityFactorSchema implements Schema, Cloneable {
    // @Field
    /** 编号 */
    private String SerialNo;
    /** 社保责任类型 */
    private String SecurityType;
    /** 医院等级 */
    private String LevelCode;
    /** 参保人员性质 */
    private String InsuredStat;
    /** 账单费用类型 */
    private String FeeType;
    /** 第一次住院标志 */
    private String FirstInHos;
    /** 起付限 */
    private double GetLimit;
    /** 封顶线 */
    private double PeakLine;
    /** 赔付分段下限 */
    private double DownLimit;
    /** 赔付分段上限 */
    private double UpLimit;
    /** 赔付比例 */
    private double GetRate;
    /** 生效日期 */
    private Date StartDate;
    /** 失效日期 */
    private Date EndDate;
    /** 管理机构 */
    private String ComCode;
    /** 备用属性字段1 */
    private String StandbyFlag1;
    /** 备用属性字段2 */
    private String StandbyFlag2;
    /** 备用属性字段3 */
    private String StandbyFlag3;
    /** 分段序号 */
    private int SpanNo;
    /** 分段起始年龄 */
    private int AgeStart;
    /** 分段终止年龄 */
    private int AgeEnd;

    public static final int FIELDNUM = 20; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LLSecurityFactorSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "SerialNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LLSecurityFactorSchema cloned = (LLSecurityFactorSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getSerialNo() {
        return SerialNo;
    }

    public void setSerialNo(String aSerialNo) {
        SerialNo = aSerialNo;
    }

    public String getSecurityType() {
        return SecurityType;
    }

    public void setSecurityType(String aSecurityType) {
        SecurityType = aSecurityType;
    }

    public String getLevelCode() {
        return LevelCode;
    }

    public void setLevelCode(String aLevelCode) {
        LevelCode = aLevelCode;
    }

    public String getInsuredStat() {
        return InsuredStat;
    }

    public void setInsuredStat(String aInsuredStat) {
        InsuredStat = aInsuredStat;
    }

    public String getFeeType() {
        return FeeType;
    }

    public void setFeeType(String aFeeType) {
        FeeType = aFeeType;
    }

    public String getFirstInHos() {
        return FirstInHos;
    }

    public void setFirstInHos(String aFirstInHos) {
        FirstInHos = aFirstInHos;
    }

    public double getGetLimit() {
        return GetLimit;
    }

    public void setGetLimit(double aGetLimit) {
        GetLimit = Arith.round(aGetLimit, 2);
    }

    public void setGetLimit(String aGetLimit) {
        if (aGetLimit != null && !aGetLimit.equals("")) {
            Double tDouble = new Double(aGetLimit);
            double d = tDouble.doubleValue();
            GetLimit = Arith.round(d, 2);
        }
    }

    public double getPeakLine() {
        return PeakLine;
    }

    public void setPeakLine(double aPeakLine) {
        PeakLine = Arith.round(aPeakLine, 2);
    }

    public void setPeakLine(String aPeakLine) {
        if (aPeakLine != null && !aPeakLine.equals("")) {
            Double tDouble = new Double(aPeakLine);
            double d = tDouble.doubleValue();
            PeakLine = Arith.round(d, 2);
        }
    }

    public double getDownLimit() {
        return DownLimit;
    }

    public void setDownLimit(double aDownLimit) {
        DownLimit = Arith.round(aDownLimit, 2);
    }

    public void setDownLimit(String aDownLimit) {
        if (aDownLimit != null && !aDownLimit.equals("")) {
            Double tDouble = new Double(aDownLimit);
            double d = tDouble.doubleValue();
            DownLimit = Arith.round(d, 2);
        }
    }

    public double getUpLimit() {
        return UpLimit;
    }

    public void setUpLimit(double aUpLimit) {
        UpLimit = Arith.round(aUpLimit, 2);
    }

    public void setUpLimit(String aUpLimit) {
        if (aUpLimit != null && !aUpLimit.equals("")) {
            Double tDouble = new Double(aUpLimit);
            double d = tDouble.doubleValue();
            UpLimit = Arith.round(d, 2);
        }
    }

    public double getGetRate() {
        return GetRate;
    }

    public void setGetRate(double aGetRate) {
        GetRate = Arith.round(aGetRate, 4);
    }

    public void setGetRate(String aGetRate) {
        if (aGetRate != null && !aGetRate.equals("")) {
            Double tDouble = new Double(aGetRate);
            double d = tDouble.doubleValue();
            GetRate = Arith.round(d, 4);
        }
    }

    public String getStartDate() {
        if (StartDate != null) {
            return fDate.getString(StartDate);
        } else {
            return null;
        }
    }

    public void setStartDate(Date aStartDate) {
        StartDate = aStartDate;
    }

    public void setStartDate(String aStartDate) {
        if (aStartDate != null && !aStartDate.equals("")) {
            StartDate = fDate.getDate(aStartDate);
        } else {
            StartDate = null;
        }
    }

    public String getEndDate() {
        if (EndDate != null) {
            return fDate.getString(EndDate);
        } else {
            return null;
        }
    }

    public void setEndDate(Date aEndDate) {
        EndDate = aEndDate;
    }

    public void setEndDate(String aEndDate) {
        if (aEndDate != null && !aEndDate.equals("")) {
            EndDate = fDate.getDate(aEndDate);
        } else {
            EndDate = null;
        }
    }

    public String getComCode() {
        return ComCode;
    }

    public void setComCode(String aComCode) {
        ComCode = aComCode;
    }

    public String getStandbyFlag1() {
        return StandbyFlag1;
    }

    public void setStandbyFlag1(String aStandbyFlag1) {
        StandbyFlag1 = aStandbyFlag1;
    }

    public String getStandbyFlag2() {
        return StandbyFlag2;
    }

    public void setStandbyFlag2(String aStandbyFlag2) {
        StandbyFlag2 = aStandbyFlag2;
    }

    public String getStandbyFlag3() {
        return StandbyFlag3;
    }

    public void setStandbyFlag3(String aStandbyFlag3) {
        StandbyFlag3 = aStandbyFlag3;
    }

    public int getSpanNo() {
        return SpanNo;
    }

    public void setSpanNo(int aSpanNo) {
        SpanNo = aSpanNo;
    }

    public void setSpanNo(String aSpanNo) {
        if (aSpanNo != null && !aSpanNo.equals("")) {
            Integer tInteger = new Integer(aSpanNo);
            int i = tInteger.intValue();
            SpanNo = i;
        }
    }

    public int getAgeStart() {
        return AgeStart;
    }

    public void setAgeStart(int aAgeStart) {
        AgeStart = aAgeStart;
    }

    public void setAgeStart(String aAgeStart) {
        if (aAgeStart != null && !aAgeStart.equals("")) {
            Integer tInteger = new Integer(aAgeStart);
            int i = tInteger.intValue();
            AgeStart = i;
        }
    }

    public int getAgeEnd() {
        return AgeEnd;
    }

    public void setAgeEnd(int aAgeEnd) {
        AgeEnd = aAgeEnd;
    }

    public void setAgeEnd(String aAgeEnd) {
        if (aAgeEnd != null && !aAgeEnd.equals("")) {
            Integer tInteger = new Integer(aAgeEnd);
            int i = tInteger.intValue();
            AgeEnd = i;
        }
    }


    /**
     * 使用另外一个 LLSecurityFactorSchema 对象给 Schema 赋值
     * @param: aLLSecurityFactorSchema LLSecurityFactorSchema
     **/
    public void setSchema(LLSecurityFactorSchema aLLSecurityFactorSchema) {
        this.SerialNo = aLLSecurityFactorSchema.getSerialNo();
        this.SecurityType = aLLSecurityFactorSchema.getSecurityType();
        this.LevelCode = aLLSecurityFactorSchema.getLevelCode();
        this.InsuredStat = aLLSecurityFactorSchema.getInsuredStat();
        this.FeeType = aLLSecurityFactorSchema.getFeeType();
        this.FirstInHos = aLLSecurityFactorSchema.getFirstInHos();
        this.GetLimit = aLLSecurityFactorSchema.getGetLimit();
        this.PeakLine = aLLSecurityFactorSchema.getPeakLine();
        this.DownLimit = aLLSecurityFactorSchema.getDownLimit();
        this.UpLimit = aLLSecurityFactorSchema.getUpLimit();
        this.GetRate = aLLSecurityFactorSchema.getGetRate();
        this.StartDate = fDate.getDate(aLLSecurityFactorSchema.getStartDate());
        this.EndDate = fDate.getDate(aLLSecurityFactorSchema.getEndDate());
        this.ComCode = aLLSecurityFactorSchema.getComCode();
        this.StandbyFlag1 = aLLSecurityFactorSchema.getStandbyFlag1();
        this.StandbyFlag2 = aLLSecurityFactorSchema.getStandbyFlag2();
        this.StandbyFlag3 = aLLSecurityFactorSchema.getStandbyFlag3();
        this.SpanNo = aLLSecurityFactorSchema.getSpanNo();
        this.AgeStart = aLLSecurityFactorSchema.getAgeStart();
        this.AgeEnd = aLLSecurityFactorSchema.getAgeEnd();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("SerialNo") == null) {
                this.SerialNo = null;
            } else {
                this.SerialNo = rs.getString("SerialNo").trim();
            }

            if (rs.getString("SecurityType") == null) {
                this.SecurityType = null;
            } else {
                this.SecurityType = rs.getString("SecurityType").trim();
            }

            if (rs.getString("LevelCode") == null) {
                this.LevelCode = null;
            } else {
                this.LevelCode = rs.getString("LevelCode").trim();
            }

            if (rs.getString("InsuredStat") == null) {
                this.InsuredStat = null;
            } else {
                this.InsuredStat = rs.getString("InsuredStat").trim();
            }

            if (rs.getString("FeeType") == null) {
                this.FeeType = null;
            } else {
                this.FeeType = rs.getString("FeeType").trim();
            }

            if (rs.getString("FirstInHos") == null) {
                this.FirstInHos = null;
            } else {
                this.FirstInHos = rs.getString("FirstInHos").trim();
            }

            this.GetLimit = rs.getDouble("GetLimit");
            this.PeakLine = rs.getDouble("PeakLine");
            this.DownLimit = rs.getDouble("DownLimit");
            this.UpLimit = rs.getDouble("UpLimit");
            this.GetRate = rs.getDouble("GetRate");
            this.StartDate = rs.getDate("StartDate");
            this.EndDate = rs.getDate("EndDate");
            if (rs.getString("ComCode") == null) {
                this.ComCode = null;
            } else {
                this.ComCode = rs.getString("ComCode").trim();
            }

            if (rs.getString("StandbyFlag1") == null) {
                this.StandbyFlag1 = null;
            } else {
                this.StandbyFlag1 = rs.getString("StandbyFlag1").trim();
            }

            if (rs.getString("StandbyFlag2") == null) {
                this.StandbyFlag2 = null;
            } else {
                this.StandbyFlag2 = rs.getString("StandbyFlag2").trim();
            }

            if (rs.getString("StandbyFlag3") == null) {
                this.StandbyFlag3 = null;
            } else {
                this.StandbyFlag3 = rs.getString("StandbyFlag3").trim();
            }

            this.SpanNo = rs.getInt("SpanNo");
            this.AgeStart = rs.getInt("AgeStart");
            this.AgeEnd = rs.getInt("AgeEnd");
        } catch (SQLException sqle) {
            System.out.println("数据库中的LLSecurityFactor表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLSecurityFactorSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LLSecurityFactorSchema getSchema() {
        LLSecurityFactorSchema aLLSecurityFactorSchema = new
                LLSecurityFactorSchema();
        aLLSecurityFactorSchema.setSchema(this);
        return aLLSecurityFactorSchema;
    }

    public LLSecurityFactorDB getDB() {
        LLSecurityFactorDB aDBOper = new LLSecurityFactorDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLSecurityFactor描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(SerialNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SecurityType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(LevelCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuredStat));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FeeType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FirstInHos));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(GetLimit));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PeakLine));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DownLimit));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(UpLimit));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(GetRate));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(StartDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(EndDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ComCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandbyFlag1));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandbyFlag2));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandbyFlag3));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SpanNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(AgeStart));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(AgeEnd));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLSecurityFactor>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            SecurityType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                          SysConst.PACKAGESPILTER);
            LevelCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                       SysConst.PACKAGESPILTER);
            InsuredStat = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                         SysConst.PACKAGESPILTER);
            FeeType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                     SysConst.PACKAGESPILTER);
            FirstInHos = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                        SysConst.PACKAGESPILTER);
            GetLimit = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 7, SysConst.PACKAGESPILTER))).doubleValue();
            PeakLine = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 8, SysConst.PACKAGESPILTER))).doubleValue();
            DownLimit = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 9, SysConst.PACKAGESPILTER))).doubleValue();
            UpLimit = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 10, SysConst.PACKAGESPILTER))).doubleValue();
            GetRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 11, SysConst.PACKAGESPILTER))).doubleValue();
            StartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 12, SysConst.PACKAGESPILTER));
            EndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 13, SysConst.PACKAGESPILTER));
            ComCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                     SysConst.PACKAGESPILTER);
            StandbyFlag1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                          SysConst.PACKAGESPILTER);
            StandbyFlag2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                          SysConst.PACKAGESPILTER);
            StandbyFlag3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                          SysConst.PACKAGESPILTER);
            SpanNo = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 18, SysConst.PACKAGESPILTER))).intValue();
            AgeStart = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 19, SysConst.PACKAGESPILTER))).intValue();
            AgeEnd = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 20, SysConst.PACKAGESPILTER))).intValue();
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLSecurityFactorSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equals("SecurityType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SecurityType));
        }
        if (FCode.equals("LevelCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LevelCode));
        }
        if (FCode.equals("InsuredStat")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredStat));
        }
        if (FCode.equals("FeeType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeType));
        }
        if (FCode.equals("FirstInHos")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FirstInHos));
        }
        if (FCode.equals("GetLimit")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetLimit));
        }
        if (FCode.equals("PeakLine")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PeakLine));
        }
        if (FCode.equals("DownLimit")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DownLimit));
        }
        if (FCode.equals("UpLimit")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UpLimit));
        }
        if (FCode.equals("GetRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetRate));
        }
        if (FCode.equals("StartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getStartDate()));
        }
        if (FCode.equals("EndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getEndDate()));
        }
        if (FCode.equals("ComCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ComCode));
        }
        if (FCode.equals("StandbyFlag1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag1));
        }
        if (FCode.equals("StandbyFlag2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag2));
        }
        if (FCode.equals("StandbyFlag3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag3));
        }
        if (FCode.equals("SpanNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SpanNo));
        }
        if (FCode.equals("AgeStart")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgeStart));
        }
        if (FCode.equals("AgeEnd")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgeEnd));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(SerialNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(SecurityType);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(LevelCode);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(InsuredStat);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(FeeType);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(FirstInHos);
            break;
        case 6:
            strFieldValue = String.valueOf(GetLimit);
            break;
        case 7:
            strFieldValue = String.valueOf(PeakLine);
            break;
        case 8:
            strFieldValue = String.valueOf(DownLimit);
            break;
        case 9:
            strFieldValue = String.valueOf(UpLimit);
            break;
        case 10:
            strFieldValue = String.valueOf(GetRate);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getStartDate()));
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.getEndDate()));
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(ComCode);
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(StandbyFlag1);
            break;
        case 15:
            strFieldValue = StrTool.GBKToUnicode(StandbyFlag2);
            break;
        case 16:
            strFieldValue = StrTool.GBKToUnicode(StandbyFlag3);
            break;
        case 17:
            strFieldValue = String.valueOf(SpanNo);
            break;
        case 18:
            strFieldValue = String.valueOf(AgeStart);
            break;
        case 19:
            strFieldValue = String.valueOf(AgeEnd);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("SerialNo")) {
            if (FValue != null && !FValue.equals("")) {
                SerialNo = FValue.trim();
            } else {
                SerialNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("SecurityType")) {
            if (FValue != null && !FValue.equals("")) {
                SecurityType = FValue.trim();
            } else {
                SecurityType = null;
            }
        }
        if (FCode.equalsIgnoreCase("LevelCode")) {
            if (FValue != null && !FValue.equals("")) {
                LevelCode = FValue.trim();
            } else {
                LevelCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("InsuredStat")) {
            if (FValue != null && !FValue.equals("")) {
                InsuredStat = FValue.trim();
            } else {
                InsuredStat = null;
            }
        }
        if (FCode.equalsIgnoreCase("FeeType")) {
            if (FValue != null && !FValue.equals("")) {
                FeeType = FValue.trim();
            } else {
                FeeType = null;
            }
        }
        if (FCode.equalsIgnoreCase("FirstInHos")) {
            if (FValue != null && !FValue.equals("")) {
                FirstInHos = FValue.trim();
            } else {
                FirstInHos = null;
            }
        }
        if (FCode.equalsIgnoreCase("GetLimit")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                GetLimit = d;
            }
        }
        if (FCode.equalsIgnoreCase("PeakLine")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                PeakLine = d;
            }
        }
        if (FCode.equalsIgnoreCase("DownLimit")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                DownLimit = d;
            }
        }
        if (FCode.equalsIgnoreCase("UpLimit")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                UpLimit = d;
            }
        }
        if (FCode.equalsIgnoreCase("GetRate")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                GetRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("StartDate")) {
            if (FValue != null && !FValue.equals("")) {
                StartDate = fDate.getDate(FValue);
            } else {
                StartDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            if (FValue != null && !FValue.equals("")) {
                EndDate = fDate.getDate(FValue);
            } else {
                EndDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ComCode")) {
            if (FValue != null && !FValue.equals("")) {
                ComCode = FValue.trim();
            } else {
                ComCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("StandbyFlag1")) {
            if (FValue != null && !FValue.equals("")) {
                StandbyFlag1 = FValue.trim();
            } else {
                StandbyFlag1 = null;
            }
        }
        if (FCode.equalsIgnoreCase("StandbyFlag2")) {
            if (FValue != null && !FValue.equals("")) {
                StandbyFlag2 = FValue.trim();
            } else {
                StandbyFlag2 = null;
            }
        }
        if (FCode.equalsIgnoreCase("StandbyFlag3")) {
            if (FValue != null && !FValue.equals("")) {
                StandbyFlag3 = FValue.trim();
            } else {
                StandbyFlag3 = null;
            }
        }
        if (FCode.equalsIgnoreCase("SpanNo")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                SpanNo = i;
            }
        }
        if (FCode.equalsIgnoreCase("AgeStart")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                AgeStart = i;
            }
        }
        if (FCode.equalsIgnoreCase("AgeEnd")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                AgeEnd = i;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LLSecurityFactorSchema other = (LLSecurityFactorSchema) otherObject;
        return
                SerialNo.equals(other.getSerialNo())
                && SecurityType.equals(other.getSecurityType())
                && LevelCode.equals(other.getLevelCode())
                && InsuredStat.equals(other.getInsuredStat())
                && FeeType.equals(other.getFeeType())
                && FirstInHos.equals(other.getFirstInHos())
                && GetLimit == other.getGetLimit()
                && PeakLine == other.getPeakLine()
                && DownLimit == other.getDownLimit()
                && UpLimit == other.getUpLimit()
                && GetRate == other.getGetRate()
                && fDate.getString(StartDate).equals(other.getStartDate())
                && fDate.getString(EndDate).equals(other.getEndDate())
                && ComCode.equals(other.getComCode())
                && StandbyFlag1.equals(other.getStandbyFlag1())
                && StandbyFlag2.equals(other.getStandbyFlag2())
                && StandbyFlag3.equals(other.getStandbyFlag3())
                && SpanNo == other.getSpanNo()
                && AgeStart == other.getAgeStart()
                && AgeEnd == other.getAgeEnd();
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("SerialNo")) {
            return 0;
        }
        if (strFieldName.equals("SecurityType")) {
            return 1;
        }
        if (strFieldName.equals("LevelCode")) {
            return 2;
        }
        if (strFieldName.equals("InsuredStat")) {
            return 3;
        }
        if (strFieldName.equals("FeeType")) {
            return 4;
        }
        if (strFieldName.equals("FirstInHos")) {
            return 5;
        }
        if (strFieldName.equals("GetLimit")) {
            return 6;
        }
        if (strFieldName.equals("PeakLine")) {
            return 7;
        }
        if (strFieldName.equals("DownLimit")) {
            return 8;
        }
        if (strFieldName.equals("UpLimit")) {
            return 9;
        }
        if (strFieldName.equals("GetRate")) {
            return 10;
        }
        if (strFieldName.equals("StartDate")) {
            return 11;
        }
        if (strFieldName.equals("EndDate")) {
            return 12;
        }
        if (strFieldName.equals("ComCode")) {
            return 13;
        }
        if (strFieldName.equals("StandbyFlag1")) {
            return 14;
        }
        if (strFieldName.equals("StandbyFlag2")) {
            return 15;
        }
        if (strFieldName.equals("StandbyFlag3")) {
            return 16;
        }
        if (strFieldName.equals("SpanNo")) {
            return 17;
        }
        if (strFieldName.equals("AgeStart")) {
            return 18;
        }
        if (strFieldName.equals("AgeEnd")) {
            return 19;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "SerialNo";
            break;
        case 1:
            strFieldName = "SecurityType";
            break;
        case 2:
            strFieldName = "LevelCode";
            break;
        case 3:
            strFieldName = "InsuredStat";
            break;
        case 4:
            strFieldName = "FeeType";
            break;
        case 5:
            strFieldName = "FirstInHos";
            break;
        case 6:
            strFieldName = "GetLimit";
            break;
        case 7:
            strFieldName = "PeakLine";
            break;
        case 8:
            strFieldName = "DownLimit";
            break;
        case 9:
            strFieldName = "UpLimit";
            break;
        case 10:
            strFieldName = "GetRate";
            break;
        case 11:
            strFieldName = "StartDate";
            break;
        case 12:
            strFieldName = "EndDate";
            break;
        case 13:
            strFieldName = "ComCode";
            break;
        case 14:
            strFieldName = "StandbyFlag1";
            break;
        case 15:
            strFieldName = "StandbyFlag2";
            break;
        case 16:
            strFieldName = "StandbyFlag3";
            break;
        case 17:
            strFieldName = "SpanNo";
            break;
        case 18:
            strFieldName = "AgeStart";
            break;
        case 19:
            strFieldName = "AgeEnd";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("SerialNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SecurityType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("LevelCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsuredStat")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FeeType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FirstInHos")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetLimit")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("PeakLine")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("DownLimit")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("UpLimit")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("GetRate")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("StartDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("EndDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ComCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StandbyFlag1")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StandbyFlag2")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StandbyFlag3")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SpanNo")) {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("AgeStart")) {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("AgeEnd")) {
            return Schema.TYPE_INT;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 7:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 8:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 9:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 10:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 11:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 12:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 13:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 14:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 15:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 16:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 17:
            nFieldType = Schema.TYPE_INT;
            break;
        case 18:
            nFieldType = Schema.TYPE_INT;
            break;
        case 19:
            nFieldType = Schema.TYPE_INT;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
