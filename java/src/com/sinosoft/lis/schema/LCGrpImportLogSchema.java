/*
 * <p>ClassName: LCGrpImportLogSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2004-12-17
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LCGrpImportLogDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LCGrpImportLogSchema implements Schema
{
    // @Field
    /** 团体保单号 */
    private String GrpContNo;
    /** 批次号 */
    private String BatchNo;
    /** 合同id */
    private String ContID;
    /** 险种唯一id */
    private String IdNo;
    /** 被保人客户id */
    private String InsuredID;
    /** 印刷号 */
    private String PrtNo;
    /** 合同号码 */
    private String ContNo;
    /** 保单号码 */
    private String PolNo;
    /** 险种代码 */
    private String RiskCode;
    /** 保险计划编码 */
    private String ContPlanCode;
    /** 错误类型 */
    private String ErrorType;
    /** 错误信息 */
    private String ErrorInfo;
    /** 错误状态 */
    private String ErrorState;
    /** 被保人客户号 */
    private String InsuredNo;
    /** 被保人客户姓名 */
    private String InsuredName;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;

    public static final int FIELDNUM = 18; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LCGrpImportLogSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "BatchNo";
        pk[1] = "ContID";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getGrpContNo()
    {
        if (GrpContNo != null && !GrpContNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GrpContNo = StrTool.unicodeToGBK(GrpContNo);
        }
        return GrpContNo;
    }

    public void setGrpContNo(String aGrpContNo)
    {
        GrpContNo = aGrpContNo;
    }

    public String getBatchNo()
    {
        if (BatchNo != null && !BatchNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            BatchNo = StrTool.unicodeToGBK(BatchNo);
        }
        return BatchNo;
    }

    public void setBatchNo(String aBatchNo)
    {
        BatchNo = aBatchNo;
    }

    public String getContID()
    {
        if (ContID != null && !ContID.equals("") && SysConst.CHANGECHARSET == true)
        {
            ContID = StrTool.unicodeToGBK(ContID);
        }
        return ContID;
    }

    public void setContID(String aContID)
    {
        ContID = aContID;
    }

    public String getIdNo()
    {
        if (IdNo != null && !IdNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            IdNo = StrTool.unicodeToGBK(IdNo);
        }
        return IdNo;
    }

    public void setIdNo(String aIdNo)
    {
        IdNo = aIdNo;
    }

    public String getInsuredID()
    {
        if (InsuredID != null && !InsuredID.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            InsuredID = StrTool.unicodeToGBK(InsuredID);
        }
        return InsuredID;
    }

    public void setInsuredID(String aInsuredID)
    {
        InsuredID = aInsuredID;
    }

    public String getPrtNo()
    {
        if (PrtNo != null && !PrtNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            PrtNo = StrTool.unicodeToGBK(PrtNo);
        }
        return PrtNo;
    }

    public void setPrtNo(String aPrtNo)
    {
        PrtNo = aPrtNo;
    }

    public String getContNo()
    {
        if (ContNo != null && !ContNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            ContNo = StrTool.unicodeToGBK(ContNo);
        }
        return ContNo;
    }

    public void setContNo(String aContNo)
    {
        ContNo = aContNo;
    }

    public String getPolNo()
    {
        if (PolNo != null && !PolNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            PolNo = StrTool.unicodeToGBK(PolNo);
        }
        return PolNo;
    }

    public void setPolNo(String aPolNo)
    {
        PolNo = aPolNo;
    }

    public String getRiskCode()
    {
        if (RiskCode != null && !RiskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getContPlanCode()
    {
        if (ContPlanCode != null && !ContPlanCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ContPlanCode = StrTool.unicodeToGBK(ContPlanCode);
        }
        return ContPlanCode;
    }

    public void setContPlanCode(String aContPlanCode)
    {
        ContPlanCode = aContPlanCode;
    }

    public String getErrorType()
    {
        if (ErrorType != null && !ErrorType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ErrorType = StrTool.unicodeToGBK(ErrorType);
        }
        return ErrorType;
    }

    public void setErrorType(String aErrorType)
    {
        ErrorType = aErrorType;
    }

    public String getErrorInfo()
    {
        if (ErrorInfo != null && !ErrorInfo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ErrorInfo = StrTool.unicodeToGBK(ErrorInfo);
        }
        return ErrorInfo;
    }

    public void setErrorInfo(String aErrorInfo)
    {
        ErrorInfo = aErrorInfo;
    }

    public String getErrorState()
    {
        if (ErrorState != null && !ErrorState.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ErrorState = StrTool.unicodeToGBK(ErrorState);
        }
        return ErrorState;
    }

    public void setErrorState(String aErrorState)
    {
        ErrorState = aErrorState;
    }

    public String getInsuredNo()
    {
        if (InsuredNo != null && !InsuredNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            InsuredNo = StrTool.unicodeToGBK(InsuredNo);
        }
        return InsuredNo;
    }

    public void setInsuredNo(String aInsuredNo)
    {
        InsuredNo = aInsuredNo;
    }

    public String getInsuredName()
    {
        if (InsuredName != null && !InsuredName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            InsuredName = StrTool.unicodeToGBK(InsuredName);
        }
        return InsuredName;
    }

    public void setInsuredName(String aInsuredName)
    {
        InsuredName = aInsuredName;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    /**
     * 使用另外一个 LCGrpImportLogSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LCGrpImportLogSchema aLCGrpImportLogSchema)
    {
        this.GrpContNo = aLCGrpImportLogSchema.getGrpContNo();
        this.BatchNo = aLCGrpImportLogSchema.getBatchNo();
        this.ContID = aLCGrpImportLogSchema.getContID();
        this.IdNo = aLCGrpImportLogSchema.getIdNo();
        this.InsuredID = aLCGrpImportLogSchema.getInsuredID();
        this.PrtNo = aLCGrpImportLogSchema.getPrtNo();
        this.ContNo = aLCGrpImportLogSchema.getContNo();
        this.PolNo = aLCGrpImportLogSchema.getPolNo();
        this.RiskCode = aLCGrpImportLogSchema.getRiskCode();
        this.ContPlanCode = aLCGrpImportLogSchema.getContPlanCode();
        this.ErrorType = aLCGrpImportLogSchema.getErrorType();
        this.ErrorInfo = aLCGrpImportLogSchema.getErrorInfo();
        this.ErrorState = aLCGrpImportLogSchema.getErrorState();
        this.InsuredNo = aLCGrpImportLogSchema.getInsuredNo();
        this.InsuredName = aLCGrpImportLogSchema.getInsuredName();
        this.Operator = aLCGrpImportLogSchema.getOperator();
        this.MakeDate = fDate.getDate(aLCGrpImportLogSchema.getMakeDate());
        this.MakeTime = aLCGrpImportLogSchema.getMakeTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("GrpContNo") == null)
            {
                this.GrpContNo = null;
            }
            else
            {
                this.GrpContNo = rs.getString("GrpContNo").trim();
            }

            if (rs.getString("BatchNo") == null)
            {
                this.BatchNo = null;
            }
            else
            {
                this.BatchNo = rs.getString("BatchNo").trim();
            }

            if (rs.getString("ContID") == null)
            {
                this.ContID = null;
            }
            else
            {
                this.ContID = rs.getString("ContID").trim();
            }

            if (rs.getString("IdNo") == null)
            {
                this.IdNo = null;
            }
            else
            {
                this.IdNo = rs.getString("IdNo").trim();
            }

            if (rs.getString("InsuredID") == null)
            {
                this.InsuredID = null;
            }
            else
            {
                this.InsuredID = rs.getString("InsuredID").trim();
            }

            if (rs.getString("PrtNo") == null)
            {
                this.PrtNo = null;
            }
            else
            {
                this.PrtNo = rs.getString("PrtNo").trim();
            }

            if (rs.getString("ContNo") == null)
            {
                this.ContNo = null;
            }
            else
            {
                this.ContNo = rs.getString("ContNo").trim();
            }

            if (rs.getString("PolNo") == null)
            {
                this.PolNo = null;
            }
            else
            {
                this.PolNo = rs.getString("PolNo").trim();
            }

            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("ContPlanCode") == null)
            {
                this.ContPlanCode = null;
            }
            else
            {
                this.ContPlanCode = rs.getString("ContPlanCode").trim();
            }

            if (rs.getString("ErrorType") == null)
            {
                this.ErrorType = null;
            }
            else
            {
                this.ErrorType = rs.getString("ErrorType").trim();
            }

            if (rs.getString("ErrorInfo") == null)
            {
                this.ErrorInfo = null;
            }
            else
            {
                this.ErrorInfo = rs.getString("ErrorInfo").trim();
            }

            if (rs.getString("ErrorState") == null)
            {
                this.ErrorState = null;
            }
            else
            {
                this.ErrorState = rs.getString("ErrorState").trim();
            }

            if (rs.getString("InsuredNo") == null)
            {
                this.InsuredNo = null;
            }
            else
            {
                this.InsuredNo = rs.getString("InsuredNo").trim();
            }

            if (rs.getString("InsuredName") == null)
            {
                this.InsuredName = null;
            }
            else
            {
                this.InsuredName = rs.getString("InsuredName").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCGrpImportLogSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LCGrpImportLogSchema getSchema()
    {
        LCGrpImportLogSchema aLCGrpImportLogSchema = new LCGrpImportLogSchema();
        aLCGrpImportLogSchema.setSchema(this);
        return aLCGrpImportLogSchema;
    }

    public LCGrpImportLogDB getDB()
    {
        LCGrpImportLogDB aDBOper = new LCGrpImportLogDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCGrpImportLog描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(GrpContNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(BatchNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ContID)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(IdNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InsuredID)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PrtNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ContNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PolNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ContPlanCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ErrorType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ErrorInfo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ErrorState)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InsuredNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InsuredName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCGrpImportLog>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            BatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                     SysConst.PACKAGESPILTER);
            ContID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                    SysConst.PACKAGESPILTER);
            IdNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                  SysConst.PACKAGESPILTER);
            InsuredID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                       SysConst.PACKAGESPILTER);
            PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                   SysConst.PACKAGESPILTER);
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                    SysConst.PACKAGESPILTER);
            PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                   SysConst.PACKAGESPILTER);
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                      SysConst.PACKAGESPILTER);
            ContPlanCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                          SysConst.PACKAGESPILTER);
            ErrorType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                       SysConst.PACKAGESPILTER);
            ErrorInfo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                       SysConst.PACKAGESPILTER);
            ErrorState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                        SysConst.PACKAGESPILTER);
            InsuredNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                       SysConst.PACKAGESPILTER);
            InsuredName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                         SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 17, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                      SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCGrpImportLogSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("GrpContNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GrpContNo));
        }
        if (FCode.equals("BatchNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(BatchNo));
        }
        if (FCode.equals("ContID"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ContID));
        }
        if (FCode.equals("IdNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(IdNo));
        }
        if (FCode.equals("InsuredID"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(InsuredID));
        }
        if (FCode.equals("PrtNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PrtNo));
        }
        if (FCode.equals("ContNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ContNo));
        }
        if (FCode.equals("PolNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PolNo));
        }
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskCode));
        }
        if (FCode.equals("ContPlanCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ContPlanCode));
        }
        if (FCode.equals("ErrorType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ErrorType));
        }
        if (FCode.equals("ErrorInfo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ErrorInfo));
        }
        if (FCode.equals("ErrorState"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ErrorState));
        }
        if (FCode.equals("InsuredNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(InsuredNo));
        }
        if (FCode.equals("InsuredName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(InsuredName));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(GrpContNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(BatchNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ContID);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(IdNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(InsuredID);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(PrtNo);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(PolNo);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(ContPlanCode);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(ErrorType);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(ErrorInfo);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(ErrorState);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(InsuredNo);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(InsuredName);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("GrpContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
            {
                GrpContNo = null;
            }
        }
        if (FCode.equals("BatchNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BatchNo = FValue.trim();
            }
            else
            {
                BatchNo = null;
            }
        }
        if (FCode.equals("ContID"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ContID = FValue.trim();
            }
            else
            {
                ContID = null;
            }
        }
        if (FCode.equals("IdNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                IdNo = FValue.trim();
            }
            else
            {
                IdNo = null;
            }
        }
        if (FCode.equals("InsuredID"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InsuredID = FValue.trim();
            }
            else
            {
                InsuredID = null;
            }
        }
        if (FCode.equals("PrtNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PrtNo = FValue.trim();
            }
            else
            {
                PrtNo = null;
            }
        }
        if (FCode.equals("ContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
            {
                ContNo = null;
            }
        }
        if (FCode.equals("PolNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
            {
                PolNo = null;
            }
        }
        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("ContPlanCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ContPlanCode = FValue.trim();
            }
            else
            {
                ContPlanCode = null;
            }
        }
        if (FCode.equals("ErrorType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ErrorType = FValue.trim();
            }
            else
            {
                ErrorType = null;
            }
        }
        if (FCode.equals("ErrorInfo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ErrorInfo = FValue.trim();
            }
            else
            {
                ErrorInfo = null;
            }
        }
        if (FCode.equals("ErrorState"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ErrorState = FValue.trim();
            }
            else
            {
                ErrorState = null;
            }
        }
        if (FCode.equals("InsuredNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InsuredNo = FValue.trim();
            }
            else
            {
                InsuredNo = null;
            }
        }
        if (FCode.equals("InsuredName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InsuredName = FValue.trim();
            }
            else
            {
                InsuredName = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LCGrpImportLogSchema other = (LCGrpImportLogSchema) otherObject;
        return
                GrpContNo.equals(other.getGrpContNo())
                && BatchNo.equals(other.getBatchNo())
                && ContID.equals(other.getContID())
                && IdNo.equals(other.getIdNo())
                && InsuredID.equals(other.getInsuredID())
                && PrtNo.equals(other.getPrtNo())
                && ContNo.equals(other.getContNo())
                && PolNo.equals(other.getPolNo())
                && RiskCode.equals(other.getRiskCode())
                && ContPlanCode.equals(other.getContPlanCode())
                && ErrorType.equals(other.getErrorType())
                && ErrorInfo.equals(other.getErrorInfo())
                && ErrorState.equals(other.getErrorState())
                && InsuredNo.equals(other.getInsuredNo())
                && InsuredName.equals(other.getInsuredName())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("GrpContNo"))
        {
            return 0;
        }
        if (strFieldName.equals("BatchNo"))
        {
            return 1;
        }
        if (strFieldName.equals("ContID"))
        {
            return 2;
        }
        if (strFieldName.equals("IdNo"))
        {
            return 3;
        }
        if (strFieldName.equals("InsuredID"))
        {
            return 4;
        }
        if (strFieldName.equals("PrtNo"))
        {
            return 5;
        }
        if (strFieldName.equals("ContNo"))
        {
            return 6;
        }
        if (strFieldName.equals("PolNo"))
        {
            return 7;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return 8;
        }
        if (strFieldName.equals("ContPlanCode"))
        {
            return 9;
        }
        if (strFieldName.equals("ErrorType"))
        {
            return 10;
        }
        if (strFieldName.equals("ErrorInfo"))
        {
            return 11;
        }
        if (strFieldName.equals("ErrorState"))
        {
            return 12;
        }
        if (strFieldName.equals("InsuredNo"))
        {
            return 13;
        }
        if (strFieldName.equals("InsuredName"))
        {
            return 14;
        }
        if (strFieldName.equals("Operator"))
        {
            return 15;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 16;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 17;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "GrpContNo";
                break;
            case 1:
                strFieldName = "BatchNo";
                break;
            case 2:
                strFieldName = "ContID";
                break;
            case 3:
                strFieldName = "IdNo";
                break;
            case 4:
                strFieldName = "InsuredID";
                break;
            case 5:
                strFieldName = "PrtNo";
                break;
            case 6:
                strFieldName = "ContNo";
                break;
            case 7:
                strFieldName = "PolNo";
                break;
            case 8:
                strFieldName = "RiskCode";
                break;
            case 9:
                strFieldName = "ContPlanCode";
                break;
            case 10:
                strFieldName = "ErrorType";
                break;
            case 11:
                strFieldName = "ErrorInfo";
                break;
            case 12:
                strFieldName = "ErrorState";
                break;
            case 13:
                strFieldName = "InsuredNo";
                break;
            case 14:
                strFieldName = "InsuredName";
                break;
            case 15:
                strFieldName = "Operator";
                break;
            case 16:
                strFieldName = "MakeDate";
                break;
            case 17:
                strFieldName = "MakeTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("GrpContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BatchNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContID"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("IdNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsuredID"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PrtNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PolNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContPlanCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ErrorType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ErrorInfo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ErrorState"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsuredNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsuredName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 17:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
