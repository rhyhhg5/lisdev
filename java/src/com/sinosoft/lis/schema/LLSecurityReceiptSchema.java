/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LLSecurityReceiptDB;

/*
 * <p>ClassName: LLSecurityReceiptSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2015-12-11
 */
public class LLSecurityReceiptSchema implements Schema, Cloneable
{
	// @Field
	/** 账单费用明细 */
	private String FeeDetailNo;
	/** 账单号 */
	private String MainFeeNo;
	/** 立案号(申请登记号) */
	private String RgtNo;
	/** 分案号 */
	private String CaseNo;
	/** 申报金额 */
	private double ApplyAmnt;
	/** 医保目录内金额 */
	private double FeeInSecu;
	/** 医保目录外金额 */
	private double FeeOutSecu;
	/** 门诊大额年度累计支付金额 */
	private double TotalSupDoorFee;
	/** 报销单据张数 */
	private int PayReceipt;
	/** 拒付单据张数 */
	private int RefuseReceipt;
	/** 本次门诊大额医疗费用 */
	private double SupDoorFee;
	/** 年度累计门诊大额医疗费用 */
	private double YearSupDoorFee;
	/** 本次统筹基金医疗费用 */
	private double PlanFee;
	/** 年度累计统筹基金医疗费用 */
	private double YearPlayFee;
	/** 本次住院大额医疗费用 */
	private double SupInHosFee;
	/** 年度累计住院大额医疗费用 */
	private double YearSupInHosFee;
	/** 医保支付合计 */
	private double SecurityFee;
	/** 小额门诊费用 */
	private double SmallDoorPay;
	/** 门急诊费用 */
	private double EmergencyPay;
	/** 自付一金额 */
	private double SelfPay1;
	/** 自付二金额 */
	private double SelfPay2;
	/** 医保不予支付金额合计 */
	private double SecuRefuseFee;
	/** 自费金额 */
	private double SelfAmnt;
	/** 起付限 */
	private double GetLimit;
	/** 低段责任金额 */
	private double LowAmnt;
	/** 中段责任金额 */
	private double MidAmnt;
	/** 高段一责任金额 */
	private double HighAmnt1;
	/** 高段二责任金额 */
	private double HighAmnt2;
	/** 超高段责任金额 */
	private double SuperAmnt;
	/** 管理机构 */
	private String MngCom;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 帐户支付金额 */
	private double AccountPay;
	/** 公务员医疗补助支付金额 */
	private double OfficialSubsidy;
	/** 大额门诊责任金额 */
	private double HighDoorAmnt;
	/** 退休人员补充医疗费用 */
	private double RetireAddFee;
	/** 年度累计退休人员补充医疗费用 */
	private double YearRetireAddFee;
	/** 备用金额字段 */
	private double StandbyAmnt;
	/** 第三方支付金额 */
	private double OtherPay;
	/** 剩余赔付金额 */
	private double RemPay;

	public static final int FIELDNUM = 43;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LLSecurityReceiptSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "FeeDetailNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LLSecurityReceiptSchema cloned = (LLSecurityReceiptSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getFeeDetailNo()
	{
		return FeeDetailNo;
	}
	public void setFeeDetailNo(String aFeeDetailNo)
	{
		FeeDetailNo = aFeeDetailNo;
	}
	public String getMainFeeNo()
	{
		return MainFeeNo;
	}
	public void setMainFeeNo(String aMainFeeNo)
	{
		MainFeeNo = aMainFeeNo;
	}
	public String getRgtNo()
	{
		return RgtNo;
	}
	public void setRgtNo(String aRgtNo)
	{
		RgtNo = aRgtNo;
	}
	public String getCaseNo()
	{
		return CaseNo;
	}
	public void setCaseNo(String aCaseNo)
	{
		CaseNo = aCaseNo;
	}
	public double getApplyAmnt()
	{
		return ApplyAmnt;
	}
	public void setApplyAmnt(double aApplyAmnt)
	{
		ApplyAmnt = Arith.round(aApplyAmnt,2);
	}
	public void setApplyAmnt(String aApplyAmnt)
	{
		if (aApplyAmnt != null && !aApplyAmnt.equals(""))
		{
			Double tDouble = new Double(aApplyAmnt);
			double d = tDouble.doubleValue();
                ApplyAmnt = Arith.round(d,2);
		}
	}

	public double getFeeInSecu()
	{
		return FeeInSecu;
	}
	public void setFeeInSecu(double aFeeInSecu)
	{
		FeeInSecu = Arith.round(aFeeInSecu,2);
	}
	public void setFeeInSecu(String aFeeInSecu)
	{
		if (aFeeInSecu != null && !aFeeInSecu.equals(""))
		{
			Double tDouble = new Double(aFeeInSecu);
			double d = tDouble.doubleValue();
                FeeInSecu = Arith.round(d,2);
		}
	}

	public double getFeeOutSecu()
	{
		return FeeOutSecu;
	}
	public void setFeeOutSecu(double aFeeOutSecu)
	{
		FeeOutSecu = Arith.round(aFeeOutSecu,2);
	}
	public void setFeeOutSecu(String aFeeOutSecu)
	{
		if (aFeeOutSecu != null && !aFeeOutSecu.equals(""))
		{
			Double tDouble = new Double(aFeeOutSecu);
			double d = tDouble.doubleValue();
                FeeOutSecu = Arith.round(d,2);
		}
	}

	public double getTotalSupDoorFee()
	{
		return TotalSupDoorFee;
	}
	public void setTotalSupDoorFee(double aTotalSupDoorFee)
	{
		TotalSupDoorFee = Arith.round(aTotalSupDoorFee,2);
	}
	public void setTotalSupDoorFee(String aTotalSupDoorFee)
	{
		if (aTotalSupDoorFee != null && !aTotalSupDoorFee.equals(""))
		{
			Double tDouble = new Double(aTotalSupDoorFee);
			double d = tDouble.doubleValue();
                TotalSupDoorFee = Arith.round(d,2);
		}
	}

	public int getPayReceipt()
	{
		return PayReceipt;
	}
	public void setPayReceipt(int aPayReceipt)
	{
		PayReceipt = aPayReceipt;
	}
	public void setPayReceipt(String aPayReceipt)
	{
		if (aPayReceipt != null && !aPayReceipt.equals(""))
		{
			Integer tInteger = new Integer(aPayReceipt);
			int i = tInteger.intValue();
			PayReceipt = i;
		}
	}

	public int getRefuseReceipt()
	{
		return RefuseReceipt;
	}
	public void setRefuseReceipt(int aRefuseReceipt)
	{
		RefuseReceipt = aRefuseReceipt;
	}
	public void setRefuseReceipt(String aRefuseReceipt)
	{
		if (aRefuseReceipt != null && !aRefuseReceipt.equals(""))
		{
			Integer tInteger = new Integer(aRefuseReceipt);
			int i = tInteger.intValue();
			RefuseReceipt = i;
		}
	}

	public double getSupDoorFee()
	{
		return SupDoorFee;
	}
	public void setSupDoorFee(double aSupDoorFee)
	{
		SupDoorFee = Arith.round(aSupDoorFee,2);
	}
	public void setSupDoorFee(String aSupDoorFee)
	{
		if (aSupDoorFee != null && !aSupDoorFee.equals(""))
		{
			Double tDouble = new Double(aSupDoorFee);
			double d = tDouble.doubleValue();
                SupDoorFee = Arith.round(d,2);
		}
	}

	public double getYearSupDoorFee()
	{
		return YearSupDoorFee;
	}
	public void setYearSupDoorFee(double aYearSupDoorFee)
	{
		YearSupDoorFee = Arith.round(aYearSupDoorFee,2);
	}
	public void setYearSupDoorFee(String aYearSupDoorFee)
	{
		if (aYearSupDoorFee != null && !aYearSupDoorFee.equals(""))
		{
			Double tDouble = new Double(aYearSupDoorFee);
			double d = tDouble.doubleValue();
                YearSupDoorFee = Arith.round(d,2);
		}
	}

	public double getPlanFee()
	{
		return PlanFee;
	}
	public void setPlanFee(double aPlanFee)
	{
		PlanFee = Arith.round(aPlanFee,2);
	}
	public void setPlanFee(String aPlanFee)
	{
		if (aPlanFee != null && !aPlanFee.equals(""))
		{
			Double tDouble = new Double(aPlanFee);
			double d = tDouble.doubleValue();
                PlanFee = Arith.round(d,2);
		}
	}

	public double getYearPlayFee()
	{
		return YearPlayFee;
	}
	public void setYearPlayFee(double aYearPlayFee)
	{
		YearPlayFee = Arith.round(aYearPlayFee,2);
	}
	public void setYearPlayFee(String aYearPlayFee)
	{
		if (aYearPlayFee != null && !aYearPlayFee.equals(""))
		{
			Double tDouble = new Double(aYearPlayFee);
			double d = tDouble.doubleValue();
                YearPlayFee = Arith.round(d,2);
		}
	}

	public double getSupInHosFee()
	{
		return SupInHosFee;
	}
	public void setSupInHosFee(double aSupInHosFee)
	{
		SupInHosFee = Arith.round(aSupInHosFee,2);
	}
	public void setSupInHosFee(String aSupInHosFee)
	{
		if (aSupInHosFee != null && !aSupInHosFee.equals(""))
		{
			Double tDouble = new Double(aSupInHosFee);
			double d = tDouble.doubleValue();
                SupInHosFee = Arith.round(d,2);
		}
	}

	public double getYearSupInHosFee()
	{
		return YearSupInHosFee;
	}
	public void setYearSupInHosFee(double aYearSupInHosFee)
	{
		YearSupInHosFee = Arith.round(aYearSupInHosFee,2);
	}
	public void setYearSupInHosFee(String aYearSupInHosFee)
	{
		if (aYearSupInHosFee != null && !aYearSupInHosFee.equals(""))
		{
			Double tDouble = new Double(aYearSupInHosFee);
			double d = tDouble.doubleValue();
                YearSupInHosFee = Arith.round(d,2);
		}
	}

	public double getSecurityFee()
	{
		return SecurityFee;
	}
	public void setSecurityFee(double aSecurityFee)
	{
		SecurityFee = Arith.round(aSecurityFee,2);
	}
	public void setSecurityFee(String aSecurityFee)
	{
		if (aSecurityFee != null && !aSecurityFee.equals(""))
		{
			Double tDouble = new Double(aSecurityFee);
			double d = tDouble.doubleValue();
                SecurityFee = Arith.round(d,2);
		}
	}

	public double getSmallDoorPay()
	{
		return SmallDoorPay;
	}
	public void setSmallDoorPay(double aSmallDoorPay)
	{
		SmallDoorPay = Arith.round(aSmallDoorPay,2);
	}
	public void setSmallDoorPay(String aSmallDoorPay)
	{
		if (aSmallDoorPay != null && !aSmallDoorPay.equals(""))
		{
			Double tDouble = new Double(aSmallDoorPay);
			double d = tDouble.doubleValue();
                SmallDoorPay = Arith.round(d,2);
		}
	}

	public double getEmergencyPay()
	{
		return EmergencyPay;
	}
	public void setEmergencyPay(double aEmergencyPay)
	{
		EmergencyPay = Arith.round(aEmergencyPay,2);
	}
	public void setEmergencyPay(String aEmergencyPay)
	{
		if (aEmergencyPay != null && !aEmergencyPay.equals(""))
		{
			Double tDouble = new Double(aEmergencyPay);
			double d = tDouble.doubleValue();
                EmergencyPay = Arith.round(d,2);
		}
	}

	public double getSelfPay1()
	{
		return SelfPay1;
	}
	public void setSelfPay1(double aSelfPay1)
	{
		SelfPay1 = Arith.round(aSelfPay1,2);
	}
	public void setSelfPay1(String aSelfPay1)
	{
		if (aSelfPay1 != null && !aSelfPay1.equals(""))
		{
			Double tDouble = new Double(aSelfPay1);
			double d = tDouble.doubleValue();
                SelfPay1 = Arith.round(d,2);
		}
	}

	public double getSelfPay2()
	{
		return SelfPay2;
	}
	public void setSelfPay2(double aSelfPay2)
	{
		SelfPay2 = Arith.round(aSelfPay2,2);
	}
	public void setSelfPay2(String aSelfPay2)
	{
		if (aSelfPay2 != null && !aSelfPay2.equals(""))
		{
			Double tDouble = new Double(aSelfPay2);
			double d = tDouble.doubleValue();
                SelfPay2 = Arith.round(d,2);
		}
	}

	public double getSecuRefuseFee()
	{
		return SecuRefuseFee;
	}
	public void setSecuRefuseFee(double aSecuRefuseFee)
	{
		SecuRefuseFee = Arith.round(aSecuRefuseFee,2);
	}
	public void setSecuRefuseFee(String aSecuRefuseFee)
	{
		if (aSecuRefuseFee != null && !aSecuRefuseFee.equals(""))
		{
			Double tDouble = new Double(aSecuRefuseFee);
			double d = tDouble.doubleValue();
                SecuRefuseFee = Arith.round(d,2);
		}
	}

	public double getSelfAmnt()
	{
		return SelfAmnt;
	}
	public void setSelfAmnt(double aSelfAmnt)
	{
		SelfAmnt = Arith.round(aSelfAmnt,2);
	}
	public void setSelfAmnt(String aSelfAmnt)
	{
		if (aSelfAmnt != null && !aSelfAmnt.equals(""))
		{
			Double tDouble = new Double(aSelfAmnt);
			double d = tDouble.doubleValue();
                SelfAmnt = Arith.round(d,2);
		}
	}

	public double getGetLimit()
	{
		return GetLimit;
	}
	public void setGetLimit(double aGetLimit)
	{
		GetLimit = Arith.round(aGetLimit,2);
	}
	public void setGetLimit(String aGetLimit)
	{
		if (aGetLimit != null && !aGetLimit.equals(""))
		{
			Double tDouble = new Double(aGetLimit);
			double d = tDouble.doubleValue();
                GetLimit = Arith.round(d,2);
		}
	}

	public double getLowAmnt()
	{
		return LowAmnt;
	}
	public void setLowAmnt(double aLowAmnt)
	{
		LowAmnt = Arith.round(aLowAmnt,2);
	}
	public void setLowAmnt(String aLowAmnt)
	{
		if (aLowAmnt != null && !aLowAmnt.equals(""))
		{
			Double tDouble = new Double(aLowAmnt);
			double d = tDouble.doubleValue();
                LowAmnt = Arith.round(d,2);
		}
	}

	public double getMidAmnt()
	{
		return MidAmnt;
	}
	public void setMidAmnt(double aMidAmnt)
	{
		MidAmnt = Arith.round(aMidAmnt,2);
	}
	public void setMidAmnt(String aMidAmnt)
	{
		if (aMidAmnt != null && !aMidAmnt.equals(""))
		{
			Double tDouble = new Double(aMidAmnt);
			double d = tDouble.doubleValue();
                MidAmnt = Arith.round(d,2);
		}
	}

	public double getHighAmnt1()
	{
		return HighAmnt1;
	}
	public void setHighAmnt1(double aHighAmnt1)
	{
		HighAmnt1 = Arith.round(aHighAmnt1,2);
	}
	public void setHighAmnt1(String aHighAmnt1)
	{
		if (aHighAmnt1 != null && !aHighAmnt1.equals(""))
		{
			Double tDouble = new Double(aHighAmnt1);
			double d = tDouble.doubleValue();
                HighAmnt1 = Arith.round(d,2);
		}
	}

	public double getHighAmnt2()
	{
		return HighAmnt2;
	}
	public void setHighAmnt2(double aHighAmnt2)
	{
		HighAmnt2 = Arith.round(aHighAmnt2,2);
	}
	public void setHighAmnt2(String aHighAmnt2)
	{
		if (aHighAmnt2 != null && !aHighAmnt2.equals(""))
		{
			Double tDouble = new Double(aHighAmnt2);
			double d = tDouble.doubleValue();
                HighAmnt2 = Arith.round(d,2);
		}
	}

	public double getSuperAmnt()
	{
		return SuperAmnt;
	}
	public void setSuperAmnt(double aSuperAmnt)
	{
		SuperAmnt = Arith.round(aSuperAmnt,2);
	}
	public void setSuperAmnt(String aSuperAmnt)
	{
		if (aSuperAmnt != null && !aSuperAmnt.equals(""))
		{
			Double tDouble = new Double(aSuperAmnt);
			double d = tDouble.doubleValue();
                SuperAmnt = Arith.round(d,2);
		}
	}

	public String getMngCom()
	{
		return MngCom;
	}
	public void setMngCom(String aMngCom)
	{
		MngCom = aMngCom;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public double getAccountPay()
	{
		return AccountPay;
	}
	public void setAccountPay(double aAccountPay)
	{
		AccountPay = Arith.round(aAccountPay,2);
	}
	public void setAccountPay(String aAccountPay)
	{
		if (aAccountPay != null && !aAccountPay.equals(""))
		{
			Double tDouble = new Double(aAccountPay);
			double d = tDouble.doubleValue();
                AccountPay = Arith.round(d,2);
		}
	}

	public double getOfficialSubsidy()
	{
		return OfficialSubsidy;
	}
	public void setOfficialSubsidy(double aOfficialSubsidy)
	{
		OfficialSubsidy = Arith.round(aOfficialSubsidy,2);
	}
	public void setOfficialSubsidy(String aOfficialSubsidy)
	{
		if (aOfficialSubsidy != null && !aOfficialSubsidy.equals(""))
		{
			Double tDouble = new Double(aOfficialSubsidy);
			double d = tDouble.doubleValue();
                OfficialSubsidy = Arith.round(d,2);
		}
	}

	public double getHighDoorAmnt()
	{
		return HighDoorAmnt;
	}
	public void setHighDoorAmnt(double aHighDoorAmnt)
	{
		HighDoorAmnt = Arith.round(aHighDoorAmnt,2);
	}
	public void setHighDoorAmnt(String aHighDoorAmnt)
	{
		if (aHighDoorAmnt != null && !aHighDoorAmnt.equals(""))
		{
			Double tDouble = new Double(aHighDoorAmnt);
			double d = tDouble.doubleValue();
                HighDoorAmnt = Arith.round(d,2);
		}
	}

	public double getRetireAddFee()
	{
		return RetireAddFee;
	}
	public void setRetireAddFee(double aRetireAddFee)
	{
		RetireAddFee = Arith.round(aRetireAddFee,2);
	}
	public void setRetireAddFee(String aRetireAddFee)
	{
		if (aRetireAddFee != null && !aRetireAddFee.equals(""))
		{
			Double tDouble = new Double(aRetireAddFee);
			double d = tDouble.doubleValue();
                RetireAddFee = Arith.round(d,2);
		}
	}

	public double getYearRetireAddFee()
	{
		return YearRetireAddFee;
	}
	public void setYearRetireAddFee(double aYearRetireAddFee)
	{
		YearRetireAddFee = Arith.round(aYearRetireAddFee,2);
	}
	public void setYearRetireAddFee(String aYearRetireAddFee)
	{
		if (aYearRetireAddFee != null && !aYearRetireAddFee.equals(""))
		{
			Double tDouble = new Double(aYearRetireAddFee);
			double d = tDouble.doubleValue();
                YearRetireAddFee = Arith.round(d,2);
		}
	}

	public double getStandbyAmnt()
	{
		return StandbyAmnt;
	}
	public void setStandbyAmnt(double aStandbyAmnt)
	{
		StandbyAmnt = Arith.round(aStandbyAmnt,2);
	}
	public void setStandbyAmnt(String aStandbyAmnt)
	{
		if (aStandbyAmnt != null && !aStandbyAmnt.equals(""))
		{
			Double tDouble = new Double(aStandbyAmnt);
			double d = tDouble.doubleValue();
                StandbyAmnt = Arith.round(d,2);
		}
	}

	public double getOtherPay()
	{
		return OtherPay;
	}
	public void setOtherPay(double aOtherPay)
	{
		OtherPay = Arith.round(aOtherPay,2);
	}
	public void setOtherPay(String aOtherPay)
	{
		if (aOtherPay != null && !aOtherPay.equals(""))
		{
			Double tDouble = new Double(aOtherPay);
			double d = tDouble.doubleValue();
                OtherPay = Arith.round(d,2);
		}
	}

	public double getRemPay()
	{
		return RemPay;
	}
	public void setRemPay(double aRemPay)
	{
		RemPay = Arith.round(aRemPay,2);
	}
	public void setRemPay(String aRemPay)
	{
		if (aRemPay != null && !aRemPay.equals(""))
		{
			Double tDouble = new Double(aRemPay);
			double d = tDouble.doubleValue();
                RemPay = Arith.round(d,2);
		}
	}


	/**
	* 使用另外一个 LLSecurityReceiptSchema 对象给 Schema 赋值
	* @param: aLLSecurityReceiptSchema LLSecurityReceiptSchema
	**/
	public void setSchema(LLSecurityReceiptSchema aLLSecurityReceiptSchema)
	{
		this.FeeDetailNo = aLLSecurityReceiptSchema.getFeeDetailNo();
		this.MainFeeNo = aLLSecurityReceiptSchema.getMainFeeNo();
		this.RgtNo = aLLSecurityReceiptSchema.getRgtNo();
		this.CaseNo = aLLSecurityReceiptSchema.getCaseNo();
		this.ApplyAmnt = aLLSecurityReceiptSchema.getApplyAmnt();
		this.FeeInSecu = aLLSecurityReceiptSchema.getFeeInSecu();
		this.FeeOutSecu = aLLSecurityReceiptSchema.getFeeOutSecu();
		this.TotalSupDoorFee = aLLSecurityReceiptSchema.getTotalSupDoorFee();
		this.PayReceipt = aLLSecurityReceiptSchema.getPayReceipt();
		this.RefuseReceipt = aLLSecurityReceiptSchema.getRefuseReceipt();
		this.SupDoorFee = aLLSecurityReceiptSchema.getSupDoorFee();
		this.YearSupDoorFee = aLLSecurityReceiptSchema.getYearSupDoorFee();
		this.PlanFee = aLLSecurityReceiptSchema.getPlanFee();
		this.YearPlayFee = aLLSecurityReceiptSchema.getYearPlayFee();
		this.SupInHosFee = aLLSecurityReceiptSchema.getSupInHosFee();
		this.YearSupInHosFee = aLLSecurityReceiptSchema.getYearSupInHosFee();
		this.SecurityFee = aLLSecurityReceiptSchema.getSecurityFee();
		this.SmallDoorPay = aLLSecurityReceiptSchema.getSmallDoorPay();
		this.EmergencyPay = aLLSecurityReceiptSchema.getEmergencyPay();
		this.SelfPay1 = aLLSecurityReceiptSchema.getSelfPay1();
		this.SelfPay2 = aLLSecurityReceiptSchema.getSelfPay2();
		this.SecuRefuseFee = aLLSecurityReceiptSchema.getSecuRefuseFee();
		this.SelfAmnt = aLLSecurityReceiptSchema.getSelfAmnt();
		this.GetLimit = aLLSecurityReceiptSchema.getGetLimit();
		this.LowAmnt = aLLSecurityReceiptSchema.getLowAmnt();
		this.MidAmnt = aLLSecurityReceiptSchema.getMidAmnt();
		this.HighAmnt1 = aLLSecurityReceiptSchema.getHighAmnt1();
		this.HighAmnt2 = aLLSecurityReceiptSchema.getHighAmnt2();
		this.SuperAmnt = aLLSecurityReceiptSchema.getSuperAmnt();
		this.MngCom = aLLSecurityReceiptSchema.getMngCom();
		this.Operator = aLLSecurityReceiptSchema.getOperator();
		this.MakeDate = fDate.getDate( aLLSecurityReceiptSchema.getMakeDate());
		this.MakeTime = aLLSecurityReceiptSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLLSecurityReceiptSchema.getModifyDate());
		this.ModifyTime = aLLSecurityReceiptSchema.getModifyTime();
		this.AccountPay = aLLSecurityReceiptSchema.getAccountPay();
		this.OfficialSubsidy = aLLSecurityReceiptSchema.getOfficialSubsidy();
		this.HighDoorAmnt = aLLSecurityReceiptSchema.getHighDoorAmnt();
		this.RetireAddFee = aLLSecurityReceiptSchema.getRetireAddFee();
		this.YearRetireAddFee = aLLSecurityReceiptSchema.getYearRetireAddFee();
		this.StandbyAmnt = aLLSecurityReceiptSchema.getStandbyAmnt();
		this.OtherPay = aLLSecurityReceiptSchema.getOtherPay();
		this.RemPay = aLLSecurityReceiptSchema.getRemPay();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("FeeDetailNo") == null )
				this.FeeDetailNo = null;
			else
				this.FeeDetailNo = rs.getString("FeeDetailNo").trim();

			if( rs.getString("MainFeeNo") == null )
				this.MainFeeNo = null;
			else
				this.MainFeeNo = rs.getString("MainFeeNo").trim();

			if( rs.getString("RgtNo") == null )
				this.RgtNo = null;
			else
				this.RgtNo = rs.getString("RgtNo").trim();

			if( rs.getString("CaseNo") == null )
				this.CaseNo = null;
			else
				this.CaseNo = rs.getString("CaseNo").trim();

			this.ApplyAmnt = rs.getDouble("ApplyAmnt");
			this.FeeInSecu = rs.getDouble("FeeInSecu");
			this.FeeOutSecu = rs.getDouble("FeeOutSecu");
			this.TotalSupDoorFee = rs.getDouble("TotalSupDoorFee");
			this.PayReceipt = rs.getInt("PayReceipt");
			this.RefuseReceipt = rs.getInt("RefuseReceipt");
			this.SupDoorFee = rs.getDouble("SupDoorFee");
			this.YearSupDoorFee = rs.getDouble("YearSupDoorFee");
			this.PlanFee = rs.getDouble("PlanFee");
			this.YearPlayFee = rs.getDouble("YearPlayFee");
			this.SupInHosFee = rs.getDouble("SupInHosFee");
			this.YearSupInHosFee = rs.getDouble("YearSupInHosFee");
			this.SecurityFee = rs.getDouble("SecurityFee");
			this.SmallDoorPay = rs.getDouble("SmallDoorPay");
			this.EmergencyPay = rs.getDouble("EmergencyPay");
			this.SelfPay1 = rs.getDouble("SelfPay1");
			this.SelfPay2 = rs.getDouble("SelfPay2");
			this.SecuRefuseFee = rs.getDouble("SecuRefuseFee");
			this.SelfAmnt = rs.getDouble("SelfAmnt");
			this.GetLimit = rs.getDouble("GetLimit");
			this.LowAmnt = rs.getDouble("LowAmnt");
			this.MidAmnt = rs.getDouble("MidAmnt");
			this.HighAmnt1 = rs.getDouble("HighAmnt1");
			this.HighAmnt2 = rs.getDouble("HighAmnt2");
			this.SuperAmnt = rs.getDouble("SuperAmnt");
			if( rs.getString("MngCom") == null )
				this.MngCom = null;
			else
				this.MngCom = rs.getString("MngCom").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			this.AccountPay = rs.getDouble("AccountPay");
			this.OfficialSubsidy = rs.getDouble("OfficialSubsidy");
			this.HighDoorAmnt = rs.getDouble("HighDoorAmnt");
			this.RetireAddFee = rs.getDouble("RetireAddFee");
			this.YearRetireAddFee = rs.getDouble("YearRetireAddFee");
			this.StandbyAmnt = rs.getDouble("StandbyAmnt");
			this.OtherPay = rs.getDouble("OtherPay");
			this.RemPay = rs.getDouble("RemPay");
		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LLSecurityReceipt表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLSecurityReceiptSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LLSecurityReceiptSchema getSchema()
	{
		LLSecurityReceiptSchema aLLSecurityReceiptSchema = new LLSecurityReceiptSchema();
		aLLSecurityReceiptSchema.setSchema(this);
		return aLLSecurityReceiptSchema;
	}

	public LLSecurityReceiptDB getDB()
	{
		LLSecurityReceiptDB aDBOper = new LLSecurityReceiptDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLSecurityReceipt描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(FeeDetailNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MainFeeNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RgtNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CaseNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ApplyAmnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(FeeInSecu));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(FeeOutSecu));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(TotalSupDoorFee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PayReceipt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(RefuseReceipt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SupDoorFee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(YearSupDoorFee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PlanFee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(YearPlayFee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SupInHosFee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(YearSupInHosFee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SecurityFee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SmallDoorPay));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(EmergencyPay));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SelfPay1));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SelfPay2));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SecuRefuseFee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SelfAmnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(GetLimit));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(LowAmnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(MidAmnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(HighAmnt1));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(HighAmnt2));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SuperAmnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MngCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(AccountPay));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(OfficialSubsidy));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(HighDoorAmnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(RetireAddFee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(YearRetireAddFee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(StandbyAmnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(OtherPay));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(RemPay));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLSecurityReceipt>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			FeeDetailNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			MainFeeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			RgtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			CaseNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			ApplyAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,5,SysConst.PACKAGESPILTER))).doubleValue();
			FeeInSecu = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,6,SysConst.PACKAGESPILTER))).doubleValue();
			FeeOutSecu = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,7,SysConst.PACKAGESPILTER))).doubleValue();
			TotalSupDoorFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,8,SysConst.PACKAGESPILTER))).doubleValue();
			PayReceipt= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,9,SysConst.PACKAGESPILTER))).intValue();
			RefuseReceipt= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,10,SysConst.PACKAGESPILTER))).intValue();
			SupDoorFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,11,SysConst.PACKAGESPILTER))).doubleValue();
			YearSupDoorFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,12,SysConst.PACKAGESPILTER))).doubleValue();
			PlanFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,13,SysConst.PACKAGESPILTER))).doubleValue();
			YearPlayFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,14,SysConst.PACKAGESPILTER))).doubleValue();
			SupInHosFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,15,SysConst.PACKAGESPILTER))).doubleValue();
			YearSupInHosFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,16,SysConst.PACKAGESPILTER))).doubleValue();
			SecurityFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,17,SysConst.PACKAGESPILTER))).doubleValue();
			SmallDoorPay = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,18,SysConst.PACKAGESPILTER))).doubleValue();
			EmergencyPay = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,19,SysConst.PACKAGESPILTER))).doubleValue();
			SelfPay1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,20,SysConst.PACKAGESPILTER))).doubleValue();
			SelfPay2 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,21,SysConst.PACKAGESPILTER))).doubleValue();
			SecuRefuseFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,22,SysConst.PACKAGESPILTER))).doubleValue();
			SelfAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,23,SysConst.PACKAGESPILTER))).doubleValue();
			GetLimit = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,24,SysConst.PACKAGESPILTER))).doubleValue();
			LowAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,25,SysConst.PACKAGESPILTER))).doubleValue();
			MidAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,26,SysConst.PACKAGESPILTER))).doubleValue();
			HighAmnt1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,27,SysConst.PACKAGESPILTER))).doubleValue();
			HighAmnt2 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,28,SysConst.PACKAGESPILTER))).doubleValue();
			SuperAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,29,SysConst.PACKAGESPILTER))).doubleValue();
			MngCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			AccountPay = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,36,SysConst.PACKAGESPILTER))).doubleValue();
			OfficialSubsidy = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,37,SysConst.PACKAGESPILTER))).doubleValue();
			HighDoorAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,38,SysConst.PACKAGESPILTER))).doubleValue();
			RetireAddFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,39,SysConst.PACKAGESPILTER))).doubleValue();
			YearRetireAddFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,40,SysConst.PACKAGESPILTER))).doubleValue();
			StandbyAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,41,SysConst.PACKAGESPILTER))).doubleValue();
			OtherPay = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,42,SysConst.PACKAGESPILTER))).doubleValue();
			RemPay = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,43,SysConst.PACKAGESPILTER))).doubleValue();
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLSecurityReceiptSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("FeeDetailNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FeeDetailNo));
		}
		if (FCode.equals("MainFeeNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MainFeeNo));
		}
		if (FCode.equals("RgtNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RgtNo));
		}
		if (FCode.equals("CaseNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CaseNo));
		}
		if (FCode.equals("ApplyAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ApplyAmnt));
		}
		if (FCode.equals("FeeInSecu"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FeeInSecu));
		}
		if (FCode.equals("FeeOutSecu"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FeeOutSecu));
		}
		if (FCode.equals("TotalSupDoorFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TotalSupDoorFee));
		}
		if (FCode.equals("PayReceipt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayReceipt));
		}
		if (FCode.equals("RefuseReceipt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RefuseReceipt));
		}
		if (FCode.equals("SupDoorFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SupDoorFee));
		}
		if (FCode.equals("YearSupDoorFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(YearSupDoorFee));
		}
		if (FCode.equals("PlanFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PlanFee));
		}
		if (FCode.equals("YearPlayFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(YearPlayFee));
		}
		if (FCode.equals("SupInHosFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SupInHosFee));
		}
		if (FCode.equals("YearSupInHosFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(YearSupInHosFee));
		}
		if (FCode.equals("SecurityFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SecurityFee));
		}
		if (FCode.equals("SmallDoorPay"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SmallDoorPay));
		}
		if (FCode.equals("EmergencyPay"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EmergencyPay));
		}
		if (FCode.equals("SelfPay1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SelfPay1));
		}
		if (FCode.equals("SelfPay2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SelfPay2));
		}
		if (FCode.equals("SecuRefuseFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SecuRefuseFee));
		}
		if (FCode.equals("SelfAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SelfAmnt));
		}
		if (FCode.equals("GetLimit"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GetLimit));
		}
		if (FCode.equals("LowAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LowAmnt));
		}
		if (FCode.equals("MidAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MidAmnt));
		}
		if (FCode.equals("HighAmnt1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HighAmnt1));
		}
		if (FCode.equals("HighAmnt2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HighAmnt2));
		}
		if (FCode.equals("SuperAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SuperAmnt));
		}
		if (FCode.equals("MngCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MngCom));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("AccountPay"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccountPay));
		}
		if (FCode.equals("OfficialSubsidy"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OfficialSubsidy));
		}
		if (FCode.equals("HighDoorAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HighDoorAmnt));
		}
		if (FCode.equals("RetireAddFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RetireAddFee));
		}
		if (FCode.equals("YearRetireAddFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(YearRetireAddFee));
		}
		if (FCode.equals("StandbyAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyAmnt));
		}
		if (FCode.equals("OtherPay"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OtherPay));
		}
		if (FCode.equals("RemPay"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RemPay));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(FeeDetailNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(MainFeeNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(RgtNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(CaseNo);
				break;
			case 4:
				strFieldValue = String.valueOf(ApplyAmnt);
				break;
			case 5:
				strFieldValue = String.valueOf(FeeInSecu);
				break;
			case 6:
				strFieldValue = String.valueOf(FeeOutSecu);
				break;
			case 7:
				strFieldValue = String.valueOf(TotalSupDoorFee);
				break;
			case 8:
				strFieldValue = String.valueOf(PayReceipt);
				break;
			case 9:
				strFieldValue = String.valueOf(RefuseReceipt);
				break;
			case 10:
				strFieldValue = String.valueOf(SupDoorFee);
				break;
			case 11:
				strFieldValue = String.valueOf(YearSupDoorFee);
				break;
			case 12:
				strFieldValue = String.valueOf(PlanFee);
				break;
			case 13:
				strFieldValue = String.valueOf(YearPlayFee);
				break;
			case 14:
				strFieldValue = String.valueOf(SupInHosFee);
				break;
			case 15:
				strFieldValue = String.valueOf(YearSupInHosFee);
				break;
			case 16:
				strFieldValue = String.valueOf(SecurityFee);
				break;
			case 17:
				strFieldValue = String.valueOf(SmallDoorPay);
				break;
			case 18:
				strFieldValue = String.valueOf(EmergencyPay);
				break;
			case 19:
				strFieldValue = String.valueOf(SelfPay1);
				break;
			case 20:
				strFieldValue = String.valueOf(SelfPay2);
				break;
			case 21:
				strFieldValue = String.valueOf(SecuRefuseFee);
				break;
			case 22:
				strFieldValue = String.valueOf(SelfAmnt);
				break;
			case 23:
				strFieldValue = String.valueOf(GetLimit);
				break;
			case 24:
				strFieldValue = String.valueOf(LowAmnt);
				break;
			case 25:
				strFieldValue = String.valueOf(MidAmnt);
				break;
			case 26:
				strFieldValue = String.valueOf(HighAmnt1);
				break;
			case 27:
				strFieldValue = String.valueOf(HighAmnt2);
				break;
			case 28:
				strFieldValue = String.valueOf(SuperAmnt);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(MngCom);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 35:
				strFieldValue = String.valueOf(AccountPay);
				break;
			case 36:
				strFieldValue = String.valueOf(OfficialSubsidy);
				break;
			case 37:
				strFieldValue = String.valueOf(HighDoorAmnt);
				break;
			case 38:
				strFieldValue = String.valueOf(RetireAddFee);
				break;
			case 39:
				strFieldValue = String.valueOf(YearRetireAddFee);
				break;
			case 40:
				strFieldValue = String.valueOf(StandbyAmnt);
				break;
			case 41:
				strFieldValue = String.valueOf(OtherPay);
				break;
			case 42:
				strFieldValue = String.valueOf(RemPay);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("FeeDetailNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FeeDetailNo = FValue.trim();
			}
			else
				FeeDetailNo = null;
		}
		if (FCode.equalsIgnoreCase("MainFeeNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MainFeeNo = FValue.trim();
			}
			else
				MainFeeNo = null;
		}
		if (FCode.equalsIgnoreCase("RgtNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RgtNo = FValue.trim();
			}
			else
				RgtNo = null;
		}
		if (FCode.equalsIgnoreCase("CaseNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CaseNo = FValue.trim();
			}
			else
				CaseNo = null;
		}
		if (FCode.equalsIgnoreCase("ApplyAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ApplyAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("FeeInSecu"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				FeeInSecu = d;
			}
		}
		if (FCode.equalsIgnoreCase("FeeOutSecu"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				FeeOutSecu = d;
			}
		}
		if (FCode.equalsIgnoreCase("TotalSupDoorFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				TotalSupDoorFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("PayReceipt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PayReceipt = i;
			}
		}
		if (FCode.equalsIgnoreCase("RefuseReceipt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				RefuseReceipt = i;
			}
		}
		if (FCode.equalsIgnoreCase("SupDoorFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SupDoorFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("YearSupDoorFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				YearSupDoorFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("PlanFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				PlanFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("YearPlayFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				YearPlayFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("SupInHosFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SupInHosFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("YearSupInHosFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				YearSupInHosFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("SecurityFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SecurityFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("SmallDoorPay"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SmallDoorPay = d;
			}
		}
		if (FCode.equalsIgnoreCase("EmergencyPay"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				EmergencyPay = d;
			}
		}
		if (FCode.equalsIgnoreCase("SelfPay1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SelfPay1 = d;
			}
		}
		if (FCode.equalsIgnoreCase("SelfPay2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SelfPay2 = d;
			}
		}
		if (FCode.equalsIgnoreCase("SecuRefuseFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SecuRefuseFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("SelfAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SelfAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("GetLimit"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				GetLimit = d;
			}
		}
		if (FCode.equalsIgnoreCase("LowAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				LowAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("MidAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				MidAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("HighAmnt1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				HighAmnt1 = d;
			}
		}
		if (FCode.equalsIgnoreCase("HighAmnt2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				HighAmnt2 = d;
			}
		}
		if (FCode.equalsIgnoreCase("SuperAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SuperAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("MngCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MngCom = FValue.trim();
			}
			else
				MngCom = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("AccountPay"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				AccountPay = d;
			}
		}
		if (FCode.equalsIgnoreCase("OfficialSubsidy"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				OfficialSubsidy = d;
			}
		}
		if (FCode.equalsIgnoreCase("HighDoorAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				HighDoorAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("RetireAddFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				RetireAddFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("YearRetireAddFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				YearRetireAddFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("StandbyAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				StandbyAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("OtherPay"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				OtherPay = d;
			}
		}
		if (FCode.equalsIgnoreCase("RemPay"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				RemPay = d;
			}
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LLSecurityReceiptSchema other = (LLSecurityReceiptSchema)otherObject;
		return
			(FeeDetailNo == null ? other.getFeeDetailNo() == null : FeeDetailNo.equals(other.getFeeDetailNo()))
			&& (MainFeeNo == null ? other.getMainFeeNo() == null : MainFeeNo.equals(other.getMainFeeNo()))
			&& (RgtNo == null ? other.getRgtNo() == null : RgtNo.equals(other.getRgtNo()))
			&& (CaseNo == null ? other.getCaseNo() == null : CaseNo.equals(other.getCaseNo()))
			&& ApplyAmnt == other.getApplyAmnt()
			&& FeeInSecu == other.getFeeInSecu()
			&& FeeOutSecu == other.getFeeOutSecu()
			&& TotalSupDoorFee == other.getTotalSupDoorFee()
			&& PayReceipt == other.getPayReceipt()
			&& RefuseReceipt == other.getRefuseReceipt()
			&& SupDoorFee == other.getSupDoorFee()
			&& YearSupDoorFee == other.getYearSupDoorFee()
			&& PlanFee == other.getPlanFee()
			&& YearPlayFee == other.getYearPlayFee()
			&& SupInHosFee == other.getSupInHosFee()
			&& YearSupInHosFee == other.getYearSupInHosFee()
			&& SecurityFee == other.getSecurityFee()
			&& SmallDoorPay == other.getSmallDoorPay()
			&& EmergencyPay == other.getEmergencyPay()
			&& SelfPay1 == other.getSelfPay1()
			&& SelfPay2 == other.getSelfPay2()
			&& SecuRefuseFee == other.getSecuRefuseFee()
			&& SelfAmnt == other.getSelfAmnt()
			&& GetLimit == other.getGetLimit()
			&& LowAmnt == other.getLowAmnt()
			&& MidAmnt == other.getMidAmnt()
			&& HighAmnt1 == other.getHighAmnt1()
			&& HighAmnt2 == other.getHighAmnt2()
			&& SuperAmnt == other.getSuperAmnt()
			&& (MngCom == null ? other.getMngCom() == null : MngCom.equals(other.getMngCom()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& AccountPay == other.getAccountPay()
			&& OfficialSubsidy == other.getOfficialSubsidy()
			&& HighDoorAmnt == other.getHighDoorAmnt()
			&& RetireAddFee == other.getRetireAddFee()
			&& YearRetireAddFee == other.getYearRetireAddFee()
			&& StandbyAmnt == other.getStandbyAmnt()
			&& OtherPay == other.getOtherPay()
			&& RemPay == other.getRemPay();
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("FeeDetailNo") ) {
			return 0;
		}
		if( strFieldName.equals("MainFeeNo") ) {
			return 1;
		}
		if( strFieldName.equals("RgtNo") ) {
			return 2;
		}
		if( strFieldName.equals("CaseNo") ) {
			return 3;
		}
		if( strFieldName.equals("ApplyAmnt") ) {
			return 4;
		}
		if( strFieldName.equals("FeeInSecu") ) {
			return 5;
		}
		if( strFieldName.equals("FeeOutSecu") ) {
			return 6;
		}
		if( strFieldName.equals("TotalSupDoorFee") ) {
			return 7;
		}
		if( strFieldName.equals("PayReceipt") ) {
			return 8;
		}
		if( strFieldName.equals("RefuseReceipt") ) {
			return 9;
		}
		if( strFieldName.equals("SupDoorFee") ) {
			return 10;
		}
		if( strFieldName.equals("YearSupDoorFee") ) {
			return 11;
		}
		if( strFieldName.equals("PlanFee") ) {
			return 12;
		}
		if( strFieldName.equals("YearPlayFee") ) {
			return 13;
		}
		if( strFieldName.equals("SupInHosFee") ) {
			return 14;
		}
		if( strFieldName.equals("YearSupInHosFee") ) {
			return 15;
		}
		if( strFieldName.equals("SecurityFee") ) {
			return 16;
		}
		if( strFieldName.equals("SmallDoorPay") ) {
			return 17;
		}
		if( strFieldName.equals("EmergencyPay") ) {
			return 18;
		}
		if( strFieldName.equals("SelfPay1") ) {
			return 19;
		}
		if( strFieldName.equals("SelfPay2") ) {
			return 20;
		}
		if( strFieldName.equals("SecuRefuseFee") ) {
			return 21;
		}
		if( strFieldName.equals("SelfAmnt") ) {
			return 22;
		}
		if( strFieldName.equals("GetLimit") ) {
			return 23;
		}
		if( strFieldName.equals("LowAmnt") ) {
			return 24;
		}
		if( strFieldName.equals("MidAmnt") ) {
			return 25;
		}
		if( strFieldName.equals("HighAmnt1") ) {
			return 26;
		}
		if( strFieldName.equals("HighAmnt2") ) {
			return 27;
		}
		if( strFieldName.equals("SuperAmnt") ) {
			return 28;
		}
		if( strFieldName.equals("MngCom") ) {
			return 29;
		}
		if( strFieldName.equals("Operator") ) {
			return 30;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 31;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 32;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 33;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 34;
		}
		if( strFieldName.equals("AccountPay") ) {
			return 35;
		}
		if( strFieldName.equals("OfficialSubsidy") ) {
			return 36;
		}
		if( strFieldName.equals("HighDoorAmnt") ) {
			return 37;
		}
		if( strFieldName.equals("RetireAddFee") ) {
			return 38;
		}
		if( strFieldName.equals("YearRetireAddFee") ) {
			return 39;
		}
		if( strFieldName.equals("StandbyAmnt") ) {
			return 40;
		}
		if( strFieldName.equals("OtherPay") ) {
			return 41;
		}
		if( strFieldName.equals("RemPay") ) {
			return 42;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "FeeDetailNo";
				break;
			case 1:
				strFieldName = "MainFeeNo";
				break;
			case 2:
				strFieldName = "RgtNo";
				break;
			case 3:
				strFieldName = "CaseNo";
				break;
			case 4:
				strFieldName = "ApplyAmnt";
				break;
			case 5:
				strFieldName = "FeeInSecu";
				break;
			case 6:
				strFieldName = "FeeOutSecu";
				break;
			case 7:
				strFieldName = "TotalSupDoorFee";
				break;
			case 8:
				strFieldName = "PayReceipt";
				break;
			case 9:
				strFieldName = "RefuseReceipt";
				break;
			case 10:
				strFieldName = "SupDoorFee";
				break;
			case 11:
				strFieldName = "YearSupDoorFee";
				break;
			case 12:
				strFieldName = "PlanFee";
				break;
			case 13:
				strFieldName = "YearPlayFee";
				break;
			case 14:
				strFieldName = "SupInHosFee";
				break;
			case 15:
				strFieldName = "YearSupInHosFee";
				break;
			case 16:
				strFieldName = "SecurityFee";
				break;
			case 17:
				strFieldName = "SmallDoorPay";
				break;
			case 18:
				strFieldName = "EmergencyPay";
				break;
			case 19:
				strFieldName = "SelfPay1";
				break;
			case 20:
				strFieldName = "SelfPay2";
				break;
			case 21:
				strFieldName = "SecuRefuseFee";
				break;
			case 22:
				strFieldName = "SelfAmnt";
				break;
			case 23:
				strFieldName = "GetLimit";
				break;
			case 24:
				strFieldName = "LowAmnt";
				break;
			case 25:
				strFieldName = "MidAmnt";
				break;
			case 26:
				strFieldName = "HighAmnt1";
				break;
			case 27:
				strFieldName = "HighAmnt2";
				break;
			case 28:
				strFieldName = "SuperAmnt";
				break;
			case 29:
				strFieldName = "MngCom";
				break;
			case 30:
				strFieldName = "Operator";
				break;
			case 31:
				strFieldName = "MakeDate";
				break;
			case 32:
				strFieldName = "MakeTime";
				break;
			case 33:
				strFieldName = "ModifyDate";
				break;
			case 34:
				strFieldName = "ModifyTime";
				break;
			case 35:
				strFieldName = "AccountPay";
				break;
			case 36:
				strFieldName = "OfficialSubsidy";
				break;
			case 37:
				strFieldName = "HighDoorAmnt";
				break;
			case 38:
				strFieldName = "RetireAddFee";
				break;
			case 39:
				strFieldName = "YearRetireAddFee";
				break;
			case 40:
				strFieldName = "StandbyAmnt";
				break;
			case 41:
				strFieldName = "OtherPay";
				break;
			case 42:
				strFieldName = "RemPay";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("FeeDetailNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MainFeeNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RgtNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CaseNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ApplyAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("FeeInSecu") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("FeeOutSecu") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("TotalSupDoorFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("PayReceipt") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("RefuseReceipt") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("SupDoorFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("YearSupDoorFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("PlanFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("YearPlayFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SupInHosFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("YearSupInHosFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SecurityFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SmallDoorPay") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("EmergencyPay") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SelfPay1") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SelfPay2") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SecuRefuseFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SelfAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("GetLimit") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("LowAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("MidAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("HighAmnt1") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("HighAmnt2") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SuperAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("MngCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccountPay") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("OfficialSubsidy") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("HighDoorAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("RetireAddFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("YearRetireAddFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("StandbyAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("OtherPay") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("RemPay") ) {
			return Schema.TYPE_DOUBLE;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 5:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 6:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 7:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 8:
				nFieldType = Schema.TYPE_INT;
				break;
			case 9:
				nFieldType = Schema.TYPE_INT;
				break;
			case 10:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 11:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 12:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 13:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 14:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 15:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 16:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 17:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 18:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 19:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 20:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 21:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 22:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 23:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 24:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 25:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 26:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 27:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 28:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 36:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 37:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 38:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 39:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 40:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 41:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 42:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
