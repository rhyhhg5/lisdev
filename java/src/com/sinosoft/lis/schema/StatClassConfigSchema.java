/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.StatClassConfigDB;

/*
 * <p>ClassName: StatClassConfigSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 数据校验
 * @CreateDate：2006-07-04
 */
public class StatClassConfigSchema implements Schema, Cloneable {
    // @Field
    /** 校验类代码 */
    private String ClassCode;
    /** 校验类入口 */
    private String ClassEntrance;
    /** 有效性 */
    private String ValiFlag;
    /** 模块代码 */
    private String ModuCode;
    /** 模块名称 */
    private String ModuName;
    /** 模块中执行顺序号 */
    private int SerialNo;
    /** 校验类描述 */
    private String ClassDescribe;

    public static final int FIELDNUM = 7; // 数据库表的字段个数

    private static String[] PK; // 主键

    public CErrors mErrors; // 错误信息

    // @Constructor
    public StatClassConfigSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "ClassCode";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        StatClassConfigSchema cloned = (StatClassConfigSchema)super.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getClassCode() {
        return ClassCode;
    }

    public void setClassCode(String aClassCode) {
        ClassCode = aClassCode;
    }

    public String getClassEntrance() {
        return ClassEntrance;
    }

    public void setClassEntrance(String aClassEntrance) {
        ClassEntrance = aClassEntrance;
    }

    public String getValiFlag() {
        return ValiFlag;
    }

    public void setValiFlag(String aValiFlag) {
        ValiFlag = aValiFlag;
    }

    public String getModuCode() {
        return ModuCode;
    }

    public void setModuCode(String aModuCode) {
        ModuCode = aModuCode;
    }

    public String getModuName() {
        return ModuName;
    }

    public void setModuName(String aModuName) {
        ModuName = aModuName;
    }

    public int getSerialNo() {
        return SerialNo;
    }

    public void setSerialNo(int aSerialNo) {
        SerialNo = aSerialNo;
    }

    public void setSerialNo(String aSerialNo) {
        if (aSerialNo != null && !aSerialNo.equals("")) {
            Integer tInteger = new Integer(aSerialNo);
            int i = tInteger.intValue();
            SerialNo = i;
        }
    }

    public String getClassDescribe() {
        return ClassDescribe;
    }

    public void setClassDescribe(String aClassDescribe) {
        ClassDescribe = aClassDescribe;
    }

    /**
     * 使用另外一个 StatClassConfigSchema 对象给 Schema 赋值
     * @param: aStatClassConfigSchema StatClassConfigSchema
     **/
    public void setSchema(StatClassConfigSchema aStatClassConfigSchema) {
        this.ClassCode = aStatClassConfigSchema.getClassCode();
        this.ClassEntrance = aStatClassConfigSchema.getClassEntrance();
        this.ValiFlag = aStatClassConfigSchema.getValiFlag();
        this.ModuCode = aStatClassConfigSchema.getModuCode();
        this.ModuName = aStatClassConfigSchema.getModuName();
        this.SerialNo = aStatClassConfigSchema.getSerialNo();
        this.ClassDescribe = aStatClassConfigSchema.getClassDescribe();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ClassCode") == null) {
                this.ClassCode = null;
            } else {
                this.ClassCode = rs.getString("ClassCode").trim();
            }

            if (rs.getString("ClassEntrance") == null) {
                this.ClassEntrance = null;
            } else {
                this.ClassEntrance = rs.getString("ClassEntrance").trim();
            }

            if (rs.getString("ValiFlag") == null) {
                this.ValiFlag = null;
            } else {
                this.ValiFlag = rs.getString("ValiFlag").trim();
            }

            if (rs.getString("ModuCode") == null) {
                this.ModuCode = null;
            } else {
                this.ModuCode = rs.getString("ModuCode").trim();
            }

            if (rs.getString("ModuName") == null) {
                this.ModuName = null;
            } else {
                this.ModuName = rs.getString("ModuName").trim();
            }

            this.SerialNo = rs.getInt("SerialNo");
            if (rs.getString("ClassDescribe") == null) {
                this.ClassDescribe = null;
            } else {
                this.ClassDescribe = rs.getString("ClassDescribe").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的StatClassConfig表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "StatClassConfigSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public StatClassConfigSchema getSchema() {
        StatClassConfigSchema aStatClassConfigSchema = new
                StatClassConfigSchema();
        aStatClassConfigSchema.setSchema(this);
        return aStatClassConfigSchema;
    }

    public StatClassConfigDB getDB() {
        StatClassConfigDB aDBOper = new StatClassConfigDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpStatClassConfig描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(ClassCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ClassEntrance));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ValiFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModuCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModuName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SerialNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ClassDescribe));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpStatClassConfig>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            ClassCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            ClassEntrance = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                           SysConst.PACKAGESPILTER);
            ValiFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            ModuCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                      SysConst.PACKAGESPILTER);
            ModuName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                      SysConst.PACKAGESPILTER);
            SerialNo = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 6, SysConst.PACKAGESPILTER))).intValue();
            ClassDescribe = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                           SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "StatClassConfigSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("ClassCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClassCode));
        }
        if (FCode.equals("ClassEntrance")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClassEntrance));
        }
        if (FCode.equals("ValiFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ValiFlag));
        }
        if (FCode.equals("ModuCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModuCode));
        }
        if (FCode.equals("ModuName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModuName));
        }
        if (FCode.equals("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equals("ClassDescribe")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClassDescribe));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(ClassCode);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(ClassEntrance);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(ValiFlag);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(ModuCode);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(ModuName);
            break;
        case 5:
            strFieldValue = String.valueOf(SerialNo);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(ClassDescribe);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("ClassCode")) {
            if (FValue != null && !FValue.equals("")) {
                ClassCode = FValue.trim();
            } else {
                ClassCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("ClassEntrance")) {
            if (FValue != null && !FValue.equals("")) {
                ClassEntrance = FValue.trim();
            } else {
                ClassEntrance = null;
            }
        }
        if (FCode.equalsIgnoreCase("ValiFlag")) {
            if (FValue != null && !FValue.equals("")) {
                ValiFlag = FValue.trim();
            } else {
                ValiFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModuCode")) {
            if (FValue != null && !FValue.equals("")) {
                ModuCode = FValue.trim();
            } else {
                ModuCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModuName")) {
            if (FValue != null && !FValue.equals("")) {
                ModuName = FValue.trim();
            } else {
                ModuName = null;
            }
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                SerialNo = i;
            }
        }
        if (FCode.equalsIgnoreCase("ClassDescribe")) {
            if (FValue != null && !FValue.equals("")) {
                ClassDescribe = FValue.trim();
            } else {
                ClassDescribe = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        StatClassConfigSchema other = (StatClassConfigSchema) otherObject;
        return
                ClassCode.equals(other.getClassCode())
                && ClassEntrance.equals(other.getClassEntrance())
                && ValiFlag.equals(other.getValiFlag())
                && ModuCode.equals(other.getModuCode())
                && ModuName.equals(other.getModuName())
                && SerialNo == other.getSerialNo()
                && ClassDescribe.equals(other.getClassDescribe());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("ClassCode")) {
            return 0;
        }
        if (strFieldName.equals("ClassEntrance")) {
            return 1;
        }
        if (strFieldName.equals("ValiFlag")) {
            return 2;
        }
        if (strFieldName.equals("ModuCode")) {
            return 3;
        }
        if (strFieldName.equals("ModuName")) {
            return 4;
        }
        if (strFieldName.equals("SerialNo")) {
            return 5;
        }
        if (strFieldName.equals("ClassDescribe")) {
            return 6;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "ClassCode";
            break;
        case 1:
            strFieldName = "ClassEntrance";
            break;
        case 2:
            strFieldName = "ValiFlag";
            break;
        case 3:
            strFieldName = "ModuCode";
            break;
        case 4:
            strFieldName = "ModuName";
            break;
        case 5:
            strFieldName = "SerialNo";
            break;
        case 6:
            strFieldName = "ClassDescribe";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("ClassCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ClassEntrance")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ValiFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModuCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModuName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SerialNo")) {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("ClassDescribe")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_INT;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
