/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LLLockRecordDB;

/*
 * <p>ClassName: LLLockRecordSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2018-05-14
 */
public class LLLockRecordSchema implements Schema, Cloneable
{
	// @Field
	/** 保单号 */
	private String GrpContNo;
	/** 批次号 */
	private String Rgtno;
	/** 保单状态 */
	private String State;
	/** 锁定/解锁日期 */
	private Date LockDate;
	/** 锁定/解锁时间 */
	private String LockTime;
	/** 操作员 */
	private String Operator;
	/** 管理机构 */
	private String Managecom;
	/** 其他字段1 */
	private String Other1;
	/** 其他字段2 */
	private String Other2;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 解锁属性 */
	private String LockAttri;
	/** 解锁流水号 */
	private String LLFlowNumber;
	/** 项目年度实收保费 */
	private double SumGetFee;

	public static final int FIELDNUM = 16;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LLLockRecordSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "Rgtno";
		pk[1] = "LLFlowNumber";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LLLockRecordSchema cloned = (LLLockRecordSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getGrpContNo()
	{
		return GrpContNo;
	}
	public void setGrpContNo(String aGrpContNo)
	{
		GrpContNo = aGrpContNo;
	}
	public String getRgtno()
	{
		return Rgtno;
	}
	public void setRgtno(String aRgtno)
	{
		Rgtno = aRgtno;
	}
	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}
	public String getLockDate()
	{
		if( LockDate != null )
			return fDate.getString(LockDate);
		else
			return null;
	}
	public void setLockDate(Date aLockDate)
	{
		LockDate = aLockDate;
	}
	public void setLockDate(String aLockDate)
	{
		if (aLockDate != null && !aLockDate.equals("") )
		{
			LockDate = fDate.getDate( aLockDate );
		}
		else
			LockDate = null;
	}

	public String getLockTime()
	{
		return LockTime;
	}
	public void setLockTime(String aLockTime)
	{
		LockTime = aLockTime;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getManagecom()
	{
		return Managecom;
	}
	public void setManagecom(String aManagecom)
	{
		Managecom = aManagecom;
	}
	public String getOther1()
	{
		return Other1;
	}
	public void setOther1(String aOther1)
	{
		Other1 = aOther1;
	}
	public String getOther2()
	{
		return Other2;
	}
	public void setOther2(String aOther2)
	{
		Other2 = aOther2;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getLockAttri()
	{
		return LockAttri;
	}
	public void setLockAttri(String aLockAttri)
	{
		LockAttri = aLockAttri;
	}
	public String getLLFlowNumber()
	{
		return LLFlowNumber;
	}
	public void setLLFlowNumber(String aLLFlowNumber)
	{
		LLFlowNumber = aLLFlowNumber;
	}
	public double getSumGetFee()
	{
		return SumGetFee;
	}
	public void setSumGetFee(double aSumGetFee)
	{
		SumGetFee = Arith.round(aSumGetFee,2);
	}
	public void setSumGetFee(String aSumGetFee)
	{
		if (aSumGetFee != null && !aSumGetFee.equals(""))
		{
			Double tDouble = new Double(aSumGetFee);
			double d = tDouble.doubleValue();
                SumGetFee = Arith.round(d,2);
		}
	}


	/**
	* 使用另外一个 LLLockRecordSchema 对象给 Schema 赋值
	* @param: aLLLockRecordSchema LLLockRecordSchema
	**/
	public void setSchema(LLLockRecordSchema aLLLockRecordSchema)
	{
		this.GrpContNo = aLLLockRecordSchema.getGrpContNo();
		this.Rgtno = aLLLockRecordSchema.getRgtno();
		this.State = aLLLockRecordSchema.getState();
		this.LockDate = fDate.getDate( aLLLockRecordSchema.getLockDate());
		this.LockTime = aLLLockRecordSchema.getLockTime();
		this.Operator = aLLLockRecordSchema.getOperator();
		this.Managecom = aLLLockRecordSchema.getManagecom();
		this.Other1 = aLLLockRecordSchema.getOther1();
		this.Other2 = aLLLockRecordSchema.getOther2();
		this.MakeDate = fDate.getDate( aLLLockRecordSchema.getMakeDate());
		this.MakeTime = aLLLockRecordSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLLLockRecordSchema.getModifyDate());
		this.ModifyTime = aLLLockRecordSchema.getModifyTime();
		this.LockAttri = aLLLockRecordSchema.getLockAttri();
		this.LLFlowNumber = aLLLockRecordSchema.getLLFlowNumber();
		this.SumGetFee = aLLLockRecordSchema.getSumGetFee();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("GrpContNo") == null )
				this.GrpContNo = null;
			else
				this.GrpContNo = rs.getString("GrpContNo").trim();

			if( rs.getString("Rgtno") == null )
				this.Rgtno = null;
			else
				this.Rgtno = rs.getString("Rgtno").trim();

			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			this.LockDate = rs.getDate("LockDate");
			if( rs.getString("LockTime") == null )
				this.LockTime = null;
			else
				this.LockTime = rs.getString("LockTime").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			if( rs.getString("Managecom") == null )
				this.Managecom = null;
			else
				this.Managecom = rs.getString("Managecom").trim();

			if( rs.getString("Other1") == null )
				this.Other1 = null;
			else
				this.Other1 = rs.getString("Other1").trim();

			if( rs.getString("Other2") == null )
				this.Other2 = null;
			else
				this.Other2 = rs.getString("Other2").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("LockAttri") == null )
				this.LockAttri = null;
			else
				this.LockAttri = rs.getString("LockAttri").trim();

			if( rs.getString("LLFlowNumber") == null )
				this.LLFlowNumber = null;
			else
				this.LLFlowNumber = rs.getString("LLFlowNumber").trim();

			this.SumGetFee = rs.getDouble("SumGetFee");
		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LLLockRecord表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLLockRecordSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LLLockRecordSchema getSchema()
	{
		LLLockRecordSchema aLLLockRecordSchema = new LLLockRecordSchema();
		aLLLockRecordSchema.setSchema(this);
		return aLLLockRecordSchema;
	}

	public LLLockRecordDB getDB()
	{
		LLLockRecordDB aDBOper = new LLLockRecordDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLLockRecord描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Rgtno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( LockDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LockTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Managecom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Other1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Other2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LockAttri)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LLFlowNumber)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SumGetFee));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLLockRecord>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			Rgtno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			LockDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,SysConst.PACKAGESPILTER));
			LockTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			Managecom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			Other1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			Other2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			LockAttri = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			LLFlowNumber = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			SumGetFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,16,SysConst.PACKAGESPILTER))).doubleValue();
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLLockRecordSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("GrpContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
		}
		if (FCode.equals("Rgtno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Rgtno));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("LockDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getLockDate()));
		}
		if (FCode.equals("LockTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LockTime));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("Managecom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Managecom));
		}
		if (FCode.equals("Other1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Other1));
		}
		if (FCode.equals("Other2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Other2));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("LockAttri"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LockAttri));
		}
		if (FCode.equals("LLFlowNumber"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LLFlowNumber));
		}
		if (FCode.equals("SumGetFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SumGetFee));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(GrpContNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(Rgtno);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getLockDate()));
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(LockTime);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(Managecom);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(Other1);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(Other2);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(LockAttri);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(LLFlowNumber);
				break;
			case 15:
				strFieldValue = String.valueOf(SumGetFee);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("GrpContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpContNo = FValue.trim();
			}
			else
				GrpContNo = null;
		}
		if (FCode.equalsIgnoreCase("Rgtno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Rgtno = FValue.trim();
			}
			else
				Rgtno = null;
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("LockDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				LockDate = fDate.getDate( FValue );
			}
			else
				LockDate = null;
		}
		if (FCode.equalsIgnoreCase("LockTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LockTime = FValue.trim();
			}
			else
				LockTime = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("Managecom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Managecom = FValue.trim();
			}
			else
				Managecom = null;
		}
		if (FCode.equalsIgnoreCase("Other1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Other1 = FValue.trim();
			}
			else
				Other1 = null;
		}
		if (FCode.equalsIgnoreCase("Other2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Other2 = FValue.trim();
			}
			else
				Other2 = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("LockAttri"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LockAttri = FValue.trim();
			}
			else
				LockAttri = null;
		}
		if (FCode.equalsIgnoreCase("LLFlowNumber"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LLFlowNumber = FValue.trim();
			}
			else
				LLFlowNumber = null;
		}
		if (FCode.equalsIgnoreCase("SumGetFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SumGetFee = d;
			}
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LLLockRecordSchema other = (LLLockRecordSchema)otherObject;
		return
			(GrpContNo == null ? other.getGrpContNo() == null : GrpContNo.equals(other.getGrpContNo()))
			&& (Rgtno == null ? other.getRgtno() == null : Rgtno.equals(other.getRgtno()))
			&& (State == null ? other.getState() == null : State.equals(other.getState()))
			&& (LockDate == null ? other.getLockDate() == null : fDate.getString(LockDate).equals(other.getLockDate()))
			&& (LockTime == null ? other.getLockTime() == null : LockTime.equals(other.getLockTime()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (Managecom == null ? other.getManagecom() == null : Managecom.equals(other.getManagecom()))
			&& (Other1 == null ? other.getOther1() == null : Other1.equals(other.getOther1()))
			&& (Other2 == null ? other.getOther2() == null : Other2.equals(other.getOther2()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (LockAttri == null ? other.getLockAttri() == null : LockAttri.equals(other.getLockAttri()))
			&& (LLFlowNumber == null ? other.getLLFlowNumber() == null : LLFlowNumber.equals(other.getLLFlowNumber()))
			&& SumGetFee == other.getSumGetFee();
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("GrpContNo") ) {
			return 0;
		}
		if( strFieldName.equals("Rgtno") ) {
			return 1;
		}
		if( strFieldName.equals("State") ) {
			return 2;
		}
		if( strFieldName.equals("LockDate") ) {
			return 3;
		}
		if( strFieldName.equals("LockTime") ) {
			return 4;
		}
		if( strFieldName.equals("Operator") ) {
			return 5;
		}
		if( strFieldName.equals("Managecom") ) {
			return 6;
		}
		if( strFieldName.equals("Other1") ) {
			return 7;
		}
		if( strFieldName.equals("Other2") ) {
			return 8;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 9;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 10;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 11;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 12;
		}
		if( strFieldName.equals("LockAttri") ) {
			return 13;
		}
		if( strFieldName.equals("LLFlowNumber") ) {
			return 14;
		}
		if( strFieldName.equals("SumGetFee") ) {
			return 15;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "GrpContNo";
				break;
			case 1:
				strFieldName = "Rgtno";
				break;
			case 2:
				strFieldName = "State";
				break;
			case 3:
				strFieldName = "LockDate";
				break;
			case 4:
				strFieldName = "LockTime";
				break;
			case 5:
				strFieldName = "Operator";
				break;
			case 6:
				strFieldName = "Managecom";
				break;
			case 7:
				strFieldName = "Other1";
				break;
			case 8:
				strFieldName = "Other2";
				break;
			case 9:
				strFieldName = "MakeDate";
				break;
			case 10:
				strFieldName = "MakeTime";
				break;
			case 11:
				strFieldName = "ModifyDate";
				break;
			case 12:
				strFieldName = "ModifyTime";
				break;
			case 13:
				strFieldName = "LockAttri";
				break;
			case 14:
				strFieldName = "LLFlowNumber";
				break;
			case 15:
				strFieldName = "SumGetFee";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("GrpContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Rgtno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LockDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("LockTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Managecom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Other1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Other2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LockAttri") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LLFlowNumber") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SumGetFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
