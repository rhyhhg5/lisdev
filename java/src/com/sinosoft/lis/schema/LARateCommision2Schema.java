/*
 * <p>ClassName: LARateCommision2Schema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2004-12-20
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LARateCommision2DB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LARateCommision2Schema implements Schema
{
    // @Field
    /** 流水号 */
    private String SerialNo;
    /** 展业类型 */
    private String BranchType;
    /** 险种 */
    private String RiskCode;
    /** 性别 */
    private String sex;
    /** 投保年龄 */
    private int AppAge;
    /** 保险年期 */
    private int Year;
    /** 交费间隔 */
    private String PayIntv;
    /** 保单年度 */
    private int CurYear;
    /** 要素1 */
    private String F01;
    /** 要素2 */
    private String F02;
    /** 要素3 */
    private double F03;
    /** 要素4 */
    private double F04;
    /** 要素5 */
    private String F05;
    /** 要素6 */
    private String F06;
    /** 比率 */
    private double Rate;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 管理机构 */
    private String ManageCom;
    /** 费率版本 */
    private String VersionType;

    public static final int FIELDNUM = 22; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LARateCommision2Schema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "SerialNo";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getSerialNo()
    {
        if (SerialNo != null && !SerialNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            SerialNo = StrTool.unicodeToGBK(SerialNo);
        }
        return SerialNo;
    }

    public void setSerialNo(String aSerialNo)
    {
        SerialNo = aSerialNo;
    }

    public String getBranchType()
    {
        if (BranchType != null && !BranchType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            BranchType = StrTool.unicodeToGBK(BranchType);
        }
        return BranchType;
    }

    public void setBranchType(String aBranchType)
    {
        BranchType = aBranchType;
    }

    public String getRiskCode()
    {
        if (RiskCode != null && !RiskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getsex()
    {
        if (sex != null && !sex.equals("") && SysConst.CHANGECHARSET == true)
        {
            sex = StrTool.unicodeToGBK(sex);
        }
        return sex;
    }

    public void setsex(String asex)
    {
        sex = asex;
    }

    public int getAppAge()
    {
        return AppAge;
    }

    public void setAppAge(int aAppAge)
    {
        AppAge = aAppAge;
    }

    public void setAppAge(String aAppAge)
    {
        if (aAppAge != null && !aAppAge.equals(""))
        {
            Integer tInteger = new Integer(aAppAge);
            int i = tInteger.intValue();
            AppAge = i;
        }
    }

    public int getYear()
    {
        return Year;
    }

    public void setYear(int aYear)
    {
        Year = aYear;
    }

    public void setYear(String aYear)
    {
        if (aYear != null && !aYear.equals(""))
        {
            Integer tInteger = new Integer(aYear);
            int i = tInteger.intValue();
            Year = i;
        }
    }

    public String getPayIntv()
    {
        if (PayIntv != null && !PayIntv.equals("") && SysConst.CHANGECHARSET == true)
        {
            PayIntv = StrTool.unicodeToGBK(PayIntv);
        }
        return PayIntv;
    }

    public void setPayIntv(String aPayIntv)
    {
        PayIntv = aPayIntv;
    }

    public int getCurYear()
    {
        return CurYear;
    }

    public void setCurYear(int aCurYear)
    {
        CurYear = aCurYear;
    }

    public void setCurYear(String aCurYear)
    {
        if (aCurYear != null && !aCurYear.equals(""))
        {
            Integer tInteger = new Integer(aCurYear);
            int i = tInteger.intValue();
            CurYear = i;
        }
    }

    public String getF01()
    {
        if (F01 != null && !F01.equals("") && SysConst.CHANGECHARSET == true)
        {
            F01 = StrTool.unicodeToGBK(F01);
        }
        return F01;
    }

    public void setF01(String aF01)
    {
        F01 = aF01;
    }

    public String getF02()
    {
        if (F02 != null && !F02.equals("") && SysConst.CHANGECHARSET == true)
        {
            F02 = StrTool.unicodeToGBK(F02);
        }
        return F02;
    }

    public void setF02(String aF02)
    {
        F02 = aF02;
    }

    public double getF03()
    {
        return F03;
    }

    public void setF03(double aF03)
    {
        F03 = aF03;
    }

    public void setF03(String aF03)
    {
        if (aF03 != null && !aF03.equals(""))
        {
            Double tDouble = new Double(aF03);
            double d = tDouble.doubleValue();
            F03 = d;
        }
    }

    public double getF04()
    {
        return F04;
    }

    public void setF04(double aF04)
    {
        F04 = aF04;
    }

    public void setF04(String aF04)
    {
        if (aF04 != null && !aF04.equals(""))
        {
            Double tDouble = new Double(aF04);
            double d = tDouble.doubleValue();
            F04 = d;
        }
    }

    public String getF05()
    {
        if (F05 != null && !F05.equals("") && SysConst.CHANGECHARSET == true)
        {
            F05 = StrTool.unicodeToGBK(F05);
        }
        return F05;
    }

    public void setF05(String aF05)
    {
        F05 = aF05;
    }

    public String getF06()
    {
        if (F06 != null && !F06.equals("") && SysConst.CHANGECHARSET == true)
        {
            F06 = StrTool.unicodeToGBK(F06);
        }
        return F06;
    }

    public void setF06(String aF06)
    {
        F06 = aF06;
    }

    public double getRate()
    {
        return Rate;
    }

    public void setRate(double aRate)
    {
        Rate = aRate;
    }

    public void setRate(String aRate)
    {
        if (aRate != null && !aRate.equals(""))
        {
            Double tDouble = new Double(aRate);
            double d = tDouble.doubleValue();
            Rate = d;
        }
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    public String getManageCom()
    {
        if (ManageCom != null && !ManageCom.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ManageCom = StrTool.unicodeToGBK(ManageCom);
        }
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public String getVersionType()
    {
        if (VersionType != null && !VersionType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            VersionType = StrTool.unicodeToGBK(VersionType);
        }
        return VersionType;
    }

    public void setVersionType(String aVersionType)
    {
        VersionType = aVersionType;
    }

    /**
     * 使用另外一个 LARateCommision2Schema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LARateCommision2Schema aLARateCommision2Schema)
    {
        this.SerialNo = aLARateCommision2Schema.getSerialNo();
        this.BranchType = aLARateCommision2Schema.getBranchType();
        this.RiskCode = aLARateCommision2Schema.getRiskCode();
        this.sex = aLARateCommision2Schema.getsex();
        this.AppAge = aLARateCommision2Schema.getAppAge();
        this.Year = aLARateCommision2Schema.getYear();
        this.PayIntv = aLARateCommision2Schema.getPayIntv();
        this.CurYear = aLARateCommision2Schema.getCurYear();
        this.F01 = aLARateCommision2Schema.getF01();
        this.F02 = aLARateCommision2Schema.getF02();
        this.F03 = aLARateCommision2Schema.getF03();
        this.F04 = aLARateCommision2Schema.getF04();
        this.F05 = aLARateCommision2Schema.getF05();
        this.F06 = aLARateCommision2Schema.getF06();
        this.Rate = aLARateCommision2Schema.getRate();
        this.Operator = aLARateCommision2Schema.getOperator();
        this.MakeDate = fDate.getDate(aLARateCommision2Schema.getMakeDate());
        this.MakeTime = aLARateCommision2Schema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLARateCommision2Schema.getModifyDate());
        this.ModifyTime = aLARateCommision2Schema.getModifyTime();
        this.ManageCom = aLARateCommision2Schema.getManageCom();
        this.VersionType = aLARateCommision2Schema.getVersionType();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("SerialNo") == null)
            {
                this.SerialNo = null;
            }
            else
            {
                this.SerialNo = rs.getString("SerialNo").trim();
            }

            if (rs.getString("BranchType") == null)
            {
                this.BranchType = null;
            }
            else
            {
                this.BranchType = rs.getString("BranchType").trim();
            }

            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("sex") == null)
            {
                this.sex = null;
            }
            else
            {
                this.sex = rs.getString("sex").trim();
            }

            this.AppAge = rs.getInt("AppAge");
            this.Year = rs.getInt("Year");
            if (rs.getString("PayIntv") == null)
            {
                this.PayIntv = null;
            }
            else
            {
                this.PayIntv = rs.getString("PayIntv").trim();
            }

            this.CurYear = rs.getInt("CurYear");
            if (rs.getString("F01") == null)
            {
                this.F01 = null;
            }
            else
            {
                this.F01 = rs.getString("F01").trim();
            }

            if (rs.getString("F02") == null)
            {
                this.F02 = null;
            }
            else
            {
                this.F02 = rs.getString("F02").trim();
            }

            this.F03 = rs.getDouble("F03");
            this.F04 = rs.getDouble("F04");
            if (rs.getString("F05") == null)
            {
                this.F05 = null;
            }
            else
            {
                this.F05 = rs.getString("F05").trim();
            }

            if (rs.getString("F06") == null)
            {
                this.F06 = null;
            }
            else
            {
                this.F06 = rs.getString("F06").trim();
            }

            this.Rate = rs.getDouble("Rate");
            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("VersionType") == null)
            {
                this.VersionType = null;
            }
            else
            {
                this.VersionType = rs.getString("VersionType").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LARateCommision2Schema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LARateCommision2Schema getSchema()
    {
        LARateCommision2Schema aLARateCommision2Schema = new
                LARateCommision2Schema();
        aLARateCommision2Schema.setSchema(this);
        return aLARateCommision2Schema;
    }

    public LARateCommision2DB getDB()
    {
        LARateCommision2DB aDBOper = new LARateCommision2DB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLARateCommision2描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(SerialNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(BranchType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(sex)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(AppAge) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(Year) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PayIntv)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(CurYear) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(F01)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(F02)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(F03) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(F04) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(F05)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(F06)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(Rate) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ManageCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(VersionType));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLARateCommision2>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            sex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                 SysConst.PACKAGESPILTER);
            AppAge = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 5, SysConst.PACKAGESPILTER))).intValue();
            Year = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    6, SysConst.PACKAGESPILTER))).intValue();
            PayIntv = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                     SysConst.PACKAGESPILTER);
            CurYear = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 8, SysConst.PACKAGESPILTER))).intValue();
            F01 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                 SysConst.PACKAGESPILTER);
            F02 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                 SysConst.PACKAGESPILTER);
            F03 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    11, SysConst.PACKAGESPILTER))).doubleValue();
            F04 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    12, SysConst.PACKAGESPILTER))).doubleValue();
            F05 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                 SysConst.PACKAGESPILTER);
            F06 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                 SysConst.PACKAGESPILTER);
            Rate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    15, SysConst.PACKAGESPILTER))).doubleValue();
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 17, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 19, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                        SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,
                                       SysConst.PACKAGESPILTER);
            VersionType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,
                                         SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LARateCommision2Schema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("SerialNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SerialNo));
        }
        if (FCode.equals("BranchType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(BranchType));
        }
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskCode));
        }
        if (FCode.equals("sex"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(sex));
        }
        if (FCode.equals("AppAge"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AppAge));
        }
        if (FCode.equals("Year"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Year));
        }
        if (FCode.equals("PayIntv"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PayIntv));
        }
        if (FCode.equals("CurYear"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CurYear));
        }
        if (FCode.equals("F01"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(F01));
        }
        if (FCode.equals("F02"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(F02));
        }
        if (FCode.equals("F03"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(F03));
        }
        if (FCode.equals("F04"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(F04));
        }
        if (FCode.equals("F05"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(F05));
        }
        if (FCode.equals("F06"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(F06));
        }
        if (FCode.equals("Rate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Rate));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ManageCom));
        }
        if (FCode.equals("VersionType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(VersionType));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(SerialNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(BranchType);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(sex);
                break;
            case 4:
                strFieldValue = String.valueOf(AppAge);
                break;
            case 5:
                strFieldValue = String.valueOf(Year);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(PayIntv);
                break;
            case 7:
                strFieldValue = String.valueOf(CurYear);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(F01);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(F02);
                break;
            case 10:
                strFieldValue = String.valueOf(F03);
                break;
            case 11:
                strFieldValue = String.valueOf(F04);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(F05);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(F06);
                break;
            case 14:
                strFieldValue = String.valueOf(Rate);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(VersionType);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("SerialNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
            {
                SerialNo = null;
            }
        }
        if (FCode.equals("BranchType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchType = FValue.trim();
            }
            else
            {
                BranchType = null;
            }
        }
        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("sex"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                sex = FValue.trim();
            }
            else
            {
                sex = null;
            }
        }
        if (FCode.equals("AppAge"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                AppAge = i;
            }
        }
        if (FCode.equals("Year"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                Year = i;
            }
        }
        if (FCode.equals("PayIntv"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PayIntv = FValue.trim();
            }
            else
            {
                PayIntv = null;
            }
        }
        if (FCode.equals("CurYear"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                CurYear = i;
            }
        }
        if (FCode.equals("F01"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                F01 = FValue.trim();
            }
            else
            {
                F01 = null;
            }
        }
        if (FCode.equals("F02"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                F02 = FValue.trim();
            }
            else
            {
                F02 = null;
            }
        }
        if (FCode.equals("F03"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                F03 = d;
            }
        }
        if (FCode.equals("F04"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                F04 = d;
            }
        }
        if (FCode.equals("F05"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                F05 = FValue.trim();
            }
            else
            {
                F05 = null;
            }
        }
        if (FCode.equals("F06"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                F06 = FValue.trim();
            }
            else
            {
                F06 = null;
            }
        }
        if (FCode.equals("Rate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Rate = d;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("VersionType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                VersionType = FValue.trim();
            }
            else
            {
                VersionType = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LARateCommision2Schema other = (LARateCommision2Schema) otherObject;
        return
                SerialNo.equals(other.getSerialNo())
                && BranchType.equals(other.getBranchType())
                && RiskCode.equals(other.getRiskCode())
                && sex.equals(other.getsex())
                && AppAge == other.getAppAge()
                && Year == other.getYear()
                && PayIntv.equals(other.getPayIntv())
                && CurYear == other.getCurYear()
                && F01.equals(other.getF01())
                && F02.equals(other.getF02())
                && F03 == other.getF03()
                && F04 == other.getF04()
                && F05.equals(other.getF05())
                && F06.equals(other.getF06())
                && Rate == other.getRate()
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && ManageCom.equals(other.getManageCom())
                && VersionType.equals(other.getVersionType());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("SerialNo"))
        {
            return 0;
        }
        if (strFieldName.equals("BranchType"))
        {
            return 1;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return 2;
        }
        if (strFieldName.equals("sex"))
        {
            return 3;
        }
        if (strFieldName.equals("AppAge"))
        {
            return 4;
        }
        if (strFieldName.equals("Year"))
        {
            return 5;
        }
        if (strFieldName.equals("PayIntv"))
        {
            return 6;
        }
        if (strFieldName.equals("CurYear"))
        {
            return 7;
        }
        if (strFieldName.equals("F01"))
        {
            return 8;
        }
        if (strFieldName.equals("F02"))
        {
            return 9;
        }
        if (strFieldName.equals("F03"))
        {
            return 10;
        }
        if (strFieldName.equals("F04"))
        {
            return 11;
        }
        if (strFieldName.equals("F05"))
        {
            return 12;
        }
        if (strFieldName.equals("F06"))
        {
            return 13;
        }
        if (strFieldName.equals("Rate"))
        {
            return 14;
        }
        if (strFieldName.equals("Operator"))
        {
            return 15;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 16;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 17;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 18;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 19;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return 20;
        }
        if (strFieldName.equals("VersionType"))
        {
            return 21;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "SerialNo";
                break;
            case 1:
                strFieldName = "BranchType";
                break;
            case 2:
                strFieldName = "RiskCode";
                break;
            case 3:
                strFieldName = "sex";
                break;
            case 4:
                strFieldName = "AppAge";
                break;
            case 5:
                strFieldName = "Year";
                break;
            case 6:
                strFieldName = "PayIntv";
                break;
            case 7:
                strFieldName = "CurYear";
                break;
            case 8:
                strFieldName = "F01";
                break;
            case 9:
                strFieldName = "F02";
                break;
            case 10:
                strFieldName = "F03";
                break;
            case 11:
                strFieldName = "F04";
                break;
            case 12:
                strFieldName = "F05";
                break;
            case 13:
                strFieldName = "F06";
                break;
            case 14:
                strFieldName = "Rate";
                break;
            case 15:
                strFieldName = "Operator";
                break;
            case 16:
                strFieldName = "MakeDate";
                break;
            case 17:
                strFieldName = "MakeTime";
                break;
            case 18:
                strFieldName = "ModifyDate";
                break;
            case 19:
                strFieldName = "ModifyTime";
                break;
            case 20:
                strFieldName = "ManageCom";
                break;
            case 21:
                strFieldName = "VersionType";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("SerialNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("sex"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppAge"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("Year"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("PayIntv"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CurYear"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("F01"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("F02"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("F03"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("F04"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("F05"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("F06"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Rate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("VersionType"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_INT;
                break;
            case 5:
                nFieldType = Schema.TYPE_INT;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_INT;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 11:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 17:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 18:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 19:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 20:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 21:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
