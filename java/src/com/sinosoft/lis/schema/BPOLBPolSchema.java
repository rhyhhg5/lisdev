/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.BPOLBPolDB;

/*
 * <p>ClassName: BPOLBPolSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 外包录入
 * @CreateDate：2007-10-10
 */
public class BPOLBPolSchema implements Schema, Cloneable
{
	// @Field
	/** 处理次数 */
	private int DealCount;
	/** 外包批次 */
	private String BPOBatchNo;
	/** 险种id */
	private int PolID;
	/** 合同id */
	private int ContID;
	/** 印刷号 */
	private String PrtNo;
	/** 险种代码 */
	private String RiskCode;
	/** 保单类型 */
	private String FamilyType;
	/** 主险id */
	private String MainPolID;
	/** 被保人id */
	private int InsuredID;
	/** 与投保人关系 */
	private String RelationToAppnt;
	/** 与主被保人关系 */
	private String RelationToMainInsured;
	/** 投保人id */
	private int AppntID;
	/** 连身被保险人id */
	private String RelaId;
	/** 投保申请日期 */
	private Date PolApplyDate;
	/** 生效日期 */
	private Date CValiDate;
	/** 初审人 */
	private String FirstTrialOperator;
	/** 初审时间 */
	private String FirstTrialTime;
	/** 收单人 */
	private String ReceiveOperator;
	/** 收单日期 */
	private Date ReceiveDate;
	/** 收单时间 */
	private String ReceiveTime;
	/** 暂收据号 */
	private String TempFeeNo;
	/** 交费方式 */
	private String PayMode;
	/** 生存保险金领取方式 */
	private String LiveGetMode;
	/** 身故金领取方式 */
	private String DeadGetMode;
	/** 交费间隔 */
	private int PayIntv;
	/** 保险期间 */
	private int InsuYear;
	/** 保险期间标志 */
	private String InsuYearFlag;
	/** 交费年期 */
	private int PayEndYear;
	/** 交费年期标志 */
	private String PayEndYearFlag;
	/** 年金开始领取年龄/年期 */
	private int GetYear;
	/** 年金开始领取标志 */
	private String GetYearFlag;
	/** 起领日期计算类型 */
	private String GetStartType;
	/** 年金领取类型 */
	private String GetDutyKind;
	/** 红利领取方式 */
	private String BonusGetMode;
	/** 计算方向 */
	private String PremToAmnt;
	/** 档次 */
	private double Mult;
	/** 保费 */
	private double Prem;
	/** 保额 */
	private double Amnt;
	/** 计算规则 */
	private String CalRule;
	/** 费率 */
	private double FloatRate;
	/** 免赔额 */
	private double GetLimit;
	/** 赔付比例 */
	private double GetRate;
	/** 是否体检 */
	private String HealthCheckFlag;
	/** 溢交保费方式 */
	private String OutPayFlag;
	/** 管理机构 */
	private String ManageCom;
	/** 销售渠道 */
	private String SaleChnl;
	/** 代理人编码 */
	private String AgentCode;
	/** 代理人姓名 */
	private String AgentName;
	/** 代理机构 */
	private String AgentCom;
	/** 银行营业网点 */
	private String BankWorkSite;
	/** 备用字段1 */
	private String StandbyFlag1;
	/** 备用字段2 */
	private String StandbyFlag2;
	/** 备用字段3 */
	private String StandbyFlag3;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 58;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public BPOLBPolSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[0];

		PK = pk;
	}

            /**
             * Schema克隆
             * @return Object
             * @throws CloneNotSupportedException
             */
            public Object clone()
                    throws CloneNotSupportedException
            {
                BPOLBPolSchema cloned = (BPOLBPolSchema)super.clone();
                cloned.fDate = (FDate) fDate.clone();
                cloned.mErrors = (CErrors) mErrors.clone();
                return cloned;
            }

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public int getDealCount()
	{
		return DealCount;
	}
	public void setDealCount(int aDealCount)
	{
            DealCount = aDealCount;
	}
	public void setDealCount(String aDealCount)
	{
		if (aDealCount != null && !aDealCount.equals(""))
		{
			Integer tInteger = new Integer(aDealCount);
			int i = tInteger.intValue();
			DealCount = i;
		}
	}

	public String getBPOBatchNo()
	{
		return BPOBatchNo;
	}
	public void setBPOBatchNo(String aBPOBatchNo)
	{
            BPOBatchNo = aBPOBatchNo;
	}
	public int getPolID()
	{
		return PolID;
	}
	public void setPolID(int aPolID)
	{
            PolID = aPolID;
	}
	public void setPolID(String aPolID)
	{
		if (aPolID != null && !aPolID.equals(""))
		{
			Integer tInteger = new Integer(aPolID);
			int i = tInteger.intValue();
			PolID = i;
		}
	}

	public int getContID()
	{
		return ContID;
	}
	public void setContID(int aContID)
	{
            ContID = aContID;
	}
	public void setContID(String aContID)
	{
		if (aContID != null && !aContID.equals(""))
		{
			Integer tInteger = new Integer(aContID);
			int i = tInteger.intValue();
			ContID = i;
		}
	}

	public String getPrtNo()
	{
		return PrtNo;
	}
	public void setPrtNo(String aPrtNo)
	{
            PrtNo = aPrtNo;
	}
	public String getRiskCode()
	{
		return RiskCode;
	}
	public void setRiskCode(String aRiskCode)
	{
            RiskCode = aRiskCode;
	}
	public String getFamilyType()
	{
		return FamilyType;
	}
	public void setFamilyType(String aFamilyType)
	{
            FamilyType = aFamilyType;
	}
	public String getMainPolID()
	{
		return MainPolID;
	}
	public void setMainPolID(String aMainPolID)
	{
            MainPolID = aMainPolID;
	}
	public int getInsuredID()
	{
		return InsuredID;
	}
	public void setInsuredID(int aInsuredID)
	{
            InsuredID = aInsuredID;
	}
	public void setInsuredID(String aInsuredID)
	{
		if (aInsuredID != null && !aInsuredID.equals(""))
		{
			Integer tInteger = new Integer(aInsuredID);
			int i = tInteger.intValue();
			InsuredID = i;
		}
	}

	public String getRelationToAppnt()
	{
		return RelationToAppnt;
	}
	public void setRelationToAppnt(String aRelationToAppnt)
	{
            RelationToAppnt = aRelationToAppnt;
	}
	public String getRelationToMainInsured()
	{
		return RelationToMainInsured;
	}
	public void setRelationToMainInsured(String aRelationToMainInsured)
	{
            RelationToMainInsured = aRelationToMainInsured;
	}
	public int getAppntID()
	{
		return AppntID;
	}
	public void setAppntID(int aAppntID)
	{
            AppntID = aAppntID;
	}
	public void setAppntID(String aAppntID)
	{
		if (aAppntID != null && !aAppntID.equals(""))
		{
			Integer tInteger = new Integer(aAppntID);
			int i = tInteger.intValue();
			AppntID = i;
		}
	}

	public String getRelaId()
	{
		return RelaId;
	}
	public void setRelaId(String aRelaId)
	{
            RelaId = aRelaId;
	}
	public String getPolApplyDate()
	{
		if( PolApplyDate != null )
			return fDate.getString(PolApplyDate);
		else
			return null;
	}
	public void setPolApplyDate(Date aPolApplyDate)
	{
            PolApplyDate = aPolApplyDate;
	}
	public void setPolApplyDate(String aPolApplyDate)
	{
		if (aPolApplyDate != null && !aPolApplyDate.equals("") )
		{
			PolApplyDate = fDate.getDate( aPolApplyDate );
		}
		else
			PolApplyDate = null;
	}

	public String getCValiDate()
	{
		if( CValiDate != null )
			return fDate.getString(CValiDate);
		else
			return null;
	}
	public void setCValiDate(Date aCValiDate)
	{
            CValiDate = aCValiDate;
	}
	public void setCValiDate(String aCValiDate)
	{
		if (aCValiDate != null && !aCValiDate.equals("") )
		{
			CValiDate = fDate.getDate( aCValiDate );
		}
		else
			CValiDate = null;
	}

	public String getFirstTrialOperator()
	{
		return FirstTrialOperator;
	}
	public void setFirstTrialOperator(String aFirstTrialOperator)
	{
            FirstTrialOperator = aFirstTrialOperator;
	}
	public String getFirstTrialTime()
	{
		return FirstTrialTime;
	}
	public void setFirstTrialTime(String aFirstTrialTime)
	{
            FirstTrialTime = aFirstTrialTime;
	}
	public String getReceiveOperator()
	{
		return ReceiveOperator;
	}
	public void setReceiveOperator(String aReceiveOperator)
	{
            ReceiveOperator = aReceiveOperator;
	}
	public String getReceiveDate()
	{
		if( ReceiveDate != null )
			return fDate.getString(ReceiveDate);
		else
			return null;
	}
	public void setReceiveDate(Date aReceiveDate)
	{
            ReceiveDate = aReceiveDate;
	}
	public void setReceiveDate(String aReceiveDate)
	{
		if (aReceiveDate != null && !aReceiveDate.equals("") )
		{
			ReceiveDate = fDate.getDate( aReceiveDate );
		}
		else
			ReceiveDate = null;
	}

	public String getReceiveTime()
	{
		return ReceiveTime;
	}
	public void setReceiveTime(String aReceiveTime)
	{
            ReceiveTime = aReceiveTime;
	}
	public String getTempFeeNo()
	{
		return TempFeeNo;
	}
	public void setTempFeeNo(String aTempFeeNo)
	{
            TempFeeNo = aTempFeeNo;
	}
	public String getPayMode()
	{
		return PayMode;
	}
	public void setPayMode(String aPayMode)
	{
            PayMode = aPayMode;
	}
	public String getLiveGetMode()
	{
		return LiveGetMode;
	}
	public void setLiveGetMode(String aLiveGetMode)
	{
            LiveGetMode = aLiveGetMode;
	}
	public String getDeadGetMode()
	{
		return DeadGetMode;
	}
	public void setDeadGetMode(String aDeadGetMode)
	{
            DeadGetMode = aDeadGetMode;
	}
	public int getPayIntv()
	{
		return PayIntv;
	}
	public void setPayIntv(int aPayIntv)
	{
            PayIntv = aPayIntv;
	}
	public void setPayIntv(String aPayIntv)
	{
		if (aPayIntv != null && !aPayIntv.equals(""))
		{
			Integer tInteger = new Integer(aPayIntv);
			int i = tInteger.intValue();
			PayIntv = i;
		}
	}

	public int getInsuYear()
	{
		return InsuYear;
	}
	public void setInsuYear(int aInsuYear)
	{
            InsuYear = aInsuYear;
	}
	public void setInsuYear(String aInsuYear)
	{
		if (aInsuYear != null && !aInsuYear.equals(""))
		{
			Integer tInteger = new Integer(aInsuYear);
			int i = tInteger.intValue();
			InsuYear = i;
		}
	}

	public String getInsuYearFlag()
	{
		return InsuYearFlag;
	}
	public void setInsuYearFlag(String aInsuYearFlag)
	{
            InsuYearFlag = aInsuYearFlag;
	}
	public int getPayEndYear()
	{
		return PayEndYear;
	}
	public void setPayEndYear(int aPayEndYear)
	{
            PayEndYear = aPayEndYear;
	}
	public void setPayEndYear(String aPayEndYear)
	{
		if (aPayEndYear != null && !aPayEndYear.equals(""))
		{
			Integer tInteger = new Integer(aPayEndYear);
			int i = tInteger.intValue();
			PayEndYear = i;
		}
	}

	public String getPayEndYearFlag()
	{
		return PayEndYearFlag;
	}
	public void setPayEndYearFlag(String aPayEndYearFlag)
	{
            PayEndYearFlag = aPayEndYearFlag;
	}
	public int getGetYear()
	{
		return GetYear;
	}
	public void setGetYear(int aGetYear)
	{
            GetYear = aGetYear;
	}
	public void setGetYear(String aGetYear)
	{
		if (aGetYear != null && !aGetYear.equals(""))
		{
			Integer tInteger = new Integer(aGetYear);
			int i = tInteger.intValue();
			GetYear = i;
		}
	}

	public String getGetYearFlag()
	{
		return GetYearFlag;
	}
	public void setGetYearFlag(String aGetYearFlag)
	{
            GetYearFlag = aGetYearFlag;
	}
	public String getGetStartType()
	{
		return GetStartType;
	}
	public void setGetStartType(String aGetStartType)
	{
            GetStartType = aGetStartType;
	}
	public String getGetDutyKind()
	{
		return GetDutyKind;
	}
	public void setGetDutyKind(String aGetDutyKind)
	{
            GetDutyKind = aGetDutyKind;
	}
	public String getBonusGetMode()
	{
		return BonusGetMode;
	}
	public void setBonusGetMode(String aBonusGetMode)
	{
            BonusGetMode = aBonusGetMode;
	}
	public String getPremToAmnt()
	{
		return PremToAmnt;
	}
	public void setPremToAmnt(String aPremToAmnt)
	{
            PremToAmnt = aPremToAmnt;
	}
	public double getMult()
	{
		return Mult;
	}
	public void setMult(double aMult)
	{
            Mult = Arith.round(aMult,5);
	}
	public void setMult(String aMult)
	{
		if (aMult != null && !aMult.equals(""))
		{
			Double tDouble = new Double(aMult);
			double d = tDouble.doubleValue();
                Mult = Arith.round(d,5);
		}
	}

	public double getPrem()
	{
		return Prem;
	}
	public void setPrem(double aPrem)
	{
            Prem = Arith.round(aPrem,2);
	}
	public void setPrem(String aPrem)
	{
		if (aPrem != null && !aPrem.equals(""))
		{
			Double tDouble = new Double(aPrem);
			double d = tDouble.doubleValue();
                Prem = Arith.round(d,2);
		}
	}

	public double getAmnt()
	{
		return Amnt;
	}
	public void setAmnt(double aAmnt)
	{
            Amnt = Arith.round(aAmnt,2);
	}
	public void setAmnt(String aAmnt)
	{
		if (aAmnt != null && !aAmnt.equals(""))
		{
			Double tDouble = new Double(aAmnt);
			double d = tDouble.doubleValue();
                Amnt = Arith.round(d,2);
		}
	}

	public String getCalRule()
	{
		return CalRule;
	}
	public void setCalRule(String aCalRule)
	{
            CalRule = aCalRule;
	}
	public double getFloatRate()
	{
		return FloatRate;
	}
	public void setFloatRate(double aFloatRate)
	{
            FloatRate = Arith.round(aFloatRate,10);
	}
	public void setFloatRate(String aFloatRate)
	{
		if (aFloatRate != null && !aFloatRate.equals(""))
		{
			Double tDouble = new Double(aFloatRate);
			double d = tDouble.doubleValue();
                FloatRate = Arith.round(d,10);
		}
	}

	public double getGetLimit()
	{
		return GetLimit;
	}
	public void setGetLimit(double aGetLimit)
	{
            GetLimit = Arith.round(aGetLimit,2);
	}
	public void setGetLimit(String aGetLimit)
	{
		if (aGetLimit != null && !aGetLimit.equals(""))
		{
			Double tDouble = new Double(aGetLimit);
			double d = tDouble.doubleValue();
                GetLimit = Arith.round(d,2);
		}
	}

	public double getGetRate()
	{
		return GetRate;
	}
	public void setGetRate(double aGetRate)
	{
            GetRate = Arith.round(aGetRate,6);
	}
	public void setGetRate(String aGetRate)
	{
		if (aGetRate != null && !aGetRate.equals(""))
		{
			Double tDouble = new Double(aGetRate);
			double d = tDouble.doubleValue();
                GetRate = Arith.round(d,6);
		}
	}

	public String getHealthCheckFlag()
	{
		return HealthCheckFlag;
	}
	public void setHealthCheckFlag(String aHealthCheckFlag)
	{
            HealthCheckFlag = aHealthCheckFlag;
	}
	public String getOutPayFlag()
	{
		return OutPayFlag;
	}
	public void setOutPayFlag(String aOutPayFlag)
	{
            OutPayFlag = aOutPayFlag;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
            ManageCom = aManageCom;
	}
	public String getSaleChnl()
	{
		return SaleChnl;
	}
	public void setSaleChnl(String aSaleChnl)
	{
            SaleChnl = aSaleChnl;
	}
	public String getAgentCode()
	{
		return AgentCode;
	}
	public void setAgentCode(String aAgentCode)
	{
            AgentCode = aAgentCode;
	}
	public String getAgentName()
	{
		return AgentName;
	}
	public void setAgentName(String aAgentName)
	{
            AgentName = aAgentName;
	}
	public String getAgentCom()
	{
		return AgentCom;
	}
	public void setAgentCom(String aAgentCom)
	{
            AgentCom = aAgentCom;
	}
	public String getBankWorkSite()
	{
		return BankWorkSite;
	}
	public void setBankWorkSite(String aBankWorkSite)
	{
            BankWorkSite = aBankWorkSite;
	}
	public String getStandbyFlag1()
	{
		return StandbyFlag1;
	}
	public void setStandbyFlag1(String aStandbyFlag1)
	{
            StandbyFlag1 = aStandbyFlag1;
	}
	public String getStandbyFlag2()
	{
		return StandbyFlag2;
	}
	public void setStandbyFlag2(String aStandbyFlag2)
	{
            StandbyFlag2 = aStandbyFlag2;
	}
	public String getStandbyFlag3()
	{
		return StandbyFlag3;
	}
	public void setStandbyFlag3(String aStandbyFlag3)
	{
            StandbyFlag3 = aStandbyFlag3;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
            Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
            MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
            MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
            ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
            ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 BPOLBPolSchema 对象给 Schema 赋值
	* @param: aBPOLBPolSchema BPOLBPolSchema
	**/
	public void setSchema(BPOLBPolSchema aBPOLBPolSchema)
	{
		this.DealCount = aBPOLBPolSchema.getDealCount();
		this.BPOBatchNo = aBPOLBPolSchema.getBPOBatchNo();
		this.PolID = aBPOLBPolSchema.getPolID();
		this.ContID = aBPOLBPolSchema.getContID();
		this.PrtNo = aBPOLBPolSchema.getPrtNo();
		this.RiskCode = aBPOLBPolSchema.getRiskCode();
		this.FamilyType = aBPOLBPolSchema.getFamilyType();
		this.MainPolID = aBPOLBPolSchema.getMainPolID();
		this.InsuredID = aBPOLBPolSchema.getInsuredID();
		this.RelationToAppnt = aBPOLBPolSchema.getRelationToAppnt();
		this.RelationToMainInsured = aBPOLBPolSchema.getRelationToMainInsured();
		this.AppntID = aBPOLBPolSchema.getAppntID();
		this.RelaId = aBPOLBPolSchema.getRelaId();
		this.PolApplyDate = fDate.getDate( aBPOLBPolSchema.getPolApplyDate());
		this.CValiDate = fDate.getDate( aBPOLBPolSchema.getCValiDate());
		this.FirstTrialOperator = aBPOLBPolSchema.getFirstTrialOperator();
		this.FirstTrialTime = aBPOLBPolSchema.getFirstTrialTime();
		this.ReceiveOperator = aBPOLBPolSchema.getReceiveOperator();
		this.ReceiveDate = fDate.getDate( aBPOLBPolSchema.getReceiveDate());
		this.ReceiveTime = aBPOLBPolSchema.getReceiveTime();
		this.TempFeeNo = aBPOLBPolSchema.getTempFeeNo();
		this.PayMode = aBPOLBPolSchema.getPayMode();
		this.LiveGetMode = aBPOLBPolSchema.getLiveGetMode();
		this.DeadGetMode = aBPOLBPolSchema.getDeadGetMode();
		this.PayIntv = aBPOLBPolSchema.getPayIntv();
		this.InsuYear = aBPOLBPolSchema.getInsuYear();
		this.InsuYearFlag = aBPOLBPolSchema.getInsuYearFlag();
		this.PayEndYear = aBPOLBPolSchema.getPayEndYear();
		this.PayEndYearFlag = aBPOLBPolSchema.getPayEndYearFlag();
		this.GetYear = aBPOLBPolSchema.getGetYear();
		this.GetYearFlag = aBPOLBPolSchema.getGetYearFlag();
		this.GetStartType = aBPOLBPolSchema.getGetStartType();
		this.GetDutyKind = aBPOLBPolSchema.getGetDutyKind();
		this.BonusGetMode = aBPOLBPolSchema.getBonusGetMode();
		this.PremToAmnt = aBPOLBPolSchema.getPremToAmnt();
		this.Mult = aBPOLBPolSchema.getMult();
		this.Prem = aBPOLBPolSchema.getPrem();
		this.Amnt = aBPOLBPolSchema.getAmnt();
		this.CalRule = aBPOLBPolSchema.getCalRule();
		this.FloatRate = aBPOLBPolSchema.getFloatRate();
		this.GetLimit = aBPOLBPolSchema.getGetLimit();
		this.GetRate = aBPOLBPolSchema.getGetRate();
		this.HealthCheckFlag = aBPOLBPolSchema.getHealthCheckFlag();
		this.OutPayFlag = aBPOLBPolSchema.getOutPayFlag();
		this.ManageCom = aBPOLBPolSchema.getManageCom();
		this.SaleChnl = aBPOLBPolSchema.getSaleChnl();
		this.AgentCode = aBPOLBPolSchema.getAgentCode();
		this.AgentName = aBPOLBPolSchema.getAgentName();
		this.AgentCom = aBPOLBPolSchema.getAgentCom();
		this.BankWorkSite = aBPOLBPolSchema.getBankWorkSite();
		this.StandbyFlag1 = aBPOLBPolSchema.getStandbyFlag1();
		this.StandbyFlag2 = aBPOLBPolSchema.getStandbyFlag2();
		this.StandbyFlag3 = aBPOLBPolSchema.getStandbyFlag3();
		this.Operator = aBPOLBPolSchema.getOperator();
		this.MakeDate = fDate.getDate( aBPOLBPolSchema.getMakeDate());
		this.MakeTime = aBPOLBPolSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aBPOLBPolSchema.getModifyDate());
		this.ModifyTime = aBPOLBPolSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			this.DealCount = rs.getInt("DealCount");
			if( rs.getString("BPOBatchNo") == null )
				this.BPOBatchNo = null;
			else
				this.BPOBatchNo = rs.getString("BPOBatchNo").trim();

			this.PolID = rs.getInt("PolID");
			this.ContID = rs.getInt("ContID");
			if( rs.getString("PrtNo") == null )
				this.PrtNo = null;
			else
				this.PrtNo = rs.getString("PrtNo").trim();

			if( rs.getString("RiskCode") == null )
				this.RiskCode = null;
			else
				this.RiskCode = rs.getString("RiskCode").trim();

			if( rs.getString("FamilyType") == null )
				this.FamilyType = null;
			else
				this.FamilyType = rs.getString("FamilyType").trim();

			if( rs.getString("MainPolID") == null )
				this.MainPolID = null;
			else
				this.MainPolID = rs.getString("MainPolID").trim();

			this.InsuredID = rs.getInt("InsuredID");
			if( rs.getString("RelationToAppnt") == null )
				this.RelationToAppnt = null;
			else
				this.RelationToAppnt = rs.getString("RelationToAppnt").trim();

			if( rs.getString("RelationToMainInsured") == null )
				this.RelationToMainInsured = null;
			else
				this.RelationToMainInsured = rs.getString("RelationToMainInsured").trim();

			this.AppntID = rs.getInt("AppntID");
			if( rs.getString("RelaId") == null )
				this.RelaId = null;
			else
				this.RelaId = rs.getString("RelaId").trim();

			this.PolApplyDate = rs.getDate("PolApplyDate");
			this.CValiDate = rs.getDate("CValiDate");
			if( rs.getString("FirstTrialOperator") == null )
				this.FirstTrialOperator = null;
			else
				this.FirstTrialOperator = rs.getString("FirstTrialOperator").trim();

			if( rs.getString("FirstTrialTime") == null )
				this.FirstTrialTime = null;
			else
				this.FirstTrialTime = rs.getString("FirstTrialTime").trim();

			if( rs.getString("ReceiveOperator") == null )
				this.ReceiveOperator = null;
			else
				this.ReceiveOperator = rs.getString("ReceiveOperator").trim();

			this.ReceiveDate = rs.getDate("ReceiveDate");
			if( rs.getString("ReceiveTime") == null )
				this.ReceiveTime = null;
			else
				this.ReceiveTime = rs.getString("ReceiveTime").trim();

			if( rs.getString("TempFeeNo") == null )
				this.TempFeeNo = null;
			else
				this.TempFeeNo = rs.getString("TempFeeNo").trim();

			if( rs.getString("PayMode") == null )
				this.PayMode = null;
			else
				this.PayMode = rs.getString("PayMode").trim();

			if( rs.getString("LiveGetMode") == null )
				this.LiveGetMode = null;
			else
				this.LiveGetMode = rs.getString("LiveGetMode").trim();

			if( rs.getString("DeadGetMode") == null )
				this.DeadGetMode = null;
			else
				this.DeadGetMode = rs.getString("DeadGetMode").trim();

			this.PayIntv = rs.getInt("PayIntv");
			this.InsuYear = rs.getInt("InsuYear");
			if( rs.getString("InsuYearFlag") == null )
				this.InsuYearFlag = null;
			else
				this.InsuYearFlag = rs.getString("InsuYearFlag").trim();

			this.PayEndYear = rs.getInt("PayEndYear");
			if( rs.getString("PayEndYearFlag") == null )
				this.PayEndYearFlag = null;
			else
				this.PayEndYearFlag = rs.getString("PayEndYearFlag").trim();

			this.GetYear = rs.getInt("GetYear");
			if( rs.getString("GetYearFlag") == null )
				this.GetYearFlag = null;
			else
				this.GetYearFlag = rs.getString("GetYearFlag").trim();

			if( rs.getString("GetStartType") == null )
				this.GetStartType = null;
			else
				this.GetStartType = rs.getString("GetStartType").trim();

			if( rs.getString("GetDutyKind") == null )
				this.GetDutyKind = null;
			else
				this.GetDutyKind = rs.getString("GetDutyKind").trim();

			if( rs.getString("BonusGetMode") == null )
				this.BonusGetMode = null;
			else
				this.BonusGetMode = rs.getString("BonusGetMode").trim();

			if( rs.getString("PremToAmnt") == null )
				this.PremToAmnt = null;
			else
				this.PremToAmnt = rs.getString("PremToAmnt").trim();

			this.Mult = rs.getDouble("Mult");
			this.Prem = rs.getDouble("Prem");
			this.Amnt = rs.getDouble("Amnt");
			if( rs.getString("CalRule") == null )
				this.CalRule = null;
			else
				this.CalRule = rs.getString("CalRule").trim();

			this.FloatRate = rs.getDouble("FloatRate");
			this.GetLimit = rs.getDouble("GetLimit");
			this.GetRate = rs.getDouble("GetRate");
			if( rs.getString("HealthCheckFlag") == null )
				this.HealthCheckFlag = null;
			else
				this.HealthCheckFlag = rs.getString("HealthCheckFlag").trim();

			if( rs.getString("OutPayFlag") == null )
				this.OutPayFlag = null;
			else
				this.OutPayFlag = rs.getString("OutPayFlag").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("SaleChnl") == null )
				this.SaleChnl = null;
			else
				this.SaleChnl = rs.getString("SaleChnl").trim();

			if( rs.getString("AgentCode") == null )
				this.AgentCode = null;
			else
				this.AgentCode = rs.getString("AgentCode").trim();

			if( rs.getString("AgentName") == null )
				this.AgentName = null;
			else
				this.AgentName = rs.getString("AgentName").trim();

			if( rs.getString("AgentCom") == null )
				this.AgentCom = null;
			else
				this.AgentCom = rs.getString("AgentCom").trim();

			if( rs.getString("BankWorkSite") == null )
				this.BankWorkSite = null;
			else
				this.BankWorkSite = rs.getString("BankWorkSite").trim();

			if( rs.getString("StandbyFlag1") == null )
				this.StandbyFlag1 = null;
			else
				this.StandbyFlag1 = rs.getString("StandbyFlag1").trim();

			if( rs.getString("StandbyFlag2") == null )
				this.StandbyFlag2 = null;
			else
				this.StandbyFlag2 = rs.getString("StandbyFlag2").trim();

			if( rs.getString("StandbyFlag3") == null )
				this.StandbyFlag3 = null;
			else
				this.StandbyFlag3 = rs.getString("StandbyFlag3").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的BPOLBPol表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "BPOLBPolSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public BPOLBPolSchema getSchema()
	{
		BPOLBPolSchema aBPOLBPolSchema = new BPOLBPolSchema();
		aBPOLBPolSchema.setSchema(this);
		return aBPOLBPolSchema;
	}

	public BPOLBPolDB getDB()
	{
		BPOLBPolDB aDBOper = new BPOLBPolDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpBPOLBPol描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append( ChgData.chgData(DealCount));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(BPOBatchNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(PolID));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(ContID));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(PrtNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(FamilyType)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MainPolID)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(InsuredID));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(RelationToAppnt)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(RelationToMainInsured)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(AppntID));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(RelaId)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( PolApplyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( CValiDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(FirstTrialOperator)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(FirstTrialTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ReceiveOperator)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( ReceiveDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ReceiveTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(TempFeeNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(PayMode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(LiveGetMode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(DeadGetMode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(PayIntv));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(InsuYear));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(InsuYearFlag)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(PayEndYear));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(PayEndYearFlag)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(GetYear));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(GetYearFlag)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(GetStartType)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(GetDutyKind)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(BonusGetMode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(PremToAmnt)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(Mult));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(Prem));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(Amnt));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(CalRule)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(FloatRate));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(GetLimit));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(GetRate));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(HealthCheckFlag)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(OutPayFlag)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(SaleChnl)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(AgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(AgentName)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(AgentCom)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(BankWorkSite)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(StandbyFlag1)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(StandbyFlag2)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(StandbyFlag3)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpBPOLBPol>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			DealCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,1,SysConst.PACKAGESPILTER))).intValue();
			BPOBatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			PolID= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,3,SysConst.PACKAGESPILTER))).intValue();
			ContID= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,4,SysConst.PACKAGESPILTER))).intValue();
			PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			FamilyType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			MainPolID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			InsuredID= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,9,SysConst.PACKAGESPILTER))).intValue();
			RelationToAppnt = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			RelationToMainInsured = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			AppntID= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,12,SysConst.PACKAGESPILTER))).intValue();
			RelaId = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			PolApplyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,SysConst.PACKAGESPILTER));
			CValiDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,SysConst.PACKAGESPILTER));
			FirstTrialOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			FirstTrialTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			ReceiveOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			ReceiveDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,SysConst.PACKAGESPILTER));
			ReceiveTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			TempFeeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			PayMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			LiveGetMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			DeadGetMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			PayIntv= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,25,SysConst.PACKAGESPILTER))).intValue();
			InsuYear= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,26,SysConst.PACKAGESPILTER))).intValue();
			InsuYearFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			PayEndYear= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,28,SysConst.PACKAGESPILTER))).intValue();
			PayEndYearFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			GetYear= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,30,SysConst.PACKAGESPILTER))).intValue();
			GetYearFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			GetStartType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
			GetDutyKind = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			BonusGetMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			PremToAmnt = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			Mult = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,36,SysConst.PACKAGESPILTER))).doubleValue();
			Prem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,37,SysConst.PACKAGESPILTER))).doubleValue();
			Amnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,38,SysConst.PACKAGESPILTER))).doubleValue();
			CalRule = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
			FloatRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,40,SysConst.PACKAGESPILTER))).doubleValue();
			GetLimit = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,41,SysConst.PACKAGESPILTER))).doubleValue();
			GetRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,42,SysConst.PACKAGESPILTER))).doubleValue();
			HealthCheckFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43, SysConst.PACKAGESPILTER );
			OutPayFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 45, SysConst.PACKAGESPILTER );
			SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 46, SysConst.PACKAGESPILTER );
			AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 47, SysConst.PACKAGESPILTER );
			AgentName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 48, SysConst.PACKAGESPILTER );
			AgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 49, SysConst.PACKAGESPILTER );
			BankWorkSite = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 50, SysConst.PACKAGESPILTER );
			StandbyFlag1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 51, SysConst.PACKAGESPILTER );
			StandbyFlag2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 52, SysConst.PACKAGESPILTER );
			StandbyFlag3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 53, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 54, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 55,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 56, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 57,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 58, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "BPOLBPolSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("DealCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DealCount));
		}
		if (FCode.equals("BPOBatchNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BPOBatchNo));
		}
		if (FCode.equals("PolID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolID));
		}
		if (FCode.equals("ContID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContID));
		}
		if (FCode.equals("PrtNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
		}
		if (FCode.equals("RiskCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
		}
		if (FCode.equals("FamilyType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FamilyType));
		}
		if (FCode.equals("MainPolID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MainPolID));
		}
		if (FCode.equals("InsuredID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredID));
		}
		if (FCode.equals("RelationToAppnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RelationToAppnt));
		}
		if (FCode.equals("RelationToMainInsured"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RelationToMainInsured));
		}
		if (FCode.equals("AppntID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppntID));
		}
		if (FCode.equals("RelaId"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RelaId));
		}
		if (FCode.equals("PolApplyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getPolApplyDate()));
		}
		if (FCode.equals("CValiDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCValiDate()));
		}
		if (FCode.equals("FirstTrialOperator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FirstTrialOperator));
		}
		if (FCode.equals("FirstTrialTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FirstTrialTime));
		}
		if (FCode.equals("ReceiveOperator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiveOperator));
		}
		if (FCode.equals("ReceiveDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getReceiveDate()));
		}
		if (FCode.equals("ReceiveTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiveTime));
		}
		if (FCode.equals("TempFeeNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TempFeeNo));
		}
		if (FCode.equals("PayMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayMode));
		}
		if (FCode.equals("LiveGetMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LiveGetMode));
		}
		if (FCode.equals("DeadGetMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DeadGetMode));
		}
		if (FCode.equals("PayIntv"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayIntv));
		}
		if (FCode.equals("InsuYear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuYear));
		}
		if (FCode.equals("InsuYearFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuYearFlag));
		}
		if (FCode.equals("PayEndYear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayEndYear));
		}
		if (FCode.equals("PayEndYearFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayEndYearFlag));
		}
		if (FCode.equals("GetYear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GetYear));
		}
		if (FCode.equals("GetYearFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GetYearFlag));
		}
		if (FCode.equals("GetStartType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GetStartType));
		}
		if (FCode.equals("GetDutyKind"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GetDutyKind));
		}
		if (FCode.equals("BonusGetMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BonusGetMode));
		}
		if (FCode.equals("PremToAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PremToAmnt));
		}
		if (FCode.equals("Mult"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Mult));
		}
		if (FCode.equals("Prem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Prem));
		}
		if (FCode.equals("Amnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Amnt));
		}
		if (FCode.equals("CalRule"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CalRule));
		}
		if (FCode.equals("FloatRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FloatRate));
		}
		if (FCode.equals("GetLimit"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GetLimit));
		}
		if (FCode.equals("GetRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GetRate));
		}
		if (FCode.equals("HealthCheckFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HealthCheckFlag));
		}
		if (FCode.equals("OutPayFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OutPayFlag));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("SaleChnl"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
		}
		if (FCode.equals("AgentCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
		}
		if (FCode.equals("AgentName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentName));
		}
		if (FCode.equals("AgentCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
		}
		if (FCode.equals("BankWorkSite"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankWorkSite));
		}
		if (FCode.equals("StandbyFlag1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag1));
		}
		if (FCode.equals("StandbyFlag2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag2));
		}
		if (FCode.equals("StandbyFlag3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag3));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = String.valueOf(DealCount);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(BPOBatchNo);
				break;
			case 2:
				strFieldValue = String.valueOf(PolID);
				break;
			case 3:
				strFieldValue = String.valueOf(ContID);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(PrtNo);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(RiskCode);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(FamilyType);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(MainPolID);
				break;
			case 8:
				strFieldValue = String.valueOf(InsuredID);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(RelationToAppnt);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(RelationToMainInsured);
				break;
			case 11:
				strFieldValue = String.valueOf(AppntID);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(RelaId);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getPolApplyDate()));
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCValiDate()));
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(FirstTrialOperator);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(FirstTrialTime);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(ReceiveOperator);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getReceiveDate()));
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(ReceiveTime);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(TempFeeNo);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(PayMode);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(LiveGetMode);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(DeadGetMode);
				break;
			case 24:
				strFieldValue = String.valueOf(PayIntv);
				break;
			case 25:
				strFieldValue = String.valueOf(InsuYear);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(InsuYearFlag);
				break;
			case 27:
				strFieldValue = String.valueOf(PayEndYear);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(PayEndYearFlag);
				break;
			case 29:
				strFieldValue = String.valueOf(GetYear);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(GetYearFlag);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(GetStartType);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(GetDutyKind);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(BonusGetMode);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(PremToAmnt);
				break;
			case 35:
				strFieldValue = String.valueOf(Mult);
				break;
			case 36:
				strFieldValue = String.valueOf(Prem);
				break;
			case 37:
				strFieldValue = String.valueOf(Amnt);
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(CalRule);
				break;
			case 39:
				strFieldValue = String.valueOf(FloatRate);
				break;
			case 40:
				strFieldValue = String.valueOf(GetLimit);
				break;
			case 41:
				strFieldValue = String.valueOf(GetRate);
				break;
			case 42:
				strFieldValue = StrTool.GBKToUnicode(HealthCheckFlag);
				break;
			case 43:
				strFieldValue = StrTool.GBKToUnicode(OutPayFlag);
				break;
			case 44:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 45:
				strFieldValue = StrTool.GBKToUnicode(SaleChnl);
				break;
			case 46:
				strFieldValue = StrTool.GBKToUnicode(AgentCode);
				break;
			case 47:
				strFieldValue = StrTool.GBKToUnicode(AgentName);
				break;
			case 48:
				strFieldValue = StrTool.GBKToUnicode(AgentCom);
				break;
			case 49:
				strFieldValue = StrTool.GBKToUnicode(BankWorkSite);
				break;
			case 50:
				strFieldValue = StrTool.GBKToUnicode(StandbyFlag1);
				break;
			case 51:
				strFieldValue = StrTool.GBKToUnicode(StandbyFlag2);
				break;
			case 52:
				strFieldValue = StrTool.GBKToUnicode(StandbyFlag3);
				break;
			case 53:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 54:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 55:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 56:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 57:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("DealCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				DealCount = i;
			}
		}
		if (FCode.equalsIgnoreCase("BPOBatchNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BPOBatchNo = FValue.trim();
			}
			else
				BPOBatchNo = null;
		}
		if (FCode.equalsIgnoreCase("PolID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PolID = i;
			}
		}
		if (FCode.equalsIgnoreCase("ContID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				ContID = i;
			}
		}
		if (FCode.equalsIgnoreCase("PrtNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PrtNo = FValue.trim();
			}
			else
				PrtNo = null;
		}
		if (FCode.equalsIgnoreCase("RiskCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskCode = FValue.trim();
			}
			else
				RiskCode = null;
		}
		if (FCode.equalsIgnoreCase("FamilyType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FamilyType = FValue.trim();
			}
			else
				FamilyType = null;
		}
		if (FCode.equalsIgnoreCase("MainPolID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MainPolID = FValue.trim();
			}
			else
				MainPolID = null;
		}
		if (FCode.equalsIgnoreCase("InsuredID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				InsuredID = i;
			}
		}
		if (FCode.equalsIgnoreCase("RelationToAppnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RelationToAppnt = FValue.trim();
			}
			else
				RelationToAppnt = null;
		}
		if (FCode.equalsIgnoreCase("RelationToMainInsured"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RelationToMainInsured = FValue.trim();
			}
			else
				RelationToMainInsured = null;
		}
		if (FCode.equalsIgnoreCase("AppntID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				AppntID = i;
			}
		}
		if (FCode.equalsIgnoreCase("RelaId"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RelaId = FValue.trim();
			}
			else
				RelaId = null;
		}
		if (FCode.equalsIgnoreCase("PolApplyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				PolApplyDate = fDate.getDate( FValue );
			}
			else
				PolApplyDate = null;
		}
		if (FCode.equalsIgnoreCase("CValiDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CValiDate = fDate.getDate( FValue );
			}
			else
				CValiDate = null;
		}
		if (FCode.equalsIgnoreCase("FirstTrialOperator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FirstTrialOperator = FValue.trim();
			}
			else
				FirstTrialOperator = null;
		}
		if (FCode.equalsIgnoreCase("FirstTrialTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FirstTrialTime = FValue.trim();
			}
			else
				FirstTrialTime = null;
		}
		if (FCode.equalsIgnoreCase("ReceiveOperator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReceiveOperator = FValue.trim();
			}
			else
				ReceiveOperator = null;
		}
		if (FCode.equalsIgnoreCase("ReceiveDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ReceiveDate = fDate.getDate( FValue );
			}
			else
				ReceiveDate = null;
		}
		if (FCode.equalsIgnoreCase("ReceiveTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReceiveTime = FValue.trim();
			}
			else
				ReceiveTime = null;
		}
		if (FCode.equalsIgnoreCase("TempFeeNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TempFeeNo = FValue.trim();
			}
			else
				TempFeeNo = null;
		}
		if (FCode.equalsIgnoreCase("PayMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayMode = FValue.trim();
			}
			else
				PayMode = null;
		}
		if (FCode.equalsIgnoreCase("LiveGetMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LiveGetMode = FValue.trim();
			}
			else
				LiveGetMode = null;
		}
		if (FCode.equalsIgnoreCase("DeadGetMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DeadGetMode = FValue.trim();
			}
			else
				DeadGetMode = null;
		}
		if (FCode.equalsIgnoreCase("PayIntv"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PayIntv = i;
			}
		}
		if (FCode.equalsIgnoreCase("InsuYear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				InsuYear = i;
			}
		}
		if (FCode.equalsIgnoreCase("InsuYearFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuYearFlag = FValue.trim();
			}
			else
				InsuYearFlag = null;
		}
		if (FCode.equalsIgnoreCase("PayEndYear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PayEndYear = i;
			}
		}
		if (FCode.equalsIgnoreCase("PayEndYearFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayEndYearFlag = FValue.trim();
			}
			else
				PayEndYearFlag = null;
		}
		if (FCode.equalsIgnoreCase("GetYear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				GetYear = i;
			}
		}
		if (FCode.equalsIgnoreCase("GetYearFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GetYearFlag = FValue.trim();
			}
			else
				GetYearFlag = null;
		}
		if (FCode.equalsIgnoreCase("GetStartType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GetStartType = FValue.trim();
			}
			else
				GetStartType = null;
		}
		if (FCode.equalsIgnoreCase("GetDutyKind"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GetDutyKind = FValue.trim();
			}
			else
				GetDutyKind = null;
		}
		if (FCode.equalsIgnoreCase("BonusGetMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BonusGetMode = FValue.trim();
			}
			else
				BonusGetMode = null;
		}
		if (FCode.equalsIgnoreCase("PremToAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PremToAmnt = FValue.trim();
			}
			else
				PremToAmnt = null;
		}
		if (FCode.equalsIgnoreCase("Mult"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Mult = d;
			}
		}
		if (FCode.equalsIgnoreCase("Prem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Prem = d;
			}
		}
		if (FCode.equalsIgnoreCase("Amnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Amnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("CalRule"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CalRule = FValue.trim();
			}
			else
				CalRule = null;
		}
		if (FCode.equalsIgnoreCase("FloatRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				FloatRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("GetLimit"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				GetLimit = d;
			}
		}
		if (FCode.equalsIgnoreCase("GetRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				GetRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("HealthCheckFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HealthCheckFlag = FValue.trim();
			}
			else
				HealthCheckFlag = null;
		}
		if (FCode.equalsIgnoreCase("OutPayFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OutPayFlag = FValue.trim();
			}
			else
				OutPayFlag = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("SaleChnl"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SaleChnl = FValue.trim();
			}
			else
				SaleChnl = null;
		}
		if (FCode.equalsIgnoreCase("AgentCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCode = FValue.trim();
			}
			else
				AgentCode = null;
		}
		if (FCode.equalsIgnoreCase("AgentName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentName = FValue.trim();
			}
			else
				AgentName = null;
		}
		if (FCode.equalsIgnoreCase("AgentCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCom = FValue.trim();
			}
			else
				AgentCom = null;
		}
		if (FCode.equalsIgnoreCase("BankWorkSite"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankWorkSite = FValue.trim();
			}
			else
				BankWorkSite = null;
		}
		if (FCode.equalsIgnoreCase("StandbyFlag1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandbyFlag1 = FValue.trim();
			}
			else
				StandbyFlag1 = null;
		}
		if (FCode.equalsIgnoreCase("StandbyFlag2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandbyFlag2 = FValue.trim();
			}
			else
				StandbyFlag2 = null;
		}
		if (FCode.equalsIgnoreCase("StandbyFlag3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandbyFlag3 = FValue.trim();
			}
			else
				StandbyFlag3 = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		BPOLBPolSchema other = (BPOLBPolSchema)otherObject;
		return
			DealCount == other.getDealCount()
			&& BPOBatchNo.equals(other.getBPOBatchNo())
			&& PolID == other.getPolID()
			&& ContID == other.getContID()
			&& PrtNo.equals(other.getPrtNo())
			&& RiskCode.equals(other.getRiskCode())
			&& FamilyType.equals(other.getFamilyType())
			&& MainPolID.equals(other.getMainPolID())
			&& InsuredID == other.getInsuredID()
			&& RelationToAppnt.equals(other.getRelationToAppnt())
			&& RelationToMainInsured.equals(other.getRelationToMainInsured())
			&& AppntID == other.getAppntID()
			&& RelaId.equals(other.getRelaId())
			&& fDate.getString(PolApplyDate).equals(other.getPolApplyDate())
			&& fDate.getString(CValiDate).equals(other.getCValiDate())
			&& FirstTrialOperator.equals(other.getFirstTrialOperator())
			&& FirstTrialTime.equals(other.getFirstTrialTime())
			&& ReceiveOperator.equals(other.getReceiveOperator())
			&& fDate.getString(ReceiveDate).equals(other.getReceiveDate())
			&& ReceiveTime.equals(other.getReceiveTime())
			&& TempFeeNo.equals(other.getTempFeeNo())
			&& PayMode.equals(other.getPayMode())
			&& LiveGetMode.equals(other.getLiveGetMode())
			&& DeadGetMode.equals(other.getDeadGetMode())
			&& PayIntv == other.getPayIntv()
			&& InsuYear == other.getInsuYear()
			&& InsuYearFlag.equals(other.getInsuYearFlag())
			&& PayEndYear == other.getPayEndYear()
			&& PayEndYearFlag.equals(other.getPayEndYearFlag())
			&& GetYear == other.getGetYear()
			&& GetYearFlag.equals(other.getGetYearFlag())
			&& GetStartType.equals(other.getGetStartType())
			&& GetDutyKind.equals(other.getGetDutyKind())
			&& BonusGetMode.equals(other.getBonusGetMode())
			&& PremToAmnt.equals(other.getPremToAmnt())
			&& Mult == other.getMult()
			&& Prem == other.getPrem()
			&& Amnt == other.getAmnt()
			&& CalRule.equals(other.getCalRule())
			&& FloatRate == other.getFloatRate()
			&& GetLimit == other.getGetLimit()
			&& GetRate == other.getGetRate()
			&& HealthCheckFlag.equals(other.getHealthCheckFlag())
			&& OutPayFlag.equals(other.getOutPayFlag())
			&& ManageCom.equals(other.getManageCom())
			&& SaleChnl.equals(other.getSaleChnl())
			&& AgentCode.equals(other.getAgentCode())
			&& AgentName.equals(other.getAgentName())
			&& AgentCom.equals(other.getAgentCom())
			&& BankWorkSite.equals(other.getBankWorkSite())
			&& StandbyFlag1.equals(other.getStandbyFlag1())
			&& StandbyFlag2.equals(other.getStandbyFlag2())
			&& StandbyFlag3.equals(other.getStandbyFlag3())
			&& Operator.equals(other.getOperator())
			&& fDate.getString(MakeDate).equals(other.getMakeDate())
			&& MakeTime.equals(other.getMakeTime())
			&& fDate.getString(ModifyDate).equals(other.getModifyDate())
			&& ModifyTime.equals(other.getModifyTime());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("DealCount") ) {
			return 0;
		}
		if( strFieldName.equals("BPOBatchNo") ) {
			return 1;
		}
		if( strFieldName.equals("PolID") ) {
			return 2;
		}
		if( strFieldName.equals("ContID") ) {
			return 3;
		}
		if( strFieldName.equals("PrtNo") ) {
			return 4;
		}
		if( strFieldName.equals("RiskCode") ) {
			return 5;
		}
		if( strFieldName.equals("FamilyType") ) {
			return 6;
		}
		if( strFieldName.equals("MainPolID") ) {
			return 7;
		}
		if( strFieldName.equals("InsuredID") ) {
			return 8;
		}
		if( strFieldName.equals("RelationToAppnt") ) {
			return 9;
		}
		if( strFieldName.equals("RelationToMainInsured") ) {
			return 10;
		}
		if( strFieldName.equals("AppntID") ) {
			return 11;
		}
		if( strFieldName.equals("RelaId") ) {
			return 12;
		}
		if( strFieldName.equals("PolApplyDate") ) {
			return 13;
		}
		if( strFieldName.equals("CValiDate") ) {
			return 14;
		}
		if( strFieldName.equals("FirstTrialOperator") ) {
			return 15;
		}
		if( strFieldName.equals("FirstTrialTime") ) {
			return 16;
		}
		if( strFieldName.equals("ReceiveOperator") ) {
			return 17;
		}
		if( strFieldName.equals("ReceiveDate") ) {
			return 18;
		}
		if( strFieldName.equals("ReceiveTime") ) {
			return 19;
		}
		if( strFieldName.equals("TempFeeNo") ) {
			return 20;
		}
		if( strFieldName.equals("PayMode") ) {
			return 21;
		}
		if( strFieldName.equals("LiveGetMode") ) {
			return 22;
		}
		if( strFieldName.equals("DeadGetMode") ) {
			return 23;
		}
		if( strFieldName.equals("PayIntv") ) {
			return 24;
		}
		if( strFieldName.equals("InsuYear") ) {
			return 25;
		}
		if( strFieldName.equals("InsuYearFlag") ) {
			return 26;
		}
		if( strFieldName.equals("PayEndYear") ) {
			return 27;
		}
		if( strFieldName.equals("PayEndYearFlag") ) {
			return 28;
		}
		if( strFieldName.equals("GetYear") ) {
			return 29;
		}
		if( strFieldName.equals("GetYearFlag") ) {
			return 30;
		}
		if( strFieldName.equals("GetStartType") ) {
			return 31;
		}
		if( strFieldName.equals("GetDutyKind") ) {
			return 32;
		}
		if( strFieldName.equals("BonusGetMode") ) {
			return 33;
		}
		if( strFieldName.equals("PremToAmnt") ) {
			return 34;
		}
		if( strFieldName.equals("Mult") ) {
			return 35;
		}
		if( strFieldName.equals("Prem") ) {
			return 36;
		}
		if( strFieldName.equals("Amnt") ) {
			return 37;
		}
		if( strFieldName.equals("CalRule") ) {
			return 38;
		}
		if( strFieldName.equals("FloatRate") ) {
			return 39;
		}
		if( strFieldName.equals("GetLimit") ) {
			return 40;
		}
		if( strFieldName.equals("GetRate") ) {
			return 41;
		}
		if( strFieldName.equals("HealthCheckFlag") ) {
			return 42;
		}
		if( strFieldName.equals("OutPayFlag") ) {
			return 43;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 44;
		}
		if( strFieldName.equals("SaleChnl") ) {
			return 45;
		}
		if( strFieldName.equals("AgentCode") ) {
			return 46;
		}
		if( strFieldName.equals("AgentName") ) {
			return 47;
		}
		if( strFieldName.equals("AgentCom") ) {
			return 48;
		}
		if( strFieldName.equals("BankWorkSite") ) {
			return 49;
		}
		if( strFieldName.equals("StandbyFlag1") ) {
			return 50;
		}
		if( strFieldName.equals("StandbyFlag2") ) {
			return 51;
		}
		if( strFieldName.equals("StandbyFlag3") ) {
			return 52;
		}
		if( strFieldName.equals("Operator") ) {
			return 53;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 54;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 55;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 56;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 57;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "DealCount";
				break;
			case 1:
				strFieldName = "BPOBatchNo";
				break;
			case 2:
				strFieldName = "PolID";
				break;
			case 3:
				strFieldName = "ContID";
				break;
			case 4:
				strFieldName = "PrtNo";
				break;
			case 5:
				strFieldName = "RiskCode";
				break;
			case 6:
				strFieldName = "FamilyType";
				break;
			case 7:
				strFieldName = "MainPolID";
				break;
			case 8:
				strFieldName = "InsuredID";
				break;
			case 9:
				strFieldName = "RelationToAppnt";
				break;
			case 10:
				strFieldName = "RelationToMainInsured";
				break;
			case 11:
				strFieldName = "AppntID";
				break;
			case 12:
				strFieldName = "RelaId";
				break;
			case 13:
				strFieldName = "PolApplyDate";
				break;
			case 14:
				strFieldName = "CValiDate";
				break;
			case 15:
				strFieldName = "FirstTrialOperator";
				break;
			case 16:
				strFieldName = "FirstTrialTime";
				break;
			case 17:
				strFieldName = "ReceiveOperator";
				break;
			case 18:
				strFieldName = "ReceiveDate";
				break;
			case 19:
				strFieldName = "ReceiveTime";
				break;
			case 20:
				strFieldName = "TempFeeNo";
				break;
			case 21:
				strFieldName = "PayMode";
				break;
			case 22:
				strFieldName = "LiveGetMode";
				break;
			case 23:
				strFieldName = "DeadGetMode";
				break;
			case 24:
				strFieldName = "PayIntv";
				break;
			case 25:
				strFieldName = "InsuYear";
				break;
			case 26:
				strFieldName = "InsuYearFlag";
				break;
			case 27:
				strFieldName = "PayEndYear";
				break;
			case 28:
				strFieldName = "PayEndYearFlag";
				break;
			case 29:
				strFieldName = "GetYear";
				break;
			case 30:
				strFieldName = "GetYearFlag";
				break;
			case 31:
				strFieldName = "GetStartType";
				break;
			case 32:
				strFieldName = "GetDutyKind";
				break;
			case 33:
				strFieldName = "BonusGetMode";
				break;
			case 34:
				strFieldName = "PremToAmnt";
				break;
			case 35:
				strFieldName = "Mult";
				break;
			case 36:
				strFieldName = "Prem";
				break;
			case 37:
				strFieldName = "Amnt";
				break;
			case 38:
				strFieldName = "CalRule";
				break;
			case 39:
				strFieldName = "FloatRate";
				break;
			case 40:
				strFieldName = "GetLimit";
				break;
			case 41:
				strFieldName = "GetRate";
				break;
			case 42:
				strFieldName = "HealthCheckFlag";
				break;
			case 43:
				strFieldName = "OutPayFlag";
				break;
			case 44:
				strFieldName = "ManageCom";
				break;
			case 45:
				strFieldName = "SaleChnl";
				break;
			case 46:
				strFieldName = "AgentCode";
				break;
			case 47:
				strFieldName = "AgentName";
				break;
			case 48:
				strFieldName = "AgentCom";
				break;
			case 49:
				strFieldName = "BankWorkSite";
				break;
			case 50:
				strFieldName = "StandbyFlag1";
				break;
			case 51:
				strFieldName = "StandbyFlag2";
				break;
			case 52:
				strFieldName = "StandbyFlag3";
				break;
			case 53:
				strFieldName = "Operator";
				break;
			case 54:
				strFieldName = "MakeDate";
				break;
			case 55:
				strFieldName = "MakeTime";
				break;
			case 56:
				strFieldName = "ModifyDate";
				break;
			case 57:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("DealCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("BPOBatchNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolID") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("ContID") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("PrtNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FamilyType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MainPolID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuredID") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("RelationToAppnt") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RelationToMainInsured") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppntID") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("RelaId") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolApplyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("CValiDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("FirstTrialOperator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FirstTrialTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReceiveOperator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReceiveDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ReceiveTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TempFeeNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayMode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LiveGetMode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DeadGetMode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayIntv") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("InsuYear") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("InsuYearFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayEndYear") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("PayEndYearFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GetYear") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("GetYearFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GetStartType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GetDutyKind") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BonusGetMode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PremToAmnt") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Mult") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Prem") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Amnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("CalRule") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FloatRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("GetLimit") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("GetRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("HealthCheckFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OutPayFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SaleChnl") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankWorkSite") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandbyFlag1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandbyFlag2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandbyFlag3") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_INT;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_INT;
				break;
			case 3:
				nFieldType = Schema.TYPE_INT;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_INT;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_INT;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 14:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_INT;
				break;
			case 25:
				nFieldType = Schema.TYPE_INT;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_INT;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_INT;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 36:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 37:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 38:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 39:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 40:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 41:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 42:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 43:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 44:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 45:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 46:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 47:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 48:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 49:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 50:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 51:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 52:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 53:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 54:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 55:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 56:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 57:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
