/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LHGrpServPlanDB;

/*
 * <p>ClassName: LHGrpServPlanSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PICCH-刘莹-表修改-团体服务计划_20060706
 * @CreateDate：2006-07-07
 */
public class LHGrpServPlanSchema implements Schema, Cloneable {
    // @Field
    /** 团体服务计划号码 */
    private String GrpServPlanNo;
    /** 团单号 */
    private String GrpContNo;
    /** 团体客户号 */
    private String GrpCustomerNo;
    /** 团体客户名称 */
    private String GrpName;
    /** 服务计划代码 */
    private String ServPlanCode;
    /** 服务计划名称 */
    private String ServPlanName;
    /** 服务计划档次 */
    private String ServPlanLevel;
    /** 服务计划类型 */
    private String ServPlayType;
    /** 机构属性标识 */
    private String ComID;
    /** 服务计划起始时间 */
    private Date StartDate;
    /** 服务计划终止时间 */
    private Date EndDate;
    /** 健管保费（元） */
    private double ServPrem;
    /** 管理机构 */
    private String ManageCom;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 约定客户人数 */
    private String CustomNumInDate;
    /** 实际客户人数 */
    private String CustomNumActrually;
    /** 档次说明 */
    private String LevelDescription;

    public static final int FIELDNUM = 21; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LHGrpServPlanSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "GrpServPlanNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LHGrpServPlanSchema cloned = (LHGrpServPlanSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getGrpServPlanNo() {
        return GrpServPlanNo;
    }

    public void setGrpServPlanNo(String aGrpServPlanNo) {
        GrpServPlanNo = aGrpServPlanNo;
    }

    public String getGrpContNo() {
        return GrpContNo;
    }

    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }

    public String getGrpCustomerNo() {
        return GrpCustomerNo;
    }

    public void setGrpCustomerNo(String aGrpCustomerNo) {
        GrpCustomerNo = aGrpCustomerNo;
    }

    public String getGrpName() {
        return GrpName;
    }

    public void setGrpName(String aGrpName) {
        GrpName = aGrpName;
    }

    public String getServPlanCode() {
        return ServPlanCode;
    }

    public void setServPlanCode(String aServPlanCode) {
        ServPlanCode = aServPlanCode;
    }

    public String getServPlanName() {
        return ServPlanName;
    }

    public void setServPlanName(String aServPlanName) {
        ServPlanName = aServPlanName;
    }

    public String getServPlanLevel() {
        return ServPlanLevel;
    }

    public void setServPlanLevel(String aServPlanLevel) {
        ServPlanLevel = aServPlanLevel;
    }

    public String getServPlayType() {
        return ServPlayType;
    }

    public void setServPlayType(String aServPlayType) {
        ServPlayType = aServPlayType;
    }

    public String getComID() {
        return ComID;
    }

    public void setComID(String aComID) {
        ComID = aComID;
    }

    public String getStartDate() {
        if (StartDate != null) {
            return fDate.getString(StartDate);
        } else {
            return null;
        }
    }

    public void setStartDate(Date aStartDate) {
        StartDate = aStartDate;
    }

    public void setStartDate(String aStartDate) {
        if (aStartDate != null && !aStartDate.equals("")) {
            StartDate = fDate.getDate(aStartDate);
        } else {
            StartDate = null;
        }
    }

    public String getEndDate() {
        if (EndDate != null) {
            return fDate.getString(EndDate);
        } else {
            return null;
        }
    }

    public void setEndDate(Date aEndDate) {
        EndDate = aEndDate;
    }

    public void setEndDate(String aEndDate) {
        if (aEndDate != null && !aEndDate.equals("")) {
            EndDate = fDate.getDate(aEndDate);
        } else {
            EndDate = null;
        }
    }

    public double getServPrem() {
        return ServPrem;
    }

    public void setServPrem(double aServPrem) {
        ServPrem = Arith.round(aServPrem, 2);
    }

    public void setServPrem(String aServPrem) {
        if (aServPrem != null && !aServPrem.equals("")) {
            Double tDouble = new Double(aServPrem);
            double d = tDouble.doubleValue();
            ServPrem = Arith.round(d, 2);
        }
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    public String getCustomNumInDate() {
        return CustomNumInDate;
    }

    public void setCustomNumInDate(String aCustomNumInDate) {
        CustomNumInDate = aCustomNumInDate;
    }

    public String getCustomNumActrually() {
        return CustomNumActrually;
    }

    public void setCustomNumActrually(String aCustomNumActrually) {
        CustomNumActrually = aCustomNumActrually;
    }

    public String getLevelDescription() {
        return LevelDescription;
    }

    public void setLevelDescription(String aLevelDescription) {
        LevelDescription = aLevelDescription;
    }

    /**
     * 使用另外一个 LHGrpServPlanSchema 对象给 Schema 赋值
     * @param: aLHGrpServPlanSchema LHGrpServPlanSchema
     **/
    public void setSchema(LHGrpServPlanSchema aLHGrpServPlanSchema) {
        this.GrpServPlanNo = aLHGrpServPlanSchema.getGrpServPlanNo();
        this.GrpContNo = aLHGrpServPlanSchema.getGrpContNo();
        this.GrpCustomerNo = aLHGrpServPlanSchema.getGrpCustomerNo();
        this.GrpName = aLHGrpServPlanSchema.getGrpName();
        this.ServPlanCode = aLHGrpServPlanSchema.getServPlanCode();
        this.ServPlanName = aLHGrpServPlanSchema.getServPlanName();
        this.ServPlanLevel = aLHGrpServPlanSchema.getServPlanLevel();
        this.ServPlayType = aLHGrpServPlanSchema.getServPlayType();
        this.ComID = aLHGrpServPlanSchema.getComID();
        this.StartDate = fDate.getDate(aLHGrpServPlanSchema.getStartDate());
        this.EndDate = fDate.getDate(aLHGrpServPlanSchema.getEndDate());
        this.ServPrem = aLHGrpServPlanSchema.getServPrem();
        this.ManageCom = aLHGrpServPlanSchema.getManageCom();
        this.Operator = aLHGrpServPlanSchema.getOperator();
        this.MakeDate = fDate.getDate(aLHGrpServPlanSchema.getMakeDate());
        this.MakeTime = aLHGrpServPlanSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLHGrpServPlanSchema.getModifyDate());
        this.ModifyTime = aLHGrpServPlanSchema.getModifyTime();
        this.CustomNumInDate = aLHGrpServPlanSchema.getCustomNumInDate();
        this.CustomNumActrually = aLHGrpServPlanSchema.getCustomNumActrually();
        this.LevelDescription = aLHGrpServPlanSchema.getLevelDescription();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("GrpServPlanNo") == null) {
                this.GrpServPlanNo = null;
            } else {
                this.GrpServPlanNo = rs.getString("GrpServPlanNo").trim();
            }

            if (rs.getString("GrpContNo") == null) {
                this.GrpContNo = null;
            } else {
                this.GrpContNo = rs.getString("GrpContNo").trim();
            }

            if (rs.getString("GrpCustomerNo") == null) {
                this.GrpCustomerNo = null;
            } else {
                this.GrpCustomerNo = rs.getString("GrpCustomerNo").trim();
            }

            if (rs.getString("GrpName") == null) {
                this.GrpName = null;
            } else {
                this.GrpName = rs.getString("GrpName").trim();
            }

            if (rs.getString("ServPlanCode") == null) {
                this.ServPlanCode = null;
            } else {
                this.ServPlanCode = rs.getString("ServPlanCode").trim();
            }

            if (rs.getString("ServPlanName") == null) {
                this.ServPlanName = null;
            } else {
                this.ServPlanName = rs.getString("ServPlanName").trim();
            }

            if (rs.getString("ServPlanLevel") == null) {
                this.ServPlanLevel = null;
            } else {
                this.ServPlanLevel = rs.getString("ServPlanLevel").trim();
            }

            if (rs.getString("ServPlayType") == null) {
                this.ServPlayType = null;
            } else {
                this.ServPlayType = rs.getString("ServPlayType").trim();
            }

            if (rs.getString("ComID") == null) {
                this.ComID = null;
            } else {
                this.ComID = rs.getString("ComID").trim();
            }

            this.StartDate = rs.getDate("StartDate");
            this.EndDate = rs.getDate("EndDate");
            this.ServPrem = rs.getDouble("ServPrem");
            if (rs.getString("ManageCom") == null) {
                this.ManageCom = null;
            } else {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("CustomNumInDate") == null) {
                this.CustomNumInDate = null;
            } else {
                this.CustomNumInDate = rs.getString("CustomNumInDate").trim();
            }

            if (rs.getString("CustomNumActrually") == null) {
                this.CustomNumActrually = null;
            } else {
                this.CustomNumActrually = rs.getString("CustomNumActrually").
                                          trim();
            }

            if (rs.getString("LevelDescription") == null) {
                this.LevelDescription = null;
            } else {
                this.LevelDescription = rs.getString("LevelDescription").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LHGrpServPlan表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHGrpServPlanSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LHGrpServPlanSchema getSchema() {
        LHGrpServPlanSchema aLHGrpServPlanSchema = new LHGrpServPlanSchema();
        aLHGrpServPlanSchema.setSchema(this);
        return aLHGrpServPlanSchema;
    }

    public LHGrpServPlanDB getDB() {
        LHGrpServPlanDB aDBOper = new LHGrpServPlanDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHGrpServPlan描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(GrpServPlanNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpContNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpCustomerNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServPlanCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServPlanName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServPlanLevel));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServPlayType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ComID));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(StartDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(EndDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(ServPrem));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CustomNumInDate));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CustomNumActrually));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(LevelDescription));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHGrpServPlan>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            GrpServPlanNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                           SysConst.PACKAGESPILTER);
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                       SysConst.PACKAGESPILTER);
            GrpCustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                           SysConst.PACKAGESPILTER);
            GrpName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                     SysConst.PACKAGESPILTER);
            ServPlanCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                          SysConst.PACKAGESPILTER);
            ServPlanName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                          SysConst.PACKAGESPILTER);
            ServPlanLevel = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                           SysConst.PACKAGESPILTER);
            ServPlayType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                          SysConst.PACKAGESPILTER);
            ComID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                   SysConst.PACKAGESPILTER);
            StartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 10, SysConst.PACKAGESPILTER));
            EndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 11, SysConst.PACKAGESPILTER));
            ServPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 12, SysConst.PACKAGESPILTER))).doubleValue();
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                       SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 15, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 17, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                        SysConst.PACKAGESPILTER);
            CustomNumInDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             19, SysConst.PACKAGESPILTER);
            CustomNumActrually = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                                20, SysConst.PACKAGESPILTER);
            LevelDescription = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                              21, SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHGrpServPlanSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("GrpServPlanNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpServPlanNo));
        }
        if (FCode.equals("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equals("GrpCustomerNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpCustomerNo));
        }
        if (FCode.equals("GrpName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpName));
        }
        if (FCode.equals("ServPlanCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServPlanCode));
        }
        if (FCode.equals("ServPlanName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServPlanName));
        }
        if (FCode.equals("ServPlanLevel")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServPlanLevel));
        }
        if (FCode.equals("ServPlayType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServPlayType));
        }
        if (FCode.equals("ComID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ComID));
        }
        if (FCode.equals("StartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getStartDate()));
        }
        if (FCode.equals("EndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getEndDate()));
        }
        if (FCode.equals("ServPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServPrem));
        }
        if (FCode.equals("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("CustomNumInDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomNumInDate));
        }
        if (FCode.equals("CustomNumActrually")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomNumActrually));
        }
        if (FCode.equals("LevelDescription")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LevelDescription));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(GrpServPlanNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(GrpContNo);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(GrpCustomerNo);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(GrpName);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(ServPlanCode);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(ServPlanName);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(ServPlanLevel);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(ServPlayType);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(ComID);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getStartDate()));
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.getEndDate()));
            break;
        case 11:
            strFieldValue = String.valueOf(ServPrem);
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(ManageCom);
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 15:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 16:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 17:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        case 18:
            strFieldValue = StrTool.GBKToUnicode(CustomNumInDate);
            break;
        case 19:
            strFieldValue = StrTool.GBKToUnicode(CustomNumActrually);
            break;
        case 20:
            strFieldValue = StrTool.GBKToUnicode(LevelDescription);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("GrpServPlanNo")) {
            if (FValue != null && !FValue.equals("")) {
                GrpServPlanNo = FValue.trim();
            } else {
                GrpServPlanNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if (FValue != null && !FValue.equals("")) {
                GrpContNo = FValue.trim();
            } else {
                GrpContNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("GrpCustomerNo")) {
            if (FValue != null && !FValue.equals("")) {
                GrpCustomerNo = FValue.trim();
            } else {
                GrpCustomerNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("GrpName")) {
            if (FValue != null && !FValue.equals("")) {
                GrpName = FValue.trim();
            } else {
                GrpName = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServPlanCode")) {
            if (FValue != null && !FValue.equals("")) {
                ServPlanCode = FValue.trim();
            } else {
                ServPlanCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServPlanName")) {
            if (FValue != null && !FValue.equals("")) {
                ServPlanName = FValue.trim();
            } else {
                ServPlanName = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServPlanLevel")) {
            if (FValue != null && !FValue.equals("")) {
                ServPlanLevel = FValue.trim();
            } else {
                ServPlanLevel = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServPlayType")) {
            if (FValue != null && !FValue.equals("")) {
                ServPlayType = FValue.trim();
            } else {
                ServPlayType = null;
            }
        }
        if (FCode.equalsIgnoreCase("ComID")) {
            if (FValue != null && !FValue.equals("")) {
                ComID = FValue.trim();
            } else {
                ComID = null;
            }
        }
        if (FCode.equalsIgnoreCase("StartDate")) {
            if (FValue != null && !FValue.equals("")) {
                StartDate = fDate.getDate(FValue);
            } else {
                StartDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            if (FValue != null && !FValue.equals("")) {
                EndDate = fDate.getDate(FValue);
            } else {
                EndDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServPrem")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                ServPrem = d;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ManageCom = FValue.trim();
            } else {
                ManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("CustomNumInDate")) {
            if (FValue != null && !FValue.equals("")) {
                CustomNumInDate = FValue.trim();
            } else {
                CustomNumInDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("CustomNumActrually")) {
            if (FValue != null && !FValue.equals("")) {
                CustomNumActrually = FValue.trim();
            } else {
                CustomNumActrually = null;
            }
        }
        if (FCode.equalsIgnoreCase("LevelDescription")) {
            if (FValue != null && !FValue.equals("")) {
                LevelDescription = FValue.trim();
            } else {
                LevelDescription = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LHGrpServPlanSchema other = (LHGrpServPlanSchema) otherObject;
        return
                GrpServPlanNo.equals(other.getGrpServPlanNo())
                && GrpContNo.equals(other.getGrpContNo())
                && GrpCustomerNo.equals(other.getGrpCustomerNo())
                && GrpName.equals(other.getGrpName())
                && ServPlanCode.equals(other.getServPlanCode())
                && ServPlanName.equals(other.getServPlanName())
                && ServPlanLevel.equals(other.getServPlanLevel())
                && ServPlayType.equals(other.getServPlayType())
                && ComID.equals(other.getComID())
                && fDate.getString(StartDate).equals(other.getStartDate())
                && fDate.getString(EndDate).equals(other.getEndDate())
                && ServPrem == other.getServPrem()
                && ManageCom.equals(other.getManageCom())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && CustomNumInDate.equals(other.getCustomNumInDate())
                && CustomNumActrually.equals(other.getCustomNumActrually())
                && LevelDescription.equals(other.getLevelDescription());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("GrpServPlanNo")) {
            return 0;
        }
        if (strFieldName.equals("GrpContNo")) {
            return 1;
        }
        if (strFieldName.equals("GrpCustomerNo")) {
            return 2;
        }
        if (strFieldName.equals("GrpName")) {
            return 3;
        }
        if (strFieldName.equals("ServPlanCode")) {
            return 4;
        }
        if (strFieldName.equals("ServPlanName")) {
            return 5;
        }
        if (strFieldName.equals("ServPlanLevel")) {
            return 6;
        }
        if (strFieldName.equals("ServPlayType")) {
            return 7;
        }
        if (strFieldName.equals("ComID")) {
            return 8;
        }
        if (strFieldName.equals("StartDate")) {
            return 9;
        }
        if (strFieldName.equals("EndDate")) {
            return 10;
        }
        if (strFieldName.equals("ServPrem")) {
            return 11;
        }
        if (strFieldName.equals("ManageCom")) {
            return 12;
        }
        if (strFieldName.equals("Operator")) {
            return 13;
        }
        if (strFieldName.equals("MakeDate")) {
            return 14;
        }
        if (strFieldName.equals("MakeTime")) {
            return 15;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 16;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 17;
        }
        if (strFieldName.equals("CustomNumInDate")) {
            return 18;
        }
        if (strFieldName.equals("CustomNumActrually")) {
            return 19;
        }
        if (strFieldName.equals("LevelDescription")) {
            return 20;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "GrpServPlanNo";
            break;
        case 1:
            strFieldName = "GrpContNo";
            break;
        case 2:
            strFieldName = "GrpCustomerNo";
            break;
        case 3:
            strFieldName = "GrpName";
            break;
        case 4:
            strFieldName = "ServPlanCode";
            break;
        case 5:
            strFieldName = "ServPlanName";
            break;
        case 6:
            strFieldName = "ServPlanLevel";
            break;
        case 7:
            strFieldName = "ServPlayType";
            break;
        case 8:
            strFieldName = "ComID";
            break;
        case 9:
            strFieldName = "StartDate";
            break;
        case 10:
            strFieldName = "EndDate";
            break;
        case 11:
            strFieldName = "ServPrem";
            break;
        case 12:
            strFieldName = "ManageCom";
            break;
        case 13:
            strFieldName = "Operator";
            break;
        case 14:
            strFieldName = "MakeDate";
            break;
        case 15:
            strFieldName = "MakeTime";
            break;
        case 16:
            strFieldName = "ModifyDate";
            break;
        case 17:
            strFieldName = "ModifyTime";
            break;
        case 18:
            strFieldName = "CustomNumInDate";
            break;
        case 19:
            strFieldName = "CustomNumActrually";
            break;
        case 20:
            strFieldName = "LevelDescription";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("GrpServPlanNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpContNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpCustomerNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServPlanCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServPlanName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServPlanLevel")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServPlayType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ComID")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StartDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("EndDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ServPrem")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("ManageCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CustomNumInDate")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CustomNumActrually")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("LevelDescription")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 10:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 11:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 12:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 13:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 14:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 15:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 16:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 17:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 18:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 19:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 20:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
