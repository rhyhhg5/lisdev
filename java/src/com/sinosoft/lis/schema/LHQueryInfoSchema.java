/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LHQueryInfoDB;

/*
 * <p>ClassName: LHQueryInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PICCH-郭楠-表结构修改-20060223-查询
 * @CreateDate：2006-02-25
 */
public class LHQueryInfoSchema implements Schema, Cloneable
{
	// @Field
	/** 项目代码 */
	private String ItemCode;
	/** 项目名称 */
	private String ItemName;
	/** 项目类型 */
	private String ItemType;
	/** 项目备用代码 */
	private String ItemSndCode;
	/** 项目状态 */
	private String ItemState;
	/** 项目别名 */
	private String ItemOtherName;
	/** 项目说明 */
	private String ItemContent;
	/** 项目关键字 */
	private String ItemKeyWord;
	/** 文件名 */
	private String PageName;
	/** 文件后缀 */
	private String PageSuffix;
	/** 文件存放相对路径ftp */
	private String PagePathFTP;
	/** 点击次数 */
	private String ClickCount;
	/** 管理机构 */
	private String ManageCom;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后修改日期 */
	private Date ModifyDate;
	/** 最后修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 18;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LHQueryInfoSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "ItemCode";

		PK = pk;
	}

            /**
             * Schema克隆
             * @return Object
             * @throws CloneNotSupportedException
             */
            public Object clone()
                    throws CloneNotSupportedException
            {
                LHQueryInfoSchema cloned = (LHQueryInfoSchema)super.clone();
                cloned.fDate = (FDate) fDate.clone();
                cloned.mErrors = (CErrors) mErrors.clone();
                return cloned;
            }

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getItemCode()
	{
		return ItemCode;
	}
	public void setItemCode(String aItemCode)
	{
            ItemCode = aItemCode;
	}
	public String getItemName()
	{
		return ItemName;
	}
	public void setItemName(String aItemName)
	{
            ItemName = aItemName;
	}
	public String getItemType()
	{
		return ItemType;
	}
	public void setItemType(String aItemType)
	{
            ItemType = aItemType;
	}
	public String getItemSndCode()
	{
		return ItemSndCode;
	}
	public void setItemSndCode(String aItemSndCode)
	{
            ItemSndCode = aItemSndCode;
	}
	public String getItemState()
	{
		return ItemState;
	}
	public void setItemState(String aItemState)
	{
            ItemState = aItemState;
	}
	public String getItemOtherName()
	{
		return ItemOtherName;
	}
	public void setItemOtherName(String aItemOtherName)
	{
            ItemOtherName = aItemOtherName;
	}
	public String getItemContent()
	{
		return ItemContent;
	}
	public void setItemContent(String aItemContent)
	{
            ItemContent = aItemContent;
	}
	public String getItemKeyWord()
	{
		return ItemKeyWord;
	}
	public void setItemKeyWord(String aItemKeyWord)
	{
            ItemKeyWord = aItemKeyWord;
	}
	public String getPageName()
	{
		return PageName;
	}
	public void setPageName(String aPageName)
	{
            PageName = aPageName;
	}
	public String getPageSuffix()
	{
		return PageSuffix;
	}
	public void setPageSuffix(String aPageSuffix)
	{
            PageSuffix = aPageSuffix;
	}
	public String getPagePathFTP()
	{
		return PagePathFTP;
	}
	public void setPagePathFTP(String aPagePathFTP)
	{
            PagePathFTP = aPagePathFTP;
	}
	public String getClickCount()
	{
		return ClickCount;
	}
	public void setClickCount(String aClickCount)
	{
            ClickCount = aClickCount;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
            ManageCom = aManageCom;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
            Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
            MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
            MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
            ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
            ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LHQueryInfoSchema 对象给 Schema 赋值
	* @param: aLHQueryInfoSchema LHQueryInfoSchema
	**/
	public void setSchema(LHQueryInfoSchema aLHQueryInfoSchema)
	{
		this.ItemCode = aLHQueryInfoSchema.getItemCode();
		this.ItemName = aLHQueryInfoSchema.getItemName();
		this.ItemType = aLHQueryInfoSchema.getItemType();
		this.ItemSndCode = aLHQueryInfoSchema.getItemSndCode();
		this.ItemState = aLHQueryInfoSchema.getItemState();
		this.ItemOtherName = aLHQueryInfoSchema.getItemOtherName();
		this.ItemContent = aLHQueryInfoSchema.getItemContent();
		this.ItemKeyWord = aLHQueryInfoSchema.getItemKeyWord();
		this.PageName = aLHQueryInfoSchema.getPageName();
		this.PageSuffix = aLHQueryInfoSchema.getPageSuffix();
		this.PagePathFTP = aLHQueryInfoSchema.getPagePathFTP();
		this.ClickCount = aLHQueryInfoSchema.getClickCount();
		this.ManageCom = aLHQueryInfoSchema.getManageCom();
		this.Operator = aLHQueryInfoSchema.getOperator();
		this.MakeDate = fDate.getDate( aLHQueryInfoSchema.getMakeDate());
		this.MakeTime = aLHQueryInfoSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLHQueryInfoSchema.getModifyDate());
		this.ModifyTime = aLHQueryInfoSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("ItemCode") == null )
				this.ItemCode = null;
			else
				this.ItemCode = rs.getString("ItemCode").trim();

			if( rs.getString("ItemName") == null )
				this.ItemName = null;
			else
				this.ItemName = rs.getString("ItemName").trim();

			if( rs.getString("ItemType") == null )
				this.ItemType = null;
			else
				this.ItemType = rs.getString("ItemType").trim();

			if( rs.getString("ItemSndCode") == null )
				this.ItemSndCode = null;
			else
				this.ItemSndCode = rs.getString("ItemSndCode").trim();

			if( rs.getString("ItemState") == null )
				this.ItemState = null;
			else
				this.ItemState = rs.getString("ItemState").trim();

			if( rs.getString("ItemOtherName") == null )
				this.ItemOtherName = null;
			else
				this.ItemOtherName = rs.getString("ItemOtherName").trim();

			if( rs.getString("ItemContent") == null )
				this.ItemContent = null;
			else
				this.ItemContent = rs.getString("ItemContent").trim();

			if( rs.getString("ItemKeyWord") == null )
				this.ItemKeyWord = null;
			else
				this.ItemKeyWord = rs.getString("ItemKeyWord").trim();

			if( rs.getString("PageName") == null )
				this.PageName = null;
			else
				this.PageName = rs.getString("PageName").trim();

			if( rs.getString("PageSuffix") == null )
				this.PageSuffix = null;
			else
				this.PageSuffix = rs.getString("PageSuffix").trim();

			if( rs.getString("PagePathFTP") == null )
				this.PagePathFTP = null;
			else
				this.PagePathFTP = rs.getString("PagePathFTP").trim();

			if( rs.getString("ClickCount") == null )
				this.ClickCount = null;
			else
				this.ClickCount = rs.getString("ClickCount").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LHQueryInfo表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LHQueryInfoSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LHQueryInfoSchema getSchema()
	{
		LHQueryInfoSchema aLHQueryInfoSchema = new LHQueryInfoSchema();
		aLHQueryInfoSchema.setSchema(this);
		return aLHQueryInfoSchema;
	}

	public LHQueryInfoDB getDB()
	{
		LHQueryInfoDB aDBOper = new LHQueryInfoDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHQueryInfo描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append(StrTool.cTrim(ItemCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ItemName)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ItemType)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ItemSndCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ItemState)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ItemOtherName)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ItemContent)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ItemKeyWord)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(PageName)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(PageSuffix)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(PagePathFTP)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ClickCount)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHQueryInfo>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			ItemCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			ItemName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			ItemType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			ItemSndCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			ItemState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			ItemOtherName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			ItemContent = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			ItemKeyWord = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			PageName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			PageSuffix = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			PagePathFTP = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			ClickCount = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LHQueryInfoSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("ItemCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ItemCode));
		}
		if (FCode.equals("ItemName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ItemName));
		}
		if (FCode.equals("ItemType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ItemType));
		}
		if (FCode.equals("ItemSndCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ItemSndCode));
		}
		if (FCode.equals("ItemState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ItemState));
		}
		if (FCode.equals("ItemOtherName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ItemOtherName));
		}
		if (FCode.equals("ItemContent"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ItemContent));
		}
		if (FCode.equals("ItemKeyWord"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ItemKeyWord));
		}
		if (FCode.equals("PageName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PageName));
		}
		if (FCode.equals("PageSuffix"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PageSuffix));
		}
		if (FCode.equals("PagePathFTP"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PagePathFTP));
		}
		if (FCode.equals("ClickCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClickCount));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(ItemCode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(ItemName);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(ItemType);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(ItemSndCode);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(ItemState);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(ItemOtherName);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(ItemContent);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(ItemKeyWord);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(PageName);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(PageSuffix);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(PagePathFTP);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(ClickCount);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("ItemCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ItemCode = FValue.trim();
			}
			else
				ItemCode = null;
		}
		if (FCode.equalsIgnoreCase("ItemName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ItemName = FValue.trim();
			}
			else
				ItemName = null;
		}
		if (FCode.equalsIgnoreCase("ItemType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ItemType = FValue.trim();
			}
			else
				ItemType = null;
		}
		if (FCode.equalsIgnoreCase("ItemSndCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ItemSndCode = FValue.trim();
			}
			else
				ItemSndCode = null;
		}
		if (FCode.equalsIgnoreCase("ItemState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ItemState = FValue.trim();
			}
			else
				ItemState = null;
		}
		if (FCode.equalsIgnoreCase("ItemOtherName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ItemOtherName = FValue.trim();
			}
			else
				ItemOtherName = null;
		}
		if (FCode.equalsIgnoreCase("ItemContent"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ItemContent = FValue.trim();
			}
			else
				ItemContent = null;
		}
		if (FCode.equalsIgnoreCase("ItemKeyWord"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ItemKeyWord = FValue.trim();
			}
			else
				ItemKeyWord = null;
		}
		if (FCode.equalsIgnoreCase("PageName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PageName = FValue.trim();
			}
			else
				PageName = null;
		}
		if (FCode.equalsIgnoreCase("PageSuffix"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PageSuffix = FValue.trim();
			}
			else
				PageSuffix = null;
		}
		if (FCode.equalsIgnoreCase("PagePathFTP"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PagePathFTP = FValue.trim();
			}
			else
				PagePathFTP = null;
		}
		if (FCode.equalsIgnoreCase("ClickCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ClickCount = FValue.trim();
			}
			else
				ClickCount = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LHQueryInfoSchema other = (LHQueryInfoSchema)otherObject;
		return
			ItemCode.equals(other.getItemCode())
			&& ItemName.equals(other.getItemName())
			&& ItemType.equals(other.getItemType())
			&& ItemSndCode.equals(other.getItemSndCode())
			&& ItemState.equals(other.getItemState())
			&& ItemOtherName.equals(other.getItemOtherName())
			&& ItemContent.equals(other.getItemContent())
			&& ItemKeyWord.equals(other.getItemKeyWord())
			&& PageName.equals(other.getPageName())
			&& PageSuffix.equals(other.getPageSuffix())
			&& PagePathFTP.equals(other.getPagePathFTP())
			&& ClickCount.equals(other.getClickCount())
			&& ManageCom.equals(other.getManageCom())
			&& Operator.equals(other.getOperator())
			&& fDate.getString(MakeDate).equals(other.getMakeDate())
			&& MakeTime.equals(other.getMakeTime())
			&& fDate.getString(ModifyDate).equals(other.getModifyDate())
			&& ModifyTime.equals(other.getModifyTime());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("ItemCode") ) {
			return 0;
		}
		if( strFieldName.equals("ItemName") ) {
			return 1;
		}
		if( strFieldName.equals("ItemType") ) {
			return 2;
		}
		if( strFieldName.equals("ItemSndCode") ) {
			return 3;
		}
		if( strFieldName.equals("ItemState") ) {
			return 4;
		}
		if( strFieldName.equals("ItemOtherName") ) {
			return 5;
		}
		if( strFieldName.equals("ItemContent") ) {
			return 6;
		}
		if( strFieldName.equals("ItemKeyWord") ) {
			return 7;
		}
		if( strFieldName.equals("PageName") ) {
			return 8;
		}
		if( strFieldName.equals("PageSuffix") ) {
			return 9;
		}
		if( strFieldName.equals("PagePathFTP") ) {
			return 10;
		}
		if( strFieldName.equals("ClickCount") ) {
			return 11;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 12;
		}
		if( strFieldName.equals("Operator") ) {
			return 13;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 14;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 15;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 16;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 17;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "ItemCode";
				break;
			case 1:
				strFieldName = "ItemName";
				break;
			case 2:
				strFieldName = "ItemType";
				break;
			case 3:
				strFieldName = "ItemSndCode";
				break;
			case 4:
				strFieldName = "ItemState";
				break;
			case 5:
				strFieldName = "ItemOtherName";
				break;
			case 6:
				strFieldName = "ItemContent";
				break;
			case 7:
				strFieldName = "ItemKeyWord";
				break;
			case 8:
				strFieldName = "PageName";
				break;
			case 9:
				strFieldName = "PageSuffix";
				break;
			case 10:
				strFieldName = "PagePathFTP";
				break;
			case 11:
				strFieldName = "ClickCount";
				break;
			case 12:
				strFieldName = "ManageCom";
				break;
			case 13:
				strFieldName = "Operator";
				break;
			case 14:
				strFieldName = "MakeDate";
				break;
			case 15:
				strFieldName = "MakeTime";
				break;
			case 16:
				strFieldName = "ModifyDate";
				break;
			case 17:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("ItemCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ItemName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ItemType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ItemSndCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ItemState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ItemOtherName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ItemContent") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ItemKeyWord") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PageName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PageSuffix") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PagePathFTP") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ClickCount") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
