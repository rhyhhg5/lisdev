/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LAAcrossCommDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LAAcrossCommSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-04-30
 */
public class LAAcrossCommSchema implements Schema, Cloneable
{
    // @Field
    /** 序列号 */
    private String AcrossSN;
    /** 统计时间 */
    private Date CalDate;
    /** 原保单号 */
    private String preContNo;
    /** 原投保人姓名 */
    private String preProposalName;
    /** 原业务员代码 */
    private String preAgentCode;
    /** 原业务员姓名 */
    private String preAgentName;
    /** 待遇扣除 */
    private double FYCDeduction;
    /** 原保费 */
    private double preAmnt;
    /** 被保人姓名 */
    private String AppntName;
    /** 被保人身份证号码 */
    private String AppntIdNo;
    /** 现保单号 */
    private String ContNo;
    /** 现投保人姓名 */
    private String ProposalName;
    /** 业务员代码 */
    private String AgentCode;
    /** 业务员姓名 */
    private String AgentName;
    /** 现保费 */
    private String Amnt;
    /** 险种 */
    private String RiskCode;
    /** 缴费年限 */
    private int Payyears;
    /** 待遇补发 */
    private double FYCReinsure;
    /** 是否处理完毕 */
    private String IFFinish;
    /** 结果判定 */
    private String Result;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最近一次修改日期 */
    private Date ModifyDate;
    /** 最近一次修改时间 */
    private String ModifyTime;
    /** 操作员 */
    private String operator;

    public static final int FIELDNUM = 25; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LAAcrossCommSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "AcrossSN";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException
    {
        LAAcrossCommSchema cloned = (LAAcrossCommSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getAcrossSN()
    {
        if (SysConst.CHANGECHARSET && AcrossSN != null && !AcrossSN.equals(""))
        {
            AcrossSN = StrTool.unicodeToGBK(AcrossSN);
        }
        return AcrossSN;
    }

    public void setAcrossSN(String aAcrossSN)
    {
        AcrossSN = aAcrossSN;
    }

    public String getCalDate()
    {
        if (CalDate != null)
        {
            return fDate.getString(CalDate);
        }
        else
        {
            return null;
        }
    }

    public void setCalDate(Date aCalDate)
    {
        CalDate = aCalDate;
    }

    public void setCalDate(String aCalDate)
    {
        if (aCalDate != null && !aCalDate.equals(""))
        {
            CalDate = fDate.getDate(aCalDate);
        }
        else
        {
            CalDate = null;
        }
    }

    public String getpreContNo()
    {
        if (SysConst.CHANGECHARSET && preContNo != null && !preContNo.equals(""))
        {
            preContNo = StrTool.unicodeToGBK(preContNo);
        }
        return preContNo;
    }

    public void setpreContNo(String apreContNo)
    {
        preContNo = apreContNo;
    }

    public String getpreProposalName()
    {
        if (SysConst.CHANGECHARSET && preProposalName != null &&
            !preProposalName.equals(""))
        {
            preProposalName = StrTool.unicodeToGBK(preProposalName);
        }
        return preProposalName;
    }

    public void setpreProposalName(String apreProposalName)
    {
        preProposalName = apreProposalName;
    }

    public String getpreAgentCode()
    {
        if (SysConst.CHANGECHARSET && preAgentCode != null &&
            !preAgentCode.equals(""))
        {
            preAgentCode = StrTool.unicodeToGBK(preAgentCode);
        }
        return preAgentCode;
    }

    public void setpreAgentCode(String apreAgentCode)
    {
        preAgentCode = apreAgentCode;
    }

    public String getpreAgentName()
    {
        if (SysConst.CHANGECHARSET && preAgentName != null &&
            !preAgentName.equals(""))
        {
            preAgentName = StrTool.unicodeToGBK(preAgentName);
        }
        return preAgentName;
    }

    public void setpreAgentName(String apreAgentName)
    {
        preAgentName = apreAgentName;
    }

    public double getFYCDeduction()
    {
        return FYCDeduction;
    }

    public void setFYCDeduction(double aFYCDeduction)
    {
        FYCDeduction = aFYCDeduction;
    }

    public void setFYCDeduction(String aFYCDeduction)
    {
        if (aFYCDeduction != null && !aFYCDeduction.equals(""))
        {
            Double tDouble = new Double(aFYCDeduction);
            double d = tDouble.doubleValue();
            FYCDeduction = d;
        }
    }

    public double getpreAmnt()
    {
        return preAmnt;
    }

    public void setpreAmnt(double apreAmnt)
    {
        preAmnt = apreAmnt;
    }

    public void setpreAmnt(String apreAmnt)
    {
        if (apreAmnt != null && !apreAmnt.equals(""))
        {
            Double tDouble = new Double(apreAmnt);
            double d = tDouble.doubleValue();
            preAmnt = d;
        }
    }

    public String getAppntName()
    {
        if (SysConst.CHANGECHARSET && AppntName != null && !AppntName.equals(""))
        {
            AppntName = StrTool.unicodeToGBK(AppntName);
        }
        return AppntName;
    }

    public void setAppntName(String aAppntName)
    {
        AppntName = aAppntName;
    }

    public String getAppntIdNo()
    {
        if (SysConst.CHANGECHARSET && AppntIdNo != null && !AppntIdNo.equals(""))
        {
            AppntIdNo = StrTool.unicodeToGBK(AppntIdNo);
        }
        return AppntIdNo;
    }

    public void setAppntIdNo(String aAppntIdNo)
    {
        AppntIdNo = aAppntIdNo;
    }

    public String getContNo()
    {
        if (SysConst.CHANGECHARSET && ContNo != null && !ContNo.equals(""))
        {
            ContNo = StrTool.unicodeToGBK(ContNo);
        }
        return ContNo;
    }

    public void setContNo(String aContNo)
    {
        ContNo = aContNo;
    }

    public String getProposalName()
    {
        if (SysConst.CHANGECHARSET && ProposalName != null &&
            !ProposalName.equals(""))
        {
            ProposalName = StrTool.unicodeToGBK(ProposalName);
        }
        return ProposalName;
    }

    public void setProposalName(String aProposalName)
    {
        ProposalName = aProposalName;
    }

    public String getAgentCode()
    {
        if (SysConst.CHANGECHARSET && AgentCode != null && !AgentCode.equals(""))
        {
            AgentCode = StrTool.unicodeToGBK(AgentCode);
        }
        return AgentCode;
    }

    public void setAgentCode(String aAgentCode)
    {
        AgentCode = aAgentCode;
    }

    public String getAgentName()
    {
        if (SysConst.CHANGECHARSET && AgentName != null && !AgentName.equals(""))
        {
            AgentName = StrTool.unicodeToGBK(AgentName);
        }
        return AgentName;
    }

    public void setAgentName(String aAgentName)
    {
        AgentName = aAgentName;
    }

    public String getAmnt()
    {
        if (SysConst.CHANGECHARSET && Amnt != null && !Amnt.equals(""))
        {
            Amnt = StrTool.unicodeToGBK(Amnt);
        }
        return Amnt;
    }

    public void setAmnt(String aAmnt)
    {
        Amnt = aAmnt;
    }

    public String getRiskCode()
    {
        if (SysConst.CHANGECHARSET && RiskCode != null && !RiskCode.equals(""))
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public int getPayyears()
    {
        return Payyears;
    }

    public void setPayyears(int aPayyears)
    {
        Payyears = aPayyears;
    }

    public void setPayyears(String aPayyears)
    {
        if (aPayyears != null && !aPayyears.equals(""))
        {
            Integer tInteger = new Integer(aPayyears);
            int i = tInteger.intValue();
            Payyears = i;
        }
    }

    public double getFYCReinsure()
    {
        return FYCReinsure;
    }

    public void setFYCReinsure(double aFYCReinsure)
    {
        FYCReinsure = aFYCReinsure;
    }

    public void setFYCReinsure(String aFYCReinsure)
    {
        if (aFYCReinsure != null && !aFYCReinsure.equals(""))
        {
            Double tDouble = new Double(aFYCReinsure);
            double d = tDouble.doubleValue();
            FYCReinsure = d;
        }
    }

    public String getIFFinish()
    {
        if (SysConst.CHANGECHARSET && IFFinish != null && !IFFinish.equals(""))
        {
            IFFinish = StrTool.unicodeToGBK(IFFinish);
        }
        return IFFinish;
    }

    public void setIFFinish(String aIFFinish)
    {
        IFFinish = aIFFinish;
    }

    public String getResult()
    {
        if (SysConst.CHANGECHARSET && Result != null && !Result.equals(""))
        {
            Result = StrTool.unicodeToGBK(Result);
        }
        return Result;
    }

    public void setResult(String aResult)
    {
        Result = aResult;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (SysConst.CHANGECHARSET && MakeTime != null && !MakeTime.equals(""))
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (SysConst.CHANGECHARSET && ModifyTime != null &&
            !ModifyTime.equals(""))
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    public String getoperator()
    {
        if (SysConst.CHANGECHARSET && operator != null && !operator.equals(""))
        {
            operator = StrTool.unicodeToGBK(operator);
        }
        return operator;
    }

    public void setoperator(String aoperator)
    {
        operator = aoperator;
    }

    /**
     * 使用另外一个 LAAcrossCommSchema 对象给 Schema 赋值
     * @param: aLAAcrossCommSchema LAAcrossCommSchema
     **/
    public void setSchema(LAAcrossCommSchema aLAAcrossCommSchema)
    {
        this.AcrossSN = aLAAcrossCommSchema.getAcrossSN();
        this.CalDate = fDate.getDate(aLAAcrossCommSchema.getCalDate());
        this.preContNo = aLAAcrossCommSchema.getpreContNo();
        this.preProposalName = aLAAcrossCommSchema.getpreProposalName();
        this.preAgentCode = aLAAcrossCommSchema.getpreAgentCode();
        this.preAgentName = aLAAcrossCommSchema.getpreAgentName();
        this.FYCDeduction = aLAAcrossCommSchema.getFYCDeduction();
        this.preAmnt = aLAAcrossCommSchema.getpreAmnt();
        this.AppntName = aLAAcrossCommSchema.getAppntName();
        this.AppntIdNo = aLAAcrossCommSchema.getAppntIdNo();
        this.ContNo = aLAAcrossCommSchema.getContNo();
        this.ProposalName = aLAAcrossCommSchema.getProposalName();
        this.AgentCode = aLAAcrossCommSchema.getAgentCode();
        this.AgentName = aLAAcrossCommSchema.getAgentName();
        this.Amnt = aLAAcrossCommSchema.getAmnt();
        this.RiskCode = aLAAcrossCommSchema.getRiskCode();
        this.Payyears = aLAAcrossCommSchema.getPayyears();
        this.FYCReinsure = aLAAcrossCommSchema.getFYCReinsure();
        this.IFFinish = aLAAcrossCommSchema.getIFFinish();
        this.Result = aLAAcrossCommSchema.getResult();
        this.MakeDate = fDate.getDate(aLAAcrossCommSchema.getMakeDate());
        this.MakeTime = aLAAcrossCommSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLAAcrossCommSchema.getModifyDate());
        this.ModifyTime = aLAAcrossCommSchema.getModifyTime();
        this.operator = aLAAcrossCommSchema.getoperator();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("AcrossSN") == null)
            {
                this.AcrossSN = null;
            }
            else
            {
                this.AcrossSN = rs.getString("AcrossSN").trim();
            }

            this.CalDate = rs.getDate("CalDate");
            if (rs.getString("preContNo") == null)
            {
                this.preContNo = null;
            }
            else
            {
                this.preContNo = rs.getString("preContNo").trim();
            }

            if (rs.getString("preProposalName") == null)
            {
                this.preProposalName = null;
            }
            else
            {
                this.preProposalName = rs.getString("preProposalName").trim();
            }

            if (rs.getString("preAgentCode") == null)
            {
                this.preAgentCode = null;
            }
            else
            {
                this.preAgentCode = rs.getString("preAgentCode").trim();
            }

            if (rs.getString("preAgentName") == null)
            {
                this.preAgentName = null;
            }
            else
            {
                this.preAgentName = rs.getString("preAgentName").trim();
            }

            this.FYCDeduction = rs.getDouble("FYCDeduction");
            this.preAmnt = rs.getDouble("preAmnt");
            if (rs.getString("AppntName") == null)
            {
                this.AppntName = null;
            }
            else
            {
                this.AppntName = rs.getString("AppntName").trim();
            }

            if (rs.getString("AppntIdNo") == null)
            {
                this.AppntIdNo = null;
            }
            else
            {
                this.AppntIdNo = rs.getString("AppntIdNo").trim();
            }

            if (rs.getString("ContNo") == null)
            {
                this.ContNo = null;
            }
            else
            {
                this.ContNo = rs.getString("ContNo").trim();
            }

            if (rs.getString("ProposalName") == null)
            {
                this.ProposalName = null;
            }
            else
            {
                this.ProposalName = rs.getString("ProposalName").trim();
            }

            if (rs.getString("AgentCode") == null)
            {
                this.AgentCode = null;
            }
            else
            {
                this.AgentCode = rs.getString("AgentCode").trim();
            }

            if (rs.getString("AgentName") == null)
            {
                this.AgentName = null;
            }
            else
            {
                this.AgentName = rs.getString("AgentName").trim();
            }

            if (rs.getString("Amnt") == null)
            {
                this.Amnt = null;
            }
            else
            {
                this.Amnt = rs.getString("Amnt").trim();
            }

            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            this.Payyears = rs.getInt("Payyears");
            this.FYCReinsure = rs.getDouble("FYCReinsure");
            if (rs.getString("IFFinish") == null)
            {
                this.IFFinish = null;
            }
            else
            {
                this.IFFinish = rs.getString("IFFinish").trim();
            }

            if (rs.getString("Result") == null)
            {
                this.Result = null;
            }
            else
            {
                this.Result = rs.getString("Result").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("operator") == null)
            {
                this.operator = null;
            }
            else
            {
                this.operator = rs.getString("operator").trim();
            }

        }
        catch (SQLException sqle)
        {
            System.out.println("数据库中的LAAcrossComm表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAcrossCommSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LAAcrossCommSchema getSchema()
    {
        LAAcrossCommSchema aLAAcrossCommSchema = new LAAcrossCommSchema();
        aLAAcrossCommSchema.setSchema(this);
        return aLAAcrossCommSchema;
    }

    public LAAcrossCommDB getDB()
    {
        LAAcrossCommDB aDBOper = new LAAcrossCommDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAAcrossComm描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AcrossSN)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                CalDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(preContNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(preProposalName)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(preAgentCode)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(preAgentName)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(FYCDeduction));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(preAmnt));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AppntName)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AppntIdNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ContNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ProposalName)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AgentCode)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AgentName)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Amnt)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Payyears));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(FYCReinsure));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(IFFinish)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Result)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                MakeDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                ModifyDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(operator)));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAAcrossComm>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            AcrossSN = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            CalDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 2, SysConst.PACKAGESPILTER));
            preContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                       SysConst.PACKAGESPILTER);
            preProposalName = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             4, SysConst.PACKAGESPILTER);
            preAgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                          SysConst.PACKAGESPILTER);
            preAgentName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                          SysConst.PACKAGESPILTER);
            FYCDeduction = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 7, SysConst.PACKAGESPILTER))).doubleValue();
            preAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 8, SysConst.PACKAGESPILTER))).doubleValue();
            AppntName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                       SysConst.PACKAGESPILTER);
            AppntIdNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                       SysConst.PACKAGESPILTER);
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                    SysConst.PACKAGESPILTER);
            ProposalName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                          SysConst.PACKAGESPILTER);
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                       SysConst.PACKAGESPILTER);
            AgentName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                       SysConst.PACKAGESPILTER);
            Amnt = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                  SysConst.PACKAGESPILTER);
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                      SysConst.PACKAGESPILTER);
            Payyears = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 17, SysConst.PACKAGESPILTER))).intValue();
            FYCReinsure = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 18, SysConst.PACKAGESPILTER))).doubleValue();
            IFFinish = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                      SysConst.PACKAGESPILTER);
            Result = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                    SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 21, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 23, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,
                                        SysConst.PACKAGESPILTER);
            operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,
                                      SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAcrossCommSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("AcrossSN"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AcrossSN));
        }
        if (FCode.equals("CalDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getCalDate()));
        }
        if (FCode.equals("preContNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(preContNo));
        }
        if (FCode.equals("preProposalName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(preProposalName));
        }
        if (FCode.equals("preAgentCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(preAgentCode));
        }
        if (FCode.equals("preAgentName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(preAgentName));
        }
        if (FCode.equals("FYCDeduction"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FYCDeduction));
        }
        if (FCode.equals("preAmnt"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(preAmnt));
        }
        if (FCode.equals("AppntName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntName));
        }
        if (FCode.equals("AppntIdNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntIdNo));
        }
        if (FCode.equals("ContNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equals("ProposalName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProposalName));
        }
        if (FCode.equals("AgentCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equals("AgentName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentName));
        }
        if (FCode.equals("Amnt"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Amnt));
        }
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equals("Payyears"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Payyears));
        }
        if (FCode.equals("FYCReinsure"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FYCReinsure));
        }
        if (FCode.equals("IFFinish"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IFFinish));
        }
        if (FCode.equals("Result"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Result));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(operator));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(AcrossSN);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getCalDate()));
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(preContNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(preProposalName);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(preAgentCode);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(preAgentName);
                break;
            case 6:
                strFieldValue = String.valueOf(FYCDeduction);
                break;
            case 7:
                strFieldValue = String.valueOf(preAmnt);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(AppntName);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(AppntIdNo);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(ProposalName);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(AgentCode);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(AgentName);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(Amnt);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 16:
                strFieldValue = String.valueOf(Payyears);
                break;
            case 17:
                strFieldValue = String.valueOf(FYCReinsure);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(IFFinish);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(Result);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(operator);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("AcrossSN"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AcrossSN = FValue.trim();
            }
            else
            {
                AcrossSN = null;
            }
        }
        if (FCode.equals("CalDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CalDate = fDate.getDate(FValue);
            }
            else
            {
                CalDate = null;
            }
        }
        if (FCode.equals("preContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                preContNo = FValue.trim();
            }
            else
            {
                preContNo = null;
            }
        }
        if (FCode.equals("preProposalName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                preProposalName = FValue.trim();
            }
            else
            {
                preProposalName = null;
            }
        }
        if (FCode.equals("preAgentCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                preAgentCode = FValue.trim();
            }
            else
            {
                preAgentCode = null;
            }
        }
        if (FCode.equals("preAgentName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                preAgentName = FValue.trim();
            }
            else
            {
                preAgentName = null;
            }
        }
        if (FCode.equals("FYCDeduction"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                FYCDeduction = d;
            }
        }
        if (FCode.equals("preAmnt"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                preAmnt = d;
            }
        }
        if (FCode.equals("AppntName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppntName = FValue.trim();
            }
            else
            {
                AppntName = null;
            }
        }
        if (FCode.equals("AppntIdNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppntIdNo = FValue.trim();
            }
            else
            {
                AppntIdNo = null;
            }
        }
        if (FCode.equals("ContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
            {
                ContNo = null;
            }
        }
        if (FCode.equals("ProposalName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ProposalName = FValue.trim();
            }
            else
            {
                ProposalName = null;
            }
        }
        if (FCode.equals("AgentCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
            {
                AgentCode = null;
            }
        }
        if (FCode.equals("AgentName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentName = FValue.trim();
            }
            else
            {
                AgentName = null;
            }
        }
        if (FCode.equals("Amnt"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Amnt = FValue.trim();
            }
            else
            {
                Amnt = null;
            }
        }
        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("Payyears"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                Payyears = i;
            }
        }
        if (FCode.equals("FYCReinsure"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                FYCReinsure = d;
            }
        }
        if (FCode.equals("IFFinish"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                IFFinish = FValue.trim();
            }
            else
            {
                IFFinish = null;
            }
        }
        if (FCode.equals("Result"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Result = FValue.trim();
            }
            else
            {
                Result = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        if (FCode.equals("operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                operator = FValue.trim();
            }
            else
            {
                operator = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LAAcrossCommSchema other = (LAAcrossCommSchema) otherObject;
        return
                AcrossSN.equals(other.getAcrossSN())
                && fDate.getString(CalDate).equals(other.getCalDate())
                && preContNo.equals(other.getpreContNo())
                && preProposalName.equals(other.getpreProposalName())
                && preAgentCode.equals(other.getpreAgentCode())
                && preAgentName.equals(other.getpreAgentName())
                && FYCDeduction == other.getFYCDeduction()
                && preAmnt == other.getpreAmnt()
                && AppntName.equals(other.getAppntName())
                && AppntIdNo.equals(other.getAppntIdNo())
                && ContNo.equals(other.getContNo())
                && ProposalName.equals(other.getProposalName())
                && AgentCode.equals(other.getAgentCode())
                && AgentName.equals(other.getAgentName())
                && Amnt.equals(other.getAmnt())
                && RiskCode.equals(other.getRiskCode())
                && Payyears == other.getPayyears()
                && FYCReinsure == other.getFYCReinsure()
                && IFFinish.equals(other.getIFFinish())
                && Result.equals(other.getResult())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && operator.equals(other.getoperator());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("AcrossSN"))
        {
            return 0;
        }
        if (strFieldName.equals("CalDate"))
        {
            return 1;
        }
        if (strFieldName.equals("preContNo"))
        {
            return 2;
        }
        if (strFieldName.equals("preProposalName"))
        {
            return 3;
        }
        if (strFieldName.equals("preAgentCode"))
        {
            return 4;
        }
        if (strFieldName.equals("preAgentName"))
        {
            return 5;
        }
        if (strFieldName.equals("FYCDeduction"))
        {
            return 6;
        }
        if (strFieldName.equals("preAmnt"))
        {
            return 7;
        }
        if (strFieldName.equals("AppntName"))
        {
            return 8;
        }
        if (strFieldName.equals("AppntIdNo"))
        {
            return 9;
        }
        if (strFieldName.equals("ContNo"))
        {
            return 10;
        }
        if (strFieldName.equals("ProposalName"))
        {
            return 11;
        }
        if (strFieldName.equals("AgentCode"))
        {
            return 12;
        }
        if (strFieldName.equals("AgentName"))
        {
            return 13;
        }
        if (strFieldName.equals("Amnt"))
        {
            return 14;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return 15;
        }
        if (strFieldName.equals("Payyears"))
        {
            return 16;
        }
        if (strFieldName.equals("FYCReinsure"))
        {
            return 17;
        }
        if (strFieldName.equals("IFFinish"))
        {
            return 18;
        }
        if (strFieldName.equals("Result"))
        {
            return 19;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 20;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 21;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 22;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 23;
        }
        if (strFieldName.equals("operator"))
        {
            return 24;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "AcrossSN";
                break;
            case 1:
                strFieldName = "CalDate";
                break;
            case 2:
                strFieldName = "preContNo";
                break;
            case 3:
                strFieldName = "preProposalName";
                break;
            case 4:
                strFieldName = "preAgentCode";
                break;
            case 5:
                strFieldName = "preAgentName";
                break;
            case 6:
                strFieldName = "FYCDeduction";
                break;
            case 7:
                strFieldName = "preAmnt";
                break;
            case 8:
                strFieldName = "AppntName";
                break;
            case 9:
                strFieldName = "AppntIdNo";
                break;
            case 10:
                strFieldName = "ContNo";
                break;
            case 11:
                strFieldName = "ProposalName";
                break;
            case 12:
                strFieldName = "AgentCode";
                break;
            case 13:
                strFieldName = "AgentName";
                break;
            case 14:
                strFieldName = "Amnt";
                break;
            case 15:
                strFieldName = "RiskCode";
                break;
            case 16:
                strFieldName = "Payyears";
                break;
            case 17:
                strFieldName = "FYCReinsure";
                break;
            case 18:
                strFieldName = "IFFinish";
                break;
            case 19:
                strFieldName = "Result";
                break;
            case 20:
                strFieldName = "MakeDate";
                break;
            case 21:
                strFieldName = "MakeTime";
                break;
            case 22:
                strFieldName = "ModifyDate";
                break;
            case 23:
                strFieldName = "ModifyTime";
                break;
            case 24:
                strFieldName = "operator";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("AcrossSN"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CalDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("preContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("preProposalName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("preAgentCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("preAgentName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FYCDeduction"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("preAmnt"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("AppntName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppntIdNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ProposalName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Amnt"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Payyears"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("FYCReinsure"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("IFFinish"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Result"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("operator"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 7:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_INT;
                break;
            case 17:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 18:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 19:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 20:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 21:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 22:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 23:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 24:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
