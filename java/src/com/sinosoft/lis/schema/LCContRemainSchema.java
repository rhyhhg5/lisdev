/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LCContRemainDB;

/*
 * <p>ClassName: LCContRemainSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2014-05-15
 */
public class LCContRemainSchema implements Schema, Cloneable
{
	// @Field
	/** 保单号 */
	private String ContNo;
	/** 险种代码 */
	private String RiskCode;
	/** 申请留存时间 */
	private Date ApplyDate;
	/** 申请的保单年度 */
	private String ApplyYears;
	/** 保单留存状态 */
	private String RemainState;
	/** 留存期满时间 */
	private Date RemainEndDate;
	/** 退保时间 */
	private Date EdorZTDate;
	/** 退保费用 */
	private double EdorZTFee;
	/** 流水线号 */
	private String SerialNo;
	/** 备用字段1 */
	private String StandBy1;
	/** 备用字段2 */
	private String StandBy2;
	/** 备用字段3 */
	private String StandBy3;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 17;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LCContRemainSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "SerialNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LCContRemainSchema cloned = (LCContRemainSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getContNo()
	{
		return ContNo;
	}
	public void setContNo(String aContNo)
	{
		ContNo = aContNo;
	}
	public String getRiskCode()
	{
		return RiskCode;
	}
	public void setRiskCode(String aRiskCode)
	{
		RiskCode = aRiskCode;
	}
	public String getApplyDate()
	{
		if( ApplyDate != null )
			return fDate.getString(ApplyDate);
		else
			return null;
	}
	public void setApplyDate(Date aApplyDate)
	{
		ApplyDate = aApplyDate;
	}
	public void setApplyDate(String aApplyDate)
	{
		if (aApplyDate != null && !aApplyDate.equals("") )
		{
			ApplyDate = fDate.getDate( aApplyDate );
		}
		else
			ApplyDate = null;
	}

	public String getApplyYears()
	{
		return ApplyYears;
	}
	public void setApplyYears(String aApplyYears)
	{
		ApplyYears = aApplyYears;
	}
	public String getRemainState()
	{
		return RemainState;
	}
	public void setRemainState(String aRemainState)
	{
		RemainState = aRemainState;
	}
	public String getRemainEndDate()
	{
		if( RemainEndDate != null )
			return fDate.getString(RemainEndDate);
		else
			return null;
	}
	public void setRemainEndDate(Date aRemainEndDate)
	{
		RemainEndDate = aRemainEndDate;
	}
	public void setRemainEndDate(String aRemainEndDate)
	{
		if (aRemainEndDate != null && !aRemainEndDate.equals("") )
		{
			RemainEndDate = fDate.getDate( aRemainEndDate );
		}
		else
			RemainEndDate = null;
	}

	public String getEdorZTDate()
	{
		if( EdorZTDate != null )
			return fDate.getString(EdorZTDate);
		else
			return null;
	}
	public void setEdorZTDate(Date aEdorZTDate)
	{
		EdorZTDate = aEdorZTDate;
	}
	public void setEdorZTDate(String aEdorZTDate)
	{
		if (aEdorZTDate != null && !aEdorZTDate.equals("") )
		{
			EdorZTDate = fDate.getDate( aEdorZTDate );
		}
		else
			EdorZTDate = null;
	}

	public double getEdorZTFee()
	{
		return EdorZTFee;
	}
	public void setEdorZTFee(double aEdorZTFee)
	{
		EdorZTFee = Arith.round(aEdorZTFee,2);
	}
	public void setEdorZTFee(String aEdorZTFee)
	{
		if (aEdorZTFee != null && !aEdorZTFee.equals(""))
		{
			Double tDouble = new Double(aEdorZTFee);
			double d = tDouble.doubleValue();
                EdorZTFee = Arith.round(d,2);
		}
	}

	public String getSerialNo()
	{
		return SerialNo;
	}
	public void setSerialNo(String aSerialNo)
	{
		SerialNo = aSerialNo;
	}
	public String getStandBy1()
	{
		return StandBy1;
	}
	public void setStandBy1(String aStandBy1)
	{
		StandBy1 = aStandBy1;
	}
	public String getStandBy2()
	{
		return StandBy2;
	}
	public void setStandBy2(String aStandBy2)
	{
		StandBy2 = aStandBy2;
	}
	public String getStandBy3()
	{
		return StandBy3;
	}
	public void setStandBy3(String aStandBy3)
	{
		StandBy3 = aStandBy3;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LCContRemainSchema 对象给 Schema 赋值
	* @param: aLCContRemainSchema LCContRemainSchema
	**/
	public void setSchema(LCContRemainSchema aLCContRemainSchema)
	{
		this.ContNo = aLCContRemainSchema.getContNo();
		this.RiskCode = aLCContRemainSchema.getRiskCode();
		this.ApplyDate = fDate.getDate( aLCContRemainSchema.getApplyDate());
		this.ApplyYears = aLCContRemainSchema.getApplyYears();
		this.RemainState = aLCContRemainSchema.getRemainState();
		this.RemainEndDate = fDate.getDate( aLCContRemainSchema.getRemainEndDate());
		this.EdorZTDate = fDate.getDate( aLCContRemainSchema.getEdorZTDate());
		this.EdorZTFee = aLCContRemainSchema.getEdorZTFee();
		this.SerialNo = aLCContRemainSchema.getSerialNo();
		this.StandBy1 = aLCContRemainSchema.getStandBy1();
		this.StandBy2 = aLCContRemainSchema.getStandBy2();
		this.StandBy3 = aLCContRemainSchema.getStandBy3();
		this.Operator = aLCContRemainSchema.getOperator();
		this.MakeDate = fDate.getDate( aLCContRemainSchema.getMakeDate());
		this.MakeTime = aLCContRemainSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLCContRemainSchema.getModifyDate());
		this.ModifyTime = aLCContRemainSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("ContNo") == null )
				this.ContNo = null;
			else
				this.ContNo = rs.getString("ContNo").trim();

			if( rs.getString("RiskCode") == null )
				this.RiskCode = null;
			else
				this.RiskCode = rs.getString("RiskCode").trim();

			this.ApplyDate = rs.getDate("ApplyDate");
			if( rs.getString("ApplyYears") == null )
				this.ApplyYears = null;
			else
				this.ApplyYears = rs.getString("ApplyYears").trim();

			if( rs.getString("RemainState") == null )
				this.RemainState = null;
			else
				this.RemainState = rs.getString("RemainState").trim();

			this.RemainEndDate = rs.getDate("RemainEndDate");
			this.EdorZTDate = rs.getDate("EdorZTDate");
			this.EdorZTFee = rs.getDouble("EdorZTFee");
			if( rs.getString("SerialNo") == null )
				this.SerialNo = null;
			else
				this.SerialNo = rs.getString("SerialNo").trim();

			if( rs.getString("StandBy1") == null )
				this.StandBy1 = null;
			else
				this.StandBy1 = rs.getString("StandBy1").trim();

			if( rs.getString("StandBy2") == null )
				this.StandBy2 = null;
			else
				this.StandBy2 = rs.getString("StandBy2").trim();

			if( rs.getString("StandBy3") == null )
				this.StandBy3 = null;
			else
				this.StandBy3 = rs.getString("StandBy3").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LCContRemain表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCContRemainSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LCContRemainSchema getSchema()
	{
		LCContRemainSchema aLCContRemainSchema = new LCContRemainSchema();
		aLCContRemainSchema.setSchema(this);
		return aLCContRemainSchema;
	}

	public LCContRemainDB getDB()
	{
		LCContRemainDB aDBOper = new LCContRemainDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCContRemain描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ApplyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ApplyYears)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RemainState)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( RemainEndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( EdorZTDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(EdorZTFee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandBy1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandBy2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandBy3)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCContRemain>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			ApplyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,SysConst.PACKAGESPILTER));
			ApplyYears = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			RemainState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			RemainEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,SysConst.PACKAGESPILTER));
			EdorZTDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,SysConst.PACKAGESPILTER));
			EdorZTFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,8,SysConst.PACKAGESPILTER))).doubleValue();
			SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			StandBy1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			StandBy2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			StandBy3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCContRemainSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("ContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
		}
		if (FCode.equals("RiskCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
		}
		if (FCode.equals("ApplyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getApplyDate()));
		}
		if (FCode.equals("ApplyYears"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ApplyYears));
		}
		if (FCode.equals("RemainState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RemainState));
		}
		if (FCode.equals("RemainEndDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getRemainEndDate()));
		}
		if (FCode.equals("EdorZTDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEdorZTDate()));
		}
		if (FCode.equals("EdorZTFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EdorZTFee));
		}
		if (FCode.equals("SerialNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
		}
		if (FCode.equals("StandBy1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandBy1));
		}
		if (FCode.equals("StandBy2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandBy2));
		}
		if (FCode.equals("StandBy3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandBy3));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(ContNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(RiskCode);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getApplyDate()));
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(ApplyYears);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(RemainState);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getRemainEndDate()));
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEdorZTDate()));
				break;
			case 7:
				strFieldValue = String.valueOf(EdorZTFee);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(SerialNo);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(StandBy1);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(StandBy2);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(StandBy3);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("ContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContNo = FValue.trim();
			}
			else
				ContNo = null;
		}
		if (FCode.equalsIgnoreCase("RiskCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskCode = FValue.trim();
			}
			else
				RiskCode = null;
		}
		if (FCode.equalsIgnoreCase("ApplyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ApplyDate = fDate.getDate( FValue );
			}
			else
				ApplyDate = null;
		}
		if (FCode.equalsIgnoreCase("ApplyYears"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ApplyYears = FValue.trim();
			}
			else
				ApplyYears = null;
		}
		if (FCode.equalsIgnoreCase("RemainState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RemainState = FValue.trim();
			}
			else
				RemainState = null;
		}
		if (FCode.equalsIgnoreCase("RemainEndDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				RemainEndDate = fDate.getDate( FValue );
			}
			else
				RemainEndDate = null;
		}
		if (FCode.equalsIgnoreCase("EdorZTDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				EdorZTDate = fDate.getDate( FValue );
			}
			else
				EdorZTDate = null;
		}
		if (FCode.equalsIgnoreCase("EdorZTFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				EdorZTFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("SerialNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SerialNo = FValue.trim();
			}
			else
				SerialNo = null;
		}
		if (FCode.equalsIgnoreCase("StandBy1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandBy1 = FValue.trim();
			}
			else
				StandBy1 = null;
		}
		if (FCode.equalsIgnoreCase("StandBy2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandBy2 = FValue.trim();
			}
			else
				StandBy2 = null;
		}
		if (FCode.equalsIgnoreCase("StandBy3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandBy3 = FValue.trim();
			}
			else
				StandBy3 = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LCContRemainSchema other = (LCContRemainSchema)otherObject;
		return
			(ContNo == null ? other.getContNo() == null : ContNo.equals(other.getContNo()))
			&& (RiskCode == null ? other.getRiskCode() == null : RiskCode.equals(other.getRiskCode()))
			&& (ApplyDate == null ? other.getApplyDate() == null : fDate.getString(ApplyDate).equals(other.getApplyDate()))
			&& (ApplyYears == null ? other.getApplyYears() == null : ApplyYears.equals(other.getApplyYears()))
			&& (RemainState == null ? other.getRemainState() == null : RemainState.equals(other.getRemainState()))
			&& (RemainEndDate == null ? other.getRemainEndDate() == null : fDate.getString(RemainEndDate).equals(other.getRemainEndDate()))
			&& (EdorZTDate == null ? other.getEdorZTDate() == null : fDate.getString(EdorZTDate).equals(other.getEdorZTDate()))
			&& EdorZTFee == other.getEdorZTFee()
			&& (SerialNo == null ? other.getSerialNo() == null : SerialNo.equals(other.getSerialNo()))
			&& (StandBy1 == null ? other.getStandBy1() == null : StandBy1.equals(other.getStandBy1()))
			&& (StandBy2 == null ? other.getStandBy2() == null : StandBy2.equals(other.getStandBy2()))
			&& (StandBy3 == null ? other.getStandBy3() == null : StandBy3.equals(other.getStandBy3()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("ContNo") ) {
			return 0;
		}
		if( strFieldName.equals("RiskCode") ) {
			return 1;
		}
		if( strFieldName.equals("ApplyDate") ) {
			return 2;
		}
		if( strFieldName.equals("ApplyYears") ) {
			return 3;
		}
		if( strFieldName.equals("RemainState") ) {
			return 4;
		}
		if( strFieldName.equals("RemainEndDate") ) {
			return 5;
		}
		if( strFieldName.equals("EdorZTDate") ) {
			return 6;
		}
		if( strFieldName.equals("EdorZTFee") ) {
			return 7;
		}
		if( strFieldName.equals("SerialNo") ) {
			return 8;
		}
		if( strFieldName.equals("StandBy1") ) {
			return 9;
		}
		if( strFieldName.equals("StandBy2") ) {
			return 10;
		}
		if( strFieldName.equals("StandBy3") ) {
			return 11;
		}
		if( strFieldName.equals("Operator") ) {
			return 12;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 13;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 14;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 15;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 16;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "ContNo";
				break;
			case 1:
				strFieldName = "RiskCode";
				break;
			case 2:
				strFieldName = "ApplyDate";
				break;
			case 3:
				strFieldName = "ApplyYears";
				break;
			case 4:
				strFieldName = "RemainState";
				break;
			case 5:
				strFieldName = "RemainEndDate";
				break;
			case 6:
				strFieldName = "EdorZTDate";
				break;
			case 7:
				strFieldName = "EdorZTFee";
				break;
			case 8:
				strFieldName = "SerialNo";
				break;
			case 9:
				strFieldName = "StandBy1";
				break;
			case 10:
				strFieldName = "StandBy2";
				break;
			case 11:
				strFieldName = "StandBy3";
				break;
			case 12:
				strFieldName = "Operator";
				break;
			case 13:
				strFieldName = "MakeDate";
				break;
			case 14:
				strFieldName = "MakeTime";
				break;
			case 15:
				strFieldName = "ModifyDate";
				break;
			case 16:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("ContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ApplyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ApplyYears") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RemainState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RemainEndDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("EdorZTDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("EdorZTFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SerialNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandBy1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandBy2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandBy3") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 6:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 7:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
