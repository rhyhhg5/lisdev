/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.XingYeManualUnderwritingDB;

/*
 * <p>ClassName: XingYeManualUnderwritingSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2017-11-21
 */
public class XingYeManualUnderwritingSchema implements Schema, Cloneable
{
	// @Field
	/** Xyid */
	private String xyid;
	/** Zoneno */
	private String zoneno;
	/** Brno */
	private String brno;
	/** Tellerno */
	private String tellerno;
	/** Riskcode */
	private String riskcode;
	/** Riskname */
	private String riskname;
	/** Transdate */
	private String transdate;
	/** Transno */
	private String transno;
	/** Proposalno */
	private String proposalno;
	/** Customerrname */
	private String customerrname;
	/** Customersex */
	private String customersex;
	/** Certificatetype */
	private String certificatetype;
	/** Certificateno */
	private String certificateno;
	/** Accno */
	private String accno;
	/** Prem */
	private double prem;
	/** Sourcetype */
	private String sourcetype;
	/** Paytype */
	private String paytype;
	/** Payendyear */
	private String payendyear;
	/** Payendyearflag */
	private String payendyearflag;
	/** Insutype */
	private String insutype;
	/** Insuyear */
	private String insuyear;
	/** Insryearflag */
	private String insryearflag;
	/** Dealflag */
	private String dealflag;
	/** Descr */
	private String descr;
	/** Dealdate */
	private String dealdate;
	/** Bak1 */
	private String bak1;
	/** Bak2 */
	private String bak2;
	/** Bak3 */
	private String bak3;

	public static final int FIELDNUM = 28;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public XingYeManualUnderwritingSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[0];

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		XingYeManualUnderwritingSchema cloned = (XingYeManualUnderwritingSchema) super.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getxyid()
	{
		return xyid;
	}
	public void setxyid(String axyid)
	{
		xyid = axyid;
	}
	public String getzoneno()
	{
		return zoneno;
	}
	public void setzoneno(String azoneno)
	{
		zoneno = azoneno;
	}
	public String getbrno()
	{
		return brno;
	}
	public void setbrno(String abrno)
	{
		brno = abrno;
	}
	public String gettellerno()
	{
		return tellerno;
	}
	public void settellerno(String atellerno)
	{
		tellerno = atellerno;
	}
	public String getriskcode()
	{
		return riskcode;
	}
	public void setriskcode(String ariskcode)
	{
		riskcode = ariskcode;
	}
	public String getriskname()
	{
		return riskname;
	}
	public void setriskname(String ariskname)
	{
		riskname = ariskname;
	}
	public String gettransdate()
	{
		return transdate;
	}
	public void settransdate(String atransdate)
	{
		transdate = atransdate;
	}
	public String gettransno()
	{
		return transno;
	}
	public void settransno(String atransno)
	{
		transno = atransno;
	}
	public String getproposalno()
	{
		return proposalno;
	}
	public void setproposalno(String aproposalno)
	{
		proposalno = aproposalno;
	}
	public String getcustomerrname()
	{
		return customerrname;
	}
	public void setcustomerrname(String acustomerrname)
	{
		customerrname = acustomerrname;
	}
	public String getcustomersex()
	{
		return customersex;
	}
	public void setcustomersex(String acustomersex)
	{
		customersex = acustomersex;
	}
	public String getcertificatetype()
	{
		return certificatetype;
	}
	public void setcertificatetype(String acertificatetype)
	{
		certificatetype = acertificatetype;
	}
	public String getcertificateno()
	{
		return certificateno;
	}
	public void setcertificateno(String acertificateno)
	{
		certificateno = acertificateno;
	}
	public String getaccno()
	{
		return accno;
	}
	public void setaccno(String aaccno)
	{
		accno = aaccno;
	}
	public double getprem()
	{
		return prem;
	}
	public void setprem(double aprem)
	{
		prem = Arith.round(aprem,2);
	}
	public void setprem(String aprem)
	{
		if (aprem != null && !aprem.equals(""))
		{
			Double tDouble = new Double(aprem);
			double d = tDouble.doubleValue();
                prem = Arith.round(d,2);
		}
	}

	public String getsourcetype()
	{
		return sourcetype;
	}
	public void setsourcetype(String asourcetype)
	{
		sourcetype = asourcetype;
	}
	public String getpaytype()
	{
		return paytype;
	}
	public void setpaytype(String apaytype)
	{
		paytype = apaytype;
	}
	public String getpayendyear()
	{
		return payendyear;
	}
	public void setpayendyear(String apayendyear)
	{
		payendyear = apayendyear;
	}
	public String getpayendyearflag()
	{
		return payendyearflag;
	}
	public void setpayendyearflag(String apayendyearflag)
	{
		payendyearflag = apayendyearflag;
	}
	public String getinsutype()
	{
		return insutype;
	}
	public void setinsutype(String ainsutype)
	{
		insutype = ainsutype;
	}
	public String getinsuyear()
	{
		return insuyear;
	}
	public void setinsuyear(String ainsuyear)
	{
		insuyear = ainsuyear;
	}
	public String getinsryearflag()
	{
		return insryearflag;
	}
	public void setinsryearflag(String ainsryearflag)
	{
		insryearflag = ainsryearflag;
	}
	public String getdealflag()
	{
		return dealflag;
	}
	public void setdealflag(String adealflag)
	{
		dealflag = adealflag;
	}
	public String getdescr()
	{
		return descr;
	}
	public void setdescr(String adescr)
	{
		descr = adescr;
	}
	public String getdealdate()
	{
		return dealdate;
	}
	public void setdealdate(String adealdate)
	{
		dealdate = adealdate;
	}
	public String getbak1()
	{
		return bak1;
	}
	public void setbak1(String abak1)
	{
		bak1 = abak1;
	}
	public String getbak2()
	{
		return bak2;
	}
	public void setbak2(String abak2)
	{
		bak2 = abak2;
	}
	public String getbak3()
	{
		return bak3;
	}
	public void setbak3(String abak3)
	{
		bak3 = abak3;
	}

	/**
	* 使用另外一个 XingYeManualUnderwritingSchema 对象给 Schema 赋值
	* @param: aXingYeManualUnderwritingSchema XingYeManualUnderwritingSchema
	**/
	public void setSchema(XingYeManualUnderwritingSchema aXingYeManualUnderwritingSchema)
	{
		this.xyid = aXingYeManualUnderwritingSchema.getxyid();
		this.zoneno = aXingYeManualUnderwritingSchema.getzoneno();
		this.brno = aXingYeManualUnderwritingSchema.getbrno();
		this.tellerno = aXingYeManualUnderwritingSchema.gettellerno();
		this.riskcode = aXingYeManualUnderwritingSchema.getriskcode();
		this.riskname = aXingYeManualUnderwritingSchema.getriskname();
		this.transdate = aXingYeManualUnderwritingSchema.gettransdate();
		this.transno = aXingYeManualUnderwritingSchema.gettransno();
		this.proposalno = aXingYeManualUnderwritingSchema.getproposalno();
		this.customerrname = aXingYeManualUnderwritingSchema.getcustomerrname();
		this.customersex = aXingYeManualUnderwritingSchema.getcustomersex();
		this.certificatetype = aXingYeManualUnderwritingSchema.getcertificatetype();
		this.certificateno = aXingYeManualUnderwritingSchema.getcertificateno();
		this.accno = aXingYeManualUnderwritingSchema.getaccno();
		this.prem = aXingYeManualUnderwritingSchema.getprem();
		this.sourcetype = aXingYeManualUnderwritingSchema.getsourcetype();
		this.paytype = aXingYeManualUnderwritingSchema.getpaytype();
		this.payendyear = aXingYeManualUnderwritingSchema.getpayendyear();
		this.payendyearflag = aXingYeManualUnderwritingSchema.getpayendyearflag();
		this.insutype = aXingYeManualUnderwritingSchema.getinsutype();
		this.insuyear = aXingYeManualUnderwritingSchema.getinsuyear();
		this.insryearflag = aXingYeManualUnderwritingSchema.getinsryearflag();
		this.dealflag = aXingYeManualUnderwritingSchema.getdealflag();
		this.descr = aXingYeManualUnderwritingSchema.getdescr();
		this.dealdate = aXingYeManualUnderwritingSchema.getdealdate();
		this.bak1 = aXingYeManualUnderwritingSchema.getbak1();
		this.bak2 = aXingYeManualUnderwritingSchema.getbak2();
		this.bak3 = aXingYeManualUnderwritingSchema.getbak3();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("xyid") == null )
				this.xyid = null;
			else
				this.xyid = rs.getString("xyid").trim();

			if( rs.getString("zoneno") == null )
				this.zoneno = null;
			else
				this.zoneno = rs.getString("zoneno").trim();

			if( rs.getString("brno") == null )
				this.brno = null;
			else
				this.brno = rs.getString("brno").trim();

			if( rs.getString("tellerno") == null )
				this.tellerno = null;
			else
				this.tellerno = rs.getString("tellerno").trim();

			if( rs.getString("riskcode") == null )
				this.riskcode = null;
			else
				this.riskcode = rs.getString("riskcode").trim();

			if( rs.getString("riskname") == null )
				this.riskname = null;
			else
				this.riskname = rs.getString("riskname").trim();

			if( rs.getString("transdate") == null )
				this.transdate = null;
			else
				this.transdate = rs.getString("transdate").trim();

			if( rs.getString("transno") == null )
				this.transno = null;
			else
				this.transno = rs.getString("transno").trim();

			if( rs.getString("proposalno") == null )
				this.proposalno = null;
			else
				this.proposalno = rs.getString("proposalno").trim();

			if( rs.getString("customerrname") == null )
				this.customerrname = null;
			else
				this.customerrname = rs.getString("customerrname").trim();

			if( rs.getString("customersex") == null )
				this.customersex = null;
			else
				this.customersex = rs.getString("customersex").trim();

			if( rs.getString("certificatetype") == null )
				this.certificatetype = null;
			else
				this.certificatetype = rs.getString("certificatetype").trim();

			if( rs.getString("certificateno") == null )
				this.certificateno = null;
			else
				this.certificateno = rs.getString("certificateno").trim();

			if( rs.getString("accno") == null )
				this.accno = null;
			else
				this.accno = rs.getString("accno").trim();

			this.prem = rs.getDouble("prem");
			if( rs.getString("sourcetype") == null )
				this.sourcetype = null;
			else
				this.sourcetype = rs.getString("sourcetype").trim();

			if( rs.getString("paytype") == null )
				this.paytype = null;
			else
				this.paytype = rs.getString("paytype").trim();

			if( rs.getString("payendyear") == null )
				this.payendyear = null;
			else
				this.payendyear = rs.getString("payendyear").trim();

			if( rs.getString("payendyearflag") == null )
				this.payendyearflag = null;
			else
				this.payendyearflag = rs.getString("payendyearflag").trim();

			if( rs.getString("insutype") == null )
				this.insutype = null;
			else
				this.insutype = rs.getString("insutype").trim();

			if( rs.getString("insuyear") == null )
				this.insuyear = null;
			else
				this.insuyear = rs.getString("insuyear").trim();

			if( rs.getString("insryearflag") == null )
				this.insryearflag = null;
			else
				this.insryearflag = rs.getString("insryearflag").trim();

			if( rs.getString("dealflag") == null )
				this.dealflag = null;
			else
				this.dealflag = rs.getString("dealflag").trim();

			if( rs.getString("descr") == null )
				this.descr = null;
			else
				this.descr = rs.getString("descr").trim();

			if( rs.getString("dealdate") == null )
				this.dealdate = null;
			else
				this.dealdate = rs.getString("dealdate").trim();

			if( rs.getString("bak1") == null )
				this.bak1 = null;
			else
				this.bak1 = rs.getString("bak1").trim();

			if( rs.getString("bak2") == null )
				this.bak2 = null;
			else
				this.bak2 = rs.getString("bak2").trim();

			if( rs.getString("bak3") == null )
				this.bak3 = null;
			else
				this.bak3 = rs.getString("bak3").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的XingYeManualUnderwriting表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "XingYeManualUnderwritingSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public XingYeManualUnderwritingSchema getSchema()
	{
		XingYeManualUnderwritingSchema aXingYeManualUnderwritingSchema = new XingYeManualUnderwritingSchema();
		aXingYeManualUnderwritingSchema.setSchema(this);
		return aXingYeManualUnderwritingSchema;
	}

	public XingYeManualUnderwritingDB getDB()
	{
		XingYeManualUnderwritingDB aDBOper = new XingYeManualUnderwritingDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpXingYeManualUnderwriting描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(xyid)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(zoneno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(brno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(tellerno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(riskcode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(riskname)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(transdate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(transno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(proposalno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(customerrname)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(customersex)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(certificatetype)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(certificateno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(accno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(prem));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(sourcetype)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(paytype)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(payendyear)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(payendyearflag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(insutype)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(insuyear)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(insryearflag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(dealflag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(descr)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(dealdate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak3));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpXingYeManualUnderwriting>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			xyid = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			zoneno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			brno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			tellerno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			riskcode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			riskname = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			transdate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			transno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			proposalno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			customerrname = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			customersex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			certificatetype = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			certificateno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			accno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			prem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,15,SysConst.PACKAGESPILTER))).doubleValue();
			sourcetype = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			paytype = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			payendyear = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			payendyearflag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			insutype = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			insuyear = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			insryearflag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			dealflag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			descr = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			dealdate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			bak1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			bak2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			bak3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "XingYeManualUnderwritingSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("xyid"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(xyid));
		}
		if (FCode.equals("zoneno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(zoneno));
		}
		if (FCode.equals("brno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(brno));
		}
		if (FCode.equals("tellerno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(tellerno));
		}
		if (FCode.equals("riskcode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(riskcode));
		}
		if (FCode.equals("riskname"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(riskname));
		}
		if (FCode.equals("transdate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(transdate));
		}
		if (FCode.equals("transno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(transno));
		}
		if (FCode.equals("proposalno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(proposalno));
		}
		if (FCode.equals("customerrname"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(customerrname));
		}
		if (FCode.equals("customersex"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(customersex));
		}
		if (FCode.equals("certificatetype"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(certificatetype));
		}
		if (FCode.equals("certificateno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(certificateno));
		}
		if (FCode.equals("accno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(accno));
		}
		if (FCode.equals("prem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(prem));
		}
		if (FCode.equals("sourcetype"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(sourcetype));
		}
		if (FCode.equals("paytype"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(paytype));
		}
		if (FCode.equals("payendyear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(payendyear));
		}
		if (FCode.equals("payendyearflag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(payendyearflag));
		}
		if (FCode.equals("insutype"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(insutype));
		}
		if (FCode.equals("insuyear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(insuyear));
		}
		if (FCode.equals("insryearflag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(insryearflag));
		}
		if (FCode.equals("dealflag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(dealflag));
		}
		if (FCode.equals("descr"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(descr));
		}
		if (FCode.equals("dealdate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(dealdate));
		}
		if (FCode.equals("bak1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak1));
		}
		if (FCode.equals("bak2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak2));
		}
		if (FCode.equals("bak3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak3));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(xyid);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(zoneno);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(brno);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(tellerno);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(riskcode);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(riskname);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(transdate);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(transno);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(proposalno);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(customerrname);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(customersex);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(certificatetype);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(certificateno);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(accno);
				break;
			case 14:
				strFieldValue = String.valueOf(prem);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(sourcetype);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(paytype);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(payendyear);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(payendyearflag);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(insutype);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(insuyear);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(insryearflag);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(dealflag);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(descr);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(dealdate);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(bak1);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(bak2);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(bak3);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("xyid"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				xyid = FValue.trim();
			}
			else
				xyid = null;
		}
		if (FCode.equalsIgnoreCase("zoneno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				zoneno = FValue.trim();
			}
			else
				zoneno = null;
		}
		if (FCode.equalsIgnoreCase("brno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				brno = FValue.trim();
			}
			else
				brno = null;
		}
		if (FCode.equalsIgnoreCase("tellerno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				tellerno = FValue.trim();
			}
			else
				tellerno = null;
		}
		if (FCode.equalsIgnoreCase("riskcode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				riskcode = FValue.trim();
			}
			else
				riskcode = null;
		}
		if (FCode.equalsIgnoreCase("riskname"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				riskname = FValue.trim();
			}
			else
				riskname = null;
		}
		if (FCode.equalsIgnoreCase("transdate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				transdate = FValue.trim();
			}
			else
				transdate = null;
		}
		if (FCode.equalsIgnoreCase("transno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				transno = FValue.trim();
			}
			else
				transno = null;
		}
		if (FCode.equalsIgnoreCase("proposalno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				proposalno = FValue.trim();
			}
			else
				proposalno = null;
		}
		if (FCode.equalsIgnoreCase("customerrname"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				customerrname = FValue.trim();
			}
			else
				customerrname = null;
		}
		if (FCode.equalsIgnoreCase("customersex"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				customersex = FValue.trim();
			}
			else
				customersex = null;
		}
		if (FCode.equalsIgnoreCase("certificatetype"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				certificatetype = FValue.trim();
			}
			else
				certificatetype = null;
		}
		if (FCode.equalsIgnoreCase("certificateno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				certificateno = FValue.trim();
			}
			else
				certificateno = null;
		}
		if (FCode.equalsIgnoreCase("accno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				accno = FValue.trim();
			}
			else
				accno = null;
		}
		if (FCode.equalsIgnoreCase("prem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				prem = d;
			}
		}
		if (FCode.equalsIgnoreCase("sourcetype"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				sourcetype = FValue.trim();
			}
			else
				sourcetype = null;
		}
		if (FCode.equalsIgnoreCase("paytype"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				paytype = FValue.trim();
			}
			else
				paytype = null;
		}
		if (FCode.equalsIgnoreCase("payendyear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				payendyear = FValue.trim();
			}
			else
				payendyear = null;
		}
		if (FCode.equalsIgnoreCase("payendyearflag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				payendyearflag = FValue.trim();
			}
			else
				payendyearflag = null;
		}
		if (FCode.equalsIgnoreCase("insutype"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				insutype = FValue.trim();
			}
			else
				insutype = null;
		}
		if (FCode.equalsIgnoreCase("insuyear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				insuyear = FValue.trim();
			}
			else
				insuyear = null;
		}
		if (FCode.equalsIgnoreCase("insryearflag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				insryearflag = FValue.trim();
			}
			else
				insryearflag = null;
		}
		if (FCode.equalsIgnoreCase("dealflag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				dealflag = FValue.trim();
			}
			else
				dealflag = null;
		}
		if (FCode.equalsIgnoreCase("descr"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				descr = FValue.trim();
			}
			else
				descr = null;
		}
		if (FCode.equalsIgnoreCase("dealdate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				dealdate = FValue.trim();
			}
			else
				dealdate = null;
		}
		if (FCode.equalsIgnoreCase("bak1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak1 = FValue.trim();
			}
			else
				bak1 = null;
		}
		if (FCode.equalsIgnoreCase("bak2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak2 = FValue.trim();
			}
			else
				bak2 = null;
		}
		if (FCode.equalsIgnoreCase("bak3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak3 = FValue.trim();
			}
			else
				bak3 = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		XingYeManualUnderwritingSchema other = (XingYeManualUnderwritingSchema)otherObject;
		return
			(xyid == null ? other.getxyid() == null : xyid.equals(other.getxyid()))
			&& (zoneno == null ? other.getzoneno() == null : zoneno.equals(other.getzoneno()))
			&& (brno == null ? other.getbrno() == null : brno.equals(other.getbrno()))
			&& (tellerno == null ? other.gettellerno() == null : tellerno.equals(other.gettellerno()))
			&& (riskcode == null ? other.getriskcode() == null : riskcode.equals(other.getriskcode()))
			&& (riskname == null ? other.getriskname() == null : riskname.equals(other.getriskname()))
			&& (transdate == null ? other.gettransdate() == null : transdate.equals(other.gettransdate()))
			&& (transno == null ? other.gettransno() == null : transno.equals(other.gettransno()))
			&& (proposalno == null ? other.getproposalno() == null : proposalno.equals(other.getproposalno()))
			&& (customerrname == null ? other.getcustomerrname() == null : customerrname.equals(other.getcustomerrname()))
			&& (customersex == null ? other.getcustomersex() == null : customersex.equals(other.getcustomersex()))
			&& (certificatetype == null ? other.getcertificatetype() == null : certificatetype.equals(other.getcertificatetype()))
			&& (certificateno == null ? other.getcertificateno() == null : certificateno.equals(other.getcertificateno()))
			&& (accno == null ? other.getaccno() == null : accno.equals(other.getaccno()))
			&& prem == other.getprem()
			&& (sourcetype == null ? other.getsourcetype() == null : sourcetype.equals(other.getsourcetype()))
			&& (paytype == null ? other.getpaytype() == null : paytype.equals(other.getpaytype()))
			&& (payendyear == null ? other.getpayendyear() == null : payendyear.equals(other.getpayendyear()))
			&& (payendyearflag == null ? other.getpayendyearflag() == null : payendyearflag.equals(other.getpayendyearflag()))
			&& (insutype == null ? other.getinsutype() == null : insutype.equals(other.getinsutype()))
			&& (insuyear == null ? other.getinsuyear() == null : insuyear.equals(other.getinsuyear()))
			&& (insryearflag == null ? other.getinsryearflag() == null : insryearflag.equals(other.getinsryearflag()))
			&& (dealflag == null ? other.getdealflag() == null : dealflag.equals(other.getdealflag()))
			&& (descr == null ? other.getdescr() == null : descr.equals(other.getdescr()))
			&& (dealdate == null ? other.getdealdate() == null : dealdate.equals(other.getdealdate()))
			&& (bak1 == null ? other.getbak1() == null : bak1.equals(other.getbak1()))
			&& (bak2 == null ? other.getbak2() == null : bak2.equals(other.getbak2()))
			&& (bak3 == null ? other.getbak3() == null : bak3.equals(other.getbak3()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("xyid") ) {
			return 0;
		}
		if( strFieldName.equals("zoneno") ) {
			return 1;
		}
		if( strFieldName.equals("brno") ) {
			return 2;
		}
		if( strFieldName.equals("tellerno") ) {
			return 3;
		}
		if( strFieldName.equals("riskcode") ) {
			return 4;
		}
		if( strFieldName.equals("riskname") ) {
			return 5;
		}
		if( strFieldName.equals("transdate") ) {
			return 6;
		}
		if( strFieldName.equals("transno") ) {
			return 7;
		}
		if( strFieldName.equals("proposalno") ) {
			return 8;
		}
		if( strFieldName.equals("customerrname") ) {
			return 9;
		}
		if( strFieldName.equals("customersex") ) {
			return 10;
		}
		if( strFieldName.equals("certificatetype") ) {
			return 11;
		}
		if( strFieldName.equals("certificateno") ) {
			return 12;
		}
		if( strFieldName.equals("accno") ) {
			return 13;
		}
		if( strFieldName.equals("prem") ) {
			return 14;
		}
		if( strFieldName.equals("sourcetype") ) {
			return 15;
		}
		if( strFieldName.equals("paytype") ) {
			return 16;
		}
		if( strFieldName.equals("payendyear") ) {
			return 17;
		}
		if( strFieldName.equals("payendyearflag") ) {
			return 18;
		}
		if( strFieldName.equals("insutype") ) {
			return 19;
		}
		if( strFieldName.equals("insuyear") ) {
			return 20;
		}
		if( strFieldName.equals("insryearflag") ) {
			return 21;
		}
		if( strFieldName.equals("dealflag") ) {
			return 22;
		}
		if( strFieldName.equals("descr") ) {
			return 23;
		}
		if( strFieldName.equals("dealdate") ) {
			return 24;
		}
		if( strFieldName.equals("bak1") ) {
			return 25;
		}
		if( strFieldName.equals("bak2") ) {
			return 26;
		}
		if( strFieldName.equals("bak3") ) {
			return 27;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "xyid";
				break;
			case 1:
				strFieldName = "zoneno";
				break;
			case 2:
				strFieldName = "brno";
				break;
			case 3:
				strFieldName = "tellerno";
				break;
			case 4:
				strFieldName = "riskcode";
				break;
			case 5:
				strFieldName = "riskname";
				break;
			case 6:
				strFieldName = "transdate";
				break;
			case 7:
				strFieldName = "transno";
				break;
			case 8:
				strFieldName = "proposalno";
				break;
			case 9:
				strFieldName = "customerrname";
				break;
			case 10:
				strFieldName = "customersex";
				break;
			case 11:
				strFieldName = "certificatetype";
				break;
			case 12:
				strFieldName = "certificateno";
				break;
			case 13:
				strFieldName = "accno";
				break;
			case 14:
				strFieldName = "prem";
				break;
			case 15:
				strFieldName = "sourcetype";
				break;
			case 16:
				strFieldName = "paytype";
				break;
			case 17:
				strFieldName = "payendyear";
				break;
			case 18:
				strFieldName = "payendyearflag";
				break;
			case 19:
				strFieldName = "insutype";
				break;
			case 20:
				strFieldName = "insuyear";
				break;
			case 21:
				strFieldName = "insryearflag";
				break;
			case 22:
				strFieldName = "dealflag";
				break;
			case 23:
				strFieldName = "descr";
				break;
			case 24:
				strFieldName = "dealdate";
				break;
			case 25:
				strFieldName = "bak1";
				break;
			case 26:
				strFieldName = "bak2";
				break;
			case 27:
				strFieldName = "bak3";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("xyid") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("zoneno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("brno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("tellerno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("riskcode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("riskname") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("transdate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("transno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("proposalno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("customerrname") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("customersex") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("certificatetype") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("certificateno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("accno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("prem") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("sourcetype") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("paytype") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("payendyear") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("payendyearflag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("insutype") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("insuyear") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("insryearflag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("dealflag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("descr") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("dealdate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak3") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
