/*
 * <p>ClassName: LREdorMainSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LREdorMainDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LREdorMainSchema implements Schema
{
    // @Field
    /** 批单号 */
    private String EdorNo;
    /** 保单号码 */
    private String PolNo;
    /** 批改类型 */
    private String EdorType;
    /** 再保险公司 */
    private String ReinsureCom;
    /** 再保项目 */
    private String ReinsurItem;
    /** 保单年度 */
    private int InsuredYear;
    /** 批改申请号 */
    private String EdorAppNo;
    /** 管理机构 */
    private String ManageCom;
    /** 总单/合同号码 */
    private String ContNo;
    /** 集体保单号码 */
    private String GrpPolNo;
    /** 被保人客户号码 */
    private String InsuredNo;
    /** 被保人名称 */
    private String InsuredName;
    /** 交至日期 */
    private Date PaytoDate;
    /** 总累计保费 */
    private double SumPrem;
    /** 算法编码 */
    private String CalCode;
    /** 变动的保费 */
    private double ChgPrem;
    /** 变动的保额 */
    private double ChgAmnt;
    /** 变动的领取保额 */
    private double ChgGetAmnt;
    /** 补/退费金额 */
    private double GetMoney;
    /** 补/退费利息 */
    private double GetInterest;
    /** 批改生效日期 */
    private Date EdorValiDate;
    /** 批改申请有效日期 */
    private Date EdorAppValiDate;
    /** 批改申请日期 */
    private Date EdorAppDate;
    /** 批改状态 */
    private String EdorState;
    /** 核保标志 */
    private String UWState;
    /** 转入公共帐户 */
    private String PubAccFlag;
    /** 复核人编码 */
    private String ApproveCode;
    /** 复核日期 */
    private Date ApproveDate;
    /** 核保人 */
    private String UWOperator;
    /** 确认人 */
    private String ConfOperator;
    /** 确认日期 */
    private Date ConfDate;
    /** 确认时间 */
    private Date ConfTime;
    /** 减少分保保额 */
    private double ChgCessAmt;
    /** 应退回分保费 */
    private double ShRePrem;
    /** 应扣除分保佣金 */
    private double ShReComm;
    /** 分保开始日期 */
    private Date CessStart;
    /** 分保结束日期 */
    private Date CessEnd;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 流水号 */
    private String SerialNo;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 险种计算分类 */
    private String RiskCalSort;

    public static final int FIELDNUM = 44; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LREdorMainSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[7];
        pk[0] = "EdorNo";
        pk[1] = "PolNo";
        pk[2] = "EdorType";
        pk[3] = "ReinsureCom";
        pk[4] = "ReinsurItem";
        pk[5] = "InsuredYear";
        pk[6] = "RiskCalSort";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getEdorNo()
    {
        if (EdorNo != null && !EdorNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            EdorNo = StrTool.unicodeToGBK(EdorNo);
        }
        return EdorNo;
    }

    public void setEdorNo(String aEdorNo)
    {
        EdorNo = aEdorNo;
    }

    public String getPolNo()
    {
        if (PolNo != null && !PolNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            PolNo = StrTool.unicodeToGBK(PolNo);
        }
        return PolNo;
    }

    public void setPolNo(String aPolNo)
    {
        PolNo = aPolNo;
    }

    public String getEdorType()
    {
        if (EdorType != null && !EdorType.equals("") && SysConst.CHANGECHARSET == true)
        {
            EdorType = StrTool.unicodeToGBK(EdorType);
        }
        return EdorType;
    }

    public void setEdorType(String aEdorType)
    {
        EdorType = aEdorType;
    }

    public String getReinsureCom()
    {
        if (ReinsureCom != null && !ReinsureCom.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ReinsureCom = StrTool.unicodeToGBK(ReinsureCom);
        }
        return ReinsureCom;
    }

    public void setReinsureCom(String aReinsureCom)
    {
        ReinsureCom = aReinsureCom;
    }

    public String getReinsurItem()
    {
        if (ReinsurItem != null && !ReinsurItem.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ReinsurItem = StrTool.unicodeToGBK(ReinsurItem);
        }
        return ReinsurItem;
    }

    public void setReinsurItem(String aReinsurItem)
    {
        ReinsurItem = aReinsurItem;
    }

    public int getInsuredYear()
    {
        return InsuredYear;
    }

    public void setInsuredYear(int aInsuredYear)
    {
        InsuredYear = aInsuredYear;
    }

    public void setInsuredYear(String aInsuredYear)
    {
        if (aInsuredYear != null && !aInsuredYear.equals(""))
        {
            Integer tInteger = new Integer(aInsuredYear);
            int i = tInteger.intValue();
            InsuredYear = i;
        }
    }

    public String getEdorAppNo()
    {
        if (EdorAppNo != null && !EdorAppNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            EdorAppNo = StrTool.unicodeToGBK(EdorAppNo);
        }
        return EdorAppNo;
    }

    public void setEdorAppNo(String aEdorAppNo)
    {
        EdorAppNo = aEdorAppNo;
    }

    public String getManageCom()
    {
        if (ManageCom != null && !ManageCom.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ManageCom = StrTool.unicodeToGBK(ManageCom);
        }
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public String getContNo()
    {
        if (ContNo != null && !ContNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            ContNo = StrTool.unicodeToGBK(ContNo);
        }
        return ContNo;
    }

    public void setContNo(String aContNo)
    {
        ContNo = aContNo;
    }

    public String getGrpPolNo()
    {
        if (GrpPolNo != null && !GrpPolNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            GrpPolNo = StrTool.unicodeToGBK(GrpPolNo);
        }
        return GrpPolNo;
    }

    public void setGrpPolNo(String aGrpPolNo)
    {
        GrpPolNo = aGrpPolNo;
    }

    public String getInsuredNo()
    {
        if (InsuredNo != null && !InsuredNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            InsuredNo = StrTool.unicodeToGBK(InsuredNo);
        }
        return InsuredNo;
    }

    public void setInsuredNo(String aInsuredNo)
    {
        InsuredNo = aInsuredNo;
    }

    public String getInsuredName()
    {
        if (InsuredName != null && !InsuredName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            InsuredName = StrTool.unicodeToGBK(InsuredName);
        }
        return InsuredName;
    }

    public void setInsuredName(String aInsuredName)
    {
        InsuredName = aInsuredName;
    }

    public String getPaytoDate()
    {
        if (PaytoDate != null)
        {
            return fDate.getString(PaytoDate);
        }
        else
        {
            return null;
        }
    }

    public void setPaytoDate(Date aPaytoDate)
    {
        PaytoDate = aPaytoDate;
    }

    public void setPaytoDate(String aPaytoDate)
    {
        if (aPaytoDate != null && !aPaytoDate.equals(""))
        {
            PaytoDate = fDate.getDate(aPaytoDate);
        }
        else
        {
            PaytoDate = null;
        }
    }

    public double getSumPrem()
    {
        return SumPrem;
    }

    public void setSumPrem(double aSumPrem)
    {
        SumPrem = aSumPrem;
    }

    public void setSumPrem(String aSumPrem)
    {
        if (aSumPrem != null && !aSumPrem.equals(""))
        {
            Double tDouble = new Double(aSumPrem);
            double d = tDouble.doubleValue();
            SumPrem = d;
        }
    }

    public String getCalCode()
    {
        if (CalCode != null && !CalCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            CalCode = StrTool.unicodeToGBK(CalCode);
        }
        return CalCode;
    }

    public void setCalCode(String aCalCode)
    {
        CalCode = aCalCode;
    }

    public double getChgPrem()
    {
        return ChgPrem;
    }

    public void setChgPrem(double aChgPrem)
    {
        ChgPrem = aChgPrem;
    }

    public void setChgPrem(String aChgPrem)
    {
        if (aChgPrem != null && !aChgPrem.equals(""))
        {
            Double tDouble = new Double(aChgPrem);
            double d = tDouble.doubleValue();
            ChgPrem = d;
        }
    }

    public double getChgAmnt()
    {
        return ChgAmnt;
    }

    public void setChgAmnt(double aChgAmnt)
    {
        ChgAmnt = aChgAmnt;
    }

    public void setChgAmnt(String aChgAmnt)
    {
        if (aChgAmnt != null && !aChgAmnt.equals(""))
        {
            Double tDouble = new Double(aChgAmnt);
            double d = tDouble.doubleValue();
            ChgAmnt = d;
        }
    }

    public double getChgGetAmnt()
    {
        return ChgGetAmnt;
    }

    public void setChgGetAmnt(double aChgGetAmnt)
    {
        ChgGetAmnt = aChgGetAmnt;
    }

    public void setChgGetAmnt(String aChgGetAmnt)
    {
        if (aChgGetAmnt != null && !aChgGetAmnt.equals(""))
        {
            Double tDouble = new Double(aChgGetAmnt);
            double d = tDouble.doubleValue();
            ChgGetAmnt = d;
        }
    }

    public double getGetMoney()
    {
        return GetMoney;
    }

    public void setGetMoney(double aGetMoney)
    {
        GetMoney = aGetMoney;
    }

    public void setGetMoney(String aGetMoney)
    {
        if (aGetMoney != null && !aGetMoney.equals(""))
        {
            Double tDouble = new Double(aGetMoney);
            double d = tDouble.doubleValue();
            GetMoney = d;
        }
    }

    public double getGetInterest()
    {
        return GetInterest;
    }

    public void setGetInterest(double aGetInterest)
    {
        GetInterest = aGetInterest;
    }

    public void setGetInterest(String aGetInterest)
    {
        if (aGetInterest != null && !aGetInterest.equals(""))
        {
            Double tDouble = new Double(aGetInterest);
            double d = tDouble.doubleValue();
            GetInterest = d;
        }
    }

    public String getEdorValiDate()
    {
        if (EdorValiDate != null)
        {
            return fDate.getString(EdorValiDate);
        }
        else
        {
            return null;
        }
    }

    public void setEdorValiDate(Date aEdorValiDate)
    {
        EdorValiDate = aEdorValiDate;
    }

    public void setEdorValiDate(String aEdorValiDate)
    {
        if (aEdorValiDate != null && !aEdorValiDate.equals(""))
        {
            EdorValiDate = fDate.getDate(aEdorValiDate);
        }
        else
        {
            EdorValiDate = null;
        }
    }

    public String getEdorAppValiDate()
    {
        if (EdorAppValiDate != null)
        {
            return fDate.getString(EdorAppValiDate);
        }
        else
        {
            return null;
        }
    }

    public void setEdorAppValiDate(Date aEdorAppValiDate)
    {
        EdorAppValiDate = aEdorAppValiDate;
    }

    public void setEdorAppValiDate(String aEdorAppValiDate)
    {
        if (aEdorAppValiDate != null && !aEdorAppValiDate.equals(""))
        {
            EdorAppValiDate = fDate.getDate(aEdorAppValiDate);
        }
        else
        {
            EdorAppValiDate = null;
        }
    }

    public String getEdorAppDate()
    {
        if (EdorAppDate != null)
        {
            return fDate.getString(EdorAppDate);
        }
        else
        {
            return null;
        }
    }

    public void setEdorAppDate(Date aEdorAppDate)
    {
        EdorAppDate = aEdorAppDate;
    }

    public void setEdorAppDate(String aEdorAppDate)
    {
        if (aEdorAppDate != null && !aEdorAppDate.equals(""))
        {
            EdorAppDate = fDate.getDate(aEdorAppDate);
        }
        else
        {
            EdorAppDate = null;
        }
    }

    public String getEdorState()
    {
        if (EdorState != null && !EdorState.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            EdorState = StrTool.unicodeToGBK(EdorState);
        }
        return EdorState;
    }

    public void setEdorState(String aEdorState)
    {
        EdorState = aEdorState;
    }

    public String getUWState()
    {
        if (UWState != null && !UWState.equals("") && SysConst.CHANGECHARSET == true)
        {
            UWState = StrTool.unicodeToGBK(UWState);
        }
        return UWState;
    }

    public void setUWState(String aUWState)
    {
        UWState = aUWState;
    }

    public String getPubAccFlag()
    {
        if (PubAccFlag != null && !PubAccFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            PubAccFlag = StrTool.unicodeToGBK(PubAccFlag);
        }
        return PubAccFlag;
    }

    public void setPubAccFlag(String aPubAccFlag)
    {
        PubAccFlag = aPubAccFlag;
    }

    public String getApproveCode()
    {
        if (ApproveCode != null && !ApproveCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ApproveCode = StrTool.unicodeToGBK(ApproveCode);
        }
        return ApproveCode;
    }

    public void setApproveCode(String aApproveCode)
    {
        ApproveCode = aApproveCode;
    }

    public String getApproveDate()
    {
        if (ApproveDate != null)
        {
            return fDate.getString(ApproveDate);
        }
        else
        {
            return null;
        }
    }

    public void setApproveDate(Date aApproveDate)
    {
        ApproveDate = aApproveDate;
    }

    public void setApproveDate(String aApproveDate)
    {
        if (aApproveDate != null && !aApproveDate.equals(""))
        {
            ApproveDate = fDate.getDate(aApproveDate);
        }
        else
        {
            ApproveDate = null;
        }
    }

    public String getUWOperator()
    {
        if (UWOperator != null && !UWOperator.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            UWOperator = StrTool.unicodeToGBK(UWOperator);
        }
        return UWOperator;
    }

    public void setUWOperator(String aUWOperator)
    {
        UWOperator = aUWOperator;
    }

    public String getConfOperator()
    {
        if (ConfOperator != null && !ConfOperator.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ConfOperator = StrTool.unicodeToGBK(ConfOperator);
        }
        return ConfOperator;
    }

    public void setConfOperator(String aConfOperator)
    {
        ConfOperator = aConfOperator;
    }

    public String getConfDate()
    {
        if (ConfDate != null)
        {
            return fDate.getString(ConfDate);
        }
        else
        {
            return null;
        }
    }

    public void setConfDate(Date aConfDate)
    {
        ConfDate = aConfDate;
    }

    public void setConfDate(String aConfDate)
    {
        if (aConfDate != null && !aConfDate.equals(""))
        {
            ConfDate = fDate.getDate(aConfDate);
        }
        else
        {
            ConfDate = null;
        }
    }

    public String getConfTime()
    {
        if (ConfTime != null)
        {
            return fDate.getString(ConfTime);
        }
        else
        {
            return null;
        }
    }

    public void setConfTime(Date aConfTime)
    {
        ConfTime = aConfTime;
    }

    public void setConfTime(String aConfTime)
    {
        if (aConfTime != null && !aConfTime.equals(""))
        {
            ConfTime = fDate.getDate(aConfTime);
        }
        else
        {
            ConfTime = null;
        }
    }

    public double getChgCessAmt()
    {
        return ChgCessAmt;
    }

    public void setChgCessAmt(double aChgCessAmt)
    {
        ChgCessAmt = aChgCessAmt;
    }

    public void setChgCessAmt(String aChgCessAmt)
    {
        if (aChgCessAmt != null && !aChgCessAmt.equals(""))
        {
            Double tDouble = new Double(aChgCessAmt);
            double d = tDouble.doubleValue();
            ChgCessAmt = d;
        }
    }

    public double getShRePrem()
    {
        return ShRePrem;
    }

    public void setShRePrem(double aShRePrem)
    {
        ShRePrem = aShRePrem;
    }

    public void setShRePrem(String aShRePrem)
    {
        if (aShRePrem != null && !aShRePrem.equals(""))
        {
            Double tDouble = new Double(aShRePrem);
            double d = tDouble.doubleValue();
            ShRePrem = d;
        }
    }

    public double getShReComm()
    {
        return ShReComm;
    }

    public void setShReComm(double aShReComm)
    {
        ShReComm = aShReComm;
    }

    public void setShReComm(String aShReComm)
    {
        if (aShReComm != null && !aShReComm.equals(""))
        {
            Double tDouble = new Double(aShReComm);
            double d = tDouble.doubleValue();
            ShReComm = d;
        }
    }

    public String getCessStart()
    {
        if (CessStart != null)
        {
            return fDate.getString(CessStart);
        }
        else
        {
            return null;
        }
    }

    public void setCessStart(Date aCessStart)
    {
        CessStart = aCessStart;
    }

    public void setCessStart(String aCessStart)
    {
        if (aCessStart != null && !aCessStart.equals(""))
        {
            CessStart = fDate.getDate(aCessStart);
        }
        else
        {
            CessStart = null;
        }
    }

    public String getCessEnd()
    {
        if (CessEnd != null)
        {
            return fDate.getString(CessEnd);
        }
        else
        {
            return null;
        }
    }

    public void setCessEnd(Date aCessEnd)
    {
        CessEnd = aCessEnd;
    }

    public void setCessEnd(String aCessEnd)
    {
        if (aCessEnd != null && !aCessEnd.equals(""))
        {
            CessEnd = fDate.getDate(aCessEnd);
        }
        else
        {
            CessEnd = null;
        }
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getSerialNo()
    {
        if (SerialNo != null && !SerialNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            SerialNo = StrTool.unicodeToGBK(SerialNo);
        }
        return SerialNo;
    }

    public void setSerialNo(String aSerialNo)
    {
        SerialNo = aSerialNo;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    public String getRiskCalSort()
    {
        if (RiskCalSort != null && !RiskCalSort.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            RiskCalSort = StrTool.unicodeToGBK(RiskCalSort);
        }
        return RiskCalSort;
    }

    public void setRiskCalSort(String aRiskCalSort)
    {
        RiskCalSort = aRiskCalSort;
    }

    /**
     * 使用另外一个 LREdorMainSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LREdorMainSchema aLREdorMainSchema)
    {
        this.EdorNo = aLREdorMainSchema.getEdorNo();
        this.PolNo = aLREdorMainSchema.getPolNo();
        this.EdorType = aLREdorMainSchema.getEdorType();
        this.ReinsureCom = aLREdorMainSchema.getReinsureCom();
        this.ReinsurItem = aLREdorMainSchema.getReinsurItem();
        this.InsuredYear = aLREdorMainSchema.getInsuredYear();
        this.EdorAppNo = aLREdorMainSchema.getEdorAppNo();
        this.ManageCom = aLREdorMainSchema.getManageCom();
        this.ContNo = aLREdorMainSchema.getContNo();
        this.GrpPolNo = aLREdorMainSchema.getGrpPolNo();
        this.InsuredNo = aLREdorMainSchema.getInsuredNo();
        this.InsuredName = aLREdorMainSchema.getInsuredName();
        this.PaytoDate = fDate.getDate(aLREdorMainSchema.getPaytoDate());
        this.SumPrem = aLREdorMainSchema.getSumPrem();
        this.CalCode = aLREdorMainSchema.getCalCode();
        this.ChgPrem = aLREdorMainSchema.getChgPrem();
        this.ChgAmnt = aLREdorMainSchema.getChgAmnt();
        this.ChgGetAmnt = aLREdorMainSchema.getChgGetAmnt();
        this.GetMoney = aLREdorMainSchema.getGetMoney();
        this.GetInterest = aLREdorMainSchema.getGetInterest();
        this.EdorValiDate = fDate.getDate(aLREdorMainSchema.getEdorValiDate());
        this.EdorAppValiDate = fDate.getDate(aLREdorMainSchema.
                                             getEdorAppValiDate());
        this.EdorAppDate = fDate.getDate(aLREdorMainSchema.getEdorAppDate());
        this.EdorState = aLREdorMainSchema.getEdorState();
        this.UWState = aLREdorMainSchema.getUWState();
        this.PubAccFlag = aLREdorMainSchema.getPubAccFlag();
        this.ApproveCode = aLREdorMainSchema.getApproveCode();
        this.ApproveDate = fDate.getDate(aLREdorMainSchema.getApproveDate());
        this.UWOperator = aLREdorMainSchema.getUWOperator();
        this.ConfOperator = aLREdorMainSchema.getConfOperator();
        this.ConfDate = fDate.getDate(aLREdorMainSchema.getConfDate());
        this.ConfTime = fDate.getDate(aLREdorMainSchema.getConfTime());
        this.ChgCessAmt = aLREdorMainSchema.getChgCessAmt();
        this.ShRePrem = aLREdorMainSchema.getShRePrem();
        this.ShReComm = aLREdorMainSchema.getShReComm();
        this.CessStart = fDate.getDate(aLREdorMainSchema.getCessStart());
        this.CessEnd = fDate.getDate(aLREdorMainSchema.getCessEnd());
        this.Operator = aLREdorMainSchema.getOperator();
        this.MakeDate = fDate.getDate(aLREdorMainSchema.getMakeDate());
        this.MakeTime = aLREdorMainSchema.getMakeTime();
        this.SerialNo = aLREdorMainSchema.getSerialNo();
        this.ModifyDate = fDate.getDate(aLREdorMainSchema.getModifyDate());
        this.ModifyTime = aLREdorMainSchema.getModifyTime();
        this.RiskCalSort = aLREdorMainSchema.getRiskCalSort();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("EdorNo") == null)
            {
                this.EdorNo = null;
            }
            else
            {
                this.EdorNo = rs.getString("EdorNo").trim();
            }

            if (rs.getString("PolNo") == null)
            {
                this.PolNo = null;
            }
            else
            {
                this.PolNo = rs.getString("PolNo").trim();
            }

            if (rs.getString("EdorType") == null)
            {
                this.EdorType = null;
            }
            else
            {
                this.EdorType = rs.getString("EdorType").trim();
            }

            if (rs.getString("ReinsureCom") == null)
            {
                this.ReinsureCom = null;
            }
            else
            {
                this.ReinsureCom = rs.getString("ReinsureCom").trim();
            }

            if (rs.getString("ReinsurItem") == null)
            {
                this.ReinsurItem = null;
            }
            else
            {
                this.ReinsurItem = rs.getString("ReinsurItem").trim();
            }

            this.InsuredYear = rs.getInt("InsuredYear");
            if (rs.getString("EdorAppNo") == null)
            {
                this.EdorAppNo = null;
            }
            else
            {
                this.EdorAppNo = rs.getString("EdorAppNo").trim();
            }

            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("ContNo") == null)
            {
                this.ContNo = null;
            }
            else
            {
                this.ContNo = rs.getString("ContNo").trim();
            }

            if (rs.getString("GrpPolNo") == null)
            {
                this.GrpPolNo = null;
            }
            else
            {
                this.GrpPolNo = rs.getString("GrpPolNo").trim();
            }

            if (rs.getString("InsuredNo") == null)
            {
                this.InsuredNo = null;
            }
            else
            {
                this.InsuredNo = rs.getString("InsuredNo").trim();
            }

            if (rs.getString("InsuredName") == null)
            {
                this.InsuredName = null;
            }
            else
            {
                this.InsuredName = rs.getString("InsuredName").trim();
            }

            this.PaytoDate = rs.getDate("PaytoDate");
            this.SumPrem = rs.getDouble("SumPrem");
            if (rs.getString("CalCode") == null)
            {
                this.CalCode = null;
            }
            else
            {
                this.CalCode = rs.getString("CalCode").trim();
            }

            this.ChgPrem = rs.getDouble("ChgPrem");
            this.ChgAmnt = rs.getDouble("ChgAmnt");
            this.ChgGetAmnt = rs.getDouble("ChgGetAmnt");
            this.GetMoney = rs.getDouble("GetMoney");
            this.GetInterest = rs.getDouble("GetInterest");
            this.EdorValiDate = rs.getDate("EdorValiDate");
            this.EdorAppValiDate = rs.getDate("EdorAppValiDate");
            this.EdorAppDate = rs.getDate("EdorAppDate");
            if (rs.getString("EdorState") == null)
            {
                this.EdorState = null;
            }
            else
            {
                this.EdorState = rs.getString("EdorState").trim();
            }

            if (rs.getString("UWState") == null)
            {
                this.UWState = null;
            }
            else
            {
                this.UWState = rs.getString("UWState").trim();
            }

            if (rs.getString("PubAccFlag") == null)
            {
                this.PubAccFlag = null;
            }
            else
            {
                this.PubAccFlag = rs.getString("PubAccFlag").trim();
            }

            if (rs.getString("ApproveCode") == null)
            {
                this.ApproveCode = null;
            }
            else
            {
                this.ApproveCode = rs.getString("ApproveCode").trim();
            }

            this.ApproveDate = rs.getDate("ApproveDate");
            if (rs.getString("UWOperator") == null)
            {
                this.UWOperator = null;
            }
            else
            {
                this.UWOperator = rs.getString("UWOperator").trim();
            }

            if (rs.getString("ConfOperator") == null)
            {
                this.ConfOperator = null;
            }
            else
            {
                this.ConfOperator = rs.getString("ConfOperator").trim();
            }

            this.ConfDate = rs.getDate("ConfDate");
            this.ConfTime = rs.getDate("ConfTime");
            this.ChgCessAmt = rs.getDouble("ChgCessAmt");
            this.ShRePrem = rs.getDouble("ShRePrem");
            this.ShReComm = rs.getDouble("ShReComm");
            this.CessStart = rs.getDate("CessStart");
            this.CessEnd = rs.getDate("CessEnd");
            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            if (rs.getString("SerialNo") == null)
            {
                this.SerialNo = null;
            }
            else
            {
                this.SerialNo = rs.getString("SerialNo").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("RiskCalSort") == null)
            {
                this.RiskCalSort = null;
            }
            else
            {
                this.RiskCalSort = rs.getString("RiskCalSort").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LREdorMainSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LREdorMainSchema getSchema()
    {
        LREdorMainSchema aLREdorMainSchema = new LREdorMainSchema();
        aLREdorMainSchema.setSchema(this);
        return aLREdorMainSchema;
    }

    public LREdorMainDB getDB()
    {
        LREdorMainDB aDBOper = new LREdorMainDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLREdorMain描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(EdorNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PolNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(EdorType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ReinsureCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ReinsurItem)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(InsuredYear) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(EdorAppNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ManageCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ContNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GrpPolNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InsuredNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InsuredName)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            PaytoDate))) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(SumPrem) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CalCode)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(ChgPrem) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(ChgAmnt) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(ChgGetAmnt) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(GetMoney) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(GetInterest) + SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            EdorValiDate))) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            EdorAppValiDate))) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            EdorAppDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(EdorState)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(UWState)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PubAccFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ApproveCode)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ApproveDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(UWOperator)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ConfOperator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(ConfDate))) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(ConfTime))) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(ChgCessAmt) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(ShRePrem) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(ShReComm) + SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            CessStart))) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(CessEnd))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SerialNo)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskCalSort));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLREdorMain>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            EdorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                    SysConst.PACKAGESPILTER);
            PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                   SysConst.PACKAGESPILTER);
            EdorType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            ReinsureCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                         SysConst.PACKAGESPILTER);
            ReinsurItem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                         SysConst.PACKAGESPILTER);
            InsuredYear = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 6, SysConst.PACKAGESPILTER))).intValue();
            EdorAppNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                       SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                       SysConst.PACKAGESPILTER);
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                    SysConst.PACKAGESPILTER);
            GrpPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                      SysConst.PACKAGESPILTER);
            InsuredNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                       SysConst.PACKAGESPILTER);
            InsuredName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                         SysConst.PACKAGESPILTER);
            PaytoDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 13, SysConst.PACKAGESPILTER));
            SumPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 14, SysConst.PACKAGESPILTER))).doubleValue();
            CalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                     SysConst.PACKAGESPILTER);
            ChgPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 16, SysConst.PACKAGESPILTER))).doubleValue();
            ChgAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 17, SysConst.PACKAGESPILTER))).doubleValue();
            ChgGetAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 18, SysConst.PACKAGESPILTER))).doubleValue();
            GetMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 19, SysConst.PACKAGESPILTER))).doubleValue();
            GetInterest = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 20, SysConst.PACKAGESPILTER))).doubleValue();
            EdorValiDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 21, SysConst.PACKAGESPILTER));
            EdorAppValiDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 22, SysConst.PACKAGESPILTER));
            EdorAppDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 23, SysConst.PACKAGESPILTER));
            EdorState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,
                                       SysConst.PACKAGESPILTER);
            UWState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,
                                     SysConst.PACKAGESPILTER);
            PubAccFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26,
                                        SysConst.PACKAGESPILTER);
            ApproveCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27,
                                         SysConst.PACKAGESPILTER);
            ApproveDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 28, SysConst.PACKAGESPILTER));
            UWOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29,
                                        SysConst.PACKAGESPILTER);
            ConfOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30,
                                          SysConst.PACKAGESPILTER);
            ConfDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 31, SysConst.PACKAGESPILTER));
            ConfTime = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 32, SysConst.PACKAGESPILTER));
            ChgCessAmt = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 33, SysConst.PACKAGESPILTER))).doubleValue();
            ShRePrem = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 34, SysConst.PACKAGESPILTER))).doubleValue();
            ShReComm = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 35, SysConst.PACKAGESPILTER))).doubleValue();
            CessStart = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 36, SysConst.PACKAGESPILTER));
            CessEnd = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 37, SysConst.PACKAGESPILTER));
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 39, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40,
                                      SysConst.PACKAGESPILTER);
            SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 42, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43,
                                        SysConst.PACKAGESPILTER);
            RiskCalSort = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44,
                                         SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LREdorMainSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("EdorNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(EdorNo));
        }
        if (FCode.equals("PolNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PolNo));
        }
        if (FCode.equals("EdorType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(EdorType));
        }
        if (FCode.equals("ReinsureCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ReinsureCom));
        }
        if (FCode.equals("ReinsurItem"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ReinsurItem));
        }
        if (FCode.equals("InsuredYear"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(InsuredYear));
        }
        if (FCode.equals("EdorAppNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(EdorAppNo));
        }
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ManageCom));
        }
        if (FCode.equals("ContNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ContNo));
        }
        if (FCode.equals("GrpPolNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GrpPolNo));
        }
        if (FCode.equals("InsuredNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(InsuredNo));
        }
        if (FCode.equals("InsuredName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(InsuredName));
        }
        if (FCode.equals("PaytoDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getPaytoDate()));
        }
        if (FCode.equals("SumPrem"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SumPrem));
        }
        if (FCode.equals("CalCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CalCode));
        }
        if (FCode.equals("ChgPrem"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ChgPrem));
        }
        if (FCode.equals("ChgAmnt"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ChgAmnt));
        }
        if (FCode.equals("ChgGetAmnt"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ChgGetAmnt));
        }
        if (FCode.equals("GetMoney"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetMoney));
        }
        if (FCode.equals("GetInterest"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetInterest));
        }
        if (FCode.equals("EdorValiDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getEdorValiDate()));
        }
        if (FCode.equals("EdorAppValiDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getEdorAppValiDate()));
        }
        if (FCode.equals("EdorAppDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getEdorAppDate()));
        }
        if (FCode.equals("EdorState"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(EdorState));
        }
        if (FCode.equals("UWState"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(UWState));
        }
        if (FCode.equals("PubAccFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PubAccFlag));
        }
        if (FCode.equals("ApproveCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ApproveCode));
        }
        if (FCode.equals("ApproveDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getApproveDate()));
        }
        if (FCode.equals("UWOperator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(UWOperator));
        }
        if (FCode.equals("ConfOperator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ConfOperator));
        }
        if (FCode.equals("ConfDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getConfDate()));
        }
        if (FCode.equals("ConfTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getConfTime()));
        }
        if (FCode.equals("ChgCessAmt"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ChgCessAmt));
        }
        if (FCode.equals("ShRePrem"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ShRePrem));
        }
        if (FCode.equals("ShReComm"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ShReComm));
        }
        if (FCode.equals("CessStart"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getCessStart()));
        }
        if (FCode.equals("CessEnd"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getCessEnd()));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("SerialNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SerialNo));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (FCode.equals("RiskCalSort"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskCalSort));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(EdorNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(PolNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(EdorType);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(ReinsureCom);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(ReinsurItem);
                break;
            case 5:
                strFieldValue = String.valueOf(InsuredYear);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(EdorAppNo);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(GrpPolNo);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(InsuredNo);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(InsuredName);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getPaytoDate()));
                break;
            case 13:
                strFieldValue = String.valueOf(SumPrem);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(CalCode);
                break;
            case 15:
                strFieldValue = String.valueOf(ChgPrem);
                break;
            case 16:
                strFieldValue = String.valueOf(ChgAmnt);
                break;
            case 17:
                strFieldValue = String.valueOf(ChgGetAmnt);
                break;
            case 18:
                strFieldValue = String.valueOf(GetMoney);
                break;
            case 19:
                strFieldValue = String.valueOf(GetInterest);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getEdorValiDate()));
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getEdorAppValiDate()));
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getEdorAppDate()));
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(EdorState);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(UWState);
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(PubAccFlag);
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(ApproveCode);
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getApproveDate()));
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(UWOperator);
                break;
            case 29:
                strFieldValue = StrTool.GBKToUnicode(ConfOperator);
                break;
            case 30:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getConfDate()));
                break;
            case 31:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getConfTime()));
                break;
            case 32:
                strFieldValue = String.valueOf(ChgCessAmt);
                break;
            case 33:
                strFieldValue = String.valueOf(ShRePrem);
                break;
            case 34:
                strFieldValue = String.valueOf(ShReComm);
                break;
            case 35:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getCessStart()));
                break;
            case 36:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getCessEnd()));
                break;
            case 37:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 38:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 39:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 40:
                strFieldValue = StrTool.GBKToUnicode(SerialNo);
                break;
            case 41:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 42:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 43:
                strFieldValue = StrTool.GBKToUnicode(RiskCalSort);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("EdorNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorNo = FValue.trim();
            }
            else
            {
                EdorNo = null;
            }
        }
        if (FCode.equals("PolNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
            {
                PolNo = null;
            }
        }
        if (FCode.equals("EdorType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorType = FValue.trim();
            }
            else
            {
                EdorType = null;
            }
        }
        if (FCode.equals("ReinsureCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ReinsureCom = FValue.trim();
            }
            else
            {
                ReinsureCom = null;
            }
        }
        if (FCode.equals("ReinsurItem"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ReinsurItem = FValue.trim();
            }
            else
            {
                ReinsurItem = null;
            }
        }
        if (FCode.equals("InsuredYear"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                InsuredYear = i;
            }
        }
        if (FCode.equals("EdorAppNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorAppNo = FValue.trim();
            }
            else
            {
                EdorAppNo = null;
            }
        }
        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("ContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
            {
                ContNo = null;
            }
        }
        if (FCode.equals("GrpPolNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpPolNo = FValue.trim();
            }
            else
            {
                GrpPolNo = null;
            }
        }
        if (FCode.equals("InsuredNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InsuredNo = FValue.trim();
            }
            else
            {
                InsuredNo = null;
            }
        }
        if (FCode.equals("InsuredName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InsuredName = FValue.trim();
            }
            else
            {
                InsuredName = null;
            }
        }
        if (FCode.equals("PaytoDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PaytoDate = fDate.getDate(FValue);
            }
            else
            {
                PaytoDate = null;
            }
        }
        if (FCode.equals("SumPrem"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                SumPrem = d;
            }
        }
        if (FCode.equals("CalCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CalCode = FValue.trim();
            }
            else
            {
                CalCode = null;
            }
        }
        if (FCode.equals("ChgPrem"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                ChgPrem = d;
            }
        }
        if (FCode.equals("ChgAmnt"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                ChgAmnt = d;
            }
        }
        if (FCode.equals("ChgGetAmnt"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                ChgGetAmnt = d;
            }
        }
        if (FCode.equals("GetMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                GetMoney = d;
            }
        }
        if (FCode.equals("GetInterest"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                GetInterest = d;
            }
        }
        if (FCode.equals("EdorValiDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorValiDate = fDate.getDate(FValue);
            }
            else
            {
                EdorValiDate = null;
            }
        }
        if (FCode.equals("EdorAppValiDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorAppValiDate = fDate.getDate(FValue);
            }
            else
            {
                EdorAppValiDate = null;
            }
        }
        if (FCode.equals("EdorAppDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorAppDate = fDate.getDate(FValue);
            }
            else
            {
                EdorAppDate = null;
            }
        }
        if (FCode.equals("EdorState"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorState = FValue.trim();
            }
            else
            {
                EdorState = null;
            }
        }
        if (FCode.equals("UWState"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UWState = FValue.trim();
            }
            else
            {
                UWState = null;
            }
        }
        if (FCode.equals("PubAccFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PubAccFlag = FValue.trim();
            }
            else
            {
                PubAccFlag = null;
            }
        }
        if (FCode.equals("ApproveCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ApproveCode = FValue.trim();
            }
            else
            {
                ApproveCode = null;
            }
        }
        if (FCode.equals("ApproveDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ApproveDate = fDate.getDate(FValue);
            }
            else
            {
                ApproveDate = null;
            }
        }
        if (FCode.equals("UWOperator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UWOperator = FValue.trim();
            }
            else
            {
                UWOperator = null;
            }
        }
        if (FCode.equals("ConfOperator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ConfOperator = FValue.trim();
            }
            else
            {
                ConfOperator = null;
            }
        }
        if (FCode.equals("ConfDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ConfDate = fDate.getDate(FValue);
            }
            else
            {
                ConfDate = null;
            }
        }
        if (FCode.equals("ConfTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ConfTime = fDate.getDate(FValue);
            }
            else
            {
                ConfTime = null;
            }
        }
        if (FCode.equals("ChgCessAmt"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                ChgCessAmt = d;
            }
        }
        if (FCode.equals("ShRePrem"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                ShRePrem = d;
            }
        }
        if (FCode.equals("ShReComm"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                ShReComm = d;
            }
        }
        if (FCode.equals("CessStart"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CessStart = fDate.getDate(FValue);
            }
            else
            {
                CessStart = null;
            }
        }
        if (FCode.equals("CessEnd"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CessEnd = fDate.getDate(FValue);
            }
            else
            {
                CessEnd = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("SerialNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
            {
                SerialNo = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        if (FCode.equals("RiskCalSort"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCalSort = FValue.trim();
            }
            else
            {
                RiskCalSort = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LREdorMainSchema other = (LREdorMainSchema) otherObject;
        return
                EdorNo.equals(other.getEdorNo())
                && PolNo.equals(other.getPolNo())
                && EdorType.equals(other.getEdorType())
                && ReinsureCom.equals(other.getReinsureCom())
                && ReinsurItem.equals(other.getReinsurItem())
                && InsuredYear == other.getInsuredYear()
                && EdorAppNo.equals(other.getEdorAppNo())
                && ManageCom.equals(other.getManageCom())
                && ContNo.equals(other.getContNo())
                && GrpPolNo.equals(other.getGrpPolNo())
                && InsuredNo.equals(other.getInsuredNo())
                && InsuredName.equals(other.getInsuredName())
                && fDate.getString(PaytoDate).equals(other.getPaytoDate())
                && SumPrem == other.getSumPrem()
                && CalCode.equals(other.getCalCode())
                && ChgPrem == other.getChgPrem()
                && ChgAmnt == other.getChgAmnt()
                && ChgGetAmnt == other.getChgGetAmnt()
                && GetMoney == other.getGetMoney()
                && GetInterest == other.getGetInterest()
                && fDate.getString(EdorValiDate).equals(other.getEdorValiDate())
                &&
                fDate.getString(EdorAppValiDate).equals(other.
                getEdorAppValiDate())
                && fDate.getString(EdorAppDate).equals(other.getEdorAppDate())
                && EdorState.equals(other.getEdorState())
                && UWState.equals(other.getUWState())
                && PubAccFlag.equals(other.getPubAccFlag())
                && ApproveCode.equals(other.getApproveCode())
                && fDate.getString(ApproveDate).equals(other.getApproveDate())
                && UWOperator.equals(other.getUWOperator())
                && ConfOperator.equals(other.getConfOperator())
                && fDate.getString(ConfDate).equals(other.getConfDate())
                && fDate.getString(ConfTime).equals(other.getConfTime())
                && ChgCessAmt == other.getChgCessAmt()
                && ShRePrem == other.getShRePrem()
                && ShReComm == other.getShReComm()
                && fDate.getString(CessStart).equals(other.getCessStart())
                && fDate.getString(CessEnd).equals(other.getCessEnd())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && SerialNo.equals(other.getSerialNo())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && RiskCalSort.equals(other.getRiskCalSort());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("EdorNo"))
        {
            return 0;
        }
        if (strFieldName.equals("PolNo"))
        {
            return 1;
        }
        if (strFieldName.equals("EdorType"))
        {
            return 2;
        }
        if (strFieldName.equals("ReinsureCom"))
        {
            return 3;
        }
        if (strFieldName.equals("ReinsurItem"))
        {
            return 4;
        }
        if (strFieldName.equals("InsuredYear"))
        {
            return 5;
        }
        if (strFieldName.equals("EdorAppNo"))
        {
            return 6;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return 7;
        }
        if (strFieldName.equals("ContNo"))
        {
            return 8;
        }
        if (strFieldName.equals("GrpPolNo"))
        {
            return 9;
        }
        if (strFieldName.equals("InsuredNo"))
        {
            return 10;
        }
        if (strFieldName.equals("InsuredName"))
        {
            return 11;
        }
        if (strFieldName.equals("PaytoDate"))
        {
            return 12;
        }
        if (strFieldName.equals("SumPrem"))
        {
            return 13;
        }
        if (strFieldName.equals("CalCode"))
        {
            return 14;
        }
        if (strFieldName.equals("ChgPrem"))
        {
            return 15;
        }
        if (strFieldName.equals("ChgAmnt"))
        {
            return 16;
        }
        if (strFieldName.equals("ChgGetAmnt"))
        {
            return 17;
        }
        if (strFieldName.equals("GetMoney"))
        {
            return 18;
        }
        if (strFieldName.equals("GetInterest"))
        {
            return 19;
        }
        if (strFieldName.equals("EdorValiDate"))
        {
            return 20;
        }
        if (strFieldName.equals("EdorAppValiDate"))
        {
            return 21;
        }
        if (strFieldName.equals("EdorAppDate"))
        {
            return 22;
        }
        if (strFieldName.equals("EdorState"))
        {
            return 23;
        }
        if (strFieldName.equals("UWState"))
        {
            return 24;
        }
        if (strFieldName.equals("PubAccFlag"))
        {
            return 25;
        }
        if (strFieldName.equals("ApproveCode"))
        {
            return 26;
        }
        if (strFieldName.equals("ApproveDate"))
        {
            return 27;
        }
        if (strFieldName.equals("UWOperator"))
        {
            return 28;
        }
        if (strFieldName.equals("ConfOperator"))
        {
            return 29;
        }
        if (strFieldName.equals("ConfDate"))
        {
            return 30;
        }
        if (strFieldName.equals("ConfTime"))
        {
            return 31;
        }
        if (strFieldName.equals("ChgCessAmt"))
        {
            return 32;
        }
        if (strFieldName.equals("ShRePrem"))
        {
            return 33;
        }
        if (strFieldName.equals("ShReComm"))
        {
            return 34;
        }
        if (strFieldName.equals("CessStart"))
        {
            return 35;
        }
        if (strFieldName.equals("CessEnd"))
        {
            return 36;
        }
        if (strFieldName.equals("Operator"))
        {
            return 37;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 38;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 39;
        }
        if (strFieldName.equals("SerialNo"))
        {
            return 40;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 41;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 42;
        }
        if (strFieldName.equals("RiskCalSort"))
        {
            return 43;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "EdorNo";
                break;
            case 1:
                strFieldName = "PolNo";
                break;
            case 2:
                strFieldName = "EdorType";
                break;
            case 3:
                strFieldName = "ReinsureCom";
                break;
            case 4:
                strFieldName = "ReinsurItem";
                break;
            case 5:
                strFieldName = "InsuredYear";
                break;
            case 6:
                strFieldName = "EdorAppNo";
                break;
            case 7:
                strFieldName = "ManageCom";
                break;
            case 8:
                strFieldName = "ContNo";
                break;
            case 9:
                strFieldName = "GrpPolNo";
                break;
            case 10:
                strFieldName = "InsuredNo";
                break;
            case 11:
                strFieldName = "InsuredName";
                break;
            case 12:
                strFieldName = "PaytoDate";
                break;
            case 13:
                strFieldName = "SumPrem";
                break;
            case 14:
                strFieldName = "CalCode";
                break;
            case 15:
                strFieldName = "ChgPrem";
                break;
            case 16:
                strFieldName = "ChgAmnt";
                break;
            case 17:
                strFieldName = "ChgGetAmnt";
                break;
            case 18:
                strFieldName = "GetMoney";
                break;
            case 19:
                strFieldName = "GetInterest";
                break;
            case 20:
                strFieldName = "EdorValiDate";
                break;
            case 21:
                strFieldName = "EdorAppValiDate";
                break;
            case 22:
                strFieldName = "EdorAppDate";
                break;
            case 23:
                strFieldName = "EdorState";
                break;
            case 24:
                strFieldName = "UWState";
                break;
            case 25:
                strFieldName = "PubAccFlag";
                break;
            case 26:
                strFieldName = "ApproveCode";
                break;
            case 27:
                strFieldName = "ApproveDate";
                break;
            case 28:
                strFieldName = "UWOperator";
                break;
            case 29:
                strFieldName = "ConfOperator";
                break;
            case 30:
                strFieldName = "ConfDate";
                break;
            case 31:
                strFieldName = "ConfTime";
                break;
            case 32:
                strFieldName = "ChgCessAmt";
                break;
            case 33:
                strFieldName = "ShRePrem";
                break;
            case 34:
                strFieldName = "ShReComm";
                break;
            case 35:
                strFieldName = "CessStart";
                break;
            case 36:
                strFieldName = "CessEnd";
                break;
            case 37:
                strFieldName = "Operator";
                break;
            case 38:
                strFieldName = "MakeDate";
                break;
            case 39:
                strFieldName = "MakeTime";
                break;
            case 40:
                strFieldName = "SerialNo";
                break;
            case 41:
                strFieldName = "ModifyDate";
                break;
            case 42:
                strFieldName = "ModifyTime";
                break;
            case 43:
                strFieldName = "RiskCalSort";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("EdorNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PolNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EdorType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ReinsureCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ReinsurItem"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsuredYear"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("EdorAppNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpPolNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsuredNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsuredName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PaytoDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("SumPrem"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("CalCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ChgPrem"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("ChgAmnt"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("ChgGetAmnt"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("GetMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("GetInterest"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("EdorValiDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("EdorAppValiDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("EdorAppDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("EdorState"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UWState"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PubAccFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ApproveCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ApproveDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("UWOperator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ConfOperator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ConfDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ConfTime"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ChgCessAmt"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("ShRePrem"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("ShReComm"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("CessStart"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("CessEnd"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SerialNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskCalSort"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_INT;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 13:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 16:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 17:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 18:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 19:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 20:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 21:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 22:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 23:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 24:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 25:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 26:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 27:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 28:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 29:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 30:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 31:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 32:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 33:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 34:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 35:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 36:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 37:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 38:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 39:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 40:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 41:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 42:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 43:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
