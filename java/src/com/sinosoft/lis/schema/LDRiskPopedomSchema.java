/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LDRiskPopedomDB;

/*
 * <p>ClassName: LDRiskPopedomSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-01-05
 */
public class LDRiskPopedomSchema implements Schema, Cloneable {
    // @Field
    /** 险种代码 */
    private String RiskCode;
    /** 权限类型 */
    private String PopedomType;
    /** 最大值 */
    private String MaxValue;
    /** 最小值 */
    private String MinValue;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 管理机构 */
    private String ManageCom;

    public static final int FIELDNUM = 10; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LDRiskPopedomSchema() {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "RiskCode";
        pk[1] = "PopedomType";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LDRiskPopedomSchema cloned = (LDRiskPopedomSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getRiskCode() {
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }

    public String getPopedomType() {
        return PopedomType;
    }

    public void setPopedomType(String aPopedomType) {
        PopedomType = aPopedomType;
    }

    public String getMaxValue() {
        return MaxValue;
    }

    public void setMaxValue(String aMaxValue) {
        MaxValue = aMaxValue;
    }

    public String getMinValue() {
        return MinValue;
    }

    public void setMinValue(String aMinValue) {
        MinValue = aMinValue;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }

    /**
     * 使用另外一个 LDRiskPopedomSchema 对象给 Schema 赋值
     * @param: aLDRiskPopedomSchema LDRiskPopedomSchema
     **/
    public void setSchema(LDRiskPopedomSchema aLDRiskPopedomSchema) {
        this.RiskCode = aLDRiskPopedomSchema.getRiskCode();
        this.PopedomType = aLDRiskPopedomSchema.getPopedomType();
        this.MaxValue = aLDRiskPopedomSchema.getMaxValue();
        this.MinValue = aLDRiskPopedomSchema.getMinValue();
        this.Operator = aLDRiskPopedomSchema.getOperator();
        this.MakeDate = fDate.getDate(aLDRiskPopedomSchema.getMakeDate());
        this.MakeTime = aLDRiskPopedomSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLDRiskPopedomSchema.getModifyDate());
        this.ModifyTime = aLDRiskPopedomSchema.getModifyTime();
        this.ManageCom = aLDRiskPopedomSchema.getManageCom();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("RiskCode") == null) {
                this.RiskCode = null;
            } else {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("PopedomType") == null) {
                this.PopedomType = null;
            } else {
                this.PopedomType = rs.getString("PopedomType").trim();
            }

            if (rs.getString("MaxValue") == null) {
                this.MaxValue = null;
            } else {
                this.MaxValue = rs.getString("MaxValue").trim();
            }

            if (rs.getString("MinValue") == null) {
                this.MinValue = null;
            } else {
                this.MinValue = rs.getString("MinValue").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("ManageCom") == null) {
                this.ManageCom = null;
            } else {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LDRiskPopedom表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDRiskPopedomSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LDRiskPopedomSchema getSchema() {
        LDRiskPopedomSchema aLDRiskPopedomSchema = new LDRiskPopedomSchema();
        aLDRiskPopedomSchema.setSchema(this);
        return aLDRiskPopedomSchema;
    }

    public LDRiskPopedomDB getDB() {
        LDRiskPopedomDB aDBOper = new LDRiskPopedomDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDRiskPopedom描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(RiskCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PopedomType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MaxValue));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MinValue));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDRiskPopedom>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            PopedomType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                         SysConst.PACKAGESPILTER);
            MaxValue = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            MinValue = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                      SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 6, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 8, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                        SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                       SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDRiskPopedomSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("PopedomType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PopedomType));
        }
        if (FCode.equalsIgnoreCase("MaxValue")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MaxValue));
        }
        if (FCode.equalsIgnoreCase("MinValue")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MinValue));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(RiskCode);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(PopedomType);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(MaxValue);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(MinValue);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(ManageCom);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("RiskCode")) {
            if (FValue != null && !FValue.equals("")) {
                RiskCode = FValue.trim();
            } else {
                RiskCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("PopedomType")) {
            if (FValue != null && !FValue.equals("")) {
                PopedomType = FValue.trim();
            } else {
                PopedomType = null;
            }
        }
        if (FCode.equalsIgnoreCase("MaxValue")) {
            if (FValue != null && !FValue.equals("")) {
                MaxValue = FValue.trim();
            } else {
                MaxValue = null;
            }
        }
        if (FCode.equalsIgnoreCase("MinValue")) {
            if (FValue != null && !FValue.equals("")) {
                MinValue = FValue.trim();
            } else {
                MinValue = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ManageCom = FValue.trim();
            } else {
                ManageCom = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LDRiskPopedomSchema other = (LDRiskPopedomSchema) otherObject;
        return
                RiskCode.equals(other.getRiskCode())
                && PopedomType.equals(other.getPopedomType())
                && MaxValue.equals(other.getMaxValue())
                && MinValue.equals(other.getMinValue())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && ManageCom.equals(other.getManageCom());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("RiskCode")) {
            return 0;
        }
        if (strFieldName.equals("PopedomType")) {
            return 1;
        }
        if (strFieldName.equals("MaxValue")) {
            return 2;
        }
        if (strFieldName.equals("MinValue")) {
            return 3;
        }
        if (strFieldName.equals("Operator")) {
            return 4;
        }
        if (strFieldName.equals("MakeDate")) {
            return 5;
        }
        if (strFieldName.equals("MakeTime")) {
            return 6;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 7;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 8;
        }
        if (strFieldName.equals("ManageCom")) {
            return 9;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "RiskCode";
            break;
        case 1:
            strFieldName = "PopedomType";
            break;
        case 2:
            strFieldName = "MaxValue";
            break;
        case 3:
            strFieldName = "MinValue";
            break;
        case 4:
            strFieldName = "Operator";
            break;
        case 5:
            strFieldName = "MakeDate";
            break;
        case 6:
            strFieldName = "MakeTime";
            break;
        case 7:
            strFieldName = "ModifyDate";
            break;
        case 8:
            strFieldName = "ModifyTime";
            break;
        case 9:
            strFieldName = "ManageCom";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("RiskCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PopedomType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MaxValue")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MinValue")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
