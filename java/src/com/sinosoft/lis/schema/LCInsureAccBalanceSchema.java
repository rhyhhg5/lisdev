/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LCInsureAccBalanceDB;

/*
 * <p>ClassName: LCInsureAccBalanceSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 保全代理人信息
 * @CreateDate：2015-04-01
 */
public class LCInsureAccBalanceSchema implements Schema, Cloneable
{
	// @Field
	/** 结算序号 */
	private String SequenceNo;
	/** 合同号码 */
	private String ContNo;
	/** 险种号码 */
	private String PolNo;
	/** 保险帐户号码 */
	private String InsuAccNo;
	/** 结算次数 */
	private int BalaCount;
	/** 应结算日 */
	private Date DueBalaDate;
	/** 应结算时间 */
	private String DueBalaTime;
	/** 结算操作日期 */
	private Date RunDate;
	/** 结算操作时间 */
	private String RunTime;
	/** 结算年度 */
	private int PolYear;
	/** 结算月度 */
	private int PolMonth;
	/** 结算前账户余额 */
	private double InsuAccBalaBefore;
	/** 结算后账户余额 */
	private double InsuAccBalaAfter;
	/** 保证账户价值 */
	private double InsuAccBalaGurat;
	/** 打印次数 */
	private int PrintCount;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 修改日期 */
	private Date ModifyDate;
	/** 修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 20;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LCInsureAccBalanceSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "SequenceNo";
		pk[1] = "PolNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LCInsureAccBalanceSchema cloned = (LCInsureAccBalanceSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getSequenceNo()
	{
		return SequenceNo;
	}
	public void setSequenceNo(String aSequenceNo)
	{
		SequenceNo = aSequenceNo;
	}
	public String getContNo()
	{
		return ContNo;
	}
	public void setContNo(String aContNo)
	{
		ContNo = aContNo;
	}
	public String getPolNo()
	{
		return PolNo;
	}
	public void setPolNo(String aPolNo)
	{
		PolNo = aPolNo;
	}
	public String getInsuAccNo()
	{
		return InsuAccNo;
	}
	public void setInsuAccNo(String aInsuAccNo)
	{
		InsuAccNo = aInsuAccNo;
	}
	public int getBalaCount()
	{
		return BalaCount;
	}
	public void setBalaCount(int aBalaCount)
	{
		BalaCount = aBalaCount;
	}
	public void setBalaCount(String aBalaCount)
	{
		if (aBalaCount != null && !aBalaCount.equals(""))
		{
			Integer tInteger = new Integer(aBalaCount);
			int i = tInteger.intValue();
			BalaCount = i;
		}
	}

	public String getDueBalaDate()
	{
		if( DueBalaDate != null )
			return fDate.getString(DueBalaDate);
		else
			return null;
	}
	public void setDueBalaDate(Date aDueBalaDate)
	{
		DueBalaDate = aDueBalaDate;
	}
	public void setDueBalaDate(String aDueBalaDate)
	{
		if (aDueBalaDate != null && !aDueBalaDate.equals("") )
		{
			DueBalaDate = fDate.getDate( aDueBalaDate );
		}
		else
			DueBalaDate = null;
	}

	public String getDueBalaTime()
	{
		return DueBalaTime;
	}
	public void setDueBalaTime(String aDueBalaTime)
	{
		DueBalaTime = aDueBalaTime;
	}
	public String getRunDate()
	{
		if( RunDate != null )
			return fDate.getString(RunDate);
		else
			return null;
	}
	public void setRunDate(Date aRunDate)
	{
		RunDate = aRunDate;
	}
	public void setRunDate(String aRunDate)
	{
		if (aRunDate != null && !aRunDate.equals("") )
		{
			RunDate = fDate.getDate( aRunDate );
		}
		else
			RunDate = null;
	}

	public String getRunTime()
	{
		return RunTime;
	}
	public void setRunTime(String aRunTime)
	{
		RunTime = aRunTime;
	}
	public int getPolYear()
	{
		return PolYear;
	}
	public void setPolYear(int aPolYear)
	{
		PolYear = aPolYear;
	}
	public void setPolYear(String aPolYear)
	{
		if (aPolYear != null && !aPolYear.equals(""))
		{
			Integer tInteger = new Integer(aPolYear);
			int i = tInteger.intValue();
			PolYear = i;
		}
	}

	public int getPolMonth()
	{
		return PolMonth;
	}
	public void setPolMonth(int aPolMonth)
	{
		PolMonth = aPolMonth;
	}
	public void setPolMonth(String aPolMonth)
	{
		if (aPolMonth != null && !aPolMonth.equals(""))
		{
			Integer tInteger = new Integer(aPolMonth);
			int i = tInteger.intValue();
			PolMonth = i;
		}
	}

	public double getInsuAccBalaBefore()
	{
		return InsuAccBalaBefore;
	}
	public void setInsuAccBalaBefore(double aInsuAccBalaBefore)
	{
		InsuAccBalaBefore = Arith.round(aInsuAccBalaBefore,6);
	}
	public void setInsuAccBalaBefore(String aInsuAccBalaBefore)
	{
		if (aInsuAccBalaBefore != null && !aInsuAccBalaBefore.equals(""))
		{
			Double tDouble = new Double(aInsuAccBalaBefore);
			double d = tDouble.doubleValue();
                InsuAccBalaBefore = Arith.round(d,6);
		}
	}

	public double getInsuAccBalaAfter()
	{
		return InsuAccBalaAfter;
	}
	public void setInsuAccBalaAfter(double aInsuAccBalaAfter)
	{
		InsuAccBalaAfter = Arith.round(aInsuAccBalaAfter,6);
	}
	public void setInsuAccBalaAfter(String aInsuAccBalaAfter)
	{
		if (aInsuAccBalaAfter != null && !aInsuAccBalaAfter.equals(""))
		{
			Double tDouble = new Double(aInsuAccBalaAfter);
			double d = tDouble.doubleValue();
                InsuAccBalaAfter = Arith.round(d,6);
		}
	}

	public double getInsuAccBalaGurat()
	{
		return InsuAccBalaGurat;
	}
	public void setInsuAccBalaGurat(double aInsuAccBalaGurat)
	{
		InsuAccBalaGurat = Arith.round(aInsuAccBalaGurat,6);
	}
	public void setInsuAccBalaGurat(String aInsuAccBalaGurat)
	{
		if (aInsuAccBalaGurat != null && !aInsuAccBalaGurat.equals(""))
		{
			Double tDouble = new Double(aInsuAccBalaGurat);
			double d = tDouble.doubleValue();
                InsuAccBalaGurat = Arith.round(d,6);
		}
	}

	public int getPrintCount()
	{
		return PrintCount;
	}
	public void setPrintCount(int aPrintCount)
	{
		PrintCount = aPrintCount;
	}
	public void setPrintCount(String aPrintCount)
	{
		if (aPrintCount != null && !aPrintCount.equals(""))
		{
			Integer tInteger = new Integer(aPrintCount);
			int i = tInteger.intValue();
			PrintCount = i;
		}
	}

	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LCInsureAccBalanceSchema 对象给 Schema 赋值
	* @param: aLCInsureAccBalanceSchema LCInsureAccBalanceSchema
	**/
	public void setSchema(LCInsureAccBalanceSchema aLCInsureAccBalanceSchema)
	{
		this.SequenceNo = aLCInsureAccBalanceSchema.getSequenceNo();
		this.ContNo = aLCInsureAccBalanceSchema.getContNo();
		this.PolNo = aLCInsureAccBalanceSchema.getPolNo();
		this.InsuAccNo = aLCInsureAccBalanceSchema.getInsuAccNo();
		this.BalaCount = aLCInsureAccBalanceSchema.getBalaCount();
		this.DueBalaDate = fDate.getDate( aLCInsureAccBalanceSchema.getDueBalaDate());
		this.DueBalaTime = aLCInsureAccBalanceSchema.getDueBalaTime();
		this.RunDate = fDate.getDate( aLCInsureAccBalanceSchema.getRunDate());
		this.RunTime = aLCInsureAccBalanceSchema.getRunTime();
		this.PolYear = aLCInsureAccBalanceSchema.getPolYear();
		this.PolMonth = aLCInsureAccBalanceSchema.getPolMonth();
		this.InsuAccBalaBefore = aLCInsureAccBalanceSchema.getInsuAccBalaBefore();
		this.InsuAccBalaAfter = aLCInsureAccBalanceSchema.getInsuAccBalaAfter();
		this.InsuAccBalaGurat = aLCInsureAccBalanceSchema.getInsuAccBalaGurat();
		this.PrintCount = aLCInsureAccBalanceSchema.getPrintCount();
		this.Operator = aLCInsureAccBalanceSchema.getOperator();
		this.MakeDate = fDate.getDate( aLCInsureAccBalanceSchema.getMakeDate());
		this.MakeTime = aLCInsureAccBalanceSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLCInsureAccBalanceSchema.getModifyDate());
		this.ModifyTime = aLCInsureAccBalanceSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("SequenceNo") == null )
				this.SequenceNo = null;
			else
				this.SequenceNo = rs.getString("SequenceNo").trim();

			if( rs.getString("ContNo") == null )
				this.ContNo = null;
			else
				this.ContNo = rs.getString("ContNo").trim();

			if( rs.getString("PolNo") == null )
				this.PolNo = null;
			else
				this.PolNo = rs.getString("PolNo").trim();

			if( rs.getString("InsuAccNo") == null )
				this.InsuAccNo = null;
			else
				this.InsuAccNo = rs.getString("InsuAccNo").trim();

			this.BalaCount = rs.getInt("BalaCount");
			this.DueBalaDate = rs.getDate("DueBalaDate");
			if( rs.getString("DueBalaTime") == null )
				this.DueBalaTime = null;
			else
				this.DueBalaTime = rs.getString("DueBalaTime").trim();

			this.RunDate = rs.getDate("RunDate");
			if( rs.getString("RunTime") == null )
				this.RunTime = null;
			else
				this.RunTime = rs.getString("RunTime").trim();

			this.PolYear = rs.getInt("PolYear");
			this.PolMonth = rs.getInt("PolMonth");
			this.InsuAccBalaBefore = rs.getDouble("InsuAccBalaBefore");
			this.InsuAccBalaAfter = rs.getDouble("InsuAccBalaAfter");
			this.InsuAccBalaGurat = rs.getDouble("InsuAccBalaGurat");
			this.PrintCount = rs.getInt("PrintCount");
			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LCInsureAccBalance表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCInsureAccBalanceSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LCInsureAccBalanceSchema getSchema()
	{
		LCInsureAccBalanceSchema aLCInsureAccBalanceSchema = new LCInsureAccBalanceSchema();
		aLCInsureAccBalanceSchema.setSchema(this);
		return aLCInsureAccBalanceSchema;
	}

	public LCInsureAccBalanceDB getDB()
	{
		LCInsureAccBalanceDB aDBOper = new LCInsureAccBalanceDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCInsureAccBalance描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(SequenceNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PolNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(BalaCount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( DueBalaDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DueBalaTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( RunDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RunTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PolYear));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PolMonth));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(InsuAccBalaBefore));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(InsuAccBalaAfter));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(InsuAccBalaGurat));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PrintCount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCInsureAccBalance>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			SequenceNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			InsuAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			BalaCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,5,SysConst.PACKAGESPILTER))).intValue();
			DueBalaDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,SysConst.PACKAGESPILTER));
			DueBalaTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			RunDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,SysConst.PACKAGESPILTER));
			RunTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			PolYear= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,10,SysConst.PACKAGESPILTER))).intValue();
			PolMonth= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,11,SysConst.PACKAGESPILTER))).intValue();
			InsuAccBalaBefore = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,12,SysConst.PACKAGESPILTER))).doubleValue();
			InsuAccBalaAfter = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,13,SysConst.PACKAGESPILTER))).doubleValue();
			InsuAccBalaGurat = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,14,SysConst.PACKAGESPILTER))).doubleValue();
			PrintCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,15,SysConst.PACKAGESPILTER))).intValue();
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCInsureAccBalanceSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("SequenceNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SequenceNo));
		}
		if (FCode.equals("ContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
		}
		if (FCode.equals("PolNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
		}
		if (FCode.equals("InsuAccNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuAccNo));
		}
		if (FCode.equals("BalaCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BalaCount));
		}
		if (FCode.equals("DueBalaDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getDueBalaDate()));
		}
		if (FCode.equals("DueBalaTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DueBalaTime));
		}
		if (FCode.equals("RunDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getRunDate()));
		}
		if (FCode.equals("RunTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RunTime));
		}
		if (FCode.equals("PolYear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolYear));
		}
		if (FCode.equals("PolMonth"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolMonth));
		}
		if (FCode.equals("InsuAccBalaBefore"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuAccBalaBefore));
		}
		if (FCode.equals("InsuAccBalaAfter"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuAccBalaAfter));
		}
		if (FCode.equals("InsuAccBalaGurat"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuAccBalaGurat));
		}
		if (FCode.equals("PrintCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrintCount));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(SequenceNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(ContNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(PolNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(InsuAccNo);
				break;
			case 4:
				strFieldValue = String.valueOf(BalaCount);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getDueBalaDate()));
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(DueBalaTime);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getRunDate()));
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(RunTime);
				break;
			case 9:
				strFieldValue = String.valueOf(PolYear);
				break;
			case 10:
				strFieldValue = String.valueOf(PolMonth);
				break;
			case 11:
				strFieldValue = String.valueOf(InsuAccBalaBefore);
				break;
			case 12:
				strFieldValue = String.valueOf(InsuAccBalaAfter);
				break;
			case 13:
				strFieldValue = String.valueOf(InsuAccBalaGurat);
				break;
			case 14:
				strFieldValue = String.valueOf(PrintCount);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("SequenceNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SequenceNo = FValue.trim();
			}
			else
				SequenceNo = null;
		}
		if (FCode.equalsIgnoreCase("ContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContNo = FValue.trim();
			}
			else
				ContNo = null;
		}
		if (FCode.equalsIgnoreCase("PolNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PolNo = FValue.trim();
			}
			else
				PolNo = null;
		}
		if (FCode.equalsIgnoreCase("InsuAccNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuAccNo = FValue.trim();
			}
			else
				InsuAccNo = null;
		}
		if (FCode.equalsIgnoreCase("BalaCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				BalaCount = i;
			}
		}
		if (FCode.equalsIgnoreCase("DueBalaDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				DueBalaDate = fDate.getDate( FValue );
			}
			else
				DueBalaDate = null;
		}
		if (FCode.equalsIgnoreCase("DueBalaTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DueBalaTime = FValue.trim();
			}
			else
				DueBalaTime = null;
		}
		if (FCode.equalsIgnoreCase("RunDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				RunDate = fDate.getDate( FValue );
			}
			else
				RunDate = null;
		}
		if (FCode.equalsIgnoreCase("RunTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RunTime = FValue.trim();
			}
			else
				RunTime = null;
		}
		if (FCode.equalsIgnoreCase("PolYear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PolYear = i;
			}
		}
		if (FCode.equalsIgnoreCase("PolMonth"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PolMonth = i;
			}
		}
		if (FCode.equalsIgnoreCase("InsuAccBalaBefore"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				InsuAccBalaBefore = d;
			}
		}
		if (FCode.equalsIgnoreCase("InsuAccBalaAfter"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				InsuAccBalaAfter = d;
			}
		}
		if (FCode.equalsIgnoreCase("InsuAccBalaGurat"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				InsuAccBalaGurat = d;
			}
		}
		if (FCode.equalsIgnoreCase("PrintCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PrintCount = i;
			}
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LCInsureAccBalanceSchema other = (LCInsureAccBalanceSchema)otherObject;
		return
			(SequenceNo == null ? other.getSequenceNo() == null : SequenceNo.equals(other.getSequenceNo()))
			&& (ContNo == null ? other.getContNo() == null : ContNo.equals(other.getContNo()))
			&& (PolNo == null ? other.getPolNo() == null : PolNo.equals(other.getPolNo()))
			&& (InsuAccNo == null ? other.getInsuAccNo() == null : InsuAccNo.equals(other.getInsuAccNo()))
			&& BalaCount == other.getBalaCount()
			&& (DueBalaDate == null ? other.getDueBalaDate() == null : fDate.getString(DueBalaDate).equals(other.getDueBalaDate()))
			&& (DueBalaTime == null ? other.getDueBalaTime() == null : DueBalaTime.equals(other.getDueBalaTime()))
			&& (RunDate == null ? other.getRunDate() == null : fDate.getString(RunDate).equals(other.getRunDate()))
			&& (RunTime == null ? other.getRunTime() == null : RunTime.equals(other.getRunTime()))
			&& PolYear == other.getPolYear()
			&& PolMonth == other.getPolMonth()
			&& InsuAccBalaBefore == other.getInsuAccBalaBefore()
			&& InsuAccBalaAfter == other.getInsuAccBalaAfter()
			&& InsuAccBalaGurat == other.getInsuAccBalaGurat()
			&& PrintCount == other.getPrintCount()
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("SequenceNo") ) {
			return 0;
		}
		if( strFieldName.equals("ContNo") ) {
			return 1;
		}
		if( strFieldName.equals("PolNo") ) {
			return 2;
		}
		if( strFieldName.equals("InsuAccNo") ) {
			return 3;
		}
		if( strFieldName.equals("BalaCount") ) {
			return 4;
		}
		if( strFieldName.equals("DueBalaDate") ) {
			return 5;
		}
		if( strFieldName.equals("DueBalaTime") ) {
			return 6;
		}
		if( strFieldName.equals("RunDate") ) {
			return 7;
		}
		if( strFieldName.equals("RunTime") ) {
			return 8;
		}
		if( strFieldName.equals("PolYear") ) {
			return 9;
		}
		if( strFieldName.equals("PolMonth") ) {
			return 10;
		}
		if( strFieldName.equals("InsuAccBalaBefore") ) {
			return 11;
		}
		if( strFieldName.equals("InsuAccBalaAfter") ) {
			return 12;
		}
		if( strFieldName.equals("InsuAccBalaGurat") ) {
			return 13;
		}
		if( strFieldName.equals("PrintCount") ) {
			return 14;
		}
		if( strFieldName.equals("Operator") ) {
			return 15;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 16;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 17;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 18;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 19;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "SequenceNo";
				break;
			case 1:
				strFieldName = "ContNo";
				break;
			case 2:
				strFieldName = "PolNo";
				break;
			case 3:
				strFieldName = "InsuAccNo";
				break;
			case 4:
				strFieldName = "BalaCount";
				break;
			case 5:
				strFieldName = "DueBalaDate";
				break;
			case 6:
				strFieldName = "DueBalaTime";
				break;
			case 7:
				strFieldName = "RunDate";
				break;
			case 8:
				strFieldName = "RunTime";
				break;
			case 9:
				strFieldName = "PolYear";
				break;
			case 10:
				strFieldName = "PolMonth";
				break;
			case 11:
				strFieldName = "InsuAccBalaBefore";
				break;
			case 12:
				strFieldName = "InsuAccBalaAfter";
				break;
			case 13:
				strFieldName = "InsuAccBalaGurat";
				break;
			case 14:
				strFieldName = "PrintCount";
				break;
			case 15:
				strFieldName = "Operator";
				break;
			case 16:
				strFieldName = "MakeDate";
				break;
			case 17:
				strFieldName = "MakeTime";
				break;
			case 18:
				strFieldName = "ModifyDate";
				break;
			case 19:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("SequenceNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuAccNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BalaCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("DueBalaDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("DueBalaTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RunDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("RunTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolYear") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("PolMonth") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("InsuAccBalaBefore") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("InsuAccBalaAfter") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("InsuAccBalaGurat") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("PrintCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_INT;
				break;
			case 5:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_INT;
				break;
			case 10:
				nFieldType = Schema.TYPE_INT;
				break;
			case 11:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 12:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 13:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 14:
				nFieldType = Schema.TYPE_INT;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
