/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.TPH_LPUnderWritingDB;

/*
 * <p>ClassName: TPH_LPUnderWritingSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2015-11-03
 */
public class TPH_LPUnderWritingSchema implements Schema, Cloneable
{
	// @Field
	/** 数据批次号 */
	private String dataBatchNo;
	/** 数据流水号 */
	private String dataSerialNo;
	/** 保单号 */
	private String policyNo;
	/** 续保批单号 */
	private String renewalEndorsementNo;
	/** 分单号 */
	private String sequenceNo;
	/** 客户编码 */
	private String customerNo;
	/** 公司险种代码 */
	private String comCoverageCode;
	/** 核保编号 */
	private String underNo;
	/** 核保结论日期 */
	private Date underwritingDate;
	/** 核保决定 */
	private String underwritingDecision;
	/** 核保描述 */
	private String underwritingDes;
	/** 处理日期 */
	private Date makeDate;
	/** 处理时间 */
	private String makeTime;
	/** 修改日期 */
	private Date modifyDate;
	/** 修改时间 */
	private String modifyTime;

	public static final int FIELDNUM = 15;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public TPH_LPUnderWritingSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[7];
		pk[0] = "dataBatchNo";
		pk[1] = "policyNo";
		pk[2] = "renewalEndorsementNo";
		pk[3] = "sequenceNo";
		pk[4] = "customerNo";
		pk[5] = "comCoverageCode";
		pk[6] = "underNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		TPH_LPUnderWritingSchema cloned = (TPH_LPUnderWritingSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getdataBatchNo()
	{
		return dataBatchNo;
	}
	public void setdataBatchNo(String adataBatchNo)
	{
		dataBatchNo = adataBatchNo;
	}
	public String getdataSerialNo()
	{
		return dataSerialNo;
	}
	public void setdataSerialNo(String adataSerialNo)
	{
		dataSerialNo = adataSerialNo;
	}
	public String getpolicyNo()
	{
		return policyNo;
	}
	public void setpolicyNo(String apolicyNo)
	{
		policyNo = apolicyNo;
	}
	public String getrenewalEndorsementNo()
	{
		return renewalEndorsementNo;
	}
	public void setrenewalEndorsementNo(String arenewalEndorsementNo)
	{
		renewalEndorsementNo = arenewalEndorsementNo;
	}
	public String getsequenceNo()
	{
		return sequenceNo;
	}
	public void setsequenceNo(String asequenceNo)
	{
		sequenceNo = asequenceNo;
	}
	public String getcustomerNo()
	{
		return customerNo;
	}
	public void setcustomerNo(String acustomerNo)
	{
		customerNo = acustomerNo;
	}
	public String getcomCoverageCode()
	{
		return comCoverageCode;
	}
	public void setcomCoverageCode(String acomCoverageCode)
	{
		comCoverageCode = acomCoverageCode;
	}
	public String getunderNo()
	{
		return underNo;
	}
	public void setunderNo(String aunderNo)
	{
		underNo = aunderNo;
	}
	public String getunderwritingDate()
	{
		if( underwritingDate != null )
			return fDate.getString(underwritingDate);
		else
			return null;
	}
	public void setunderwritingDate(Date aunderwritingDate)
	{
		underwritingDate = aunderwritingDate;
	}
	public void setunderwritingDate(String aunderwritingDate)
	{
		if (aunderwritingDate != null && !aunderwritingDate.equals("") )
		{
			underwritingDate = fDate.getDate( aunderwritingDate );
		}
		else
			underwritingDate = null;
	}

	public String getunderwritingDecision()
	{
		return underwritingDecision;
	}
	public void setunderwritingDecision(String aunderwritingDecision)
	{
		underwritingDecision = aunderwritingDecision;
	}
	public String getunderwritingDes()
	{
		return underwritingDes;
	}
	public void setunderwritingDes(String aunderwritingDes)
	{
		underwritingDes = aunderwritingDes;
	}
	public String getmakeDate()
	{
		if( makeDate != null )
			return fDate.getString(makeDate);
		else
			return null;
	}
	public void setmakeDate(Date amakeDate)
	{
		makeDate = amakeDate;
	}
	public void setmakeDate(String amakeDate)
	{
		if (amakeDate != null && !amakeDate.equals("") )
		{
			makeDate = fDate.getDate( amakeDate );
		}
		else
			makeDate = null;
	}

	public String getmakeTime()
	{
		return makeTime;
	}
	public void setmakeTime(String amakeTime)
	{
		makeTime = amakeTime;
	}
	public String getmodifyDate()
	{
		if( modifyDate != null )
			return fDate.getString(modifyDate);
		else
			return null;
	}
	public void setmodifyDate(Date amodifyDate)
	{
		modifyDate = amodifyDate;
	}
	public void setmodifyDate(String amodifyDate)
	{
		if (amodifyDate != null && !amodifyDate.equals("") )
		{
			modifyDate = fDate.getDate( amodifyDate );
		}
		else
			modifyDate = null;
	}

	public String getmodifyTime()
	{
		return modifyTime;
	}
	public void setmodifyTime(String amodifyTime)
	{
		modifyTime = amodifyTime;
	}

	/**
	* 使用另外一个 TPH_LPUnderWritingSchema 对象给 Schema 赋值
	* @param: aTPH_LPUnderWritingSchema TPH_LPUnderWritingSchema
	**/
	public void setSchema(TPH_LPUnderWritingSchema aTPH_LPUnderWritingSchema)
	{
		this.dataBatchNo = aTPH_LPUnderWritingSchema.getdataBatchNo();
		this.dataSerialNo = aTPH_LPUnderWritingSchema.getdataSerialNo();
		this.policyNo = aTPH_LPUnderWritingSchema.getpolicyNo();
		this.renewalEndorsementNo = aTPH_LPUnderWritingSchema.getrenewalEndorsementNo();
		this.sequenceNo = aTPH_LPUnderWritingSchema.getsequenceNo();
		this.customerNo = aTPH_LPUnderWritingSchema.getcustomerNo();
		this.comCoverageCode = aTPH_LPUnderWritingSchema.getcomCoverageCode();
		this.underNo = aTPH_LPUnderWritingSchema.getunderNo();
		this.underwritingDate = fDate.getDate( aTPH_LPUnderWritingSchema.getunderwritingDate());
		this.underwritingDecision = aTPH_LPUnderWritingSchema.getunderwritingDecision();
		this.underwritingDes = aTPH_LPUnderWritingSchema.getunderwritingDes();
		this.makeDate = fDate.getDate( aTPH_LPUnderWritingSchema.getmakeDate());
		this.makeTime = aTPH_LPUnderWritingSchema.getmakeTime();
		this.modifyDate = fDate.getDate( aTPH_LPUnderWritingSchema.getmodifyDate());
		this.modifyTime = aTPH_LPUnderWritingSchema.getmodifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("dataBatchNo") == null )
				this.dataBatchNo = null;
			else
				this.dataBatchNo = rs.getString("dataBatchNo").trim();

			if( rs.getString("dataSerialNo") == null )
				this.dataSerialNo = null;
			else
				this.dataSerialNo = rs.getString("dataSerialNo").trim();

			if( rs.getString("policyNo") == null )
				this.policyNo = null;
			else
				this.policyNo = rs.getString("policyNo").trim();

			if( rs.getString("renewalEndorsementNo") == null )
				this.renewalEndorsementNo = null;
			else
				this.renewalEndorsementNo = rs.getString("renewalEndorsementNo").trim();

			if( rs.getString("sequenceNo") == null )
				this.sequenceNo = null;
			else
				this.sequenceNo = rs.getString("sequenceNo").trim();

			if( rs.getString("customerNo") == null )
				this.customerNo = null;
			else
				this.customerNo = rs.getString("customerNo").trim();

			if( rs.getString("comCoverageCode") == null )
				this.comCoverageCode = null;
			else
				this.comCoverageCode = rs.getString("comCoverageCode").trim();

			if( rs.getString("underNo") == null )
				this.underNo = null;
			else
				this.underNo = rs.getString("underNo").trim();

			this.underwritingDate = rs.getDate("underwritingDate");
			if( rs.getString("underwritingDecision") == null )
				this.underwritingDecision = null;
			else
				this.underwritingDecision = rs.getString("underwritingDecision").trim();

			if( rs.getString("underwritingDes") == null )
				this.underwritingDes = null;
			else
				this.underwritingDes = rs.getString("underwritingDes").trim();

			this.makeDate = rs.getDate("makeDate");
			if( rs.getString("makeTime") == null )
				this.makeTime = null;
			else
				this.makeTime = rs.getString("makeTime").trim();

			this.modifyDate = rs.getDate("modifyDate");
			if( rs.getString("modifyTime") == null )
				this.modifyTime = null;
			else
				this.modifyTime = rs.getString("modifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的TPH_LPUnderWriting表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "TPH_LPUnderWritingSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public TPH_LPUnderWritingSchema getSchema()
	{
		TPH_LPUnderWritingSchema aTPH_LPUnderWritingSchema = new TPH_LPUnderWritingSchema();
		aTPH_LPUnderWritingSchema.setSchema(this);
		return aTPH_LPUnderWritingSchema;
	}

	public TPH_LPUnderWritingDB getDB()
	{
		TPH_LPUnderWritingDB aDBOper = new TPH_LPUnderWritingDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpTPH_LPUnderWriting描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(dataBatchNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(dataSerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(policyNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(renewalEndorsementNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(sequenceNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(customerNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(comCoverageCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(underNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( underwritingDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(underwritingDecision)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(underwritingDes)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( makeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(makeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( modifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(modifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpTPH_LPUnderWriting>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			dataBatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			dataSerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			policyNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			renewalEndorsementNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			sequenceNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			customerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			comCoverageCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			underNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			underwritingDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,SysConst.PACKAGESPILTER));
			underwritingDecision = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			underwritingDes = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			makeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
			makeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			modifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,SysConst.PACKAGESPILTER));
			modifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "TPH_LPUnderWritingSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("dataBatchNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(dataBatchNo));
		}
		if (FCode.equals("dataSerialNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(dataSerialNo));
		}
		if (FCode.equals("policyNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(policyNo));
		}
		if (FCode.equals("renewalEndorsementNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(renewalEndorsementNo));
		}
		if (FCode.equals("sequenceNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(sequenceNo));
		}
		if (FCode.equals("customerNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(customerNo));
		}
		if (FCode.equals("comCoverageCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(comCoverageCode));
		}
		if (FCode.equals("underNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(underNo));
		}
		if (FCode.equals("underwritingDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getunderwritingDate()));
		}
		if (FCode.equals("underwritingDecision"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(underwritingDecision));
		}
		if (FCode.equals("underwritingDes"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(underwritingDes));
		}
		if (FCode.equals("makeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getmakeDate()));
		}
		if (FCode.equals("makeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(makeTime));
		}
		if (FCode.equals("modifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getmodifyDate()));
		}
		if (FCode.equals("modifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(modifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(dataBatchNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(dataSerialNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(policyNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(renewalEndorsementNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(sequenceNo);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(customerNo);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(comCoverageCode);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(underNo);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getunderwritingDate()));
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(underwritingDecision);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(underwritingDes);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getmakeDate()));
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(makeTime);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getmodifyDate()));
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(modifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("dataBatchNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				dataBatchNo = FValue.trim();
			}
			else
				dataBatchNo = null;
		}
		if (FCode.equalsIgnoreCase("dataSerialNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				dataSerialNo = FValue.trim();
			}
			else
				dataSerialNo = null;
		}
		if (FCode.equalsIgnoreCase("policyNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				policyNo = FValue.trim();
			}
			else
				policyNo = null;
		}
		if (FCode.equalsIgnoreCase("renewalEndorsementNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				renewalEndorsementNo = FValue.trim();
			}
			else
				renewalEndorsementNo = null;
		}
		if (FCode.equalsIgnoreCase("sequenceNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				sequenceNo = FValue.trim();
			}
			else
				sequenceNo = null;
		}
		if (FCode.equalsIgnoreCase("customerNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				customerNo = FValue.trim();
			}
			else
				customerNo = null;
		}
		if (FCode.equalsIgnoreCase("comCoverageCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				comCoverageCode = FValue.trim();
			}
			else
				comCoverageCode = null;
		}
		if (FCode.equalsIgnoreCase("underNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				underNo = FValue.trim();
			}
			else
				underNo = null;
		}
		if (FCode.equalsIgnoreCase("underwritingDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				underwritingDate = fDate.getDate( FValue );
			}
			else
				underwritingDate = null;
		}
		if (FCode.equalsIgnoreCase("underwritingDecision"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				underwritingDecision = FValue.trim();
			}
			else
				underwritingDecision = null;
		}
		if (FCode.equalsIgnoreCase("underwritingDes"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				underwritingDes = FValue.trim();
			}
			else
				underwritingDes = null;
		}
		if (FCode.equalsIgnoreCase("makeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				makeDate = fDate.getDate( FValue );
			}
			else
				makeDate = null;
		}
		if (FCode.equalsIgnoreCase("makeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				makeTime = FValue.trim();
			}
			else
				makeTime = null;
		}
		if (FCode.equalsIgnoreCase("modifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				modifyDate = fDate.getDate( FValue );
			}
			else
				modifyDate = null;
		}
		if (FCode.equalsIgnoreCase("modifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				modifyTime = FValue.trim();
			}
			else
				modifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		TPH_LPUnderWritingSchema other = (TPH_LPUnderWritingSchema)otherObject;
		return
			(dataBatchNo == null ? other.getdataBatchNo() == null : dataBatchNo.equals(other.getdataBatchNo()))
			&& (dataSerialNo == null ? other.getdataSerialNo() == null : dataSerialNo.equals(other.getdataSerialNo()))
			&& (policyNo == null ? other.getpolicyNo() == null : policyNo.equals(other.getpolicyNo()))
			&& (renewalEndorsementNo == null ? other.getrenewalEndorsementNo() == null : renewalEndorsementNo.equals(other.getrenewalEndorsementNo()))
			&& (sequenceNo == null ? other.getsequenceNo() == null : sequenceNo.equals(other.getsequenceNo()))
			&& (customerNo == null ? other.getcustomerNo() == null : customerNo.equals(other.getcustomerNo()))
			&& (comCoverageCode == null ? other.getcomCoverageCode() == null : comCoverageCode.equals(other.getcomCoverageCode()))
			&& (underNo == null ? other.getunderNo() == null : underNo.equals(other.getunderNo()))
			&& (underwritingDate == null ? other.getunderwritingDate() == null : fDate.getString(underwritingDate).equals(other.getunderwritingDate()))
			&& (underwritingDecision == null ? other.getunderwritingDecision() == null : underwritingDecision.equals(other.getunderwritingDecision()))
			&& (underwritingDes == null ? other.getunderwritingDes() == null : underwritingDes.equals(other.getunderwritingDes()))
			&& (makeDate == null ? other.getmakeDate() == null : fDate.getString(makeDate).equals(other.getmakeDate()))
			&& (makeTime == null ? other.getmakeTime() == null : makeTime.equals(other.getmakeTime()))
			&& (modifyDate == null ? other.getmodifyDate() == null : fDate.getString(modifyDate).equals(other.getmodifyDate()))
			&& (modifyTime == null ? other.getmodifyTime() == null : modifyTime.equals(other.getmodifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("dataBatchNo") ) {
			return 0;
		}
		if( strFieldName.equals("dataSerialNo") ) {
			return 1;
		}
		if( strFieldName.equals("policyNo") ) {
			return 2;
		}
		if( strFieldName.equals("renewalEndorsementNo") ) {
			return 3;
		}
		if( strFieldName.equals("sequenceNo") ) {
			return 4;
		}
		if( strFieldName.equals("customerNo") ) {
			return 5;
		}
		if( strFieldName.equals("comCoverageCode") ) {
			return 6;
		}
		if( strFieldName.equals("underNo") ) {
			return 7;
		}
		if( strFieldName.equals("underwritingDate") ) {
			return 8;
		}
		if( strFieldName.equals("underwritingDecision") ) {
			return 9;
		}
		if( strFieldName.equals("underwritingDes") ) {
			return 10;
		}
		if( strFieldName.equals("makeDate") ) {
			return 11;
		}
		if( strFieldName.equals("makeTime") ) {
			return 12;
		}
		if( strFieldName.equals("modifyDate") ) {
			return 13;
		}
		if( strFieldName.equals("modifyTime") ) {
			return 14;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "dataBatchNo";
				break;
			case 1:
				strFieldName = "dataSerialNo";
				break;
			case 2:
				strFieldName = "policyNo";
				break;
			case 3:
				strFieldName = "renewalEndorsementNo";
				break;
			case 4:
				strFieldName = "sequenceNo";
				break;
			case 5:
				strFieldName = "customerNo";
				break;
			case 6:
				strFieldName = "comCoverageCode";
				break;
			case 7:
				strFieldName = "underNo";
				break;
			case 8:
				strFieldName = "underwritingDate";
				break;
			case 9:
				strFieldName = "underwritingDecision";
				break;
			case 10:
				strFieldName = "underwritingDes";
				break;
			case 11:
				strFieldName = "makeDate";
				break;
			case 12:
				strFieldName = "makeTime";
				break;
			case 13:
				strFieldName = "modifyDate";
				break;
			case 14:
				strFieldName = "modifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("dataBatchNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("dataSerialNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("policyNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("renewalEndorsementNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("sequenceNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("customerNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("comCoverageCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("underNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("underwritingDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("underwritingDecision") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("underwritingDes") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("makeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("makeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("modifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("modifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
