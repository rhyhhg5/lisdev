/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LDBlackListReasonDetailDB;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LDBlackListReasonDetailSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 黑名单管理
 * @CreateDate：2005-05-17
 */
public class LDBlackListReasonDetailSchema implements Schema, Cloneable
{
    // @Field
    /** 黑名单流水号 */
    private String BlackListSerialNo;
    /** 录入原因 */
    private String BlackListReasonKind;
    /** 录入原因明细类别 */
    private String BlackListReasonDetailKind;
    /** 录入原因明细编码 */
    private String BlackListReasonDetailNo;
    /** 录入原因明细内容 */
    private String BlackListReasonDetail;
    /** 黑名单级别 */
    private String BlackListGrade;
    /** 黑名单类别 */
    private String BlackListKind;
    /** 相关合同号 */
    private String RelaContNo;
    /** 相关保单号 */
    private String RelaPolNo;
    /** 其它相关号 */
    private String RelaOtherNo;
    /** 其它相关号类型 */
    private String RelaOtherNoType;
    /** 黑名单录入员类别 */
    private String BlackListOperatorType;

    public static final int FIELDNUM = 12; // 数据库表的字段个数

    private static String[] PK; // 主键

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LDBlackListReasonDetailSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "BlackListSerialNo";
        pk[1] = "BlackListReasonKind";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException
    {
        LDBlackListReasonDetailSchema cloned = (LDBlackListReasonDetailSchema)super.
                                               clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getBlackListSerialNo()
    {
        if (SysConst.CHANGECHARSET && BlackListSerialNo != null &&
            !BlackListSerialNo.equals(""))
        {
            BlackListSerialNo = StrTool.unicodeToGBK(BlackListSerialNo);
        }
        return BlackListSerialNo;
    }

    public void setBlackListSerialNo(String aBlackListSerialNo)
    {
        BlackListSerialNo = aBlackListSerialNo;
    }

    public String getBlackListReasonKind()
    {
        if (SysConst.CHANGECHARSET && BlackListReasonKind != null &&
            !BlackListReasonKind.equals(""))
        {
            BlackListReasonKind = StrTool.unicodeToGBK(BlackListReasonKind);
        }
        return BlackListReasonKind;
    }

    public void setBlackListReasonKind(String aBlackListReasonKind)
    {
        BlackListReasonKind = aBlackListReasonKind;
    }

    public String getBlackListReasonDetailKind()
    {
        if (SysConst.CHANGECHARSET && BlackListReasonDetailKind != null &&
            !BlackListReasonDetailKind.equals(""))
        {
            BlackListReasonDetailKind = StrTool.unicodeToGBK(
                    BlackListReasonDetailKind);
        }
        return BlackListReasonDetailKind;
    }

    public void setBlackListReasonDetailKind(String aBlackListReasonDetailKind)
    {
        BlackListReasonDetailKind = aBlackListReasonDetailKind;
    }

    public String getBlackListReasonDetailNo()
    {
        if (SysConst.CHANGECHARSET && BlackListReasonDetailNo != null &&
            !BlackListReasonDetailNo.equals(""))
        {
            BlackListReasonDetailNo = StrTool.unicodeToGBK(
                    BlackListReasonDetailNo);
        }
        return BlackListReasonDetailNo;
    }

    public void setBlackListReasonDetailNo(String aBlackListReasonDetailNo)
    {
        BlackListReasonDetailNo = aBlackListReasonDetailNo;
    }

    public String getBlackListReasonDetail()
    {
        if (SysConst.CHANGECHARSET && BlackListReasonDetail != null &&
            !BlackListReasonDetail.equals(""))
        {
            BlackListReasonDetail = StrTool.unicodeToGBK(BlackListReasonDetail);
        }
        return BlackListReasonDetail;
    }

    public void setBlackListReasonDetail(String aBlackListReasonDetail)
    {
        BlackListReasonDetail = aBlackListReasonDetail;
    }

    public String getBlackListGrade()
    {
        if (SysConst.CHANGECHARSET && BlackListGrade != null &&
            !BlackListGrade.equals(""))
        {
            BlackListGrade = StrTool.unicodeToGBK(BlackListGrade);
        }
        return BlackListGrade;
    }

    public void setBlackListGrade(String aBlackListGrade)
    {
        BlackListGrade = aBlackListGrade;
    }

    public String getBlackListKind()
    {
        if (SysConst.CHANGECHARSET && BlackListKind != null &&
            !BlackListKind.equals(""))
        {
            BlackListKind = StrTool.unicodeToGBK(BlackListKind);
        }
        return BlackListKind;
    }

    public void setBlackListKind(String aBlackListKind)
    {
        BlackListKind = aBlackListKind;
    }

    public String getRelaContNo()
    {
        if (SysConst.CHANGECHARSET && RelaContNo != null &&
            !RelaContNo.equals(""))
        {
            RelaContNo = StrTool.unicodeToGBK(RelaContNo);
        }
        return RelaContNo;
    }

    public void setRelaContNo(String aRelaContNo)
    {
        RelaContNo = aRelaContNo;
    }

    public String getRelaPolNo()
    {
        if (SysConst.CHANGECHARSET && RelaPolNo != null && !RelaPolNo.equals(""))
        {
            RelaPolNo = StrTool.unicodeToGBK(RelaPolNo);
        }
        return RelaPolNo;
    }

    public void setRelaPolNo(String aRelaPolNo)
    {
        RelaPolNo = aRelaPolNo;
    }

    public String getRelaOtherNo()
    {
        if (SysConst.CHANGECHARSET && RelaOtherNo != null &&
            !RelaOtherNo.equals(""))
        {
            RelaOtherNo = StrTool.unicodeToGBK(RelaOtherNo);
        }
        return RelaOtherNo;
    }

    public void setRelaOtherNo(String aRelaOtherNo)
    {
        RelaOtherNo = aRelaOtherNo;
    }

    public String getRelaOtherNoType()
    {
        if (SysConst.CHANGECHARSET && RelaOtherNoType != null &&
            !RelaOtherNoType.equals(""))
        {
            RelaOtherNoType = StrTool.unicodeToGBK(RelaOtherNoType);
        }
        return RelaOtherNoType;
    }

    public void setRelaOtherNoType(String aRelaOtherNoType)
    {
        RelaOtherNoType = aRelaOtherNoType;
    }

    public String getBlackListOperatorType()
    {
        if (SysConst.CHANGECHARSET && BlackListOperatorType != null &&
            !BlackListOperatorType.equals(""))
        {
            BlackListOperatorType = StrTool.unicodeToGBK(BlackListOperatorType);
        }
        return BlackListOperatorType;
    }

    public void setBlackListOperatorType(String aBlackListOperatorType)
    {
        BlackListOperatorType = aBlackListOperatorType;
    }

    /**
     * 使用另外一个 LDBlackListReasonDetailSchema 对象给 Schema 赋值
     * @param: aLDBlackListReasonDetailSchema LDBlackListReasonDetailSchema
     **/
    public void setSchema(LDBlackListReasonDetailSchema
                          aLDBlackListReasonDetailSchema)
    {
        this.BlackListSerialNo = aLDBlackListReasonDetailSchema.
                                 getBlackListSerialNo();
        this.BlackListReasonKind = aLDBlackListReasonDetailSchema.
                                   getBlackListReasonKind();
        this.BlackListReasonDetailKind = aLDBlackListReasonDetailSchema.
                                         getBlackListReasonDetailKind();
        this.BlackListReasonDetailNo = aLDBlackListReasonDetailSchema.
                                       getBlackListReasonDetailNo();
        this.BlackListReasonDetail = aLDBlackListReasonDetailSchema.
                                     getBlackListReasonDetail();
        this.BlackListGrade = aLDBlackListReasonDetailSchema.getBlackListGrade();
        this.BlackListKind = aLDBlackListReasonDetailSchema.getBlackListKind();
        this.RelaContNo = aLDBlackListReasonDetailSchema.getRelaContNo();
        this.RelaPolNo = aLDBlackListReasonDetailSchema.getRelaPolNo();
        this.RelaOtherNo = aLDBlackListReasonDetailSchema.getRelaOtherNo();
        this.RelaOtherNoType = aLDBlackListReasonDetailSchema.
                               getRelaOtherNoType();
        this.BlackListOperatorType = aLDBlackListReasonDetailSchema.
                                     getBlackListOperatorType();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("BlackListSerialNo") == null)
            {
                this.BlackListSerialNo = null;
            }
            else
            {
                this.BlackListSerialNo = rs.getString("BlackListSerialNo").trim();
            }

            if (rs.getString("BlackListReasonKind") == null)
            {
                this.BlackListReasonKind = null;
            }
            else
            {
                this.BlackListReasonKind = rs.getString("BlackListReasonKind").
                                           trim();
            }

            if (rs.getString("BlackListReasonDetailKind") == null)
            {
                this.BlackListReasonDetailKind = null;
            }
            else
            {
                this.BlackListReasonDetailKind = rs.getString(
                        "BlackListReasonDetailKind").trim();
            }

            if (rs.getString("BlackListReasonDetailNo") == null)
            {
                this.BlackListReasonDetailNo = null;
            }
            else
            {
                this.BlackListReasonDetailNo = rs.getString(
                        "BlackListReasonDetailNo").trim();
            }

            if (rs.getString("BlackListReasonDetail") == null)
            {
                this.BlackListReasonDetail = null;
            }
            else
            {
                this.BlackListReasonDetail = rs.getString(
                        "BlackListReasonDetail").trim();
            }

            if (rs.getString("BlackListGrade") == null)
            {
                this.BlackListGrade = null;
            }
            else
            {
                this.BlackListGrade = rs.getString("BlackListGrade").trim();
            }

            if (rs.getString("BlackListKind") == null)
            {
                this.BlackListKind = null;
            }
            else
            {
                this.BlackListKind = rs.getString("BlackListKind").trim();
            }

            if (rs.getString("RelaContNo") == null)
            {
                this.RelaContNo = null;
            }
            else
            {
                this.RelaContNo = rs.getString("RelaContNo").trim();
            }

            if (rs.getString("RelaPolNo") == null)
            {
                this.RelaPolNo = null;
            }
            else
            {
                this.RelaPolNo = rs.getString("RelaPolNo").trim();
            }

            if (rs.getString("RelaOtherNo") == null)
            {
                this.RelaOtherNo = null;
            }
            else
            {
                this.RelaOtherNo = rs.getString("RelaOtherNo").trim();
            }

            if (rs.getString("RelaOtherNoType") == null)
            {
                this.RelaOtherNoType = null;
            }
            else
            {
                this.RelaOtherNoType = rs.getString("RelaOtherNoType").trim();
            }

            if (rs.getString("BlackListOperatorType") == null)
            {
                this.BlackListOperatorType = null;
            }
            else
            {
                this.BlackListOperatorType = rs.getString(
                        "BlackListOperatorType").trim();
            }

        }
        catch (SQLException sqle)
        {
            System.out.println("数据库中的LDBlackListReasonDetail表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDBlackListReasonDetailSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LDBlackListReasonDetailSchema getSchema()
    {
        LDBlackListReasonDetailSchema aLDBlackListReasonDetailSchema = new
                LDBlackListReasonDetailSchema();
        aLDBlackListReasonDetailSchema.setSchema(this);
        return aLDBlackListReasonDetailSchema;
    }

    public LDBlackListReasonDetailDB getDB()
    {
        LDBlackListReasonDetailDB aDBOper = new LDBlackListReasonDetailDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDBlackListReasonDetail描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BlackListSerialNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BlackListReasonKind)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(
                BlackListReasonDetailKind)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(
                BlackListReasonDetailNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(
                BlackListReasonDetail)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BlackListGrade)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BlackListKind)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(RelaContNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(RelaPolNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(RelaOtherNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(RelaOtherNoType)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(
                BlackListOperatorType)));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDBlackListReasonDetail>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            BlackListSerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                               1, SysConst.PACKAGESPILTER);
            BlackListReasonKind = StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 2, SysConst.PACKAGESPILTER);
            BlackListReasonDetailKind = StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 3, SysConst.PACKAGESPILTER);
            BlackListReasonDetailNo = StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 4, SysConst.PACKAGESPILTER);
            BlackListReasonDetail = StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 5, SysConst.PACKAGESPILTER);
            BlackListGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                            SysConst.PACKAGESPILTER);
            BlackListKind = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                           SysConst.PACKAGESPILTER);
            RelaContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                        SysConst.PACKAGESPILTER);
            RelaPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                       SysConst.PACKAGESPILTER);
            RelaOtherNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                         SysConst.PACKAGESPILTER);
            RelaOtherNoType = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             11, SysConst.PACKAGESPILTER);
            BlackListOperatorType = StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 12, SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDBlackListReasonDetailSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("BlackListSerialNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BlackListSerialNo));
        }
        if (FCode.equals("BlackListReasonKind"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BlackListReasonKind));
        }
        if (FCode.equals("BlackListReasonDetailKind"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(
                    BlackListReasonDetailKind));
        }
        if (FCode.equals("BlackListReasonDetailNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(
                    BlackListReasonDetailNo));
        }
        if (FCode.equals("BlackListReasonDetail"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(
                    BlackListReasonDetail));
        }
        if (FCode.equals("BlackListGrade"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BlackListGrade));
        }
        if (FCode.equals("BlackListKind"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BlackListKind));
        }
        if (FCode.equals("RelaContNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelaContNo));
        }
        if (FCode.equals("RelaPolNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelaPolNo));
        }
        if (FCode.equals("RelaOtherNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelaOtherNo));
        }
        if (FCode.equals("RelaOtherNoType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelaOtherNoType));
        }
        if (FCode.equals("BlackListOperatorType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(
                    BlackListOperatorType));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(BlackListSerialNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(BlackListReasonKind);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(BlackListReasonDetailKind);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(BlackListReasonDetailNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(BlackListReasonDetail);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(BlackListGrade);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(BlackListKind);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(RelaContNo);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(RelaPolNo);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(RelaOtherNo);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(RelaOtherNoType);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(BlackListOperatorType);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("BlackListSerialNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BlackListSerialNo = FValue.trim();
            }
            else
            {
                BlackListSerialNo = null;
            }
        }
        if (FCode.equals("BlackListReasonKind"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BlackListReasonKind = FValue.trim();
            }
            else
            {
                BlackListReasonKind = null;
            }
        }
        if (FCode.equals("BlackListReasonDetailKind"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BlackListReasonDetailKind = FValue.trim();
            }
            else
            {
                BlackListReasonDetailKind = null;
            }
        }
        if (FCode.equals("BlackListReasonDetailNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BlackListReasonDetailNo = FValue.trim();
            }
            else
            {
                BlackListReasonDetailNo = null;
            }
        }
        if (FCode.equals("BlackListReasonDetail"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BlackListReasonDetail = FValue.trim();
            }
            else
            {
                BlackListReasonDetail = null;
            }
        }
        if (FCode.equals("BlackListGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BlackListGrade = FValue.trim();
            }
            else
            {
                BlackListGrade = null;
            }
        }
        if (FCode.equals("BlackListKind"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BlackListKind = FValue.trim();
            }
            else
            {
                BlackListKind = null;
            }
        }
        if (FCode.equals("RelaContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RelaContNo = FValue.trim();
            }
            else
            {
                RelaContNo = null;
            }
        }
        if (FCode.equals("RelaPolNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RelaPolNo = FValue.trim();
            }
            else
            {
                RelaPolNo = null;
            }
        }
        if (FCode.equals("RelaOtherNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RelaOtherNo = FValue.trim();
            }
            else
            {
                RelaOtherNo = null;
            }
        }
        if (FCode.equals("RelaOtherNoType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RelaOtherNoType = FValue.trim();
            }
            else
            {
                RelaOtherNoType = null;
            }
        }
        if (FCode.equals("BlackListOperatorType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BlackListOperatorType = FValue.trim();
            }
            else
            {
                BlackListOperatorType = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LDBlackListReasonDetailSchema other = (LDBlackListReasonDetailSchema)
                                              otherObject;
        return
                BlackListSerialNo.equals(other.getBlackListSerialNo())
                && BlackListReasonKind.equals(other.getBlackListReasonKind())
                &&
                BlackListReasonDetailKind.equals(other.getBlackListReasonDetailKind())
                &&
                BlackListReasonDetailNo.equals(other.getBlackListReasonDetailNo())
                && BlackListReasonDetail.equals(other.getBlackListReasonDetail())
                && BlackListGrade.equals(other.getBlackListGrade())
                && BlackListKind.equals(other.getBlackListKind())
                && RelaContNo.equals(other.getRelaContNo())
                && RelaPolNo.equals(other.getRelaPolNo())
                && RelaOtherNo.equals(other.getRelaOtherNo())
                && RelaOtherNoType.equals(other.getRelaOtherNoType())
                && BlackListOperatorType.equals(other.getBlackListOperatorType());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("BlackListSerialNo"))
        {
            return 0;
        }
        if (strFieldName.equals("BlackListReasonKind"))
        {
            return 1;
        }
        if (strFieldName.equals("BlackListReasonDetailKind"))
        {
            return 2;
        }
        if (strFieldName.equals("BlackListReasonDetailNo"))
        {
            return 3;
        }
        if (strFieldName.equals("BlackListReasonDetail"))
        {
            return 4;
        }
        if (strFieldName.equals("BlackListGrade"))
        {
            return 5;
        }
        if (strFieldName.equals("BlackListKind"))
        {
            return 6;
        }
        if (strFieldName.equals("RelaContNo"))
        {
            return 7;
        }
        if (strFieldName.equals("RelaPolNo"))
        {
            return 8;
        }
        if (strFieldName.equals("RelaOtherNo"))
        {
            return 9;
        }
        if (strFieldName.equals("RelaOtherNoType"))
        {
            return 10;
        }
        if (strFieldName.equals("BlackListOperatorType"))
        {
            return 11;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "BlackListSerialNo";
                break;
            case 1:
                strFieldName = "BlackListReasonKind";
                break;
            case 2:
                strFieldName = "BlackListReasonDetailKind";
                break;
            case 3:
                strFieldName = "BlackListReasonDetailNo";
                break;
            case 4:
                strFieldName = "BlackListReasonDetail";
                break;
            case 5:
                strFieldName = "BlackListGrade";
                break;
            case 6:
                strFieldName = "BlackListKind";
                break;
            case 7:
                strFieldName = "RelaContNo";
                break;
            case 8:
                strFieldName = "RelaPolNo";
                break;
            case 9:
                strFieldName = "RelaOtherNo";
                break;
            case 10:
                strFieldName = "RelaOtherNoType";
                break;
            case 11:
                strFieldName = "BlackListOperatorType";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("BlackListSerialNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BlackListReasonKind"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BlackListReasonDetailKind"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BlackListReasonDetailNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BlackListReasonDetail"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BlackListGrade"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BlackListKind"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RelaContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RelaPolNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RelaOtherNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RelaOtherNoType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BlackListOperatorType"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
