/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LLHospCaseDB;

/*
 * <p>ClassName: LLHospCaseSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 新系统模型
 * @CreateDate：2011-05-25
 */
public class LLHospCaseSchema implements Schema, Cloneable
{
	// @Field
	/** 案件号 */
	private String CaseNo;
	/** 医院编码 */
	private String HospitCode;
	/** 结算批次号 */
	private String HCNo;
	/** 处理人 */
	private String Handler;
	/** 请求交互编码 */
	private String AppTranNo;
	/** 请求日期 */
	private Date AppDate;
	/** 请求时间 */
	private String AppTime;
	/** 理赔金额 */
	private double Realpay;
	/** 确认交互编码 */
	private String ConfirmTranNo;
	/** 确认日期 */
	private Date ConfirmDate;
	/** 确认时间 */
	private String ConfirmTime;
	/** 确认状态 */
	private String ConfirmState;
	/** 创建日期 */
	private Date MakeDate;
	/** 创建时间 */
	private String MakeTime;
	/** 修改日期 */
	private Date ModifyDate;
	/** 修改时间 */
	private String ModifyTime;
	/** 处理类型 */
	private String DealType;
	/** 案件类型 */
	private String CaseType;
	/** 平台理赔号 */
	private String ClaimNo;
	/** 受益人客户号 */
	private String BnfNo;

	public static final int FIELDNUM = 20;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LLHospCaseSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "CaseNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LLHospCaseSchema cloned = (LLHospCaseSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getCaseNo()
	{
		return CaseNo;
	}
	public void setCaseNo(String aCaseNo)
	{
		CaseNo = aCaseNo;
	}
	public String getHospitCode()
	{
		return HospitCode;
	}
	public void setHospitCode(String aHospitCode)
	{
		HospitCode = aHospitCode;
	}
	public String getHCNo()
	{
		return HCNo;
	}
	public void setHCNo(String aHCNo)
	{
		HCNo = aHCNo;
	}
	public String getHandler()
	{
		return Handler;
	}
	public void setHandler(String aHandler)
	{
		Handler = aHandler;
	}
	public String getAppTranNo()
	{
		return AppTranNo;
	}
	public void setAppTranNo(String aAppTranNo)
	{
		AppTranNo = aAppTranNo;
	}
	public String getAppDate()
	{
		if( AppDate != null )
			return fDate.getString(AppDate);
		else
			return null;
	}
	public void setAppDate(Date aAppDate)
	{
		AppDate = aAppDate;
	}
	public void setAppDate(String aAppDate)
	{
		if (aAppDate != null && !aAppDate.equals("") )
		{
			AppDate = fDate.getDate( aAppDate );
		}
		else
			AppDate = null;
	}

	public String getAppTime()
	{
		return AppTime;
	}
	public void setAppTime(String aAppTime)
	{
		AppTime = aAppTime;
	}
	public double getRealpay()
	{
		return Realpay;
	}
	public void setRealpay(double aRealpay)
	{
		Realpay = Arith.round(aRealpay,2);
	}
	public void setRealpay(String aRealpay)
	{
		if (aRealpay != null && !aRealpay.equals(""))
		{
			Double tDouble = new Double(aRealpay);
			double d = tDouble.doubleValue();
                Realpay = Arith.round(d,2);
		}
	}

	public String getConfirmTranNo()
	{
		return ConfirmTranNo;
	}
	public void setConfirmTranNo(String aConfirmTranNo)
	{
		ConfirmTranNo = aConfirmTranNo;
	}
	public String getConfirmDate()
	{
		if( ConfirmDate != null )
			return fDate.getString(ConfirmDate);
		else
			return null;
	}
	public void setConfirmDate(Date aConfirmDate)
	{
		ConfirmDate = aConfirmDate;
	}
	public void setConfirmDate(String aConfirmDate)
	{
		if (aConfirmDate != null && !aConfirmDate.equals("") )
		{
			ConfirmDate = fDate.getDate( aConfirmDate );
		}
		else
			ConfirmDate = null;
	}

	public String getConfirmTime()
	{
		return ConfirmTime;
	}
	public void setConfirmTime(String aConfirmTime)
	{
		ConfirmTime = aConfirmTime;
	}
	public String getConfirmState()
	{
		return ConfirmState;
	}
	public void setConfirmState(String aConfirmState)
	{
		ConfirmState = aConfirmState;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getDealType()
	{
		return DealType;
	}
	public void setDealType(String aDealType)
	{
		DealType = aDealType;
	}
	public String getCaseType()
	{
		return CaseType;
	}
	public void setCaseType(String aCaseType)
	{
		CaseType = aCaseType;
	}
	public String getClaimNo()
	{
		return ClaimNo;
	}
	public void setClaimNo(String aClaimNo)
	{
		ClaimNo = aClaimNo;
	}
	public String getBnfNo()
	{
		return BnfNo;
	}
	public void setBnfNo(String aBnfNo)
	{
		BnfNo = aBnfNo;
	}

	/**
	* 使用另外一个 LLHospCaseSchema 对象给 Schema 赋值
	* @param: aLLHospCaseSchema LLHospCaseSchema
	**/
	public void setSchema(LLHospCaseSchema aLLHospCaseSchema)
	{
		this.CaseNo = aLLHospCaseSchema.getCaseNo();
		this.HospitCode = aLLHospCaseSchema.getHospitCode();
		this.HCNo = aLLHospCaseSchema.getHCNo();
		this.Handler = aLLHospCaseSchema.getHandler();
		this.AppTranNo = aLLHospCaseSchema.getAppTranNo();
		this.AppDate = fDate.getDate( aLLHospCaseSchema.getAppDate());
		this.AppTime = aLLHospCaseSchema.getAppTime();
		this.Realpay = aLLHospCaseSchema.getRealpay();
		this.ConfirmTranNo = aLLHospCaseSchema.getConfirmTranNo();
		this.ConfirmDate = fDate.getDate( aLLHospCaseSchema.getConfirmDate());
		this.ConfirmTime = aLLHospCaseSchema.getConfirmTime();
		this.ConfirmState = aLLHospCaseSchema.getConfirmState();
		this.MakeDate = fDate.getDate( aLLHospCaseSchema.getMakeDate());
		this.MakeTime = aLLHospCaseSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLLHospCaseSchema.getModifyDate());
		this.ModifyTime = aLLHospCaseSchema.getModifyTime();
		this.DealType = aLLHospCaseSchema.getDealType();
		this.CaseType = aLLHospCaseSchema.getCaseType();
		this.ClaimNo = aLLHospCaseSchema.getClaimNo();
		this.BnfNo = aLLHospCaseSchema.getBnfNo();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("CaseNo") == null )
				this.CaseNo = null;
			else
				this.CaseNo = rs.getString("CaseNo").trim();

			if( rs.getString("HospitCode") == null )
				this.HospitCode = null;
			else
				this.HospitCode = rs.getString("HospitCode").trim();

			if( rs.getString("HCNo") == null )
				this.HCNo = null;
			else
				this.HCNo = rs.getString("HCNo").trim();

			if( rs.getString("Handler") == null )
				this.Handler = null;
			else
				this.Handler = rs.getString("Handler").trim();

			if( rs.getString("AppTranNo") == null )
				this.AppTranNo = null;
			else
				this.AppTranNo = rs.getString("AppTranNo").trim();

			this.AppDate = rs.getDate("AppDate");
			if( rs.getString("AppTime") == null )
				this.AppTime = null;
			else
				this.AppTime = rs.getString("AppTime").trim();

			this.Realpay = rs.getDouble("Realpay");
			if( rs.getString("ConfirmTranNo") == null )
				this.ConfirmTranNo = null;
			else
				this.ConfirmTranNo = rs.getString("ConfirmTranNo").trim();

			this.ConfirmDate = rs.getDate("ConfirmDate");
			if( rs.getString("ConfirmTime") == null )
				this.ConfirmTime = null;
			else
				this.ConfirmTime = rs.getString("ConfirmTime").trim();

			if( rs.getString("ConfirmState") == null )
				this.ConfirmState = null;
			else
				this.ConfirmState = rs.getString("ConfirmState").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("DealType") == null )
				this.DealType = null;
			else
				this.DealType = rs.getString("DealType").trim();

			if( rs.getString("CaseType") == null )
				this.CaseType = null;
			else
				this.CaseType = rs.getString("CaseType").trim();

			if( rs.getString("ClaimNo") == null )
				this.ClaimNo = null;
			else
				this.ClaimNo = rs.getString("ClaimNo").trim();

			if( rs.getString("BnfNo") == null )
				this.BnfNo = null;
			else
				this.BnfNo = rs.getString("BnfNo").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LLHospCase表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLHospCaseSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LLHospCaseSchema getSchema()
	{
		LLHospCaseSchema aLLHospCaseSchema = new LLHospCaseSchema();
		aLLHospCaseSchema.setSchema(this);
		return aLLHospCaseSchema;
	}

	public LLHospCaseDB getDB()
	{
		LLHospCaseDB aDBOper = new LLHospCaseDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLHospCase描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(CaseNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HospitCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HCNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Handler)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppTranNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( AppDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Realpay));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ConfirmTranNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ConfirmDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ConfirmTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ConfirmState)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DealType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CaseType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ClaimNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BnfNo));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLHospCase>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			CaseNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			HospitCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			HCNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			Handler = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			AppTranNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			AppDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,SysConst.PACKAGESPILTER));
			AppTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			Realpay = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,8,SysConst.PACKAGESPILTER))).doubleValue();
			ConfirmTranNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			ConfirmDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,SysConst.PACKAGESPILTER));
			ConfirmTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			ConfirmState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			DealType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			CaseType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			ClaimNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			BnfNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLHospCaseSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("CaseNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CaseNo));
		}
		if (FCode.equals("HospitCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HospitCode));
		}
		if (FCode.equals("HCNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HCNo));
		}
		if (FCode.equals("Handler"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Handler));
		}
		if (FCode.equals("AppTranNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppTranNo));
		}
		if (FCode.equals("AppDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getAppDate()));
		}
		if (FCode.equals("AppTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppTime));
		}
		if (FCode.equals("Realpay"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Realpay));
		}
		if (FCode.equals("ConfirmTranNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ConfirmTranNo));
		}
		if (FCode.equals("ConfirmDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getConfirmDate()));
		}
		if (FCode.equals("ConfirmTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ConfirmTime));
		}
		if (FCode.equals("ConfirmState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ConfirmState));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("DealType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DealType));
		}
		if (FCode.equals("CaseType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CaseType));
		}
		if (FCode.equals("ClaimNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClaimNo));
		}
		if (FCode.equals("BnfNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BnfNo));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(CaseNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(HospitCode);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(HCNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(Handler);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(AppTranNo);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getAppDate()));
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(AppTime);
				break;
			case 7:
				strFieldValue = String.valueOf(Realpay);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(ConfirmTranNo);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getConfirmDate()));
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(ConfirmTime);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(ConfirmState);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(DealType);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(CaseType);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(ClaimNo);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(BnfNo);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("CaseNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CaseNo = FValue.trim();
			}
			else
				CaseNo = null;
		}
		if (FCode.equalsIgnoreCase("HospitCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HospitCode = FValue.trim();
			}
			else
				HospitCode = null;
		}
		if (FCode.equalsIgnoreCase("HCNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HCNo = FValue.trim();
			}
			else
				HCNo = null;
		}
		if (FCode.equalsIgnoreCase("Handler"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Handler = FValue.trim();
			}
			else
				Handler = null;
		}
		if (FCode.equalsIgnoreCase("AppTranNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppTranNo = FValue.trim();
			}
			else
				AppTranNo = null;
		}
		if (FCode.equalsIgnoreCase("AppDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				AppDate = fDate.getDate( FValue );
			}
			else
				AppDate = null;
		}
		if (FCode.equalsIgnoreCase("AppTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppTime = FValue.trim();
			}
			else
				AppTime = null;
		}
		if (FCode.equalsIgnoreCase("Realpay"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Realpay = d;
			}
		}
		if (FCode.equalsIgnoreCase("ConfirmTranNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ConfirmTranNo = FValue.trim();
			}
			else
				ConfirmTranNo = null;
		}
		if (FCode.equalsIgnoreCase("ConfirmDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ConfirmDate = fDate.getDate( FValue );
			}
			else
				ConfirmDate = null;
		}
		if (FCode.equalsIgnoreCase("ConfirmTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ConfirmTime = FValue.trim();
			}
			else
				ConfirmTime = null;
		}
		if (FCode.equalsIgnoreCase("ConfirmState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ConfirmState = FValue.trim();
			}
			else
				ConfirmState = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("DealType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DealType = FValue.trim();
			}
			else
				DealType = null;
		}
		if (FCode.equalsIgnoreCase("CaseType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CaseType = FValue.trim();
			}
			else
				CaseType = null;
		}
		if (FCode.equalsIgnoreCase("ClaimNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ClaimNo = FValue.trim();
			}
			else
				ClaimNo = null;
		}
		if (FCode.equalsIgnoreCase("BnfNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BnfNo = FValue.trim();
			}
			else
				BnfNo = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LLHospCaseSchema other = (LLHospCaseSchema)otherObject;
		return
			(CaseNo == null ? other.getCaseNo() == null : CaseNo.equals(other.getCaseNo()))
			&& (HospitCode == null ? other.getHospitCode() == null : HospitCode.equals(other.getHospitCode()))
			&& (HCNo == null ? other.getHCNo() == null : HCNo.equals(other.getHCNo()))
			&& (Handler == null ? other.getHandler() == null : Handler.equals(other.getHandler()))
			&& (AppTranNo == null ? other.getAppTranNo() == null : AppTranNo.equals(other.getAppTranNo()))
			&& (AppDate == null ? other.getAppDate() == null : fDate.getString(AppDate).equals(other.getAppDate()))
			&& (AppTime == null ? other.getAppTime() == null : AppTime.equals(other.getAppTime()))
			&& Realpay == other.getRealpay()
			&& (ConfirmTranNo == null ? other.getConfirmTranNo() == null : ConfirmTranNo.equals(other.getConfirmTranNo()))
			&& (ConfirmDate == null ? other.getConfirmDate() == null : fDate.getString(ConfirmDate).equals(other.getConfirmDate()))
			&& (ConfirmTime == null ? other.getConfirmTime() == null : ConfirmTime.equals(other.getConfirmTime()))
			&& (ConfirmState == null ? other.getConfirmState() == null : ConfirmState.equals(other.getConfirmState()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (DealType == null ? other.getDealType() == null : DealType.equals(other.getDealType()))
			&& (CaseType == null ? other.getCaseType() == null : CaseType.equals(other.getCaseType()))
			&& (ClaimNo == null ? other.getClaimNo() == null : ClaimNo.equals(other.getClaimNo()))
			&& (BnfNo == null ? other.getBnfNo() == null : BnfNo.equals(other.getBnfNo()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("CaseNo") ) {
			return 0;
		}
		if( strFieldName.equals("HospitCode") ) {
			return 1;
		}
		if( strFieldName.equals("HCNo") ) {
			return 2;
		}
		if( strFieldName.equals("Handler") ) {
			return 3;
		}
		if( strFieldName.equals("AppTranNo") ) {
			return 4;
		}
		if( strFieldName.equals("AppDate") ) {
			return 5;
		}
		if( strFieldName.equals("AppTime") ) {
			return 6;
		}
		if( strFieldName.equals("Realpay") ) {
			return 7;
		}
		if( strFieldName.equals("ConfirmTranNo") ) {
			return 8;
		}
		if( strFieldName.equals("ConfirmDate") ) {
			return 9;
		}
		if( strFieldName.equals("ConfirmTime") ) {
			return 10;
		}
		if( strFieldName.equals("ConfirmState") ) {
			return 11;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 12;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 13;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 14;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 15;
		}
		if( strFieldName.equals("DealType") ) {
			return 16;
		}
		if( strFieldName.equals("CaseType") ) {
			return 17;
		}
		if( strFieldName.equals("ClaimNo") ) {
			return 18;
		}
		if( strFieldName.equals("BnfNo") ) {
			return 19;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "CaseNo";
				break;
			case 1:
				strFieldName = "HospitCode";
				break;
			case 2:
				strFieldName = "HCNo";
				break;
			case 3:
				strFieldName = "Handler";
				break;
			case 4:
				strFieldName = "AppTranNo";
				break;
			case 5:
				strFieldName = "AppDate";
				break;
			case 6:
				strFieldName = "AppTime";
				break;
			case 7:
				strFieldName = "Realpay";
				break;
			case 8:
				strFieldName = "ConfirmTranNo";
				break;
			case 9:
				strFieldName = "ConfirmDate";
				break;
			case 10:
				strFieldName = "ConfirmTime";
				break;
			case 11:
				strFieldName = "ConfirmState";
				break;
			case 12:
				strFieldName = "MakeDate";
				break;
			case 13:
				strFieldName = "MakeTime";
				break;
			case 14:
				strFieldName = "ModifyDate";
				break;
			case 15:
				strFieldName = "ModifyTime";
				break;
			case 16:
				strFieldName = "DealType";
				break;
			case 17:
				strFieldName = "CaseType";
				break;
			case 18:
				strFieldName = "ClaimNo";
				break;
			case 19:
				strFieldName = "BnfNo";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("CaseNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HospitCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HCNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Handler") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppTranNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("AppTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Realpay") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ConfirmTranNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ConfirmDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ConfirmTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ConfirmState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DealType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CaseType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ClaimNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BnfNo") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
