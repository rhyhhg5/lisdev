/*
 * <p>ClassName: LOBUWSendTraceSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 核保上报轨迹表
 * @CreateDate：2005-02-01
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LOBUWSendTraceDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LOBUWSendTraceSchema implements Schema
{
    // @Field
    /** 合同号 */
    private String ContNo;
    /** 上报流水号 */
    private int UWNo;
    /** 上报标志 */
    private String SendFlag;
    /** 上报核保师编码 */
    private String UpUserCode;
    /** 上报核保师级别 */
    private String UpUWPopedom;
    /** 下级核保师编码 */
    private String DownUWCode;
    /** 下级核保师级别 */
    private String DownUWPopedom;
    /** 核保标记 */
    private String UWFlag;
    /** 核保结论 */
    private String UWIdea;
    /** 操作员 */
    private String Operator;
    /** 管理机构 */
    private String ManageCom;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 修改日期 */
    private Date ModifyDate;
    /** 修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 15; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LOBUWSendTraceSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "ContNo";
        pk[1] = "UWNo";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getContNo()
    {
        if (ContNo != null && !ContNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            ContNo = StrTool.unicodeToGBK(ContNo);
        }
        return ContNo;
    }

    public void setContNo(String aContNo)
    {
        ContNo = aContNo;
    }

    public int getUWNo()
    {
        return UWNo;
    }

    public void setUWNo(int aUWNo)
    {
        UWNo = aUWNo;
    }

    public void setUWNo(String aUWNo)
    {
        if (aUWNo != null && !aUWNo.equals(""))
        {
            Integer tInteger = new Integer(aUWNo);
            int i = tInteger.intValue();
            UWNo = i;
        }
    }

    public String getSendFlag()
    {
        if (SendFlag != null && !SendFlag.equals("") && SysConst.CHANGECHARSET == true)
        {
            SendFlag = StrTool.unicodeToGBK(SendFlag);
        }
        return SendFlag;
    }

    public void setSendFlag(String aSendFlag)
    {
        SendFlag = aSendFlag;
    }

    public String getUpUserCode()
    {
        if (UpUserCode != null && !UpUserCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            UpUserCode = StrTool.unicodeToGBK(UpUserCode);
        }
        return UpUserCode;
    }

    public void setUpUserCode(String aUpUserCode)
    {
        UpUserCode = aUpUserCode;
    }

    public String getUpUWPopedom()
    {
        if (UpUWPopedom != null && !UpUWPopedom.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            UpUWPopedom = StrTool.unicodeToGBK(UpUWPopedom);
        }
        return UpUWPopedom;
    }

    public void setUpUWPopedom(String aUpUWPopedom)
    {
        UpUWPopedom = aUpUWPopedom;
    }

    public String getDownUWCode()
    {
        if (DownUWCode != null && !DownUWCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            DownUWCode = StrTool.unicodeToGBK(DownUWCode);
        }
        return DownUWCode;
    }

    public void setDownUWCode(String aDownUWCode)
    {
        DownUWCode = aDownUWCode;
    }

    public String getDownUWPopedom()
    {
        if (DownUWPopedom != null && !DownUWPopedom.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            DownUWPopedom = StrTool.unicodeToGBK(DownUWPopedom);
        }
        return DownUWPopedom;
    }

    public void setDownUWPopedom(String aDownUWPopedom)
    {
        DownUWPopedom = aDownUWPopedom;
    }

    public String getUWFlag()
    {
        if (UWFlag != null && !UWFlag.equals("") && SysConst.CHANGECHARSET == true)
        {
            UWFlag = StrTool.unicodeToGBK(UWFlag);
        }
        return UWFlag;
    }

    public void setUWFlag(String aUWFlag)
    {
        UWFlag = aUWFlag;
    }

    public String getUWIdea()
    {
        if (UWIdea != null && !UWIdea.equals("") && SysConst.CHANGECHARSET == true)
        {
            UWIdea = StrTool.unicodeToGBK(UWIdea);
        }
        return UWIdea;
    }

    public void setUWIdea(String aUWIdea)
    {
        UWIdea = aUWIdea;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getManageCom()
    {
        if (ManageCom != null && !ManageCom.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ManageCom = StrTool.unicodeToGBK(ManageCom);
        }
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LOBUWSendTraceSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LOBUWSendTraceSchema aLOBUWSendTraceSchema)
    {
        this.ContNo = aLOBUWSendTraceSchema.getContNo();
        this.UWNo = aLOBUWSendTraceSchema.getUWNo();
        this.SendFlag = aLOBUWSendTraceSchema.getSendFlag();
        this.UpUserCode = aLOBUWSendTraceSchema.getUpUserCode();
        this.UpUWPopedom = aLOBUWSendTraceSchema.getUpUWPopedom();
        this.DownUWCode = aLOBUWSendTraceSchema.getDownUWCode();
        this.DownUWPopedom = aLOBUWSendTraceSchema.getDownUWPopedom();
        this.UWFlag = aLOBUWSendTraceSchema.getUWFlag();
        this.UWIdea = aLOBUWSendTraceSchema.getUWIdea();
        this.Operator = aLOBUWSendTraceSchema.getOperator();
        this.ManageCom = aLOBUWSendTraceSchema.getManageCom();
        this.MakeDate = fDate.getDate(aLOBUWSendTraceSchema.getMakeDate());
        this.MakeTime = aLOBUWSendTraceSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLOBUWSendTraceSchema.getModifyDate());
        this.ModifyTime = aLOBUWSendTraceSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ContNo") == null)
            {
                this.ContNo = null;
            }
            else
            {
                this.ContNo = rs.getString("ContNo").trim();
            }

            this.UWNo = rs.getInt("UWNo");
            if (rs.getString("SendFlag") == null)
            {
                this.SendFlag = null;
            }
            else
            {
                this.SendFlag = rs.getString("SendFlag").trim();
            }

            if (rs.getString("UpUserCode") == null)
            {
                this.UpUserCode = null;
            }
            else
            {
                this.UpUserCode = rs.getString("UpUserCode").trim();
            }

            if (rs.getString("UpUWPopedom") == null)
            {
                this.UpUWPopedom = null;
            }
            else
            {
                this.UpUWPopedom = rs.getString("UpUWPopedom").trim();
            }

            if (rs.getString("DownUWCode") == null)
            {
                this.DownUWCode = null;
            }
            else
            {
                this.DownUWCode = rs.getString("DownUWCode").trim();
            }

            if (rs.getString("DownUWPopedom") == null)
            {
                this.DownUWPopedom = null;
            }
            else
            {
                this.DownUWPopedom = rs.getString("DownUWPopedom").trim();
            }

            if (rs.getString("UWFlag") == null)
            {
                this.UWFlag = null;
            }
            else
            {
                this.UWFlag = rs.getString("UWFlag").trim();
            }

            if (rs.getString("UWIdea") == null)
            {
                this.UWIdea = null;
            }
            else
            {
                this.UWIdea = rs.getString("UWIdea").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LOBUWSendTraceSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LOBUWSendTraceSchema getSchema()
    {
        LOBUWSendTraceSchema aLOBUWSendTraceSchema = new LOBUWSendTraceSchema();
        aLOBUWSendTraceSchema.setSchema(this);
        return aLOBUWSendTraceSchema;
    }

    public LOBUWSendTraceDB getDB()
    {
        LOBUWSendTraceDB aDBOper = new LOBUWSendTraceDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOBUWSendTrace描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(ContNo)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(UWNo) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SendFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(UpUserCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(UpUWPopedom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(DownUWCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(DownUWPopedom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(UWFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(UWIdea)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ManageCom)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOBUWSendTrace>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                    SysConst.PACKAGESPILTER);
            UWNo = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    2, SysConst.PACKAGESPILTER))).intValue();
            SendFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            UpUserCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                        SysConst.PACKAGESPILTER);
            UpUWPopedom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                         SysConst.PACKAGESPILTER);
            DownUWCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                        SysConst.PACKAGESPILTER);
            DownUWPopedom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                           SysConst.PACKAGESPILTER);
            UWFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                    SysConst.PACKAGESPILTER);
            UWIdea = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                    SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                      SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                       SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 12, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 14, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LOBUWSendTraceSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("ContNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ContNo));
        }
        if (FCode.equals("UWNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(UWNo));
        }
        if (FCode.equals("SendFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SendFlag));
        }
        if (FCode.equals("UpUserCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(UpUserCode));
        }
        if (FCode.equals("UpUWPopedom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(UpUWPopedom));
        }
        if (FCode.equals("DownUWCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DownUWCode));
        }
        if (FCode.equals("DownUWPopedom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DownUWPopedom));
        }
        if (FCode.equals("UWFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(UWFlag));
        }
        if (FCode.equals("UWIdea"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(UWIdea));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ManageCom));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            case 1:
                strFieldValue = String.valueOf(UWNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(SendFlag);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(UpUserCode);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(UpUWPopedom);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(DownUWCode);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(DownUWPopedom);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(UWFlag);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(UWIdea);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("ContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
            {
                ContNo = null;
            }
        }
        if (FCode.equals("UWNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                UWNo = i;
            }
        }
        if (FCode.equals("SendFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SendFlag = FValue.trim();
            }
            else
            {
                SendFlag = null;
            }
        }
        if (FCode.equals("UpUserCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UpUserCode = FValue.trim();
            }
            else
            {
                UpUserCode = null;
            }
        }
        if (FCode.equals("UpUWPopedom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UpUWPopedom = FValue.trim();
            }
            else
            {
                UpUWPopedom = null;
            }
        }
        if (FCode.equals("DownUWCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DownUWCode = FValue.trim();
            }
            else
            {
                DownUWCode = null;
            }
        }
        if (FCode.equals("DownUWPopedom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DownUWPopedom = FValue.trim();
            }
            else
            {
                DownUWPopedom = null;
            }
        }
        if (FCode.equals("UWFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UWFlag = FValue.trim();
            }
            else
            {
                UWFlag = null;
            }
        }
        if (FCode.equals("UWIdea"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UWIdea = FValue.trim();
            }
            else
            {
                UWIdea = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LOBUWSendTraceSchema other = (LOBUWSendTraceSchema) otherObject;
        return
                ContNo.equals(other.getContNo())
                && UWNo == other.getUWNo()
                && SendFlag.equals(other.getSendFlag())
                && UpUserCode.equals(other.getUpUserCode())
                && UpUWPopedom.equals(other.getUpUWPopedom())
                && DownUWCode.equals(other.getDownUWCode())
                && DownUWPopedom.equals(other.getDownUWPopedom())
                && UWFlag.equals(other.getUWFlag())
                && UWIdea.equals(other.getUWIdea())
                && Operator.equals(other.getOperator())
                && ManageCom.equals(other.getManageCom())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("ContNo"))
        {
            return 0;
        }
        if (strFieldName.equals("UWNo"))
        {
            return 1;
        }
        if (strFieldName.equals("SendFlag"))
        {
            return 2;
        }
        if (strFieldName.equals("UpUserCode"))
        {
            return 3;
        }
        if (strFieldName.equals("UpUWPopedom"))
        {
            return 4;
        }
        if (strFieldName.equals("DownUWCode"))
        {
            return 5;
        }
        if (strFieldName.equals("DownUWPopedom"))
        {
            return 6;
        }
        if (strFieldName.equals("UWFlag"))
        {
            return 7;
        }
        if (strFieldName.equals("UWIdea"))
        {
            return 8;
        }
        if (strFieldName.equals("Operator"))
        {
            return 9;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return 10;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 11;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 12;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 13;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 14;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "ContNo";
                break;
            case 1:
                strFieldName = "UWNo";
                break;
            case 2:
                strFieldName = "SendFlag";
                break;
            case 3:
                strFieldName = "UpUserCode";
                break;
            case 4:
                strFieldName = "UpUWPopedom";
                break;
            case 5:
                strFieldName = "DownUWCode";
                break;
            case 6:
                strFieldName = "DownUWPopedom";
                break;
            case 7:
                strFieldName = "UWFlag";
                break;
            case 8:
                strFieldName = "UWIdea";
                break;
            case 9:
                strFieldName = "Operator";
                break;
            case 10:
                strFieldName = "ManageCom";
                break;
            case 11:
                strFieldName = "MakeDate";
                break;
            case 12:
                strFieldName = "MakeTime";
                break;
            case 13:
                strFieldName = "ModifyDate";
                break;
            case 14:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("ContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UWNo"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("SendFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UpUserCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UpUWPopedom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DownUWCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DownUWPopedom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UWFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UWIdea"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_INT;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
