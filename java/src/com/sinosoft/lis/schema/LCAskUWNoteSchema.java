/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LCAskUWNoteDB;

/*
 * <p>ClassName: LCAskUWNoteSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-10-24
 */
public class LCAskUWNoteSchema implements Schema, Cloneable {
    // @Field
    /** 请求次数 */
    private String NoteID;
    /** 印刷号 */
    private String PrtNo;
    /** 集体合同号码 */
    private String GrpContNo;
    /** 核保意见 */
    private String UWPlan;
    /** 核保操作员 */
    private String UWOperator;
    /** 产品意见 */
    private String ProPlan;
    /** 产品操作员 */
    private String ProOperator;
    /** 状态 */
    private String StateFlag;
    /** 管理机构 */
    private String MngCom;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 13; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LCAskUWNoteSchema() {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "NoteID";
        pk[1] = "PrtNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LCAskUWNoteSchema cloned = (LCAskUWNoteSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getNoteID() {
        return NoteID;
    }

    public void setNoteID(String aNoteID) {
        NoteID = aNoteID;
    }

    public String getPrtNo() {
        return PrtNo;
    }

    public void setPrtNo(String aPrtNo) {
        PrtNo = aPrtNo;
    }

    public String getGrpContNo() {
        return GrpContNo;
    }

    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }

    public String getUWPlan() {
        return UWPlan;
    }

    public void setUWPlan(String aUWPlan) {
        UWPlan = aUWPlan;
    }

    public String getUWOperator() {
        return UWOperator;
    }

    public void setUWOperator(String aUWOperator) {
        UWOperator = aUWOperator;
    }

    public String getProPlan() {
        return ProPlan;
    }

    public void setProPlan(String aProPlan) {
        ProPlan = aProPlan;
    }

    public String getProOperator() {
        return ProOperator;
    }

    public void setProOperator(String aProOperator) {
        ProOperator = aProOperator;
    }

    public String getStateFlag() {
        return StateFlag;
    }

    public void setStateFlag(String aStateFlag) {
        StateFlag = aStateFlag;
    }

    public String getMngCom() {
        return MngCom;
    }

    public void setMngCom(String aMngCom) {
        MngCom = aMngCom;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LCAskUWNoteSchema 对象给 Schema 赋值
     * @param: aLCAskUWNoteSchema LCAskUWNoteSchema
     **/
    public void setSchema(LCAskUWNoteSchema aLCAskUWNoteSchema) {
        this.NoteID = aLCAskUWNoteSchema.getNoteID();
        this.PrtNo = aLCAskUWNoteSchema.getPrtNo();
        this.GrpContNo = aLCAskUWNoteSchema.getGrpContNo();
        this.UWPlan = aLCAskUWNoteSchema.getUWPlan();
        this.UWOperator = aLCAskUWNoteSchema.getUWOperator();
        this.ProPlan = aLCAskUWNoteSchema.getProPlan();
        this.ProOperator = aLCAskUWNoteSchema.getProOperator();
        this.StateFlag = aLCAskUWNoteSchema.getStateFlag();
        this.MngCom = aLCAskUWNoteSchema.getMngCom();
        this.MakeDate = fDate.getDate(aLCAskUWNoteSchema.getMakeDate());
        this.MakeTime = aLCAskUWNoteSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLCAskUWNoteSchema.getModifyDate());
        this.ModifyTime = aLCAskUWNoteSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("NoteID") == null) {
                this.NoteID = null;
            } else {
                this.NoteID = rs.getString("NoteID").trim();
            }

            if (rs.getString("PrtNo") == null) {
                this.PrtNo = null;
            } else {
                this.PrtNo = rs.getString("PrtNo").trim();
            }

            if (rs.getString("GrpContNo") == null) {
                this.GrpContNo = null;
            } else {
                this.GrpContNo = rs.getString("GrpContNo").trim();
            }

            if (rs.getString("UWPlan") == null) {
                this.UWPlan = null;
            } else {
                this.UWPlan = rs.getString("UWPlan").trim();
            }

            if (rs.getString("UWOperator") == null) {
                this.UWOperator = null;
            } else {
                this.UWOperator = rs.getString("UWOperator").trim();
            }

            if (rs.getString("ProPlan") == null) {
                this.ProPlan = null;
            } else {
                this.ProPlan = rs.getString("ProPlan").trim();
            }

            if (rs.getString("ProOperator") == null) {
                this.ProOperator = null;
            } else {
                this.ProOperator = rs.getString("ProOperator").trim();
            }

            if (rs.getString("StateFlag") == null) {
                this.StateFlag = null;
            } else {
                this.StateFlag = rs.getString("StateFlag").trim();
            }

            if (rs.getString("MngCom") == null) {
                this.MngCom = null;
            } else {
                this.MngCom = rs.getString("MngCom").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LCAskUWNote表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAskUWNoteSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LCAskUWNoteSchema getSchema() {
        LCAskUWNoteSchema aLCAskUWNoteSchema = new LCAskUWNoteSchema();
        aLCAskUWNoteSchema.setSchema(this);
        return aLCAskUWNoteSchema;
    }

    public LCAskUWNoteDB getDB() {
        LCAskUWNoteDB aDBOper = new LCAskUWNoteDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCAskUWNote描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(NoteID));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PrtNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpContNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UWPlan));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UWOperator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ProPlan));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ProOperator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StateFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MngCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCAskUWNote>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            NoteID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                    SysConst.PACKAGESPILTER);
            PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                   SysConst.PACKAGESPILTER);
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                       SysConst.PACKAGESPILTER);
            UWPlan = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                    SysConst.PACKAGESPILTER);
            UWOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                        SysConst.PACKAGESPILTER);
            ProPlan = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                     SysConst.PACKAGESPILTER);
            ProOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                         SysConst.PACKAGESPILTER);
            StateFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                       SysConst.PACKAGESPILTER);
            MngCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                    SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 10, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 12, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                        SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAskUWNoteSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("NoteID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NoteID));
        }
        if (FCode.equals("PrtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equals("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equals("UWPlan")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWPlan));
        }
        if (FCode.equals("UWOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWOperator));
        }
        if (FCode.equals("ProPlan")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProPlan));
        }
        if (FCode.equals("ProOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProOperator));
        }
        if (FCode.equals("StateFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StateFlag));
        }
        if (FCode.equals("MngCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MngCom));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(NoteID);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(PrtNo);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(GrpContNo);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(UWPlan);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(UWOperator);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(ProPlan);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(ProOperator);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(StateFlag);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(MngCom);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("NoteID")) {
            if (FValue != null && !FValue.equals("")) {
                NoteID = FValue.trim();
            } else {
                NoteID = null;
            }
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            if (FValue != null && !FValue.equals("")) {
                PrtNo = FValue.trim();
            } else {
                PrtNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if (FValue != null && !FValue.equals("")) {
                GrpContNo = FValue.trim();
            } else {
                GrpContNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("UWPlan")) {
            if (FValue != null && !FValue.equals("")) {
                UWPlan = FValue.trim();
            } else {
                UWPlan = null;
            }
        }
        if (FCode.equalsIgnoreCase("UWOperator")) {
            if (FValue != null && !FValue.equals("")) {
                UWOperator = FValue.trim();
            } else {
                UWOperator = null;
            }
        }
        if (FCode.equalsIgnoreCase("ProPlan")) {
            if (FValue != null && !FValue.equals("")) {
                ProPlan = FValue.trim();
            } else {
                ProPlan = null;
            }
        }
        if (FCode.equalsIgnoreCase("ProOperator")) {
            if (FValue != null && !FValue.equals("")) {
                ProOperator = FValue.trim();
            } else {
                ProOperator = null;
            }
        }
        if (FCode.equalsIgnoreCase("StateFlag")) {
            if (FValue != null && !FValue.equals("")) {
                StateFlag = FValue.trim();
            } else {
                StateFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("MngCom")) {
            if (FValue != null && !FValue.equals("")) {
                MngCom = FValue.trim();
            } else {
                MngCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LCAskUWNoteSchema other = (LCAskUWNoteSchema) otherObject;
        return
                NoteID.equals(other.getNoteID())
                && PrtNo.equals(other.getPrtNo())
                && GrpContNo.equals(other.getGrpContNo())
                && UWPlan.equals(other.getUWPlan())
                && UWOperator.equals(other.getUWOperator())
                && ProPlan.equals(other.getProPlan())
                && ProOperator.equals(other.getProOperator())
                && StateFlag.equals(other.getStateFlag())
                && MngCom.equals(other.getMngCom())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("NoteID")) {
            return 0;
        }
        if (strFieldName.equals("PrtNo")) {
            return 1;
        }
        if (strFieldName.equals("GrpContNo")) {
            return 2;
        }
        if (strFieldName.equals("UWPlan")) {
            return 3;
        }
        if (strFieldName.equals("UWOperator")) {
            return 4;
        }
        if (strFieldName.equals("ProPlan")) {
            return 5;
        }
        if (strFieldName.equals("ProOperator")) {
            return 6;
        }
        if (strFieldName.equals("StateFlag")) {
            return 7;
        }
        if (strFieldName.equals("MngCom")) {
            return 8;
        }
        if (strFieldName.equals("MakeDate")) {
            return 9;
        }
        if (strFieldName.equals("MakeTime")) {
            return 10;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 11;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 12;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "NoteID";
            break;
        case 1:
            strFieldName = "PrtNo";
            break;
        case 2:
            strFieldName = "GrpContNo";
            break;
        case 3:
            strFieldName = "UWPlan";
            break;
        case 4:
            strFieldName = "UWOperator";
            break;
        case 5:
            strFieldName = "ProPlan";
            break;
        case 6:
            strFieldName = "ProOperator";
            break;
        case 7:
            strFieldName = "StateFlag";
            break;
        case 8:
            strFieldName = "MngCom";
            break;
        case 9:
            strFieldName = "MakeDate";
            break;
        case 10:
            strFieldName = "MakeTime";
            break;
        case 11:
            strFieldName = "ModifyDate";
            break;
        case 12:
            strFieldName = "ModifyTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("NoteID")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PrtNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpContNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UWPlan")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UWOperator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ProPlan")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ProOperator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StateFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MngCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 12:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
