/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LRComRiskResultDB;

/*
 * <p>ClassName: LRComRiskResultSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 再保财务接口
 * @CreateDate：2008-10-08
 */
public class LRComRiskResultSchema implements Schema, Cloneable
{
	// @Field
	/** 机构名称 */
	private String ManageCom;
	/** 险种编码 */
	private String RiskCode;
	/** 再保合同号 */
	private String RecontCode;
	/** 统计起始日期 */
	private Date StartDate;
	/** 统计结束日期 */
	private Date EndData;
	/** 成本中心编码 */
	private String CostCenter;
	/** 分保保费 */
	private double CessPrem;
	/** 退保摊回保费 */
	private double EdorBackFee;
	/** 分保保费合计 */
	private double SumPrem;
	/** 分保手续费 */
	private double ReProcFee;
	/** 退保摊回手续费 */
	private double EdorProcFee;
	/** 手续费合计 */
	private double SumFee;
	/** 理赔摊回 */
	private double ClaimBackFee;
	/** 备用字段1 */
	private String StandbyFlag1;
	/** 备用字段2 */
	private String StandbyFlag2;
	/** 备用字段3 */
	private double StandbyFlag3;
	/** 备用字段4 */
	private double StandbyFlag4;
	/** 备用字段5 */
	private Date StandbyFlag5;
	/** 备用字段6 */
	private Date StandbyFlag6;
	/** 可更改否 */
	private String ModifyFlag;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 修改日期 */
	private Date ModifyDate;
	/** 修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 25;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LRComRiskResultSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[6];
		pk[0] = "ManageCom";
		pk[1] = "RiskCode";
		pk[2] = "RecontCode";
		pk[3] = "StartDate";
		pk[4] = "EndData";
		pk[5] = "CostCenter";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LRComRiskResultSchema cloned = (LRComRiskResultSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getRiskCode()
	{
		return RiskCode;
	}
	public void setRiskCode(String aRiskCode)
	{
		RiskCode = aRiskCode;
	}
	public String getRecontCode()
	{
		return RecontCode;
	}
	public void setRecontCode(String aRecontCode)
	{
		RecontCode = aRecontCode;
	}
	public String getStartDate()
	{
		if( StartDate != null )
			return fDate.getString(StartDate);
		else
			return null;
	}
	public void setStartDate(Date aStartDate)
	{
		StartDate = aStartDate;
	}
	public void setStartDate(String aStartDate)
	{
		if (aStartDate != null && !aStartDate.equals("") )
		{
			StartDate = fDate.getDate( aStartDate );
		}
		else
			StartDate = null;
	}

	public String getEndData()
	{
		if( EndData != null )
			return fDate.getString(EndData);
		else
			return null;
	}
	public void setEndData(Date aEndData)
	{
		EndData = aEndData;
	}
	public void setEndData(String aEndData)
	{
		if (aEndData != null && !aEndData.equals("") )
		{
			EndData = fDate.getDate( aEndData );
		}
		else
			EndData = null;
	}

	public String getCostCenter()
	{
		return CostCenter;
	}
	public void setCostCenter(String aCostCenter)
	{
		CostCenter = aCostCenter;
	}
	public double getCessPrem()
	{
		return CessPrem;
	}
	public void setCessPrem(double aCessPrem)
	{
		CessPrem = Arith.round(aCessPrem,2);
	}
	public void setCessPrem(String aCessPrem)
	{
		if (aCessPrem != null && !aCessPrem.equals(""))
		{
			Double tDouble = new Double(aCessPrem);
			double d = tDouble.doubleValue();
                CessPrem = Arith.round(d,2);
		}
	}

	public double getEdorBackFee()
	{
		return EdorBackFee;
	}
	public void setEdorBackFee(double aEdorBackFee)
	{
		EdorBackFee = Arith.round(aEdorBackFee,2);
	}
	public void setEdorBackFee(String aEdorBackFee)
	{
		if (aEdorBackFee != null && !aEdorBackFee.equals(""))
		{
			Double tDouble = new Double(aEdorBackFee);
			double d = tDouble.doubleValue();
                EdorBackFee = Arith.round(d,2);
		}
	}

	public double getSumPrem()
	{
		return SumPrem;
	}
	public void setSumPrem(double aSumPrem)
	{
		SumPrem = Arith.round(aSumPrem,2);
	}
	public void setSumPrem(String aSumPrem)
	{
		if (aSumPrem != null && !aSumPrem.equals(""))
		{
			Double tDouble = new Double(aSumPrem);
			double d = tDouble.doubleValue();
                SumPrem = Arith.round(d,2);
		}
	}

	public double getReProcFee()
	{
		return ReProcFee;
	}
	public void setReProcFee(double aReProcFee)
	{
		ReProcFee = Arith.round(aReProcFee,2);
	}
	public void setReProcFee(String aReProcFee)
	{
		if (aReProcFee != null && !aReProcFee.equals(""))
		{
			Double tDouble = new Double(aReProcFee);
			double d = tDouble.doubleValue();
                ReProcFee = Arith.round(d,2);
		}
	}

	public double getEdorProcFee()
	{
		return EdorProcFee;
	}
	public void setEdorProcFee(double aEdorProcFee)
	{
		EdorProcFee = Arith.round(aEdorProcFee,2);
	}
	public void setEdorProcFee(String aEdorProcFee)
	{
		if (aEdorProcFee != null && !aEdorProcFee.equals(""))
		{
			Double tDouble = new Double(aEdorProcFee);
			double d = tDouble.doubleValue();
                EdorProcFee = Arith.round(d,2);
		}
	}

	public double getSumFee()
	{
		return SumFee;
	}
	public void setSumFee(double aSumFee)
	{
		SumFee = Arith.round(aSumFee,2);
	}
	public void setSumFee(String aSumFee)
	{
		if (aSumFee != null && !aSumFee.equals(""))
		{
			Double tDouble = new Double(aSumFee);
			double d = tDouble.doubleValue();
                SumFee = Arith.round(d,2);
		}
	}

	public double getClaimBackFee()
	{
		return ClaimBackFee;
	}
	public void setClaimBackFee(double aClaimBackFee)
	{
		ClaimBackFee = Arith.round(aClaimBackFee,2);
	}
	public void setClaimBackFee(String aClaimBackFee)
	{
		if (aClaimBackFee != null && !aClaimBackFee.equals(""))
		{
			Double tDouble = new Double(aClaimBackFee);
			double d = tDouble.doubleValue();
                ClaimBackFee = Arith.round(d,2);
		}
	}

	public String getStandbyFlag1()
	{
		return StandbyFlag1;
	}
	public void setStandbyFlag1(String aStandbyFlag1)
	{
		StandbyFlag1 = aStandbyFlag1;
	}
	public String getStandbyFlag2()
	{
		return StandbyFlag2;
	}
	public void setStandbyFlag2(String aStandbyFlag2)
	{
		StandbyFlag2 = aStandbyFlag2;
	}
	public double getStandbyFlag3()
	{
		return StandbyFlag3;
	}
	public void setStandbyFlag3(double aStandbyFlag3)
	{
		StandbyFlag3 = Arith.round(aStandbyFlag3,2);
	}
	public void setStandbyFlag3(String aStandbyFlag3)
	{
		if (aStandbyFlag3 != null && !aStandbyFlag3.equals(""))
		{
			Double tDouble = new Double(aStandbyFlag3);
			double d = tDouble.doubleValue();
                StandbyFlag3 = Arith.round(d,2);
		}
	}

	public double getStandbyFlag4()
	{
		return StandbyFlag4;
	}
	public void setStandbyFlag4(double aStandbyFlag4)
	{
		StandbyFlag4 = Arith.round(aStandbyFlag4,2);
	}
	public void setStandbyFlag4(String aStandbyFlag4)
	{
		if (aStandbyFlag4 != null && !aStandbyFlag4.equals(""))
		{
			Double tDouble = new Double(aStandbyFlag4);
			double d = tDouble.doubleValue();
                StandbyFlag4 = Arith.round(d,2);
		}
	}

	public String getStandbyFlag5()
	{
		if( StandbyFlag5 != null )
			return fDate.getString(StandbyFlag5);
		else
			return null;
	}
	public void setStandbyFlag5(Date aStandbyFlag5)
	{
		StandbyFlag5 = aStandbyFlag5;
	}
	public void setStandbyFlag5(String aStandbyFlag5)
	{
		if (aStandbyFlag5 != null && !aStandbyFlag5.equals("") )
		{
			StandbyFlag5 = fDate.getDate( aStandbyFlag5 );
		}
		else
			StandbyFlag5 = null;
	}

	public String getStandbyFlag6()
	{
		if( StandbyFlag6 != null )
			return fDate.getString(StandbyFlag6);
		else
			return null;
	}
	public void setStandbyFlag6(Date aStandbyFlag6)
	{
		StandbyFlag6 = aStandbyFlag6;
	}
	public void setStandbyFlag6(String aStandbyFlag6)
	{
		if (aStandbyFlag6 != null && !aStandbyFlag6.equals("") )
		{
			StandbyFlag6 = fDate.getDate( aStandbyFlag6 );
		}
		else
			StandbyFlag6 = null;
	}

	public String getModifyFlag()
	{
		return ModifyFlag;
	}
	public void setModifyFlag(String aModifyFlag)
	{
		ModifyFlag = aModifyFlag;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LRComRiskResultSchema 对象给 Schema 赋值
	* @param: aLRComRiskResultSchema LRComRiskResultSchema
	**/
	public void setSchema(LRComRiskResultSchema aLRComRiskResultSchema)
	{
		this.ManageCom = aLRComRiskResultSchema.getManageCom();
		this.RiskCode = aLRComRiskResultSchema.getRiskCode();
		this.RecontCode = aLRComRiskResultSchema.getRecontCode();
		this.StartDate = fDate.getDate( aLRComRiskResultSchema.getStartDate());
		this.EndData = fDate.getDate( aLRComRiskResultSchema.getEndData());
		this.CostCenter = aLRComRiskResultSchema.getCostCenter();
		this.CessPrem = aLRComRiskResultSchema.getCessPrem();
		this.EdorBackFee = aLRComRiskResultSchema.getEdorBackFee();
		this.SumPrem = aLRComRiskResultSchema.getSumPrem();
		this.ReProcFee = aLRComRiskResultSchema.getReProcFee();
		this.EdorProcFee = aLRComRiskResultSchema.getEdorProcFee();
		this.SumFee = aLRComRiskResultSchema.getSumFee();
		this.ClaimBackFee = aLRComRiskResultSchema.getClaimBackFee();
		this.StandbyFlag1 = aLRComRiskResultSchema.getStandbyFlag1();
		this.StandbyFlag2 = aLRComRiskResultSchema.getStandbyFlag2();
		this.StandbyFlag3 = aLRComRiskResultSchema.getStandbyFlag3();
		this.StandbyFlag4 = aLRComRiskResultSchema.getStandbyFlag4();
		this.StandbyFlag5 = fDate.getDate( aLRComRiskResultSchema.getStandbyFlag5());
		this.StandbyFlag6 = fDate.getDate( aLRComRiskResultSchema.getStandbyFlag6());
		this.ModifyFlag = aLRComRiskResultSchema.getModifyFlag();
		this.Operator = aLRComRiskResultSchema.getOperator();
		this.MakeDate = fDate.getDate( aLRComRiskResultSchema.getMakeDate());
		this.MakeTime = aLRComRiskResultSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLRComRiskResultSchema.getModifyDate());
		this.ModifyTime = aLRComRiskResultSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("RiskCode") == null )
				this.RiskCode = null;
			else
				this.RiskCode = rs.getString("RiskCode").trim();

			if( rs.getString("RecontCode") == null )
				this.RecontCode = null;
			else
				this.RecontCode = rs.getString("RecontCode").trim();

			this.StartDate = rs.getDate("StartDate");
			this.EndData = rs.getDate("EndData");
			if( rs.getString("CostCenter") == null )
				this.CostCenter = null;
			else
				this.CostCenter = rs.getString("CostCenter").trim();

			this.CessPrem = rs.getDouble("CessPrem");
			this.EdorBackFee = rs.getDouble("EdorBackFee");
			this.SumPrem = rs.getDouble("SumPrem");
			this.ReProcFee = rs.getDouble("ReProcFee");
			this.EdorProcFee = rs.getDouble("EdorProcFee");
			this.SumFee = rs.getDouble("SumFee");
			this.ClaimBackFee = rs.getDouble("ClaimBackFee");
			if( rs.getString("StandbyFlag1") == null )
				this.StandbyFlag1 = null;
			else
				this.StandbyFlag1 = rs.getString("StandbyFlag1").trim();

			if( rs.getString("StandbyFlag2") == null )
				this.StandbyFlag2 = null;
			else
				this.StandbyFlag2 = rs.getString("StandbyFlag2").trim();

			this.StandbyFlag3 = rs.getDouble("StandbyFlag3");
			this.StandbyFlag4 = rs.getDouble("StandbyFlag4");
			this.StandbyFlag5 = rs.getDate("StandbyFlag5");
			this.StandbyFlag6 = rs.getDate("StandbyFlag6");
			if( rs.getString("ModifyFlag") == null )
				this.ModifyFlag = null;
			else
				this.ModifyFlag = rs.getString("ModifyFlag").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LRComRiskResult表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LRComRiskResultSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LRComRiskResultSchema getSchema()
	{
		LRComRiskResultSchema aLRComRiskResultSchema = new LRComRiskResultSchema();
		aLRComRiskResultSchema.setSchema(this);
		return aLRComRiskResultSchema;
	}

	public LRComRiskResultDB getDB()
	{
		LRComRiskResultDB aDBOper = new LRComRiskResultDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLRComRiskResult描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RecontCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( StartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( EndData ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CostCenter)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(CessPrem));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(EdorBackFee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SumPrem));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ReProcFee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(EdorProcFee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SumFee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ClaimBackFee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandbyFlag1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandbyFlag2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(StandbyFlag3));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(StandbyFlag4));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( StandbyFlag5 ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( StandbyFlag6 ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLRComRiskResult>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			RecontCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			StartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,SysConst.PACKAGESPILTER));
			EndData = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,SysConst.PACKAGESPILTER));
			CostCenter = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			CessPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,7,SysConst.PACKAGESPILTER))).doubleValue();
			EdorBackFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,8,SysConst.PACKAGESPILTER))).doubleValue();
			SumPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,9,SysConst.PACKAGESPILTER))).doubleValue();
			ReProcFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,10,SysConst.PACKAGESPILTER))).doubleValue();
			EdorProcFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,11,SysConst.PACKAGESPILTER))).doubleValue();
			SumFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,12,SysConst.PACKAGESPILTER))).doubleValue();
			ClaimBackFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,13,SysConst.PACKAGESPILTER))).doubleValue();
			StandbyFlag1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			StandbyFlag2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			StandbyFlag3 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,16,SysConst.PACKAGESPILTER))).doubleValue();
			StandbyFlag4 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,17,SysConst.PACKAGESPILTER))).doubleValue();
			StandbyFlag5 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,SysConst.PACKAGESPILTER));
			StandbyFlag6 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,SysConst.PACKAGESPILTER));
			ModifyFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LRComRiskResultSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("RiskCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
		}
		if (FCode.equals("RecontCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RecontCode));
		}
		if (FCode.equals("StartDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStartDate()));
		}
		if (FCode.equals("EndData"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEndData()));
		}
		if (FCode.equals("CostCenter"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CostCenter));
		}
		if (FCode.equals("CessPrem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CessPrem));
		}
		if (FCode.equals("EdorBackFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EdorBackFee));
		}
		if (FCode.equals("SumPrem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SumPrem));
		}
		if (FCode.equals("ReProcFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReProcFee));
		}
		if (FCode.equals("EdorProcFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EdorProcFee));
		}
		if (FCode.equals("SumFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SumFee));
		}
		if (FCode.equals("ClaimBackFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClaimBackFee));
		}
		if (FCode.equals("StandbyFlag1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag1));
		}
		if (FCode.equals("StandbyFlag2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag2));
		}
		if (FCode.equals("StandbyFlag3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag3));
		}
		if (FCode.equals("StandbyFlag4"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag4));
		}
		if (FCode.equals("StandbyFlag5"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStandbyFlag5()));
		}
		if (FCode.equals("StandbyFlag6"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStandbyFlag6()));
		}
		if (FCode.equals("ModifyFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyFlag));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(RiskCode);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(RecontCode);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStartDate()));
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEndData()));
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(CostCenter);
				break;
			case 6:
				strFieldValue = String.valueOf(CessPrem);
				break;
			case 7:
				strFieldValue = String.valueOf(EdorBackFee);
				break;
			case 8:
				strFieldValue = String.valueOf(SumPrem);
				break;
			case 9:
				strFieldValue = String.valueOf(ReProcFee);
				break;
			case 10:
				strFieldValue = String.valueOf(EdorProcFee);
				break;
			case 11:
				strFieldValue = String.valueOf(SumFee);
				break;
			case 12:
				strFieldValue = String.valueOf(ClaimBackFee);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(StandbyFlag1);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(StandbyFlag2);
				break;
			case 15:
				strFieldValue = String.valueOf(StandbyFlag3);
				break;
			case 16:
				strFieldValue = String.valueOf(StandbyFlag4);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStandbyFlag5()));
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStandbyFlag6()));
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(ModifyFlag);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("RiskCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskCode = FValue.trim();
			}
			else
				RiskCode = null;
		}
		if (FCode.equalsIgnoreCase("RecontCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RecontCode = FValue.trim();
			}
			else
				RecontCode = null;
		}
		if (FCode.equalsIgnoreCase("StartDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				StartDate = fDate.getDate( FValue );
			}
			else
				StartDate = null;
		}
		if (FCode.equalsIgnoreCase("EndData"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				EndData = fDate.getDate( FValue );
			}
			else
				EndData = null;
		}
		if (FCode.equalsIgnoreCase("CostCenter"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CostCenter = FValue.trim();
			}
			else
				CostCenter = null;
		}
		if (FCode.equalsIgnoreCase("CessPrem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				CessPrem = d;
			}
		}
		if (FCode.equalsIgnoreCase("EdorBackFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				EdorBackFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("SumPrem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SumPrem = d;
			}
		}
		if (FCode.equalsIgnoreCase("ReProcFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ReProcFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("EdorProcFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				EdorProcFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("SumFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SumFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("ClaimBackFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ClaimBackFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("StandbyFlag1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandbyFlag1 = FValue.trim();
			}
			else
				StandbyFlag1 = null;
		}
		if (FCode.equalsIgnoreCase("StandbyFlag2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandbyFlag2 = FValue.trim();
			}
			else
				StandbyFlag2 = null;
		}
		if (FCode.equalsIgnoreCase("StandbyFlag3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				StandbyFlag3 = d;
			}
		}
		if (FCode.equalsIgnoreCase("StandbyFlag4"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				StandbyFlag4 = d;
			}
		}
		if (FCode.equalsIgnoreCase("StandbyFlag5"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				StandbyFlag5 = fDate.getDate( FValue );
			}
			else
				StandbyFlag5 = null;
		}
		if (FCode.equalsIgnoreCase("StandbyFlag6"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				StandbyFlag6 = fDate.getDate( FValue );
			}
			else
				StandbyFlag6 = null;
		}
		if (FCode.equalsIgnoreCase("ModifyFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyFlag = FValue.trim();
			}
			else
				ModifyFlag = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LRComRiskResultSchema other = (LRComRiskResultSchema)otherObject;
		return
			ManageCom.equals(other.getManageCom())
			&& RiskCode.equals(other.getRiskCode())
			&& RecontCode.equals(other.getRecontCode())
			&& fDate.getString(StartDate).equals(other.getStartDate())
			&& fDate.getString(EndData).equals(other.getEndData())
			&& CostCenter.equals(other.getCostCenter())
			&& CessPrem == other.getCessPrem()
			&& EdorBackFee == other.getEdorBackFee()
			&& SumPrem == other.getSumPrem()
			&& ReProcFee == other.getReProcFee()
			&& EdorProcFee == other.getEdorProcFee()
			&& SumFee == other.getSumFee()
			&& ClaimBackFee == other.getClaimBackFee()
			&& StandbyFlag1.equals(other.getStandbyFlag1())
			&& StandbyFlag2.equals(other.getStandbyFlag2())
			&& StandbyFlag3 == other.getStandbyFlag3()
			&& StandbyFlag4 == other.getStandbyFlag4()
			&& fDate.getString(StandbyFlag5).equals(other.getStandbyFlag5())
			&& fDate.getString(StandbyFlag6).equals(other.getStandbyFlag6())
			&& ModifyFlag.equals(other.getModifyFlag())
			&& Operator.equals(other.getOperator())
			&& fDate.getString(MakeDate).equals(other.getMakeDate())
			&& MakeTime.equals(other.getMakeTime())
			&& fDate.getString(ModifyDate).equals(other.getModifyDate())
			&& ModifyTime.equals(other.getModifyTime());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("ManageCom") ) {
			return 0;
		}
		if( strFieldName.equals("RiskCode") ) {
			return 1;
		}
		if( strFieldName.equals("RecontCode") ) {
			return 2;
		}
		if( strFieldName.equals("StartDate") ) {
			return 3;
		}
		if( strFieldName.equals("EndData") ) {
			return 4;
		}
		if( strFieldName.equals("CostCenter") ) {
			return 5;
		}
		if( strFieldName.equals("CessPrem") ) {
			return 6;
		}
		if( strFieldName.equals("EdorBackFee") ) {
			return 7;
		}
		if( strFieldName.equals("SumPrem") ) {
			return 8;
		}
		if( strFieldName.equals("ReProcFee") ) {
			return 9;
		}
		if( strFieldName.equals("EdorProcFee") ) {
			return 10;
		}
		if( strFieldName.equals("SumFee") ) {
			return 11;
		}
		if( strFieldName.equals("ClaimBackFee") ) {
			return 12;
		}
		if( strFieldName.equals("StandbyFlag1") ) {
			return 13;
		}
		if( strFieldName.equals("StandbyFlag2") ) {
			return 14;
		}
		if( strFieldName.equals("StandbyFlag3") ) {
			return 15;
		}
		if( strFieldName.equals("StandbyFlag4") ) {
			return 16;
		}
		if( strFieldName.equals("StandbyFlag5") ) {
			return 17;
		}
		if( strFieldName.equals("StandbyFlag6") ) {
			return 18;
		}
		if( strFieldName.equals("ModifyFlag") ) {
			return 19;
		}
		if( strFieldName.equals("Operator") ) {
			return 20;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 21;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 22;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 23;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 24;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "ManageCom";
				break;
			case 1:
				strFieldName = "RiskCode";
				break;
			case 2:
				strFieldName = "RecontCode";
				break;
			case 3:
				strFieldName = "StartDate";
				break;
			case 4:
				strFieldName = "EndData";
				break;
			case 5:
				strFieldName = "CostCenter";
				break;
			case 6:
				strFieldName = "CessPrem";
				break;
			case 7:
				strFieldName = "EdorBackFee";
				break;
			case 8:
				strFieldName = "SumPrem";
				break;
			case 9:
				strFieldName = "ReProcFee";
				break;
			case 10:
				strFieldName = "EdorProcFee";
				break;
			case 11:
				strFieldName = "SumFee";
				break;
			case 12:
				strFieldName = "ClaimBackFee";
				break;
			case 13:
				strFieldName = "StandbyFlag1";
				break;
			case 14:
				strFieldName = "StandbyFlag2";
				break;
			case 15:
				strFieldName = "StandbyFlag3";
				break;
			case 16:
				strFieldName = "StandbyFlag4";
				break;
			case 17:
				strFieldName = "StandbyFlag5";
				break;
			case 18:
				strFieldName = "StandbyFlag6";
				break;
			case 19:
				strFieldName = "ModifyFlag";
				break;
			case 20:
				strFieldName = "Operator";
				break;
			case 21:
				strFieldName = "MakeDate";
				break;
			case 22:
				strFieldName = "MakeTime";
				break;
			case 23:
				strFieldName = "ModifyDate";
				break;
			case 24:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RecontCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StartDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("EndData") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("CostCenter") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CessPrem") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("EdorBackFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SumPrem") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ReProcFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("EdorProcFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SumFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ClaimBackFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("StandbyFlag1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandbyFlag2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandbyFlag3") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("StandbyFlag4") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("StandbyFlag5") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("StandbyFlag6") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 4:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 7:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 8:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 9:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 10:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 11:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 12:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 16:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 17:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 18:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
