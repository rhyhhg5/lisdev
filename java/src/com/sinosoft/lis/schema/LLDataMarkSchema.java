/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LLDataMarkDB;

/*
 * <p>ClassName: LLDataMarkSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2019-04-22
 */
public class LLDataMarkSchema implements Schema, Cloneable
{
	// @Field
	/** 流水号 */
	private String SerialNo;
	/** 接口类型 */
	private String InterfaceType;
	/** 接口名称 */
	private String InterfaceName;
	/** 接口功能描述 */
	private String InterfaceFunctionDis;
	/** 外包方名称 */
	private String OutSParty;
	/** 机构编码 */
	private String MngCom;
	/** 其他号码1 */
	private String OtherNo1;
	/** 其他号码2 */
	private String OtherNo2;
	/** 上报次数 */
	private int UploadTimes;
	/** 上报日期 */
	private Date UploadDate;
	/** 上报时间 */
	private String UploadTime;
	/** 上报标记 */
	private String Mark;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 修改日期 */
	private Date ModifyDate;
	/** 修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 16;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LLDataMarkSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "SerialNo";
		pk[1] = "InterfaceType";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LLDataMarkSchema cloned = (LLDataMarkSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getSerialNo()
	{
		return SerialNo;
	}
	public void setSerialNo(String aSerialNo)
	{
		SerialNo = aSerialNo;
	}
	public String getInterfaceType()
	{
		return InterfaceType;
	}
	public void setInterfaceType(String aInterfaceType)
	{
		InterfaceType = aInterfaceType;
	}
	public String getInterfaceName()
	{
		return InterfaceName;
	}
	public void setInterfaceName(String aInterfaceName)
	{
		InterfaceName = aInterfaceName;
	}
	public String getInterfaceFunctionDis()
	{
		return InterfaceFunctionDis;
	}
	public void setInterfaceFunctionDis(String aInterfaceFunctionDis)
	{
		InterfaceFunctionDis = aInterfaceFunctionDis;
	}
	public String getOutSParty()
	{
		return OutSParty;
	}
	public void setOutSParty(String aOutSParty)
	{
		OutSParty = aOutSParty;
	}
	public String getMngCom()
	{
		return MngCom;
	}
	public void setMngCom(String aMngCom)
	{
		MngCom = aMngCom;
	}
	public String getOtherNo1()
	{
		return OtherNo1;
	}
	public void setOtherNo1(String aOtherNo1)
	{
		OtherNo1 = aOtherNo1;
	}
	public String getOtherNo2()
	{
		return OtherNo2;
	}
	public void setOtherNo2(String aOtherNo2)
	{
		OtherNo2 = aOtherNo2;
	}
	public int getUploadTimes()
	{
		return UploadTimes;
	}
	public void setUploadTimes(int aUploadTimes)
	{
		UploadTimes = aUploadTimes;
	}
	public void setUploadTimes(String aUploadTimes)
	{
		if (aUploadTimes != null && !aUploadTimes.equals(""))
		{
			Integer tInteger = new Integer(aUploadTimes);
			int i = tInteger.intValue();
			UploadTimes = i;
		}
	}

	public String getUploadDate()
	{
		if( UploadDate != null )
			return fDate.getString(UploadDate);
		else
			return null;
	}
	public void setUploadDate(Date aUploadDate)
	{
		UploadDate = aUploadDate;
	}
	public void setUploadDate(String aUploadDate)
	{
		if (aUploadDate != null && !aUploadDate.equals("") )
		{
			UploadDate = fDate.getDate( aUploadDate );
		}
		else
			UploadDate = null;
	}

	public String getUploadTime()
	{
		return UploadTime;
	}
	public void setUploadTime(String aUploadTime)
	{
		UploadTime = aUploadTime;
	}
	public String getMark()
	{
		return Mark;
	}
	public void setMark(String aMark)
	{
		Mark = aMark;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LLDataMarkSchema 对象给 Schema 赋值
	* @param: aLLDataMarkSchema LLDataMarkSchema
	**/
	public void setSchema(LLDataMarkSchema aLLDataMarkSchema)
	{
		this.SerialNo = aLLDataMarkSchema.getSerialNo();
		this.InterfaceType = aLLDataMarkSchema.getInterfaceType();
		this.InterfaceName = aLLDataMarkSchema.getInterfaceName();
		this.InterfaceFunctionDis = aLLDataMarkSchema.getInterfaceFunctionDis();
		this.OutSParty = aLLDataMarkSchema.getOutSParty();
		this.MngCom = aLLDataMarkSchema.getMngCom();
		this.OtherNo1 = aLLDataMarkSchema.getOtherNo1();
		this.OtherNo2 = aLLDataMarkSchema.getOtherNo2();
		this.UploadTimes = aLLDataMarkSchema.getUploadTimes();
		this.UploadDate = fDate.getDate( aLLDataMarkSchema.getUploadDate());
		this.UploadTime = aLLDataMarkSchema.getUploadTime();
		this.Mark = aLLDataMarkSchema.getMark();
		this.MakeDate = fDate.getDate( aLLDataMarkSchema.getMakeDate());
		this.MakeTime = aLLDataMarkSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLLDataMarkSchema.getModifyDate());
		this.ModifyTime = aLLDataMarkSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("SerialNo") == null )
				this.SerialNo = null;
			else
				this.SerialNo = rs.getString("SerialNo").trim();

			if( rs.getString("InterfaceType") == null )
				this.InterfaceType = null;
			else
				this.InterfaceType = rs.getString("InterfaceType").trim();

			if( rs.getString("InterfaceName") == null )
				this.InterfaceName = null;
			else
				this.InterfaceName = rs.getString("InterfaceName").trim();

			if( rs.getString("InterfaceFunctionDis") == null )
				this.InterfaceFunctionDis = null;
			else
				this.InterfaceFunctionDis = rs.getString("InterfaceFunctionDis").trim();

			if( rs.getString("OutSParty") == null )
				this.OutSParty = null;
			else
				this.OutSParty = rs.getString("OutSParty").trim();

			if( rs.getString("MngCom") == null )
				this.MngCom = null;
			else
				this.MngCom = rs.getString("MngCom").trim();

			if( rs.getString("OtherNo1") == null )
				this.OtherNo1 = null;
			else
				this.OtherNo1 = rs.getString("OtherNo1").trim();

			if( rs.getString("OtherNo2") == null )
				this.OtherNo2 = null;
			else
				this.OtherNo2 = rs.getString("OtherNo2").trim();

			this.UploadTimes = rs.getInt("UploadTimes");
			this.UploadDate = rs.getDate("UploadDate");
			if( rs.getString("UploadTime") == null )
				this.UploadTime = null;
			else
				this.UploadTime = rs.getString("UploadTime").trim();

			if( rs.getString("Mark") == null )
				this.Mark = null;
			else
				this.Mark = rs.getString("Mark").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LLDataMark表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLDataMarkSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LLDataMarkSchema getSchema()
	{
		LLDataMarkSchema aLLDataMarkSchema = new LLDataMarkSchema();
		aLLDataMarkSchema.setSchema(this);
		return aLLDataMarkSchema;
	}

	public LLDataMarkDB getDB()
	{
		LLDataMarkDB aDBOper = new LLDataMarkDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLDataMark描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(SerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InterfaceType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InterfaceName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InterfaceFunctionDis)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OutSParty)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MngCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OtherNo1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OtherNo2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(UploadTimes));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( UploadDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UploadTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Mark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLDataMark>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			InterfaceType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			InterfaceName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			InterfaceFunctionDis = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			OutSParty = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			MngCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			OtherNo1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			OtherNo2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			UploadTimes= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,9,SysConst.PACKAGESPILTER))).intValue();
			UploadDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,SysConst.PACKAGESPILTER));
			UploadTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			Mark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLDataMarkSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("SerialNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
		}
		if (FCode.equals("InterfaceType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InterfaceType));
		}
		if (FCode.equals("InterfaceName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InterfaceName));
		}
		if (FCode.equals("InterfaceFunctionDis"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InterfaceFunctionDis));
		}
		if (FCode.equals("OutSParty"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OutSParty));
		}
		if (FCode.equals("MngCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MngCom));
		}
		if (FCode.equals("OtherNo1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNo1));
		}
		if (FCode.equals("OtherNo2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNo2));
		}
		if (FCode.equals("UploadTimes"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UploadTimes));
		}
		if (FCode.equals("UploadDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getUploadDate()));
		}
		if (FCode.equals("UploadTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UploadTime));
		}
		if (FCode.equals("Mark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Mark));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(SerialNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(InterfaceType);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(InterfaceName);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(InterfaceFunctionDis);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(OutSParty);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(MngCom);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(OtherNo1);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(OtherNo2);
				break;
			case 8:
				strFieldValue = String.valueOf(UploadTimes);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getUploadDate()));
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(UploadTime);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(Mark);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("SerialNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SerialNo = FValue.trim();
			}
			else
				SerialNo = null;
		}
		if (FCode.equalsIgnoreCase("InterfaceType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InterfaceType = FValue.trim();
			}
			else
				InterfaceType = null;
		}
		if (FCode.equalsIgnoreCase("InterfaceName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InterfaceName = FValue.trim();
			}
			else
				InterfaceName = null;
		}
		if (FCode.equalsIgnoreCase("InterfaceFunctionDis"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InterfaceFunctionDis = FValue.trim();
			}
			else
				InterfaceFunctionDis = null;
		}
		if (FCode.equalsIgnoreCase("OutSParty"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OutSParty = FValue.trim();
			}
			else
				OutSParty = null;
		}
		if (FCode.equalsIgnoreCase("MngCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MngCom = FValue.trim();
			}
			else
				MngCom = null;
		}
		if (FCode.equalsIgnoreCase("OtherNo1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OtherNo1 = FValue.trim();
			}
			else
				OtherNo1 = null;
		}
		if (FCode.equalsIgnoreCase("OtherNo2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OtherNo2 = FValue.trim();
			}
			else
				OtherNo2 = null;
		}
		if (FCode.equalsIgnoreCase("UploadTimes"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				UploadTimes = i;
			}
		}
		if (FCode.equalsIgnoreCase("UploadDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				UploadDate = fDate.getDate( FValue );
			}
			else
				UploadDate = null;
		}
		if (FCode.equalsIgnoreCase("UploadTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UploadTime = FValue.trim();
			}
			else
				UploadTime = null;
		}
		if (FCode.equalsIgnoreCase("Mark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Mark = FValue.trim();
			}
			else
				Mark = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LLDataMarkSchema other = (LLDataMarkSchema)otherObject;
		return
			(SerialNo == null ? other.getSerialNo() == null : SerialNo.equals(other.getSerialNo()))
			&& (InterfaceType == null ? other.getInterfaceType() == null : InterfaceType.equals(other.getInterfaceType()))
			&& (InterfaceName == null ? other.getInterfaceName() == null : InterfaceName.equals(other.getInterfaceName()))
			&& (InterfaceFunctionDis == null ? other.getInterfaceFunctionDis() == null : InterfaceFunctionDis.equals(other.getInterfaceFunctionDis()))
			&& (OutSParty == null ? other.getOutSParty() == null : OutSParty.equals(other.getOutSParty()))
			&& (MngCom == null ? other.getMngCom() == null : MngCom.equals(other.getMngCom()))
			&& (OtherNo1 == null ? other.getOtherNo1() == null : OtherNo1.equals(other.getOtherNo1()))
			&& (OtherNo2 == null ? other.getOtherNo2() == null : OtherNo2.equals(other.getOtherNo2()))
			&& UploadTimes == other.getUploadTimes()
			&& (UploadDate == null ? other.getUploadDate() == null : fDate.getString(UploadDate).equals(other.getUploadDate()))
			&& (UploadTime == null ? other.getUploadTime() == null : UploadTime.equals(other.getUploadTime()))
			&& (Mark == null ? other.getMark() == null : Mark.equals(other.getMark()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("SerialNo") ) {
			return 0;
		}
		if( strFieldName.equals("InterfaceType") ) {
			return 1;
		}
		if( strFieldName.equals("InterfaceName") ) {
			return 2;
		}
		if( strFieldName.equals("InterfaceFunctionDis") ) {
			return 3;
		}
		if( strFieldName.equals("OutSParty") ) {
			return 4;
		}
		if( strFieldName.equals("MngCom") ) {
			return 5;
		}
		if( strFieldName.equals("OtherNo1") ) {
			return 6;
		}
		if( strFieldName.equals("OtherNo2") ) {
			return 7;
		}
		if( strFieldName.equals("UploadTimes") ) {
			return 8;
		}
		if( strFieldName.equals("UploadDate") ) {
			return 9;
		}
		if( strFieldName.equals("UploadTime") ) {
			return 10;
		}
		if( strFieldName.equals("Mark") ) {
			return 11;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 12;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 13;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 14;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 15;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "SerialNo";
				break;
			case 1:
				strFieldName = "InterfaceType";
				break;
			case 2:
				strFieldName = "InterfaceName";
				break;
			case 3:
				strFieldName = "InterfaceFunctionDis";
				break;
			case 4:
				strFieldName = "OutSParty";
				break;
			case 5:
				strFieldName = "MngCom";
				break;
			case 6:
				strFieldName = "OtherNo1";
				break;
			case 7:
				strFieldName = "OtherNo2";
				break;
			case 8:
				strFieldName = "UploadTimes";
				break;
			case 9:
				strFieldName = "UploadDate";
				break;
			case 10:
				strFieldName = "UploadTime";
				break;
			case 11:
				strFieldName = "Mark";
				break;
			case 12:
				strFieldName = "MakeDate";
				break;
			case 13:
				strFieldName = "MakeTime";
				break;
			case 14:
				strFieldName = "ModifyDate";
				break;
			case 15:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("SerialNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InterfaceType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InterfaceName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InterfaceFunctionDis") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OutSParty") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MngCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OtherNo1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OtherNo2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UploadTimes") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("UploadDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("UploadTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Mark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_INT;
				break;
			case 9:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
