/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LIClassTypeKeyDefDB;

/*
 * <p>ClassName: LIClassTypeKeyDefSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 财务接口最新版
 * @CreateDate：2008-05-20
 */
public class LIClassTypeKeyDefSchema implements Schema, Cloneable
{
	// @Field
	/** 凭证类型 */
	private String ClassType;
	/** 凭证名称 */
	private String ClassTypeName;
	/** 键值编号 */
	private String KeyID;
	/** 键值名称 */
	private String KeyName;
	/** 说明 */
	private String Remark;

	public static final int FIELDNUM = 5;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LIClassTypeKeyDefSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "ClassType";

		PK = pk;
	}

            /**
             * Schema克隆
             * @return Object
             * @throws CloneNotSupportedException
             */
            public Object clone()
                    throws CloneNotSupportedException
            {
                LIClassTypeKeyDefSchema cloned = (LIClassTypeKeyDefSchema)super.clone();
                cloned.mErrors = (CErrors) mErrors.clone();
                return cloned;
            }

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getClassType()
	{
		return ClassType;
	}
	public void setClassType(String aClassType)
	{
            ClassType = aClassType;
	}
	public String getClassTypeName()
	{
		return ClassTypeName;
	}
	public void setClassTypeName(String aClassTypeName)
	{
            ClassTypeName = aClassTypeName;
	}
	public String getKeyID()
	{
		return KeyID;
	}
	public void setKeyID(String aKeyID)
	{
            KeyID = aKeyID;
	}
	public String getKeyName()
	{
		return KeyName;
	}
	public void setKeyName(String aKeyName)
	{
            KeyName = aKeyName;
	}
	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
            Remark = aRemark;
	}

	/**
	* 使用另外一个 LIClassTypeKeyDefSchema 对象给 Schema 赋值
	* @param: aLIClassTypeKeyDefSchema LIClassTypeKeyDefSchema
	**/
	public void setSchema(LIClassTypeKeyDefSchema aLIClassTypeKeyDefSchema)
	{
		this.ClassType = aLIClassTypeKeyDefSchema.getClassType();
		this.ClassTypeName = aLIClassTypeKeyDefSchema.getClassTypeName();
		this.KeyID = aLIClassTypeKeyDefSchema.getKeyID();
		this.KeyName = aLIClassTypeKeyDefSchema.getKeyName();
		this.Remark = aLIClassTypeKeyDefSchema.getRemark();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("ClassType") == null )
				this.ClassType = null;
			else
				this.ClassType = rs.getString("ClassType").trim();

			if( rs.getString("ClassTypeName") == null )
				this.ClassTypeName = null;
			else
				this.ClassTypeName = rs.getString("ClassTypeName").trim();

			if( rs.getString("KeyID") == null )
				this.KeyID = null;
			else
				this.KeyID = rs.getString("KeyID").trim();

			if( rs.getString("KeyName") == null )
				this.KeyName = null;
			else
				this.KeyName = rs.getString("KeyName").trim();

			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LIClassTypeKeyDef表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LIClassTypeKeyDefSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LIClassTypeKeyDefSchema getSchema()
	{
		LIClassTypeKeyDefSchema aLIClassTypeKeyDefSchema = new LIClassTypeKeyDefSchema();
		aLIClassTypeKeyDefSchema.setSchema(this);
		return aLIClassTypeKeyDefSchema;
	}

	public LIClassTypeKeyDefDB getDB()
	{
		LIClassTypeKeyDefDB aDBOper = new LIClassTypeKeyDefDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLIClassTypeKeyDef描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append(StrTool.cTrim(ClassType)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ClassTypeName)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(KeyID)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(KeyName)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Remark));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLIClassTypeKeyDef>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			ClassType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			ClassTypeName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			KeyID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			KeyName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LIClassTypeKeyDefSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("ClassType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClassType));
		}
		if (FCode.equals("ClassTypeName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClassTypeName));
		}
		if (FCode.equals("KeyID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(KeyID));
		}
		if (FCode.equals("KeyName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(KeyName));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(ClassType);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(ClassTypeName);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(KeyID);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(KeyName);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("ClassType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ClassType = FValue.trim();
			}
			else
				ClassType = null;
		}
		if (FCode.equalsIgnoreCase("ClassTypeName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ClassTypeName = FValue.trim();
			}
			else
				ClassTypeName = null;
		}
		if (FCode.equalsIgnoreCase("KeyID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				KeyID = FValue.trim();
			}
			else
				KeyID = null;
		}
		if (FCode.equalsIgnoreCase("KeyName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				KeyName = FValue.trim();
			}
			else
				KeyName = null;
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LIClassTypeKeyDefSchema other = (LIClassTypeKeyDefSchema)otherObject;
		return
			ClassType.equals(other.getClassType())
			&& ClassTypeName.equals(other.getClassTypeName())
			&& KeyID.equals(other.getKeyID())
			&& KeyName.equals(other.getKeyName())
			&& Remark.equals(other.getRemark());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("ClassType") ) {
			return 0;
		}
		if( strFieldName.equals("ClassTypeName") ) {
			return 1;
		}
		if( strFieldName.equals("KeyID") ) {
			return 2;
		}
		if( strFieldName.equals("KeyName") ) {
			return 3;
		}
		if( strFieldName.equals("Remark") ) {
			return 4;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "ClassType";
				break;
			case 1:
				strFieldName = "ClassTypeName";
				break;
			case 2:
				strFieldName = "KeyID";
				break;
			case 3:
				strFieldName = "KeyName";
				break;
			case 4:
				strFieldName = "Remark";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("ClassType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ClassTypeName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("KeyID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("KeyName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
