/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LMDutyGetAddFactorDB;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LMDutyGetAddFactorSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-07-14
 */
public class LMDutyGetAddFactorSchema implements Schema, Cloneable
{
    // @Field
    /** 限制类型 */
    private String LimitType;
    /** 给付代码 */
    private String GetDutyCode;
    /** 给付名称 */
    private String GetDutyName;
    /** 险种代码 */
    private String RiskCode;
    /** 档次 */
    private String Mult;
    /** 限额/日补贴额 */
    private double LimitValue;
    /** 免责期/最长给付天数 */
    private int DutyRelaDays;

    public static final int FIELDNUM = 7; // 数据库表的字段个数

    private static String[] PK; // 主键

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LMDutyGetAddFactorSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "LimitType";
        pk[1] = "GetDutyCode";
        pk[2] = "Mult";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException
    {
        LMDutyGetAddFactorSchema cloned = (LMDutyGetAddFactorSchema)super.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getLimitType()
    {
        return LimitType;
    }

    public void setLimitType(String aLimitType)
    {
        LimitType = aLimitType;
    }

    public String getGetDutyCode()
    {
        return GetDutyCode;
    }

    public void setGetDutyCode(String aGetDutyCode)
    {
        GetDutyCode = aGetDutyCode;
    }

    public String getGetDutyName()
    {
        return GetDutyName;
    }

    public void setGetDutyName(String aGetDutyName)
    {
        GetDutyName = aGetDutyName;
    }

    public String getRiskCode()
    {
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getMult()
    {
        return Mult;
    }

    public void setMult(String aMult)
    {
        Mult = aMult;
    }

    public double getLimitValue()
    {
        return LimitValue;
    }

    public void setLimitValue(double aLimitValue)
    {
        LimitValue = Arith.round(aLimitValue, 6);
    }

    public void setLimitValue(String aLimitValue)
    {
        if (aLimitValue != null && !aLimitValue.equals(""))
        {
            Double tDouble = new Double(aLimitValue);
            double d = tDouble.doubleValue();
            LimitValue = Arith.round(d, 6);
        }
    }

    public int getDutyRelaDays()
    {
        return DutyRelaDays;
    }

    public void setDutyRelaDays(int aDutyRelaDays)
    {
        DutyRelaDays = aDutyRelaDays;
    }

    public void setDutyRelaDays(String aDutyRelaDays)
    {
        if (aDutyRelaDays != null && !aDutyRelaDays.equals(""))
        {
            Integer tInteger = new Integer(aDutyRelaDays);
            int i = tInteger.intValue();
            DutyRelaDays = i;
        }
    }


    /**
     * 使用另外一个 LMDutyGetAddFactorSchema 对象给 Schema 赋值
     * @param: aLMDutyGetAddFactorSchema LMDutyGetAddFactorSchema
     **/
    public void setSchema(LMDutyGetAddFactorSchema aLMDutyGetAddFactorSchema)
    {
        this.LimitType = aLMDutyGetAddFactorSchema.getLimitType();
        this.GetDutyCode = aLMDutyGetAddFactorSchema.getGetDutyCode();
        this.GetDutyName = aLMDutyGetAddFactorSchema.getGetDutyName();
        this.RiskCode = aLMDutyGetAddFactorSchema.getRiskCode();
        this.Mult = aLMDutyGetAddFactorSchema.getMult();
        this.LimitValue = aLMDutyGetAddFactorSchema.getLimitValue();
        this.DutyRelaDays = aLMDutyGetAddFactorSchema.getDutyRelaDays();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("LimitType") == null)
            {
                this.LimitType = null;
            }
            else
            {
                this.LimitType = rs.getString("LimitType").trim();
            }

            if (rs.getString("GetDutyCode") == null)
            {
                this.GetDutyCode = null;
            }
            else
            {
                this.GetDutyCode = rs.getString("GetDutyCode").trim();
            }

            if (rs.getString("GetDutyName") == null)
            {
                this.GetDutyName = null;
            }
            else
            {
                this.GetDutyName = rs.getString("GetDutyName").trim();
            }

            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("Mult") == null)
            {
                this.Mult = null;
            }
            else
            {
                this.Mult = rs.getString("Mult").trim();
            }

            this.LimitValue = rs.getDouble("LimitValue");
            this.DutyRelaDays = rs.getInt("DutyRelaDays");
        }
        catch (SQLException sqle)
        {
            System.out.println("数据库中的LMDutyGetAddFactor表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMDutyGetAddFactorSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LMDutyGetAddFactorSchema getSchema()
    {
        LMDutyGetAddFactorSchema aLMDutyGetAddFactorSchema = new
                LMDutyGetAddFactorSchema();
        aLMDutyGetAddFactorSchema.setSchema(this);
        return aLMDutyGetAddFactorSchema;
    }

    public LMDutyGetAddFactorDB getDB()
    {
        LMDutyGetAddFactorDB aDBOper = new LMDutyGetAddFactorDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMDutyGetAddFactor描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(LimitType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GetDutyCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GetDutyName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Mult));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(LimitValue));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DutyRelaDays));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMDutyGetAddFactor>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            LimitType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            GetDutyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                         SysConst.PACKAGESPILTER);
            GetDutyName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                         SysConst.PACKAGESPILTER);
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                      SysConst.PACKAGESPILTER);
            Mult = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                  SysConst.PACKAGESPILTER);
            LimitValue = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 6, SysConst.PACKAGESPILTER))).doubleValue();
            DutyRelaDays = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 7, SysConst.PACKAGESPILTER))).intValue();
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMDutyGetAddFactorSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("LimitType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LimitType));
        }
        if (FCode.equals("GetDutyCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetDutyCode));
        }
        if (FCode.equals("GetDutyName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetDutyName));
        }
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equals("Mult"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Mult));
        }
        if (FCode.equals("LimitValue"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LimitValue));
        }
        if (FCode.equals("DutyRelaDays"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DutyRelaDays));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(LimitType);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(GetDutyCode);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(GetDutyName);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(Mult);
                break;
            case 5:
                strFieldValue = String.valueOf(LimitValue);
                break;
            case 6:
                strFieldValue = String.valueOf(DutyRelaDays);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("LimitType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                LimitType = FValue.trim();
            }
            else
            {
                LimitType = null;
            }
        }
        if (FCode.equals("GetDutyCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetDutyCode = FValue.trim();
            }
            else
            {
                GetDutyCode = null;
            }
        }
        if (FCode.equals("GetDutyName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetDutyName = FValue.trim();
            }
            else
            {
                GetDutyName = null;
            }
        }
        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("Mult"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Mult = FValue.trim();
            }
            else
            {
                Mult = null;
            }
        }
        if (FCode.equals("LimitValue"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                LimitValue = d;
            }
        }
        if (FCode.equals("DutyRelaDays"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                DutyRelaDays = i;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LMDutyGetAddFactorSchema other = (LMDutyGetAddFactorSchema) otherObject;
        return
                LimitType.equals(other.getLimitType())
                && GetDutyCode.equals(other.getGetDutyCode())
                && GetDutyName.equals(other.getGetDutyName())
                && RiskCode.equals(other.getRiskCode())
                && Mult.equals(other.getMult())
                && LimitValue == other.getLimitValue()
                && DutyRelaDays == other.getDutyRelaDays();
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("LimitType"))
        {
            return 0;
        }
        if (strFieldName.equals("GetDutyCode"))
        {
            return 1;
        }
        if (strFieldName.equals("GetDutyName"))
        {
            return 2;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return 3;
        }
        if (strFieldName.equals("Mult"))
        {
            return 4;
        }
        if (strFieldName.equals("LimitValue"))
        {
            return 5;
        }
        if (strFieldName.equals("DutyRelaDays"))
        {
            return 6;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "LimitType";
                break;
            case 1:
                strFieldName = "GetDutyCode";
                break;
            case 2:
                strFieldName = "GetDutyName";
                break;
            case 3:
                strFieldName = "RiskCode";
                break;
            case 4:
                strFieldName = "Mult";
                break;
            case 5:
                strFieldName = "LimitValue";
                break;
            case 6:
                strFieldName = "DutyRelaDays";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("LimitType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetDutyCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetDutyName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Mult"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("LimitValue"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("DutyRelaDays"))
        {
            return Schema.TYPE_INT;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 6:
                nFieldType = Schema.TYPE_INT;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
