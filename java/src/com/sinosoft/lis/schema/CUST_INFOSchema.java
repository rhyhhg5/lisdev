/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.CUST_INFODB;

/*
 * <p>ClassName: CUST_INFOSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 新寿险业务系统模型
 * @CreateDate：2014-04-23
 */
public class CUST_INFOSchema implements Schema, Cloneable
{
	// @Field
	/** Uid */
	private String UID;
	/** Customerno */
	private String CUSTOMERNO;
	/** Name */
	private String NAME;
	/** Sex */
	private String SEX;
	/** Birthday */
	private String BIRTHDAY;
	/** Idtype */
	private String IDTYPE;
	/** Idno */
	private String IDNO;
	/** Tel_no */
	private String TEL_NO;
	/** Bankcode */
	private String BANKCODE;
	/** Bankname */
	private String BANKNAME;
	/** Accno */
	private String ACCNO;
	/** Etltime */
	private Date ETLTIME;

	public static final int FIELDNUM = 12;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public CUST_INFOSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[0];

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		CUST_INFOSchema cloned = (CUST_INFOSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getUID()
	{
		return UID;
	}
	public void setUID(String aUID)
	{
		UID = aUID;
	}
	public String getCUSTOMERNO()
	{
		return CUSTOMERNO;
	}
	public void setCUSTOMERNO(String aCUSTOMERNO)
	{
		CUSTOMERNO = aCUSTOMERNO;
	}
	public String getNAME()
	{
		return NAME;
	}
	public void setNAME(String aNAME)
	{
		NAME = aNAME;
	}
	public String getSEX()
	{
		return SEX;
	}
	public void setSEX(String aSEX)
	{
		SEX = aSEX;
	}
	public String getBIRTHDAY()
	{
		return BIRTHDAY;
	}
	public void setBIRTHDAY(String aBIRTHDAY)
	{
		BIRTHDAY = aBIRTHDAY;
	}
	public String getIDTYPE()
	{
		return IDTYPE;
	}
	public void setIDTYPE(String aIDTYPE)
	{
		IDTYPE = aIDTYPE;
	}
	public String getIDNO()
	{
		return IDNO;
	}
	public void setIDNO(String aIDNO)
	{
		IDNO = aIDNO;
	}
	public String getTEL_NO()
	{
		return TEL_NO;
	}
	public void setTEL_NO(String aTEL_NO)
	{
		TEL_NO = aTEL_NO;
	}
	public String getBANKCODE()
	{
		return BANKCODE;
	}
	public void setBANKCODE(String aBANKCODE)
	{
		BANKCODE = aBANKCODE;
	}
	public String getBANKNAME()
	{
		return BANKNAME;
	}
	public void setBANKNAME(String aBANKNAME)
	{
		BANKNAME = aBANKNAME;
	}
	public String getACCNO()
	{
		return ACCNO;
	}
	public void setACCNO(String aACCNO)
	{
		ACCNO = aACCNO;
	}
	public String getETLTIME()
	{
		if( ETLTIME != null )
			return fDate.getString(ETLTIME);
		else
			return null;
	}
	public void setETLTIME(Date aETLTIME)
	{
		ETLTIME = aETLTIME;
	}
	public void setETLTIME(String aETLTIME)
	{
		if (aETLTIME != null && !aETLTIME.equals("") )
		{
			ETLTIME = fDate.getDate( aETLTIME );
		}
		else
			ETLTIME = null;
	}


	/**
	* 使用另外一个 CUST_INFOSchema 对象给 Schema 赋值
	* @param: aCUST_INFOSchema CUST_INFOSchema
	**/
	public void setSchema(CUST_INFOSchema aCUST_INFOSchema)
	{
		this.UID = aCUST_INFOSchema.getUID();
		this.CUSTOMERNO = aCUST_INFOSchema.getCUSTOMERNO();
		this.NAME = aCUST_INFOSchema.getNAME();
		this.SEX = aCUST_INFOSchema.getSEX();
		this.BIRTHDAY = aCUST_INFOSchema.getBIRTHDAY();
		this.IDTYPE = aCUST_INFOSchema.getIDTYPE();
		this.IDNO = aCUST_INFOSchema.getIDNO();
		this.TEL_NO = aCUST_INFOSchema.getTEL_NO();
		this.BANKCODE = aCUST_INFOSchema.getBANKCODE();
		this.BANKNAME = aCUST_INFOSchema.getBANKNAME();
		this.ACCNO = aCUST_INFOSchema.getACCNO();
		this.ETLTIME = fDate.getDate( aCUST_INFOSchema.getETLTIME());
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("UID") == null )
				this.UID = null;
			else
				this.UID = rs.getString("UID").trim();

			if( rs.getString("CUSTOMERNO") == null )
				this.CUSTOMERNO = null;
			else
				this.CUSTOMERNO = rs.getString("CUSTOMERNO").trim();

			if( rs.getString("NAME") == null )
				this.NAME = null;
			else
				this.NAME = rs.getString("NAME").trim();

			if( rs.getString("SEX") == null )
				this.SEX = null;
			else
				this.SEX = rs.getString("SEX").trim();

			if( rs.getString("BIRTHDAY") == null )
				this.BIRTHDAY = null;
			else
				this.BIRTHDAY = rs.getString("BIRTHDAY").trim();

			if( rs.getString("IDTYPE") == null )
				this.IDTYPE = null;
			else
				this.IDTYPE = rs.getString("IDTYPE").trim();

			if( rs.getString("IDNO") == null )
				this.IDNO = null;
			else
				this.IDNO = rs.getString("IDNO").trim();

			if( rs.getString("TEL_NO") == null )
				this.TEL_NO = null;
			else
				this.TEL_NO = rs.getString("TEL_NO").trim();

			if( rs.getString("BANKCODE") == null )
				this.BANKCODE = null;
			else
				this.BANKCODE = rs.getString("BANKCODE").trim();

			if( rs.getString("BANKNAME") == null )
				this.BANKNAME = null;
			else
				this.BANKNAME = rs.getString("BANKNAME").trim();

			if( rs.getString("ACCNO") == null )
				this.ACCNO = null;
			else
				this.ACCNO = rs.getString("ACCNO").trim();

			this.ETLTIME = rs.getDate("ETLTIME");
		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的CUST_INFO表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "CUST_INFOSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public CUST_INFOSchema getSchema()
	{
		CUST_INFOSchema aCUST_INFOSchema = new CUST_INFOSchema();
		aCUST_INFOSchema.setSchema(this);
		return aCUST_INFOSchema;
	}

	public CUST_INFODB getDB()
	{
		CUST_INFODB aDBOper = new CUST_INFODB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpCUST_INFO描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(UID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CUSTOMERNO)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(NAME)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SEX)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BIRTHDAY)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IDTYPE)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IDNO)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TEL_NO)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BANKCODE)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BANKNAME)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ACCNO)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ETLTIME )));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpCUST_INFO>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			UID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			CUSTOMERNO = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			NAME = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			SEX = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			BIRTHDAY = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			IDTYPE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			IDNO = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			TEL_NO = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			BANKCODE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			BANKNAME = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			ACCNO = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			ETLTIME = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "CUST_INFOSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("UID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UID));
		}
		if (FCode.equals("CUSTOMERNO"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CUSTOMERNO));
		}
		if (FCode.equals("NAME"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(NAME));
		}
		if (FCode.equals("SEX"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SEX));
		}
		if (FCode.equals("BIRTHDAY"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BIRTHDAY));
		}
		if (FCode.equals("IDTYPE"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IDTYPE));
		}
		if (FCode.equals("IDNO"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IDNO));
		}
		if (FCode.equals("TEL_NO"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TEL_NO));
		}
		if (FCode.equals("BANKCODE"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BANKCODE));
		}
		if (FCode.equals("BANKNAME"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BANKNAME));
		}
		if (FCode.equals("ACCNO"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ACCNO));
		}
		if (FCode.equals("ETLTIME"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getETLTIME()));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(UID);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(CUSTOMERNO);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(NAME);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(SEX);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(BIRTHDAY);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(IDTYPE);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(IDNO);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(TEL_NO);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(BANKCODE);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(BANKNAME);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(ACCNO);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getETLTIME()));
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("UID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UID = FValue.trim();
			}
			else
				UID = null;
		}
		if (FCode.equalsIgnoreCase("CUSTOMERNO"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CUSTOMERNO = FValue.trim();
			}
			else
				CUSTOMERNO = null;
		}
		if (FCode.equalsIgnoreCase("NAME"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				NAME = FValue.trim();
			}
			else
				NAME = null;
		}
		if (FCode.equalsIgnoreCase("SEX"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SEX = FValue.trim();
			}
			else
				SEX = null;
		}
		if (FCode.equalsIgnoreCase("BIRTHDAY"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BIRTHDAY = FValue.trim();
			}
			else
				BIRTHDAY = null;
		}
		if (FCode.equalsIgnoreCase("IDTYPE"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IDTYPE = FValue.trim();
			}
			else
				IDTYPE = null;
		}
		if (FCode.equalsIgnoreCase("IDNO"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IDNO = FValue.trim();
			}
			else
				IDNO = null;
		}
		if (FCode.equalsIgnoreCase("TEL_NO"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TEL_NO = FValue.trim();
			}
			else
				TEL_NO = null;
		}
		if (FCode.equalsIgnoreCase("BANKCODE"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BANKCODE = FValue.trim();
			}
			else
				BANKCODE = null;
		}
		if (FCode.equalsIgnoreCase("BANKNAME"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BANKNAME = FValue.trim();
			}
			else
				BANKNAME = null;
		}
		if (FCode.equalsIgnoreCase("ACCNO"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ACCNO = FValue.trim();
			}
			else
				ACCNO = null;
		}
		if (FCode.equalsIgnoreCase("ETLTIME"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ETLTIME = fDate.getDate( FValue );
			}
			else
				ETLTIME = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		CUST_INFOSchema other = (CUST_INFOSchema)otherObject;
		return
			(UID == null ? other.getUID() == null : UID.equals(other.getUID()))
			&& (CUSTOMERNO == null ? other.getCUSTOMERNO() == null : CUSTOMERNO.equals(other.getCUSTOMERNO()))
			&& (NAME == null ? other.getNAME() == null : NAME.equals(other.getNAME()))
			&& (SEX == null ? other.getSEX() == null : SEX.equals(other.getSEX()))
			&& (BIRTHDAY == null ? other.getBIRTHDAY() == null : BIRTHDAY.equals(other.getBIRTHDAY()))
			&& (IDTYPE == null ? other.getIDTYPE() == null : IDTYPE.equals(other.getIDTYPE()))
			&& (IDNO == null ? other.getIDNO() == null : IDNO.equals(other.getIDNO()))
			&& (TEL_NO == null ? other.getTEL_NO() == null : TEL_NO.equals(other.getTEL_NO()))
			&& (BANKCODE == null ? other.getBANKCODE() == null : BANKCODE.equals(other.getBANKCODE()))
			&& (BANKNAME == null ? other.getBANKNAME() == null : BANKNAME.equals(other.getBANKNAME()))
			&& (ACCNO == null ? other.getACCNO() == null : ACCNO.equals(other.getACCNO()))
			&& (ETLTIME == null ? other.getETLTIME() == null : fDate.getString(ETLTIME).equals(other.getETLTIME()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("UID") ) {
			return 0;
		}
		if( strFieldName.equals("CUSTOMERNO") ) {
			return 1;
		}
		if( strFieldName.equals("NAME") ) {
			return 2;
		}
		if( strFieldName.equals("SEX") ) {
			return 3;
		}
		if( strFieldName.equals("BIRTHDAY") ) {
			return 4;
		}
		if( strFieldName.equals("IDTYPE") ) {
			return 5;
		}
		if( strFieldName.equals("IDNO") ) {
			return 6;
		}
		if( strFieldName.equals("TEL_NO") ) {
			return 7;
		}
		if( strFieldName.equals("BANKCODE") ) {
			return 8;
		}
		if( strFieldName.equals("BANKNAME") ) {
			return 9;
		}
		if( strFieldName.equals("ACCNO") ) {
			return 10;
		}
		if( strFieldName.equals("ETLTIME") ) {
			return 11;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "UID";
				break;
			case 1:
				strFieldName = "CUSTOMERNO";
				break;
			case 2:
				strFieldName = "NAME";
				break;
			case 3:
				strFieldName = "SEX";
				break;
			case 4:
				strFieldName = "BIRTHDAY";
				break;
			case 5:
				strFieldName = "IDTYPE";
				break;
			case 6:
				strFieldName = "IDNO";
				break;
			case 7:
				strFieldName = "TEL_NO";
				break;
			case 8:
				strFieldName = "BANKCODE";
				break;
			case 9:
				strFieldName = "BANKNAME";
				break;
			case 10:
				strFieldName = "ACCNO";
				break;
			case 11:
				strFieldName = "ETLTIME";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("UID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CUSTOMERNO") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("NAME") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SEX") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BIRTHDAY") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IDTYPE") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IDNO") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TEL_NO") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BANKCODE") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BANKNAME") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ACCNO") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ETLTIME") ) {
			return Schema.TYPE_DATE;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
