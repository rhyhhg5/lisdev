/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LLCaseOpTimeDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LLCaseOpTimeSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-09-19
 */
public class LLCaseOpTimeSchema implements Schema, Cloneable {
    // @Field
    /** 案件号 */
    private String CaseNo;
    /** 案件状态 */
    private String RgtState;
    /** 重复操作次数 */
    private int Sequance;
    /** 页面编码 */
    private int PageNo;
    /** 管理机构 */
    private String ManageCom;
    /** 操作员 */
    private String Operator;
    /** 操作时间 */
    private String OpTime;
    /** 开始日期 */
    private Date StartDate;
    /** 开始时间 */
    private String StartTime;
    /** 结束日期 */
    private Date EndDate;
    /** 结束时间 */
    private String EndTime;

    public static final int FIELDNUM = 11; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LLCaseOpTimeSchema() {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "CaseNo";
        pk[1] = "RgtState";
        pk[2] = "Sequance";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LLCaseOpTimeSchema cloned = (LLCaseOpTimeSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getCaseNo() {
        return CaseNo;
    }

    public void setCaseNo(String aCaseNo) {
        CaseNo = aCaseNo;
    }

    public String getRgtState() {
        return RgtState;
    }

    public void setRgtState(String aRgtState) {
        RgtState = aRgtState;
    }

    public int getSequance() {
        return Sequance;
    }

    public void setSequance(int aSequance) {
        Sequance = aSequance;
    }

    public void setSequance(String aSequance) {
        if (aSequance != null && !aSequance.equals("")) {
            Integer tInteger = new Integer(aSequance);
            int i = tInteger.intValue();
            Sequance = i;
        }
    }

    public int getPageNo() {
        return PageNo;
    }

    public void setPageNo(int aPageNo) {
        PageNo = aPageNo;
    }

    public void setPageNo(String aPageNo) {
        if (aPageNo != null && !aPageNo.equals("")) {
            Integer tInteger = new Integer(aPageNo);
            int i = tInteger.intValue();
            PageNo = i;
        }
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getOpTime() {
        return OpTime;
    }

    public void setOpTime(String aOpTime) {
        OpTime = aOpTime;
    }

    public String getStartDate() {
        if (StartDate != null) {
            return fDate.getString(StartDate);
        } else {
            return null;
        }
    }

    public void setStartDate(Date aStartDate) {
        StartDate = aStartDate;
    }

    public void setStartDate(String aStartDate) {
        if (aStartDate != null && !aStartDate.equals("")) {
            StartDate = fDate.getDate(aStartDate);
        } else {
            StartDate = null;
        }
    }

    public String getStartTime() {
        return StartTime;
    }

    public void setStartTime(String aStartTime) {
        StartTime = aStartTime;
    }

    public String getEndDate() {
        if (EndDate != null) {
            return fDate.getString(EndDate);
        } else {
            return null;
        }
    }

    public void setEndDate(Date aEndDate) {
        EndDate = aEndDate;
    }

    public void setEndDate(String aEndDate) {
        if (aEndDate != null && !aEndDate.equals("")) {
            EndDate = fDate.getDate(aEndDate);
        } else {
            EndDate = null;
        }
    }

    public String getEndTime() {
        return EndTime;
    }

    public void setEndTime(String aEndTime) {
        EndTime = aEndTime;
    }

    /**
     * 使用另外一个 LLCaseOpTimeSchema 对象给 Schema 赋值
     * @param: aLLCaseOpTimeSchema LLCaseOpTimeSchema
     **/
    public void setSchema(LLCaseOpTimeSchema aLLCaseOpTimeSchema) {
        this.CaseNo = aLLCaseOpTimeSchema.getCaseNo();
        this.RgtState = aLLCaseOpTimeSchema.getRgtState();
        this.Sequance = aLLCaseOpTimeSchema.getSequance();
        this.PageNo = aLLCaseOpTimeSchema.getPageNo();
        this.ManageCom = aLLCaseOpTimeSchema.getManageCom();
        this.Operator = aLLCaseOpTimeSchema.getOperator();
        this.OpTime = aLLCaseOpTimeSchema.getOpTime();
        this.StartDate = fDate.getDate(aLLCaseOpTimeSchema.getStartDate());
        this.StartTime = aLLCaseOpTimeSchema.getStartTime();
        this.EndDate = fDate.getDate(aLLCaseOpTimeSchema.getEndDate());
        this.EndTime = aLLCaseOpTimeSchema.getEndTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("CaseNo") == null) {
                this.CaseNo = null;
            } else {
                this.CaseNo = rs.getString("CaseNo").trim();
            }

            if (rs.getString("RgtState") == null) {
                this.RgtState = null;
            } else {
                this.RgtState = rs.getString("RgtState").trim();
            }

            this.Sequance = rs.getInt("Sequance");
            this.PageNo = rs.getInt("PageNo");
            if (rs.getString("ManageCom") == null) {
                this.ManageCom = null;
            } else {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            if (rs.getString("OpTime") == null) {
                this.OpTime = null;
            } else {
                this.OpTime = rs.getString("OpTime").trim();
            }

            this.StartDate = rs.getDate("StartDate");
            if (rs.getString("StartTime") == null) {
                this.StartTime = null;
            } else {
                this.StartTime = rs.getString("StartTime").trim();
            }

            this.EndDate = rs.getDate("EndDate");
            if (rs.getString("EndTime") == null) {
                this.EndTime = null;
            } else {
                this.EndTime = rs.getString("EndTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LLCaseOpTime表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLCaseOpTimeSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LLCaseOpTimeSchema getSchema() {
        LLCaseOpTimeSchema aLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
        aLLCaseOpTimeSchema.setSchema(this);
        return aLLCaseOpTimeSchema;
    }

    public LLCaseOpTimeDB getDB() {
        LLCaseOpTimeDB aDBOper = new LLCaseOpTimeDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLCaseOpTime描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(CaseNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RgtState));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Sequance));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PageNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OpTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(StartDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StartTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(EndDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EndTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLCaseOpTime>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            CaseNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                    SysConst.PACKAGESPILTER);
            RgtState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            Sequance = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 3, SysConst.PACKAGESPILTER))).intValue();
            PageNo = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 4, SysConst.PACKAGESPILTER))).intValue();
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                       SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                      SysConst.PACKAGESPILTER);
            OpTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                    SysConst.PACKAGESPILTER);
            StartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 8, SysConst.PACKAGESPILTER));
            StartTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                       SysConst.PACKAGESPILTER);
            EndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 10, SysConst.PACKAGESPILTER));
            EndTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                     SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLCaseOpTimeSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("CaseNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CaseNo));
        }
        if (FCode.equals("RgtState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RgtState));
        }
        if (FCode.equals("Sequance")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Sequance));
        }
        if (FCode.equals("PageNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PageNo));
        }
        if (FCode.equals("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("OpTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OpTime));
        }
        if (FCode.equals("StartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getStartDate()));
        }
        if (FCode.equals("StartTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StartTime));
        }
        if (FCode.equals("EndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getEndDate()));
        }
        if (FCode.equals("EndTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EndTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(CaseNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(RgtState);
            break;
        case 2:
            strFieldValue = String.valueOf(Sequance);
            break;
        case 3:
            strFieldValue = String.valueOf(PageNo);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(ManageCom);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(OpTime);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getStartDate()));
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(StartTime);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.getEndDate()));
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(EndTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("CaseNo")) {
            if (FValue != null && !FValue.equals("")) {
                CaseNo = FValue.trim();
            } else {
                CaseNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("RgtState")) {
            if (FValue != null && !FValue.equals("")) {
                RgtState = FValue.trim();
            } else {
                RgtState = null;
            }
        }
        if (FCode.equalsIgnoreCase("Sequance")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                Sequance = i;
            }
        }
        if (FCode.equalsIgnoreCase("PageNo")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                PageNo = i;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ManageCom = FValue.trim();
            } else {
                ManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("OpTime")) {
            if (FValue != null && !FValue.equals("")) {
                OpTime = FValue.trim();
            } else {
                OpTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("StartDate")) {
            if (FValue != null && !FValue.equals("")) {
                StartDate = fDate.getDate(FValue);
            } else {
                StartDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("StartTime")) {
            if (FValue != null && !FValue.equals("")) {
                StartTime = FValue.trim();
            } else {
                StartTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            if (FValue != null && !FValue.equals("")) {
                EndDate = fDate.getDate(FValue);
            } else {
                EndDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("EndTime")) {
            if (FValue != null && !FValue.equals("")) {
                EndTime = FValue.trim();
            } else {
                EndTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LLCaseOpTimeSchema other = (LLCaseOpTimeSchema) otherObject;
        return
                CaseNo.equals(other.getCaseNo())
                && RgtState.equals(other.getRgtState())
                && Sequance == other.getSequance()
                && PageNo == other.getPageNo()
                && ManageCom.equals(other.getManageCom())
                && Operator.equals(other.getOperator())
                && OpTime.equals(other.getOpTime())
                && fDate.getString(StartDate).equals(other.getStartDate())
                && StartTime.equals(other.getStartTime())
                && fDate.getString(EndDate).equals(other.getEndDate())
                && EndTime.equals(other.getEndTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("CaseNo")) {
            return 0;
        }
        if (strFieldName.equals("RgtState")) {
            return 1;
        }
        if (strFieldName.equals("Sequance")) {
            return 2;
        }
        if (strFieldName.equals("PageNo")) {
            return 3;
        }
        if (strFieldName.equals("ManageCom")) {
            return 4;
        }
        if (strFieldName.equals("Operator")) {
            return 5;
        }
        if (strFieldName.equals("OpTime")) {
            return 6;
        }
        if (strFieldName.equals("StartDate")) {
            return 7;
        }
        if (strFieldName.equals("StartTime")) {
            return 8;
        }
        if (strFieldName.equals("EndDate")) {
            return 9;
        }
        if (strFieldName.equals("EndTime")) {
            return 10;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "CaseNo";
            break;
        case 1:
            strFieldName = "RgtState";
            break;
        case 2:
            strFieldName = "Sequance";
            break;
        case 3:
            strFieldName = "PageNo";
            break;
        case 4:
            strFieldName = "ManageCom";
            break;
        case 5:
            strFieldName = "Operator";
            break;
        case 6:
            strFieldName = "OpTime";
            break;
        case 7:
            strFieldName = "StartDate";
            break;
        case 8:
            strFieldName = "StartTime";
            break;
        case 9:
            strFieldName = "EndDate";
            break;
        case 10:
            strFieldName = "EndTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("CaseNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RgtState")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Sequance")) {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("PageNo")) {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("ManageCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OpTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StartDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("StartTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EndDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("EndTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_INT;
            break;
        case 3:
            nFieldType = Schema.TYPE_INT;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
