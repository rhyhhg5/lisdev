/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.ImageDetailed_ZBX_SLDB;

/*
 * <p>ClassName: ImageDetailed_ZBX_SLSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2017-10-19
 */
public class ImageDetailed_ZBX_SLSchema implements Schema, Cloneable
{
	// @Field
	/** Tranno */
	private String Tranno;
	/** Batchno */
	private String BatchNo;
	/** Funcflag */
	private String FuncFlag;
	/** Proposalno */
	private String ProposalNo;
	/** Contno */
	private String ContNo;
	/** Manegecom */
	private String ManegeCom;
	/** Sourcechannel */
	private String SourceChannel;
	/** Filename */
	private String FileName;
	/** Sourcepath */
	private String SourcePath;
	/** Task_id */
	private String task_id;
	/** Osspath */
	private String OssPath;
	/** Ossurl */
	private String OssUrl;
	/** Osskey */
	private String OSSKey;
	/** Bucketname */
	private String BucketName;
	/** Imagemessage */
	private InputStream ImageMessage;
	/** Status */
	private String Status;
	/** Checkmes */
	private String CheckMes;
	/** Statusmsg */
	private String StatusMsg;
	/** Makedate */
	private Date MakeDate;
	/** Modifydate */
	private Date ModifyDate;
	/** Etag */
	private String ETag;
	/** Uploadid */
	private String UploadId;
	/** Nonce */
	private String nonce;
	/** Bak1 */
	private String bak1;
	/** Bak2 */
	private String bak2;
	/** Bak3 */
	private String bak3;

	public static final int FIELDNUM = 26;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public ImageDetailed_ZBX_SLSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "Tranno";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		ImageDetailed_ZBX_SLSchema cloned = (ImageDetailed_ZBX_SLSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getTranno()
	{
		return Tranno;
	}
	public void setTranno(String aTranno)
	{
		Tranno = aTranno;
	}
	public String getBatchNo()
	{
		return BatchNo;
	}
	public void setBatchNo(String aBatchNo)
	{
		BatchNo = aBatchNo;
	}
	public String getFuncFlag()
	{
		return FuncFlag;
	}
	public void setFuncFlag(String aFuncFlag)
	{
		FuncFlag = aFuncFlag;
	}
	public String getProposalNo()
	{
		return ProposalNo;
	}
	public void setProposalNo(String aProposalNo)
	{
		ProposalNo = aProposalNo;
	}
	public String getContNo()
	{
		return ContNo;
	}
	public void setContNo(String aContNo)
	{
		ContNo = aContNo;
	}
	public String getManegeCom()
	{
		return ManegeCom;
	}
	public void setManegeCom(String aManegeCom)
	{
		ManegeCom = aManegeCom;
	}
	public String getSourceChannel()
	{
		return SourceChannel;
	}
	public void setSourceChannel(String aSourceChannel)
	{
		SourceChannel = aSourceChannel;
	}
	public String getFileName()
	{
		return FileName;
	}
	public void setFileName(String aFileName)
	{
		FileName = aFileName;
	}
	public String getSourcePath()
	{
		return SourcePath;
	}
	public void setSourcePath(String aSourcePath)
	{
		SourcePath = aSourcePath;
	}
	public String gettask_id()
	{
		return task_id;
	}
	public void settask_id(String atask_id)
	{
		task_id = atask_id;
	}
	public String getOssPath()
	{
		return OssPath;
	}
	public void setOssPath(String aOssPath)
	{
		OssPath = aOssPath;
	}
	public String getOssUrl()
	{
		return OssUrl;
	}
	public void setOssUrl(String aOssUrl)
	{
		OssUrl = aOssUrl;
	}
	public String getOSSKey()
	{
		return OSSKey;
	}
	public void setOSSKey(String aOSSKey)
	{
		OSSKey = aOSSKey;
	}
	public String getBucketName()
	{
		return BucketName;
	}
	public void setBucketName(String aBucketName)
	{
		BucketName = aBucketName;
	}
	public InputStream getImageMessage()
	{
		return ImageMessage;
	}
	public void setImageMessage(InputStream aImageMessage)
	{
		ImageMessage = aImageMessage;
	}
	public void setImageMessage(String aImageMessage)
	{
	}

	public String getStatus()
	{
		return Status;
	}
	public void setStatus(String aStatus)
	{
		Status = aStatus;
	}
	public String getCheckMes()
	{
		return CheckMes;
	}
	public void setCheckMes(String aCheckMes)
	{
		CheckMes = aCheckMes;
	}
	public String getStatusMsg()
	{
		return StatusMsg;
	}
	public void setStatusMsg(String aStatusMsg)
	{
		StatusMsg = aStatusMsg;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getETag()
	{
		return ETag;
	}
	public void setETag(String aETag)
	{
		ETag = aETag;
	}
	public String getUploadId()
	{
		return UploadId;
	}
	public void setUploadId(String aUploadId)
	{
		UploadId = aUploadId;
	}
	public String getnonce()
	{
		return nonce;
	}
	public void setnonce(String anonce)
	{
		nonce = anonce;
	}
	public String getbak1()
	{
		return bak1;
	}
	public void setbak1(String abak1)
	{
		bak1 = abak1;
	}
	public String getbak2()
	{
		return bak2;
	}
	public void setbak2(String abak2)
	{
		bak2 = abak2;
	}
	public String getbak3()
	{
		return bak3;
	}
	public void setbak3(String abak3)
	{
		bak3 = abak3;
	}

	/**
	* 使用另外一个 ImageDetailed_ZBX_SLSchema 对象给 Schema 赋值
	* @param: aImageDetailed_ZBX_SLSchema ImageDetailed_ZBX_SLSchema
	**/
	public void setSchema(ImageDetailed_ZBX_SLSchema aImageDetailed_ZBX_SLSchema)
	{
		this.Tranno = aImageDetailed_ZBX_SLSchema.getTranno();
		this.BatchNo = aImageDetailed_ZBX_SLSchema.getBatchNo();
		this.FuncFlag = aImageDetailed_ZBX_SLSchema.getFuncFlag();
		this.ProposalNo = aImageDetailed_ZBX_SLSchema.getProposalNo();
		this.ContNo = aImageDetailed_ZBX_SLSchema.getContNo();
		this.ManegeCom = aImageDetailed_ZBX_SLSchema.getManegeCom();
		this.SourceChannel = aImageDetailed_ZBX_SLSchema.getSourceChannel();
		this.FileName = aImageDetailed_ZBX_SLSchema.getFileName();
		this.SourcePath = aImageDetailed_ZBX_SLSchema.getSourcePath();
		this.task_id = aImageDetailed_ZBX_SLSchema.gettask_id();
		this.OssPath = aImageDetailed_ZBX_SLSchema.getOssPath();
		this.OssUrl = aImageDetailed_ZBX_SLSchema.getOssUrl();
		this.OSSKey = aImageDetailed_ZBX_SLSchema.getOSSKey();
		this.BucketName = aImageDetailed_ZBX_SLSchema.getBucketName();
		this.ImageMessage = aImageDetailed_ZBX_SLSchema.getImageMessage();
		this.Status = aImageDetailed_ZBX_SLSchema.getStatus();
		this.CheckMes = aImageDetailed_ZBX_SLSchema.getCheckMes();
		this.StatusMsg = aImageDetailed_ZBX_SLSchema.getStatusMsg();
		this.MakeDate = fDate.getDate( aImageDetailed_ZBX_SLSchema.getMakeDate());
		this.ModifyDate = fDate.getDate( aImageDetailed_ZBX_SLSchema.getModifyDate());
		this.ETag = aImageDetailed_ZBX_SLSchema.getETag();
		this.UploadId = aImageDetailed_ZBX_SLSchema.getUploadId();
		this.nonce = aImageDetailed_ZBX_SLSchema.getnonce();
		this.bak1 = aImageDetailed_ZBX_SLSchema.getbak1();
		this.bak2 = aImageDetailed_ZBX_SLSchema.getbak2();
		this.bak3 = aImageDetailed_ZBX_SLSchema.getbak3();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("Tranno") == null )
				this.Tranno = null;
			else
				this.Tranno = rs.getString("Tranno").trim();

			if( rs.getString("BatchNo") == null )
				this.BatchNo = null;
			else
				this.BatchNo = rs.getString("BatchNo").trim();

			if( rs.getString("FuncFlag") == null )
				this.FuncFlag = null;
			else
				this.FuncFlag = rs.getString("FuncFlag").trim();

			if( rs.getString("ProposalNo") == null )
				this.ProposalNo = null;
			else
				this.ProposalNo = rs.getString("ProposalNo").trim();

			if( rs.getString("ContNo") == null )
				this.ContNo = null;
			else
				this.ContNo = rs.getString("ContNo").trim();

			if( rs.getString("ManegeCom") == null )
				this.ManegeCom = null;
			else
				this.ManegeCom = rs.getString("ManegeCom").trim();

			if( rs.getString("SourceChannel") == null )
				this.SourceChannel = null;
			else
				this.SourceChannel = rs.getString("SourceChannel").trim();

			if( rs.getString("FileName") == null )
				this.FileName = null;
			else
				this.FileName = rs.getString("FileName").trim();

			if( rs.getString("SourcePath") == null )
				this.SourcePath = null;
			else
				this.SourcePath = rs.getString("SourcePath").trim();

			if( rs.getString("task_id") == null )
				this.task_id = null;
			else
				this.task_id = rs.getString("task_id").trim();

			if( rs.getString("OssPath") == null )
				this.OssPath = null;
			else
				this.OssPath = rs.getString("OssPath").trim();

			if( rs.getString("OssUrl") == null )
				this.OssUrl = null;
			else
				this.OssUrl = rs.getString("OssUrl").trim();

			if( rs.getString("OSSKey") == null )
				this.OSSKey = null;
			else
				this.OSSKey = rs.getString("OSSKey").trim();

			if( rs.getString("BucketName") == null )
				this.BucketName = null;
			else
				this.BucketName = rs.getString("BucketName").trim();

			this.ImageMessage = rs.getBinaryStream("ImageMessage");
			if( rs.getString("Status") == null )
				this.Status = null;
			else
				this.Status = rs.getString("Status").trim();

			if( rs.getString("CheckMes") == null )
				this.CheckMes = null;
			else
				this.CheckMes = rs.getString("CheckMes").trim();

			if( rs.getString("StatusMsg") == null )
				this.StatusMsg = null;
			else
				this.StatusMsg = rs.getString("StatusMsg").trim();

			this.MakeDate = rs.getDate("MakeDate");
			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ETag") == null )
				this.ETag = null;
			else
				this.ETag = rs.getString("ETag").trim();

			if( rs.getString("UploadId") == null )
				this.UploadId = null;
			else
				this.UploadId = rs.getString("UploadId").trim();

			if( rs.getString("nonce") == null )
				this.nonce = null;
			else
				this.nonce = rs.getString("nonce").trim();

			if( rs.getString("bak1") == null )
				this.bak1 = null;
			else
				this.bak1 = rs.getString("bak1").trim();

			if( rs.getString("bak2") == null )
				this.bak2 = null;
			else
				this.bak2 = rs.getString("bak2").trim();

			if( rs.getString("bak3") == null )
				this.bak3 = null;
			else
				this.bak3 = rs.getString("bak3").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的ImageDetailed_ZBX_SL表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ImageDetailed_ZBX_SLSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public ImageDetailed_ZBX_SLSchema getSchema()
	{
		ImageDetailed_ZBX_SLSchema aImageDetailed_ZBX_SLSchema = new ImageDetailed_ZBX_SLSchema();
		aImageDetailed_ZBX_SLSchema.setSchema(this);
		return aImageDetailed_ZBX_SLSchema;
	}

	public ImageDetailed_ZBX_SLDB getDB()
	{
		ImageDetailed_ZBX_SLDB aDBOper = new ImageDetailed_ZBX_SLDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpImageDetailed_ZBX_SL描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(Tranno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BatchNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FuncFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProposalNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManegeCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SourceChannel)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FileName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SourcePath)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(task_id)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OssPath)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OssUrl)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OSSKey)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BucketName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( 1 );strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Status)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CheckMes)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StatusMsg)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ETag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UploadId)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(nonce)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak3));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpImageDetailed_ZBX_SL>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			Tranno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			BatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			FuncFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			ProposalNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			ManegeCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			SourceChannel = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			FileName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			SourcePath = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			task_id = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			OssPath = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			OssUrl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			OSSKey = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			BucketName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			
			Status = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			CheckMes = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			StatusMsg = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,SysConst.PACKAGESPILTER));
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,SysConst.PACKAGESPILTER));
			ETag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			UploadId = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			nonce = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			bak1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			bak2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			bak3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ImageDetailed_ZBX_SLSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("Tranno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Tranno));
		}
		if (FCode.equals("BatchNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BatchNo));
		}
		if (FCode.equals("FuncFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FuncFlag));
		}
		if (FCode.equals("ProposalNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProposalNo));
		}
		if (FCode.equals("ContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
		}
		if (FCode.equals("ManegeCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManegeCom));
		}
		if (FCode.equals("SourceChannel"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SourceChannel));
		}
		if (FCode.equals("FileName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FileName));
		}
		if (FCode.equals("SourcePath"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SourcePath));
		}
		if (FCode.equals("task_id"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(task_id));
		}
		if (FCode.equals("OssPath"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OssPath));
		}
		if (FCode.equals("OssUrl"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OssUrl));
		}
		if (FCode.equals("OSSKey"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OSSKey));
		}
		if (FCode.equals("BucketName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BucketName));
		}
		if (FCode.equals("ImageMessage"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ImageMessage));
		}
		if (FCode.equals("Status"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Status));
		}
		if (FCode.equals("CheckMes"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CheckMes));
		}
		if (FCode.equals("StatusMsg"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StatusMsg));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ETag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ETag));
		}
		if (FCode.equals("UploadId"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UploadId));
		}
		if (FCode.equals("nonce"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(nonce));
		}
		if (FCode.equals("bak1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak1));
		}
		if (FCode.equals("bak2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak2));
		}
		if (FCode.equals("bak3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak3));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(Tranno);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(BatchNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(FuncFlag);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(ProposalNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(ContNo);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(ManegeCom);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(SourceChannel);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(FileName);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(SourcePath);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(task_id);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(OssPath);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(OssUrl);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(OSSKey);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(BucketName);
				break;
			case 14:
				strFieldValue = String.valueOf(ImageMessage);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(Status);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(CheckMes);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(StatusMsg);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(ETag);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(UploadId);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(nonce);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(bak1);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(bak2);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(bak3);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("Tranno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Tranno = FValue.trim();
			}
			else
				Tranno = null;
		}
		if (FCode.equalsIgnoreCase("BatchNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BatchNo = FValue.trim();
			}
			else
				BatchNo = null;
		}
		if (FCode.equalsIgnoreCase("FuncFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FuncFlag = FValue.trim();
			}
			else
				FuncFlag = null;
		}
		if (FCode.equalsIgnoreCase("ProposalNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProposalNo = FValue.trim();
			}
			else
				ProposalNo = null;
		}
		if (FCode.equalsIgnoreCase("ContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContNo = FValue.trim();
			}
			else
				ContNo = null;
		}
		if (FCode.equalsIgnoreCase("ManegeCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManegeCom = FValue.trim();
			}
			else
				ManegeCom = null;
		}
		if (FCode.equalsIgnoreCase("SourceChannel"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SourceChannel = FValue.trim();
			}
			else
				SourceChannel = null;
		}
		if (FCode.equalsIgnoreCase("FileName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FileName = FValue.trim();
			}
			else
				FileName = null;
		}
		if (FCode.equalsIgnoreCase("SourcePath"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SourcePath = FValue.trim();
			}
			else
				SourcePath = null;
		}
		if (FCode.equalsIgnoreCase("task_id"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				task_id = FValue.trim();
			}
			else
				task_id = null;
		}
		if (FCode.equalsIgnoreCase("OssPath"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OssPath = FValue.trim();
			}
			else
				OssPath = null;
		}
		if (FCode.equalsIgnoreCase("OssUrl"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OssUrl = FValue.trim();
			}
			else
				OssUrl = null;
		}
		if (FCode.equalsIgnoreCase("OSSKey"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OSSKey = FValue.trim();
			}
			else
				OSSKey = null;
		}
		if (FCode.equalsIgnoreCase("BucketName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BucketName = FValue.trim();
			}
			else
				BucketName = null;
		}
		if (FCode.equalsIgnoreCase("ImageMessage"))
		{
		}
		if (FCode.equalsIgnoreCase("Status"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Status = FValue.trim();
			}
			else
				Status = null;
		}
		if (FCode.equalsIgnoreCase("CheckMes"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CheckMes = FValue.trim();
			}
			else
				CheckMes = null;
		}
		if (FCode.equalsIgnoreCase("StatusMsg"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StatusMsg = FValue.trim();
			}
			else
				StatusMsg = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ETag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ETag = FValue.trim();
			}
			else
				ETag = null;
		}
		if (FCode.equalsIgnoreCase("UploadId"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UploadId = FValue.trim();
			}
			else
				UploadId = null;
		}
		if (FCode.equalsIgnoreCase("nonce"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				nonce = FValue.trim();
			}
			else
				nonce = null;
		}
		if (FCode.equalsIgnoreCase("bak1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak1 = FValue.trim();
			}
			else
				bak1 = null;
		}
		if (FCode.equalsIgnoreCase("bak2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak2 = FValue.trim();
			}
			else
				bak2 = null;
		}
		if (FCode.equalsIgnoreCase("bak3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak3 = FValue.trim();
			}
			else
				bak3 = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		ImageDetailed_ZBX_SLSchema other = (ImageDetailed_ZBX_SLSchema)otherObject;
		return
			(Tranno == null ? other.getTranno() == null : Tranno.equals(other.getTranno()))
			&& (BatchNo == null ? other.getBatchNo() == null : BatchNo.equals(other.getBatchNo()))
			&& (FuncFlag == null ? other.getFuncFlag() == null : FuncFlag.equals(other.getFuncFlag()))
			&& (ProposalNo == null ? other.getProposalNo() == null : ProposalNo.equals(other.getProposalNo()))
			&& (ContNo == null ? other.getContNo() == null : ContNo.equals(other.getContNo()))
			&& (ManegeCom == null ? other.getManegeCom() == null : ManegeCom.equals(other.getManegeCom()))
			&& (SourceChannel == null ? other.getSourceChannel() == null : SourceChannel.equals(other.getSourceChannel()))
			&& (FileName == null ? other.getFileName() == null : FileName.equals(other.getFileName()))
			&& (SourcePath == null ? other.getSourcePath() == null : SourcePath.equals(other.getSourcePath()))
			&& (task_id == null ? other.gettask_id() == null : task_id.equals(other.gettask_id()))
			&& (OssPath == null ? other.getOssPath() == null : OssPath.equals(other.getOssPath()))
			&& (OssUrl == null ? other.getOssUrl() == null : OssUrl.equals(other.getOssUrl()))
			&& (OSSKey == null ? other.getOSSKey() == null : OSSKey.equals(other.getOSSKey()))
			&& (BucketName == null ? other.getBucketName() == null : BucketName.equals(other.getBucketName()))
			
			&& (Status == null ? other.getStatus() == null : Status.equals(other.getStatus()))
			&& (CheckMes == null ? other.getCheckMes() == null : CheckMes.equals(other.getCheckMes()))
			&& (StatusMsg == null ? other.getStatusMsg() == null : StatusMsg.equals(other.getStatusMsg()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ETag == null ? other.getETag() == null : ETag.equals(other.getETag()))
			&& (UploadId == null ? other.getUploadId() == null : UploadId.equals(other.getUploadId()))
			&& (nonce == null ? other.getnonce() == null : nonce.equals(other.getnonce()))
			&& (bak1 == null ? other.getbak1() == null : bak1.equals(other.getbak1()))
			&& (bak2 == null ? other.getbak2() == null : bak2.equals(other.getbak2()))
			&& (bak3 == null ? other.getbak3() == null : bak3.equals(other.getbak3()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("Tranno") ) {
			return 0;
		}
		if( strFieldName.equals("BatchNo") ) {
			return 1;
		}
		if( strFieldName.equals("FuncFlag") ) {
			return 2;
		}
		if( strFieldName.equals("ProposalNo") ) {
			return 3;
		}
		if( strFieldName.equals("ContNo") ) {
			return 4;
		}
		if( strFieldName.equals("ManegeCom") ) {
			return 5;
		}
		if( strFieldName.equals("SourceChannel") ) {
			return 6;
		}
		if( strFieldName.equals("FileName") ) {
			return 7;
		}
		if( strFieldName.equals("SourcePath") ) {
			return 8;
		}
		if( strFieldName.equals("task_id") ) {
			return 9;
		}
		if( strFieldName.equals("OssPath") ) {
			return 10;
		}
		if( strFieldName.equals("OssUrl") ) {
			return 11;
		}
		if( strFieldName.equals("OSSKey") ) {
			return 12;
		}
		if( strFieldName.equals("BucketName") ) {
			return 13;
		}
		if( strFieldName.equals("ImageMessage") ) {
			return 14;
		}
		if( strFieldName.equals("Status") ) {
			return 15;
		}
		if( strFieldName.equals("CheckMes") ) {
			return 16;
		}
		if( strFieldName.equals("StatusMsg") ) {
			return 17;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 18;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 19;
		}
		if( strFieldName.equals("ETag") ) {
			return 20;
		}
		if( strFieldName.equals("UploadId") ) {
			return 21;
		}
		if( strFieldName.equals("nonce") ) {
			return 22;
		}
		if( strFieldName.equals("bak1") ) {
			return 23;
		}
		if( strFieldName.equals("bak2") ) {
			return 24;
		}
		if( strFieldName.equals("bak3") ) {
			return 25;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "Tranno";
				break;
			case 1:
				strFieldName = "BatchNo";
				break;
			case 2:
				strFieldName = "FuncFlag";
				break;
			case 3:
				strFieldName = "ProposalNo";
				break;
			case 4:
				strFieldName = "ContNo";
				break;
			case 5:
				strFieldName = "ManegeCom";
				break;
			case 6:
				strFieldName = "SourceChannel";
				break;
			case 7:
				strFieldName = "FileName";
				break;
			case 8:
				strFieldName = "SourcePath";
				break;
			case 9:
				strFieldName = "task_id";
				break;
			case 10:
				strFieldName = "OssPath";
				break;
			case 11:
				strFieldName = "OssUrl";
				break;
			case 12:
				strFieldName = "OSSKey";
				break;
			case 13:
				strFieldName = "BucketName";
				break;
			case 14:
				strFieldName = "ImageMessage";
				break;
			case 15:
				strFieldName = "Status";
				break;
			case 16:
				strFieldName = "CheckMes";
				break;
			case 17:
				strFieldName = "StatusMsg";
				break;
			case 18:
				strFieldName = "MakeDate";
				break;
			case 19:
				strFieldName = "ModifyDate";
				break;
			case 20:
				strFieldName = "ETag";
				break;
			case 21:
				strFieldName = "UploadId";
				break;
			case 22:
				strFieldName = "nonce";
				break;
			case 23:
				strFieldName = "bak1";
				break;
			case 24:
				strFieldName = "bak2";
				break;
			case 25:
				strFieldName = "bak3";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("Tranno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BatchNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FuncFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ProposalNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManegeCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SourceChannel") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FileName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SourcePath") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("task_id") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OssPath") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OssUrl") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OSSKey") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BucketName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ImageMessage") ) {
			return Schema.TYPE_NOFOUND;
		}
		if( strFieldName.equals("Status") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CheckMes") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StatusMsg") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ETag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UploadId") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("nonce") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak3") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_NOFOUND;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 19:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
