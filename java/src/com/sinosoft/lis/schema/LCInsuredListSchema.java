/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LCInsuredListDB;

/*
 * <p>ClassName: LCInsuredListSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2019-07-22
 */
public class LCInsuredListSchema implements Schema, Cloneable
{
	// @Field
	/** 集体合同号码 */
	private String GrpContNo;
	/** 被保人序号 */
	private String InsuredID;
	/** 状态 */
	private String State;
	/** 合同号码 */
	private String ContNo;
	/** 批次号 */
	private String BatchNo;
	/** 被保人客户号 */
	private String InsuredNo;
	/** 在职/退休 */
	private String Retire;
	/** 员工姓名 */
	private String EmployeeName;
	/** 被保人姓名 */
	private String InsuredName;
	/** 与员工关系 */
	private String Relation;
	/** 性别 */
	private String Sex;
	/** 出生日期 */
	private Date Birthday;
	/** 证件类型 */
	private String IDType;
	/** 证件号码 */
	private String IDNo;
	/** 保险计划 */
	private String ContPlanCode;
	/** 职业类别 */
	private String OccupationType;
	/** 理赔金转帐银行 */
	private String BankCode;
	/** 帐号 */
	private String BankAccNo;
	/** 户名 */
	private String AccName;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 公共账户类型 */
	private String PublicAccType;
	/** 公共账户 */
	private double PublicAcc;
	/** 保全号 */
	private String EdorNo;
	/** 险种号码 */
	private String RiskCode;
	/** 保费计算方式 */
	private String CalPremType;
	/** 其它证件类型 */
	private String OthIDType;
	/** 其它证件号码 */
	private String OthIDNo;
	/** 英文名称 */
	private String EnglishName;
	/** 电话 */
	private String Phone;
	/** 计算规则 */
	private String CalRule;
	/** 无名单人数 */
	private int NoNamePeoples;
	/** 保全生效日期 */
	private Date EdorValiDate;
	/** 期交保费 */
	private String Prem;
	/** 未满期保费 */
	private String EdorPrem;
	/** 单位编码 */
	private String GrpNo;
	/** 单位名称 */
	private String GrpName;
	/** 社保分中心代码 */
	private String SocialCenterNo;
	/** 社保中心名称 */
	private String SocialCenterName;
	/** 投保单填写日期 */
	private Date HandlerDate;
	/** 投保人签章 */
	private String HandlerPrint;
	/** 投保人 */
	private String Handler;
	/** 入司日期 */
	private Date JoinCompanyDate;
	/** 职级 */
	private String Position;
	/** 归属规则 */
	private String AscriptionRuleCode;
	/** 领取年龄年期 */
	private int GetYear;
	/** 领取年龄年期标志 */
	private String GetYearFlag;
	/** 给付责任类型 */
	private String GetDutyKind;
	/** 投保人缴费 */
	private double AppntPrem;
	/** 个人协议缴费 */
	private double PersonPrem;
	/** 个人自愿缴费 */
	private double PersonOwnPrem;
	/** 个人直接缴费转账银行 */
	private String PerBankCode;
	/** 个人直接缴费账号 */
	private String PerBankAccNo;
	/** 个人直接缴费户名 */
	private String PerAccName;
	/** 学校 */
	private String SchoolNmae;
	/** 班级 */
	private String ClassName;
	/** 投保人姓名 */
	private String AppntName;
	/** 投保人性别 */
	private String AppntSex;
	/** 投保人出生日期 */
	private Date AppntBirthday;
	/** 投保人证件类型 */
	private String AppntIdType;
	/** 投保人证件号码 */
	private String AppntIdNo;
	/** 国籍 */
	private String NativePlace;
	/** 岗位 */
	private String Position2;
	/** 职业代码 */
	private String OccupationCode;
	/** 国家 */
	private String NativeCity;
	/** 生僻字标识 */
	private String UnCommonChar;

	public static final int FIELDNUM = 69;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LCInsuredListSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "GrpContNo";
		pk[1] = "InsuredID";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LCInsuredListSchema cloned = (LCInsuredListSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getGrpContNo()
	{
		return GrpContNo;
	}
	public void setGrpContNo(String aGrpContNo)
	{
		GrpContNo = aGrpContNo;
	}
	public String getInsuredID()
	{
		return InsuredID;
	}
	public void setInsuredID(String aInsuredID)
	{
		InsuredID = aInsuredID;
	}
	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}
	public String getContNo()
	{
		return ContNo;
	}
	public void setContNo(String aContNo)
	{
		ContNo = aContNo;
	}
	public String getBatchNo()
	{
		return BatchNo;
	}
	public void setBatchNo(String aBatchNo)
	{
		BatchNo = aBatchNo;
	}
	public String getInsuredNo()
	{
		return InsuredNo;
	}
	public void setInsuredNo(String aInsuredNo)
	{
		InsuredNo = aInsuredNo;
	}
	public String getRetire()
	{
		return Retire;
	}
	public void setRetire(String aRetire)
	{
		Retire = aRetire;
	}
	public String getEmployeeName()
	{
		return EmployeeName;
	}
	public void setEmployeeName(String aEmployeeName)
	{
		EmployeeName = aEmployeeName;
	}
	public String getInsuredName()
	{
		return InsuredName;
	}
	public void setInsuredName(String aInsuredName)
	{
		InsuredName = aInsuredName;
	}
	public String getRelation()
	{
		return Relation;
	}
	public void setRelation(String aRelation)
	{
		Relation = aRelation;
	}
	public String getSex()
	{
		return Sex;
	}
	public void setSex(String aSex)
	{
		Sex = aSex;
	}
	public String getBirthday()
	{
		if( Birthday != null )
			return fDate.getString(Birthday);
		else
			return null;
	}
	public void setBirthday(Date aBirthday)
	{
		Birthday = aBirthday;
	}
	public void setBirthday(String aBirthday)
	{
		if (aBirthday != null && !aBirthday.equals("") )
		{
			Birthday = fDate.getDate( aBirthday );
		}
		else
			Birthday = null;
	}

	public String getIDType()
	{
		return IDType;
	}
	public void setIDType(String aIDType)
	{
		IDType = aIDType;
	}
	public String getIDNo()
	{
		return IDNo;
	}
	public void setIDNo(String aIDNo)
	{
		IDNo = aIDNo;
	}
	public String getContPlanCode()
	{
		return ContPlanCode;
	}
	public void setContPlanCode(String aContPlanCode)
	{
		ContPlanCode = aContPlanCode;
	}
	public String getOccupationType()
	{
		return OccupationType;
	}
	public void setOccupationType(String aOccupationType)
	{
		OccupationType = aOccupationType;
	}
	public String getBankCode()
	{
		return BankCode;
	}
	public void setBankCode(String aBankCode)
	{
		BankCode = aBankCode;
	}
	public String getBankAccNo()
	{
		return BankAccNo;
	}
	public void setBankAccNo(String aBankAccNo)
	{
		BankAccNo = aBankAccNo;
	}
	public String getAccName()
	{
		return AccName;
	}
	public void setAccName(String aAccName)
	{
		AccName = aAccName;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getPublicAccType()
	{
		return PublicAccType;
	}
	public void setPublicAccType(String aPublicAccType)
	{
		PublicAccType = aPublicAccType;
	}
	public double getPublicAcc()
	{
		return PublicAcc;
	}
	public void setPublicAcc(double aPublicAcc)
	{
		PublicAcc = Arith.round(aPublicAcc,2);
	}
	public void setPublicAcc(String aPublicAcc)
	{
		if (aPublicAcc != null && !aPublicAcc.equals(""))
		{
			Double tDouble = new Double(aPublicAcc);
			double d = tDouble.doubleValue();
                PublicAcc = Arith.round(d,2);
		}
	}

	public String getEdorNo()
	{
		return EdorNo;
	}
	public void setEdorNo(String aEdorNo)
	{
		EdorNo = aEdorNo;
	}
	public String getRiskCode()
	{
		return RiskCode;
	}
	public void setRiskCode(String aRiskCode)
	{
		RiskCode = aRiskCode;
	}
	public String getCalPremType()
	{
		return CalPremType;
	}
	public void setCalPremType(String aCalPremType)
	{
		CalPremType = aCalPremType;
	}
	public String getOthIDType()
	{
		return OthIDType;
	}
	public void setOthIDType(String aOthIDType)
	{
		OthIDType = aOthIDType;
	}
	public String getOthIDNo()
	{
		return OthIDNo;
	}
	public void setOthIDNo(String aOthIDNo)
	{
		OthIDNo = aOthIDNo;
	}
	public String getEnglishName()
	{
		return EnglishName;
	}
	public void setEnglishName(String aEnglishName)
	{
		EnglishName = aEnglishName;
	}
	public String getPhone()
	{
		return Phone;
	}
	public void setPhone(String aPhone)
	{
		Phone = aPhone;
	}
	public String getCalRule()
	{
		return CalRule;
	}
	public void setCalRule(String aCalRule)
	{
		CalRule = aCalRule;
	}
	public int getNoNamePeoples()
	{
		return NoNamePeoples;
	}
	public void setNoNamePeoples(int aNoNamePeoples)
	{
		NoNamePeoples = aNoNamePeoples;
	}
	public void setNoNamePeoples(String aNoNamePeoples)
	{
		if (aNoNamePeoples != null && !aNoNamePeoples.equals(""))
		{
			Integer tInteger = new Integer(aNoNamePeoples);
			int i = tInteger.intValue();
			NoNamePeoples = i;
		}
	}

	public String getEdorValiDate()
	{
		if( EdorValiDate != null )
			return fDate.getString(EdorValiDate);
		else
			return null;
	}
	public void setEdorValiDate(Date aEdorValiDate)
	{
		EdorValiDate = aEdorValiDate;
	}
	public void setEdorValiDate(String aEdorValiDate)
	{
		if (aEdorValiDate != null && !aEdorValiDate.equals("") )
		{
			EdorValiDate = fDate.getDate( aEdorValiDate );
		}
		else
			EdorValiDate = null;
	}

	public String getPrem()
	{
		return Prem;
	}
	public void setPrem(String aPrem)
	{
		Prem = aPrem;
	}
	public String getEdorPrem()
	{
		return EdorPrem;
	}
	public void setEdorPrem(String aEdorPrem)
	{
		EdorPrem = aEdorPrem;
	}
	public String getGrpNo()
	{
		return GrpNo;
	}
	public void setGrpNo(String aGrpNo)
	{
		GrpNo = aGrpNo;
	}
	public String getGrpName()
	{
		return GrpName;
	}
	public void setGrpName(String aGrpName)
	{
		GrpName = aGrpName;
	}
	public String getSocialCenterNo()
	{
		return SocialCenterNo;
	}
	public void setSocialCenterNo(String aSocialCenterNo)
	{
		SocialCenterNo = aSocialCenterNo;
	}
	public String getSocialCenterName()
	{
		return SocialCenterName;
	}
	public void setSocialCenterName(String aSocialCenterName)
	{
		SocialCenterName = aSocialCenterName;
	}
	public String getHandlerDate()
	{
		if( HandlerDate != null )
			return fDate.getString(HandlerDate);
		else
			return null;
	}
	public void setHandlerDate(Date aHandlerDate)
	{
		HandlerDate = aHandlerDate;
	}
	public void setHandlerDate(String aHandlerDate)
	{
		if (aHandlerDate != null && !aHandlerDate.equals("") )
		{
			HandlerDate = fDate.getDate( aHandlerDate );
		}
		else
			HandlerDate = null;
	}

	public String getHandlerPrint()
	{
		return HandlerPrint;
	}
	public void setHandlerPrint(String aHandlerPrint)
	{
		HandlerPrint = aHandlerPrint;
	}
	public String getHandler()
	{
		return Handler;
	}
	public void setHandler(String aHandler)
	{
		Handler = aHandler;
	}
	public String getJoinCompanyDate()
	{
		if( JoinCompanyDate != null )
			return fDate.getString(JoinCompanyDate);
		else
			return null;
	}
	public void setJoinCompanyDate(Date aJoinCompanyDate)
	{
		JoinCompanyDate = aJoinCompanyDate;
	}
	public void setJoinCompanyDate(String aJoinCompanyDate)
	{
		if (aJoinCompanyDate != null && !aJoinCompanyDate.equals("") )
		{
			JoinCompanyDate = fDate.getDate( aJoinCompanyDate );
		}
		else
			JoinCompanyDate = null;
	}

	public String getPosition()
	{
		return Position;
	}
	public void setPosition(String aPosition)
	{
		Position = aPosition;
	}
	public String getAscriptionRuleCode()
	{
		return AscriptionRuleCode;
	}
	public void setAscriptionRuleCode(String aAscriptionRuleCode)
	{
		AscriptionRuleCode = aAscriptionRuleCode;
	}
	public int getGetYear()
	{
		return GetYear;
	}
	public void setGetYear(int aGetYear)
	{
		GetYear = aGetYear;
	}
	public void setGetYear(String aGetYear)
	{
		if (aGetYear != null && !aGetYear.equals(""))
		{
			Integer tInteger = new Integer(aGetYear);
			int i = tInteger.intValue();
			GetYear = i;
		}
	}

	public String getGetYearFlag()
	{
		return GetYearFlag;
	}
	public void setGetYearFlag(String aGetYearFlag)
	{
		GetYearFlag = aGetYearFlag;
	}
	public String getGetDutyKind()
	{
		return GetDutyKind;
	}
	public void setGetDutyKind(String aGetDutyKind)
	{
		GetDutyKind = aGetDutyKind;
	}
	public double getAppntPrem()
	{
		return AppntPrem;
	}
	public void setAppntPrem(double aAppntPrem)
	{
		AppntPrem = Arith.round(aAppntPrem,2);
	}
	public void setAppntPrem(String aAppntPrem)
	{
		if (aAppntPrem != null && !aAppntPrem.equals(""))
		{
			Double tDouble = new Double(aAppntPrem);
			double d = tDouble.doubleValue();
                AppntPrem = Arith.round(d,2);
		}
	}

	public double getPersonPrem()
	{
		return PersonPrem;
	}
	public void setPersonPrem(double aPersonPrem)
	{
		PersonPrem = Arith.round(aPersonPrem,2);
	}
	public void setPersonPrem(String aPersonPrem)
	{
		if (aPersonPrem != null && !aPersonPrem.equals(""))
		{
			Double tDouble = new Double(aPersonPrem);
			double d = tDouble.doubleValue();
                PersonPrem = Arith.round(d,2);
		}
	}

	public double getPersonOwnPrem()
	{
		return PersonOwnPrem;
	}
	public void setPersonOwnPrem(double aPersonOwnPrem)
	{
		PersonOwnPrem = Arith.round(aPersonOwnPrem,2);
	}
	public void setPersonOwnPrem(String aPersonOwnPrem)
	{
		if (aPersonOwnPrem != null && !aPersonOwnPrem.equals(""))
		{
			Double tDouble = new Double(aPersonOwnPrem);
			double d = tDouble.doubleValue();
                PersonOwnPrem = Arith.round(d,2);
		}
	}

	public String getPerBankCode()
	{
		return PerBankCode;
	}
	public void setPerBankCode(String aPerBankCode)
	{
		PerBankCode = aPerBankCode;
	}
	public String getPerBankAccNo()
	{
		return PerBankAccNo;
	}
	public void setPerBankAccNo(String aPerBankAccNo)
	{
		PerBankAccNo = aPerBankAccNo;
	}
	public String getPerAccName()
	{
		return PerAccName;
	}
	public void setPerAccName(String aPerAccName)
	{
		PerAccName = aPerAccName;
	}
	public String getSchoolNmae()
	{
		return SchoolNmae;
	}
	public void setSchoolNmae(String aSchoolNmae)
	{
		SchoolNmae = aSchoolNmae;
	}
	public String getClassName()
	{
		return ClassName;
	}
	public void setClassName(String aClassName)
	{
		ClassName = aClassName;
	}
	public String getAppntName()
	{
		return AppntName;
	}
	public void setAppntName(String aAppntName)
	{
		AppntName = aAppntName;
	}
	public String getAppntSex()
	{
		return AppntSex;
	}
	public void setAppntSex(String aAppntSex)
	{
		AppntSex = aAppntSex;
	}
	public String getAppntBirthday()
	{
		if( AppntBirthday != null )
			return fDate.getString(AppntBirthday);
		else
			return null;
	}
	public void setAppntBirthday(Date aAppntBirthday)
	{
		AppntBirthday = aAppntBirthday;
	}
	public void setAppntBirthday(String aAppntBirthday)
	{
		if (aAppntBirthday != null && !aAppntBirthday.equals("") )
		{
			AppntBirthday = fDate.getDate( aAppntBirthday );
		}
		else
			AppntBirthday = null;
	}

	public String getAppntIdType()
	{
		return AppntIdType;
	}
	public void setAppntIdType(String aAppntIdType)
	{
		AppntIdType = aAppntIdType;
	}
	public String getAppntIdNo()
	{
		return AppntIdNo;
	}
	public void setAppntIdNo(String aAppntIdNo)
	{
		AppntIdNo = aAppntIdNo;
	}
	public String getNativePlace()
	{
		return NativePlace;
	}
	public void setNativePlace(String aNativePlace)
	{
		NativePlace = aNativePlace;
	}
	public String getPosition2()
	{
		return Position2;
	}
	public void setPosition2(String aPosition2)
	{
		Position2 = aPosition2;
	}
	public String getOccupationCode()
	{
		return OccupationCode;
	}
	public void setOccupationCode(String aOccupationCode)
	{
		OccupationCode = aOccupationCode;
	}
	public String getNativeCity()
	{
		return NativeCity;
	}
	public void setNativeCity(String aNativeCity)
	{
		NativeCity = aNativeCity;
	}
	public String getUnCommonChar()
	{
		return UnCommonChar;
	}
	public void setUnCommonChar(String aUnCommonChar)
	{
		UnCommonChar = aUnCommonChar;
	}

	/**
	* 使用另外一个 LCInsuredListSchema 对象给 Schema 赋值
	* @param: aLCInsuredListSchema LCInsuredListSchema
	**/
	public void setSchema(LCInsuredListSchema aLCInsuredListSchema)
	{
		this.GrpContNo = aLCInsuredListSchema.getGrpContNo();
		this.InsuredID = aLCInsuredListSchema.getInsuredID();
		this.State = aLCInsuredListSchema.getState();
		this.ContNo = aLCInsuredListSchema.getContNo();
		this.BatchNo = aLCInsuredListSchema.getBatchNo();
		this.InsuredNo = aLCInsuredListSchema.getInsuredNo();
		this.Retire = aLCInsuredListSchema.getRetire();
		this.EmployeeName = aLCInsuredListSchema.getEmployeeName();
		this.InsuredName = aLCInsuredListSchema.getInsuredName();
		this.Relation = aLCInsuredListSchema.getRelation();
		this.Sex = aLCInsuredListSchema.getSex();
		this.Birthday = fDate.getDate( aLCInsuredListSchema.getBirthday());
		this.IDType = aLCInsuredListSchema.getIDType();
		this.IDNo = aLCInsuredListSchema.getIDNo();
		this.ContPlanCode = aLCInsuredListSchema.getContPlanCode();
		this.OccupationType = aLCInsuredListSchema.getOccupationType();
		this.BankCode = aLCInsuredListSchema.getBankCode();
		this.BankAccNo = aLCInsuredListSchema.getBankAccNo();
		this.AccName = aLCInsuredListSchema.getAccName();
		this.Operator = aLCInsuredListSchema.getOperator();
		this.MakeDate = fDate.getDate( aLCInsuredListSchema.getMakeDate());
		this.MakeTime = aLCInsuredListSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLCInsuredListSchema.getModifyDate());
		this.ModifyTime = aLCInsuredListSchema.getModifyTime();
		this.PublicAccType = aLCInsuredListSchema.getPublicAccType();
		this.PublicAcc = aLCInsuredListSchema.getPublicAcc();
		this.EdorNo = aLCInsuredListSchema.getEdorNo();
		this.RiskCode = aLCInsuredListSchema.getRiskCode();
		this.CalPremType = aLCInsuredListSchema.getCalPremType();
		this.OthIDType = aLCInsuredListSchema.getOthIDType();
		this.OthIDNo = aLCInsuredListSchema.getOthIDNo();
		this.EnglishName = aLCInsuredListSchema.getEnglishName();
		this.Phone = aLCInsuredListSchema.getPhone();
		this.CalRule = aLCInsuredListSchema.getCalRule();
		this.NoNamePeoples = aLCInsuredListSchema.getNoNamePeoples();
		this.EdorValiDate = fDate.getDate( aLCInsuredListSchema.getEdorValiDate());
		this.Prem = aLCInsuredListSchema.getPrem();
		this.EdorPrem = aLCInsuredListSchema.getEdorPrem();
		this.GrpNo = aLCInsuredListSchema.getGrpNo();
		this.GrpName = aLCInsuredListSchema.getGrpName();
		this.SocialCenterNo = aLCInsuredListSchema.getSocialCenterNo();
		this.SocialCenterName = aLCInsuredListSchema.getSocialCenterName();
		this.HandlerDate = fDate.getDate( aLCInsuredListSchema.getHandlerDate());
		this.HandlerPrint = aLCInsuredListSchema.getHandlerPrint();
		this.Handler = aLCInsuredListSchema.getHandler();
		this.JoinCompanyDate = fDate.getDate( aLCInsuredListSchema.getJoinCompanyDate());
		this.Position = aLCInsuredListSchema.getPosition();
		this.AscriptionRuleCode = aLCInsuredListSchema.getAscriptionRuleCode();
		this.GetYear = aLCInsuredListSchema.getGetYear();
		this.GetYearFlag = aLCInsuredListSchema.getGetYearFlag();
		this.GetDutyKind = aLCInsuredListSchema.getGetDutyKind();
		this.AppntPrem = aLCInsuredListSchema.getAppntPrem();
		this.PersonPrem = aLCInsuredListSchema.getPersonPrem();
		this.PersonOwnPrem = aLCInsuredListSchema.getPersonOwnPrem();
		this.PerBankCode = aLCInsuredListSchema.getPerBankCode();
		this.PerBankAccNo = aLCInsuredListSchema.getPerBankAccNo();
		this.PerAccName = aLCInsuredListSchema.getPerAccName();
		this.SchoolNmae = aLCInsuredListSchema.getSchoolNmae();
		this.ClassName = aLCInsuredListSchema.getClassName();
		this.AppntName = aLCInsuredListSchema.getAppntName();
		this.AppntSex = aLCInsuredListSchema.getAppntSex();
		this.AppntBirthday = fDate.getDate( aLCInsuredListSchema.getAppntBirthday());
		this.AppntIdType = aLCInsuredListSchema.getAppntIdType();
		this.AppntIdNo = aLCInsuredListSchema.getAppntIdNo();
		this.NativePlace = aLCInsuredListSchema.getNativePlace();
		this.Position2 = aLCInsuredListSchema.getPosition2();
		this.OccupationCode = aLCInsuredListSchema.getOccupationCode();
		this.NativeCity = aLCInsuredListSchema.getNativeCity();
		this.UnCommonChar = aLCInsuredListSchema.getUnCommonChar();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("GrpContNo") == null )
				this.GrpContNo = null;
			else
				this.GrpContNo = rs.getString("GrpContNo").trim();

			if( rs.getString("InsuredID") == null )
				this.InsuredID = null;
			else
				this.InsuredID = rs.getString("InsuredID").trim();

			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			if( rs.getString("ContNo") == null )
				this.ContNo = null;
			else
				this.ContNo = rs.getString("ContNo").trim();

			if( rs.getString("BatchNo") == null )
				this.BatchNo = null;
			else
				this.BatchNo = rs.getString("BatchNo").trim();

			if( rs.getString("InsuredNo") == null )
				this.InsuredNo = null;
			else
				this.InsuredNo = rs.getString("InsuredNo").trim();

			if( rs.getString("Retire") == null )
				this.Retire = null;
			else
				this.Retire = rs.getString("Retire").trim();

			if( rs.getString("EmployeeName") == null )
				this.EmployeeName = null;
			else
				this.EmployeeName = rs.getString("EmployeeName").trim();

			if( rs.getString("InsuredName") == null )
				this.InsuredName = null;
			else
				this.InsuredName = rs.getString("InsuredName").trim();

			if( rs.getString("Relation") == null )
				this.Relation = null;
			else
				this.Relation = rs.getString("Relation").trim();

			if( rs.getString("Sex") == null )
				this.Sex = null;
			else
				this.Sex = rs.getString("Sex").trim();

			this.Birthday = rs.getDate("Birthday");
			if( rs.getString("IDType") == null )
				this.IDType = null;
			else
				this.IDType = rs.getString("IDType").trim();

			if( rs.getString("IDNo") == null )
				this.IDNo = null;
			else
				this.IDNo = rs.getString("IDNo").trim();

			if( rs.getString("ContPlanCode") == null )
				this.ContPlanCode = null;
			else
				this.ContPlanCode = rs.getString("ContPlanCode").trim();

			if( rs.getString("OccupationType") == null )
				this.OccupationType = null;
			else
				this.OccupationType = rs.getString("OccupationType").trim();

			if( rs.getString("BankCode") == null )
				this.BankCode = null;
			else
				this.BankCode = rs.getString("BankCode").trim();

			if( rs.getString("BankAccNo") == null )
				this.BankAccNo = null;
			else
				this.BankAccNo = rs.getString("BankAccNo").trim();

			if( rs.getString("AccName") == null )
				this.AccName = null;
			else
				this.AccName = rs.getString("AccName").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("PublicAccType") == null )
				this.PublicAccType = null;
			else
				this.PublicAccType = rs.getString("PublicAccType").trim();

			this.PublicAcc = rs.getDouble("PublicAcc");
			if( rs.getString("EdorNo") == null )
				this.EdorNo = null;
			else
				this.EdorNo = rs.getString("EdorNo").trim();

			if( rs.getString("RiskCode") == null )
				this.RiskCode = null;
			else
				this.RiskCode = rs.getString("RiskCode").trim();

			if( rs.getString("CalPremType") == null )
				this.CalPremType = null;
			else
				this.CalPremType = rs.getString("CalPremType").trim();

			if( rs.getString("OthIDType") == null )
				this.OthIDType = null;
			else
				this.OthIDType = rs.getString("OthIDType").trim();

			if( rs.getString("OthIDNo") == null )
				this.OthIDNo = null;
			else
				this.OthIDNo = rs.getString("OthIDNo").trim();

			if( rs.getString("EnglishName") == null )
				this.EnglishName = null;
			else
				this.EnglishName = rs.getString("EnglishName").trim();

			if( rs.getString("Phone") == null )
				this.Phone = null;
			else
				this.Phone = rs.getString("Phone").trim();

			if( rs.getString("CalRule") == null )
				this.CalRule = null;
			else
				this.CalRule = rs.getString("CalRule").trim();

			this.NoNamePeoples = rs.getInt("NoNamePeoples");
			this.EdorValiDate = rs.getDate("EdorValiDate");
			if( rs.getString("Prem") == null )
				this.Prem = null;
			else
				this.Prem = rs.getString("Prem").trim();

			if( rs.getString("EdorPrem") == null )
				this.EdorPrem = null;
			else
				this.EdorPrem = rs.getString("EdorPrem").trim();

			if( rs.getString("GrpNo") == null )
				this.GrpNo = null;
			else
				this.GrpNo = rs.getString("GrpNo").trim();

			if( rs.getString("GrpName") == null )
				this.GrpName = null;
			else
				this.GrpName = rs.getString("GrpName").trim();

			if( rs.getString("SocialCenterNo") == null )
				this.SocialCenterNo = null;
			else
				this.SocialCenterNo = rs.getString("SocialCenterNo").trim();

			if( rs.getString("SocialCenterName") == null )
				this.SocialCenterName = null;
			else
				this.SocialCenterName = rs.getString("SocialCenterName").trim();

			this.HandlerDate = rs.getDate("HandlerDate");
			if( rs.getString("HandlerPrint") == null )
				this.HandlerPrint = null;
			else
				this.HandlerPrint = rs.getString("HandlerPrint").trim();

			if( rs.getString("Handler") == null )
				this.Handler = null;
			else
				this.Handler = rs.getString("Handler").trim();

			this.JoinCompanyDate = rs.getDate("JoinCompanyDate");
			if( rs.getString("Position") == null )
				this.Position = null;
			else
				this.Position = rs.getString("Position").trim();

			if( rs.getString("AscriptionRuleCode") == null )
				this.AscriptionRuleCode = null;
			else
				this.AscriptionRuleCode = rs.getString("AscriptionRuleCode").trim();

			this.GetYear = rs.getInt("GetYear");
			if( rs.getString("GetYearFlag") == null )
				this.GetYearFlag = null;
			else
				this.GetYearFlag = rs.getString("GetYearFlag").trim();

			if( rs.getString("GetDutyKind") == null )
				this.GetDutyKind = null;
			else
				this.GetDutyKind = rs.getString("GetDutyKind").trim();

			this.AppntPrem = rs.getDouble("AppntPrem");
			this.PersonPrem = rs.getDouble("PersonPrem");
			this.PersonOwnPrem = rs.getDouble("PersonOwnPrem");
			if( rs.getString("PerBankCode") == null )
				this.PerBankCode = null;
			else
				this.PerBankCode = rs.getString("PerBankCode").trim();

			if( rs.getString("PerBankAccNo") == null )
				this.PerBankAccNo = null;
			else
				this.PerBankAccNo = rs.getString("PerBankAccNo").trim();

			if( rs.getString("PerAccName") == null )
				this.PerAccName = null;
			else
				this.PerAccName = rs.getString("PerAccName").trim();

			if( rs.getString("SchoolNmae") == null )
				this.SchoolNmae = null;
			else
				this.SchoolNmae = rs.getString("SchoolNmae").trim();

			if( rs.getString("ClassName") == null )
				this.ClassName = null;
			else
				this.ClassName = rs.getString("ClassName").trim();

			if( rs.getString("AppntName") == null )
				this.AppntName = null;
			else
				this.AppntName = rs.getString("AppntName").trim();

			if( rs.getString("AppntSex") == null )
				this.AppntSex = null;
			else
				this.AppntSex = rs.getString("AppntSex").trim();

			this.AppntBirthday = rs.getDate("AppntBirthday");
			if( rs.getString("AppntIdType") == null )
				this.AppntIdType = null;
			else
				this.AppntIdType = rs.getString("AppntIdType").trim();

			if( rs.getString("AppntIdNo") == null )
				this.AppntIdNo = null;
			else
				this.AppntIdNo = rs.getString("AppntIdNo").trim();

			if( rs.getString("NativePlace") == null )
				this.NativePlace = null;
			else
				this.NativePlace = rs.getString("NativePlace").trim();

			if( rs.getString("Position2") == null )
				this.Position2 = null;
			else
				this.Position2 = rs.getString("Position2").trim();

			if( rs.getString("OccupationCode") == null )
				this.OccupationCode = null;
			else
				this.OccupationCode = rs.getString("OccupationCode").trim();

			if( rs.getString("NativeCity") == null )
				this.NativeCity = null;
			else
				this.NativeCity = rs.getString("NativeCity").trim();

			if( rs.getString("UnCommonChar") == null )
				this.UnCommonChar = null;
			else
				this.UnCommonChar = rs.getString("UnCommonChar").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LCInsuredList表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCInsuredListSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LCInsuredListSchema getSchema()
	{
		LCInsuredListSchema aLCInsuredListSchema = new LCInsuredListSchema();
		aLCInsuredListSchema.setSchema(this);
		return aLCInsuredListSchema;
	}

	public LCInsuredListDB getDB()
	{
		LCInsuredListDB aDBOper = new LCInsuredListDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCInsuredList描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuredID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BatchNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuredNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Retire)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EmployeeName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuredName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Relation)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Sex)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Birthday ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IDType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IDNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContPlanCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OccupationType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PublicAccType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PublicAcc));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EdorNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CalPremType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OthIDType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OthIDNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EnglishName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Phone)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CalRule)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(NoNamePeoples));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( EdorValiDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Prem)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EdorPrem)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SocialCenterNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SocialCenterName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( HandlerDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HandlerPrint)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Handler)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( JoinCompanyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Position)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AscriptionRuleCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(GetYear));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GetYearFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GetDutyKind)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(AppntPrem));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PersonPrem));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PersonOwnPrem));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PerBankCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PerBankAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PerAccName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SchoolNmae)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ClassName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppntName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppntSex)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( AppntBirthday ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppntIdType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppntIdNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(NativePlace)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Position2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OccupationCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(NativeCity)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UnCommonChar));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCInsuredList>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			InsuredID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			BatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			InsuredNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			Retire = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			EmployeeName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			InsuredName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			Relation = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			Sex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			Birthday = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
			IDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			IDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			ContPlanCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			OccupationType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			BankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			AccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			PublicAccType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			PublicAcc = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,26,SysConst.PACKAGESPILTER))).doubleValue();
			EdorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			CalPremType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			OthIDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			OthIDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			EnglishName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
			Phone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			CalRule = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			NoNamePeoples= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,35,SysConst.PACKAGESPILTER))).intValue();
			EdorValiDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36,SysConst.PACKAGESPILTER));
			Prem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
			EdorPrem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
			GrpNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
			GrpName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
			SocialCenterNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41, SysConst.PACKAGESPILTER );
			SocialCenterName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
			HandlerDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43,SysConst.PACKAGESPILTER));
			HandlerPrint = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44, SysConst.PACKAGESPILTER );
			Handler = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 45, SysConst.PACKAGESPILTER );
			JoinCompanyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 46,SysConst.PACKAGESPILTER));
			Position = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 47, SysConst.PACKAGESPILTER );
			AscriptionRuleCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 48, SysConst.PACKAGESPILTER );
			GetYear= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,49,SysConst.PACKAGESPILTER))).intValue();
			GetYearFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 50, SysConst.PACKAGESPILTER );
			GetDutyKind = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 51, SysConst.PACKAGESPILTER );
			AppntPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,52,SysConst.PACKAGESPILTER))).doubleValue();
			PersonPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,53,SysConst.PACKAGESPILTER))).doubleValue();
			PersonOwnPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,54,SysConst.PACKAGESPILTER))).doubleValue();
			PerBankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 55, SysConst.PACKAGESPILTER );
			PerBankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 56, SysConst.PACKAGESPILTER );
			PerAccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 57, SysConst.PACKAGESPILTER );
			SchoolNmae = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 58, SysConst.PACKAGESPILTER );
			ClassName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 59, SysConst.PACKAGESPILTER );
			AppntName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 60, SysConst.PACKAGESPILTER );
			AppntSex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 61, SysConst.PACKAGESPILTER );
			AppntBirthday = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 62,SysConst.PACKAGESPILTER));
			AppntIdType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 63, SysConst.PACKAGESPILTER );
			AppntIdNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 64, SysConst.PACKAGESPILTER );
			NativePlace = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 65, SysConst.PACKAGESPILTER );
			Position2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 66, SysConst.PACKAGESPILTER );
			OccupationCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 67, SysConst.PACKAGESPILTER );
			NativeCity = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 68, SysConst.PACKAGESPILTER );
			UnCommonChar = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 69, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCInsuredListSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("GrpContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
		}
		if (FCode.equals("InsuredID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredID));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("ContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
		}
		if (FCode.equals("BatchNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BatchNo));
		}
		if (FCode.equals("InsuredNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredNo));
		}
		if (FCode.equals("Retire"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Retire));
		}
		if (FCode.equals("EmployeeName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EmployeeName));
		}
		if (FCode.equals("InsuredName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredName));
		}
		if (FCode.equals("Relation"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Relation));
		}
		if (FCode.equals("Sex"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Sex));
		}
		if (FCode.equals("Birthday"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getBirthday()));
		}
		if (FCode.equals("IDType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IDType));
		}
		if (FCode.equals("IDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
		}
		if (FCode.equals("ContPlanCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContPlanCode));
		}
		if (FCode.equals("OccupationType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationType));
		}
		if (FCode.equals("BankCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
		}
		if (FCode.equals("BankAccNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccNo));
		}
		if (FCode.equals("AccName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccName));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("PublicAccType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PublicAccType));
		}
		if (FCode.equals("PublicAcc"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PublicAcc));
		}
		if (FCode.equals("EdorNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EdorNo));
		}
		if (FCode.equals("RiskCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
		}
		if (FCode.equals("CalPremType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CalPremType));
		}
		if (FCode.equals("OthIDType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OthIDType));
		}
		if (FCode.equals("OthIDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OthIDNo));
		}
		if (FCode.equals("EnglishName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EnglishName));
		}
		if (FCode.equals("Phone"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Phone));
		}
		if (FCode.equals("CalRule"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CalRule));
		}
		if (FCode.equals("NoNamePeoples"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(NoNamePeoples));
		}
		if (FCode.equals("EdorValiDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEdorValiDate()));
		}
		if (FCode.equals("Prem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Prem));
		}
		if (FCode.equals("EdorPrem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EdorPrem));
		}
		if (FCode.equals("GrpNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpNo));
		}
		if (FCode.equals("GrpName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpName));
		}
		if (FCode.equals("SocialCenterNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SocialCenterNo));
		}
		if (FCode.equals("SocialCenterName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SocialCenterName));
		}
		if (FCode.equals("HandlerDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getHandlerDate()));
		}
		if (FCode.equals("HandlerPrint"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HandlerPrint));
		}
		if (FCode.equals("Handler"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Handler));
		}
		if (FCode.equals("JoinCompanyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getJoinCompanyDate()));
		}
		if (FCode.equals("Position"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Position));
		}
		if (FCode.equals("AscriptionRuleCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AscriptionRuleCode));
		}
		if (FCode.equals("GetYear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GetYear));
		}
		if (FCode.equals("GetYearFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GetYearFlag));
		}
		if (FCode.equals("GetDutyKind"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GetDutyKind));
		}
		if (FCode.equals("AppntPrem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppntPrem));
		}
		if (FCode.equals("PersonPrem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PersonPrem));
		}
		if (FCode.equals("PersonOwnPrem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PersonOwnPrem));
		}
		if (FCode.equals("PerBankCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PerBankCode));
		}
		if (FCode.equals("PerBankAccNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PerBankAccNo));
		}
		if (FCode.equals("PerAccName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PerAccName));
		}
		if (FCode.equals("SchoolNmae"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SchoolNmae));
		}
		if (FCode.equals("ClassName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClassName));
		}
		if (FCode.equals("AppntName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppntName));
		}
		if (FCode.equals("AppntSex"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppntSex));
		}
		if (FCode.equals("AppntBirthday"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getAppntBirthday()));
		}
		if (FCode.equals("AppntIdType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppntIdType));
		}
		if (FCode.equals("AppntIdNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppntIdNo));
		}
		if (FCode.equals("NativePlace"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(NativePlace));
		}
		if (FCode.equals("Position2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Position2));
		}
		if (FCode.equals("OccupationCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationCode));
		}
		if (FCode.equals("NativeCity"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(NativeCity));
		}
		if (FCode.equals("UnCommonChar"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UnCommonChar));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(GrpContNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(InsuredID);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(ContNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(BatchNo);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(InsuredNo);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(Retire);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(EmployeeName);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(InsuredName);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(Relation);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(Sex);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getBirthday()));
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(IDType);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(IDNo);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(ContPlanCode);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(OccupationType);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(BankCode);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(BankAccNo);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(AccName);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(PublicAccType);
				break;
			case 25:
				strFieldValue = String.valueOf(PublicAcc);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(EdorNo);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(RiskCode);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(CalPremType);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(OthIDType);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(OthIDNo);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(EnglishName);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(Phone);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(CalRule);
				break;
			case 34:
				strFieldValue = String.valueOf(NoNamePeoples);
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEdorValiDate()));
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(Prem);
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(EdorPrem);
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(GrpNo);
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(GrpName);
				break;
			case 40:
				strFieldValue = StrTool.GBKToUnicode(SocialCenterNo);
				break;
			case 41:
				strFieldValue = StrTool.GBKToUnicode(SocialCenterName);
				break;
			case 42:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getHandlerDate()));
				break;
			case 43:
				strFieldValue = StrTool.GBKToUnicode(HandlerPrint);
				break;
			case 44:
				strFieldValue = StrTool.GBKToUnicode(Handler);
				break;
			case 45:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getJoinCompanyDate()));
				break;
			case 46:
				strFieldValue = StrTool.GBKToUnicode(Position);
				break;
			case 47:
				strFieldValue = StrTool.GBKToUnicode(AscriptionRuleCode);
				break;
			case 48:
				strFieldValue = String.valueOf(GetYear);
				break;
			case 49:
				strFieldValue = StrTool.GBKToUnicode(GetYearFlag);
				break;
			case 50:
				strFieldValue = StrTool.GBKToUnicode(GetDutyKind);
				break;
			case 51:
				strFieldValue = String.valueOf(AppntPrem);
				break;
			case 52:
				strFieldValue = String.valueOf(PersonPrem);
				break;
			case 53:
				strFieldValue = String.valueOf(PersonOwnPrem);
				break;
			case 54:
				strFieldValue = StrTool.GBKToUnicode(PerBankCode);
				break;
			case 55:
				strFieldValue = StrTool.GBKToUnicode(PerBankAccNo);
				break;
			case 56:
				strFieldValue = StrTool.GBKToUnicode(PerAccName);
				break;
			case 57:
				strFieldValue = StrTool.GBKToUnicode(SchoolNmae);
				break;
			case 58:
				strFieldValue = StrTool.GBKToUnicode(ClassName);
				break;
			case 59:
				strFieldValue = StrTool.GBKToUnicode(AppntName);
				break;
			case 60:
				strFieldValue = StrTool.GBKToUnicode(AppntSex);
				break;
			case 61:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getAppntBirthday()));
				break;
			case 62:
				strFieldValue = StrTool.GBKToUnicode(AppntIdType);
				break;
			case 63:
				strFieldValue = StrTool.GBKToUnicode(AppntIdNo);
				break;
			case 64:
				strFieldValue = StrTool.GBKToUnicode(NativePlace);
				break;
			case 65:
				strFieldValue = StrTool.GBKToUnicode(Position2);
				break;
			case 66:
				strFieldValue = StrTool.GBKToUnicode(OccupationCode);
				break;
			case 67:
				strFieldValue = StrTool.GBKToUnicode(NativeCity);
				break;
			case 68:
				strFieldValue = StrTool.GBKToUnicode(UnCommonChar);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("GrpContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpContNo = FValue.trim();
			}
			else
				GrpContNo = null;
		}
		if (FCode.equalsIgnoreCase("InsuredID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuredID = FValue.trim();
			}
			else
				InsuredID = null;
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("ContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContNo = FValue.trim();
			}
			else
				ContNo = null;
		}
		if (FCode.equalsIgnoreCase("BatchNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BatchNo = FValue.trim();
			}
			else
				BatchNo = null;
		}
		if (FCode.equalsIgnoreCase("InsuredNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuredNo = FValue.trim();
			}
			else
				InsuredNo = null;
		}
		if (FCode.equalsIgnoreCase("Retire"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Retire = FValue.trim();
			}
			else
				Retire = null;
		}
		if (FCode.equalsIgnoreCase("EmployeeName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EmployeeName = FValue.trim();
			}
			else
				EmployeeName = null;
		}
		if (FCode.equalsIgnoreCase("InsuredName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuredName = FValue.trim();
			}
			else
				InsuredName = null;
		}
		if (FCode.equalsIgnoreCase("Relation"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Relation = FValue.trim();
			}
			else
				Relation = null;
		}
		if (FCode.equalsIgnoreCase("Sex"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Sex = FValue.trim();
			}
			else
				Sex = null;
		}
		if (FCode.equalsIgnoreCase("Birthday"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Birthday = fDate.getDate( FValue );
			}
			else
				Birthday = null;
		}
		if (FCode.equalsIgnoreCase("IDType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IDType = FValue.trim();
			}
			else
				IDType = null;
		}
		if (FCode.equalsIgnoreCase("IDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IDNo = FValue.trim();
			}
			else
				IDNo = null;
		}
		if (FCode.equalsIgnoreCase("ContPlanCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContPlanCode = FValue.trim();
			}
			else
				ContPlanCode = null;
		}
		if (FCode.equalsIgnoreCase("OccupationType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OccupationType = FValue.trim();
			}
			else
				OccupationType = null;
		}
		if (FCode.equalsIgnoreCase("BankCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankCode = FValue.trim();
			}
			else
				BankCode = null;
		}
		if (FCode.equalsIgnoreCase("BankAccNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankAccNo = FValue.trim();
			}
			else
				BankAccNo = null;
		}
		if (FCode.equalsIgnoreCase("AccName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccName = FValue.trim();
			}
			else
				AccName = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("PublicAccType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PublicAccType = FValue.trim();
			}
			else
				PublicAccType = null;
		}
		if (FCode.equalsIgnoreCase("PublicAcc"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				PublicAcc = d;
			}
		}
		if (FCode.equalsIgnoreCase("EdorNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EdorNo = FValue.trim();
			}
			else
				EdorNo = null;
		}
		if (FCode.equalsIgnoreCase("RiskCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskCode = FValue.trim();
			}
			else
				RiskCode = null;
		}
		if (FCode.equalsIgnoreCase("CalPremType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CalPremType = FValue.trim();
			}
			else
				CalPremType = null;
		}
		if (FCode.equalsIgnoreCase("OthIDType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OthIDType = FValue.trim();
			}
			else
				OthIDType = null;
		}
		if (FCode.equalsIgnoreCase("OthIDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OthIDNo = FValue.trim();
			}
			else
				OthIDNo = null;
		}
		if (FCode.equalsIgnoreCase("EnglishName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EnglishName = FValue.trim();
			}
			else
				EnglishName = null;
		}
		if (FCode.equalsIgnoreCase("Phone"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Phone = FValue.trim();
			}
			else
				Phone = null;
		}
		if (FCode.equalsIgnoreCase("CalRule"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CalRule = FValue.trim();
			}
			else
				CalRule = null;
		}
		if (FCode.equalsIgnoreCase("NoNamePeoples"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				NoNamePeoples = i;
			}
		}
		if (FCode.equalsIgnoreCase("EdorValiDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				EdorValiDate = fDate.getDate( FValue );
			}
			else
				EdorValiDate = null;
		}
		if (FCode.equalsIgnoreCase("Prem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Prem = FValue.trim();
			}
			else
				Prem = null;
		}
		if (FCode.equalsIgnoreCase("EdorPrem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EdorPrem = FValue.trim();
			}
			else
				EdorPrem = null;
		}
		if (FCode.equalsIgnoreCase("GrpNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpNo = FValue.trim();
			}
			else
				GrpNo = null;
		}
		if (FCode.equalsIgnoreCase("GrpName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpName = FValue.trim();
			}
			else
				GrpName = null;
		}
		if (FCode.equalsIgnoreCase("SocialCenterNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SocialCenterNo = FValue.trim();
			}
			else
				SocialCenterNo = null;
		}
		if (FCode.equalsIgnoreCase("SocialCenterName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SocialCenterName = FValue.trim();
			}
			else
				SocialCenterName = null;
		}
		if (FCode.equalsIgnoreCase("HandlerDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				HandlerDate = fDate.getDate( FValue );
			}
			else
				HandlerDate = null;
		}
		if (FCode.equalsIgnoreCase("HandlerPrint"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HandlerPrint = FValue.trim();
			}
			else
				HandlerPrint = null;
		}
		if (FCode.equalsIgnoreCase("Handler"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Handler = FValue.trim();
			}
			else
				Handler = null;
		}
		if (FCode.equalsIgnoreCase("JoinCompanyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				JoinCompanyDate = fDate.getDate( FValue );
			}
			else
				JoinCompanyDate = null;
		}
		if (FCode.equalsIgnoreCase("Position"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Position = FValue.trim();
			}
			else
				Position = null;
		}
		if (FCode.equalsIgnoreCase("AscriptionRuleCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AscriptionRuleCode = FValue.trim();
			}
			else
				AscriptionRuleCode = null;
		}
		if (FCode.equalsIgnoreCase("GetYear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				GetYear = i;
			}
		}
		if (FCode.equalsIgnoreCase("GetYearFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GetYearFlag = FValue.trim();
			}
			else
				GetYearFlag = null;
		}
		if (FCode.equalsIgnoreCase("GetDutyKind"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GetDutyKind = FValue.trim();
			}
			else
				GetDutyKind = null;
		}
		if (FCode.equalsIgnoreCase("AppntPrem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				AppntPrem = d;
			}
		}
		if (FCode.equalsIgnoreCase("PersonPrem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				PersonPrem = d;
			}
		}
		if (FCode.equalsIgnoreCase("PersonOwnPrem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				PersonOwnPrem = d;
			}
		}
		if (FCode.equalsIgnoreCase("PerBankCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PerBankCode = FValue.trim();
			}
			else
				PerBankCode = null;
		}
		if (FCode.equalsIgnoreCase("PerBankAccNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PerBankAccNo = FValue.trim();
			}
			else
				PerBankAccNo = null;
		}
		if (FCode.equalsIgnoreCase("PerAccName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PerAccName = FValue.trim();
			}
			else
				PerAccName = null;
		}
		if (FCode.equalsIgnoreCase("SchoolNmae"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SchoolNmae = FValue.trim();
			}
			else
				SchoolNmae = null;
		}
		if (FCode.equalsIgnoreCase("ClassName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ClassName = FValue.trim();
			}
			else
				ClassName = null;
		}
		if (FCode.equalsIgnoreCase("AppntName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppntName = FValue.trim();
			}
			else
				AppntName = null;
		}
		if (FCode.equalsIgnoreCase("AppntSex"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppntSex = FValue.trim();
			}
			else
				AppntSex = null;
		}
		if (FCode.equalsIgnoreCase("AppntBirthday"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				AppntBirthday = fDate.getDate( FValue );
			}
			else
				AppntBirthday = null;
		}
		if (FCode.equalsIgnoreCase("AppntIdType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppntIdType = FValue.trim();
			}
			else
				AppntIdType = null;
		}
		if (FCode.equalsIgnoreCase("AppntIdNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppntIdNo = FValue.trim();
			}
			else
				AppntIdNo = null;
		}
		if (FCode.equalsIgnoreCase("NativePlace"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				NativePlace = FValue.trim();
			}
			else
				NativePlace = null;
		}
		if (FCode.equalsIgnoreCase("Position2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Position2 = FValue.trim();
			}
			else
				Position2 = null;
		}
		if (FCode.equalsIgnoreCase("OccupationCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OccupationCode = FValue.trim();
			}
			else
				OccupationCode = null;
		}
		if (FCode.equalsIgnoreCase("NativeCity"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				NativeCity = FValue.trim();
			}
			else
				NativeCity = null;
		}
		if (FCode.equalsIgnoreCase("UnCommonChar"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UnCommonChar = FValue.trim();
			}
			else
				UnCommonChar = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LCInsuredListSchema other = (LCInsuredListSchema)otherObject;
		return
			(GrpContNo == null ? other.getGrpContNo() == null : GrpContNo.equals(other.getGrpContNo()))
			&& (InsuredID == null ? other.getInsuredID() == null : InsuredID.equals(other.getInsuredID()))
			&& (State == null ? other.getState() == null : State.equals(other.getState()))
			&& (ContNo == null ? other.getContNo() == null : ContNo.equals(other.getContNo()))
			&& (BatchNo == null ? other.getBatchNo() == null : BatchNo.equals(other.getBatchNo()))
			&& (InsuredNo == null ? other.getInsuredNo() == null : InsuredNo.equals(other.getInsuredNo()))
			&& (Retire == null ? other.getRetire() == null : Retire.equals(other.getRetire()))
			&& (EmployeeName == null ? other.getEmployeeName() == null : EmployeeName.equals(other.getEmployeeName()))
			&& (InsuredName == null ? other.getInsuredName() == null : InsuredName.equals(other.getInsuredName()))
			&& (Relation == null ? other.getRelation() == null : Relation.equals(other.getRelation()))
			&& (Sex == null ? other.getSex() == null : Sex.equals(other.getSex()))
			&& (Birthday == null ? other.getBirthday() == null : fDate.getString(Birthday).equals(other.getBirthday()))
			&& (IDType == null ? other.getIDType() == null : IDType.equals(other.getIDType()))
			&& (IDNo == null ? other.getIDNo() == null : IDNo.equals(other.getIDNo()))
			&& (ContPlanCode == null ? other.getContPlanCode() == null : ContPlanCode.equals(other.getContPlanCode()))
			&& (OccupationType == null ? other.getOccupationType() == null : OccupationType.equals(other.getOccupationType()))
			&& (BankCode == null ? other.getBankCode() == null : BankCode.equals(other.getBankCode()))
			&& (BankAccNo == null ? other.getBankAccNo() == null : BankAccNo.equals(other.getBankAccNo()))
			&& (AccName == null ? other.getAccName() == null : AccName.equals(other.getAccName()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (PublicAccType == null ? other.getPublicAccType() == null : PublicAccType.equals(other.getPublicAccType()))
			&& PublicAcc == other.getPublicAcc()
			&& (EdorNo == null ? other.getEdorNo() == null : EdorNo.equals(other.getEdorNo()))
			&& (RiskCode == null ? other.getRiskCode() == null : RiskCode.equals(other.getRiskCode()))
			&& (CalPremType == null ? other.getCalPremType() == null : CalPremType.equals(other.getCalPremType()))
			&& (OthIDType == null ? other.getOthIDType() == null : OthIDType.equals(other.getOthIDType()))
			&& (OthIDNo == null ? other.getOthIDNo() == null : OthIDNo.equals(other.getOthIDNo()))
			&& (EnglishName == null ? other.getEnglishName() == null : EnglishName.equals(other.getEnglishName()))
			&& (Phone == null ? other.getPhone() == null : Phone.equals(other.getPhone()))
			&& (CalRule == null ? other.getCalRule() == null : CalRule.equals(other.getCalRule()))
			&& NoNamePeoples == other.getNoNamePeoples()
			&& (EdorValiDate == null ? other.getEdorValiDate() == null : fDate.getString(EdorValiDate).equals(other.getEdorValiDate()))
			&& (Prem == null ? other.getPrem() == null : Prem.equals(other.getPrem()))
			&& (EdorPrem == null ? other.getEdorPrem() == null : EdorPrem.equals(other.getEdorPrem()))
			&& (GrpNo == null ? other.getGrpNo() == null : GrpNo.equals(other.getGrpNo()))
			&& (GrpName == null ? other.getGrpName() == null : GrpName.equals(other.getGrpName()))
			&& (SocialCenterNo == null ? other.getSocialCenterNo() == null : SocialCenterNo.equals(other.getSocialCenterNo()))
			&& (SocialCenterName == null ? other.getSocialCenterName() == null : SocialCenterName.equals(other.getSocialCenterName()))
			&& (HandlerDate == null ? other.getHandlerDate() == null : fDate.getString(HandlerDate).equals(other.getHandlerDate()))
			&& (HandlerPrint == null ? other.getHandlerPrint() == null : HandlerPrint.equals(other.getHandlerPrint()))
			&& (Handler == null ? other.getHandler() == null : Handler.equals(other.getHandler()))
			&& (JoinCompanyDate == null ? other.getJoinCompanyDate() == null : fDate.getString(JoinCompanyDate).equals(other.getJoinCompanyDate()))
			&& (Position == null ? other.getPosition() == null : Position.equals(other.getPosition()))
			&& (AscriptionRuleCode == null ? other.getAscriptionRuleCode() == null : AscriptionRuleCode.equals(other.getAscriptionRuleCode()))
			&& GetYear == other.getGetYear()
			&& (GetYearFlag == null ? other.getGetYearFlag() == null : GetYearFlag.equals(other.getGetYearFlag()))
			&& (GetDutyKind == null ? other.getGetDutyKind() == null : GetDutyKind.equals(other.getGetDutyKind()))
			&& AppntPrem == other.getAppntPrem()
			&& PersonPrem == other.getPersonPrem()
			&& PersonOwnPrem == other.getPersonOwnPrem()
			&& (PerBankCode == null ? other.getPerBankCode() == null : PerBankCode.equals(other.getPerBankCode()))
			&& (PerBankAccNo == null ? other.getPerBankAccNo() == null : PerBankAccNo.equals(other.getPerBankAccNo()))
			&& (PerAccName == null ? other.getPerAccName() == null : PerAccName.equals(other.getPerAccName()))
			&& (SchoolNmae == null ? other.getSchoolNmae() == null : SchoolNmae.equals(other.getSchoolNmae()))
			&& (ClassName == null ? other.getClassName() == null : ClassName.equals(other.getClassName()))
			&& (AppntName == null ? other.getAppntName() == null : AppntName.equals(other.getAppntName()))
			&& (AppntSex == null ? other.getAppntSex() == null : AppntSex.equals(other.getAppntSex()))
			&& (AppntBirthday == null ? other.getAppntBirthday() == null : fDate.getString(AppntBirthday).equals(other.getAppntBirthday()))
			&& (AppntIdType == null ? other.getAppntIdType() == null : AppntIdType.equals(other.getAppntIdType()))
			&& (AppntIdNo == null ? other.getAppntIdNo() == null : AppntIdNo.equals(other.getAppntIdNo()))
			&& (NativePlace == null ? other.getNativePlace() == null : NativePlace.equals(other.getNativePlace()))
			&& (Position2 == null ? other.getPosition2() == null : Position2.equals(other.getPosition2()))
			&& (OccupationCode == null ? other.getOccupationCode() == null : OccupationCode.equals(other.getOccupationCode()))
			&& (NativeCity == null ? other.getNativeCity() == null : NativeCity.equals(other.getNativeCity()))
			&& (UnCommonChar == null ? other.getUnCommonChar() == null : UnCommonChar.equals(other.getUnCommonChar()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("GrpContNo") ) {
			return 0;
		}
		if( strFieldName.equals("InsuredID") ) {
			return 1;
		}
		if( strFieldName.equals("State") ) {
			return 2;
		}
		if( strFieldName.equals("ContNo") ) {
			return 3;
		}
		if( strFieldName.equals("BatchNo") ) {
			return 4;
		}
		if( strFieldName.equals("InsuredNo") ) {
			return 5;
		}
		if( strFieldName.equals("Retire") ) {
			return 6;
		}
		if( strFieldName.equals("EmployeeName") ) {
			return 7;
		}
		if( strFieldName.equals("InsuredName") ) {
			return 8;
		}
		if( strFieldName.equals("Relation") ) {
			return 9;
		}
		if( strFieldName.equals("Sex") ) {
			return 10;
		}
		if( strFieldName.equals("Birthday") ) {
			return 11;
		}
		if( strFieldName.equals("IDType") ) {
			return 12;
		}
		if( strFieldName.equals("IDNo") ) {
			return 13;
		}
		if( strFieldName.equals("ContPlanCode") ) {
			return 14;
		}
		if( strFieldName.equals("OccupationType") ) {
			return 15;
		}
		if( strFieldName.equals("BankCode") ) {
			return 16;
		}
		if( strFieldName.equals("BankAccNo") ) {
			return 17;
		}
		if( strFieldName.equals("AccName") ) {
			return 18;
		}
		if( strFieldName.equals("Operator") ) {
			return 19;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 20;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 21;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 22;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 23;
		}
		if( strFieldName.equals("PublicAccType") ) {
			return 24;
		}
		if( strFieldName.equals("PublicAcc") ) {
			return 25;
		}
		if( strFieldName.equals("EdorNo") ) {
			return 26;
		}
		if( strFieldName.equals("RiskCode") ) {
			return 27;
		}
		if( strFieldName.equals("CalPremType") ) {
			return 28;
		}
		if( strFieldName.equals("OthIDType") ) {
			return 29;
		}
		if( strFieldName.equals("OthIDNo") ) {
			return 30;
		}
		if( strFieldName.equals("EnglishName") ) {
			return 31;
		}
		if( strFieldName.equals("Phone") ) {
			return 32;
		}
		if( strFieldName.equals("CalRule") ) {
			return 33;
		}
		if( strFieldName.equals("NoNamePeoples") ) {
			return 34;
		}
		if( strFieldName.equals("EdorValiDate") ) {
			return 35;
		}
		if( strFieldName.equals("Prem") ) {
			return 36;
		}
		if( strFieldName.equals("EdorPrem") ) {
			return 37;
		}
		if( strFieldName.equals("GrpNo") ) {
			return 38;
		}
		if( strFieldName.equals("GrpName") ) {
			return 39;
		}
		if( strFieldName.equals("SocialCenterNo") ) {
			return 40;
		}
		if( strFieldName.equals("SocialCenterName") ) {
			return 41;
		}
		if( strFieldName.equals("HandlerDate") ) {
			return 42;
		}
		if( strFieldName.equals("HandlerPrint") ) {
			return 43;
		}
		if( strFieldName.equals("Handler") ) {
			return 44;
		}
		if( strFieldName.equals("JoinCompanyDate") ) {
			return 45;
		}
		if( strFieldName.equals("Position") ) {
			return 46;
		}
		if( strFieldName.equals("AscriptionRuleCode") ) {
			return 47;
		}
		if( strFieldName.equals("GetYear") ) {
			return 48;
		}
		if( strFieldName.equals("GetYearFlag") ) {
			return 49;
		}
		if( strFieldName.equals("GetDutyKind") ) {
			return 50;
		}
		if( strFieldName.equals("AppntPrem") ) {
			return 51;
		}
		if( strFieldName.equals("PersonPrem") ) {
			return 52;
		}
		if( strFieldName.equals("PersonOwnPrem") ) {
			return 53;
		}
		if( strFieldName.equals("PerBankCode") ) {
			return 54;
		}
		if( strFieldName.equals("PerBankAccNo") ) {
			return 55;
		}
		if( strFieldName.equals("PerAccName") ) {
			return 56;
		}
		if( strFieldName.equals("SchoolNmae") ) {
			return 57;
		}
		if( strFieldName.equals("ClassName") ) {
			return 58;
		}
		if( strFieldName.equals("AppntName") ) {
			return 59;
		}
		if( strFieldName.equals("AppntSex") ) {
			return 60;
		}
		if( strFieldName.equals("AppntBirthday") ) {
			return 61;
		}
		if( strFieldName.equals("AppntIdType") ) {
			return 62;
		}
		if( strFieldName.equals("AppntIdNo") ) {
			return 63;
		}
		if( strFieldName.equals("NativePlace") ) {
			return 64;
		}
		if( strFieldName.equals("Position2") ) {
			return 65;
		}
		if( strFieldName.equals("OccupationCode") ) {
			return 66;
		}
		if( strFieldName.equals("NativeCity") ) {
			return 67;
		}
		if( strFieldName.equals("UnCommonChar") ) {
			return 68;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "GrpContNo";
				break;
			case 1:
				strFieldName = "InsuredID";
				break;
			case 2:
				strFieldName = "State";
				break;
			case 3:
				strFieldName = "ContNo";
				break;
			case 4:
				strFieldName = "BatchNo";
				break;
			case 5:
				strFieldName = "InsuredNo";
				break;
			case 6:
				strFieldName = "Retire";
				break;
			case 7:
				strFieldName = "EmployeeName";
				break;
			case 8:
				strFieldName = "InsuredName";
				break;
			case 9:
				strFieldName = "Relation";
				break;
			case 10:
				strFieldName = "Sex";
				break;
			case 11:
				strFieldName = "Birthday";
				break;
			case 12:
				strFieldName = "IDType";
				break;
			case 13:
				strFieldName = "IDNo";
				break;
			case 14:
				strFieldName = "ContPlanCode";
				break;
			case 15:
				strFieldName = "OccupationType";
				break;
			case 16:
				strFieldName = "BankCode";
				break;
			case 17:
				strFieldName = "BankAccNo";
				break;
			case 18:
				strFieldName = "AccName";
				break;
			case 19:
				strFieldName = "Operator";
				break;
			case 20:
				strFieldName = "MakeDate";
				break;
			case 21:
				strFieldName = "MakeTime";
				break;
			case 22:
				strFieldName = "ModifyDate";
				break;
			case 23:
				strFieldName = "ModifyTime";
				break;
			case 24:
				strFieldName = "PublicAccType";
				break;
			case 25:
				strFieldName = "PublicAcc";
				break;
			case 26:
				strFieldName = "EdorNo";
				break;
			case 27:
				strFieldName = "RiskCode";
				break;
			case 28:
				strFieldName = "CalPremType";
				break;
			case 29:
				strFieldName = "OthIDType";
				break;
			case 30:
				strFieldName = "OthIDNo";
				break;
			case 31:
				strFieldName = "EnglishName";
				break;
			case 32:
				strFieldName = "Phone";
				break;
			case 33:
				strFieldName = "CalRule";
				break;
			case 34:
				strFieldName = "NoNamePeoples";
				break;
			case 35:
				strFieldName = "EdorValiDate";
				break;
			case 36:
				strFieldName = "Prem";
				break;
			case 37:
				strFieldName = "EdorPrem";
				break;
			case 38:
				strFieldName = "GrpNo";
				break;
			case 39:
				strFieldName = "GrpName";
				break;
			case 40:
				strFieldName = "SocialCenterNo";
				break;
			case 41:
				strFieldName = "SocialCenterName";
				break;
			case 42:
				strFieldName = "HandlerDate";
				break;
			case 43:
				strFieldName = "HandlerPrint";
				break;
			case 44:
				strFieldName = "Handler";
				break;
			case 45:
				strFieldName = "JoinCompanyDate";
				break;
			case 46:
				strFieldName = "Position";
				break;
			case 47:
				strFieldName = "AscriptionRuleCode";
				break;
			case 48:
				strFieldName = "GetYear";
				break;
			case 49:
				strFieldName = "GetYearFlag";
				break;
			case 50:
				strFieldName = "GetDutyKind";
				break;
			case 51:
				strFieldName = "AppntPrem";
				break;
			case 52:
				strFieldName = "PersonPrem";
				break;
			case 53:
				strFieldName = "PersonOwnPrem";
				break;
			case 54:
				strFieldName = "PerBankCode";
				break;
			case 55:
				strFieldName = "PerBankAccNo";
				break;
			case 56:
				strFieldName = "PerAccName";
				break;
			case 57:
				strFieldName = "SchoolNmae";
				break;
			case 58:
				strFieldName = "ClassName";
				break;
			case 59:
				strFieldName = "AppntName";
				break;
			case 60:
				strFieldName = "AppntSex";
				break;
			case 61:
				strFieldName = "AppntBirthday";
				break;
			case 62:
				strFieldName = "AppntIdType";
				break;
			case 63:
				strFieldName = "AppntIdNo";
				break;
			case 64:
				strFieldName = "NativePlace";
				break;
			case 65:
				strFieldName = "Position2";
				break;
			case 66:
				strFieldName = "OccupationCode";
				break;
			case 67:
				strFieldName = "NativeCity";
				break;
			case 68:
				strFieldName = "UnCommonChar";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("GrpContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuredID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BatchNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuredNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Retire") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EmployeeName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuredName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Relation") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Sex") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Birthday") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("IDType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IDNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContPlanCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OccupationType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankAccNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PublicAccType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PublicAcc") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("EdorNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CalPremType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OthIDType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OthIDNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EnglishName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Phone") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CalRule") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("NoNamePeoples") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("EdorValiDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Prem") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EdorPrem") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SocialCenterNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SocialCenterName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HandlerDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("HandlerPrint") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Handler") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("JoinCompanyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Position") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AscriptionRuleCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GetYear") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("GetYearFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GetDutyKind") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppntPrem") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("PersonPrem") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("PersonOwnPrem") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("PerBankCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PerBankAccNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PerAccName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SchoolNmae") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ClassName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppntName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppntSex") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppntBirthday") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("AppntIdType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppntIdNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("NativePlace") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Position2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OccupationCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("NativeCity") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UnCommonChar") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_INT;
				break;
			case 35:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 36:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 37:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 38:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 39:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 40:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 41:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 42:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 43:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 44:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 45:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 46:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 47:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 48:
				nFieldType = Schema.TYPE_INT;
				break;
			case 49:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 50:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 51:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 52:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 53:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 54:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 55:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 56:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 57:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 58:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 59:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 60:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 61:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 62:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 63:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 64:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 65:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 66:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 67:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 68:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
