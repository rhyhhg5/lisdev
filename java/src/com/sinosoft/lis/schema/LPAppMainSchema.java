/*
 * <p>ClassName: LPAppMainSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2004-12-20
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LPAppMainDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LPAppMainSchema implements Schema
{
    // @Field
    /** 上报申请号 */
    private String AppID;
    /** 保险公司id */
    private String InsureID;
    /** 上报日期 */
    private Date AppDate;
    /** 上报人 */
    private String AppMan;
    /** 申请状态 */
    private String State;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 10; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LPAppMainSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "AppID";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getAppID()
    {
        if (AppID != null && !AppID.equals("") && SysConst.CHANGECHARSET == true)
        {
            AppID = StrTool.unicodeToGBK(AppID);
        }
        return AppID;
    }

    public void setAppID(String aAppID)
    {
        AppID = aAppID;
    }

    public String getInsureID()
    {
        if (InsureID != null && !InsureID.equals("") && SysConst.CHANGECHARSET == true)
        {
            InsureID = StrTool.unicodeToGBK(InsureID);
        }
        return InsureID;
    }

    public void setInsureID(String aInsureID)
    {
        InsureID = aInsureID;
    }

    public String getAppDate()
    {
        if (AppDate != null)
        {
            return fDate.getString(AppDate);
        }
        else
        {
            return null;
        }
    }

    public void setAppDate(Date aAppDate)
    {
        AppDate = aAppDate;
    }

    public void setAppDate(String aAppDate)
    {
        if (aAppDate != null && !aAppDate.equals(""))
        {
            AppDate = fDate.getDate(aAppDate);
        }
        else
        {
            AppDate = null;
        }
    }

    public String getAppMan()
    {
        if (AppMan != null && !AppMan.equals("") && SysConst.CHANGECHARSET == true)
        {
            AppMan = StrTool.unicodeToGBK(AppMan);
        }
        return AppMan;
    }

    public void setAppMan(String aAppMan)
    {
        AppMan = aAppMan;
    }

    public String getState()
    {
        if (State != null && !State.equals("") && SysConst.CHANGECHARSET == true)
        {
            State = StrTool.unicodeToGBK(State);
        }
        return State;
    }

    public void setState(String aState)
    {
        State = aState;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LPAppMainSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LPAppMainSchema aLPAppMainSchema)
    {
        this.AppID = aLPAppMainSchema.getAppID();
        this.InsureID = aLPAppMainSchema.getInsureID();
        this.AppDate = fDate.getDate(aLPAppMainSchema.getAppDate());
        this.AppMan = aLPAppMainSchema.getAppMan();
        this.State = aLPAppMainSchema.getState();
        this.Operator = aLPAppMainSchema.getOperator();
        this.MakeDate = fDate.getDate(aLPAppMainSchema.getMakeDate());
        this.MakeTime = aLPAppMainSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLPAppMainSchema.getModifyDate());
        this.ModifyTime = aLPAppMainSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("AppID") == null)
            {
                this.AppID = null;
            }
            else
            {
                this.AppID = rs.getString("AppID").trim();
            }

            if (rs.getString("InsureID") == null)
            {
                this.InsureID = null;
            }
            else
            {
                this.InsureID = rs.getString("InsureID").trim();
            }

            this.AppDate = rs.getDate("AppDate");
            if (rs.getString("AppMan") == null)
            {
                this.AppMan = null;
            }
            else
            {
                this.AppMan = rs.getString("AppMan").trim();
            }

            if (rs.getString("State") == null)
            {
                this.State = null;
            }
            else
            {
                this.State = rs.getString("State").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LPAppMainSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LPAppMainSchema getSchema()
    {
        LPAppMainSchema aLPAppMainSchema = new LPAppMainSchema();
        aLPAppMainSchema.setSchema(this);
        return aLPAppMainSchema;
    }

    public LPAppMainDB getDB()
    {
        LPAppMainDB aDBOper = new LPAppMainDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLPAppMain描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(AppID)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InsureID)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(AppDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AppMan)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(State)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLPAppMain>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            AppID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                   SysConst.PACKAGESPILTER);
            InsureID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            AppDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 3, SysConst.PACKAGESPILTER));
            AppMan = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                    SysConst.PACKAGESPILTER);
            State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                   SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 7, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 9, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LPAppMainSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("AppID"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AppID));
        }
        if (FCode.equals("InsureID"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(InsureID));
        }
        if (FCode.equals("AppDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getAppDate()));
        }
        if (FCode.equals("AppMan"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AppMan));
        }
        if (FCode.equals("State"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(State));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(AppID);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(InsureID);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getAppDate()));
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(AppMan);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(State);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("AppID"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppID = FValue.trim();
            }
            else
            {
                AppID = null;
            }
        }
        if (FCode.equals("InsureID"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InsureID = FValue.trim();
            }
            else
            {
                InsureID = null;
            }
        }
        if (FCode.equals("AppDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppDate = fDate.getDate(FValue);
            }
            else
            {
                AppDate = null;
            }
        }
        if (FCode.equals("AppMan"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppMan = FValue.trim();
            }
            else
            {
                AppMan = null;
            }
        }
        if (FCode.equals("State"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
            {
                State = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LPAppMainSchema other = (LPAppMainSchema) otherObject;
        return
                AppID.equals(other.getAppID())
                && InsureID.equals(other.getInsureID())
                && fDate.getString(AppDate).equals(other.getAppDate())
                && AppMan.equals(other.getAppMan())
                && State.equals(other.getState())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("AppID"))
        {
            return 0;
        }
        if (strFieldName.equals("InsureID"))
        {
            return 1;
        }
        if (strFieldName.equals("AppDate"))
        {
            return 2;
        }
        if (strFieldName.equals("AppMan"))
        {
            return 3;
        }
        if (strFieldName.equals("State"))
        {
            return 4;
        }
        if (strFieldName.equals("Operator"))
        {
            return 5;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 6;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 7;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 8;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 9;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "AppID";
                break;
            case 1:
                strFieldName = "InsureID";
                break;
            case 2:
                strFieldName = "AppDate";
                break;
            case 3:
                strFieldName = "AppMan";
                break;
            case 4:
                strFieldName = "State";
                break;
            case 5:
                strFieldName = "Operator";
                break;
            case 6:
                strFieldName = "MakeDate";
                break;
            case 7:
                strFieldName = "MakeTime";
                break;
            case 8:
                strFieldName = "ModifyDate";
                break;
            case 9:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("AppID"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsureID"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("AppMan"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("State"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
