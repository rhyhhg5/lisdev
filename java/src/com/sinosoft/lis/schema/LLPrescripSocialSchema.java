/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LLPrescripSocialDB;

/*
 * <p>ClassName: LLPrescripSocialSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2015-12-10
 */
public class LLPrescripSocialSchema implements Schema, Cloneable
{
	// @Field
	/** 账单项目流水号 */
	private String SocialFeeNo;
	/** 案件流水号 */
	private String SocialCaseNo;
	/** 批次流水号 */
	private String SocialRgtNo;
	/** 理赔信息序号 */
	private String TransacTionNo;
	/** 医院代码 */
	private String HospitalCode;
	/** 实际住院天数 */
	private int RealHospDate;
	/** 医院名称 */
	private String HospitalName;
	/** 账单号码 */
	private String ReceiptNo;
	/** 账单日期 */
	private Date FeeDate;
	/** 住院号 */
	private String InHosNo;
	/** 合计金额 */
	private double ApplyAmnt;
	/** 统筹基金支付 */
	private double PlanFee;
	/** 大额医疗支付 */
	private double SupInHosFee;
	/** 公务员医疗补助 */
	private double OfficialSubsidy;
	/** 自费 */
	private double SelfAmnt;
	/** 自付二 */
	private double SelfPay2;
	/** 自付一 */
	private double SelfPay1;
	/** 起付线 */
	private double GetLimit;
	/** 中段 */
	private double MidAmnt;
	/** 高段1 */
	private double HighAmnt1;
	/** 高段2 */
	private double HighAmnt2;
	/** 超高段 */
	private double SuperAmnt;
	/** 退休补充医疗 */
	private double RetireAddFee;
	/** 小额门急诊 */
	private double SmallDoorPay;
	/** 大额门急诊 */
	private double HighDoorAmnt;
	/** 可报销范围内金额 */
	private double Reimbursement;
	/** 管理机构 */
	private String MngCom;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 32;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LLPrescripSocialSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "SocialFeeNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LLPrescripSocialSchema cloned = (LLPrescripSocialSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getSocialFeeNo()
	{
		return SocialFeeNo;
	}
	public void setSocialFeeNo(String aSocialFeeNo)
	{
		SocialFeeNo = aSocialFeeNo;
	}
	public String getSocialCaseNo()
	{
		return SocialCaseNo;
	}
	public void setSocialCaseNo(String aSocialCaseNo)
	{
		SocialCaseNo = aSocialCaseNo;
	}
	public String getSocialRgtNo()
	{
		return SocialRgtNo;
	}
	public void setSocialRgtNo(String aSocialRgtNo)
	{
		SocialRgtNo = aSocialRgtNo;
	}
	public String getTransacTionNo()
	{
		return TransacTionNo;
	}
	public void setTransacTionNo(String aTransacTionNo)
	{
		TransacTionNo = aTransacTionNo;
	}
	public String getHospitalCode()
	{
		return HospitalCode;
	}
	public void setHospitalCode(String aHospitalCode)
	{
		HospitalCode = aHospitalCode;
	}
	public int getRealHospDate()
	{
		return RealHospDate;
	}
	public void setRealHospDate(int aRealHospDate)
	{
		RealHospDate = aRealHospDate;
	}
	public void setRealHospDate(String aRealHospDate)
	{
		if (aRealHospDate != null && !aRealHospDate.equals(""))
		{
			Integer tInteger = new Integer(aRealHospDate);
			int i = tInteger.intValue();
			RealHospDate = i;
		}
	}

	public String getHospitalName()
	{
		return HospitalName;
	}
	public void setHospitalName(String aHospitalName)
	{
		HospitalName = aHospitalName;
	}
	public String getReceiptNo()
	{
		return ReceiptNo;
	}
	public void setReceiptNo(String aReceiptNo)
	{
		ReceiptNo = aReceiptNo;
	}
	public String getFeeDate()
	{
		if( FeeDate != null )
			return fDate.getString(FeeDate);
		else
			return null;
	}
	public void setFeeDate(Date aFeeDate)
	{
		FeeDate = aFeeDate;
	}
	public void setFeeDate(String aFeeDate)
	{
		if (aFeeDate != null && !aFeeDate.equals("") )
		{
			FeeDate = fDate.getDate( aFeeDate );
		}
		else
			FeeDate = null;
	}

	public String getInHosNo()
	{
		return InHosNo;
	}
	public void setInHosNo(String aInHosNo)
	{
		InHosNo = aInHosNo;
	}
	public double getApplyAmnt()
	{
		return ApplyAmnt;
	}
	public void setApplyAmnt(double aApplyAmnt)
	{
		ApplyAmnt = Arith.round(aApplyAmnt,2);
	}
	public void setApplyAmnt(String aApplyAmnt)
	{
		if (aApplyAmnt != null && !aApplyAmnt.equals(""))
		{
			Double tDouble = new Double(aApplyAmnt);
			double d = tDouble.doubleValue();
                ApplyAmnt = Arith.round(d,2);
		}
	}

	public double getPlanFee()
	{
		return PlanFee;
	}
	public void setPlanFee(double aPlanFee)
	{
		PlanFee = Arith.round(aPlanFee,2);
	}
	public void setPlanFee(String aPlanFee)
	{
		if (aPlanFee != null && !aPlanFee.equals(""))
		{
			Double tDouble = new Double(aPlanFee);
			double d = tDouble.doubleValue();
                PlanFee = Arith.round(d,2);
		}
	}

	public double getSupInHosFee()
	{
		return SupInHosFee;
	}
	public void setSupInHosFee(double aSupInHosFee)
	{
		SupInHosFee = Arith.round(aSupInHosFee,2);
	}
	public void setSupInHosFee(String aSupInHosFee)
	{
		if (aSupInHosFee != null && !aSupInHosFee.equals(""))
		{
			Double tDouble = new Double(aSupInHosFee);
			double d = tDouble.doubleValue();
                SupInHosFee = Arith.round(d,2);
		}
	}

	public double getOfficialSubsidy()
	{
		return OfficialSubsidy;
	}
	public void setOfficialSubsidy(double aOfficialSubsidy)
	{
		OfficialSubsidy = Arith.round(aOfficialSubsidy,2);
	}
	public void setOfficialSubsidy(String aOfficialSubsidy)
	{
		if (aOfficialSubsidy != null && !aOfficialSubsidy.equals(""))
		{
			Double tDouble = new Double(aOfficialSubsidy);
			double d = tDouble.doubleValue();
                OfficialSubsidy = Arith.round(d,2);
		}
	}

	public double getSelfAmnt()
	{
		return SelfAmnt;
	}
	public void setSelfAmnt(double aSelfAmnt)
	{
		SelfAmnt = Arith.round(aSelfAmnt,2);
	}
	public void setSelfAmnt(String aSelfAmnt)
	{
		if (aSelfAmnt != null && !aSelfAmnt.equals(""))
		{
			Double tDouble = new Double(aSelfAmnt);
			double d = tDouble.doubleValue();
                SelfAmnt = Arith.round(d,2);
		}
	}

	public double getSelfPay2()
	{
		return SelfPay2;
	}
	public void setSelfPay2(double aSelfPay2)
	{
		SelfPay2 = Arith.round(aSelfPay2,2);
	}
	public void setSelfPay2(String aSelfPay2)
	{
		if (aSelfPay2 != null && !aSelfPay2.equals(""))
		{
			Double tDouble = new Double(aSelfPay2);
			double d = tDouble.doubleValue();
                SelfPay2 = Arith.round(d,2);
		}
	}

	public double getSelfPay1()
	{
		return SelfPay1;
	}
	public void setSelfPay1(double aSelfPay1)
	{
		SelfPay1 = Arith.round(aSelfPay1,2);
	}
	public void setSelfPay1(String aSelfPay1)
	{
		if (aSelfPay1 != null && !aSelfPay1.equals(""))
		{
			Double tDouble = new Double(aSelfPay1);
			double d = tDouble.doubleValue();
                SelfPay1 = Arith.round(d,2);
		}
	}

	public double getGetLimit()
	{
		return GetLimit;
	}
	public void setGetLimit(double aGetLimit)
	{
		GetLimit = Arith.round(aGetLimit,2);
	}
	public void setGetLimit(String aGetLimit)
	{
		if (aGetLimit != null && !aGetLimit.equals(""))
		{
			Double tDouble = new Double(aGetLimit);
			double d = tDouble.doubleValue();
                GetLimit = Arith.round(d,2);
		}
	}

	public double getMidAmnt()
	{
		return MidAmnt;
	}
	public void setMidAmnt(double aMidAmnt)
	{
		MidAmnt = Arith.round(aMidAmnt,2);
	}
	public void setMidAmnt(String aMidAmnt)
	{
		if (aMidAmnt != null && !aMidAmnt.equals(""))
		{
			Double tDouble = new Double(aMidAmnt);
			double d = tDouble.doubleValue();
                MidAmnt = Arith.round(d,2);
		}
	}

	public double getHighAmnt1()
	{
		return HighAmnt1;
	}
	public void setHighAmnt1(double aHighAmnt1)
	{
		HighAmnt1 = Arith.round(aHighAmnt1,2);
	}
	public void setHighAmnt1(String aHighAmnt1)
	{
		if (aHighAmnt1 != null && !aHighAmnt1.equals(""))
		{
			Double tDouble = new Double(aHighAmnt1);
			double d = tDouble.doubleValue();
                HighAmnt1 = Arith.round(d,2);
		}
	}

	public double getHighAmnt2()
	{
		return HighAmnt2;
	}
	public void setHighAmnt2(double aHighAmnt2)
	{
		HighAmnt2 = Arith.round(aHighAmnt2,2);
	}
	public void setHighAmnt2(String aHighAmnt2)
	{
		if (aHighAmnt2 != null && !aHighAmnt2.equals(""))
		{
			Double tDouble = new Double(aHighAmnt2);
			double d = tDouble.doubleValue();
                HighAmnt2 = Arith.round(d,2);
		}
	}

	public double getSuperAmnt()
	{
		return SuperAmnt;
	}
	public void setSuperAmnt(double aSuperAmnt)
	{
		SuperAmnt = Arith.round(aSuperAmnt,2);
	}
	public void setSuperAmnt(String aSuperAmnt)
	{
		if (aSuperAmnt != null && !aSuperAmnt.equals(""))
		{
			Double tDouble = new Double(aSuperAmnt);
			double d = tDouble.doubleValue();
                SuperAmnt = Arith.round(d,2);
		}
	}

	public double getRetireAddFee()
	{
		return RetireAddFee;
	}
	public void setRetireAddFee(double aRetireAddFee)
	{
		RetireAddFee = Arith.round(aRetireAddFee,2);
	}
	public void setRetireAddFee(String aRetireAddFee)
	{
		if (aRetireAddFee != null && !aRetireAddFee.equals(""))
		{
			Double tDouble = new Double(aRetireAddFee);
			double d = tDouble.doubleValue();
                RetireAddFee = Arith.round(d,2);
		}
	}

	public double getSmallDoorPay()
	{
		return SmallDoorPay;
	}
	public void setSmallDoorPay(double aSmallDoorPay)
	{
		SmallDoorPay = Arith.round(aSmallDoorPay,2);
	}
	public void setSmallDoorPay(String aSmallDoorPay)
	{
		if (aSmallDoorPay != null && !aSmallDoorPay.equals(""))
		{
			Double tDouble = new Double(aSmallDoorPay);
			double d = tDouble.doubleValue();
                SmallDoorPay = Arith.round(d,2);
		}
	}

	public double getHighDoorAmnt()
	{
		return HighDoorAmnt;
	}
	public void setHighDoorAmnt(double aHighDoorAmnt)
	{
		HighDoorAmnt = Arith.round(aHighDoorAmnt,2);
	}
	public void setHighDoorAmnt(String aHighDoorAmnt)
	{
		if (aHighDoorAmnt != null && !aHighDoorAmnt.equals(""))
		{
			Double tDouble = new Double(aHighDoorAmnt);
			double d = tDouble.doubleValue();
                HighDoorAmnt = Arith.round(d,2);
		}
	}

	public double getReimbursement()
	{
		return Reimbursement;
	}
	public void setReimbursement(double aReimbursement)
	{
		Reimbursement = Arith.round(aReimbursement,2);
	}
	public void setReimbursement(String aReimbursement)
	{
		if (aReimbursement != null && !aReimbursement.equals(""))
		{
			Double tDouble = new Double(aReimbursement);
			double d = tDouble.doubleValue();
                Reimbursement = Arith.round(d,2);
		}
	}

	public String getMngCom()
	{
		return MngCom;
	}
	public void setMngCom(String aMngCom)
	{
		MngCom = aMngCom;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LLPrescripSocialSchema 对象给 Schema 赋值
	* @param: aLLPrescripSocialSchema LLPrescripSocialSchema
	**/
	public void setSchema(LLPrescripSocialSchema aLLPrescripSocialSchema)
	{
		this.SocialFeeNo = aLLPrescripSocialSchema.getSocialFeeNo();
		this.SocialCaseNo = aLLPrescripSocialSchema.getSocialCaseNo();
		this.SocialRgtNo = aLLPrescripSocialSchema.getSocialRgtNo();
		this.TransacTionNo = aLLPrescripSocialSchema.getTransacTionNo();
		this.HospitalCode = aLLPrescripSocialSchema.getHospitalCode();
		this.RealHospDate = aLLPrescripSocialSchema.getRealHospDate();
		this.HospitalName = aLLPrescripSocialSchema.getHospitalName();
		this.ReceiptNo = aLLPrescripSocialSchema.getReceiptNo();
		this.FeeDate = fDate.getDate( aLLPrescripSocialSchema.getFeeDate());
		this.InHosNo = aLLPrescripSocialSchema.getInHosNo();
		this.ApplyAmnt = aLLPrescripSocialSchema.getApplyAmnt();
		this.PlanFee = aLLPrescripSocialSchema.getPlanFee();
		this.SupInHosFee = aLLPrescripSocialSchema.getSupInHosFee();
		this.OfficialSubsidy = aLLPrescripSocialSchema.getOfficialSubsidy();
		this.SelfAmnt = aLLPrescripSocialSchema.getSelfAmnt();
		this.SelfPay2 = aLLPrescripSocialSchema.getSelfPay2();
		this.SelfPay1 = aLLPrescripSocialSchema.getSelfPay1();
		this.GetLimit = aLLPrescripSocialSchema.getGetLimit();
		this.MidAmnt = aLLPrescripSocialSchema.getMidAmnt();
		this.HighAmnt1 = aLLPrescripSocialSchema.getHighAmnt1();
		this.HighAmnt2 = aLLPrescripSocialSchema.getHighAmnt2();
		this.SuperAmnt = aLLPrescripSocialSchema.getSuperAmnt();
		this.RetireAddFee = aLLPrescripSocialSchema.getRetireAddFee();
		this.SmallDoorPay = aLLPrescripSocialSchema.getSmallDoorPay();
		this.HighDoorAmnt = aLLPrescripSocialSchema.getHighDoorAmnt();
		this.Reimbursement = aLLPrescripSocialSchema.getReimbursement();
		this.MngCom = aLLPrescripSocialSchema.getMngCom();
		this.Operator = aLLPrescripSocialSchema.getOperator();
		this.MakeDate = fDate.getDate( aLLPrescripSocialSchema.getMakeDate());
		this.MakeTime = aLLPrescripSocialSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLLPrescripSocialSchema.getModifyDate());
		this.ModifyTime = aLLPrescripSocialSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("SocialFeeNo") == null )
				this.SocialFeeNo = null;
			else
				this.SocialFeeNo = rs.getString("SocialFeeNo").trim();

			if( rs.getString("SocialCaseNo") == null )
				this.SocialCaseNo = null;
			else
				this.SocialCaseNo = rs.getString("SocialCaseNo").trim();

			if( rs.getString("SocialRgtNo") == null )
				this.SocialRgtNo = null;
			else
				this.SocialRgtNo = rs.getString("SocialRgtNo").trim();

			if( rs.getString("TransacTionNo") == null )
				this.TransacTionNo = null;
			else
				this.TransacTionNo = rs.getString("TransacTionNo").trim();

			if( rs.getString("HospitalCode") == null )
				this.HospitalCode = null;
			else
				this.HospitalCode = rs.getString("HospitalCode").trim();

			this.RealHospDate = rs.getInt("RealHospDate");
			if( rs.getString("HospitalName") == null )
				this.HospitalName = null;
			else
				this.HospitalName = rs.getString("HospitalName").trim();

			if( rs.getString("ReceiptNo") == null )
				this.ReceiptNo = null;
			else
				this.ReceiptNo = rs.getString("ReceiptNo").trim();

			this.FeeDate = rs.getDate("FeeDate");
			if( rs.getString("InHosNo") == null )
				this.InHosNo = null;
			else
				this.InHosNo = rs.getString("InHosNo").trim();

			this.ApplyAmnt = rs.getDouble("ApplyAmnt");
			this.PlanFee = rs.getDouble("PlanFee");
			this.SupInHosFee = rs.getDouble("SupInHosFee");
			this.OfficialSubsidy = rs.getDouble("OfficialSubsidy");
			this.SelfAmnt = rs.getDouble("SelfAmnt");
			this.SelfPay2 = rs.getDouble("SelfPay2");
			this.SelfPay1 = rs.getDouble("SelfPay1");
			this.GetLimit = rs.getDouble("GetLimit");
			this.MidAmnt = rs.getDouble("MidAmnt");
			this.HighAmnt1 = rs.getDouble("HighAmnt1");
			this.HighAmnt2 = rs.getDouble("HighAmnt2");
			this.SuperAmnt = rs.getDouble("SuperAmnt");
			this.RetireAddFee = rs.getDouble("RetireAddFee");
			this.SmallDoorPay = rs.getDouble("SmallDoorPay");
			this.HighDoorAmnt = rs.getDouble("HighDoorAmnt");
			this.Reimbursement = rs.getDouble("Reimbursement");
			if( rs.getString("MngCom") == null )
				this.MngCom = null;
			else
				this.MngCom = rs.getString("MngCom").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LLPrescripSocial表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLPrescripSocialSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LLPrescripSocialSchema getSchema()
	{
		LLPrescripSocialSchema aLLPrescripSocialSchema = new LLPrescripSocialSchema();
		aLLPrescripSocialSchema.setSchema(this);
		return aLLPrescripSocialSchema;
	}

	public LLPrescripSocialDB getDB()
	{
		LLPrescripSocialDB aDBOper = new LLPrescripSocialDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLPrescripSocial描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(SocialFeeNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SocialCaseNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SocialRgtNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TransacTionNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HospitalCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(RealHospDate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HospitalName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReceiptNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( FeeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InHosNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ApplyAmnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PlanFee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SupInHosFee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(OfficialSubsidy));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SelfAmnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SelfPay2));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SelfPay1));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(GetLimit));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(MidAmnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(HighAmnt1));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(HighAmnt2));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SuperAmnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(RetireAddFee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SmallDoorPay));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(HighDoorAmnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Reimbursement));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MngCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLPrescripSocial>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			SocialFeeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			SocialCaseNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			SocialRgtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			TransacTionNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			HospitalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			RealHospDate= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,6,SysConst.PACKAGESPILTER))).intValue();
			HospitalName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			ReceiptNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			FeeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,SysConst.PACKAGESPILTER));
			InHosNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			ApplyAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,11,SysConst.PACKAGESPILTER))).doubleValue();
			PlanFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,12,SysConst.PACKAGESPILTER))).doubleValue();
			SupInHosFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,13,SysConst.PACKAGESPILTER))).doubleValue();
			OfficialSubsidy = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,14,SysConst.PACKAGESPILTER))).doubleValue();
			SelfAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,15,SysConst.PACKAGESPILTER))).doubleValue();
			SelfPay2 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,16,SysConst.PACKAGESPILTER))).doubleValue();
			SelfPay1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,17,SysConst.PACKAGESPILTER))).doubleValue();
			GetLimit = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,18,SysConst.PACKAGESPILTER))).doubleValue();
			MidAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,19,SysConst.PACKAGESPILTER))).doubleValue();
			HighAmnt1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,20,SysConst.PACKAGESPILTER))).doubleValue();
			HighAmnt2 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,21,SysConst.PACKAGESPILTER))).doubleValue();
			SuperAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,22,SysConst.PACKAGESPILTER))).doubleValue();
			RetireAddFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,23,SysConst.PACKAGESPILTER))).doubleValue();
			SmallDoorPay = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,24,SysConst.PACKAGESPILTER))).doubleValue();
			HighDoorAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,25,SysConst.PACKAGESPILTER))).doubleValue();
			Reimbursement = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,26,SysConst.PACKAGESPILTER))).doubleValue();
			MngCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLPrescripSocialSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("SocialFeeNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SocialFeeNo));
		}
		if (FCode.equals("SocialCaseNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SocialCaseNo));
		}
		if (FCode.equals("SocialRgtNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SocialRgtNo));
		}
		if (FCode.equals("TransacTionNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransacTionNo));
		}
		if (FCode.equals("HospitalCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HospitalCode));
		}
		if (FCode.equals("RealHospDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RealHospDate));
		}
		if (FCode.equals("HospitalName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HospitalName));
		}
		if (FCode.equals("ReceiptNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiptNo));
		}
		if (FCode.equals("FeeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getFeeDate()));
		}
		if (FCode.equals("InHosNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InHosNo));
		}
		if (FCode.equals("ApplyAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ApplyAmnt));
		}
		if (FCode.equals("PlanFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PlanFee));
		}
		if (FCode.equals("SupInHosFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SupInHosFee));
		}
		if (FCode.equals("OfficialSubsidy"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OfficialSubsidy));
		}
		if (FCode.equals("SelfAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SelfAmnt));
		}
		if (FCode.equals("SelfPay2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SelfPay2));
		}
		if (FCode.equals("SelfPay1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SelfPay1));
		}
		if (FCode.equals("GetLimit"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GetLimit));
		}
		if (FCode.equals("MidAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MidAmnt));
		}
		if (FCode.equals("HighAmnt1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HighAmnt1));
		}
		if (FCode.equals("HighAmnt2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HighAmnt2));
		}
		if (FCode.equals("SuperAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SuperAmnt));
		}
		if (FCode.equals("RetireAddFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RetireAddFee));
		}
		if (FCode.equals("SmallDoorPay"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SmallDoorPay));
		}
		if (FCode.equals("HighDoorAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HighDoorAmnt));
		}
		if (FCode.equals("Reimbursement"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Reimbursement));
		}
		if (FCode.equals("MngCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MngCom));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(SocialFeeNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(SocialCaseNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(SocialRgtNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(TransacTionNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(HospitalCode);
				break;
			case 5:
				strFieldValue = String.valueOf(RealHospDate);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(HospitalName);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(ReceiptNo);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getFeeDate()));
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(InHosNo);
				break;
			case 10:
				strFieldValue = String.valueOf(ApplyAmnt);
				break;
			case 11:
				strFieldValue = String.valueOf(PlanFee);
				break;
			case 12:
				strFieldValue = String.valueOf(SupInHosFee);
				break;
			case 13:
				strFieldValue = String.valueOf(OfficialSubsidy);
				break;
			case 14:
				strFieldValue = String.valueOf(SelfAmnt);
				break;
			case 15:
				strFieldValue = String.valueOf(SelfPay2);
				break;
			case 16:
				strFieldValue = String.valueOf(SelfPay1);
				break;
			case 17:
				strFieldValue = String.valueOf(GetLimit);
				break;
			case 18:
				strFieldValue = String.valueOf(MidAmnt);
				break;
			case 19:
				strFieldValue = String.valueOf(HighAmnt1);
				break;
			case 20:
				strFieldValue = String.valueOf(HighAmnt2);
				break;
			case 21:
				strFieldValue = String.valueOf(SuperAmnt);
				break;
			case 22:
				strFieldValue = String.valueOf(RetireAddFee);
				break;
			case 23:
				strFieldValue = String.valueOf(SmallDoorPay);
				break;
			case 24:
				strFieldValue = String.valueOf(HighDoorAmnt);
				break;
			case 25:
				strFieldValue = String.valueOf(Reimbursement);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(MngCom);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("SocialFeeNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SocialFeeNo = FValue.trim();
			}
			else
				SocialFeeNo = null;
		}
		if (FCode.equalsIgnoreCase("SocialCaseNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SocialCaseNo = FValue.trim();
			}
			else
				SocialCaseNo = null;
		}
		if (FCode.equalsIgnoreCase("SocialRgtNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SocialRgtNo = FValue.trim();
			}
			else
				SocialRgtNo = null;
		}
		if (FCode.equalsIgnoreCase("TransacTionNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TransacTionNo = FValue.trim();
			}
			else
				TransacTionNo = null;
		}
		if (FCode.equalsIgnoreCase("HospitalCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HospitalCode = FValue.trim();
			}
			else
				HospitalCode = null;
		}
		if (FCode.equalsIgnoreCase("RealHospDate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				RealHospDate = i;
			}
		}
		if (FCode.equalsIgnoreCase("HospitalName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HospitalName = FValue.trim();
			}
			else
				HospitalName = null;
		}
		if (FCode.equalsIgnoreCase("ReceiptNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReceiptNo = FValue.trim();
			}
			else
				ReceiptNo = null;
		}
		if (FCode.equalsIgnoreCase("FeeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				FeeDate = fDate.getDate( FValue );
			}
			else
				FeeDate = null;
		}
		if (FCode.equalsIgnoreCase("InHosNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InHosNo = FValue.trim();
			}
			else
				InHosNo = null;
		}
		if (FCode.equalsIgnoreCase("ApplyAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ApplyAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("PlanFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				PlanFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("SupInHosFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SupInHosFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("OfficialSubsidy"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				OfficialSubsidy = d;
			}
		}
		if (FCode.equalsIgnoreCase("SelfAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SelfAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("SelfPay2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SelfPay2 = d;
			}
		}
		if (FCode.equalsIgnoreCase("SelfPay1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SelfPay1 = d;
			}
		}
		if (FCode.equalsIgnoreCase("GetLimit"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				GetLimit = d;
			}
		}
		if (FCode.equalsIgnoreCase("MidAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				MidAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("HighAmnt1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				HighAmnt1 = d;
			}
		}
		if (FCode.equalsIgnoreCase("HighAmnt2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				HighAmnt2 = d;
			}
		}
		if (FCode.equalsIgnoreCase("SuperAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SuperAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("RetireAddFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				RetireAddFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("SmallDoorPay"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SmallDoorPay = d;
			}
		}
		if (FCode.equalsIgnoreCase("HighDoorAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				HighDoorAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("Reimbursement"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Reimbursement = d;
			}
		}
		if (FCode.equalsIgnoreCase("MngCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MngCom = FValue.trim();
			}
			else
				MngCom = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LLPrescripSocialSchema other = (LLPrescripSocialSchema)otherObject;
		return
			(SocialFeeNo == null ? other.getSocialFeeNo() == null : SocialFeeNo.equals(other.getSocialFeeNo()))
			&& (SocialCaseNo == null ? other.getSocialCaseNo() == null : SocialCaseNo.equals(other.getSocialCaseNo()))
			&& (SocialRgtNo == null ? other.getSocialRgtNo() == null : SocialRgtNo.equals(other.getSocialRgtNo()))
			&& (TransacTionNo == null ? other.getTransacTionNo() == null : TransacTionNo.equals(other.getTransacTionNo()))
			&& (HospitalCode == null ? other.getHospitalCode() == null : HospitalCode.equals(other.getHospitalCode()))
			&& RealHospDate == other.getRealHospDate()
			&& (HospitalName == null ? other.getHospitalName() == null : HospitalName.equals(other.getHospitalName()))
			&& (ReceiptNo == null ? other.getReceiptNo() == null : ReceiptNo.equals(other.getReceiptNo()))
			&& (FeeDate == null ? other.getFeeDate() == null : fDate.getString(FeeDate).equals(other.getFeeDate()))
			&& (InHosNo == null ? other.getInHosNo() == null : InHosNo.equals(other.getInHosNo()))
			&& ApplyAmnt == other.getApplyAmnt()
			&& PlanFee == other.getPlanFee()
			&& SupInHosFee == other.getSupInHosFee()
			&& OfficialSubsidy == other.getOfficialSubsidy()
			&& SelfAmnt == other.getSelfAmnt()
			&& SelfPay2 == other.getSelfPay2()
			&& SelfPay1 == other.getSelfPay1()
			&& GetLimit == other.getGetLimit()
			&& MidAmnt == other.getMidAmnt()
			&& HighAmnt1 == other.getHighAmnt1()
			&& HighAmnt2 == other.getHighAmnt2()
			&& SuperAmnt == other.getSuperAmnt()
			&& RetireAddFee == other.getRetireAddFee()
			&& SmallDoorPay == other.getSmallDoorPay()
			&& HighDoorAmnt == other.getHighDoorAmnt()
			&& Reimbursement == other.getReimbursement()
			&& (MngCom == null ? other.getMngCom() == null : MngCom.equals(other.getMngCom()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("SocialFeeNo") ) {
			return 0;
		}
		if( strFieldName.equals("SocialCaseNo") ) {
			return 1;
		}
		if( strFieldName.equals("SocialRgtNo") ) {
			return 2;
		}
		if( strFieldName.equals("TransacTionNo") ) {
			return 3;
		}
		if( strFieldName.equals("HospitalCode") ) {
			return 4;
		}
		if( strFieldName.equals("RealHospDate") ) {
			return 5;
		}
		if( strFieldName.equals("HospitalName") ) {
			return 6;
		}
		if( strFieldName.equals("ReceiptNo") ) {
			return 7;
		}
		if( strFieldName.equals("FeeDate") ) {
			return 8;
		}
		if( strFieldName.equals("InHosNo") ) {
			return 9;
		}
		if( strFieldName.equals("ApplyAmnt") ) {
			return 10;
		}
		if( strFieldName.equals("PlanFee") ) {
			return 11;
		}
		if( strFieldName.equals("SupInHosFee") ) {
			return 12;
		}
		if( strFieldName.equals("OfficialSubsidy") ) {
			return 13;
		}
		if( strFieldName.equals("SelfAmnt") ) {
			return 14;
		}
		if( strFieldName.equals("SelfPay2") ) {
			return 15;
		}
		if( strFieldName.equals("SelfPay1") ) {
			return 16;
		}
		if( strFieldName.equals("GetLimit") ) {
			return 17;
		}
		if( strFieldName.equals("MidAmnt") ) {
			return 18;
		}
		if( strFieldName.equals("HighAmnt1") ) {
			return 19;
		}
		if( strFieldName.equals("HighAmnt2") ) {
			return 20;
		}
		if( strFieldName.equals("SuperAmnt") ) {
			return 21;
		}
		if( strFieldName.equals("RetireAddFee") ) {
			return 22;
		}
		if( strFieldName.equals("SmallDoorPay") ) {
			return 23;
		}
		if( strFieldName.equals("HighDoorAmnt") ) {
			return 24;
		}
		if( strFieldName.equals("Reimbursement") ) {
			return 25;
		}
		if( strFieldName.equals("MngCom") ) {
			return 26;
		}
		if( strFieldName.equals("Operator") ) {
			return 27;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 28;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 29;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 30;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 31;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "SocialFeeNo";
				break;
			case 1:
				strFieldName = "SocialCaseNo";
				break;
			case 2:
				strFieldName = "SocialRgtNo";
				break;
			case 3:
				strFieldName = "TransacTionNo";
				break;
			case 4:
				strFieldName = "HospitalCode";
				break;
			case 5:
				strFieldName = "RealHospDate";
				break;
			case 6:
				strFieldName = "HospitalName";
				break;
			case 7:
				strFieldName = "ReceiptNo";
				break;
			case 8:
				strFieldName = "FeeDate";
				break;
			case 9:
				strFieldName = "InHosNo";
				break;
			case 10:
				strFieldName = "ApplyAmnt";
				break;
			case 11:
				strFieldName = "PlanFee";
				break;
			case 12:
				strFieldName = "SupInHosFee";
				break;
			case 13:
				strFieldName = "OfficialSubsidy";
				break;
			case 14:
				strFieldName = "SelfAmnt";
				break;
			case 15:
				strFieldName = "SelfPay2";
				break;
			case 16:
				strFieldName = "SelfPay1";
				break;
			case 17:
				strFieldName = "GetLimit";
				break;
			case 18:
				strFieldName = "MidAmnt";
				break;
			case 19:
				strFieldName = "HighAmnt1";
				break;
			case 20:
				strFieldName = "HighAmnt2";
				break;
			case 21:
				strFieldName = "SuperAmnt";
				break;
			case 22:
				strFieldName = "RetireAddFee";
				break;
			case 23:
				strFieldName = "SmallDoorPay";
				break;
			case 24:
				strFieldName = "HighDoorAmnt";
				break;
			case 25:
				strFieldName = "Reimbursement";
				break;
			case 26:
				strFieldName = "MngCom";
				break;
			case 27:
				strFieldName = "Operator";
				break;
			case 28:
				strFieldName = "MakeDate";
				break;
			case 29:
				strFieldName = "MakeTime";
				break;
			case 30:
				strFieldName = "ModifyDate";
				break;
			case 31:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("SocialFeeNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SocialCaseNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SocialRgtNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TransacTionNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HospitalCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RealHospDate") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("HospitalName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReceiptNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FeeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("InHosNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ApplyAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("PlanFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SupInHosFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("OfficialSubsidy") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SelfAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SelfPay2") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SelfPay1") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("GetLimit") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("MidAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("HighAmnt1") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("HighAmnt2") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SuperAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("RetireAddFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SmallDoorPay") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("HighDoorAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Reimbursement") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("MngCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_INT;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 11:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 12:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 13:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 14:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 15:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 16:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 17:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 18:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 19:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 20:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 21:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 22:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 23:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 24:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 25:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
