/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LATreeAccessoryBDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

/*
 * <p>ClassName: LATreeAccessoryBSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-03-29
 */
public class LATreeAccessoryBSchema implements Schema
{
    // @Field
    /** 转储号码 */
    private String EdorNo;
    /** 转储类型 */
    private String EdorType;
    /** 代理人编码 */
    private String AgentCode;
    /** 代理人职级 */
    private String AgentGrade;
    /** 代理人展业机构代码 */
    private String AgentGroup;
    /** 育成代理人编码 */
    private String RearAgentCode;
    /** 抽佣链断裂标记 */
    private String CommBreakFlag;
    /** 间接抽佣链断裂标记 */
    private String IntroBreakFlag;
    /** 抽佣起期 */
    private Date startdate;
    /** 抽佣止期 */
    private Date enddate;
    /** 回算标志 */
    private String BackCalFlag;
    /** 指标计算编码 */
    private String IndexCalNo;
    /** 状态 */
    private String State;
    /** 育成关系标志 */
    private String RearFlag;
    /** 预留 */
    private String Flag;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 20; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LATreeAccessoryBSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[4];
        pk[0] = "EdorNo";
        pk[1] = "EdorType";
        pk[2] = "AgentCode";
        pk[3] = "AgentGrade";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getEdorNo()
    {
        if (SysConst.CHANGECHARSET && EdorNo != null && !EdorNo.equals(""))
        {
            EdorNo = StrTool.unicodeToGBK(EdorNo);
        }
        return EdorNo;
    }

    public void setEdorNo(String aEdorNo)
    {
        EdorNo = aEdorNo;
    }

    public String getEdorType()
    {
        if (SysConst.CHANGECHARSET && EdorType != null && !EdorType.equals(""))
        {
            EdorType = StrTool.unicodeToGBK(EdorType);
        }
        return EdorType;
    }

    public void setEdorType(String aEdorType)
    {
        EdorType = aEdorType;
    }

    public String getAgentCode()
    {
        if (SysConst.CHANGECHARSET && AgentCode != null && !AgentCode.equals(""))
        {
            AgentCode = StrTool.unicodeToGBK(AgentCode);
        }
        return AgentCode;
    }

    public void setAgentCode(String aAgentCode)
    {
        AgentCode = aAgentCode;
    }

    public String getAgentGrade()
    {
        if (SysConst.CHANGECHARSET && AgentGrade != null &&
            !AgentGrade.equals(""))
        {
            AgentGrade = StrTool.unicodeToGBK(AgentGrade);
        }
        return AgentGrade;
    }

    public void setAgentGrade(String aAgentGrade)
    {
        AgentGrade = aAgentGrade;
    }

    public String getAgentGroup()
    {
        if (SysConst.CHANGECHARSET && AgentGroup != null &&
            !AgentGroup.equals(""))
        {
            AgentGroup = StrTool.unicodeToGBK(AgentGroup);
        }
        return AgentGroup;
    }

    public void setAgentGroup(String aAgentGroup)
    {
        AgentGroup = aAgentGroup;
    }

    public String getRearAgentCode()
    {
        if (SysConst.CHANGECHARSET && RearAgentCode != null &&
            !RearAgentCode.equals(""))
        {
            RearAgentCode = StrTool.unicodeToGBK(RearAgentCode);
        }
        return RearAgentCode;
    }

    public void setRearAgentCode(String aRearAgentCode)
    {
        RearAgentCode = aRearAgentCode;
    }

    public String getCommBreakFlag()
    {
        if (SysConst.CHANGECHARSET && CommBreakFlag != null &&
            !CommBreakFlag.equals(""))
        {
            CommBreakFlag = StrTool.unicodeToGBK(CommBreakFlag);
        }
        return CommBreakFlag;
    }

    public void setCommBreakFlag(String aCommBreakFlag)
    {
        CommBreakFlag = aCommBreakFlag;
    }

    public String getIntroBreakFlag()
    {
        if (SysConst.CHANGECHARSET && IntroBreakFlag != null &&
            !IntroBreakFlag.equals(""))
        {
            IntroBreakFlag = StrTool.unicodeToGBK(IntroBreakFlag);
        }
        return IntroBreakFlag;
    }

    public void setIntroBreakFlag(String aIntroBreakFlag)
    {
        IntroBreakFlag = aIntroBreakFlag;
    }

    public String getstartdate()
    {
        if (startdate != null)
        {
            return fDate.getString(startdate);
        }
        else
        {
            return null;
        }
    }

    public void setstartdate(Date astartdate)
    {
        startdate = astartdate;
    }

    public void setstartdate(String astartdate)
    {
        if (astartdate != null && !astartdate.equals(""))
        {
            startdate = fDate.getDate(astartdate);
        }
        else
        {
            startdate = null;
        }
    }

    public String getenddate()
    {
        if (enddate != null)
        {
            return fDate.getString(enddate);
        }
        else
        {
            return null;
        }
    }

    public void setenddate(Date aenddate)
    {
        enddate = aenddate;
    }

    public void setenddate(String aenddate)
    {
        if (aenddate != null && !aenddate.equals(""))
        {
            enddate = fDate.getDate(aenddate);
        }
        else
        {
            enddate = null;
        }
    }

    public String getBackCalFlag()
    {
        if (SysConst.CHANGECHARSET && BackCalFlag != null &&
            !BackCalFlag.equals(""))
        {
            BackCalFlag = StrTool.unicodeToGBK(BackCalFlag);
        }
        return BackCalFlag;
    }

    public void setBackCalFlag(String aBackCalFlag)
    {
        BackCalFlag = aBackCalFlag;
    }

    public String getIndexCalNo()
    {
        if (SysConst.CHANGECHARSET && IndexCalNo != null &&
            !IndexCalNo.equals(""))
        {
            IndexCalNo = StrTool.unicodeToGBK(IndexCalNo);
        }
        return IndexCalNo;
    }

    public void setIndexCalNo(String aIndexCalNo)
    {
        IndexCalNo = aIndexCalNo;
    }

    public String getState()
    {
        if (SysConst.CHANGECHARSET && State != null && !State.equals(""))
        {
            State = StrTool.unicodeToGBK(State);
        }
        return State;
    }

    public void setState(String aState)
    {
        State = aState;
    }

    public String getRearFlag()
    {
        if (SysConst.CHANGECHARSET && RearFlag != null && !RearFlag.equals(""))
        {
            RearFlag = StrTool.unicodeToGBK(RearFlag);
        }
        return RearFlag;
    }

    public void setRearFlag(String aRearFlag)
    {
        RearFlag = aRearFlag;
    }

    public String getFlag()
    {
        if (SysConst.CHANGECHARSET && Flag != null && !Flag.equals(""))
        {
            Flag = StrTool.unicodeToGBK(Flag);
        }
        return Flag;
    }

    public void setFlag(String aFlag)
    {
        Flag = aFlag;
    }

    public String getOperator()
    {
        if (SysConst.CHANGECHARSET && Operator != null && !Operator.equals(""))
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (SysConst.CHANGECHARSET && MakeTime != null && !MakeTime.equals(""))
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (SysConst.CHANGECHARSET && ModifyTime != null &&
            !ModifyTime.equals(""))
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LATreeAccessoryBSchema 对象给 Schema 赋值
     * @param: aLATreeAccessoryBSchema LATreeAccessoryBSchema
     **/
    public void setSchema(LATreeAccessoryBSchema aLATreeAccessoryBSchema)
    {
        this.EdorNo = aLATreeAccessoryBSchema.getEdorNo();
        this.EdorType = aLATreeAccessoryBSchema.getEdorType();
        this.AgentCode = aLATreeAccessoryBSchema.getAgentCode();
        this.AgentGrade = aLATreeAccessoryBSchema.getAgentGrade();
        this.AgentGroup = aLATreeAccessoryBSchema.getAgentGroup();
        this.RearAgentCode = aLATreeAccessoryBSchema.getRearAgentCode();
        this.CommBreakFlag = aLATreeAccessoryBSchema.getCommBreakFlag();
        this.IntroBreakFlag = aLATreeAccessoryBSchema.getIntroBreakFlag();
        this.startdate = fDate.getDate(aLATreeAccessoryBSchema.getstartdate());
        this.enddate = fDate.getDate(aLATreeAccessoryBSchema.getenddate());
        this.BackCalFlag = aLATreeAccessoryBSchema.getBackCalFlag();
        this.IndexCalNo = aLATreeAccessoryBSchema.getIndexCalNo();
        this.State = aLATreeAccessoryBSchema.getState();
        this.RearFlag = aLATreeAccessoryBSchema.getRearFlag();
        this.Flag = aLATreeAccessoryBSchema.getFlag();
        this.Operator = aLATreeAccessoryBSchema.getOperator();
        this.MakeDate = fDate.getDate(aLATreeAccessoryBSchema.getMakeDate());
        this.MakeTime = aLATreeAccessoryBSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLATreeAccessoryBSchema.getModifyDate());
        this.ModifyTime = aLATreeAccessoryBSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("EdorNo") == null)
            {
                this.EdorNo = null;
            }
            else
            {
                this.EdorNo = rs.getString("EdorNo").trim();
            }

            if (rs.getString("EdorType") == null)
            {
                this.EdorType = null;
            }
            else
            {
                this.EdorType = rs.getString("EdorType").trim();
            }

            if (rs.getString("AgentCode") == null)
            {
                this.AgentCode = null;
            }
            else
            {
                this.AgentCode = rs.getString("AgentCode").trim();
            }

            if (rs.getString("AgentGrade") == null)
            {
                this.AgentGrade = null;
            }
            else
            {
                this.AgentGrade = rs.getString("AgentGrade").trim();
            }

            if (rs.getString("AgentGroup") == null)
            {
                this.AgentGroup = null;
            }
            else
            {
                this.AgentGroup = rs.getString("AgentGroup").trim();
            }

            if (rs.getString("RearAgentCode") == null)
            {
                this.RearAgentCode = null;
            }
            else
            {
                this.RearAgentCode = rs.getString("RearAgentCode").trim();
            }

            if (rs.getString("CommBreakFlag") == null)
            {
                this.CommBreakFlag = null;
            }
            else
            {
                this.CommBreakFlag = rs.getString("CommBreakFlag").trim();
            }

            if (rs.getString("IntroBreakFlag") == null)
            {
                this.IntroBreakFlag = null;
            }
            else
            {
                this.IntroBreakFlag = rs.getString("IntroBreakFlag").trim();
            }

            this.startdate = rs.getDate("startdate");
            this.enddate = rs.getDate("enddate");
            if (rs.getString("BackCalFlag") == null)
            {
                this.BackCalFlag = null;
            }
            else
            {
                this.BackCalFlag = rs.getString("BackCalFlag").trim();
            }

            if (rs.getString("IndexCalNo") == null)
            {
                this.IndexCalNo = null;
            }
            else
            {
                this.IndexCalNo = rs.getString("IndexCalNo").trim();
            }

            if (rs.getString("State") == null)
            {
                this.State = null;
            }
            else
            {
                this.State = rs.getString("State").trim();
            }

            if (rs.getString("RearFlag") == null)
            {
                this.RearFlag = null;
            }
            else
            {
                this.RearFlag = rs.getString("RearFlag").trim();
            }

            if (rs.getString("Flag") == null)
            {
                this.Flag = null;
            }
            else
            {
                this.Flag = rs.getString("Flag").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LATreeAccessoryBSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LATreeAccessoryBSchema getSchema()
    {
        LATreeAccessoryBSchema aLATreeAccessoryBSchema = new
                LATreeAccessoryBSchema();
        aLATreeAccessoryBSchema.setSchema(this);
        return aLATreeAccessoryBSchema;
    }

    public LATreeAccessoryBDB getDB()
    {
        LATreeAccessoryBDB aDBOper = new LATreeAccessoryBDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLATreeAccessoryB描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(EdorNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(EdorType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgentCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgentGrade)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgentGroup)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RearAgentCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CommBreakFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(IntroBreakFlag)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(startdate))) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(enddate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(BackCalFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(IndexCalNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(State)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RearFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Flag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLATreeAccessoryB>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            EdorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                    SysConst.PACKAGESPILTER);
            EdorType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                       SysConst.PACKAGESPILTER);
            AgentGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                        SysConst.PACKAGESPILTER);
            AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                        SysConst.PACKAGESPILTER);
            RearAgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                           SysConst.PACKAGESPILTER);
            CommBreakFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                           SysConst.PACKAGESPILTER);
            IntroBreakFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                            SysConst.PACKAGESPILTER);
            startdate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 9, SysConst.PACKAGESPILTER));
            enddate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 10, SysConst.PACKAGESPILTER));
            BackCalFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                         SysConst.PACKAGESPILTER);
            IndexCalNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                        SysConst.PACKAGESPILTER);
            State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                   SysConst.PACKAGESPILTER);
            RearFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                      SysConst.PACKAGESPILTER);
            Flag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                  SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 17, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 19, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LATreeAccessoryBSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("EdorNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorNo));
        }
        if (FCode.equals("EdorType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorType));
        }
        if (FCode.equals("AgentCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equals("AgentGrade"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGrade));
        }
        if (FCode.equals("AgentGroup"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
        }
        if (FCode.equals("RearAgentCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RearAgentCode));
        }
        if (FCode.equals("CommBreakFlag"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CommBreakFlag));
        }
        if (FCode.equals("IntroBreakFlag"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IntroBreakFlag));
        }
        if (FCode.equals("startdate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getstartdate()));
        }
        if (FCode.equals("enddate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getenddate()));
        }
        if (FCode.equals("BackCalFlag"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BackCalFlag));
        }
        if (FCode.equals("IndexCalNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IndexCalNo));
        }
        if (FCode.equals("State"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equals("RearFlag"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RearFlag));
        }
        if (FCode.equals("Flag"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Flag));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(EdorNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(EdorType);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(AgentCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(AgentGrade);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(AgentGroup);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(RearAgentCode);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(CommBreakFlag);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(IntroBreakFlag);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getstartdate()));
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getenddate()));
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(BackCalFlag);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(IndexCalNo);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(State);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(RearFlag);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(Flag);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("EdorNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorNo = FValue.trim();
            }
            else
            {
                EdorNo = null;
            }
        }
        if (FCode.equals("EdorType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorType = FValue.trim();
            }
            else
            {
                EdorType = null;
            }
        }
        if (FCode.equals("AgentCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
            {
                AgentCode = null;
            }
        }
        if (FCode.equals("AgentGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentGrade = FValue.trim();
            }
            else
            {
                AgentGrade = null;
            }
        }
        if (FCode.equals("AgentGroup"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
            {
                AgentGroup = null;
            }
        }
        if (FCode.equals("RearAgentCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RearAgentCode = FValue.trim();
            }
            else
            {
                RearAgentCode = null;
            }
        }
        if (FCode.equals("CommBreakFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CommBreakFlag = FValue.trim();
            }
            else
            {
                CommBreakFlag = null;
            }
        }
        if (FCode.equals("IntroBreakFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                IntroBreakFlag = FValue.trim();
            }
            else
            {
                IntroBreakFlag = null;
            }
        }
        if (FCode.equals("startdate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                startdate = fDate.getDate(FValue);
            }
            else
            {
                startdate = null;
            }
        }
        if (FCode.equals("enddate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                enddate = fDate.getDate(FValue);
            }
            else
            {
                enddate = null;
            }
        }
        if (FCode.equals("BackCalFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BackCalFlag = FValue.trim();
            }
            else
            {
                BackCalFlag = null;
            }
        }
        if (FCode.equals("IndexCalNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                IndexCalNo = FValue.trim();
            }
            else
            {
                IndexCalNo = null;
            }
        }
        if (FCode.equals("State"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
            {
                State = null;
            }
        }
        if (FCode.equals("RearFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RearFlag = FValue.trim();
            }
            else
            {
                RearFlag = null;
            }
        }
        if (FCode.equals("Flag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Flag = FValue.trim();
            }
            else
            {
                Flag = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LATreeAccessoryBSchema other = (LATreeAccessoryBSchema) otherObject;
        return
                EdorNo.equals(other.getEdorNo())
                && EdorType.equals(other.getEdorType())
                && AgentCode.equals(other.getAgentCode())
                && AgentGrade.equals(other.getAgentGrade())
                && AgentGroup.equals(other.getAgentGroup())
                && RearAgentCode.equals(other.getRearAgentCode())
                && CommBreakFlag.equals(other.getCommBreakFlag())
                && IntroBreakFlag.equals(other.getIntroBreakFlag())
                && fDate.getString(startdate).equals(other.getstartdate())
                && fDate.getString(enddate).equals(other.getenddate())
                && BackCalFlag.equals(other.getBackCalFlag())
                && IndexCalNo.equals(other.getIndexCalNo())
                && State.equals(other.getState())
                && RearFlag.equals(other.getRearFlag())
                && Flag.equals(other.getFlag())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("EdorNo"))
        {
            return 0;
        }
        if (strFieldName.equals("EdorType"))
        {
            return 1;
        }
        if (strFieldName.equals("AgentCode"))
        {
            return 2;
        }
        if (strFieldName.equals("AgentGrade"))
        {
            return 3;
        }
        if (strFieldName.equals("AgentGroup"))
        {
            return 4;
        }
        if (strFieldName.equals("RearAgentCode"))
        {
            return 5;
        }
        if (strFieldName.equals("CommBreakFlag"))
        {
            return 6;
        }
        if (strFieldName.equals("IntroBreakFlag"))
        {
            return 7;
        }
        if (strFieldName.equals("startdate"))
        {
            return 8;
        }
        if (strFieldName.equals("enddate"))
        {
            return 9;
        }
        if (strFieldName.equals("BackCalFlag"))
        {
            return 10;
        }
        if (strFieldName.equals("IndexCalNo"))
        {
            return 11;
        }
        if (strFieldName.equals("State"))
        {
            return 12;
        }
        if (strFieldName.equals("RearFlag"))
        {
            return 13;
        }
        if (strFieldName.equals("Flag"))
        {
            return 14;
        }
        if (strFieldName.equals("Operator"))
        {
            return 15;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 16;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 17;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 18;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 19;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "EdorNo";
                break;
            case 1:
                strFieldName = "EdorType";
                break;
            case 2:
                strFieldName = "AgentCode";
                break;
            case 3:
                strFieldName = "AgentGrade";
                break;
            case 4:
                strFieldName = "AgentGroup";
                break;
            case 5:
                strFieldName = "RearAgentCode";
                break;
            case 6:
                strFieldName = "CommBreakFlag";
                break;
            case 7:
                strFieldName = "IntroBreakFlag";
                break;
            case 8:
                strFieldName = "startdate";
                break;
            case 9:
                strFieldName = "enddate";
                break;
            case 10:
                strFieldName = "BackCalFlag";
                break;
            case 11:
                strFieldName = "IndexCalNo";
                break;
            case 12:
                strFieldName = "State";
                break;
            case 13:
                strFieldName = "RearFlag";
                break;
            case 14:
                strFieldName = "Flag";
                break;
            case 15:
                strFieldName = "Operator";
                break;
            case 16:
                strFieldName = "MakeDate";
                break;
            case 17:
                strFieldName = "MakeTime";
                break;
            case 18:
                strFieldName = "ModifyDate";
                break;
            case 19:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("EdorNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EdorType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentGrade"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentGroup"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RearAgentCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CommBreakFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("IntroBreakFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("startdate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("enddate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("BackCalFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("IndexCalNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("State"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RearFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Flag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 9:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 17:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 18:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 19:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
