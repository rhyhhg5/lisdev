/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.ITImportDataCatchDB;

/*
 * <p>ClassName: ITImportDataCatchSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2011-12-21
 */
public class ITImportDataCatchSchema implements Schema, Cloneable
{
	// @Field
	/** Telementname */
	private String TELEMENTNAME;
	/** Seqno */
	private int SEQNO;
	/** Validflag */
	private String VALIDFLAG;
	/** Calsql1 */
	private String CALSQL1;
	/** Calsql2 */
	private String CALSQL2;
	/** Calsql3 */
	private String CALSQL3;
	/** Calsql4 */
	private String CALSQL4;
	/** Operator */
	private String OPERATOR;
	/** Makedate */
	private Date MAKEDATE;
	/** Maketime */
	private String MAKETIME;
	/** Modifydate */
	private Date MODIFYDATE;
	/** Modifytime */
	private String MODIFYTIME;
	/** Managecom */
	private String MANAGECOM;
	/** Calcondition */
	private String CALCONDITION;

	public static final int FIELDNUM = 14;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public ITImportDataCatchSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "TELEMENTNAME";
		pk[1] = "SEQNO";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		ITImportDataCatchSchema cloned = (ITImportDataCatchSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getTELEMENTNAME()
	{
		return TELEMENTNAME;
	}
	public void setTELEMENTNAME(String aTELEMENTNAME)
	{
		TELEMENTNAME = aTELEMENTNAME;
	}
	public int getSEQNO()
	{
		return SEQNO;
	}
	public void setSEQNO(int aSEQNO)
	{
		SEQNO = aSEQNO;
	}
	public void setSEQNO(String aSEQNO)
	{
		if (aSEQNO != null && !aSEQNO.equals(""))
		{
			Integer tInteger = new Integer(aSEQNO);
			int i = tInteger.intValue();
			SEQNO = i;
		}
	}

	public String getVALIDFLAG()
	{
		return VALIDFLAG;
	}
	public void setVALIDFLAG(String aVALIDFLAG)
	{
		VALIDFLAG = aVALIDFLAG;
	}
	public String getCALSQL1()
	{
		return CALSQL1;
	}
	public void setCALSQL1(String aCALSQL1)
	{
		CALSQL1 = aCALSQL1;
	}
	public String getCALSQL2()
	{
		return CALSQL2;
	}
	public void setCALSQL2(String aCALSQL2)
	{
		CALSQL2 = aCALSQL2;
	}
	public String getCALSQL3()
	{
		return CALSQL3;
	}
	public void setCALSQL3(String aCALSQL3)
	{
		CALSQL3 = aCALSQL3;
	}
	public String getCALSQL4()
	{
		return CALSQL4;
	}
	public void setCALSQL4(String aCALSQL4)
	{
		CALSQL4 = aCALSQL4;
	}
	public String getOPERATOR()
	{
		return OPERATOR;
	}
	public void setOPERATOR(String aOPERATOR)
	{
		OPERATOR = aOPERATOR;
	}
	public String getMAKEDATE()
	{
		if( MAKEDATE != null )
			return fDate.getString(MAKEDATE);
		else
			return null;
	}
	public void setMAKEDATE(Date aMAKEDATE)
	{
		MAKEDATE = aMAKEDATE;
	}
	public void setMAKEDATE(String aMAKEDATE)
	{
		if (aMAKEDATE != null && !aMAKEDATE.equals("") )
		{
			MAKEDATE = fDate.getDate( aMAKEDATE );
		}
		else
			MAKEDATE = null;
	}

	public String getMAKETIME()
	{
		return MAKETIME;
	}
	public void setMAKETIME(String aMAKETIME)
	{
		MAKETIME = aMAKETIME;
	}
	public String getMODIFYDATE()
	{
		if( MODIFYDATE != null )
			return fDate.getString(MODIFYDATE);
		else
			return null;
	}
	public void setMODIFYDATE(Date aMODIFYDATE)
	{
		MODIFYDATE = aMODIFYDATE;
	}
	public void setMODIFYDATE(String aMODIFYDATE)
	{
		if (aMODIFYDATE != null && !aMODIFYDATE.equals("") )
		{
			MODIFYDATE = fDate.getDate( aMODIFYDATE );
		}
		else
			MODIFYDATE = null;
	}

	public String getMODIFYTIME()
	{
		return MODIFYTIME;
	}
	public void setMODIFYTIME(String aMODIFYTIME)
	{
		MODIFYTIME = aMODIFYTIME;
	}
	public String getMANAGECOM()
	{
		return MANAGECOM;
	}
	public void setMANAGECOM(String aMANAGECOM)
	{
		MANAGECOM = aMANAGECOM;
	}
	public String getCALCONDITION()
	{
		return CALCONDITION;
	}
	public void setCALCONDITION(String aCALCONDITION)
	{
		CALCONDITION = aCALCONDITION;
	}

	/**
	* 使用另外一个 ITImportDataCatchSchema 对象给 Schema 赋值
	* @param: aITImportDataCatchSchema ITImportDataCatchSchema
	**/
	public void setSchema(ITImportDataCatchSchema aITImportDataCatchSchema)
	{
		this.TELEMENTNAME = aITImportDataCatchSchema.getTELEMENTNAME();
		this.SEQNO = aITImportDataCatchSchema.getSEQNO();
		this.VALIDFLAG = aITImportDataCatchSchema.getVALIDFLAG();
		this.CALSQL1 = aITImportDataCatchSchema.getCALSQL1();
		this.CALSQL2 = aITImportDataCatchSchema.getCALSQL2();
		this.CALSQL3 = aITImportDataCatchSchema.getCALSQL3();
		this.CALSQL4 = aITImportDataCatchSchema.getCALSQL4();
		this.OPERATOR = aITImportDataCatchSchema.getOPERATOR();
		this.MAKEDATE = fDate.getDate( aITImportDataCatchSchema.getMAKEDATE());
		this.MAKETIME = aITImportDataCatchSchema.getMAKETIME();
		this.MODIFYDATE = fDate.getDate( aITImportDataCatchSchema.getMODIFYDATE());
		this.MODIFYTIME = aITImportDataCatchSchema.getMODIFYTIME();
		this.MANAGECOM = aITImportDataCatchSchema.getMANAGECOM();
		this.CALCONDITION = aITImportDataCatchSchema.getCALCONDITION();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("TELEMENTNAME") == null )
				this.TELEMENTNAME = null;
			else
				this.TELEMENTNAME = rs.getString("TELEMENTNAME").trim();

			this.SEQNO = rs.getInt("SEQNO");
			if( rs.getString("VALIDFLAG") == null )
				this.VALIDFLAG = null;
			else
				this.VALIDFLAG = rs.getString("VALIDFLAG").trim();

			if( rs.getString("CALSQL1") == null )
				this.CALSQL1 = null;
			else
				this.CALSQL1 = rs.getString("CALSQL1").trim();

			if( rs.getString("CALSQL2") == null )
				this.CALSQL2 = null;
			else
				this.CALSQL2 = rs.getString("CALSQL2").trim();

			if( rs.getString("CALSQL3") == null )
				this.CALSQL3 = null;
			else
				this.CALSQL3 = rs.getString("CALSQL3").trim();

			if( rs.getString("CALSQL4") == null )
				this.CALSQL4 = null;
			else
				this.CALSQL4 = rs.getString("CALSQL4").trim();

			if( rs.getString("OPERATOR") == null )
				this.OPERATOR = null;
			else
				this.OPERATOR = rs.getString("OPERATOR").trim();

			this.MAKEDATE = rs.getDate("MAKEDATE");
			if( rs.getString("MAKETIME") == null )
				this.MAKETIME = null;
			else
				this.MAKETIME = rs.getString("MAKETIME").trim();

			this.MODIFYDATE = rs.getDate("MODIFYDATE");
			if( rs.getString("MODIFYTIME") == null )
				this.MODIFYTIME = null;
			else
				this.MODIFYTIME = rs.getString("MODIFYTIME").trim();

			if( rs.getString("MANAGECOM") == null )
				this.MANAGECOM = null;
			else
				this.MANAGECOM = rs.getString("MANAGECOM").trim();

			if( rs.getString("CALCONDITION") == null )
				this.CALCONDITION = null;
			else
				this.CALCONDITION = rs.getString("CALCONDITION").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的ITImportDataCatch表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ITImportDataCatchSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public ITImportDataCatchSchema getSchema()
	{
		ITImportDataCatchSchema aITImportDataCatchSchema = new ITImportDataCatchSchema();
		aITImportDataCatchSchema.setSchema(this);
		return aITImportDataCatchSchema;
	}

	public ITImportDataCatchDB getDB()
	{
		ITImportDataCatchDB aDBOper = new ITImportDataCatchDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpITImportDataCatch描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(TELEMENTNAME)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SEQNO));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(VALIDFLAG)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CALSQL1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CALSQL2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CALSQL3)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CALSQL4)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OPERATOR)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MAKEDATE ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MAKETIME)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MODIFYDATE ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MODIFYTIME)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MANAGECOM)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CALCONDITION));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpITImportDataCatch>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			TELEMENTNAME = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			SEQNO= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,2,SysConst.PACKAGESPILTER))).intValue();
			VALIDFLAG = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			CALSQL1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			CALSQL2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			CALSQL3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			CALSQL4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			OPERATOR = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			MAKEDATE = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,SysConst.PACKAGESPILTER));
			MAKETIME = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			MODIFYDATE = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
			MODIFYTIME = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			MANAGECOM = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			CALCONDITION = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ITImportDataCatchSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("TELEMENTNAME"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TELEMENTNAME));
		}
		if (FCode.equals("SEQNO"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SEQNO));
		}
		if (FCode.equals("VALIDFLAG"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(VALIDFLAG));
		}
		if (FCode.equals("CALSQL1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CALSQL1));
		}
		if (FCode.equals("CALSQL2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CALSQL2));
		}
		if (FCode.equals("CALSQL3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CALSQL3));
		}
		if (FCode.equals("CALSQL4"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CALSQL4));
		}
		if (FCode.equals("OPERATOR"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OPERATOR));
		}
		if (FCode.equals("MAKEDATE"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMAKEDATE()));
		}
		if (FCode.equals("MAKETIME"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MAKETIME));
		}
		if (FCode.equals("MODIFYDATE"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMODIFYDATE()));
		}
		if (FCode.equals("MODIFYTIME"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MODIFYTIME));
		}
		if (FCode.equals("MANAGECOM"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MANAGECOM));
		}
		if (FCode.equals("CALCONDITION"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CALCONDITION));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(TELEMENTNAME);
				break;
			case 1:
				strFieldValue = String.valueOf(SEQNO);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(VALIDFLAG);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(CALSQL1);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(CALSQL2);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(CALSQL3);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(CALSQL4);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(OPERATOR);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMAKEDATE()));
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(MAKETIME);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMODIFYDATE()));
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(MODIFYTIME);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(MANAGECOM);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(CALCONDITION);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("TELEMENTNAME"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TELEMENTNAME = FValue.trim();
			}
			else
				TELEMENTNAME = null;
		}
		if (FCode.equalsIgnoreCase("SEQNO"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				SEQNO = i;
			}
		}
		if (FCode.equalsIgnoreCase("VALIDFLAG"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				VALIDFLAG = FValue.trim();
			}
			else
				VALIDFLAG = null;
		}
		if (FCode.equalsIgnoreCase("CALSQL1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CALSQL1 = FValue.trim();
			}
			else
				CALSQL1 = null;
		}
		if (FCode.equalsIgnoreCase("CALSQL2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CALSQL2 = FValue.trim();
			}
			else
				CALSQL2 = null;
		}
		if (FCode.equalsIgnoreCase("CALSQL3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CALSQL3 = FValue.trim();
			}
			else
				CALSQL3 = null;
		}
		if (FCode.equalsIgnoreCase("CALSQL4"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CALSQL4 = FValue.trim();
			}
			else
				CALSQL4 = null;
		}
		if (FCode.equalsIgnoreCase("OPERATOR"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OPERATOR = FValue.trim();
			}
			else
				OPERATOR = null;
		}
		if (FCode.equalsIgnoreCase("MAKEDATE"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MAKEDATE = fDate.getDate( FValue );
			}
			else
				MAKEDATE = null;
		}
		if (FCode.equalsIgnoreCase("MAKETIME"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MAKETIME = FValue.trim();
			}
			else
				MAKETIME = null;
		}
		if (FCode.equalsIgnoreCase("MODIFYDATE"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MODIFYDATE = fDate.getDate( FValue );
			}
			else
				MODIFYDATE = null;
		}
		if (FCode.equalsIgnoreCase("MODIFYTIME"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MODIFYTIME = FValue.trim();
			}
			else
				MODIFYTIME = null;
		}
		if (FCode.equalsIgnoreCase("MANAGECOM"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MANAGECOM = FValue.trim();
			}
			else
				MANAGECOM = null;
		}
		if (FCode.equalsIgnoreCase("CALCONDITION"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CALCONDITION = FValue.trim();
			}
			else
				CALCONDITION = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		ITImportDataCatchSchema other = (ITImportDataCatchSchema)otherObject;
		return
			(TELEMENTNAME == null ? other.getTELEMENTNAME() == null : TELEMENTNAME.equals(other.getTELEMENTNAME()))
			&& SEQNO == other.getSEQNO()
			&& (VALIDFLAG == null ? other.getVALIDFLAG() == null : VALIDFLAG.equals(other.getVALIDFLAG()))
			&& (CALSQL1 == null ? other.getCALSQL1() == null : CALSQL1.equals(other.getCALSQL1()))
			&& (CALSQL2 == null ? other.getCALSQL2() == null : CALSQL2.equals(other.getCALSQL2()))
			&& (CALSQL3 == null ? other.getCALSQL3() == null : CALSQL3.equals(other.getCALSQL3()))
			&& (CALSQL4 == null ? other.getCALSQL4() == null : CALSQL4.equals(other.getCALSQL4()))
			&& (OPERATOR == null ? other.getOPERATOR() == null : OPERATOR.equals(other.getOPERATOR()))
			&& (MAKEDATE == null ? other.getMAKEDATE() == null : fDate.getString(MAKEDATE).equals(other.getMAKEDATE()))
			&& (MAKETIME == null ? other.getMAKETIME() == null : MAKETIME.equals(other.getMAKETIME()))
			&& (MODIFYDATE == null ? other.getMODIFYDATE() == null : fDate.getString(MODIFYDATE).equals(other.getMODIFYDATE()))
			&& (MODIFYTIME == null ? other.getMODIFYTIME() == null : MODIFYTIME.equals(other.getMODIFYTIME()))
			&& (MANAGECOM == null ? other.getMANAGECOM() == null : MANAGECOM.equals(other.getMANAGECOM()))
			&& (CALCONDITION == null ? other.getCALCONDITION() == null : CALCONDITION.equals(other.getCALCONDITION()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("TELEMENTNAME") ) {
			return 0;
		}
		if( strFieldName.equals("SEQNO") ) {
			return 1;
		}
		if( strFieldName.equals("VALIDFLAG") ) {
			return 2;
		}
		if( strFieldName.equals("CALSQL1") ) {
			return 3;
		}
		if( strFieldName.equals("CALSQL2") ) {
			return 4;
		}
		if( strFieldName.equals("CALSQL3") ) {
			return 5;
		}
		if( strFieldName.equals("CALSQL4") ) {
			return 6;
		}
		if( strFieldName.equals("OPERATOR") ) {
			return 7;
		}
		if( strFieldName.equals("MAKEDATE") ) {
			return 8;
		}
		if( strFieldName.equals("MAKETIME") ) {
			return 9;
		}
		if( strFieldName.equals("MODIFYDATE") ) {
			return 10;
		}
		if( strFieldName.equals("MODIFYTIME") ) {
			return 11;
		}
		if( strFieldName.equals("MANAGECOM") ) {
			return 12;
		}
		if( strFieldName.equals("CALCONDITION") ) {
			return 13;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "TELEMENTNAME";
				break;
			case 1:
				strFieldName = "SEQNO";
				break;
			case 2:
				strFieldName = "VALIDFLAG";
				break;
			case 3:
				strFieldName = "CALSQL1";
				break;
			case 4:
				strFieldName = "CALSQL2";
				break;
			case 5:
				strFieldName = "CALSQL3";
				break;
			case 6:
				strFieldName = "CALSQL4";
				break;
			case 7:
				strFieldName = "OPERATOR";
				break;
			case 8:
				strFieldName = "MAKEDATE";
				break;
			case 9:
				strFieldName = "MAKETIME";
				break;
			case 10:
				strFieldName = "MODIFYDATE";
				break;
			case 11:
				strFieldName = "MODIFYTIME";
				break;
			case 12:
				strFieldName = "MANAGECOM";
				break;
			case 13:
				strFieldName = "CALCONDITION";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("TELEMENTNAME") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SEQNO") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("VALIDFLAG") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CALSQL1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CALSQL2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CALSQL3") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CALSQL4") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OPERATOR") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MAKEDATE") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MAKETIME") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MODIFYDATE") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MODIFYTIME") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MANAGECOM") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CALCONDITION") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_INT;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
