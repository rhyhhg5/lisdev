/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LCContTranferOutRegisterQueryDB;

/*
 * <p>ClassName: LCContTranferOutRegisterQuerySchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 税优
 * @CreateDate：2015-10-20
 */
public class LCContTranferOutRegisterQuerySchema implements Schema, Cloneable
{
	// @Field
	/** 流水号 */
	private String SerNo;
	/** 转出保单号 */
	private String PolicyNo;
	/** 转出分单号 */
	private String SequenceNo;
	/** 转出登记日期 */
	private String RegisterDate;
	/** 预计终止日期 */
	private Date ExpectedTerminiateDate;
	/** 无法转出原因 */
	private String RejectReson;
	/** 转出公司联系人 */
	private String ContactName;
	/** 电话 */
	private String ContactTele;
	/** Email */
	private String ContactEmail;
	/** 保单转出登记平台接收日期 */
	private Date TransReceivDate;
	/** 响应状态 */
	private String ResponseCode;
	/** 结果状态 */
	private String ResultStatus;
	/** 结果信息 */
	private String ResultInfoDesc;
	/** Ciitc状态 */
	private String CiitcResultCode;
	/** Ciitc信息 */
	private String CiitcResultMessage;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 修改日期 */
	private Date ModifyDate;
	/** 修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 19;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LCContTranferOutRegisterQuerySchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "SerNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LCContTranferOutRegisterQuerySchema cloned = (LCContTranferOutRegisterQuerySchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getSerNo()
	{
		return SerNo;
	}
	public void setSerNo(String aSerNo)
	{
		SerNo = aSerNo;
	}
	public String getPolicyNo()
	{
		return PolicyNo;
	}
	public void setPolicyNo(String aPolicyNo)
	{
		PolicyNo = aPolicyNo;
	}
	public String getSequenceNo()
	{
		return SequenceNo;
	}
	public void setSequenceNo(String aSequenceNo)
	{
		SequenceNo = aSequenceNo;
	}
	public String getRegisterDate()
	{
		return RegisterDate;
	}
	public void setRegisterDate(String aRegisterDate)
	{
		RegisterDate = aRegisterDate;
	}
	public String getExpectedTerminiateDate()
	{
		if( ExpectedTerminiateDate != null )
			return fDate.getString(ExpectedTerminiateDate);
		else
			return null;
	}
	public void setExpectedTerminiateDate(Date aExpectedTerminiateDate)
	{
		ExpectedTerminiateDate = aExpectedTerminiateDate;
	}
	public void setExpectedTerminiateDate(String aExpectedTerminiateDate)
	{
		if (aExpectedTerminiateDate != null && !aExpectedTerminiateDate.equals("") )
		{
			ExpectedTerminiateDate = fDate.getDate( aExpectedTerminiateDate );
		}
		else
			ExpectedTerminiateDate = null;
	}

	public String getRejectReson()
	{
		return RejectReson;
	}
	public void setRejectReson(String aRejectReson)
	{
		RejectReson = aRejectReson;
	}
	public String getContactName()
	{
		return ContactName;
	}
	public void setContactName(String aContactName)
	{
		ContactName = aContactName;
	}
	public String getContactTele()
	{
		return ContactTele;
	}
	public void setContactTele(String aContactTele)
	{
		ContactTele = aContactTele;
	}
	public String getContactEmail()
	{
		return ContactEmail;
	}
	public void setContactEmail(String aContactEmail)
	{
		ContactEmail = aContactEmail;
	}
	public String getTransReceivDate()
	{
		if( TransReceivDate != null )
			return fDate.getString(TransReceivDate);
		else
			return null;
	}
	public void setTransReceivDate(Date aTransReceivDate)
	{
		TransReceivDate = aTransReceivDate;
	}
	public void setTransReceivDate(String aTransReceivDate)
	{
		if (aTransReceivDate != null && !aTransReceivDate.equals("") )
		{
			TransReceivDate = fDate.getDate( aTransReceivDate );
		}
		else
			TransReceivDate = null;
	}

	public String getResponseCode()
	{
		return ResponseCode;
	}
	public void setResponseCode(String aResponseCode)
	{
		ResponseCode = aResponseCode;
	}
	public String getResultStatus()
	{
		return ResultStatus;
	}
	public void setResultStatus(String aResultStatus)
	{
		ResultStatus = aResultStatus;
	}
	public String getResultInfoDesc()
	{
		return ResultInfoDesc;
	}
	public void setResultInfoDesc(String aResultInfoDesc)
	{
		ResultInfoDesc = aResultInfoDesc;
	}
	public String getCiitcResultCode()
	{
		return CiitcResultCode;
	}
	public void setCiitcResultCode(String aCiitcResultCode)
	{
		CiitcResultCode = aCiitcResultCode;
	}
	public String getCiitcResultMessage()
	{
		return CiitcResultMessage;
	}
	public void setCiitcResultMessage(String aCiitcResultMessage)
	{
		CiitcResultMessage = aCiitcResultMessage;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LCContTranferOutRegisterQuerySchema 对象给 Schema 赋值
	* @param: aLCContTranferOutRegisterQuerySchema LCContTranferOutRegisterQuerySchema
	**/
	public void setSchema(LCContTranferOutRegisterQuerySchema aLCContTranferOutRegisterQuerySchema)
	{
		this.SerNo = aLCContTranferOutRegisterQuerySchema.getSerNo();
		this.PolicyNo = aLCContTranferOutRegisterQuerySchema.getPolicyNo();
		this.SequenceNo = aLCContTranferOutRegisterQuerySchema.getSequenceNo();
		this.RegisterDate = aLCContTranferOutRegisterQuerySchema.getRegisterDate();
		this.ExpectedTerminiateDate = fDate.getDate( aLCContTranferOutRegisterQuerySchema.getExpectedTerminiateDate());
		this.RejectReson = aLCContTranferOutRegisterQuerySchema.getRejectReson();
		this.ContactName = aLCContTranferOutRegisterQuerySchema.getContactName();
		this.ContactTele = aLCContTranferOutRegisterQuerySchema.getContactTele();
		this.ContactEmail = aLCContTranferOutRegisterQuerySchema.getContactEmail();
		this.TransReceivDate = fDate.getDate( aLCContTranferOutRegisterQuerySchema.getTransReceivDate());
		this.ResponseCode = aLCContTranferOutRegisterQuerySchema.getResponseCode();
		this.ResultStatus = aLCContTranferOutRegisterQuerySchema.getResultStatus();
		this.ResultInfoDesc = aLCContTranferOutRegisterQuerySchema.getResultInfoDesc();
		this.CiitcResultCode = aLCContTranferOutRegisterQuerySchema.getCiitcResultCode();
		this.CiitcResultMessage = aLCContTranferOutRegisterQuerySchema.getCiitcResultMessage();
		this.MakeDate = fDate.getDate( aLCContTranferOutRegisterQuerySchema.getMakeDate());
		this.MakeTime = aLCContTranferOutRegisterQuerySchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLCContTranferOutRegisterQuerySchema.getModifyDate());
		this.ModifyTime = aLCContTranferOutRegisterQuerySchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("SerNo") == null )
				this.SerNo = null;
			else
				this.SerNo = rs.getString("SerNo").trim();

			if( rs.getString("PolicyNo") == null )
				this.PolicyNo = null;
			else
				this.PolicyNo = rs.getString("PolicyNo").trim();

			if( rs.getString("SequenceNo") == null )
				this.SequenceNo = null;
			else
				this.SequenceNo = rs.getString("SequenceNo").trim();

			if( rs.getString("RegisterDate") == null )
				this.RegisterDate = null;
			else
				this.RegisterDate = rs.getString("RegisterDate").trim();

			this.ExpectedTerminiateDate = rs.getDate("ExpectedTerminiateDate");
			if( rs.getString("RejectReson") == null )
				this.RejectReson = null;
			else
				this.RejectReson = rs.getString("RejectReson").trim();

			if( rs.getString("ContactName") == null )
				this.ContactName = null;
			else
				this.ContactName = rs.getString("ContactName").trim();

			if( rs.getString("ContactTele") == null )
				this.ContactTele = null;
			else
				this.ContactTele = rs.getString("ContactTele").trim();

			if( rs.getString("ContactEmail") == null )
				this.ContactEmail = null;
			else
				this.ContactEmail = rs.getString("ContactEmail").trim();

			this.TransReceivDate = rs.getDate("TransReceivDate");
			if( rs.getString("ResponseCode") == null )
				this.ResponseCode = null;
			else
				this.ResponseCode = rs.getString("ResponseCode").trim();

			if( rs.getString("ResultStatus") == null )
				this.ResultStatus = null;
			else
				this.ResultStatus = rs.getString("ResultStatus").trim();

			if( rs.getString("ResultInfoDesc") == null )
				this.ResultInfoDesc = null;
			else
				this.ResultInfoDesc = rs.getString("ResultInfoDesc").trim();

			if( rs.getString("CiitcResultCode") == null )
				this.CiitcResultCode = null;
			else
				this.CiitcResultCode = rs.getString("CiitcResultCode").trim();

			if( rs.getString("CiitcResultMessage") == null )
				this.CiitcResultMessage = null;
			else
				this.CiitcResultMessage = rs.getString("CiitcResultMessage").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LCContTranferOutRegisterQuery表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCContTranferOutRegisterQuerySchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LCContTranferOutRegisterQuerySchema getSchema()
	{
		LCContTranferOutRegisterQuerySchema aLCContTranferOutRegisterQuerySchema = new LCContTranferOutRegisterQuerySchema();
		aLCContTranferOutRegisterQuerySchema.setSchema(this);
		return aLCContTranferOutRegisterQuerySchema;
	}

	public LCContTranferOutRegisterQueryDB getDB()
	{
		LCContTranferOutRegisterQueryDB aDBOper = new LCContTranferOutRegisterQueryDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCContTranferOutRegisterQuery描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(SerNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PolicyNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SequenceNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RegisterDate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ExpectedTerminiateDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RejectReson)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContactName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContactTele)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContactEmail)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( TransReceivDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ResponseCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ResultStatus)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ResultInfoDesc)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CiitcResultCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CiitcResultMessage)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCContTranferOutRegisterQuery>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			SerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			PolicyNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			SequenceNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			RegisterDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			ExpectedTerminiateDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,SysConst.PACKAGESPILTER));
			RejectReson = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			ContactName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			ContactTele = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			ContactEmail = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			TransReceivDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,SysConst.PACKAGESPILTER));
			ResponseCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			ResultStatus = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			ResultInfoDesc = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			CiitcResultCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			CiitcResultMessage = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCContTranferOutRegisterQuerySchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("SerNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SerNo));
		}
		if (FCode.equals("PolicyNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolicyNo));
		}
		if (FCode.equals("SequenceNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SequenceNo));
		}
		if (FCode.equals("RegisterDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RegisterDate));
		}
		if (FCode.equals("ExpectedTerminiateDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getExpectedTerminiateDate()));
		}
		if (FCode.equals("RejectReson"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RejectReson));
		}
		if (FCode.equals("ContactName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContactName));
		}
		if (FCode.equals("ContactTele"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContactTele));
		}
		if (FCode.equals("ContactEmail"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContactEmail));
		}
		if (FCode.equals("TransReceivDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getTransReceivDate()));
		}
		if (FCode.equals("ResponseCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ResponseCode));
		}
		if (FCode.equals("ResultStatus"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ResultStatus));
		}
		if (FCode.equals("ResultInfoDesc"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ResultInfoDesc));
		}
		if (FCode.equals("CiitcResultCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CiitcResultCode));
		}
		if (FCode.equals("CiitcResultMessage"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CiitcResultMessage));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(SerNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(PolicyNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(SequenceNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(RegisterDate);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getExpectedTerminiateDate()));
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(RejectReson);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(ContactName);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(ContactTele);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(ContactEmail);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getTransReceivDate()));
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(ResponseCode);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(ResultStatus);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(ResultInfoDesc);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(CiitcResultCode);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(CiitcResultMessage);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("SerNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SerNo = FValue.trim();
			}
			else
				SerNo = null;
		}
		if (FCode.equalsIgnoreCase("PolicyNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PolicyNo = FValue.trim();
			}
			else
				PolicyNo = null;
		}
		if (FCode.equalsIgnoreCase("SequenceNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SequenceNo = FValue.trim();
			}
			else
				SequenceNo = null;
		}
		if (FCode.equalsIgnoreCase("RegisterDate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RegisterDate = FValue.trim();
			}
			else
				RegisterDate = null;
		}
		if (FCode.equalsIgnoreCase("ExpectedTerminiateDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ExpectedTerminiateDate = fDate.getDate( FValue );
			}
			else
				ExpectedTerminiateDate = null;
		}
		if (FCode.equalsIgnoreCase("RejectReson"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RejectReson = FValue.trim();
			}
			else
				RejectReson = null;
		}
		if (FCode.equalsIgnoreCase("ContactName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContactName = FValue.trim();
			}
			else
				ContactName = null;
		}
		if (FCode.equalsIgnoreCase("ContactTele"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContactTele = FValue.trim();
			}
			else
				ContactTele = null;
		}
		if (FCode.equalsIgnoreCase("ContactEmail"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContactEmail = FValue.trim();
			}
			else
				ContactEmail = null;
		}
		if (FCode.equalsIgnoreCase("TransReceivDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				TransReceivDate = fDate.getDate( FValue );
			}
			else
				TransReceivDate = null;
		}
		if (FCode.equalsIgnoreCase("ResponseCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ResponseCode = FValue.trim();
			}
			else
				ResponseCode = null;
		}
		if (FCode.equalsIgnoreCase("ResultStatus"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ResultStatus = FValue.trim();
			}
			else
				ResultStatus = null;
		}
		if (FCode.equalsIgnoreCase("ResultInfoDesc"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ResultInfoDesc = FValue.trim();
			}
			else
				ResultInfoDesc = null;
		}
		if (FCode.equalsIgnoreCase("CiitcResultCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CiitcResultCode = FValue.trim();
			}
			else
				CiitcResultCode = null;
		}
		if (FCode.equalsIgnoreCase("CiitcResultMessage"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CiitcResultMessage = FValue.trim();
			}
			else
				CiitcResultMessage = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LCContTranferOutRegisterQuerySchema other = (LCContTranferOutRegisterQuerySchema)otherObject;
		return
			(SerNo == null ? other.getSerNo() == null : SerNo.equals(other.getSerNo()))
			&& (PolicyNo == null ? other.getPolicyNo() == null : PolicyNo.equals(other.getPolicyNo()))
			&& (SequenceNo == null ? other.getSequenceNo() == null : SequenceNo.equals(other.getSequenceNo()))
			&& (RegisterDate == null ? other.getRegisterDate() == null : RegisterDate.equals(other.getRegisterDate()))
			&& (ExpectedTerminiateDate == null ? other.getExpectedTerminiateDate() == null : fDate.getString(ExpectedTerminiateDate).equals(other.getExpectedTerminiateDate()))
			&& (RejectReson == null ? other.getRejectReson() == null : RejectReson.equals(other.getRejectReson()))
			&& (ContactName == null ? other.getContactName() == null : ContactName.equals(other.getContactName()))
			&& (ContactTele == null ? other.getContactTele() == null : ContactTele.equals(other.getContactTele()))
			&& (ContactEmail == null ? other.getContactEmail() == null : ContactEmail.equals(other.getContactEmail()))
			&& (TransReceivDate == null ? other.getTransReceivDate() == null : fDate.getString(TransReceivDate).equals(other.getTransReceivDate()))
			&& (ResponseCode == null ? other.getResponseCode() == null : ResponseCode.equals(other.getResponseCode()))
			&& (ResultStatus == null ? other.getResultStatus() == null : ResultStatus.equals(other.getResultStatus()))
			&& (ResultInfoDesc == null ? other.getResultInfoDesc() == null : ResultInfoDesc.equals(other.getResultInfoDesc()))
			&& (CiitcResultCode == null ? other.getCiitcResultCode() == null : CiitcResultCode.equals(other.getCiitcResultCode()))
			&& (CiitcResultMessage == null ? other.getCiitcResultMessage() == null : CiitcResultMessage.equals(other.getCiitcResultMessage()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("SerNo") ) {
			return 0;
		}
		if( strFieldName.equals("PolicyNo") ) {
			return 1;
		}
		if( strFieldName.equals("SequenceNo") ) {
			return 2;
		}
		if( strFieldName.equals("RegisterDate") ) {
			return 3;
		}
		if( strFieldName.equals("ExpectedTerminiateDate") ) {
			return 4;
		}
		if( strFieldName.equals("RejectReson") ) {
			return 5;
		}
		if( strFieldName.equals("ContactName") ) {
			return 6;
		}
		if( strFieldName.equals("ContactTele") ) {
			return 7;
		}
		if( strFieldName.equals("ContactEmail") ) {
			return 8;
		}
		if( strFieldName.equals("TransReceivDate") ) {
			return 9;
		}
		if( strFieldName.equals("ResponseCode") ) {
			return 10;
		}
		if( strFieldName.equals("ResultStatus") ) {
			return 11;
		}
		if( strFieldName.equals("ResultInfoDesc") ) {
			return 12;
		}
		if( strFieldName.equals("CiitcResultCode") ) {
			return 13;
		}
		if( strFieldName.equals("CiitcResultMessage") ) {
			return 14;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 15;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 16;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 17;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 18;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "SerNo";
				break;
			case 1:
				strFieldName = "PolicyNo";
				break;
			case 2:
				strFieldName = "SequenceNo";
				break;
			case 3:
				strFieldName = "RegisterDate";
				break;
			case 4:
				strFieldName = "ExpectedTerminiateDate";
				break;
			case 5:
				strFieldName = "RejectReson";
				break;
			case 6:
				strFieldName = "ContactName";
				break;
			case 7:
				strFieldName = "ContactTele";
				break;
			case 8:
				strFieldName = "ContactEmail";
				break;
			case 9:
				strFieldName = "TransReceivDate";
				break;
			case 10:
				strFieldName = "ResponseCode";
				break;
			case 11:
				strFieldName = "ResultStatus";
				break;
			case 12:
				strFieldName = "ResultInfoDesc";
				break;
			case 13:
				strFieldName = "CiitcResultCode";
				break;
			case 14:
				strFieldName = "CiitcResultMessage";
				break;
			case 15:
				strFieldName = "MakeDate";
				break;
			case 16:
				strFieldName = "MakeTime";
				break;
			case 17:
				strFieldName = "ModifyDate";
				break;
			case 18:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("SerNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolicyNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SequenceNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RegisterDate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ExpectedTerminiateDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("RejectReson") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContactName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContactTele") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContactEmail") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TransReceivDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ResponseCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ResultStatus") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ResultInfoDesc") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CiitcResultCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CiitcResultMessage") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
