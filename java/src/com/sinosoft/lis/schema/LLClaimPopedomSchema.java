/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LLClaimPopedomDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

/*
 * <p>ClassName: LLClaimPopedomSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-02-28
 */
public class LLClaimPopedomSchema implements Schema
{
    // @Field
    /** 理赔师级别代码 */
    private String ClaimPopedom;
    /** 理赔师权限 */
    private String PopedomName;
    /** 给付类别 */
    private String GetDutyKind;
    /** 给付权限类型 */
    private String GetDutyType;
    /** 给付金额(期数)限额 */
    private double LimitMoney;

    public static final int FIELDNUM = 5; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LLClaimPopedomSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "ClaimPopedom";
        pk[1] = "GetDutyKind";
        pk[2] = "GetDutyType";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getClaimPopedom()
    {
        if (ClaimPopedom != null && !ClaimPopedom.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ClaimPopedom = StrTool.unicodeToGBK(ClaimPopedom);
        }
        return ClaimPopedom;
    }

    public void setClaimPopedom(String aClaimPopedom)
    {
        ClaimPopedom = aClaimPopedom;
    }

    public String getPopedomName()
    {
        if (PopedomName != null && !PopedomName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            PopedomName = StrTool.unicodeToGBK(PopedomName);
        }
        return PopedomName;
    }

    public void setPopedomName(String aPopedomName)
    {
        PopedomName = aPopedomName;
    }

    public String getGetDutyKind()
    {
        if (GetDutyKind != null && !GetDutyKind.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GetDutyKind = StrTool.unicodeToGBK(GetDutyKind);
        }
        return GetDutyKind;
    }

    public void setGetDutyKind(String aGetDutyKind)
    {
        GetDutyKind = aGetDutyKind;
    }

    public String getGetDutyType()
    {
        if (GetDutyType != null && !GetDutyType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GetDutyType = StrTool.unicodeToGBK(GetDutyType);
        }
        return GetDutyType;
    }

    public void setGetDutyType(String aGetDutyType)
    {
        GetDutyType = aGetDutyType;
    }

    public double getLimitMoney()
    {
        return LimitMoney;
    }

    public void setLimitMoney(double aLimitMoney)
    {
        LimitMoney = aLimitMoney;
    }

    public void setLimitMoney(String aLimitMoney)
    {
        if (aLimitMoney != null && !aLimitMoney.equals(""))
        {
            Double tDouble = new Double(aLimitMoney);
            double d = tDouble.doubleValue();
            LimitMoney = d;
        }
    }


    /**
     * 使用另外一个 LLClaimPopedomSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LLClaimPopedomSchema aLLClaimPopedomSchema)
    {
        this.ClaimPopedom = aLLClaimPopedomSchema.getClaimPopedom();
        this.PopedomName = aLLClaimPopedomSchema.getPopedomName();
        this.GetDutyKind = aLLClaimPopedomSchema.getGetDutyKind();
        this.GetDutyType = aLLClaimPopedomSchema.getGetDutyType();
        this.LimitMoney = aLLClaimPopedomSchema.getLimitMoney();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ClaimPopedom") == null)
            {
                this.ClaimPopedom = null;
            }
            else
            {
                this.ClaimPopedom = rs.getString("ClaimPopedom").trim();
            }

            if (rs.getString("PopedomName") == null)
            {
                this.PopedomName = null;
            }
            else
            {
                this.PopedomName = rs.getString("PopedomName").trim();
            }

            if (rs.getString("GetDutyKind") == null)
            {
                this.GetDutyKind = null;
            }
            else
            {
                this.GetDutyKind = rs.getString("GetDutyKind").trim();
            }

            if (rs.getString("GetDutyType") == null)
            {
                this.GetDutyType = null;
            }
            else
            {
                this.GetDutyType = rs.getString("GetDutyType").trim();
            }

            this.LimitMoney = rs.getDouble("LimitMoney");
        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLClaimPopedomSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LLClaimPopedomSchema getSchema()
    {
        LLClaimPopedomSchema aLLClaimPopedomSchema = new LLClaimPopedomSchema();
        aLLClaimPopedomSchema.setSchema(this);
        return aLLClaimPopedomSchema;
    }

    public LLClaimPopedomDB getDB()
    {
        LLClaimPopedomDB aDBOper = new LLClaimPopedomDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLClaimPopedom描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(ClaimPopedom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PopedomName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GetDutyKind)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GetDutyType)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(LimitMoney);
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLClaimPopedom>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            ClaimPopedom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                          SysConst.PACKAGESPILTER);
            PopedomName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                         SysConst.PACKAGESPILTER);
            GetDutyKind = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                         SysConst.PACKAGESPILTER);
            GetDutyType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                         SysConst.PACKAGESPILTER);
            LimitMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 5, SysConst.PACKAGESPILTER))).doubleValue();
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLClaimPopedomSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("ClaimPopedom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ClaimPopedom));
        }
        if (FCode.equals("PopedomName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PopedomName));
        }
        if (FCode.equals("GetDutyKind"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetDutyKind));
        }
        if (FCode.equals("GetDutyType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetDutyType));
        }
        if (FCode.equals("LimitMoney"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(LimitMoney));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ClaimPopedom);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(PopedomName);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(GetDutyKind);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(GetDutyType);
                break;
            case 4:
                strFieldValue = String.valueOf(LimitMoney);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("ClaimPopedom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ClaimPopedom = FValue.trim();
            }
            else
            {
                ClaimPopedom = null;
            }
        }
        if (FCode.equals("PopedomName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PopedomName = FValue.trim();
            }
            else
            {
                PopedomName = null;
            }
        }
        if (FCode.equals("GetDutyKind"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetDutyKind = FValue.trim();
            }
            else
            {
                GetDutyKind = null;
            }
        }
        if (FCode.equals("GetDutyType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetDutyType = FValue.trim();
            }
            else
            {
                GetDutyType = null;
            }
        }
        if (FCode.equals("LimitMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                LimitMoney = d;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LLClaimPopedomSchema other = (LLClaimPopedomSchema) otherObject;
        return
                ClaimPopedom.equals(other.getClaimPopedom())
                && PopedomName.equals(other.getPopedomName())
                && GetDutyKind.equals(other.getGetDutyKind())
                && GetDutyType.equals(other.getGetDutyType())
                && LimitMoney == other.getLimitMoney();
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("ClaimPopedom"))
        {
            return 0;
        }
        if (strFieldName.equals("PopedomName"))
        {
            return 1;
        }
        if (strFieldName.equals("GetDutyKind"))
        {
            return 2;
        }
        if (strFieldName.equals("GetDutyType"))
        {
            return 3;
        }
        if (strFieldName.equals("LimitMoney"))
        {
            return 4;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "ClaimPopedom";
                break;
            case 1:
                strFieldName = "PopedomName";
                break;
            case 2:
                strFieldName = "GetDutyKind";
                break;
            case 3:
                strFieldName = "GetDutyType";
                break;
            case 4:
                strFieldName = "LimitMoney";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("ClaimPopedom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PopedomName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetDutyKind"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetDutyType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("LimitMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
