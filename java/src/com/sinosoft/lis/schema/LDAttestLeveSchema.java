/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LDAttestLeveDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

/*
 * <p>ClassName: LDAttestLeveSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_3
 * @CreateDate：2005-03-03
 */
public class LDAttestLeveSchema implements Schema
{
    // @Field
    /** 认证级别代码 */
    private String AttestLevelCode;
    /** 认证级别 */
    private String AttestLevel;
    /** 认证级别释意 */
    private String AttestExplai;

    public static final int FIELDNUM = 3; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LDAttestLeveSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "AttestLevelCode";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getAttestLevelCode()
    {
        if (AttestLevelCode != null && !AttestLevelCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AttestLevelCode = StrTool.unicodeToGBK(AttestLevelCode);
        }
        return AttestLevelCode;
    }

    public void setAttestLevelCode(String aAttestLevelCode)
    {
        AttestLevelCode = aAttestLevelCode;
    }

    public String getAttestLevel()
    {
        if (AttestLevel != null && !AttestLevel.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AttestLevel = StrTool.unicodeToGBK(AttestLevel);
        }
        return AttestLevel;
    }

    public void setAttestLevel(String aAttestLevel)
    {
        AttestLevel = aAttestLevel;
    }

    public String getAttestExplai()
    {
        if (AttestExplai != null && !AttestExplai.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AttestExplai = StrTool.unicodeToGBK(AttestExplai);
        }
        return AttestExplai;
    }

    public void setAttestExplai(String aAttestExplai)
    {
        AttestExplai = aAttestExplai;
    }

    /**
     * 使用另外一个 LDAttestLeveSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LDAttestLeveSchema aLDAttestLeveSchema)
    {
        this.AttestLevelCode = aLDAttestLeveSchema.getAttestLevelCode();
        this.AttestLevel = aLDAttestLeveSchema.getAttestLevel();
        this.AttestExplai = aLDAttestLeveSchema.getAttestExplai();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("AttestLevelCode") == null)
            {
                this.AttestLevelCode = null;
            }
            else
            {
                this.AttestLevelCode = rs.getString("AttestLevelCode").trim();
            }

            if (rs.getString("AttestLevel") == null)
            {
                this.AttestLevel = null;
            }
            else
            {
                this.AttestLevel = rs.getString("AttestLevel").trim();
            }

            if (rs.getString("AttestExplai") == null)
            {
                this.AttestExplai = null;
            }
            else
            {
                this.AttestExplai = rs.getString("AttestExplai").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDAttestLeveSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LDAttestLeveSchema getSchema()
    {
        LDAttestLeveSchema aLDAttestLeveSchema = new LDAttestLeveSchema();
        aLDAttestLeveSchema.setSchema(this);
        return aLDAttestLeveSchema;
    }

    public LDAttestLeveDB getDB()
    {
        LDAttestLeveDB aDBOper = new LDAttestLeveDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDAttestLeve描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(AttestLevelCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AttestLevel)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AttestExplai));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDAttestLeve>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            AttestLevelCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             1, SysConst.PACKAGESPILTER);
            AttestLevel = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                         SysConst.PACKAGESPILTER);
            AttestExplai = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                          SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDAttestLeveSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("AttestLevelCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AttestLevelCode));
        }
        if (FCode.equals("AttestLevel"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AttestLevel));
        }
        if (FCode.equals("AttestExplai"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AttestExplai));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(AttestLevelCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(AttestLevel);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(AttestExplai);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("AttestLevelCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AttestLevelCode = FValue.trim();
            }
            else
            {
                AttestLevelCode = null;
            }
        }
        if (FCode.equals("AttestLevel"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AttestLevel = FValue.trim();
            }
            else
            {
                AttestLevel = null;
            }
        }
        if (FCode.equals("AttestExplai"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AttestExplai = FValue.trim();
            }
            else
            {
                AttestExplai = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LDAttestLeveSchema other = (LDAttestLeveSchema) otherObject;
        return
                AttestLevelCode.equals(other.getAttestLevelCode())
                && AttestLevel.equals(other.getAttestLevel())
                && AttestExplai.equals(other.getAttestExplai());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("AttestLevelCode"))
        {
            return 0;
        }
        if (strFieldName.equals("AttestLevel"))
        {
            return 1;
        }
        if (strFieldName.equals("AttestExplai"))
        {
            return 2;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "AttestLevelCode";
                break;
            case 1:
                strFieldName = "AttestLevel";
                break;
            case 2:
                strFieldName = "AttestExplai";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("AttestLevelCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AttestLevel"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AttestExplai"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
