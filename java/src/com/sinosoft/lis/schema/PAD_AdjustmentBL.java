package com.sinosoft.lis.schema;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;

public class PAD_AdjustmentBL {
	public CErrors mErrors = new CErrors();
	private VData Result = new VData();
	 /** 往后面传输数据的容器 */
	private VData tVData;
	
	private String printNo,FlagStr,Content,transact,mDOCCODE;
	public SSRS PageId;
	 //数据库查询对象
	public ExeSQL exeSql = new ExeSQL();
	TransferData tTransferData = new TransferData();
	PubSubmit tPubSubmit = new PubSubmit();
	MMap tmap = new MMap();
	private ES_DOC_QC_MAINSchema mes_doc_qc_mainschema=new ES_DOC_QC_MAINSchema();
	private GlobalInput mGlobalInput = new GlobalInput();
	
	public boolean submitData(VData cInputData,String transact) {
		this.tVData = cInputData;
		this.transact=transact;
		//1、获取数据
		if(!getInputData(cInputData)) {
			return false;
		}
		//2、检查数据
		if(!checkData()) {
			return false;
		}
		//3、处理数据
		if(!dealData()) {
			return false;
		}
		return true;
	}


	//获取数据
	private boolean getInputData(VData cInputData) {
		System.out.println("开始获取数据...");
		tTransferData = (TransferData)tVData.getObjectByObjectName("TransferData", 0);
		printNo = (String)tTransferData.getValueByName("prtno");
		transact = (String)tTransferData.getValueByName("transact");
		mDOCCODE = (String)tTransferData.getValueByName("DOCCODE");
		System.out.println("获取到输入的业务号为："+printNo);
		System.out.println("获取到的操作为："+transact);
		System.out.println("获取到的DOCCODE为："+mDOCCODE);
		return true;
	}
	
	//检查数据
	private boolean checkData() {
		System.out.println("开始检查数据...");
		if("UPDATE".equals(transact)) {
			System.out.println("业务形式：更新为最新影像件");
		}
		if("DELETE".equals(transact)) {
			System.out.println("业务形式：删除指定影像件！");
		}
		System.out.println("数据检查完毕...");
		return true;
	}
	
	//业务处理
	private boolean dealData() {
		if("UPDATE".equals(transact)) {
			return update();
		}
		if("DELETE".equals(transact)) {
			return delete();
		}
		return true;
	}
	
	public boolean update(){
		mDOCCODE = (String)tTransferData.getValueByName("DOCCODE");
		/*String sql1 = "update es_doc_pages set = '"+printNo+"' where Docid='"+printNo+"' and subtype='TB27'";
		tmap.put(sql1, "UPDATE");*/
		   
		String sql2 = "update es_doc_relation re set re.BUSSNO='"+printNo+"' ,re.DOCCODE='"+printNo+"' where docid in (select ma.docid from es_doc_main ma where ma.DOCCODE='"+mDOCCODE+"') with ur";
		tmap.put(sql2, "UPDATE");
		  
		String sql3 = "update es_doc_main ma set ma.DOCCODE='"+printNo+"'  where ma.DOCCODE='"+mDOCCODE+"' with ur";
		tmap.put(sql3, "UPDATE");
		   
		Result.clear();
		Result.add(tmap);
		tPubSubmit.submitData(Result, "UPDATE");
		return true;
	   }
	
	//删除处理
	   public boolean delete(){
		   mDOCCODE = (String)tTransferData.getValueByName("DOCCODE");
		   String sql1 = "delete from es_doc_pages where docid in (select ma.docid from es_doc_main ma where ma.DOCCODE='"+mDOCCODE+"') with ur";
		   tmap.put(sql1, "DELETE");
		   
		   String sql2 = "delete from es_doc_relation where docid in (select ma.docid from es_doc_main ma where ma.DOCCODE='"+mDOCCODE+"') with ur";
		   tmap.put(sql2, "DELETE");
		   
		   String sql3 = "delete from es_doc_main ma where ma.DOCCODE='"+mDOCCODE+"' with ur";
		   tmap.put(sql3, "DELETE");
		   
		   Result.clear();
		   Result.add(tmap);
		   if(!tPubSubmit.submitData(Result, "")){
				System.out.println("数据出错");
				FlagStr="Fail";
				Content="处理失败!";
			}else{
				FlagStr="true";
				Content="处理成功!";
			}
		  return true;
	   }
}
