/*
 * <p>ClassName: LCErrSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LCErrDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LCErrSchema implements Schema
{
    // @Field
    /** 错误信息序列号 */
    private String ErrorNo;
    /** 投保单号码 */
    private String ProposalNo;
    /** 保单号码 */
    private String PolNo;
    /** 管理机构 */
    private String ManageCom;
    /** 其它号码 */
    private String OtherNo;
    /** 其它号码类型 */
    private String OtherNoType;
    /** 错误编码 */
    private String ErrorCode;
    /** 出错信息 */
    private String ErrorInformation;
    /** 当前值 */
    private String CurrentValue;
    /** 错误严重级别 */
    private String ErrorGrade;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;

    public static final int FIELDNUM = 13; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LCErrSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "ErrorNo";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getErrorNo()
    {
        if (ErrorNo != null && !ErrorNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            ErrorNo = StrTool.unicodeToGBK(ErrorNo);
        }
        return ErrorNo;
    }

    public void setErrorNo(String aErrorNo)
    {
        ErrorNo = aErrorNo;
    }

    public String getProposalNo()
    {
        if (ProposalNo != null && !ProposalNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ProposalNo = StrTool.unicodeToGBK(ProposalNo);
        }
        return ProposalNo;
    }

    public void setProposalNo(String aProposalNo)
    {
        ProposalNo = aProposalNo;
    }

    public String getPolNo()
    {
        if (PolNo != null && !PolNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            PolNo = StrTool.unicodeToGBK(PolNo);
        }
        return PolNo;
    }

    public void setPolNo(String aPolNo)
    {
        PolNo = aPolNo;
    }

    public String getManageCom()
    {
        if (ManageCom != null && !ManageCom.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ManageCom = StrTool.unicodeToGBK(ManageCom);
        }
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public String getOtherNo()
    {
        if (OtherNo != null && !OtherNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            OtherNo = StrTool.unicodeToGBK(OtherNo);
        }
        return OtherNo;
    }

    public void setOtherNo(String aOtherNo)
    {
        OtherNo = aOtherNo;
    }

    public String getOtherNoType()
    {
        if (OtherNoType != null && !OtherNoType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            OtherNoType = StrTool.unicodeToGBK(OtherNoType);
        }
        return OtherNoType;
    }

    public void setOtherNoType(String aOtherNoType)
    {
        OtherNoType = aOtherNoType;
    }

    public String getErrorCode()
    {
        if (ErrorCode != null && !ErrorCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ErrorCode = StrTool.unicodeToGBK(ErrorCode);
        }
        return ErrorCode;
    }

    public void setErrorCode(String aErrorCode)
    {
        ErrorCode = aErrorCode;
    }

    public String getErrorInformation()
    {
        if (ErrorInformation != null && !ErrorInformation.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ErrorInformation = StrTool.unicodeToGBK(ErrorInformation);
        }
        return ErrorInformation;
    }

    public void setErrorInformation(String aErrorInformation)
    {
        ErrorInformation = aErrorInformation;
    }

    public String getCurrentValue()
    {
        if (CurrentValue != null && !CurrentValue.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CurrentValue = StrTool.unicodeToGBK(CurrentValue);
        }
        return CurrentValue;
    }

    public void setCurrentValue(String aCurrentValue)
    {
        CurrentValue = aCurrentValue;
    }

    public String getErrorGrade()
    {
        if (ErrorGrade != null && !ErrorGrade.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ErrorGrade = StrTool.unicodeToGBK(ErrorGrade);
        }
        return ErrorGrade;
    }

    public void setErrorGrade(String aErrorGrade)
    {
        ErrorGrade = aErrorGrade;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    /**
     * 使用另外一个 LCErrSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LCErrSchema aLCErrSchema)
    {
        this.ErrorNo = aLCErrSchema.getErrorNo();
        this.ProposalNo = aLCErrSchema.getProposalNo();
        this.PolNo = aLCErrSchema.getPolNo();
        this.ManageCom = aLCErrSchema.getManageCom();
        this.OtherNo = aLCErrSchema.getOtherNo();
        this.OtherNoType = aLCErrSchema.getOtherNoType();
        this.ErrorCode = aLCErrSchema.getErrorCode();
        this.ErrorInformation = aLCErrSchema.getErrorInformation();
        this.CurrentValue = aLCErrSchema.getCurrentValue();
        this.ErrorGrade = aLCErrSchema.getErrorGrade();
        this.Operator = aLCErrSchema.getOperator();
        this.MakeDate = fDate.getDate(aLCErrSchema.getMakeDate());
        this.MakeTime = aLCErrSchema.getMakeTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ErrorNo") == null)
            {
                this.ErrorNo = null;
            }
            else
            {
                this.ErrorNo = rs.getString("ErrorNo").trim();
            }

            if (rs.getString("ProposalNo") == null)
            {
                this.ProposalNo = null;
            }
            else
            {
                this.ProposalNo = rs.getString("ProposalNo").trim();
            }

            if (rs.getString("PolNo") == null)
            {
                this.PolNo = null;
            }
            else
            {
                this.PolNo = rs.getString("PolNo").trim();
            }

            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("OtherNo") == null)
            {
                this.OtherNo = null;
            }
            else
            {
                this.OtherNo = rs.getString("OtherNo").trim();
            }

            if (rs.getString("OtherNoType") == null)
            {
                this.OtherNoType = null;
            }
            else
            {
                this.OtherNoType = rs.getString("OtherNoType").trim();
            }

            if (rs.getString("ErrorCode") == null)
            {
                this.ErrorCode = null;
            }
            else
            {
                this.ErrorCode = rs.getString("ErrorCode").trim();
            }

            if (rs.getString("ErrorInformation") == null)
            {
                this.ErrorInformation = null;
            }
            else
            {
                this.ErrorInformation = rs.getString("ErrorInformation").trim();
            }

            if (rs.getString("CurrentValue") == null)
            {
                this.CurrentValue = null;
            }
            else
            {
                this.CurrentValue = rs.getString("CurrentValue").trim();
            }

            if (rs.getString("ErrorGrade") == null)
            {
                this.ErrorGrade = null;
            }
            else
            {
                this.ErrorGrade = rs.getString("ErrorGrade").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCErrSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LCErrSchema getSchema()
    {
        LCErrSchema aLCErrSchema = new LCErrSchema();
        aLCErrSchema.setSchema(this);
        return aLCErrSchema;
    }

    public LCErrDB getDB()
    {
        LCErrDB aDBOper = new LCErrDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCErr描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(ErrorNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ProposalNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PolNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ManageCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(OtherNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(OtherNoType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ErrorCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ErrorInformation)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CurrentValue)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ErrorGrade)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCErr>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            ErrorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                     SysConst.PACKAGESPILTER);
            ProposalNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
            PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                   SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                       SysConst.PACKAGESPILTER);
            OtherNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                     SysConst.PACKAGESPILTER);
            OtherNoType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                         SysConst.PACKAGESPILTER);
            ErrorCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                       SysConst.PACKAGESPILTER);
            ErrorInformation = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                              8, SysConst.PACKAGESPILTER);
            CurrentValue = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                          SysConst.PACKAGESPILTER);
            ErrorGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                        SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 12, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                      SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCErrSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("ErrorNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ErrorNo));
        }
        if (FCode.equals("ProposalNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ProposalNo));
        }
        if (FCode.equals("PolNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PolNo));
        }
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ManageCom));
        }
        if (FCode.equals("OtherNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(OtherNo));
        }
        if (FCode.equals("OtherNoType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(OtherNoType));
        }
        if (FCode.equals("ErrorCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ErrorCode));
        }
        if (FCode.equals("ErrorInformation"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ErrorInformation));
        }
        if (FCode.equals("CurrentValue"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CurrentValue));
        }
        if (FCode.equals("ErrorGrade"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ErrorGrade));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ErrorNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ProposalNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(PolNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(OtherNo);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(OtherNoType);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(ErrorCode);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(ErrorInformation);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(CurrentValue);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(ErrorGrade);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("ErrorNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ErrorNo = FValue.trim();
            }
            else
            {
                ErrorNo = null;
            }
        }
        if (FCode.equals("ProposalNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ProposalNo = FValue.trim();
            }
            else
            {
                ProposalNo = null;
            }
        }
        if (FCode.equals("PolNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
            {
                PolNo = null;
            }
        }
        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("OtherNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OtherNo = FValue.trim();
            }
            else
            {
                OtherNo = null;
            }
        }
        if (FCode.equals("OtherNoType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OtherNoType = FValue.trim();
            }
            else
            {
                OtherNoType = null;
            }
        }
        if (FCode.equals("ErrorCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ErrorCode = FValue.trim();
            }
            else
            {
                ErrorCode = null;
            }
        }
        if (FCode.equals("ErrorInformation"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ErrorInformation = FValue.trim();
            }
            else
            {
                ErrorInformation = null;
            }
        }
        if (FCode.equals("CurrentValue"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CurrentValue = FValue.trim();
            }
            else
            {
                CurrentValue = null;
            }
        }
        if (FCode.equals("ErrorGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ErrorGrade = FValue.trim();
            }
            else
            {
                ErrorGrade = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LCErrSchema other = (LCErrSchema) otherObject;
        return
                ErrorNo.equals(other.getErrorNo())
                && ProposalNo.equals(other.getProposalNo())
                && PolNo.equals(other.getPolNo())
                && ManageCom.equals(other.getManageCom())
                && OtherNo.equals(other.getOtherNo())
                && OtherNoType.equals(other.getOtherNoType())
                && ErrorCode.equals(other.getErrorCode())
                && ErrorInformation.equals(other.getErrorInformation())
                && CurrentValue.equals(other.getCurrentValue())
                && ErrorGrade.equals(other.getErrorGrade())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("ErrorNo"))
        {
            return 0;
        }
        if (strFieldName.equals("ProposalNo"))
        {
            return 1;
        }
        if (strFieldName.equals("PolNo"))
        {
            return 2;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return 3;
        }
        if (strFieldName.equals("OtherNo"))
        {
            return 4;
        }
        if (strFieldName.equals("OtherNoType"))
        {
            return 5;
        }
        if (strFieldName.equals("ErrorCode"))
        {
            return 6;
        }
        if (strFieldName.equals("ErrorInformation"))
        {
            return 7;
        }
        if (strFieldName.equals("CurrentValue"))
        {
            return 8;
        }
        if (strFieldName.equals("ErrorGrade"))
        {
            return 9;
        }
        if (strFieldName.equals("Operator"))
        {
            return 10;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 11;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 12;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "ErrorNo";
                break;
            case 1:
                strFieldName = "ProposalNo";
                break;
            case 2:
                strFieldName = "PolNo";
                break;
            case 3:
                strFieldName = "ManageCom";
                break;
            case 4:
                strFieldName = "OtherNo";
                break;
            case 5:
                strFieldName = "OtherNoType";
                break;
            case 6:
                strFieldName = "ErrorCode";
                break;
            case 7:
                strFieldName = "ErrorInformation";
                break;
            case 8:
                strFieldName = "CurrentValue";
                break;
            case 9:
                strFieldName = "ErrorGrade";
                break;
            case 10:
                strFieldName = "Operator";
                break;
            case 11:
                strFieldName = "MakeDate";
                break;
            case 12:
                strFieldName = "MakeTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("ErrorNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ProposalNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PolNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OtherNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OtherNoType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ErrorCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ErrorInformation"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CurrentValue"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ErrorGrade"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
