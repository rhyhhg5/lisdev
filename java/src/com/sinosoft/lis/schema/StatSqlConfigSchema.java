/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.StatSqlConfigDB;

/*
 * <p>ClassName: StatSqlConfigSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-07-11
 */
public class StatSqlConfigSchema implements Schema, Cloneable {
    // @Field
    /** Sql代码 */
    private String CalCode;
    /** 模块代码 */
    private String ModuCode;
    /** 模块名称 */
    private String ModuName;
    /** 启用标识 */
    private String ValiFlag;
    /** 校验sql */
    private String CalSql;
    /** 错误描述 */
    private String Remark;

    public static final int FIELDNUM = 6; // 数据库表的字段个数

    private static String[] PK; // 主键

    public CErrors mErrors; // 错误信息

    // @Constructor
    public StatSqlConfigSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "CalCode";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        StatSqlConfigSchema cloned = (StatSqlConfigSchema)super.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getCalCode() {
        return CalCode;
    }

    public void setCalCode(String aCalCode) {
        CalCode = aCalCode;
    }

    public String getModuCode() {
        return ModuCode;
    }

    public void setModuCode(String aModuCode) {
        ModuCode = aModuCode;
    }

    public String getModuName() {
        return ModuName;
    }

    public void setModuName(String aModuName) {
        ModuName = aModuName;
    }

    public String getValiFlag() {
        return ValiFlag;
    }

    public void setValiFlag(String aValiFlag) {
        ValiFlag = aValiFlag;
    }

    public String getCalSql() {
        return CalSql;
    }

    public void setCalSql(String aCalSql) {
        CalSql = aCalSql;
    }

    public String getRemark() {
        return Remark;
    }

    public void setRemark(String aRemark) {
        Remark = aRemark;
    }

    /**
     * 使用另外一个 StatSqlConfigSchema 对象给 Schema 赋值
     * @param: aStatSqlConfigSchema StatSqlConfigSchema
     **/
    public void setSchema(StatSqlConfigSchema aStatSqlConfigSchema) {
        this.CalCode = aStatSqlConfigSchema.getCalCode();
        this.ModuCode = aStatSqlConfigSchema.getModuCode();
        this.ModuName = aStatSqlConfigSchema.getModuName();
        this.ValiFlag = aStatSqlConfigSchema.getValiFlag();
        this.CalSql = aStatSqlConfigSchema.getCalSql();
        this.Remark = aStatSqlConfigSchema.getRemark();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("CalCode") == null) {
                this.CalCode = null;
            } else {
                this.CalCode = rs.getString("CalCode").trim();
            }

            if (rs.getString("ModuCode") == null) {
                this.ModuCode = null;
            } else {
                this.ModuCode = rs.getString("ModuCode").trim();
            }

            if (rs.getString("ModuName") == null) {
                this.ModuName = null;
            } else {
                this.ModuName = rs.getString("ModuName").trim();
            }

            if (rs.getString("ValiFlag") == null) {
                this.ValiFlag = null;
            } else {
                this.ValiFlag = rs.getString("ValiFlag").trim();
            }

            if (rs.getString("CalSql") == null) {
                this.CalSql = null;
            } else {
                this.CalSql = rs.getString("CalSql").trim();
            }

            if (rs.getString("Remark") == null) {
                this.Remark = null;
            } else {
                this.Remark = rs.getString("Remark").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的StatSqlConfig表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "StatSqlConfigSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public StatSqlConfigSchema getSchema() {
        StatSqlConfigSchema aStatSqlConfigSchema = new StatSqlConfigSchema();
        aStatSqlConfigSchema.setSchema(this);
        return aStatSqlConfigSchema;
    }

    public StatSqlConfigDB getDB() {
        StatSqlConfigDB aDBOper = new StatSqlConfigDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpStatSqlConfig描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(CalCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModuCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModuName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ValiFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CalSql));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Remark));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpStatSqlConfig>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            CalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                     SysConst.PACKAGESPILTER);
            ModuCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            ModuName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            ValiFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                      SysConst.PACKAGESPILTER);
            CalSql = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                    SysConst.PACKAGESPILTER);
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                    SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "StatSqlConfigSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("CalCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalCode));
        }
        if (FCode.equals("ModuCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModuCode));
        }
        if (FCode.equals("ModuName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModuName));
        }
        if (FCode.equals("ValiFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ValiFlag));
        }
        if (FCode.equals("CalSql")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalSql));
        }
        if (FCode.equals("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(CalCode);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(ModuCode);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(ModuName);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(ValiFlag);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(CalSql);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(Remark);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("CalCode")) {
            if (FValue != null && !FValue.equals("")) {
                CalCode = FValue.trim();
            } else {
                CalCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModuCode")) {
            if (FValue != null && !FValue.equals("")) {
                ModuCode = FValue.trim();
            } else {
                ModuCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModuName")) {
            if (FValue != null && !FValue.equals("")) {
                ModuName = FValue.trim();
            } else {
                ModuName = null;
            }
        }
        if (FCode.equalsIgnoreCase("ValiFlag")) {
            if (FValue != null && !FValue.equals("")) {
                ValiFlag = FValue.trim();
            } else {
                ValiFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("CalSql")) {
            if (FValue != null && !FValue.equals("")) {
                CalSql = FValue.trim();
            } else {
                CalSql = null;
            }
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if (FValue != null && !FValue.equals("")) {
                Remark = FValue.trim();
            } else {
                Remark = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        StatSqlConfigSchema other = (StatSqlConfigSchema) otherObject;
        return
                CalCode.equals(other.getCalCode())
                && ModuCode.equals(other.getModuCode())
                && ModuName.equals(other.getModuName())
                && ValiFlag.equals(other.getValiFlag())
                && CalSql.equals(other.getCalSql())
                && Remark.equals(other.getRemark());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("CalCode")) {
            return 0;
        }
        if (strFieldName.equals("ModuCode")) {
            return 1;
        }
        if (strFieldName.equals("ModuName")) {
            return 2;
        }
        if (strFieldName.equals("ValiFlag")) {
            return 3;
        }
        if (strFieldName.equals("CalSql")) {
            return 4;
        }
        if (strFieldName.equals("Remark")) {
            return 5;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "CalCode";
            break;
        case 1:
            strFieldName = "ModuCode";
            break;
        case 2:
            strFieldName = "ModuName";
            break;
        case 3:
            strFieldName = "ValiFlag";
            break;
        case 4:
            strFieldName = "CalSql";
            break;
        case 5:
            strFieldName = "Remark";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("CalCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModuCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModuName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ValiFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CalSql")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Remark")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
