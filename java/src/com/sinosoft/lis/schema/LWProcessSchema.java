/*
 * <p>ClassName: LWProcessSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 工作流模型
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LWProcessDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LWProcessSchema implements Schema
{
    // @Field
    /** 过程id */
    private String ProcessID;
    /** 过程名 */
    private String ProcessName;
    /** 过程说明 */
    private String ProcessDesc;
    /** 平均等待时间 */
    private double WatingTime;
    /** 平均执行时间 */
    private double WorkingTime;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 10; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LWProcessSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "ProcessID";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getProcessID()
    {
        if (ProcessID != null && !ProcessID.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ProcessID = StrTool.unicodeToGBK(ProcessID);
        }
        return ProcessID;
    }

    public void setProcessID(String aProcessID)
    {
        ProcessID = aProcessID;
    }

    public String getProcessName()
    {
        if (ProcessName != null && !ProcessName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ProcessName = StrTool.unicodeToGBK(ProcessName);
        }
        return ProcessName;
    }

    public void setProcessName(String aProcessName)
    {
        ProcessName = aProcessName;
    }

    public String getProcessDesc()
    {
        if (ProcessDesc != null && !ProcessDesc.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ProcessDesc = StrTool.unicodeToGBK(ProcessDesc);
        }
        return ProcessDesc;
    }

    public void setProcessDesc(String aProcessDesc)
    {
        ProcessDesc = aProcessDesc;
    }

    public double getWatingTime()
    {
        return WatingTime;
    }

    public void setWatingTime(double aWatingTime)
    {
        WatingTime = aWatingTime;
    }

    public void setWatingTime(String aWatingTime)
    {
        if (aWatingTime != null && !aWatingTime.equals(""))
        {
            Double tDouble = new Double(aWatingTime);
            double d = tDouble.doubleValue();
            WatingTime = d;
        }
    }

    public double getWorkingTime()
    {
        return WorkingTime;
    }

    public void setWorkingTime(double aWorkingTime)
    {
        WorkingTime = aWorkingTime;
    }

    public void setWorkingTime(String aWorkingTime)
    {
        if (aWorkingTime != null && !aWorkingTime.equals(""))
        {
            Double tDouble = new Double(aWorkingTime);
            double d = tDouble.doubleValue();
            WorkingTime = d;
        }
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LWProcessSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LWProcessSchema aLWProcessSchema)
    {
        this.ProcessID = aLWProcessSchema.getProcessID();
        this.ProcessName = aLWProcessSchema.getProcessName();
        this.ProcessDesc = aLWProcessSchema.getProcessDesc();
        this.WatingTime = aLWProcessSchema.getWatingTime();
        this.WorkingTime = aLWProcessSchema.getWorkingTime();
        this.Operator = aLWProcessSchema.getOperator();
        this.MakeDate = fDate.getDate(aLWProcessSchema.getMakeDate());
        this.MakeTime = aLWProcessSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLWProcessSchema.getModifyDate());
        this.ModifyTime = aLWProcessSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ProcessID") == null)
            {
                this.ProcessID = null;
            }
            else
            {
                this.ProcessID = rs.getString("ProcessID").trim();
            }

            if (rs.getString("ProcessName") == null)
            {
                this.ProcessName = null;
            }
            else
            {
                this.ProcessName = rs.getString("ProcessName").trim();
            }

            if (rs.getString("ProcessDesc") == null)
            {
                this.ProcessDesc = null;
            }
            else
            {
                this.ProcessDesc = rs.getString("ProcessDesc").trim();
            }

            this.WatingTime = rs.getDouble("WatingTime");
            this.WorkingTime = rs.getDouble("WorkingTime");
            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LWProcessSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LWProcessSchema getSchema()
    {
        LWProcessSchema aLWProcessSchema = new LWProcessSchema();
        aLWProcessSchema.setSchema(this);
        return aLWProcessSchema;
    }

    public LWProcessDB getDB()
    {
        LWProcessDB aDBOper = new LWProcessDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLWProcess描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(ProcessID)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ProcessName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ProcessDesc)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(WatingTime) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(WorkingTime) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLWProcess>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            ProcessID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            ProcessName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                         SysConst.PACKAGESPILTER);
            ProcessDesc = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                         SysConst.PACKAGESPILTER);
            WatingTime = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 4, SysConst.PACKAGESPILTER))).doubleValue();
            WorkingTime = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 5, SysConst.PACKAGESPILTER))).doubleValue();
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 7, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 9, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LWProcessSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("ProcessID"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ProcessID));
        }
        if (FCode.equals("ProcessName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ProcessName));
        }
        if (FCode.equals("ProcessDesc"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ProcessDesc));
        }
        if (FCode.equals("WatingTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(WatingTime));
        }
        if (FCode.equals("WorkingTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(WorkingTime));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ProcessID);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ProcessName);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ProcessDesc);
                break;
            case 3:
                strFieldValue = String.valueOf(WatingTime);
                break;
            case 4:
                strFieldValue = String.valueOf(WorkingTime);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("ProcessID"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ProcessID = FValue.trim();
            }
            else
            {
                ProcessID = null;
            }
        }
        if (FCode.equals("ProcessName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ProcessName = FValue.trim();
            }
            else
            {
                ProcessName = null;
            }
        }
        if (FCode.equals("ProcessDesc"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ProcessDesc = FValue.trim();
            }
            else
            {
                ProcessDesc = null;
            }
        }
        if (FCode.equals("WatingTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                WatingTime = d;
            }
        }
        if (FCode.equals("WorkingTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                WorkingTime = d;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LWProcessSchema other = (LWProcessSchema) otherObject;
        return
                ProcessID.equals(other.getProcessID())
                && ProcessName.equals(other.getProcessName())
                && ProcessDesc.equals(other.getProcessDesc())
                && WatingTime == other.getWatingTime()
                && WorkingTime == other.getWorkingTime()
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("ProcessID"))
        {
            return 0;
        }
        if (strFieldName.equals("ProcessName"))
        {
            return 1;
        }
        if (strFieldName.equals("ProcessDesc"))
        {
            return 2;
        }
        if (strFieldName.equals("WatingTime"))
        {
            return 3;
        }
        if (strFieldName.equals("WorkingTime"))
        {
            return 4;
        }
        if (strFieldName.equals("Operator"))
        {
            return 5;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 6;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 7;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 8;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 9;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "ProcessID";
                break;
            case 1:
                strFieldName = "ProcessName";
                break;
            case 2:
                strFieldName = "ProcessDesc";
                break;
            case 3:
                strFieldName = "WatingTime";
                break;
            case 4:
                strFieldName = "WorkingTime";
                break;
            case 5:
                strFieldName = "Operator";
                break;
            case 6:
                strFieldName = "MakeDate";
                break;
            case 7:
                strFieldName = "MakeTime";
                break;
            case 8:
                strFieldName = "ModifyDate";
                break;
            case 9:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("ProcessID"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ProcessName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ProcessDesc"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("WatingTime"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("WorkingTime"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 4:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
