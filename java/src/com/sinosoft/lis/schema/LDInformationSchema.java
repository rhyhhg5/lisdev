/*
 * <p>ClassName: LDInformationSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 团单补充资料信息表
 * @CreateDate：2005-01-20
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LDInformationDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LDInformationSchema implements Schema
{
    // @Field
    /** 流水号 */
    private String SerialNo;
    /** 资料分类 */
    private String InformationKind;
    /** 资料代码 */
    private String InformationCode;
    /** 资料名称 */
    private String InformationName;
    /** 资料额度起始值 */
    private double StartMoney;
    /** 资料额度终止值 */
    private double EndMoney;
    /** 资料年龄起始值 */
    private int StartAge;
    /** 资料年龄终止值 */
    private int EndAge;
    /** 资料人性别 */
    private String Sex;
    /** 资料说明 */
    private String Remark;
    /** 备注2 */
    private String Note2;

    public static final int FIELDNUM = 11; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LDInformationSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "SerialNo";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getSerialNo()
    {
        if (SerialNo != null && !SerialNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            SerialNo = StrTool.unicodeToGBK(SerialNo);
        }
        return SerialNo;
    }

    public void setSerialNo(String aSerialNo)
    {
        SerialNo = aSerialNo;
    }

    public String getInformationKind()
    {
        if (InformationKind != null && !InformationKind.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            InformationKind = StrTool.unicodeToGBK(InformationKind);
        }
        return InformationKind;
    }

    public void setInformationKind(String aInformationKind)
    {
        InformationKind = aInformationKind;
    }

    public String getInformationCode()
    {
        if (InformationCode != null && !InformationCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            InformationCode = StrTool.unicodeToGBK(InformationCode);
        }
        return InformationCode;
    }

    public void setInformationCode(String aInformationCode)
    {
        InformationCode = aInformationCode;
    }

    public String getInformationName()
    {
        if (InformationName != null && !InformationName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            InformationName = StrTool.unicodeToGBK(InformationName);
        }
        return InformationName;
    }

    public void setInformationName(String aInformationName)
    {
        InformationName = aInformationName;
    }

    public double getStartMoney()
    {
        return StartMoney;
    }

    public void setStartMoney(double aStartMoney)
    {
        StartMoney = aStartMoney;
    }

    public void setStartMoney(String aStartMoney)
    {
        if (aStartMoney != null && !aStartMoney.equals(""))
        {
            Double tDouble = new Double(aStartMoney);
            double d = tDouble.doubleValue();
            StartMoney = d;
        }
    }

    public double getEndMoney()
    {
        return EndMoney;
    }

    public void setEndMoney(double aEndMoney)
    {
        EndMoney = aEndMoney;
    }

    public void setEndMoney(String aEndMoney)
    {
        if (aEndMoney != null && !aEndMoney.equals(""))
        {
            Double tDouble = new Double(aEndMoney);
            double d = tDouble.doubleValue();
            EndMoney = d;
        }
    }

    public int getStartAge()
    {
        return StartAge;
    }

    public void setStartAge(int aStartAge)
    {
        StartAge = aStartAge;
    }

    public void setStartAge(String aStartAge)
    {
        if (aStartAge != null && !aStartAge.equals(""))
        {
            Integer tInteger = new Integer(aStartAge);
            int i = tInteger.intValue();
            StartAge = i;
        }
    }

    public int getEndAge()
    {
        return EndAge;
    }

    public void setEndAge(int aEndAge)
    {
        EndAge = aEndAge;
    }

    public void setEndAge(String aEndAge)
    {
        if (aEndAge != null && !aEndAge.equals(""))
        {
            Integer tInteger = new Integer(aEndAge);
            int i = tInteger.intValue();
            EndAge = i;
        }
    }

    public String getSex()
    {
        if (Sex != null && !Sex.equals("") && SysConst.CHANGECHARSET == true)
        {
            Sex = StrTool.unicodeToGBK(Sex);
        }
        return Sex;
    }

    public void setSex(String aSex)
    {
        Sex = aSex;
    }

    public String getRemark()
    {
        if (Remark != null && !Remark.equals("") && SysConst.CHANGECHARSET == true)
        {
            Remark = StrTool.unicodeToGBK(Remark);
        }
        return Remark;
    }

    public void setRemark(String aRemark)
    {
        Remark = aRemark;
    }

    public String getNote2()
    {
        if (Note2 != null && !Note2.equals("") && SysConst.CHANGECHARSET == true)
        {
            Note2 = StrTool.unicodeToGBK(Note2);
        }
        return Note2;
    }

    public void setNote2(String aNote2)
    {
        Note2 = aNote2;
    }

    /**
     * 使用另外一个 LDInformationSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LDInformationSchema aLDInformationSchema)
    {
        this.SerialNo = aLDInformationSchema.getSerialNo();
        this.InformationKind = aLDInformationSchema.getInformationKind();
        this.InformationCode = aLDInformationSchema.getInformationCode();
        this.InformationName = aLDInformationSchema.getInformationName();
        this.StartMoney = aLDInformationSchema.getStartMoney();
        this.EndMoney = aLDInformationSchema.getEndMoney();
        this.StartAge = aLDInformationSchema.getStartAge();
        this.EndAge = aLDInformationSchema.getEndAge();
        this.Sex = aLDInformationSchema.getSex();
        this.Remark = aLDInformationSchema.getRemark();
        this.Note2 = aLDInformationSchema.getNote2();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("SerialNo") == null)
            {
                this.SerialNo = null;
            }
            else
            {
                this.SerialNo = rs.getString("SerialNo").trim();
            }

            if (rs.getString("InformationKind") == null)
            {
                this.InformationKind = null;
            }
            else
            {
                this.InformationKind = rs.getString("InformationKind").trim();
            }

            if (rs.getString("InformationCode") == null)
            {
                this.InformationCode = null;
            }
            else
            {
                this.InformationCode = rs.getString("InformationCode").trim();
            }

            if (rs.getString("InformationName") == null)
            {
                this.InformationName = null;
            }
            else
            {
                this.InformationName = rs.getString("InformationName").trim();
            }

            this.StartMoney = rs.getDouble("StartMoney");
            this.EndMoney = rs.getDouble("EndMoney");
            this.StartAge = rs.getInt("StartAge");
            this.EndAge = rs.getInt("EndAge");
            if (rs.getString("Sex") == null)
            {
                this.Sex = null;
            }
            else
            {
                this.Sex = rs.getString("Sex").trim();
            }

            if (rs.getString("Remark") == null)
            {
                this.Remark = null;
            }
            else
            {
                this.Remark = rs.getString("Remark").trim();
            }

            if (rs.getString("Note2") == null)
            {
                this.Note2 = null;
            }
            else
            {
                this.Note2 = rs.getString("Note2").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDInformationSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LDInformationSchema getSchema()
    {
        LDInformationSchema aLDInformationSchema = new LDInformationSchema();
        aLDInformationSchema.setSchema(this);
        return aLDInformationSchema;
    }

    public LDInformationDB getDB()
    {
        LDInformationDB aDBOper = new LDInformationDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDInformation描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(SerialNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InformationKind)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InformationCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InformationName)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(StartMoney) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(EndMoney) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(StartAge) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(EndAge) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Sex)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Remark)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Note2));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDInformation>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            InformationKind = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             2, SysConst.PACKAGESPILTER);
            InformationCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             3, SysConst.PACKAGESPILTER);
            InformationName = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             4, SysConst.PACKAGESPILTER);
            StartMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 5, SysConst.PACKAGESPILTER))).doubleValue();
            EndMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 6, SysConst.PACKAGESPILTER))).doubleValue();
            StartAge = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 7, SysConst.PACKAGESPILTER))).intValue();
            EndAge = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 8, SysConst.PACKAGESPILTER))).intValue();
            Sex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                 SysConst.PACKAGESPILTER);
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                    SysConst.PACKAGESPILTER);
            Note2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                   SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDInformationSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("SerialNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SerialNo));
        }
        if (FCode.equals("InformationKind"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(InformationKind));
        }
        if (FCode.equals("InformationCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(InformationCode));
        }
        if (FCode.equals("InformationName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(InformationName));
        }
        if (FCode.equals("StartMoney"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(StartMoney));
        }
        if (FCode.equals("EndMoney"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(EndMoney));
        }
        if (FCode.equals("StartAge"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(StartAge));
        }
        if (FCode.equals("EndAge"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(EndAge));
        }
        if (FCode.equals("Sex"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Sex));
        }
        if (FCode.equals("Remark"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Remark));
        }
        if (FCode.equals("Note2"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Note2));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(SerialNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(InformationKind);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(InformationCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(InformationName);
                break;
            case 4:
                strFieldValue = String.valueOf(StartMoney);
                break;
            case 5:
                strFieldValue = String.valueOf(EndMoney);
                break;
            case 6:
                strFieldValue = String.valueOf(StartAge);
                break;
            case 7:
                strFieldValue = String.valueOf(EndAge);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(Sex);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(Note2);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("SerialNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
            {
                SerialNo = null;
            }
        }
        if (FCode.equals("InformationKind"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InformationKind = FValue.trim();
            }
            else
            {
                InformationKind = null;
            }
        }
        if (FCode.equals("InformationCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InformationCode = FValue.trim();
            }
            else
            {
                InformationCode = null;
            }
        }
        if (FCode.equals("InformationName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InformationName = FValue.trim();
            }
            else
            {
                InformationName = null;
            }
        }
        if (FCode.equals("StartMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                StartMoney = d;
            }
        }
        if (FCode.equals("EndMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                EndMoney = d;
            }
        }
        if (FCode.equals("StartAge"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                StartAge = i;
            }
        }
        if (FCode.equals("EndAge"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                EndAge = i;
            }
        }
        if (FCode.equals("Sex"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Sex = FValue.trim();
            }
            else
            {
                Sex = null;
            }
        }
        if (FCode.equals("Remark"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
            {
                Remark = null;
            }
        }
        if (FCode.equals("Note2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Note2 = FValue.trim();
            }
            else
            {
                Note2 = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LDInformationSchema other = (LDInformationSchema) otherObject;
        return
                SerialNo.equals(other.getSerialNo())
                && InformationKind.equals(other.getInformationKind())
                && InformationCode.equals(other.getInformationCode())
                && InformationName.equals(other.getInformationName())
                && StartMoney == other.getStartMoney()
                && EndMoney == other.getEndMoney()
                && StartAge == other.getStartAge()
                && EndAge == other.getEndAge()
                && Sex.equals(other.getSex())
                && Remark.equals(other.getRemark())
                && Note2.equals(other.getNote2());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("SerialNo"))
        {
            return 0;
        }
        if (strFieldName.equals("InformationKind"))
        {
            return 1;
        }
        if (strFieldName.equals("InformationCode"))
        {
            return 2;
        }
        if (strFieldName.equals("InformationName"))
        {
            return 3;
        }
        if (strFieldName.equals("StartMoney"))
        {
            return 4;
        }
        if (strFieldName.equals("EndMoney"))
        {
            return 5;
        }
        if (strFieldName.equals("StartAge"))
        {
            return 6;
        }
        if (strFieldName.equals("EndAge"))
        {
            return 7;
        }
        if (strFieldName.equals("Sex"))
        {
            return 8;
        }
        if (strFieldName.equals("Remark"))
        {
            return 9;
        }
        if (strFieldName.equals("Note2"))
        {
            return 10;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "SerialNo";
                break;
            case 1:
                strFieldName = "InformationKind";
                break;
            case 2:
                strFieldName = "InformationCode";
                break;
            case 3:
                strFieldName = "InformationName";
                break;
            case 4:
                strFieldName = "StartMoney";
                break;
            case 5:
                strFieldName = "EndMoney";
                break;
            case 6:
                strFieldName = "StartAge";
                break;
            case 7:
                strFieldName = "EndAge";
                break;
            case 8:
                strFieldName = "Sex";
                break;
            case 9:
                strFieldName = "Remark";
                break;
            case 10:
                strFieldName = "Note2";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("SerialNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InformationKind"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InformationCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InformationName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StartMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("EndMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("StartAge"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("EndAge"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("Sex"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Remark"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Note2"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 5:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 6:
                nFieldType = Schema.TYPE_INT;
                break;
            case 7:
                nFieldType = Schema.TYPE_INT;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
