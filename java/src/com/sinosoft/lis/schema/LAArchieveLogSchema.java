/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LAArchieveLogDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LAArchieveLogSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-03-24
 */
public class LAArchieveLogSchema implements Schema
{
    // @Field
    /** 流水号 */
    private String SeriesNo;
    /** 档案编码 */
    private String ArchieveNo;
    /** 档案项目流水号 */
    private int ItemIdx;
    /** 档案日期 */
    private Date ArchieveDate;
    /** 档案类型 */
    private String ArchType;
    /** 档案项目 */
    private String ArchItem;
    /** 档案借阅人 */
    private String Borrower;
    /** 借阅时间 */
    private Date BorrowDate;
    /** 计划归还时间 */
    private Date ReturnDate1;
    /** 实际归还时间 */
    private Date ReturnDate2;
    /** 批准人 */
    private String Confirmer;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 上一次修改日期 */
    private Date ModifyDate;
    /** 上一次修改时间 */
    private String ModifyTime;
    /** 操作员编码 */
    private String Operator;
    /** 档案项目编码 */
    private String ItemCode;

    public static final int FIELDNUM = 17; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LAArchieveLogSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "SeriesNo";
        pk[1] = "ArchieveNo";
        pk[2] = "ItemIdx";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getSeriesNo()
    {
        if (SeriesNo != null && !SeriesNo.equals("") && SysConst.CHANGECHARSET)
        {
            SeriesNo = StrTool.unicodeToGBK(SeriesNo);
        }
        return SeriesNo;
    }

    public void setSeriesNo(String aSeriesNo)
    {
        SeriesNo = aSeriesNo;
    }

    public String getArchieveNo()
    {
        if (ArchieveNo != null && !ArchieveNo.equals("") &&
            SysConst.CHANGECHARSET)
        {
            ArchieveNo = StrTool.unicodeToGBK(ArchieveNo);
        }
        return ArchieveNo;
    }

    public void setArchieveNo(String aArchieveNo)
    {
        ArchieveNo = aArchieveNo;
    }

    public int getItemIdx()
    {
        return ItemIdx;
    }

    public void setItemIdx(int aItemIdx)
    {
        ItemIdx = aItemIdx;
    }

    public void setItemIdx(String aItemIdx)
    {
        if (aItemIdx != null && !aItemIdx.equals(""))
        {
            Integer tInteger = new Integer(aItemIdx);
            int i = tInteger.intValue();
            ItemIdx = i;
        }
    }

    public String getArchieveDate()
    {
        if (ArchieveDate != null)
        {
            return fDate.getString(ArchieveDate);
        }
        else
        {
            return null;
        }
    }

    public void setArchieveDate(Date aArchieveDate)
    {
        ArchieveDate = aArchieveDate;
    }

    public void setArchieveDate(String aArchieveDate)
    {
        if (aArchieveDate != null && !aArchieveDate.equals(""))
        {
            ArchieveDate = fDate.getDate(aArchieveDate);
        }
        else
        {
            ArchieveDate = null;
        }
    }

    public String getArchType()
    {
        if (ArchType != null && !ArchType.equals("") && SysConst.CHANGECHARSET)
        {
            ArchType = StrTool.unicodeToGBK(ArchType);
        }
        return ArchType;
    }

    public void setArchType(String aArchType)
    {
        ArchType = aArchType;
    }

    public String getArchItem()
    {
        if (ArchItem != null && !ArchItem.equals("") && SysConst.CHANGECHARSET)
        {
            ArchItem = StrTool.unicodeToGBK(ArchItem);
        }
        return ArchItem;
    }

    public void setArchItem(String aArchItem)
    {
        ArchItem = aArchItem;
    }

    public String getBorrower()
    {
        if (Borrower != null && !Borrower.equals("") && SysConst.CHANGECHARSET)
        {
            Borrower = StrTool.unicodeToGBK(Borrower);
        }
        return Borrower;
    }

    public void setBorrower(String aBorrower)
    {
        Borrower = aBorrower;
    }

    public String getBorrowDate()
    {
        if (BorrowDate != null)
        {
            return fDate.getString(BorrowDate);
        }
        else
        {
            return null;
        }
    }

    public void setBorrowDate(Date aBorrowDate)
    {
        BorrowDate = aBorrowDate;
    }

    public void setBorrowDate(String aBorrowDate)
    {
        if (aBorrowDate != null && !aBorrowDate.equals(""))
        {
            BorrowDate = fDate.getDate(aBorrowDate);
        }
        else
        {
            BorrowDate = null;
        }
    }

    public String getReturnDate1()
    {
        if (ReturnDate1 != null)
        {
            return fDate.getString(ReturnDate1);
        }
        else
        {
            return null;
        }
    }

    public void setReturnDate1(Date aReturnDate1)
    {
        ReturnDate1 = aReturnDate1;
    }

    public void setReturnDate1(String aReturnDate1)
    {
        if (aReturnDate1 != null && !aReturnDate1.equals(""))
        {
            ReturnDate1 = fDate.getDate(aReturnDate1);
        }
        else
        {
            ReturnDate1 = null;
        }
    }

    public String getReturnDate2()
    {
        if (ReturnDate2 != null)
        {
            return fDate.getString(ReturnDate2);
        }
        else
        {
            return null;
        }
    }

    public void setReturnDate2(Date aReturnDate2)
    {
        ReturnDate2 = aReturnDate2;
    }

    public void setReturnDate2(String aReturnDate2)
    {
        if (aReturnDate2 != null && !aReturnDate2.equals(""))
        {
            ReturnDate2 = fDate.getDate(aReturnDate2);
        }
        else
        {
            ReturnDate2 = null;
        }
    }

    public String getConfirmer()
    {
        if (Confirmer != null && !Confirmer.equals("") &&
            SysConst.CHANGECHARSET)
        {
            Confirmer = StrTool.unicodeToGBK(Confirmer);
        }
        return Confirmer;
    }

    public void setConfirmer(String aConfirmer)
    {
        Confirmer = aConfirmer;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getItemCode()
    {
        if (ItemCode != null && !ItemCode.equals("") && SysConst.CHANGECHARSET)
        {
            ItemCode = StrTool.unicodeToGBK(ItemCode);
        }
        return ItemCode;
    }

    public void setItemCode(String aItemCode)
    {
        ItemCode = aItemCode;
    }

    /**
     * 使用另外一个 LAArchieveLogSchema 对象给 Schema 赋值
     * @param: aLAArchieveLogSchema LAArchieveLogSchema
     **/
    public void setSchema(LAArchieveLogSchema aLAArchieveLogSchema)
    {
        this.SeriesNo = aLAArchieveLogSchema.getSeriesNo();
        this.ArchieveNo = aLAArchieveLogSchema.getArchieveNo();
        this.ItemIdx = aLAArchieveLogSchema.getItemIdx();
        this.ArchieveDate = fDate.getDate(aLAArchieveLogSchema.getArchieveDate());
        this.ArchType = aLAArchieveLogSchema.getArchType();
        this.ArchItem = aLAArchieveLogSchema.getArchItem();
        this.Borrower = aLAArchieveLogSchema.getBorrower();
        this.BorrowDate = fDate.getDate(aLAArchieveLogSchema.getBorrowDate());
        this.ReturnDate1 = fDate.getDate(aLAArchieveLogSchema.getReturnDate1());
        this.ReturnDate2 = fDate.getDate(aLAArchieveLogSchema.getReturnDate2());
        this.Confirmer = aLAArchieveLogSchema.getConfirmer();
        this.MakeDate = fDate.getDate(aLAArchieveLogSchema.getMakeDate());
        this.MakeTime = aLAArchieveLogSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLAArchieveLogSchema.getModifyDate());
        this.ModifyTime = aLAArchieveLogSchema.getModifyTime();
        this.Operator = aLAArchieveLogSchema.getOperator();
        this.ItemCode = aLAArchieveLogSchema.getItemCode();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("SeriesNo") == null)
            {
                this.SeriesNo = null;
            }
            else
            {
                this.SeriesNo = rs.getString("SeriesNo").trim();
            }

            if (rs.getString("ArchieveNo") == null)
            {
                this.ArchieveNo = null;
            }
            else
            {
                this.ArchieveNo = rs.getString("ArchieveNo").trim();
            }

            this.ItemIdx = rs.getInt("ItemIdx");
            this.ArchieveDate = rs.getDate("ArchieveDate");
            if (rs.getString("ArchType") == null)
            {
                this.ArchType = null;
            }
            else
            {
                this.ArchType = rs.getString("ArchType").trim();
            }

            if (rs.getString("ArchItem") == null)
            {
                this.ArchItem = null;
            }
            else
            {
                this.ArchItem = rs.getString("ArchItem").trim();
            }

            if (rs.getString("Borrower") == null)
            {
                this.Borrower = null;
            }
            else
            {
                this.Borrower = rs.getString("Borrower").trim();
            }

            this.BorrowDate = rs.getDate("BorrowDate");
            this.ReturnDate1 = rs.getDate("ReturnDate1");
            this.ReturnDate2 = rs.getDate("ReturnDate2");
            if (rs.getString("Confirmer") == null)
            {
                this.Confirmer = null;
            }
            else
            {
                this.Confirmer = rs.getString("Confirmer").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            if (rs.getString("ItemCode") == null)
            {
                this.ItemCode = null;
            }
            else
            {
                this.ItemCode = rs.getString("ItemCode").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAArchieveLogSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LAArchieveLogSchema getSchema()
    {
        LAArchieveLogSchema aLAArchieveLogSchema = new LAArchieveLogSchema();
        aLAArchieveLogSchema.setSchema(this);
        return aLAArchieveLogSchema;
    }

    public LAArchieveLogDB getDB()
    {
        LAArchieveLogDB aDBOper = new LAArchieveLogDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAArchieveLog描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(SeriesNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ArchieveNo)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(ItemIdx) + SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(ArchieveDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ArchType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ArchItem)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Borrower)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(BorrowDate))) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(ReturnDate1))) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(ReturnDate2))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Confirmer)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ItemCode));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAArchieveLog>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            SeriesNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            ArchieveNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
            ItemIdx = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 3, SysConst.PACKAGESPILTER))).intValue();
            ArchieveDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 4, SysConst.PACKAGESPILTER));
            ArchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                      SysConst.PACKAGESPILTER);
            ArchItem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                      SysConst.PACKAGESPILTER);
            Borrower = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                      SysConst.PACKAGESPILTER);
            BorrowDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 8, SysConst.PACKAGESPILTER));
            ReturnDate1 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 9, SysConst.PACKAGESPILTER));
            ReturnDate2 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 10, SysConst.PACKAGESPILTER));
            Confirmer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                       SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 12, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 14, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                        SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                      SysConst.PACKAGESPILTER);
            ItemCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                      SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAArchieveLogSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("SeriesNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SeriesNo));
        }
        if (FCode.equals("ArchieveNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ArchieveNo));
        }
        if (FCode.equals("ItemIdx"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ItemIdx));
        }
        if (FCode.equals("ArchieveDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getArchieveDate()));
        }
        if (FCode.equals("ArchType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ArchType));
        }
        if (FCode.equals("ArchItem"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ArchItem));
        }
        if (FCode.equals("Borrower"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Borrower));
        }
        if (FCode.equals("BorrowDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getBorrowDate()));
        }
        if (FCode.equals("ReturnDate1"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getReturnDate1()));
        }
        if (FCode.equals("ReturnDate2"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getReturnDate2()));
        }
        if (FCode.equals("Confirmer"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Confirmer));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("ItemCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ItemCode));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(SeriesNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ArchieveNo);
                break;
            case 2:
                strFieldValue = String.valueOf(ItemIdx);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getArchieveDate()));
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(ArchType);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(ArchItem);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(Borrower);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getBorrowDate()));
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getReturnDate1()));
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getReturnDate2()));
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(Confirmer);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(ItemCode);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("SeriesNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SeriesNo = FValue.trim();
            }
            else
            {
                SeriesNo = null;
            }
        }
        if (FCode.equals("ArchieveNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ArchieveNo = FValue.trim();
            }
            else
            {
                ArchieveNo = null;
            }
        }
        if (FCode.equals("ItemIdx"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                ItemIdx = i;
            }
        }
        if (FCode.equals("ArchieveDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ArchieveDate = fDate.getDate(FValue);
            }
            else
            {
                ArchieveDate = null;
            }
        }
        if (FCode.equals("ArchType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ArchType = FValue.trim();
            }
            else
            {
                ArchType = null;
            }
        }
        if (FCode.equals("ArchItem"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ArchItem = FValue.trim();
            }
            else
            {
                ArchItem = null;
            }
        }
        if (FCode.equals("Borrower"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Borrower = FValue.trim();
            }
            else
            {
                Borrower = null;
            }
        }
        if (FCode.equals("BorrowDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BorrowDate = fDate.getDate(FValue);
            }
            else
            {
                BorrowDate = null;
            }
        }
        if (FCode.equals("ReturnDate1"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ReturnDate1 = fDate.getDate(FValue);
            }
            else
            {
                ReturnDate1 = null;
            }
        }
        if (FCode.equals("ReturnDate2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ReturnDate2 = fDate.getDate(FValue);
            }
            else
            {
                ReturnDate2 = null;
            }
        }
        if (FCode.equals("Confirmer"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Confirmer = FValue.trim();
            }
            else
            {
                Confirmer = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("ItemCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ItemCode = FValue.trim();
            }
            else
            {
                ItemCode = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LAArchieveLogSchema other = (LAArchieveLogSchema) otherObject;
        return
                SeriesNo.equals(other.getSeriesNo())
                && ArchieveNo.equals(other.getArchieveNo())
                && ItemIdx == other.getItemIdx()
                && fDate.getString(ArchieveDate).equals(other.getArchieveDate())
                && ArchType.equals(other.getArchType())
                && ArchItem.equals(other.getArchItem())
                && Borrower.equals(other.getBorrower())
                && fDate.getString(BorrowDate).equals(other.getBorrowDate())
                && fDate.getString(ReturnDate1).equals(other.getReturnDate1())
                && fDate.getString(ReturnDate2).equals(other.getReturnDate2())
                && Confirmer.equals(other.getConfirmer())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && Operator.equals(other.getOperator())
                && ItemCode.equals(other.getItemCode());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("SeriesNo"))
        {
            return 0;
        }
        if (strFieldName.equals("ArchieveNo"))
        {
            return 1;
        }
        if (strFieldName.equals("ItemIdx"))
        {
            return 2;
        }
        if (strFieldName.equals("ArchieveDate"))
        {
            return 3;
        }
        if (strFieldName.equals("ArchType"))
        {
            return 4;
        }
        if (strFieldName.equals("ArchItem"))
        {
            return 5;
        }
        if (strFieldName.equals("Borrower"))
        {
            return 6;
        }
        if (strFieldName.equals("BorrowDate"))
        {
            return 7;
        }
        if (strFieldName.equals("ReturnDate1"))
        {
            return 8;
        }
        if (strFieldName.equals("ReturnDate2"))
        {
            return 9;
        }
        if (strFieldName.equals("Confirmer"))
        {
            return 10;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 11;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 12;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 13;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 14;
        }
        if (strFieldName.equals("Operator"))
        {
            return 15;
        }
        if (strFieldName.equals("ItemCode"))
        {
            return 16;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "SeriesNo";
                break;
            case 1:
                strFieldName = "ArchieveNo";
                break;
            case 2:
                strFieldName = "ItemIdx";
                break;
            case 3:
                strFieldName = "ArchieveDate";
                break;
            case 4:
                strFieldName = "ArchType";
                break;
            case 5:
                strFieldName = "ArchItem";
                break;
            case 6:
                strFieldName = "Borrower";
                break;
            case 7:
                strFieldName = "BorrowDate";
                break;
            case 8:
                strFieldName = "ReturnDate1";
                break;
            case 9:
                strFieldName = "ReturnDate2";
                break;
            case 10:
                strFieldName = "Confirmer";
                break;
            case 11:
                strFieldName = "MakeDate";
                break;
            case 12:
                strFieldName = "MakeTime";
                break;
            case 13:
                strFieldName = "ModifyDate";
                break;
            case 14:
                strFieldName = "ModifyTime";
                break;
            case 15:
                strFieldName = "Operator";
                break;
            case 16:
                strFieldName = "ItemCode";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("SeriesNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ArchieveNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ItemIdx"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("ArchieveDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ArchType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ArchItem"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Borrower"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BorrowDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ReturnDate1"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ReturnDate2"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("Confirmer"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ItemCode"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_INT;
                break;
            case 3:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 8:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 9:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
