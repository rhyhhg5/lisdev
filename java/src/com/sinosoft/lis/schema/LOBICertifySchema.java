/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LOBICertifyDB;

/*
 * <p>ClassName: LOBICertifySchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 卡式业务
 * @CreateDate：2010-02-08
 */
public class LOBICertifySchema implements Schema, Cloneable
{
	// @Field
	/** 流水号 */
	private String SerialNo;
	/** 批次号 */
	private String BatchNo;
	/** 磁盘导入顺序 */
	private String DiskSeqNo;
	/** 团体合同号 */
	private String GrpContNo;
	/** 集体投保单号 */
	private String ProposalGrpContNo;
	/** 印刷号 */
	private String PrtNo;
	/** 分单合同号 */
	private String ContNo;
	/** 分单投保单号 */
	private String ProposalContNo;
	/** 销售机构 */
	private String SaleChnl;
	/** 代理机构 */
	private String AgentCom;
	/** 业务员代码 */
	private String AgentCode;
	/** 业务员姓名 */
	private String AgentName;
	/** 单证号 */
	private String CardNo;
	/** 管理机构 */
	private String ManangeCom;
	/** 单证类别 */
	private String CertifyCode;
	/** 单证价值 */
	private double Prem;
	/** 保额 */
	private double Amnt;
	/** 档次 */
	private double Mult;
	/** 份数 */
	private double Copys;
	/** 生效日期 */
	private Date CValidate;
	/** 承保人数 */
	private int InsuPeoples;
	/** 单证状态 */
	private String State;
	/** 操作员 */
	private String Operator;
	/** 创建日期 */
	private Date MakeDate;
	/** 创建时间 */
	private String MakeTime;
	/** 修改日期 */
	private Date ModifyDate;
	/** 修改时间 */
	private String ModifyTime;
	/** 激活日期 */
	private Date ActiveDate;
	/** 激活标志 */
	private String ActiveFlag;
	/** 结算单录入日期 */
	private Date SettlementInputDate;
	/** 结算录入人 */
	private String SettlementInputOperator;
	/** 实名化状态 */
	private String WSState;
	/** 集团交叉销售渠道 */
	private String Crs_SaleChnl;
	/** 集团销售业务类型 */
	private String Crs_BussType;
	/** 集团代理机构 */
	private String GrpAgentCom;
	/** 集团代理人 */
	private String GrpAgentCode;
	/** 集团代理人姓名 */
	private String GrpAgentName;
	/** 集团代理人身份证 */
	private String GrpAgentIDNo;
	/** 单证有效状态 */
	private String CertifyStatus;

	public static final int FIELDNUM = 39;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LOBICertifySchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "SerialNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LOBICertifySchema cloned = (LOBICertifySchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getSerialNo()
	{
		return SerialNo;
	}
	public void setSerialNo(String aSerialNo)
	{
		SerialNo = aSerialNo;
	}
	public String getBatchNo()
	{
		return BatchNo;
	}
	public void setBatchNo(String aBatchNo)
	{
		BatchNo = aBatchNo;
	}
	public String getDiskSeqNo()
	{
		return DiskSeqNo;
	}
	public void setDiskSeqNo(String aDiskSeqNo)
	{
		DiskSeqNo = aDiskSeqNo;
	}
	public String getGrpContNo()
	{
		return GrpContNo;
	}
	public void setGrpContNo(String aGrpContNo)
	{
		GrpContNo = aGrpContNo;
	}
	public String getProposalGrpContNo()
	{
		return ProposalGrpContNo;
	}
	public void setProposalGrpContNo(String aProposalGrpContNo)
	{
		ProposalGrpContNo = aProposalGrpContNo;
	}
	public String getPrtNo()
	{
		return PrtNo;
	}
	public void setPrtNo(String aPrtNo)
	{
		PrtNo = aPrtNo;
	}
	public String getContNo()
	{
		return ContNo;
	}
	public void setContNo(String aContNo)
	{
		ContNo = aContNo;
	}
	public String getProposalContNo()
	{
		return ProposalContNo;
	}
	public void setProposalContNo(String aProposalContNo)
	{
		ProposalContNo = aProposalContNo;
	}
	public String getSaleChnl()
	{
		return SaleChnl;
	}
	public void setSaleChnl(String aSaleChnl)
	{
		SaleChnl = aSaleChnl;
	}
	public String getAgentCom()
	{
		return AgentCom;
	}
	public void setAgentCom(String aAgentCom)
	{
		AgentCom = aAgentCom;
	}
	public String getAgentCode()
	{
		return AgentCode;
	}
	public void setAgentCode(String aAgentCode)
	{
		AgentCode = aAgentCode;
	}
	public String getAgentName()
	{
		return AgentName;
	}
	public void setAgentName(String aAgentName)
	{
		AgentName = aAgentName;
	}
	public String getCardNo()
	{
		return CardNo;
	}
	public void setCardNo(String aCardNo)
	{
		CardNo = aCardNo;
	}
	public String getManangeCom()
	{
		return ManangeCom;
	}
	public void setManangeCom(String aManangeCom)
	{
		ManangeCom = aManangeCom;
	}
	public String getCertifyCode()
	{
		return CertifyCode;
	}
	public void setCertifyCode(String aCertifyCode)
	{
		CertifyCode = aCertifyCode;
	}
	public double getPrem()
	{
		return Prem;
	}
	public void setPrem(double aPrem)
	{
		Prem = Arith.round(aPrem,2);
	}
	public void setPrem(String aPrem)
	{
		if (aPrem != null && !aPrem.equals(""))
		{
			Double tDouble = new Double(aPrem);
			double d = tDouble.doubleValue();
                Prem = Arith.round(d,2);
		}
	}

	public double getAmnt()
	{
		return Amnt;
	}
	public void setAmnt(double aAmnt)
	{
		Amnt = Arith.round(aAmnt,2);
	}
	public void setAmnt(String aAmnt)
	{
		if (aAmnt != null && !aAmnt.equals(""))
		{
			Double tDouble = new Double(aAmnt);
			double d = tDouble.doubleValue();
                Amnt = Arith.round(d,2);
		}
	}

	public double getMult()
	{
		return Mult;
	}
	public void setMult(double aMult)
	{
		Mult = Arith.round(aMult,5);
	}
	public void setMult(String aMult)
	{
		if (aMult != null && !aMult.equals(""))
		{
			Double tDouble = new Double(aMult);
			double d = tDouble.doubleValue();
                Mult = Arith.round(d,5);
		}
	}

	public double getCopys()
	{
		return Copys;
	}
	public void setCopys(double aCopys)
	{
		Copys = Arith.round(aCopys,5);
	}
	public void setCopys(String aCopys)
	{
		if (aCopys != null && !aCopys.equals(""))
		{
			Double tDouble = new Double(aCopys);
			double d = tDouble.doubleValue();
                Copys = Arith.round(d,5);
		}
	}

	public String getCValidate()
	{
		if( CValidate != null )
			return fDate.getString(CValidate);
		else
			return null;
	}
	public void setCValidate(Date aCValidate)
	{
		CValidate = aCValidate;
	}
	public void setCValidate(String aCValidate)
	{
		if (aCValidate != null && !aCValidate.equals("") )
		{
			CValidate = fDate.getDate( aCValidate );
		}
		else
			CValidate = null;
	}

	public int getInsuPeoples()
	{
		return InsuPeoples;
	}
	public void setInsuPeoples(int aInsuPeoples)
	{
		InsuPeoples = aInsuPeoples;
	}
	public void setInsuPeoples(String aInsuPeoples)
	{
		if (aInsuPeoples != null && !aInsuPeoples.equals(""))
		{
			Integer tInteger = new Integer(aInsuPeoples);
			int i = tInteger.intValue();
			InsuPeoples = i;
		}
	}

	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getActiveDate()
	{
		if( ActiveDate != null )
			return fDate.getString(ActiveDate);
		else
			return null;
	}
	public void setActiveDate(Date aActiveDate)
	{
		ActiveDate = aActiveDate;
	}
	public void setActiveDate(String aActiveDate)
	{
		if (aActiveDate != null && !aActiveDate.equals("") )
		{
			ActiveDate = fDate.getDate( aActiveDate );
		}
		else
			ActiveDate = null;
	}

	public String getActiveFlag()
	{
		return ActiveFlag;
	}
	public void setActiveFlag(String aActiveFlag)
	{
		ActiveFlag = aActiveFlag;
	}
	public String getSettlementInputDate()
	{
		if( SettlementInputDate != null )
			return fDate.getString(SettlementInputDate);
		else
			return null;
	}
	public void setSettlementInputDate(Date aSettlementInputDate)
	{
		SettlementInputDate = aSettlementInputDate;
	}
	public void setSettlementInputDate(String aSettlementInputDate)
	{
		if (aSettlementInputDate != null && !aSettlementInputDate.equals("") )
		{
			SettlementInputDate = fDate.getDate( aSettlementInputDate );
		}
		else
			SettlementInputDate = null;
	}

	public String getSettlementInputOperator()
	{
		return SettlementInputOperator;
	}
	public void setSettlementInputOperator(String aSettlementInputOperator)
	{
		SettlementInputOperator = aSettlementInputOperator;
	}
	public String getWSState()
	{
		return WSState;
	}
	public void setWSState(String aWSState)
	{
		WSState = aWSState;
	}
	public String getCrs_SaleChnl()
	{
		return Crs_SaleChnl;
	}
	public void setCrs_SaleChnl(String aCrs_SaleChnl)
	{
		Crs_SaleChnl = aCrs_SaleChnl;
	}
	public String getCrs_BussType()
	{
		return Crs_BussType;
	}
	public void setCrs_BussType(String aCrs_BussType)
	{
		Crs_BussType = aCrs_BussType;
	}
	public String getGrpAgentCom()
	{
		return GrpAgentCom;
	}
	public void setGrpAgentCom(String aGrpAgentCom)
	{
		GrpAgentCom = aGrpAgentCom;
	}
	public String getGrpAgentCode()
	{
		return GrpAgentCode;
	}
	public void setGrpAgentCode(String aGrpAgentCode)
	{
		GrpAgentCode = aGrpAgentCode;
	}
	public String getGrpAgentName()
	{
		return GrpAgentName;
	}
	public void setGrpAgentName(String aGrpAgentName)
	{
		GrpAgentName = aGrpAgentName;
	}
	public String getGrpAgentIDNo()
	{
		return GrpAgentIDNo;
	}
	public void setGrpAgentIDNo(String aGrpAgentIDNo)
	{
		GrpAgentIDNo = aGrpAgentIDNo;
	}
	public String getCertifyStatus()
	{
		return CertifyStatus;
	}
	public void setCertifyStatus(String aCertifyStatus)
	{
		CertifyStatus = aCertifyStatus;
	}

	/**
	* 使用另外一个 LOBICertifySchema 对象给 Schema 赋值
	* @param: aLOBICertifySchema LOBICertifySchema
	**/
	public void setSchema(LOBICertifySchema aLOBICertifySchema)
	{
		this.SerialNo = aLOBICertifySchema.getSerialNo();
		this.BatchNo = aLOBICertifySchema.getBatchNo();
		this.DiskSeqNo = aLOBICertifySchema.getDiskSeqNo();
		this.GrpContNo = aLOBICertifySchema.getGrpContNo();
		this.ProposalGrpContNo = aLOBICertifySchema.getProposalGrpContNo();
		this.PrtNo = aLOBICertifySchema.getPrtNo();
		this.ContNo = aLOBICertifySchema.getContNo();
		this.ProposalContNo = aLOBICertifySchema.getProposalContNo();
		this.SaleChnl = aLOBICertifySchema.getSaleChnl();
		this.AgentCom = aLOBICertifySchema.getAgentCom();
		this.AgentCode = aLOBICertifySchema.getAgentCode();
		this.AgentName = aLOBICertifySchema.getAgentName();
		this.CardNo = aLOBICertifySchema.getCardNo();
		this.ManangeCom = aLOBICertifySchema.getManangeCom();
		this.CertifyCode = aLOBICertifySchema.getCertifyCode();
		this.Prem = aLOBICertifySchema.getPrem();
		this.Amnt = aLOBICertifySchema.getAmnt();
		this.Mult = aLOBICertifySchema.getMult();
		this.Copys = aLOBICertifySchema.getCopys();
		this.CValidate = fDate.getDate( aLOBICertifySchema.getCValidate());
		this.InsuPeoples = aLOBICertifySchema.getInsuPeoples();
		this.State = aLOBICertifySchema.getState();
		this.Operator = aLOBICertifySchema.getOperator();
		this.MakeDate = fDate.getDate( aLOBICertifySchema.getMakeDate());
		this.MakeTime = aLOBICertifySchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLOBICertifySchema.getModifyDate());
		this.ModifyTime = aLOBICertifySchema.getModifyTime();
		this.ActiveDate = fDate.getDate( aLOBICertifySchema.getActiveDate());
		this.ActiveFlag = aLOBICertifySchema.getActiveFlag();
		this.SettlementInputDate = fDate.getDate( aLOBICertifySchema.getSettlementInputDate());
		this.SettlementInputOperator = aLOBICertifySchema.getSettlementInputOperator();
		this.WSState = aLOBICertifySchema.getWSState();
		this.Crs_SaleChnl = aLOBICertifySchema.getCrs_SaleChnl();
		this.Crs_BussType = aLOBICertifySchema.getCrs_BussType();
		this.GrpAgentCom = aLOBICertifySchema.getGrpAgentCom();
		this.GrpAgentCode = aLOBICertifySchema.getGrpAgentCode();
		this.GrpAgentName = aLOBICertifySchema.getGrpAgentName();
		this.GrpAgentIDNo = aLOBICertifySchema.getGrpAgentIDNo();
		this.CertifyStatus = aLOBICertifySchema.getCertifyStatus();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("SerialNo") == null )
				this.SerialNo = null;
			else
				this.SerialNo = rs.getString("SerialNo").trim();

			if( rs.getString("BatchNo") == null )
				this.BatchNo = null;
			else
				this.BatchNo = rs.getString("BatchNo").trim();

			if( rs.getString("DiskSeqNo") == null )
				this.DiskSeqNo = null;
			else
				this.DiskSeqNo = rs.getString("DiskSeqNo").trim();

			if( rs.getString("GrpContNo") == null )
				this.GrpContNo = null;
			else
				this.GrpContNo = rs.getString("GrpContNo").trim();

			if( rs.getString("ProposalGrpContNo") == null )
				this.ProposalGrpContNo = null;
			else
				this.ProposalGrpContNo = rs.getString("ProposalGrpContNo").trim();

			if( rs.getString("PrtNo") == null )
				this.PrtNo = null;
			else
				this.PrtNo = rs.getString("PrtNo").trim();

			if( rs.getString("ContNo") == null )
				this.ContNo = null;
			else
				this.ContNo = rs.getString("ContNo").trim();

			if( rs.getString("ProposalContNo") == null )
				this.ProposalContNo = null;
			else
				this.ProposalContNo = rs.getString("ProposalContNo").trim();

			if( rs.getString("SaleChnl") == null )
				this.SaleChnl = null;
			else
				this.SaleChnl = rs.getString("SaleChnl").trim();

			if( rs.getString("AgentCom") == null )
				this.AgentCom = null;
			else
				this.AgentCom = rs.getString("AgentCom").trim();

			if( rs.getString("AgentCode") == null )
				this.AgentCode = null;
			else
				this.AgentCode = rs.getString("AgentCode").trim();

			if( rs.getString("AgentName") == null )
				this.AgentName = null;
			else
				this.AgentName = rs.getString("AgentName").trim();

			if( rs.getString("CardNo") == null )
				this.CardNo = null;
			else
				this.CardNo = rs.getString("CardNo").trim();

			if( rs.getString("ManangeCom") == null )
				this.ManangeCom = null;
			else
				this.ManangeCom = rs.getString("ManangeCom").trim();

			if( rs.getString("CertifyCode") == null )
				this.CertifyCode = null;
			else
				this.CertifyCode = rs.getString("CertifyCode").trim();

			this.Prem = rs.getDouble("Prem");
			this.Amnt = rs.getDouble("Amnt");
			this.Mult = rs.getDouble("Mult");
			this.Copys = rs.getDouble("Copys");
			this.CValidate = rs.getDate("CValidate");
			this.InsuPeoples = rs.getInt("InsuPeoples");
			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			this.ActiveDate = rs.getDate("ActiveDate");
			if( rs.getString("ActiveFlag") == null )
				this.ActiveFlag = null;
			else
				this.ActiveFlag = rs.getString("ActiveFlag").trim();

			this.SettlementInputDate = rs.getDate("SettlementInputDate");
			if( rs.getString("SettlementInputOperator") == null )
				this.SettlementInputOperator = null;
			else
				this.SettlementInputOperator = rs.getString("SettlementInputOperator").trim();

			if( rs.getString("WSState") == null )
				this.WSState = null;
			else
				this.WSState = rs.getString("WSState").trim();

			if( rs.getString("Crs_SaleChnl") == null )
				this.Crs_SaleChnl = null;
			else
				this.Crs_SaleChnl = rs.getString("Crs_SaleChnl").trim();

			if( rs.getString("Crs_BussType") == null )
				this.Crs_BussType = null;
			else
				this.Crs_BussType = rs.getString("Crs_BussType").trim();

			if( rs.getString("GrpAgentCom") == null )
				this.GrpAgentCom = null;
			else
				this.GrpAgentCom = rs.getString("GrpAgentCom").trim();

			if( rs.getString("GrpAgentCode") == null )
				this.GrpAgentCode = null;
			else
				this.GrpAgentCode = rs.getString("GrpAgentCode").trim();

			if( rs.getString("GrpAgentName") == null )
				this.GrpAgentName = null;
			else
				this.GrpAgentName = rs.getString("GrpAgentName").trim();

			if( rs.getString("GrpAgentIDNo") == null )
				this.GrpAgentIDNo = null;
			else
				this.GrpAgentIDNo = rs.getString("GrpAgentIDNo").trim();

			if( rs.getString("CertifyStatus") == null )
				this.CertifyStatus = null;
			else
				this.CertifyStatus = rs.getString("CertifyStatus").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LOBICertify表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LOBICertifySchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LOBICertifySchema getSchema()
	{
		LOBICertifySchema aLOBICertifySchema = new LOBICertifySchema();
		aLOBICertifySchema.setSchema(this);
		return aLOBICertifySchema;
	}

	public LOBICertifyDB getDB()
	{
		LOBICertifyDB aDBOper = new LOBICertifyDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOBICertify描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(SerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BatchNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DiskSeqNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProposalGrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PrtNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProposalContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SaleChnl)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CardNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManangeCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CertifyCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Prem));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Amnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Mult));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Copys));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CValidate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(InsuPeoples));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ActiveDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ActiveFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( SettlementInputDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SettlementInputOperator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(WSState)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Crs_SaleChnl)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Crs_BussType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpAgentCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpAgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpAgentName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpAgentIDNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CertifyStatus));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOBICertify>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			BatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			DiskSeqNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			ProposalGrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			ProposalContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			AgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			AgentName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			CardNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			ManangeCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			CertifyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			Prem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,16,SysConst.PACKAGESPILTER))).doubleValue();
			Amnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,17,SysConst.PACKAGESPILTER))).doubleValue();
			Mult = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,18,SysConst.PACKAGESPILTER))).doubleValue();
			Copys = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,19,SysConst.PACKAGESPILTER))).doubleValue();
			CValidate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,SysConst.PACKAGESPILTER));
			InsuPeoples= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,21,SysConst.PACKAGESPILTER))).intValue();
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			ActiveDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28,SysConst.PACKAGESPILTER));
			ActiveFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			SettlementInputDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30,SysConst.PACKAGESPILTER));
			SettlementInputOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			WSState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
			Crs_SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			Crs_BussType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			GrpAgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			GrpAgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
			GrpAgentName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
			GrpAgentIDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
			CertifyStatus = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LOBICertifySchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("SerialNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
		}
		if (FCode.equals("BatchNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BatchNo));
		}
		if (FCode.equals("DiskSeqNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DiskSeqNo));
		}
		if (FCode.equals("GrpContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
		}
		if (FCode.equals("ProposalGrpContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProposalGrpContNo));
		}
		if (FCode.equals("PrtNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
		}
		if (FCode.equals("ContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
		}
		if (FCode.equals("ProposalContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProposalContNo));
		}
		if (FCode.equals("SaleChnl"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
		}
		if (FCode.equals("AgentCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
		}
		if (FCode.equals("AgentCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
		}
		if (FCode.equals("AgentName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentName));
		}
		if (FCode.equals("CardNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CardNo));
		}
		if (FCode.equals("ManangeCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManangeCom));
		}
		if (FCode.equals("CertifyCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CertifyCode));
		}
		if (FCode.equals("Prem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Prem));
		}
		if (FCode.equals("Amnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Amnt));
		}
		if (FCode.equals("Mult"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Mult));
		}
		if (FCode.equals("Copys"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Copys));
		}
		if (FCode.equals("CValidate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCValidate()));
		}
		if (FCode.equals("InsuPeoples"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuPeoples));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("ActiveDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getActiveDate()));
		}
		if (FCode.equals("ActiveFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ActiveFlag));
		}
		if (FCode.equals("SettlementInputDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getSettlementInputDate()));
		}
		if (FCode.equals("SettlementInputOperator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SettlementInputOperator));
		}
		if (FCode.equals("WSState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WSState));
		}
		if (FCode.equals("Crs_SaleChnl"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Crs_SaleChnl));
		}
		if (FCode.equals("Crs_BussType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Crs_BussType));
		}
		if (FCode.equals("GrpAgentCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpAgentCom));
		}
		if (FCode.equals("GrpAgentCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpAgentCode));
		}
		if (FCode.equals("GrpAgentName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpAgentName));
		}
		if (FCode.equals("GrpAgentIDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpAgentIDNo));
		}
		if (FCode.equals("CertifyStatus"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CertifyStatus));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(SerialNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(BatchNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(DiskSeqNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(GrpContNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(ProposalGrpContNo);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(PrtNo);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(ContNo);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(ProposalContNo);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(SaleChnl);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(AgentCom);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(AgentCode);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(AgentName);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(CardNo);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(ManangeCom);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(CertifyCode);
				break;
			case 15:
				strFieldValue = String.valueOf(Prem);
				break;
			case 16:
				strFieldValue = String.valueOf(Amnt);
				break;
			case 17:
				strFieldValue = String.valueOf(Mult);
				break;
			case 18:
				strFieldValue = String.valueOf(Copys);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCValidate()));
				break;
			case 20:
				strFieldValue = String.valueOf(InsuPeoples);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getActiveDate()));
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(ActiveFlag);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getSettlementInputDate()));
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(SettlementInputOperator);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(WSState);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(Crs_SaleChnl);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(Crs_BussType);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(GrpAgentCom);
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(GrpAgentCode);
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(GrpAgentName);
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(GrpAgentIDNo);
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(CertifyStatus);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("SerialNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SerialNo = FValue.trim();
			}
			else
				SerialNo = null;
		}
		if (FCode.equalsIgnoreCase("BatchNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BatchNo = FValue.trim();
			}
			else
				BatchNo = null;
		}
		if (FCode.equalsIgnoreCase("DiskSeqNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DiskSeqNo = FValue.trim();
			}
			else
				DiskSeqNo = null;
		}
		if (FCode.equalsIgnoreCase("GrpContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpContNo = FValue.trim();
			}
			else
				GrpContNo = null;
		}
		if (FCode.equalsIgnoreCase("ProposalGrpContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProposalGrpContNo = FValue.trim();
			}
			else
				ProposalGrpContNo = null;
		}
		if (FCode.equalsIgnoreCase("PrtNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PrtNo = FValue.trim();
			}
			else
				PrtNo = null;
		}
		if (FCode.equalsIgnoreCase("ContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContNo = FValue.trim();
			}
			else
				ContNo = null;
		}
		if (FCode.equalsIgnoreCase("ProposalContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProposalContNo = FValue.trim();
			}
			else
				ProposalContNo = null;
		}
		if (FCode.equalsIgnoreCase("SaleChnl"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SaleChnl = FValue.trim();
			}
			else
				SaleChnl = null;
		}
		if (FCode.equalsIgnoreCase("AgentCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCom = FValue.trim();
			}
			else
				AgentCom = null;
		}
		if (FCode.equalsIgnoreCase("AgentCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCode = FValue.trim();
			}
			else
				AgentCode = null;
		}
		if (FCode.equalsIgnoreCase("AgentName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentName = FValue.trim();
			}
			else
				AgentName = null;
		}
		if (FCode.equalsIgnoreCase("CardNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CardNo = FValue.trim();
			}
			else
				CardNo = null;
		}
		if (FCode.equalsIgnoreCase("ManangeCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManangeCom = FValue.trim();
			}
			else
				ManangeCom = null;
		}
		if (FCode.equalsIgnoreCase("CertifyCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CertifyCode = FValue.trim();
			}
			else
				CertifyCode = null;
		}
		if (FCode.equalsIgnoreCase("Prem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Prem = d;
			}
		}
		if (FCode.equalsIgnoreCase("Amnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Amnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("Mult"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Mult = d;
			}
		}
		if (FCode.equalsIgnoreCase("Copys"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Copys = d;
			}
		}
		if (FCode.equalsIgnoreCase("CValidate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CValidate = fDate.getDate( FValue );
			}
			else
				CValidate = null;
		}
		if (FCode.equalsIgnoreCase("InsuPeoples"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				InsuPeoples = i;
			}
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("ActiveDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ActiveDate = fDate.getDate( FValue );
			}
			else
				ActiveDate = null;
		}
		if (FCode.equalsIgnoreCase("ActiveFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ActiveFlag = FValue.trim();
			}
			else
				ActiveFlag = null;
		}
		if (FCode.equalsIgnoreCase("SettlementInputDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				SettlementInputDate = fDate.getDate( FValue );
			}
			else
				SettlementInputDate = null;
		}
		if (FCode.equalsIgnoreCase("SettlementInputOperator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SettlementInputOperator = FValue.trim();
			}
			else
				SettlementInputOperator = null;
		}
		if (FCode.equalsIgnoreCase("WSState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WSState = FValue.trim();
			}
			else
				WSState = null;
		}
		if (FCode.equalsIgnoreCase("Crs_SaleChnl"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Crs_SaleChnl = FValue.trim();
			}
			else
				Crs_SaleChnl = null;
		}
		if (FCode.equalsIgnoreCase("Crs_BussType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Crs_BussType = FValue.trim();
			}
			else
				Crs_BussType = null;
		}
		if (FCode.equalsIgnoreCase("GrpAgentCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpAgentCom = FValue.trim();
			}
			else
				GrpAgentCom = null;
		}
		if (FCode.equalsIgnoreCase("GrpAgentCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpAgentCode = FValue.trim();
			}
			else
				GrpAgentCode = null;
		}
		if (FCode.equalsIgnoreCase("GrpAgentName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpAgentName = FValue.trim();
			}
			else
				GrpAgentName = null;
		}
		if (FCode.equalsIgnoreCase("GrpAgentIDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpAgentIDNo = FValue.trim();
			}
			else
				GrpAgentIDNo = null;
		}
		if (FCode.equalsIgnoreCase("CertifyStatus"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CertifyStatus = FValue.trim();
			}
			else
				CertifyStatus = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LOBICertifySchema other = (LOBICertifySchema)otherObject;
		return
			(SerialNo == null ? other.getSerialNo() == null : SerialNo.equals(other.getSerialNo()))
			&& (BatchNo == null ? other.getBatchNo() == null : BatchNo.equals(other.getBatchNo()))
			&& (DiskSeqNo == null ? other.getDiskSeqNo() == null : DiskSeqNo.equals(other.getDiskSeqNo()))
			&& (GrpContNo == null ? other.getGrpContNo() == null : GrpContNo.equals(other.getGrpContNo()))
			&& (ProposalGrpContNo == null ? other.getProposalGrpContNo() == null : ProposalGrpContNo.equals(other.getProposalGrpContNo()))
			&& (PrtNo == null ? other.getPrtNo() == null : PrtNo.equals(other.getPrtNo()))
			&& (ContNo == null ? other.getContNo() == null : ContNo.equals(other.getContNo()))
			&& (ProposalContNo == null ? other.getProposalContNo() == null : ProposalContNo.equals(other.getProposalContNo()))
			&& (SaleChnl == null ? other.getSaleChnl() == null : SaleChnl.equals(other.getSaleChnl()))
			&& (AgentCom == null ? other.getAgentCom() == null : AgentCom.equals(other.getAgentCom()))
			&& (AgentCode == null ? other.getAgentCode() == null : AgentCode.equals(other.getAgentCode()))
			&& (AgentName == null ? other.getAgentName() == null : AgentName.equals(other.getAgentName()))
			&& (CardNo == null ? other.getCardNo() == null : CardNo.equals(other.getCardNo()))
			&& (ManangeCom == null ? other.getManangeCom() == null : ManangeCom.equals(other.getManangeCom()))
			&& (CertifyCode == null ? other.getCertifyCode() == null : CertifyCode.equals(other.getCertifyCode()))
			&& Prem == other.getPrem()
			&& Amnt == other.getAmnt()
			&& Mult == other.getMult()
			&& Copys == other.getCopys()
			&& (CValidate == null ? other.getCValidate() == null : fDate.getString(CValidate).equals(other.getCValidate()))
			&& InsuPeoples == other.getInsuPeoples()
			&& (State == null ? other.getState() == null : State.equals(other.getState()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (ActiveDate == null ? other.getActiveDate() == null : fDate.getString(ActiveDate).equals(other.getActiveDate()))
			&& (ActiveFlag == null ? other.getActiveFlag() == null : ActiveFlag.equals(other.getActiveFlag()))
			&& (SettlementInputDate == null ? other.getSettlementInputDate() == null : fDate.getString(SettlementInputDate).equals(other.getSettlementInputDate()))
			&& (SettlementInputOperator == null ? other.getSettlementInputOperator() == null : SettlementInputOperator.equals(other.getSettlementInputOperator()))
			&& (WSState == null ? other.getWSState() == null : WSState.equals(other.getWSState()))
			&& (Crs_SaleChnl == null ? other.getCrs_SaleChnl() == null : Crs_SaleChnl.equals(other.getCrs_SaleChnl()))
			&& (Crs_BussType == null ? other.getCrs_BussType() == null : Crs_BussType.equals(other.getCrs_BussType()))
			&& (GrpAgentCom == null ? other.getGrpAgentCom() == null : GrpAgentCom.equals(other.getGrpAgentCom()))
			&& (GrpAgentCode == null ? other.getGrpAgentCode() == null : GrpAgentCode.equals(other.getGrpAgentCode()))
			&& (GrpAgentName == null ? other.getGrpAgentName() == null : GrpAgentName.equals(other.getGrpAgentName()))
			&& (GrpAgentIDNo == null ? other.getGrpAgentIDNo() == null : GrpAgentIDNo.equals(other.getGrpAgentIDNo()))
			&& (CertifyStatus == null ? other.getCertifyStatus() == null : CertifyStatus.equals(other.getCertifyStatus()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("SerialNo") ) {
			return 0;
		}
		if( strFieldName.equals("BatchNo") ) {
			return 1;
		}
		if( strFieldName.equals("DiskSeqNo") ) {
			return 2;
		}
		if( strFieldName.equals("GrpContNo") ) {
			return 3;
		}
		if( strFieldName.equals("ProposalGrpContNo") ) {
			return 4;
		}
		if( strFieldName.equals("PrtNo") ) {
			return 5;
		}
		if( strFieldName.equals("ContNo") ) {
			return 6;
		}
		if( strFieldName.equals("ProposalContNo") ) {
			return 7;
		}
		if( strFieldName.equals("SaleChnl") ) {
			return 8;
		}
		if( strFieldName.equals("AgentCom") ) {
			return 9;
		}
		if( strFieldName.equals("AgentCode") ) {
			return 10;
		}
		if( strFieldName.equals("AgentName") ) {
			return 11;
		}
		if( strFieldName.equals("CardNo") ) {
			return 12;
		}
		if( strFieldName.equals("ManangeCom") ) {
			return 13;
		}
		if( strFieldName.equals("CertifyCode") ) {
			return 14;
		}
		if( strFieldName.equals("Prem") ) {
			return 15;
		}
		if( strFieldName.equals("Amnt") ) {
			return 16;
		}
		if( strFieldName.equals("Mult") ) {
			return 17;
		}
		if( strFieldName.equals("Copys") ) {
			return 18;
		}
		if( strFieldName.equals("CValidate") ) {
			return 19;
		}
		if( strFieldName.equals("InsuPeoples") ) {
			return 20;
		}
		if( strFieldName.equals("State") ) {
			return 21;
		}
		if( strFieldName.equals("Operator") ) {
			return 22;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 23;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 24;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 25;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 26;
		}
		if( strFieldName.equals("ActiveDate") ) {
			return 27;
		}
		if( strFieldName.equals("ActiveFlag") ) {
			return 28;
		}
		if( strFieldName.equals("SettlementInputDate") ) {
			return 29;
		}
		if( strFieldName.equals("SettlementInputOperator") ) {
			return 30;
		}
		if( strFieldName.equals("WSState") ) {
			return 31;
		}
		if( strFieldName.equals("Crs_SaleChnl") ) {
			return 32;
		}
		if( strFieldName.equals("Crs_BussType") ) {
			return 33;
		}
		if( strFieldName.equals("GrpAgentCom") ) {
			return 34;
		}
		if( strFieldName.equals("GrpAgentCode") ) {
			return 35;
		}
		if( strFieldName.equals("GrpAgentName") ) {
			return 36;
		}
		if( strFieldName.equals("GrpAgentIDNo") ) {
			return 37;
		}
		if( strFieldName.equals("CertifyStatus") ) {
			return 38;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "SerialNo";
				break;
			case 1:
				strFieldName = "BatchNo";
				break;
			case 2:
				strFieldName = "DiskSeqNo";
				break;
			case 3:
				strFieldName = "GrpContNo";
				break;
			case 4:
				strFieldName = "ProposalGrpContNo";
				break;
			case 5:
				strFieldName = "PrtNo";
				break;
			case 6:
				strFieldName = "ContNo";
				break;
			case 7:
				strFieldName = "ProposalContNo";
				break;
			case 8:
				strFieldName = "SaleChnl";
				break;
			case 9:
				strFieldName = "AgentCom";
				break;
			case 10:
				strFieldName = "AgentCode";
				break;
			case 11:
				strFieldName = "AgentName";
				break;
			case 12:
				strFieldName = "CardNo";
				break;
			case 13:
				strFieldName = "ManangeCom";
				break;
			case 14:
				strFieldName = "CertifyCode";
				break;
			case 15:
				strFieldName = "Prem";
				break;
			case 16:
				strFieldName = "Amnt";
				break;
			case 17:
				strFieldName = "Mult";
				break;
			case 18:
				strFieldName = "Copys";
				break;
			case 19:
				strFieldName = "CValidate";
				break;
			case 20:
				strFieldName = "InsuPeoples";
				break;
			case 21:
				strFieldName = "State";
				break;
			case 22:
				strFieldName = "Operator";
				break;
			case 23:
				strFieldName = "MakeDate";
				break;
			case 24:
				strFieldName = "MakeTime";
				break;
			case 25:
				strFieldName = "ModifyDate";
				break;
			case 26:
				strFieldName = "ModifyTime";
				break;
			case 27:
				strFieldName = "ActiveDate";
				break;
			case 28:
				strFieldName = "ActiveFlag";
				break;
			case 29:
				strFieldName = "SettlementInputDate";
				break;
			case 30:
				strFieldName = "SettlementInputOperator";
				break;
			case 31:
				strFieldName = "WSState";
				break;
			case 32:
				strFieldName = "Crs_SaleChnl";
				break;
			case 33:
				strFieldName = "Crs_BussType";
				break;
			case 34:
				strFieldName = "GrpAgentCom";
				break;
			case 35:
				strFieldName = "GrpAgentCode";
				break;
			case 36:
				strFieldName = "GrpAgentName";
				break;
			case 37:
				strFieldName = "GrpAgentIDNo";
				break;
			case 38:
				strFieldName = "CertifyStatus";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("SerialNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BatchNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DiskSeqNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ProposalGrpContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PrtNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ProposalContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SaleChnl") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CardNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManangeCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CertifyCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Prem") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Amnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Mult") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Copys") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("CValidate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("InsuPeoples") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ActiveDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ActiveFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SettlementInputDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("SettlementInputOperator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("WSState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Crs_SaleChnl") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Crs_BussType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpAgentCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpAgentCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpAgentName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpAgentIDNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CertifyStatus") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 16:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 17:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 18:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 19:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 20:
				nFieldType = Schema.TYPE_INT;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 36:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 37:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 38:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
