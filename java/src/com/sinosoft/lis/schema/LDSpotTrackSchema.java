/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LDSpotTrackDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

/*
 * <p>ClassName: LDSpotTrackSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 抽取
 * @CreateDate：2005-02-21
 */
public class LDSpotTrackSchema implements Schema
{
    // @Field
    /** 抽取批次号 */
    private String SpotID;
    /** 其他编码 */
    private String OtherNo;
    /** 其他编码类型 */
    private String OtherType;
    /** 抽取类型 */
    private String SpotType;
    /** 抽取状态 */
    private String SpotFlag;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;

    public static final int FIELDNUM = 7; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LDSpotTrackSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "SpotID";
        pk[1] = "OtherNo";
        pk[2] = "OtherType";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getSpotID()
    {
        if (SpotID != null && !SpotID.equals("") && SysConst.CHANGECHARSET == true)
        {
            SpotID = StrTool.unicodeToGBK(SpotID);
        }
        return SpotID;
    }

    public void setSpotID(String aSpotID)
    {
        SpotID = aSpotID;
    }

    public String getOtherNo()
    {
        if (OtherNo != null && !OtherNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            OtherNo = StrTool.unicodeToGBK(OtherNo);
        }
        return OtherNo;
    }

    public void setOtherNo(String aOtherNo)
    {
        OtherNo = aOtherNo;
    }

    public String getOtherType()
    {
        if (OtherType != null && !OtherType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            OtherType = StrTool.unicodeToGBK(OtherType);
        }
        return OtherType;
    }

    public void setOtherType(String aOtherType)
    {
        OtherType = aOtherType;
    }

    public String getSpotType()
    {
        if (SpotType != null && !SpotType.equals("") && SysConst.CHANGECHARSET == true)
        {
            SpotType = StrTool.unicodeToGBK(SpotType);
        }
        return SpotType;
    }

    public void setSpotType(String aSpotType)
    {
        SpotType = aSpotType;
    }

    public String getSpotFlag()
    {
        if (SpotFlag != null && !SpotFlag.equals("") && SysConst.CHANGECHARSET == true)
        {
            SpotFlag = StrTool.unicodeToGBK(SpotFlag);
        }
        return SpotFlag;
    }

    public void setSpotFlag(String aSpotFlag)
    {
        SpotFlag = aSpotFlag;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    /**
     * 使用另外一个 LDSpotTrackSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LDSpotTrackSchema aLDSpotTrackSchema)
    {
        this.SpotID = aLDSpotTrackSchema.getSpotID();
        this.OtherNo = aLDSpotTrackSchema.getOtherNo();
        this.OtherType = aLDSpotTrackSchema.getOtherType();
        this.SpotType = aLDSpotTrackSchema.getSpotType();
        this.SpotFlag = aLDSpotTrackSchema.getSpotFlag();
        this.MakeDate = fDate.getDate(aLDSpotTrackSchema.getMakeDate());
        this.MakeTime = aLDSpotTrackSchema.getMakeTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("SpotID") == null)
            {
                this.SpotID = null;
            }
            else
            {
                this.SpotID = rs.getString("SpotID").trim();
            }

            if (rs.getString("OtherNo") == null)
            {
                this.OtherNo = null;
            }
            else
            {
                this.OtherNo = rs.getString("OtherNo").trim();
            }

            if (rs.getString("OtherType") == null)
            {
                this.OtherType = null;
            }
            else
            {
                this.OtherType = rs.getString("OtherType").trim();
            }

            if (rs.getString("SpotType") == null)
            {
                this.SpotType = null;
            }
            else
            {
                this.SpotType = rs.getString("SpotType").trim();
            }

            if (rs.getString("SpotFlag") == null)
            {
                this.SpotFlag = null;
            }
            else
            {
                this.SpotFlag = rs.getString("SpotFlag").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDSpotTrackSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LDSpotTrackSchema getSchema()
    {
        LDSpotTrackSchema aLDSpotTrackSchema = new LDSpotTrackSchema();
        aLDSpotTrackSchema.setSchema(this);
        return aLDSpotTrackSchema;
    }

    public LDSpotTrackDB getDB()
    {
        LDSpotTrackDB aDBOper = new LDSpotTrackDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDSpotTrack描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(SpotID)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(OtherNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(OtherType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SpotType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SpotFlag)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDSpotTrack>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            SpotID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                    SysConst.PACKAGESPILTER);
            OtherNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                     SysConst.PACKAGESPILTER);
            OtherType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                       SysConst.PACKAGESPILTER);
            SpotType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                      SysConst.PACKAGESPILTER);
            SpotFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 6, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                      SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDSpotTrackSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("SpotID"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SpotID));
        }
        if (FCode.equals("OtherNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(OtherNo));
        }
        if (FCode.equals("OtherType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(OtherType));
        }
        if (FCode.equals("SpotType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SpotType));
        }
        if (FCode.equals("SpotFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SpotFlag));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(SpotID);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(OtherNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(OtherType);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(SpotType);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(SpotFlag);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("SpotID"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SpotID = FValue.trim();
            }
            else
            {
                SpotID = null;
            }
        }
        if (FCode.equals("OtherNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OtherNo = FValue.trim();
            }
            else
            {
                OtherNo = null;
            }
        }
        if (FCode.equals("OtherType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OtherType = FValue.trim();
            }
            else
            {
                OtherType = null;
            }
        }
        if (FCode.equals("SpotType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SpotType = FValue.trim();
            }
            else
            {
                SpotType = null;
            }
        }
        if (FCode.equals("SpotFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SpotFlag = FValue.trim();
            }
            else
            {
                SpotFlag = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LDSpotTrackSchema other = (LDSpotTrackSchema) otherObject;
        return
                SpotID.equals(other.getSpotID())
                && OtherNo.equals(other.getOtherNo())
                && OtherType.equals(other.getOtherType())
                && SpotType.equals(other.getSpotType())
                && SpotFlag.equals(other.getSpotFlag())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("SpotID"))
        {
            return 0;
        }
        if (strFieldName.equals("OtherNo"))
        {
            return 1;
        }
        if (strFieldName.equals("OtherType"))
        {
            return 2;
        }
        if (strFieldName.equals("SpotType"))
        {
            return 3;
        }
        if (strFieldName.equals("SpotFlag"))
        {
            return 4;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 5;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 6;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "SpotID";
                break;
            case 1:
                strFieldName = "OtherNo";
                break;
            case 2:
                strFieldName = "OtherType";
                break;
            case 3:
                strFieldName = "SpotType";
                break;
            case 4:
                strFieldName = "SpotFlag";
                break;
            case 5:
                strFieldName = "MakeDate";
                break;
            case 6:
                strFieldName = "MakeTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("SpotID"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OtherNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OtherType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SpotType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SpotFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
