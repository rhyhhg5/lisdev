/*
 * <p>ClassName: msh_faSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: MSREPORT
 * @CreateDate：2005-01-10
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.msh_faDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class msh_faSchema implements Schema
{
    // @Field
    /** 公司编码 */
    private String unitcode;
    /** 公司名称 */
    private String unitname;
    /** 年度 */
    private String year;
    /** 会计期间 */
    private String month;
    /** 类别编码 */
    private String cate_code;
    /** 类别名称 */
    private String cate_name;
    /** 原值 */
    private double originvalue;
    /** 累计折旧 */
    private double accudep;
    /** 净值 */
    private double netvalue;

    public static final int FIELDNUM = 9; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public msh_faSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[0];

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getunitcode()
    {
        if (unitcode != null && !unitcode.equals("") && SysConst.CHANGECHARSET == true)
        {
            unitcode = StrTool.unicodeToGBK(unitcode);
        }
        return unitcode;
    }

    public void setunitcode(String aunitcode)
    {
        unitcode = aunitcode;
    }

    public String getunitname()
    {
        if (unitname != null && !unitname.equals("") && SysConst.CHANGECHARSET == true)
        {
            unitname = StrTool.unicodeToGBK(unitname);
        }
        return unitname;
    }

    public void setunitname(String aunitname)
    {
        unitname = aunitname;
    }

    public String getyear()
    {
        if (year != null && !year.equals("") && SysConst.CHANGECHARSET == true)
        {
            year = StrTool.unicodeToGBK(year);
        }
        return year;
    }

    public void setyear(String ayear)
    {
        year = ayear;
    }

    public String getmonth()
    {
        if (month != null && !month.equals("") && SysConst.CHANGECHARSET == true)
        {
            month = StrTool.unicodeToGBK(month);
        }
        return month;
    }

    public void setmonth(String amonth)
    {
        month = amonth;
    }

    public String getcate_code()
    {
        if (cate_code != null && !cate_code.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            cate_code = StrTool.unicodeToGBK(cate_code);
        }
        return cate_code;
    }

    public void setcate_code(String acate_code)
    {
        cate_code = acate_code;
    }

    public String getcate_name()
    {
        if (cate_name != null && !cate_name.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            cate_name = StrTool.unicodeToGBK(cate_name);
        }
        return cate_name;
    }

    public void setcate_name(String acate_name)
    {
        cate_name = acate_name;
    }

    public double getoriginvalue()
    {
        return originvalue;
    }

    public void setoriginvalue(double aoriginvalue)
    {
        originvalue = aoriginvalue;
    }

    public void setoriginvalue(String aoriginvalue)
    {
        if (aoriginvalue != null && !aoriginvalue.equals(""))
        {
            Double tDouble = new Double(aoriginvalue);
            double d = tDouble.doubleValue();
            originvalue = d;
        }
    }

    public double getaccudep()
    {
        return accudep;
    }

    public void setaccudep(double aaccudep)
    {
        accudep = aaccudep;
    }

    public void setaccudep(String aaccudep)
    {
        if (aaccudep != null && !aaccudep.equals(""))
        {
            Double tDouble = new Double(aaccudep);
            double d = tDouble.doubleValue();
            accudep = d;
        }
    }

    public double getnetvalue()
    {
        return netvalue;
    }

    public void setnetvalue(double anetvalue)
    {
        netvalue = anetvalue;
    }

    public void setnetvalue(String anetvalue)
    {
        if (anetvalue != null && !anetvalue.equals(""))
        {
            Double tDouble = new Double(anetvalue);
            double d = tDouble.doubleValue();
            netvalue = d;
        }
    }


    /**
     * 使用另外一个 msh_faSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(msh_faSchema amsh_faSchema)
    {
        this.unitcode = amsh_faSchema.getunitcode();
        this.unitname = amsh_faSchema.getunitname();
        this.year = amsh_faSchema.getyear();
        this.month = amsh_faSchema.getmonth();
        this.cate_code = amsh_faSchema.getcate_code();
        this.cate_name = amsh_faSchema.getcate_name();
        this.originvalue = amsh_faSchema.getoriginvalue();
        this.accudep = amsh_faSchema.getaccudep();
        this.netvalue = amsh_faSchema.getnetvalue();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("unitcode") == null)
            {
                this.unitcode = null;
            }
            else
            {
                this.unitcode = rs.getString("unitcode").trim();
            }

            if (rs.getString("unitname") == null)
            {
                this.unitname = null;
            }
            else
            {
                this.unitname = rs.getString("unitname").trim();
            }

            if (rs.getString("year") == null)
            {
                this.year = null;
            }
            else
            {
                this.year = rs.getString("year").trim();
            }

            if (rs.getString("month") == null)
            {
                this.month = null;
            }
            else
            {
                this.month = rs.getString("month").trim();
            }

            if (rs.getString("cate_code") == null)
            {
                this.cate_code = null;
            }
            else
            {
                this.cate_code = rs.getString("cate_code").trim();
            }

            if (rs.getString("cate_name") == null)
            {
                this.cate_name = null;
            }
            else
            {
                this.cate_name = rs.getString("cate_name").trim();
            }

            this.originvalue = rs.getDouble("originvalue");
            this.accudep = rs.getDouble("accudep");
            this.netvalue = rs.getDouble("netvalue");
        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "msh_faSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public msh_faSchema getSchema()
    {
        msh_faSchema amsh_faSchema = new msh_faSchema();
        amsh_faSchema.setSchema(this);
        return amsh_faSchema;
    }

    public msh_faDB getDB()
    {
        msh_faDB aDBOper = new msh_faDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#Prpmsh_fa描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(unitcode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(unitname)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(year)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(month)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(cate_code)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(cate_name)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(originvalue) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(accudep) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(netvalue);
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#Prpmsh_fa>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            unitcode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            unitname = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            year = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                  SysConst.PACKAGESPILTER);
            month = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                   SysConst.PACKAGESPILTER);
            cate_code = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                       SysConst.PACKAGESPILTER);
            cate_name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                       SysConst.PACKAGESPILTER);
            originvalue = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 7, SysConst.PACKAGESPILTER))).doubleValue();
            accudep = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 8, SysConst.PACKAGESPILTER))).doubleValue();
            netvalue = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 9, SysConst.PACKAGESPILTER))).doubleValue();
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "msh_faSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("unitcode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(unitcode));
        }
        if (FCode.equals("unitname"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(unitname));
        }
        if (FCode.equals("year"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(year));
        }
        if (FCode.equals("month"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(month));
        }
        if (FCode.equals("cate_code"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(cate_code));
        }
        if (FCode.equals("cate_name"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(cate_name));
        }
        if (FCode.equals("originvalue"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(originvalue));
        }
        if (FCode.equals("accudep"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(accudep));
        }
        if (FCode.equals("netvalue"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(netvalue));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(unitcode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(unitname);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(year);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(month);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(cate_code);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(cate_name);
                break;
            case 6:
                strFieldValue = String.valueOf(originvalue);
                break;
            case 7:
                strFieldValue = String.valueOf(accudep);
                break;
            case 8:
                strFieldValue = String.valueOf(netvalue);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("unitcode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                unitcode = FValue.trim();
            }
            else
            {
                unitcode = null;
            }
        }
        if (FCode.equals("unitname"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                unitname = FValue.trim();
            }
            else
            {
                unitname = null;
            }
        }
        if (FCode.equals("year"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                year = FValue.trim();
            }
            else
            {
                year = null;
            }
        }
        if (FCode.equals("month"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                month = FValue.trim();
            }
            else
            {
                month = null;
            }
        }
        if (FCode.equals("cate_code"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                cate_code = FValue.trim();
            }
            else
            {
                cate_code = null;
            }
        }
        if (FCode.equals("cate_name"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                cate_name = FValue.trim();
            }
            else
            {
                cate_name = null;
            }
        }
        if (FCode.equals("originvalue"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                originvalue = d;
            }
        }
        if (FCode.equals("accudep"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                accudep = d;
            }
        }
        if (FCode.equals("netvalue"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                netvalue = d;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        msh_faSchema other = (msh_faSchema) otherObject;
        return
                unitcode.equals(other.getunitcode())
                && unitname.equals(other.getunitname())
                && year.equals(other.getyear())
                && month.equals(other.getmonth())
                && cate_code.equals(other.getcate_code())
                && cate_name.equals(other.getcate_name())
                && originvalue == other.getoriginvalue()
                && accudep == other.getaccudep()
                && netvalue == other.getnetvalue();
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("unitcode"))
        {
            return 0;
        }
        if (strFieldName.equals("unitname"))
        {
            return 1;
        }
        if (strFieldName.equals("year"))
        {
            return 2;
        }
        if (strFieldName.equals("month"))
        {
            return 3;
        }
        if (strFieldName.equals("cate_code"))
        {
            return 4;
        }
        if (strFieldName.equals("cate_name"))
        {
            return 5;
        }
        if (strFieldName.equals("originvalue"))
        {
            return 6;
        }
        if (strFieldName.equals("accudep"))
        {
            return 7;
        }
        if (strFieldName.equals("netvalue"))
        {
            return 8;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "unitcode";
                break;
            case 1:
                strFieldName = "unitname";
                break;
            case 2:
                strFieldName = "year";
                break;
            case 3:
                strFieldName = "month";
                break;
            case 4:
                strFieldName = "cate_code";
                break;
            case 5:
                strFieldName = "cate_name";
                break;
            case 6:
                strFieldName = "originvalue";
                break;
            case 7:
                strFieldName = "accudep";
                break;
            case 8:
                strFieldName = "netvalue";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("unitcode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("unitname"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("year"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("month"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("cate_code"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("cate_name"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("originvalue"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("accudep"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("netvalue"))
        {
            return Schema.TYPE_DOUBLE;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 7:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 8:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
