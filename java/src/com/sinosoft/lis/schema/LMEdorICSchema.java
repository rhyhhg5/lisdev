/*
 * <p>ClassName: LMEdorICSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LMEdorICDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LMEdorICSchema implements Schema
{
    // @Field
    /** 险种编码 */
    private String RiskCode;
    /** 险种版本 */
    private String RiskVersion;
    /** 责任代码 */
    private String DutyCode;
    /** 缴费编码 */
    private String PayPlanCode;
    /** 算法编码 */
    private String CalCode;
    /** 间隔类型 */
    private String IntvType;

    public static final int FIELDNUM = 6; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LMEdorICSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "RiskCode";
        pk[1] = "DutyCode";
        pk[2] = "PayPlanCode";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getRiskCode()
    {
        if (RiskCode != null && !RiskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getRiskVersion()
    {
        if (RiskVersion != null && !RiskVersion.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            RiskVersion = StrTool.unicodeToGBK(RiskVersion);
        }
        return RiskVersion;
    }

    public void setRiskVersion(String aRiskVersion)
    {
        RiskVersion = aRiskVersion;
    }

    public String getDutyCode()
    {
        if (DutyCode != null && !DutyCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            DutyCode = StrTool.unicodeToGBK(DutyCode);
        }
        return DutyCode;
    }

    public void setDutyCode(String aDutyCode)
    {
        DutyCode = aDutyCode;
    }

    public String getPayPlanCode()
    {
        if (PayPlanCode != null && !PayPlanCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            PayPlanCode = StrTool.unicodeToGBK(PayPlanCode);
        }
        return PayPlanCode;
    }

    public void setPayPlanCode(String aPayPlanCode)
    {
        PayPlanCode = aPayPlanCode;
    }

    public String getCalCode()
    {
        if (CalCode != null && !CalCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            CalCode = StrTool.unicodeToGBK(CalCode);
        }
        return CalCode;
    }

    public void setCalCode(String aCalCode)
    {
        CalCode = aCalCode;
    }

    public String getIntvType()
    {
        if (IntvType != null && !IntvType.equals("") && SysConst.CHANGECHARSET == true)
        {
            IntvType = StrTool.unicodeToGBK(IntvType);
        }
        return IntvType;
    }

    public void setIntvType(String aIntvType)
    {
        IntvType = aIntvType;
    }

    /**
     * 使用另外一个 LMEdorICSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LMEdorICSchema aLMEdorICSchema)
    {
        this.RiskCode = aLMEdorICSchema.getRiskCode();
        this.RiskVersion = aLMEdorICSchema.getRiskVersion();
        this.DutyCode = aLMEdorICSchema.getDutyCode();
        this.PayPlanCode = aLMEdorICSchema.getPayPlanCode();
        this.CalCode = aLMEdorICSchema.getCalCode();
        this.IntvType = aLMEdorICSchema.getIntvType();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("RiskVersion") == null)
            {
                this.RiskVersion = null;
            }
            else
            {
                this.RiskVersion = rs.getString("RiskVersion").trim();
            }

            if (rs.getString("DutyCode") == null)
            {
                this.DutyCode = null;
            }
            else
            {
                this.DutyCode = rs.getString("DutyCode").trim();
            }

            if (rs.getString("PayPlanCode") == null)
            {
                this.PayPlanCode = null;
            }
            else
            {
                this.PayPlanCode = rs.getString("PayPlanCode").trim();
            }

            if (rs.getString("CalCode") == null)
            {
                this.CalCode = null;
            }
            else
            {
                this.CalCode = rs.getString("CalCode").trim();
            }

            if (rs.getString("IntvType") == null)
            {
                this.IntvType = null;
            }
            else
            {
                this.IntvType = rs.getString("IntvType").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMEdorICSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LMEdorICSchema getSchema()
    {
        LMEdorICSchema aLMEdorICSchema = new LMEdorICSchema();
        aLMEdorICSchema.setSchema(this);
        return aLMEdorICSchema;
    }

    public LMEdorICDB getDB()
    {
        LMEdorICDB aDBOper = new LMEdorICDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMEdorIC描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskVersion)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(DutyCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PayPlanCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CalCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(IntvType));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMEdorIC>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            RiskVersion = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                         SysConst.PACKAGESPILTER);
            DutyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            PayPlanCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                         SysConst.PACKAGESPILTER);
            CalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                     SysConst.PACKAGESPILTER);
            IntvType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                      SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMEdorICSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskCode));
        }
        if (FCode.equals("RiskVersion"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskVersion));
        }
        if (FCode.equals("DutyCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DutyCode));
        }
        if (FCode.equals("PayPlanCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PayPlanCode));
        }
        if (FCode.equals("CalCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CalCode));
        }
        if (FCode.equals("IntvType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(IntvType));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(RiskVersion);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(DutyCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(PayPlanCode);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(CalCode);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(IntvType);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("RiskVersion"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskVersion = FValue.trim();
            }
            else
            {
                RiskVersion = null;
            }
        }
        if (FCode.equals("DutyCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DutyCode = FValue.trim();
            }
            else
            {
                DutyCode = null;
            }
        }
        if (FCode.equals("PayPlanCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PayPlanCode = FValue.trim();
            }
            else
            {
                PayPlanCode = null;
            }
        }
        if (FCode.equals("CalCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CalCode = FValue.trim();
            }
            else
            {
                CalCode = null;
            }
        }
        if (FCode.equals("IntvType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                IntvType = FValue.trim();
            }
            else
            {
                IntvType = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LMEdorICSchema other = (LMEdorICSchema) otherObject;
        return
                RiskCode.equals(other.getRiskCode())
                && RiskVersion.equals(other.getRiskVersion())
                && DutyCode.equals(other.getDutyCode())
                && PayPlanCode.equals(other.getPayPlanCode())
                && CalCode.equals(other.getCalCode())
                && IntvType.equals(other.getIntvType());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("RiskCode"))
        {
            return 0;
        }
        if (strFieldName.equals("RiskVersion"))
        {
            return 1;
        }
        if (strFieldName.equals("DutyCode"))
        {
            return 2;
        }
        if (strFieldName.equals("PayPlanCode"))
        {
            return 3;
        }
        if (strFieldName.equals("CalCode"))
        {
            return 4;
        }
        if (strFieldName.equals("IntvType"))
        {
            return 5;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "RiskVersion";
                break;
            case 2:
                strFieldName = "DutyCode";
                break;
            case 3:
                strFieldName = "PayPlanCode";
                break;
            case 4:
                strFieldName = "CalCode";
                break;
            case 5:
                strFieldName = "IntvType";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskVersion"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DutyCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PayPlanCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CalCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("IntvType"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
