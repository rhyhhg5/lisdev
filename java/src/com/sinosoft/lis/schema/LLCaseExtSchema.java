/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LLCaseExtDB;

/*
 * <p>ClassName: LLCaseExtSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2019-04-10
 */
public class LLCaseExtSchema implements Schema, Cloneable
{
	// @Field
	/** 分案号(个人理赔号) */
	private String CaseNo;
	/** 领款人与被保人关系 */
	private String RelaDrawerInsured;
	/** 管理机构 */
	private String MngCom;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 地区代码 */
	private String AreaCode;
	/** 县区代码 */
	private String CountyCode;
	/** 合规费用 */
	private double RationalAmnt;
	/** 咨询号码 */
	private String ConsultNo;
	/** 理赔终止 */
	private String ClaimEnd;
	/** 客户地址 */
	private String PostalAddress;
	/** 扣除明细 */
	private String Remark;
	/** 领款人证件类型 */
	private String DrawerIDType;
	/** 领款人证件号码 */
	private String DrawerID;
	/** 被保险人职业名称 */
	private String JobTitle;
	/** 授权使用客户信息 */
	private String Authorization;
	/** 领款人姓名 */
	private String DrawerName;
	/** 领款人性别 */
	private String DrawerSex;
	/** 领款人证件生效日期 */
	private Date DrawerIDStartDate;
	/** 领款人证件失效日期 */
	private Date DrawerIDEndDate;
	/** 领款人国籍 */
	private String DrawNativePlace;
	/** 领款人职业 */
	private String DrawerOccupation;
	/** 领款人联系地址 */
	private String DrawerAddress;
	/** 领款人联系电话 */
	private String DrawerPhone;
	/** 职业类别 */
	private String OccupationType;

	public static final int FIELDNUM = 28;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LLCaseExtSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "CaseNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LLCaseExtSchema cloned = (LLCaseExtSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getCaseNo()
	{
		return CaseNo;
	}
	public void setCaseNo(String aCaseNo)
	{
		CaseNo = aCaseNo;
	}
	public String getRelaDrawerInsured()
	{
		return RelaDrawerInsured;
	}
	public void setRelaDrawerInsured(String aRelaDrawerInsured)
	{
		RelaDrawerInsured = aRelaDrawerInsured;
	}
	public String getMngCom()
	{
		return MngCom;
	}
	public void setMngCom(String aMngCom)
	{
		MngCom = aMngCom;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getAreaCode()
	{
		return AreaCode;
	}
	public void setAreaCode(String aAreaCode)
	{
		AreaCode = aAreaCode;
	}
	public String getCountyCode()
	{
		return CountyCode;
	}
	public void setCountyCode(String aCountyCode)
	{
		CountyCode = aCountyCode;
	}
	public double getRationalAmnt()
	{
		return RationalAmnt;
	}
	public void setRationalAmnt(double aRationalAmnt)
	{
		RationalAmnt = Arith.round(aRationalAmnt,2);
	}
	public void setRationalAmnt(String aRationalAmnt)
	{
		if (aRationalAmnt != null && !aRationalAmnt.equals(""))
		{
			Double tDouble = new Double(aRationalAmnt);
			double d = tDouble.doubleValue();
                RationalAmnt = Arith.round(d,2);
		}
	}

	public String getConsultNo()
	{
		return ConsultNo;
	}
	public void setConsultNo(String aConsultNo)
	{
		ConsultNo = aConsultNo;
	}
	public String getClaimEnd()
	{
		return ClaimEnd;
	}
	public void setClaimEnd(String aClaimEnd)
	{
		ClaimEnd = aClaimEnd;
	}
	public String getPostalAddress()
	{
		return PostalAddress;
	}
	public void setPostalAddress(String aPostalAddress)
	{
		PostalAddress = aPostalAddress;
	}
	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
		Remark = aRemark;
	}
	public String getDrawerIDType()
	{
		return DrawerIDType;
	}
	public void setDrawerIDType(String aDrawerIDType)
	{
		DrawerIDType = aDrawerIDType;
	}
	public String getDrawerID()
	{
		return DrawerID;
	}
	public void setDrawerID(String aDrawerID)
	{
		DrawerID = aDrawerID;
	}
	public String getJobTitle()
	{
		return JobTitle;
	}
	public void setJobTitle(String aJobTitle)
	{
		JobTitle = aJobTitle;
	}
	public String getAuthorization()
	{
		return Authorization;
	}
	public void setAuthorization(String aAuthorization)
	{
		Authorization = aAuthorization;
	}
	public String getDrawerName()
	{
		return DrawerName;
	}
	public void setDrawerName(String aDrawerName)
	{
		DrawerName = aDrawerName;
	}
	public String getDrawerSex()
	{
		return DrawerSex;
	}
	public void setDrawerSex(String aDrawerSex)
	{
		DrawerSex = aDrawerSex;
	}
	public String getDrawerIDStartDate()
	{
		if( DrawerIDStartDate != null )
			return fDate.getString(DrawerIDStartDate);
		else
			return null;
	}
	public void setDrawerIDStartDate(Date aDrawerIDStartDate)
	{
		DrawerIDStartDate = aDrawerIDStartDate;
	}
	public void setDrawerIDStartDate(String aDrawerIDStartDate)
	{
		if (aDrawerIDStartDate != null && !aDrawerIDStartDate.equals("") )
		{
			DrawerIDStartDate = fDate.getDate( aDrawerIDStartDate );
		}
		else
			DrawerIDStartDate = null;
	}

	public String getDrawerIDEndDate()
	{
		if( DrawerIDEndDate != null )
			return fDate.getString(DrawerIDEndDate);
		else
			return null;
	}
	public void setDrawerIDEndDate(Date aDrawerIDEndDate)
	{
		DrawerIDEndDate = aDrawerIDEndDate;
	}
	public void setDrawerIDEndDate(String aDrawerIDEndDate)
	{
		if (aDrawerIDEndDate != null && !aDrawerIDEndDate.equals("") )
		{
			DrawerIDEndDate = fDate.getDate( aDrawerIDEndDate );
		}
		else
			DrawerIDEndDate = null;
	}

	public String getDrawNativePlace()
	{
		return DrawNativePlace;
	}
	public void setDrawNativePlace(String aDrawNativePlace)
	{
		DrawNativePlace = aDrawNativePlace;
	}
	public String getDrawerOccupation()
	{
		return DrawerOccupation;
	}
	public void setDrawerOccupation(String aDrawerOccupation)
	{
		DrawerOccupation = aDrawerOccupation;
	}
	public String getDrawerAddress()
	{
		return DrawerAddress;
	}
	public void setDrawerAddress(String aDrawerAddress)
	{
		DrawerAddress = aDrawerAddress;
	}
	public String getDrawerPhone()
	{
		return DrawerPhone;
	}
	public void setDrawerPhone(String aDrawerPhone)
	{
		DrawerPhone = aDrawerPhone;
	}
	public String getOccupationType()
	{
		return OccupationType;
	}
	public void setOccupationType(String aOccupationType)
	{
		OccupationType = aOccupationType;
	}

	/**
	* 使用另外一个 LLCaseExtSchema 对象给 Schema 赋值
	* @param: aLLCaseExtSchema LLCaseExtSchema
	**/
	public void setSchema(LLCaseExtSchema aLLCaseExtSchema)
	{
		this.CaseNo = aLLCaseExtSchema.getCaseNo();
		this.RelaDrawerInsured = aLLCaseExtSchema.getRelaDrawerInsured();
		this.MngCom = aLLCaseExtSchema.getMngCom();
		this.Operator = aLLCaseExtSchema.getOperator();
		this.MakeDate = fDate.getDate( aLLCaseExtSchema.getMakeDate());
		this.MakeTime = aLLCaseExtSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLLCaseExtSchema.getModifyDate());
		this.ModifyTime = aLLCaseExtSchema.getModifyTime();
		this.AreaCode = aLLCaseExtSchema.getAreaCode();
		this.CountyCode = aLLCaseExtSchema.getCountyCode();
		this.RationalAmnt = aLLCaseExtSchema.getRationalAmnt();
		this.ConsultNo = aLLCaseExtSchema.getConsultNo();
		this.ClaimEnd = aLLCaseExtSchema.getClaimEnd();
		this.PostalAddress = aLLCaseExtSchema.getPostalAddress();
		this.Remark = aLLCaseExtSchema.getRemark();
		this.DrawerIDType = aLLCaseExtSchema.getDrawerIDType();
		this.DrawerID = aLLCaseExtSchema.getDrawerID();
		this.JobTitle = aLLCaseExtSchema.getJobTitle();
		this.Authorization = aLLCaseExtSchema.getAuthorization();
		this.DrawerName = aLLCaseExtSchema.getDrawerName();
		this.DrawerSex = aLLCaseExtSchema.getDrawerSex();
		this.DrawerIDStartDate = fDate.getDate( aLLCaseExtSchema.getDrawerIDStartDate());
		this.DrawerIDEndDate = fDate.getDate( aLLCaseExtSchema.getDrawerIDEndDate());
		this.DrawNativePlace = aLLCaseExtSchema.getDrawNativePlace();
		this.DrawerOccupation = aLLCaseExtSchema.getDrawerOccupation();
		this.DrawerAddress = aLLCaseExtSchema.getDrawerAddress();
		this.DrawerPhone = aLLCaseExtSchema.getDrawerPhone();
		this.OccupationType = aLLCaseExtSchema.getOccupationType();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("CaseNo") == null )
				this.CaseNo = null;
			else
				this.CaseNo = rs.getString("CaseNo").trim();

			if( rs.getString("RelaDrawerInsured") == null )
				this.RelaDrawerInsured = null;
			else
				this.RelaDrawerInsured = rs.getString("RelaDrawerInsured").trim();

			if( rs.getString("MngCom") == null )
				this.MngCom = null;
			else
				this.MngCom = rs.getString("MngCom").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("AreaCode") == null )
				this.AreaCode = null;
			else
				this.AreaCode = rs.getString("AreaCode").trim();

			if( rs.getString("CountyCode") == null )
				this.CountyCode = null;
			else
				this.CountyCode = rs.getString("CountyCode").trim();

			this.RationalAmnt = rs.getDouble("RationalAmnt");
			if( rs.getString("ConsultNo") == null )
				this.ConsultNo = null;
			else
				this.ConsultNo = rs.getString("ConsultNo").trim();

			if( rs.getString("ClaimEnd") == null )
				this.ClaimEnd = null;
			else
				this.ClaimEnd = rs.getString("ClaimEnd").trim();

			if( rs.getString("PostalAddress") == null )
				this.PostalAddress = null;
			else
				this.PostalAddress = rs.getString("PostalAddress").trim();

			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

			if( rs.getString("DrawerIDType") == null )
				this.DrawerIDType = null;
			else
				this.DrawerIDType = rs.getString("DrawerIDType").trim();

			if( rs.getString("DrawerID") == null )
				this.DrawerID = null;
			else
				this.DrawerID = rs.getString("DrawerID").trim();

			if( rs.getString("JobTitle") == null )
				this.JobTitle = null;
			else
				this.JobTitle = rs.getString("JobTitle").trim();

			if( rs.getString("Authorization") == null )
				this.Authorization = null;
			else
				this.Authorization = rs.getString("Authorization").trim();

			if( rs.getString("DrawerName") == null )
				this.DrawerName = null;
			else
				this.DrawerName = rs.getString("DrawerName").trim();

			if( rs.getString("DrawerSex") == null )
				this.DrawerSex = null;
			else
				this.DrawerSex = rs.getString("DrawerSex").trim();

			this.DrawerIDStartDate = rs.getDate("DrawerIDStartDate");
			this.DrawerIDEndDate = rs.getDate("DrawerIDEndDate");
			if( rs.getString("DrawNativePlace") == null )
				this.DrawNativePlace = null;
			else
				this.DrawNativePlace = rs.getString("DrawNativePlace").trim();

			if( rs.getString("DrawerOccupation") == null )
				this.DrawerOccupation = null;
			else
				this.DrawerOccupation = rs.getString("DrawerOccupation").trim();

			if( rs.getString("DrawerAddress") == null )
				this.DrawerAddress = null;
			else
				this.DrawerAddress = rs.getString("DrawerAddress").trim();

			if( rs.getString("DrawerPhone") == null )
				this.DrawerPhone = null;
			else
				this.DrawerPhone = rs.getString("DrawerPhone").trim();

			if( rs.getString("OccupationType") == null )
				this.OccupationType = null;
			else
				this.OccupationType = rs.getString("OccupationType").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LLCaseExt表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLCaseExtSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LLCaseExtSchema getSchema()
	{
		LLCaseExtSchema aLLCaseExtSchema = new LLCaseExtSchema();
		aLLCaseExtSchema.setSchema(this);
		return aLLCaseExtSchema;
	}

	public LLCaseExtDB getDB()
	{
		LLCaseExtDB aDBOper = new LLCaseExtDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLCaseExt描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(CaseNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RelaDrawerInsured)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MngCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AreaCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CountyCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(RationalAmnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ConsultNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ClaimEnd)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PostalAddress)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DrawerIDType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DrawerID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(JobTitle)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Authorization)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DrawerName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DrawerSex)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( DrawerIDStartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( DrawerIDEndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DrawNativePlace)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DrawerOccupation)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DrawerAddress)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DrawerPhone)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OccupationType));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLCaseExt>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			CaseNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			RelaDrawerInsured = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			MngCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			AreaCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			CountyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			RationalAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,11,SysConst.PACKAGESPILTER))).doubleValue();
			ConsultNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			ClaimEnd = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			PostalAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			DrawerIDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			DrawerID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			JobTitle = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			Authorization = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			DrawerName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			DrawerSex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			DrawerIDStartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,SysConst.PACKAGESPILTER));
			DrawerIDEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,SysConst.PACKAGESPILTER));
			DrawNativePlace = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			DrawerOccupation = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			DrawerAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			DrawerPhone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			OccupationType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLCaseExtSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("CaseNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CaseNo));
		}
		if (FCode.equals("RelaDrawerInsured"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RelaDrawerInsured));
		}
		if (FCode.equals("MngCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MngCom));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("AreaCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AreaCode));
		}
		if (FCode.equals("CountyCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CountyCode));
		}
		if (FCode.equals("RationalAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RationalAmnt));
		}
		if (FCode.equals("ConsultNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ConsultNo));
		}
		if (FCode.equals("ClaimEnd"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClaimEnd));
		}
		if (FCode.equals("PostalAddress"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PostalAddress));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (FCode.equals("DrawerIDType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DrawerIDType));
		}
		if (FCode.equals("DrawerID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DrawerID));
		}
		if (FCode.equals("JobTitle"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(JobTitle));
		}
		if (FCode.equals("Authorization"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Authorization));
		}
		if (FCode.equals("DrawerName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DrawerName));
		}
		if (FCode.equals("DrawerSex"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DrawerSex));
		}
		if (FCode.equals("DrawerIDStartDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getDrawerIDStartDate()));
		}
		if (FCode.equals("DrawerIDEndDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getDrawerIDEndDate()));
		}
		if (FCode.equals("DrawNativePlace"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DrawNativePlace));
		}
		if (FCode.equals("DrawerOccupation"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DrawerOccupation));
		}
		if (FCode.equals("DrawerAddress"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DrawerAddress));
		}
		if (FCode.equals("DrawerPhone"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DrawerPhone));
		}
		if (FCode.equals("OccupationType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationType));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(CaseNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(RelaDrawerInsured);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(MngCom);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(AreaCode);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(CountyCode);
				break;
			case 10:
				strFieldValue = String.valueOf(RationalAmnt);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(ConsultNo);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(ClaimEnd);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(PostalAddress);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(DrawerIDType);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(DrawerID);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(JobTitle);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(Authorization);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(DrawerName);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(DrawerSex);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getDrawerIDStartDate()));
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getDrawerIDEndDate()));
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(DrawNativePlace);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(DrawerOccupation);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(DrawerAddress);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(DrawerPhone);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(OccupationType);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("CaseNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CaseNo = FValue.trim();
			}
			else
				CaseNo = null;
		}
		if (FCode.equalsIgnoreCase("RelaDrawerInsured"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RelaDrawerInsured = FValue.trim();
			}
			else
				RelaDrawerInsured = null;
		}
		if (FCode.equalsIgnoreCase("MngCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MngCom = FValue.trim();
			}
			else
				MngCom = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("AreaCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AreaCode = FValue.trim();
			}
			else
				AreaCode = null;
		}
		if (FCode.equalsIgnoreCase("CountyCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CountyCode = FValue.trim();
			}
			else
				CountyCode = null;
		}
		if (FCode.equalsIgnoreCase("RationalAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				RationalAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("ConsultNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ConsultNo = FValue.trim();
			}
			else
				ConsultNo = null;
		}
		if (FCode.equalsIgnoreCase("ClaimEnd"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ClaimEnd = FValue.trim();
			}
			else
				ClaimEnd = null;
		}
		if (FCode.equalsIgnoreCase("PostalAddress"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PostalAddress = FValue.trim();
			}
			else
				PostalAddress = null;
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		if (FCode.equalsIgnoreCase("DrawerIDType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DrawerIDType = FValue.trim();
			}
			else
				DrawerIDType = null;
		}
		if (FCode.equalsIgnoreCase("DrawerID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DrawerID = FValue.trim();
			}
			else
				DrawerID = null;
		}
		if (FCode.equalsIgnoreCase("JobTitle"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				JobTitle = FValue.trim();
			}
			else
				JobTitle = null;
		}
		if (FCode.equalsIgnoreCase("Authorization"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Authorization = FValue.trim();
			}
			else
				Authorization = null;
		}
		if (FCode.equalsIgnoreCase("DrawerName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DrawerName = FValue.trim();
			}
			else
				DrawerName = null;
		}
		if (FCode.equalsIgnoreCase("DrawerSex"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DrawerSex = FValue.trim();
			}
			else
				DrawerSex = null;
		}
		if (FCode.equalsIgnoreCase("DrawerIDStartDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				DrawerIDStartDate = fDate.getDate( FValue );
			}
			else
				DrawerIDStartDate = null;
		}
		if (FCode.equalsIgnoreCase("DrawerIDEndDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				DrawerIDEndDate = fDate.getDate( FValue );
			}
			else
				DrawerIDEndDate = null;
		}
		if (FCode.equalsIgnoreCase("DrawNativePlace"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DrawNativePlace = FValue.trim();
			}
			else
				DrawNativePlace = null;
		}
		if (FCode.equalsIgnoreCase("DrawerOccupation"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DrawerOccupation = FValue.trim();
			}
			else
				DrawerOccupation = null;
		}
		if (FCode.equalsIgnoreCase("DrawerAddress"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DrawerAddress = FValue.trim();
			}
			else
				DrawerAddress = null;
		}
		if (FCode.equalsIgnoreCase("DrawerPhone"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DrawerPhone = FValue.trim();
			}
			else
				DrawerPhone = null;
		}
		if (FCode.equalsIgnoreCase("OccupationType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OccupationType = FValue.trim();
			}
			else
				OccupationType = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LLCaseExtSchema other = (LLCaseExtSchema)otherObject;
		return
			(CaseNo == null ? other.getCaseNo() == null : CaseNo.equals(other.getCaseNo()))
			&& (RelaDrawerInsured == null ? other.getRelaDrawerInsured() == null : RelaDrawerInsured.equals(other.getRelaDrawerInsured()))
			&& (MngCom == null ? other.getMngCom() == null : MngCom.equals(other.getMngCom()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (AreaCode == null ? other.getAreaCode() == null : AreaCode.equals(other.getAreaCode()))
			&& (CountyCode == null ? other.getCountyCode() == null : CountyCode.equals(other.getCountyCode()))
			&& RationalAmnt == other.getRationalAmnt()
			&& (ConsultNo == null ? other.getConsultNo() == null : ConsultNo.equals(other.getConsultNo()))
			&& (ClaimEnd == null ? other.getClaimEnd() == null : ClaimEnd.equals(other.getClaimEnd()))
			&& (PostalAddress == null ? other.getPostalAddress() == null : PostalAddress.equals(other.getPostalAddress()))
			&& (Remark == null ? other.getRemark() == null : Remark.equals(other.getRemark()))
			&& (DrawerIDType == null ? other.getDrawerIDType() == null : DrawerIDType.equals(other.getDrawerIDType()))
			&& (DrawerID == null ? other.getDrawerID() == null : DrawerID.equals(other.getDrawerID()))
			&& (JobTitle == null ? other.getJobTitle() == null : JobTitle.equals(other.getJobTitle()))
			&& (Authorization == null ? other.getAuthorization() == null : Authorization.equals(other.getAuthorization()))
			&& (DrawerName == null ? other.getDrawerName() == null : DrawerName.equals(other.getDrawerName()))
			&& (DrawerSex == null ? other.getDrawerSex() == null : DrawerSex.equals(other.getDrawerSex()))
			&& (DrawerIDStartDate == null ? other.getDrawerIDStartDate() == null : fDate.getString(DrawerIDStartDate).equals(other.getDrawerIDStartDate()))
			&& (DrawerIDEndDate == null ? other.getDrawerIDEndDate() == null : fDate.getString(DrawerIDEndDate).equals(other.getDrawerIDEndDate()))
			&& (DrawNativePlace == null ? other.getDrawNativePlace() == null : DrawNativePlace.equals(other.getDrawNativePlace()))
			&& (DrawerOccupation == null ? other.getDrawerOccupation() == null : DrawerOccupation.equals(other.getDrawerOccupation()))
			&& (DrawerAddress == null ? other.getDrawerAddress() == null : DrawerAddress.equals(other.getDrawerAddress()))
			&& (DrawerPhone == null ? other.getDrawerPhone() == null : DrawerPhone.equals(other.getDrawerPhone()))
			&& (OccupationType == null ? other.getOccupationType() == null : OccupationType.equals(other.getOccupationType()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("CaseNo") ) {
			return 0;
		}
		if( strFieldName.equals("RelaDrawerInsured") ) {
			return 1;
		}
		if( strFieldName.equals("MngCom") ) {
			return 2;
		}
		if( strFieldName.equals("Operator") ) {
			return 3;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 4;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 5;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 6;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 7;
		}
		if( strFieldName.equals("AreaCode") ) {
			return 8;
		}
		if( strFieldName.equals("CountyCode") ) {
			return 9;
		}
		if( strFieldName.equals("RationalAmnt") ) {
			return 10;
		}
		if( strFieldName.equals("ConsultNo") ) {
			return 11;
		}
		if( strFieldName.equals("ClaimEnd") ) {
			return 12;
		}
		if( strFieldName.equals("PostalAddress") ) {
			return 13;
		}
		if( strFieldName.equals("Remark") ) {
			return 14;
		}
		if( strFieldName.equals("DrawerIDType") ) {
			return 15;
		}
		if( strFieldName.equals("DrawerID") ) {
			return 16;
		}
		if( strFieldName.equals("JobTitle") ) {
			return 17;
		}
		if( strFieldName.equals("Authorization") ) {
			return 18;
		}
		if( strFieldName.equals("DrawerName") ) {
			return 19;
		}
		if( strFieldName.equals("DrawerSex") ) {
			return 20;
		}
		if( strFieldName.equals("DrawerIDStartDate") ) {
			return 21;
		}
		if( strFieldName.equals("DrawerIDEndDate") ) {
			return 22;
		}
		if( strFieldName.equals("DrawNativePlace") ) {
			return 23;
		}
		if( strFieldName.equals("DrawerOccupation") ) {
			return 24;
		}
		if( strFieldName.equals("DrawerAddress") ) {
			return 25;
		}
		if( strFieldName.equals("DrawerPhone") ) {
			return 26;
		}
		if( strFieldName.equals("OccupationType") ) {
			return 27;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "CaseNo";
				break;
			case 1:
				strFieldName = "RelaDrawerInsured";
				break;
			case 2:
				strFieldName = "MngCom";
				break;
			case 3:
				strFieldName = "Operator";
				break;
			case 4:
				strFieldName = "MakeDate";
				break;
			case 5:
				strFieldName = "MakeTime";
				break;
			case 6:
				strFieldName = "ModifyDate";
				break;
			case 7:
				strFieldName = "ModifyTime";
				break;
			case 8:
				strFieldName = "AreaCode";
				break;
			case 9:
				strFieldName = "CountyCode";
				break;
			case 10:
				strFieldName = "RationalAmnt";
				break;
			case 11:
				strFieldName = "ConsultNo";
				break;
			case 12:
				strFieldName = "ClaimEnd";
				break;
			case 13:
				strFieldName = "PostalAddress";
				break;
			case 14:
				strFieldName = "Remark";
				break;
			case 15:
				strFieldName = "DrawerIDType";
				break;
			case 16:
				strFieldName = "DrawerID";
				break;
			case 17:
				strFieldName = "JobTitle";
				break;
			case 18:
				strFieldName = "Authorization";
				break;
			case 19:
				strFieldName = "DrawerName";
				break;
			case 20:
				strFieldName = "DrawerSex";
				break;
			case 21:
				strFieldName = "DrawerIDStartDate";
				break;
			case 22:
				strFieldName = "DrawerIDEndDate";
				break;
			case 23:
				strFieldName = "DrawNativePlace";
				break;
			case 24:
				strFieldName = "DrawerOccupation";
				break;
			case 25:
				strFieldName = "DrawerAddress";
				break;
			case 26:
				strFieldName = "DrawerPhone";
				break;
			case 27:
				strFieldName = "OccupationType";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("CaseNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RelaDrawerInsured") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MngCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AreaCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CountyCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RationalAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ConsultNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ClaimEnd") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PostalAddress") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DrawerIDType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DrawerID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("JobTitle") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Authorization") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DrawerName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DrawerSex") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DrawerIDStartDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("DrawerIDEndDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("DrawNativePlace") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DrawerOccupation") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DrawerAddress") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DrawerPhone") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OccupationType") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 22:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
