/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LITransInfoDefDB;

/*
 * <p>ClassName: LITransInfoDefSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 财务接口最新版
 * @CreateDate：2008-05-20
 */
public class LITransInfoDefSchema implements Schema, Cloneable
{
	// @Field
	/** 数据类型编码 */
	private String ClassID;
	/** 科目类型编码 */
	private String FinItemID;
	/** 科目类型名称 */
	private String FinItemName;
	/** 借贷标志 */
	private String FinItemType;

	public static final int FIELDNUM = 4;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LITransInfoDefSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "ClassID";
		pk[1] = "FinItemID";

		PK = pk;
	}

            /**
             * Schema克隆
             * @return Object
             * @throws CloneNotSupportedException
             */
            public Object clone()
                    throws CloneNotSupportedException
            {
                LITransInfoDefSchema cloned = (LITransInfoDefSchema)super.clone();
                cloned.mErrors = (CErrors) mErrors.clone();
                return cloned;
            }

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getClassID()
	{
		return ClassID;
	}
	public void setClassID(String aClassID)
	{
            ClassID = aClassID;
	}
	public String getFinItemID()
	{
		return FinItemID;
	}
	public void setFinItemID(String aFinItemID)
	{
            FinItemID = aFinItemID;
	}
	public String getFinItemName()
	{
		return FinItemName;
	}
	public void setFinItemName(String aFinItemName)
	{
            FinItemName = aFinItemName;
	}
	public String getFinItemType()
	{
		return FinItemType;
	}
	public void setFinItemType(String aFinItemType)
	{
            FinItemType = aFinItemType;
	}

	/**
	* 使用另外一个 LITransInfoDefSchema 对象给 Schema 赋值
	* @param: aLITransInfoDefSchema LITransInfoDefSchema
	**/
	public void setSchema(LITransInfoDefSchema aLITransInfoDefSchema)
	{
		this.ClassID = aLITransInfoDefSchema.getClassID();
		this.FinItemID = aLITransInfoDefSchema.getFinItemID();
		this.FinItemName = aLITransInfoDefSchema.getFinItemName();
		this.FinItemType = aLITransInfoDefSchema.getFinItemType();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("ClassID") == null )
				this.ClassID = null;
			else
				this.ClassID = rs.getString("ClassID").trim();

			if( rs.getString("FinItemID") == null )
				this.FinItemID = null;
			else
				this.FinItemID = rs.getString("FinItemID").trim();

			if( rs.getString("FinItemName") == null )
				this.FinItemName = null;
			else
				this.FinItemName = rs.getString("FinItemName").trim();

			if( rs.getString("FinItemType") == null )
				this.FinItemType = null;
			else
				this.FinItemType = rs.getString("FinItemType").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LITransInfoDef表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LITransInfoDefSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LITransInfoDefSchema getSchema()
	{
		LITransInfoDefSchema aLITransInfoDefSchema = new LITransInfoDefSchema();
		aLITransInfoDefSchema.setSchema(this);
		return aLITransInfoDefSchema;
	}

	public LITransInfoDefDB getDB()
	{
		LITransInfoDefDB aDBOper = new LITransInfoDefDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLITransInfoDef描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append(StrTool.cTrim(ClassID)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(FinItemID)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(FinItemName)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(FinItemType));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLITransInfoDef>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			ClassID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			FinItemID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			FinItemName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			FinItemType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LITransInfoDefSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("ClassID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClassID));
		}
		if (FCode.equals("FinItemID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FinItemID));
		}
		if (FCode.equals("FinItemName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FinItemName));
		}
		if (FCode.equals("FinItemType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FinItemType));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(ClassID);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(FinItemID);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(FinItemName);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(FinItemType);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("ClassID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ClassID = FValue.trim();
			}
			else
				ClassID = null;
		}
		if (FCode.equalsIgnoreCase("FinItemID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FinItemID = FValue.trim();
			}
			else
				FinItemID = null;
		}
		if (FCode.equalsIgnoreCase("FinItemName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FinItemName = FValue.trim();
			}
			else
				FinItemName = null;
		}
		if (FCode.equalsIgnoreCase("FinItemType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FinItemType = FValue.trim();
			}
			else
				FinItemType = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LITransInfoDefSchema other = (LITransInfoDefSchema)otherObject;
		return
			ClassID.equals(other.getClassID())
			&& FinItemID.equals(other.getFinItemID())
			&& FinItemName.equals(other.getFinItemName())
			&& FinItemType.equals(other.getFinItemType());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("ClassID") ) {
			return 0;
		}
		if( strFieldName.equals("FinItemID") ) {
			return 1;
		}
		if( strFieldName.equals("FinItemName") ) {
			return 2;
		}
		if( strFieldName.equals("FinItemType") ) {
			return 3;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "ClassID";
				break;
			case 1:
				strFieldName = "FinItemID";
				break;
			case 2:
				strFieldName = "FinItemName";
				break;
			case 3:
				strFieldName = "FinItemType";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("ClassID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FinItemID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FinItemName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FinItemType") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
