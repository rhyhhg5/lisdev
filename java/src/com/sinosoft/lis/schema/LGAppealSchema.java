/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LGAppealDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LGAppealSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 新表结构
 * @CreateDate：2005-07-06
 */
public class LGAppealSchema implements Schema, Cloneable
{
    // @Field
    /** 投诉单编号 */
    private String AppealNo;
    /** 处理编号 */
    private String DealNo;
    /** 投诉方式 */
    private String AppealWayNo;
    /** 受理人编号 */
    private String AcceptorNo;
    /** 受理人姓名 */
    private String AcceptorName;
    /** 受理人岗位 */
    private String AcceptorPost;
    /** 投诉对象编号 */
    private String AppealObjNo;
    /** 投诉对象名 */
    private String AppealObjName;
    /** 投诉对象联系电话 */
    private String AppealObjPhone;
    /** 投诉对象所属部门 */
    private String AppealObjDep;
    /** 上级经理 */
    private String DirectBoss;
    /** 投诉内容 */
    private String AppealContent;
    /** 投诉处理 */
    private String AppealFeeback;
    /** 客户态度 */
    private String CustomerOpinion;
    /** 再处理与否 */
    private String DealTwiseFlag;
    /** 客服总监意见 */
    private String ServiceMangerOpinion;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 投诉人地址 */
    private String LawManAddress;
    /** 投诉人姓名 */
    private String LawMan;
    /** 投诉人电话 */
    private String LawManTel;
    /** 投诉类别 */
    private String AppealKind;
    /** 投诉原因分类 */
    private String AppealReasonKind;
    /** 处理状态 */
    private String State;

    public static final int FIELDNUM = 27; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LGAppealSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "AppealNo";
        pk[1] = "DealNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException
    {
        LGAppealSchema cloned = (LGAppealSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getAppealNo()
    {
        return AppealNo;
    }

    public void setAppealNo(String aAppealNo)
    {
        AppealNo = aAppealNo;
    }

    public String getDealNo()
    {
        return DealNo;
    }

    public void setDealNo(String aDealNo)
    {
        DealNo = aDealNo;
    }

    public String getAppealWayNo()
    {
        return AppealWayNo;
    }

    public void setAppealWayNo(String aAppealWayNo)
    {
        AppealWayNo = aAppealWayNo;
    }

    public String getAcceptorNo()
    {
        return AcceptorNo;
    }

    public void setAcceptorNo(String aAcceptorNo)
    {
        AcceptorNo = aAcceptorNo;
    }

    public String getAcceptorName()
    {
        return AcceptorName;
    }

    public void setAcceptorName(String aAcceptorName)
    {
        AcceptorName = aAcceptorName;
    }

    public String getAcceptorPost()
    {
        return AcceptorPost;
    }

    public void setAcceptorPost(String aAcceptorPost)
    {
        AcceptorPost = aAcceptorPost;
    }

    public String getAppealObjNo()
    {
        return AppealObjNo;
    }

    public void setAppealObjNo(String aAppealObjNo)
    {
        AppealObjNo = aAppealObjNo;
    }

    public String getAppealObjName()
    {
        return AppealObjName;
    }

    public void setAppealObjName(String aAppealObjName)
    {
        AppealObjName = aAppealObjName;
    }

    public String getAppealObjPhone()
    {
        return AppealObjPhone;
    }

    public void setAppealObjPhone(String aAppealObjPhone)
    {
        AppealObjPhone = aAppealObjPhone;
    }

    public String getAppealObjDep()
    {
        return AppealObjDep;
    }

    public void setAppealObjDep(String aAppealObjDep)
    {
        AppealObjDep = aAppealObjDep;
    }

    public String getDirectBoss()
    {
        return DirectBoss;
    }

    public void setDirectBoss(String aDirectBoss)
    {
        DirectBoss = aDirectBoss;
    }

    public String getAppealContent()
    {
        return AppealContent;
    }

    public void setAppealContent(String aAppealContent)
    {
        AppealContent = aAppealContent;
    }

    public String getAppealFeeback()
    {
        return AppealFeeback;
    }

    public void setAppealFeeback(String aAppealFeeback)
    {
        AppealFeeback = aAppealFeeback;
    }

    public String getCustomerOpinion()
    {
        return CustomerOpinion;
    }

    public void setCustomerOpinion(String aCustomerOpinion)
    {
        CustomerOpinion = aCustomerOpinion;
    }

    public String getDealTwiseFlag()
    {
        return DealTwiseFlag;
    }

    public void setDealTwiseFlag(String aDealTwiseFlag)
    {
        DealTwiseFlag = aDealTwiseFlag;
    }

    public String getServiceMangerOpinion()
    {
        return ServiceMangerOpinion;
    }

    public void setServiceMangerOpinion(String aServiceMangerOpinion)
    {
        ServiceMangerOpinion = aServiceMangerOpinion;
    }

    public String getOperator()
    {
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    public String getLawManAddress()
    {
        return LawManAddress;
    }

    public void setLawManAddress(String aLawManAddress)
    {
        LawManAddress = aLawManAddress;
    }

    public String getLawMan()
    {
        return LawMan;
    }

    public void setLawMan(String aLawMan)
    {
        LawMan = aLawMan;
    }

    public String getLawManTel()
    {
        return LawManTel;
    }

    public void setLawManTel(String aLawManTel)
    {
        LawManTel = aLawManTel;
    }

    public String getAppealKind()
    {
        return AppealKind;
    }

    public void setAppealKind(String aAppealKind)
    {
        AppealKind = aAppealKind;
    }

    public String getAppealReasonKind()
    {
        return AppealReasonKind;
    }

    public void setAppealReasonKind(String aAppealReasonKind)
    {
        AppealReasonKind = aAppealReasonKind;
    }

    public String getState()
    {
        return State;
    }

    public void setState(String aState)
    {
        State = aState;
    }

    /**
     * 使用另外一个 LGAppealSchema 对象给 Schema 赋值
     * @param: aLGAppealSchema LGAppealSchema
     **/
    public void setSchema(LGAppealSchema aLGAppealSchema)
    {
        this.AppealNo = aLGAppealSchema.getAppealNo();
        this.DealNo = aLGAppealSchema.getDealNo();
        this.AppealWayNo = aLGAppealSchema.getAppealWayNo();
        this.AcceptorNo = aLGAppealSchema.getAcceptorNo();
        this.AcceptorName = aLGAppealSchema.getAcceptorName();
        this.AcceptorPost = aLGAppealSchema.getAcceptorPost();
        this.AppealObjNo = aLGAppealSchema.getAppealObjNo();
        this.AppealObjName = aLGAppealSchema.getAppealObjName();
        this.AppealObjPhone = aLGAppealSchema.getAppealObjPhone();
        this.AppealObjDep = aLGAppealSchema.getAppealObjDep();
        this.DirectBoss = aLGAppealSchema.getDirectBoss();
        this.AppealContent = aLGAppealSchema.getAppealContent();
        this.AppealFeeback = aLGAppealSchema.getAppealFeeback();
        this.CustomerOpinion = aLGAppealSchema.getCustomerOpinion();
        this.DealTwiseFlag = aLGAppealSchema.getDealTwiseFlag();
        this.ServiceMangerOpinion = aLGAppealSchema.getServiceMangerOpinion();
        this.Operator = aLGAppealSchema.getOperator();
        this.MakeDate = fDate.getDate(aLGAppealSchema.getMakeDate());
        this.MakeTime = aLGAppealSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLGAppealSchema.getModifyDate());
        this.ModifyTime = aLGAppealSchema.getModifyTime();
        this.LawManAddress = aLGAppealSchema.getLawManAddress();
        this.LawMan = aLGAppealSchema.getLawMan();
        this.LawManTel = aLGAppealSchema.getLawManTel();
        this.AppealKind = aLGAppealSchema.getAppealKind();
        this.AppealReasonKind = aLGAppealSchema.getAppealReasonKind();
        this.State = aLGAppealSchema.getState();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("AppealNo") == null)
            {
                this.AppealNo = null;
            }
            else
            {
                this.AppealNo = rs.getString("AppealNo").trim();
            }

            if (rs.getString("DealNo") == null)
            {
                this.DealNo = null;
            }
            else
            {
                this.DealNo = rs.getString("DealNo").trim();
            }

            if (rs.getString("AppealWayNo") == null)
            {
                this.AppealWayNo = null;
            }
            else
            {
                this.AppealWayNo = rs.getString("AppealWayNo").trim();
            }

            if (rs.getString("AcceptorNo") == null)
            {
                this.AcceptorNo = null;
            }
            else
            {
                this.AcceptorNo = rs.getString("AcceptorNo").trim();
            }

            if (rs.getString("AcceptorName") == null)
            {
                this.AcceptorName = null;
            }
            else
            {
                this.AcceptorName = rs.getString("AcceptorName").trim();
            }

            if (rs.getString("AcceptorPost") == null)
            {
                this.AcceptorPost = null;
            }
            else
            {
                this.AcceptorPost = rs.getString("AcceptorPost").trim();
            }

            if (rs.getString("AppealObjNo") == null)
            {
                this.AppealObjNo = null;
            }
            else
            {
                this.AppealObjNo = rs.getString("AppealObjNo").trim();
            }

            if (rs.getString("AppealObjName") == null)
            {
                this.AppealObjName = null;
            }
            else
            {
                this.AppealObjName = rs.getString("AppealObjName").trim();
            }

            if (rs.getString("AppealObjPhone") == null)
            {
                this.AppealObjPhone = null;
            }
            else
            {
                this.AppealObjPhone = rs.getString("AppealObjPhone").trim();
            }

            if (rs.getString("AppealObjDep") == null)
            {
                this.AppealObjDep = null;
            }
            else
            {
                this.AppealObjDep = rs.getString("AppealObjDep").trim();
            }

            if (rs.getString("DirectBoss") == null)
            {
                this.DirectBoss = null;
            }
            else
            {
                this.DirectBoss = rs.getString("DirectBoss").trim();
            }

            if (rs.getString("AppealContent") == null)
            {
                this.AppealContent = null;
            }
            else
            {
                this.AppealContent = rs.getString("AppealContent").trim();
            }

            if (rs.getString("AppealFeeback") == null)
            {
                this.AppealFeeback = null;
            }
            else
            {
                this.AppealFeeback = rs.getString("AppealFeeback").trim();
            }

            if (rs.getString("CustomerOpinion") == null)
            {
                this.CustomerOpinion = null;
            }
            else
            {
                this.CustomerOpinion = rs.getString("CustomerOpinion").trim();
            }

            if (rs.getString("DealTwiseFlag") == null)
            {
                this.DealTwiseFlag = null;
            }
            else
            {
                this.DealTwiseFlag = rs.getString("DealTwiseFlag").trim();
            }

            if (rs.getString("ServiceMangerOpinion") == null)
            {
                this.ServiceMangerOpinion = null;
            }
            else
            {
                this.ServiceMangerOpinion = rs.getString("ServiceMangerOpinion").
                                            trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("LawManAddress") == null)
            {
                this.LawManAddress = null;
            }
            else
            {
                this.LawManAddress = rs.getString("LawManAddress").trim();
            }

            if (rs.getString("LawMan") == null)
            {
                this.LawMan = null;
            }
            else
            {
                this.LawMan = rs.getString("LawMan").trim();
            }

            if (rs.getString("LawManTel") == null)
            {
                this.LawManTel = null;
            }
            else
            {
                this.LawManTel = rs.getString("LawManTel").trim();
            }

            if (rs.getString("AppealKind") == null)
            {
                this.AppealKind = null;
            }
            else
            {
                this.AppealKind = rs.getString("AppealKind").trim();
            }

            if (rs.getString("AppealReasonKind") == null)
            {
                this.AppealReasonKind = null;
            }
            else
            {
                this.AppealReasonKind = rs.getString("AppealReasonKind").trim();
            }

            if (rs.getString("State") == null)
            {
                this.State = null;
            }
            else
            {
                this.State = rs.getString("State").trim();
            }

        }
        catch (SQLException sqle)
        {
            System.out.println("数据库中的LGAppeal表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LGAppealSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LGAppealSchema getSchema()
    {
        LGAppealSchema aLGAppealSchema = new LGAppealSchema();
        aLGAppealSchema.setSchema(this);
        return aLGAppealSchema;
    }

    public LGAppealDB getDB()
    {
        LGAppealDB aDBOper = new LGAppealDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLGAppeal描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(AppealNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DealNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppealWayNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AcceptorNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AcceptorName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AcceptorPost));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppealObjNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppealObjName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppealObjPhone));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppealObjDep));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DirectBoss));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppealContent));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppealFeeback));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CustomerOpinion));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DealTwiseFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServiceMangerOpinion));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(LawManAddress));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(LawMan));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(LawManTel));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppealKind));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppealReasonKind));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(State));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLGAppeal>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            AppealNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            DealNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                    SysConst.PACKAGESPILTER);
            AppealWayNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                         SysConst.PACKAGESPILTER);
            AcceptorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                        SysConst.PACKAGESPILTER);
            AcceptorName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                          SysConst.PACKAGESPILTER);
            AcceptorPost = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                          SysConst.PACKAGESPILTER);
            AppealObjNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                         SysConst.PACKAGESPILTER);
            AppealObjName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                           SysConst.PACKAGESPILTER);
            AppealObjPhone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                            SysConst.PACKAGESPILTER);
            AppealObjDep = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                          SysConst.PACKAGESPILTER);
            DirectBoss = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                        SysConst.PACKAGESPILTER);
            AppealContent = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                           SysConst.PACKAGESPILTER);
            AppealFeeback = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                           SysConst.PACKAGESPILTER);
            CustomerOpinion = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             14, SysConst.PACKAGESPILTER);
            DealTwiseFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                           SysConst.PACKAGESPILTER);
            ServiceMangerOpinion = StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 16, SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 18, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 20, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,
                                        SysConst.PACKAGESPILTER);
            LawManAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,
                                           SysConst.PACKAGESPILTER);
            LawMan = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,
                                    SysConst.PACKAGESPILTER);
            LawManTel = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,
                                       SysConst.PACKAGESPILTER);
            AppealKind = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,
                                        SysConst.PACKAGESPILTER);
            AppealReasonKind = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                              26, SysConst.PACKAGESPILTER);
            State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27,
                                   SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LGAppealSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("AppealNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppealNo));
        }
        if (FCode.equals("DealNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DealNo));
        }
        if (FCode.equals("AppealWayNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppealWayNo));
        }
        if (FCode.equals("AcceptorNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AcceptorNo));
        }
        if (FCode.equals("AcceptorName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AcceptorName));
        }
        if (FCode.equals("AcceptorPost"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AcceptorPost));
        }
        if (FCode.equals("AppealObjNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppealObjNo));
        }
        if (FCode.equals("AppealObjName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppealObjName));
        }
        if (FCode.equals("AppealObjPhone"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppealObjPhone));
        }
        if (FCode.equals("AppealObjDep"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppealObjDep));
        }
        if (FCode.equals("DirectBoss"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DirectBoss));
        }
        if (FCode.equals("AppealContent"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppealContent));
        }
        if (FCode.equals("AppealFeeback"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppealFeeback));
        }
        if (FCode.equals("CustomerOpinion"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerOpinion));
        }
        if (FCode.equals("DealTwiseFlag"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DealTwiseFlag));
        }
        if (FCode.equals("ServiceMangerOpinion"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(
                    ServiceMangerOpinion));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("LawManAddress"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LawManAddress));
        }
        if (FCode.equals("LawMan"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LawMan));
        }
        if (FCode.equals("LawManTel"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LawManTel));
        }
        if (FCode.equals("AppealKind"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppealKind));
        }
        if (FCode.equals("AppealReasonKind"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppealReasonKind));
        }
        if (FCode.equals("State"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(AppealNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(DealNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(AppealWayNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(AcceptorNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(AcceptorName);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(AcceptorPost);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(AppealObjNo);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(AppealObjName);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(AppealObjPhone);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(AppealObjDep);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(DirectBoss);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(AppealContent);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(AppealFeeback);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(CustomerOpinion);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(DealTwiseFlag);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(ServiceMangerOpinion);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(LawManAddress);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(LawMan);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(LawManTel);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(AppealKind);
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(AppealReasonKind);
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(State);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("AppealNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppealNo = FValue.trim();
            }
            else
            {
                AppealNo = null;
            }
        }
        if (FCode.equals("DealNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DealNo = FValue.trim();
            }
            else
            {
                DealNo = null;
            }
        }
        if (FCode.equals("AppealWayNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppealWayNo = FValue.trim();
            }
            else
            {
                AppealWayNo = null;
            }
        }
        if (FCode.equals("AcceptorNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AcceptorNo = FValue.trim();
            }
            else
            {
                AcceptorNo = null;
            }
        }
        if (FCode.equals("AcceptorName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AcceptorName = FValue.trim();
            }
            else
            {
                AcceptorName = null;
            }
        }
        if (FCode.equals("AcceptorPost"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AcceptorPost = FValue.trim();
            }
            else
            {
                AcceptorPost = null;
            }
        }
        if (FCode.equals("AppealObjNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppealObjNo = FValue.trim();
            }
            else
            {
                AppealObjNo = null;
            }
        }
        if (FCode.equals("AppealObjName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppealObjName = FValue.trim();
            }
            else
            {
                AppealObjName = null;
            }
        }
        if (FCode.equals("AppealObjPhone"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppealObjPhone = FValue.trim();
            }
            else
            {
                AppealObjPhone = null;
            }
        }
        if (FCode.equals("AppealObjDep"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppealObjDep = FValue.trim();
            }
            else
            {
                AppealObjDep = null;
            }
        }
        if (FCode.equals("DirectBoss"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DirectBoss = FValue.trim();
            }
            else
            {
                DirectBoss = null;
            }
        }
        if (FCode.equals("AppealContent"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppealContent = FValue.trim();
            }
            else
            {
                AppealContent = null;
            }
        }
        if (FCode.equals("AppealFeeback"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppealFeeback = FValue.trim();
            }
            else
            {
                AppealFeeback = null;
            }
        }
        if (FCode.equals("CustomerOpinion"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CustomerOpinion = FValue.trim();
            }
            else
            {
                CustomerOpinion = null;
            }
        }
        if (FCode.equals("DealTwiseFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DealTwiseFlag = FValue.trim();
            }
            else
            {
                DealTwiseFlag = null;
            }
        }
        if (FCode.equals("ServiceMangerOpinion"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ServiceMangerOpinion = FValue.trim();
            }
            else
            {
                ServiceMangerOpinion = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        if (FCode.equals("LawManAddress"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                LawManAddress = FValue.trim();
            }
            else
            {
                LawManAddress = null;
            }
        }
        if (FCode.equals("LawMan"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                LawMan = FValue.trim();
            }
            else
            {
                LawMan = null;
            }
        }
        if (FCode.equals("LawManTel"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                LawManTel = FValue.trim();
            }
            else
            {
                LawManTel = null;
            }
        }
        if (FCode.equals("AppealKind"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppealKind = FValue.trim();
            }
            else
            {
                AppealKind = null;
            }
        }
        if (FCode.equals("AppealReasonKind"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppealReasonKind = FValue.trim();
            }
            else
            {
                AppealReasonKind = null;
            }
        }
        if (FCode.equals("State"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
            {
                State = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LGAppealSchema other = (LGAppealSchema) otherObject;
        return
                AppealNo.equals(other.getAppealNo())
                && DealNo.equals(other.getDealNo())
                && AppealWayNo.equals(other.getAppealWayNo())
                && AcceptorNo.equals(other.getAcceptorNo())
                && AcceptorName.equals(other.getAcceptorName())
                && AcceptorPost.equals(other.getAcceptorPost())
                && AppealObjNo.equals(other.getAppealObjNo())
                && AppealObjName.equals(other.getAppealObjName())
                && AppealObjPhone.equals(other.getAppealObjPhone())
                && AppealObjDep.equals(other.getAppealObjDep())
                && DirectBoss.equals(other.getDirectBoss())
                && AppealContent.equals(other.getAppealContent())
                && AppealFeeback.equals(other.getAppealFeeback())
                && CustomerOpinion.equals(other.getCustomerOpinion())
                && DealTwiseFlag.equals(other.getDealTwiseFlag())
                && ServiceMangerOpinion.equals(other.getServiceMangerOpinion())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && LawManAddress.equals(other.getLawManAddress())
                && LawMan.equals(other.getLawMan())
                && LawManTel.equals(other.getLawManTel())
                && AppealKind.equals(other.getAppealKind())
                && AppealReasonKind.equals(other.getAppealReasonKind())
                && State.equals(other.getState());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("AppealNo"))
        {
            return 0;
        }
        if (strFieldName.equals("DealNo"))
        {
            return 1;
        }
        if (strFieldName.equals("AppealWayNo"))
        {
            return 2;
        }
        if (strFieldName.equals("AcceptorNo"))
        {
            return 3;
        }
        if (strFieldName.equals("AcceptorName"))
        {
            return 4;
        }
        if (strFieldName.equals("AcceptorPost"))
        {
            return 5;
        }
        if (strFieldName.equals("AppealObjNo"))
        {
            return 6;
        }
        if (strFieldName.equals("AppealObjName"))
        {
            return 7;
        }
        if (strFieldName.equals("AppealObjPhone"))
        {
            return 8;
        }
        if (strFieldName.equals("AppealObjDep"))
        {
            return 9;
        }
        if (strFieldName.equals("DirectBoss"))
        {
            return 10;
        }
        if (strFieldName.equals("AppealContent"))
        {
            return 11;
        }
        if (strFieldName.equals("AppealFeeback"))
        {
            return 12;
        }
        if (strFieldName.equals("CustomerOpinion"))
        {
            return 13;
        }
        if (strFieldName.equals("DealTwiseFlag"))
        {
            return 14;
        }
        if (strFieldName.equals("ServiceMangerOpinion"))
        {
            return 15;
        }
        if (strFieldName.equals("Operator"))
        {
            return 16;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 17;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 18;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 19;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 20;
        }
        if (strFieldName.equals("LawManAddress"))
        {
            return 21;
        }
        if (strFieldName.equals("LawMan"))
        {
            return 22;
        }
        if (strFieldName.equals("LawManTel"))
        {
            return 23;
        }
        if (strFieldName.equals("AppealKind"))
        {
            return 24;
        }
        if (strFieldName.equals("AppealReasonKind"))
        {
            return 25;
        }
        if (strFieldName.equals("State"))
        {
            return 26;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "AppealNo";
                break;
            case 1:
                strFieldName = "DealNo";
                break;
            case 2:
                strFieldName = "AppealWayNo";
                break;
            case 3:
                strFieldName = "AcceptorNo";
                break;
            case 4:
                strFieldName = "AcceptorName";
                break;
            case 5:
                strFieldName = "AcceptorPost";
                break;
            case 6:
                strFieldName = "AppealObjNo";
                break;
            case 7:
                strFieldName = "AppealObjName";
                break;
            case 8:
                strFieldName = "AppealObjPhone";
                break;
            case 9:
                strFieldName = "AppealObjDep";
                break;
            case 10:
                strFieldName = "DirectBoss";
                break;
            case 11:
                strFieldName = "AppealContent";
                break;
            case 12:
                strFieldName = "AppealFeeback";
                break;
            case 13:
                strFieldName = "CustomerOpinion";
                break;
            case 14:
                strFieldName = "DealTwiseFlag";
                break;
            case 15:
                strFieldName = "ServiceMangerOpinion";
                break;
            case 16:
                strFieldName = "Operator";
                break;
            case 17:
                strFieldName = "MakeDate";
                break;
            case 18:
                strFieldName = "MakeTime";
                break;
            case 19:
                strFieldName = "ModifyDate";
                break;
            case 20:
                strFieldName = "ModifyTime";
                break;
            case 21:
                strFieldName = "LawManAddress";
                break;
            case 22:
                strFieldName = "LawMan";
                break;
            case 23:
                strFieldName = "LawManTel";
                break;
            case 24:
                strFieldName = "AppealKind";
                break;
            case 25:
                strFieldName = "AppealReasonKind";
                break;
            case 26:
                strFieldName = "State";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("AppealNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DealNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppealWayNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AcceptorNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AcceptorName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AcceptorPost"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppealObjNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppealObjName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppealObjPhone"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppealObjDep"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DirectBoss"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppealContent"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppealFeeback"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CustomerOpinion"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DealTwiseFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServiceMangerOpinion"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("LawManAddress"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("LawMan"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("LawManTel"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppealKind"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppealReasonKind"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("State"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 17:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 18:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 19:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 20:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 21:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 22:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 23:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 24:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 25:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 26:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
