/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LLTreatmentDetailsDB;

/*
 * <p>ClassName: LLTreatmentDetailsSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 受益人
 * @CreateDate：2015-05-21
 */
public class LLTreatmentDetailsSchema implements Schema, Cloneable
{
	// @Field
	/** 分案案件号 */
	private String CaseNo;
	/** 明细序号 */
	private String DetailNo;
	/** 立案号/批次号 */
	private String RgtNo;
	/** 就诊顺序号 */
	private String CureNo;
	/** 汇总类型 */
	private String CureType;
	/** 医院编码 */
	private String HospitalCode;
	/** 服务项目大类编码 */
	private String SerProNo;
	/** 无含义项目编码顺序号 */
	private String NoMeanNo;
	/** 统一项目 */
	private String UnifiedPro;
	/** 服务项目名称 */
	private String SerProName;
	/** 医院服务项目名称 */
	private String HosSerProName;
	/** 本项目自付比例 */
	private double SelfRate;
	/** 剂型 */
	private String ReagentType;
	/** 规格 */
	private String Spec;
	/** 单价 */
	private double Price;
	/** 数量 */
	private double Quantity;
	/** 发生金额 */
	private double AccMoney;
	/** 全自费金额 */
	private double SelfMoney;
	/** 增负金额 */
	private double AddSelfMoney;
	/** 申报金额 */
	private double AppAmnt;
	/** 审批金额 */
	private double HandMoney;
	/** 拒付金额 */
	private double DeclineAmnt;
	/** 费用发生时间 */
	private Date AmntAccdate;
	/** 序号 */
	private String No;
	/** 医保专用票据号 */
	private String BillNo;
	/** 用法 */
	private String UseMethod;
	/** 用量 */
	private String UseLevel;
	/** 用药天数 */
	private String UseDays;
	/** 特殊情况说明 */
	private String AccDesc;
	/** 备注 */
	private String Remark;
	/** 管理机构 */
	private String MngCom;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 36;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LLTreatmentDetailsSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "CaseNo";
		pk[1] = "DetailNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LLTreatmentDetailsSchema cloned = (LLTreatmentDetailsSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getCaseNo()
	{
		return CaseNo;
	}
	public void setCaseNo(String aCaseNo)
	{
		CaseNo = aCaseNo;
	}
	public String getDetailNo()
	{
		return DetailNo;
	}
	public void setDetailNo(String aDetailNo)
	{
		DetailNo = aDetailNo;
	}
	public String getRgtNo()
	{
		return RgtNo;
	}
	public void setRgtNo(String aRgtNo)
	{
		RgtNo = aRgtNo;
	}
	public String getCureNo()
	{
		return CureNo;
	}
	public void setCureNo(String aCureNo)
	{
		CureNo = aCureNo;
	}
	public String getCureType()
	{
		return CureType;
	}
	public void setCureType(String aCureType)
	{
		CureType = aCureType;
	}
	public String getHospitalCode()
	{
		return HospitalCode;
	}
	public void setHospitalCode(String aHospitalCode)
	{
		HospitalCode = aHospitalCode;
	}
	public String getSerProNo()
	{
		return SerProNo;
	}
	public void setSerProNo(String aSerProNo)
	{
		SerProNo = aSerProNo;
	}
	public String getNoMeanNo()
	{
		return NoMeanNo;
	}
	public void setNoMeanNo(String aNoMeanNo)
	{
		NoMeanNo = aNoMeanNo;
	}
	public String getUnifiedPro()
	{
		return UnifiedPro;
	}
	public void setUnifiedPro(String aUnifiedPro)
	{
		UnifiedPro = aUnifiedPro;
	}
	public String getSerProName()
	{
		return SerProName;
	}
	public void setSerProName(String aSerProName)
	{
		SerProName = aSerProName;
	}
	public String getHosSerProName()
	{
		return HosSerProName;
	}
	public void setHosSerProName(String aHosSerProName)
	{
		HosSerProName = aHosSerProName;
	}
	public double getSelfRate()
	{
		return SelfRate;
	}
	public void setSelfRate(double aSelfRate)
	{
		SelfRate = Arith.round(aSelfRate,2);
	}
	public void setSelfRate(String aSelfRate)
	{
		if (aSelfRate != null && !aSelfRate.equals(""))
		{
			Double tDouble = new Double(aSelfRate);
			double d = tDouble.doubleValue();
                SelfRate = Arith.round(d,2);
		}
	}

	public String getReagentType()
	{
		return ReagentType;
	}
	public void setReagentType(String aReagentType)
	{
		ReagentType = aReagentType;
	}
	public String getSpec()
	{
		return Spec;
	}
	public void setSpec(String aSpec)
	{
		Spec = aSpec;
	}
	public double getPrice()
	{
		return Price;
	}
	public void setPrice(double aPrice)
	{
		Price = Arith.round(aPrice,2);
	}
	public void setPrice(String aPrice)
	{
		if (aPrice != null && !aPrice.equals(""))
		{
			Double tDouble = new Double(aPrice);
			double d = tDouble.doubleValue();
                Price = Arith.round(d,2);
		}
	}

	public double getQuantity()
	{
		return Quantity;
	}
	public void setQuantity(double aQuantity)
	{
		Quantity = Arith.round(aQuantity,2);
	}
	public void setQuantity(String aQuantity)
	{
		if (aQuantity != null && !aQuantity.equals(""))
		{
			Double tDouble = new Double(aQuantity);
			double d = tDouble.doubleValue();
                Quantity = Arith.round(d,2);
		}
	}

	public double getAccMoney()
	{
		return AccMoney;
	}
	public void setAccMoney(double aAccMoney)
	{
		AccMoney = Arith.round(aAccMoney,2);
	}
	public void setAccMoney(String aAccMoney)
	{
		if (aAccMoney != null && !aAccMoney.equals(""))
		{
			Double tDouble = new Double(aAccMoney);
			double d = tDouble.doubleValue();
                AccMoney = Arith.round(d,2);
		}
	}

	public double getSelfMoney()
	{
		return SelfMoney;
	}
	public void setSelfMoney(double aSelfMoney)
	{
		SelfMoney = Arith.round(aSelfMoney,2);
	}
	public void setSelfMoney(String aSelfMoney)
	{
		if (aSelfMoney != null && !aSelfMoney.equals(""))
		{
			Double tDouble = new Double(aSelfMoney);
			double d = tDouble.doubleValue();
                SelfMoney = Arith.round(d,2);
		}
	}

	public double getAddSelfMoney()
	{
		return AddSelfMoney;
	}
	public void setAddSelfMoney(double aAddSelfMoney)
	{
		AddSelfMoney = Arith.round(aAddSelfMoney,2);
	}
	public void setAddSelfMoney(String aAddSelfMoney)
	{
		if (aAddSelfMoney != null && !aAddSelfMoney.equals(""))
		{
			Double tDouble = new Double(aAddSelfMoney);
			double d = tDouble.doubleValue();
                AddSelfMoney = Arith.round(d,2);
		}
	}

	public double getAppAmnt()
	{
		return AppAmnt;
	}
	public void setAppAmnt(double aAppAmnt)
	{
		AppAmnt = Arith.round(aAppAmnt,2);
	}
	public void setAppAmnt(String aAppAmnt)
	{
		if (aAppAmnt != null && !aAppAmnt.equals(""))
		{
			Double tDouble = new Double(aAppAmnt);
			double d = tDouble.doubleValue();
                AppAmnt = Arith.round(d,2);
		}
	}

	public double getHandMoney()
	{
		return HandMoney;
	}
	public void setHandMoney(double aHandMoney)
	{
		HandMoney = Arith.round(aHandMoney,2);
	}
	public void setHandMoney(String aHandMoney)
	{
		if (aHandMoney != null && !aHandMoney.equals(""))
		{
			Double tDouble = new Double(aHandMoney);
			double d = tDouble.doubleValue();
                HandMoney = Arith.round(d,2);
		}
	}

	public double getDeclineAmnt()
	{
		return DeclineAmnt;
	}
	public void setDeclineAmnt(double aDeclineAmnt)
	{
		DeclineAmnt = Arith.round(aDeclineAmnt,2);
	}
	public void setDeclineAmnt(String aDeclineAmnt)
	{
		if (aDeclineAmnt != null && !aDeclineAmnt.equals(""))
		{
			Double tDouble = new Double(aDeclineAmnt);
			double d = tDouble.doubleValue();
                DeclineAmnt = Arith.round(d,2);
		}
	}

	public String getAmntAccdate()
	{
		if( AmntAccdate != null )
			return fDate.getString(AmntAccdate);
		else
			return null;
	}
	public void setAmntAccdate(Date aAmntAccdate)
	{
		AmntAccdate = aAmntAccdate;
	}
	public void setAmntAccdate(String aAmntAccdate)
	{
		if (aAmntAccdate != null && !aAmntAccdate.equals("") )
		{
			AmntAccdate = fDate.getDate( aAmntAccdate );
		}
		else
			AmntAccdate = null;
	}

	public String getNo()
	{
		return No;
	}
	public void setNo(String aNo)
	{
		No = aNo;
	}
	public String getBillNo()
	{
		return BillNo;
	}
	public void setBillNo(String aBillNo)
	{
		BillNo = aBillNo;
	}
	public String getUseMethod()
	{
		return UseMethod;
	}
	public void setUseMethod(String aUseMethod)
	{
		UseMethod = aUseMethod;
	}
	public String getUseLevel()
	{
		return UseLevel;
	}
	public void setUseLevel(String aUseLevel)
	{
		UseLevel = aUseLevel;
	}
	public String getUseDays()
	{
		return UseDays;
	}
	public void setUseDays(String aUseDays)
	{
		UseDays = aUseDays;
	}
	public String getAccDesc()
	{
		return AccDesc;
	}
	public void setAccDesc(String aAccDesc)
	{
		AccDesc = aAccDesc;
	}
	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
		Remark = aRemark;
	}
	public String getMngCom()
	{
		return MngCom;
	}
	public void setMngCom(String aMngCom)
	{
		MngCom = aMngCom;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LLTreatmentDetailsSchema 对象给 Schema 赋值
	* @param: aLLTreatmentDetailsSchema LLTreatmentDetailsSchema
	**/
	public void setSchema(LLTreatmentDetailsSchema aLLTreatmentDetailsSchema)
	{
		this.CaseNo = aLLTreatmentDetailsSchema.getCaseNo();
		this.DetailNo = aLLTreatmentDetailsSchema.getDetailNo();
		this.RgtNo = aLLTreatmentDetailsSchema.getRgtNo();
		this.CureNo = aLLTreatmentDetailsSchema.getCureNo();
		this.CureType = aLLTreatmentDetailsSchema.getCureType();
		this.HospitalCode = aLLTreatmentDetailsSchema.getHospitalCode();
		this.SerProNo = aLLTreatmentDetailsSchema.getSerProNo();
		this.NoMeanNo = aLLTreatmentDetailsSchema.getNoMeanNo();
		this.UnifiedPro = aLLTreatmentDetailsSchema.getUnifiedPro();
		this.SerProName = aLLTreatmentDetailsSchema.getSerProName();
		this.HosSerProName = aLLTreatmentDetailsSchema.getHosSerProName();
		this.SelfRate = aLLTreatmentDetailsSchema.getSelfRate();
		this.ReagentType = aLLTreatmentDetailsSchema.getReagentType();
		this.Spec = aLLTreatmentDetailsSchema.getSpec();
		this.Price = aLLTreatmentDetailsSchema.getPrice();
		this.Quantity = aLLTreatmentDetailsSchema.getQuantity();
		this.AccMoney = aLLTreatmentDetailsSchema.getAccMoney();
		this.SelfMoney = aLLTreatmentDetailsSchema.getSelfMoney();
		this.AddSelfMoney = aLLTreatmentDetailsSchema.getAddSelfMoney();
		this.AppAmnt = aLLTreatmentDetailsSchema.getAppAmnt();
		this.HandMoney = aLLTreatmentDetailsSchema.getHandMoney();
		this.DeclineAmnt = aLLTreatmentDetailsSchema.getDeclineAmnt();
		this.AmntAccdate = fDate.getDate( aLLTreatmentDetailsSchema.getAmntAccdate());
		this.No = aLLTreatmentDetailsSchema.getNo();
		this.BillNo = aLLTreatmentDetailsSchema.getBillNo();
		this.UseMethod = aLLTreatmentDetailsSchema.getUseMethod();
		this.UseLevel = aLLTreatmentDetailsSchema.getUseLevel();
		this.UseDays = aLLTreatmentDetailsSchema.getUseDays();
		this.AccDesc = aLLTreatmentDetailsSchema.getAccDesc();
		this.Remark = aLLTreatmentDetailsSchema.getRemark();
		this.MngCom = aLLTreatmentDetailsSchema.getMngCom();
		this.Operator = aLLTreatmentDetailsSchema.getOperator();
		this.MakeDate = fDate.getDate( aLLTreatmentDetailsSchema.getMakeDate());
		this.MakeTime = aLLTreatmentDetailsSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLLTreatmentDetailsSchema.getModifyDate());
		this.ModifyTime = aLLTreatmentDetailsSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("CaseNo") == null )
				this.CaseNo = null;
			else
				this.CaseNo = rs.getString("CaseNo").trim();

			if( rs.getString("DetailNo") == null )
				this.DetailNo = null;
			else
				this.DetailNo = rs.getString("DetailNo").trim();

			if( rs.getString("RgtNo") == null )
				this.RgtNo = null;
			else
				this.RgtNo = rs.getString("RgtNo").trim();

			if( rs.getString("CureNo") == null )
				this.CureNo = null;
			else
				this.CureNo = rs.getString("CureNo").trim();

			if( rs.getString("CureType") == null )
				this.CureType = null;
			else
				this.CureType = rs.getString("CureType").trim();

			if( rs.getString("HospitalCode") == null )
				this.HospitalCode = null;
			else
				this.HospitalCode = rs.getString("HospitalCode").trim();

			if( rs.getString("SerProNo") == null )
				this.SerProNo = null;
			else
				this.SerProNo = rs.getString("SerProNo").trim();

			if( rs.getString("NoMeanNo") == null )
				this.NoMeanNo = null;
			else
				this.NoMeanNo = rs.getString("NoMeanNo").trim();

			if( rs.getString("UnifiedPro") == null )
				this.UnifiedPro = null;
			else
				this.UnifiedPro = rs.getString("UnifiedPro").trim();

			if( rs.getString("SerProName") == null )
				this.SerProName = null;
			else
				this.SerProName = rs.getString("SerProName").trim();

			if( rs.getString("HosSerProName") == null )
				this.HosSerProName = null;
			else
				this.HosSerProName = rs.getString("HosSerProName").trim();

			this.SelfRate = rs.getDouble("SelfRate");
			if( rs.getString("ReagentType") == null )
				this.ReagentType = null;
			else
				this.ReagentType = rs.getString("ReagentType").trim();

			if( rs.getString("Spec") == null )
				this.Spec = null;
			else
				this.Spec = rs.getString("Spec").trim();

			this.Price = rs.getDouble("Price");
			this.Quantity = rs.getDouble("Quantity");
			this.AccMoney = rs.getDouble("AccMoney");
			this.SelfMoney = rs.getDouble("SelfMoney");
			this.AddSelfMoney = rs.getDouble("AddSelfMoney");
			this.AppAmnt = rs.getDouble("AppAmnt");
			this.HandMoney = rs.getDouble("HandMoney");
			this.DeclineAmnt = rs.getDouble("DeclineAmnt");
			this.AmntAccdate = rs.getDate("AmntAccdate");
			if( rs.getString("No") == null )
				this.No = null;
			else
				this.No = rs.getString("No").trim();

			if( rs.getString("BillNo") == null )
				this.BillNo = null;
			else
				this.BillNo = rs.getString("BillNo").trim();

			if( rs.getString("UseMethod") == null )
				this.UseMethod = null;
			else
				this.UseMethod = rs.getString("UseMethod").trim();

			if( rs.getString("UseLevel") == null )
				this.UseLevel = null;
			else
				this.UseLevel = rs.getString("UseLevel").trim();

			if( rs.getString("UseDays") == null )
				this.UseDays = null;
			else
				this.UseDays = rs.getString("UseDays").trim();

			if( rs.getString("AccDesc") == null )
				this.AccDesc = null;
			else
				this.AccDesc = rs.getString("AccDesc").trim();

			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

			if( rs.getString("MngCom") == null )
				this.MngCom = null;
			else
				this.MngCom = rs.getString("MngCom").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LLTreatmentDetails表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLTreatmentDetailsSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LLTreatmentDetailsSchema getSchema()
	{
		LLTreatmentDetailsSchema aLLTreatmentDetailsSchema = new LLTreatmentDetailsSchema();
		aLLTreatmentDetailsSchema.setSchema(this);
		return aLLTreatmentDetailsSchema;
	}

	public LLTreatmentDetailsDB getDB()
	{
		LLTreatmentDetailsDB aDBOper = new LLTreatmentDetailsDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLTreatmentDetails描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(CaseNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DetailNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RgtNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CureNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CureType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HospitalCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SerProNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(NoMeanNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UnifiedPro)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SerProName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HosSerProName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SelfRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReagentType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Spec)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Price));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Quantity));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(AccMoney));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SelfMoney));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(AddSelfMoney));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(AppAmnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(HandMoney));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(DeclineAmnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( AmntAccdate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(No)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BillNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UseMethod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UseLevel)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UseDays)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccDesc)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MngCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLTreatmentDetails>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			CaseNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			DetailNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			RgtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			CureNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			CureType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			HospitalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			SerProNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			NoMeanNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			UnifiedPro = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			SerProName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			HosSerProName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			SelfRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,12,SysConst.PACKAGESPILTER))).doubleValue();
			ReagentType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			Spec = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			Price = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,15,SysConst.PACKAGESPILTER))).doubleValue();
			Quantity = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,16,SysConst.PACKAGESPILTER))).doubleValue();
			AccMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,17,SysConst.PACKAGESPILTER))).doubleValue();
			SelfMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,18,SysConst.PACKAGESPILTER))).doubleValue();
			AddSelfMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,19,SysConst.PACKAGESPILTER))).doubleValue();
			AppAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,20,SysConst.PACKAGESPILTER))).doubleValue();
			HandMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,21,SysConst.PACKAGESPILTER))).doubleValue();
			DeclineAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,22,SysConst.PACKAGESPILTER))).doubleValue();
			AmntAccdate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,SysConst.PACKAGESPILTER));
			No = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			BillNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			UseMethod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			UseLevel = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			UseDays = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			AccDesc = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			MngCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLTreatmentDetailsSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("CaseNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CaseNo));
		}
		if (FCode.equals("DetailNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DetailNo));
		}
		if (FCode.equals("RgtNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RgtNo));
		}
		if (FCode.equals("CureNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CureNo));
		}
		if (FCode.equals("CureType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CureType));
		}
		if (FCode.equals("HospitalCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HospitalCode));
		}
		if (FCode.equals("SerProNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SerProNo));
		}
		if (FCode.equals("NoMeanNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(NoMeanNo));
		}
		if (FCode.equals("UnifiedPro"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UnifiedPro));
		}
		if (FCode.equals("SerProName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SerProName));
		}
		if (FCode.equals("HosSerProName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HosSerProName));
		}
		if (FCode.equals("SelfRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SelfRate));
		}
		if (FCode.equals("ReagentType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReagentType));
		}
		if (FCode.equals("Spec"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Spec));
		}
		if (FCode.equals("Price"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Price));
		}
		if (FCode.equals("Quantity"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Quantity));
		}
		if (FCode.equals("AccMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccMoney));
		}
		if (FCode.equals("SelfMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SelfMoney));
		}
		if (FCode.equals("AddSelfMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AddSelfMoney));
		}
		if (FCode.equals("AppAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppAmnt));
		}
		if (FCode.equals("HandMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HandMoney));
		}
		if (FCode.equals("DeclineAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DeclineAmnt));
		}
		if (FCode.equals("AmntAccdate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getAmntAccdate()));
		}
		if (FCode.equals("No"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(No));
		}
		if (FCode.equals("BillNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BillNo));
		}
		if (FCode.equals("UseMethod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UseMethod));
		}
		if (FCode.equals("UseLevel"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UseLevel));
		}
		if (FCode.equals("UseDays"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UseDays));
		}
		if (FCode.equals("AccDesc"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccDesc));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (FCode.equals("MngCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MngCom));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(CaseNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(DetailNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(RgtNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(CureNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(CureType);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(HospitalCode);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(SerProNo);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(NoMeanNo);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(UnifiedPro);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(SerProName);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(HosSerProName);
				break;
			case 11:
				strFieldValue = String.valueOf(SelfRate);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(ReagentType);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(Spec);
				break;
			case 14:
				strFieldValue = String.valueOf(Price);
				break;
			case 15:
				strFieldValue = String.valueOf(Quantity);
				break;
			case 16:
				strFieldValue = String.valueOf(AccMoney);
				break;
			case 17:
				strFieldValue = String.valueOf(SelfMoney);
				break;
			case 18:
				strFieldValue = String.valueOf(AddSelfMoney);
				break;
			case 19:
				strFieldValue = String.valueOf(AppAmnt);
				break;
			case 20:
				strFieldValue = String.valueOf(HandMoney);
				break;
			case 21:
				strFieldValue = String.valueOf(DeclineAmnt);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getAmntAccdate()));
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(No);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(BillNo);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(UseMethod);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(UseLevel);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(UseDays);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(AccDesc);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(MngCom);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("CaseNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CaseNo = FValue.trim();
			}
			else
				CaseNo = null;
		}
		if (FCode.equalsIgnoreCase("DetailNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DetailNo = FValue.trim();
			}
			else
				DetailNo = null;
		}
		if (FCode.equalsIgnoreCase("RgtNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RgtNo = FValue.trim();
			}
			else
				RgtNo = null;
		}
		if (FCode.equalsIgnoreCase("CureNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CureNo = FValue.trim();
			}
			else
				CureNo = null;
		}
		if (FCode.equalsIgnoreCase("CureType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CureType = FValue.trim();
			}
			else
				CureType = null;
		}
		if (FCode.equalsIgnoreCase("HospitalCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HospitalCode = FValue.trim();
			}
			else
				HospitalCode = null;
		}
		if (FCode.equalsIgnoreCase("SerProNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SerProNo = FValue.trim();
			}
			else
				SerProNo = null;
		}
		if (FCode.equalsIgnoreCase("NoMeanNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				NoMeanNo = FValue.trim();
			}
			else
				NoMeanNo = null;
		}
		if (FCode.equalsIgnoreCase("UnifiedPro"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UnifiedPro = FValue.trim();
			}
			else
				UnifiedPro = null;
		}
		if (FCode.equalsIgnoreCase("SerProName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SerProName = FValue.trim();
			}
			else
				SerProName = null;
		}
		if (FCode.equalsIgnoreCase("HosSerProName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HosSerProName = FValue.trim();
			}
			else
				HosSerProName = null;
		}
		if (FCode.equalsIgnoreCase("SelfRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SelfRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("ReagentType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReagentType = FValue.trim();
			}
			else
				ReagentType = null;
		}
		if (FCode.equalsIgnoreCase("Spec"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Spec = FValue.trim();
			}
			else
				Spec = null;
		}
		if (FCode.equalsIgnoreCase("Price"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Price = d;
			}
		}
		if (FCode.equalsIgnoreCase("Quantity"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Quantity = d;
			}
		}
		if (FCode.equalsIgnoreCase("AccMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				AccMoney = d;
			}
		}
		if (FCode.equalsIgnoreCase("SelfMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SelfMoney = d;
			}
		}
		if (FCode.equalsIgnoreCase("AddSelfMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				AddSelfMoney = d;
			}
		}
		if (FCode.equalsIgnoreCase("AppAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				AppAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("HandMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				HandMoney = d;
			}
		}
		if (FCode.equalsIgnoreCase("DeclineAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				DeclineAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("AmntAccdate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				AmntAccdate = fDate.getDate( FValue );
			}
			else
				AmntAccdate = null;
		}
		if (FCode.equalsIgnoreCase("No"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				No = FValue.trim();
			}
			else
				No = null;
		}
		if (FCode.equalsIgnoreCase("BillNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BillNo = FValue.trim();
			}
			else
				BillNo = null;
		}
		if (FCode.equalsIgnoreCase("UseMethod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UseMethod = FValue.trim();
			}
			else
				UseMethod = null;
		}
		if (FCode.equalsIgnoreCase("UseLevel"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UseLevel = FValue.trim();
			}
			else
				UseLevel = null;
		}
		if (FCode.equalsIgnoreCase("UseDays"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UseDays = FValue.trim();
			}
			else
				UseDays = null;
		}
		if (FCode.equalsIgnoreCase("AccDesc"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccDesc = FValue.trim();
			}
			else
				AccDesc = null;
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		if (FCode.equalsIgnoreCase("MngCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MngCom = FValue.trim();
			}
			else
				MngCom = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LLTreatmentDetailsSchema other = (LLTreatmentDetailsSchema)otherObject;
		return
			(CaseNo == null ? other.getCaseNo() == null : CaseNo.equals(other.getCaseNo()))
			&& (DetailNo == null ? other.getDetailNo() == null : DetailNo.equals(other.getDetailNo()))
			&& (RgtNo == null ? other.getRgtNo() == null : RgtNo.equals(other.getRgtNo()))
			&& (CureNo == null ? other.getCureNo() == null : CureNo.equals(other.getCureNo()))
			&& (CureType == null ? other.getCureType() == null : CureType.equals(other.getCureType()))
			&& (HospitalCode == null ? other.getHospitalCode() == null : HospitalCode.equals(other.getHospitalCode()))
			&& (SerProNo == null ? other.getSerProNo() == null : SerProNo.equals(other.getSerProNo()))
			&& (NoMeanNo == null ? other.getNoMeanNo() == null : NoMeanNo.equals(other.getNoMeanNo()))
			&& (UnifiedPro == null ? other.getUnifiedPro() == null : UnifiedPro.equals(other.getUnifiedPro()))
			&& (SerProName == null ? other.getSerProName() == null : SerProName.equals(other.getSerProName()))
			&& (HosSerProName == null ? other.getHosSerProName() == null : HosSerProName.equals(other.getHosSerProName()))
			&& SelfRate == other.getSelfRate()
			&& (ReagentType == null ? other.getReagentType() == null : ReagentType.equals(other.getReagentType()))
			&& (Spec == null ? other.getSpec() == null : Spec.equals(other.getSpec()))
			&& Price == other.getPrice()
			&& Quantity == other.getQuantity()
			&& AccMoney == other.getAccMoney()
			&& SelfMoney == other.getSelfMoney()
			&& AddSelfMoney == other.getAddSelfMoney()
			&& AppAmnt == other.getAppAmnt()
			&& HandMoney == other.getHandMoney()
			&& DeclineAmnt == other.getDeclineAmnt()
			&& (AmntAccdate == null ? other.getAmntAccdate() == null : fDate.getString(AmntAccdate).equals(other.getAmntAccdate()))
			&& (No == null ? other.getNo() == null : No.equals(other.getNo()))
			&& (BillNo == null ? other.getBillNo() == null : BillNo.equals(other.getBillNo()))
			&& (UseMethod == null ? other.getUseMethod() == null : UseMethod.equals(other.getUseMethod()))
			&& (UseLevel == null ? other.getUseLevel() == null : UseLevel.equals(other.getUseLevel()))
			&& (UseDays == null ? other.getUseDays() == null : UseDays.equals(other.getUseDays()))
			&& (AccDesc == null ? other.getAccDesc() == null : AccDesc.equals(other.getAccDesc()))
			&& (Remark == null ? other.getRemark() == null : Remark.equals(other.getRemark()))
			&& (MngCom == null ? other.getMngCom() == null : MngCom.equals(other.getMngCom()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("CaseNo") ) {
			return 0;
		}
		if( strFieldName.equals("DetailNo") ) {
			return 1;
		}
		if( strFieldName.equals("RgtNo") ) {
			return 2;
		}
		if( strFieldName.equals("CureNo") ) {
			return 3;
		}
		if( strFieldName.equals("CureType") ) {
			return 4;
		}
		if( strFieldName.equals("HospitalCode") ) {
			return 5;
		}
		if( strFieldName.equals("SerProNo") ) {
			return 6;
		}
		if( strFieldName.equals("NoMeanNo") ) {
			return 7;
		}
		if( strFieldName.equals("UnifiedPro") ) {
			return 8;
		}
		if( strFieldName.equals("SerProName") ) {
			return 9;
		}
		if( strFieldName.equals("HosSerProName") ) {
			return 10;
		}
		if( strFieldName.equals("SelfRate") ) {
			return 11;
		}
		if( strFieldName.equals("ReagentType") ) {
			return 12;
		}
		if( strFieldName.equals("Spec") ) {
			return 13;
		}
		if( strFieldName.equals("Price") ) {
			return 14;
		}
		if( strFieldName.equals("Quantity") ) {
			return 15;
		}
		if( strFieldName.equals("AccMoney") ) {
			return 16;
		}
		if( strFieldName.equals("SelfMoney") ) {
			return 17;
		}
		if( strFieldName.equals("AddSelfMoney") ) {
			return 18;
		}
		if( strFieldName.equals("AppAmnt") ) {
			return 19;
		}
		if( strFieldName.equals("HandMoney") ) {
			return 20;
		}
		if( strFieldName.equals("DeclineAmnt") ) {
			return 21;
		}
		if( strFieldName.equals("AmntAccdate") ) {
			return 22;
		}
		if( strFieldName.equals("No") ) {
			return 23;
		}
		if( strFieldName.equals("BillNo") ) {
			return 24;
		}
		if( strFieldName.equals("UseMethod") ) {
			return 25;
		}
		if( strFieldName.equals("UseLevel") ) {
			return 26;
		}
		if( strFieldName.equals("UseDays") ) {
			return 27;
		}
		if( strFieldName.equals("AccDesc") ) {
			return 28;
		}
		if( strFieldName.equals("Remark") ) {
			return 29;
		}
		if( strFieldName.equals("MngCom") ) {
			return 30;
		}
		if( strFieldName.equals("Operator") ) {
			return 31;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 32;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 33;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 34;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 35;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "CaseNo";
				break;
			case 1:
				strFieldName = "DetailNo";
				break;
			case 2:
				strFieldName = "RgtNo";
				break;
			case 3:
				strFieldName = "CureNo";
				break;
			case 4:
				strFieldName = "CureType";
				break;
			case 5:
				strFieldName = "HospitalCode";
				break;
			case 6:
				strFieldName = "SerProNo";
				break;
			case 7:
				strFieldName = "NoMeanNo";
				break;
			case 8:
				strFieldName = "UnifiedPro";
				break;
			case 9:
				strFieldName = "SerProName";
				break;
			case 10:
				strFieldName = "HosSerProName";
				break;
			case 11:
				strFieldName = "SelfRate";
				break;
			case 12:
				strFieldName = "ReagentType";
				break;
			case 13:
				strFieldName = "Spec";
				break;
			case 14:
				strFieldName = "Price";
				break;
			case 15:
				strFieldName = "Quantity";
				break;
			case 16:
				strFieldName = "AccMoney";
				break;
			case 17:
				strFieldName = "SelfMoney";
				break;
			case 18:
				strFieldName = "AddSelfMoney";
				break;
			case 19:
				strFieldName = "AppAmnt";
				break;
			case 20:
				strFieldName = "HandMoney";
				break;
			case 21:
				strFieldName = "DeclineAmnt";
				break;
			case 22:
				strFieldName = "AmntAccdate";
				break;
			case 23:
				strFieldName = "No";
				break;
			case 24:
				strFieldName = "BillNo";
				break;
			case 25:
				strFieldName = "UseMethod";
				break;
			case 26:
				strFieldName = "UseLevel";
				break;
			case 27:
				strFieldName = "UseDays";
				break;
			case 28:
				strFieldName = "AccDesc";
				break;
			case 29:
				strFieldName = "Remark";
				break;
			case 30:
				strFieldName = "MngCom";
				break;
			case 31:
				strFieldName = "Operator";
				break;
			case 32:
				strFieldName = "MakeDate";
				break;
			case 33:
				strFieldName = "MakeTime";
				break;
			case 34:
				strFieldName = "ModifyDate";
				break;
			case 35:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("CaseNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DetailNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RgtNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CureNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CureType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HospitalCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SerProNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("NoMeanNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UnifiedPro") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SerProName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HosSerProName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SelfRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ReagentType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Spec") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Price") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Quantity") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("AccMoney") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SelfMoney") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("AddSelfMoney") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("AppAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("HandMoney") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("DeclineAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("AmntAccdate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("No") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BillNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UseMethod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UseLevel") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UseDays") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccDesc") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MngCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 15:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 16:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 17:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 18:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 19:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 20:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 21:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 22:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 32:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 35:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
