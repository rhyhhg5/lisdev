/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LAHolsDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LAHolsSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-03-30
 */
public class LAHolsSchema implements Schema
{
    // @Field
    /** 代理人编码 */
    private String AgentCode;
    /** 代理人展业机构代码 */
    private String AgentGroup;
    /** 管理机构 */
    private String ManageCom;
    /** 纪录顺序号 */
    private int Idx;
    /** 指标计算编码 */
    private String IndexCalNo;
    /** 请假类型 */
    private String AClass;
    /** 假期天数 */
    private int VacDays;
    /** 请假日期 */
    private Date LeaveDate;
    /** 销假日期 */
    private Date EndDate;
    /** 缺勤天数 */
    private String AbsDays;
    /** 请假单填写标志 */
    private String FillFlag;
    /** 诊断证明书标志 */
    private String ConfIdenFlag;
    /** 产假增加标志 */
    private String AddVacFlag;
    /** 金额 */
    private double SumMoney;
    /** 核准人 */
    private String ApproveCode;
    /** 批注 */
    private String Noti;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 展业类型 */
    private String BranchType;
    /** 应销假日期 */
    private Date ShouldEndDate;
    /** 请假状态 */
    private String LeaveState;
    /** 展业机构外部编码 */
    private String BranchAttr;
    /** 渠道 */
    private String BranchType2;

    public static final int FIELDNUM = 26; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LAHolsSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "AgentCode";
        pk[1] = "Idx";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getAgentCode()
    {
        if (SysConst.CHANGECHARSET && AgentCode != null && !AgentCode.equals(""))
        {
            AgentCode = StrTool.unicodeToGBK(AgentCode);
        }
        return AgentCode;
    }

    public void setAgentCode(String aAgentCode)
    {
        AgentCode = aAgentCode;
    }

    public String getAgentGroup()
    {
        if (SysConst.CHANGECHARSET && AgentGroup != null &&
            !AgentGroup.equals(""))
        {
            AgentGroup = StrTool.unicodeToGBK(AgentGroup);
        }
        return AgentGroup;
    }

    public void setAgentGroup(String aAgentGroup)
    {
        AgentGroup = aAgentGroup;
    }

    public String getManageCom()
    {
        if (SysConst.CHANGECHARSET && ManageCom != null && !ManageCom.equals(""))
        {
            ManageCom = StrTool.unicodeToGBK(ManageCom);
        }
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public int getIdx()
    {
        return Idx;
    }

    public void setIdx(int aIdx)
    {
        Idx = aIdx;
    }

    public void setIdx(String aIdx)
    {
        if (aIdx != null && !aIdx.equals(""))
        {
            Integer tInteger = new Integer(aIdx);
            int i = tInteger.intValue();
            Idx = i;
        }
    }

    public String getIndexCalNo()
    {
        if (SysConst.CHANGECHARSET && IndexCalNo != null &&
            !IndexCalNo.equals(""))
        {
            IndexCalNo = StrTool.unicodeToGBK(IndexCalNo);
        }
        return IndexCalNo;
    }

    public void setIndexCalNo(String aIndexCalNo)
    {
        IndexCalNo = aIndexCalNo;
    }

    public String getAClass()
    {
        if (SysConst.CHANGECHARSET && AClass != null && !AClass.equals(""))
        {
            AClass = StrTool.unicodeToGBK(AClass);
        }
        return AClass;
    }

    public void setAClass(String aAClass)
    {
        AClass = aAClass;
    }

    public int getVacDays()
    {
        return VacDays;
    }

    public void setVacDays(int aVacDays)
    {
        VacDays = aVacDays;
    }

    public void setVacDays(String aVacDays)
    {
        if (aVacDays != null && !aVacDays.equals(""))
        {
            Integer tInteger = new Integer(aVacDays);
            int i = tInteger.intValue();
            VacDays = i;
        }
    }

    public String getLeaveDate()
    {
        if (LeaveDate != null)
        {
            return fDate.getString(LeaveDate);
        }
        else
        {
            return null;
        }
    }

    public void setLeaveDate(Date aLeaveDate)
    {
        LeaveDate = aLeaveDate;
    }

    public void setLeaveDate(String aLeaveDate)
    {
        if (aLeaveDate != null && !aLeaveDate.equals(""))
        {
            LeaveDate = fDate.getDate(aLeaveDate);
        }
        else
        {
            LeaveDate = null;
        }
    }

    public String getEndDate()
    {
        if (EndDate != null)
        {
            return fDate.getString(EndDate);
        }
        else
        {
            return null;
        }
    }

    public void setEndDate(Date aEndDate)
    {
        EndDate = aEndDate;
    }

    public void setEndDate(String aEndDate)
    {
        if (aEndDate != null && !aEndDate.equals(""))
        {
            EndDate = fDate.getDate(aEndDate);
        }
        else
        {
            EndDate = null;
        }
    }

    public String getAbsDays()
    {
        if (SysConst.CHANGECHARSET && AbsDays != null && !AbsDays.equals(""))
        {
            AbsDays = StrTool.unicodeToGBK(AbsDays);
        }
        return AbsDays;
    }

    public void setAbsDays(String aAbsDays)
    {
        AbsDays = aAbsDays;
    }

    public String getFillFlag()
    {
        if (SysConst.CHANGECHARSET && FillFlag != null && !FillFlag.equals(""))
        {
            FillFlag = StrTool.unicodeToGBK(FillFlag);
        }
        return FillFlag;
    }

    public void setFillFlag(String aFillFlag)
    {
        FillFlag = aFillFlag;
    }

    public String getConfIdenFlag()
    {
        if (SysConst.CHANGECHARSET && ConfIdenFlag != null &&
            !ConfIdenFlag.equals(""))
        {
            ConfIdenFlag = StrTool.unicodeToGBK(ConfIdenFlag);
        }
        return ConfIdenFlag;
    }

    public void setConfIdenFlag(String aConfIdenFlag)
    {
        ConfIdenFlag = aConfIdenFlag;
    }

    public String getAddVacFlag()
    {
        if (SysConst.CHANGECHARSET && AddVacFlag != null &&
            !AddVacFlag.equals(""))
        {
            AddVacFlag = StrTool.unicodeToGBK(AddVacFlag);
        }
        return AddVacFlag;
    }

    public void setAddVacFlag(String aAddVacFlag)
    {
        AddVacFlag = aAddVacFlag;
    }

    public double getSumMoney()
    {
        return SumMoney;
    }

    public void setSumMoney(double aSumMoney)
    {
        SumMoney = aSumMoney;
    }

    public void setSumMoney(String aSumMoney)
    {
        if (aSumMoney != null && !aSumMoney.equals(""))
        {
            Double tDouble = new Double(aSumMoney);
            double d = tDouble.doubleValue();
            SumMoney = d;
        }
    }

    public String getApproveCode()
    {
        if (SysConst.CHANGECHARSET && ApproveCode != null &&
            !ApproveCode.equals(""))
        {
            ApproveCode = StrTool.unicodeToGBK(ApproveCode);
        }
        return ApproveCode;
    }

    public void setApproveCode(String aApproveCode)
    {
        ApproveCode = aApproveCode;
    }

    public String getNoti()
    {
        if (SysConst.CHANGECHARSET && Noti != null && !Noti.equals(""))
        {
            Noti = StrTool.unicodeToGBK(Noti);
        }
        return Noti;
    }

    public void setNoti(String aNoti)
    {
        Noti = aNoti;
    }

    public String getOperator()
    {
        if (SysConst.CHANGECHARSET && Operator != null && !Operator.equals(""))
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (SysConst.CHANGECHARSET && MakeTime != null && !MakeTime.equals(""))
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (SysConst.CHANGECHARSET && ModifyTime != null &&
            !ModifyTime.equals(""))
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    public String getBranchType()
    {
        if (SysConst.CHANGECHARSET && BranchType != null &&
            !BranchType.equals(""))
        {
            BranchType = StrTool.unicodeToGBK(BranchType);
        }
        return BranchType;
    }

    public void setBranchType(String aBranchType)
    {
        BranchType = aBranchType;
    }

    public String getShouldEndDate()
    {
        if (ShouldEndDate != null)
        {
            return fDate.getString(ShouldEndDate);
        }
        else
        {
            return null;
        }
    }

    public void setShouldEndDate(Date aShouldEndDate)
    {
        ShouldEndDate = aShouldEndDate;
    }

    public void setShouldEndDate(String aShouldEndDate)
    {
        if (aShouldEndDate != null && !aShouldEndDate.equals(""))
        {
            ShouldEndDate = fDate.getDate(aShouldEndDate);
        }
        else
        {
            ShouldEndDate = null;
        }
    }

    public String getLeaveState()
    {
        if (SysConst.CHANGECHARSET && LeaveState != null &&
            !LeaveState.equals(""))
        {
            LeaveState = StrTool.unicodeToGBK(LeaveState);
        }
        return LeaveState;
    }

    public void setLeaveState(String aLeaveState)
    {
        LeaveState = aLeaveState;
    }

    public String getBranchAttr()
    {
        if (SysConst.CHANGECHARSET && BranchAttr != null &&
            !BranchAttr.equals(""))
        {
            BranchAttr = StrTool.unicodeToGBK(BranchAttr);
        }
        return BranchAttr;
    }

    public void setBranchAttr(String aBranchAttr)
    {
        BranchAttr = aBranchAttr;
    }

    public String getBranchType2()
    {
        if (SysConst.CHANGECHARSET && BranchType2 != null &&
            !BranchType2.equals(""))
        {
            BranchType2 = StrTool.unicodeToGBK(BranchType2);
        }
        return BranchType2;
    }

    public void setBranchType2(String aBranchType2)
    {
        BranchType2 = aBranchType2;
    }

    /**
     * 使用另外一个 LAHolsSchema 对象给 Schema 赋值
     * @param: aLAHolsSchema LAHolsSchema
     **/
    public void setSchema(LAHolsSchema aLAHolsSchema)
    {
        this.AgentCode = aLAHolsSchema.getAgentCode();
        this.AgentGroup = aLAHolsSchema.getAgentGroup();
        this.ManageCom = aLAHolsSchema.getManageCom();
        this.Idx = aLAHolsSchema.getIdx();
        this.IndexCalNo = aLAHolsSchema.getIndexCalNo();
        this.AClass = aLAHolsSchema.getAClass();
        this.VacDays = aLAHolsSchema.getVacDays();
        this.LeaveDate = fDate.getDate(aLAHolsSchema.getLeaveDate());
        this.EndDate = fDate.getDate(aLAHolsSchema.getEndDate());
        this.AbsDays = aLAHolsSchema.getAbsDays();
        this.FillFlag = aLAHolsSchema.getFillFlag();
        this.ConfIdenFlag = aLAHolsSchema.getConfIdenFlag();
        this.AddVacFlag = aLAHolsSchema.getAddVacFlag();
        this.SumMoney = aLAHolsSchema.getSumMoney();
        this.ApproveCode = aLAHolsSchema.getApproveCode();
        this.Noti = aLAHolsSchema.getNoti();
        this.Operator = aLAHolsSchema.getOperator();
        this.MakeDate = fDate.getDate(aLAHolsSchema.getMakeDate());
        this.MakeTime = aLAHolsSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLAHolsSchema.getModifyDate());
        this.ModifyTime = aLAHolsSchema.getModifyTime();
        this.BranchType = aLAHolsSchema.getBranchType();
        this.ShouldEndDate = fDate.getDate(aLAHolsSchema.getShouldEndDate());
        this.LeaveState = aLAHolsSchema.getLeaveState();
        this.BranchAttr = aLAHolsSchema.getBranchAttr();
        this.BranchType2 = aLAHolsSchema.getBranchType2();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("AgentCode") == null)
            {
                this.AgentCode = null;
            }
            else
            {
                this.AgentCode = rs.getString("AgentCode").trim();
            }

            if (rs.getString("AgentGroup") == null)
            {
                this.AgentGroup = null;
            }
            else
            {
                this.AgentGroup = rs.getString("AgentGroup").trim();
            }

            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            this.Idx = rs.getInt("Idx");
            if (rs.getString("IndexCalNo") == null)
            {
                this.IndexCalNo = null;
            }
            else
            {
                this.IndexCalNo = rs.getString("IndexCalNo").trim();
            }

            if (rs.getString("AClass") == null)
            {
                this.AClass = null;
            }
            else
            {
                this.AClass = rs.getString("AClass").trim();
            }

            this.VacDays = rs.getInt("VacDays");
            this.LeaveDate = rs.getDate("LeaveDate");
            this.EndDate = rs.getDate("EndDate");
            if (rs.getString("AbsDays") == null)
            {
                this.AbsDays = null;
            }
            else
            {
                this.AbsDays = rs.getString("AbsDays").trim();
            }

            if (rs.getString("FillFlag") == null)
            {
                this.FillFlag = null;
            }
            else
            {
                this.FillFlag = rs.getString("FillFlag").trim();
            }

            if (rs.getString("ConfIdenFlag") == null)
            {
                this.ConfIdenFlag = null;
            }
            else
            {
                this.ConfIdenFlag = rs.getString("ConfIdenFlag").trim();
            }

            if (rs.getString("AddVacFlag") == null)
            {
                this.AddVacFlag = null;
            }
            else
            {
                this.AddVacFlag = rs.getString("AddVacFlag").trim();
            }

            this.SumMoney = rs.getDouble("SumMoney");
            if (rs.getString("ApproveCode") == null)
            {
                this.ApproveCode = null;
            }
            else
            {
                this.ApproveCode = rs.getString("ApproveCode").trim();
            }

            if (rs.getString("Noti") == null)
            {
                this.Noti = null;
            }
            else
            {
                this.Noti = rs.getString("Noti").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("BranchType") == null)
            {
                this.BranchType = null;
            }
            else
            {
                this.BranchType = rs.getString("BranchType").trim();
            }

            this.ShouldEndDate = rs.getDate("ShouldEndDate");
            if (rs.getString("LeaveState") == null)
            {
                this.LeaveState = null;
            }
            else
            {
                this.LeaveState = rs.getString("LeaveState").trim();
            }

            if (rs.getString("BranchAttr") == null)
            {
                this.BranchAttr = null;
            }
            else
            {
                this.BranchAttr = rs.getString("BranchAttr").trim();
            }

            if (rs.getString("BranchType2") == null)
            {
                this.BranchType2 = null;
            }
            else
            {
                this.BranchType2 = rs.getString("BranchType2").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAHolsSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LAHolsSchema getSchema()
    {
        LAHolsSchema aLAHolsSchema = new LAHolsSchema();
        aLAHolsSchema.setSchema(this);
        return aLAHolsSchema;
    }

    public LAHolsDB getDB()
    {
        LAHolsDB aDBOper = new LAHolsDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAHols描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AgentCode)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AgentGroup)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ManageCom)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Idx));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(IndexCalNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AClass)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(VacDays));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                LeaveDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                EndDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AbsDays)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(FillFlag)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ConfIdenFlag)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AddVacFlag)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SumMoney));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ApproveCode)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Noti)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Operator)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                MakeDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                ModifyDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BranchType)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                ShouldEndDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(LeaveState)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BranchAttr)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BranchType2)));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAHols>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                       SysConst.PACKAGESPILTER);
            Idx = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    4, SysConst.PACKAGESPILTER))).intValue();
            IndexCalNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                        SysConst.PACKAGESPILTER);
            AClass = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                    SysConst.PACKAGESPILTER);
            VacDays = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 7, SysConst.PACKAGESPILTER))).intValue();
            LeaveDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 8, SysConst.PACKAGESPILTER));
            EndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 9, SysConst.PACKAGESPILTER));
            AbsDays = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                     SysConst.PACKAGESPILTER);
            FillFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                      SysConst.PACKAGESPILTER);
            ConfIdenFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                          SysConst.PACKAGESPILTER);
            AddVacFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                        SysConst.PACKAGESPILTER);
            SumMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 14, SysConst.PACKAGESPILTER))).doubleValue();
            ApproveCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                         SysConst.PACKAGESPILTER);
            Noti = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                  SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 18, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 20, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,
                                        SysConst.PACKAGESPILTER);
            BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,
                                        SysConst.PACKAGESPILTER);
            ShouldEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 23, SysConst.PACKAGESPILTER));
            LeaveState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,
                                        SysConst.PACKAGESPILTER);
            BranchAttr = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,
                                        SysConst.PACKAGESPILTER);
            BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26,
                                         SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAHolsSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("AgentCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equals("AgentGroup"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
        }
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("Idx"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Idx));
        }
        if (FCode.equals("IndexCalNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IndexCalNo));
        }
        if (FCode.equals("AClass"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AClass));
        }
        if (FCode.equals("VacDays"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VacDays));
        }
        if (FCode.equals("LeaveDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getLeaveDate()));
        }
        if (FCode.equals("EndDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getEndDate()));
        }
        if (FCode.equals("AbsDays"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AbsDays));
        }
        if (FCode.equals("FillFlag"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FillFlag));
        }
        if (FCode.equals("ConfIdenFlag"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ConfIdenFlag));
        }
        if (FCode.equals("AddVacFlag"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AddVacFlag));
        }
        if (FCode.equals("SumMoney"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumMoney));
        }
        if (FCode.equals("ApproveCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveCode));
        }
        if (FCode.equals("Noti"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Noti));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("BranchType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
        }
        if (FCode.equals("ShouldEndDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getShouldEndDate()));
        }
        if (FCode.equals("LeaveState"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LeaveState));
        }
        if (FCode.equals("BranchAttr"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchAttr));
        }
        if (FCode.equals("BranchType2"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(AgentCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(AgentGroup);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 3:
                strFieldValue = String.valueOf(Idx);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(IndexCalNo);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(AClass);
                break;
            case 6:
                strFieldValue = String.valueOf(VacDays);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getLeaveDate()));
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getEndDate()));
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(AbsDays);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(FillFlag);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(ConfIdenFlag);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(AddVacFlag);
                break;
            case 13:
                strFieldValue = String.valueOf(SumMoney);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(ApproveCode);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(Noti);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(BranchType);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getShouldEndDate()));
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(LeaveState);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(BranchAttr);
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(BranchType2);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("AgentCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
            {
                AgentCode = null;
            }
        }
        if (FCode.equals("AgentGroup"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
            {
                AgentGroup = null;
            }
        }
        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("Idx"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                Idx = i;
            }
        }
        if (FCode.equals("IndexCalNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                IndexCalNo = FValue.trim();
            }
            else
            {
                IndexCalNo = null;
            }
        }
        if (FCode.equals("AClass"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AClass = FValue.trim();
            }
            else
            {
                AClass = null;
            }
        }
        if (FCode.equals("VacDays"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                VacDays = i;
            }
        }
        if (FCode.equals("LeaveDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                LeaveDate = fDate.getDate(FValue);
            }
            else
            {
                LeaveDate = null;
            }
        }
        if (FCode.equals("EndDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EndDate = fDate.getDate(FValue);
            }
            else
            {
                EndDate = null;
            }
        }
        if (FCode.equals("AbsDays"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AbsDays = FValue.trim();
            }
            else
            {
                AbsDays = null;
            }
        }
        if (FCode.equals("FillFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                FillFlag = FValue.trim();
            }
            else
            {
                FillFlag = null;
            }
        }
        if (FCode.equals("ConfIdenFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ConfIdenFlag = FValue.trim();
            }
            else
            {
                ConfIdenFlag = null;
            }
        }
        if (FCode.equals("AddVacFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AddVacFlag = FValue.trim();
            }
            else
            {
                AddVacFlag = null;
            }
        }
        if (FCode.equals("SumMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                SumMoney = d;
            }
        }
        if (FCode.equals("ApproveCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ApproveCode = FValue.trim();
            }
            else
            {
                ApproveCode = null;
            }
        }
        if (FCode.equals("Noti"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Noti = FValue.trim();
            }
            else
            {
                Noti = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        if (FCode.equals("BranchType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchType = FValue.trim();
            }
            else
            {
                BranchType = null;
            }
        }
        if (FCode.equals("ShouldEndDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ShouldEndDate = fDate.getDate(FValue);
            }
            else
            {
                ShouldEndDate = null;
            }
        }
        if (FCode.equals("LeaveState"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                LeaveState = FValue.trim();
            }
            else
            {
                LeaveState = null;
            }
        }
        if (FCode.equals("BranchAttr"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchAttr = FValue.trim();
            }
            else
            {
                BranchAttr = null;
            }
        }
        if (FCode.equals("BranchType2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchType2 = FValue.trim();
            }
            else
            {
                BranchType2 = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LAHolsSchema other = (LAHolsSchema) otherObject;
        return
                AgentCode.equals(other.getAgentCode())
                && AgentGroup.equals(other.getAgentGroup())
                && ManageCom.equals(other.getManageCom())
                && Idx == other.getIdx()
                && IndexCalNo.equals(other.getIndexCalNo())
                && AClass.equals(other.getAClass())
                && VacDays == other.getVacDays()
                && fDate.getString(LeaveDate).equals(other.getLeaveDate())
                && fDate.getString(EndDate).equals(other.getEndDate())
                && AbsDays.equals(other.getAbsDays())
                && FillFlag.equals(other.getFillFlag())
                && ConfIdenFlag.equals(other.getConfIdenFlag())
                && AddVacFlag.equals(other.getAddVacFlag())
                && SumMoney == other.getSumMoney()
                && ApproveCode.equals(other.getApproveCode())
                && Noti.equals(other.getNoti())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && BranchType.equals(other.getBranchType())
                && fDate.getString(ShouldEndDate).equals(other.getShouldEndDate())
                && LeaveState.equals(other.getLeaveState())
                && BranchAttr.equals(other.getBranchAttr())
                && BranchType2.equals(other.getBranchType2());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("AgentCode"))
        {
            return 0;
        }
        if (strFieldName.equals("AgentGroup"))
        {
            return 1;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return 2;
        }
        if (strFieldName.equals("Idx"))
        {
            return 3;
        }
        if (strFieldName.equals("IndexCalNo"))
        {
            return 4;
        }
        if (strFieldName.equals("AClass"))
        {
            return 5;
        }
        if (strFieldName.equals("VacDays"))
        {
            return 6;
        }
        if (strFieldName.equals("LeaveDate"))
        {
            return 7;
        }
        if (strFieldName.equals("EndDate"))
        {
            return 8;
        }
        if (strFieldName.equals("AbsDays"))
        {
            return 9;
        }
        if (strFieldName.equals("FillFlag"))
        {
            return 10;
        }
        if (strFieldName.equals("ConfIdenFlag"))
        {
            return 11;
        }
        if (strFieldName.equals("AddVacFlag"))
        {
            return 12;
        }
        if (strFieldName.equals("SumMoney"))
        {
            return 13;
        }
        if (strFieldName.equals("ApproveCode"))
        {
            return 14;
        }
        if (strFieldName.equals("Noti"))
        {
            return 15;
        }
        if (strFieldName.equals("Operator"))
        {
            return 16;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 17;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 18;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 19;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 20;
        }
        if (strFieldName.equals("BranchType"))
        {
            return 21;
        }
        if (strFieldName.equals("ShouldEndDate"))
        {
            return 22;
        }
        if (strFieldName.equals("LeaveState"))
        {
            return 23;
        }
        if (strFieldName.equals("BranchAttr"))
        {
            return 24;
        }
        if (strFieldName.equals("BranchType2"))
        {
            return 25;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "AgentCode";
                break;
            case 1:
                strFieldName = "AgentGroup";
                break;
            case 2:
                strFieldName = "ManageCom";
                break;
            case 3:
                strFieldName = "Idx";
                break;
            case 4:
                strFieldName = "IndexCalNo";
                break;
            case 5:
                strFieldName = "AClass";
                break;
            case 6:
                strFieldName = "VacDays";
                break;
            case 7:
                strFieldName = "LeaveDate";
                break;
            case 8:
                strFieldName = "EndDate";
                break;
            case 9:
                strFieldName = "AbsDays";
                break;
            case 10:
                strFieldName = "FillFlag";
                break;
            case 11:
                strFieldName = "ConfIdenFlag";
                break;
            case 12:
                strFieldName = "AddVacFlag";
                break;
            case 13:
                strFieldName = "SumMoney";
                break;
            case 14:
                strFieldName = "ApproveCode";
                break;
            case 15:
                strFieldName = "Noti";
                break;
            case 16:
                strFieldName = "Operator";
                break;
            case 17:
                strFieldName = "MakeDate";
                break;
            case 18:
                strFieldName = "MakeTime";
                break;
            case 19:
                strFieldName = "ModifyDate";
                break;
            case 20:
                strFieldName = "ModifyTime";
                break;
            case 21:
                strFieldName = "BranchType";
                break;
            case 22:
                strFieldName = "ShouldEndDate";
                break;
            case 23:
                strFieldName = "LeaveState";
                break;
            case 24:
                strFieldName = "BranchAttr";
                break;
            case 25:
                strFieldName = "BranchType2";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("AgentCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentGroup"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Idx"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("IndexCalNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AClass"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("VacDays"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("LeaveDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("EndDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("AbsDays"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FillFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ConfIdenFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AddVacFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SumMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("ApproveCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Noti"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ShouldEndDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("LeaveState"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchAttr"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchType2"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_INT;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_INT;
                break;
            case 7:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 8:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 17:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 18:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 19:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 20:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 21:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 22:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 23:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 24:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 25:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
