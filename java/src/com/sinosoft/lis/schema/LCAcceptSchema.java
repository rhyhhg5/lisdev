/*
 * <p>ClassName: LCAcceptSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LCAcceptDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LCAcceptSchema implements Schema
{
    // @Field
    /** 印刷号码 */
    private String PrtNo;
    /** 管理机构 */
    private String ManageCom;
    /** 险种编码 */
    private String RiskCode;
    /** 险种版本 */
    private String RiskVersion;
    /** 团单标志 */
    private String GrpFlag;
    /** 体件标志 */
    private String MEflag;
    /** 交费金额 */
    private double PayMoney;
    /** 投保人名称 */
    private String AppntName;
    /** 被保人数目 */
    private int InsuredPeoples;
    /** 代理人编码 */
    private String AgentCode;
    /** 附件份数 */
    private int AffixNum;
    /** 附件页数 */
    private int AffixPageNum;

    public static final int FIELDNUM = 12; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LCAcceptSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "PrtNo";
        pk[1] = "ManageCom";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getPrtNo()
    {
        if (PrtNo != null && !PrtNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            PrtNo = StrTool.unicodeToGBK(PrtNo);
        }
        return PrtNo;
    }

    public void setPrtNo(String aPrtNo)
    {
        PrtNo = aPrtNo;
    }

    public String getManageCom()
    {
        if (ManageCom != null && !ManageCom.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ManageCom = StrTool.unicodeToGBK(ManageCom);
        }
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public String getRiskCode()
    {
        if (RiskCode != null && !RiskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getRiskVersion()
    {
        if (RiskVersion != null && !RiskVersion.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            RiskVersion = StrTool.unicodeToGBK(RiskVersion);
        }
        return RiskVersion;
    }

    public void setRiskVersion(String aRiskVersion)
    {
        RiskVersion = aRiskVersion;
    }

    public String getGrpFlag()
    {
        if (GrpFlag != null && !GrpFlag.equals("") && SysConst.CHANGECHARSET == true)
        {
            GrpFlag = StrTool.unicodeToGBK(GrpFlag);
        }
        return GrpFlag;
    }

    public void setGrpFlag(String aGrpFlag)
    {
        GrpFlag = aGrpFlag;
    }

    public String getMEflag()
    {
        if (MEflag != null && !MEflag.equals("") && SysConst.CHANGECHARSET == true)
        {
            MEflag = StrTool.unicodeToGBK(MEflag);
        }
        return MEflag;
    }

    public void setMEflag(String aMEflag)
    {
        MEflag = aMEflag;
    }

    public double getPayMoney()
    {
        return PayMoney;
    }

    public void setPayMoney(double aPayMoney)
    {
        PayMoney = aPayMoney;
    }

    public void setPayMoney(String aPayMoney)
    {
        if (aPayMoney != null && !aPayMoney.equals(""))
        {
            Double tDouble = new Double(aPayMoney);
            double d = tDouble.doubleValue();
            PayMoney = d;
        }
    }

    public String getAppntName()
    {
        if (AppntName != null && !AppntName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AppntName = StrTool.unicodeToGBK(AppntName);
        }
        return AppntName;
    }

    public void setAppntName(String aAppntName)
    {
        AppntName = aAppntName;
    }

    public int getInsuredPeoples()
    {
        return InsuredPeoples;
    }

    public void setInsuredPeoples(int aInsuredPeoples)
    {
        InsuredPeoples = aInsuredPeoples;
    }

    public void setInsuredPeoples(String aInsuredPeoples)
    {
        if (aInsuredPeoples != null && !aInsuredPeoples.equals(""))
        {
            Integer tInteger = new Integer(aInsuredPeoples);
            int i = tInteger.intValue();
            InsuredPeoples = i;
        }
    }

    public String getAgentCode()
    {
        if (AgentCode != null && !AgentCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AgentCode = StrTool.unicodeToGBK(AgentCode);
        }
        return AgentCode;
    }

    public void setAgentCode(String aAgentCode)
    {
        AgentCode = aAgentCode;
    }

    public int getAffixNum()
    {
        return AffixNum;
    }

    public void setAffixNum(int aAffixNum)
    {
        AffixNum = aAffixNum;
    }

    public void setAffixNum(String aAffixNum)
    {
        if (aAffixNum != null && !aAffixNum.equals(""))
        {
            Integer tInteger = new Integer(aAffixNum);
            int i = tInteger.intValue();
            AffixNum = i;
        }
    }

    public int getAffixPageNum()
    {
        return AffixPageNum;
    }

    public void setAffixPageNum(int aAffixPageNum)
    {
        AffixPageNum = aAffixPageNum;
    }

    public void setAffixPageNum(String aAffixPageNum)
    {
        if (aAffixPageNum != null && !aAffixPageNum.equals(""))
        {
            Integer tInteger = new Integer(aAffixPageNum);
            int i = tInteger.intValue();
            AffixPageNum = i;
        }
    }


    /**
     * 使用另外一个 LCAcceptSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LCAcceptSchema aLCAcceptSchema)
    {
        this.PrtNo = aLCAcceptSchema.getPrtNo();
        this.ManageCom = aLCAcceptSchema.getManageCom();
        this.RiskCode = aLCAcceptSchema.getRiskCode();
        this.RiskVersion = aLCAcceptSchema.getRiskVersion();
        this.GrpFlag = aLCAcceptSchema.getGrpFlag();
        this.MEflag = aLCAcceptSchema.getMEflag();
        this.PayMoney = aLCAcceptSchema.getPayMoney();
        this.AppntName = aLCAcceptSchema.getAppntName();
        this.InsuredPeoples = aLCAcceptSchema.getInsuredPeoples();
        this.AgentCode = aLCAcceptSchema.getAgentCode();
        this.AffixNum = aLCAcceptSchema.getAffixNum();
        this.AffixPageNum = aLCAcceptSchema.getAffixPageNum();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("PrtNo") == null)
            {
                this.PrtNo = null;
            }
            else
            {
                this.PrtNo = rs.getString("PrtNo").trim();
            }

            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("RiskVersion") == null)
            {
                this.RiskVersion = null;
            }
            else
            {
                this.RiskVersion = rs.getString("RiskVersion").trim();
            }

            if (rs.getString("GrpFlag") == null)
            {
                this.GrpFlag = null;
            }
            else
            {
                this.GrpFlag = rs.getString("GrpFlag").trim();
            }

            if (rs.getString("MEflag") == null)
            {
                this.MEflag = null;
            }
            else
            {
                this.MEflag = rs.getString("MEflag").trim();
            }

            this.PayMoney = rs.getDouble("PayMoney");
            if (rs.getString("AppntName") == null)
            {
                this.AppntName = null;
            }
            else
            {
                this.AppntName = rs.getString("AppntName").trim();
            }

            this.InsuredPeoples = rs.getInt("InsuredPeoples");
            if (rs.getString("AgentCode") == null)
            {
                this.AgentCode = null;
            }
            else
            {
                this.AgentCode = rs.getString("AgentCode").trim();
            }

            this.AffixNum = rs.getInt("AffixNum");
            this.AffixPageNum = rs.getInt("AffixPageNum");
        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAcceptSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LCAcceptSchema getSchema()
    {
        LCAcceptSchema aLCAcceptSchema = new LCAcceptSchema();
        aLCAcceptSchema.setSchema(this);
        return aLCAcceptSchema;
    }

    public LCAcceptDB getDB()
    {
        LCAcceptDB aDBOper = new LCAcceptDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCAccept描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(PrtNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ManageCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskVersion)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GrpFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MEflag)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(PayMoney) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AppntName)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(InsuredPeoples) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgentCode)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(AffixNum) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(AffixPageNum);
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCAccept>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                   SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                       SysConst.PACKAGESPILTER);
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            RiskVersion = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                         SysConst.PACKAGESPILTER);
            GrpFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                     SysConst.PACKAGESPILTER);
            MEflag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                    SysConst.PACKAGESPILTER);
            PayMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 7, SysConst.PACKAGESPILTER))).doubleValue();
            AppntName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                       SysConst.PACKAGESPILTER);
            InsuredPeoples = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 9, SysConst.PACKAGESPILTER))).intValue();
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                       SysConst.PACKAGESPILTER);
            AffixNum = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 11, SysConst.PACKAGESPILTER))).intValue();
            AffixPageNum = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 12, SysConst.PACKAGESPILTER))).intValue();
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAcceptSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("PrtNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PrtNo));
        }
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ManageCom));
        }
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskCode));
        }
        if (FCode.equals("RiskVersion"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskVersion));
        }
        if (FCode.equals("GrpFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GrpFlag));
        }
        if (FCode.equals("MEflag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MEflag));
        }
        if (FCode.equals("PayMoney"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PayMoney));
        }
        if (FCode.equals("AppntName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AppntName));
        }
        if (FCode.equals("InsuredPeoples"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(InsuredPeoples));
        }
        if (FCode.equals("AgentCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AgentCode));
        }
        if (FCode.equals("AffixNum"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AffixNum));
        }
        if (FCode.equals("AffixPageNum"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AffixPageNum));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(PrtNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(RiskVersion);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(GrpFlag);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(MEflag);
                break;
            case 6:
                strFieldValue = String.valueOf(PayMoney);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(AppntName);
                break;
            case 8:
                strFieldValue = String.valueOf(InsuredPeoples);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(AgentCode);
                break;
            case 10:
                strFieldValue = String.valueOf(AffixNum);
                break;
            case 11:
                strFieldValue = String.valueOf(AffixPageNum);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("PrtNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PrtNo = FValue.trim();
            }
            else
            {
                PrtNo = null;
            }
        }
        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("RiskVersion"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskVersion = FValue.trim();
            }
            else
            {
                RiskVersion = null;
            }
        }
        if (FCode.equals("GrpFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpFlag = FValue.trim();
            }
            else
            {
                GrpFlag = null;
            }
        }
        if (FCode.equals("MEflag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MEflag = FValue.trim();
            }
            else
            {
                MEflag = null;
            }
        }
        if (FCode.equals("PayMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                PayMoney = d;
            }
        }
        if (FCode.equals("AppntName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppntName = FValue.trim();
            }
            else
            {
                AppntName = null;
            }
        }
        if (FCode.equals("InsuredPeoples"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                InsuredPeoples = i;
            }
        }
        if (FCode.equals("AgentCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
            {
                AgentCode = null;
            }
        }
        if (FCode.equals("AffixNum"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                AffixNum = i;
            }
        }
        if (FCode.equals("AffixPageNum"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                AffixPageNum = i;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LCAcceptSchema other = (LCAcceptSchema) otherObject;
        return
                PrtNo.equals(other.getPrtNo())
                && ManageCom.equals(other.getManageCom())
                && RiskCode.equals(other.getRiskCode())
                && RiskVersion.equals(other.getRiskVersion())
                && GrpFlag.equals(other.getGrpFlag())
                && MEflag.equals(other.getMEflag())
                && PayMoney == other.getPayMoney()
                && AppntName.equals(other.getAppntName())
                && InsuredPeoples == other.getInsuredPeoples()
                && AgentCode.equals(other.getAgentCode())
                && AffixNum == other.getAffixNum()
                && AffixPageNum == other.getAffixPageNum();
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("PrtNo"))
        {
            return 0;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return 1;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return 2;
        }
        if (strFieldName.equals("RiskVersion"))
        {
            return 3;
        }
        if (strFieldName.equals("GrpFlag"))
        {
            return 4;
        }
        if (strFieldName.equals("MEflag"))
        {
            return 5;
        }
        if (strFieldName.equals("PayMoney"))
        {
            return 6;
        }
        if (strFieldName.equals("AppntName"))
        {
            return 7;
        }
        if (strFieldName.equals("InsuredPeoples"))
        {
            return 8;
        }
        if (strFieldName.equals("AgentCode"))
        {
            return 9;
        }
        if (strFieldName.equals("AffixNum"))
        {
            return 10;
        }
        if (strFieldName.equals("AffixPageNum"))
        {
            return 11;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "PrtNo";
                break;
            case 1:
                strFieldName = "ManageCom";
                break;
            case 2:
                strFieldName = "RiskCode";
                break;
            case 3:
                strFieldName = "RiskVersion";
                break;
            case 4:
                strFieldName = "GrpFlag";
                break;
            case 5:
                strFieldName = "MEflag";
                break;
            case 6:
                strFieldName = "PayMoney";
                break;
            case 7:
                strFieldName = "AppntName";
                break;
            case 8:
                strFieldName = "InsuredPeoples";
                break;
            case 9:
                strFieldName = "AgentCode";
                break;
            case 10:
                strFieldName = "AffixNum";
                break;
            case 11:
                strFieldName = "AffixPageNum";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("PrtNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskVersion"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MEflag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PayMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("AppntName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsuredPeoples"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("AgentCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AffixNum"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("AffixPageNum"))
        {
            return Schema.TYPE_INT;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_INT;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_INT;
                break;
            case 11:
                nFieldType = Schema.TYPE_INT;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
