/*
 * <p>ClassName: LCContPlanDutySchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 更新表
 * @CreateDate：2004-11-24
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LCContPlanDutyDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LCContPlanDutySchema implements Schema
{
    // @Field
    /** 集体合同号码 */
    private String GrpContNo;
    /** 集体投保单号码 */
    private String ProposalGrpContNo;
    /** 集体保单险种号码 */
    private String GrpPolNo;
    /** 险种编码 */
    private String RiskCode;
    /** 险种版本 */
    private String RiskVersion;
    /** 保险计划编码 */
    private String ContPlanCode;
    /** 保险计划名称 */
    private String ContPlanName;
    /** 责任编码 */
    private String DutyCode;
    /** 份数 */
    private double Mult;
    /** 标准保费 */
    private double StandPrem;
    /** 基本保额 */
    private double Amnt;
    /** 终交年龄年期标志 */
    private String PayEndYearFlag;
    /** 终交年龄年期 */
    private int PayEndYear;
    /** 领取年龄年期标志 */
    private String GetYearFlag;
    /** 领取年龄年期 */
    private int GetYear;
    /** 保险年龄年期标志 */
    private String InsuYearFlag;
    /** 保险年龄年期 */
    private int InsuYear;
    /** 意外年龄年期标志 */
    private String AcciYearFlag;
    /** 意外年龄年期 */
    private int AcciYear;
    /** 免交标志 */
    private String FreeFlag;
    /** 免交比率 */
    private double FreeRate;
    /** 免交起期 */
    private Date FreeStartDate;
    /** 免交止期 */
    private Date FreeEndDate;
    /** 社保标记 */
    private String SSFlag;
    /** 封顶线 */
    private double PeakLine;
    /** 起付限 */
    private double GetLimit;
    /** 赔付比例 */
    private double GetRate;
    /** 备用属性字段1 */
    private String StandbyFlag1;
    /** 备用属性字段2 */
    private String StandbyFlag2;
    /** 备用属性字段3 */
    private String StandbyFlag3;
    /** 浮动费率 */
    private double FloatRate;
    /** 保费计算规则 */
    private String CalRule;
    /** 特别说明 */
    private String Noti;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 38; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LCContPlanDutySchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[5];
        pk[0] = "GrpContNo";
        pk[1] = "GrpPolNo";
        pk[2] = "RiskCode";
        pk[3] = "ContPlanCode";
        pk[4] = "DutyCode";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getGrpContNo()
    {
        if (GrpContNo != null && !GrpContNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GrpContNo = StrTool.unicodeToGBK(GrpContNo);
        }
        return GrpContNo;
    }

    public void setGrpContNo(String aGrpContNo)
    {
        GrpContNo = aGrpContNo;
    }

    public String getProposalGrpContNo()
    {
        if (ProposalGrpContNo != null && !ProposalGrpContNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ProposalGrpContNo = StrTool.unicodeToGBK(ProposalGrpContNo);
        }
        return ProposalGrpContNo;
    }

    public void setProposalGrpContNo(String aProposalGrpContNo)
    {
        ProposalGrpContNo = aProposalGrpContNo;
    }

    public String getGrpPolNo()
    {
        if (GrpPolNo != null && !GrpPolNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            GrpPolNo = StrTool.unicodeToGBK(GrpPolNo);
        }
        return GrpPolNo;
    }

    public void setGrpPolNo(String aGrpPolNo)
    {
        GrpPolNo = aGrpPolNo;
    }

    public String getRiskCode()
    {
        if (RiskCode != null && !RiskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getRiskVersion()
    {
        if (RiskVersion != null && !RiskVersion.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            RiskVersion = StrTool.unicodeToGBK(RiskVersion);
        }
        return RiskVersion;
    }

    public void setRiskVersion(String aRiskVersion)
    {
        RiskVersion = aRiskVersion;
    }

    public String getContPlanCode()
    {
        if (ContPlanCode != null && !ContPlanCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ContPlanCode = StrTool.unicodeToGBK(ContPlanCode);
        }
        return ContPlanCode;
    }

    public void setContPlanCode(String aContPlanCode)
    {
        ContPlanCode = aContPlanCode;
    }

    public String getContPlanName()
    {
        if (ContPlanName != null && !ContPlanName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ContPlanName = StrTool.unicodeToGBK(ContPlanName);
        }
        return ContPlanName;
    }

    public void setContPlanName(String aContPlanName)
    {
        ContPlanName = aContPlanName;
    }

    public String getDutyCode()
    {
        if (DutyCode != null && !DutyCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            DutyCode = StrTool.unicodeToGBK(DutyCode);
        }
        return DutyCode;
    }

    public void setDutyCode(String aDutyCode)
    {
        DutyCode = aDutyCode;
    }

    public double getMult()
    {
        return Mult;
    }

    public void setMult(double aMult)
    {
        Mult = aMult;
    }

    public void setMult(String aMult)
    {
        if (aMult != null && !aMult.equals(""))
        {
            Double tDouble = new Double(aMult);
            double d = tDouble.doubleValue();
            Mult = d;
        }
    }

    public double getStandPrem()
    {
        return StandPrem;
    }

    public void setStandPrem(double aStandPrem)
    {
        StandPrem = aStandPrem;
    }

    public void setStandPrem(String aStandPrem)
    {
        if (aStandPrem != null && !aStandPrem.equals(""))
        {
            Double tDouble = new Double(aStandPrem);
            double d = tDouble.doubleValue();
            StandPrem = d;
        }
    }

    public double getAmnt()
    {
        return Amnt;
    }

    public void setAmnt(double aAmnt)
    {
        Amnt = aAmnt;
    }

    public void setAmnt(String aAmnt)
    {
        if (aAmnt != null && !aAmnt.equals(""))
        {
            Double tDouble = new Double(aAmnt);
            double d = tDouble.doubleValue();
            Amnt = d;
        }
    }

    public String getPayEndYearFlag()
    {
        if (PayEndYearFlag != null && !PayEndYearFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            PayEndYearFlag = StrTool.unicodeToGBK(PayEndYearFlag);
        }
        return PayEndYearFlag;
    }

    public void setPayEndYearFlag(String aPayEndYearFlag)
    {
        PayEndYearFlag = aPayEndYearFlag;
    }

    public int getPayEndYear()
    {
        return PayEndYear;
    }

    public void setPayEndYear(int aPayEndYear)
    {
        PayEndYear = aPayEndYear;
    }

    public void setPayEndYear(String aPayEndYear)
    {
        if (aPayEndYear != null && !aPayEndYear.equals(""))
        {
            Integer tInteger = new Integer(aPayEndYear);
            int i = tInteger.intValue();
            PayEndYear = i;
        }
    }

    public String getGetYearFlag()
    {
        if (GetYearFlag != null && !GetYearFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GetYearFlag = StrTool.unicodeToGBK(GetYearFlag);
        }
        return GetYearFlag;
    }

    public void setGetYearFlag(String aGetYearFlag)
    {
        GetYearFlag = aGetYearFlag;
    }

    public int getGetYear()
    {
        return GetYear;
    }

    public void setGetYear(int aGetYear)
    {
        GetYear = aGetYear;
    }

    public void setGetYear(String aGetYear)
    {
        if (aGetYear != null && !aGetYear.equals(""))
        {
            Integer tInteger = new Integer(aGetYear);
            int i = tInteger.intValue();
            GetYear = i;
        }
    }

    public String getInsuYearFlag()
    {
        if (InsuYearFlag != null && !InsuYearFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            InsuYearFlag = StrTool.unicodeToGBK(InsuYearFlag);
        }
        return InsuYearFlag;
    }

    public void setInsuYearFlag(String aInsuYearFlag)
    {
        InsuYearFlag = aInsuYearFlag;
    }

    public int getInsuYear()
    {
        return InsuYear;
    }

    public void setInsuYear(int aInsuYear)
    {
        InsuYear = aInsuYear;
    }

    public void setInsuYear(String aInsuYear)
    {
        if (aInsuYear != null && !aInsuYear.equals(""))
        {
            Integer tInteger = new Integer(aInsuYear);
            int i = tInteger.intValue();
            InsuYear = i;
        }
    }

    public String getAcciYearFlag()
    {
        if (AcciYearFlag != null && !AcciYearFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AcciYearFlag = StrTool.unicodeToGBK(AcciYearFlag);
        }
        return AcciYearFlag;
    }

    public void setAcciYearFlag(String aAcciYearFlag)
    {
        AcciYearFlag = aAcciYearFlag;
    }

    public int getAcciYear()
    {
        return AcciYear;
    }

    public void setAcciYear(int aAcciYear)
    {
        AcciYear = aAcciYear;
    }

    public void setAcciYear(String aAcciYear)
    {
        if (aAcciYear != null && !aAcciYear.equals(""))
        {
            Integer tInteger = new Integer(aAcciYear);
            int i = tInteger.intValue();
            AcciYear = i;
        }
    }

    public String getFreeFlag()
    {
        if (FreeFlag != null && !FreeFlag.equals("") && SysConst.CHANGECHARSET == true)
        {
            FreeFlag = StrTool.unicodeToGBK(FreeFlag);
        }
        return FreeFlag;
    }

    public void setFreeFlag(String aFreeFlag)
    {
        FreeFlag = aFreeFlag;
    }

    public double getFreeRate()
    {
        return FreeRate;
    }

    public void setFreeRate(double aFreeRate)
    {
        FreeRate = aFreeRate;
    }

    public void setFreeRate(String aFreeRate)
    {
        if (aFreeRate != null && !aFreeRate.equals(""))
        {
            Double tDouble = new Double(aFreeRate);
            double d = tDouble.doubleValue();
            FreeRate = d;
        }
    }

    public String getFreeStartDate()
    {
        if (FreeStartDate != null)
        {
            return fDate.getString(FreeStartDate);
        }
        else
        {
            return null;
        }
    }

    public void setFreeStartDate(Date aFreeStartDate)
    {
        FreeStartDate = aFreeStartDate;
    }

    public void setFreeStartDate(String aFreeStartDate)
    {
        if (aFreeStartDate != null && !aFreeStartDate.equals(""))
        {
            FreeStartDate = fDate.getDate(aFreeStartDate);
        }
        else
        {
            FreeStartDate = null;
        }
    }

    public String getFreeEndDate()
    {
        if (FreeEndDate != null)
        {
            return fDate.getString(FreeEndDate);
        }
        else
        {
            return null;
        }
    }

    public void setFreeEndDate(Date aFreeEndDate)
    {
        FreeEndDate = aFreeEndDate;
    }

    public void setFreeEndDate(String aFreeEndDate)
    {
        if (aFreeEndDate != null && !aFreeEndDate.equals(""))
        {
            FreeEndDate = fDate.getDate(aFreeEndDate);
        }
        else
        {
            FreeEndDate = null;
        }
    }

    public String getSSFlag()
    {
        if (SSFlag != null && !SSFlag.equals("") && SysConst.CHANGECHARSET == true)
        {
            SSFlag = StrTool.unicodeToGBK(SSFlag);
        }
        return SSFlag;
    }

    public void setSSFlag(String aSSFlag)
    {
        SSFlag = aSSFlag;
    }

    public double getPeakLine()
    {
        return PeakLine;
    }

    public void setPeakLine(double aPeakLine)
    {
        PeakLine = aPeakLine;
    }

    public void setPeakLine(String aPeakLine)
    {
        if (aPeakLine != null && !aPeakLine.equals(""))
        {
            Double tDouble = new Double(aPeakLine);
            double d = tDouble.doubleValue();
            PeakLine = d;
        }
    }

    public double getGetLimit()
    {
        return GetLimit;
    }

    public void setGetLimit(double aGetLimit)
    {
        GetLimit = aGetLimit;
    }

    public void setGetLimit(String aGetLimit)
    {
        if (aGetLimit != null && !aGetLimit.equals(""))
        {
            Double tDouble = new Double(aGetLimit);
            double d = tDouble.doubleValue();
            GetLimit = d;
        }
    }

    public double getGetRate()
    {
        return GetRate;
    }

    public void setGetRate(double aGetRate)
    {
        GetRate = aGetRate;
    }

    public void setGetRate(String aGetRate)
    {
        if (aGetRate != null && !aGetRate.equals(""))
        {
            Double tDouble = new Double(aGetRate);
            double d = tDouble.doubleValue();
            GetRate = d;
        }
    }

    public String getStandbyFlag1()
    {
        if (StandbyFlag1 != null && !StandbyFlag1.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            StandbyFlag1 = StrTool.unicodeToGBK(StandbyFlag1);
        }
        return StandbyFlag1;
    }

    public void setStandbyFlag1(String aStandbyFlag1)
    {
        StandbyFlag1 = aStandbyFlag1;
    }

    public String getStandbyFlag2()
    {
        if (StandbyFlag2 != null && !StandbyFlag2.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            StandbyFlag2 = StrTool.unicodeToGBK(StandbyFlag2);
        }
        return StandbyFlag2;
    }

    public void setStandbyFlag2(String aStandbyFlag2)
    {
        StandbyFlag2 = aStandbyFlag2;
    }

    public String getStandbyFlag3()
    {
        if (StandbyFlag3 != null && !StandbyFlag3.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            StandbyFlag3 = StrTool.unicodeToGBK(StandbyFlag3);
        }
        return StandbyFlag3;
    }

    public void setStandbyFlag3(String aStandbyFlag3)
    {
        StandbyFlag3 = aStandbyFlag3;
    }

    public double getFloatRate()
    {
        return FloatRate;
    }

    public void setFloatRate(double aFloatRate)
    {
        FloatRate = aFloatRate;
    }

    public void setFloatRate(String aFloatRate)
    {
        if (aFloatRate != null && !aFloatRate.equals(""))
        {
            Double tDouble = new Double(aFloatRate);
            double d = tDouble.doubleValue();
            FloatRate = d;
        }
    }

    public String getCalRule()
    {
        if (CalRule != null && !CalRule.equals("") && SysConst.CHANGECHARSET == true)
        {
            CalRule = StrTool.unicodeToGBK(CalRule);
        }
        return CalRule;
    }

    public void setCalRule(String aCalRule)
    {
        CalRule = aCalRule;
    }

    public String getNoti()
    {
        if (Noti != null && !Noti.equals("") && SysConst.CHANGECHARSET == true)
        {
            Noti = StrTool.unicodeToGBK(Noti);
        }
        return Noti;
    }

    public void setNoti(String aNoti)
    {
        Noti = aNoti;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LCContPlanDutySchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LCContPlanDutySchema aLCContPlanDutySchema)
    {
        this.GrpContNo = aLCContPlanDutySchema.getGrpContNo();
        this.ProposalGrpContNo = aLCContPlanDutySchema.getProposalGrpContNo();
        this.GrpPolNo = aLCContPlanDutySchema.getGrpPolNo();
        this.RiskCode = aLCContPlanDutySchema.getRiskCode();
        this.RiskVersion = aLCContPlanDutySchema.getRiskVersion();
        this.ContPlanCode = aLCContPlanDutySchema.getContPlanCode();
        this.ContPlanName = aLCContPlanDutySchema.getContPlanName();
        this.DutyCode = aLCContPlanDutySchema.getDutyCode();
        this.Mult = aLCContPlanDutySchema.getMult();
        this.StandPrem = aLCContPlanDutySchema.getStandPrem();
        this.Amnt = aLCContPlanDutySchema.getAmnt();
        this.PayEndYearFlag = aLCContPlanDutySchema.getPayEndYearFlag();
        this.PayEndYear = aLCContPlanDutySchema.getPayEndYear();
        this.GetYearFlag = aLCContPlanDutySchema.getGetYearFlag();
        this.GetYear = aLCContPlanDutySchema.getGetYear();
        this.InsuYearFlag = aLCContPlanDutySchema.getInsuYearFlag();
        this.InsuYear = aLCContPlanDutySchema.getInsuYear();
        this.AcciYearFlag = aLCContPlanDutySchema.getAcciYearFlag();
        this.AcciYear = aLCContPlanDutySchema.getAcciYear();
        this.FreeFlag = aLCContPlanDutySchema.getFreeFlag();
        this.FreeRate = aLCContPlanDutySchema.getFreeRate();
        this.FreeStartDate = fDate.getDate(aLCContPlanDutySchema.
                                           getFreeStartDate());
        this.FreeEndDate = fDate.getDate(aLCContPlanDutySchema.getFreeEndDate());
        this.SSFlag = aLCContPlanDutySchema.getSSFlag();
        this.PeakLine = aLCContPlanDutySchema.getPeakLine();
        this.GetLimit = aLCContPlanDutySchema.getGetLimit();
        this.GetRate = aLCContPlanDutySchema.getGetRate();
        this.StandbyFlag1 = aLCContPlanDutySchema.getStandbyFlag1();
        this.StandbyFlag2 = aLCContPlanDutySchema.getStandbyFlag2();
        this.StandbyFlag3 = aLCContPlanDutySchema.getStandbyFlag3();
        this.FloatRate = aLCContPlanDutySchema.getFloatRate();
        this.CalRule = aLCContPlanDutySchema.getCalRule();
        this.Noti = aLCContPlanDutySchema.getNoti();
        this.Operator = aLCContPlanDutySchema.getOperator();
        this.MakeDate = fDate.getDate(aLCContPlanDutySchema.getMakeDate());
        this.MakeTime = aLCContPlanDutySchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLCContPlanDutySchema.getModifyDate());
        this.ModifyTime = aLCContPlanDutySchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("GrpContNo") == null)
            {
                this.GrpContNo = null;
            }
            else
            {
                this.GrpContNo = rs.getString("GrpContNo").trim();
            }

            if (rs.getString("ProposalGrpContNo") == null)
            {
                this.ProposalGrpContNo = null;
            }
            else
            {
                this.ProposalGrpContNo = rs.getString("ProposalGrpContNo").trim();
            }

            if (rs.getString("GrpPolNo") == null)
            {
                this.GrpPolNo = null;
            }
            else
            {
                this.GrpPolNo = rs.getString("GrpPolNo").trim();
            }

            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("RiskVersion") == null)
            {
                this.RiskVersion = null;
            }
            else
            {
                this.RiskVersion = rs.getString("RiskVersion").trim();
            }

            if (rs.getString("ContPlanCode") == null)
            {
                this.ContPlanCode = null;
            }
            else
            {
                this.ContPlanCode = rs.getString("ContPlanCode").trim();
            }

            if (rs.getString("ContPlanName") == null)
            {
                this.ContPlanName = null;
            }
            else
            {
                this.ContPlanName = rs.getString("ContPlanName").trim();
            }

            if (rs.getString("DutyCode") == null)
            {
                this.DutyCode = null;
            }
            else
            {
                this.DutyCode = rs.getString("DutyCode").trim();
            }

            this.Mult = rs.getDouble("Mult");
            this.StandPrem = rs.getDouble("StandPrem");
            this.Amnt = rs.getDouble("Amnt");
            if (rs.getString("PayEndYearFlag") == null)
            {
                this.PayEndYearFlag = null;
            }
            else
            {
                this.PayEndYearFlag = rs.getString("PayEndYearFlag").trim();
            }

            this.PayEndYear = rs.getInt("PayEndYear");
            if (rs.getString("GetYearFlag") == null)
            {
                this.GetYearFlag = null;
            }
            else
            {
                this.GetYearFlag = rs.getString("GetYearFlag").trim();
            }

            this.GetYear = rs.getInt("GetYear");
            if (rs.getString("InsuYearFlag") == null)
            {
                this.InsuYearFlag = null;
            }
            else
            {
                this.InsuYearFlag = rs.getString("InsuYearFlag").trim();
            }

            this.InsuYear = rs.getInt("InsuYear");
            if (rs.getString("AcciYearFlag") == null)
            {
                this.AcciYearFlag = null;
            }
            else
            {
                this.AcciYearFlag = rs.getString("AcciYearFlag").trim();
            }

            this.AcciYear = rs.getInt("AcciYear");
            if (rs.getString("FreeFlag") == null)
            {
                this.FreeFlag = null;
            }
            else
            {
                this.FreeFlag = rs.getString("FreeFlag").trim();
            }

            this.FreeRate = rs.getDouble("FreeRate");
            this.FreeStartDate = rs.getDate("FreeStartDate");
            this.FreeEndDate = rs.getDate("FreeEndDate");
            if (rs.getString("SSFlag") == null)
            {
                this.SSFlag = null;
            }
            else
            {
                this.SSFlag = rs.getString("SSFlag").trim();
            }

            this.PeakLine = rs.getDouble("PeakLine");
            this.GetLimit = rs.getDouble("GetLimit");
            this.GetRate = rs.getDouble("GetRate");
            if (rs.getString("StandbyFlag1") == null)
            {
                this.StandbyFlag1 = null;
            }
            else
            {
                this.StandbyFlag1 = rs.getString("StandbyFlag1").trim();
            }

            if (rs.getString("StandbyFlag2") == null)
            {
                this.StandbyFlag2 = null;
            }
            else
            {
                this.StandbyFlag2 = rs.getString("StandbyFlag2").trim();
            }

            if (rs.getString("StandbyFlag3") == null)
            {
                this.StandbyFlag3 = null;
            }
            else
            {
                this.StandbyFlag3 = rs.getString("StandbyFlag3").trim();
            }

            this.FloatRate = rs.getDouble("FloatRate");
            if (rs.getString("CalRule") == null)
            {
                this.CalRule = null;
            }
            else
            {
                this.CalRule = rs.getString("CalRule").trim();
            }

            if (rs.getString("Noti") == null)
            {
                this.Noti = null;
            }
            else
            {
                this.Noti = rs.getString("Noti").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCContPlanDutySchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LCContPlanDutySchema getSchema()
    {
        LCContPlanDutySchema aLCContPlanDutySchema = new LCContPlanDutySchema();
        aLCContPlanDutySchema.setSchema(this);
        return aLCContPlanDutySchema;
    }

    public LCContPlanDutyDB getDB()
    {
        LCContPlanDutyDB aDBOper = new LCContPlanDutyDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCContPlanDuty描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(GrpContNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ProposalGrpContNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GrpPolNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskVersion)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ContPlanCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ContPlanName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(DutyCode)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(Mult) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(StandPrem) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(Amnt) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PayEndYearFlag)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(PayEndYear) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GetYearFlag)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(GetYear) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InsuYearFlag)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(InsuYear) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AcciYearFlag)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(AcciYear) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(FreeFlag)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(FreeRate) + SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            FreeStartDate))) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            FreeEndDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SSFlag)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(PeakLine) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(GetLimit) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(GetRate) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(StandbyFlag1)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(StandbyFlag2)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(StandbyFlag3)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(FloatRate) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CalRule)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Noti)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCContPlanDuty>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            ProposalGrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                               2, SysConst.PACKAGESPILTER);
            GrpPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                      SysConst.PACKAGESPILTER);
            RiskVersion = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                         SysConst.PACKAGESPILTER);
            ContPlanCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                          SysConst.PACKAGESPILTER);
            ContPlanName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                          SysConst.PACKAGESPILTER);
            DutyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                      SysConst.PACKAGESPILTER);
            Mult = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    9, SysConst.PACKAGESPILTER))).doubleValue();
            StandPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 10, SysConst.PACKAGESPILTER))).doubleValue();
            Amnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    11, SysConst.PACKAGESPILTER))).doubleValue();
            PayEndYearFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                            12, SysConst.PACKAGESPILTER);
            PayEndYear = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 13, SysConst.PACKAGESPILTER))).intValue();
            GetYearFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                         SysConst.PACKAGESPILTER);
            GetYear = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 15, SysConst.PACKAGESPILTER))).intValue();
            InsuYearFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                          SysConst.PACKAGESPILTER);
            InsuYear = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 17, SysConst.PACKAGESPILTER))).intValue();
            AcciYearFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                          SysConst.PACKAGESPILTER);
            AcciYear = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 19, SysConst.PACKAGESPILTER))).intValue();
            FreeFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                      SysConst.PACKAGESPILTER);
            FreeRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 21, SysConst.PACKAGESPILTER))).doubleValue();
            FreeStartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 22, SysConst.PACKAGESPILTER));
            FreeEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 23, SysConst.PACKAGESPILTER));
            SSFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,
                                    SysConst.PACKAGESPILTER);
            PeakLine = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 25, SysConst.PACKAGESPILTER))).doubleValue();
            GetLimit = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 26, SysConst.PACKAGESPILTER))).doubleValue();
            GetRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 27, SysConst.PACKAGESPILTER))).doubleValue();
            StandbyFlag1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28,
                                          SysConst.PACKAGESPILTER);
            StandbyFlag2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29,
                                          SysConst.PACKAGESPILTER);
            StandbyFlag3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30,
                                          SysConst.PACKAGESPILTER);
            FloatRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 31, SysConst.PACKAGESPILTER))).doubleValue();
            CalRule = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32,
                                     SysConst.PACKAGESPILTER);
            Noti = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33,
                                  SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 35, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 37, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCContPlanDutySchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("GrpContNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GrpContNo));
        }
        if (FCode.equals("ProposalGrpContNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(
                    ProposalGrpContNo));
        }
        if (FCode.equals("GrpPolNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GrpPolNo));
        }
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskCode));
        }
        if (FCode.equals("RiskVersion"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskVersion));
        }
        if (FCode.equals("ContPlanCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ContPlanCode));
        }
        if (FCode.equals("ContPlanName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ContPlanName));
        }
        if (FCode.equals("DutyCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DutyCode));
        }
        if (FCode.equals("Mult"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Mult));
        }
        if (FCode.equals("StandPrem"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(StandPrem));
        }
        if (FCode.equals("Amnt"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Amnt));
        }
        if (FCode.equals("PayEndYearFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PayEndYearFlag));
        }
        if (FCode.equals("PayEndYear"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PayEndYear));
        }
        if (FCode.equals("GetYearFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetYearFlag));
        }
        if (FCode.equals("GetYear"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetYear));
        }
        if (FCode.equals("InsuYearFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(InsuYearFlag));
        }
        if (FCode.equals("InsuYear"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(InsuYear));
        }
        if (FCode.equals("AcciYearFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AcciYearFlag));
        }
        if (FCode.equals("AcciYear"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AcciYear));
        }
        if (FCode.equals("FreeFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(FreeFlag));
        }
        if (FCode.equals("FreeRate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(FreeRate));
        }
        if (FCode.equals("FreeStartDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getFreeStartDate()));
        }
        if (FCode.equals("FreeEndDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getFreeEndDate()));
        }
        if (FCode.equals("SSFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SSFlag));
        }
        if (FCode.equals("PeakLine"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PeakLine));
        }
        if (FCode.equals("GetLimit"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetLimit));
        }
        if (FCode.equals("GetRate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetRate));
        }
        if (FCode.equals("StandbyFlag1"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(StandbyFlag1));
        }
        if (FCode.equals("StandbyFlag2"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(StandbyFlag2));
        }
        if (FCode.equals("StandbyFlag3"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(StandbyFlag3));
        }
        if (FCode.equals("FloatRate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(FloatRate));
        }
        if (FCode.equals("CalRule"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CalRule));
        }
        if (FCode.equals("Noti"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Noti));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(GrpContNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ProposalGrpContNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(GrpPolNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(RiskVersion);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(ContPlanCode);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(ContPlanName);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(DutyCode);
                break;
            case 8:
                strFieldValue = String.valueOf(Mult);
                break;
            case 9:
                strFieldValue = String.valueOf(StandPrem);
                break;
            case 10:
                strFieldValue = String.valueOf(Amnt);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(PayEndYearFlag);
                break;
            case 12:
                strFieldValue = String.valueOf(PayEndYear);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(GetYearFlag);
                break;
            case 14:
                strFieldValue = String.valueOf(GetYear);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(InsuYearFlag);
                break;
            case 16:
                strFieldValue = String.valueOf(InsuYear);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(AcciYearFlag);
                break;
            case 18:
                strFieldValue = String.valueOf(AcciYear);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(FreeFlag);
                break;
            case 20:
                strFieldValue = String.valueOf(FreeRate);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getFreeStartDate()));
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getFreeEndDate()));
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(SSFlag);
                break;
            case 24:
                strFieldValue = String.valueOf(PeakLine);
                break;
            case 25:
                strFieldValue = String.valueOf(GetLimit);
                break;
            case 26:
                strFieldValue = String.valueOf(GetRate);
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(StandbyFlag1);
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(StandbyFlag2);
                break;
            case 29:
                strFieldValue = StrTool.GBKToUnicode(StandbyFlag3);
                break;
            case 30:
                strFieldValue = String.valueOf(FloatRate);
                break;
            case 31:
                strFieldValue = StrTool.GBKToUnicode(CalRule);
                break;
            case 32:
                strFieldValue = StrTool.GBKToUnicode(Noti);
                break;
            case 33:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 34:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 35:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 36:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 37:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("GrpContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
            {
                GrpContNo = null;
            }
        }
        if (FCode.equals("ProposalGrpContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ProposalGrpContNo = FValue.trim();
            }
            else
            {
                ProposalGrpContNo = null;
            }
        }
        if (FCode.equals("GrpPolNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpPolNo = FValue.trim();
            }
            else
            {
                GrpPolNo = null;
            }
        }
        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("RiskVersion"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskVersion = FValue.trim();
            }
            else
            {
                RiskVersion = null;
            }
        }
        if (FCode.equals("ContPlanCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ContPlanCode = FValue.trim();
            }
            else
            {
                ContPlanCode = null;
            }
        }
        if (FCode.equals("ContPlanName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ContPlanName = FValue.trim();
            }
            else
            {
                ContPlanName = null;
            }
        }
        if (FCode.equals("DutyCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DutyCode = FValue.trim();
            }
            else
            {
                DutyCode = null;
            }
        }
        if (FCode.equals("Mult"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Mult = d;
            }
        }
        if (FCode.equals("StandPrem"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                StandPrem = d;
            }
        }
        if (FCode.equals("Amnt"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Amnt = d;
            }
        }
        if (FCode.equals("PayEndYearFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PayEndYearFlag = FValue.trim();
            }
            else
            {
                PayEndYearFlag = null;
            }
        }
        if (FCode.equals("PayEndYear"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                PayEndYear = i;
            }
        }
        if (FCode.equals("GetYearFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetYearFlag = FValue.trim();
            }
            else
            {
                GetYearFlag = null;
            }
        }
        if (FCode.equals("GetYear"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                GetYear = i;
            }
        }
        if (FCode.equals("InsuYearFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InsuYearFlag = FValue.trim();
            }
            else
            {
                InsuYearFlag = null;
            }
        }
        if (FCode.equals("InsuYear"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                InsuYear = i;
            }
        }
        if (FCode.equals("AcciYearFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AcciYearFlag = FValue.trim();
            }
            else
            {
                AcciYearFlag = null;
            }
        }
        if (FCode.equals("AcciYear"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                AcciYear = i;
            }
        }
        if (FCode.equals("FreeFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                FreeFlag = FValue.trim();
            }
            else
            {
                FreeFlag = null;
            }
        }
        if (FCode.equals("FreeRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                FreeRate = d;
            }
        }
        if (FCode.equals("FreeStartDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                FreeStartDate = fDate.getDate(FValue);
            }
            else
            {
                FreeStartDate = null;
            }
        }
        if (FCode.equals("FreeEndDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                FreeEndDate = fDate.getDate(FValue);
            }
            else
            {
                FreeEndDate = null;
            }
        }
        if (FCode.equals("SSFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SSFlag = FValue.trim();
            }
            else
            {
                SSFlag = null;
            }
        }
        if (FCode.equals("PeakLine"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                PeakLine = d;
            }
        }
        if (FCode.equals("GetLimit"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                GetLimit = d;
            }
        }
        if (FCode.equals("GetRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                GetRate = d;
            }
        }
        if (FCode.equals("StandbyFlag1"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                StandbyFlag1 = FValue.trim();
            }
            else
            {
                StandbyFlag1 = null;
            }
        }
        if (FCode.equals("StandbyFlag2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                StandbyFlag2 = FValue.trim();
            }
            else
            {
                StandbyFlag2 = null;
            }
        }
        if (FCode.equals("StandbyFlag3"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                StandbyFlag3 = FValue.trim();
            }
            else
            {
                StandbyFlag3 = null;
            }
        }
        if (FCode.equals("FloatRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                FloatRate = d;
            }
        }
        if (FCode.equals("CalRule"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CalRule = FValue.trim();
            }
            else
            {
                CalRule = null;
            }
        }
        if (FCode.equals("Noti"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Noti = FValue.trim();
            }
            else
            {
                Noti = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LCContPlanDutySchema other = (LCContPlanDutySchema) otherObject;
        return
                GrpContNo.equals(other.getGrpContNo())
                && ProposalGrpContNo.equals(other.getProposalGrpContNo())
                && GrpPolNo.equals(other.getGrpPolNo())
                && RiskCode.equals(other.getRiskCode())
                && RiskVersion.equals(other.getRiskVersion())
                && ContPlanCode.equals(other.getContPlanCode())
                && ContPlanName.equals(other.getContPlanName())
                && DutyCode.equals(other.getDutyCode())
                && Mult == other.getMult()
                && StandPrem == other.getStandPrem()
                && Amnt == other.getAmnt()
                && PayEndYearFlag.equals(other.getPayEndYearFlag())
                && PayEndYear == other.getPayEndYear()
                && GetYearFlag.equals(other.getGetYearFlag())
                && GetYear == other.getGetYear()
                && InsuYearFlag.equals(other.getInsuYearFlag())
                && InsuYear == other.getInsuYear()
                && AcciYearFlag.equals(other.getAcciYearFlag())
                && AcciYear == other.getAcciYear()
                && FreeFlag.equals(other.getFreeFlag())
                && FreeRate == other.getFreeRate()
                && fDate.getString(FreeStartDate).equals(other.getFreeStartDate())
                && fDate.getString(FreeEndDate).equals(other.getFreeEndDate())
                && SSFlag.equals(other.getSSFlag())
                && PeakLine == other.getPeakLine()
                && GetLimit == other.getGetLimit()
                && GetRate == other.getGetRate()
                && StandbyFlag1.equals(other.getStandbyFlag1())
                && StandbyFlag2.equals(other.getStandbyFlag2())
                && StandbyFlag3.equals(other.getStandbyFlag3())
                && FloatRate == other.getFloatRate()
                && CalRule.equals(other.getCalRule())
                && Noti.equals(other.getNoti())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("GrpContNo"))
        {
            return 0;
        }
        if (strFieldName.equals("ProposalGrpContNo"))
        {
            return 1;
        }
        if (strFieldName.equals("GrpPolNo"))
        {
            return 2;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return 3;
        }
        if (strFieldName.equals("RiskVersion"))
        {
            return 4;
        }
        if (strFieldName.equals("ContPlanCode"))
        {
            return 5;
        }
        if (strFieldName.equals("ContPlanName"))
        {
            return 6;
        }
        if (strFieldName.equals("DutyCode"))
        {
            return 7;
        }
        if (strFieldName.equals("Mult"))
        {
            return 8;
        }
        if (strFieldName.equals("StandPrem"))
        {
            return 9;
        }
        if (strFieldName.equals("Amnt"))
        {
            return 10;
        }
        if (strFieldName.equals("PayEndYearFlag"))
        {
            return 11;
        }
        if (strFieldName.equals("PayEndYear"))
        {
            return 12;
        }
        if (strFieldName.equals("GetYearFlag"))
        {
            return 13;
        }
        if (strFieldName.equals("GetYear"))
        {
            return 14;
        }
        if (strFieldName.equals("InsuYearFlag"))
        {
            return 15;
        }
        if (strFieldName.equals("InsuYear"))
        {
            return 16;
        }
        if (strFieldName.equals("AcciYearFlag"))
        {
            return 17;
        }
        if (strFieldName.equals("AcciYear"))
        {
            return 18;
        }
        if (strFieldName.equals("FreeFlag"))
        {
            return 19;
        }
        if (strFieldName.equals("FreeRate"))
        {
            return 20;
        }
        if (strFieldName.equals("FreeStartDate"))
        {
            return 21;
        }
        if (strFieldName.equals("FreeEndDate"))
        {
            return 22;
        }
        if (strFieldName.equals("SSFlag"))
        {
            return 23;
        }
        if (strFieldName.equals("PeakLine"))
        {
            return 24;
        }
        if (strFieldName.equals("GetLimit"))
        {
            return 25;
        }
        if (strFieldName.equals("GetRate"))
        {
            return 26;
        }
        if (strFieldName.equals("StandbyFlag1"))
        {
            return 27;
        }
        if (strFieldName.equals("StandbyFlag2"))
        {
            return 28;
        }
        if (strFieldName.equals("StandbyFlag3"))
        {
            return 29;
        }
        if (strFieldName.equals("FloatRate"))
        {
            return 30;
        }
        if (strFieldName.equals("CalRule"))
        {
            return 31;
        }
        if (strFieldName.equals("Noti"))
        {
            return 32;
        }
        if (strFieldName.equals("Operator"))
        {
            return 33;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 34;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 35;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 36;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 37;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "GrpContNo";
                break;
            case 1:
                strFieldName = "ProposalGrpContNo";
                break;
            case 2:
                strFieldName = "GrpPolNo";
                break;
            case 3:
                strFieldName = "RiskCode";
                break;
            case 4:
                strFieldName = "RiskVersion";
                break;
            case 5:
                strFieldName = "ContPlanCode";
                break;
            case 6:
                strFieldName = "ContPlanName";
                break;
            case 7:
                strFieldName = "DutyCode";
                break;
            case 8:
                strFieldName = "Mult";
                break;
            case 9:
                strFieldName = "StandPrem";
                break;
            case 10:
                strFieldName = "Amnt";
                break;
            case 11:
                strFieldName = "PayEndYearFlag";
                break;
            case 12:
                strFieldName = "PayEndYear";
                break;
            case 13:
                strFieldName = "GetYearFlag";
                break;
            case 14:
                strFieldName = "GetYear";
                break;
            case 15:
                strFieldName = "InsuYearFlag";
                break;
            case 16:
                strFieldName = "InsuYear";
                break;
            case 17:
                strFieldName = "AcciYearFlag";
                break;
            case 18:
                strFieldName = "AcciYear";
                break;
            case 19:
                strFieldName = "FreeFlag";
                break;
            case 20:
                strFieldName = "FreeRate";
                break;
            case 21:
                strFieldName = "FreeStartDate";
                break;
            case 22:
                strFieldName = "FreeEndDate";
                break;
            case 23:
                strFieldName = "SSFlag";
                break;
            case 24:
                strFieldName = "PeakLine";
                break;
            case 25:
                strFieldName = "GetLimit";
                break;
            case 26:
                strFieldName = "GetRate";
                break;
            case 27:
                strFieldName = "StandbyFlag1";
                break;
            case 28:
                strFieldName = "StandbyFlag2";
                break;
            case 29:
                strFieldName = "StandbyFlag3";
                break;
            case 30:
                strFieldName = "FloatRate";
                break;
            case 31:
                strFieldName = "CalRule";
                break;
            case 32:
                strFieldName = "Noti";
                break;
            case 33:
                strFieldName = "Operator";
                break;
            case 34:
                strFieldName = "MakeDate";
                break;
            case 35:
                strFieldName = "MakeTime";
                break;
            case 36:
                strFieldName = "ModifyDate";
                break;
            case 37:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("GrpContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ProposalGrpContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpPolNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskVersion"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContPlanCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContPlanName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DutyCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Mult"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("StandPrem"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Amnt"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("PayEndYearFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PayEndYear"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("GetYearFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetYear"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("InsuYearFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsuYear"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("AcciYearFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AcciYear"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("FreeFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FreeRate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("FreeStartDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("FreeEndDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("SSFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PeakLine"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("GetLimit"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("GetRate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("StandbyFlag1"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StandbyFlag2"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StandbyFlag3"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FloatRate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("CalRule"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Noti"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 9:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 10:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_INT;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_INT;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_INT;
                break;
            case 17:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 18:
                nFieldType = Schema.TYPE_INT;
                break;
            case 19:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 20:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 21:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 22:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 23:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 24:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 25:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 26:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 27:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 28:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 29:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 30:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 31:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 32:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 33:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 34:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 35:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 36:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 37:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
