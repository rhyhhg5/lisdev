/*
 * <p>ClassName: LDTaskParamSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 后台任务服务
 * @CreateDate：2004-12-15
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LDTaskParamDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LDTaskParamSchema implements Schema
{
    // @Field
    /** 任务代码 */
    private String TaskCode;
    /** 任务计划代码 */
    private String TaskPlanCode;
    /** 参数名 */
    private String ParamName;
    /** 参数值 */
    private String ParamValue;
    /** 操作员 */
    private String Operator;
    /** 录入日期 */
    private Date MakeDate;
    /** 录入时间 */
    private String MakeTime;
    /** 修改日期 */
    private Date ModifyDate;
    /** 修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 9; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LDTaskParamSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "TaskCode";
        pk[1] = "TaskPlanCode";
        pk[2] = "ParamName";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getTaskCode()
    {
        if (TaskCode != null && !TaskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            TaskCode = StrTool.unicodeToGBK(TaskCode);
        }
        return TaskCode;
    }

    public void setTaskCode(String aTaskCode)
    {
        TaskCode = aTaskCode;
    }

    public String getTaskPlanCode()
    {
        if (TaskPlanCode != null && !TaskPlanCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            TaskPlanCode = StrTool.unicodeToGBK(TaskPlanCode);
        }
        return TaskPlanCode;
    }

    public void setTaskPlanCode(String aTaskPlanCode)
    {
        TaskPlanCode = aTaskPlanCode;
    }

    public String getParamName()
    {
        if (ParamName != null && !ParamName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ParamName = StrTool.unicodeToGBK(ParamName);
        }
        return ParamName;
    }

    public void setParamName(String aParamName)
    {
        ParamName = aParamName;
    }

    public String getParamValue()
    {
        if (ParamValue != null && !ParamValue.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ParamValue = StrTool.unicodeToGBK(ParamValue);
        }
        return ParamValue;
    }

    public void setParamValue(String aParamValue)
    {
        ParamValue = aParamValue;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LDTaskParamSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LDTaskParamSchema aLDTaskParamSchema)
    {
        this.TaskCode = aLDTaskParamSchema.getTaskCode();
        this.TaskPlanCode = aLDTaskParamSchema.getTaskPlanCode();
        this.ParamName = aLDTaskParamSchema.getParamName();
        this.ParamValue = aLDTaskParamSchema.getParamValue();
        this.Operator = aLDTaskParamSchema.getOperator();
        this.MakeDate = fDate.getDate(aLDTaskParamSchema.getMakeDate());
        this.MakeTime = aLDTaskParamSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLDTaskParamSchema.getModifyDate());
        this.ModifyTime = aLDTaskParamSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("TaskCode") == null)
            {
                this.TaskCode = null;
            }
            else
            {
                this.TaskCode = rs.getString("TaskCode").trim();
            }

            if (rs.getString("TaskPlanCode") == null)
            {
                this.TaskPlanCode = null;
            }
            else
            {
                this.TaskPlanCode = rs.getString("TaskPlanCode").trim();
            }

            if (rs.getString("ParamName") == null)
            {
                this.ParamName = null;
            }
            else
            {
                this.ParamName = rs.getString("ParamName").trim();
            }

            if (rs.getString("ParamValue") == null)
            {
                this.ParamValue = null;
            }
            else
            {
                this.ParamValue = rs.getString("ParamValue").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDTaskParamSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LDTaskParamSchema getSchema()
    {
        LDTaskParamSchema aLDTaskParamSchema = new LDTaskParamSchema();
        aLDTaskParamSchema.setSchema(this);
        return aLDTaskParamSchema;
    }

    public LDTaskParamDB getDB()
    {
        LDTaskParamDB aDBOper = new LDTaskParamDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDTaskParam描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(TaskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(TaskPlanCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ParamName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ParamValue)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDTaskParam>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            TaskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            TaskPlanCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                          SysConst.PACKAGESPILTER);
            ParamName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                       SysConst.PACKAGESPILTER);
            ParamValue = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                        SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 6, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 8, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDTaskParamSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("TaskCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(TaskCode));
        }
        if (FCode.equals("TaskPlanCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(TaskPlanCode));
        }
        if (FCode.equals("ParamName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ParamName));
        }
        if (FCode.equals("ParamValue"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ParamValue));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(TaskCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(TaskPlanCode);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ParamName);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(ParamValue);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("TaskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                TaskCode = FValue.trim();
            }
            else
            {
                TaskCode = null;
            }
        }
        if (FCode.equals("TaskPlanCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                TaskPlanCode = FValue.trim();
            }
            else
            {
                TaskPlanCode = null;
            }
        }
        if (FCode.equals("ParamName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ParamName = FValue.trim();
            }
            else
            {
                ParamName = null;
            }
        }
        if (FCode.equals("ParamValue"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ParamValue = FValue.trim();
            }
            else
            {
                ParamValue = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LDTaskParamSchema other = (LDTaskParamSchema) otherObject;
        return
                TaskCode.equals(other.getTaskCode())
                && TaskPlanCode.equals(other.getTaskPlanCode())
                && ParamName.equals(other.getParamName())
                && ParamValue.equals(other.getParamValue())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("TaskCode"))
        {
            return 0;
        }
        if (strFieldName.equals("TaskPlanCode"))
        {
            return 1;
        }
        if (strFieldName.equals("ParamName"))
        {
            return 2;
        }
        if (strFieldName.equals("ParamValue"))
        {
            return 3;
        }
        if (strFieldName.equals("Operator"))
        {
            return 4;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 5;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 6;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 7;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 8;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "TaskCode";
                break;
            case 1:
                strFieldName = "TaskPlanCode";
                break;
            case 2:
                strFieldName = "ParamName";
                break;
            case 3:
                strFieldName = "ParamValue";
                break;
            case 4:
                strFieldName = "Operator";
                break;
            case 5:
                strFieldName = "MakeDate";
                break;
            case 6:
                strFieldName = "MakeTime";
                break;
            case 7:
                strFieldName = "ModifyDate";
                break;
            case 8:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("TaskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("TaskPlanCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ParamName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ParamValue"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
