/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LRReComInfoDB;

/*
 * <p>ClassName: LRReComInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 再保信息表
 * @CreateDate：2018-04-27
 */
public class LRReComInfoSchema implements Schema, Cloneable
{
	// @Field
	/** 公司代码 */
	private String ReComCode;
	/** 公司名称 */
	private String ReComName;
	/** 公司简称 */
	private String SimpName;
	/** 英文名称 */
	private String EngName;
	/** 邮政编码 */
	private String PostalCode;
	/** 通讯地址 */
	private String Address;
	/** 传真 */
	private String FaxNo;
	/** 网址 */
	private String WebAddress;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 修改日期 */
	private Date ModifyDate;
	/** 修改时间 */
	private String ModifyTime;
	/** 操作人 */
	private String Operator;
	/** 管理机构 */
	private String ManageCom;
	/** 备注 */
	private String Note;
	/** Sap客户号 */
	private String SAPAppntNo;
	/** 保险机构代码 */
	private String CompanyCode;

	public static final int FIELDNUM = 17;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LRReComInfoSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "ReComCode";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LRReComInfoSchema cloned = (LRReComInfoSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getReComCode()
	{
		return ReComCode;
	}
	public void setReComCode(String aReComCode)
	{
		ReComCode = aReComCode;
	}
	public String getReComName()
	{
		return ReComName;
	}
	public void setReComName(String aReComName)
	{
		ReComName = aReComName;
	}
	public String getSimpName()
	{
		return SimpName;
	}
	public void setSimpName(String aSimpName)
	{
		SimpName = aSimpName;
	}
	public String getEngName()
	{
		return EngName;
	}
	public void setEngName(String aEngName)
	{
		EngName = aEngName;
	}
	public String getPostalCode()
	{
		return PostalCode;
	}
	public void setPostalCode(String aPostalCode)
	{
		PostalCode = aPostalCode;
	}
	public String getAddress()
	{
		return Address;
	}
	public void setAddress(String aAddress)
	{
		Address = aAddress;
	}
	public String getFaxNo()
	{
		return FaxNo;
	}
	public void setFaxNo(String aFaxNo)
	{
		FaxNo = aFaxNo;
	}
	public String getWebAddress()
	{
		return WebAddress;
	}
	public void setWebAddress(String aWebAddress)
	{
		WebAddress = aWebAddress;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getNote()
	{
		return Note;
	}
	public void setNote(String aNote)
	{
		Note = aNote;
	}
	public String getSAPAppntNo()
	{
		return SAPAppntNo;
	}
	public void setSAPAppntNo(String aSAPAppntNo)
	{
		SAPAppntNo = aSAPAppntNo;
	}
	public String getCompanyCode()
	{
		return CompanyCode;
	}
	public void setCompanyCode(String aCompanyCode)
	{
		CompanyCode = aCompanyCode;
	}

	/**
	* 使用另外一个 LRReComInfoSchema 对象给 Schema 赋值
	* @param: aLRReComInfoSchema LRReComInfoSchema
	**/
	public void setSchema(LRReComInfoSchema aLRReComInfoSchema)
	{
		this.ReComCode = aLRReComInfoSchema.getReComCode();
		this.ReComName = aLRReComInfoSchema.getReComName();
		this.SimpName = aLRReComInfoSchema.getSimpName();
		this.EngName = aLRReComInfoSchema.getEngName();
		this.PostalCode = aLRReComInfoSchema.getPostalCode();
		this.Address = aLRReComInfoSchema.getAddress();
		this.FaxNo = aLRReComInfoSchema.getFaxNo();
		this.WebAddress = aLRReComInfoSchema.getWebAddress();
		this.MakeDate = fDate.getDate( aLRReComInfoSchema.getMakeDate());
		this.MakeTime = aLRReComInfoSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLRReComInfoSchema.getModifyDate());
		this.ModifyTime = aLRReComInfoSchema.getModifyTime();
		this.Operator = aLRReComInfoSchema.getOperator();
		this.ManageCom = aLRReComInfoSchema.getManageCom();
		this.Note = aLRReComInfoSchema.getNote();
		this.SAPAppntNo = aLRReComInfoSchema.getSAPAppntNo();
		this.CompanyCode = aLRReComInfoSchema.getCompanyCode();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("ReComCode") == null )
				this.ReComCode = null;
			else
				this.ReComCode = rs.getString("ReComCode").trim();

			if( rs.getString("ReComName") == null )
				this.ReComName = null;
			else
				this.ReComName = rs.getString("ReComName").trim();

			if( rs.getString("SimpName") == null )
				this.SimpName = null;
			else
				this.SimpName = rs.getString("SimpName").trim();

			if( rs.getString("EngName") == null )
				this.EngName = null;
			else
				this.EngName = rs.getString("EngName").trim();

			if( rs.getString("PostalCode") == null )
				this.PostalCode = null;
			else
				this.PostalCode = rs.getString("PostalCode").trim();

			if( rs.getString("Address") == null )
				this.Address = null;
			else
				this.Address = rs.getString("Address").trim();

			if( rs.getString("FaxNo") == null )
				this.FaxNo = null;
			else
				this.FaxNo = rs.getString("FaxNo").trim();

			if( rs.getString("WebAddress") == null )
				this.WebAddress = null;
			else
				this.WebAddress = rs.getString("WebAddress").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("Note") == null )
				this.Note = null;
			else
				this.Note = rs.getString("Note").trim();

			if( rs.getString("SAPAppntNo") == null )
				this.SAPAppntNo = null;
			else
				this.SAPAppntNo = rs.getString("SAPAppntNo").trim();

			if( rs.getString("CompanyCode") == null )
				this.CompanyCode = null;
			else
				this.CompanyCode = rs.getString("CompanyCode").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LRReComInfo表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LRReComInfoSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LRReComInfoSchema getSchema()
	{
		LRReComInfoSchema aLRReComInfoSchema = new LRReComInfoSchema();
		aLRReComInfoSchema.setSchema(this);
		return aLRReComInfoSchema;
	}

	public LRReComInfoDB getDB()
	{
		LRReComInfoDB aDBOper = new LRReComInfoDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLRReComInfo描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(ReComCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReComName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SimpName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EngName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PostalCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Address)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FaxNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(WebAddress)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Note)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SAPAppntNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CompanyCode));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLRReComInfo>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			ReComCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			ReComName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			SimpName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			EngName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			PostalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			Address = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			FaxNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			WebAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			Note = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			SAPAppntNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			CompanyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LRReComInfoSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("ReComCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReComCode));
		}
		if (FCode.equals("ReComName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReComName));
		}
		if (FCode.equals("SimpName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SimpName));
		}
		if (FCode.equals("EngName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EngName));
		}
		if (FCode.equals("PostalCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PostalCode));
		}
		if (FCode.equals("Address"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Address));
		}
		if (FCode.equals("FaxNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FaxNo));
		}
		if (FCode.equals("WebAddress"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WebAddress));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("Note"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Note));
		}
		if (FCode.equals("SAPAppntNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SAPAppntNo));
		}
		if (FCode.equals("CompanyCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CompanyCode));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(ReComCode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(ReComName);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(SimpName);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(EngName);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(PostalCode);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(Address);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(FaxNo);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(WebAddress);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(Note);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(SAPAppntNo);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(CompanyCode);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("ReComCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReComCode = FValue.trim();
			}
			else
				ReComCode = null;
		}
		if (FCode.equalsIgnoreCase("ReComName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReComName = FValue.trim();
			}
			else
				ReComName = null;
		}
		if (FCode.equalsIgnoreCase("SimpName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SimpName = FValue.trim();
			}
			else
				SimpName = null;
		}
		if (FCode.equalsIgnoreCase("EngName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EngName = FValue.trim();
			}
			else
				EngName = null;
		}
		if (FCode.equalsIgnoreCase("PostalCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PostalCode = FValue.trim();
			}
			else
				PostalCode = null;
		}
		if (FCode.equalsIgnoreCase("Address"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Address = FValue.trim();
			}
			else
				Address = null;
		}
		if (FCode.equalsIgnoreCase("FaxNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FaxNo = FValue.trim();
			}
			else
				FaxNo = null;
		}
		if (FCode.equalsIgnoreCase("WebAddress"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WebAddress = FValue.trim();
			}
			else
				WebAddress = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("Note"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Note = FValue.trim();
			}
			else
				Note = null;
		}
		if (FCode.equalsIgnoreCase("SAPAppntNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SAPAppntNo = FValue.trim();
			}
			else
				SAPAppntNo = null;
		}
		if (FCode.equalsIgnoreCase("CompanyCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CompanyCode = FValue.trim();
			}
			else
				CompanyCode = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LRReComInfoSchema other = (LRReComInfoSchema)otherObject;
		return
			(ReComCode == null ? other.getReComCode() == null : ReComCode.equals(other.getReComCode()))
			&& (ReComName == null ? other.getReComName() == null : ReComName.equals(other.getReComName()))
			&& (SimpName == null ? other.getSimpName() == null : SimpName.equals(other.getSimpName()))
			&& (EngName == null ? other.getEngName() == null : EngName.equals(other.getEngName()))
			&& (PostalCode == null ? other.getPostalCode() == null : PostalCode.equals(other.getPostalCode()))
			&& (Address == null ? other.getAddress() == null : Address.equals(other.getAddress()))
			&& (FaxNo == null ? other.getFaxNo() == null : FaxNo.equals(other.getFaxNo()))
			&& (WebAddress == null ? other.getWebAddress() == null : WebAddress.equals(other.getWebAddress()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (Note == null ? other.getNote() == null : Note.equals(other.getNote()))
			&& (SAPAppntNo == null ? other.getSAPAppntNo() == null : SAPAppntNo.equals(other.getSAPAppntNo()))
			&& (CompanyCode == null ? other.getCompanyCode() == null : CompanyCode.equals(other.getCompanyCode()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("ReComCode") ) {
			return 0;
		}
		if( strFieldName.equals("ReComName") ) {
			return 1;
		}
		if( strFieldName.equals("SimpName") ) {
			return 2;
		}
		if( strFieldName.equals("EngName") ) {
			return 3;
		}
		if( strFieldName.equals("PostalCode") ) {
			return 4;
		}
		if( strFieldName.equals("Address") ) {
			return 5;
		}
		if( strFieldName.equals("FaxNo") ) {
			return 6;
		}
		if( strFieldName.equals("WebAddress") ) {
			return 7;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 8;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 9;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 10;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 11;
		}
		if( strFieldName.equals("Operator") ) {
			return 12;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 13;
		}
		if( strFieldName.equals("Note") ) {
			return 14;
		}
		if( strFieldName.equals("SAPAppntNo") ) {
			return 15;
		}
		if( strFieldName.equals("CompanyCode") ) {
			return 16;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "ReComCode";
				break;
			case 1:
				strFieldName = "ReComName";
				break;
			case 2:
				strFieldName = "SimpName";
				break;
			case 3:
				strFieldName = "EngName";
				break;
			case 4:
				strFieldName = "PostalCode";
				break;
			case 5:
				strFieldName = "Address";
				break;
			case 6:
				strFieldName = "FaxNo";
				break;
			case 7:
				strFieldName = "WebAddress";
				break;
			case 8:
				strFieldName = "MakeDate";
				break;
			case 9:
				strFieldName = "MakeTime";
				break;
			case 10:
				strFieldName = "ModifyDate";
				break;
			case 11:
				strFieldName = "ModifyTime";
				break;
			case 12:
				strFieldName = "Operator";
				break;
			case 13:
				strFieldName = "ManageCom";
				break;
			case 14:
				strFieldName = "Note";
				break;
			case 15:
				strFieldName = "SAPAppntNo";
				break;
			case 16:
				strFieldName = "CompanyCode";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("ReComCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReComName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SimpName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EngName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PostalCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Address") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FaxNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("WebAddress") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Note") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SAPAppntNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CompanyCode") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
