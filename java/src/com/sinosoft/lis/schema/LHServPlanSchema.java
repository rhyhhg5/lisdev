/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LHServPlanDB;

/*
 * <p>ClassName: LHServPlanSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PICCH-郭丽颖-表结构修改提交单-20060704
 * @CreateDate：2006-07-04
 */
public class LHServPlanSchema implements Schema, Cloneable {
    // @Field
    /** 团体服务计划号码 */
    private String GrpServPlanNo;
    /** 个人服务计划号码 */
    private String ServPlanNo;
    /** 保单号 */
    private String ContNo;
    /** 客户号 */
    private String CustomerNo;
    /** 姓名 */
    private String Name;
    /** 服务计划代码 */
    private String ServPlanCode;
    /** 服务计划名称 */
    private String ServPlanName;
    /** 个人服务计划档次 */
    private String ServPlanLevel;
    /** 服务计划类型 */
    private String ServPlayType;
    /** 机构属性标识 */
    private String ComID;
    /** 服务计划起始时间 */
    private Date StartDate;
    /** 服务计划终止时间 */
    private Date EndDate;
    /** 健管保费（元） */
    private double ServPrem;
    /** 管理机构 */
    private String ManageCom;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 契约事件状态 */
    private String CaseState;

    public static final int FIELDNUM = 20; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LHServPlanSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "ServPlanNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LHServPlanSchema cloned = (LHServPlanSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getGrpServPlanNo() {
        return GrpServPlanNo;
    }

    public void setGrpServPlanNo(String aGrpServPlanNo) {
        GrpServPlanNo = aGrpServPlanNo;
    }

    public String getServPlanNo() {
        return ServPlanNo;
    }

    public void setServPlanNo(String aServPlanNo) {
        ServPlanNo = aServPlanNo;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }

    public String getCustomerNo() {
        return CustomerNo;
    }

    public void setCustomerNo(String aCustomerNo) {
        CustomerNo = aCustomerNo;
    }

    public String getName() {
        return Name;
    }

    public void setName(String aName) {
        Name = aName;
    }

    public String getServPlanCode() {
        return ServPlanCode;
    }

    public void setServPlanCode(String aServPlanCode) {
        ServPlanCode = aServPlanCode;
    }

    public String getServPlanName() {
        return ServPlanName;
    }

    public void setServPlanName(String aServPlanName) {
        ServPlanName = aServPlanName;
    }

    public String getServPlanLevel() {
        return ServPlanLevel;
    }

    public void setServPlanLevel(String aServPlanLevel) {
        ServPlanLevel = aServPlanLevel;
    }

    public String getServPlayType() {
        return ServPlayType;
    }

    public void setServPlayType(String aServPlayType) {
        ServPlayType = aServPlayType;
    }

    public String getComID() {
        return ComID;
    }

    public void setComID(String aComID) {
        ComID = aComID;
    }

    public String getStartDate() {
        if (StartDate != null) {
            return fDate.getString(StartDate);
        } else {
            return null;
        }
    }

    public void setStartDate(Date aStartDate) {
        StartDate = aStartDate;
    }

    public void setStartDate(String aStartDate) {
        if (aStartDate != null && !aStartDate.equals("")) {
            StartDate = fDate.getDate(aStartDate);
        } else {
            StartDate = null;
        }
    }

    public String getEndDate() {
        if (EndDate != null) {
            return fDate.getString(EndDate);
        } else {
            return null;
        }
    }

    public void setEndDate(Date aEndDate) {
        EndDate = aEndDate;
    }

    public void setEndDate(String aEndDate) {
        if (aEndDate != null && !aEndDate.equals("")) {
            EndDate = fDate.getDate(aEndDate);
        } else {
            EndDate = null;
        }
    }

    public double getServPrem() {
        return ServPrem;
    }

    public void setServPrem(double aServPrem) {
        ServPrem = Arith.round(aServPrem, 2);
    }

    public void setServPrem(String aServPrem) {
        if (aServPrem != null && !aServPrem.equals("")) {
            Double tDouble = new Double(aServPrem);
            double d = tDouble.doubleValue();
            ServPrem = Arith.round(d, 2);
        }
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    public String getCaseState() {
        return CaseState;
    }

    public void setCaseState(String aCaseState) {
        CaseState = aCaseState;
    }

    /**
     * 使用另外一个 LHServPlanSchema 对象给 Schema 赋值
     * @param: aLHServPlanSchema LHServPlanSchema
     **/
    public void setSchema(LHServPlanSchema aLHServPlanSchema) {
        this.GrpServPlanNo = aLHServPlanSchema.getGrpServPlanNo();
        this.ServPlanNo = aLHServPlanSchema.getServPlanNo();
        this.ContNo = aLHServPlanSchema.getContNo();
        this.CustomerNo = aLHServPlanSchema.getCustomerNo();
        this.Name = aLHServPlanSchema.getName();
        this.ServPlanCode = aLHServPlanSchema.getServPlanCode();
        this.ServPlanName = aLHServPlanSchema.getServPlanName();
        this.ServPlanLevel = aLHServPlanSchema.getServPlanLevel();
        this.ServPlayType = aLHServPlanSchema.getServPlayType();
        this.ComID = aLHServPlanSchema.getComID();
        this.StartDate = fDate.getDate(aLHServPlanSchema.getStartDate());
        this.EndDate = fDate.getDate(aLHServPlanSchema.getEndDate());
        this.ServPrem = aLHServPlanSchema.getServPrem();
        this.ManageCom = aLHServPlanSchema.getManageCom();
        this.Operator = aLHServPlanSchema.getOperator();
        this.MakeDate = fDate.getDate(aLHServPlanSchema.getMakeDate());
        this.MakeTime = aLHServPlanSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLHServPlanSchema.getModifyDate());
        this.ModifyTime = aLHServPlanSchema.getModifyTime();
        this.CaseState = aLHServPlanSchema.getCaseState();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("GrpServPlanNo") == null) {
                this.GrpServPlanNo = null;
            } else {
                this.GrpServPlanNo = rs.getString("GrpServPlanNo").trim();
            }

            if (rs.getString("ServPlanNo") == null) {
                this.ServPlanNo = null;
            } else {
                this.ServPlanNo = rs.getString("ServPlanNo").trim();
            }

            if (rs.getString("ContNo") == null) {
                this.ContNo = null;
            } else {
                this.ContNo = rs.getString("ContNo").trim();
            }

            if (rs.getString("CustomerNo") == null) {
                this.CustomerNo = null;
            } else {
                this.CustomerNo = rs.getString("CustomerNo").trim();
            }

            if (rs.getString("Name") == null) {
                this.Name = null;
            } else {
                this.Name = rs.getString("Name").trim();
            }

            if (rs.getString("ServPlanCode") == null) {
                this.ServPlanCode = null;
            } else {
                this.ServPlanCode = rs.getString("ServPlanCode").trim();
            }

            if (rs.getString("ServPlanName") == null) {
                this.ServPlanName = null;
            } else {
                this.ServPlanName = rs.getString("ServPlanName").trim();
            }

            if (rs.getString("ServPlanLevel") == null) {
                this.ServPlanLevel = null;
            } else {
                this.ServPlanLevel = rs.getString("ServPlanLevel").trim();
            }

            if (rs.getString("ServPlayType") == null) {
                this.ServPlayType = null;
            } else {
                this.ServPlayType = rs.getString("ServPlayType").trim();
            }

            if (rs.getString("ComID") == null) {
                this.ComID = null;
            } else {
                this.ComID = rs.getString("ComID").trim();
            }

            this.StartDate = rs.getDate("StartDate");
            this.EndDate = rs.getDate("EndDate");
            this.ServPrem = rs.getDouble("ServPrem");
            if (rs.getString("ManageCom") == null) {
                this.ManageCom = null;
            } else {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("CaseState") == null) {
                this.CaseState = null;
            } else {
                this.CaseState = rs.getString("CaseState").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LHServPlan表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHServPlanSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LHServPlanSchema getSchema() {
        LHServPlanSchema aLHServPlanSchema = new LHServPlanSchema();
        aLHServPlanSchema.setSchema(this);
        return aLHServPlanSchema;
    }

    public LHServPlanDB getDB() {
        LHServPlanDB aDBOper = new LHServPlanDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHServPlan描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(GrpServPlanNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServPlanNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CustomerNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Name));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServPlanCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServPlanName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServPlanLevel));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServPlayType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ComID));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(StartDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(EndDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(ServPrem));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CaseState));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHServPlan>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            GrpServPlanNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                           SysConst.PACKAGESPILTER);
            ServPlanNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                    SysConst.PACKAGESPILTER);
            CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                        SysConst.PACKAGESPILTER);
            Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                  SysConst.PACKAGESPILTER);
            ServPlanCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                          SysConst.PACKAGESPILTER);
            ServPlanName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                          SysConst.PACKAGESPILTER);
            ServPlanLevel = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                           SysConst.PACKAGESPILTER);
            ServPlayType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                          SysConst.PACKAGESPILTER);
            ComID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                   SysConst.PACKAGESPILTER);
            StartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 11, SysConst.PACKAGESPILTER));
            EndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 12, SysConst.PACKAGESPILTER));
            ServPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 13, SysConst.PACKAGESPILTER))).doubleValue();
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                       SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 16, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 18, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                        SysConst.PACKAGESPILTER);
            CaseState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                       SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHServPlanSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("GrpServPlanNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpServPlanNo));
        }
        if (FCode.equals("ServPlanNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServPlanNo));
        }
        if (FCode.equals("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equals("CustomerNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
        }
        if (FCode.equals("Name")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
        }
        if (FCode.equals("ServPlanCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServPlanCode));
        }
        if (FCode.equals("ServPlanName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServPlanName));
        }
        if (FCode.equals("ServPlanLevel")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServPlanLevel));
        }
        if (FCode.equals("ServPlayType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServPlayType));
        }
        if (FCode.equals("ComID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ComID));
        }
        if (FCode.equals("StartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getStartDate()));
        }
        if (FCode.equals("EndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getEndDate()));
        }
        if (FCode.equals("ServPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServPrem));
        }
        if (FCode.equals("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("CaseState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CaseState));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(GrpServPlanNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(ServPlanNo);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(ContNo);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(CustomerNo);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(Name);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(ServPlanCode);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(ServPlanName);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(ServPlanLevel);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(ServPlayType);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(ComID);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getStartDate()));
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.getEndDate()));
            break;
        case 12:
            strFieldValue = String.valueOf(ServPrem);
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(ManageCom);
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 15:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 16:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 17:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 18:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        case 19:
            strFieldValue = StrTool.GBKToUnicode(CaseState);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("GrpServPlanNo")) {
            if (FValue != null && !FValue.equals("")) {
                GrpServPlanNo = FValue.trim();
            } else {
                GrpServPlanNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServPlanNo")) {
            if (FValue != null && !FValue.equals("")) {
                ServPlanNo = FValue.trim();
            } else {
                ServPlanNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if (FValue != null && !FValue.equals("")) {
                ContNo = FValue.trim();
            } else {
                ContNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            if (FValue != null && !FValue.equals("")) {
                CustomerNo = FValue.trim();
            } else {
                CustomerNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("Name")) {
            if (FValue != null && !FValue.equals("")) {
                Name = FValue.trim();
            } else {
                Name = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServPlanCode")) {
            if (FValue != null && !FValue.equals("")) {
                ServPlanCode = FValue.trim();
            } else {
                ServPlanCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServPlanName")) {
            if (FValue != null && !FValue.equals("")) {
                ServPlanName = FValue.trim();
            } else {
                ServPlanName = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServPlanLevel")) {
            if (FValue != null && !FValue.equals("")) {
                ServPlanLevel = FValue.trim();
            } else {
                ServPlanLevel = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServPlayType")) {
            if (FValue != null && !FValue.equals("")) {
                ServPlayType = FValue.trim();
            } else {
                ServPlayType = null;
            }
        }
        if (FCode.equalsIgnoreCase("ComID")) {
            if (FValue != null && !FValue.equals("")) {
                ComID = FValue.trim();
            } else {
                ComID = null;
            }
        }
        if (FCode.equalsIgnoreCase("StartDate")) {
            if (FValue != null && !FValue.equals("")) {
                StartDate = fDate.getDate(FValue);
            } else {
                StartDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            if (FValue != null && !FValue.equals("")) {
                EndDate = fDate.getDate(FValue);
            } else {
                EndDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServPrem")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                ServPrem = d;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ManageCom = FValue.trim();
            } else {
                ManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("CaseState")) {
            if (FValue != null && !FValue.equals("")) {
                CaseState = FValue.trim();
            } else {
                CaseState = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LHServPlanSchema other = (LHServPlanSchema) otherObject;
        return
                GrpServPlanNo.equals(other.getGrpServPlanNo())
                && ServPlanNo.equals(other.getServPlanNo())
                && ContNo.equals(other.getContNo())
                && CustomerNo.equals(other.getCustomerNo())
                && Name.equals(other.getName())
                && ServPlanCode.equals(other.getServPlanCode())
                && ServPlanName.equals(other.getServPlanName())
                && ServPlanLevel.equals(other.getServPlanLevel())
                && ServPlayType.equals(other.getServPlayType())
                && ComID.equals(other.getComID())
                && fDate.getString(StartDate).equals(other.getStartDate())
                && fDate.getString(EndDate).equals(other.getEndDate())
                && ServPrem == other.getServPrem()
                && ManageCom.equals(other.getManageCom())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && CaseState.equals(other.getCaseState());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("GrpServPlanNo")) {
            return 0;
        }
        if (strFieldName.equals("ServPlanNo")) {
            return 1;
        }
        if (strFieldName.equals("ContNo")) {
            return 2;
        }
        if (strFieldName.equals("CustomerNo")) {
            return 3;
        }
        if (strFieldName.equals("Name")) {
            return 4;
        }
        if (strFieldName.equals("ServPlanCode")) {
            return 5;
        }
        if (strFieldName.equals("ServPlanName")) {
            return 6;
        }
        if (strFieldName.equals("ServPlanLevel")) {
            return 7;
        }
        if (strFieldName.equals("ServPlayType")) {
            return 8;
        }
        if (strFieldName.equals("ComID")) {
            return 9;
        }
        if (strFieldName.equals("StartDate")) {
            return 10;
        }
        if (strFieldName.equals("EndDate")) {
            return 11;
        }
        if (strFieldName.equals("ServPrem")) {
            return 12;
        }
        if (strFieldName.equals("ManageCom")) {
            return 13;
        }
        if (strFieldName.equals("Operator")) {
            return 14;
        }
        if (strFieldName.equals("MakeDate")) {
            return 15;
        }
        if (strFieldName.equals("MakeTime")) {
            return 16;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 17;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 18;
        }
        if (strFieldName.equals("CaseState")) {
            return 19;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "GrpServPlanNo";
            break;
        case 1:
            strFieldName = "ServPlanNo";
            break;
        case 2:
            strFieldName = "ContNo";
            break;
        case 3:
            strFieldName = "CustomerNo";
            break;
        case 4:
            strFieldName = "Name";
            break;
        case 5:
            strFieldName = "ServPlanCode";
            break;
        case 6:
            strFieldName = "ServPlanName";
            break;
        case 7:
            strFieldName = "ServPlanLevel";
            break;
        case 8:
            strFieldName = "ServPlayType";
            break;
        case 9:
            strFieldName = "ComID";
            break;
        case 10:
            strFieldName = "StartDate";
            break;
        case 11:
            strFieldName = "EndDate";
            break;
        case 12:
            strFieldName = "ServPrem";
            break;
        case 13:
            strFieldName = "ManageCom";
            break;
        case 14:
            strFieldName = "Operator";
            break;
        case 15:
            strFieldName = "MakeDate";
            break;
        case 16:
            strFieldName = "MakeTime";
            break;
        case 17:
            strFieldName = "ModifyDate";
            break;
        case 18:
            strFieldName = "ModifyTime";
            break;
        case 19:
            strFieldName = "CaseState";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("GrpServPlanNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServPlanNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CustomerNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Name")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServPlanCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServPlanName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServPlanLevel")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServPlayType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ComID")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StartDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("EndDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ServPrem")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("ManageCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CaseState")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 11:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 12:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 13:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 14:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 15:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 16:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 17:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 18:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 19:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
