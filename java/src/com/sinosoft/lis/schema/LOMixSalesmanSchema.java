/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LOMixSalesmanDB;

/*
 * <p>ClassName: LOMixSalesmanSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LOMixSalesman
 * @CreateDate：2010-08-02
 */
public class LOMixSalesmanSchema implements Schema, Cloneable
{
	// @Field
	/** 销售人员工号 */
	private String Sales_Cod;
	/** 子公司代码 */
	private String Comp_Cod;
	/** 子公司名称 */
	private String Comp_Nam;
	/** 当事方编码 */
	private String Party_Id;
	/** 当事方角色代码 */
	private String Party_Rol_Cod;
	/** 姓名 */
	private String Sales_Nam;
	/** 出生日期 */
	private String Date_Birthd;
	/** 性别代码 */
	private String Sex_Cod;
	/** 管理机构代码 */
	private String Man_Org_Cod;
	/** 管理机构名称 */
	private String Man_Org_Nam;
	/** 证件类型代码 */
	private String Idtyp_Cod;
	/** 证件类型名称 */
	private String Idtyp_Nam;
	/** 证件号码 */
	private String Id_No;
	/** 联系电话 */
	private String Sales_Tel;
	/** 联系手机 */
	private String Sales_Mob;
	/** 电子邮件 */
	private String Sales_Mail;
	/** 在职状态代码 */
	private String Status_Cod;
	/** 在职状态名称 */
	private String Status;
	/** 销售人员类型代码 */
	private String Sales_Typ_Cod;
	/** 销售人员类型名称 */
	private String Sales_Typ;
	/** 数据变更类型 */
	private String Data_Upd_Typ;
	/** 子公司报送时间 */
	private String Date_Send;
	/** 时间戳 */
	private String Date_Update;
	/** 销售渠道代码 */
	private String Salecha_Cod;
	/** 销售渠道名称 */
	private String Salecha_Nam;
	/** 入职时间 */
	private String Entrant_Date;
	/** 准离司时间/离司时间 */
	private String Dimission_Date;
	/** 集团校验标志 */
	private String Check_Status;
	/** 校验时间 */
	private String Check_Date;
	/** 交叉销售权限 */
	private String Coll_Type;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最近一次修改日期 */
	private Date ModifyDate;
	/** 最近一次修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 34;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LOMixSalesmanSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "Sales_Cod";
		pk[1] = "Comp_Cod";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LOMixSalesmanSchema cloned = (LOMixSalesmanSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getSales_Cod()
	{
		return Sales_Cod;
	}
	public void setSales_Cod(String aSales_Cod)
	{
		Sales_Cod = aSales_Cod;
	}
	public String getComp_Cod()
	{
		return Comp_Cod;
	}
	public void setComp_Cod(String aComp_Cod)
	{
		Comp_Cod = aComp_Cod;
	}
	public String getComp_Nam()
	{
		return Comp_Nam;
	}
	public void setComp_Nam(String aComp_Nam)
	{
		Comp_Nam = aComp_Nam;
	}
	public String getParty_Id()
	{
		return Party_Id;
	}
	public void setParty_Id(String aParty_Id)
	{
		Party_Id = aParty_Id;
	}
	public String getParty_Rol_Cod()
	{
		return Party_Rol_Cod;
	}
	public void setParty_Rol_Cod(String aParty_Rol_Cod)
	{
		Party_Rol_Cod = aParty_Rol_Cod;
	}
	public String getSales_Nam()
	{
		return Sales_Nam;
	}
	public void setSales_Nam(String aSales_Nam)
	{
		Sales_Nam = aSales_Nam;
	}
	public String getDate_Birthd()
	{
		return Date_Birthd;
	}
	public void setDate_Birthd(String aDate_Birthd)
	{
		Date_Birthd = aDate_Birthd;
	}
	public String getSex_Cod()
	{
		return Sex_Cod;
	}
	public void setSex_Cod(String aSex_Cod)
	{
		Sex_Cod = aSex_Cod;
	}
	public String getMan_Org_Cod()
	{
		return Man_Org_Cod;
	}
	public void setMan_Org_Cod(String aMan_Org_Cod)
	{
		Man_Org_Cod = aMan_Org_Cod;
	}
	public String getMan_Org_Nam()
	{
		return Man_Org_Nam;
	}
	public void setMan_Org_Nam(String aMan_Org_Nam)
	{
		Man_Org_Nam = aMan_Org_Nam;
	}
	public String getIdtyp_Cod()
	{
		return Idtyp_Cod;
	}
	public void setIdtyp_Cod(String aIdtyp_Cod)
	{
		Idtyp_Cod = aIdtyp_Cod;
	}
	public String getIdtyp_Nam()
	{
		return Idtyp_Nam;
	}
	public void setIdtyp_Nam(String aIdtyp_Nam)
	{
		Idtyp_Nam = aIdtyp_Nam;
	}
	public String getId_No()
	{
		return Id_No;
	}
	public void setId_No(String aId_No)
	{
		Id_No = aId_No;
	}
	public String getSales_Tel()
	{
		return Sales_Tel;
	}
	public void setSales_Tel(String aSales_Tel)
	{
		Sales_Tel = aSales_Tel;
	}
	public String getSales_Mob()
	{
		return Sales_Mob;
	}
	public void setSales_Mob(String aSales_Mob)
	{
		Sales_Mob = aSales_Mob;
	}
	public String getSales_Mail()
	{
		return Sales_Mail;
	}
	public void setSales_Mail(String aSales_Mail)
	{
		Sales_Mail = aSales_Mail;
	}
	public String getStatus_Cod()
	{
		return Status_Cod;
	}
	public void setStatus_Cod(String aStatus_Cod)
	{
		Status_Cod = aStatus_Cod;
	}
	public String getStatus()
	{
		return Status;
	}
	public void setStatus(String aStatus)
	{
		Status = aStatus;
	}
	public String getSales_Typ_Cod()
	{
		return Sales_Typ_Cod;
	}
	public void setSales_Typ_Cod(String aSales_Typ_Cod)
	{
		Sales_Typ_Cod = aSales_Typ_Cod;
	}
	public String getSales_Typ()
	{
		return Sales_Typ;
	}
	public void setSales_Typ(String aSales_Typ)
	{
		Sales_Typ = aSales_Typ;
	}
	public String getData_Upd_Typ()
	{
		return Data_Upd_Typ;
	}
	public void setData_Upd_Typ(String aData_Upd_Typ)
	{
		Data_Upd_Typ = aData_Upd_Typ;
	}
	public String getDate_Send()
	{
		return Date_Send;
	}
	public void setDate_Send(String aDate_Send)
	{
		Date_Send = aDate_Send;
	}
	public String getDate_Update()
	{
		return Date_Update;
	}
	public void setDate_Update(String aDate_Update)
	{
		Date_Update = aDate_Update;
	}
	public String getSalecha_Cod()
	{
		return Salecha_Cod;
	}
	public void setSalecha_Cod(String aSalecha_Cod)
	{
		Salecha_Cod = aSalecha_Cod;
	}
	public String getSalecha_Nam()
	{
		return Salecha_Nam;
	}
	public void setSalecha_Nam(String aSalecha_Nam)
	{
		Salecha_Nam = aSalecha_Nam;
	}
	public String getEntrant_Date()
	{
		return Entrant_Date;
	}
	public void setEntrant_Date(String aEntrant_Date)
	{
		Entrant_Date = aEntrant_Date;
	}
	public String getDimission_Date()
	{
		return Dimission_Date;
	}
	public void setDimission_Date(String aDimission_Date)
	{
		Dimission_Date = aDimission_Date;
	}
	public String getCheck_Status()
	{
		return Check_Status;
	}
	public void setCheck_Status(String aCheck_Status)
	{
		Check_Status = aCheck_Status;
	}
	public String getCheck_Date()
	{
		return Check_Date;
	}
	public void setCheck_Date(String aCheck_Date)
	{
		Check_Date = aCheck_Date;
	}
	public String getColl_Type()
	{
		return Coll_Type;
	}
	public void setColl_Type(String aColl_Type)
	{
		Coll_Type = aColl_Type;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LOMixSalesmanSchema 对象给 Schema 赋值
	* @param: aLOMixSalesmanSchema LOMixSalesmanSchema
	**/
	public void setSchema(LOMixSalesmanSchema aLOMixSalesmanSchema)
	{
		this.Sales_Cod = aLOMixSalesmanSchema.getSales_Cod();
		this.Comp_Cod = aLOMixSalesmanSchema.getComp_Cod();
		this.Comp_Nam = aLOMixSalesmanSchema.getComp_Nam();
		this.Party_Id = aLOMixSalesmanSchema.getParty_Id();
		this.Party_Rol_Cod = aLOMixSalesmanSchema.getParty_Rol_Cod();
		this.Sales_Nam = aLOMixSalesmanSchema.getSales_Nam();
		this.Date_Birthd = aLOMixSalesmanSchema.getDate_Birthd();
		this.Sex_Cod = aLOMixSalesmanSchema.getSex_Cod();
		this.Man_Org_Cod = aLOMixSalesmanSchema.getMan_Org_Cod();
		this.Man_Org_Nam = aLOMixSalesmanSchema.getMan_Org_Nam();
		this.Idtyp_Cod = aLOMixSalesmanSchema.getIdtyp_Cod();
		this.Idtyp_Nam = aLOMixSalesmanSchema.getIdtyp_Nam();
		this.Id_No = aLOMixSalesmanSchema.getId_No();
		this.Sales_Tel = aLOMixSalesmanSchema.getSales_Tel();
		this.Sales_Mob = aLOMixSalesmanSchema.getSales_Mob();
		this.Sales_Mail = aLOMixSalesmanSchema.getSales_Mail();
		this.Status_Cod = aLOMixSalesmanSchema.getStatus_Cod();
		this.Status = aLOMixSalesmanSchema.getStatus();
		this.Sales_Typ_Cod = aLOMixSalesmanSchema.getSales_Typ_Cod();
		this.Sales_Typ = aLOMixSalesmanSchema.getSales_Typ();
		this.Data_Upd_Typ = aLOMixSalesmanSchema.getData_Upd_Typ();
		this.Date_Send = aLOMixSalesmanSchema.getDate_Send();
		this.Date_Update = aLOMixSalesmanSchema.getDate_Update();
		this.Salecha_Cod = aLOMixSalesmanSchema.getSalecha_Cod();
		this.Salecha_Nam = aLOMixSalesmanSchema.getSalecha_Nam();
		this.Entrant_Date = aLOMixSalesmanSchema.getEntrant_Date();
		this.Dimission_Date = aLOMixSalesmanSchema.getDimission_Date();
		this.Check_Status = aLOMixSalesmanSchema.getCheck_Status();
		this.Check_Date = aLOMixSalesmanSchema.getCheck_Date();
		this.Coll_Type = aLOMixSalesmanSchema.getColl_Type();
		this.MakeDate = fDate.getDate( aLOMixSalesmanSchema.getMakeDate());
		this.MakeTime = aLOMixSalesmanSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLOMixSalesmanSchema.getModifyDate());
		this.ModifyTime = aLOMixSalesmanSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("Sales_Cod") == null )
				this.Sales_Cod = null;
			else
				this.Sales_Cod = rs.getString("Sales_Cod").trim();

			if( rs.getString("Comp_Cod") == null )
				this.Comp_Cod = null;
			else
				this.Comp_Cod = rs.getString("Comp_Cod").trim();

			if( rs.getString("Comp_Nam") == null )
				this.Comp_Nam = null;
			else
				this.Comp_Nam = rs.getString("Comp_Nam").trim();

			if( rs.getString("Party_Id") == null )
				this.Party_Id = null;
			else
				this.Party_Id = rs.getString("Party_Id").trim();

			if( rs.getString("Party_Rol_Cod") == null )
				this.Party_Rol_Cod = null;
			else
				this.Party_Rol_Cod = rs.getString("Party_Rol_Cod").trim();

			if( rs.getString("Sales_Nam") == null )
				this.Sales_Nam = null;
			else
				this.Sales_Nam = rs.getString("Sales_Nam").trim();

			if( rs.getString("Date_Birthd") == null )
				this.Date_Birthd = null;
			else
				this.Date_Birthd = rs.getString("Date_Birthd").trim();

			if( rs.getString("Sex_Cod") == null )
				this.Sex_Cod = null;
			else
				this.Sex_Cod = rs.getString("Sex_Cod").trim();

			if( rs.getString("Man_Org_Cod") == null )
				this.Man_Org_Cod = null;
			else
				this.Man_Org_Cod = rs.getString("Man_Org_Cod").trim();

			if( rs.getString("Man_Org_Nam") == null )
				this.Man_Org_Nam = null;
			else
				this.Man_Org_Nam = rs.getString("Man_Org_Nam").trim();

			if( rs.getString("Idtyp_Cod") == null )
				this.Idtyp_Cod = null;
			else
				this.Idtyp_Cod = rs.getString("Idtyp_Cod").trim();

			if( rs.getString("Idtyp_Nam") == null )
				this.Idtyp_Nam = null;
			else
				this.Idtyp_Nam = rs.getString("Idtyp_Nam").trim();

			if( rs.getString("Id_No") == null )
				this.Id_No = null;
			else
				this.Id_No = rs.getString("Id_No").trim();

			if( rs.getString("Sales_Tel") == null )
				this.Sales_Tel = null;
			else
				this.Sales_Tel = rs.getString("Sales_Tel").trim();

			if( rs.getString("Sales_Mob") == null )
				this.Sales_Mob = null;
			else
				this.Sales_Mob = rs.getString("Sales_Mob").trim();

			if( rs.getString("Sales_Mail") == null )
				this.Sales_Mail = null;
			else
				this.Sales_Mail = rs.getString("Sales_Mail").trim();

			if( rs.getString("Status_Cod") == null )
				this.Status_Cod = null;
			else
				this.Status_Cod = rs.getString("Status_Cod").trim();

			if( rs.getString("Status") == null )
				this.Status = null;
			else
				this.Status = rs.getString("Status").trim();

			if( rs.getString("Sales_Typ_Cod") == null )
				this.Sales_Typ_Cod = null;
			else
				this.Sales_Typ_Cod = rs.getString("Sales_Typ_Cod").trim();

			if( rs.getString("Sales_Typ") == null )
				this.Sales_Typ = null;
			else
				this.Sales_Typ = rs.getString("Sales_Typ").trim();

			if( rs.getString("Data_Upd_Typ") == null )
				this.Data_Upd_Typ = null;
			else
				this.Data_Upd_Typ = rs.getString("Data_Upd_Typ").trim();

			if( rs.getString("Date_Send") == null )
				this.Date_Send = null;
			else
				this.Date_Send = rs.getString("Date_Send").trim();

			if( rs.getString("Date_Update") == null )
				this.Date_Update = null;
			else
				this.Date_Update = rs.getString("Date_Update").trim();

			if( rs.getString("Salecha_Cod") == null )
				this.Salecha_Cod = null;
			else
				this.Salecha_Cod = rs.getString("Salecha_Cod").trim();

			if( rs.getString("Salecha_Nam") == null )
				this.Salecha_Nam = null;
			else
				this.Salecha_Nam = rs.getString("Salecha_Nam").trim();

			if( rs.getString("Entrant_Date") == null )
				this.Entrant_Date = null;
			else
				this.Entrant_Date = rs.getString("Entrant_Date").trim();

			if( rs.getString("Dimission_Date") == null )
				this.Dimission_Date = null;
			else
				this.Dimission_Date = rs.getString("Dimission_Date").trim();

			if( rs.getString("Check_Status") == null )
				this.Check_Status = null;
			else
				this.Check_Status = rs.getString("Check_Status").trim();

			if( rs.getString("Check_Date") == null )
				this.Check_Date = null;
			else
				this.Check_Date = rs.getString("Check_Date").trim();

			if( rs.getString("Coll_Type") == null )
				this.Coll_Type = null;
			else
				this.Coll_Type = rs.getString("Coll_Type").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LOMixSalesman表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LOMixSalesmanSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LOMixSalesmanSchema getSchema()
	{
		LOMixSalesmanSchema aLOMixSalesmanSchema = new LOMixSalesmanSchema();
		aLOMixSalesmanSchema.setSchema(this);
		return aLOMixSalesmanSchema;
	}

	public LOMixSalesmanDB getDB()
	{
		LOMixSalesmanDB aDBOper = new LOMixSalesmanDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOMixSalesman描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(Sales_Cod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Comp_Cod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Comp_Nam)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Party_Id)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Party_Rol_Cod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Sales_Nam)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Date_Birthd)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Sex_Cod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Man_Org_Cod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Man_Org_Nam)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Idtyp_Cod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Idtyp_Nam)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Id_No)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Sales_Tel)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Sales_Mob)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Sales_Mail)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Status_Cod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Status)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Sales_Typ_Cod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Sales_Typ)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Data_Upd_Typ)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Date_Send)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Date_Update)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Salecha_Cod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Salecha_Nam)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Entrant_Date)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Dimission_Date)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Check_Status)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Check_Date)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Coll_Type)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOMixSalesman>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			Sales_Cod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			Comp_Cod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			Comp_Nam = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			Party_Id = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			Party_Rol_Cod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			Sales_Nam = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			Date_Birthd = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			Sex_Cod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			Man_Org_Cod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			Man_Org_Nam = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			Idtyp_Cod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			Idtyp_Nam = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			Id_No = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			Sales_Tel = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			Sales_Mob = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			Sales_Mail = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			Status_Cod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			Status = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			Sales_Typ_Cod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			Sales_Typ = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			Data_Upd_Typ = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			Date_Send = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			Date_Update = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			Salecha_Cod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			Salecha_Nam = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			Entrant_Date = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			Dimission_Date = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			Check_Status = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			Check_Date = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			Coll_Type = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LOMixSalesmanSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("Sales_Cod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Sales_Cod));
		}
		if (FCode.equals("Comp_Cod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Comp_Cod));
		}
		if (FCode.equals("Comp_Nam"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Comp_Nam));
		}
		if (FCode.equals("Party_Id"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Party_Id));
		}
		if (FCode.equals("Party_Rol_Cod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Party_Rol_Cod));
		}
		if (FCode.equals("Sales_Nam"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Sales_Nam));
		}
		if (FCode.equals("Date_Birthd"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Date_Birthd));
		}
		if (FCode.equals("Sex_Cod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Sex_Cod));
		}
		if (FCode.equals("Man_Org_Cod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Man_Org_Cod));
		}
		if (FCode.equals("Man_Org_Nam"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Man_Org_Nam));
		}
		if (FCode.equals("Idtyp_Cod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Idtyp_Cod));
		}
		if (FCode.equals("Idtyp_Nam"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Idtyp_Nam));
		}
		if (FCode.equals("Id_No"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Id_No));
		}
		if (FCode.equals("Sales_Tel"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Sales_Tel));
		}
		if (FCode.equals("Sales_Mob"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Sales_Mob));
		}
		if (FCode.equals("Sales_Mail"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Sales_Mail));
		}
		if (FCode.equals("Status_Cod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Status_Cod));
		}
		if (FCode.equals("Status"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Status));
		}
		if (FCode.equals("Sales_Typ_Cod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Sales_Typ_Cod));
		}
		if (FCode.equals("Sales_Typ"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Sales_Typ));
		}
		if (FCode.equals("Data_Upd_Typ"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Data_Upd_Typ));
		}
		if (FCode.equals("Date_Send"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Date_Send));
		}
		if (FCode.equals("Date_Update"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Date_Update));
		}
		if (FCode.equals("Salecha_Cod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Salecha_Cod));
		}
		if (FCode.equals("Salecha_Nam"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Salecha_Nam));
		}
		if (FCode.equals("Entrant_Date"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Entrant_Date));
		}
		if (FCode.equals("Dimission_Date"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Dimission_Date));
		}
		if (FCode.equals("Check_Status"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Check_Status));
		}
		if (FCode.equals("Check_Date"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Check_Date));
		}
		if (FCode.equals("Coll_Type"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Coll_Type));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(Sales_Cod);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(Comp_Cod);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(Comp_Nam);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(Party_Id);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(Party_Rol_Cod);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(Sales_Nam);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(Date_Birthd);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(Sex_Cod);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(Man_Org_Cod);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(Man_Org_Nam);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(Idtyp_Cod);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(Idtyp_Nam);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(Id_No);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(Sales_Tel);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(Sales_Mob);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(Sales_Mail);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(Status_Cod);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(Status);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(Sales_Typ_Cod);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(Sales_Typ);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(Data_Upd_Typ);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(Date_Send);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(Date_Update);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(Salecha_Cod);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(Salecha_Nam);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(Entrant_Date);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(Dimission_Date);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(Check_Status);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(Check_Date);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(Coll_Type);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("Sales_Cod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Sales_Cod = FValue.trim();
			}
			else
				Sales_Cod = null;
		}
		if (FCode.equalsIgnoreCase("Comp_Cod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Comp_Cod = FValue.trim();
			}
			else
				Comp_Cod = null;
		}
		if (FCode.equalsIgnoreCase("Comp_Nam"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Comp_Nam = FValue.trim();
			}
			else
				Comp_Nam = null;
		}
		if (FCode.equalsIgnoreCase("Party_Id"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Party_Id = FValue.trim();
			}
			else
				Party_Id = null;
		}
		if (FCode.equalsIgnoreCase("Party_Rol_Cod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Party_Rol_Cod = FValue.trim();
			}
			else
				Party_Rol_Cod = null;
		}
		if (FCode.equalsIgnoreCase("Sales_Nam"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Sales_Nam = FValue.trim();
			}
			else
				Sales_Nam = null;
		}
		if (FCode.equalsIgnoreCase("Date_Birthd"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Date_Birthd = FValue.trim();
			}
			else
				Date_Birthd = null;
		}
		if (FCode.equalsIgnoreCase("Sex_Cod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Sex_Cod = FValue.trim();
			}
			else
				Sex_Cod = null;
		}
		if (FCode.equalsIgnoreCase("Man_Org_Cod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Man_Org_Cod = FValue.trim();
			}
			else
				Man_Org_Cod = null;
		}
		if (FCode.equalsIgnoreCase("Man_Org_Nam"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Man_Org_Nam = FValue.trim();
			}
			else
				Man_Org_Nam = null;
		}
		if (FCode.equalsIgnoreCase("Idtyp_Cod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Idtyp_Cod = FValue.trim();
			}
			else
				Idtyp_Cod = null;
		}
		if (FCode.equalsIgnoreCase("Idtyp_Nam"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Idtyp_Nam = FValue.trim();
			}
			else
				Idtyp_Nam = null;
		}
		if (FCode.equalsIgnoreCase("Id_No"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Id_No = FValue.trim();
			}
			else
				Id_No = null;
		}
		if (FCode.equalsIgnoreCase("Sales_Tel"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Sales_Tel = FValue.trim();
			}
			else
				Sales_Tel = null;
		}
		if (FCode.equalsIgnoreCase("Sales_Mob"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Sales_Mob = FValue.trim();
			}
			else
				Sales_Mob = null;
		}
		if (FCode.equalsIgnoreCase("Sales_Mail"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Sales_Mail = FValue.trim();
			}
			else
				Sales_Mail = null;
		}
		if (FCode.equalsIgnoreCase("Status_Cod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Status_Cod = FValue.trim();
			}
			else
				Status_Cod = null;
		}
		if (FCode.equalsIgnoreCase("Status"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Status = FValue.trim();
			}
			else
				Status = null;
		}
		if (FCode.equalsIgnoreCase("Sales_Typ_Cod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Sales_Typ_Cod = FValue.trim();
			}
			else
				Sales_Typ_Cod = null;
		}
		if (FCode.equalsIgnoreCase("Sales_Typ"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Sales_Typ = FValue.trim();
			}
			else
				Sales_Typ = null;
		}
		if (FCode.equalsIgnoreCase("Data_Upd_Typ"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Data_Upd_Typ = FValue.trim();
			}
			else
				Data_Upd_Typ = null;
		}
		if (FCode.equalsIgnoreCase("Date_Send"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Date_Send = FValue.trim();
			}
			else
				Date_Send = null;
		}
		if (FCode.equalsIgnoreCase("Date_Update"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Date_Update = FValue.trim();
			}
			else
				Date_Update = null;
		}
		if (FCode.equalsIgnoreCase("Salecha_Cod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Salecha_Cod = FValue.trim();
			}
			else
				Salecha_Cod = null;
		}
		if (FCode.equalsIgnoreCase("Salecha_Nam"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Salecha_Nam = FValue.trim();
			}
			else
				Salecha_Nam = null;
		}
		if (FCode.equalsIgnoreCase("Entrant_Date"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Entrant_Date = FValue.trim();
			}
			else
				Entrant_Date = null;
		}
		if (FCode.equalsIgnoreCase("Dimission_Date"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Dimission_Date = FValue.trim();
			}
			else
				Dimission_Date = null;
		}
		if (FCode.equalsIgnoreCase("Check_Status"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Check_Status = FValue.trim();
			}
			else
				Check_Status = null;
		}
		if (FCode.equalsIgnoreCase("Check_Date"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Check_Date = FValue.trim();
			}
			else
				Check_Date = null;
		}
		if (FCode.equalsIgnoreCase("Coll_Type"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Coll_Type = FValue.trim();
			}
			else
				Coll_Type = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LOMixSalesmanSchema other = (LOMixSalesmanSchema)otherObject;
		return
			(Sales_Cod == null ? other.getSales_Cod() == null : Sales_Cod.equals(other.getSales_Cod()))
			&& (Comp_Cod == null ? other.getComp_Cod() == null : Comp_Cod.equals(other.getComp_Cod()))
			&& (Comp_Nam == null ? other.getComp_Nam() == null : Comp_Nam.equals(other.getComp_Nam()))
			&& (Party_Id == null ? other.getParty_Id() == null : Party_Id.equals(other.getParty_Id()))
			&& (Party_Rol_Cod == null ? other.getParty_Rol_Cod() == null : Party_Rol_Cod.equals(other.getParty_Rol_Cod()))
			&& (Sales_Nam == null ? other.getSales_Nam() == null : Sales_Nam.equals(other.getSales_Nam()))
			&& (Date_Birthd == null ? other.getDate_Birthd() == null : Date_Birthd.equals(other.getDate_Birthd()))
			&& (Sex_Cod == null ? other.getSex_Cod() == null : Sex_Cod.equals(other.getSex_Cod()))
			&& (Man_Org_Cod == null ? other.getMan_Org_Cod() == null : Man_Org_Cod.equals(other.getMan_Org_Cod()))
			&& (Man_Org_Nam == null ? other.getMan_Org_Nam() == null : Man_Org_Nam.equals(other.getMan_Org_Nam()))
			&& (Idtyp_Cod == null ? other.getIdtyp_Cod() == null : Idtyp_Cod.equals(other.getIdtyp_Cod()))
			&& (Idtyp_Nam == null ? other.getIdtyp_Nam() == null : Idtyp_Nam.equals(other.getIdtyp_Nam()))
			&& (Id_No == null ? other.getId_No() == null : Id_No.equals(other.getId_No()))
			&& (Sales_Tel == null ? other.getSales_Tel() == null : Sales_Tel.equals(other.getSales_Tel()))
			&& (Sales_Mob == null ? other.getSales_Mob() == null : Sales_Mob.equals(other.getSales_Mob()))
			&& (Sales_Mail == null ? other.getSales_Mail() == null : Sales_Mail.equals(other.getSales_Mail()))
			&& (Status_Cod == null ? other.getStatus_Cod() == null : Status_Cod.equals(other.getStatus_Cod()))
			&& (Status == null ? other.getStatus() == null : Status.equals(other.getStatus()))
			&& (Sales_Typ_Cod == null ? other.getSales_Typ_Cod() == null : Sales_Typ_Cod.equals(other.getSales_Typ_Cod()))
			&& (Sales_Typ == null ? other.getSales_Typ() == null : Sales_Typ.equals(other.getSales_Typ()))
			&& (Data_Upd_Typ == null ? other.getData_Upd_Typ() == null : Data_Upd_Typ.equals(other.getData_Upd_Typ()))
			&& (Date_Send == null ? other.getDate_Send() == null : Date_Send.equals(other.getDate_Send()))
			&& (Date_Update == null ? other.getDate_Update() == null : Date_Update.equals(other.getDate_Update()))
			&& (Salecha_Cod == null ? other.getSalecha_Cod() == null : Salecha_Cod.equals(other.getSalecha_Cod()))
			&& (Salecha_Nam == null ? other.getSalecha_Nam() == null : Salecha_Nam.equals(other.getSalecha_Nam()))
			&& (Entrant_Date == null ? other.getEntrant_Date() == null : Entrant_Date.equals(other.getEntrant_Date()))
			&& (Dimission_Date == null ? other.getDimission_Date() == null : Dimission_Date.equals(other.getDimission_Date()))
			&& (Check_Status == null ? other.getCheck_Status() == null : Check_Status.equals(other.getCheck_Status()))
			&& (Check_Date == null ? other.getCheck_Date() == null : Check_Date.equals(other.getCheck_Date()))
			&& (Coll_Type == null ? other.getColl_Type() == null : Coll_Type.equals(other.getColl_Type()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("Sales_Cod") ) {
			return 0;
		}
		if( strFieldName.equals("Comp_Cod") ) {
			return 1;
		}
		if( strFieldName.equals("Comp_Nam") ) {
			return 2;
		}
		if( strFieldName.equals("Party_Id") ) {
			return 3;
		}
		if( strFieldName.equals("Party_Rol_Cod") ) {
			return 4;
		}
		if( strFieldName.equals("Sales_Nam") ) {
			return 5;
		}
		if( strFieldName.equals("Date_Birthd") ) {
			return 6;
		}
		if( strFieldName.equals("Sex_Cod") ) {
			return 7;
		}
		if( strFieldName.equals("Man_Org_Cod") ) {
			return 8;
		}
		if( strFieldName.equals("Man_Org_Nam") ) {
			return 9;
		}
		if( strFieldName.equals("Idtyp_Cod") ) {
			return 10;
		}
		if( strFieldName.equals("Idtyp_Nam") ) {
			return 11;
		}
		if( strFieldName.equals("Id_No") ) {
			return 12;
		}
		if( strFieldName.equals("Sales_Tel") ) {
			return 13;
		}
		if( strFieldName.equals("Sales_Mob") ) {
			return 14;
		}
		if( strFieldName.equals("Sales_Mail") ) {
			return 15;
		}
		if( strFieldName.equals("Status_Cod") ) {
			return 16;
		}
		if( strFieldName.equals("Status") ) {
			return 17;
		}
		if( strFieldName.equals("Sales_Typ_Cod") ) {
			return 18;
		}
		if( strFieldName.equals("Sales_Typ") ) {
			return 19;
		}
		if( strFieldName.equals("Data_Upd_Typ") ) {
			return 20;
		}
		if( strFieldName.equals("Date_Send") ) {
			return 21;
		}
		if( strFieldName.equals("Date_Update") ) {
			return 22;
		}
		if( strFieldName.equals("Salecha_Cod") ) {
			return 23;
		}
		if( strFieldName.equals("Salecha_Nam") ) {
			return 24;
		}
		if( strFieldName.equals("Entrant_Date") ) {
			return 25;
		}
		if( strFieldName.equals("Dimission_Date") ) {
			return 26;
		}
		if( strFieldName.equals("Check_Status") ) {
			return 27;
		}
		if( strFieldName.equals("Check_Date") ) {
			return 28;
		}
		if( strFieldName.equals("Coll_Type") ) {
			return 29;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 30;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 31;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 32;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 33;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "Sales_Cod";
				break;
			case 1:
				strFieldName = "Comp_Cod";
				break;
			case 2:
				strFieldName = "Comp_Nam";
				break;
			case 3:
				strFieldName = "Party_Id";
				break;
			case 4:
				strFieldName = "Party_Rol_Cod";
				break;
			case 5:
				strFieldName = "Sales_Nam";
				break;
			case 6:
				strFieldName = "Date_Birthd";
				break;
			case 7:
				strFieldName = "Sex_Cod";
				break;
			case 8:
				strFieldName = "Man_Org_Cod";
				break;
			case 9:
				strFieldName = "Man_Org_Nam";
				break;
			case 10:
				strFieldName = "Idtyp_Cod";
				break;
			case 11:
				strFieldName = "Idtyp_Nam";
				break;
			case 12:
				strFieldName = "Id_No";
				break;
			case 13:
				strFieldName = "Sales_Tel";
				break;
			case 14:
				strFieldName = "Sales_Mob";
				break;
			case 15:
				strFieldName = "Sales_Mail";
				break;
			case 16:
				strFieldName = "Status_Cod";
				break;
			case 17:
				strFieldName = "Status";
				break;
			case 18:
				strFieldName = "Sales_Typ_Cod";
				break;
			case 19:
				strFieldName = "Sales_Typ";
				break;
			case 20:
				strFieldName = "Data_Upd_Typ";
				break;
			case 21:
				strFieldName = "Date_Send";
				break;
			case 22:
				strFieldName = "Date_Update";
				break;
			case 23:
				strFieldName = "Salecha_Cod";
				break;
			case 24:
				strFieldName = "Salecha_Nam";
				break;
			case 25:
				strFieldName = "Entrant_Date";
				break;
			case 26:
				strFieldName = "Dimission_Date";
				break;
			case 27:
				strFieldName = "Check_Status";
				break;
			case 28:
				strFieldName = "Check_Date";
				break;
			case 29:
				strFieldName = "Coll_Type";
				break;
			case 30:
				strFieldName = "MakeDate";
				break;
			case 31:
				strFieldName = "MakeTime";
				break;
			case 32:
				strFieldName = "ModifyDate";
				break;
			case 33:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("Sales_Cod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Comp_Cod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Comp_Nam") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Party_Id") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Party_Rol_Cod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Sales_Nam") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Date_Birthd") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Sex_Cod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Man_Org_Cod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Man_Org_Nam") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Idtyp_Cod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Idtyp_Nam") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Id_No") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Sales_Tel") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Sales_Mob") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Sales_Mail") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Status_Cod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Status") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Sales_Typ_Cod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Sales_Typ") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Data_Upd_Typ") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Date_Send") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Date_Update") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Salecha_Cod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Salecha_Nam") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Entrant_Date") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Dimission_Date") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Check_Status") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Check_Date") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Coll_Type") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 32:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
