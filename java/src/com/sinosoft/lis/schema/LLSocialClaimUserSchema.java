/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LLSocialClaimUserDB;

/*
 * <p>ClassName: LLSocialClaimUserSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2014-10-27
 */
public class LLSocialClaimUserSchema implements Schema, Cloneable
{
	// @Field
	/** 用户姓名 */
	private String UserName;
	/** 机构编码 */
	private String ComCode;
	/** 上级用户代码 */
	private String UpUserCode;
	/** 理赔处理权限 */
	private String ClaimDeal;
	/** 理赔查询权限 */
	private String ClaimQuery;
	/** 报案权限 */
	private String ReportFlag;
	/** 立案权限 */
	private String RegisterFlag;
	/** 理赔计算权限 */
	private String ClaimFlag;
	/** 用户编码 */
	private String UserCode;
	/** 核赔权限 */
	private String ClaimPopedom;
	/** 审核权限 */
	private String CheckFlag;
	/** 签批权限 */
	private String UWFlag;
	/** 调查权限 */
	private String SurveyFlag;
	/** 给付权限 */
	private String PayFlag;
	/** 有效标识 */
	private String StateFlag;
	/** 管理机构 */
	private String ManageCom;
	/** 原因 */
	private String Reason;
	/** 备注 */
	private String Remark;
	/** 理赔标记 */
	private String RgtFlag;
	/** 是否参加案件分配 */
	private String HandleFlag;
	/** 是否参加账单录入分配 */
	private String InputFlag;
	/** 案件分配比例 */
	private int DispatchRate;
	/** 预付赔款审批权限 */
	private String PrepaidFlag;
	/** 预付赔款审批上级 */
	private String PrepaidUpUserCode;
	/** 预付赔款审批额度 */
	private double PrepaidLimit;
	/** 社保业务结案金额 */
	private double SocialMoney;
	/** 入机时间 */
	private Date MakeDate;
	/** 入机日期 */
	private String MakeTime;
	/** 修改时间 */
	private Date ModifyDate;
	/** 修改日期 */
	private String ModifyTime;
	/** 操作人员 */
	private String Operator;
	/** 预付赔款审批金额 */
	private double PrepaidMoney;

	public static final int FIELDNUM = 32;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LLSocialClaimUserSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "UserCode";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LLSocialClaimUserSchema cloned = (LLSocialClaimUserSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getUserName()
	{
		return UserName;
	}
	public void setUserName(String aUserName)
	{
		UserName = aUserName;
	}
	public String getComCode()
	{
		return ComCode;
	}
	public void setComCode(String aComCode)
	{
		ComCode = aComCode;
	}
	public String getUpUserCode()
	{
		return UpUserCode;
	}
	public void setUpUserCode(String aUpUserCode)
	{
		UpUserCode = aUpUserCode;
	}
	public String getClaimDeal()
	{
		return ClaimDeal;
	}
	public void setClaimDeal(String aClaimDeal)
	{
		ClaimDeal = aClaimDeal;
	}
	public String getClaimQuery()
	{
		return ClaimQuery;
	}
	public void setClaimQuery(String aClaimQuery)
	{
		ClaimQuery = aClaimQuery;
	}
	public String getReportFlag()
	{
		return ReportFlag;
	}
	public void setReportFlag(String aReportFlag)
	{
		ReportFlag = aReportFlag;
	}
	public String getRegisterFlag()
	{
		return RegisterFlag;
	}
	public void setRegisterFlag(String aRegisterFlag)
	{
		RegisterFlag = aRegisterFlag;
	}
	public String getClaimFlag()
	{
		return ClaimFlag;
	}
	public void setClaimFlag(String aClaimFlag)
	{
		ClaimFlag = aClaimFlag;
	}
	public String getUserCode()
	{
		return UserCode;
	}
	public void setUserCode(String aUserCode)
	{
		UserCode = aUserCode;
	}
	public String getClaimPopedom()
	{
		return ClaimPopedom;
	}
	public void setClaimPopedom(String aClaimPopedom)
	{
		ClaimPopedom = aClaimPopedom;
	}
	public String getCheckFlag()
	{
		return CheckFlag;
	}
	public void setCheckFlag(String aCheckFlag)
	{
		CheckFlag = aCheckFlag;
	}
	public String getUWFlag()
	{
		return UWFlag;
	}
	public void setUWFlag(String aUWFlag)
	{
		UWFlag = aUWFlag;
	}
	public String getSurveyFlag()
	{
		return SurveyFlag;
	}
	public void setSurveyFlag(String aSurveyFlag)
	{
		SurveyFlag = aSurveyFlag;
	}
	public String getPayFlag()
	{
		return PayFlag;
	}
	public void setPayFlag(String aPayFlag)
	{
		PayFlag = aPayFlag;
	}
	public String getStateFlag()
	{
		return StateFlag;
	}
	public void setStateFlag(String aStateFlag)
	{
		StateFlag = aStateFlag;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getReason()
	{
		return Reason;
	}
	public void setReason(String aReason)
	{
		Reason = aReason;
	}
	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
		Remark = aRemark;
	}
	public String getRgtFlag()
	{
		return RgtFlag;
	}
	public void setRgtFlag(String aRgtFlag)
	{
		RgtFlag = aRgtFlag;
	}
	public String getHandleFlag()
	{
		return HandleFlag;
	}
	public void setHandleFlag(String aHandleFlag)
	{
		HandleFlag = aHandleFlag;
	}
	public String getInputFlag()
	{
		return InputFlag;
	}
	public void setInputFlag(String aInputFlag)
	{
		InputFlag = aInputFlag;
	}
	public int getDispatchRate()
	{
		return DispatchRate;
	}
	public void setDispatchRate(int aDispatchRate)
	{
		DispatchRate = aDispatchRate;
	}
	public void setDispatchRate(String aDispatchRate)
	{
		if (aDispatchRate != null && !aDispatchRate.equals(""))
		{
			Integer tInteger = new Integer(aDispatchRate);
			int i = tInteger.intValue();
			DispatchRate = i;
		}
	}

	public String getPrepaidFlag()
	{
		return PrepaidFlag;
	}
	public void setPrepaidFlag(String aPrepaidFlag)
	{
		PrepaidFlag = aPrepaidFlag;
	}
	public String getPrepaidUpUserCode()
	{
		return PrepaidUpUserCode;
	}
	public void setPrepaidUpUserCode(String aPrepaidUpUserCode)
	{
		PrepaidUpUserCode = aPrepaidUpUserCode;
	}
	public double getPrepaidLimit()
	{
		return PrepaidLimit;
	}
	public void setPrepaidLimit(double aPrepaidLimit)
	{
		PrepaidLimit = Arith.round(aPrepaidLimit,2);
	}
	public void setPrepaidLimit(String aPrepaidLimit)
	{
		if (aPrepaidLimit != null && !aPrepaidLimit.equals(""))
		{
			Double tDouble = new Double(aPrepaidLimit);
			double d = tDouble.doubleValue();
                PrepaidLimit = Arith.round(d,2);
		}
	}

	public double getSocialMoney()
	{
		return SocialMoney;
	}
	public void setSocialMoney(double aSocialMoney)
	{
		SocialMoney = Arith.round(aSocialMoney,2);
	}
	public void setSocialMoney(String aSocialMoney)
	{
		if (aSocialMoney != null && !aSocialMoney.equals(""))
		{
			Double tDouble = new Double(aSocialMoney);
			double d = tDouble.doubleValue();
                SocialMoney = Arith.round(d,2);
		}
	}

	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public double getPrepaidMoney()
	{
		return PrepaidMoney;
	}
	public void setPrepaidMoney(double aPrepaidMoney)
	{
		PrepaidMoney = Arith.round(aPrepaidMoney,2);
	}
	public void setPrepaidMoney(String aPrepaidMoney)
	{
		if (aPrepaidMoney != null && !aPrepaidMoney.equals(""))
		{
			Double tDouble = new Double(aPrepaidMoney);
			double d = tDouble.doubleValue();
                PrepaidMoney = Arith.round(d,2);
		}
	}


	/**
	* 使用另外一个 LLSocialClaimUserSchema 对象给 Schema 赋值
	* @param: aLLSocialClaimUserSchema LLSocialClaimUserSchema
	**/
	public void setSchema(LLSocialClaimUserSchema aLLSocialClaimUserSchema)
	{
		this.UserName = aLLSocialClaimUserSchema.getUserName();
		this.ComCode = aLLSocialClaimUserSchema.getComCode();
		this.UpUserCode = aLLSocialClaimUserSchema.getUpUserCode();
		this.ClaimDeal = aLLSocialClaimUserSchema.getClaimDeal();
		this.ClaimQuery = aLLSocialClaimUserSchema.getClaimQuery();
		this.ReportFlag = aLLSocialClaimUserSchema.getReportFlag();
		this.RegisterFlag = aLLSocialClaimUserSchema.getRegisterFlag();
		this.ClaimFlag = aLLSocialClaimUserSchema.getClaimFlag();
		this.UserCode = aLLSocialClaimUserSchema.getUserCode();
		this.ClaimPopedom = aLLSocialClaimUserSchema.getClaimPopedom();
		this.CheckFlag = aLLSocialClaimUserSchema.getCheckFlag();
		this.UWFlag = aLLSocialClaimUserSchema.getUWFlag();
		this.SurveyFlag = aLLSocialClaimUserSchema.getSurveyFlag();
		this.PayFlag = aLLSocialClaimUserSchema.getPayFlag();
		this.StateFlag = aLLSocialClaimUserSchema.getStateFlag();
		this.ManageCom = aLLSocialClaimUserSchema.getManageCom();
		this.Reason = aLLSocialClaimUserSchema.getReason();
		this.Remark = aLLSocialClaimUserSchema.getRemark();
		this.RgtFlag = aLLSocialClaimUserSchema.getRgtFlag();
		this.HandleFlag = aLLSocialClaimUserSchema.getHandleFlag();
		this.InputFlag = aLLSocialClaimUserSchema.getInputFlag();
		this.DispatchRate = aLLSocialClaimUserSchema.getDispatchRate();
		this.PrepaidFlag = aLLSocialClaimUserSchema.getPrepaidFlag();
		this.PrepaidUpUserCode = aLLSocialClaimUserSchema.getPrepaidUpUserCode();
		this.PrepaidLimit = aLLSocialClaimUserSchema.getPrepaidLimit();
		this.SocialMoney = aLLSocialClaimUserSchema.getSocialMoney();
		this.MakeDate = fDate.getDate( aLLSocialClaimUserSchema.getMakeDate());
		this.MakeTime = aLLSocialClaimUserSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLLSocialClaimUserSchema.getModifyDate());
		this.ModifyTime = aLLSocialClaimUserSchema.getModifyTime();
		this.Operator = aLLSocialClaimUserSchema.getOperator();
		this.PrepaidMoney = aLLSocialClaimUserSchema.getPrepaidMoney();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("UserName") == null )
				this.UserName = null;
			else
				this.UserName = rs.getString("UserName").trim();

			if( rs.getString("ComCode") == null )
				this.ComCode = null;
			else
				this.ComCode = rs.getString("ComCode").trim();

			if( rs.getString("UpUserCode") == null )
				this.UpUserCode = null;
			else
				this.UpUserCode = rs.getString("UpUserCode").trim();

			if( rs.getString("ClaimDeal") == null )
				this.ClaimDeal = null;
			else
				this.ClaimDeal = rs.getString("ClaimDeal").trim();

			if( rs.getString("ClaimQuery") == null )
				this.ClaimQuery = null;
			else
				this.ClaimQuery = rs.getString("ClaimQuery").trim();

			if( rs.getString("ReportFlag") == null )
				this.ReportFlag = null;
			else
				this.ReportFlag = rs.getString("ReportFlag").trim();

			if( rs.getString("RegisterFlag") == null )
				this.RegisterFlag = null;
			else
				this.RegisterFlag = rs.getString("RegisterFlag").trim();

			if( rs.getString("ClaimFlag") == null )
				this.ClaimFlag = null;
			else
				this.ClaimFlag = rs.getString("ClaimFlag").trim();

			if( rs.getString("UserCode") == null )
				this.UserCode = null;
			else
				this.UserCode = rs.getString("UserCode").trim();

			if( rs.getString("ClaimPopedom") == null )
				this.ClaimPopedom = null;
			else
				this.ClaimPopedom = rs.getString("ClaimPopedom").trim();

			if( rs.getString("CheckFlag") == null )
				this.CheckFlag = null;
			else
				this.CheckFlag = rs.getString("CheckFlag").trim();

			if( rs.getString("UWFlag") == null )
				this.UWFlag = null;
			else
				this.UWFlag = rs.getString("UWFlag").trim();

			if( rs.getString("SurveyFlag") == null )
				this.SurveyFlag = null;
			else
				this.SurveyFlag = rs.getString("SurveyFlag").trim();

			if( rs.getString("PayFlag") == null )
				this.PayFlag = null;
			else
				this.PayFlag = rs.getString("PayFlag").trim();

			if( rs.getString("StateFlag") == null )
				this.StateFlag = null;
			else
				this.StateFlag = rs.getString("StateFlag").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("Reason") == null )
				this.Reason = null;
			else
				this.Reason = rs.getString("Reason").trim();

			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

			if( rs.getString("RgtFlag") == null )
				this.RgtFlag = null;
			else
				this.RgtFlag = rs.getString("RgtFlag").trim();

			if( rs.getString("HandleFlag") == null )
				this.HandleFlag = null;
			else
				this.HandleFlag = rs.getString("HandleFlag").trim();

			if( rs.getString("InputFlag") == null )
				this.InputFlag = null;
			else
				this.InputFlag = rs.getString("InputFlag").trim();

			this.DispatchRate = rs.getInt("DispatchRate");
			if( rs.getString("PrepaidFlag") == null )
				this.PrepaidFlag = null;
			else
				this.PrepaidFlag = rs.getString("PrepaidFlag").trim();

			if( rs.getString("PrepaidUpUserCode") == null )
				this.PrepaidUpUserCode = null;
			else
				this.PrepaidUpUserCode = rs.getString("PrepaidUpUserCode").trim();

			this.PrepaidLimit = rs.getDouble("PrepaidLimit");
			this.SocialMoney = rs.getDouble("SocialMoney");
			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.PrepaidMoney = rs.getDouble("PrepaidMoney");
		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LLSocialClaimUser表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLSocialClaimUserSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LLSocialClaimUserSchema getSchema()
	{
		LLSocialClaimUserSchema aLLSocialClaimUserSchema = new LLSocialClaimUserSchema();
		aLLSocialClaimUserSchema.setSchema(this);
		return aLLSocialClaimUserSchema;
	}

	public LLSocialClaimUserDB getDB()
	{
		LLSocialClaimUserDB aDBOper = new LLSocialClaimUserDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLSocialClaimUser描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(UserName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ComCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UpUserCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ClaimDeal)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ClaimQuery)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReportFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RegisterFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ClaimFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UserCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ClaimPopedom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CheckFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UWFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SurveyFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StateFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Reason)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RgtFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HandleFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InputFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(DispatchRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PrepaidFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PrepaidUpUserCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PrepaidLimit));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SocialMoney));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PrepaidMoney));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLSocialClaimUser>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			UserName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			ComCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			UpUserCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			ClaimDeal = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			ClaimQuery = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			ReportFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			RegisterFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			ClaimFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			UserCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			ClaimPopedom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			CheckFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			UWFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			SurveyFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			PayFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			StateFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			Reason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			RgtFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			HandleFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			InputFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			DispatchRate= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,22,SysConst.PACKAGESPILTER))).intValue();
			PrepaidFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			PrepaidUpUserCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			PrepaidLimit = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,25,SysConst.PACKAGESPILTER))).doubleValue();
			SocialMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,26,SysConst.PACKAGESPILTER))).doubleValue();
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			PrepaidMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,32,SysConst.PACKAGESPILTER))).doubleValue();
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLSocialClaimUserSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("UserName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UserName));
		}
		if (FCode.equals("ComCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ComCode));
		}
		if (FCode.equals("UpUserCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UpUserCode));
		}
		if (FCode.equals("ClaimDeal"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClaimDeal));
		}
		if (FCode.equals("ClaimQuery"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClaimQuery));
		}
		if (FCode.equals("ReportFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReportFlag));
		}
		if (FCode.equals("RegisterFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RegisterFlag));
		}
		if (FCode.equals("ClaimFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClaimFlag));
		}
		if (FCode.equals("UserCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UserCode));
		}
		if (FCode.equals("ClaimPopedom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClaimPopedom));
		}
		if (FCode.equals("CheckFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CheckFlag));
		}
		if (FCode.equals("UWFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UWFlag));
		}
		if (FCode.equals("SurveyFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SurveyFlag));
		}
		if (FCode.equals("PayFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayFlag));
		}
		if (FCode.equals("StateFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StateFlag));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("Reason"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Reason));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (FCode.equals("RgtFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RgtFlag));
		}
		if (FCode.equals("HandleFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HandleFlag));
		}
		if (FCode.equals("InputFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InputFlag));
		}
		if (FCode.equals("DispatchRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DispatchRate));
		}
		if (FCode.equals("PrepaidFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrepaidFlag));
		}
		if (FCode.equals("PrepaidUpUserCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrepaidUpUserCode));
		}
		if (FCode.equals("PrepaidLimit"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrepaidLimit));
		}
		if (FCode.equals("SocialMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SocialMoney));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("PrepaidMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrepaidMoney));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(UserName);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(ComCode);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(UpUserCode);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(ClaimDeal);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(ClaimQuery);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(ReportFlag);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(RegisterFlag);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(ClaimFlag);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(UserCode);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(ClaimPopedom);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(CheckFlag);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(UWFlag);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(SurveyFlag);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(PayFlag);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(StateFlag);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(Reason);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(RgtFlag);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(HandleFlag);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(InputFlag);
				break;
			case 21:
				strFieldValue = String.valueOf(DispatchRate);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(PrepaidFlag);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(PrepaidUpUserCode);
				break;
			case 24:
				strFieldValue = String.valueOf(PrepaidLimit);
				break;
			case 25:
				strFieldValue = String.valueOf(SocialMoney);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 31:
				strFieldValue = String.valueOf(PrepaidMoney);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("UserName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UserName = FValue.trim();
			}
			else
				UserName = null;
		}
		if (FCode.equalsIgnoreCase("ComCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ComCode = FValue.trim();
			}
			else
				ComCode = null;
		}
		if (FCode.equalsIgnoreCase("UpUserCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UpUserCode = FValue.trim();
			}
			else
				UpUserCode = null;
		}
		if (FCode.equalsIgnoreCase("ClaimDeal"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ClaimDeal = FValue.trim();
			}
			else
				ClaimDeal = null;
		}
		if (FCode.equalsIgnoreCase("ClaimQuery"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ClaimQuery = FValue.trim();
			}
			else
				ClaimQuery = null;
		}
		if (FCode.equalsIgnoreCase("ReportFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReportFlag = FValue.trim();
			}
			else
				ReportFlag = null;
		}
		if (FCode.equalsIgnoreCase("RegisterFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RegisterFlag = FValue.trim();
			}
			else
				RegisterFlag = null;
		}
		if (FCode.equalsIgnoreCase("ClaimFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ClaimFlag = FValue.trim();
			}
			else
				ClaimFlag = null;
		}
		if (FCode.equalsIgnoreCase("UserCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UserCode = FValue.trim();
			}
			else
				UserCode = null;
		}
		if (FCode.equalsIgnoreCase("ClaimPopedom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ClaimPopedom = FValue.trim();
			}
			else
				ClaimPopedom = null;
		}
		if (FCode.equalsIgnoreCase("CheckFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CheckFlag = FValue.trim();
			}
			else
				CheckFlag = null;
		}
		if (FCode.equalsIgnoreCase("UWFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UWFlag = FValue.trim();
			}
			else
				UWFlag = null;
		}
		if (FCode.equalsIgnoreCase("SurveyFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SurveyFlag = FValue.trim();
			}
			else
				SurveyFlag = null;
		}
		if (FCode.equalsIgnoreCase("PayFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayFlag = FValue.trim();
			}
			else
				PayFlag = null;
		}
		if (FCode.equalsIgnoreCase("StateFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StateFlag = FValue.trim();
			}
			else
				StateFlag = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("Reason"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Reason = FValue.trim();
			}
			else
				Reason = null;
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		if (FCode.equalsIgnoreCase("RgtFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RgtFlag = FValue.trim();
			}
			else
				RgtFlag = null;
		}
		if (FCode.equalsIgnoreCase("HandleFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HandleFlag = FValue.trim();
			}
			else
				HandleFlag = null;
		}
		if (FCode.equalsIgnoreCase("InputFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InputFlag = FValue.trim();
			}
			else
				InputFlag = null;
		}
		if (FCode.equalsIgnoreCase("DispatchRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				DispatchRate = i;
			}
		}
		if (FCode.equalsIgnoreCase("PrepaidFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PrepaidFlag = FValue.trim();
			}
			else
				PrepaidFlag = null;
		}
		if (FCode.equalsIgnoreCase("PrepaidUpUserCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PrepaidUpUserCode = FValue.trim();
			}
			else
				PrepaidUpUserCode = null;
		}
		if (FCode.equalsIgnoreCase("PrepaidLimit"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				PrepaidLimit = d;
			}
		}
		if (FCode.equalsIgnoreCase("SocialMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SocialMoney = d;
			}
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("PrepaidMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				PrepaidMoney = d;
			}
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LLSocialClaimUserSchema other = (LLSocialClaimUserSchema)otherObject;
		return
			(UserName == null ? other.getUserName() == null : UserName.equals(other.getUserName()))
			&& (ComCode == null ? other.getComCode() == null : ComCode.equals(other.getComCode()))
			&& (UpUserCode == null ? other.getUpUserCode() == null : UpUserCode.equals(other.getUpUserCode()))
			&& (ClaimDeal == null ? other.getClaimDeal() == null : ClaimDeal.equals(other.getClaimDeal()))
			&& (ClaimQuery == null ? other.getClaimQuery() == null : ClaimQuery.equals(other.getClaimQuery()))
			&& (ReportFlag == null ? other.getReportFlag() == null : ReportFlag.equals(other.getReportFlag()))
			&& (RegisterFlag == null ? other.getRegisterFlag() == null : RegisterFlag.equals(other.getRegisterFlag()))
			&& (ClaimFlag == null ? other.getClaimFlag() == null : ClaimFlag.equals(other.getClaimFlag()))
			&& (UserCode == null ? other.getUserCode() == null : UserCode.equals(other.getUserCode()))
			&& (ClaimPopedom == null ? other.getClaimPopedom() == null : ClaimPopedom.equals(other.getClaimPopedom()))
			&& (CheckFlag == null ? other.getCheckFlag() == null : CheckFlag.equals(other.getCheckFlag()))
			&& (UWFlag == null ? other.getUWFlag() == null : UWFlag.equals(other.getUWFlag()))
			&& (SurveyFlag == null ? other.getSurveyFlag() == null : SurveyFlag.equals(other.getSurveyFlag()))
			&& (PayFlag == null ? other.getPayFlag() == null : PayFlag.equals(other.getPayFlag()))
			&& (StateFlag == null ? other.getStateFlag() == null : StateFlag.equals(other.getStateFlag()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (Reason == null ? other.getReason() == null : Reason.equals(other.getReason()))
			&& (Remark == null ? other.getRemark() == null : Remark.equals(other.getRemark()))
			&& (RgtFlag == null ? other.getRgtFlag() == null : RgtFlag.equals(other.getRgtFlag()))
			&& (HandleFlag == null ? other.getHandleFlag() == null : HandleFlag.equals(other.getHandleFlag()))
			&& (InputFlag == null ? other.getInputFlag() == null : InputFlag.equals(other.getInputFlag()))
			&& DispatchRate == other.getDispatchRate()
			&& (PrepaidFlag == null ? other.getPrepaidFlag() == null : PrepaidFlag.equals(other.getPrepaidFlag()))
			&& (PrepaidUpUserCode == null ? other.getPrepaidUpUserCode() == null : PrepaidUpUserCode.equals(other.getPrepaidUpUserCode()))
			&& PrepaidLimit == other.getPrepaidLimit()
			&& SocialMoney == other.getSocialMoney()
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& PrepaidMoney == other.getPrepaidMoney();
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("UserName") ) {
			return 0;
		}
		if( strFieldName.equals("ComCode") ) {
			return 1;
		}
		if( strFieldName.equals("UpUserCode") ) {
			return 2;
		}
		if( strFieldName.equals("ClaimDeal") ) {
			return 3;
		}
		if( strFieldName.equals("ClaimQuery") ) {
			return 4;
		}
		if( strFieldName.equals("ReportFlag") ) {
			return 5;
		}
		if( strFieldName.equals("RegisterFlag") ) {
			return 6;
		}
		if( strFieldName.equals("ClaimFlag") ) {
			return 7;
		}
		if( strFieldName.equals("UserCode") ) {
			return 8;
		}
		if( strFieldName.equals("ClaimPopedom") ) {
			return 9;
		}
		if( strFieldName.equals("CheckFlag") ) {
			return 10;
		}
		if( strFieldName.equals("UWFlag") ) {
			return 11;
		}
		if( strFieldName.equals("SurveyFlag") ) {
			return 12;
		}
		if( strFieldName.equals("PayFlag") ) {
			return 13;
		}
		if( strFieldName.equals("StateFlag") ) {
			return 14;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 15;
		}
		if( strFieldName.equals("Reason") ) {
			return 16;
		}
		if( strFieldName.equals("Remark") ) {
			return 17;
		}
		if( strFieldName.equals("RgtFlag") ) {
			return 18;
		}
		if( strFieldName.equals("HandleFlag") ) {
			return 19;
		}
		if( strFieldName.equals("InputFlag") ) {
			return 20;
		}
		if( strFieldName.equals("DispatchRate") ) {
			return 21;
		}
		if( strFieldName.equals("PrepaidFlag") ) {
			return 22;
		}
		if( strFieldName.equals("PrepaidUpUserCode") ) {
			return 23;
		}
		if( strFieldName.equals("PrepaidLimit") ) {
			return 24;
		}
		if( strFieldName.equals("SocialMoney") ) {
			return 25;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 26;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 27;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 28;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 29;
		}
		if( strFieldName.equals("Operator") ) {
			return 30;
		}
		if( strFieldName.equals("PrepaidMoney") ) {
			return 31;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "UserName";
				break;
			case 1:
				strFieldName = "ComCode";
				break;
			case 2:
				strFieldName = "UpUserCode";
				break;
			case 3:
				strFieldName = "ClaimDeal";
				break;
			case 4:
				strFieldName = "ClaimQuery";
				break;
			case 5:
				strFieldName = "ReportFlag";
				break;
			case 6:
				strFieldName = "RegisterFlag";
				break;
			case 7:
				strFieldName = "ClaimFlag";
				break;
			case 8:
				strFieldName = "UserCode";
				break;
			case 9:
				strFieldName = "ClaimPopedom";
				break;
			case 10:
				strFieldName = "CheckFlag";
				break;
			case 11:
				strFieldName = "UWFlag";
				break;
			case 12:
				strFieldName = "SurveyFlag";
				break;
			case 13:
				strFieldName = "PayFlag";
				break;
			case 14:
				strFieldName = "StateFlag";
				break;
			case 15:
				strFieldName = "ManageCom";
				break;
			case 16:
				strFieldName = "Reason";
				break;
			case 17:
				strFieldName = "Remark";
				break;
			case 18:
				strFieldName = "RgtFlag";
				break;
			case 19:
				strFieldName = "HandleFlag";
				break;
			case 20:
				strFieldName = "InputFlag";
				break;
			case 21:
				strFieldName = "DispatchRate";
				break;
			case 22:
				strFieldName = "PrepaidFlag";
				break;
			case 23:
				strFieldName = "PrepaidUpUserCode";
				break;
			case 24:
				strFieldName = "PrepaidLimit";
				break;
			case 25:
				strFieldName = "SocialMoney";
				break;
			case 26:
				strFieldName = "MakeDate";
				break;
			case 27:
				strFieldName = "MakeTime";
				break;
			case 28:
				strFieldName = "ModifyDate";
				break;
			case 29:
				strFieldName = "ModifyTime";
				break;
			case 30:
				strFieldName = "Operator";
				break;
			case 31:
				strFieldName = "PrepaidMoney";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("UserName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ComCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UpUserCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ClaimDeal") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ClaimQuery") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReportFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RegisterFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ClaimFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UserCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ClaimPopedom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CheckFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UWFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SurveyFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StateFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Reason") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RgtFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HandleFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InputFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DispatchRate") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("PrepaidFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PrepaidUpUserCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PrepaidLimit") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SocialMoney") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PrepaidMoney") ) {
			return Schema.TYPE_DOUBLE;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_INT;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 25:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 26:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
