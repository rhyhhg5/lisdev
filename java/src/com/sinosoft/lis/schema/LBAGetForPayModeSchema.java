/*
 * <p>ClassName: LBAGetForPayModeSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LBAGetForPayModeDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LBAGetForPayModeSchema implements Schema
{
    // @Field
    /** 实付号码 */
    private String ActuGetNo;
    /** 变更流水号 */
    private String ChangeSerialNo;
    /** 其它号码 */
    private String OtherNo;
    /** 其它号码类型 */
    private String OtherNoType;
    /** 交费方式 */
    private String PayMode;
    /** 投保人客户号码 */
    private String AppntNo;
    /** 总给付金额 */
    private double SumGetMoney;
    /** 销售渠道 */
    private String SaleChnl;
    /** 应付日期 */
    private Date ShouldDate;
    /** 财务到帐日期 */
    private Date EnterAccDate;
    /** 财务确认日期 */
    private Date ConfDate;
    /** 复核人编码 */
    private String ApproveCode;
    /** 复核日期 */
    private Date ApproveDate;
    /** 给付通知书号码 */
    private String GetNoticeNo;
    /** 银行编码 */
    private String BankCode;
    /** 银行帐号 */
    private String BankAccNo;
    /** 领取人 */
    private String Drawer;
    /** 领取人身份证号 */
    private String DrawerID;
    /** 流水号 */
    private String SerialNo;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 银行在途标志 */
    private String BankOnTheWayFlag;
    /** 银行转帐成功标记 */
    private String BankSuccFlag;
    /** 送银行次数 */
    private int SendBankCount;
    /** 管理机构 */
    private String ManageCom;
    /** 代理机构 */
    private String AgentCom;
    /** 代理机构内部分类 */
    private String AgentType;
    /** 代理人编码 */
    private String AgentCode;
    /** 代理人组别 */
    private String AgentGroup;
    /** 帐户名 */
    private String AccName;
    /** 最早付费日期 */
    private Date StartGetDate;

    public static final int FIELDNUM = 34; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LBAGetForPayModeSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "ActuGetNo";
        pk[1] = "ChangeSerialNo";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getActuGetNo()
    {
        if (ActuGetNo != null && !ActuGetNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ActuGetNo = StrTool.unicodeToGBK(ActuGetNo);
        }
        return ActuGetNo;
    }

    public void setActuGetNo(String aActuGetNo)
    {
        ActuGetNo = aActuGetNo;
    }

    public String getChangeSerialNo()
    {
        if (ChangeSerialNo != null && !ChangeSerialNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ChangeSerialNo = StrTool.unicodeToGBK(ChangeSerialNo);
        }
        return ChangeSerialNo;
    }

    public void setChangeSerialNo(String aChangeSerialNo)
    {
        ChangeSerialNo = aChangeSerialNo;
    }

    public String getOtherNo()
    {
        if (OtherNo != null && !OtherNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            OtherNo = StrTool.unicodeToGBK(OtherNo);
        }
        return OtherNo;
    }

    public void setOtherNo(String aOtherNo)
    {
        OtherNo = aOtherNo;
    }

    public String getOtherNoType()
    {
        if (OtherNoType != null && !OtherNoType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            OtherNoType = StrTool.unicodeToGBK(OtherNoType);
        }
        return OtherNoType;
    }

    public void setOtherNoType(String aOtherNoType)
    {
        OtherNoType = aOtherNoType;
    }

    public String getPayMode()
    {
        if (PayMode != null && !PayMode.equals("") && SysConst.CHANGECHARSET == true)
        {
            PayMode = StrTool.unicodeToGBK(PayMode);
        }
        return PayMode;
    }

    public void setPayMode(String aPayMode)
    {
        PayMode = aPayMode;
    }

    public String getAppntNo()
    {
        if (AppntNo != null && !AppntNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            AppntNo = StrTool.unicodeToGBK(AppntNo);
        }
        return AppntNo;
    }

    public void setAppntNo(String aAppntNo)
    {
        AppntNo = aAppntNo;
    }

    public double getSumGetMoney()
    {
        return SumGetMoney;
    }

    public void setSumGetMoney(double aSumGetMoney)
    {
        SumGetMoney = aSumGetMoney;
    }

    public void setSumGetMoney(String aSumGetMoney)
    {
        if (aSumGetMoney != null && !aSumGetMoney.equals(""))
        {
            Double tDouble = new Double(aSumGetMoney);
            double d = tDouble.doubleValue();
            SumGetMoney = d;
        }
    }

    public String getSaleChnl()
    {
        if (SaleChnl != null && !SaleChnl.equals("") && SysConst.CHANGECHARSET == true)
        {
            SaleChnl = StrTool.unicodeToGBK(SaleChnl);
        }
        return SaleChnl;
    }

    public void setSaleChnl(String aSaleChnl)
    {
        SaleChnl = aSaleChnl;
    }

    public String getShouldDate()
    {
        if (ShouldDate != null)
        {
            return fDate.getString(ShouldDate);
        }
        else
        {
            return null;
        }
    }

    public void setShouldDate(Date aShouldDate)
    {
        ShouldDate = aShouldDate;
    }

    public void setShouldDate(String aShouldDate)
    {
        if (aShouldDate != null && !aShouldDate.equals(""))
        {
            ShouldDate = fDate.getDate(aShouldDate);
        }
        else
        {
            ShouldDate = null;
        }
    }

    public String getEnterAccDate()
    {
        if (EnterAccDate != null)
        {
            return fDate.getString(EnterAccDate);
        }
        else
        {
            return null;
        }
    }

    public void setEnterAccDate(Date aEnterAccDate)
    {
        EnterAccDate = aEnterAccDate;
    }

    public void setEnterAccDate(String aEnterAccDate)
    {
        if (aEnterAccDate != null && !aEnterAccDate.equals(""))
        {
            EnterAccDate = fDate.getDate(aEnterAccDate);
        }
        else
        {
            EnterAccDate = null;
        }
    }

    public String getConfDate()
    {
        if (ConfDate != null)
        {
            return fDate.getString(ConfDate);
        }
        else
        {
            return null;
        }
    }

    public void setConfDate(Date aConfDate)
    {
        ConfDate = aConfDate;
    }

    public void setConfDate(String aConfDate)
    {
        if (aConfDate != null && !aConfDate.equals(""))
        {
            ConfDate = fDate.getDate(aConfDate);
        }
        else
        {
            ConfDate = null;
        }
    }

    public String getApproveCode()
    {
        if (ApproveCode != null && !ApproveCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ApproveCode = StrTool.unicodeToGBK(ApproveCode);
        }
        return ApproveCode;
    }

    public void setApproveCode(String aApproveCode)
    {
        ApproveCode = aApproveCode;
    }

    public String getApproveDate()
    {
        if (ApproveDate != null)
        {
            return fDate.getString(ApproveDate);
        }
        else
        {
            return null;
        }
    }

    public void setApproveDate(Date aApproveDate)
    {
        ApproveDate = aApproveDate;
    }

    public void setApproveDate(String aApproveDate)
    {
        if (aApproveDate != null && !aApproveDate.equals(""))
        {
            ApproveDate = fDate.getDate(aApproveDate);
        }
        else
        {
            ApproveDate = null;
        }
    }

    public String getGetNoticeNo()
    {
        if (GetNoticeNo != null && !GetNoticeNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GetNoticeNo = StrTool.unicodeToGBK(GetNoticeNo);
        }
        return GetNoticeNo;
    }

    public void setGetNoticeNo(String aGetNoticeNo)
    {
        GetNoticeNo = aGetNoticeNo;
    }

    public String getBankCode()
    {
        if (BankCode != null && !BankCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            BankCode = StrTool.unicodeToGBK(BankCode);
        }
        return BankCode;
    }

    public void setBankCode(String aBankCode)
    {
        BankCode = aBankCode;
    }

    public String getBankAccNo()
    {
        if (BankAccNo != null && !BankAccNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            BankAccNo = StrTool.unicodeToGBK(BankAccNo);
        }
        return BankAccNo;
    }

    public void setBankAccNo(String aBankAccNo)
    {
        BankAccNo = aBankAccNo;
    }

    public String getDrawer()
    {
        if (Drawer != null && !Drawer.equals("") && SysConst.CHANGECHARSET == true)
        {
            Drawer = StrTool.unicodeToGBK(Drawer);
        }
        return Drawer;
    }

    public void setDrawer(String aDrawer)
    {
        Drawer = aDrawer;
    }

    public String getDrawerID()
    {
        if (DrawerID != null && !DrawerID.equals("") && SysConst.CHANGECHARSET == true)
        {
            DrawerID = StrTool.unicodeToGBK(DrawerID);
        }
        return DrawerID;
    }

    public void setDrawerID(String aDrawerID)
    {
        DrawerID = aDrawerID;
    }

    public String getSerialNo()
    {
        if (SerialNo != null && !SerialNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            SerialNo = StrTool.unicodeToGBK(SerialNo);
        }
        return SerialNo;
    }

    public void setSerialNo(String aSerialNo)
    {
        SerialNo = aSerialNo;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    public String getBankOnTheWayFlag()
    {
        if (BankOnTheWayFlag != null && !BankOnTheWayFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            BankOnTheWayFlag = StrTool.unicodeToGBK(BankOnTheWayFlag);
        }
        return BankOnTheWayFlag;
    }

    public void setBankOnTheWayFlag(String aBankOnTheWayFlag)
    {
        BankOnTheWayFlag = aBankOnTheWayFlag;
    }

    public String getBankSuccFlag()
    {
        if (BankSuccFlag != null && !BankSuccFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            BankSuccFlag = StrTool.unicodeToGBK(BankSuccFlag);
        }
        return BankSuccFlag;
    }

    public void setBankSuccFlag(String aBankSuccFlag)
    {
        BankSuccFlag = aBankSuccFlag;
    }

    public int getSendBankCount()
    {
        return SendBankCount;
    }

    public void setSendBankCount(int aSendBankCount)
    {
        SendBankCount = aSendBankCount;
    }

    public void setSendBankCount(String aSendBankCount)
    {
        if (aSendBankCount != null && !aSendBankCount.equals(""))
        {
            Integer tInteger = new Integer(aSendBankCount);
            int i = tInteger.intValue();
            SendBankCount = i;
        }
    }

    public String getManageCom()
    {
        if (ManageCom != null && !ManageCom.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ManageCom = StrTool.unicodeToGBK(ManageCom);
        }
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public String getAgentCom()
    {
        if (AgentCom != null && !AgentCom.equals("") && SysConst.CHANGECHARSET == true)
        {
            AgentCom = StrTool.unicodeToGBK(AgentCom);
        }
        return AgentCom;
    }

    public void setAgentCom(String aAgentCom)
    {
        AgentCom = aAgentCom;
    }

    public String getAgentType()
    {
        if (AgentType != null && !AgentType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AgentType = StrTool.unicodeToGBK(AgentType);
        }
        return AgentType;
    }

    public void setAgentType(String aAgentType)
    {
        AgentType = aAgentType;
    }

    public String getAgentCode()
    {
        if (AgentCode != null && !AgentCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AgentCode = StrTool.unicodeToGBK(AgentCode);
        }
        return AgentCode;
    }

    public void setAgentCode(String aAgentCode)
    {
        AgentCode = aAgentCode;
    }

    public String getAgentGroup()
    {
        if (AgentGroup != null && !AgentGroup.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AgentGroup = StrTool.unicodeToGBK(AgentGroup);
        }
        return AgentGroup;
    }

    public void setAgentGroup(String aAgentGroup)
    {
        AgentGroup = aAgentGroup;
    }

    public String getAccName()
    {
        if (AccName != null && !AccName.equals("") && SysConst.CHANGECHARSET == true)
        {
            AccName = StrTool.unicodeToGBK(AccName);
        }
        return AccName;
    }

    public void setAccName(String aAccName)
    {
        AccName = aAccName;
    }

    public String getStartGetDate()
    {
        if (StartGetDate != null)
        {
            return fDate.getString(StartGetDate);
        }
        else
        {
            return null;
        }
    }

    public void setStartGetDate(Date aStartGetDate)
    {
        StartGetDate = aStartGetDate;
    }

    public void setStartGetDate(String aStartGetDate)
    {
        if (aStartGetDate != null && !aStartGetDate.equals(""))
        {
            StartGetDate = fDate.getDate(aStartGetDate);
        }
        else
        {
            StartGetDate = null;
        }
    }


    /**
     * 使用另外一个 LBAGetForPayModeSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LBAGetForPayModeSchema aLBAGetForPayModeSchema)
    {
        this.ActuGetNo = aLBAGetForPayModeSchema.getActuGetNo();
        this.ChangeSerialNo = aLBAGetForPayModeSchema.getChangeSerialNo();
        this.OtherNo = aLBAGetForPayModeSchema.getOtherNo();
        this.OtherNoType = aLBAGetForPayModeSchema.getOtherNoType();
        this.PayMode = aLBAGetForPayModeSchema.getPayMode();
        this.AppntNo = aLBAGetForPayModeSchema.getAppntNo();
        this.SumGetMoney = aLBAGetForPayModeSchema.getSumGetMoney();
        this.SaleChnl = aLBAGetForPayModeSchema.getSaleChnl();
        this.ShouldDate = fDate.getDate(aLBAGetForPayModeSchema.getShouldDate());
        this.EnterAccDate = fDate.getDate(aLBAGetForPayModeSchema.
                                          getEnterAccDate());
        this.ConfDate = fDate.getDate(aLBAGetForPayModeSchema.getConfDate());
        this.ApproveCode = aLBAGetForPayModeSchema.getApproveCode();
        this.ApproveDate = fDate.getDate(aLBAGetForPayModeSchema.getApproveDate());
        this.GetNoticeNo = aLBAGetForPayModeSchema.getGetNoticeNo();
        this.BankCode = aLBAGetForPayModeSchema.getBankCode();
        this.BankAccNo = aLBAGetForPayModeSchema.getBankAccNo();
        this.Drawer = aLBAGetForPayModeSchema.getDrawer();
        this.DrawerID = aLBAGetForPayModeSchema.getDrawerID();
        this.SerialNo = aLBAGetForPayModeSchema.getSerialNo();
        this.Operator = aLBAGetForPayModeSchema.getOperator();
        this.MakeDate = fDate.getDate(aLBAGetForPayModeSchema.getMakeDate());
        this.MakeTime = aLBAGetForPayModeSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLBAGetForPayModeSchema.getModifyDate());
        this.ModifyTime = aLBAGetForPayModeSchema.getModifyTime();
        this.BankOnTheWayFlag = aLBAGetForPayModeSchema.getBankOnTheWayFlag();
        this.BankSuccFlag = aLBAGetForPayModeSchema.getBankSuccFlag();
        this.SendBankCount = aLBAGetForPayModeSchema.getSendBankCount();
        this.ManageCom = aLBAGetForPayModeSchema.getManageCom();
        this.AgentCom = aLBAGetForPayModeSchema.getAgentCom();
        this.AgentType = aLBAGetForPayModeSchema.getAgentType();
        this.AgentCode = aLBAGetForPayModeSchema.getAgentCode();
        this.AgentGroup = aLBAGetForPayModeSchema.getAgentGroup();
        this.AccName = aLBAGetForPayModeSchema.getAccName();
        this.StartGetDate = fDate.getDate(aLBAGetForPayModeSchema.
                                          getStartGetDate());
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ActuGetNo") == null)
            {
                this.ActuGetNo = null;
            }
            else
            {
                this.ActuGetNo = rs.getString("ActuGetNo").trim();
            }

            if (rs.getString("ChangeSerialNo") == null)
            {
                this.ChangeSerialNo = null;
            }
            else
            {
                this.ChangeSerialNo = rs.getString("ChangeSerialNo").trim();
            }

            if (rs.getString("OtherNo") == null)
            {
                this.OtherNo = null;
            }
            else
            {
                this.OtherNo = rs.getString("OtherNo").trim();
            }

            if (rs.getString("OtherNoType") == null)
            {
                this.OtherNoType = null;
            }
            else
            {
                this.OtherNoType = rs.getString("OtherNoType").trim();
            }

            if (rs.getString("PayMode") == null)
            {
                this.PayMode = null;
            }
            else
            {
                this.PayMode = rs.getString("PayMode").trim();
            }

            if (rs.getString("AppntNo") == null)
            {
                this.AppntNo = null;
            }
            else
            {
                this.AppntNo = rs.getString("AppntNo").trim();
            }

            this.SumGetMoney = rs.getDouble("SumGetMoney");
            if (rs.getString("SaleChnl") == null)
            {
                this.SaleChnl = null;
            }
            else
            {
                this.SaleChnl = rs.getString("SaleChnl").trim();
            }

            this.ShouldDate = rs.getDate("ShouldDate");
            this.EnterAccDate = rs.getDate("EnterAccDate");
            this.ConfDate = rs.getDate("ConfDate");
            if (rs.getString("ApproveCode") == null)
            {
                this.ApproveCode = null;
            }
            else
            {
                this.ApproveCode = rs.getString("ApproveCode").trim();
            }

            this.ApproveDate = rs.getDate("ApproveDate");
            if (rs.getString("GetNoticeNo") == null)
            {
                this.GetNoticeNo = null;
            }
            else
            {
                this.GetNoticeNo = rs.getString("GetNoticeNo").trim();
            }

            if (rs.getString("BankCode") == null)
            {
                this.BankCode = null;
            }
            else
            {
                this.BankCode = rs.getString("BankCode").trim();
            }

            if (rs.getString("BankAccNo") == null)
            {
                this.BankAccNo = null;
            }
            else
            {
                this.BankAccNo = rs.getString("BankAccNo").trim();
            }

            if (rs.getString("Drawer") == null)
            {
                this.Drawer = null;
            }
            else
            {
                this.Drawer = rs.getString("Drawer").trim();
            }

            if (rs.getString("DrawerID") == null)
            {
                this.DrawerID = null;
            }
            else
            {
                this.DrawerID = rs.getString("DrawerID").trim();
            }

            if (rs.getString("SerialNo") == null)
            {
                this.SerialNo = null;
            }
            else
            {
                this.SerialNo = rs.getString("SerialNo").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("BankOnTheWayFlag") == null)
            {
                this.BankOnTheWayFlag = null;
            }
            else
            {
                this.BankOnTheWayFlag = rs.getString("BankOnTheWayFlag").trim();
            }

            if (rs.getString("BankSuccFlag") == null)
            {
                this.BankSuccFlag = null;
            }
            else
            {
                this.BankSuccFlag = rs.getString("BankSuccFlag").trim();
            }

            this.SendBankCount = rs.getInt("SendBankCount");
            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("AgentCom") == null)
            {
                this.AgentCom = null;
            }
            else
            {
                this.AgentCom = rs.getString("AgentCom").trim();
            }

            if (rs.getString("AgentType") == null)
            {
                this.AgentType = null;
            }
            else
            {
                this.AgentType = rs.getString("AgentType").trim();
            }

            if (rs.getString("AgentCode") == null)
            {
                this.AgentCode = null;
            }
            else
            {
                this.AgentCode = rs.getString("AgentCode").trim();
            }

            if (rs.getString("AgentGroup") == null)
            {
                this.AgentGroup = null;
            }
            else
            {
                this.AgentGroup = rs.getString("AgentGroup").trim();
            }

            if (rs.getString("AccName") == null)
            {
                this.AccName = null;
            }
            else
            {
                this.AccName = rs.getString("AccName").trim();
            }

            this.StartGetDate = rs.getDate("StartGetDate");
        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBAGetForPayModeSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LBAGetForPayModeSchema getSchema()
    {
        LBAGetForPayModeSchema aLBAGetForPayModeSchema = new
                LBAGetForPayModeSchema();
        aLBAGetForPayModeSchema.setSchema(this);
        return aLBAGetForPayModeSchema;
    }

    public LBAGetForPayModeDB getDB()
    {
        LBAGetForPayModeDB aDBOper = new LBAGetForPayModeDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLBAGetForPayMode描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(ActuGetNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ChangeSerialNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(OtherNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(OtherNoType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PayMode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AppntNo)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(SumGetMoney) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SaleChnl)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ShouldDate))) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            EnterAccDate))) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(ConfDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ApproveCode)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ApproveDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GetNoticeNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(BankCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(BankAccNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Drawer)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(DrawerID)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SerialNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(BankOnTheWayFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(BankSuccFlag)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(SendBankCount) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ManageCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgentCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgentType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgentCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgentGroup)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AccName)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            StartGetDate)));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLBAGetForPayMode>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            ActuGetNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            ChangeSerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                            SysConst.PACKAGESPILTER);
            OtherNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                     SysConst.PACKAGESPILTER);
            OtherNoType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                         SysConst.PACKAGESPILTER);
            PayMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                     SysConst.PACKAGESPILTER);
            AppntNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                     SysConst.PACKAGESPILTER);
            SumGetMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 7, SysConst.PACKAGESPILTER))).doubleValue();
            SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                      SysConst.PACKAGESPILTER);
            ShouldDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 9, SysConst.PACKAGESPILTER));
            EnterAccDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 10, SysConst.PACKAGESPILTER));
            ConfDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 11, SysConst.PACKAGESPILTER));
            ApproveCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                         SysConst.PACKAGESPILTER);
            ApproveDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 13, SysConst.PACKAGESPILTER));
            GetNoticeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                         SysConst.PACKAGESPILTER);
            BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                      SysConst.PACKAGESPILTER);
            BankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                       SysConst.PACKAGESPILTER);
            Drawer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                    SysConst.PACKAGESPILTER);
            DrawerID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                      SysConst.PACKAGESPILTER);
            SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                      SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 21, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 23, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,
                                        SysConst.PACKAGESPILTER);
            BankOnTheWayFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                              25, SysConst.PACKAGESPILTER);
            BankSuccFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26,
                                          SysConst.PACKAGESPILTER);
            SendBankCount = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 27, SysConst.PACKAGESPILTER))).intValue();
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28,
                                       SysConst.PACKAGESPILTER);
            AgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29,
                                      SysConst.PACKAGESPILTER);
            AgentType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30,
                                       SysConst.PACKAGESPILTER);
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31,
                                       SysConst.PACKAGESPILTER);
            AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32,
                                        SysConst.PACKAGESPILTER);
            AccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33,
                                     SysConst.PACKAGESPILTER);
            StartGetDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 34, SysConst.PACKAGESPILTER));
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBAGetForPayModeSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("ActuGetNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ActuGetNo));
        }
        if (FCode.equals("ChangeSerialNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ChangeSerialNo));
        }
        if (FCode.equals("OtherNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(OtherNo));
        }
        if (FCode.equals("OtherNoType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(OtherNoType));
        }
        if (FCode.equals("PayMode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PayMode));
        }
        if (FCode.equals("AppntNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AppntNo));
        }
        if (FCode.equals("SumGetMoney"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SumGetMoney));
        }
        if (FCode.equals("SaleChnl"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SaleChnl));
        }
        if (FCode.equals("ShouldDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getShouldDate()));
        }
        if (FCode.equals("EnterAccDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getEnterAccDate()));
        }
        if (FCode.equals("ConfDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getConfDate()));
        }
        if (FCode.equals("ApproveCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ApproveCode));
        }
        if (FCode.equals("ApproveDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getApproveDate()));
        }
        if (FCode.equals("GetNoticeNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetNoticeNo));
        }
        if (FCode.equals("BankCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(BankCode));
        }
        if (FCode.equals("BankAccNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(BankAccNo));
        }
        if (FCode.equals("Drawer"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Drawer));
        }
        if (FCode.equals("DrawerID"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DrawerID));
        }
        if (FCode.equals("SerialNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SerialNo));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (FCode.equals("BankOnTheWayFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(BankOnTheWayFlag));
        }
        if (FCode.equals("BankSuccFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(BankSuccFlag));
        }
        if (FCode.equals("SendBankCount"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SendBankCount));
        }
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ManageCom));
        }
        if (FCode.equals("AgentCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AgentCom));
        }
        if (FCode.equals("AgentType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AgentType));
        }
        if (FCode.equals("AgentCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AgentCode));
        }
        if (FCode.equals("AgentGroup"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AgentGroup));
        }
        if (FCode.equals("AccName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AccName));
        }
        if (FCode.equals("StartGetDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getStartGetDate()));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ActuGetNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ChangeSerialNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(OtherNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(OtherNoType);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(PayMode);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(AppntNo);
                break;
            case 6:
                strFieldValue = String.valueOf(SumGetMoney);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(SaleChnl);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getShouldDate()));
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getEnterAccDate()));
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getConfDate()));
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(ApproveCode);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getApproveDate()));
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(GetNoticeNo);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(BankCode);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(BankAccNo);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(Drawer);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(DrawerID);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(SerialNo);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(BankOnTheWayFlag);
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(BankSuccFlag);
                break;
            case 26:
                strFieldValue = String.valueOf(SendBankCount);
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(AgentCom);
                break;
            case 29:
                strFieldValue = StrTool.GBKToUnicode(AgentType);
                break;
            case 30:
                strFieldValue = StrTool.GBKToUnicode(AgentCode);
                break;
            case 31:
                strFieldValue = StrTool.GBKToUnicode(AgentGroup);
                break;
            case 32:
                strFieldValue = StrTool.GBKToUnicode(AccName);
                break;
            case 33:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getStartGetDate()));
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("ActuGetNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ActuGetNo = FValue.trim();
            }
            else
            {
                ActuGetNo = null;
            }
        }
        if (FCode.equals("ChangeSerialNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ChangeSerialNo = FValue.trim();
            }
            else
            {
                ChangeSerialNo = null;
            }
        }
        if (FCode.equals("OtherNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OtherNo = FValue.trim();
            }
            else
            {
                OtherNo = null;
            }
        }
        if (FCode.equals("OtherNoType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OtherNoType = FValue.trim();
            }
            else
            {
                OtherNoType = null;
            }
        }
        if (FCode.equals("PayMode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PayMode = FValue.trim();
            }
            else
            {
                PayMode = null;
            }
        }
        if (FCode.equals("AppntNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppntNo = FValue.trim();
            }
            else
            {
                AppntNo = null;
            }
        }
        if (FCode.equals("SumGetMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                SumGetMoney = d;
            }
        }
        if (FCode.equals("SaleChnl"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SaleChnl = FValue.trim();
            }
            else
            {
                SaleChnl = null;
            }
        }
        if (FCode.equals("ShouldDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ShouldDate = fDate.getDate(FValue);
            }
            else
            {
                ShouldDate = null;
            }
        }
        if (FCode.equals("EnterAccDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EnterAccDate = fDate.getDate(FValue);
            }
            else
            {
                EnterAccDate = null;
            }
        }
        if (FCode.equals("ConfDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ConfDate = fDate.getDate(FValue);
            }
            else
            {
                ConfDate = null;
            }
        }
        if (FCode.equals("ApproveCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ApproveCode = FValue.trim();
            }
            else
            {
                ApproveCode = null;
            }
        }
        if (FCode.equals("ApproveDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ApproveDate = fDate.getDate(FValue);
            }
            else
            {
                ApproveDate = null;
            }
        }
        if (FCode.equals("GetNoticeNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetNoticeNo = FValue.trim();
            }
            else
            {
                GetNoticeNo = null;
            }
        }
        if (FCode.equals("BankCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BankCode = FValue.trim();
            }
            else
            {
                BankCode = null;
            }
        }
        if (FCode.equals("BankAccNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BankAccNo = FValue.trim();
            }
            else
            {
                BankAccNo = null;
            }
        }
        if (FCode.equals("Drawer"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Drawer = FValue.trim();
            }
            else
            {
                Drawer = null;
            }
        }
        if (FCode.equals("DrawerID"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DrawerID = FValue.trim();
            }
            else
            {
                DrawerID = null;
            }
        }
        if (FCode.equals("SerialNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
            {
                SerialNo = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        if (FCode.equals("BankOnTheWayFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BankOnTheWayFlag = FValue.trim();
            }
            else
            {
                BankOnTheWayFlag = null;
            }
        }
        if (FCode.equals("BankSuccFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BankSuccFlag = FValue.trim();
            }
            else
            {
                BankSuccFlag = null;
            }
        }
        if (FCode.equals("SendBankCount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                SendBankCount = i;
            }
        }
        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("AgentCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentCom = FValue.trim();
            }
            else
            {
                AgentCom = null;
            }
        }
        if (FCode.equals("AgentType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentType = FValue.trim();
            }
            else
            {
                AgentType = null;
            }
        }
        if (FCode.equals("AgentCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
            {
                AgentCode = null;
            }
        }
        if (FCode.equals("AgentGroup"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
            {
                AgentGroup = null;
            }
        }
        if (FCode.equals("AccName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AccName = FValue.trim();
            }
            else
            {
                AccName = null;
            }
        }
        if (FCode.equals("StartGetDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                StartGetDate = fDate.getDate(FValue);
            }
            else
            {
                StartGetDate = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LBAGetForPayModeSchema other = (LBAGetForPayModeSchema) otherObject;
        return
                ActuGetNo.equals(other.getActuGetNo())
                && ChangeSerialNo.equals(other.getChangeSerialNo())
                && OtherNo.equals(other.getOtherNo())
                && OtherNoType.equals(other.getOtherNoType())
                && PayMode.equals(other.getPayMode())
                && AppntNo.equals(other.getAppntNo())
                && SumGetMoney == other.getSumGetMoney()
                && SaleChnl.equals(other.getSaleChnl())
                && fDate.getString(ShouldDate).equals(other.getShouldDate())
                && fDate.getString(EnterAccDate).equals(other.getEnterAccDate())
                && fDate.getString(ConfDate).equals(other.getConfDate())
                && ApproveCode.equals(other.getApproveCode())
                && fDate.getString(ApproveDate).equals(other.getApproveDate())
                && GetNoticeNo.equals(other.getGetNoticeNo())
                && BankCode.equals(other.getBankCode())
                && BankAccNo.equals(other.getBankAccNo())
                && Drawer.equals(other.getDrawer())
                && DrawerID.equals(other.getDrawerID())
                && SerialNo.equals(other.getSerialNo())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && BankOnTheWayFlag.equals(other.getBankOnTheWayFlag())
                && BankSuccFlag.equals(other.getBankSuccFlag())
                && SendBankCount == other.getSendBankCount()
                && ManageCom.equals(other.getManageCom())
                && AgentCom.equals(other.getAgentCom())
                && AgentType.equals(other.getAgentType())
                && AgentCode.equals(other.getAgentCode())
                && AgentGroup.equals(other.getAgentGroup())
                && AccName.equals(other.getAccName())
                && fDate.getString(StartGetDate).equals(other.getStartGetDate());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("ActuGetNo"))
        {
            return 0;
        }
        if (strFieldName.equals("ChangeSerialNo"))
        {
            return 1;
        }
        if (strFieldName.equals("OtherNo"))
        {
            return 2;
        }
        if (strFieldName.equals("OtherNoType"))
        {
            return 3;
        }
        if (strFieldName.equals("PayMode"))
        {
            return 4;
        }
        if (strFieldName.equals("AppntNo"))
        {
            return 5;
        }
        if (strFieldName.equals("SumGetMoney"))
        {
            return 6;
        }
        if (strFieldName.equals("SaleChnl"))
        {
            return 7;
        }
        if (strFieldName.equals("ShouldDate"))
        {
            return 8;
        }
        if (strFieldName.equals("EnterAccDate"))
        {
            return 9;
        }
        if (strFieldName.equals("ConfDate"))
        {
            return 10;
        }
        if (strFieldName.equals("ApproveCode"))
        {
            return 11;
        }
        if (strFieldName.equals("ApproveDate"))
        {
            return 12;
        }
        if (strFieldName.equals("GetNoticeNo"))
        {
            return 13;
        }
        if (strFieldName.equals("BankCode"))
        {
            return 14;
        }
        if (strFieldName.equals("BankAccNo"))
        {
            return 15;
        }
        if (strFieldName.equals("Drawer"))
        {
            return 16;
        }
        if (strFieldName.equals("DrawerID"))
        {
            return 17;
        }
        if (strFieldName.equals("SerialNo"))
        {
            return 18;
        }
        if (strFieldName.equals("Operator"))
        {
            return 19;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 20;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 21;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 22;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 23;
        }
        if (strFieldName.equals("BankOnTheWayFlag"))
        {
            return 24;
        }
        if (strFieldName.equals("BankSuccFlag"))
        {
            return 25;
        }
        if (strFieldName.equals("SendBankCount"))
        {
            return 26;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return 27;
        }
        if (strFieldName.equals("AgentCom"))
        {
            return 28;
        }
        if (strFieldName.equals("AgentType"))
        {
            return 29;
        }
        if (strFieldName.equals("AgentCode"))
        {
            return 30;
        }
        if (strFieldName.equals("AgentGroup"))
        {
            return 31;
        }
        if (strFieldName.equals("AccName"))
        {
            return 32;
        }
        if (strFieldName.equals("StartGetDate"))
        {
            return 33;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "ActuGetNo";
                break;
            case 1:
                strFieldName = "ChangeSerialNo";
                break;
            case 2:
                strFieldName = "OtherNo";
                break;
            case 3:
                strFieldName = "OtherNoType";
                break;
            case 4:
                strFieldName = "PayMode";
                break;
            case 5:
                strFieldName = "AppntNo";
                break;
            case 6:
                strFieldName = "SumGetMoney";
                break;
            case 7:
                strFieldName = "SaleChnl";
                break;
            case 8:
                strFieldName = "ShouldDate";
                break;
            case 9:
                strFieldName = "EnterAccDate";
                break;
            case 10:
                strFieldName = "ConfDate";
                break;
            case 11:
                strFieldName = "ApproveCode";
                break;
            case 12:
                strFieldName = "ApproveDate";
                break;
            case 13:
                strFieldName = "GetNoticeNo";
                break;
            case 14:
                strFieldName = "BankCode";
                break;
            case 15:
                strFieldName = "BankAccNo";
                break;
            case 16:
                strFieldName = "Drawer";
                break;
            case 17:
                strFieldName = "DrawerID";
                break;
            case 18:
                strFieldName = "SerialNo";
                break;
            case 19:
                strFieldName = "Operator";
                break;
            case 20:
                strFieldName = "MakeDate";
                break;
            case 21:
                strFieldName = "MakeTime";
                break;
            case 22:
                strFieldName = "ModifyDate";
                break;
            case 23:
                strFieldName = "ModifyTime";
                break;
            case 24:
                strFieldName = "BankOnTheWayFlag";
                break;
            case 25:
                strFieldName = "BankSuccFlag";
                break;
            case 26:
                strFieldName = "SendBankCount";
                break;
            case 27:
                strFieldName = "ManageCom";
                break;
            case 28:
                strFieldName = "AgentCom";
                break;
            case 29:
                strFieldName = "AgentType";
                break;
            case 30:
                strFieldName = "AgentCode";
                break;
            case 31:
                strFieldName = "AgentGroup";
                break;
            case 32:
                strFieldName = "AccName";
                break;
            case 33:
                strFieldName = "StartGetDate";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("ActuGetNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ChangeSerialNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OtherNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OtherNoType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PayMode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppntNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SumGetMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("SaleChnl"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ShouldDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("EnterAccDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ConfDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ApproveCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ApproveDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("GetNoticeNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BankCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BankAccNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Drawer"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DrawerID"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SerialNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BankOnTheWayFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BankSuccFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SendBankCount"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentGroup"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AccName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StartGetDate"))
        {
            return Schema.TYPE_DATE;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 9:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 10:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 17:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 18:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 19:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 20:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 21:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 22:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 23:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 24:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 25:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 26:
                nFieldType = Schema.TYPE_INT;
                break;
            case 27:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 28:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 29:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 30:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 31:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 32:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 33:
                nFieldType = Schema.TYPE_DATE;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
