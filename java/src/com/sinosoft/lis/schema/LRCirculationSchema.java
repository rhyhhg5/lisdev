/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LRCirculationDB;

/*
 * <p>ClassName: LRCirculationSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2012-01-11
 */
public class LRCirculationSchema implements Schema, Cloneable
{
	// @Field
	/** 再保合同号 */
	private String ReContCode;
	/** 产品代码 */
	private String RiskCode;
	/** 账单月份 */
	private String AccMonth;
	/** 成本中心编码 */
	private String CostCenter;
	/** 再保公司名称 */
	private String ReComName;
	/** 管理机构 */
	private String ManageCom;
	/** 分出保额 */
	private double CessionAmount;
	/** 分出保费(退保金额) */
	private double CessPrem;
	/** 分保摊回赔款 */
	private double ClaimBackFee;
	/** 备用字段1 */
	private String Standby1;
	/** 备用字段2 */
	private String Standby2;
	/** 备用字段3 */
	private String Standby3;
	/** 备用字段4 */
	private String Standby4;
	/** 操作人 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 修改日期 */
	private Date ModifyDate;
	/** 修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 18;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LRCirculationSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[5];
		pk[0] = "ReContCode";
		pk[1] = "RiskCode";
		pk[2] = "AccMonth";
		pk[3] = "CostCenter";
		pk[4] = "ManageCom";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LRCirculationSchema cloned = (LRCirculationSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getReContCode()
	{
		return ReContCode;
	}
	public void setReContCode(String aReContCode)
	{
		ReContCode = aReContCode;
	}
	public String getRiskCode()
	{
		return RiskCode;
	}
	public void setRiskCode(String aRiskCode)
	{
		RiskCode = aRiskCode;
	}
	public String getAccMonth()
	{
		return AccMonth;
	}
	public void setAccMonth(String aAccMonth)
	{
		AccMonth = aAccMonth;
	}
	public String getCostCenter()
	{
		return CostCenter;
	}
	public void setCostCenter(String aCostCenter)
	{
		CostCenter = aCostCenter;
	}
	public String getReComName()
	{
		return ReComName;
	}
	public void setReComName(String aReComName)
	{
		ReComName = aReComName;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public double getCessionAmount()
	{
		return CessionAmount;
	}
	public void setCessionAmount(double aCessionAmount)
	{
		CessionAmount = Arith.round(aCessionAmount,2);
	}
	public void setCessionAmount(String aCessionAmount)
	{
		if (aCessionAmount != null && !aCessionAmount.equals(""))
		{
			Double tDouble = new Double(aCessionAmount);
			double d = tDouble.doubleValue();
                CessionAmount = Arith.round(d,2);
		}
	}

	public double getCessPrem()
	{
		return CessPrem;
	}
	public void setCessPrem(double aCessPrem)
	{
		CessPrem = Arith.round(aCessPrem,2);
	}
	public void setCessPrem(String aCessPrem)
	{
		if (aCessPrem != null && !aCessPrem.equals(""))
		{
			Double tDouble = new Double(aCessPrem);
			double d = tDouble.doubleValue();
                CessPrem = Arith.round(d,2);
		}
	}

	public double getClaimBackFee()
	{
		return ClaimBackFee;
	}
	public void setClaimBackFee(double aClaimBackFee)
	{
		ClaimBackFee = Arith.round(aClaimBackFee,2);
	}
	public void setClaimBackFee(String aClaimBackFee)
	{
		if (aClaimBackFee != null && !aClaimBackFee.equals(""))
		{
			Double tDouble = new Double(aClaimBackFee);
			double d = tDouble.doubleValue();
                ClaimBackFee = Arith.round(d,2);
		}
	}

	public String getStandby1()
	{
		return Standby1;
	}
	public void setStandby1(String aStandby1)
	{
		Standby1 = aStandby1;
	}
	public String getStandby2()
	{
		return Standby2;
	}
	public void setStandby2(String aStandby2)
	{
		Standby2 = aStandby2;
	}
	public String getStandby3()
	{
		return Standby3;
	}
	public void setStandby3(String aStandby3)
	{
		Standby3 = aStandby3;
	}
	public String getStandby4()
	{
		return Standby4;
	}
	public void setStandby4(String aStandby4)
	{
		Standby4 = aStandby4;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LRCirculationSchema 对象给 Schema 赋值
	* @param: aLRCirculationSchema LRCirculationSchema
	**/
	public void setSchema(LRCirculationSchema aLRCirculationSchema)
	{
		this.ReContCode = aLRCirculationSchema.getReContCode();
		this.RiskCode = aLRCirculationSchema.getRiskCode();
		this.AccMonth = aLRCirculationSchema.getAccMonth();
		this.CostCenter = aLRCirculationSchema.getCostCenter();
		this.ReComName = aLRCirculationSchema.getReComName();
		this.ManageCom = aLRCirculationSchema.getManageCom();
		this.CessionAmount = aLRCirculationSchema.getCessionAmount();
		this.CessPrem = aLRCirculationSchema.getCessPrem();
		this.ClaimBackFee = aLRCirculationSchema.getClaimBackFee();
		this.Standby1 = aLRCirculationSchema.getStandby1();
		this.Standby2 = aLRCirculationSchema.getStandby2();
		this.Standby3 = aLRCirculationSchema.getStandby3();
		this.Standby4 = aLRCirculationSchema.getStandby4();
		this.Operator = aLRCirculationSchema.getOperator();
		this.MakeDate = fDate.getDate( aLRCirculationSchema.getMakeDate());
		this.MakeTime = aLRCirculationSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLRCirculationSchema.getModifyDate());
		this.ModifyTime = aLRCirculationSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("ReContCode") == null )
				this.ReContCode = null;
			else
				this.ReContCode = rs.getString("ReContCode").trim();

			if( rs.getString("RiskCode") == null )
				this.RiskCode = null;
			else
				this.RiskCode = rs.getString("RiskCode").trim();

			if( rs.getString("AccMonth") == null )
				this.AccMonth = null;
			else
				this.AccMonth = rs.getString("AccMonth").trim();

			if( rs.getString("CostCenter") == null )
				this.CostCenter = null;
			else
				this.CostCenter = rs.getString("CostCenter").trim();

			if( rs.getString("ReComName") == null )
				this.ReComName = null;
			else
				this.ReComName = rs.getString("ReComName").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			this.CessionAmount = rs.getDouble("CessionAmount");
			this.CessPrem = rs.getDouble("CessPrem");
			this.ClaimBackFee = rs.getDouble("ClaimBackFee");
			if( rs.getString("Standby1") == null )
				this.Standby1 = null;
			else
				this.Standby1 = rs.getString("Standby1").trim();

			if( rs.getString("Standby2") == null )
				this.Standby2 = null;
			else
				this.Standby2 = rs.getString("Standby2").trim();

			if( rs.getString("Standby3") == null )
				this.Standby3 = null;
			else
				this.Standby3 = rs.getString("Standby3").trim();

			if( rs.getString("Standby4") == null )
				this.Standby4 = null;
			else
				this.Standby4 = rs.getString("Standby4").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LRCirculation表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LRCirculationSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LRCirculationSchema getSchema()
	{
		LRCirculationSchema aLRCirculationSchema = new LRCirculationSchema();
		aLRCirculationSchema.setSchema(this);
		return aLRCirculationSchema;
	}

	public LRCirculationDB getDB()
	{
		LRCirculationDB aDBOper = new LRCirculationDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLRCirculation描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(ReContCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccMonth)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CostCenter)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReComName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(CessionAmount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(CessPrem));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ClaimBackFee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Standby1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Standby2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Standby3)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Standby4)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLRCirculation>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			ReContCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			AccMonth = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			CostCenter = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			ReComName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			CessionAmount = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,7,SysConst.PACKAGESPILTER))).doubleValue();
			CessPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,8,SysConst.PACKAGESPILTER))).doubleValue();
			ClaimBackFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,9,SysConst.PACKAGESPILTER))).doubleValue();
			Standby1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			Standby2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			Standby3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			Standby4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LRCirculationSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("ReContCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReContCode));
		}
		if (FCode.equals("RiskCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
		}
		if (FCode.equals("AccMonth"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccMonth));
		}
		if (FCode.equals("CostCenter"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CostCenter));
		}
		if (FCode.equals("ReComName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReComName));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("CessionAmount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CessionAmount));
		}
		if (FCode.equals("CessPrem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CessPrem));
		}
		if (FCode.equals("ClaimBackFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClaimBackFee));
		}
		if (FCode.equals("Standby1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Standby1));
		}
		if (FCode.equals("Standby2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Standby2));
		}
		if (FCode.equals("Standby3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Standby3));
		}
		if (FCode.equals("Standby4"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Standby4));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(ReContCode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(RiskCode);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(AccMonth);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(CostCenter);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(ReComName);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 6:
				strFieldValue = String.valueOf(CessionAmount);
				break;
			case 7:
				strFieldValue = String.valueOf(CessPrem);
				break;
			case 8:
				strFieldValue = String.valueOf(ClaimBackFee);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(Standby1);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(Standby2);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(Standby3);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(Standby4);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("ReContCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReContCode = FValue.trim();
			}
			else
				ReContCode = null;
		}
		if (FCode.equalsIgnoreCase("RiskCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskCode = FValue.trim();
			}
			else
				RiskCode = null;
		}
		if (FCode.equalsIgnoreCase("AccMonth"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccMonth = FValue.trim();
			}
			else
				AccMonth = null;
		}
		if (FCode.equalsIgnoreCase("CostCenter"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CostCenter = FValue.trim();
			}
			else
				CostCenter = null;
		}
		if (FCode.equalsIgnoreCase("ReComName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReComName = FValue.trim();
			}
			else
				ReComName = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("CessionAmount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				CessionAmount = d;
			}
		}
		if (FCode.equalsIgnoreCase("CessPrem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				CessPrem = d;
			}
		}
		if (FCode.equalsIgnoreCase("ClaimBackFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ClaimBackFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("Standby1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Standby1 = FValue.trim();
			}
			else
				Standby1 = null;
		}
		if (FCode.equalsIgnoreCase("Standby2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Standby2 = FValue.trim();
			}
			else
				Standby2 = null;
		}
		if (FCode.equalsIgnoreCase("Standby3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Standby3 = FValue.trim();
			}
			else
				Standby3 = null;
		}
		if (FCode.equalsIgnoreCase("Standby4"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Standby4 = FValue.trim();
			}
			else
				Standby4 = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LRCirculationSchema other = (LRCirculationSchema)otherObject;
		return
			(ReContCode == null ? other.getReContCode() == null : ReContCode.equals(other.getReContCode()))
			&& (RiskCode == null ? other.getRiskCode() == null : RiskCode.equals(other.getRiskCode()))
			&& (AccMonth == null ? other.getAccMonth() == null : AccMonth.equals(other.getAccMonth()))
			&& (CostCenter == null ? other.getCostCenter() == null : CostCenter.equals(other.getCostCenter()))
			&& (ReComName == null ? other.getReComName() == null : ReComName.equals(other.getReComName()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& CessionAmount == other.getCessionAmount()
			&& CessPrem == other.getCessPrem()
			&& ClaimBackFee == other.getClaimBackFee()
			&& (Standby1 == null ? other.getStandby1() == null : Standby1.equals(other.getStandby1()))
			&& (Standby2 == null ? other.getStandby2() == null : Standby2.equals(other.getStandby2()))
			&& (Standby3 == null ? other.getStandby3() == null : Standby3.equals(other.getStandby3()))
			&& (Standby4 == null ? other.getStandby4() == null : Standby4.equals(other.getStandby4()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("ReContCode") ) {
			return 0;
		}
		if( strFieldName.equals("RiskCode") ) {
			return 1;
		}
		if( strFieldName.equals("AccMonth") ) {
			return 2;
		}
		if( strFieldName.equals("CostCenter") ) {
			return 3;
		}
		if( strFieldName.equals("ReComName") ) {
			return 4;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 5;
		}
		if( strFieldName.equals("CessionAmount") ) {
			return 6;
		}
		if( strFieldName.equals("CessPrem") ) {
			return 7;
		}
		if( strFieldName.equals("ClaimBackFee") ) {
			return 8;
		}
		if( strFieldName.equals("Standby1") ) {
			return 9;
		}
		if( strFieldName.equals("Standby2") ) {
			return 10;
		}
		if( strFieldName.equals("Standby3") ) {
			return 11;
		}
		if( strFieldName.equals("Standby4") ) {
			return 12;
		}
		if( strFieldName.equals("Operator") ) {
			return 13;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 14;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 15;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 16;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 17;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "ReContCode";
				break;
			case 1:
				strFieldName = "RiskCode";
				break;
			case 2:
				strFieldName = "AccMonth";
				break;
			case 3:
				strFieldName = "CostCenter";
				break;
			case 4:
				strFieldName = "ReComName";
				break;
			case 5:
				strFieldName = "ManageCom";
				break;
			case 6:
				strFieldName = "CessionAmount";
				break;
			case 7:
				strFieldName = "CessPrem";
				break;
			case 8:
				strFieldName = "ClaimBackFee";
				break;
			case 9:
				strFieldName = "Standby1";
				break;
			case 10:
				strFieldName = "Standby2";
				break;
			case 11:
				strFieldName = "Standby3";
				break;
			case 12:
				strFieldName = "Standby4";
				break;
			case 13:
				strFieldName = "Operator";
				break;
			case 14:
				strFieldName = "MakeDate";
				break;
			case 15:
				strFieldName = "MakeTime";
				break;
			case 16:
				strFieldName = "ModifyDate";
				break;
			case 17:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("ReContCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccMonth") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CostCenter") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReComName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CessionAmount") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("CessPrem") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ClaimBackFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Standby1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Standby2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Standby3") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Standby4") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 7:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 8:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
