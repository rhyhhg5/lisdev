/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LDRelatedPartyDB;

/*
 * <p>ClassName: LDRelatedPartySchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 关联方表
 * @CreateDate：2018-11-22
 */
public class LDRelatedPartySchema implements Schema, Cloneable
{
	// @Field
	/** 关联方编号 */
	private String RelateNo;
	/** 名称 */
	private String Name;
	/** 注册资本 */
	private String RgtMoney;
	/** 经营范围 */
	private String BusinessScope;
	/** 公司董事长姓名 */
	private String ChairmanName;
	/** 公司总经理姓名 */
	private String GeneralManagerName;
	/** 性别 */
	private String Sex;
	/** 职务 */
	private String Position;
	/** 证件号码 */
	private String IDNo;
	/** 其他法人或其他组织 */
	private String OtherOrganization;
	/** 关联方类型 */
	private String RelatedType;
	/** 操作员代码 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 16;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LDRelatedPartySchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "RelateNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LDRelatedPartySchema cloned = (LDRelatedPartySchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getRelateNo()
	{
		return RelateNo;
	}
	public void setRelateNo(String aRelateNo)
	{
		RelateNo = aRelateNo;
	}
	public String getName()
	{
		return Name;
	}
	public void setName(String aName)
	{
		Name = aName;
	}
	public String getRgtMoney()
	{
		return RgtMoney;
	}
	public void setRgtMoney(String aRgtMoney)
	{
		RgtMoney = aRgtMoney;
	}
	public String getBusinessScope()
	{
		return BusinessScope;
	}
	public void setBusinessScope(String aBusinessScope)
	{
		BusinessScope = aBusinessScope;
	}
	public String getChairmanName()
	{
		return ChairmanName;
	}
	public void setChairmanName(String aChairmanName)
	{
		ChairmanName = aChairmanName;
	}
	public String getGeneralManagerName()
	{
		return GeneralManagerName;
	}
	public void setGeneralManagerName(String aGeneralManagerName)
	{
		GeneralManagerName = aGeneralManagerName;
	}
	public String getSex()
	{
		return Sex;
	}
	public void setSex(String aSex)
	{
		Sex = aSex;
	}
	public String getPosition()
	{
		return Position;
	}
	public void setPosition(String aPosition)
	{
		Position = aPosition;
	}
	public String getIDNo()
	{
		return IDNo;
	}
	public void setIDNo(String aIDNo)
	{
		IDNo = aIDNo;
	}
	public String getOtherOrganization()
	{
		return OtherOrganization;
	}
	public void setOtherOrganization(String aOtherOrganization)
	{
		OtherOrganization = aOtherOrganization;
	}
	public String getRelatedType()
	{
		return RelatedType;
	}
	public void setRelatedType(String aRelatedType)
	{
		RelatedType = aRelatedType;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LDRelatedPartySchema 对象给 Schema 赋值
	* @param: aLDRelatedPartySchema LDRelatedPartySchema
	**/
	public void setSchema(LDRelatedPartySchema aLDRelatedPartySchema)
	{
		this.RelateNo = aLDRelatedPartySchema.getRelateNo();
		this.Name = aLDRelatedPartySchema.getName();
		this.RgtMoney = aLDRelatedPartySchema.getRgtMoney();
		this.BusinessScope = aLDRelatedPartySchema.getBusinessScope();
		this.ChairmanName = aLDRelatedPartySchema.getChairmanName();
		this.GeneralManagerName = aLDRelatedPartySchema.getGeneralManagerName();
		this.Sex = aLDRelatedPartySchema.getSex();
		this.Position = aLDRelatedPartySchema.getPosition();
		this.IDNo = aLDRelatedPartySchema.getIDNo();
		this.OtherOrganization = aLDRelatedPartySchema.getOtherOrganization();
		this.RelatedType = aLDRelatedPartySchema.getRelatedType();
		this.Operator = aLDRelatedPartySchema.getOperator();
		this.MakeDate = fDate.getDate( aLDRelatedPartySchema.getMakeDate());
		this.MakeTime = aLDRelatedPartySchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLDRelatedPartySchema.getModifyDate());
		this.ModifyTime = aLDRelatedPartySchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("RelateNo") == null )
				this.RelateNo = null;
			else
				this.RelateNo = rs.getString("RelateNo").trim();

			if( rs.getString("Name") == null )
				this.Name = null;
			else
				this.Name = rs.getString("Name").trim();

			if( rs.getString("RgtMoney") == null )
				this.RgtMoney = null;
			else
				this.RgtMoney = rs.getString("RgtMoney").trim();

			if( rs.getString("BusinessScope") == null )
				this.BusinessScope = null;
			else
				this.BusinessScope = rs.getString("BusinessScope").trim();

			if( rs.getString("ChairmanName") == null )
				this.ChairmanName = null;
			else
				this.ChairmanName = rs.getString("ChairmanName").trim();

			if( rs.getString("GeneralManagerName") == null )
				this.GeneralManagerName = null;
			else
				this.GeneralManagerName = rs.getString("GeneralManagerName").trim();

			if( rs.getString("Sex") == null )
				this.Sex = null;
			else
				this.Sex = rs.getString("Sex").trim();

			if( rs.getString("Position") == null )
				this.Position = null;
			else
				this.Position = rs.getString("Position").trim();

			if( rs.getString("IDNo") == null )
				this.IDNo = null;
			else
				this.IDNo = rs.getString("IDNo").trim();

			if( rs.getString("OtherOrganization") == null )
				this.OtherOrganization = null;
			else
				this.OtherOrganization = rs.getString("OtherOrganization").trim();

			if( rs.getString("RelatedType") == null )
				this.RelatedType = null;
			else
				this.RelatedType = rs.getString("RelatedType").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LDRelatedParty表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LDRelatedPartySchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LDRelatedPartySchema getSchema()
	{
		LDRelatedPartySchema aLDRelatedPartySchema = new LDRelatedPartySchema();
		aLDRelatedPartySchema.setSchema(this);
		return aLDRelatedPartySchema;
	}

	public LDRelatedPartyDB getDB()
	{
		LDRelatedPartyDB aDBOper = new LDRelatedPartyDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDRelatedParty描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(RelateNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Name)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RgtMoney)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusinessScope)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ChairmanName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GeneralManagerName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Sex)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Position)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IDNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OtherOrganization)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RelatedType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDRelatedParty>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			RelateNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			RgtMoney = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			BusinessScope = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			ChairmanName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			GeneralManagerName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			Sex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			Position = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			IDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			OtherOrganization = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			RelatedType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LDRelatedPartySchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("RelateNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RelateNo));
		}
		if (FCode.equals("Name"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
		}
		if (FCode.equals("RgtMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RgtMoney));
		}
		if (FCode.equals("BusinessScope"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusinessScope));
		}
		if (FCode.equals("ChairmanName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ChairmanName));
		}
		if (FCode.equals("GeneralManagerName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GeneralManagerName));
		}
		if (FCode.equals("Sex"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Sex));
		}
		if (FCode.equals("Position"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Position));
		}
		if (FCode.equals("IDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
		}
		if (FCode.equals("OtherOrganization"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OtherOrganization));
		}
		if (FCode.equals("RelatedType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RelatedType));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(RelateNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(Name);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(RgtMoney);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(BusinessScope);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(ChairmanName);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(GeneralManagerName);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(Sex);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(Position);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(IDNo);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(OtherOrganization);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(RelatedType);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("RelateNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RelateNo = FValue.trim();
			}
			else
				RelateNo = null;
		}
		if (FCode.equalsIgnoreCase("Name"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Name = FValue.trim();
			}
			else
				Name = null;
		}
		if (FCode.equalsIgnoreCase("RgtMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RgtMoney = FValue.trim();
			}
			else
				RgtMoney = null;
		}
		if (FCode.equalsIgnoreCase("BusinessScope"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusinessScope = FValue.trim();
			}
			else
				BusinessScope = null;
		}
		if (FCode.equalsIgnoreCase("ChairmanName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ChairmanName = FValue.trim();
			}
			else
				ChairmanName = null;
		}
		if (FCode.equalsIgnoreCase("GeneralManagerName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GeneralManagerName = FValue.trim();
			}
			else
				GeneralManagerName = null;
		}
		if (FCode.equalsIgnoreCase("Sex"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Sex = FValue.trim();
			}
			else
				Sex = null;
		}
		if (FCode.equalsIgnoreCase("Position"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Position = FValue.trim();
			}
			else
				Position = null;
		}
		if (FCode.equalsIgnoreCase("IDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IDNo = FValue.trim();
			}
			else
				IDNo = null;
		}
		if (FCode.equalsIgnoreCase("OtherOrganization"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OtherOrganization = FValue.trim();
			}
			else
				OtherOrganization = null;
		}
		if (FCode.equalsIgnoreCase("RelatedType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RelatedType = FValue.trim();
			}
			else
				RelatedType = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LDRelatedPartySchema other = (LDRelatedPartySchema)otherObject;
		return
			(RelateNo == null ? other.getRelateNo() == null : RelateNo.equals(other.getRelateNo()))
			&& (Name == null ? other.getName() == null : Name.equals(other.getName()))
			&& (RgtMoney == null ? other.getRgtMoney() == null : RgtMoney.equals(other.getRgtMoney()))
			&& (BusinessScope == null ? other.getBusinessScope() == null : BusinessScope.equals(other.getBusinessScope()))
			&& (ChairmanName == null ? other.getChairmanName() == null : ChairmanName.equals(other.getChairmanName()))
			&& (GeneralManagerName == null ? other.getGeneralManagerName() == null : GeneralManagerName.equals(other.getGeneralManagerName()))
			&& (Sex == null ? other.getSex() == null : Sex.equals(other.getSex()))
			&& (Position == null ? other.getPosition() == null : Position.equals(other.getPosition()))
			&& (IDNo == null ? other.getIDNo() == null : IDNo.equals(other.getIDNo()))
			&& (OtherOrganization == null ? other.getOtherOrganization() == null : OtherOrganization.equals(other.getOtherOrganization()))
			&& (RelatedType == null ? other.getRelatedType() == null : RelatedType.equals(other.getRelatedType()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("RelateNo") ) {
			return 0;
		}
		if( strFieldName.equals("Name") ) {
			return 1;
		}
		if( strFieldName.equals("RgtMoney") ) {
			return 2;
		}
		if( strFieldName.equals("BusinessScope") ) {
			return 3;
		}
		if( strFieldName.equals("ChairmanName") ) {
			return 4;
		}
		if( strFieldName.equals("GeneralManagerName") ) {
			return 5;
		}
		if( strFieldName.equals("Sex") ) {
			return 6;
		}
		if( strFieldName.equals("Position") ) {
			return 7;
		}
		if( strFieldName.equals("IDNo") ) {
			return 8;
		}
		if( strFieldName.equals("OtherOrganization") ) {
			return 9;
		}
		if( strFieldName.equals("RelatedType") ) {
			return 10;
		}
		if( strFieldName.equals("Operator") ) {
			return 11;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 12;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 13;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 14;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 15;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "RelateNo";
				break;
			case 1:
				strFieldName = "Name";
				break;
			case 2:
				strFieldName = "RgtMoney";
				break;
			case 3:
				strFieldName = "BusinessScope";
				break;
			case 4:
				strFieldName = "ChairmanName";
				break;
			case 5:
				strFieldName = "GeneralManagerName";
				break;
			case 6:
				strFieldName = "Sex";
				break;
			case 7:
				strFieldName = "Position";
				break;
			case 8:
				strFieldName = "IDNo";
				break;
			case 9:
				strFieldName = "OtherOrganization";
				break;
			case 10:
				strFieldName = "RelatedType";
				break;
			case 11:
				strFieldName = "Operator";
				break;
			case 12:
				strFieldName = "MakeDate";
				break;
			case 13:
				strFieldName = "MakeTime";
				break;
			case 14:
				strFieldName = "ModifyDate";
				break;
			case 15:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("RelateNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Name") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RgtMoney") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BusinessScope") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ChairmanName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GeneralManagerName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Sex") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Position") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IDNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OtherOrganization") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RelatedType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
