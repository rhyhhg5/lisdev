/*
 * <p>ClassName: LMRiskPaySchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 险种定义
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LMRiskPayDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LMRiskPaySchema implements Schema
{
    // @Field
    /** 险种编码 */
    private String RiskCode;
    /** 险种版本 */
    private String RiskVer;
    /** 险种名称 */
    private String RiskName;
    /** 催缴标记 */
    private String UrgePayFlag;
    /** 手续费类型 */
    private String ChargeType;
    /** 分解缴费间隔 */
    private String CutPayIntv;
    /** 免交涉及加费 */
    private String PayAvoidType;
    /** 手续费与保费关系 */
    private String ChargeAndPrem;
    /** 结算日类型 */
    private String BalaDateType;
    /** 免交标记 */
    private String PayAvoidFlag;
    /** 缴费与复效关系 */
    private String PayAndRevEffe;
    /** 缴费宽限期 */
    private int GracePeriod;
    /** 宽限期单位 */
    private String GracePeriodUnit;
    /** 宽限日期计算方式 */
    private String GraceDateCalMode;

    public static final int FIELDNUM = 14; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LMRiskPaySchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "RiskCode";
        pk[1] = "RiskVer";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getRiskCode()
    {
        if (RiskCode != null && !RiskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getRiskVer()
    {
        if (RiskVer != null && !RiskVer.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskVer = StrTool.unicodeToGBK(RiskVer);
        }
        return RiskVer;
    }

    public void setRiskVer(String aRiskVer)
    {
        RiskVer = aRiskVer;
    }

    public String getRiskName()
    {
        if (RiskName != null && !RiskName.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskName = StrTool.unicodeToGBK(RiskName);
        }
        return RiskName;
    }

    public void setRiskName(String aRiskName)
    {
        RiskName = aRiskName;
    }

    public String getUrgePayFlag()
    {
        if (UrgePayFlag != null && !UrgePayFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            UrgePayFlag = StrTool.unicodeToGBK(UrgePayFlag);
        }
        return UrgePayFlag;
    }

    public void setUrgePayFlag(String aUrgePayFlag)
    {
        UrgePayFlag = aUrgePayFlag;
    }

    public String getChargeType()
    {
        if (ChargeType != null && !ChargeType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ChargeType = StrTool.unicodeToGBK(ChargeType);
        }
        return ChargeType;
    }

    public void setChargeType(String aChargeType)
    {
        ChargeType = aChargeType;
    }

    public String getCutPayIntv()
    {
        if (CutPayIntv != null && !CutPayIntv.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CutPayIntv = StrTool.unicodeToGBK(CutPayIntv);
        }
        return CutPayIntv;
    }

    public void setCutPayIntv(String aCutPayIntv)
    {
        CutPayIntv = aCutPayIntv;
    }

    public String getPayAvoidType()
    {
        if (PayAvoidType != null && !PayAvoidType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            PayAvoidType = StrTool.unicodeToGBK(PayAvoidType);
        }
        return PayAvoidType;
    }

    public void setPayAvoidType(String aPayAvoidType)
    {
        PayAvoidType = aPayAvoidType;
    }

    public String getChargeAndPrem()
    {
        if (ChargeAndPrem != null && !ChargeAndPrem.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ChargeAndPrem = StrTool.unicodeToGBK(ChargeAndPrem);
        }
        return ChargeAndPrem;
    }

    public void setChargeAndPrem(String aChargeAndPrem)
    {
        ChargeAndPrem = aChargeAndPrem;
    }

    public String getBalaDateType()
    {
        if (BalaDateType != null && !BalaDateType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            BalaDateType = StrTool.unicodeToGBK(BalaDateType);
        }
        return BalaDateType;
    }

    public void setBalaDateType(String aBalaDateType)
    {
        BalaDateType = aBalaDateType;
    }

    public String getPayAvoidFlag()
    {
        if (PayAvoidFlag != null && !PayAvoidFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            PayAvoidFlag = StrTool.unicodeToGBK(PayAvoidFlag);
        }
        return PayAvoidFlag;
    }

    public void setPayAvoidFlag(String aPayAvoidFlag)
    {
        PayAvoidFlag = aPayAvoidFlag;
    }

    public String getPayAndRevEffe()
    {
        if (PayAndRevEffe != null && !PayAndRevEffe.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            PayAndRevEffe = StrTool.unicodeToGBK(PayAndRevEffe);
        }
        return PayAndRevEffe;
    }

    public void setPayAndRevEffe(String aPayAndRevEffe)
    {
        PayAndRevEffe = aPayAndRevEffe;
    }

    public int getGracePeriod()
    {
        return GracePeriod;
    }

    public void setGracePeriod(int aGracePeriod)
    {
        GracePeriod = aGracePeriod;
    }

    public void setGracePeriod(String aGracePeriod)
    {
        if (aGracePeriod != null && !aGracePeriod.equals(""))
        {
            Integer tInteger = new Integer(aGracePeriod);
            int i = tInteger.intValue();
            GracePeriod = i;
        }
    }

    public String getGracePeriodUnit()
    {
        if (GracePeriodUnit != null && !GracePeriodUnit.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GracePeriodUnit = StrTool.unicodeToGBK(GracePeriodUnit);
        }
        return GracePeriodUnit;
    }

    public void setGracePeriodUnit(String aGracePeriodUnit)
    {
        GracePeriodUnit = aGracePeriodUnit;
    }

    public String getGraceDateCalMode()
    {
        if (GraceDateCalMode != null && !GraceDateCalMode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GraceDateCalMode = StrTool.unicodeToGBK(GraceDateCalMode);
        }
        return GraceDateCalMode;
    }

    public void setGraceDateCalMode(String aGraceDateCalMode)
    {
        GraceDateCalMode = aGraceDateCalMode;
    }

    /**
     * 使用另外一个 LMRiskPaySchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LMRiskPaySchema aLMRiskPaySchema)
    {
        this.RiskCode = aLMRiskPaySchema.getRiskCode();
        this.RiskVer = aLMRiskPaySchema.getRiskVer();
        this.RiskName = aLMRiskPaySchema.getRiskName();
        this.UrgePayFlag = aLMRiskPaySchema.getUrgePayFlag();
        this.ChargeType = aLMRiskPaySchema.getChargeType();
        this.CutPayIntv = aLMRiskPaySchema.getCutPayIntv();
        this.PayAvoidType = aLMRiskPaySchema.getPayAvoidType();
        this.ChargeAndPrem = aLMRiskPaySchema.getChargeAndPrem();
        this.BalaDateType = aLMRiskPaySchema.getBalaDateType();
        this.PayAvoidFlag = aLMRiskPaySchema.getPayAvoidFlag();
        this.PayAndRevEffe = aLMRiskPaySchema.getPayAndRevEffe();
        this.GracePeriod = aLMRiskPaySchema.getGracePeriod();
        this.GracePeriodUnit = aLMRiskPaySchema.getGracePeriodUnit();
        this.GraceDateCalMode = aLMRiskPaySchema.getGraceDateCalMode();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("RiskVer") == null)
            {
                this.RiskVer = null;
            }
            else
            {
                this.RiskVer = rs.getString("RiskVer").trim();
            }

            if (rs.getString("RiskName") == null)
            {
                this.RiskName = null;
            }
            else
            {
                this.RiskName = rs.getString("RiskName").trim();
            }

            if (rs.getString("UrgePayFlag") == null)
            {
                this.UrgePayFlag = null;
            }
            else
            {
                this.UrgePayFlag = rs.getString("UrgePayFlag").trim();
            }

            if (rs.getString("ChargeType") == null)
            {
                this.ChargeType = null;
            }
            else
            {
                this.ChargeType = rs.getString("ChargeType").trim();
            }

            if (rs.getString("CutPayIntv") == null)
            {
                this.CutPayIntv = null;
            }
            else
            {
                this.CutPayIntv = rs.getString("CutPayIntv").trim();
            }

            if (rs.getString("PayAvoidType") == null)
            {
                this.PayAvoidType = null;
            }
            else
            {
                this.PayAvoidType = rs.getString("PayAvoidType").trim();
            }

            if (rs.getString("ChargeAndPrem") == null)
            {
                this.ChargeAndPrem = null;
            }
            else
            {
                this.ChargeAndPrem = rs.getString("ChargeAndPrem").trim();
            }

            if (rs.getString("BalaDateType") == null)
            {
                this.BalaDateType = null;
            }
            else
            {
                this.BalaDateType = rs.getString("BalaDateType").trim();
            }

            if (rs.getString("PayAvoidFlag") == null)
            {
                this.PayAvoidFlag = null;
            }
            else
            {
                this.PayAvoidFlag = rs.getString("PayAvoidFlag").trim();
            }

            if (rs.getString("PayAndRevEffe") == null)
            {
                this.PayAndRevEffe = null;
            }
            else
            {
                this.PayAndRevEffe = rs.getString("PayAndRevEffe").trim();
            }

            this.GracePeriod = rs.getInt("GracePeriod");
            if (rs.getString("GracePeriodUnit") == null)
            {
                this.GracePeriodUnit = null;
            }
            else
            {
                this.GracePeriodUnit = rs.getString("GracePeriodUnit").trim();
            }

            if (rs.getString("GraceDateCalMode") == null)
            {
                this.GraceDateCalMode = null;
            }
            else
            {
                this.GraceDateCalMode = rs.getString("GraceDateCalMode").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMRiskPaySchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LMRiskPaySchema getSchema()
    {
        LMRiskPaySchema aLMRiskPaySchema = new LMRiskPaySchema();
        aLMRiskPaySchema.setSchema(this);
        return aLMRiskPaySchema;
    }

    public LMRiskPayDB getDB()
    {
        LMRiskPayDB aDBOper = new LMRiskPayDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMRiskPay描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskVer)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(UrgePayFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ChargeType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CutPayIntv)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PayAvoidType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ChargeAndPrem)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(BalaDateType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PayAvoidFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PayAndRevEffe)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(GracePeriod) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GracePeriodUnit)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GraceDateCalMode));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMRiskPay>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            RiskVer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                     SysConst.PACKAGESPILTER);
            RiskName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            UrgePayFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                         SysConst.PACKAGESPILTER);
            ChargeType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                        SysConst.PACKAGESPILTER);
            CutPayIntv = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                        SysConst.PACKAGESPILTER);
            PayAvoidType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                          SysConst.PACKAGESPILTER);
            ChargeAndPrem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                           SysConst.PACKAGESPILTER);
            BalaDateType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                          SysConst.PACKAGESPILTER);
            PayAvoidFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                          SysConst.PACKAGESPILTER);
            PayAndRevEffe = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                           SysConst.PACKAGESPILTER);
            GracePeriod = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 12, SysConst.PACKAGESPILTER))).intValue();
            GracePeriodUnit = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             13, SysConst.PACKAGESPILTER);
            GraceDateCalMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                              14, SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMRiskPaySchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskCode));
        }
        if (FCode.equals("RiskVer"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskVer));
        }
        if (FCode.equals("RiskName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskName));
        }
        if (FCode.equals("UrgePayFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(UrgePayFlag));
        }
        if (FCode.equals("ChargeType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ChargeType));
        }
        if (FCode.equals("CutPayIntv"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CutPayIntv));
        }
        if (FCode.equals("PayAvoidType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PayAvoidType));
        }
        if (FCode.equals("ChargeAndPrem"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ChargeAndPrem));
        }
        if (FCode.equals("BalaDateType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(BalaDateType));
        }
        if (FCode.equals("PayAvoidFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PayAvoidFlag));
        }
        if (FCode.equals("PayAndRevEffe"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PayAndRevEffe));
        }
        if (FCode.equals("GracePeriod"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GracePeriod));
        }
        if (FCode.equals("GracePeriodUnit"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GracePeriodUnit));
        }
        if (FCode.equals("GraceDateCalMode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GraceDateCalMode));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(RiskVer);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(RiskName);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(UrgePayFlag);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(ChargeType);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(CutPayIntv);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(PayAvoidType);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(ChargeAndPrem);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(BalaDateType);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(PayAvoidFlag);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(PayAndRevEffe);
                break;
            case 11:
                strFieldValue = String.valueOf(GracePeriod);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(GracePeriodUnit);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(GraceDateCalMode);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("RiskVer"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskVer = FValue.trim();
            }
            else
            {
                RiskVer = null;
            }
        }
        if (FCode.equals("RiskName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskName = FValue.trim();
            }
            else
            {
                RiskName = null;
            }
        }
        if (FCode.equals("UrgePayFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UrgePayFlag = FValue.trim();
            }
            else
            {
                UrgePayFlag = null;
            }
        }
        if (FCode.equals("ChargeType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ChargeType = FValue.trim();
            }
            else
            {
                ChargeType = null;
            }
        }
        if (FCode.equals("CutPayIntv"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CutPayIntv = FValue.trim();
            }
            else
            {
                CutPayIntv = null;
            }
        }
        if (FCode.equals("PayAvoidType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PayAvoidType = FValue.trim();
            }
            else
            {
                PayAvoidType = null;
            }
        }
        if (FCode.equals("ChargeAndPrem"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ChargeAndPrem = FValue.trim();
            }
            else
            {
                ChargeAndPrem = null;
            }
        }
        if (FCode.equals("BalaDateType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BalaDateType = FValue.trim();
            }
            else
            {
                BalaDateType = null;
            }
        }
        if (FCode.equals("PayAvoidFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PayAvoidFlag = FValue.trim();
            }
            else
            {
                PayAvoidFlag = null;
            }
        }
        if (FCode.equals("PayAndRevEffe"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PayAndRevEffe = FValue.trim();
            }
            else
            {
                PayAndRevEffe = null;
            }
        }
        if (FCode.equals("GracePeriod"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                GracePeriod = i;
            }
        }
        if (FCode.equals("GracePeriodUnit"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GracePeriodUnit = FValue.trim();
            }
            else
            {
                GracePeriodUnit = null;
            }
        }
        if (FCode.equals("GraceDateCalMode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GraceDateCalMode = FValue.trim();
            }
            else
            {
                GraceDateCalMode = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LMRiskPaySchema other = (LMRiskPaySchema) otherObject;
        return
                RiskCode.equals(other.getRiskCode())
                && RiskVer.equals(other.getRiskVer())
                && RiskName.equals(other.getRiskName())
                && UrgePayFlag.equals(other.getUrgePayFlag())
                && ChargeType.equals(other.getChargeType())
                && CutPayIntv.equals(other.getCutPayIntv())
                && PayAvoidType.equals(other.getPayAvoidType())
                && ChargeAndPrem.equals(other.getChargeAndPrem())
                && BalaDateType.equals(other.getBalaDateType())
                && PayAvoidFlag.equals(other.getPayAvoidFlag())
                && PayAndRevEffe.equals(other.getPayAndRevEffe())
                && GracePeriod == other.getGracePeriod()
                && GracePeriodUnit.equals(other.getGracePeriodUnit())
                && GraceDateCalMode.equals(other.getGraceDateCalMode());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("RiskCode"))
        {
            return 0;
        }
        if (strFieldName.equals("RiskVer"))
        {
            return 1;
        }
        if (strFieldName.equals("RiskName"))
        {
            return 2;
        }
        if (strFieldName.equals("UrgePayFlag"))
        {
            return 3;
        }
        if (strFieldName.equals("ChargeType"))
        {
            return 4;
        }
        if (strFieldName.equals("CutPayIntv"))
        {
            return 5;
        }
        if (strFieldName.equals("PayAvoidType"))
        {
            return 6;
        }
        if (strFieldName.equals("ChargeAndPrem"))
        {
            return 7;
        }
        if (strFieldName.equals("BalaDateType"))
        {
            return 8;
        }
        if (strFieldName.equals("PayAvoidFlag"))
        {
            return 9;
        }
        if (strFieldName.equals("PayAndRevEffe"))
        {
            return 10;
        }
        if (strFieldName.equals("GracePeriod"))
        {
            return 11;
        }
        if (strFieldName.equals("GracePeriodUnit"))
        {
            return 12;
        }
        if (strFieldName.equals("GraceDateCalMode"))
        {
            return 13;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "RiskVer";
                break;
            case 2:
                strFieldName = "RiskName";
                break;
            case 3:
                strFieldName = "UrgePayFlag";
                break;
            case 4:
                strFieldName = "ChargeType";
                break;
            case 5:
                strFieldName = "CutPayIntv";
                break;
            case 6:
                strFieldName = "PayAvoidType";
                break;
            case 7:
                strFieldName = "ChargeAndPrem";
                break;
            case 8:
                strFieldName = "BalaDateType";
                break;
            case 9:
                strFieldName = "PayAvoidFlag";
                break;
            case 10:
                strFieldName = "PayAndRevEffe";
                break;
            case 11:
                strFieldName = "GracePeriod";
                break;
            case 12:
                strFieldName = "GracePeriodUnit";
                break;
            case 13:
                strFieldName = "GraceDateCalMode";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskVer"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UrgePayFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ChargeType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CutPayIntv"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PayAvoidType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ChargeAndPrem"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BalaDateType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PayAvoidFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PayAndRevEffe"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GracePeriod"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("GracePeriodUnit"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GraceDateCalMode"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_INT;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
