/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LDPlanDutyParamDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

/*
 * <p>ClassName: LDPlanDutyParamSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 保险计划套餐
 * @CreateDate：2005-03-07
 */
public class LDPlanDutyParamSchema implements Schema
{
    // @Field
    /** 主险险种编码 */
    private String MainRiskCode;
    /** 主险险种版本 */
    private String MainRiskVersion;
    /** 险种编码 */
    private String RiskCode;
    /** 险种版本 */
    private String RiskVersion;
    /** 保险计划编码 */
    private String ContPlanCode;
    /** 保险计划名称 */
    private String ContPlanName;
    /** 责任编码 */
    private String DutyCode;
    /** 计划要素 */
    private String CalFactor;
    /** 计划要素类型 */
    private String CalFactorType;
    /** 计划要素值 */
    private String CalFactorValue;
    /** 备注 */
    private String Remark;
    /** 计划类别 */
    private String PlanType;

    public static final int FIELDNUM = 12; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LDPlanDutyParamSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[6];
        pk[0] = "MainRiskCode";
        pk[1] = "RiskCode";
        pk[2] = "ContPlanCode";
        pk[3] = "DutyCode";
        pk[4] = "CalFactor";
        pk[5] = "PlanType";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getMainRiskCode()
    {
        if (MainRiskCode != null && !MainRiskCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            MainRiskCode = StrTool.unicodeToGBK(MainRiskCode);
        }
        return MainRiskCode;
    }

    public void setMainRiskCode(String aMainRiskCode)
    {
        MainRiskCode = aMainRiskCode;
    }

    public String getMainRiskVersion()
    {
        if (MainRiskVersion != null && !MainRiskVersion.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            MainRiskVersion = StrTool.unicodeToGBK(MainRiskVersion);
        }
        return MainRiskVersion;
    }

    public void setMainRiskVersion(String aMainRiskVersion)
    {
        MainRiskVersion = aMainRiskVersion;
    }

    public String getRiskCode()
    {
        if (RiskCode != null && !RiskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getRiskVersion()
    {
        if (RiskVersion != null && !RiskVersion.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            RiskVersion = StrTool.unicodeToGBK(RiskVersion);
        }
        return RiskVersion;
    }

    public void setRiskVersion(String aRiskVersion)
    {
        RiskVersion = aRiskVersion;
    }

    public String getContPlanCode()
    {
        if (ContPlanCode != null && !ContPlanCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ContPlanCode = StrTool.unicodeToGBK(ContPlanCode);
        }
        return ContPlanCode;
    }

    public void setContPlanCode(String aContPlanCode)
    {
        ContPlanCode = aContPlanCode;
    }

    public String getContPlanName()
    {
        if (ContPlanName != null && !ContPlanName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ContPlanName = StrTool.unicodeToGBK(ContPlanName);
        }
        return ContPlanName;
    }

    public void setContPlanName(String aContPlanName)
    {
        ContPlanName = aContPlanName;
    }

    public String getDutyCode()
    {
        if (DutyCode != null && !DutyCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            DutyCode = StrTool.unicodeToGBK(DutyCode);
        }
        return DutyCode;
    }

    public void setDutyCode(String aDutyCode)
    {
        DutyCode = aDutyCode;
    }

    public String getCalFactor()
    {
        if (CalFactor != null && !CalFactor.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CalFactor = StrTool.unicodeToGBK(CalFactor);
        }
        return CalFactor;
    }

    public void setCalFactor(String aCalFactor)
    {
        CalFactor = aCalFactor;
    }

    public String getCalFactorType()
    {
        if (CalFactorType != null && !CalFactorType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CalFactorType = StrTool.unicodeToGBK(CalFactorType);
        }
        return CalFactorType;
    }

    public void setCalFactorType(String aCalFactorType)
    {
        CalFactorType = aCalFactorType;
    }

    public String getCalFactorValue()
    {
        if (CalFactorValue != null && !CalFactorValue.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CalFactorValue = StrTool.unicodeToGBK(CalFactorValue);
        }
        return CalFactorValue;
    }

    public void setCalFactorValue(String aCalFactorValue)
    {
        CalFactorValue = aCalFactorValue;
    }

    public String getRemark()
    {
        if (Remark != null && !Remark.equals("") && SysConst.CHANGECHARSET == true)
        {
            Remark = StrTool.unicodeToGBK(Remark);
        }
        return Remark;
    }

    public void setRemark(String aRemark)
    {
        Remark = aRemark;
    }

    public String getPlanType()
    {
        if (PlanType != null && !PlanType.equals("") && SysConst.CHANGECHARSET == true)
        {
            PlanType = StrTool.unicodeToGBK(PlanType);
        }
        return PlanType;
    }

    public void setPlanType(String aPlanType)
    {
        PlanType = aPlanType;
    }

    /**
     * 使用另外一个 LDPlanDutyParamSchema 对象给 Schema 赋值
     * @param: aLDPlanDutyParamSchema LDPlanDutyParamSchema
     **/
    public void setSchema(LDPlanDutyParamSchema aLDPlanDutyParamSchema)
    {
        this.MainRiskCode = aLDPlanDutyParamSchema.getMainRiskCode();
        this.MainRiskVersion = aLDPlanDutyParamSchema.getMainRiskVersion();
        this.RiskCode = aLDPlanDutyParamSchema.getRiskCode();
        this.RiskVersion = aLDPlanDutyParamSchema.getRiskVersion();
        this.ContPlanCode = aLDPlanDutyParamSchema.getContPlanCode();
        this.ContPlanName = aLDPlanDutyParamSchema.getContPlanName();
        this.DutyCode = aLDPlanDutyParamSchema.getDutyCode();
        this.CalFactor = aLDPlanDutyParamSchema.getCalFactor();
        this.CalFactorType = aLDPlanDutyParamSchema.getCalFactorType();
        this.CalFactorValue = aLDPlanDutyParamSchema.getCalFactorValue();
        this.Remark = aLDPlanDutyParamSchema.getRemark();
        this.PlanType = aLDPlanDutyParamSchema.getPlanType();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("MainRiskCode") == null)
            {
                this.MainRiskCode = null;
            }
            else
            {
                this.MainRiskCode = rs.getString("MainRiskCode").trim();
            }

            if (rs.getString("MainRiskVersion") == null)
            {
                this.MainRiskVersion = null;
            }
            else
            {
                this.MainRiskVersion = rs.getString("MainRiskVersion").trim();
            }

            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("RiskVersion") == null)
            {
                this.RiskVersion = null;
            }
            else
            {
                this.RiskVersion = rs.getString("RiskVersion").trim();
            }

            if (rs.getString("ContPlanCode") == null)
            {
                this.ContPlanCode = null;
            }
            else
            {
                this.ContPlanCode = rs.getString("ContPlanCode").trim();
            }

            if (rs.getString("ContPlanName") == null)
            {
                this.ContPlanName = null;
            }
            else
            {
                this.ContPlanName = rs.getString("ContPlanName").trim();
            }

            if (rs.getString("DutyCode") == null)
            {
                this.DutyCode = null;
            }
            else
            {
                this.DutyCode = rs.getString("DutyCode").trim();
            }

            if (rs.getString("CalFactor") == null)
            {
                this.CalFactor = null;
            }
            else
            {
                this.CalFactor = rs.getString("CalFactor").trim();
            }

            if (rs.getString("CalFactorType") == null)
            {
                this.CalFactorType = null;
            }
            else
            {
                this.CalFactorType = rs.getString("CalFactorType").trim();
            }

            if (rs.getString("CalFactorValue") == null)
            {
                this.CalFactorValue = null;
            }
            else
            {
                this.CalFactorValue = rs.getString("CalFactorValue").trim();
            }

            if (rs.getString("Remark") == null)
            {
                this.Remark = null;
            }
            else
            {
                this.Remark = rs.getString("Remark").trim();
            }

            if (rs.getString("PlanType") == null)
            {
                this.PlanType = null;
            }
            else
            {
                this.PlanType = rs.getString("PlanType").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDPlanDutyParamSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LDPlanDutyParamSchema getSchema()
    {
        LDPlanDutyParamSchema aLDPlanDutyParamSchema = new
                LDPlanDutyParamSchema();
        aLDPlanDutyParamSchema.setSchema(this);
        return aLDPlanDutyParamSchema;
    }

    public LDPlanDutyParamDB getDB()
    {
        LDPlanDutyParamDB aDBOper = new LDPlanDutyParamDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDPlanDutyParam描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(MainRiskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MainRiskVersion)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskVersion)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ContPlanCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ContPlanName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(DutyCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CalFactor)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CalFactorType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CalFactorValue)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Remark)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PlanType));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDPlanDutyParam>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            MainRiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                          SysConst.PACKAGESPILTER);
            MainRiskVersion = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             2, SysConst.PACKAGESPILTER);
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            RiskVersion = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                         SysConst.PACKAGESPILTER);
            ContPlanCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                          SysConst.PACKAGESPILTER);
            ContPlanName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                          SysConst.PACKAGESPILTER);
            DutyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                      SysConst.PACKAGESPILTER);
            CalFactor = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                       SysConst.PACKAGESPILTER);
            CalFactorType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                           SysConst.PACKAGESPILTER);
            CalFactorValue = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                            10, SysConst.PACKAGESPILTER);
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                    SysConst.PACKAGESPILTER);
            PlanType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                      SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDPlanDutyParamSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("MainRiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MainRiskCode));
        }
        if (FCode.equals("MainRiskVersion"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MainRiskVersion));
        }
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskCode));
        }
        if (FCode.equals("RiskVersion"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskVersion));
        }
        if (FCode.equals("ContPlanCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ContPlanCode));
        }
        if (FCode.equals("ContPlanName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ContPlanName));
        }
        if (FCode.equals("DutyCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DutyCode));
        }
        if (FCode.equals("CalFactor"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CalFactor));
        }
        if (FCode.equals("CalFactorType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CalFactorType));
        }
        if (FCode.equals("CalFactorValue"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CalFactorValue));
        }
        if (FCode.equals("Remark"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Remark));
        }
        if (FCode.equals("PlanType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PlanType));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(MainRiskCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(MainRiskVersion);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(RiskVersion);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(ContPlanCode);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(ContPlanName);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(DutyCode);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(CalFactor);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(CalFactorType);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(CalFactorValue);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(PlanType);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("MainRiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MainRiskCode = FValue.trim();
            }
            else
            {
                MainRiskCode = null;
            }
        }
        if (FCode.equals("MainRiskVersion"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MainRiskVersion = FValue.trim();
            }
            else
            {
                MainRiskVersion = null;
            }
        }
        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("RiskVersion"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskVersion = FValue.trim();
            }
            else
            {
                RiskVersion = null;
            }
        }
        if (FCode.equals("ContPlanCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ContPlanCode = FValue.trim();
            }
            else
            {
                ContPlanCode = null;
            }
        }
        if (FCode.equals("ContPlanName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ContPlanName = FValue.trim();
            }
            else
            {
                ContPlanName = null;
            }
        }
        if (FCode.equals("DutyCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DutyCode = FValue.trim();
            }
            else
            {
                DutyCode = null;
            }
        }
        if (FCode.equals("CalFactor"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CalFactor = FValue.trim();
            }
            else
            {
                CalFactor = null;
            }
        }
        if (FCode.equals("CalFactorType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CalFactorType = FValue.trim();
            }
            else
            {
                CalFactorType = null;
            }
        }
        if (FCode.equals("CalFactorValue"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CalFactorValue = FValue.trim();
            }
            else
            {
                CalFactorValue = null;
            }
        }
        if (FCode.equals("Remark"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
            {
                Remark = null;
            }
        }
        if (FCode.equals("PlanType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PlanType = FValue.trim();
            }
            else
            {
                PlanType = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LDPlanDutyParamSchema other = (LDPlanDutyParamSchema) otherObject;
        return
                MainRiskCode.equals(other.getMainRiskCode())
                && MainRiskVersion.equals(other.getMainRiskVersion())
                && RiskCode.equals(other.getRiskCode())
                && RiskVersion.equals(other.getRiskVersion())
                && ContPlanCode.equals(other.getContPlanCode())
                && ContPlanName.equals(other.getContPlanName())
                && DutyCode.equals(other.getDutyCode())
                && CalFactor.equals(other.getCalFactor())
                && CalFactorType.equals(other.getCalFactorType())
                && CalFactorValue.equals(other.getCalFactorValue())
                && Remark.equals(other.getRemark())
                && PlanType.equals(other.getPlanType());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("MainRiskCode"))
        {
            return 0;
        }
        if (strFieldName.equals("MainRiskVersion"))
        {
            return 1;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return 2;
        }
        if (strFieldName.equals("RiskVersion"))
        {
            return 3;
        }
        if (strFieldName.equals("ContPlanCode"))
        {
            return 4;
        }
        if (strFieldName.equals("ContPlanName"))
        {
            return 5;
        }
        if (strFieldName.equals("DutyCode"))
        {
            return 6;
        }
        if (strFieldName.equals("CalFactor"))
        {
            return 7;
        }
        if (strFieldName.equals("CalFactorType"))
        {
            return 8;
        }
        if (strFieldName.equals("CalFactorValue"))
        {
            return 9;
        }
        if (strFieldName.equals("Remark"))
        {
            return 10;
        }
        if (strFieldName.equals("PlanType"))
        {
            return 11;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "MainRiskCode";
                break;
            case 1:
                strFieldName = "MainRiskVersion";
                break;
            case 2:
                strFieldName = "RiskCode";
                break;
            case 3:
                strFieldName = "RiskVersion";
                break;
            case 4:
                strFieldName = "ContPlanCode";
                break;
            case 5:
                strFieldName = "ContPlanName";
                break;
            case 6:
                strFieldName = "DutyCode";
                break;
            case 7:
                strFieldName = "CalFactor";
                break;
            case 8:
                strFieldName = "CalFactorType";
                break;
            case 9:
                strFieldName = "CalFactorValue";
                break;
            case 10:
                strFieldName = "Remark";
                break;
            case 11:
                strFieldName = "PlanType";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("MainRiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MainRiskVersion"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskVersion"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContPlanCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContPlanName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DutyCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CalFactor"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CalFactorType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CalFactorValue"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Remark"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PlanType"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
