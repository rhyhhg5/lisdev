/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LDTumourDB;

/*
 * <p>ClassName: LDTumourSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2018-06-14
 */
public class LDTumourSchema implements Schema, Cloneable
{
	// @Field
	/** 肿瘤形态学代码 */
	private String LDCODE;
	/** 肿瘤形态学名称 */
	private String LDNAME;
	/** 备用字段1 */
	private String STANDBY1;
	/** 备用字段2 */
	private String STANDBY2;
	/** 备用字段3 */
	private String STANDBY3;
	/** 备用字段4 */
	private String STANDBY4;

	public static final int FIELDNUM = 6;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LDTumourSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "LDCODE";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LDTumourSchema cloned = (LDTumourSchema) super.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getLDCODE()
	{
		return LDCODE;
	}
	public void setLDCODE(String aLDCODE)
	{
		LDCODE = aLDCODE;
	}
	public String getLDNAME()
	{
		return LDNAME;
	}
	public void setLDNAME(String aLDNAME)
	{
		LDNAME = aLDNAME;
	}
	public String getSTANDBY1()
	{
		return STANDBY1;
	}
	public void setSTANDBY1(String aSTANDBY1)
	{
		STANDBY1 = aSTANDBY1;
	}
	public String getSTANDBY2()
	{
		return STANDBY2;
	}
	public void setSTANDBY2(String aSTANDBY2)
	{
		STANDBY2 = aSTANDBY2;
	}
	public String getSTANDBY3()
	{
		return STANDBY3;
	}
	public void setSTANDBY3(String aSTANDBY3)
	{
		STANDBY3 = aSTANDBY3;
	}
	public String getSTANDBY4()
	{
		return STANDBY4;
	}
	public void setSTANDBY4(String aSTANDBY4)
	{
		STANDBY4 = aSTANDBY4;
	}

	/**
	* 使用另外一个 LDTumourSchema 对象给 Schema 赋值
	* @param: aLDTumourSchema LDTumourSchema
	**/
	public void setSchema(LDTumourSchema aLDTumourSchema)
	{
		this.LDCODE = aLDTumourSchema.getLDCODE();
		this.LDNAME = aLDTumourSchema.getLDNAME();
		this.STANDBY1 = aLDTumourSchema.getSTANDBY1();
		this.STANDBY2 = aLDTumourSchema.getSTANDBY2();
		this.STANDBY3 = aLDTumourSchema.getSTANDBY3();
		this.STANDBY4 = aLDTumourSchema.getSTANDBY4();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("LDCODE") == null )
				this.LDCODE = null;
			else
				this.LDCODE = rs.getString("LDCODE").trim();

			if( rs.getString("LDNAME") == null )
				this.LDNAME = null;
			else
				this.LDNAME = rs.getString("LDNAME").trim();

			if( rs.getString("STANDBY1") == null )
				this.STANDBY1 = null;
			else
				this.STANDBY1 = rs.getString("STANDBY1").trim();

			if( rs.getString("STANDBY2") == null )
				this.STANDBY2 = null;
			else
				this.STANDBY2 = rs.getString("STANDBY2").trim();

			if( rs.getString("STANDBY3") == null )
				this.STANDBY3 = null;
			else
				this.STANDBY3 = rs.getString("STANDBY3").trim();

			if( rs.getString("STANDBY4") == null )
				this.STANDBY4 = null;
			else
				this.STANDBY4 = rs.getString("STANDBY4").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LDTumour表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LDTumourSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LDTumourSchema getSchema()
	{
		LDTumourSchema aLDTumourSchema = new LDTumourSchema();
		aLDTumourSchema.setSchema(this);
		return aLDTumourSchema;
	}

	public LDTumourDB getDB()
	{
		LDTumourDB aDBOper = new LDTumourDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDTumour描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(LDCODE)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LDNAME)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(STANDBY1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(STANDBY2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(STANDBY3)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(STANDBY4));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDTumour>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			LDCODE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			LDNAME = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			STANDBY1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			STANDBY2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			STANDBY3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			STANDBY4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LDTumourSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("LDCODE"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LDCODE));
		}
		if (FCode.equals("LDNAME"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LDNAME));
		}
		if (FCode.equals("STANDBY1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(STANDBY1));
		}
		if (FCode.equals("STANDBY2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(STANDBY2));
		}
		if (FCode.equals("STANDBY3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(STANDBY3));
		}
		if (FCode.equals("STANDBY4"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(STANDBY4));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(LDCODE);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(LDNAME);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(STANDBY1);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(STANDBY2);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(STANDBY3);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(STANDBY4);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("LDCODE"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LDCODE = FValue.trim();
			}
			else
				LDCODE = null;
		}
		if (FCode.equalsIgnoreCase("LDNAME"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LDNAME = FValue.trim();
			}
			else
				LDNAME = null;
		}
		if (FCode.equalsIgnoreCase("STANDBY1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				STANDBY1 = FValue.trim();
			}
			else
				STANDBY1 = null;
		}
		if (FCode.equalsIgnoreCase("STANDBY2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				STANDBY2 = FValue.trim();
			}
			else
				STANDBY2 = null;
		}
		if (FCode.equalsIgnoreCase("STANDBY3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				STANDBY3 = FValue.trim();
			}
			else
				STANDBY3 = null;
		}
		if (FCode.equalsIgnoreCase("STANDBY4"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				STANDBY4 = FValue.trim();
			}
			else
				STANDBY4 = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LDTumourSchema other = (LDTumourSchema)otherObject;
		return
			(LDCODE == null ? other.getLDCODE() == null : LDCODE.equals(other.getLDCODE()))
			&& (LDNAME == null ? other.getLDNAME() == null : LDNAME.equals(other.getLDNAME()))
			&& (STANDBY1 == null ? other.getSTANDBY1() == null : STANDBY1.equals(other.getSTANDBY1()))
			&& (STANDBY2 == null ? other.getSTANDBY2() == null : STANDBY2.equals(other.getSTANDBY2()))
			&& (STANDBY3 == null ? other.getSTANDBY3() == null : STANDBY3.equals(other.getSTANDBY3()))
			&& (STANDBY4 == null ? other.getSTANDBY4() == null : STANDBY4.equals(other.getSTANDBY4()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("LDCODE") ) {
			return 0;
		}
		if( strFieldName.equals("LDNAME") ) {
			return 1;
		}
		if( strFieldName.equals("STANDBY1") ) {
			return 2;
		}
		if( strFieldName.equals("STANDBY2") ) {
			return 3;
		}
		if( strFieldName.equals("STANDBY3") ) {
			return 4;
		}
		if( strFieldName.equals("STANDBY4") ) {
			return 5;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "LDCODE";
				break;
			case 1:
				strFieldName = "LDNAME";
				break;
			case 2:
				strFieldName = "STANDBY1";
				break;
			case 3:
				strFieldName = "STANDBY2";
				break;
			case 4:
				strFieldName = "STANDBY3";
				break;
			case 5:
				strFieldName = "STANDBY4";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("LDCODE") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LDNAME") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("STANDBY1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("STANDBY2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("STANDBY3") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("STANDBY4") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
