/*
 * <p>ClassName: LOReport1Schema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LOReport1DB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LOReport1Schema implements Schema
{
    // @Field
    /** 管理机构 */
    private String ManageCom;
    /** 险种编码 */
    private String RiskCode;
    /** 销售渠道 */
    private String SaleChnl;
    /** 业务日期 */
    private Date OldMakeDate;
    /** 趸交报费（本期） */
    private double Prem1;
    /** 趸交报费（累计） */
    private double Prem2;
    /** 期交首期保费（本期） */
    private double Prem3;
    /** 期交首期保费（累计） */
    private double Prem4;
    /** 期交首年续期保费（本期） */
    private double Prem5;
    /** 期交首年续期保费（累计） */
    private double Prem6;
    /** 续年保费（本期） */
    private double Prem7;
    /** 续年保费（累计） */
    private double Prem8;
    /** 承保人次（本期新增） */
    private double PeoplesCount1;
    /** 承保人次（累计新增） */
    private double PeoplesCount2;
    /** 承保人次（期末有效数） */
    private double PeoplesCount3;
    /** 保单件数（本期新增） */
    private double PolCount1;
    /** 保单件数（累计新增） */
    private double PolCount2;
    /** 保单件数（期末有效数） */
    private double PolCount3;
    /** 保险金额（本期新增） */
    private double Amnt1;
    /** 保险金额（累计新增） */
    private double Amnt2;
    /** 保险金额（期末有效数） */
    private double Amnt3;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 26; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LOReport1Schema()
    {
        mErrors = new CErrors();

        String[] pk = new String[4];
        pk[0] = "ManageCom";
        pk[1] = "RiskCode";
        pk[2] = "SaleChnl";
        pk[3] = "OldMakeDate";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getManageCom()
    {
        if (ManageCom != null && !ManageCom.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ManageCom = StrTool.unicodeToGBK(ManageCom);
        }
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public String getRiskCode()
    {
        if (RiskCode != null && !RiskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getSaleChnl()
    {
        if (SaleChnl != null && !SaleChnl.equals("") && SysConst.CHANGECHARSET == true)
        {
            SaleChnl = StrTool.unicodeToGBK(SaleChnl);
        }
        return SaleChnl;
    }

    public void setSaleChnl(String aSaleChnl)
    {
        SaleChnl = aSaleChnl;
    }

    public String getOldMakeDate()
    {
        if (OldMakeDate != null)
        {
            return fDate.getString(OldMakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setOldMakeDate(Date aOldMakeDate)
    {
        OldMakeDate = aOldMakeDate;
    }

    public void setOldMakeDate(String aOldMakeDate)
    {
        if (aOldMakeDate != null && !aOldMakeDate.equals(""))
        {
            OldMakeDate = fDate.getDate(aOldMakeDate);
        }
        else
        {
            OldMakeDate = null;
        }
    }

    public double getPrem1()
    {
        return Prem1;
    }

    public void setPrem1(double aPrem1)
    {
        Prem1 = aPrem1;
    }

    public void setPrem1(String aPrem1)
    {
        if (aPrem1 != null && !aPrem1.equals(""))
        {
            Double tDouble = new Double(aPrem1);
            double d = tDouble.doubleValue();
            Prem1 = d;
        }
    }

    public double getPrem2()
    {
        return Prem2;
    }

    public void setPrem2(double aPrem2)
    {
        Prem2 = aPrem2;
    }

    public void setPrem2(String aPrem2)
    {
        if (aPrem2 != null && !aPrem2.equals(""))
        {
            Double tDouble = new Double(aPrem2);
            double d = tDouble.doubleValue();
            Prem2 = d;
        }
    }

    public double getPrem3()
    {
        return Prem3;
    }

    public void setPrem3(double aPrem3)
    {
        Prem3 = aPrem3;
    }

    public void setPrem3(String aPrem3)
    {
        if (aPrem3 != null && !aPrem3.equals(""))
        {
            Double tDouble = new Double(aPrem3);
            double d = tDouble.doubleValue();
            Prem3 = d;
        }
    }

    public double getPrem4()
    {
        return Prem4;
    }

    public void setPrem4(double aPrem4)
    {
        Prem4 = aPrem4;
    }

    public void setPrem4(String aPrem4)
    {
        if (aPrem4 != null && !aPrem4.equals(""))
        {
            Double tDouble = new Double(aPrem4);
            double d = tDouble.doubleValue();
            Prem4 = d;
        }
    }

    public double getPrem5()
    {
        return Prem5;
    }

    public void setPrem5(double aPrem5)
    {
        Prem5 = aPrem5;
    }

    public void setPrem5(String aPrem5)
    {
        if (aPrem5 != null && !aPrem5.equals(""))
        {
            Double tDouble = new Double(aPrem5);
            double d = tDouble.doubleValue();
            Prem5 = d;
        }
    }

    public double getPrem6()
    {
        return Prem6;
    }

    public void setPrem6(double aPrem6)
    {
        Prem6 = aPrem6;
    }

    public void setPrem6(String aPrem6)
    {
        if (aPrem6 != null && !aPrem6.equals(""))
        {
            Double tDouble = new Double(aPrem6);
            double d = tDouble.doubleValue();
            Prem6 = d;
        }
    }

    public double getPrem7()
    {
        return Prem7;
    }

    public void setPrem7(double aPrem7)
    {
        Prem7 = aPrem7;
    }

    public void setPrem7(String aPrem7)
    {
        if (aPrem7 != null && !aPrem7.equals(""))
        {
            Double tDouble = new Double(aPrem7);
            double d = tDouble.doubleValue();
            Prem7 = d;
        }
    }

    public double getPrem8()
    {
        return Prem8;
    }

    public void setPrem8(double aPrem8)
    {
        Prem8 = aPrem8;
    }

    public void setPrem8(String aPrem8)
    {
        if (aPrem8 != null && !aPrem8.equals(""))
        {
            Double tDouble = new Double(aPrem8);
            double d = tDouble.doubleValue();
            Prem8 = d;
        }
    }

    public double getPeoplesCount1()
    {
        return PeoplesCount1;
    }

    public void setPeoplesCount1(double aPeoplesCount1)
    {
        PeoplesCount1 = aPeoplesCount1;
    }

    public void setPeoplesCount1(String aPeoplesCount1)
    {
        if (aPeoplesCount1 != null && !aPeoplesCount1.equals(""))
        {
            Double tDouble = new Double(aPeoplesCount1);
            double d = tDouble.doubleValue();
            PeoplesCount1 = d;
        }
    }

    public double getPeoplesCount2()
    {
        return PeoplesCount2;
    }

    public void setPeoplesCount2(double aPeoplesCount2)
    {
        PeoplesCount2 = aPeoplesCount2;
    }

    public void setPeoplesCount2(String aPeoplesCount2)
    {
        if (aPeoplesCount2 != null && !aPeoplesCount2.equals(""))
        {
            Double tDouble = new Double(aPeoplesCount2);
            double d = tDouble.doubleValue();
            PeoplesCount2 = d;
        }
    }

    public double getPeoplesCount3()
    {
        return PeoplesCount3;
    }

    public void setPeoplesCount3(double aPeoplesCount3)
    {
        PeoplesCount3 = aPeoplesCount3;
    }

    public void setPeoplesCount3(String aPeoplesCount3)
    {
        if (aPeoplesCount3 != null && !aPeoplesCount3.equals(""))
        {
            Double tDouble = new Double(aPeoplesCount3);
            double d = tDouble.doubleValue();
            PeoplesCount3 = d;
        }
    }

    public double getPolCount1()
    {
        return PolCount1;
    }

    public void setPolCount1(double aPolCount1)
    {
        PolCount1 = aPolCount1;
    }

    public void setPolCount1(String aPolCount1)
    {
        if (aPolCount1 != null && !aPolCount1.equals(""))
        {
            Double tDouble = new Double(aPolCount1);
            double d = tDouble.doubleValue();
            PolCount1 = d;
        }
    }

    public double getPolCount2()
    {
        return PolCount2;
    }

    public void setPolCount2(double aPolCount2)
    {
        PolCount2 = aPolCount2;
    }

    public void setPolCount2(String aPolCount2)
    {
        if (aPolCount2 != null && !aPolCount2.equals(""))
        {
            Double tDouble = new Double(aPolCount2);
            double d = tDouble.doubleValue();
            PolCount2 = d;
        }
    }

    public double getPolCount3()
    {
        return PolCount3;
    }

    public void setPolCount3(double aPolCount3)
    {
        PolCount3 = aPolCount3;
    }

    public void setPolCount3(String aPolCount3)
    {
        if (aPolCount3 != null && !aPolCount3.equals(""))
        {
            Double tDouble = new Double(aPolCount3);
            double d = tDouble.doubleValue();
            PolCount3 = d;
        }
    }

    public double getAmnt1()
    {
        return Amnt1;
    }

    public void setAmnt1(double aAmnt1)
    {
        Amnt1 = aAmnt1;
    }

    public void setAmnt1(String aAmnt1)
    {
        if (aAmnt1 != null && !aAmnt1.equals(""))
        {
            Double tDouble = new Double(aAmnt1);
            double d = tDouble.doubleValue();
            Amnt1 = d;
        }
    }

    public double getAmnt2()
    {
        return Amnt2;
    }

    public void setAmnt2(double aAmnt2)
    {
        Amnt2 = aAmnt2;
    }

    public void setAmnt2(String aAmnt2)
    {
        if (aAmnt2 != null && !aAmnt2.equals(""))
        {
            Double tDouble = new Double(aAmnt2);
            double d = tDouble.doubleValue();
            Amnt2 = d;
        }
    }

    public double getAmnt3()
    {
        return Amnt3;
    }

    public void setAmnt3(double aAmnt3)
    {
        Amnt3 = aAmnt3;
    }

    public void setAmnt3(String aAmnt3)
    {
        if (aAmnt3 != null && !aAmnt3.equals(""))
        {
            Double tDouble = new Double(aAmnt3);
            double d = tDouble.doubleValue();
            Amnt3 = d;
        }
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LOReport1Schema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LOReport1Schema aLOReport1Schema)
    {
        this.ManageCom = aLOReport1Schema.getManageCom();
        this.RiskCode = aLOReport1Schema.getRiskCode();
        this.SaleChnl = aLOReport1Schema.getSaleChnl();
        this.OldMakeDate = fDate.getDate(aLOReport1Schema.getOldMakeDate());
        this.Prem1 = aLOReport1Schema.getPrem1();
        this.Prem2 = aLOReport1Schema.getPrem2();
        this.Prem3 = aLOReport1Schema.getPrem3();
        this.Prem4 = aLOReport1Schema.getPrem4();
        this.Prem5 = aLOReport1Schema.getPrem5();
        this.Prem6 = aLOReport1Schema.getPrem6();
        this.Prem7 = aLOReport1Schema.getPrem7();
        this.Prem8 = aLOReport1Schema.getPrem8();
        this.PeoplesCount1 = aLOReport1Schema.getPeoplesCount1();
        this.PeoplesCount2 = aLOReport1Schema.getPeoplesCount2();
        this.PeoplesCount3 = aLOReport1Schema.getPeoplesCount3();
        this.PolCount1 = aLOReport1Schema.getPolCount1();
        this.PolCount2 = aLOReport1Schema.getPolCount2();
        this.PolCount3 = aLOReport1Schema.getPolCount3();
        this.Amnt1 = aLOReport1Schema.getAmnt1();
        this.Amnt2 = aLOReport1Schema.getAmnt2();
        this.Amnt3 = aLOReport1Schema.getAmnt3();
        this.Operator = aLOReport1Schema.getOperator();
        this.MakeDate = fDate.getDate(aLOReport1Schema.getMakeDate());
        this.MakeTime = aLOReport1Schema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLOReport1Schema.getModifyDate());
        this.ModifyTime = aLOReport1Schema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("SaleChnl") == null)
            {
                this.SaleChnl = null;
            }
            else
            {
                this.SaleChnl = rs.getString("SaleChnl").trim();
            }

            this.OldMakeDate = rs.getDate("OldMakeDate");
            this.Prem1 = rs.getDouble("Prem1");
            this.Prem2 = rs.getDouble("Prem2");
            this.Prem3 = rs.getDouble("Prem3");
            this.Prem4 = rs.getDouble("Prem4");
            this.Prem5 = rs.getDouble("Prem5");
            this.Prem6 = rs.getDouble("Prem6");
            this.Prem7 = rs.getDouble("Prem7");
            this.Prem8 = rs.getDouble("Prem8");
            this.PeoplesCount1 = rs.getDouble("PeoplesCount1");
            this.PeoplesCount2 = rs.getDouble("PeoplesCount2");
            this.PeoplesCount3 = rs.getDouble("PeoplesCount3");
            this.PolCount1 = rs.getDouble("PolCount1");
            this.PolCount2 = rs.getDouble("PolCount2");
            this.PolCount3 = rs.getDouble("PolCount3");
            this.Amnt1 = rs.getDouble("Amnt1");
            this.Amnt2 = rs.getDouble("Amnt2");
            this.Amnt3 = rs.getDouble("Amnt3");
            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LOReport1Schema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LOReport1Schema getSchema()
    {
        LOReport1Schema aLOReport1Schema = new LOReport1Schema();
        aLOReport1Schema.setSchema(this);
        return aLOReport1Schema;
    }

    public LOReport1DB getDB()
    {
        LOReport1DB aDBOper = new LOReport1DB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOReport1描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(ManageCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SaleChnl)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            OldMakeDate))) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(Prem1) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(Prem2) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(Prem3) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(Prem4) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(Prem5) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(Prem6) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(Prem7) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(Prem8) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(PeoplesCount1) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(PeoplesCount2) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(PeoplesCount3) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(PolCount1) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(PolCount2) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(PolCount3) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(Amnt1) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(Amnt2) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(Amnt3) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOReport1>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            OldMakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 4, SysConst.PACKAGESPILTER));
            Prem1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    5, SysConst.PACKAGESPILTER))).doubleValue();
            Prem2 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    6, SysConst.PACKAGESPILTER))).doubleValue();
            Prem3 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    7, SysConst.PACKAGESPILTER))).doubleValue();
            Prem4 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    8, SysConst.PACKAGESPILTER))).doubleValue();
            Prem5 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    9, SysConst.PACKAGESPILTER))).doubleValue();
            Prem6 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    10, SysConst.PACKAGESPILTER))).doubleValue();
            Prem7 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    11, SysConst.PACKAGESPILTER))).doubleValue();
            Prem8 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    12, SysConst.PACKAGESPILTER))).doubleValue();
            PeoplesCount1 = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 13, SysConst.PACKAGESPILTER))).doubleValue();
            PeoplesCount2 = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 14, SysConst.PACKAGESPILTER))).doubleValue();
            PeoplesCount3 = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 15, SysConst.PACKAGESPILTER))).doubleValue();
            PolCount1 = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 16, SysConst.PACKAGESPILTER))).doubleValue();
            PolCount2 = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 17, SysConst.PACKAGESPILTER))).doubleValue();
            PolCount3 = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 18, SysConst.PACKAGESPILTER))).doubleValue();
            Amnt1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    19, SysConst.PACKAGESPILTER))).doubleValue();
            Amnt2 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    20, SysConst.PACKAGESPILTER))).doubleValue();
            Amnt3 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    21, SysConst.PACKAGESPILTER))).doubleValue();
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 23, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 25, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LOReport1Schema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ManageCom));
        }
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskCode));
        }
        if (FCode.equals("SaleChnl"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SaleChnl));
        }
        if (FCode.equals("OldMakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getOldMakeDate()));
        }
        if (FCode.equals("Prem1"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Prem1));
        }
        if (FCode.equals("Prem2"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Prem2));
        }
        if (FCode.equals("Prem3"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Prem3));
        }
        if (FCode.equals("Prem4"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Prem4));
        }
        if (FCode.equals("Prem5"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Prem5));
        }
        if (FCode.equals("Prem6"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Prem6));
        }
        if (FCode.equals("Prem7"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Prem7));
        }
        if (FCode.equals("Prem8"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Prem8));
        }
        if (FCode.equals("PeoplesCount1"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PeoplesCount1));
        }
        if (FCode.equals("PeoplesCount2"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PeoplesCount2));
        }
        if (FCode.equals("PeoplesCount3"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PeoplesCount3));
        }
        if (FCode.equals("PolCount1"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PolCount1));
        }
        if (FCode.equals("PolCount2"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PolCount2));
        }
        if (FCode.equals("PolCount3"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PolCount3));
        }
        if (FCode.equals("Amnt1"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Amnt1));
        }
        if (FCode.equals("Amnt2"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Amnt2));
        }
        if (FCode.equals("Amnt3"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Amnt3));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(SaleChnl);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getOldMakeDate()));
                break;
            case 4:
                strFieldValue = String.valueOf(Prem1);
                break;
            case 5:
                strFieldValue = String.valueOf(Prem2);
                break;
            case 6:
                strFieldValue = String.valueOf(Prem3);
                break;
            case 7:
                strFieldValue = String.valueOf(Prem4);
                break;
            case 8:
                strFieldValue = String.valueOf(Prem5);
                break;
            case 9:
                strFieldValue = String.valueOf(Prem6);
                break;
            case 10:
                strFieldValue = String.valueOf(Prem7);
                break;
            case 11:
                strFieldValue = String.valueOf(Prem8);
                break;
            case 12:
                strFieldValue = String.valueOf(PeoplesCount1);
                break;
            case 13:
                strFieldValue = String.valueOf(PeoplesCount2);
                break;
            case 14:
                strFieldValue = String.valueOf(PeoplesCount3);
                break;
            case 15:
                strFieldValue = String.valueOf(PolCount1);
                break;
            case 16:
                strFieldValue = String.valueOf(PolCount2);
                break;
            case 17:
                strFieldValue = String.valueOf(PolCount3);
                break;
            case 18:
                strFieldValue = String.valueOf(Amnt1);
                break;
            case 19:
                strFieldValue = String.valueOf(Amnt2);
                break;
            case 20:
                strFieldValue = String.valueOf(Amnt3);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("SaleChnl"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SaleChnl = FValue.trim();
            }
            else
            {
                SaleChnl = null;
            }
        }
        if (FCode.equals("OldMakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OldMakeDate = fDate.getDate(FValue);
            }
            else
            {
                OldMakeDate = null;
            }
        }
        if (FCode.equals("Prem1"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Prem1 = d;
            }
        }
        if (FCode.equals("Prem2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Prem2 = d;
            }
        }
        if (FCode.equals("Prem3"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Prem3 = d;
            }
        }
        if (FCode.equals("Prem4"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Prem4 = d;
            }
        }
        if (FCode.equals("Prem5"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Prem5 = d;
            }
        }
        if (FCode.equals("Prem6"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Prem6 = d;
            }
        }
        if (FCode.equals("Prem7"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Prem7 = d;
            }
        }
        if (FCode.equals("Prem8"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Prem8 = d;
            }
        }
        if (FCode.equals("PeoplesCount1"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                PeoplesCount1 = d;
            }
        }
        if (FCode.equals("PeoplesCount2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                PeoplesCount2 = d;
            }
        }
        if (FCode.equals("PeoplesCount3"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                PeoplesCount3 = d;
            }
        }
        if (FCode.equals("PolCount1"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                PolCount1 = d;
            }
        }
        if (FCode.equals("PolCount2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                PolCount2 = d;
            }
        }
        if (FCode.equals("PolCount3"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                PolCount3 = d;
            }
        }
        if (FCode.equals("Amnt1"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Amnt1 = d;
            }
        }
        if (FCode.equals("Amnt2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Amnt2 = d;
            }
        }
        if (FCode.equals("Amnt3"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Amnt3 = d;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LOReport1Schema other = (LOReport1Schema) otherObject;
        return
                ManageCom.equals(other.getManageCom())
                && RiskCode.equals(other.getRiskCode())
                && SaleChnl.equals(other.getSaleChnl())
                && fDate.getString(OldMakeDate).equals(other.getOldMakeDate())
                && Prem1 == other.getPrem1()
                && Prem2 == other.getPrem2()
                && Prem3 == other.getPrem3()
                && Prem4 == other.getPrem4()
                && Prem5 == other.getPrem5()
                && Prem6 == other.getPrem6()
                && Prem7 == other.getPrem7()
                && Prem8 == other.getPrem8()
                && PeoplesCount1 == other.getPeoplesCount1()
                && PeoplesCount2 == other.getPeoplesCount2()
                && PeoplesCount3 == other.getPeoplesCount3()
                && PolCount1 == other.getPolCount1()
                && PolCount2 == other.getPolCount2()
                && PolCount3 == other.getPolCount3()
                && Amnt1 == other.getAmnt1()
                && Amnt2 == other.getAmnt2()
                && Amnt3 == other.getAmnt3()
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("ManageCom"))
        {
            return 0;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return 1;
        }
        if (strFieldName.equals("SaleChnl"))
        {
            return 2;
        }
        if (strFieldName.equals("OldMakeDate"))
        {
            return 3;
        }
        if (strFieldName.equals("Prem1"))
        {
            return 4;
        }
        if (strFieldName.equals("Prem2"))
        {
            return 5;
        }
        if (strFieldName.equals("Prem3"))
        {
            return 6;
        }
        if (strFieldName.equals("Prem4"))
        {
            return 7;
        }
        if (strFieldName.equals("Prem5"))
        {
            return 8;
        }
        if (strFieldName.equals("Prem6"))
        {
            return 9;
        }
        if (strFieldName.equals("Prem7"))
        {
            return 10;
        }
        if (strFieldName.equals("Prem8"))
        {
            return 11;
        }
        if (strFieldName.equals("PeoplesCount1"))
        {
            return 12;
        }
        if (strFieldName.equals("PeoplesCount2"))
        {
            return 13;
        }
        if (strFieldName.equals("PeoplesCount3"))
        {
            return 14;
        }
        if (strFieldName.equals("PolCount1"))
        {
            return 15;
        }
        if (strFieldName.equals("PolCount2"))
        {
            return 16;
        }
        if (strFieldName.equals("PolCount3"))
        {
            return 17;
        }
        if (strFieldName.equals("Amnt1"))
        {
            return 18;
        }
        if (strFieldName.equals("Amnt2"))
        {
            return 19;
        }
        if (strFieldName.equals("Amnt3"))
        {
            return 20;
        }
        if (strFieldName.equals("Operator"))
        {
            return 21;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 22;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 23;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 24;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 25;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "ManageCom";
                break;
            case 1:
                strFieldName = "RiskCode";
                break;
            case 2:
                strFieldName = "SaleChnl";
                break;
            case 3:
                strFieldName = "OldMakeDate";
                break;
            case 4:
                strFieldName = "Prem1";
                break;
            case 5:
                strFieldName = "Prem2";
                break;
            case 6:
                strFieldName = "Prem3";
                break;
            case 7:
                strFieldName = "Prem4";
                break;
            case 8:
                strFieldName = "Prem5";
                break;
            case 9:
                strFieldName = "Prem6";
                break;
            case 10:
                strFieldName = "Prem7";
                break;
            case 11:
                strFieldName = "Prem8";
                break;
            case 12:
                strFieldName = "PeoplesCount1";
                break;
            case 13:
                strFieldName = "PeoplesCount2";
                break;
            case 14:
                strFieldName = "PeoplesCount3";
                break;
            case 15:
                strFieldName = "PolCount1";
                break;
            case 16:
                strFieldName = "PolCount2";
                break;
            case 17:
                strFieldName = "PolCount3";
                break;
            case 18:
                strFieldName = "Amnt1";
                break;
            case 19:
                strFieldName = "Amnt2";
                break;
            case 20:
                strFieldName = "Amnt3";
                break;
            case 21:
                strFieldName = "Operator";
                break;
            case 22:
                strFieldName = "MakeDate";
                break;
            case 23:
                strFieldName = "MakeTime";
                break;
            case 24:
                strFieldName = "ModifyDate";
                break;
            case 25:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SaleChnl"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OldMakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("Prem1"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Prem2"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Prem3"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Prem4"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Prem5"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Prem6"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Prem7"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Prem8"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("PeoplesCount1"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("PeoplesCount2"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("PeoplesCount3"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("PolCount1"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("PolCount2"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("PolCount3"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Amnt1"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Amnt2"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Amnt3"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 4:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 5:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 6:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 7:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 8:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 9:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 10:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 11:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 12:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 13:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 14:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 15:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 16:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 17:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 18:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 19:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 20:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 21:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 22:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 23:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 24:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 25:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
