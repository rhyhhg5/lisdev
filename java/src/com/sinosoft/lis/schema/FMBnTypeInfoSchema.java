/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.FMBnTypeInfoDB;

/*
 * <p>ClassName: FMBnTypeInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 财务接口最新版
 * @CreateDate：2011-12-13
 */
public class FMBnTypeInfoSchema implements Schema, Cloneable
{
	// @Field
	/** 维护明细号 */
	private String MaintNo;
	/** 版本编号 */
	private String VersionNo;
	/** 业务交易编号 */
	private String BusinessID;
	/** 信息编码 */
	private String PropertyCode;
	/** 信息名称 */
	private String PropertyName;
	/** 分类编码 */
	private String BusClass;
	/** 分类名称 */
	private String BusClassName;
	/** 用途 */
	private String Purpose;
	/** 值域 */
	private String BusValues;
	/** 信息描述 */
	private String Remark;

	public static final int FIELDNUM = 10;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public FMBnTypeInfoSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[5];
		pk[0] = "MaintNo";
		pk[1] = "VersionNo";
		pk[2] = "BusinessID";
		pk[3] = "PropertyCode";
		pk[4] = "Purpose";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		FMBnTypeInfoSchema cloned = (FMBnTypeInfoSchema) super.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getMaintNo()
	{
		return MaintNo;
	}
	public void setMaintNo(String aMaintNo)
	{
		MaintNo = aMaintNo;
	}
	public String getVersionNo()
	{
		return VersionNo;
	}
	public void setVersionNo(String aVersionNo)
	{
		VersionNo = aVersionNo;
	}
	public String getBusinessID()
	{
		return BusinessID;
	}
	public void setBusinessID(String aBusinessID)
	{
		BusinessID = aBusinessID;
	}
	public String getPropertyCode()
	{
		return PropertyCode;
	}
	public void setPropertyCode(String aPropertyCode)
	{
		PropertyCode = aPropertyCode;
	}
	public String getPropertyName()
	{
		return PropertyName;
	}
	public void setPropertyName(String aPropertyName)
	{
		PropertyName = aPropertyName;
	}
	public String getBusClass()
	{
		return BusClass;
	}
	public void setBusClass(String aBusClass)
	{
		BusClass = aBusClass;
	}
	public String getBusClassName()
	{
		return BusClassName;
	}
	public void setBusClassName(String aBusClassName)
	{
		BusClassName = aBusClassName;
	}
	public String getPurpose()
	{
		return Purpose;
	}
	public void setPurpose(String aPurpose)
	{
		Purpose = aPurpose;
	}
	public String getBusValues()
	{
		return BusValues;
	}
	public void setBusValues(String aBusValues)
	{
		BusValues = aBusValues;
	}
	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
		Remark = aRemark;
	}

	/**
	* 使用另外一个 FMBnTypeInfoSchema 对象给 Schema 赋值
	* @param: aFMBnTypeInfoSchema FMBnTypeInfoSchema
	**/
	public void setSchema(FMBnTypeInfoSchema aFMBnTypeInfoSchema)
	{
		this.MaintNo = aFMBnTypeInfoSchema.getMaintNo();
		this.VersionNo = aFMBnTypeInfoSchema.getVersionNo();
		this.BusinessID = aFMBnTypeInfoSchema.getBusinessID();
		this.PropertyCode = aFMBnTypeInfoSchema.getPropertyCode();
		this.PropertyName = aFMBnTypeInfoSchema.getPropertyName();
		this.BusClass = aFMBnTypeInfoSchema.getBusClass();
		this.BusClassName = aFMBnTypeInfoSchema.getBusClassName();
		this.Purpose = aFMBnTypeInfoSchema.getPurpose();
		this.BusValues = aFMBnTypeInfoSchema.getBusValues();
		this.Remark = aFMBnTypeInfoSchema.getRemark();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("MaintNo") == null )
				this.MaintNo = null;
			else
				this.MaintNo = rs.getString("MaintNo").trim();

			if( rs.getString("VersionNo") == null )
				this.VersionNo = null;
			else
				this.VersionNo = rs.getString("VersionNo").trim();

			if( rs.getString("BusinessID") == null )
				this.BusinessID = null;
			else
				this.BusinessID = rs.getString("BusinessID").trim();

			if( rs.getString("PropertyCode") == null )
				this.PropertyCode = null;
			else
				this.PropertyCode = rs.getString("PropertyCode").trim();

			if( rs.getString("PropertyName") == null )
				this.PropertyName = null;
			else
				this.PropertyName = rs.getString("PropertyName").trim();

			if( rs.getString("BusClass") == null )
				this.BusClass = null;
			else
				this.BusClass = rs.getString("BusClass").trim();

			if( rs.getString("BusClassName") == null )
				this.BusClassName = null;
			else
				this.BusClassName = rs.getString("BusClassName").trim();

			if( rs.getString("Purpose") == null )
				this.Purpose = null;
			else
				this.Purpose = rs.getString("Purpose").trim();

			if( rs.getString("BusValues") == null )
				this.BusValues = null;
			else
				this.BusValues = rs.getString("BusValues").trim();

			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的FMBnTypeInfo表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FMBnTypeInfoSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public FMBnTypeInfoSchema getSchema()
	{
		FMBnTypeInfoSchema aFMBnTypeInfoSchema = new FMBnTypeInfoSchema();
		aFMBnTypeInfoSchema.setSchema(this);
		return aFMBnTypeInfoSchema;
	}

	public FMBnTypeInfoDB getDB()
	{
		FMBnTypeInfoDB aDBOper = new FMBnTypeInfoDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpFMBnTypeInfo描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(MaintNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(VersionNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusinessID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PropertyCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PropertyName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusClass)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusClassName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Purpose)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusValues)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpFMBnTypeInfo>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			MaintNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			VersionNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			BusinessID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			PropertyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			PropertyName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			BusClass = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			BusClassName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			Purpose = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			BusValues = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FMBnTypeInfoSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("MaintNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MaintNo));
		}
		if (FCode.equals("VersionNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(VersionNo));
		}
		if (FCode.equals("BusinessID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusinessID));
		}
		if (FCode.equals("PropertyCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PropertyCode));
		}
		if (FCode.equals("PropertyName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PropertyName));
		}
		if (FCode.equals("BusClass"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusClass));
		}
		if (FCode.equals("BusClassName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusClassName));
		}
		if (FCode.equals("Purpose"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Purpose));
		}
		if (FCode.equals("BusValues"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusValues));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(MaintNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(VersionNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(BusinessID);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(PropertyCode);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(PropertyName);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(BusClass);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(BusClassName);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(Purpose);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(BusValues);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("MaintNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MaintNo = FValue.trim();
			}
			else
				MaintNo = null;
		}
		if (FCode.equalsIgnoreCase("VersionNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				VersionNo = FValue.trim();
			}
			else
				VersionNo = null;
		}
		if (FCode.equalsIgnoreCase("BusinessID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusinessID = FValue.trim();
			}
			else
				BusinessID = null;
		}
		if (FCode.equalsIgnoreCase("PropertyCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PropertyCode = FValue.trim();
			}
			else
				PropertyCode = null;
		}
		if (FCode.equalsIgnoreCase("PropertyName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PropertyName = FValue.trim();
			}
			else
				PropertyName = null;
		}
		if (FCode.equalsIgnoreCase("BusClass"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusClass = FValue.trim();
			}
			else
				BusClass = null;
		}
		if (FCode.equalsIgnoreCase("BusClassName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusClassName = FValue.trim();
			}
			else
				BusClassName = null;
		}
		if (FCode.equalsIgnoreCase("Purpose"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Purpose = FValue.trim();
			}
			else
				Purpose = null;
		}
		if (FCode.equalsIgnoreCase("BusValues"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusValues = FValue.trim();
			}
			else
				BusValues = null;
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		FMBnTypeInfoSchema other = (FMBnTypeInfoSchema)otherObject;
		return
			(MaintNo == null ? other.getMaintNo() == null : MaintNo.equals(other.getMaintNo()))
			&& (VersionNo == null ? other.getVersionNo() == null : VersionNo.equals(other.getVersionNo()))
			&& (BusinessID == null ? other.getBusinessID() == null : BusinessID.equals(other.getBusinessID()))
			&& (PropertyCode == null ? other.getPropertyCode() == null : PropertyCode.equals(other.getPropertyCode()))
			&& (PropertyName == null ? other.getPropertyName() == null : PropertyName.equals(other.getPropertyName()))
			&& (BusClass == null ? other.getBusClass() == null : BusClass.equals(other.getBusClass()))
			&& (BusClassName == null ? other.getBusClassName() == null : BusClassName.equals(other.getBusClassName()))
			&& (Purpose == null ? other.getPurpose() == null : Purpose.equals(other.getPurpose()))
			&& (BusValues == null ? other.getBusValues() == null : BusValues.equals(other.getBusValues()))
			&& (Remark == null ? other.getRemark() == null : Remark.equals(other.getRemark()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("MaintNo") ) {
			return 0;
		}
		if( strFieldName.equals("VersionNo") ) {
			return 1;
		}
		if( strFieldName.equals("BusinessID") ) {
			return 2;
		}
		if( strFieldName.equals("PropertyCode") ) {
			return 3;
		}
		if( strFieldName.equals("PropertyName") ) {
			return 4;
		}
		if( strFieldName.equals("BusClass") ) {
			return 5;
		}
		if( strFieldName.equals("BusClassName") ) {
			return 6;
		}
		if( strFieldName.equals("Purpose") ) {
			return 7;
		}
		if( strFieldName.equals("BusValues") ) {
			return 8;
		}
		if( strFieldName.equals("Remark") ) {
			return 9;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "MaintNo";
				break;
			case 1:
				strFieldName = "VersionNo";
				break;
			case 2:
				strFieldName = "BusinessID";
				break;
			case 3:
				strFieldName = "PropertyCode";
				break;
			case 4:
				strFieldName = "PropertyName";
				break;
			case 5:
				strFieldName = "BusClass";
				break;
			case 6:
				strFieldName = "BusClassName";
				break;
			case 7:
				strFieldName = "Purpose";
				break;
			case 8:
				strFieldName = "BusValues";
				break;
			case 9:
				strFieldName = "Remark";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("MaintNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("VersionNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BusinessID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PropertyCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PropertyName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BusClass") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BusClassName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Purpose") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BusValues") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
