/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LAAgentPromRadix1DB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LAAgentPromRadix1Schema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-03-30
 */
public class LAAgentPromRadix1Schema implements Schema
{
    // @Field
    /** 代理人职级 */
    private String AgentGrade;
    /** 展业类型 */
    private String BranchType;
    /** 目标代理人职级 */
    private String DestAgentGrade;
    /** 版本号 */
    private String AreaType;
    /** 渠道类型 */
    private String ChannelType;
    /** 考核类型 */
    private String AssessCode;
    /** 现任职级要求的达到考核期限 */
    private int YearCode;
    /** 考核期限 */
    private String LimitPeriod;
    /** 降级/晋升起期 */
    private String StartDate;
    /** 通过品质考核标记 */
    private String QuaPassFlag;
    /** 通过培训并考试合格标记 */
    private String ExamPassFlag;
    /** 分数上限 */
    private double MarkBegin;
    /** 通过工作考核及素质考评标记 */
    private String WorkPassFlag;
    /** 分数下限 */
    private double MarkEnd;
    /** 所降级数 */
    private int DropGrade;
    /** 渠道 */
    private String BranchType2;

    public static final int FIELDNUM = 16; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LAAgentPromRadix1Schema()
    {
        mErrors = new CErrors();

        String[] pk = new String[6];
        pk[0] = "AgentGrade";
        pk[1] = "DestAgentGrade";
        pk[2] = "AreaType";
        pk[3] = "ChannelType";
        pk[4] = "AssessCode";
        pk[5] = "LimitPeriod";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getAgentGrade()
    {
        if (SysConst.CHANGECHARSET && AgentGrade != null &&
            !AgentGrade.equals(""))
        {
            AgentGrade = StrTool.unicodeToGBK(AgentGrade);
        }
        return AgentGrade;
    }

    public void setAgentGrade(String aAgentGrade)
    {
        AgentGrade = aAgentGrade;
    }

    public String getBranchType()
    {
        if (SysConst.CHANGECHARSET && BranchType != null &&
            !BranchType.equals(""))
        {
            BranchType = StrTool.unicodeToGBK(BranchType);
        }
        return BranchType;
    }

    public void setBranchType(String aBranchType)
    {
        BranchType = aBranchType;
    }

    public String getDestAgentGrade()
    {
        if (SysConst.CHANGECHARSET && DestAgentGrade != null &&
            !DestAgentGrade.equals(""))
        {
            DestAgentGrade = StrTool.unicodeToGBK(DestAgentGrade);
        }
        return DestAgentGrade;
    }

    public void setDestAgentGrade(String aDestAgentGrade)
    {
        DestAgentGrade = aDestAgentGrade;
    }

    public String getAreaType()
    {
        if (SysConst.CHANGECHARSET && AreaType != null && !AreaType.equals(""))
        {
            AreaType = StrTool.unicodeToGBK(AreaType);
        }
        return AreaType;
    }

    public void setAreaType(String aAreaType)
    {
        AreaType = aAreaType;
    }

    public String getChannelType()
    {
        if (SysConst.CHANGECHARSET && ChannelType != null &&
            !ChannelType.equals(""))
        {
            ChannelType = StrTool.unicodeToGBK(ChannelType);
        }
        return ChannelType;
    }

    public void setChannelType(String aChannelType)
    {
        ChannelType = aChannelType;
    }

    public String getAssessCode()
    {
        if (SysConst.CHANGECHARSET && AssessCode != null &&
            !AssessCode.equals(""))
        {
            AssessCode = StrTool.unicodeToGBK(AssessCode);
        }
        return AssessCode;
    }

    public void setAssessCode(String aAssessCode)
    {
        AssessCode = aAssessCode;
    }

    public int getYearCode()
    {
        return YearCode;
    }

    public void setYearCode(int aYearCode)
    {
        YearCode = aYearCode;
    }

    public void setYearCode(String aYearCode)
    {
        if (aYearCode != null && !aYearCode.equals(""))
        {
            Integer tInteger = new Integer(aYearCode);
            int i = tInteger.intValue();
            YearCode = i;
        }
    }

    public String getLimitPeriod()
    {
        if (SysConst.CHANGECHARSET && LimitPeriod != null &&
            !LimitPeriod.equals(""))
        {
            LimitPeriod = StrTool.unicodeToGBK(LimitPeriod);
        }
        return LimitPeriod;
    }

    public void setLimitPeriod(String aLimitPeriod)
    {
        LimitPeriod = aLimitPeriod;
    }

    public String getStartDate()
    {
        if (SysConst.CHANGECHARSET && StartDate != null && !StartDate.equals(""))
        {
            StartDate = StrTool.unicodeToGBK(StartDate);
        }
        return StartDate;
    }

    public void setStartDate(String aStartDate)
    {
        StartDate = aStartDate;
    }

    public String getQuaPassFlag()
    {
        if (SysConst.CHANGECHARSET && QuaPassFlag != null &&
            !QuaPassFlag.equals(""))
        {
            QuaPassFlag = StrTool.unicodeToGBK(QuaPassFlag);
        }
        return QuaPassFlag;
    }

    public void setQuaPassFlag(String aQuaPassFlag)
    {
        QuaPassFlag = aQuaPassFlag;
    }

    public String getExamPassFlag()
    {
        if (SysConst.CHANGECHARSET && ExamPassFlag != null &&
            !ExamPassFlag.equals(""))
        {
            ExamPassFlag = StrTool.unicodeToGBK(ExamPassFlag);
        }
        return ExamPassFlag;
    }

    public void setExamPassFlag(String aExamPassFlag)
    {
        ExamPassFlag = aExamPassFlag;
    }

    public double getMarkBegin()
    {
        return MarkBegin;
    }

    public void setMarkBegin(double aMarkBegin)
    {
        MarkBegin = aMarkBegin;
    }

    public void setMarkBegin(String aMarkBegin)
    {
        if (aMarkBegin != null && !aMarkBegin.equals(""))
        {
            Double tDouble = new Double(aMarkBegin);
            double d = tDouble.doubleValue();
            MarkBegin = d;
        }
    }

    public String getWorkPassFlag()
    {
        if (SysConst.CHANGECHARSET && WorkPassFlag != null &&
            !WorkPassFlag.equals(""))
        {
            WorkPassFlag = StrTool.unicodeToGBK(WorkPassFlag);
        }
        return WorkPassFlag;
    }

    public void setWorkPassFlag(String aWorkPassFlag)
    {
        WorkPassFlag = aWorkPassFlag;
    }

    public double getMarkEnd()
    {
        return MarkEnd;
    }

    public void setMarkEnd(double aMarkEnd)
    {
        MarkEnd = aMarkEnd;
    }

    public void setMarkEnd(String aMarkEnd)
    {
        if (aMarkEnd != null && !aMarkEnd.equals(""))
        {
            Double tDouble = new Double(aMarkEnd);
            double d = tDouble.doubleValue();
            MarkEnd = d;
        }
    }

    public int getDropGrade()
    {
        return DropGrade;
    }

    public void setDropGrade(int aDropGrade)
    {
        DropGrade = aDropGrade;
    }

    public void setDropGrade(String aDropGrade)
    {
        if (aDropGrade != null && !aDropGrade.equals(""))
        {
            Integer tInteger = new Integer(aDropGrade);
            int i = tInteger.intValue();
            DropGrade = i;
        }
    }

    public String getBranchType2()
    {
        if (SysConst.CHANGECHARSET && BranchType2 != null &&
            !BranchType2.equals(""))
        {
            BranchType2 = StrTool.unicodeToGBK(BranchType2);
        }
        return BranchType2;
    }

    public void setBranchType2(String aBranchType2)
    {
        BranchType2 = aBranchType2;
    }

    /**
     * 使用另外一个 LAAgentPromRadix1Schema 对象给 Schema 赋值
     * @param: aLAAgentPromRadix1Schema LAAgentPromRadix1Schema
     **/
    public void setSchema(LAAgentPromRadix1Schema aLAAgentPromRadix1Schema)
    {
        this.AgentGrade = aLAAgentPromRadix1Schema.getAgentGrade();
        this.BranchType = aLAAgentPromRadix1Schema.getBranchType();
        this.DestAgentGrade = aLAAgentPromRadix1Schema.getDestAgentGrade();
        this.AreaType = aLAAgentPromRadix1Schema.getAreaType();
        this.ChannelType = aLAAgentPromRadix1Schema.getChannelType();
        this.AssessCode = aLAAgentPromRadix1Schema.getAssessCode();
        this.YearCode = aLAAgentPromRadix1Schema.getYearCode();
        this.LimitPeriod = aLAAgentPromRadix1Schema.getLimitPeriod();
        this.StartDate = aLAAgentPromRadix1Schema.getStartDate();
        this.QuaPassFlag = aLAAgentPromRadix1Schema.getQuaPassFlag();
        this.ExamPassFlag = aLAAgentPromRadix1Schema.getExamPassFlag();
        this.MarkBegin = aLAAgentPromRadix1Schema.getMarkBegin();
        this.WorkPassFlag = aLAAgentPromRadix1Schema.getWorkPassFlag();
        this.MarkEnd = aLAAgentPromRadix1Schema.getMarkEnd();
        this.DropGrade = aLAAgentPromRadix1Schema.getDropGrade();
        this.BranchType2 = aLAAgentPromRadix1Schema.getBranchType2();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("AgentGrade") == null)
            {
                this.AgentGrade = null;
            }
            else
            {
                this.AgentGrade = rs.getString("AgentGrade").trim();
            }

            if (rs.getString("BranchType") == null)
            {
                this.BranchType = null;
            }
            else
            {
                this.BranchType = rs.getString("BranchType").trim();
            }

            if (rs.getString("DestAgentGrade") == null)
            {
                this.DestAgentGrade = null;
            }
            else
            {
                this.DestAgentGrade = rs.getString("DestAgentGrade").trim();
            }

            if (rs.getString("AreaType") == null)
            {
                this.AreaType = null;
            }
            else
            {
                this.AreaType = rs.getString("AreaType").trim();
            }

            if (rs.getString("ChannelType") == null)
            {
                this.ChannelType = null;
            }
            else
            {
                this.ChannelType = rs.getString("ChannelType").trim();
            }

            if (rs.getString("AssessCode") == null)
            {
                this.AssessCode = null;
            }
            else
            {
                this.AssessCode = rs.getString("AssessCode").trim();
            }

            this.YearCode = rs.getInt("YearCode");
            if (rs.getString("LimitPeriod") == null)
            {
                this.LimitPeriod = null;
            }
            else
            {
                this.LimitPeriod = rs.getString("LimitPeriod").trim();
            }

            if (rs.getString("StartDate") == null)
            {
                this.StartDate = null;
            }
            else
            {
                this.StartDate = rs.getString("StartDate").trim();
            }

            if (rs.getString("QuaPassFlag") == null)
            {
                this.QuaPassFlag = null;
            }
            else
            {
                this.QuaPassFlag = rs.getString("QuaPassFlag").trim();
            }

            if (rs.getString("ExamPassFlag") == null)
            {
                this.ExamPassFlag = null;
            }
            else
            {
                this.ExamPassFlag = rs.getString("ExamPassFlag").trim();
            }

            this.MarkBegin = rs.getDouble("MarkBegin");
            if (rs.getString("WorkPassFlag") == null)
            {
                this.WorkPassFlag = null;
            }
            else
            {
                this.WorkPassFlag = rs.getString("WorkPassFlag").trim();
            }

            this.MarkEnd = rs.getDouble("MarkEnd");
            this.DropGrade = rs.getInt("DropGrade");
            if (rs.getString("BranchType2") == null)
            {
                this.BranchType2 = null;
            }
            else
            {
                this.BranchType2 = rs.getString("BranchType2").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentPromRadix1Schema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LAAgentPromRadix1Schema getSchema()
    {
        LAAgentPromRadix1Schema aLAAgentPromRadix1Schema = new
                LAAgentPromRadix1Schema();
        aLAAgentPromRadix1Schema.setSchema(this);
        return aLAAgentPromRadix1Schema;
    }

    public LAAgentPromRadix1DB getDB()
    {
        LAAgentPromRadix1DB aDBOper = new LAAgentPromRadix1DB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAAgentPromRadix1描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AgentGrade)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BranchType)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(DestAgentGrade)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AreaType)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ChannelType)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AssessCode)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(YearCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(LimitPeriod)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(StartDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(QuaPassFlag)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ExamPassFlag)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(MarkBegin));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(WorkPassFlag)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(MarkEnd));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DropGrade));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BranchType2)));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAAgentPromRadix1>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            AgentGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                        SysConst.PACKAGESPILTER);
            BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
            DestAgentGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                            SysConst.PACKAGESPILTER);
            AreaType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                      SysConst.PACKAGESPILTER);
            ChannelType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                         SysConst.PACKAGESPILTER);
            AssessCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                        SysConst.PACKAGESPILTER);
            YearCode = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 7, SysConst.PACKAGESPILTER))).intValue();
            LimitPeriod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                         SysConst.PACKAGESPILTER);
            StartDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                       SysConst.PACKAGESPILTER);
            QuaPassFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                         SysConst.PACKAGESPILTER);
            ExamPassFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                          SysConst.PACKAGESPILTER);
            MarkBegin = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 12, SysConst.PACKAGESPILTER))).doubleValue();
            WorkPassFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                          SysConst.PACKAGESPILTER);
            MarkEnd = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 14, SysConst.PACKAGESPILTER))).doubleValue();
            DropGrade = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 15, SysConst.PACKAGESPILTER))).intValue();
            BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                         SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentPromRadix1Schema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("AgentGrade"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGrade));
        }
        if (FCode.equals("BranchType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
        }
        if (FCode.equals("DestAgentGrade"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DestAgentGrade));
        }
        if (FCode.equals("AreaType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AreaType));
        }
        if (FCode.equals("ChannelType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChannelType));
        }
        if (FCode.equals("AssessCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AssessCode));
        }
        if (FCode.equals("YearCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(YearCode));
        }
        if (FCode.equals("LimitPeriod"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LimitPeriod));
        }
        if (FCode.equals("StartDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StartDate));
        }
        if (FCode.equals("QuaPassFlag"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(QuaPassFlag));
        }
        if (FCode.equals("ExamPassFlag"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExamPassFlag));
        }
        if (FCode.equals("MarkBegin"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MarkBegin));
        }
        if (FCode.equals("WorkPassFlag"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(WorkPassFlag));
        }
        if (FCode.equals("MarkEnd"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MarkEnd));
        }
        if (FCode.equals("DropGrade"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DropGrade));
        }
        if (FCode.equals("BranchType2"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(AgentGrade);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(BranchType);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(DestAgentGrade);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(AreaType);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(ChannelType);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(AssessCode);
                break;
            case 6:
                strFieldValue = String.valueOf(YearCode);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(LimitPeriod);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(StartDate);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(QuaPassFlag);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(ExamPassFlag);
                break;
            case 11:
                strFieldValue = String.valueOf(MarkBegin);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(WorkPassFlag);
                break;
            case 13:
                strFieldValue = String.valueOf(MarkEnd);
                break;
            case 14:
                strFieldValue = String.valueOf(DropGrade);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(BranchType2);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("AgentGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentGrade = FValue.trim();
            }
            else
            {
                AgentGrade = null;
            }
        }
        if (FCode.equals("BranchType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchType = FValue.trim();
            }
            else
            {
                BranchType = null;
            }
        }
        if (FCode.equals("DestAgentGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DestAgentGrade = FValue.trim();
            }
            else
            {
                DestAgentGrade = null;
            }
        }
        if (FCode.equals("AreaType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AreaType = FValue.trim();
            }
            else
            {
                AreaType = null;
            }
        }
        if (FCode.equals("ChannelType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ChannelType = FValue.trim();
            }
            else
            {
                ChannelType = null;
            }
        }
        if (FCode.equals("AssessCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AssessCode = FValue.trim();
            }
            else
            {
                AssessCode = null;
            }
        }
        if (FCode.equals("YearCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                YearCode = i;
            }
        }
        if (FCode.equals("LimitPeriod"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                LimitPeriod = FValue.trim();
            }
            else
            {
                LimitPeriod = null;
            }
        }
        if (FCode.equals("StartDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                StartDate = FValue.trim();
            }
            else
            {
                StartDate = null;
            }
        }
        if (FCode.equals("QuaPassFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                QuaPassFlag = FValue.trim();
            }
            else
            {
                QuaPassFlag = null;
            }
        }
        if (FCode.equals("ExamPassFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ExamPassFlag = FValue.trim();
            }
            else
            {
                ExamPassFlag = null;
            }
        }
        if (FCode.equals("MarkBegin"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                MarkBegin = d;
            }
        }
        if (FCode.equals("WorkPassFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                WorkPassFlag = FValue.trim();
            }
            else
            {
                WorkPassFlag = null;
            }
        }
        if (FCode.equals("MarkEnd"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                MarkEnd = d;
            }
        }
        if (FCode.equals("DropGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                DropGrade = i;
            }
        }
        if (FCode.equals("BranchType2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchType2 = FValue.trim();
            }
            else
            {
                BranchType2 = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LAAgentPromRadix1Schema other = (LAAgentPromRadix1Schema) otherObject;
        return
                AgentGrade.equals(other.getAgentGrade())
                && BranchType.equals(other.getBranchType())
                && DestAgentGrade.equals(other.getDestAgentGrade())
                && AreaType.equals(other.getAreaType())
                && ChannelType.equals(other.getChannelType())
                && AssessCode.equals(other.getAssessCode())
                && YearCode == other.getYearCode()
                && LimitPeriod.equals(other.getLimitPeriod())
                && StartDate.equals(other.getStartDate())
                && QuaPassFlag.equals(other.getQuaPassFlag())
                && ExamPassFlag.equals(other.getExamPassFlag())
                && MarkBegin == other.getMarkBegin()
                && WorkPassFlag.equals(other.getWorkPassFlag())
                && MarkEnd == other.getMarkEnd()
                && DropGrade == other.getDropGrade()
                && BranchType2.equals(other.getBranchType2());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("AgentGrade"))
        {
            return 0;
        }
        if (strFieldName.equals("BranchType"))
        {
            return 1;
        }
        if (strFieldName.equals("DestAgentGrade"))
        {
            return 2;
        }
        if (strFieldName.equals("AreaType"))
        {
            return 3;
        }
        if (strFieldName.equals("ChannelType"))
        {
            return 4;
        }
        if (strFieldName.equals("AssessCode"))
        {
            return 5;
        }
        if (strFieldName.equals("YearCode"))
        {
            return 6;
        }
        if (strFieldName.equals("LimitPeriod"))
        {
            return 7;
        }
        if (strFieldName.equals("StartDate"))
        {
            return 8;
        }
        if (strFieldName.equals("QuaPassFlag"))
        {
            return 9;
        }
        if (strFieldName.equals("ExamPassFlag"))
        {
            return 10;
        }
        if (strFieldName.equals("MarkBegin"))
        {
            return 11;
        }
        if (strFieldName.equals("WorkPassFlag"))
        {
            return 12;
        }
        if (strFieldName.equals("MarkEnd"))
        {
            return 13;
        }
        if (strFieldName.equals("DropGrade"))
        {
            return 14;
        }
        if (strFieldName.equals("BranchType2"))
        {
            return 15;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "AgentGrade";
                break;
            case 1:
                strFieldName = "BranchType";
                break;
            case 2:
                strFieldName = "DestAgentGrade";
                break;
            case 3:
                strFieldName = "AreaType";
                break;
            case 4:
                strFieldName = "ChannelType";
                break;
            case 5:
                strFieldName = "AssessCode";
                break;
            case 6:
                strFieldName = "YearCode";
                break;
            case 7:
                strFieldName = "LimitPeriod";
                break;
            case 8:
                strFieldName = "StartDate";
                break;
            case 9:
                strFieldName = "QuaPassFlag";
                break;
            case 10:
                strFieldName = "ExamPassFlag";
                break;
            case 11:
                strFieldName = "MarkBegin";
                break;
            case 12:
                strFieldName = "WorkPassFlag";
                break;
            case 13:
                strFieldName = "MarkEnd";
                break;
            case 14:
                strFieldName = "DropGrade";
                break;
            case 15:
                strFieldName = "BranchType2";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("AgentGrade"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DestAgentGrade"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AreaType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ChannelType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AssessCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("YearCode"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("LimitPeriod"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StartDate"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("QuaPassFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ExamPassFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MarkBegin"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("WorkPassFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MarkEnd"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("DropGrade"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("BranchType2"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_INT;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 14:
                nFieldType = Schema.TYPE_INT;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
