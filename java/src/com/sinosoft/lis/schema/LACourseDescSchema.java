/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LACourseDescDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LACourseDescSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-04-01
 */
public class LACourseDescSchema implements Schema
{
    // @Field
    /** 课程编码 */
    private String CourseID;
    /** 展业类型 */
    private String BranchType;
    /** 渠道 */
    private String BranchType2;
    /** 课程类别 */
    private String CourseType;
    /** 课程系列 */
    private String CourseSeries;
    /** 课程级别 */
    private String CourseGrade;
    /** 课程序号 */
    private String CourseSN;
    /** 课程名称 */
    private String CourseName;
    /** 总课时 */
    private double Hour;
    /** 学分 */
    private double Profit;
    /** 讲师职级 */
    private String TeacherGrade;
    /** 停课标记 */
    private String StopFlag;
    /** 备注 */
    private String Noti;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最近一次修改日期 */
    private Date ModifyDate;
    /** 最近一次修改时间 */
    private String ModifyTime;
    /** 操作员 */
    private String Operator;

    public static final int FIELDNUM = 18; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LACourseDescSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "CourseID";
        pk[1] = "BranchType";
        pk[2] = "BranchType2";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getCourseID()
    {
        if (SysConst.CHANGECHARSET && CourseID != null && !CourseID.equals(""))
        {
            CourseID = StrTool.unicodeToGBK(CourseID);
        }
        return CourseID;
    }

    public void setCourseID(String aCourseID)
    {
        CourseID = aCourseID;
    }

    public String getBranchType()
    {
        if (SysConst.CHANGECHARSET && BranchType != null &&
            !BranchType.equals(""))
        {
            BranchType = StrTool.unicodeToGBK(BranchType);
        }
        return BranchType;
    }

    public void setBranchType(String aBranchType)
    {
        BranchType = aBranchType;
    }

    public String getBranchType2()
    {
        if (SysConst.CHANGECHARSET && BranchType2 != null &&
            !BranchType2.equals(""))
        {
            BranchType2 = StrTool.unicodeToGBK(BranchType2);
        }
        return BranchType2;
    }

    public void setBranchType2(String aBranchType2)
    {
        BranchType2 = aBranchType2;
    }

    public String getCourseType()
    {
        if (SysConst.CHANGECHARSET && CourseType != null &&
            !CourseType.equals(""))
        {
            CourseType = StrTool.unicodeToGBK(CourseType);
        }
        return CourseType;
    }

    public void setCourseType(String aCourseType)
    {
        CourseType = aCourseType;
    }

    public String getCourseSeries()
    {
        if (SysConst.CHANGECHARSET && CourseSeries != null &&
            !CourseSeries.equals(""))
        {
            CourseSeries = StrTool.unicodeToGBK(CourseSeries);
        }
        return CourseSeries;
    }

    public void setCourseSeries(String aCourseSeries)
    {
        CourseSeries = aCourseSeries;
    }

    public String getCourseGrade()
    {
        if (SysConst.CHANGECHARSET && CourseGrade != null &&
            !CourseGrade.equals(""))
        {
            CourseGrade = StrTool.unicodeToGBK(CourseGrade);
        }
        return CourseGrade;
    }

    public void setCourseGrade(String aCourseGrade)
    {
        CourseGrade = aCourseGrade;
    }

    public String getCourseSN()
    {
        if (SysConst.CHANGECHARSET && CourseSN != null && !CourseSN.equals(""))
        {
            CourseSN = StrTool.unicodeToGBK(CourseSN);
        }
        return CourseSN;
    }

    public void setCourseSN(String aCourseSN)
    {
        CourseSN = aCourseSN;
    }

    public String getCourseName()
    {
        if (SysConst.CHANGECHARSET && CourseName != null &&
            !CourseName.equals(""))
        {
            CourseName = StrTool.unicodeToGBK(CourseName);
        }
        return CourseName;
    }

    public void setCourseName(String aCourseName)
    {
        CourseName = aCourseName;
    }

    public double getHour()
    {
        return Hour;
    }

    public void setHour(double aHour)
    {
        Hour = aHour;
    }

    public void setHour(String aHour)
    {
        if (aHour != null && !aHour.equals(""))
        {
            Double tDouble = new Double(aHour);
            double d = tDouble.doubleValue();
            Hour = d;
        }
    }

    public double getProfit()
    {
        return Profit;
    }

    public void setProfit(double aProfit)
    {
        Profit = aProfit;
    }

    public void setProfit(String aProfit)
    {
        if (aProfit != null && !aProfit.equals(""))
        {
            Double tDouble = new Double(aProfit);
            double d = tDouble.doubleValue();
            Profit = d;
        }
    }

    public String getTeacherGrade()
    {
        if (SysConst.CHANGECHARSET && TeacherGrade != null &&
            !TeacherGrade.equals(""))
        {
            TeacherGrade = StrTool.unicodeToGBK(TeacherGrade);
        }
        return TeacherGrade;
    }

    public void setTeacherGrade(String aTeacherGrade)
    {
        TeacherGrade = aTeacherGrade;
    }

    public String getStopFlag()
    {
        if (SysConst.CHANGECHARSET && StopFlag != null && !StopFlag.equals(""))
        {
            StopFlag = StrTool.unicodeToGBK(StopFlag);
        }
        return StopFlag;
    }

    public void setStopFlag(String aStopFlag)
    {
        StopFlag = aStopFlag;
    }

    public String getNoti()
    {
        if (SysConst.CHANGECHARSET && Noti != null && !Noti.equals(""))
        {
            Noti = StrTool.unicodeToGBK(Noti);
        }
        return Noti;
    }

    public void setNoti(String aNoti)
    {
        Noti = aNoti;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (SysConst.CHANGECHARSET && MakeTime != null && !MakeTime.equals(""))
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (SysConst.CHANGECHARSET && ModifyTime != null &&
            !ModifyTime.equals(""))
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    public String getOperator()
    {
        if (SysConst.CHANGECHARSET && Operator != null && !Operator.equals(""))
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    /**
     * 使用另外一个 LACourseDescSchema 对象给 Schema 赋值
     * @param: aLACourseDescSchema LACourseDescSchema
     **/
    public void setSchema(LACourseDescSchema aLACourseDescSchema)
    {
        this.CourseID = aLACourseDescSchema.getCourseID();
        this.BranchType = aLACourseDescSchema.getBranchType();
        this.BranchType2 = aLACourseDescSchema.getBranchType2();
        this.CourseType = aLACourseDescSchema.getCourseType();
        this.CourseSeries = aLACourseDescSchema.getCourseSeries();
        this.CourseGrade = aLACourseDescSchema.getCourseGrade();
        this.CourseSN = aLACourseDescSchema.getCourseSN();
        this.CourseName = aLACourseDescSchema.getCourseName();
        this.Hour = aLACourseDescSchema.getHour();
        this.Profit = aLACourseDescSchema.getProfit();
        this.TeacherGrade = aLACourseDescSchema.getTeacherGrade();
        this.StopFlag = aLACourseDescSchema.getStopFlag();
        this.Noti = aLACourseDescSchema.getNoti();
        this.MakeDate = fDate.getDate(aLACourseDescSchema.getMakeDate());
        this.MakeTime = aLACourseDescSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLACourseDescSchema.getModifyDate());
        this.ModifyTime = aLACourseDescSchema.getModifyTime();
        this.Operator = aLACourseDescSchema.getOperator();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("CourseID") == null)
            {
                this.CourseID = null;
            }
            else
            {
                this.CourseID = rs.getString("CourseID").trim();
            }

            if (rs.getString("BranchType") == null)
            {
                this.BranchType = null;
            }
            else
            {
                this.BranchType = rs.getString("BranchType").trim();
            }

            if (rs.getString("BranchType2") == null)
            {
                this.BranchType2 = null;
            }
            else
            {
                this.BranchType2 = rs.getString("BranchType2").trim();
            }

            if (rs.getString("CourseType") == null)
            {
                this.CourseType = null;
            }
            else
            {
                this.CourseType = rs.getString("CourseType").trim();
            }

            if (rs.getString("CourseSeries") == null)
            {
                this.CourseSeries = null;
            }
            else
            {
                this.CourseSeries = rs.getString("CourseSeries").trim();
            }

            if (rs.getString("CourseGrade") == null)
            {
                this.CourseGrade = null;
            }
            else
            {
                this.CourseGrade = rs.getString("CourseGrade").trim();
            }

            if (rs.getString("CourseSN") == null)
            {
                this.CourseSN = null;
            }
            else
            {
                this.CourseSN = rs.getString("CourseSN").trim();
            }

            if (rs.getString("CourseName") == null)
            {
                this.CourseName = null;
            }
            else
            {
                this.CourseName = rs.getString("CourseName").trim();
            }

            this.Hour = rs.getDouble("Hour");
            this.Profit = rs.getDouble("Profit");
            if (rs.getString("TeacherGrade") == null)
            {
                this.TeacherGrade = null;
            }
            else
            {
                this.TeacherGrade = rs.getString("TeacherGrade").trim();
            }

            if (rs.getString("StopFlag") == null)
            {
                this.StopFlag = null;
            }
            else
            {
                this.StopFlag = rs.getString("StopFlag").trim();
            }

            if (rs.getString("Noti") == null)
            {
                this.Noti = null;
            }
            else
            {
                this.Noti = rs.getString("Noti").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LACourseDescSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LACourseDescSchema getSchema()
    {
        LACourseDescSchema aLACourseDescSchema = new LACourseDescSchema();
        aLACourseDescSchema.setSchema(this);
        return aLACourseDescSchema;
    }

    public LACourseDescDB getDB()
    {
        LACourseDescDB aDBOper = new LACourseDescDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLACourseDesc描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(CourseID)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BranchType)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BranchType2)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(CourseType)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(CourseSeries)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(CourseGrade)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(CourseSN)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(CourseName)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Hour));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Profit));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(TeacherGrade)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(StopFlag)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Noti)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                MakeDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                ModifyDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Operator)));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLACourseDesc>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            CourseID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
            BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                         SysConst.PACKAGESPILTER);
            CourseType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                        SysConst.PACKAGESPILTER);
            CourseSeries = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                          SysConst.PACKAGESPILTER);
            CourseGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                         SysConst.PACKAGESPILTER);
            CourseSN = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                      SysConst.PACKAGESPILTER);
            CourseName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                        SysConst.PACKAGESPILTER);
            Hour = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    9, SysConst.PACKAGESPILTER))).doubleValue();
            Profit = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    10, SysConst.PACKAGESPILTER))).doubleValue();
            TeacherGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                          SysConst.PACKAGESPILTER);
            StopFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                      SysConst.PACKAGESPILTER);
            Noti = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                  SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 14, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 16, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                        SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                      SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LACourseDescSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("CourseID"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CourseID));
        }
        if (FCode.equals("BranchType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
        }
        if (FCode.equals("BranchType2"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
        }
        if (FCode.equals("CourseType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CourseType));
        }
        if (FCode.equals("CourseSeries"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CourseSeries));
        }
        if (FCode.equals("CourseGrade"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CourseGrade));
        }
        if (FCode.equals("CourseSN"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CourseSN));
        }
        if (FCode.equals("CourseName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CourseName));
        }
        if (FCode.equals("Hour"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Hour));
        }
        if (FCode.equals("Profit"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Profit));
        }
        if (FCode.equals("TeacherGrade"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TeacherGrade));
        }
        if (FCode.equals("StopFlag"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StopFlag));
        }
        if (FCode.equals("Noti"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Noti));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(CourseID);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(BranchType);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(BranchType2);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(CourseType);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(CourseSeries);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(CourseGrade);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(CourseSN);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(CourseName);
                break;
            case 8:
                strFieldValue = String.valueOf(Hour);
                break;
            case 9:
                strFieldValue = String.valueOf(Profit);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(TeacherGrade);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(StopFlag);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(Noti);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("CourseID"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CourseID = FValue.trim();
            }
            else
            {
                CourseID = null;
            }
        }
        if (FCode.equals("BranchType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchType = FValue.trim();
            }
            else
            {
                BranchType = null;
            }
        }
        if (FCode.equals("BranchType2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchType2 = FValue.trim();
            }
            else
            {
                BranchType2 = null;
            }
        }
        if (FCode.equals("CourseType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CourseType = FValue.trim();
            }
            else
            {
                CourseType = null;
            }
        }
        if (FCode.equals("CourseSeries"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CourseSeries = FValue.trim();
            }
            else
            {
                CourseSeries = null;
            }
        }
        if (FCode.equals("CourseGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CourseGrade = FValue.trim();
            }
            else
            {
                CourseGrade = null;
            }
        }
        if (FCode.equals("CourseSN"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CourseSN = FValue.trim();
            }
            else
            {
                CourseSN = null;
            }
        }
        if (FCode.equals("CourseName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CourseName = FValue.trim();
            }
            else
            {
                CourseName = null;
            }
        }
        if (FCode.equals("Hour"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Hour = d;
            }
        }
        if (FCode.equals("Profit"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Profit = d;
            }
        }
        if (FCode.equals("TeacherGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                TeacherGrade = FValue.trim();
            }
            else
            {
                TeacherGrade = null;
            }
        }
        if (FCode.equals("StopFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                StopFlag = FValue.trim();
            }
            else
            {
                StopFlag = null;
            }
        }
        if (FCode.equals("Noti"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Noti = FValue.trim();
            }
            else
            {
                Noti = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LACourseDescSchema other = (LACourseDescSchema) otherObject;
        return
                CourseID.equals(other.getCourseID())
                && BranchType.equals(other.getBranchType())
                && BranchType2.equals(other.getBranchType2())
                && CourseType.equals(other.getCourseType())
                && CourseSeries.equals(other.getCourseSeries())
                && CourseGrade.equals(other.getCourseGrade())
                && CourseSN.equals(other.getCourseSN())
                && CourseName.equals(other.getCourseName())
                && Hour == other.getHour()
                && Profit == other.getProfit()
                && TeacherGrade.equals(other.getTeacherGrade())
                && StopFlag.equals(other.getStopFlag())
                && Noti.equals(other.getNoti())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && Operator.equals(other.getOperator());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("CourseID"))
        {
            return 0;
        }
        if (strFieldName.equals("BranchType"))
        {
            return 1;
        }
        if (strFieldName.equals("BranchType2"))
        {
            return 2;
        }
        if (strFieldName.equals("CourseType"))
        {
            return 3;
        }
        if (strFieldName.equals("CourseSeries"))
        {
            return 4;
        }
        if (strFieldName.equals("CourseGrade"))
        {
            return 5;
        }
        if (strFieldName.equals("CourseSN"))
        {
            return 6;
        }
        if (strFieldName.equals("CourseName"))
        {
            return 7;
        }
        if (strFieldName.equals("Hour"))
        {
            return 8;
        }
        if (strFieldName.equals("Profit"))
        {
            return 9;
        }
        if (strFieldName.equals("TeacherGrade"))
        {
            return 10;
        }
        if (strFieldName.equals("StopFlag"))
        {
            return 11;
        }
        if (strFieldName.equals("Noti"))
        {
            return 12;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 13;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 14;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 15;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 16;
        }
        if (strFieldName.equals("Operator"))
        {
            return 17;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "CourseID";
                break;
            case 1:
                strFieldName = "BranchType";
                break;
            case 2:
                strFieldName = "BranchType2";
                break;
            case 3:
                strFieldName = "CourseType";
                break;
            case 4:
                strFieldName = "CourseSeries";
                break;
            case 5:
                strFieldName = "CourseGrade";
                break;
            case 6:
                strFieldName = "CourseSN";
                break;
            case 7:
                strFieldName = "CourseName";
                break;
            case 8:
                strFieldName = "Hour";
                break;
            case 9:
                strFieldName = "Profit";
                break;
            case 10:
                strFieldName = "TeacherGrade";
                break;
            case 11:
                strFieldName = "StopFlag";
                break;
            case 12:
                strFieldName = "Noti";
                break;
            case 13:
                strFieldName = "MakeDate";
                break;
            case 14:
                strFieldName = "MakeTime";
                break;
            case 15:
                strFieldName = "ModifyDate";
                break;
            case 16:
                strFieldName = "ModifyTime";
                break;
            case 17:
                strFieldName = "Operator";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("CourseID"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchType2"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CourseType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CourseSeries"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CourseGrade"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CourseSN"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CourseName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Hour"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Profit"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("TeacherGrade"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StopFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Noti"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 9:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 17:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
