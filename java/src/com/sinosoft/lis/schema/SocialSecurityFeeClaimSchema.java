/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.SocialSecurityFeeClaimDB;

/*
 * <p>ClassName: SocialSecurityFeeClaimSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 新寿险业务系统模型
 * @CreateDate：2014-06-20
 */
public class SocialSecurityFeeClaimSchema implements Schema, Cloneable
{
	// @Field
	/** 实付号码 */
	private String ActuGetNo;
	/** 补/退费财务类型 */
	private String FeeFinaType;
	/** 补/退费业务类型 */
	private String FeeOperationType;
	/** 调查案件号 */
	private String SurveyCaseNo;
	/** 结算号 */
	private String SettlementNo;
	/** 给付凭证号 */
	private String PaymentVoucherNo;
	/** 参保地机构代码 */
	private String InsuranceComCode;
	/** 参保地机构名称 */
	private String InsuranceComName;
	/** (异地)调查机构代码 */
	private String RemoteSurveyComCode;
	/** (异地)调查机构名称 */
	private String RemoteSurveyComName;
	/** 结算总金额 */
	private double TotalAmount;
	/** 受理时间 */
	private Date SurveyStartDate;
	/** 调查结案时间 */
	private Date SurveyEndDate;
	/** 结算单生成时间 */
	private Date CreateDate;
	/** 患者姓名 */
	private String Name;
	/** 医保编号 */
	private String MedicareCode;
	/** 身份证号码 */
	private String IdNo;
	/** 结算单状态 */
	private String SettlementState;
	/** 财务到帐日期 */
	private Date EnterAccDate;
	/** 财务确认日期 */
	private Date ConfDate;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 修改日期 */
	private Date ModifyDate;
	/** 修改时间 */
	private String ModifyTime;
	/** 成本中心 */
	private String CostCenter;

	public static final int FIELDNUM = 26;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public SocialSecurityFeeClaimSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[4];
		pk[0] = "FeeFinaType";
		pk[1] = "FeeOperationType";
		pk[2] = "SurveyCaseNo";
		pk[3] = "SettlementNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		SocialSecurityFeeClaimSchema cloned = (SocialSecurityFeeClaimSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getActuGetNo()
	{
		return ActuGetNo;
	}
	public void setActuGetNo(String aActuGetNo)
	{
		ActuGetNo = aActuGetNo;
	}
	public String getFeeFinaType()
	{
		return FeeFinaType;
	}
	public void setFeeFinaType(String aFeeFinaType)
	{
		FeeFinaType = aFeeFinaType;
	}
	public String getFeeOperationType()
	{
		return FeeOperationType;
	}
	public void setFeeOperationType(String aFeeOperationType)
	{
		FeeOperationType = aFeeOperationType;
	}
	public String getSurveyCaseNo()
	{
		return SurveyCaseNo;
	}
	public void setSurveyCaseNo(String aSurveyCaseNo)
	{
		SurveyCaseNo = aSurveyCaseNo;
	}
	public String getSettlementNo()
	{
		return SettlementNo;
	}
	public void setSettlementNo(String aSettlementNo)
	{
		SettlementNo = aSettlementNo;
	}
	public String getPaymentVoucherNo()
	{
		return PaymentVoucherNo;
	}
	public void setPaymentVoucherNo(String aPaymentVoucherNo)
	{
		PaymentVoucherNo = aPaymentVoucherNo;
	}
	public String getInsuranceComCode()
	{
		return InsuranceComCode;
	}
	public void setInsuranceComCode(String aInsuranceComCode)
	{
		InsuranceComCode = aInsuranceComCode;
	}
	public String getInsuranceComName()
	{
		return InsuranceComName;
	}
	public void setInsuranceComName(String aInsuranceComName)
	{
		InsuranceComName = aInsuranceComName;
	}
	public String getRemoteSurveyComCode()
	{
		return RemoteSurveyComCode;
	}
	public void setRemoteSurveyComCode(String aRemoteSurveyComCode)
	{
		RemoteSurveyComCode = aRemoteSurveyComCode;
	}
	public String getRemoteSurveyComName()
	{
		return RemoteSurveyComName;
	}
	public void setRemoteSurveyComName(String aRemoteSurveyComName)
	{
		RemoteSurveyComName = aRemoteSurveyComName;
	}
	public double getTotalAmount()
	{
		return TotalAmount;
	}
	public void setTotalAmount(double aTotalAmount)
	{
		TotalAmount = Arith.round(aTotalAmount,2);
	}
	public void setTotalAmount(String aTotalAmount)
	{
		if (aTotalAmount != null && !aTotalAmount.equals(""))
		{
			Double tDouble = new Double(aTotalAmount);
			double d = tDouble.doubleValue();
                TotalAmount = Arith.round(d,2);
		}
	}

	public String getSurveyStartDate()
	{
		if( SurveyStartDate != null )
			return fDate.getString(SurveyStartDate);
		else
			return null;
	}
	public void setSurveyStartDate(Date aSurveyStartDate)
	{
		SurveyStartDate = aSurveyStartDate;
	}
	public void setSurveyStartDate(String aSurveyStartDate)
	{
		if (aSurveyStartDate != null && !aSurveyStartDate.equals("") )
		{
			SurveyStartDate = fDate.getDate( aSurveyStartDate );
		}
		else
			SurveyStartDate = null;
	}

	public String getSurveyEndDate()
	{
		if( SurveyEndDate != null )
			return fDate.getString(SurveyEndDate);
		else
			return null;
	}
	public void setSurveyEndDate(Date aSurveyEndDate)
	{
		SurveyEndDate = aSurveyEndDate;
	}
	public void setSurveyEndDate(String aSurveyEndDate)
	{
		if (aSurveyEndDate != null && !aSurveyEndDate.equals("") )
		{
			SurveyEndDate = fDate.getDate( aSurveyEndDate );
		}
		else
			SurveyEndDate = null;
	}

	public String getCreateDate()
	{
		if( CreateDate != null )
			return fDate.getString(CreateDate);
		else
			return null;
	}
	public void setCreateDate(Date aCreateDate)
	{
		CreateDate = aCreateDate;
	}
	public void setCreateDate(String aCreateDate)
	{
		if (aCreateDate != null && !aCreateDate.equals("") )
		{
			CreateDate = fDate.getDate( aCreateDate );
		}
		else
			CreateDate = null;
	}

	public String getName()
	{
		return Name;
	}
	public void setName(String aName)
	{
		Name = aName;
	}
	public String getMedicareCode()
	{
		return MedicareCode;
	}
	public void setMedicareCode(String aMedicareCode)
	{
		MedicareCode = aMedicareCode;
	}
	public String getIdNo()
	{
		return IdNo;
	}
	public void setIdNo(String aIdNo)
	{
		IdNo = aIdNo;
	}
	public String getSettlementState()
	{
		return SettlementState;
	}
	public void setSettlementState(String aSettlementState)
	{
		SettlementState = aSettlementState;
	}
	public String getEnterAccDate()
	{
		if( EnterAccDate != null )
			return fDate.getString(EnterAccDate);
		else
			return null;
	}
	public void setEnterAccDate(Date aEnterAccDate)
	{
		EnterAccDate = aEnterAccDate;
	}
	public void setEnterAccDate(String aEnterAccDate)
	{
		if (aEnterAccDate != null && !aEnterAccDate.equals("") )
		{
			EnterAccDate = fDate.getDate( aEnterAccDate );
		}
		else
			EnterAccDate = null;
	}

	public String getConfDate()
	{
		if( ConfDate != null )
			return fDate.getString(ConfDate);
		else
			return null;
	}
	public void setConfDate(Date aConfDate)
	{
		ConfDate = aConfDate;
	}
	public void setConfDate(String aConfDate)
	{
		if (aConfDate != null && !aConfDate.equals("") )
		{
			ConfDate = fDate.getDate( aConfDate );
		}
		else
			ConfDate = null;
	}

	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getCostCenter()
	{
		return CostCenter;
	}
	public void setCostCenter(String aCostCenter)
	{
		CostCenter = aCostCenter;
	}

	/**
	* 使用另外一个 SocialSecurityFeeClaimSchema 对象给 Schema 赋值
	* @param: aSocialSecurityFeeClaimSchema SocialSecurityFeeClaimSchema
	**/
	public void setSchema(SocialSecurityFeeClaimSchema aSocialSecurityFeeClaimSchema)
	{
		this.ActuGetNo = aSocialSecurityFeeClaimSchema.getActuGetNo();
		this.FeeFinaType = aSocialSecurityFeeClaimSchema.getFeeFinaType();
		this.FeeOperationType = aSocialSecurityFeeClaimSchema.getFeeOperationType();
		this.SurveyCaseNo = aSocialSecurityFeeClaimSchema.getSurveyCaseNo();
		this.SettlementNo = aSocialSecurityFeeClaimSchema.getSettlementNo();
		this.PaymentVoucherNo = aSocialSecurityFeeClaimSchema.getPaymentVoucherNo();
		this.InsuranceComCode = aSocialSecurityFeeClaimSchema.getInsuranceComCode();
		this.InsuranceComName = aSocialSecurityFeeClaimSchema.getInsuranceComName();
		this.RemoteSurveyComCode = aSocialSecurityFeeClaimSchema.getRemoteSurveyComCode();
		this.RemoteSurveyComName = aSocialSecurityFeeClaimSchema.getRemoteSurveyComName();
		this.TotalAmount = aSocialSecurityFeeClaimSchema.getTotalAmount();
		this.SurveyStartDate = fDate.getDate( aSocialSecurityFeeClaimSchema.getSurveyStartDate());
		this.SurveyEndDate = fDate.getDate( aSocialSecurityFeeClaimSchema.getSurveyEndDate());
		this.CreateDate = fDate.getDate( aSocialSecurityFeeClaimSchema.getCreateDate());
		this.Name = aSocialSecurityFeeClaimSchema.getName();
		this.MedicareCode = aSocialSecurityFeeClaimSchema.getMedicareCode();
		this.IdNo = aSocialSecurityFeeClaimSchema.getIdNo();
		this.SettlementState = aSocialSecurityFeeClaimSchema.getSettlementState();
		this.EnterAccDate = fDate.getDate( aSocialSecurityFeeClaimSchema.getEnterAccDate());
		this.ConfDate = fDate.getDate( aSocialSecurityFeeClaimSchema.getConfDate());
		this.Operator = aSocialSecurityFeeClaimSchema.getOperator();
		this.MakeDate = fDate.getDate( aSocialSecurityFeeClaimSchema.getMakeDate());
		this.MakeTime = aSocialSecurityFeeClaimSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aSocialSecurityFeeClaimSchema.getModifyDate());
		this.ModifyTime = aSocialSecurityFeeClaimSchema.getModifyTime();
		this.CostCenter = aSocialSecurityFeeClaimSchema.getCostCenter();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("ActuGetNo") == null )
				this.ActuGetNo = null;
			else
				this.ActuGetNo = rs.getString("ActuGetNo").trim();

			if( rs.getString("FeeFinaType") == null )
				this.FeeFinaType = null;
			else
				this.FeeFinaType = rs.getString("FeeFinaType").trim();

			if( rs.getString("FeeOperationType") == null )
				this.FeeOperationType = null;
			else
				this.FeeOperationType = rs.getString("FeeOperationType").trim();

			if( rs.getString("SurveyCaseNo") == null )
				this.SurveyCaseNo = null;
			else
				this.SurveyCaseNo = rs.getString("SurveyCaseNo").trim();

			if( rs.getString("SettlementNo") == null )
				this.SettlementNo = null;
			else
				this.SettlementNo = rs.getString("SettlementNo").trim();

			if( rs.getString("PaymentVoucherNo") == null )
				this.PaymentVoucherNo = null;
			else
				this.PaymentVoucherNo = rs.getString("PaymentVoucherNo").trim();

			if( rs.getString("InsuranceComCode") == null )
				this.InsuranceComCode = null;
			else
				this.InsuranceComCode = rs.getString("InsuranceComCode").trim();

			if( rs.getString("InsuranceComName") == null )
				this.InsuranceComName = null;
			else
				this.InsuranceComName = rs.getString("InsuranceComName").trim();

			if( rs.getString("RemoteSurveyComCode") == null )
				this.RemoteSurveyComCode = null;
			else
				this.RemoteSurveyComCode = rs.getString("RemoteSurveyComCode").trim();

			if( rs.getString("RemoteSurveyComName") == null )
				this.RemoteSurveyComName = null;
			else
				this.RemoteSurveyComName = rs.getString("RemoteSurveyComName").trim();

			this.TotalAmount = rs.getDouble("TotalAmount");
			this.SurveyStartDate = rs.getDate("SurveyStartDate");
			this.SurveyEndDate = rs.getDate("SurveyEndDate");
			this.CreateDate = rs.getDate("CreateDate");
			if( rs.getString("Name") == null )
				this.Name = null;
			else
				this.Name = rs.getString("Name").trim();

			if( rs.getString("MedicareCode") == null )
				this.MedicareCode = null;
			else
				this.MedicareCode = rs.getString("MedicareCode").trim();

			if( rs.getString("IdNo") == null )
				this.IdNo = null;
			else
				this.IdNo = rs.getString("IdNo").trim();

			if( rs.getString("SettlementState") == null )
				this.SettlementState = null;
			else
				this.SettlementState = rs.getString("SettlementState").trim();

			this.EnterAccDate = rs.getDate("EnterAccDate");
			this.ConfDate = rs.getDate("ConfDate");
			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("CostCenter") == null )
				this.CostCenter = null;
			else
				this.CostCenter = rs.getString("CostCenter").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的SocialSecurityFeeClaim表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "SocialSecurityFeeClaimSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public SocialSecurityFeeClaimSchema getSchema()
	{
		SocialSecurityFeeClaimSchema aSocialSecurityFeeClaimSchema = new SocialSecurityFeeClaimSchema();
		aSocialSecurityFeeClaimSchema.setSchema(this);
		return aSocialSecurityFeeClaimSchema;
	}

	public SocialSecurityFeeClaimDB getDB()
	{
		SocialSecurityFeeClaimDB aDBOper = new SocialSecurityFeeClaimDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpSocialSecurityFeeClaim描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(ActuGetNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FeeFinaType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FeeOperationType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SurveyCaseNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SettlementNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PaymentVoucherNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuranceComCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuranceComName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RemoteSurveyComCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RemoteSurveyComName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(TotalAmount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( SurveyStartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( SurveyEndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CreateDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Name)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MedicareCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IdNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SettlementState)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( EnterAccDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ConfDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CostCenter));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpSocialSecurityFeeClaim>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			ActuGetNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			FeeFinaType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			FeeOperationType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			SurveyCaseNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			SettlementNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			PaymentVoucherNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			InsuranceComCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			InsuranceComName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			RemoteSurveyComCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			RemoteSurveyComName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			TotalAmount = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,11,SysConst.PACKAGESPILTER))).doubleValue();
			SurveyStartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
			SurveyEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,SysConst.PACKAGESPILTER));
			CreateDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,SysConst.PACKAGESPILTER));
			Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			MedicareCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			IdNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			SettlementState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			EnterAccDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,SysConst.PACKAGESPILTER));
			ConfDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,SysConst.PACKAGESPILTER));
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			CostCenter = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "SocialSecurityFeeClaimSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("ActuGetNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ActuGetNo));
		}
		if (FCode.equals("FeeFinaType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FeeFinaType));
		}
		if (FCode.equals("FeeOperationType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FeeOperationType));
		}
		if (FCode.equals("SurveyCaseNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SurveyCaseNo));
		}
		if (FCode.equals("SettlementNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SettlementNo));
		}
		if (FCode.equals("PaymentVoucherNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PaymentVoucherNo));
		}
		if (FCode.equals("InsuranceComCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuranceComCode));
		}
		if (FCode.equals("InsuranceComName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuranceComName));
		}
		if (FCode.equals("RemoteSurveyComCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RemoteSurveyComCode));
		}
		if (FCode.equals("RemoteSurveyComName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RemoteSurveyComName));
		}
		if (FCode.equals("TotalAmount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TotalAmount));
		}
		if (FCode.equals("SurveyStartDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getSurveyStartDate()));
		}
		if (FCode.equals("SurveyEndDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getSurveyEndDate()));
		}
		if (FCode.equals("CreateDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCreateDate()));
		}
		if (FCode.equals("Name"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
		}
		if (FCode.equals("MedicareCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MedicareCode));
		}
		if (FCode.equals("IdNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IdNo));
		}
		if (FCode.equals("SettlementState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SettlementState));
		}
		if (FCode.equals("EnterAccDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEnterAccDate()));
		}
		if (FCode.equals("ConfDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getConfDate()));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("CostCenter"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CostCenter));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(ActuGetNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(FeeFinaType);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(FeeOperationType);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(SurveyCaseNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(SettlementNo);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(PaymentVoucherNo);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(InsuranceComCode);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(InsuranceComName);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(RemoteSurveyComCode);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(RemoteSurveyComName);
				break;
			case 10:
				strFieldValue = String.valueOf(TotalAmount);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getSurveyStartDate()));
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getSurveyEndDate()));
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCreateDate()));
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(Name);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(MedicareCode);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(IdNo);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(SettlementState);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEnterAccDate()));
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getConfDate()));
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(CostCenter);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("ActuGetNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ActuGetNo = FValue.trim();
			}
			else
				ActuGetNo = null;
		}
		if (FCode.equalsIgnoreCase("FeeFinaType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FeeFinaType = FValue.trim();
			}
			else
				FeeFinaType = null;
		}
		if (FCode.equalsIgnoreCase("FeeOperationType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FeeOperationType = FValue.trim();
			}
			else
				FeeOperationType = null;
		}
		if (FCode.equalsIgnoreCase("SurveyCaseNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SurveyCaseNo = FValue.trim();
			}
			else
				SurveyCaseNo = null;
		}
		if (FCode.equalsIgnoreCase("SettlementNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SettlementNo = FValue.trim();
			}
			else
				SettlementNo = null;
		}
		if (FCode.equalsIgnoreCase("PaymentVoucherNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PaymentVoucherNo = FValue.trim();
			}
			else
				PaymentVoucherNo = null;
		}
		if (FCode.equalsIgnoreCase("InsuranceComCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuranceComCode = FValue.trim();
			}
			else
				InsuranceComCode = null;
		}
		if (FCode.equalsIgnoreCase("InsuranceComName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuranceComName = FValue.trim();
			}
			else
				InsuranceComName = null;
		}
		if (FCode.equalsIgnoreCase("RemoteSurveyComCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RemoteSurveyComCode = FValue.trim();
			}
			else
				RemoteSurveyComCode = null;
		}
		if (FCode.equalsIgnoreCase("RemoteSurveyComName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RemoteSurveyComName = FValue.trim();
			}
			else
				RemoteSurveyComName = null;
		}
		if (FCode.equalsIgnoreCase("TotalAmount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				TotalAmount = d;
			}
		}
		if (FCode.equalsIgnoreCase("SurveyStartDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				SurveyStartDate = fDate.getDate( FValue );
			}
			else
				SurveyStartDate = null;
		}
		if (FCode.equalsIgnoreCase("SurveyEndDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				SurveyEndDate = fDate.getDate( FValue );
			}
			else
				SurveyEndDate = null;
		}
		if (FCode.equalsIgnoreCase("CreateDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CreateDate = fDate.getDate( FValue );
			}
			else
				CreateDate = null;
		}
		if (FCode.equalsIgnoreCase("Name"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Name = FValue.trim();
			}
			else
				Name = null;
		}
		if (FCode.equalsIgnoreCase("MedicareCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MedicareCode = FValue.trim();
			}
			else
				MedicareCode = null;
		}
		if (FCode.equalsIgnoreCase("IdNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IdNo = FValue.trim();
			}
			else
				IdNo = null;
		}
		if (FCode.equalsIgnoreCase("SettlementState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SettlementState = FValue.trim();
			}
			else
				SettlementState = null;
		}
		if (FCode.equalsIgnoreCase("EnterAccDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				EnterAccDate = fDate.getDate( FValue );
			}
			else
				EnterAccDate = null;
		}
		if (FCode.equalsIgnoreCase("ConfDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ConfDate = fDate.getDate( FValue );
			}
			else
				ConfDate = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("CostCenter"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CostCenter = FValue.trim();
			}
			else
				CostCenter = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		SocialSecurityFeeClaimSchema other = (SocialSecurityFeeClaimSchema)otherObject;
		return
			(ActuGetNo == null ? other.getActuGetNo() == null : ActuGetNo.equals(other.getActuGetNo()))
			&& (FeeFinaType == null ? other.getFeeFinaType() == null : FeeFinaType.equals(other.getFeeFinaType()))
			&& (FeeOperationType == null ? other.getFeeOperationType() == null : FeeOperationType.equals(other.getFeeOperationType()))
			&& (SurveyCaseNo == null ? other.getSurveyCaseNo() == null : SurveyCaseNo.equals(other.getSurveyCaseNo()))
			&& (SettlementNo == null ? other.getSettlementNo() == null : SettlementNo.equals(other.getSettlementNo()))
			&& (PaymentVoucherNo == null ? other.getPaymentVoucherNo() == null : PaymentVoucherNo.equals(other.getPaymentVoucherNo()))
			&& (InsuranceComCode == null ? other.getInsuranceComCode() == null : InsuranceComCode.equals(other.getInsuranceComCode()))
			&& (InsuranceComName == null ? other.getInsuranceComName() == null : InsuranceComName.equals(other.getInsuranceComName()))
			&& (RemoteSurveyComCode == null ? other.getRemoteSurveyComCode() == null : RemoteSurveyComCode.equals(other.getRemoteSurveyComCode()))
			&& (RemoteSurveyComName == null ? other.getRemoteSurveyComName() == null : RemoteSurveyComName.equals(other.getRemoteSurveyComName()))
			&& TotalAmount == other.getTotalAmount()
			&& (SurveyStartDate == null ? other.getSurveyStartDate() == null : fDate.getString(SurveyStartDate).equals(other.getSurveyStartDate()))
			&& (SurveyEndDate == null ? other.getSurveyEndDate() == null : fDate.getString(SurveyEndDate).equals(other.getSurveyEndDate()))
			&& (CreateDate == null ? other.getCreateDate() == null : fDate.getString(CreateDate).equals(other.getCreateDate()))
			&& (Name == null ? other.getName() == null : Name.equals(other.getName()))
			&& (MedicareCode == null ? other.getMedicareCode() == null : MedicareCode.equals(other.getMedicareCode()))
			&& (IdNo == null ? other.getIdNo() == null : IdNo.equals(other.getIdNo()))
			&& (SettlementState == null ? other.getSettlementState() == null : SettlementState.equals(other.getSettlementState()))
			&& (EnterAccDate == null ? other.getEnterAccDate() == null : fDate.getString(EnterAccDate).equals(other.getEnterAccDate()))
			&& (ConfDate == null ? other.getConfDate() == null : fDate.getString(ConfDate).equals(other.getConfDate()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (CostCenter == null ? other.getCostCenter() == null : CostCenter.equals(other.getCostCenter()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("ActuGetNo") ) {
			return 0;
		}
		if( strFieldName.equals("FeeFinaType") ) {
			return 1;
		}
		if( strFieldName.equals("FeeOperationType") ) {
			return 2;
		}
		if( strFieldName.equals("SurveyCaseNo") ) {
			return 3;
		}
		if( strFieldName.equals("SettlementNo") ) {
			return 4;
		}
		if( strFieldName.equals("PaymentVoucherNo") ) {
			return 5;
		}
		if( strFieldName.equals("InsuranceComCode") ) {
			return 6;
		}
		if( strFieldName.equals("InsuranceComName") ) {
			return 7;
		}
		if( strFieldName.equals("RemoteSurveyComCode") ) {
			return 8;
		}
		if( strFieldName.equals("RemoteSurveyComName") ) {
			return 9;
		}
		if( strFieldName.equals("TotalAmount") ) {
			return 10;
		}
		if( strFieldName.equals("SurveyStartDate") ) {
			return 11;
		}
		if( strFieldName.equals("SurveyEndDate") ) {
			return 12;
		}
		if( strFieldName.equals("CreateDate") ) {
			return 13;
		}
		if( strFieldName.equals("Name") ) {
			return 14;
		}
		if( strFieldName.equals("MedicareCode") ) {
			return 15;
		}
		if( strFieldName.equals("IdNo") ) {
			return 16;
		}
		if( strFieldName.equals("SettlementState") ) {
			return 17;
		}
		if( strFieldName.equals("EnterAccDate") ) {
			return 18;
		}
		if( strFieldName.equals("ConfDate") ) {
			return 19;
		}
		if( strFieldName.equals("Operator") ) {
			return 20;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 21;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 22;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 23;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 24;
		}
		if( strFieldName.equals("CostCenter") ) {
			return 25;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "ActuGetNo";
				break;
			case 1:
				strFieldName = "FeeFinaType";
				break;
			case 2:
				strFieldName = "FeeOperationType";
				break;
			case 3:
				strFieldName = "SurveyCaseNo";
				break;
			case 4:
				strFieldName = "SettlementNo";
				break;
			case 5:
				strFieldName = "PaymentVoucherNo";
				break;
			case 6:
				strFieldName = "InsuranceComCode";
				break;
			case 7:
				strFieldName = "InsuranceComName";
				break;
			case 8:
				strFieldName = "RemoteSurveyComCode";
				break;
			case 9:
				strFieldName = "RemoteSurveyComName";
				break;
			case 10:
				strFieldName = "TotalAmount";
				break;
			case 11:
				strFieldName = "SurveyStartDate";
				break;
			case 12:
				strFieldName = "SurveyEndDate";
				break;
			case 13:
				strFieldName = "CreateDate";
				break;
			case 14:
				strFieldName = "Name";
				break;
			case 15:
				strFieldName = "MedicareCode";
				break;
			case 16:
				strFieldName = "IdNo";
				break;
			case 17:
				strFieldName = "SettlementState";
				break;
			case 18:
				strFieldName = "EnterAccDate";
				break;
			case 19:
				strFieldName = "ConfDate";
				break;
			case 20:
				strFieldName = "Operator";
				break;
			case 21:
				strFieldName = "MakeDate";
				break;
			case 22:
				strFieldName = "MakeTime";
				break;
			case 23:
				strFieldName = "ModifyDate";
				break;
			case 24:
				strFieldName = "ModifyTime";
				break;
			case 25:
				strFieldName = "CostCenter";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("ActuGetNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FeeFinaType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FeeOperationType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SurveyCaseNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SettlementNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PaymentVoucherNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuranceComCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuranceComName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RemoteSurveyComCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RemoteSurveyComName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TotalAmount") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SurveyStartDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("SurveyEndDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("CreateDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Name") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MedicareCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IdNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SettlementState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EnterAccDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ConfDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CostCenter") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 12:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 13:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 19:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
