/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.WFBnfListDB;

/*
 * <p>ClassName: WFBnfListSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 新寿险业务系统模型
 * @CreateDate：2012-02-07
 */
public class WFBnfListSchema implements Schema, Cloneable
{
	// @Field
	/** 批次号 */
	private String BatchNo;
	/** 保险卡号 */
	private String CardNo;
	/** 所属被保人 */
	private String ToInsuNo;
	/** 所属被保人关系 */
	private String ToInsuRela;
	/** 受益类别 */
	private String BnfType;
	/** 受益顺位 */
	private String BnfGrade;
	/** 受益顺位流水号 */
	private String BnfGradeNo;
	/** 受益比例 */
	private double BnfRate;
	/** 受益人姓名 */
	private String Name;
	/** 性别 */
	private String Sex;
	/** 出生日期 */
	private String Birthday;
	/** 证件类型 */
	private String IDType;
	/** 证件号码 */
	private String IDNo;
	/** 备用字段1 */
	private String Bak1;
	/** 备用字段2 */
	private String Bak2;
	/** 备用字段3 */
	private String Bak3;
	/** 备用字段4 */
	private String Bak4;
	/** 备用字段5 */
	private String Bak5;
	/** 备用字段6 */
	private String Bak6;
	/** 备用字段7 */
	private String Bak7;
	/** 备用字段8 */
	private String Bak8;
	/** 备用字段9 */
	private String Bak9;
	/** 备用字段10 */
	private String Bak10;

	public static final int FIELDNUM = 23;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public WFBnfListSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[6];
		pk[0] = "BatchNo";
		pk[1] = "CardNo";
		pk[2] = "ToInsuNo";
		pk[3] = "BnfType";
		pk[4] = "BnfGrade";
		pk[5] = "BnfGradeNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		WFBnfListSchema cloned = (WFBnfListSchema) super.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getBatchNo()
	{
		return BatchNo;
	}
	public void setBatchNo(String aBatchNo)
	{
		BatchNo = aBatchNo;
	}
	public String getCardNo()
	{
		return CardNo;
	}
	public void setCardNo(String aCardNo)
	{
		CardNo = aCardNo;
	}
	public String getToInsuNo()
	{
		return ToInsuNo;
	}
	public void setToInsuNo(String aToInsuNo)
	{
		ToInsuNo = aToInsuNo;
	}
	public String getToInsuRela()
	{
		return ToInsuRela;
	}
	public void setToInsuRela(String aToInsuRela)
	{
		ToInsuRela = aToInsuRela;
	}
	public String getBnfType()
	{
		return BnfType;
	}
	public void setBnfType(String aBnfType)
	{
		BnfType = aBnfType;
	}
	public String getBnfGrade()
	{
		return BnfGrade;
	}
	public void setBnfGrade(String aBnfGrade)
	{
		BnfGrade = aBnfGrade;
	}
	public String getBnfGradeNo()
	{
		return BnfGradeNo;
	}
	public void setBnfGradeNo(String aBnfGradeNo)
	{
		BnfGradeNo = aBnfGradeNo;
	}
	public double getBnfRate()
	{
		return BnfRate;
	}
	public void setBnfRate(double aBnfRate)
	{
		BnfRate = Arith.round(aBnfRate,2);
	}
	public void setBnfRate(String aBnfRate)
	{
		if (aBnfRate != null && !aBnfRate.equals(""))
		{
			Double tDouble = new Double(aBnfRate);
			double d = tDouble.doubleValue();
                BnfRate = Arith.round(d,2);
		}
	}

	public String getName()
	{
		return Name;
	}
	public void setName(String aName)
	{
		Name = aName;
	}
	public String getSex()
	{
		return Sex;
	}
	public void setSex(String aSex)
	{
		Sex = aSex;
	}
	public String getBirthday()
	{
		return Birthday;
	}
	public void setBirthday(String aBirthday)
	{
		Birthday = aBirthday;
	}
	public String getIDType()
	{
		return IDType;
	}
	public void setIDType(String aIDType)
	{
		IDType = aIDType;
	}
	public String getIDNo()
	{
		return IDNo;
	}
	public void setIDNo(String aIDNo)
	{
		IDNo = aIDNo;
	}
	public String getBak1()
	{
		return Bak1;
	}
	public void setBak1(String aBak1)
	{
		Bak1 = aBak1;
	}
	public String getBak2()
	{
		return Bak2;
	}
	public void setBak2(String aBak2)
	{
		Bak2 = aBak2;
	}
	public String getBak3()
	{
		return Bak3;
	}
	public void setBak3(String aBak3)
	{
		Bak3 = aBak3;
	}
	public String getBak4()
	{
		return Bak4;
	}
	public void setBak4(String aBak4)
	{
		Bak4 = aBak4;
	}
	public String getBak5()
	{
		return Bak5;
	}
	public void setBak5(String aBak5)
	{
		Bak5 = aBak5;
	}
	public String getBak6()
	{
		return Bak6;
	}
	public void setBak6(String aBak6)
	{
		Bak6 = aBak6;
	}
	public String getBak7()
	{
		return Bak7;
	}
	public void setBak7(String aBak7)
	{
		Bak7 = aBak7;
	}
	public String getBak8()
	{
		return Bak8;
	}
	public void setBak8(String aBak8)
	{
		Bak8 = aBak8;
	}
	public String getBak9()
	{
		return Bak9;
	}
	public void setBak9(String aBak9)
	{
		Bak9 = aBak9;
	}
	public String getBak10()
	{
		return Bak10;
	}
	public void setBak10(String aBak10)
	{
		Bak10 = aBak10;
	}

	/**
	* 使用另外一个 WFBnfListSchema 对象给 Schema 赋值
	* @param: aWFBnfListSchema WFBnfListSchema
	**/
	public void setSchema(WFBnfListSchema aWFBnfListSchema)
	{
		this.BatchNo = aWFBnfListSchema.getBatchNo();
		this.CardNo = aWFBnfListSchema.getCardNo();
		this.ToInsuNo = aWFBnfListSchema.getToInsuNo();
		this.ToInsuRela = aWFBnfListSchema.getToInsuRela();
		this.BnfType = aWFBnfListSchema.getBnfType();
		this.BnfGrade = aWFBnfListSchema.getBnfGrade();
		this.BnfGradeNo = aWFBnfListSchema.getBnfGradeNo();
		this.BnfRate = aWFBnfListSchema.getBnfRate();
		this.Name = aWFBnfListSchema.getName();
		this.Sex = aWFBnfListSchema.getSex();
		this.Birthday = aWFBnfListSchema.getBirthday();
		this.IDType = aWFBnfListSchema.getIDType();
		this.IDNo = aWFBnfListSchema.getIDNo();
		this.Bak1 = aWFBnfListSchema.getBak1();
		this.Bak2 = aWFBnfListSchema.getBak2();
		this.Bak3 = aWFBnfListSchema.getBak3();
		this.Bak4 = aWFBnfListSchema.getBak4();
		this.Bak5 = aWFBnfListSchema.getBak5();
		this.Bak6 = aWFBnfListSchema.getBak6();
		this.Bak7 = aWFBnfListSchema.getBak7();
		this.Bak8 = aWFBnfListSchema.getBak8();
		this.Bak9 = aWFBnfListSchema.getBak9();
		this.Bak10 = aWFBnfListSchema.getBak10();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("BatchNo") == null )
				this.BatchNo = null;
			else
				this.BatchNo = rs.getString("BatchNo").trim();

			if( rs.getString("CardNo") == null )
				this.CardNo = null;
			else
				this.CardNo = rs.getString("CardNo").trim();

			if( rs.getString("ToInsuNo") == null )
				this.ToInsuNo = null;
			else
				this.ToInsuNo = rs.getString("ToInsuNo").trim();

			if( rs.getString("ToInsuRela") == null )
				this.ToInsuRela = null;
			else
				this.ToInsuRela = rs.getString("ToInsuRela").trim();

			if( rs.getString("BnfType") == null )
				this.BnfType = null;
			else
				this.BnfType = rs.getString("BnfType").trim();

			if( rs.getString("BnfGrade") == null )
				this.BnfGrade = null;
			else
				this.BnfGrade = rs.getString("BnfGrade").trim();

			if( rs.getString("BnfGradeNo") == null )
				this.BnfGradeNo = null;
			else
				this.BnfGradeNo = rs.getString("BnfGradeNo").trim();

			this.BnfRate = rs.getDouble("BnfRate");
			if( rs.getString("Name") == null )
				this.Name = null;
			else
				this.Name = rs.getString("Name").trim();

			if( rs.getString("Sex") == null )
				this.Sex = null;
			else
				this.Sex = rs.getString("Sex").trim();

			if( rs.getString("Birthday") == null )
				this.Birthday = null;
			else
				this.Birthday = rs.getString("Birthday").trim();

			if( rs.getString("IDType") == null )
				this.IDType = null;
			else
				this.IDType = rs.getString("IDType").trim();

			if( rs.getString("IDNo") == null )
				this.IDNo = null;
			else
				this.IDNo = rs.getString("IDNo").trim();

			if( rs.getString("Bak1") == null )
				this.Bak1 = null;
			else
				this.Bak1 = rs.getString("Bak1").trim();

			if( rs.getString("Bak2") == null )
				this.Bak2 = null;
			else
				this.Bak2 = rs.getString("Bak2").trim();

			if( rs.getString("Bak3") == null )
				this.Bak3 = null;
			else
				this.Bak3 = rs.getString("Bak3").trim();

			if( rs.getString("Bak4") == null )
				this.Bak4 = null;
			else
				this.Bak4 = rs.getString("Bak4").trim();

			if( rs.getString("Bak5") == null )
				this.Bak5 = null;
			else
				this.Bak5 = rs.getString("Bak5").trim();

			if( rs.getString("Bak6") == null )
				this.Bak6 = null;
			else
				this.Bak6 = rs.getString("Bak6").trim();

			if( rs.getString("Bak7") == null )
				this.Bak7 = null;
			else
				this.Bak7 = rs.getString("Bak7").trim();

			if( rs.getString("Bak8") == null )
				this.Bak8 = null;
			else
				this.Bak8 = rs.getString("Bak8").trim();

			if( rs.getString("Bak9") == null )
				this.Bak9 = null;
			else
				this.Bak9 = rs.getString("Bak9").trim();

			if( rs.getString("Bak10") == null )
				this.Bak10 = null;
			else
				this.Bak10 = rs.getString("Bak10").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的WFBnfList表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "WFBnfListSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public WFBnfListSchema getSchema()
	{
		WFBnfListSchema aWFBnfListSchema = new WFBnfListSchema();
		aWFBnfListSchema.setSchema(this);
		return aWFBnfListSchema;
	}

	public WFBnfListDB getDB()
	{
		WFBnfListDB aDBOper = new WFBnfListDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpWFBnfList描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(BatchNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CardNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ToInsuNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ToInsuRela)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BnfType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BnfGrade)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BnfGradeNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(BnfRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Name)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Sex)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Birthday)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IDType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IDNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Bak1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Bak2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Bak3)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Bak4)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Bak5)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Bak6)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Bak7)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Bak8)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Bak9)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Bak10));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpWFBnfList>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			BatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			CardNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			ToInsuNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			ToInsuRela = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			BnfType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			BnfGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			BnfGradeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			BnfRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,8,SysConst.PACKAGESPILTER))).doubleValue();
			Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			Sex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			Birthday = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			IDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			IDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			Bak1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			Bak2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			Bak3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			Bak4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			Bak5 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			Bak6 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			Bak7 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			Bak8 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			Bak9 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			Bak10 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "WFBnfListSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("BatchNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BatchNo));
		}
		if (FCode.equals("CardNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CardNo));
		}
		if (FCode.equals("ToInsuNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ToInsuNo));
		}
		if (FCode.equals("ToInsuRela"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ToInsuRela));
		}
		if (FCode.equals("BnfType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BnfType));
		}
		if (FCode.equals("BnfGrade"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BnfGrade));
		}
		if (FCode.equals("BnfGradeNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BnfGradeNo));
		}
		if (FCode.equals("BnfRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BnfRate));
		}
		if (FCode.equals("Name"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
		}
		if (FCode.equals("Sex"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Sex));
		}
		if (FCode.equals("Birthday"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Birthday));
		}
		if (FCode.equals("IDType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IDType));
		}
		if (FCode.equals("IDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
		}
		if (FCode.equals("Bak1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Bak1));
		}
		if (FCode.equals("Bak2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Bak2));
		}
		if (FCode.equals("Bak3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Bak3));
		}
		if (FCode.equals("Bak4"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Bak4));
		}
		if (FCode.equals("Bak5"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Bak5));
		}
		if (FCode.equals("Bak6"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Bak6));
		}
		if (FCode.equals("Bak7"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Bak7));
		}
		if (FCode.equals("Bak8"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Bak8));
		}
		if (FCode.equals("Bak9"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Bak9));
		}
		if (FCode.equals("Bak10"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Bak10));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(BatchNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(CardNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(ToInsuNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(ToInsuRela);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(BnfType);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(BnfGrade);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(BnfGradeNo);
				break;
			case 7:
				strFieldValue = String.valueOf(BnfRate);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(Name);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(Sex);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(Birthday);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(IDType);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(IDNo);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(Bak1);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(Bak2);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(Bak3);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(Bak4);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(Bak5);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(Bak6);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(Bak7);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(Bak8);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(Bak9);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(Bak10);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("BatchNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BatchNo = FValue.trim();
			}
			else
				BatchNo = null;
		}
		if (FCode.equalsIgnoreCase("CardNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CardNo = FValue.trim();
			}
			else
				CardNo = null;
		}
		if (FCode.equalsIgnoreCase("ToInsuNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ToInsuNo = FValue.trim();
			}
			else
				ToInsuNo = null;
		}
		if (FCode.equalsIgnoreCase("ToInsuRela"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ToInsuRela = FValue.trim();
			}
			else
				ToInsuRela = null;
		}
		if (FCode.equalsIgnoreCase("BnfType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BnfType = FValue.trim();
			}
			else
				BnfType = null;
		}
		if (FCode.equalsIgnoreCase("BnfGrade"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BnfGrade = FValue.trim();
			}
			else
				BnfGrade = null;
		}
		if (FCode.equalsIgnoreCase("BnfGradeNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BnfGradeNo = FValue.trim();
			}
			else
				BnfGradeNo = null;
		}
		if (FCode.equalsIgnoreCase("BnfRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				BnfRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("Name"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Name = FValue.trim();
			}
			else
				Name = null;
		}
		if (FCode.equalsIgnoreCase("Sex"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Sex = FValue.trim();
			}
			else
				Sex = null;
		}
		if (FCode.equalsIgnoreCase("Birthday"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Birthday = FValue.trim();
			}
			else
				Birthday = null;
		}
		if (FCode.equalsIgnoreCase("IDType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IDType = FValue.trim();
			}
			else
				IDType = null;
		}
		if (FCode.equalsIgnoreCase("IDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IDNo = FValue.trim();
			}
			else
				IDNo = null;
		}
		if (FCode.equalsIgnoreCase("Bak1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Bak1 = FValue.trim();
			}
			else
				Bak1 = null;
		}
		if (FCode.equalsIgnoreCase("Bak2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Bak2 = FValue.trim();
			}
			else
				Bak2 = null;
		}
		if (FCode.equalsIgnoreCase("Bak3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Bak3 = FValue.trim();
			}
			else
				Bak3 = null;
		}
		if (FCode.equalsIgnoreCase("Bak4"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Bak4 = FValue.trim();
			}
			else
				Bak4 = null;
		}
		if (FCode.equalsIgnoreCase("Bak5"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Bak5 = FValue.trim();
			}
			else
				Bak5 = null;
		}
		if (FCode.equalsIgnoreCase("Bak6"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Bak6 = FValue.trim();
			}
			else
				Bak6 = null;
		}
		if (FCode.equalsIgnoreCase("Bak7"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Bak7 = FValue.trim();
			}
			else
				Bak7 = null;
		}
		if (FCode.equalsIgnoreCase("Bak8"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Bak8 = FValue.trim();
			}
			else
				Bak8 = null;
		}
		if (FCode.equalsIgnoreCase("Bak9"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Bak9 = FValue.trim();
			}
			else
				Bak9 = null;
		}
		if (FCode.equalsIgnoreCase("Bak10"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Bak10 = FValue.trim();
			}
			else
				Bak10 = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		WFBnfListSchema other = (WFBnfListSchema)otherObject;
		return
			(BatchNo == null ? other.getBatchNo() == null : BatchNo.equals(other.getBatchNo()))
			&& (CardNo == null ? other.getCardNo() == null : CardNo.equals(other.getCardNo()))
			&& (ToInsuNo == null ? other.getToInsuNo() == null : ToInsuNo.equals(other.getToInsuNo()))
			&& (ToInsuRela == null ? other.getToInsuRela() == null : ToInsuRela.equals(other.getToInsuRela()))
			&& (BnfType == null ? other.getBnfType() == null : BnfType.equals(other.getBnfType()))
			&& (BnfGrade == null ? other.getBnfGrade() == null : BnfGrade.equals(other.getBnfGrade()))
			&& (BnfGradeNo == null ? other.getBnfGradeNo() == null : BnfGradeNo.equals(other.getBnfGradeNo()))
			&& BnfRate == other.getBnfRate()
			&& (Name == null ? other.getName() == null : Name.equals(other.getName()))
			&& (Sex == null ? other.getSex() == null : Sex.equals(other.getSex()))
			&& (Birthday == null ? other.getBirthday() == null : Birthday.equals(other.getBirthday()))
			&& (IDType == null ? other.getIDType() == null : IDType.equals(other.getIDType()))
			&& (IDNo == null ? other.getIDNo() == null : IDNo.equals(other.getIDNo()))
			&& (Bak1 == null ? other.getBak1() == null : Bak1.equals(other.getBak1()))
			&& (Bak2 == null ? other.getBak2() == null : Bak2.equals(other.getBak2()))
			&& (Bak3 == null ? other.getBak3() == null : Bak3.equals(other.getBak3()))
			&& (Bak4 == null ? other.getBak4() == null : Bak4.equals(other.getBak4()))
			&& (Bak5 == null ? other.getBak5() == null : Bak5.equals(other.getBak5()))
			&& (Bak6 == null ? other.getBak6() == null : Bak6.equals(other.getBak6()))
			&& (Bak7 == null ? other.getBak7() == null : Bak7.equals(other.getBak7()))
			&& (Bak8 == null ? other.getBak8() == null : Bak8.equals(other.getBak8()))
			&& (Bak9 == null ? other.getBak9() == null : Bak9.equals(other.getBak9()))
			&& (Bak10 == null ? other.getBak10() == null : Bak10.equals(other.getBak10()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("BatchNo") ) {
			return 0;
		}
		if( strFieldName.equals("CardNo") ) {
			return 1;
		}
		if( strFieldName.equals("ToInsuNo") ) {
			return 2;
		}
		if( strFieldName.equals("ToInsuRela") ) {
			return 3;
		}
		if( strFieldName.equals("BnfType") ) {
			return 4;
		}
		if( strFieldName.equals("BnfGrade") ) {
			return 5;
		}
		if( strFieldName.equals("BnfGradeNo") ) {
			return 6;
		}
		if( strFieldName.equals("BnfRate") ) {
			return 7;
		}
		if( strFieldName.equals("Name") ) {
			return 8;
		}
		if( strFieldName.equals("Sex") ) {
			return 9;
		}
		if( strFieldName.equals("Birthday") ) {
			return 10;
		}
		if( strFieldName.equals("IDType") ) {
			return 11;
		}
		if( strFieldName.equals("IDNo") ) {
			return 12;
		}
		if( strFieldName.equals("Bak1") ) {
			return 13;
		}
		if( strFieldName.equals("Bak2") ) {
			return 14;
		}
		if( strFieldName.equals("Bak3") ) {
			return 15;
		}
		if( strFieldName.equals("Bak4") ) {
			return 16;
		}
		if( strFieldName.equals("Bak5") ) {
			return 17;
		}
		if( strFieldName.equals("Bak6") ) {
			return 18;
		}
		if( strFieldName.equals("Bak7") ) {
			return 19;
		}
		if( strFieldName.equals("Bak8") ) {
			return 20;
		}
		if( strFieldName.equals("Bak9") ) {
			return 21;
		}
		if( strFieldName.equals("Bak10") ) {
			return 22;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "BatchNo";
				break;
			case 1:
				strFieldName = "CardNo";
				break;
			case 2:
				strFieldName = "ToInsuNo";
				break;
			case 3:
				strFieldName = "ToInsuRela";
				break;
			case 4:
				strFieldName = "BnfType";
				break;
			case 5:
				strFieldName = "BnfGrade";
				break;
			case 6:
				strFieldName = "BnfGradeNo";
				break;
			case 7:
				strFieldName = "BnfRate";
				break;
			case 8:
				strFieldName = "Name";
				break;
			case 9:
				strFieldName = "Sex";
				break;
			case 10:
				strFieldName = "Birthday";
				break;
			case 11:
				strFieldName = "IDType";
				break;
			case 12:
				strFieldName = "IDNo";
				break;
			case 13:
				strFieldName = "Bak1";
				break;
			case 14:
				strFieldName = "Bak2";
				break;
			case 15:
				strFieldName = "Bak3";
				break;
			case 16:
				strFieldName = "Bak4";
				break;
			case 17:
				strFieldName = "Bak5";
				break;
			case 18:
				strFieldName = "Bak6";
				break;
			case 19:
				strFieldName = "Bak7";
				break;
			case 20:
				strFieldName = "Bak8";
				break;
			case 21:
				strFieldName = "Bak9";
				break;
			case 22:
				strFieldName = "Bak10";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("BatchNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CardNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ToInsuNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ToInsuRela") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BnfType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BnfGrade") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BnfGradeNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BnfRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Name") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Sex") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Birthday") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IDType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IDNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Bak1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Bak2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Bak3") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Bak4") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Bak5") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Bak6") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Bak7") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Bak8") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Bak9") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Bak10") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
