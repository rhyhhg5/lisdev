/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.TPH_LPInsuredDB;

/*
 * <p>ClassName: TPH_LPInsuredSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 税优平台表机构变更
 * @CreateDate：2015-10-14
 */
public class TPH_LPInsuredSchema implements Schema, Cloneable
{
	// @Field
	/** 保单号 */
	private String policyNo;
	/** 保全类型 */
	private String endorsementType;
	/** 保全批单号 */
	private String endorsementNo;
	/** 分单号 */
	private String sequenceNo;
	/** 被保人客户编码 */
	private String customerNo;
	/** 被保人客户姓名 */
	private String name;
	/** 性别 */
	private String gender;
	/** 出生日期 */
	private Date birthday;
	/** 被保险人证件类型 */
	private String certiType;
	/** 被保险人证件号码 */
	private String certiNo;
	/** 国籍 */
	private String nationality;
	/** 手机号码 */
	private String mobileNo;
	/** 常驻地 */
	private String residencePlace;
	/** 医保标识 */
	private String healthFlag;
	/** 社保卡号 */
	private String socialcareNo;
	/** 职业代码 */
	private String jobCode;
	/** 被保险人类型 */
	private String insuredType;
	/** 主被保人编码 */
	private String mainInsuredNo;
	/** 投保人与被保险人关系 */
	private String phInsuredRelation;
	/** 处理日期 */
	private Date makeDate;
	/** 处理时间 */
	private String makeTime;

	public static final int FIELDNUM = 21;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public TPH_LPInsuredSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[5];
		pk[0] = "policyNo";
		pk[1] = "endorsementType";
		pk[2] = "endorsementNo";
		pk[3] = "sequenceNo";
		pk[4] = "customerNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		TPH_LPInsuredSchema cloned = (TPH_LPInsuredSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getpolicyNo()
	{
		return policyNo;
	}
	public void setpolicyNo(String apolicyNo)
	{
		policyNo = apolicyNo;
	}
	public String getendorsementType()
	{
		return endorsementType;
	}
	public void setendorsementType(String aendorsementType)
	{
		endorsementType = aendorsementType;
	}
	public String getendorsementNo()
	{
		return endorsementNo;
	}
	public void setendorsementNo(String aendorsementNo)
	{
		endorsementNo = aendorsementNo;
	}
	public String getsequenceNo()
	{
		return sequenceNo;
	}
	public void setsequenceNo(String asequenceNo)
	{
		sequenceNo = asequenceNo;
	}
	public String getcustomerNo()
	{
		return customerNo;
	}
	public void setcustomerNo(String acustomerNo)
	{
		customerNo = acustomerNo;
	}
	public String getname()
	{
		return name;
	}
	public void setname(String aname)
	{
		name = aname;
	}
	public String getgender()
	{
		return gender;
	}
	public void setgender(String agender)
	{
		gender = agender;
	}
	public String getbirthday()
	{
		if( birthday != null )
			return fDate.getString(birthday);
		else
			return null;
	}
	public void setbirthday(Date abirthday)
	{
		birthday = abirthday;
	}
	public void setbirthday(String abirthday)
	{
		if (abirthday != null && !abirthday.equals("") )
		{
			birthday = fDate.getDate( abirthday );
		}
		else
			birthday = null;
	}

	public String getcertiType()
	{
		return certiType;
	}
	public void setcertiType(String acertiType)
	{
		certiType = acertiType;
	}
	public String getcertiNo()
	{
		return certiNo;
	}
	public void setcertiNo(String acertiNo)
	{
		certiNo = acertiNo;
	}
	public String getnationality()
	{
		return nationality;
	}
	public void setnationality(String anationality)
	{
		nationality = anationality;
	}
	public String getmobileNo()
	{
		return mobileNo;
	}
	public void setmobileNo(String amobileNo)
	{
		mobileNo = amobileNo;
	}
	public String getresidencePlace()
	{
		return residencePlace;
	}
	public void setresidencePlace(String aresidencePlace)
	{
		residencePlace = aresidencePlace;
	}
	public String gethealthFlag()
	{
		return healthFlag;
	}
	public void sethealthFlag(String ahealthFlag)
	{
		healthFlag = ahealthFlag;
	}
	public String getsocialcareNo()
	{
		return socialcareNo;
	}
	public void setsocialcareNo(String asocialcareNo)
	{
		socialcareNo = asocialcareNo;
	}
	public String getjobCode()
	{
		return jobCode;
	}
	public void setjobCode(String ajobCode)
	{
		jobCode = ajobCode;
	}
	public String getinsuredType()
	{
		return insuredType;
	}
	public void setinsuredType(String ainsuredType)
	{
		insuredType = ainsuredType;
	}
	public String getmainInsuredNo()
	{
		return mainInsuredNo;
	}
	public void setmainInsuredNo(String amainInsuredNo)
	{
		mainInsuredNo = amainInsuredNo;
	}
	public String getphInsuredRelation()
	{
		return phInsuredRelation;
	}
	public void setphInsuredRelation(String aphInsuredRelation)
	{
		phInsuredRelation = aphInsuredRelation;
	}
	public String getmakeDate()
	{
		if( makeDate != null )
			return fDate.getString(makeDate);
		else
			return null;
	}
	public void setmakeDate(Date amakeDate)
	{
		makeDate = amakeDate;
	}
	public void setmakeDate(String amakeDate)
	{
		if (amakeDate != null && !amakeDate.equals("") )
		{
			makeDate = fDate.getDate( amakeDate );
		}
		else
			makeDate = null;
	}

	public String getmakeTime()
	{
		return makeTime;
	}
	public void setmakeTime(String amakeTime)
	{
		makeTime = amakeTime;
	}

	/**
	* 使用另外一个 TPH_LPInsuredSchema 对象给 Schema 赋值
	* @param: aTPH_LPInsuredSchema TPH_LPInsuredSchema
	**/
	public void setSchema(TPH_LPInsuredSchema aTPH_LPInsuredSchema)
	{
		this.policyNo = aTPH_LPInsuredSchema.getpolicyNo();
		this.endorsementType = aTPH_LPInsuredSchema.getendorsementType();
		this.endorsementNo = aTPH_LPInsuredSchema.getendorsementNo();
		this.sequenceNo = aTPH_LPInsuredSchema.getsequenceNo();
		this.customerNo = aTPH_LPInsuredSchema.getcustomerNo();
		this.name = aTPH_LPInsuredSchema.getname();
		this.gender = aTPH_LPInsuredSchema.getgender();
		this.birthday = fDate.getDate( aTPH_LPInsuredSchema.getbirthday());
		this.certiType = aTPH_LPInsuredSchema.getcertiType();
		this.certiNo = aTPH_LPInsuredSchema.getcertiNo();
		this.nationality = aTPH_LPInsuredSchema.getnationality();
		this.mobileNo = aTPH_LPInsuredSchema.getmobileNo();
		this.residencePlace = aTPH_LPInsuredSchema.getresidencePlace();
		this.healthFlag = aTPH_LPInsuredSchema.gethealthFlag();
		this.socialcareNo = aTPH_LPInsuredSchema.getsocialcareNo();
		this.jobCode = aTPH_LPInsuredSchema.getjobCode();
		this.insuredType = aTPH_LPInsuredSchema.getinsuredType();
		this.mainInsuredNo = aTPH_LPInsuredSchema.getmainInsuredNo();
		this.phInsuredRelation = aTPH_LPInsuredSchema.getphInsuredRelation();
		this.makeDate = fDate.getDate( aTPH_LPInsuredSchema.getmakeDate());
		this.makeTime = aTPH_LPInsuredSchema.getmakeTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("policyNo") == null )
				this.policyNo = null;
			else
				this.policyNo = rs.getString("policyNo").trim();

			if( rs.getString("endorsementType") == null )
				this.endorsementType = null;
			else
				this.endorsementType = rs.getString("endorsementType").trim();

			if( rs.getString("endorsementNo") == null )
				this.endorsementNo = null;
			else
				this.endorsementNo = rs.getString("endorsementNo").trim();

			if( rs.getString("sequenceNo") == null )
				this.sequenceNo = null;
			else
				this.sequenceNo = rs.getString("sequenceNo").trim();

			if( rs.getString("customerNo") == null )
				this.customerNo = null;
			else
				this.customerNo = rs.getString("customerNo").trim();

			if( rs.getString("name") == null )
				this.name = null;
			else
				this.name = rs.getString("name").trim();

			if( rs.getString("gender") == null )
				this.gender = null;
			else
				this.gender = rs.getString("gender").trim();

			this.birthday = rs.getDate("birthday");
			if( rs.getString("certiType") == null )
				this.certiType = null;
			else
				this.certiType = rs.getString("certiType").trim();

			if( rs.getString("certiNo") == null )
				this.certiNo = null;
			else
				this.certiNo = rs.getString("certiNo").trim();

			if( rs.getString("nationality") == null )
				this.nationality = null;
			else
				this.nationality = rs.getString("nationality").trim();

			if( rs.getString("mobileNo") == null )
				this.mobileNo = null;
			else
				this.mobileNo = rs.getString("mobileNo").trim();

			if( rs.getString("residencePlace") == null )
				this.residencePlace = null;
			else
				this.residencePlace = rs.getString("residencePlace").trim();

			if( rs.getString("healthFlag") == null )
				this.healthFlag = null;
			else
				this.healthFlag = rs.getString("healthFlag").trim();

			if( rs.getString("socialcareNo") == null )
				this.socialcareNo = null;
			else
				this.socialcareNo = rs.getString("socialcareNo").trim();

			if( rs.getString("jobCode") == null )
				this.jobCode = null;
			else
				this.jobCode = rs.getString("jobCode").trim();

			if( rs.getString("insuredType") == null )
				this.insuredType = null;
			else
				this.insuredType = rs.getString("insuredType").trim();

			if( rs.getString("mainInsuredNo") == null )
				this.mainInsuredNo = null;
			else
				this.mainInsuredNo = rs.getString("mainInsuredNo").trim();

			if( rs.getString("phInsuredRelation") == null )
				this.phInsuredRelation = null;
			else
				this.phInsuredRelation = rs.getString("phInsuredRelation").trim();

			this.makeDate = rs.getDate("makeDate");
			if( rs.getString("makeTime") == null )
				this.makeTime = null;
			else
				this.makeTime = rs.getString("makeTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的TPH_LPInsured表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "TPH_LPInsuredSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public TPH_LPInsuredSchema getSchema()
	{
		TPH_LPInsuredSchema aTPH_LPInsuredSchema = new TPH_LPInsuredSchema();
		aTPH_LPInsuredSchema.setSchema(this);
		return aTPH_LPInsuredSchema;
	}

	public TPH_LPInsuredDB getDB()
	{
		TPH_LPInsuredDB aDBOper = new TPH_LPInsuredDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpTPH_LPInsured描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(policyNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(endorsementType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(endorsementNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(sequenceNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(customerNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(name)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(gender)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( birthday ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(certiType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(certiNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(nationality)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(mobileNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(residencePlace)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(healthFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(socialcareNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(jobCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(insuredType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(mainInsuredNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(phInsuredRelation)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( makeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(makeTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpTPH_LPInsured>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			policyNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			endorsementType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			endorsementNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			sequenceNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			customerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			gender = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			birthday = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,SysConst.PACKAGESPILTER));
			certiType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			certiNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			nationality = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			mobileNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			residencePlace = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			healthFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			socialcareNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			jobCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			insuredType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			mainInsuredNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			phInsuredRelation = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			makeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,SysConst.PACKAGESPILTER));
			makeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "TPH_LPInsuredSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("policyNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(policyNo));
		}
		if (FCode.equals("endorsementType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(endorsementType));
		}
		if (FCode.equals("endorsementNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(endorsementNo));
		}
		if (FCode.equals("sequenceNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(sequenceNo));
		}
		if (FCode.equals("customerNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(customerNo));
		}
		if (FCode.equals("name"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(name));
		}
		if (FCode.equals("gender"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(gender));
		}
		if (FCode.equals("birthday"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getbirthday()));
		}
		if (FCode.equals("certiType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(certiType));
		}
		if (FCode.equals("certiNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(certiNo));
		}
		if (FCode.equals("nationality"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(nationality));
		}
		if (FCode.equals("mobileNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(mobileNo));
		}
		if (FCode.equals("residencePlace"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(residencePlace));
		}
		if (FCode.equals("healthFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(healthFlag));
		}
		if (FCode.equals("socialcareNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(socialcareNo));
		}
		if (FCode.equals("jobCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(jobCode));
		}
		if (FCode.equals("insuredType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(insuredType));
		}
		if (FCode.equals("mainInsuredNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(mainInsuredNo));
		}
		if (FCode.equals("phInsuredRelation"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(phInsuredRelation));
		}
		if (FCode.equals("makeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getmakeDate()));
		}
		if (FCode.equals("makeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(makeTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(policyNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(endorsementType);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(endorsementNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(sequenceNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(customerNo);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(name);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(gender);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getbirthday()));
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(certiType);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(certiNo);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(nationality);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(mobileNo);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(residencePlace);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(healthFlag);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(socialcareNo);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(jobCode);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(insuredType);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(mainInsuredNo);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(phInsuredRelation);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getmakeDate()));
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(makeTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("policyNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				policyNo = FValue.trim();
			}
			else
				policyNo = null;
		}
		if (FCode.equalsIgnoreCase("endorsementType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				endorsementType = FValue.trim();
			}
			else
				endorsementType = null;
		}
		if (FCode.equalsIgnoreCase("endorsementNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				endorsementNo = FValue.trim();
			}
			else
				endorsementNo = null;
		}
		if (FCode.equalsIgnoreCase("sequenceNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				sequenceNo = FValue.trim();
			}
			else
				sequenceNo = null;
		}
		if (FCode.equalsIgnoreCase("customerNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				customerNo = FValue.trim();
			}
			else
				customerNo = null;
		}
		if (FCode.equalsIgnoreCase("name"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				name = FValue.trim();
			}
			else
				name = null;
		}
		if (FCode.equalsIgnoreCase("gender"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				gender = FValue.trim();
			}
			else
				gender = null;
		}
		if (FCode.equalsIgnoreCase("birthday"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				birthday = fDate.getDate( FValue );
			}
			else
				birthday = null;
		}
		if (FCode.equalsIgnoreCase("certiType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				certiType = FValue.trim();
			}
			else
				certiType = null;
		}
		if (FCode.equalsIgnoreCase("certiNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				certiNo = FValue.trim();
			}
			else
				certiNo = null;
		}
		if (FCode.equalsIgnoreCase("nationality"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				nationality = FValue.trim();
			}
			else
				nationality = null;
		}
		if (FCode.equalsIgnoreCase("mobileNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				mobileNo = FValue.trim();
			}
			else
				mobileNo = null;
		}
		if (FCode.equalsIgnoreCase("residencePlace"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				residencePlace = FValue.trim();
			}
			else
				residencePlace = null;
		}
		if (FCode.equalsIgnoreCase("healthFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				healthFlag = FValue.trim();
			}
			else
				healthFlag = null;
		}
		if (FCode.equalsIgnoreCase("socialcareNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				socialcareNo = FValue.trim();
			}
			else
				socialcareNo = null;
		}
		if (FCode.equalsIgnoreCase("jobCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				jobCode = FValue.trim();
			}
			else
				jobCode = null;
		}
		if (FCode.equalsIgnoreCase("insuredType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				insuredType = FValue.trim();
			}
			else
				insuredType = null;
		}
		if (FCode.equalsIgnoreCase("mainInsuredNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				mainInsuredNo = FValue.trim();
			}
			else
				mainInsuredNo = null;
		}
		if (FCode.equalsIgnoreCase("phInsuredRelation"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				phInsuredRelation = FValue.trim();
			}
			else
				phInsuredRelation = null;
		}
		if (FCode.equalsIgnoreCase("makeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				makeDate = fDate.getDate( FValue );
			}
			else
				makeDate = null;
		}
		if (FCode.equalsIgnoreCase("makeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				makeTime = FValue.trim();
			}
			else
				makeTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		TPH_LPInsuredSchema other = (TPH_LPInsuredSchema)otherObject;
		return
			(policyNo == null ? other.getpolicyNo() == null : policyNo.equals(other.getpolicyNo()))
			&& (endorsementType == null ? other.getendorsementType() == null : endorsementType.equals(other.getendorsementType()))
			&& (endorsementNo == null ? other.getendorsementNo() == null : endorsementNo.equals(other.getendorsementNo()))
			&& (sequenceNo == null ? other.getsequenceNo() == null : sequenceNo.equals(other.getsequenceNo()))
			&& (customerNo == null ? other.getcustomerNo() == null : customerNo.equals(other.getcustomerNo()))
			&& (name == null ? other.getname() == null : name.equals(other.getname()))
			&& (gender == null ? other.getgender() == null : gender.equals(other.getgender()))
			&& (birthday == null ? other.getbirthday() == null : fDate.getString(birthday).equals(other.getbirthday()))
			&& (certiType == null ? other.getcertiType() == null : certiType.equals(other.getcertiType()))
			&& (certiNo == null ? other.getcertiNo() == null : certiNo.equals(other.getcertiNo()))
			&& (nationality == null ? other.getnationality() == null : nationality.equals(other.getnationality()))
			&& (mobileNo == null ? other.getmobileNo() == null : mobileNo.equals(other.getmobileNo()))
			&& (residencePlace == null ? other.getresidencePlace() == null : residencePlace.equals(other.getresidencePlace()))
			&& (healthFlag == null ? other.gethealthFlag() == null : healthFlag.equals(other.gethealthFlag()))
			&& (socialcareNo == null ? other.getsocialcareNo() == null : socialcareNo.equals(other.getsocialcareNo()))
			&& (jobCode == null ? other.getjobCode() == null : jobCode.equals(other.getjobCode()))
			&& (insuredType == null ? other.getinsuredType() == null : insuredType.equals(other.getinsuredType()))
			&& (mainInsuredNo == null ? other.getmainInsuredNo() == null : mainInsuredNo.equals(other.getmainInsuredNo()))
			&& (phInsuredRelation == null ? other.getphInsuredRelation() == null : phInsuredRelation.equals(other.getphInsuredRelation()))
			&& (makeDate == null ? other.getmakeDate() == null : fDate.getString(makeDate).equals(other.getmakeDate()))
			&& (makeTime == null ? other.getmakeTime() == null : makeTime.equals(other.getmakeTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("policyNo") ) {
			return 0;
		}
		if( strFieldName.equals("endorsementType") ) {
			return 1;
		}
		if( strFieldName.equals("endorsementNo") ) {
			return 2;
		}
		if( strFieldName.equals("sequenceNo") ) {
			return 3;
		}
		if( strFieldName.equals("customerNo") ) {
			return 4;
		}
		if( strFieldName.equals("name") ) {
			return 5;
		}
		if( strFieldName.equals("gender") ) {
			return 6;
		}
		if( strFieldName.equals("birthday") ) {
			return 7;
		}
		if( strFieldName.equals("certiType") ) {
			return 8;
		}
		if( strFieldName.equals("certiNo") ) {
			return 9;
		}
		if( strFieldName.equals("nationality") ) {
			return 10;
		}
		if( strFieldName.equals("mobileNo") ) {
			return 11;
		}
		if( strFieldName.equals("residencePlace") ) {
			return 12;
		}
		if( strFieldName.equals("healthFlag") ) {
			return 13;
		}
		if( strFieldName.equals("socialcareNo") ) {
			return 14;
		}
		if( strFieldName.equals("jobCode") ) {
			return 15;
		}
		if( strFieldName.equals("insuredType") ) {
			return 16;
		}
		if( strFieldName.equals("mainInsuredNo") ) {
			return 17;
		}
		if( strFieldName.equals("phInsuredRelation") ) {
			return 18;
		}
		if( strFieldName.equals("makeDate") ) {
			return 19;
		}
		if( strFieldName.equals("makeTime") ) {
			return 20;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "policyNo";
				break;
			case 1:
				strFieldName = "endorsementType";
				break;
			case 2:
				strFieldName = "endorsementNo";
				break;
			case 3:
				strFieldName = "sequenceNo";
				break;
			case 4:
				strFieldName = "customerNo";
				break;
			case 5:
				strFieldName = "name";
				break;
			case 6:
				strFieldName = "gender";
				break;
			case 7:
				strFieldName = "birthday";
				break;
			case 8:
				strFieldName = "certiType";
				break;
			case 9:
				strFieldName = "certiNo";
				break;
			case 10:
				strFieldName = "nationality";
				break;
			case 11:
				strFieldName = "mobileNo";
				break;
			case 12:
				strFieldName = "residencePlace";
				break;
			case 13:
				strFieldName = "healthFlag";
				break;
			case 14:
				strFieldName = "socialcareNo";
				break;
			case 15:
				strFieldName = "jobCode";
				break;
			case 16:
				strFieldName = "insuredType";
				break;
			case 17:
				strFieldName = "mainInsuredNo";
				break;
			case 18:
				strFieldName = "phInsuredRelation";
				break;
			case 19:
				strFieldName = "makeDate";
				break;
			case 20:
				strFieldName = "makeTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("policyNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("endorsementType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("endorsementNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("sequenceNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("customerNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("name") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("gender") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("birthday") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("certiType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("certiNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("nationality") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("mobileNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("residencePlace") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("healthFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("socialcareNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("jobCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("insuredType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("mainInsuredNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("phInsuredRelation") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("makeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("makeTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
