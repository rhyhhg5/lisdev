/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LCGrpContRoadDB;

/*
 * <p>ClassName: LCGrpContRoadSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 一带一路
 * @CreateDate：2017-11-22
 */
public class LCGrpContRoadSchema implements Schema, Cloneable
{
	// @Field
	/** 印刷号 */
	private String PrtNo;
	/** 管理机构 */
	private String ManageCom;
	/** 国家 */
	private String Country;
	/** 国家类别 */
	private String CountryCategory;
	/** 国家系数 */
	private String CountryFactor;
	/** 是否参与建筑施工项目 */
	private String EngineeringFlag;
	/** 工程类别 */
	private String EngineeringCategory;
	/** 工程系数 */
	private String EngineeringFactor;
	/** 规模系数 */
	private String ScaleFactor;
	/** 平均年龄 */
	private String Avage;
	/** 保险区间 */
	private String Insureyear;
	/** 计算后保费 */
	private String CalculPrem;
	/** 整单系数 */
	private String TotalFactor;
	/** 录入保费 */
	private String InputPrem;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 备用 */
	private String Spare;
	/** 备用2 */
	private String Spare2;
	/** 备用3 */
	private String Spare3;
	/** 备用4 */
	private String Spare4;
	/** 备用5 */
	private String Spare5;
	/** 备用6 */
	private String Spare6;
	/** 备用7 */
	private String Spare7;

	public static final int FIELDNUM = 25;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LCGrpContRoadSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "PrtNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LCGrpContRoadSchema cloned = (LCGrpContRoadSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getPrtNo()
	{
		return PrtNo;
	}
	public void setPrtNo(String aPrtNo)
	{
		PrtNo = aPrtNo;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getCountry()
	{
		return Country;
	}
	public void setCountry(String aCountry)
	{
		Country = aCountry;
	}
	public String getCountryCategory()
	{
		return CountryCategory;
	}
	public void setCountryCategory(String aCountryCategory)
	{
		CountryCategory = aCountryCategory;
	}
	public String getCountryFactor()
	{
		return CountryFactor;
	}
	public void setCountryFactor(String aCountryFactor)
	{
		CountryFactor = aCountryFactor;
	}
	public String getEngineeringFlag()
	{
		return EngineeringFlag;
	}
	public void setEngineeringFlag(String aEngineeringFlag)
	{
		EngineeringFlag = aEngineeringFlag;
	}
	public String getEngineeringCategory()
	{
		return EngineeringCategory;
	}
	public void setEngineeringCategory(String aEngineeringCategory)
	{
		EngineeringCategory = aEngineeringCategory;
	}
	public String getEngineeringFactor()
	{
		return EngineeringFactor;
	}
	public void setEngineeringFactor(String aEngineeringFactor)
	{
		EngineeringFactor = aEngineeringFactor;
	}
	public String getScaleFactor()
	{
		return ScaleFactor;
	}
	public void setScaleFactor(String aScaleFactor)
	{
		ScaleFactor = aScaleFactor;
	}
	public String getAvage()
	{
		return Avage;
	}
	public void setAvage(String aAvage)
	{
		Avage = aAvage;
	}
	public String getInsureyear()
	{
		return Insureyear;
	}
	public void setInsureyear(String aInsureyear)
	{
		Insureyear = aInsureyear;
	}
	public String getCalculPrem()
	{
		return CalculPrem;
	}
	public void setCalculPrem(String aCalculPrem)
	{
		CalculPrem = aCalculPrem;
	}
	public String getTotalFactor()
	{
		return TotalFactor;
	}
	public void setTotalFactor(String aTotalFactor)
	{
		TotalFactor = aTotalFactor;
	}
	public String getInputPrem()
	{
		return InputPrem;
	}
	public void setInputPrem(String aInputPrem)
	{
		InputPrem = aInputPrem;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getSpare()
	{
		return Spare;
	}
	public void setSpare(String aSpare)
	{
		Spare = aSpare;
	}
	public String getSpare2()
	{
		return Spare2;
	}
	public void setSpare2(String aSpare2)
	{
		Spare2 = aSpare2;
	}
	public String getSpare3()
	{
		return Spare3;
	}
	public void setSpare3(String aSpare3)
	{
		Spare3 = aSpare3;
	}
	public String getSpare4()
	{
		return Spare4;
	}
	public void setSpare4(String aSpare4)
	{
		Spare4 = aSpare4;
	}
	public String getSpare5()
	{
		return Spare5;
	}
	public void setSpare5(String aSpare5)
	{
		Spare5 = aSpare5;
	}
	public String getSpare6()
	{
		return Spare6;
	}
	public void setSpare6(String aSpare6)
	{
		Spare6 = aSpare6;
	}
	public String getSpare7()
	{
		return Spare7;
	}
	public void setSpare7(String aSpare7)
	{
		Spare7 = aSpare7;
	}

	/**
	* 使用另外一个 LCGrpContRoadSchema 对象给 Schema 赋值
	* @param: aLCGrpContRoadSchema LCGrpContRoadSchema
	**/
	public void setSchema(LCGrpContRoadSchema aLCGrpContRoadSchema)
	{
		this.PrtNo = aLCGrpContRoadSchema.getPrtNo();
		this.ManageCom = aLCGrpContRoadSchema.getManageCom();
		this.Country = aLCGrpContRoadSchema.getCountry();
		this.CountryCategory = aLCGrpContRoadSchema.getCountryCategory();
		this.CountryFactor = aLCGrpContRoadSchema.getCountryFactor();
		this.EngineeringFlag = aLCGrpContRoadSchema.getEngineeringFlag();
		this.EngineeringCategory = aLCGrpContRoadSchema.getEngineeringCategory();
		this.EngineeringFactor = aLCGrpContRoadSchema.getEngineeringFactor();
		this.ScaleFactor = aLCGrpContRoadSchema.getScaleFactor();
		this.Avage = aLCGrpContRoadSchema.getAvage();
		this.Insureyear = aLCGrpContRoadSchema.getInsureyear();
		this.CalculPrem = aLCGrpContRoadSchema.getCalculPrem();
		this.TotalFactor = aLCGrpContRoadSchema.getTotalFactor();
		this.InputPrem = aLCGrpContRoadSchema.getInputPrem();
		this.MakeDate = fDate.getDate( aLCGrpContRoadSchema.getMakeDate());
		this.MakeTime = aLCGrpContRoadSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLCGrpContRoadSchema.getModifyDate());
		this.ModifyTime = aLCGrpContRoadSchema.getModifyTime();
		this.Spare = aLCGrpContRoadSchema.getSpare();
		this.Spare2 = aLCGrpContRoadSchema.getSpare2();
		this.Spare3 = aLCGrpContRoadSchema.getSpare3();
		this.Spare4 = aLCGrpContRoadSchema.getSpare4();
		this.Spare5 = aLCGrpContRoadSchema.getSpare5();
		this.Spare6 = aLCGrpContRoadSchema.getSpare6();
		this.Spare7 = aLCGrpContRoadSchema.getSpare7();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("PrtNo") == null )
				this.PrtNo = null;
			else
				this.PrtNo = rs.getString("PrtNo").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("Country") == null )
				this.Country = null;
			else
				this.Country = rs.getString("Country").trim();

			if( rs.getString("CountryCategory") == null )
				this.CountryCategory = null;
			else
				this.CountryCategory = rs.getString("CountryCategory").trim();

			if( rs.getString("CountryFactor") == null )
				this.CountryFactor = null;
			else
				this.CountryFactor = rs.getString("CountryFactor").trim();

			if( rs.getString("EngineeringFlag") == null )
				this.EngineeringFlag = null;
			else
				this.EngineeringFlag = rs.getString("EngineeringFlag").trim();

			if( rs.getString("EngineeringCategory") == null )
				this.EngineeringCategory = null;
			else
				this.EngineeringCategory = rs.getString("EngineeringCategory").trim();

			if( rs.getString("EngineeringFactor") == null )
				this.EngineeringFactor = null;
			else
				this.EngineeringFactor = rs.getString("EngineeringFactor").trim();

			if( rs.getString("ScaleFactor") == null )
				this.ScaleFactor = null;
			else
				this.ScaleFactor = rs.getString("ScaleFactor").trim();

			if( rs.getString("Avage") == null )
				this.Avage = null;
			else
				this.Avage = rs.getString("Avage").trim();

			if( rs.getString("Insureyear") == null )
				this.Insureyear = null;
			else
				this.Insureyear = rs.getString("Insureyear").trim();

			if( rs.getString("CalculPrem") == null )
				this.CalculPrem = null;
			else
				this.CalculPrem = rs.getString("CalculPrem").trim();

			if( rs.getString("TotalFactor") == null )
				this.TotalFactor = null;
			else
				this.TotalFactor = rs.getString("TotalFactor").trim();

			if( rs.getString("InputPrem") == null )
				this.InputPrem = null;
			else
				this.InputPrem = rs.getString("InputPrem").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("Spare") == null )
				this.Spare = null;
			else
				this.Spare = rs.getString("Spare").trim();

			if( rs.getString("Spare2") == null )
				this.Spare2 = null;
			else
				this.Spare2 = rs.getString("Spare2").trim();

			if( rs.getString("Spare3") == null )
				this.Spare3 = null;
			else
				this.Spare3 = rs.getString("Spare3").trim();

			if( rs.getString("Spare4") == null )
				this.Spare4 = null;
			else
				this.Spare4 = rs.getString("Spare4").trim();

			if( rs.getString("Spare5") == null )
				this.Spare5 = null;
			else
				this.Spare5 = rs.getString("Spare5").trim();

			if( rs.getString("Spare6") == null )
				this.Spare6 = null;
			else
				this.Spare6 = rs.getString("Spare6").trim();

			if( rs.getString("Spare7") == null )
				this.Spare7 = null;
			else
				this.Spare7 = rs.getString("Spare7").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LCGrpContRoad表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCGrpContRoadSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LCGrpContRoadSchema getSchema()
	{
		LCGrpContRoadSchema aLCGrpContRoadSchema = new LCGrpContRoadSchema();
		aLCGrpContRoadSchema.setSchema(this);
		return aLCGrpContRoadSchema;
	}

	public LCGrpContRoadDB getDB()
	{
		LCGrpContRoadDB aDBOper = new LCGrpContRoadDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCGrpContRoad描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(PrtNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Country)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CountryCategory)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CountryFactor)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EngineeringFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EngineeringCategory)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EngineeringFactor)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ScaleFactor)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Avage)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Insureyear)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CalculPrem)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TotalFactor)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InputPrem)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Spare)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Spare2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Spare3)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Spare4)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Spare5)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Spare6)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Spare7));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCGrpContRoad>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			Country = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			CountryCategory = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			CountryFactor = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			EngineeringFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			EngineeringCategory = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			EngineeringFactor = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			ScaleFactor = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			Avage = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			Insureyear = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			CalculPrem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			TotalFactor = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			InputPrem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			Spare = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			Spare2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			Spare3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			Spare4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			Spare5 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			Spare6 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			Spare7 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCGrpContRoadSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("PrtNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("Country"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Country));
		}
		if (FCode.equals("CountryCategory"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CountryCategory));
		}
		if (FCode.equals("CountryFactor"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CountryFactor));
		}
		if (FCode.equals("EngineeringFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EngineeringFlag));
		}
		if (FCode.equals("EngineeringCategory"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EngineeringCategory));
		}
		if (FCode.equals("EngineeringFactor"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EngineeringFactor));
		}
		if (FCode.equals("ScaleFactor"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ScaleFactor));
		}
		if (FCode.equals("Avage"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Avage));
		}
		if (FCode.equals("Insureyear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Insureyear));
		}
		if (FCode.equals("CalculPrem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CalculPrem));
		}
		if (FCode.equals("TotalFactor"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TotalFactor));
		}
		if (FCode.equals("InputPrem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InputPrem));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("Spare"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Spare));
		}
		if (FCode.equals("Spare2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Spare2));
		}
		if (FCode.equals("Spare3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Spare3));
		}
		if (FCode.equals("Spare4"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Spare4));
		}
		if (FCode.equals("Spare5"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Spare5));
		}
		if (FCode.equals("Spare6"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Spare6));
		}
		if (FCode.equals("Spare7"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Spare7));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(PrtNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(Country);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(CountryCategory);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(CountryFactor);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(EngineeringFlag);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(EngineeringCategory);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(EngineeringFactor);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(ScaleFactor);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(Avage);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(Insureyear);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(CalculPrem);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(TotalFactor);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(InputPrem);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(Spare);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(Spare2);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(Spare3);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(Spare4);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(Spare5);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(Spare6);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(Spare7);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("PrtNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PrtNo = FValue.trim();
			}
			else
				PrtNo = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("Country"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Country = FValue.trim();
			}
			else
				Country = null;
		}
		if (FCode.equalsIgnoreCase("CountryCategory"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CountryCategory = FValue.trim();
			}
			else
				CountryCategory = null;
		}
		if (FCode.equalsIgnoreCase("CountryFactor"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CountryFactor = FValue.trim();
			}
			else
				CountryFactor = null;
		}
		if (FCode.equalsIgnoreCase("EngineeringFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EngineeringFlag = FValue.trim();
			}
			else
				EngineeringFlag = null;
		}
		if (FCode.equalsIgnoreCase("EngineeringCategory"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EngineeringCategory = FValue.trim();
			}
			else
				EngineeringCategory = null;
		}
		if (FCode.equalsIgnoreCase("EngineeringFactor"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EngineeringFactor = FValue.trim();
			}
			else
				EngineeringFactor = null;
		}
		if (FCode.equalsIgnoreCase("ScaleFactor"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ScaleFactor = FValue.trim();
			}
			else
				ScaleFactor = null;
		}
		if (FCode.equalsIgnoreCase("Avage"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Avage = FValue.trim();
			}
			else
				Avage = null;
		}
		if (FCode.equalsIgnoreCase("Insureyear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Insureyear = FValue.trim();
			}
			else
				Insureyear = null;
		}
		if (FCode.equalsIgnoreCase("CalculPrem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CalculPrem = FValue.trim();
			}
			else
				CalculPrem = null;
		}
		if (FCode.equalsIgnoreCase("TotalFactor"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TotalFactor = FValue.trim();
			}
			else
				TotalFactor = null;
		}
		if (FCode.equalsIgnoreCase("InputPrem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InputPrem = FValue.trim();
			}
			else
				InputPrem = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("Spare"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Spare = FValue.trim();
			}
			else
				Spare = null;
		}
		if (FCode.equalsIgnoreCase("Spare2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Spare2 = FValue.trim();
			}
			else
				Spare2 = null;
		}
		if (FCode.equalsIgnoreCase("Spare3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Spare3 = FValue.trim();
			}
			else
				Spare3 = null;
		}
		if (FCode.equalsIgnoreCase("Spare4"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Spare4 = FValue.trim();
			}
			else
				Spare4 = null;
		}
		if (FCode.equalsIgnoreCase("Spare5"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Spare5 = FValue.trim();
			}
			else
				Spare5 = null;
		}
		if (FCode.equalsIgnoreCase("Spare6"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Spare6 = FValue.trim();
			}
			else
				Spare6 = null;
		}
		if (FCode.equalsIgnoreCase("Spare7"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Spare7 = FValue.trim();
			}
			else
				Spare7 = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LCGrpContRoadSchema other = (LCGrpContRoadSchema)otherObject;
		return
			(PrtNo == null ? other.getPrtNo() == null : PrtNo.equals(other.getPrtNo()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (Country == null ? other.getCountry() == null : Country.equals(other.getCountry()))
			&& (CountryCategory == null ? other.getCountryCategory() == null : CountryCategory.equals(other.getCountryCategory()))
			&& (CountryFactor == null ? other.getCountryFactor() == null : CountryFactor.equals(other.getCountryFactor()))
			&& (EngineeringFlag == null ? other.getEngineeringFlag() == null : EngineeringFlag.equals(other.getEngineeringFlag()))
			&& (EngineeringCategory == null ? other.getEngineeringCategory() == null : EngineeringCategory.equals(other.getEngineeringCategory()))
			&& (EngineeringFactor == null ? other.getEngineeringFactor() == null : EngineeringFactor.equals(other.getEngineeringFactor()))
			&& (ScaleFactor == null ? other.getScaleFactor() == null : ScaleFactor.equals(other.getScaleFactor()))
			&& (Avage == null ? other.getAvage() == null : Avage.equals(other.getAvage()))
			&& (Insureyear == null ? other.getInsureyear() == null : Insureyear.equals(other.getInsureyear()))
			&& (CalculPrem == null ? other.getCalculPrem() == null : CalculPrem.equals(other.getCalculPrem()))
			&& (TotalFactor == null ? other.getTotalFactor() == null : TotalFactor.equals(other.getTotalFactor()))
			&& (InputPrem == null ? other.getInputPrem() == null : InputPrem.equals(other.getInputPrem()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (Spare == null ? other.getSpare() == null : Spare.equals(other.getSpare()))
			&& (Spare2 == null ? other.getSpare2() == null : Spare2.equals(other.getSpare2()))
			&& (Spare3 == null ? other.getSpare3() == null : Spare3.equals(other.getSpare3()))
			&& (Spare4 == null ? other.getSpare4() == null : Spare4.equals(other.getSpare4()))
			&& (Spare5 == null ? other.getSpare5() == null : Spare5.equals(other.getSpare5()))
			&& (Spare6 == null ? other.getSpare6() == null : Spare6.equals(other.getSpare6()))
			&& (Spare7 == null ? other.getSpare7() == null : Spare7.equals(other.getSpare7()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("PrtNo") ) {
			return 0;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 1;
		}
		if( strFieldName.equals("Country") ) {
			return 2;
		}
		if( strFieldName.equals("CountryCategory") ) {
			return 3;
		}
		if( strFieldName.equals("CountryFactor") ) {
			return 4;
		}
		if( strFieldName.equals("EngineeringFlag") ) {
			return 5;
		}
		if( strFieldName.equals("EngineeringCategory") ) {
			return 6;
		}
		if( strFieldName.equals("EngineeringFactor") ) {
			return 7;
		}
		if( strFieldName.equals("ScaleFactor") ) {
			return 8;
		}
		if( strFieldName.equals("Avage") ) {
			return 9;
		}
		if( strFieldName.equals("Insureyear") ) {
			return 10;
		}
		if( strFieldName.equals("CalculPrem") ) {
			return 11;
		}
		if( strFieldName.equals("TotalFactor") ) {
			return 12;
		}
		if( strFieldName.equals("InputPrem") ) {
			return 13;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 14;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 15;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 16;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 17;
		}
		if( strFieldName.equals("Spare") ) {
			return 18;
		}
		if( strFieldName.equals("Spare2") ) {
			return 19;
		}
		if( strFieldName.equals("Spare3") ) {
			return 20;
		}
		if( strFieldName.equals("Spare4") ) {
			return 21;
		}
		if( strFieldName.equals("Spare5") ) {
			return 22;
		}
		if( strFieldName.equals("Spare6") ) {
			return 23;
		}
		if( strFieldName.equals("Spare7") ) {
			return 24;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "PrtNo";
				break;
			case 1:
				strFieldName = "ManageCom";
				break;
			case 2:
				strFieldName = "Country";
				break;
			case 3:
				strFieldName = "CountryCategory";
				break;
			case 4:
				strFieldName = "CountryFactor";
				break;
			case 5:
				strFieldName = "EngineeringFlag";
				break;
			case 6:
				strFieldName = "EngineeringCategory";
				break;
			case 7:
				strFieldName = "EngineeringFactor";
				break;
			case 8:
				strFieldName = "ScaleFactor";
				break;
			case 9:
				strFieldName = "Avage";
				break;
			case 10:
				strFieldName = "Insureyear";
				break;
			case 11:
				strFieldName = "CalculPrem";
				break;
			case 12:
				strFieldName = "TotalFactor";
				break;
			case 13:
				strFieldName = "InputPrem";
				break;
			case 14:
				strFieldName = "MakeDate";
				break;
			case 15:
				strFieldName = "MakeTime";
				break;
			case 16:
				strFieldName = "ModifyDate";
				break;
			case 17:
				strFieldName = "ModifyTime";
				break;
			case 18:
				strFieldName = "Spare";
				break;
			case 19:
				strFieldName = "Spare2";
				break;
			case 20:
				strFieldName = "Spare3";
				break;
			case 21:
				strFieldName = "Spare4";
				break;
			case 22:
				strFieldName = "Spare5";
				break;
			case 23:
				strFieldName = "Spare6";
				break;
			case 24:
				strFieldName = "Spare7";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("PrtNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Country") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CountryCategory") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CountryFactor") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EngineeringFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EngineeringCategory") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EngineeringFactor") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ScaleFactor") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Avage") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Insureyear") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CalculPrem") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TotalFactor") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InputPrem") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Spare") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Spare2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Spare3") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Spare4") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Spare5") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Spare6") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Spare7") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
