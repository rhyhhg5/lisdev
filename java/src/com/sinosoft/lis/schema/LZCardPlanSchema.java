/*
 * <p>ClassName: LZCardPlanSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LZCardPlanDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LZCardPlanSchema implements Schema
{
    // @Field
    /** 计划标识 */
    private String PlanID;
    /** 单证编码 */
    private String CertifyCode;
    /** 申请数量 */
    private double AppCount;
    /** 申请人 */
    private String AppOperator;
    /** 申请机构 */
    private String AppCom;
    /** 批复数量 */
    private double RetCount;
    /** 批复人 */
    private String RetOperator;
    /** 批复机构 */
    private String RetCom;
    /** 批复情况 */
    private String RetState;
    /** 关联计划 */
    private String RelaPlan;
    /** 关联印刷 */
    private String RelaPrint;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 更新日期 */
    private Date UpdateDate;
    /** 更新时间 */
    private String UpdateTime;
    /** 计划状态 */
    private String PlanState;

    public static final int FIELDNUM = 16; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LZCardPlanSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "PlanID";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getPlanID()
    {
        if (PlanID != null && !PlanID.equals("") && SysConst.CHANGECHARSET == true)
        {
            PlanID = StrTool.unicodeToGBK(PlanID);
        }
        return PlanID;
    }

    public void setPlanID(String aPlanID)
    {
        PlanID = aPlanID;
    }

    public String getCertifyCode()
    {
        if (CertifyCode != null && !CertifyCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CertifyCode = StrTool.unicodeToGBK(CertifyCode);
        }
        return CertifyCode;
    }

    public void setCertifyCode(String aCertifyCode)
    {
        CertifyCode = aCertifyCode;
    }

    public double getAppCount()
    {
        return AppCount;
    }

    public void setAppCount(double aAppCount)
    {
        AppCount = aAppCount;
    }

    public void setAppCount(String aAppCount)
    {
        if (aAppCount != null && !aAppCount.equals(""))
        {
            Double tDouble = new Double(aAppCount);
            double d = tDouble.doubleValue();
            AppCount = d;
        }
    }

    public String getAppOperator()
    {
        if (AppOperator != null && !AppOperator.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AppOperator = StrTool.unicodeToGBK(AppOperator);
        }
        return AppOperator;
    }

    public void setAppOperator(String aAppOperator)
    {
        AppOperator = aAppOperator;
    }

    public String getAppCom()
    {
        if (AppCom != null && !AppCom.equals("") && SysConst.CHANGECHARSET == true)
        {
            AppCom = StrTool.unicodeToGBK(AppCom);
        }
        return AppCom;
    }

    public void setAppCom(String aAppCom)
    {
        AppCom = aAppCom;
    }

    public double getRetCount()
    {
        return RetCount;
    }

    public void setRetCount(double aRetCount)
    {
        RetCount = aRetCount;
    }

    public void setRetCount(String aRetCount)
    {
        if (aRetCount != null && !aRetCount.equals(""))
        {
            Double tDouble = new Double(aRetCount);
            double d = tDouble.doubleValue();
            RetCount = d;
        }
    }

    public String getRetOperator()
    {
        if (RetOperator != null && !RetOperator.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            RetOperator = StrTool.unicodeToGBK(RetOperator);
        }
        return RetOperator;
    }

    public void setRetOperator(String aRetOperator)
    {
        RetOperator = aRetOperator;
    }

    public String getRetCom()
    {
        if (RetCom != null && !RetCom.equals("") && SysConst.CHANGECHARSET == true)
        {
            RetCom = StrTool.unicodeToGBK(RetCom);
        }
        return RetCom;
    }

    public void setRetCom(String aRetCom)
    {
        RetCom = aRetCom;
    }

    public String getRetState()
    {
        if (RetState != null && !RetState.equals("") && SysConst.CHANGECHARSET == true)
        {
            RetState = StrTool.unicodeToGBK(RetState);
        }
        return RetState;
    }

    public void setRetState(String aRetState)
    {
        RetState = aRetState;
    }

    public String getRelaPlan()
    {
        if (RelaPlan != null && !RelaPlan.equals("") && SysConst.CHANGECHARSET == true)
        {
            RelaPlan = StrTool.unicodeToGBK(RelaPlan);
        }
        return RelaPlan;
    }

    public void setRelaPlan(String aRelaPlan)
    {
        RelaPlan = aRelaPlan;
    }

    public String getRelaPrint()
    {
        if (RelaPrint != null && !RelaPrint.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            RelaPrint = StrTool.unicodeToGBK(RelaPrint);
        }
        return RelaPrint;
    }

    public void setRelaPrint(String aRelaPrint)
    {
        RelaPrint = aRelaPrint;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getUpdateDate()
    {
        if (UpdateDate != null)
        {
            return fDate.getString(UpdateDate);
        }
        else
        {
            return null;
        }
    }

    public void setUpdateDate(Date aUpdateDate)
    {
        UpdateDate = aUpdateDate;
    }

    public void setUpdateDate(String aUpdateDate)
    {
        if (aUpdateDate != null && !aUpdateDate.equals(""))
        {
            UpdateDate = fDate.getDate(aUpdateDate);
        }
        else
        {
            UpdateDate = null;
        }
    }

    public String getUpdateTime()
    {
        if (UpdateTime != null && !UpdateTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            UpdateTime = StrTool.unicodeToGBK(UpdateTime);
        }
        return UpdateTime;
    }

    public void setUpdateTime(String aUpdateTime)
    {
        UpdateTime = aUpdateTime;
    }

    public String getPlanState()
    {
        if (PlanState != null && !PlanState.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            PlanState = StrTool.unicodeToGBK(PlanState);
        }
        return PlanState;
    }

    public void setPlanState(String aPlanState)
    {
        PlanState = aPlanState;
    }

    /**
     * 使用另外一个 LZCardPlanSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LZCardPlanSchema aLZCardPlanSchema)
    {
        this.PlanID = aLZCardPlanSchema.getPlanID();
        this.CertifyCode = aLZCardPlanSchema.getCertifyCode();
        this.AppCount = aLZCardPlanSchema.getAppCount();
        this.AppOperator = aLZCardPlanSchema.getAppOperator();
        this.AppCom = aLZCardPlanSchema.getAppCom();
        this.RetCount = aLZCardPlanSchema.getRetCount();
        this.RetOperator = aLZCardPlanSchema.getRetOperator();
        this.RetCom = aLZCardPlanSchema.getRetCom();
        this.RetState = aLZCardPlanSchema.getRetState();
        this.RelaPlan = aLZCardPlanSchema.getRelaPlan();
        this.RelaPrint = aLZCardPlanSchema.getRelaPrint();
        this.MakeDate = fDate.getDate(aLZCardPlanSchema.getMakeDate());
        this.MakeTime = aLZCardPlanSchema.getMakeTime();
        this.UpdateDate = fDate.getDate(aLZCardPlanSchema.getUpdateDate());
        this.UpdateTime = aLZCardPlanSchema.getUpdateTime();
        this.PlanState = aLZCardPlanSchema.getPlanState();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("PlanID") == null)
            {
                this.PlanID = null;
            }
            else
            {
                this.PlanID = rs.getString("PlanID").trim();
            }

            if (rs.getString("CertifyCode") == null)
            {
                this.CertifyCode = null;
            }
            else
            {
                this.CertifyCode = rs.getString("CertifyCode").trim();
            }

            this.AppCount = rs.getDouble("AppCount");
            if (rs.getString("AppOperator") == null)
            {
                this.AppOperator = null;
            }
            else
            {
                this.AppOperator = rs.getString("AppOperator").trim();
            }

            if (rs.getString("AppCom") == null)
            {
                this.AppCom = null;
            }
            else
            {
                this.AppCom = rs.getString("AppCom").trim();
            }

            this.RetCount = rs.getDouble("RetCount");
            if (rs.getString("RetOperator") == null)
            {
                this.RetOperator = null;
            }
            else
            {
                this.RetOperator = rs.getString("RetOperator").trim();
            }

            if (rs.getString("RetCom") == null)
            {
                this.RetCom = null;
            }
            else
            {
                this.RetCom = rs.getString("RetCom").trim();
            }

            if (rs.getString("RetState") == null)
            {
                this.RetState = null;
            }
            else
            {
                this.RetState = rs.getString("RetState").trim();
            }

            if (rs.getString("RelaPlan") == null)
            {
                this.RelaPlan = null;
            }
            else
            {
                this.RelaPlan = rs.getString("RelaPlan").trim();
            }

            if (rs.getString("RelaPrint") == null)
            {
                this.RelaPrint = null;
            }
            else
            {
                this.RelaPrint = rs.getString("RelaPrint").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.UpdateDate = rs.getDate("UpdateDate");
            if (rs.getString("UpdateTime") == null)
            {
                this.UpdateTime = null;
            }
            else
            {
                this.UpdateTime = rs.getString("UpdateTime").trim();
            }

            if (rs.getString("PlanState") == null)
            {
                this.PlanState = null;
            }
            else
            {
                this.PlanState = rs.getString("PlanState").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LZCardPlanSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LZCardPlanSchema getSchema()
    {
        LZCardPlanSchema aLZCardPlanSchema = new LZCardPlanSchema();
        aLZCardPlanSchema.setSchema(this);
        return aLZCardPlanSchema;
    }

    public LZCardPlanDB getDB()
    {
        LZCardPlanDB aDBOper = new LZCardPlanDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLZCardPlan描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(PlanID)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CertifyCode)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(AppCount) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AppOperator)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AppCom)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(RetCount) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RetOperator)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RetCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RetState)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RelaPlan)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RelaPrint)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            UpdateDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(UpdateTime)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PlanState));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLZCardPlan>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            PlanID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                    SysConst.PACKAGESPILTER);
            CertifyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                         SysConst.PACKAGESPILTER);
            AppCount = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 3, SysConst.PACKAGESPILTER))).doubleValue();
            AppOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                         SysConst.PACKAGESPILTER);
            AppCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                    SysConst.PACKAGESPILTER);
            RetCount = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 6, SysConst.PACKAGESPILTER))).doubleValue();
            RetOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                         SysConst.PACKAGESPILTER);
            RetCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                    SysConst.PACKAGESPILTER);
            RetState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                      SysConst.PACKAGESPILTER);
            RelaPlan = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                      SysConst.PACKAGESPILTER);
            RelaPrint = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                       SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 12, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                      SysConst.PACKAGESPILTER);
            UpdateDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 14, SysConst.PACKAGESPILTER));
            UpdateTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                        SysConst.PACKAGESPILTER);
            PlanState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                       SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LZCardPlanSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("PlanID"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PlanID));
        }
        if (FCode.equals("CertifyCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CertifyCode));
        }
        if (FCode.equals("AppCount"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AppCount));
        }
        if (FCode.equals("AppOperator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AppOperator));
        }
        if (FCode.equals("AppCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AppCom));
        }
        if (FCode.equals("RetCount"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RetCount));
        }
        if (FCode.equals("RetOperator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RetOperator));
        }
        if (FCode.equals("RetCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RetCom));
        }
        if (FCode.equals("RetState"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RetState));
        }
        if (FCode.equals("RelaPlan"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RelaPlan));
        }
        if (FCode.equals("RelaPrint"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RelaPrint));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("UpdateDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getUpdateDate()));
        }
        if (FCode.equals("UpdateTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(UpdateTime));
        }
        if (FCode.equals("PlanState"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PlanState));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(PlanID);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(CertifyCode);
                break;
            case 2:
                strFieldValue = String.valueOf(AppCount);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(AppOperator);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(AppCom);
                break;
            case 5:
                strFieldValue = String.valueOf(RetCount);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(RetOperator);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(RetCom);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(RetState);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(RelaPlan);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(RelaPrint);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getUpdateDate()));
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(UpdateTime);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(PlanState);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("PlanID"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PlanID = FValue.trim();
            }
            else
            {
                PlanID = null;
            }
        }
        if (FCode.equals("CertifyCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CertifyCode = FValue.trim();
            }
            else
            {
                CertifyCode = null;
            }
        }
        if (FCode.equals("AppCount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                AppCount = d;
            }
        }
        if (FCode.equals("AppOperator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppOperator = FValue.trim();
            }
            else
            {
                AppOperator = null;
            }
        }
        if (FCode.equals("AppCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppCom = FValue.trim();
            }
            else
            {
                AppCom = null;
            }
        }
        if (FCode.equals("RetCount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                RetCount = d;
            }
        }
        if (FCode.equals("RetOperator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RetOperator = FValue.trim();
            }
            else
            {
                RetOperator = null;
            }
        }
        if (FCode.equals("RetCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RetCom = FValue.trim();
            }
            else
            {
                RetCom = null;
            }
        }
        if (FCode.equals("RetState"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RetState = FValue.trim();
            }
            else
            {
                RetState = null;
            }
        }
        if (FCode.equals("RelaPlan"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RelaPlan = FValue.trim();
            }
            else
            {
                RelaPlan = null;
            }
        }
        if (FCode.equals("RelaPrint"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RelaPrint = FValue.trim();
            }
            else
            {
                RelaPrint = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("UpdateDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UpdateDate = fDate.getDate(FValue);
            }
            else
            {
                UpdateDate = null;
            }
        }
        if (FCode.equals("UpdateTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UpdateTime = FValue.trim();
            }
            else
            {
                UpdateTime = null;
            }
        }
        if (FCode.equals("PlanState"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PlanState = FValue.trim();
            }
            else
            {
                PlanState = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LZCardPlanSchema other = (LZCardPlanSchema) otherObject;
        return
                PlanID.equals(other.getPlanID())
                && CertifyCode.equals(other.getCertifyCode())
                && AppCount == other.getAppCount()
                && AppOperator.equals(other.getAppOperator())
                && AppCom.equals(other.getAppCom())
                && RetCount == other.getRetCount()
                && RetOperator.equals(other.getRetOperator())
                && RetCom.equals(other.getRetCom())
                && RetState.equals(other.getRetState())
                && RelaPlan.equals(other.getRelaPlan())
                && RelaPrint.equals(other.getRelaPrint())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(UpdateDate).equals(other.getUpdateDate())
                && UpdateTime.equals(other.getUpdateTime())
                && PlanState.equals(other.getPlanState());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("PlanID"))
        {
            return 0;
        }
        if (strFieldName.equals("CertifyCode"))
        {
            return 1;
        }
        if (strFieldName.equals("AppCount"))
        {
            return 2;
        }
        if (strFieldName.equals("AppOperator"))
        {
            return 3;
        }
        if (strFieldName.equals("AppCom"))
        {
            return 4;
        }
        if (strFieldName.equals("RetCount"))
        {
            return 5;
        }
        if (strFieldName.equals("RetOperator"))
        {
            return 6;
        }
        if (strFieldName.equals("RetCom"))
        {
            return 7;
        }
        if (strFieldName.equals("RetState"))
        {
            return 8;
        }
        if (strFieldName.equals("RelaPlan"))
        {
            return 9;
        }
        if (strFieldName.equals("RelaPrint"))
        {
            return 10;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 11;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 12;
        }
        if (strFieldName.equals("UpdateDate"))
        {
            return 13;
        }
        if (strFieldName.equals("UpdateTime"))
        {
            return 14;
        }
        if (strFieldName.equals("PlanState"))
        {
            return 15;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "PlanID";
                break;
            case 1:
                strFieldName = "CertifyCode";
                break;
            case 2:
                strFieldName = "AppCount";
                break;
            case 3:
                strFieldName = "AppOperator";
                break;
            case 4:
                strFieldName = "AppCom";
                break;
            case 5:
                strFieldName = "RetCount";
                break;
            case 6:
                strFieldName = "RetOperator";
                break;
            case 7:
                strFieldName = "RetCom";
                break;
            case 8:
                strFieldName = "RetState";
                break;
            case 9:
                strFieldName = "RelaPlan";
                break;
            case 10:
                strFieldName = "RelaPrint";
                break;
            case 11:
                strFieldName = "MakeDate";
                break;
            case 12:
                strFieldName = "MakeTime";
                break;
            case 13:
                strFieldName = "UpdateDate";
                break;
            case 14:
                strFieldName = "UpdateTime";
                break;
            case 15:
                strFieldName = "PlanState";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("PlanID"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CertifyCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppCount"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("AppOperator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RetCount"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("RetOperator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RetCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RetState"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RelaPlan"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RelaPrint"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UpdateDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("UpdateTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PlanState"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
