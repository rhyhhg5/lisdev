/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LOMCommissionDB;

/*
 * <p>ClassName: LOMCommissionSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LOMCommission
 * @CreateDate：2010-09-10
 */
public class LOMCommissionSchema implements Schema, Cloneable
{
	// @Field
	/** 代理方出单人员工号 */
	private String operatorcode;
	/** 子公司代码 */
	private String comp_cod;
	/** 子公司名称 */
	private String comp_nam;
	/** 管理机构编号 */
	private String man_org_cod;
	/** 管理机构名称 */
	private String man_org_nam;
	/** 姓名 */
	private String sales_nam;
	/** 出生日期 */
	private String date_birthd;
	/** 性别代码 */
	private String sex_cod;
	/** 证件类型代码 */
	private String idtyp_cod;
	/** 证件号 */
	private String paperworkid;
	/** 联系电话 */
	private String telephone;
	/** 联系手机 */
	private String sales_mob;
	/** 电子邮件 */
	private String sales_mail;
	/** 在职状态代码 */
	private String status_cod;
	/** 在职状态名称 */
	private String status;
	/** 职能 */
	private String function;
	/** 入职时间 */
	private String entrant_date;
	/** 离司时间 */
	private String dimission_date;
	/** 数据变更类型 */
	private String data_upd_typ;
	/** 财险委托方机构编码 */
	private String prt_entrust_orgid;
	/** 财险委托方机构名称 */
	private String prt_entrust_orgname;
	/** 健康险委托方机构编码 */
	private String li_entrust_orgid;
	/** 健康险委托方机构名称 */
	private String li_entrust_orgname;
	/** 寿险委托方机构编码 */
	private String hi_entrust_orgid;
	/** 寿险委托方机构名称 */
	private String hi_entrust_orgname;
	/** 财险审批时间 */
	private String prt_check_date;
	/** 健康险审批时间 */
	private String li_check_date;
	/** 寿险审批时间 */
	private String hi_check_date;
	/** 入机日期 */
	private Date makedate;
	/** 日期时间 */
	private String maketime;
	/** 修改日期 */
	private Date modifydate;
	/** 修改时间 */
	private String modifytime;
	/** 用户编码 */
	private String usercode;
	/** 密码 */
	private String password;
	/** 流水号 */
	private String Serialno;

	public static final int FIELDNUM = 35;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LOMCommissionSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[3];
		pk[0] = "operatorcode";
		pk[1] = "comp_cod";
		pk[2] = "man_org_cod";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LOMCommissionSchema cloned = (LOMCommissionSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getoperatorcode()
	{
		return operatorcode;
	}
	public void setoperatorcode(String aoperatorcode)
	{
		operatorcode = aoperatorcode;
	}
	public String getcomp_cod()
	{
		return comp_cod;
	}
	public void setcomp_cod(String acomp_cod)
	{
		comp_cod = acomp_cod;
	}
	public String getcomp_nam()
	{
		return comp_nam;
	}
	public void setcomp_nam(String acomp_nam)
	{
		comp_nam = acomp_nam;
	}
	public String getman_org_cod()
	{
		return man_org_cod;
	}
	public void setman_org_cod(String aman_org_cod)
	{
		man_org_cod = aman_org_cod;
	}
	public String getman_org_nam()
	{
		return man_org_nam;
	}
	public void setman_org_nam(String aman_org_nam)
	{
		man_org_nam = aman_org_nam;
	}
	public String getsales_nam()
	{
		return sales_nam;
	}
	public void setsales_nam(String asales_nam)
	{
		sales_nam = asales_nam;
	}
	public String getdate_birthd()
	{
		return date_birthd;
	}
	public void setdate_birthd(String adate_birthd)
	{
		date_birthd = adate_birthd;
	}
	public String getsex_cod()
	{
		return sex_cod;
	}
	public void setsex_cod(String asex_cod)
	{
		sex_cod = asex_cod;
	}
	public String getidtyp_cod()
	{
		return idtyp_cod;
	}
	public void setidtyp_cod(String aidtyp_cod)
	{
		idtyp_cod = aidtyp_cod;
	}
	public String getpaperworkid()
	{
		return paperworkid;
	}
	public void setpaperworkid(String apaperworkid)
	{
		paperworkid = apaperworkid;
	}
	public String gettelephone()
	{
		return telephone;
	}
	public void settelephone(String atelephone)
	{
		telephone = atelephone;
	}
	public String getsales_mob()
	{
		return sales_mob;
	}
	public void setsales_mob(String asales_mob)
	{
		sales_mob = asales_mob;
	}
	public String getsales_mail()
	{
		return sales_mail;
	}
	public void setsales_mail(String asales_mail)
	{
		sales_mail = asales_mail;
	}
	public String getstatus_cod()
	{
		return status_cod;
	}
	public void setstatus_cod(String astatus_cod)
	{
		status_cod = astatus_cod;
	}
	public String getstatus()
	{
		return status;
	}
	public void setstatus(String astatus)
	{
		status = astatus;
	}
	public String getfunction()
	{
		return function;
	}
	public void setfunction(String afunction)
	{
		function = afunction;
	}
	public String getentrant_date()
	{
		return entrant_date;
	}
	public void setentrant_date(String aentrant_date)
	{
		entrant_date = aentrant_date;
	}
	public String getdimission_date()
	{
		return dimission_date;
	}
	public void setdimission_date(String adimission_date)
	{
		dimission_date = adimission_date;
	}
	public String getdata_upd_typ()
	{
		return data_upd_typ;
	}
	public void setdata_upd_typ(String adata_upd_typ)
	{
		data_upd_typ = adata_upd_typ;
	}
	public String getprt_entrust_orgid()
	{
		return prt_entrust_orgid;
	}
	public void setprt_entrust_orgid(String aprt_entrust_orgid)
	{
		prt_entrust_orgid = aprt_entrust_orgid;
	}
	public String getprt_entrust_orgname()
	{
		return prt_entrust_orgname;
	}
	public void setprt_entrust_orgname(String aprt_entrust_orgname)
	{
		prt_entrust_orgname = aprt_entrust_orgname;
	}
	public String getli_entrust_orgid()
	{
		return li_entrust_orgid;
	}
	public void setli_entrust_orgid(String ali_entrust_orgid)
	{
		li_entrust_orgid = ali_entrust_orgid;
	}
	public String getli_entrust_orgname()
	{
		return li_entrust_orgname;
	}
	public void setli_entrust_orgname(String ali_entrust_orgname)
	{
		li_entrust_orgname = ali_entrust_orgname;
	}
	public String gethi_entrust_orgid()
	{
		return hi_entrust_orgid;
	}
	public void sethi_entrust_orgid(String ahi_entrust_orgid)
	{
		hi_entrust_orgid = ahi_entrust_orgid;
	}
	public String gethi_entrust_orgname()
	{
		return hi_entrust_orgname;
	}
	public void sethi_entrust_orgname(String ahi_entrust_orgname)
	{
		hi_entrust_orgname = ahi_entrust_orgname;
	}
	public String getprt_check_date()
	{
		return prt_check_date;
	}
	public void setprt_check_date(String aprt_check_date)
	{
		prt_check_date = aprt_check_date;
	}
	public String getli_check_date()
	{
		return li_check_date;
	}
	public void setli_check_date(String ali_check_date)
	{
		li_check_date = ali_check_date;
	}
	public String gethi_check_date()
	{
		return hi_check_date;
	}
	public void sethi_check_date(String ahi_check_date)
	{
		hi_check_date = ahi_check_date;
	}
	public String getmakedate()
	{
		if( makedate != null )
			return fDate.getString(makedate);
		else
			return null;
	}
	public void setmakedate(Date amakedate)
	{
		makedate = amakedate;
	}
	public void setmakedate(String amakedate)
	{
		if (amakedate != null && !amakedate.equals("") )
		{
			makedate = fDate.getDate( amakedate );
		}
		else
			makedate = null;
	}

	public String getmaketime()
	{
		return maketime;
	}
	public void setmaketime(String amaketime)
	{
		maketime = amaketime;
	}
	public String getmodifydate()
	{
		if( modifydate != null )
			return fDate.getString(modifydate);
		else
			return null;
	}
	public void setmodifydate(Date amodifydate)
	{
		modifydate = amodifydate;
	}
	public void setmodifydate(String amodifydate)
	{
		if (amodifydate != null && !amodifydate.equals("") )
		{
			modifydate = fDate.getDate( amodifydate );
		}
		else
			modifydate = null;
	}

	public String getmodifytime()
	{
		return modifytime;
	}
	public void setmodifytime(String amodifytime)
	{
		modifytime = amodifytime;
	}
	public String getusercode()
	{
		return usercode;
	}
	public void setusercode(String ausercode)
	{
		usercode = ausercode;
	}
	public String getpassword()
	{
		return password;
	}
	public void setpassword(String apassword)
	{
		password = apassword;
	}
	public String getSerialno()
	{
		return Serialno;
	}
	public void setSerialno(String aSerialno)
	{
		Serialno = aSerialno;
	}

	/**
	* 使用另外一个 LOMCommissionSchema 对象给 Schema 赋值
	* @param: aLOMCommissionSchema LOMCommissionSchema
	**/
	public void setSchema(LOMCommissionSchema aLOMCommissionSchema)
	{
		this.operatorcode = aLOMCommissionSchema.getoperatorcode();
		this.comp_cod = aLOMCommissionSchema.getcomp_cod();
		this.comp_nam = aLOMCommissionSchema.getcomp_nam();
		this.man_org_cod = aLOMCommissionSchema.getman_org_cod();
		this.man_org_nam = aLOMCommissionSchema.getman_org_nam();
		this.sales_nam = aLOMCommissionSchema.getsales_nam();
		this.date_birthd = aLOMCommissionSchema.getdate_birthd();
		this.sex_cod = aLOMCommissionSchema.getsex_cod();
		this.idtyp_cod = aLOMCommissionSchema.getidtyp_cod();
		this.paperworkid = aLOMCommissionSchema.getpaperworkid();
		this.telephone = aLOMCommissionSchema.gettelephone();
		this.sales_mob = aLOMCommissionSchema.getsales_mob();
		this.sales_mail = aLOMCommissionSchema.getsales_mail();
		this.status_cod = aLOMCommissionSchema.getstatus_cod();
		this.status = aLOMCommissionSchema.getstatus();
		this.function = aLOMCommissionSchema.getfunction();
		this.entrant_date = aLOMCommissionSchema.getentrant_date();
		this.dimission_date = aLOMCommissionSchema.getdimission_date();
		this.data_upd_typ = aLOMCommissionSchema.getdata_upd_typ();
		this.prt_entrust_orgid = aLOMCommissionSchema.getprt_entrust_orgid();
		this.prt_entrust_orgname = aLOMCommissionSchema.getprt_entrust_orgname();
		this.li_entrust_orgid = aLOMCommissionSchema.getli_entrust_orgid();
		this.li_entrust_orgname = aLOMCommissionSchema.getli_entrust_orgname();
		this.hi_entrust_orgid = aLOMCommissionSchema.gethi_entrust_orgid();
		this.hi_entrust_orgname = aLOMCommissionSchema.gethi_entrust_orgname();
		this.prt_check_date = aLOMCommissionSchema.getprt_check_date();
		this.li_check_date = aLOMCommissionSchema.getli_check_date();
		this.hi_check_date = aLOMCommissionSchema.gethi_check_date();
		this.makedate = fDate.getDate( aLOMCommissionSchema.getmakedate());
		this.maketime = aLOMCommissionSchema.getmaketime();
		this.modifydate = fDate.getDate( aLOMCommissionSchema.getmodifydate());
		this.modifytime = aLOMCommissionSchema.getmodifytime();
		this.usercode = aLOMCommissionSchema.getusercode();
		this.password = aLOMCommissionSchema.getpassword();
		this.Serialno = aLOMCommissionSchema.getSerialno();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("operatorcode") == null )
				this.operatorcode = null;
			else
				this.operatorcode = rs.getString("operatorcode").trim();

			if( rs.getString("comp_cod") == null )
				this.comp_cod = null;
			else
				this.comp_cod = rs.getString("comp_cod").trim();

			if( rs.getString("comp_nam") == null )
				this.comp_nam = null;
			else
				this.comp_nam = rs.getString("comp_nam").trim();

			if( rs.getString("man_org_cod") == null )
				this.man_org_cod = null;
			else
				this.man_org_cod = rs.getString("man_org_cod").trim();

			if( rs.getString("man_org_nam") == null )
				this.man_org_nam = null;
			else
				this.man_org_nam = rs.getString("man_org_nam").trim();

			if( rs.getString("sales_nam") == null )
				this.sales_nam = null;
			else
				this.sales_nam = rs.getString("sales_nam").trim();

			if( rs.getString("date_birthd") == null )
				this.date_birthd = null;
			else
				this.date_birthd = rs.getString("date_birthd").trim();

			if( rs.getString("sex_cod") == null )
				this.sex_cod = null;
			else
				this.sex_cod = rs.getString("sex_cod").trim();

			if( rs.getString("idtyp_cod") == null )
				this.idtyp_cod = null;
			else
				this.idtyp_cod = rs.getString("idtyp_cod").trim();

			if( rs.getString("paperworkid") == null )
				this.paperworkid = null;
			else
				this.paperworkid = rs.getString("paperworkid").trim();

			if( rs.getString("telephone") == null )
				this.telephone = null;
			else
				this.telephone = rs.getString("telephone").trim();

			if( rs.getString("sales_mob") == null )
				this.sales_mob = null;
			else
				this.sales_mob = rs.getString("sales_mob").trim();

			if( rs.getString("sales_mail") == null )
				this.sales_mail = null;
			else
				this.sales_mail = rs.getString("sales_mail").trim();

			if( rs.getString("status_cod") == null )
				this.status_cod = null;
			else
				this.status_cod = rs.getString("status_cod").trim();

			if( rs.getString("status") == null )
				this.status = null;
			else
				this.status = rs.getString("status").trim();

			if( rs.getString("function") == null )
				this.function = null;
			else
				this.function = rs.getString("function").trim();

			if( rs.getString("entrant_date") == null )
				this.entrant_date = null;
			else
				this.entrant_date = rs.getString("entrant_date").trim();

			if( rs.getString("dimission_date") == null )
				this.dimission_date = null;
			else
				this.dimission_date = rs.getString("dimission_date").trim();

			if( rs.getString("data_upd_typ") == null )
				this.data_upd_typ = null;
			else
				this.data_upd_typ = rs.getString("data_upd_typ").trim();

			if( rs.getString("prt_entrust_orgid") == null )
				this.prt_entrust_orgid = null;
			else
				this.prt_entrust_orgid = rs.getString("prt_entrust_orgid").trim();

			if( rs.getString("prt_entrust_orgname") == null )
				this.prt_entrust_orgname = null;
			else
				this.prt_entrust_orgname = rs.getString("prt_entrust_orgname").trim();

			if( rs.getString("li_entrust_orgid") == null )
				this.li_entrust_orgid = null;
			else
				this.li_entrust_orgid = rs.getString("li_entrust_orgid").trim();

			if( rs.getString("li_entrust_orgname") == null )
				this.li_entrust_orgname = null;
			else
				this.li_entrust_orgname = rs.getString("li_entrust_orgname").trim();

			if( rs.getString("hi_entrust_orgid") == null )
				this.hi_entrust_orgid = null;
			else
				this.hi_entrust_orgid = rs.getString("hi_entrust_orgid").trim();

			if( rs.getString("hi_entrust_orgname") == null )
				this.hi_entrust_orgname = null;
			else
				this.hi_entrust_orgname = rs.getString("hi_entrust_orgname").trim();

			if( rs.getString("prt_check_date") == null )
				this.prt_check_date = null;
			else
				this.prt_check_date = rs.getString("prt_check_date").trim();

			if( rs.getString("li_check_date") == null )
				this.li_check_date = null;
			else
				this.li_check_date = rs.getString("li_check_date").trim();

			if( rs.getString("hi_check_date") == null )
				this.hi_check_date = null;
			else
				this.hi_check_date = rs.getString("hi_check_date").trim();

			this.makedate = rs.getDate("makedate");
			if( rs.getString("maketime") == null )
				this.maketime = null;
			else
				this.maketime = rs.getString("maketime").trim();

			this.modifydate = rs.getDate("modifydate");
			if( rs.getString("modifytime") == null )
				this.modifytime = null;
			else
				this.modifytime = rs.getString("modifytime").trim();

			if( rs.getString("usercode") == null )
				this.usercode = null;
			else
				this.usercode = rs.getString("usercode").trim();

			if( rs.getString("password") == null )
				this.password = null;
			else
				this.password = rs.getString("password").trim();

			if( rs.getString("Serialno") == null )
				this.Serialno = null;
			else
				this.Serialno = rs.getString("Serialno").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LOMCommission表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LOMCommissionSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LOMCommissionSchema getSchema()
	{
		LOMCommissionSchema aLOMCommissionSchema = new LOMCommissionSchema();
		aLOMCommissionSchema.setSchema(this);
		return aLOMCommissionSchema;
	}

	public LOMCommissionDB getDB()
	{
		LOMCommissionDB aDBOper = new LOMCommissionDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOMCommission描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(operatorcode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(comp_cod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(comp_nam)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(man_org_cod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(man_org_nam)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(sales_nam)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(date_birthd)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(sex_cod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(idtyp_cod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(paperworkid)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(telephone)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(sales_mob)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(sales_mail)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(status_cod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(status)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(function)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(entrant_date)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(dimission_date)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(data_upd_typ)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(prt_entrust_orgid)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(prt_entrust_orgname)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(li_entrust_orgid)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(li_entrust_orgname)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(hi_entrust_orgid)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(hi_entrust_orgname)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(prt_check_date)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(li_check_date)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(hi_check_date)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( makedate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(maketime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( modifydate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(modifytime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(usercode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(password)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Serialno));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOMCommission>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			operatorcode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			comp_cod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			comp_nam = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			man_org_cod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			man_org_nam = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			sales_nam = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			date_birthd = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			sex_cod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			idtyp_cod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			paperworkid = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			telephone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			sales_mob = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			sales_mail = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			status_cod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			status = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			function = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			entrant_date = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			dimission_date = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			data_upd_typ = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			prt_entrust_orgid = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			prt_entrust_orgname = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			li_entrust_orgid = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			li_entrust_orgname = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			hi_entrust_orgid = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			hi_entrust_orgname = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			prt_check_date = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			li_check_date = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			hi_check_date = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			makedate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29,SysConst.PACKAGESPILTER));
			maketime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			modifydate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31,SysConst.PACKAGESPILTER));
			modifytime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
			usercode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			password = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			Serialno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LOMCommissionSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("operatorcode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(operatorcode));
		}
		if (FCode.equals("comp_cod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(comp_cod));
		}
		if (FCode.equals("comp_nam"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(comp_nam));
		}
		if (FCode.equals("man_org_cod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(man_org_cod));
		}
		if (FCode.equals("man_org_nam"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(man_org_nam));
		}
		if (FCode.equals("sales_nam"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(sales_nam));
		}
		if (FCode.equals("date_birthd"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(date_birthd));
		}
		if (FCode.equals("sex_cod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(sex_cod));
		}
		if (FCode.equals("idtyp_cod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(idtyp_cod));
		}
		if (FCode.equals("paperworkid"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(paperworkid));
		}
		if (FCode.equals("telephone"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(telephone));
		}
		if (FCode.equals("sales_mob"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(sales_mob));
		}
		if (FCode.equals("sales_mail"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(sales_mail));
		}
		if (FCode.equals("status_cod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(status_cod));
		}
		if (FCode.equals("status"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(status));
		}
		if (FCode.equals("function"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(function));
		}
		if (FCode.equals("entrant_date"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(entrant_date));
		}
		if (FCode.equals("dimission_date"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(dimission_date));
		}
		if (FCode.equals("data_upd_typ"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(data_upd_typ));
		}
		if (FCode.equals("prt_entrust_orgid"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(prt_entrust_orgid));
		}
		if (FCode.equals("prt_entrust_orgname"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(prt_entrust_orgname));
		}
		if (FCode.equals("li_entrust_orgid"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(li_entrust_orgid));
		}
		if (FCode.equals("li_entrust_orgname"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(li_entrust_orgname));
		}
		if (FCode.equals("hi_entrust_orgid"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(hi_entrust_orgid));
		}
		if (FCode.equals("hi_entrust_orgname"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(hi_entrust_orgname));
		}
		if (FCode.equals("prt_check_date"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(prt_check_date));
		}
		if (FCode.equals("li_check_date"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(li_check_date));
		}
		if (FCode.equals("hi_check_date"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(hi_check_date));
		}
		if (FCode.equals("makedate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getmakedate()));
		}
		if (FCode.equals("maketime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(maketime));
		}
		if (FCode.equals("modifydate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getmodifydate()));
		}
		if (FCode.equals("modifytime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(modifytime));
		}
		if (FCode.equals("usercode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(usercode));
		}
		if (FCode.equals("password"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(password));
		}
		if (FCode.equals("Serialno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Serialno));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(operatorcode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(comp_cod);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(comp_nam);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(man_org_cod);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(man_org_nam);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(sales_nam);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(date_birthd);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(sex_cod);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(idtyp_cod);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(paperworkid);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(telephone);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(sales_mob);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(sales_mail);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(status_cod);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(status);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(function);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(entrant_date);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(dimission_date);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(data_upd_typ);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(prt_entrust_orgid);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(prt_entrust_orgname);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(li_entrust_orgid);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(li_entrust_orgname);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(hi_entrust_orgid);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(hi_entrust_orgname);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(prt_check_date);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(li_check_date);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(hi_check_date);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getmakedate()));
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(maketime);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getmodifydate()));
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(modifytime);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(usercode);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(password);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(Serialno);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("operatorcode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				operatorcode = FValue.trim();
			}
			else
				operatorcode = null;
		}
		if (FCode.equalsIgnoreCase("comp_cod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				comp_cod = FValue.trim();
			}
			else
				comp_cod = null;
		}
		if (FCode.equalsIgnoreCase("comp_nam"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				comp_nam = FValue.trim();
			}
			else
				comp_nam = null;
		}
		if (FCode.equalsIgnoreCase("man_org_cod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				man_org_cod = FValue.trim();
			}
			else
				man_org_cod = null;
		}
		if (FCode.equalsIgnoreCase("man_org_nam"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				man_org_nam = FValue.trim();
			}
			else
				man_org_nam = null;
		}
		if (FCode.equalsIgnoreCase("sales_nam"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				sales_nam = FValue.trim();
			}
			else
				sales_nam = null;
		}
		if (FCode.equalsIgnoreCase("date_birthd"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				date_birthd = FValue.trim();
			}
			else
				date_birthd = null;
		}
		if (FCode.equalsIgnoreCase("sex_cod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				sex_cod = FValue.trim();
			}
			else
				sex_cod = null;
		}
		if (FCode.equalsIgnoreCase("idtyp_cod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				idtyp_cod = FValue.trim();
			}
			else
				idtyp_cod = null;
		}
		if (FCode.equalsIgnoreCase("paperworkid"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				paperworkid = FValue.trim();
			}
			else
				paperworkid = null;
		}
		if (FCode.equalsIgnoreCase("telephone"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				telephone = FValue.trim();
			}
			else
				telephone = null;
		}
		if (FCode.equalsIgnoreCase("sales_mob"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				sales_mob = FValue.trim();
			}
			else
				sales_mob = null;
		}
		if (FCode.equalsIgnoreCase("sales_mail"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				sales_mail = FValue.trim();
			}
			else
				sales_mail = null;
		}
		if (FCode.equalsIgnoreCase("status_cod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				status_cod = FValue.trim();
			}
			else
				status_cod = null;
		}
		if (FCode.equalsIgnoreCase("status"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				status = FValue.trim();
			}
			else
				status = null;
		}
		if (FCode.equalsIgnoreCase("function"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				function = FValue.trim();
			}
			else
				function = null;
		}
		if (FCode.equalsIgnoreCase("entrant_date"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				entrant_date = FValue.trim();
			}
			else
				entrant_date = null;
		}
		if (FCode.equalsIgnoreCase("dimission_date"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				dimission_date = FValue.trim();
			}
			else
				dimission_date = null;
		}
		if (FCode.equalsIgnoreCase("data_upd_typ"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				data_upd_typ = FValue.trim();
			}
			else
				data_upd_typ = null;
		}
		if (FCode.equalsIgnoreCase("prt_entrust_orgid"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				prt_entrust_orgid = FValue.trim();
			}
			else
				prt_entrust_orgid = null;
		}
		if (FCode.equalsIgnoreCase("prt_entrust_orgname"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				prt_entrust_orgname = FValue.trim();
			}
			else
				prt_entrust_orgname = null;
		}
		if (FCode.equalsIgnoreCase("li_entrust_orgid"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				li_entrust_orgid = FValue.trim();
			}
			else
				li_entrust_orgid = null;
		}
		if (FCode.equalsIgnoreCase("li_entrust_orgname"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				li_entrust_orgname = FValue.trim();
			}
			else
				li_entrust_orgname = null;
		}
		if (FCode.equalsIgnoreCase("hi_entrust_orgid"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				hi_entrust_orgid = FValue.trim();
			}
			else
				hi_entrust_orgid = null;
		}
		if (FCode.equalsIgnoreCase("hi_entrust_orgname"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				hi_entrust_orgname = FValue.trim();
			}
			else
				hi_entrust_orgname = null;
		}
		if (FCode.equalsIgnoreCase("prt_check_date"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				prt_check_date = FValue.trim();
			}
			else
				prt_check_date = null;
		}
		if (FCode.equalsIgnoreCase("li_check_date"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				li_check_date = FValue.trim();
			}
			else
				li_check_date = null;
		}
		if (FCode.equalsIgnoreCase("hi_check_date"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				hi_check_date = FValue.trim();
			}
			else
				hi_check_date = null;
		}
		if (FCode.equalsIgnoreCase("makedate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				makedate = fDate.getDate( FValue );
			}
			else
				makedate = null;
		}
		if (FCode.equalsIgnoreCase("maketime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				maketime = FValue.trim();
			}
			else
				maketime = null;
		}
		if (FCode.equalsIgnoreCase("modifydate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				modifydate = fDate.getDate( FValue );
			}
			else
				modifydate = null;
		}
		if (FCode.equalsIgnoreCase("modifytime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				modifytime = FValue.trim();
			}
			else
				modifytime = null;
		}
		if (FCode.equalsIgnoreCase("usercode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				usercode = FValue.trim();
			}
			else
				usercode = null;
		}
		if (FCode.equalsIgnoreCase("password"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				password = FValue.trim();
			}
			else
				password = null;
		}
		if (FCode.equalsIgnoreCase("Serialno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Serialno = FValue.trim();
			}
			else
				Serialno = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LOMCommissionSchema other = (LOMCommissionSchema)otherObject;
		return
			(operatorcode == null ? other.getoperatorcode() == null : operatorcode.equals(other.getoperatorcode()))
			&& (comp_cod == null ? other.getcomp_cod() == null : comp_cod.equals(other.getcomp_cod()))
			&& (comp_nam == null ? other.getcomp_nam() == null : comp_nam.equals(other.getcomp_nam()))
			&& (man_org_cod == null ? other.getman_org_cod() == null : man_org_cod.equals(other.getman_org_cod()))
			&& (man_org_nam == null ? other.getman_org_nam() == null : man_org_nam.equals(other.getman_org_nam()))
			&& (sales_nam == null ? other.getsales_nam() == null : sales_nam.equals(other.getsales_nam()))
			&& (date_birthd == null ? other.getdate_birthd() == null : date_birthd.equals(other.getdate_birthd()))
			&& (sex_cod == null ? other.getsex_cod() == null : sex_cod.equals(other.getsex_cod()))
			&& (idtyp_cod == null ? other.getidtyp_cod() == null : idtyp_cod.equals(other.getidtyp_cod()))
			&& (paperworkid == null ? other.getpaperworkid() == null : paperworkid.equals(other.getpaperworkid()))
			&& (telephone == null ? other.gettelephone() == null : telephone.equals(other.gettelephone()))
			&& (sales_mob == null ? other.getsales_mob() == null : sales_mob.equals(other.getsales_mob()))
			&& (sales_mail == null ? other.getsales_mail() == null : sales_mail.equals(other.getsales_mail()))
			&& (status_cod == null ? other.getstatus_cod() == null : status_cod.equals(other.getstatus_cod()))
			&& (status == null ? other.getstatus() == null : status.equals(other.getstatus()))
			&& (function == null ? other.getfunction() == null : function.equals(other.getfunction()))
			&& (entrant_date == null ? other.getentrant_date() == null : entrant_date.equals(other.getentrant_date()))
			&& (dimission_date == null ? other.getdimission_date() == null : dimission_date.equals(other.getdimission_date()))
			&& (data_upd_typ == null ? other.getdata_upd_typ() == null : data_upd_typ.equals(other.getdata_upd_typ()))
			&& (prt_entrust_orgid == null ? other.getprt_entrust_orgid() == null : prt_entrust_orgid.equals(other.getprt_entrust_orgid()))
			&& (prt_entrust_orgname == null ? other.getprt_entrust_orgname() == null : prt_entrust_orgname.equals(other.getprt_entrust_orgname()))
			&& (li_entrust_orgid == null ? other.getli_entrust_orgid() == null : li_entrust_orgid.equals(other.getli_entrust_orgid()))
			&& (li_entrust_orgname == null ? other.getli_entrust_orgname() == null : li_entrust_orgname.equals(other.getli_entrust_orgname()))
			&& (hi_entrust_orgid == null ? other.gethi_entrust_orgid() == null : hi_entrust_orgid.equals(other.gethi_entrust_orgid()))
			&& (hi_entrust_orgname == null ? other.gethi_entrust_orgname() == null : hi_entrust_orgname.equals(other.gethi_entrust_orgname()))
			&& (prt_check_date == null ? other.getprt_check_date() == null : prt_check_date.equals(other.getprt_check_date()))
			&& (li_check_date == null ? other.getli_check_date() == null : li_check_date.equals(other.getli_check_date()))
			&& (hi_check_date == null ? other.gethi_check_date() == null : hi_check_date.equals(other.gethi_check_date()))
			&& (makedate == null ? other.getmakedate() == null : fDate.getString(makedate).equals(other.getmakedate()))
			&& (maketime == null ? other.getmaketime() == null : maketime.equals(other.getmaketime()))
			&& (modifydate == null ? other.getmodifydate() == null : fDate.getString(modifydate).equals(other.getmodifydate()))
			&& (modifytime == null ? other.getmodifytime() == null : modifytime.equals(other.getmodifytime()))
			&& (usercode == null ? other.getusercode() == null : usercode.equals(other.getusercode()))
			&& (password == null ? other.getpassword() == null : password.equals(other.getpassword()))
			&& (Serialno == null ? other.getSerialno() == null : Serialno.equals(other.getSerialno()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("operatorcode") ) {
			return 0;
		}
		if( strFieldName.equals("comp_cod") ) {
			return 1;
		}
		if( strFieldName.equals("comp_nam") ) {
			return 2;
		}
		if( strFieldName.equals("man_org_cod") ) {
			return 3;
		}
		if( strFieldName.equals("man_org_nam") ) {
			return 4;
		}
		if( strFieldName.equals("sales_nam") ) {
			return 5;
		}
		if( strFieldName.equals("date_birthd") ) {
			return 6;
		}
		if( strFieldName.equals("sex_cod") ) {
			return 7;
		}
		if( strFieldName.equals("idtyp_cod") ) {
			return 8;
		}
		if( strFieldName.equals("paperworkid") ) {
			return 9;
		}
		if( strFieldName.equals("telephone") ) {
			return 10;
		}
		if( strFieldName.equals("sales_mob") ) {
			return 11;
		}
		if( strFieldName.equals("sales_mail") ) {
			return 12;
		}
		if( strFieldName.equals("status_cod") ) {
			return 13;
		}
		if( strFieldName.equals("status") ) {
			return 14;
		}
		if( strFieldName.equals("function") ) {
			return 15;
		}
		if( strFieldName.equals("entrant_date") ) {
			return 16;
		}
		if( strFieldName.equals("dimission_date") ) {
			return 17;
		}
		if( strFieldName.equals("data_upd_typ") ) {
			return 18;
		}
		if( strFieldName.equals("prt_entrust_orgid") ) {
			return 19;
		}
		if( strFieldName.equals("prt_entrust_orgname") ) {
			return 20;
		}
		if( strFieldName.equals("li_entrust_orgid") ) {
			return 21;
		}
		if( strFieldName.equals("li_entrust_orgname") ) {
			return 22;
		}
		if( strFieldName.equals("hi_entrust_orgid") ) {
			return 23;
		}
		if( strFieldName.equals("hi_entrust_orgname") ) {
			return 24;
		}
		if( strFieldName.equals("prt_check_date") ) {
			return 25;
		}
		if( strFieldName.equals("li_check_date") ) {
			return 26;
		}
		if( strFieldName.equals("hi_check_date") ) {
			return 27;
		}
		if( strFieldName.equals("makedate") ) {
			return 28;
		}
		if( strFieldName.equals("maketime") ) {
			return 29;
		}
		if( strFieldName.equals("modifydate") ) {
			return 30;
		}
		if( strFieldName.equals("modifytime") ) {
			return 31;
		}
		if( strFieldName.equals("usercode") ) {
			return 32;
		}
		if( strFieldName.equals("password") ) {
			return 33;
		}
		if( strFieldName.equals("Serialno") ) {
			return 34;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "operatorcode";
				break;
			case 1:
				strFieldName = "comp_cod";
				break;
			case 2:
				strFieldName = "comp_nam";
				break;
			case 3:
				strFieldName = "man_org_cod";
				break;
			case 4:
				strFieldName = "man_org_nam";
				break;
			case 5:
				strFieldName = "sales_nam";
				break;
			case 6:
				strFieldName = "date_birthd";
				break;
			case 7:
				strFieldName = "sex_cod";
				break;
			case 8:
				strFieldName = "idtyp_cod";
				break;
			case 9:
				strFieldName = "paperworkid";
				break;
			case 10:
				strFieldName = "telephone";
				break;
			case 11:
				strFieldName = "sales_mob";
				break;
			case 12:
				strFieldName = "sales_mail";
				break;
			case 13:
				strFieldName = "status_cod";
				break;
			case 14:
				strFieldName = "status";
				break;
			case 15:
				strFieldName = "function";
				break;
			case 16:
				strFieldName = "entrant_date";
				break;
			case 17:
				strFieldName = "dimission_date";
				break;
			case 18:
				strFieldName = "data_upd_typ";
				break;
			case 19:
				strFieldName = "prt_entrust_orgid";
				break;
			case 20:
				strFieldName = "prt_entrust_orgname";
				break;
			case 21:
				strFieldName = "li_entrust_orgid";
				break;
			case 22:
				strFieldName = "li_entrust_orgname";
				break;
			case 23:
				strFieldName = "hi_entrust_orgid";
				break;
			case 24:
				strFieldName = "hi_entrust_orgname";
				break;
			case 25:
				strFieldName = "prt_check_date";
				break;
			case 26:
				strFieldName = "li_check_date";
				break;
			case 27:
				strFieldName = "hi_check_date";
				break;
			case 28:
				strFieldName = "makedate";
				break;
			case 29:
				strFieldName = "maketime";
				break;
			case 30:
				strFieldName = "modifydate";
				break;
			case 31:
				strFieldName = "modifytime";
				break;
			case 32:
				strFieldName = "usercode";
				break;
			case 33:
				strFieldName = "password";
				break;
			case 34:
				strFieldName = "Serialno";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("operatorcode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("comp_cod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("comp_nam") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("man_org_cod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("man_org_nam") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("sales_nam") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("date_birthd") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("sex_cod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("idtyp_cod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("paperworkid") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("telephone") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("sales_mob") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("sales_mail") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("status_cod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("status") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("function") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("entrant_date") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("dimission_date") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("data_upd_typ") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("prt_entrust_orgid") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("prt_entrust_orgname") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("li_entrust_orgid") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("li_entrust_orgname") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("hi_entrust_orgid") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("hi_entrust_orgname") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("prt_check_date") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("li_check_date") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("hi_check_date") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("makedate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("maketime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("modifydate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("modifytime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("usercode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("password") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Serialno") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
