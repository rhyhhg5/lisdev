/*
 * <p>ClassName: LDControlCodeSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LDControlCodeDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LDControlCodeSchema implements Schema
{
    // @Field
    /** 控制编码 */
    private String Code;
    /** 控制分类1 */
    private String CodeType1;
    /** 控制分类2 */
    private String CodeType2;
    /** 控制说明 */
    private String CodeDescription;
    /** 其它标志 */
    private String OtherSign;

    public static final int FIELDNUM = 5; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LDControlCodeSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "Code";
        pk[1] = "CodeType1";
        pk[2] = "CodeType2";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getCode()
    {
        if (Code != null && !Code.equals("") && SysConst.CHANGECHARSET == true)
        {
            Code = StrTool.unicodeToGBK(Code);
        }
        return Code;
    }

    public void setCode(String aCode)
    {
        Code = aCode;
    }

    public String getCodeType1()
    {
        if (CodeType1 != null && !CodeType1.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CodeType1 = StrTool.unicodeToGBK(CodeType1);
        }
        return CodeType1;
    }

    public void setCodeType1(String aCodeType1)
    {
        CodeType1 = aCodeType1;
    }

    public String getCodeType2()
    {
        if (CodeType2 != null && !CodeType2.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CodeType2 = StrTool.unicodeToGBK(CodeType2);
        }
        return CodeType2;
    }

    public void setCodeType2(String aCodeType2)
    {
        CodeType2 = aCodeType2;
    }

    public String getCodeDescription()
    {
        if (CodeDescription != null && !CodeDescription.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CodeDescription = StrTool.unicodeToGBK(CodeDescription);
        }
        return CodeDescription;
    }

    public void setCodeDescription(String aCodeDescription)
    {
        CodeDescription = aCodeDescription;
    }

    public String getOtherSign()
    {
        if (OtherSign != null && !OtherSign.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            OtherSign = StrTool.unicodeToGBK(OtherSign);
        }
        return OtherSign;
    }

    public void setOtherSign(String aOtherSign)
    {
        OtherSign = aOtherSign;
    }

    /**
     * 使用另外一个 LDControlCodeSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LDControlCodeSchema aLDControlCodeSchema)
    {
        this.Code = aLDControlCodeSchema.getCode();
        this.CodeType1 = aLDControlCodeSchema.getCodeType1();
        this.CodeType2 = aLDControlCodeSchema.getCodeType2();
        this.CodeDescription = aLDControlCodeSchema.getCodeDescription();
        this.OtherSign = aLDControlCodeSchema.getOtherSign();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("Code") == null)
            {
                this.Code = null;
            }
            else
            {
                this.Code = rs.getString("Code").trim();
            }

            if (rs.getString("CodeType1") == null)
            {
                this.CodeType1 = null;
            }
            else
            {
                this.CodeType1 = rs.getString("CodeType1").trim();
            }

            if (rs.getString("CodeType2") == null)
            {
                this.CodeType2 = null;
            }
            else
            {
                this.CodeType2 = rs.getString("CodeType2").trim();
            }

            if (rs.getString("CodeDescription") == null)
            {
                this.CodeDescription = null;
            }
            else
            {
                this.CodeDescription = rs.getString("CodeDescription").trim();
            }

            if (rs.getString("OtherSign") == null)
            {
                this.OtherSign = null;
            }
            else
            {
                this.OtherSign = rs.getString("OtherSign").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDControlCodeSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LDControlCodeSchema getSchema()
    {
        LDControlCodeSchema aLDControlCodeSchema = new LDControlCodeSchema();
        aLDControlCodeSchema.setSchema(this);
        return aLDControlCodeSchema;
    }

    public LDControlCodeDB getDB()
    {
        LDControlCodeDB aDBOper = new LDControlCodeDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDControlCode描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(Code)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CodeType1)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CodeType2)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CodeDescription)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(OtherSign));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDControlCode>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            Code = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                  SysConst.PACKAGESPILTER);
            CodeType1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                       SysConst.PACKAGESPILTER);
            CodeType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                       SysConst.PACKAGESPILTER);
            CodeDescription = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             4, SysConst.PACKAGESPILTER);
            OtherSign = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                       SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDControlCodeSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("Code"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Code));
        }
        if (FCode.equals("CodeType1"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CodeType1));
        }
        if (FCode.equals("CodeType2"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CodeType2));
        }
        if (FCode.equals("CodeDescription"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CodeDescription));
        }
        if (FCode.equals("OtherSign"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(OtherSign));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(Code);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(CodeType1);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(CodeType2);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(CodeDescription);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(OtherSign);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("Code"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Code = FValue.trim();
            }
            else
            {
                Code = null;
            }
        }
        if (FCode.equals("CodeType1"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CodeType1 = FValue.trim();
            }
            else
            {
                CodeType1 = null;
            }
        }
        if (FCode.equals("CodeType2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CodeType2 = FValue.trim();
            }
            else
            {
                CodeType2 = null;
            }
        }
        if (FCode.equals("CodeDescription"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CodeDescription = FValue.trim();
            }
            else
            {
                CodeDescription = null;
            }
        }
        if (FCode.equals("OtherSign"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OtherSign = FValue.trim();
            }
            else
            {
                OtherSign = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LDControlCodeSchema other = (LDControlCodeSchema) otherObject;
        return
                Code.equals(other.getCode())
                && CodeType1.equals(other.getCodeType1())
                && CodeType2.equals(other.getCodeType2())
                && CodeDescription.equals(other.getCodeDescription())
                && OtherSign.equals(other.getOtherSign());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("Code"))
        {
            return 0;
        }
        if (strFieldName.equals("CodeType1"))
        {
            return 1;
        }
        if (strFieldName.equals("CodeType2"))
        {
            return 2;
        }
        if (strFieldName.equals("CodeDescription"))
        {
            return 3;
        }
        if (strFieldName.equals("OtherSign"))
        {
            return 4;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "Code";
                break;
            case 1:
                strFieldName = "CodeType1";
                break;
            case 2:
                strFieldName = "CodeType2";
                break;
            case 3:
                strFieldName = "CodeDescription";
                break;
            case 4:
                strFieldName = "OtherSign";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("Code"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CodeType1"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CodeType2"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CodeDescription"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OtherSign"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
