/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LAGetOwnChargeDB;

/*
 * <p>ClassName: LAGetOwnChargeSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 互动2015佣金报送存储表
 * @CreateDate：2015-12-10
 */
public class LAGetOwnChargeSchema implements Schema, Cloneable
{
	// @Field
	/** 保单号 */
	private String ContNo;
	/** 子公司代码 */
	private String CompCode;
	/** 子公司名称 */
	private String CompName;
	/** 活动业务活动日期 */
	private String ActDate;
	/** 签单日期 */
	private String SignDate;
	/** 险种号码 */
	private String RiskCode;
	/** 业务单证号码 */
	private String ActNo;
	/** 佣金生成日期 */
	private String TmakeDate;
	/** 佣金类型代码 */
	private String FeePayTypeCode;
	/** 佣金类型名称 */
	private String FeePayTypeName;
	/** 委托方机构代码 */
	private String OrgBelgCode;
	/** 委托方机构名称 */
	private String OrgBelName;
	/** 委托方支付佣金 */
	private double Charge;
	/** 委托方支付佣金比例 */
	private double ChargeRate;
	/** 委托方支付时间 */
	private String OrgBelgPayDate;
	/** 委托方支付单号(内部单号) */
	private String OrgBelgPayNo;
	/** 委托方支付凭证号(银行凭证号) */
	private String OrgBelgPayVou;
	/** 报送时间 */
	private String DateSend;
	/** 支付批次号 */
	private double PayNum;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 操作员代码 */
	private String Operator;
	/** 备用1 */
	private String F1;
	/** 备用2 */
	private String F2;
	/** 备用3 */
	private String F3;
	/** 备用4 */
	private String F4;
	/** 备用5 */
	private String F5;
	/** 备用6 */
	private Date F6;
	/** 备用7 */
	private Date F7;
	/** 备用8 */
	private Date F8;
	/** 备用9 */
	private String F9;
	/** 备用0 */
	private String F0;
	/** C1 */
	private double C1;
	/** C2 */
	private double C2;
	/** C3 */
	private double C3;
	/** R1 */
	private double R1;
	/** R2 */
	private double R2;
	/** R3 */
	private double R3;

	public static final int FIELDNUM = 40;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LAGetOwnChargeSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[7];
		pk[0] = "ContNo";
		pk[1] = "CompCode";
		pk[2] = "ActDate";
		pk[3] = "SignDate";
		pk[4] = "RiskCode";
		pk[5] = "ActNo";
		pk[6] = "FeePayTypeCode";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LAGetOwnChargeSchema cloned = (LAGetOwnChargeSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getContNo()
	{
		return ContNo;
	}
	public void setContNo(String aContNo)
	{
		ContNo = aContNo;
	}
	public String getCompCode()
	{
		return CompCode;
	}
	public void setCompCode(String aCompCode)
	{
		CompCode = aCompCode;
	}
	public String getCompName()
	{
		return CompName;
	}
	public void setCompName(String aCompName)
	{
		CompName = aCompName;
	}
	public String getActDate()
	{
		return ActDate;
	}
	public void setActDate(String aActDate)
	{
		ActDate = aActDate;
	}
	public String getSignDate()
	{
		return SignDate;
	}
	public void setSignDate(String aSignDate)
	{
		SignDate = aSignDate;
	}
	public String getRiskCode()
	{
		return RiskCode;
	}
	public void setRiskCode(String aRiskCode)
	{
		RiskCode = aRiskCode;
	}
	public String getActNo()
	{
		return ActNo;
	}
	public void setActNo(String aActNo)
	{
		ActNo = aActNo;
	}
	public String getTmakeDate()
	{
		return TmakeDate;
	}
	public void setTmakeDate(String aTmakeDate)
	{
		TmakeDate = aTmakeDate;
	}
	public String getFeePayTypeCode()
	{
		return FeePayTypeCode;
	}
	public void setFeePayTypeCode(String aFeePayTypeCode)
	{
		FeePayTypeCode = aFeePayTypeCode;
	}
	public String getFeePayTypeName()
	{
		return FeePayTypeName;
	}
	public void setFeePayTypeName(String aFeePayTypeName)
	{
		FeePayTypeName = aFeePayTypeName;
	}
	public String getOrgBelgCode()
	{
		return OrgBelgCode;
	}
	public void setOrgBelgCode(String aOrgBelgCode)
	{
		OrgBelgCode = aOrgBelgCode;
	}
	public String getOrgBelName()
	{
		return OrgBelName;
	}
	public void setOrgBelName(String aOrgBelName)
	{
		OrgBelName = aOrgBelName;
	}
	public double getCharge()
	{
		return Charge;
	}
	public void setCharge(double aCharge)
	{
		Charge = Arith.round(aCharge,2);
	}
	public void setCharge(String aCharge)
	{
		if (aCharge != null && !aCharge.equals(""))
		{
			Double tDouble = new Double(aCharge);
			double d = tDouble.doubleValue();
                Charge = Arith.round(d,2);
		}
	}

	public double getChargeRate()
	{
		return ChargeRate;
	}
	public void setChargeRate(double aChargeRate)
	{
		ChargeRate = Arith.round(aChargeRate,4);
	}
	public void setChargeRate(String aChargeRate)
	{
		if (aChargeRate != null && !aChargeRate.equals(""))
		{
			Double tDouble = new Double(aChargeRate);
			double d = tDouble.doubleValue();
                ChargeRate = Arith.round(d,4);
		}
	}

	public String getOrgBelgPayDate()
	{
		return OrgBelgPayDate;
	}
	public void setOrgBelgPayDate(String aOrgBelgPayDate)
	{
		OrgBelgPayDate = aOrgBelgPayDate;
	}
	public String getOrgBelgPayNo()
	{
		return OrgBelgPayNo;
	}
	public void setOrgBelgPayNo(String aOrgBelgPayNo)
	{
		OrgBelgPayNo = aOrgBelgPayNo;
	}
	public String getOrgBelgPayVou()
	{
		return OrgBelgPayVou;
	}
	public void setOrgBelgPayVou(String aOrgBelgPayVou)
	{
		OrgBelgPayVou = aOrgBelgPayVou;
	}
	public String getDateSend()
	{
		return DateSend;
	}
	public void setDateSend(String aDateSend)
	{
		DateSend = aDateSend;
	}
	public double getPayNum()
	{
		return PayNum;
	}
	public void setPayNum(double aPayNum)
	{
		PayNum = Arith.round(aPayNum,0);
	}
	public void setPayNum(String aPayNum)
	{
		if (aPayNum != null && !aPayNum.equals(""))
		{
			Double tDouble = new Double(aPayNum);
			double d = tDouble.doubleValue();
                PayNum = Arith.round(d,0);
		}
	}

	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getF1()
	{
		return F1;
	}
	public void setF1(String aF1)
	{
		F1 = aF1;
	}
	public String getF2()
	{
		return F2;
	}
	public void setF2(String aF2)
	{
		F2 = aF2;
	}
	public String getF3()
	{
		return F3;
	}
	public void setF3(String aF3)
	{
		F3 = aF3;
	}
	public String getF4()
	{
		return F4;
	}
	public void setF4(String aF4)
	{
		F4 = aF4;
	}
	public String getF5()
	{
		return F5;
	}
	public void setF5(String aF5)
	{
		F5 = aF5;
	}
	public String getF6()
	{
		if( F6 != null )
			return fDate.getString(F6);
		else
			return null;
	}
	public void setF6(Date aF6)
	{
		F6 = aF6;
	}
	public void setF6(String aF6)
	{
		if (aF6 != null && !aF6.equals("") )
		{
			F6 = fDate.getDate( aF6 );
		}
		else
			F6 = null;
	}

	public String getF7()
	{
		if( F7 != null )
			return fDate.getString(F7);
		else
			return null;
	}
	public void setF7(Date aF7)
	{
		F7 = aF7;
	}
	public void setF7(String aF7)
	{
		if (aF7 != null && !aF7.equals("") )
		{
			F7 = fDate.getDate( aF7 );
		}
		else
			F7 = null;
	}

	public String getF8()
	{
		if( F8 != null )
			return fDate.getString(F8);
		else
			return null;
	}
	public void setF8(Date aF8)
	{
		F8 = aF8;
	}
	public void setF8(String aF8)
	{
		if (aF8 != null && !aF8.equals("") )
		{
			F8 = fDate.getDate( aF8 );
		}
		else
			F8 = null;
	}

	public String getF9()
	{
		return F9;
	}
	public void setF9(String aF9)
	{
		F9 = aF9;
	}
	public String getF0()
	{
		return F0;
	}
	public void setF0(String aF0)
	{
		F0 = aF0;
	}
	public double getC1()
	{
		return C1;
	}
	public void setC1(double aC1)
	{
		C1 = Arith.round(aC1,2);
	}
	public void setC1(String aC1)
	{
		if (aC1 != null && !aC1.equals(""))
		{
			Double tDouble = new Double(aC1);
			double d = tDouble.doubleValue();
                C1 = Arith.round(d,2);
		}
	}

	public double getC2()
	{
		return C2;
	}
	public void setC2(double aC2)
	{
		C2 = Arith.round(aC2,2);
	}
	public void setC2(String aC2)
	{
		if (aC2 != null && !aC2.equals(""))
		{
			Double tDouble = new Double(aC2);
			double d = tDouble.doubleValue();
                C2 = Arith.round(d,2);
		}
	}

	public double getC3()
	{
		return C3;
	}
	public void setC3(double aC3)
	{
		C3 = Arith.round(aC3,2);
	}
	public void setC3(String aC3)
	{
		if (aC3 != null && !aC3.equals(""))
		{
			Double tDouble = new Double(aC3);
			double d = tDouble.doubleValue();
                C3 = Arith.round(d,2);
		}
	}

	public double getR1()
	{
		return R1;
	}
	public void setR1(double aR1)
	{
		R1 = Arith.round(aR1,4);
	}
	public void setR1(String aR1)
	{
		if (aR1 != null && !aR1.equals(""))
		{
			Double tDouble = new Double(aR1);
			double d = tDouble.doubleValue();
                R1 = Arith.round(d,4);
		}
	}

	public double getR2()
	{
		return R2;
	}
	public void setR2(double aR2)
	{
		R2 = Arith.round(aR2,4);
	}
	public void setR2(String aR2)
	{
		if (aR2 != null && !aR2.equals(""))
		{
			Double tDouble = new Double(aR2);
			double d = tDouble.doubleValue();
                R2 = Arith.round(d,4);
		}
	}

	public double getR3()
	{
		return R3;
	}
	public void setR3(double aR3)
	{
		R3 = Arith.round(aR3,4);
	}
	public void setR3(String aR3)
	{
		if (aR3 != null && !aR3.equals(""))
		{
			Double tDouble = new Double(aR3);
			double d = tDouble.doubleValue();
                R3 = Arith.round(d,4);
		}
	}


	/**
	* 使用另外一个 LAGetOwnChargeSchema 对象给 Schema 赋值
	* @param: aLAGetOwnChargeSchema LAGetOwnChargeSchema
	**/
	public void setSchema(LAGetOwnChargeSchema aLAGetOwnChargeSchema)
	{
		this.ContNo = aLAGetOwnChargeSchema.getContNo();
		this.CompCode = aLAGetOwnChargeSchema.getCompCode();
		this.CompName = aLAGetOwnChargeSchema.getCompName();
		this.ActDate = aLAGetOwnChargeSchema.getActDate();
		this.SignDate = aLAGetOwnChargeSchema.getSignDate();
		this.RiskCode = aLAGetOwnChargeSchema.getRiskCode();
		this.ActNo = aLAGetOwnChargeSchema.getActNo();
		this.TmakeDate = aLAGetOwnChargeSchema.getTmakeDate();
		this.FeePayTypeCode = aLAGetOwnChargeSchema.getFeePayTypeCode();
		this.FeePayTypeName = aLAGetOwnChargeSchema.getFeePayTypeName();
		this.OrgBelgCode = aLAGetOwnChargeSchema.getOrgBelgCode();
		this.OrgBelName = aLAGetOwnChargeSchema.getOrgBelName();
		this.Charge = aLAGetOwnChargeSchema.getCharge();
		this.ChargeRate = aLAGetOwnChargeSchema.getChargeRate();
		this.OrgBelgPayDate = aLAGetOwnChargeSchema.getOrgBelgPayDate();
		this.OrgBelgPayNo = aLAGetOwnChargeSchema.getOrgBelgPayNo();
		this.OrgBelgPayVou = aLAGetOwnChargeSchema.getOrgBelgPayVou();
		this.DateSend = aLAGetOwnChargeSchema.getDateSend();
		this.PayNum = aLAGetOwnChargeSchema.getPayNum();
		this.MakeDate = fDate.getDate( aLAGetOwnChargeSchema.getMakeDate());
		this.MakeTime = aLAGetOwnChargeSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLAGetOwnChargeSchema.getModifyDate());
		this.ModifyTime = aLAGetOwnChargeSchema.getModifyTime();
		this.Operator = aLAGetOwnChargeSchema.getOperator();
		this.F1 = aLAGetOwnChargeSchema.getF1();
		this.F2 = aLAGetOwnChargeSchema.getF2();
		this.F3 = aLAGetOwnChargeSchema.getF3();
		this.F4 = aLAGetOwnChargeSchema.getF4();
		this.F5 = aLAGetOwnChargeSchema.getF5();
		this.F6 = fDate.getDate( aLAGetOwnChargeSchema.getF6());
		this.F7 = fDate.getDate( aLAGetOwnChargeSchema.getF7());
		this.F8 = fDate.getDate( aLAGetOwnChargeSchema.getF8());
		this.F9 = aLAGetOwnChargeSchema.getF9();
		this.F0 = aLAGetOwnChargeSchema.getF0();
		this.C1 = aLAGetOwnChargeSchema.getC1();
		this.C2 = aLAGetOwnChargeSchema.getC2();
		this.C3 = aLAGetOwnChargeSchema.getC3();
		this.R1 = aLAGetOwnChargeSchema.getR1();
		this.R2 = aLAGetOwnChargeSchema.getR2();
		this.R3 = aLAGetOwnChargeSchema.getR3();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("ContNo") == null )
				this.ContNo = null;
			else
				this.ContNo = rs.getString("ContNo").trim();

			if( rs.getString("CompCode") == null )
				this.CompCode = null;
			else
				this.CompCode = rs.getString("CompCode").trim();

			if( rs.getString("CompName") == null )
				this.CompName = null;
			else
				this.CompName = rs.getString("CompName").trim();

			if( rs.getString("ActDate") == null )
				this.ActDate = null;
			else
				this.ActDate = rs.getString("ActDate").trim();

			if( rs.getString("SignDate") == null )
				this.SignDate = null;
			else
				this.SignDate = rs.getString("SignDate").trim();

			if( rs.getString("RiskCode") == null )
				this.RiskCode = null;
			else
				this.RiskCode = rs.getString("RiskCode").trim();

			if( rs.getString("ActNo") == null )
				this.ActNo = null;
			else
				this.ActNo = rs.getString("ActNo").trim();

			if( rs.getString("TmakeDate") == null )
				this.TmakeDate = null;
			else
				this.TmakeDate = rs.getString("TmakeDate").trim();

			if( rs.getString("FeePayTypeCode") == null )
				this.FeePayTypeCode = null;
			else
				this.FeePayTypeCode = rs.getString("FeePayTypeCode").trim();

			if( rs.getString("FeePayTypeName") == null )
				this.FeePayTypeName = null;
			else
				this.FeePayTypeName = rs.getString("FeePayTypeName").trim();

			if( rs.getString("OrgBelgCode") == null )
				this.OrgBelgCode = null;
			else
				this.OrgBelgCode = rs.getString("OrgBelgCode").trim();

			if( rs.getString("OrgBelName") == null )
				this.OrgBelName = null;
			else
				this.OrgBelName = rs.getString("OrgBelName").trim();

			this.Charge = rs.getDouble("Charge");
			this.ChargeRate = rs.getDouble("ChargeRate");
			if( rs.getString("OrgBelgPayDate") == null )
				this.OrgBelgPayDate = null;
			else
				this.OrgBelgPayDate = rs.getString("OrgBelgPayDate").trim();

			if( rs.getString("OrgBelgPayNo") == null )
				this.OrgBelgPayNo = null;
			else
				this.OrgBelgPayNo = rs.getString("OrgBelgPayNo").trim();

			if( rs.getString("OrgBelgPayVou") == null )
				this.OrgBelgPayVou = null;
			else
				this.OrgBelgPayVou = rs.getString("OrgBelgPayVou").trim();

			if( rs.getString("DateSend") == null )
				this.DateSend = null;
			else
				this.DateSend = rs.getString("DateSend").trim();

			this.PayNum = rs.getDouble("PayNum");
			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			if( rs.getString("F1") == null )
				this.F1 = null;
			else
				this.F1 = rs.getString("F1").trim();

			if( rs.getString("F2") == null )
				this.F2 = null;
			else
				this.F2 = rs.getString("F2").trim();

			if( rs.getString("F3") == null )
				this.F3 = null;
			else
				this.F3 = rs.getString("F3").trim();

			if( rs.getString("F4") == null )
				this.F4 = null;
			else
				this.F4 = rs.getString("F4").trim();

			if( rs.getString("F5") == null )
				this.F5 = null;
			else
				this.F5 = rs.getString("F5").trim();

			this.F6 = rs.getDate("F6");
			this.F7 = rs.getDate("F7");
			this.F8 = rs.getDate("F8");
			if( rs.getString("F9") == null )
				this.F9 = null;
			else
				this.F9 = rs.getString("F9").trim();

			if( rs.getString("F0") == null )
				this.F0 = null;
			else
				this.F0 = rs.getString("F0").trim();

			this.C1 = rs.getDouble("C1");
			this.C2 = rs.getDouble("C2");
			this.C3 = rs.getDouble("C3");
			this.R1 = rs.getDouble("R1");
			this.R2 = rs.getDouble("R2");
			this.R3 = rs.getDouble("R3");
		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LAGetOwnCharge表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAGetOwnChargeSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LAGetOwnChargeSchema getSchema()
	{
		LAGetOwnChargeSchema aLAGetOwnChargeSchema = new LAGetOwnChargeSchema();
		aLAGetOwnChargeSchema.setSchema(this);
		return aLAGetOwnChargeSchema;
	}

	public LAGetOwnChargeDB getDB()
	{
		LAGetOwnChargeDB aDBOper = new LAGetOwnChargeDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAGetOwnCharge描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CompCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CompName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ActDate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SignDate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ActNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TmakeDate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FeePayTypeCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FeePayTypeName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OrgBelgCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OrgBelName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Charge));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ChargeRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OrgBelgPayDate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OrgBelgPayNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OrgBelgPayVou)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DateSend)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PayNum));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(F1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(F2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(F3)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(F4)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(F5)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( F6 ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( F7 ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( F8 ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(F9)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(F0)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(C1));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(C2));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(C3));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(R1));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(R2));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(R3));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAGetOwnCharge>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			CompCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			CompName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			ActDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			SignDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			ActNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			TmakeDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			FeePayTypeCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			FeePayTypeName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			OrgBelgCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			OrgBelName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			Charge = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,13,SysConst.PACKAGESPILTER))).doubleValue();
			ChargeRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,14,SysConst.PACKAGESPILTER))).doubleValue();
			OrgBelgPayDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			OrgBelgPayNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			OrgBelgPayVou = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			DateSend = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			PayNum = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,19,SysConst.PACKAGESPILTER))).doubleValue();
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			F1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			F2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			F3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			F4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			F5 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			F6 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30,SysConst.PACKAGESPILTER));
			F7 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31,SysConst.PACKAGESPILTER));
			F8 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32,SysConst.PACKAGESPILTER));
			F9 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			F0 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			C1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,35,SysConst.PACKAGESPILTER))).doubleValue();
			C2 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,36,SysConst.PACKAGESPILTER))).doubleValue();
			C3 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,37,SysConst.PACKAGESPILTER))).doubleValue();
			R1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,38,SysConst.PACKAGESPILTER))).doubleValue();
			R2 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,39,SysConst.PACKAGESPILTER))).doubleValue();
			R3 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,40,SysConst.PACKAGESPILTER))).doubleValue();
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAGetOwnChargeSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("ContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
		}
		if (FCode.equals("CompCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CompCode));
		}
		if (FCode.equals("CompName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CompName));
		}
		if (FCode.equals("ActDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ActDate));
		}
		if (FCode.equals("SignDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SignDate));
		}
		if (FCode.equals("RiskCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
		}
		if (FCode.equals("ActNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ActNo));
		}
		if (FCode.equals("TmakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TmakeDate));
		}
		if (FCode.equals("FeePayTypeCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FeePayTypeCode));
		}
		if (FCode.equals("FeePayTypeName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FeePayTypeName));
		}
		if (FCode.equals("OrgBelgCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OrgBelgCode));
		}
		if (FCode.equals("OrgBelName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OrgBelName));
		}
		if (FCode.equals("Charge"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Charge));
		}
		if (FCode.equals("ChargeRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ChargeRate));
		}
		if (FCode.equals("OrgBelgPayDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OrgBelgPayDate));
		}
		if (FCode.equals("OrgBelgPayNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OrgBelgPayNo));
		}
		if (FCode.equals("OrgBelgPayVou"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OrgBelgPayVou));
		}
		if (FCode.equals("DateSend"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DateSend));
		}
		if (FCode.equals("PayNum"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayNum));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("F1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F1));
		}
		if (FCode.equals("F2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F2));
		}
		if (FCode.equals("F3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F3));
		}
		if (FCode.equals("F4"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F4));
		}
		if (FCode.equals("F5"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F5));
		}
		if (FCode.equals("F6"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getF6()));
		}
		if (FCode.equals("F7"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getF7()));
		}
		if (FCode.equals("F8"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getF8()));
		}
		if (FCode.equals("F9"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F9));
		}
		if (FCode.equals("F0"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F0));
		}
		if (FCode.equals("C1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(C1));
		}
		if (FCode.equals("C2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(C2));
		}
		if (FCode.equals("C3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(C3));
		}
		if (FCode.equals("R1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(R1));
		}
		if (FCode.equals("R2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(R2));
		}
		if (FCode.equals("R3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(R3));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(ContNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(CompCode);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(CompName);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(ActDate);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(SignDate);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(RiskCode);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(ActNo);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(TmakeDate);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(FeePayTypeCode);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(FeePayTypeName);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(OrgBelgCode);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(OrgBelName);
				break;
			case 12:
				strFieldValue = String.valueOf(Charge);
				break;
			case 13:
				strFieldValue = String.valueOf(ChargeRate);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(OrgBelgPayDate);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(OrgBelgPayNo);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(OrgBelgPayVou);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(DateSend);
				break;
			case 18:
				strFieldValue = String.valueOf(PayNum);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(F1);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(F2);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(F3);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(F4);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(F5);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getF6()));
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getF7()));
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getF8()));
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(F9);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(F0);
				break;
			case 34:
				strFieldValue = String.valueOf(C1);
				break;
			case 35:
				strFieldValue = String.valueOf(C2);
				break;
			case 36:
				strFieldValue = String.valueOf(C3);
				break;
			case 37:
				strFieldValue = String.valueOf(R1);
				break;
			case 38:
				strFieldValue = String.valueOf(R2);
				break;
			case 39:
				strFieldValue = String.valueOf(R3);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("ContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContNo = FValue.trim();
			}
			else
				ContNo = null;
		}
		if (FCode.equalsIgnoreCase("CompCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CompCode = FValue.trim();
			}
			else
				CompCode = null;
		}
		if (FCode.equalsIgnoreCase("CompName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CompName = FValue.trim();
			}
			else
				CompName = null;
		}
		if (FCode.equalsIgnoreCase("ActDate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ActDate = FValue.trim();
			}
			else
				ActDate = null;
		}
		if (FCode.equalsIgnoreCase("SignDate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SignDate = FValue.trim();
			}
			else
				SignDate = null;
		}
		if (FCode.equalsIgnoreCase("RiskCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskCode = FValue.trim();
			}
			else
				RiskCode = null;
		}
		if (FCode.equalsIgnoreCase("ActNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ActNo = FValue.trim();
			}
			else
				ActNo = null;
		}
		if (FCode.equalsIgnoreCase("TmakeDate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TmakeDate = FValue.trim();
			}
			else
				TmakeDate = null;
		}
		if (FCode.equalsIgnoreCase("FeePayTypeCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FeePayTypeCode = FValue.trim();
			}
			else
				FeePayTypeCode = null;
		}
		if (FCode.equalsIgnoreCase("FeePayTypeName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FeePayTypeName = FValue.trim();
			}
			else
				FeePayTypeName = null;
		}
		if (FCode.equalsIgnoreCase("OrgBelgCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OrgBelgCode = FValue.trim();
			}
			else
				OrgBelgCode = null;
		}
		if (FCode.equalsIgnoreCase("OrgBelName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OrgBelName = FValue.trim();
			}
			else
				OrgBelName = null;
		}
		if (FCode.equalsIgnoreCase("Charge"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Charge = d;
			}
		}
		if (FCode.equalsIgnoreCase("ChargeRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ChargeRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("OrgBelgPayDate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OrgBelgPayDate = FValue.trim();
			}
			else
				OrgBelgPayDate = null;
		}
		if (FCode.equalsIgnoreCase("OrgBelgPayNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OrgBelgPayNo = FValue.trim();
			}
			else
				OrgBelgPayNo = null;
		}
		if (FCode.equalsIgnoreCase("OrgBelgPayVou"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OrgBelgPayVou = FValue.trim();
			}
			else
				OrgBelgPayVou = null;
		}
		if (FCode.equalsIgnoreCase("DateSend"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DateSend = FValue.trim();
			}
			else
				DateSend = null;
		}
		if (FCode.equalsIgnoreCase("PayNum"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				PayNum = d;
			}
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("F1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				F1 = FValue.trim();
			}
			else
				F1 = null;
		}
		if (FCode.equalsIgnoreCase("F2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				F2 = FValue.trim();
			}
			else
				F2 = null;
		}
		if (FCode.equalsIgnoreCase("F3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				F3 = FValue.trim();
			}
			else
				F3 = null;
		}
		if (FCode.equalsIgnoreCase("F4"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				F4 = FValue.trim();
			}
			else
				F4 = null;
		}
		if (FCode.equalsIgnoreCase("F5"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				F5 = FValue.trim();
			}
			else
				F5 = null;
		}
		if (FCode.equalsIgnoreCase("F6"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				F6 = fDate.getDate( FValue );
			}
			else
				F6 = null;
		}
		if (FCode.equalsIgnoreCase("F7"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				F7 = fDate.getDate( FValue );
			}
			else
				F7 = null;
		}
		if (FCode.equalsIgnoreCase("F8"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				F8 = fDate.getDate( FValue );
			}
			else
				F8 = null;
		}
		if (FCode.equalsIgnoreCase("F9"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				F9 = FValue.trim();
			}
			else
				F9 = null;
		}
		if (FCode.equalsIgnoreCase("F0"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				F0 = FValue.trim();
			}
			else
				F0 = null;
		}
		if (FCode.equalsIgnoreCase("C1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				C1 = d;
			}
		}
		if (FCode.equalsIgnoreCase("C2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				C2 = d;
			}
		}
		if (FCode.equalsIgnoreCase("C3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				C3 = d;
			}
		}
		if (FCode.equalsIgnoreCase("R1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				R1 = d;
			}
		}
		if (FCode.equalsIgnoreCase("R2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				R2 = d;
			}
		}
		if (FCode.equalsIgnoreCase("R3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				R3 = d;
			}
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LAGetOwnChargeSchema other = (LAGetOwnChargeSchema)otherObject;
		return
			(ContNo == null ? other.getContNo() == null : ContNo.equals(other.getContNo()))
			&& (CompCode == null ? other.getCompCode() == null : CompCode.equals(other.getCompCode()))
			&& (CompName == null ? other.getCompName() == null : CompName.equals(other.getCompName()))
			&& (ActDate == null ? other.getActDate() == null : ActDate.equals(other.getActDate()))
			&& (SignDate == null ? other.getSignDate() == null : SignDate.equals(other.getSignDate()))
			&& (RiskCode == null ? other.getRiskCode() == null : RiskCode.equals(other.getRiskCode()))
			&& (ActNo == null ? other.getActNo() == null : ActNo.equals(other.getActNo()))
			&& (TmakeDate == null ? other.getTmakeDate() == null : TmakeDate.equals(other.getTmakeDate()))
			&& (FeePayTypeCode == null ? other.getFeePayTypeCode() == null : FeePayTypeCode.equals(other.getFeePayTypeCode()))
			&& (FeePayTypeName == null ? other.getFeePayTypeName() == null : FeePayTypeName.equals(other.getFeePayTypeName()))
			&& (OrgBelgCode == null ? other.getOrgBelgCode() == null : OrgBelgCode.equals(other.getOrgBelgCode()))
			&& (OrgBelName == null ? other.getOrgBelName() == null : OrgBelName.equals(other.getOrgBelName()))
			&& Charge == other.getCharge()
			&& ChargeRate == other.getChargeRate()
			&& (OrgBelgPayDate == null ? other.getOrgBelgPayDate() == null : OrgBelgPayDate.equals(other.getOrgBelgPayDate()))
			&& (OrgBelgPayNo == null ? other.getOrgBelgPayNo() == null : OrgBelgPayNo.equals(other.getOrgBelgPayNo()))
			&& (OrgBelgPayVou == null ? other.getOrgBelgPayVou() == null : OrgBelgPayVou.equals(other.getOrgBelgPayVou()))
			&& (DateSend == null ? other.getDateSend() == null : DateSend.equals(other.getDateSend()))
			&& PayNum == other.getPayNum()
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (F1 == null ? other.getF1() == null : F1.equals(other.getF1()))
			&& (F2 == null ? other.getF2() == null : F2.equals(other.getF2()))
			&& (F3 == null ? other.getF3() == null : F3.equals(other.getF3()))
			&& (F4 == null ? other.getF4() == null : F4.equals(other.getF4()))
			&& (F5 == null ? other.getF5() == null : F5.equals(other.getF5()))
			&& (F6 == null ? other.getF6() == null : fDate.getString(F6).equals(other.getF6()))
			&& (F7 == null ? other.getF7() == null : fDate.getString(F7).equals(other.getF7()))
			&& (F8 == null ? other.getF8() == null : fDate.getString(F8).equals(other.getF8()))
			&& (F9 == null ? other.getF9() == null : F9.equals(other.getF9()))
			&& (F0 == null ? other.getF0() == null : F0.equals(other.getF0()))
			&& C1 == other.getC1()
			&& C2 == other.getC2()
			&& C3 == other.getC3()
			&& R1 == other.getR1()
			&& R2 == other.getR2()
			&& R3 == other.getR3();
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("ContNo") ) {
			return 0;
		}
		if( strFieldName.equals("CompCode") ) {
			return 1;
		}
		if( strFieldName.equals("CompName") ) {
			return 2;
		}
		if( strFieldName.equals("ActDate") ) {
			return 3;
		}
		if( strFieldName.equals("SignDate") ) {
			return 4;
		}
		if( strFieldName.equals("RiskCode") ) {
			return 5;
		}
		if( strFieldName.equals("ActNo") ) {
			return 6;
		}
		if( strFieldName.equals("TmakeDate") ) {
			return 7;
		}
		if( strFieldName.equals("FeePayTypeCode") ) {
			return 8;
		}
		if( strFieldName.equals("FeePayTypeName") ) {
			return 9;
		}
		if( strFieldName.equals("OrgBelgCode") ) {
			return 10;
		}
		if( strFieldName.equals("OrgBelName") ) {
			return 11;
		}
		if( strFieldName.equals("Charge") ) {
			return 12;
		}
		if( strFieldName.equals("ChargeRate") ) {
			return 13;
		}
		if( strFieldName.equals("OrgBelgPayDate") ) {
			return 14;
		}
		if( strFieldName.equals("OrgBelgPayNo") ) {
			return 15;
		}
		if( strFieldName.equals("OrgBelgPayVou") ) {
			return 16;
		}
		if( strFieldName.equals("DateSend") ) {
			return 17;
		}
		if( strFieldName.equals("PayNum") ) {
			return 18;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 19;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 20;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 21;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 22;
		}
		if( strFieldName.equals("Operator") ) {
			return 23;
		}
		if( strFieldName.equals("F1") ) {
			return 24;
		}
		if( strFieldName.equals("F2") ) {
			return 25;
		}
		if( strFieldName.equals("F3") ) {
			return 26;
		}
		if( strFieldName.equals("F4") ) {
			return 27;
		}
		if( strFieldName.equals("F5") ) {
			return 28;
		}
		if( strFieldName.equals("F6") ) {
			return 29;
		}
		if( strFieldName.equals("F7") ) {
			return 30;
		}
		if( strFieldName.equals("F8") ) {
			return 31;
		}
		if( strFieldName.equals("F9") ) {
			return 32;
		}
		if( strFieldName.equals("F0") ) {
			return 33;
		}
		if( strFieldName.equals("C1") ) {
			return 34;
		}
		if( strFieldName.equals("C2") ) {
			return 35;
		}
		if( strFieldName.equals("C3") ) {
			return 36;
		}
		if( strFieldName.equals("R1") ) {
			return 37;
		}
		if( strFieldName.equals("R2") ) {
			return 38;
		}
		if( strFieldName.equals("R3") ) {
			return 39;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "ContNo";
				break;
			case 1:
				strFieldName = "CompCode";
				break;
			case 2:
				strFieldName = "CompName";
				break;
			case 3:
				strFieldName = "ActDate";
				break;
			case 4:
				strFieldName = "SignDate";
				break;
			case 5:
				strFieldName = "RiskCode";
				break;
			case 6:
				strFieldName = "ActNo";
				break;
			case 7:
				strFieldName = "TmakeDate";
				break;
			case 8:
				strFieldName = "FeePayTypeCode";
				break;
			case 9:
				strFieldName = "FeePayTypeName";
				break;
			case 10:
				strFieldName = "OrgBelgCode";
				break;
			case 11:
				strFieldName = "OrgBelName";
				break;
			case 12:
				strFieldName = "Charge";
				break;
			case 13:
				strFieldName = "ChargeRate";
				break;
			case 14:
				strFieldName = "OrgBelgPayDate";
				break;
			case 15:
				strFieldName = "OrgBelgPayNo";
				break;
			case 16:
				strFieldName = "OrgBelgPayVou";
				break;
			case 17:
				strFieldName = "DateSend";
				break;
			case 18:
				strFieldName = "PayNum";
				break;
			case 19:
				strFieldName = "MakeDate";
				break;
			case 20:
				strFieldName = "MakeTime";
				break;
			case 21:
				strFieldName = "ModifyDate";
				break;
			case 22:
				strFieldName = "ModifyTime";
				break;
			case 23:
				strFieldName = "Operator";
				break;
			case 24:
				strFieldName = "F1";
				break;
			case 25:
				strFieldName = "F2";
				break;
			case 26:
				strFieldName = "F3";
				break;
			case 27:
				strFieldName = "F4";
				break;
			case 28:
				strFieldName = "F5";
				break;
			case 29:
				strFieldName = "F6";
				break;
			case 30:
				strFieldName = "F7";
				break;
			case 31:
				strFieldName = "F8";
				break;
			case 32:
				strFieldName = "F9";
				break;
			case 33:
				strFieldName = "F0";
				break;
			case 34:
				strFieldName = "C1";
				break;
			case 35:
				strFieldName = "C2";
				break;
			case 36:
				strFieldName = "C3";
				break;
			case 37:
				strFieldName = "R1";
				break;
			case 38:
				strFieldName = "R2";
				break;
			case 39:
				strFieldName = "R3";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("ContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CompCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CompName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ActDate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SignDate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ActNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TmakeDate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FeePayTypeCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FeePayTypeName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OrgBelgCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OrgBelName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Charge") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ChargeRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("OrgBelgPayDate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OrgBelgPayNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OrgBelgPayVou") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DateSend") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayNum") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("F1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("F2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("F3") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("F4") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("F5") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("F6") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("F7") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("F8") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("F9") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("F0") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("C1") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("C2") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("C3") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("R1") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("R2") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("R3") ) {
			return Schema.TYPE_DOUBLE;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 13:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 19:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 30:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 31:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 35:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 36:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 37:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 38:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 39:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
