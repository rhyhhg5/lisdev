/*
 * <p>ClassName: LMRiskAccGetSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 险种定义
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LMRiskAccGetDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LMRiskAccGetSchema implements Schema
{
    // @Field
    /** 险种编码 */
    private String RiskCode;
    /** 险种版本 */
    private String RiskVer;
    /** 保险帐户号码 */
    private String InsuAccNo;
    /** 给付责任编码 */
    private String GetDutyCode;
    /** 默认比例 */
    private double DefaultRate;
    /** 是否需要录入 */
    private String NeedInput;
    /** 转出账户时的算法编码(现金) */
    private String CalCodeMoney;
    /** 处理方向 */
    private String DealDirection;
    /** 账户转出计算标志 */
    private String CalFlag;
    /** 账户产生位置 */
    private String AccCreatePos;
    /** 给付名称 */
    private String GetDutyName;

    public static final int FIELDNUM = 11; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LMRiskAccGetSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "RiskCode";
        pk[1] = "InsuAccNo";
        pk[2] = "GetDutyCode";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getRiskCode()
    {
        if (RiskCode != null && !RiskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getRiskVer()
    {
        if (RiskVer != null && !RiskVer.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskVer = StrTool.unicodeToGBK(RiskVer);
        }
        return RiskVer;
    }

    public void setRiskVer(String aRiskVer)
    {
        RiskVer = aRiskVer;
    }

    public String getInsuAccNo()
    {
        if (InsuAccNo != null && !InsuAccNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            InsuAccNo = StrTool.unicodeToGBK(InsuAccNo);
        }
        return InsuAccNo;
    }

    public void setInsuAccNo(String aInsuAccNo)
    {
        InsuAccNo = aInsuAccNo;
    }

    public String getGetDutyCode()
    {
        if (GetDutyCode != null && !GetDutyCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GetDutyCode = StrTool.unicodeToGBK(GetDutyCode);
        }
        return GetDutyCode;
    }

    public void setGetDutyCode(String aGetDutyCode)
    {
        GetDutyCode = aGetDutyCode;
    }

    public double getDefaultRate()
    {
        return DefaultRate;
    }

    public void setDefaultRate(double aDefaultRate)
    {
        DefaultRate = aDefaultRate;
    }

    public void setDefaultRate(String aDefaultRate)
    {
        if (aDefaultRate != null && !aDefaultRate.equals(""))
        {
            Double tDouble = new Double(aDefaultRate);
            double d = tDouble.doubleValue();
            DefaultRate = d;
        }
    }

    public String getNeedInput()
    {
        if (NeedInput != null && !NeedInput.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            NeedInput = StrTool.unicodeToGBK(NeedInput);
        }
        return NeedInput;
    }

    public void setNeedInput(String aNeedInput)
    {
        NeedInput = aNeedInput;
    }

    public String getCalCodeMoney()
    {
        if (CalCodeMoney != null && !CalCodeMoney.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CalCodeMoney = StrTool.unicodeToGBK(CalCodeMoney);
        }
        return CalCodeMoney;
    }

    public void setCalCodeMoney(String aCalCodeMoney)
    {
        CalCodeMoney = aCalCodeMoney;
    }

    public String getDealDirection()
    {
        if (DealDirection != null && !DealDirection.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            DealDirection = StrTool.unicodeToGBK(DealDirection);
        }
        return DealDirection;
    }

    public void setDealDirection(String aDealDirection)
    {
        DealDirection = aDealDirection;
    }

    public String getCalFlag()
    {
        if (CalFlag != null && !CalFlag.equals("") && SysConst.CHANGECHARSET == true)
        {
            CalFlag = StrTool.unicodeToGBK(CalFlag);
        }
        return CalFlag;
    }

    public void setCalFlag(String aCalFlag)
    {
        CalFlag = aCalFlag;
    }

    public String getAccCreatePos()
    {
        if (AccCreatePos != null && !AccCreatePos.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AccCreatePos = StrTool.unicodeToGBK(AccCreatePos);
        }
        return AccCreatePos;
    }

    public void setAccCreatePos(String aAccCreatePos)
    {
        AccCreatePos = aAccCreatePos;
    }

    public String getGetDutyName()
    {
        if (GetDutyName != null && !GetDutyName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GetDutyName = StrTool.unicodeToGBK(GetDutyName);
        }
        return GetDutyName;
    }

    public void setGetDutyName(String aGetDutyName)
    {
        GetDutyName = aGetDutyName;
    }

    /**
     * 使用另外一个 LMRiskAccGetSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LMRiskAccGetSchema aLMRiskAccGetSchema)
    {
        this.RiskCode = aLMRiskAccGetSchema.getRiskCode();
        this.RiskVer = aLMRiskAccGetSchema.getRiskVer();
        this.InsuAccNo = aLMRiskAccGetSchema.getInsuAccNo();
        this.GetDutyCode = aLMRiskAccGetSchema.getGetDutyCode();
        this.DefaultRate = aLMRiskAccGetSchema.getDefaultRate();
        this.NeedInput = aLMRiskAccGetSchema.getNeedInput();
        this.CalCodeMoney = aLMRiskAccGetSchema.getCalCodeMoney();
        this.DealDirection = aLMRiskAccGetSchema.getDealDirection();
        this.CalFlag = aLMRiskAccGetSchema.getCalFlag();
        this.AccCreatePos = aLMRiskAccGetSchema.getAccCreatePos();
        this.GetDutyName = aLMRiskAccGetSchema.getGetDutyName();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("RiskVer") == null)
            {
                this.RiskVer = null;
            }
            else
            {
                this.RiskVer = rs.getString("RiskVer").trim();
            }

            if (rs.getString("InsuAccNo") == null)
            {
                this.InsuAccNo = null;
            }
            else
            {
                this.InsuAccNo = rs.getString("InsuAccNo").trim();
            }

            if (rs.getString("GetDutyCode") == null)
            {
                this.GetDutyCode = null;
            }
            else
            {
                this.GetDutyCode = rs.getString("GetDutyCode").trim();
            }

            this.DefaultRate = rs.getDouble("DefaultRate");
            if (rs.getString("NeedInput") == null)
            {
                this.NeedInput = null;
            }
            else
            {
                this.NeedInput = rs.getString("NeedInput").trim();
            }

            if (rs.getString("CalCodeMoney") == null)
            {
                this.CalCodeMoney = null;
            }
            else
            {
                this.CalCodeMoney = rs.getString("CalCodeMoney").trim();
            }

            if (rs.getString("DealDirection") == null)
            {
                this.DealDirection = null;
            }
            else
            {
                this.DealDirection = rs.getString("DealDirection").trim();
            }

            if (rs.getString("CalFlag") == null)
            {
                this.CalFlag = null;
            }
            else
            {
                this.CalFlag = rs.getString("CalFlag").trim();
            }

            if (rs.getString("AccCreatePos") == null)
            {
                this.AccCreatePos = null;
            }
            else
            {
                this.AccCreatePos = rs.getString("AccCreatePos").trim();
            }

            if (rs.getString("GetDutyName") == null)
            {
                this.GetDutyName = null;
            }
            else
            {
                this.GetDutyName = rs.getString("GetDutyName").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMRiskAccGetSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LMRiskAccGetSchema getSchema()
    {
        LMRiskAccGetSchema aLMRiskAccGetSchema = new LMRiskAccGetSchema();
        aLMRiskAccGetSchema.setSchema(this);
        return aLMRiskAccGetSchema;
    }

    public LMRiskAccGetDB getDB()
    {
        LMRiskAccGetDB aDBOper = new LMRiskAccGetDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMRiskAccGet描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskVer)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InsuAccNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GetDutyCode)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(DefaultRate) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(NeedInput)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CalCodeMoney)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(DealDirection)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CalFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AccCreatePos)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GetDutyName));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMRiskAccGet>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            RiskVer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                     SysConst.PACKAGESPILTER);
            InsuAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                       SysConst.PACKAGESPILTER);
            GetDutyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                         SysConst.PACKAGESPILTER);
            DefaultRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 5, SysConst.PACKAGESPILTER))).doubleValue();
            NeedInput = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                       SysConst.PACKAGESPILTER);
            CalCodeMoney = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                          SysConst.PACKAGESPILTER);
            DealDirection = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                           SysConst.PACKAGESPILTER);
            CalFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                     SysConst.PACKAGESPILTER);
            AccCreatePos = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                          SysConst.PACKAGESPILTER);
            GetDutyName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                         SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMRiskAccGetSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskCode));
        }
        if (FCode.equals("RiskVer"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskVer));
        }
        if (FCode.equals("InsuAccNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(InsuAccNo));
        }
        if (FCode.equals("GetDutyCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetDutyCode));
        }
        if (FCode.equals("DefaultRate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DefaultRate));
        }
        if (FCode.equals("NeedInput"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(NeedInput));
        }
        if (FCode.equals("CalCodeMoney"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CalCodeMoney));
        }
        if (FCode.equals("DealDirection"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DealDirection));
        }
        if (FCode.equals("CalFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CalFlag));
        }
        if (FCode.equals("AccCreatePos"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AccCreatePos));
        }
        if (FCode.equals("GetDutyName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetDutyName));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(RiskVer);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(InsuAccNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(GetDutyCode);
                break;
            case 4:
                strFieldValue = String.valueOf(DefaultRate);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(NeedInput);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(CalCodeMoney);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(DealDirection);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(CalFlag);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(AccCreatePos);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(GetDutyName);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("RiskVer"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskVer = FValue.trim();
            }
            else
            {
                RiskVer = null;
            }
        }
        if (FCode.equals("InsuAccNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InsuAccNo = FValue.trim();
            }
            else
            {
                InsuAccNo = null;
            }
        }
        if (FCode.equals("GetDutyCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetDutyCode = FValue.trim();
            }
            else
            {
                GetDutyCode = null;
            }
        }
        if (FCode.equals("DefaultRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                DefaultRate = d;
            }
        }
        if (FCode.equals("NeedInput"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                NeedInput = FValue.trim();
            }
            else
            {
                NeedInput = null;
            }
        }
        if (FCode.equals("CalCodeMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CalCodeMoney = FValue.trim();
            }
            else
            {
                CalCodeMoney = null;
            }
        }
        if (FCode.equals("DealDirection"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DealDirection = FValue.trim();
            }
            else
            {
                DealDirection = null;
            }
        }
        if (FCode.equals("CalFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CalFlag = FValue.trim();
            }
            else
            {
                CalFlag = null;
            }
        }
        if (FCode.equals("AccCreatePos"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AccCreatePos = FValue.trim();
            }
            else
            {
                AccCreatePos = null;
            }
        }
        if (FCode.equals("GetDutyName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetDutyName = FValue.trim();
            }
            else
            {
                GetDutyName = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LMRiskAccGetSchema other = (LMRiskAccGetSchema) otherObject;
        return
                RiskCode.equals(other.getRiskCode())
                && RiskVer.equals(other.getRiskVer())
                && InsuAccNo.equals(other.getInsuAccNo())
                && GetDutyCode.equals(other.getGetDutyCode())
                && DefaultRate == other.getDefaultRate()
                && NeedInput.equals(other.getNeedInput())
                && CalCodeMoney.equals(other.getCalCodeMoney())
                && DealDirection.equals(other.getDealDirection())
                && CalFlag.equals(other.getCalFlag())
                && AccCreatePos.equals(other.getAccCreatePos())
                && GetDutyName.equals(other.getGetDutyName());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("RiskCode"))
        {
            return 0;
        }
        if (strFieldName.equals("RiskVer"))
        {
            return 1;
        }
        if (strFieldName.equals("InsuAccNo"))
        {
            return 2;
        }
        if (strFieldName.equals("GetDutyCode"))
        {
            return 3;
        }
        if (strFieldName.equals("DefaultRate"))
        {
            return 4;
        }
        if (strFieldName.equals("NeedInput"))
        {
            return 5;
        }
        if (strFieldName.equals("CalCodeMoney"))
        {
            return 6;
        }
        if (strFieldName.equals("DealDirection"))
        {
            return 7;
        }
        if (strFieldName.equals("CalFlag"))
        {
            return 8;
        }
        if (strFieldName.equals("AccCreatePos"))
        {
            return 9;
        }
        if (strFieldName.equals("GetDutyName"))
        {
            return 10;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "RiskVer";
                break;
            case 2:
                strFieldName = "InsuAccNo";
                break;
            case 3:
                strFieldName = "GetDutyCode";
                break;
            case 4:
                strFieldName = "DefaultRate";
                break;
            case 5:
                strFieldName = "NeedInput";
                break;
            case 6:
                strFieldName = "CalCodeMoney";
                break;
            case 7:
                strFieldName = "DealDirection";
                break;
            case 8:
                strFieldName = "CalFlag";
                break;
            case 9:
                strFieldName = "AccCreatePos";
                break;
            case 10:
                strFieldName = "GetDutyName";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskVer"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsuAccNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetDutyCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DefaultRate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("NeedInput"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CalCodeMoney"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DealDirection"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CalFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AccCreatePos"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetDutyName"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
