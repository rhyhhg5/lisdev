/*
 * <p>ClassName: LAAgentLoginSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2004-12-20
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LAAgentLoginDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LAAgentLoginSchema implements Schema
{
    // @Field
    /** 代理人编码 */
    private String AgentCode;
    /** 机构编码 */
    private String ComCode;
    /** 客户端名称 */
    private String ClientName;
    /** 客户端类型 */
    private String ClientType;
    /** 客户端ip */
    private String ClientIP;
    /** 打印机类型 */
    private String PrinterType;
    /** 输入方法 */
    private String InputType;
    /** 注册年月 */
    private Date RgtDate;
    /** 登陆日期 */
    private Date LoginDate;
    /** 登陆时间 */
    private Date LoginTime;

    public static final int FIELDNUM = 10; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LAAgentLoginSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "AgentCode";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getAgentCode()
    {
        if (AgentCode != null && !AgentCode.equals("") &&
            SysConst.CHANGECHARSET)
        {
            AgentCode = StrTool.unicodeToGBK(AgentCode);
        }
        return AgentCode;
    }

    public void setAgentCode(String aAgentCode)
    {
        AgentCode = aAgentCode;
    }

    public String getComCode()
    {
        if (ComCode != null && !ComCode.equals("") && SysConst.CHANGECHARSET)
        {
            ComCode = StrTool.unicodeToGBK(ComCode);
        }
        return ComCode;
    }

    public void setComCode(String aComCode)
    {
        ComCode = aComCode;
    }

    public String getClientName()
    {
        if (ClientName != null && !ClientName.equals("") &&
            SysConst.CHANGECHARSET)
        {
            ClientName = StrTool.unicodeToGBK(ClientName);
        }
        return ClientName;
    }

    public void setClientName(String aClientName)
    {
        ClientName = aClientName;
    }

    public String getClientType()
    {
        if (ClientType != null && !ClientType.equals("") &&
            SysConst.CHANGECHARSET)
        {
            ClientType = StrTool.unicodeToGBK(ClientType);
        }
        return ClientType;
    }

    public void setClientType(String aClientType)
    {
        ClientType = aClientType;
    }

    public String getClientIP()
    {
        if (ClientIP != null && !ClientIP.equals("") && SysConst.CHANGECHARSET)
        {
            ClientIP = StrTool.unicodeToGBK(ClientIP);
        }
        return ClientIP;
    }

    public void setClientIP(String aClientIP)
    {
        ClientIP = aClientIP;
    }

    public String getPrinterType()
    {
        if (PrinterType != null && !PrinterType.equals("") &&
            SysConst.CHANGECHARSET)
        {
            PrinterType = StrTool.unicodeToGBK(PrinterType);
        }
        return PrinterType;
    }

    public void setPrinterType(String aPrinterType)
    {
        PrinterType = aPrinterType;
    }

    public String getInputType()
    {
        if (InputType != null && !InputType.equals("") &&
            SysConst.CHANGECHARSET)
        {
            InputType = StrTool.unicodeToGBK(InputType);
        }
        return InputType;
    }

    public void setInputType(String aInputType)
    {
        InputType = aInputType;
    }

    public String getRgtDate()
    {
        if (RgtDate != null)
        {
            return fDate.getString(RgtDate);
        }
        else
        {
            return null;
        }
    }

    public void setRgtDate(Date aRgtDate)
    {
        RgtDate = aRgtDate;
    }

    public void setRgtDate(String aRgtDate)
    {
        if (aRgtDate != null && !aRgtDate.equals(""))
        {
            RgtDate = fDate.getDate(aRgtDate);
        }
        else
        {
            RgtDate = null;
        }
    }

    public String getLoginDate()
    {
        if (LoginDate != null)
        {
            return fDate.getString(LoginDate);
        }
        else
        {
            return null;
        }
    }

    public void setLoginDate(Date aLoginDate)
    {
        LoginDate = aLoginDate;
    }

    public void setLoginDate(String aLoginDate)
    {
        if (aLoginDate != null && !aLoginDate.equals(""))
        {
            LoginDate = fDate.getDate(aLoginDate);
        }
        else
        {
            LoginDate = null;
        }
    }

    public String getLoginTime()
    {
        if (LoginTime != null)
        {
            return fDate.getString(LoginTime);
        }
        else
        {
            return null;
        }
    }

    public void setLoginTime(Date aLoginTime)
    {
        LoginTime = aLoginTime;
    }

    public void setLoginTime(String aLoginTime)
    {
        if (aLoginTime != null && !aLoginTime.equals(""))
        {
            LoginTime = fDate.getDate(aLoginTime);
        }
        else
        {
            LoginTime = null;
        }
    }


    /**
     * 使用另外一个 LAAgentLoginSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LAAgentLoginSchema aLAAgentLoginSchema)
    {
        this.AgentCode = aLAAgentLoginSchema.getAgentCode();
        this.ComCode = aLAAgentLoginSchema.getComCode();
        this.ClientName = aLAAgentLoginSchema.getClientName();
        this.ClientType = aLAAgentLoginSchema.getClientType();
        this.ClientIP = aLAAgentLoginSchema.getClientIP();
        this.PrinterType = aLAAgentLoginSchema.getPrinterType();
        this.InputType = aLAAgentLoginSchema.getInputType();
        this.RgtDate = fDate.getDate(aLAAgentLoginSchema.getRgtDate());
        this.LoginDate = fDate.getDate(aLAAgentLoginSchema.getLoginDate());
        this.LoginTime = fDate.getDate(aLAAgentLoginSchema.getLoginTime());
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("AgentCode") == null)
            {
                this.AgentCode = null;
            }
            else
            {
                this.AgentCode = rs.getString("AgentCode").trim();
            }

            if (rs.getString("ComCode") == null)
            {
                this.ComCode = null;
            }
            else
            {
                this.ComCode = rs.getString("ComCode").trim();
            }

            if (rs.getString("ClientName") == null)
            {
                this.ClientName = null;
            }
            else
            {
                this.ClientName = rs.getString("ClientName").trim();
            }

            if (rs.getString("ClientType") == null)
            {
                this.ClientType = null;
            }
            else
            {
                this.ClientType = rs.getString("ClientType").trim();
            }

            if (rs.getString("ClientIP") == null)
            {
                this.ClientIP = null;
            }
            else
            {
                this.ClientIP = rs.getString("ClientIP").trim();
            }

            if (rs.getString("PrinterType") == null)
            {
                this.PrinterType = null;
            }
            else
            {
                this.PrinterType = rs.getString("PrinterType").trim();
            }

            if (rs.getString("InputType") == null)
            {
                this.InputType = null;
            }
            else
            {
                this.InputType = rs.getString("InputType").trim();
            }

            this.RgtDate = rs.getDate("RgtDate");
            this.LoginDate = rs.getDate("LoginDate");
            this.LoginTime = rs.getDate("LoginTime");
        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentLoginSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LAAgentLoginSchema getSchema()
    {
        LAAgentLoginSchema aLAAgentLoginSchema = new LAAgentLoginSchema();
        aLAAgentLoginSchema.setSchema(this);
        return aLAAgentLoginSchema;
    }

    public LAAgentLoginDB getDB()
    {
        LAAgentLoginDB aDBOper = new LAAgentLoginDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAAgentLogin描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(AgentCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ComCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ClientName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ClientType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ClientIP)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PrinterType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InputType)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(RgtDate))) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            LoginDate))) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            LoginTime)));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAAgentLogin>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            ComCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                     SysConst.PACKAGESPILTER);
            ClientName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                        SysConst.PACKAGESPILTER);
            ClientType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                        SysConst.PACKAGESPILTER);
            ClientIP = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                      SysConst.PACKAGESPILTER);
            PrinterType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                         SysConst.PACKAGESPILTER);
            InputType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                       SysConst.PACKAGESPILTER);
            RgtDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 8, SysConst.PACKAGESPILTER));
            LoginDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 9, SysConst.PACKAGESPILTER));
            LoginTime = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 10, SysConst.PACKAGESPILTER));
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentLoginSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("AgentCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equals("ComCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ComCode));
        }
        if (FCode.equals("ClientName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClientName));
        }
        if (FCode.equals("ClientType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClientType));
        }
        if (FCode.equals("ClientIP"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClientIP));
        }
        if (FCode.equals("PrinterType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrinterType));
        }
        if (FCode.equals("InputType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InputType));
        }
        if (FCode.equals("RgtDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getRgtDate()));
        }
        if (FCode.equals("LoginDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getLoginDate()));
        }
        if (FCode.equals("LoginTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getLoginTime()));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(AgentCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ComCode);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ClientName);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(ClientType);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(ClientIP);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(PrinterType);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(InputType);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getRgtDate()));
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getLoginDate()));
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getLoginTime()));
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("AgentCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
            {
                AgentCode = null;
            }
        }
        if (FCode.equals("ComCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ComCode = FValue.trim();
            }
            else
            {
                ComCode = null;
            }
        }
        if (FCode.equals("ClientName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ClientName = FValue.trim();
            }
            else
            {
                ClientName = null;
            }
        }
        if (FCode.equals("ClientType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ClientType = FValue.trim();
            }
            else
            {
                ClientType = null;
            }
        }
        if (FCode.equals("ClientIP"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ClientIP = FValue.trim();
            }
            else
            {
                ClientIP = null;
            }
        }
        if (FCode.equals("PrinterType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PrinterType = FValue.trim();
            }
            else
            {
                PrinterType = null;
            }
        }
        if (FCode.equals("InputType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InputType = FValue.trim();
            }
            else
            {
                InputType = null;
            }
        }
        if (FCode.equals("RgtDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RgtDate = fDate.getDate(FValue);
            }
            else
            {
                RgtDate = null;
            }
        }
        if (FCode.equals("LoginDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                LoginDate = fDate.getDate(FValue);
            }
            else
            {
                LoginDate = null;
            }
        }
        if (FCode.equals("LoginTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                LoginTime = fDate.getDate(FValue);
            }
            else
            {
                LoginTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LAAgentLoginSchema other = (LAAgentLoginSchema) otherObject;
        return
                AgentCode.equals(other.getAgentCode())
                && ComCode.equals(other.getComCode())
                && ClientName.equals(other.getClientName())
                && ClientType.equals(other.getClientType())
                && ClientIP.equals(other.getClientIP())
                && PrinterType.equals(other.getPrinterType())
                && InputType.equals(other.getInputType())
                && fDate.getString(RgtDate).equals(other.getRgtDate())
                && fDate.getString(LoginDate).equals(other.getLoginDate())
                && fDate.getString(LoginTime).equals(other.getLoginTime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("AgentCode"))
        {
            return 0;
        }
        if (strFieldName.equals("ComCode"))
        {
            return 1;
        }
        if (strFieldName.equals("ClientName"))
        {
            return 2;
        }
        if (strFieldName.equals("ClientType"))
        {
            return 3;
        }
        if (strFieldName.equals("ClientIP"))
        {
            return 4;
        }
        if (strFieldName.equals("PrinterType"))
        {
            return 5;
        }
        if (strFieldName.equals("InputType"))
        {
            return 6;
        }
        if (strFieldName.equals("RgtDate"))
        {
            return 7;
        }
        if (strFieldName.equals("LoginDate"))
        {
            return 8;
        }
        if (strFieldName.equals("LoginTime"))
        {
            return 9;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "AgentCode";
                break;
            case 1:
                strFieldName = "ComCode";
                break;
            case 2:
                strFieldName = "ClientName";
                break;
            case 3:
                strFieldName = "ClientType";
                break;
            case 4:
                strFieldName = "ClientIP";
                break;
            case 5:
                strFieldName = "PrinterType";
                break;
            case 6:
                strFieldName = "InputType";
                break;
            case 7:
                strFieldName = "RgtDate";
                break;
            case 8:
                strFieldName = "LoginDate";
                break;
            case 9:
                strFieldName = "LoginTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("AgentCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ComCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ClientName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ClientType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ClientIP"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PrinterType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InputType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RgtDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("LoginDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("LoginTime"))
        {
            return Schema.TYPE_DATE;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 8:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 9:
                nFieldType = Schema.TYPE_DATE;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
