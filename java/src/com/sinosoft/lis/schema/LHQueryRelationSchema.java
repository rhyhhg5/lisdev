/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LHQueryRelationDB;

/*
 * <p>ClassName: LHQueryRelationSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PICCH-郭楠-表结构修改-20060207-查询
 * @CreateDate：2006-02-14
 */
public class LHQueryRelationSchema implements Schema, Cloneable {
    // @Field
    /** 项目代码 */
    private String ItemCode;
    /** 关联项目代码 */
    private String RelaItemCode;
    /** 关联项目类型 */
    private String RelaItemType;

    public static final int FIELDNUM = 3; // 数据库表的字段个数

    private static String[] PK; // 主键

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LHQueryRelationSchema() {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "ItemCode";
        pk[1] = "RelaItemCode";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LHQueryRelationSchema cloned = (LHQueryRelationSchema)super.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getItemCode() {
        return ItemCode;
    }

    public void setItemCode(String aItemCode) {
        ItemCode = aItemCode;
    }

    public String getRelaItemCode() {
        return RelaItemCode;
    }

    public void setRelaItemCode(String aRelaItemCode) {
        RelaItemCode = aRelaItemCode;
    }

    public String getRelaItemType() {
        return RelaItemType;
    }

    public void setRelaItemType(String aRelaItemType) {
        RelaItemType = aRelaItemType;
    }

    /**
     * 使用另外一个 LHQueryRelationSchema 对象给 Schema 赋值
     * @param: aLHQueryRelationSchema LHQueryRelationSchema
     **/
    public void setSchema(LHQueryRelationSchema aLHQueryRelationSchema) {
        this.ItemCode = aLHQueryRelationSchema.getItemCode();
        this.RelaItemCode = aLHQueryRelationSchema.getRelaItemCode();
        this.RelaItemType = aLHQueryRelationSchema.getRelaItemType();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ItemCode") == null) {
                this.ItemCode = null;
            } else {
                this.ItemCode = rs.getString("ItemCode").trim();
            }

            if (rs.getString("RelaItemCode") == null) {
                this.RelaItemCode = null;
            } else {
                this.RelaItemCode = rs.getString("RelaItemCode").trim();
            }

            if (rs.getString("RelaItemType") == null) {
                this.RelaItemType = null;
            } else {
                this.RelaItemType = rs.getString("RelaItemType").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LHQueryRelation表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHQueryRelationSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LHQueryRelationSchema getSchema() {
        LHQueryRelationSchema aLHQueryRelationSchema = new
                LHQueryRelationSchema();
        aLHQueryRelationSchema.setSchema(this);
        return aLHQueryRelationSchema;
    }

    public LHQueryRelationDB getDB() {
        LHQueryRelationDB aDBOper = new LHQueryRelationDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHQueryRelation描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(ItemCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RelaItemCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RelaItemType));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHQueryRelation>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            ItemCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            RelaItemCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                          SysConst.PACKAGESPILTER);
            RelaItemType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                          SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHQueryRelationSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("ItemCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ItemCode));
        }
        if (FCode.equals("RelaItemCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelaItemCode));
        }
        if (FCode.equals("RelaItemType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelaItemType));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(ItemCode);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(RelaItemCode);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(RelaItemType);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("ItemCode")) {
            if (FValue != null && !FValue.equals("")) {
                ItemCode = FValue.trim();
            } else {
                ItemCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("RelaItemCode")) {
            if (FValue != null && !FValue.equals("")) {
                RelaItemCode = FValue.trim();
            } else {
                RelaItemCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("RelaItemType")) {
            if (FValue != null && !FValue.equals("")) {
                RelaItemType = FValue.trim();
            } else {
                RelaItemType = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LHQueryRelationSchema other = (LHQueryRelationSchema) otherObject;
        return
                ItemCode.equals(other.getItemCode())
                && RelaItemCode.equals(other.getRelaItemCode())
                && RelaItemType.equals(other.getRelaItemType());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("ItemCode")) {
            return 0;
        }
        if (strFieldName.equals("RelaItemCode")) {
            return 1;
        }
        if (strFieldName.equals("RelaItemType")) {
            return 2;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "ItemCode";
            break;
        case 1:
            strFieldName = "RelaItemCode";
            break;
        case 2:
            strFieldName = "RelaItemType";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("ItemCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RelaItemCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RelaItemType")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
