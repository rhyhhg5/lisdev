/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.XFYJAppntDB;

/*
 * <p>ClassName: XFYJAppntSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 幸福一家数据存储表
 * @CreateDate：2017-09-13
 */
public class XFYJAppntSchema implements Schema, Cloneable
{
	// @Field
	/** 批次号 */
	private String BatchNo;
	/** 印刷号 */
	private String PrtNo;
	/** 管理机构 */
	private String ManageCom;
	/** 销售渠道 */
	private String SaleChnl;
	/** 中介公司代码 */
	private String AgentCom;
	/** 业务员代码 */
	private String AgentCode;
	/** 业务员名称 */
	private String AgentName;
	/** 投保单申请日期 */
	private Date PolapplyDate;
	/** 收单日期 */
	private Date ReceiveDate;
	/** 投保人姓名 */
	private String AppntName;
	/** 证件类型 */
	private String AppntIDtype;
	/** 证件号码 */
	private String AppntIDno;
	/** 客户类型 */
	private String AppntType;
	/** 证件生效日 */
	private Date IDStartDate;
	/** 证件失效日 */
	private Date IDEndDate;
	/** 性别 */
	private String AppntSex;
	/** 家庭收入 */
	private double FamilySalary;
	/** 出生日期 */
	private Date AppntBirthday;
	/** 职业代码 */
	private String OccupationCode;
	/** 年收入 */
	private double Salary;
	/** 工作单位 */
	private String WorkName;
	/** 岗位职务 */
	private String Position;
	/** 国籍 */
	private String NativePlace;
	/** 邮政编码 */
	private String ZipCode;
	/** 电子邮箱 */
	private String Email;
	/** 联系电话 */
	private String Phone;
	/** 固定电话 */
	private String HomePhone;
	/** 移动电话 */
	private String Mobile;
	/** 联系地址 */
	private String PostAddress;
	/** 缴费频次 */
	private int PayIntv;
	/** 缴费方式 */
	private String PayMode;
	/** 开户银行 */
	private String BankCode;
	/** 账 号 */
	private String BankAccNo;
	/** 是否需要续期缴费提醒 */
	private String DueFeeMsgFlag;
	/** 特别约定 */
	private String Remark;
	/** 套餐份数 */
	private double Copys;
	/** 交叉销售渠道 */
	private String Crs_SaleChnl;
	/** 交叉销售业务类型 */
	private String Crs_SaleType;
	/** 对方业务员代码 */
	private String GrpAgentCode;
	/** 对方机构代码 */
	private String GrpAgentCom;
	/** 对方业务员姓名 */
	private String GrpAgentName;
	/** 对方业务员证件号码 */
	private String GrpAgentIDno;
	/** 创建时间 */
	private String MakeTime;
	/** 最后修改时间 */
	private String ModifyTime;
	/** 操作员 */
	private String Operator;
	/** 创建日期 */
	private Date MakeDate;
	/** 最后修改日期 */
	private Date ModifyDate;
	/** 处理状态 */
	private String State;
	/** 备用状态 */
	private String Flag;
	/** 备用说明 */
	private String Other;
	/** 录入机构 */
	private String MngCom;
	/** 户名 */
	private String AccName;
	/** 代理销售业务员编码 */
	private String AgentSaleCode;
	/** 联系省份 */
	private String PostalProvince;
	/** 联系市 */
	private String PostalCity;
	/** 联系县 */
	private String PostalCounty;
	/** 联系乡镇 */
	private String PostalStreet;
	/** 联系村（社区） */
	private String PostalCommunity;

	public static final int FIELDNUM = 58;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public XFYJAppntSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "BatchNo";
		pk[1] = "PrtNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		XFYJAppntSchema cloned = (XFYJAppntSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getBatchNo()
	{
		return BatchNo;
	}
	public void setBatchNo(String aBatchNo)
	{
		BatchNo = aBatchNo;
	}
	public String getPrtNo()
	{
		return PrtNo;
	}
	public void setPrtNo(String aPrtNo)
	{
		PrtNo = aPrtNo;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getSaleChnl()
	{
		return SaleChnl;
	}
	public void setSaleChnl(String aSaleChnl)
	{
		SaleChnl = aSaleChnl;
	}
	public String getAgentCom()
	{
		return AgentCom;
	}
	public void setAgentCom(String aAgentCom)
	{
		AgentCom = aAgentCom;
	}
	public String getAgentCode()
	{
		return AgentCode;
	}
	public void setAgentCode(String aAgentCode)
	{
		AgentCode = aAgentCode;
	}
	public String getAgentName()
	{
		return AgentName;
	}
	public void setAgentName(String aAgentName)
	{
		AgentName = aAgentName;
	}
	public String getPolapplyDate()
	{
		if( PolapplyDate != null )
			return fDate.getString(PolapplyDate);
		else
			return null;
	}
	public void setPolapplyDate(Date aPolapplyDate)
	{
		PolapplyDate = aPolapplyDate;
	}
	public void setPolapplyDate(String aPolapplyDate)
	{
		if (aPolapplyDate != null && !aPolapplyDate.equals("") )
		{
			PolapplyDate = fDate.getDate( aPolapplyDate );
		}
		else
			PolapplyDate = null;
	}

	public String getReceiveDate()
	{
		if( ReceiveDate != null )
			return fDate.getString(ReceiveDate);
		else
			return null;
	}
	public void setReceiveDate(Date aReceiveDate)
	{
		ReceiveDate = aReceiveDate;
	}
	public void setReceiveDate(String aReceiveDate)
	{
		if (aReceiveDate != null && !aReceiveDate.equals("") )
		{
			ReceiveDate = fDate.getDate( aReceiveDate );
		}
		else
			ReceiveDate = null;
	}

	public String getAppntName()
	{
		return AppntName;
	}
	public void setAppntName(String aAppntName)
	{
		AppntName = aAppntName;
	}
	public String getAppntIDtype()
	{
		return AppntIDtype;
	}
	public void setAppntIDtype(String aAppntIDtype)
	{
		AppntIDtype = aAppntIDtype;
	}
	public String getAppntIDno()
	{
		return AppntIDno;
	}
	public void setAppntIDno(String aAppntIDno)
	{
		AppntIDno = aAppntIDno;
	}
	public String getAppntType()
	{
		return AppntType;
	}
	public void setAppntType(String aAppntType)
	{
		AppntType = aAppntType;
	}
	public String getIDStartDate()
	{
		if( IDStartDate != null )
			return fDate.getString(IDStartDate);
		else
			return null;
	}
	public void setIDStartDate(Date aIDStartDate)
	{
		IDStartDate = aIDStartDate;
	}
	public void setIDStartDate(String aIDStartDate)
	{
		if (aIDStartDate != null && !aIDStartDate.equals("") )
		{
			IDStartDate = fDate.getDate( aIDStartDate );
		}
		else
			IDStartDate = null;
	}

	public String getIDEndDate()
	{
		if( IDEndDate != null )
			return fDate.getString(IDEndDate);
		else
			return null;
	}
	public void setIDEndDate(Date aIDEndDate)
	{
		IDEndDate = aIDEndDate;
	}
	public void setIDEndDate(String aIDEndDate)
	{
		if (aIDEndDate != null && !aIDEndDate.equals("") )
		{
			IDEndDate = fDate.getDate( aIDEndDate );
		}
		else
			IDEndDate = null;
	}

	public String getAppntSex()
	{
		return AppntSex;
	}
	public void setAppntSex(String aAppntSex)
	{
		AppntSex = aAppntSex;
	}
	public double getFamilySalary()
	{
		return FamilySalary;
	}
	public void setFamilySalary(double aFamilySalary)
	{
		FamilySalary = Arith.round(aFamilySalary,2);
	}
	public void setFamilySalary(String aFamilySalary)
	{
		if (aFamilySalary != null && !aFamilySalary.equals(""))
		{
			Double tDouble = new Double(aFamilySalary);
			double d = tDouble.doubleValue();
                FamilySalary = Arith.round(d,2);
		}
	}

	public String getAppntBirthday()
	{
		if( AppntBirthday != null )
			return fDate.getString(AppntBirthday);
		else
			return null;
	}
	public void setAppntBirthday(Date aAppntBirthday)
	{
		AppntBirthday = aAppntBirthday;
	}
	public void setAppntBirthday(String aAppntBirthday)
	{
		if (aAppntBirthday != null && !aAppntBirthday.equals("") )
		{
			AppntBirthday = fDate.getDate( aAppntBirthday );
		}
		else
			AppntBirthday = null;
	}

	public String getOccupationCode()
	{
		return OccupationCode;
	}
	public void setOccupationCode(String aOccupationCode)
	{
		OccupationCode = aOccupationCode;
	}
	public double getSalary()
	{
		return Salary;
	}
	public void setSalary(double aSalary)
	{
		Salary = Arith.round(aSalary,2);
	}
	public void setSalary(String aSalary)
	{
		if (aSalary != null && !aSalary.equals(""))
		{
			Double tDouble = new Double(aSalary);
			double d = tDouble.doubleValue();
                Salary = Arith.round(d,2);
		}
	}

	public String getWorkName()
	{
		return WorkName;
	}
	public void setWorkName(String aWorkName)
	{
		WorkName = aWorkName;
	}
	public String getPosition()
	{
		return Position;
	}
	public void setPosition(String aPosition)
	{
		Position = aPosition;
	}
	public String getNativePlace()
	{
		return NativePlace;
	}
	public void setNativePlace(String aNativePlace)
	{
		NativePlace = aNativePlace;
	}
	public String getZipCode()
	{
		return ZipCode;
	}
	public void setZipCode(String aZipCode)
	{
		ZipCode = aZipCode;
	}
	public String getEmail()
	{
		return Email;
	}
	public void setEmail(String aEmail)
	{
		Email = aEmail;
	}
	public String getPhone()
	{
		return Phone;
	}
	public void setPhone(String aPhone)
	{
		Phone = aPhone;
	}
	public String getHomePhone()
	{
		return HomePhone;
	}
	public void setHomePhone(String aHomePhone)
	{
		HomePhone = aHomePhone;
	}
	public String getMobile()
	{
		return Mobile;
	}
	public void setMobile(String aMobile)
	{
		Mobile = aMobile;
	}
	public String getPostAddress()
	{
		return PostAddress;
	}
	public void setPostAddress(String aPostAddress)
	{
		PostAddress = aPostAddress;
	}
	public int getPayIntv()
	{
		return PayIntv;
	}
	public void setPayIntv(int aPayIntv)
	{
		PayIntv = aPayIntv;
	}
	public void setPayIntv(String aPayIntv)
	{
		if (aPayIntv != null && !aPayIntv.equals(""))
		{
			Integer tInteger = new Integer(aPayIntv);
			int i = tInteger.intValue();
			PayIntv = i;
		}
	}

	public String getPayMode()
	{
		return PayMode;
	}
	public void setPayMode(String aPayMode)
	{
		PayMode = aPayMode;
	}
	public String getBankCode()
	{
		return BankCode;
	}
	public void setBankCode(String aBankCode)
	{
		BankCode = aBankCode;
	}
	public String getBankAccNo()
	{
		return BankAccNo;
	}
	public void setBankAccNo(String aBankAccNo)
	{
		BankAccNo = aBankAccNo;
	}
	public String getDueFeeMsgFlag()
	{
		return DueFeeMsgFlag;
	}
	public void setDueFeeMsgFlag(String aDueFeeMsgFlag)
	{
		DueFeeMsgFlag = aDueFeeMsgFlag;
	}
	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
		Remark = aRemark;
	}
	public double getCopys()
	{
		return Copys;
	}
	public void setCopys(double aCopys)
	{
		Copys = Arith.round(aCopys,2);
	}
	public void setCopys(String aCopys)
	{
		if (aCopys != null && !aCopys.equals(""))
		{
			Double tDouble = new Double(aCopys);
			double d = tDouble.doubleValue();
                Copys = Arith.round(d,2);
		}
	}

	public String getCrs_SaleChnl()
	{
		return Crs_SaleChnl;
	}
	public void setCrs_SaleChnl(String aCrs_SaleChnl)
	{
		Crs_SaleChnl = aCrs_SaleChnl;
	}
	public String getCrs_SaleType()
	{
		return Crs_SaleType;
	}
	public void setCrs_SaleType(String aCrs_SaleType)
	{
		Crs_SaleType = aCrs_SaleType;
	}
	public String getGrpAgentCode()
	{
		return GrpAgentCode;
	}
	public void setGrpAgentCode(String aGrpAgentCode)
	{
		GrpAgentCode = aGrpAgentCode;
	}
	public String getGrpAgentCom()
	{
		return GrpAgentCom;
	}
	public void setGrpAgentCom(String aGrpAgentCom)
	{
		GrpAgentCom = aGrpAgentCom;
	}
	public String getGrpAgentName()
	{
		return GrpAgentName;
	}
	public void setGrpAgentName(String aGrpAgentName)
	{
		GrpAgentName = aGrpAgentName;
	}
	public String getGrpAgentIDno()
	{
		return GrpAgentIDno;
	}
	public void setGrpAgentIDno(String aGrpAgentIDno)
	{
		GrpAgentIDno = aGrpAgentIDno;
	}
	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}
	public String getFlag()
	{
		return Flag;
	}
	public void setFlag(String aFlag)
	{
		Flag = aFlag;
	}
	public String getOther()
	{
		return Other;
	}
	public void setOther(String aOther)
	{
		Other = aOther;
	}
	public String getMngCom()
	{
		return MngCom;
	}
	public void setMngCom(String aMngCom)
	{
		MngCom = aMngCom;
	}
	public String getAccName()
	{
		return AccName;
	}
	public void setAccName(String aAccName)
	{
		AccName = aAccName;
	}
	public String getAgentSaleCode()
	{
		return AgentSaleCode;
	}
	public void setAgentSaleCode(String aAgentSaleCode)
	{
		AgentSaleCode = aAgentSaleCode;
	}
	public String getPostalProvince()
	{
		return PostalProvince;
	}
	public void setPostalProvince(String aPostalProvince)
	{
		PostalProvince = aPostalProvince;
	}
	public String getPostalCity()
	{
		return PostalCity;
	}
	public void setPostalCity(String aPostalCity)
	{
		PostalCity = aPostalCity;
	}
	public String getPostalCounty()
	{
		return PostalCounty;
	}
	public void setPostalCounty(String aPostalCounty)
	{
		PostalCounty = aPostalCounty;
	}
	public String getPostalStreet()
	{
		return PostalStreet;
	}
	public void setPostalStreet(String aPostalStreet)
	{
		PostalStreet = aPostalStreet;
	}
	public String getPostalCommunity()
	{
		return PostalCommunity;
	}
	public void setPostalCommunity(String aPostalCommunity)
	{
		PostalCommunity = aPostalCommunity;
	}

	/**
	* 使用另外一个 XFYJAppntSchema 对象给 Schema 赋值
	* @param: aXFYJAppntSchema XFYJAppntSchema
	**/
	public void setSchema(XFYJAppntSchema aXFYJAppntSchema)
	{
		this.BatchNo = aXFYJAppntSchema.getBatchNo();
		this.PrtNo = aXFYJAppntSchema.getPrtNo();
		this.ManageCom = aXFYJAppntSchema.getManageCom();
		this.SaleChnl = aXFYJAppntSchema.getSaleChnl();
		this.AgentCom = aXFYJAppntSchema.getAgentCom();
		this.AgentCode = aXFYJAppntSchema.getAgentCode();
		this.AgentName = aXFYJAppntSchema.getAgentName();
		this.PolapplyDate = fDate.getDate( aXFYJAppntSchema.getPolapplyDate());
		this.ReceiveDate = fDate.getDate( aXFYJAppntSchema.getReceiveDate());
		this.AppntName = aXFYJAppntSchema.getAppntName();
		this.AppntIDtype = aXFYJAppntSchema.getAppntIDtype();
		this.AppntIDno = aXFYJAppntSchema.getAppntIDno();
		this.AppntType = aXFYJAppntSchema.getAppntType();
		this.IDStartDate = fDate.getDate( aXFYJAppntSchema.getIDStartDate());
		this.IDEndDate = fDate.getDate( aXFYJAppntSchema.getIDEndDate());
		this.AppntSex = aXFYJAppntSchema.getAppntSex();
		this.FamilySalary = aXFYJAppntSchema.getFamilySalary();
		this.AppntBirthday = fDate.getDate( aXFYJAppntSchema.getAppntBirthday());
		this.OccupationCode = aXFYJAppntSchema.getOccupationCode();
		this.Salary = aXFYJAppntSchema.getSalary();
		this.WorkName = aXFYJAppntSchema.getWorkName();
		this.Position = aXFYJAppntSchema.getPosition();
		this.NativePlace = aXFYJAppntSchema.getNativePlace();
		this.ZipCode = aXFYJAppntSchema.getZipCode();
		this.Email = aXFYJAppntSchema.getEmail();
		this.Phone = aXFYJAppntSchema.getPhone();
		this.HomePhone = aXFYJAppntSchema.getHomePhone();
		this.Mobile = aXFYJAppntSchema.getMobile();
		this.PostAddress = aXFYJAppntSchema.getPostAddress();
		this.PayIntv = aXFYJAppntSchema.getPayIntv();
		this.PayMode = aXFYJAppntSchema.getPayMode();
		this.BankCode = aXFYJAppntSchema.getBankCode();
		this.BankAccNo = aXFYJAppntSchema.getBankAccNo();
		this.DueFeeMsgFlag = aXFYJAppntSchema.getDueFeeMsgFlag();
		this.Remark = aXFYJAppntSchema.getRemark();
		this.Copys = aXFYJAppntSchema.getCopys();
		this.Crs_SaleChnl = aXFYJAppntSchema.getCrs_SaleChnl();
		this.Crs_SaleType = aXFYJAppntSchema.getCrs_SaleType();
		this.GrpAgentCode = aXFYJAppntSchema.getGrpAgentCode();
		this.GrpAgentCom = aXFYJAppntSchema.getGrpAgentCom();
		this.GrpAgentName = aXFYJAppntSchema.getGrpAgentName();
		this.GrpAgentIDno = aXFYJAppntSchema.getGrpAgentIDno();
		this.MakeTime = aXFYJAppntSchema.getMakeTime();
		this.ModifyTime = aXFYJAppntSchema.getModifyTime();
		this.Operator = aXFYJAppntSchema.getOperator();
		this.MakeDate = fDate.getDate( aXFYJAppntSchema.getMakeDate());
		this.ModifyDate = fDate.getDate( aXFYJAppntSchema.getModifyDate());
		this.State = aXFYJAppntSchema.getState();
		this.Flag = aXFYJAppntSchema.getFlag();
		this.Other = aXFYJAppntSchema.getOther();
		this.MngCom = aXFYJAppntSchema.getMngCom();
		this.AccName = aXFYJAppntSchema.getAccName();
		this.AgentSaleCode = aXFYJAppntSchema.getAgentSaleCode();
		this.PostalProvince = aXFYJAppntSchema.getPostalProvince();
		this.PostalCity = aXFYJAppntSchema.getPostalCity();
		this.PostalCounty = aXFYJAppntSchema.getPostalCounty();
		this.PostalStreet = aXFYJAppntSchema.getPostalStreet();
		this.PostalCommunity = aXFYJAppntSchema.getPostalCommunity();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("BatchNo") == null )
				this.BatchNo = null;
			else
				this.BatchNo = rs.getString("BatchNo").trim();

			if( rs.getString("PrtNo") == null )
				this.PrtNo = null;
			else
				this.PrtNo = rs.getString("PrtNo").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("SaleChnl") == null )
				this.SaleChnl = null;
			else
				this.SaleChnl = rs.getString("SaleChnl").trim();

			if( rs.getString("AgentCom") == null )
				this.AgentCom = null;
			else
				this.AgentCom = rs.getString("AgentCom").trim();

			if( rs.getString("AgentCode") == null )
				this.AgentCode = null;
			else
				this.AgentCode = rs.getString("AgentCode").trim();

			if( rs.getString("AgentName") == null )
				this.AgentName = null;
			else
				this.AgentName = rs.getString("AgentName").trim();

			this.PolapplyDate = rs.getDate("PolapplyDate");
			this.ReceiveDate = rs.getDate("ReceiveDate");
			if( rs.getString("AppntName") == null )
				this.AppntName = null;
			else
				this.AppntName = rs.getString("AppntName").trim();

			if( rs.getString("AppntIDtype") == null )
				this.AppntIDtype = null;
			else
				this.AppntIDtype = rs.getString("AppntIDtype").trim();

			if( rs.getString("AppntIDno") == null )
				this.AppntIDno = null;
			else
				this.AppntIDno = rs.getString("AppntIDno").trim();

			if( rs.getString("AppntType") == null )
				this.AppntType = null;
			else
				this.AppntType = rs.getString("AppntType").trim();

			this.IDStartDate = rs.getDate("IDStartDate");
			this.IDEndDate = rs.getDate("IDEndDate");
			if( rs.getString("AppntSex") == null )
				this.AppntSex = null;
			else
				this.AppntSex = rs.getString("AppntSex").trim();

			this.FamilySalary = rs.getDouble("FamilySalary");
			this.AppntBirthday = rs.getDate("AppntBirthday");
			if( rs.getString("OccupationCode") == null )
				this.OccupationCode = null;
			else
				this.OccupationCode = rs.getString("OccupationCode").trim();

			this.Salary = rs.getDouble("Salary");
			if( rs.getString("WorkName") == null )
				this.WorkName = null;
			else
				this.WorkName = rs.getString("WorkName").trim();

			if( rs.getString("Position") == null )
				this.Position = null;
			else
				this.Position = rs.getString("Position").trim();

			if( rs.getString("NativePlace") == null )
				this.NativePlace = null;
			else
				this.NativePlace = rs.getString("NativePlace").trim();

			if( rs.getString("ZipCode") == null )
				this.ZipCode = null;
			else
				this.ZipCode = rs.getString("ZipCode").trim();

			if( rs.getString("Email") == null )
				this.Email = null;
			else
				this.Email = rs.getString("Email").trim();

			if( rs.getString("Phone") == null )
				this.Phone = null;
			else
				this.Phone = rs.getString("Phone").trim();

			if( rs.getString("HomePhone") == null )
				this.HomePhone = null;
			else
				this.HomePhone = rs.getString("HomePhone").trim();

			if( rs.getString("Mobile") == null )
				this.Mobile = null;
			else
				this.Mobile = rs.getString("Mobile").trim();

			if( rs.getString("PostAddress") == null )
				this.PostAddress = null;
			else
				this.PostAddress = rs.getString("PostAddress").trim();

			this.PayIntv = rs.getInt("PayIntv");
			if( rs.getString("PayMode") == null )
				this.PayMode = null;
			else
				this.PayMode = rs.getString("PayMode").trim();

			if( rs.getString("BankCode") == null )
				this.BankCode = null;
			else
				this.BankCode = rs.getString("BankCode").trim();

			if( rs.getString("BankAccNo") == null )
				this.BankAccNo = null;
			else
				this.BankAccNo = rs.getString("BankAccNo").trim();

			if( rs.getString("DueFeeMsgFlag") == null )
				this.DueFeeMsgFlag = null;
			else
				this.DueFeeMsgFlag = rs.getString("DueFeeMsgFlag").trim();

			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

			this.Copys = rs.getDouble("Copys");
			if( rs.getString("Crs_SaleChnl") == null )
				this.Crs_SaleChnl = null;
			else
				this.Crs_SaleChnl = rs.getString("Crs_SaleChnl").trim();

			if( rs.getString("Crs_SaleType") == null )
				this.Crs_SaleType = null;
			else
				this.Crs_SaleType = rs.getString("Crs_SaleType").trim();

			if( rs.getString("GrpAgentCode") == null )
				this.GrpAgentCode = null;
			else
				this.GrpAgentCode = rs.getString("GrpAgentCode").trim();

			if( rs.getString("GrpAgentCom") == null )
				this.GrpAgentCom = null;
			else
				this.GrpAgentCom = rs.getString("GrpAgentCom").trim();

			if( rs.getString("GrpAgentName") == null )
				this.GrpAgentName = null;
			else
				this.GrpAgentName = rs.getString("GrpAgentName").trim();

			if( rs.getString("GrpAgentIDno") == null )
				this.GrpAgentIDno = null;
			else
				this.GrpAgentIDno = rs.getString("GrpAgentIDno").trim();

			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			if( rs.getString("Flag") == null )
				this.Flag = null;
			else
				this.Flag = rs.getString("Flag").trim();

			if( rs.getString("Other") == null )
				this.Other = null;
			else
				this.Other = rs.getString("Other").trim();

			if( rs.getString("MngCom") == null )
				this.MngCom = null;
			else
				this.MngCom = rs.getString("MngCom").trim();

			if( rs.getString("AccName") == null )
				this.AccName = null;
			else
				this.AccName = rs.getString("AccName").trim();

			if( rs.getString("AgentSaleCode") == null )
				this.AgentSaleCode = null;
			else
				this.AgentSaleCode = rs.getString("AgentSaleCode").trim();

			if( rs.getString("PostalProvince") == null )
				this.PostalProvince = null;
			else
				this.PostalProvince = rs.getString("PostalProvince").trim();

			if( rs.getString("PostalCity") == null )
				this.PostalCity = null;
			else
				this.PostalCity = rs.getString("PostalCity").trim();

			if( rs.getString("PostalCounty") == null )
				this.PostalCounty = null;
			else
				this.PostalCounty = rs.getString("PostalCounty").trim();

			if( rs.getString("PostalStreet") == null )
				this.PostalStreet = null;
			else
				this.PostalStreet = rs.getString("PostalStreet").trim();

			if( rs.getString("PostalCommunity") == null )
				this.PostalCommunity = null;
			else
				this.PostalCommunity = rs.getString("PostalCommunity").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的XFYJAppnt表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "XFYJAppntSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public XFYJAppntSchema getSchema()
	{
		XFYJAppntSchema aXFYJAppntSchema = new XFYJAppntSchema();
		aXFYJAppntSchema.setSchema(this);
		return aXFYJAppntSchema;
	}

	public XFYJAppntDB getDB()
	{
		XFYJAppntDB aDBOper = new XFYJAppntDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpXFYJAppnt描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(BatchNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PrtNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SaleChnl)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( PolapplyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ReceiveDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppntName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppntIDtype)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppntIDno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppntType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( IDStartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( IDEndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppntSex)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(FamilySalary));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( AppntBirthday ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OccupationCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Salary));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(WorkName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Position)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(NativePlace)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ZipCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Email)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Phone)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HomePhone)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Mobile)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PostAddress)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PayIntv));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayMode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DueFeeMsgFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Copys));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Crs_SaleChnl)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Crs_SaleType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpAgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpAgentCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpAgentName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpAgentIDno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Flag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Other)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MngCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentSaleCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PostalProvince)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PostalCity)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PostalCounty)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PostalStreet)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PostalCommunity));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpXFYJAppnt>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			BatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			AgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			AgentName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			PolapplyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,SysConst.PACKAGESPILTER));
			ReceiveDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,SysConst.PACKAGESPILTER));
			AppntName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			AppntIDtype = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			AppntIDno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			AppntType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			IDStartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,SysConst.PACKAGESPILTER));
			IDEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,SysConst.PACKAGESPILTER));
			AppntSex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			FamilySalary = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,17,SysConst.PACKAGESPILTER))).doubleValue();
			AppntBirthday = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,SysConst.PACKAGESPILTER));
			OccupationCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			Salary = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,20,SysConst.PACKAGESPILTER))).doubleValue();
			WorkName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			Position = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			NativePlace = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			ZipCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			Email = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			Phone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			HomePhone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			Mobile = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			PostAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			PayIntv= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,30,SysConst.PACKAGESPILTER))).intValue();
			PayMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
			BankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			DueFeeMsgFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			Copys = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,36,SysConst.PACKAGESPILTER))).doubleValue();
			Crs_SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
			Crs_SaleType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
			GrpAgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
			GrpAgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
			GrpAgentName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41, SysConst.PACKAGESPILTER );
			GrpAgentIDno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43, SysConst.PACKAGESPILTER );
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 45, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 46,SysConst.PACKAGESPILTER));
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 47,SysConst.PACKAGESPILTER));
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 48, SysConst.PACKAGESPILTER );
			Flag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 49, SysConst.PACKAGESPILTER );
			Other = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 50, SysConst.PACKAGESPILTER );
			MngCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 51, SysConst.PACKAGESPILTER );
			AccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 52, SysConst.PACKAGESPILTER );
			AgentSaleCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 53, SysConst.PACKAGESPILTER );
			PostalProvince = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 54, SysConst.PACKAGESPILTER );
			PostalCity = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 55, SysConst.PACKAGESPILTER );
			PostalCounty = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 56, SysConst.PACKAGESPILTER );
			PostalStreet = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 57, SysConst.PACKAGESPILTER );
			PostalCommunity = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 58, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "XFYJAppntSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("BatchNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BatchNo));
		}
		if (FCode.equals("PrtNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("SaleChnl"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
		}
		if (FCode.equals("AgentCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
		}
		if (FCode.equals("AgentCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
		}
		if (FCode.equals("AgentName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentName));
		}
		if (FCode.equals("PolapplyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getPolapplyDate()));
		}
		if (FCode.equals("ReceiveDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getReceiveDate()));
		}
		if (FCode.equals("AppntName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppntName));
		}
		if (FCode.equals("AppntIDtype"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppntIDtype));
		}
		if (FCode.equals("AppntIDno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppntIDno));
		}
		if (FCode.equals("AppntType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppntType));
		}
		if (FCode.equals("IDStartDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getIDStartDate()));
		}
		if (FCode.equals("IDEndDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getIDEndDate()));
		}
		if (FCode.equals("AppntSex"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppntSex));
		}
		if (FCode.equals("FamilySalary"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FamilySalary));
		}
		if (FCode.equals("AppntBirthday"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getAppntBirthday()));
		}
		if (FCode.equals("OccupationCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationCode));
		}
		if (FCode.equals("Salary"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Salary));
		}
		if (FCode.equals("WorkName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WorkName));
		}
		if (FCode.equals("Position"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Position));
		}
		if (FCode.equals("NativePlace"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(NativePlace));
		}
		if (FCode.equals("ZipCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ZipCode));
		}
		if (FCode.equals("Email"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Email));
		}
		if (FCode.equals("Phone"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Phone));
		}
		if (FCode.equals("HomePhone"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HomePhone));
		}
		if (FCode.equals("Mobile"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Mobile));
		}
		if (FCode.equals("PostAddress"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PostAddress));
		}
		if (FCode.equals("PayIntv"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayIntv));
		}
		if (FCode.equals("PayMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayMode));
		}
		if (FCode.equals("BankCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
		}
		if (FCode.equals("BankAccNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccNo));
		}
		if (FCode.equals("DueFeeMsgFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DueFeeMsgFlag));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (FCode.equals("Copys"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Copys));
		}
		if (FCode.equals("Crs_SaleChnl"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Crs_SaleChnl));
		}
		if (FCode.equals("Crs_SaleType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Crs_SaleType));
		}
		if (FCode.equals("GrpAgentCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpAgentCode));
		}
		if (FCode.equals("GrpAgentCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpAgentCom));
		}
		if (FCode.equals("GrpAgentName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpAgentName));
		}
		if (FCode.equals("GrpAgentIDno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpAgentIDno));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("Flag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Flag));
		}
		if (FCode.equals("Other"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Other));
		}
		if (FCode.equals("MngCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MngCom));
		}
		if (FCode.equals("AccName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccName));
		}
		if (FCode.equals("AgentSaleCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentSaleCode));
		}
		if (FCode.equals("PostalProvince"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PostalProvince));
		}
		if (FCode.equals("PostalCity"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PostalCity));
		}
		if (FCode.equals("PostalCounty"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PostalCounty));
		}
		if (FCode.equals("PostalStreet"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PostalStreet));
		}
		if (FCode.equals("PostalCommunity"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PostalCommunity));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(BatchNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(PrtNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(SaleChnl);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(AgentCom);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(AgentCode);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(AgentName);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getPolapplyDate()));
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getReceiveDate()));
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(AppntName);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(AppntIDtype);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(AppntIDno);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(AppntType);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getIDStartDate()));
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getIDEndDate()));
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(AppntSex);
				break;
			case 16:
				strFieldValue = String.valueOf(FamilySalary);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getAppntBirthday()));
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(OccupationCode);
				break;
			case 19:
				strFieldValue = String.valueOf(Salary);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(WorkName);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(Position);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(NativePlace);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(ZipCode);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(Email);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(Phone);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(HomePhone);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(Mobile);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(PostAddress);
				break;
			case 29:
				strFieldValue = String.valueOf(PayIntv);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(PayMode);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(BankCode);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(BankAccNo);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(DueFeeMsgFlag);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			case 35:
				strFieldValue = String.valueOf(Copys);
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(Crs_SaleChnl);
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(Crs_SaleType);
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(GrpAgentCode);
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(GrpAgentCom);
				break;
			case 40:
				strFieldValue = StrTool.GBKToUnicode(GrpAgentName);
				break;
			case 41:
				strFieldValue = StrTool.GBKToUnicode(GrpAgentIDno);
				break;
			case 42:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 43:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 44:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 45:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 46:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 47:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 48:
				strFieldValue = StrTool.GBKToUnicode(Flag);
				break;
			case 49:
				strFieldValue = StrTool.GBKToUnicode(Other);
				break;
			case 50:
				strFieldValue = StrTool.GBKToUnicode(MngCom);
				break;
			case 51:
				strFieldValue = StrTool.GBKToUnicode(AccName);
				break;
			case 52:
				strFieldValue = StrTool.GBKToUnicode(AgentSaleCode);
				break;
			case 53:
				strFieldValue = StrTool.GBKToUnicode(PostalProvince);
				break;
			case 54:
				strFieldValue = StrTool.GBKToUnicode(PostalCity);
				break;
			case 55:
				strFieldValue = StrTool.GBKToUnicode(PostalCounty);
				break;
			case 56:
				strFieldValue = StrTool.GBKToUnicode(PostalStreet);
				break;
			case 57:
				strFieldValue = StrTool.GBKToUnicode(PostalCommunity);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("BatchNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BatchNo = FValue.trim();
			}
			else
				BatchNo = null;
		}
		if (FCode.equalsIgnoreCase("PrtNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PrtNo = FValue.trim();
			}
			else
				PrtNo = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("SaleChnl"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SaleChnl = FValue.trim();
			}
			else
				SaleChnl = null;
		}
		if (FCode.equalsIgnoreCase("AgentCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCom = FValue.trim();
			}
			else
				AgentCom = null;
		}
		if (FCode.equalsIgnoreCase("AgentCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCode = FValue.trim();
			}
			else
				AgentCode = null;
		}
		if (FCode.equalsIgnoreCase("AgentName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentName = FValue.trim();
			}
			else
				AgentName = null;
		}
		if (FCode.equalsIgnoreCase("PolapplyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				PolapplyDate = fDate.getDate( FValue );
			}
			else
				PolapplyDate = null;
		}
		if (FCode.equalsIgnoreCase("ReceiveDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ReceiveDate = fDate.getDate( FValue );
			}
			else
				ReceiveDate = null;
		}
		if (FCode.equalsIgnoreCase("AppntName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppntName = FValue.trim();
			}
			else
				AppntName = null;
		}
		if (FCode.equalsIgnoreCase("AppntIDtype"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppntIDtype = FValue.trim();
			}
			else
				AppntIDtype = null;
		}
		if (FCode.equalsIgnoreCase("AppntIDno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppntIDno = FValue.trim();
			}
			else
				AppntIDno = null;
		}
		if (FCode.equalsIgnoreCase("AppntType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppntType = FValue.trim();
			}
			else
				AppntType = null;
		}
		if (FCode.equalsIgnoreCase("IDStartDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				IDStartDate = fDate.getDate( FValue );
			}
			else
				IDStartDate = null;
		}
		if (FCode.equalsIgnoreCase("IDEndDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				IDEndDate = fDate.getDate( FValue );
			}
			else
				IDEndDate = null;
		}
		if (FCode.equalsIgnoreCase("AppntSex"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppntSex = FValue.trim();
			}
			else
				AppntSex = null;
		}
		if (FCode.equalsIgnoreCase("FamilySalary"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				FamilySalary = d;
			}
		}
		if (FCode.equalsIgnoreCase("AppntBirthday"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				AppntBirthday = fDate.getDate( FValue );
			}
			else
				AppntBirthday = null;
		}
		if (FCode.equalsIgnoreCase("OccupationCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OccupationCode = FValue.trim();
			}
			else
				OccupationCode = null;
		}
		if (FCode.equalsIgnoreCase("Salary"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Salary = d;
			}
		}
		if (FCode.equalsIgnoreCase("WorkName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WorkName = FValue.trim();
			}
			else
				WorkName = null;
		}
		if (FCode.equalsIgnoreCase("Position"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Position = FValue.trim();
			}
			else
				Position = null;
		}
		if (FCode.equalsIgnoreCase("NativePlace"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				NativePlace = FValue.trim();
			}
			else
				NativePlace = null;
		}
		if (FCode.equalsIgnoreCase("ZipCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ZipCode = FValue.trim();
			}
			else
				ZipCode = null;
		}
		if (FCode.equalsIgnoreCase("Email"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Email = FValue.trim();
			}
			else
				Email = null;
		}
		if (FCode.equalsIgnoreCase("Phone"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Phone = FValue.trim();
			}
			else
				Phone = null;
		}
		if (FCode.equalsIgnoreCase("HomePhone"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HomePhone = FValue.trim();
			}
			else
				HomePhone = null;
		}
		if (FCode.equalsIgnoreCase("Mobile"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Mobile = FValue.trim();
			}
			else
				Mobile = null;
		}
		if (FCode.equalsIgnoreCase("PostAddress"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PostAddress = FValue.trim();
			}
			else
				PostAddress = null;
		}
		if (FCode.equalsIgnoreCase("PayIntv"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PayIntv = i;
			}
		}
		if (FCode.equalsIgnoreCase("PayMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayMode = FValue.trim();
			}
			else
				PayMode = null;
		}
		if (FCode.equalsIgnoreCase("BankCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankCode = FValue.trim();
			}
			else
				BankCode = null;
		}
		if (FCode.equalsIgnoreCase("BankAccNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankAccNo = FValue.trim();
			}
			else
				BankAccNo = null;
		}
		if (FCode.equalsIgnoreCase("DueFeeMsgFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DueFeeMsgFlag = FValue.trim();
			}
			else
				DueFeeMsgFlag = null;
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		if (FCode.equalsIgnoreCase("Copys"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Copys = d;
			}
		}
		if (FCode.equalsIgnoreCase("Crs_SaleChnl"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Crs_SaleChnl = FValue.trim();
			}
			else
				Crs_SaleChnl = null;
		}
		if (FCode.equalsIgnoreCase("Crs_SaleType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Crs_SaleType = FValue.trim();
			}
			else
				Crs_SaleType = null;
		}
		if (FCode.equalsIgnoreCase("GrpAgentCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpAgentCode = FValue.trim();
			}
			else
				GrpAgentCode = null;
		}
		if (FCode.equalsIgnoreCase("GrpAgentCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpAgentCom = FValue.trim();
			}
			else
				GrpAgentCom = null;
		}
		if (FCode.equalsIgnoreCase("GrpAgentName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpAgentName = FValue.trim();
			}
			else
				GrpAgentName = null;
		}
		if (FCode.equalsIgnoreCase("GrpAgentIDno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpAgentIDno = FValue.trim();
			}
			else
				GrpAgentIDno = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("Flag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Flag = FValue.trim();
			}
			else
				Flag = null;
		}
		if (FCode.equalsIgnoreCase("Other"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Other = FValue.trim();
			}
			else
				Other = null;
		}
		if (FCode.equalsIgnoreCase("MngCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MngCom = FValue.trim();
			}
			else
				MngCom = null;
		}
		if (FCode.equalsIgnoreCase("AccName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccName = FValue.trim();
			}
			else
				AccName = null;
		}
		if (FCode.equalsIgnoreCase("AgentSaleCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentSaleCode = FValue.trim();
			}
			else
				AgentSaleCode = null;
		}
		if (FCode.equalsIgnoreCase("PostalProvince"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PostalProvince = FValue.trim();
			}
			else
				PostalProvince = null;
		}
		if (FCode.equalsIgnoreCase("PostalCity"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PostalCity = FValue.trim();
			}
			else
				PostalCity = null;
		}
		if (FCode.equalsIgnoreCase("PostalCounty"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PostalCounty = FValue.trim();
			}
			else
				PostalCounty = null;
		}
		if (FCode.equalsIgnoreCase("PostalStreet"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PostalStreet = FValue.trim();
			}
			else
				PostalStreet = null;
		}
		if (FCode.equalsIgnoreCase("PostalCommunity"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PostalCommunity = FValue.trim();
			}
			else
				PostalCommunity = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		XFYJAppntSchema other = (XFYJAppntSchema)otherObject;
		return
			(BatchNo == null ? other.getBatchNo() == null : BatchNo.equals(other.getBatchNo()))
			&& (PrtNo == null ? other.getPrtNo() == null : PrtNo.equals(other.getPrtNo()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (SaleChnl == null ? other.getSaleChnl() == null : SaleChnl.equals(other.getSaleChnl()))
			&& (AgentCom == null ? other.getAgentCom() == null : AgentCom.equals(other.getAgentCom()))
			&& (AgentCode == null ? other.getAgentCode() == null : AgentCode.equals(other.getAgentCode()))
			&& (AgentName == null ? other.getAgentName() == null : AgentName.equals(other.getAgentName()))
			&& (PolapplyDate == null ? other.getPolapplyDate() == null : fDate.getString(PolapplyDate).equals(other.getPolapplyDate()))
			&& (ReceiveDate == null ? other.getReceiveDate() == null : fDate.getString(ReceiveDate).equals(other.getReceiveDate()))
			&& (AppntName == null ? other.getAppntName() == null : AppntName.equals(other.getAppntName()))
			&& (AppntIDtype == null ? other.getAppntIDtype() == null : AppntIDtype.equals(other.getAppntIDtype()))
			&& (AppntIDno == null ? other.getAppntIDno() == null : AppntIDno.equals(other.getAppntIDno()))
			&& (AppntType == null ? other.getAppntType() == null : AppntType.equals(other.getAppntType()))
			&& (IDStartDate == null ? other.getIDStartDate() == null : fDate.getString(IDStartDate).equals(other.getIDStartDate()))
			&& (IDEndDate == null ? other.getIDEndDate() == null : fDate.getString(IDEndDate).equals(other.getIDEndDate()))
			&& (AppntSex == null ? other.getAppntSex() == null : AppntSex.equals(other.getAppntSex()))
			&& FamilySalary == other.getFamilySalary()
			&& (AppntBirthday == null ? other.getAppntBirthday() == null : fDate.getString(AppntBirthday).equals(other.getAppntBirthday()))
			&& (OccupationCode == null ? other.getOccupationCode() == null : OccupationCode.equals(other.getOccupationCode()))
			&& Salary == other.getSalary()
			&& (WorkName == null ? other.getWorkName() == null : WorkName.equals(other.getWorkName()))
			&& (Position == null ? other.getPosition() == null : Position.equals(other.getPosition()))
			&& (NativePlace == null ? other.getNativePlace() == null : NativePlace.equals(other.getNativePlace()))
			&& (ZipCode == null ? other.getZipCode() == null : ZipCode.equals(other.getZipCode()))
			&& (Email == null ? other.getEmail() == null : Email.equals(other.getEmail()))
			&& (Phone == null ? other.getPhone() == null : Phone.equals(other.getPhone()))
			&& (HomePhone == null ? other.getHomePhone() == null : HomePhone.equals(other.getHomePhone()))
			&& (Mobile == null ? other.getMobile() == null : Mobile.equals(other.getMobile()))
			&& (PostAddress == null ? other.getPostAddress() == null : PostAddress.equals(other.getPostAddress()))
			&& PayIntv == other.getPayIntv()
			&& (PayMode == null ? other.getPayMode() == null : PayMode.equals(other.getPayMode()))
			&& (BankCode == null ? other.getBankCode() == null : BankCode.equals(other.getBankCode()))
			&& (BankAccNo == null ? other.getBankAccNo() == null : BankAccNo.equals(other.getBankAccNo()))
			&& (DueFeeMsgFlag == null ? other.getDueFeeMsgFlag() == null : DueFeeMsgFlag.equals(other.getDueFeeMsgFlag()))
			&& (Remark == null ? other.getRemark() == null : Remark.equals(other.getRemark()))
			&& Copys == other.getCopys()
			&& (Crs_SaleChnl == null ? other.getCrs_SaleChnl() == null : Crs_SaleChnl.equals(other.getCrs_SaleChnl()))
			&& (Crs_SaleType == null ? other.getCrs_SaleType() == null : Crs_SaleType.equals(other.getCrs_SaleType()))
			&& (GrpAgentCode == null ? other.getGrpAgentCode() == null : GrpAgentCode.equals(other.getGrpAgentCode()))
			&& (GrpAgentCom == null ? other.getGrpAgentCom() == null : GrpAgentCom.equals(other.getGrpAgentCom()))
			&& (GrpAgentName == null ? other.getGrpAgentName() == null : GrpAgentName.equals(other.getGrpAgentName()))
			&& (GrpAgentIDno == null ? other.getGrpAgentIDno() == null : GrpAgentIDno.equals(other.getGrpAgentIDno()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (State == null ? other.getState() == null : State.equals(other.getState()))
			&& (Flag == null ? other.getFlag() == null : Flag.equals(other.getFlag()))
			&& (Other == null ? other.getOther() == null : Other.equals(other.getOther()))
			&& (MngCom == null ? other.getMngCom() == null : MngCom.equals(other.getMngCom()))
			&& (AccName == null ? other.getAccName() == null : AccName.equals(other.getAccName()))
			&& (AgentSaleCode == null ? other.getAgentSaleCode() == null : AgentSaleCode.equals(other.getAgentSaleCode()))
			&& (PostalProvince == null ? other.getPostalProvince() == null : PostalProvince.equals(other.getPostalProvince()))
			&& (PostalCity == null ? other.getPostalCity() == null : PostalCity.equals(other.getPostalCity()))
			&& (PostalCounty == null ? other.getPostalCounty() == null : PostalCounty.equals(other.getPostalCounty()))
			&& (PostalStreet == null ? other.getPostalStreet() == null : PostalStreet.equals(other.getPostalStreet()))
			&& (PostalCommunity == null ? other.getPostalCommunity() == null : PostalCommunity.equals(other.getPostalCommunity()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("BatchNo") ) {
			return 0;
		}
		if( strFieldName.equals("PrtNo") ) {
			return 1;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 2;
		}
		if( strFieldName.equals("SaleChnl") ) {
			return 3;
		}
		if( strFieldName.equals("AgentCom") ) {
			return 4;
		}
		if( strFieldName.equals("AgentCode") ) {
			return 5;
		}
		if( strFieldName.equals("AgentName") ) {
			return 6;
		}
		if( strFieldName.equals("PolapplyDate") ) {
			return 7;
		}
		if( strFieldName.equals("ReceiveDate") ) {
			return 8;
		}
		if( strFieldName.equals("AppntName") ) {
			return 9;
		}
		if( strFieldName.equals("AppntIDtype") ) {
			return 10;
		}
		if( strFieldName.equals("AppntIDno") ) {
			return 11;
		}
		if( strFieldName.equals("AppntType") ) {
			return 12;
		}
		if( strFieldName.equals("IDStartDate") ) {
			return 13;
		}
		if( strFieldName.equals("IDEndDate") ) {
			return 14;
		}
		if( strFieldName.equals("AppntSex") ) {
			return 15;
		}
		if( strFieldName.equals("FamilySalary") ) {
			return 16;
		}
		if( strFieldName.equals("AppntBirthday") ) {
			return 17;
		}
		if( strFieldName.equals("OccupationCode") ) {
			return 18;
		}
		if( strFieldName.equals("Salary") ) {
			return 19;
		}
		if( strFieldName.equals("WorkName") ) {
			return 20;
		}
		if( strFieldName.equals("Position") ) {
			return 21;
		}
		if( strFieldName.equals("NativePlace") ) {
			return 22;
		}
		if( strFieldName.equals("ZipCode") ) {
			return 23;
		}
		if( strFieldName.equals("Email") ) {
			return 24;
		}
		if( strFieldName.equals("Phone") ) {
			return 25;
		}
		if( strFieldName.equals("HomePhone") ) {
			return 26;
		}
		if( strFieldName.equals("Mobile") ) {
			return 27;
		}
		if( strFieldName.equals("PostAddress") ) {
			return 28;
		}
		if( strFieldName.equals("PayIntv") ) {
			return 29;
		}
		if( strFieldName.equals("PayMode") ) {
			return 30;
		}
		if( strFieldName.equals("BankCode") ) {
			return 31;
		}
		if( strFieldName.equals("BankAccNo") ) {
			return 32;
		}
		if( strFieldName.equals("DueFeeMsgFlag") ) {
			return 33;
		}
		if( strFieldName.equals("Remark") ) {
			return 34;
		}
		if( strFieldName.equals("Copys") ) {
			return 35;
		}
		if( strFieldName.equals("Crs_SaleChnl") ) {
			return 36;
		}
		if( strFieldName.equals("Crs_SaleType") ) {
			return 37;
		}
		if( strFieldName.equals("GrpAgentCode") ) {
			return 38;
		}
		if( strFieldName.equals("GrpAgentCom") ) {
			return 39;
		}
		if( strFieldName.equals("GrpAgentName") ) {
			return 40;
		}
		if( strFieldName.equals("GrpAgentIDno") ) {
			return 41;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 42;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 43;
		}
		if( strFieldName.equals("Operator") ) {
			return 44;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 45;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 46;
		}
		if( strFieldName.equals("State") ) {
			return 47;
		}
		if( strFieldName.equals("Flag") ) {
			return 48;
		}
		if( strFieldName.equals("Other") ) {
			return 49;
		}
		if( strFieldName.equals("MngCom") ) {
			return 50;
		}
		if( strFieldName.equals("AccName") ) {
			return 51;
		}
		if( strFieldName.equals("AgentSaleCode") ) {
			return 52;
		}
		if( strFieldName.equals("PostalProvince") ) {
			return 53;
		}
		if( strFieldName.equals("PostalCity") ) {
			return 54;
		}
		if( strFieldName.equals("PostalCounty") ) {
			return 55;
		}
		if( strFieldName.equals("PostalStreet") ) {
			return 56;
		}
		if( strFieldName.equals("PostalCommunity") ) {
			return 57;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "BatchNo";
				break;
			case 1:
				strFieldName = "PrtNo";
				break;
			case 2:
				strFieldName = "ManageCom";
				break;
			case 3:
				strFieldName = "SaleChnl";
				break;
			case 4:
				strFieldName = "AgentCom";
				break;
			case 5:
				strFieldName = "AgentCode";
				break;
			case 6:
				strFieldName = "AgentName";
				break;
			case 7:
				strFieldName = "PolapplyDate";
				break;
			case 8:
				strFieldName = "ReceiveDate";
				break;
			case 9:
				strFieldName = "AppntName";
				break;
			case 10:
				strFieldName = "AppntIDtype";
				break;
			case 11:
				strFieldName = "AppntIDno";
				break;
			case 12:
				strFieldName = "AppntType";
				break;
			case 13:
				strFieldName = "IDStartDate";
				break;
			case 14:
				strFieldName = "IDEndDate";
				break;
			case 15:
				strFieldName = "AppntSex";
				break;
			case 16:
				strFieldName = "FamilySalary";
				break;
			case 17:
				strFieldName = "AppntBirthday";
				break;
			case 18:
				strFieldName = "OccupationCode";
				break;
			case 19:
				strFieldName = "Salary";
				break;
			case 20:
				strFieldName = "WorkName";
				break;
			case 21:
				strFieldName = "Position";
				break;
			case 22:
				strFieldName = "NativePlace";
				break;
			case 23:
				strFieldName = "ZipCode";
				break;
			case 24:
				strFieldName = "Email";
				break;
			case 25:
				strFieldName = "Phone";
				break;
			case 26:
				strFieldName = "HomePhone";
				break;
			case 27:
				strFieldName = "Mobile";
				break;
			case 28:
				strFieldName = "PostAddress";
				break;
			case 29:
				strFieldName = "PayIntv";
				break;
			case 30:
				strFieldName = "PayMode";
				break;
			case 31:
				strFieldName = "BankCode";
				break;
			case 32:
				strFieldName = "BankAccNo";
				break;
			case 33:
				strFieldName = "DueFeeMsgFlag";
				break;
			case 34:
				strFieldName = "Remark";
				break;
			case 35:
				strFieldName = "Copys";
				break;
			case 36:
				strFieldName = "Crs_SaleChnl";
				break;
			case 37:
				strFieldName = "Crs_SaleType";
				break;
			case 38:
				strFieldName = "GrpAgentCode";
				break;
			case 39:
				strFieldName = "GrpAgentCom";
				break;
			case 40:
				strFieldName = "GrpAgentName";
				break;
			case 41:
				strFieldName = "GrpAgentIDno";
				break;
			case 42:
				strFieldName = "MakeTime";
				break;
			case 43:
				strFieldName = "ModifyTime";
				break;
			case 44:
				strFieldName = "Operator";
				break;
			case 45:
				strFieldName = "MakeDate";
				break;
			case 46:
				strFieldName = "ModifyDate";
				break;
			case 47:
				strFieldName = "State";
				break;
			case 48:
				strFieldName = "Flag";
				break;
			case 49:
				strFieldName = "Other";
				break;
			case 50:
				strFieldName = "MngCom";
				break;
			case 51:
				strFieldName = "AccName";
				break;
			case 52:
				strFieldName = "AgentSaleCode";
				break;
			case 53:
				strFieldName = "PostalProvince";
				break;
			case 54:
				strFieldName = "PostalCity";
				break;
			case 55:
				strFieldName = "PostalCounty";
				break;
			case 56:
				strFieldName = "PostalStreet";
				break;
			case 57:
				strFieldName = "PostalCommunity";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("BatchNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PrtNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SaleChnl") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolapplyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ReceiveDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("AppntName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppntIDtype") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppntIDno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppntType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IDStartDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("IDEndDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("AppntSex") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FamilySalary") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("AppntBirthday") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("OccupationCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Salary") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("WorkName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Position") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("NativePlace") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ZipCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Email") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Phone") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HomePhone") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Mobile") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PostAddress") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayIntv") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("PayMode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankAccNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DueFeeMsgFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Copys") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Crs_SaleChnl") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Crs_SaleType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpAgentCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpAgentCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpAgentName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpAgentIDno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Flag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Other") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MngCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentSaleCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PostalProvince") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PostalCity") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PostalCounty") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PostalStreet") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PostalCommunity") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 8:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 14:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 17:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_INT;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 36:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 37:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 38:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 39:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 40:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 41:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 42:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 43:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 44:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 45:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 46:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 47:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 48:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 49:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 50:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 51:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 52:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 53:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 54:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 55:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 56:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 57:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
